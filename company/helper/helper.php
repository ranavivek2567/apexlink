<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:23 PM
 */
error_reporting(E_ALL);
ini_set("display_errors",1);
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //include_once ('../../constants.php');
    header('Location: ' . BASE_URL);
};

include(ROOT_URL . "/config.php");

/**
 * Function to print error logs.
 * @param $errorMessage
 */
function printErrorLog($errorMessage) {
    error_log('[' . date("Y-m-d h:i:s") . '] ' . $errorMessage . PHP_EOL, '3', ROOT_URL . "/company/apexCompany.log");
}

/**
 * Function to send mail through curl request.
 * @param $request
 * @return string
 */
function curlRequest($request) {
    try {
        $ch = curl_init();
        curl_setopt_array($ch, array(
            //CURLOPT_URL => 'http://rentconnect.net/apex-email/index.php/api/email',
            CURLOPT_URL => 'https://rentconnect.net/apex-email/index.php',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 200000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($request),
        ));
        $server_output = curl_exec($ch);
        $server_output = json_decode($server_output);

        return $server_output;
    } catch (Exception $exception) {
        return $exception->getMessage();
    }
}



/**
 * function for super admin user login
 * @author Deepak
 * @return array
 */
function getCountryCode($connection) {
    try {
        $db = $_POST['db'];
        $query = $connection->query("SELECT * FROM countries");
        $data = $query->fetchAll();

        if (!empty($data)) {
            return array('status' => 'success', 'data' => $data, 'message' => 'Data Retrieved Successfully.');
        }

        return array('status' => 'error', 'data' => $data, 'message' => 'No Data!');
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * Random String!
 * Use this endpoint to generate the eight number random string.
 * @author Parvesh
 * @param int $length
 * @success Success
 * @error (Exceptions) Validation of Model failed. See status.returnValues
 * @return string
 */
function randomString($length = 8) {
    $str = "";
    $length = 7;
    $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));

    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    $str1 = $str. mt_rand(0, 9);
    return $str1;
}

/**
 * Random String!
 * Use this endpoint to generate the random string.
 * @author Sarita
 * @param int $length
 * @success Success
 * @error (Exceptions) Validation of Model failed. See status.returnValues
 * @return string
 */
function randomTokenString($length) {
    $str = "";
    $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
        $rand = mt_rand(0, $max);
        $str .= $characters[$rand];
    }
    return $str;
}

/**
 * function to get post data array
 * @author Deepak
 * @param $post
 * @return array
 */
function postArray($post, $cond = 'false') {
    $data = [];
    foreach ($post as $key => $value) {

        if (!empty($value['value']) && isset($value['value']) && !is_array($value['value'])) {
            $dataValue = test_input($value['value']);
        } else {
            $dataValue = $value['value'];
        }
        $pos = strpos($value['name'], "@");

        $dataName = ($pos === 0) ? explode("@", $value['name']) : $value['name'];

        if ($cond == 'true') {
            if ($pos === 0) {
                $pos2 = strpos($dataName[1], "[");
                if(!empty($pos2)){
                    $dataName = explode("[", $dataName[1]);
                    $dataName = $dataName[0];
                } else {
                    $dataName = $dataName[1];
                }
                $data[$dataName] = $dataValue;
                continue;
            }
        }
        if ($pos !== 0) {
            if(!is_array($value['value'])) {
                $pos2 = strpos($dataName, "[");
                if(!empty($pos2)){
                    $dataName = explode("[", $dataName);
                    $dataName = $dataName[0];
                }
                if (isset($data[$dataName])) {
                    if (is_array($data[$dataName])) {
                        array_push($data[$dataName], $dataValue);
                    } else {
                        $previousValue = $data[$dataName];
                        $data[$dataName] = [];
                        array_push($data[$dataName], $previousValue);
                        array_push($data[$dataName], $dataValue);
                    }
                    continue;
                }
            }
            $data[$dataName] = $dataValue;
        }
    }
    return $data;
}
/**
 * function to serializedata to postArray
 * @author Deepak
 * @param $post
 * @return array
 */
function serializeToPostArray($post) {

    $data = [];
    $newData = [];
    parse_str($post, $data);
    foreach ($data as $key => $value) {
        array_push($newData, ['name' => $key, 'value' => $value]);
    }
    return $newData;
}

/**
 * Server side validation function
 * @author Deepak
 * @param $data
 * @return string
 */
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

/**
 * Server side validation function
 * @author Deepak
 * @param $data
 * @return array
 */
function createSqlColVal($data) {
    $columns = '';
    $columnsValues = '';
    foreach ($data as $key => $value) {
        $columns .= $key . ',';
        $columnsValues .= ':' . "$key" . ',';
    }
    $columns = substr_replace($columns, "", -1);
    $columnsValues = substr_replace($columnsValues, "", -1);
    $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
    return $sqlData;
}

/**
 * Column value pair for update query
 * @param $data
 * @return array
 */
function createSqlColValPair($data) {
    $columnsValuesPair = '';
    foreach ($data as $key => $value) {
        if ($key == 'c_id') {
            //do nothing
        } else {
            $columnsValuesPair .= $key . "='" . $value . "',";
        }
    }
    $columnsValuesPair = substr_replace($columnsValuesPair, "", -1);
    $sqlData = ['columnsValuesPair' => $columnsValuesPair];
    return $sqlData;
}
function createSqlColPropertyGroup($data) {
    $columnsValuesPair = '';
    foreach ($data as $key => $value) {
        if ($key == 'c_id') {
            //do nothing
        } else {
            $columnsValuesPair .= $key . '="' . $value . '",';
        }
    }
    $columnsValuesPair = substr_replace($columnsValuesPair, "", -1);
    $sqlData = ['columnsValuesPair' => $columnsValuesPair];
    return $sqlData;
}

/**
 * function to check validation array
 * @author Deepak
 * @param $data
 * @return bool
 */
function checkValidationArray($data) {
    foreach ($data as $key => $value) {
        if (!empty($value)) {
            return true;
        }
    }
    return false;
}

/**
 * Server side Validations
 * @author Deepak
 * @param $data
 * @param $db
 * @param array $required_array
 * @param array $maxlength_array
 * @param array $number_array
 * @param null $updateId
 * @param string $table
 * @return array
 */
function validation($data, $db, $required_array = [], $maxlength_array = [], $number_array = [], $updateId = null,$table='users') {
    $err_array = [];
//    print_r($required_array);die('aaaa');
    foreach ($data as $key => $value) {
        $errName = $key . 'Err';
        $err_array[$errName] = [];
        /*Required Validations*/
        if (in_array($key, $required_array)) {
//            print_r($value);die('aaaa');
            if ($value == '') {
//            if (empty($value)) {
                $colName = ucfirst(str_replace('_', ' ', $key));
                if($colName == 'ReportTo'){
                    $error = "This field is required.";
                } else {
                    $error = $colName . " is required.";
                }
                array_push($err_array[$errName], $error);
            }

        }
        /*Max length Validations*/
        if (array_key_exists($key, $maxlength_array)) {
            $length = strlen($value);
            if (!empty($value) && $length > $maxlength_array[$key]) {
                $error = 'Please enter less than ' . $maxlength_array[$key] . ' characters.';
                array_push($err_array[$errName], $error);
            }
        }
        //Number Validations
        if (in_array($key, $number_array)) {
            $number = is_numeric(str_replace('-', '', $value));
            if (!empty($value) && !$number) {
                $error = 'Only number are allowed!';
                array_push($err_array[$errName], $error);
            }
        }
        //email validation
        if ($key == 'email' && !empty($value)) {
            if (!valid_email($value)) {
                $error = "Invalid email format";
                array_push($err_array[$errName], $error);
            } else {
                $emailqry = "SELECT * FROM $table WHERE email='$value'";
                $emailData = $db->query($emailqry)->fetch();
                if (!empty($emailData)) {
                    if (!empty($updateId)) {
                        if ($updateId != $emailData['id']) {
                            $error = "* Email already exist!";
                            array_push($err_array[$errName], $error);
                        }
                    } else {
                        $error = "* Email already exist!";
                        array_push($err_array[$errName], $error);
                    }
                }
            }
        }

        //email validation
        if ($key == 'domain_name' && !empty($value)) {
            $domainqry = "SELECT domain_name FROM users WHERE domain_name='$value'";
            $domainData = $db->query($domainqry)->fetch();
            if (!empty($domainData)) {
                $error = "Domain already exist!";
                array_push($err_array[$errName], $error);
            }
        }

        if (empty($err_array[$errName])) {
            unset($err_array[$errName]);
        }
    }
    return $err_array;
}

function valid_email($str) {
    return (!preg_match("/^(([^<>()\[\]\\.,;:\s@\"]+(\.[^<>()\[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/", $str)) ? FALSE : TRUE;
}

function getCompanyConnection($companyid = null) {
    try {
        $db = new DBConnection();
        $dbDetails = $db->conn->query("SELECT * FROM users where id=" . $companyid)->fetch();
        $connection = DBConnection::dynamicDbConnection($dbDetails['host'], $dbDetails['database_name'], $dbDetails['db_username'], $dbDetails['db_password']);
        return $connection;
    } catch (Exception $exception) {
        return $exception->getMessage();
    }
}

function mySqlDateFormat($date, $id = null, $connection) {
    if ($id === null) {
        $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
    }
    $sql = "SELECT * FROM default_date_clock_settings WHERE user_id=" . $id;
    $dateFormatVal = $connection->query($sql)->fetch();
    if (!empty($dateFormatVal)) {
        $date_format_val = $dateFormatVal['date_format'];
    } else {
        $date_format_val = 1;
    }
    //$result = strstr($date, '(');
    $result = explode(" (", $date);
    $date = trim($result[0]);
    //New date Format data
    $value = $date_format_val;

    switch ($value) {
        case "1":
            $format = explode("/", $date);
            $month = $format[0];
            $day = $format[1];
            $year = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "2":
            $format = explode("-", $date);
            $month = $format[0];
            $day = $format[1];
            $year = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "3":
            $format = explode(".", $date);
            $month = $format[0];
            $day = $format[1];
            $year = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "4":
            $format = explode("/", $date);
            $day = $format[0];
            $month = $format[1];
            $year = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "5":
            $format = explode("-", $date);
            $day = $format[0];
            $month = $format[1];
            $year = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "6":
            $format = explode(".", $date);
            $day = $format[0];
            $month = $format[1];
            $year = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "7":
            $format = explode("/", $date);
            $year = $format[0];
            $month = $format[1];
            $day = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "8":
            $format = explode("-", $date);
            $year = $format[0];
            $month = $format[1];
            $day = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "9":
            $format = explode(".", $date);
            $year = $format[0];
            $month = $format[1];
            $day = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "10":
            $format = explode("/", $date);
            $year = $format[0];
            $day = $format[1];
            $month = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "11":
            $format = explode("-", $date);
            $year = $format[0];
            $day = $format[1];
            $month = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "12":
            $format = explode(".", $date);
            $year = $format[0];
            $day = $format[1];
            $month = $format[2];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "13":
            $newDate = str_replace(',', "", $date);
            $data = date('d/m/Y', strtotime($newDate));
            $format = explode("/", $data);
            $year = $format[2];
            $day = $format[0];
            $month = $format[1];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        case "14":
            $reformat = explode(",", $date);
            $reformat2 = explode(" ", $reformat[0]);
            $newDay = $reformat2[0];
            $newMonth = $reformat2[1];
            $newYear = trim($reformat[1]);
            $data = date('d/m/Y', strtotime($newMonth . '' . $newDay . ', ' . $newYear));
            $format = explode("/", $data);
            $year = $format[2];
            $day = $format[0];
            $month = $format[1];
            $formatDate = $year . '-' . $month . '-' . $day;
            break;
        default:
            $reformat = explode(",", $date);
            $reformat2 = explode(" ", $reformat[0]);
            $newDay = $reformat2[0];
            $newMonth = $reformat2[1];
            $newYear = trim($reformat[1]);
            $data = date('d/m/Y', strtotime($newMonth . '' . $newDay . ', ' . $newYear));
            $format = explode("/", $data);
            $year = $format[2];
            $day = $format[0];
            $month = $format[1];
            $formatDate = $year . '-' . $month . '-' . $day;
    }
    //convert date according to timezone
    $timezone = isset($_SESSION[SESSION_DOMAIN]['timezone'])?$_SESSION[SESSION_DOMAIN]['timezone']:null;
    if(!empty($timezone)){
        $tz = $timezone;
        $timestamp = time();
        $dt = new DateTime($formatDate, new DateTimeZone($tz)); //first argument "must" be a string
        // $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        return $dt->format('Y-m-d');
    }
//    $select_timeZone = $connection->query('SELECT * FROM company_time_setting LEFT JOIN timezone ON timezone.id=company_time_setting.timezone WHERE company_time_setting.user_id='.$id)->fetch();
//    if(!empty($select_timeZone['timezone'])){
//        $tz = $select_timeZone['timezone'];
//        $timestamp = time();
//        $dt = new DateTime($formatDate, new DateTimeZone($tz)); //first argument "must" be a string
//        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
//        return $dt->format('Y-m-d');
//    }
    return date('Y-m-d', strtotime($formatDate));
}

function dateFormatUser($date, $id = null, $connection) {
    try {
        if ($date == '') {
            return $date;
        }
        if ($id === null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $timezone = isset($_SESSION[SESSION_DOMAIN]['timezone'])?$_SESSION[SESSION_DOMAIN]['timezone']:null;
        if(!empty($timezone)){
            $tz = $timezone;
            $timestamp = time();
            $dt = new DateTime($date, new DateTimeZone($tz)); //first argument "must" be a string
            // $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
            $date = $dt->format('Y-m-d');
        }
//        $select_timeZone = $connection->query('SELECT * FROM company_time_setting LEFT JOIN timezone ON timezone.id=company_time_setting.timezone WHERE company_time_setting.user_id='.$id)->fetch();
//        if(isset($select_timeZone['timezone']) && !empty($select_timeZone['timezone'])){
//            $tz = $select_timeZone['timezone'];
//            $timestamp = time();
//            $dt = new DateTime($date, new DateTimeZone($tz)); //first argument "must" be a string
//            $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
//            $date = $dt->format('Y-m-d');
//        }

        $sql = "SELECT * FROM default_date_clock_settings WHERE user_id=" . $id;
        $dateFormatVal = $connection->query($sql)->fetch();

        if (is_array($dateFormatVal) && count($dateFormatVal) > 0) {
            $date_format_val = $dateFormatVal['date_format'];
        } else {
            $date_format_val = 1;
        }

        $pos = strpos($date, "(");
        $pos2 = strpos($date, "-");

        if ($pos !== false) {
            $date = str_replace(array('(', ')'), "", $date);
        }
//        dd($date);
        $dat = strtotime($date);
        $date_created = date_create($date);
        //  dd($date_created);
        $weekendDay = date_format($date_created, 'd-m-Y');
        $dateString = strtotime($weekendDay);
        $day = date('l', $dateString);
        if (!empty($day)) {
            $day = substr($day, 0, 3);
            $day = $day . ".";
        }

        $value = $date_format_val;
        switch ($value) {
            case "1":
                $formatDate = date('m/d/Y', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "2":
                $formatDate = date('m-d-Y', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "3":
                $formatDate = date('m.d.Y', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "4":
                $formatDate = date('d/m/Y', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "5":
                $formatDate = date('d-m-Y', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "6":
                $formatDate = date('d.m.Y', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "7":
                $formatDate = date('Y/m/d', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "8":
                $formatDate = date('Y-m-d', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "9":
                $formatDate = date('Y.m.d', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "10":
                $formatDate = date('Y/d/m', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "11":
                $formatDate = date('Y-d-m', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "12":
                $formatDate = date('Y.d.m', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "13":
                $formatDate = date('M d, Y', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            case "14":
                $formatDate = date('d M, Y', $dateString);
                $date = $formatDate . ' (' . $day . ')';
                break;
            default:
                $formatDate = date('d F, Y', $dateString);
                $date = $formatDate . ' (' . $day . ')';
        }
        return $date;
    } catch (Exception $exception) {

    }
}

/**
 * Get date format for datepicker of user.
 * @param null $id
 * @return string
 */
function getDatePickerFormat($id = null, $connection) {
    if ($id === null) {
        $id = $_SESSION[SESSION_DOMAIN]['user_id'];
    }

    $sql = "SELECT * FROM default_date_clock_settings WHERE user_id=" . $id;
    $dateFormatVal = $connection->query($sql)->fetch();
    if (is_array($dateFormatVal) && count($dateFormatVal) > 0) {
        $date_format_val = $dateFormatVal['date_format'];
    } else {
        $date_format_val = 1;
    }
    $value = $date_format_val;
    switch ($value) {
        case "1":
            $date = 'm/d/yy (D.)';
            break;
        case "2":
            $date = 'm-d-yy (D.)';
            break;
        case "3":
            $date = 'm.d.yy (D.)';
            break;
        case "4":
            $date = 'd/m/yy (D.)';
            break;
        case "5":
            $date = 'd-m-yy (D.)';
            break;
        case "6":
            $date = 'd.m.yy (D.)';
            break;
        case "7":
            $date = 'yy/m/d (D.)';
            break;
        case "8":
            $date = 'yy-m-d (D.)';
            break;
        case "9":
            $date = 'yy.m.d (D.)';
            break;
        case "10":
            $date = 'yy/d/m (D.)';
            break;
        case "11":
            $date = 'yy-d-m (D.)';
            break;
        case "12":
            $date = 'yy.d.m (D.)';
            break;
        case "13":
            $date = 'M d, yy (D.)';
            break;
        case "14":
            $date = 'd M, yy (D.)';
            break;
        default:
            $date = 'd F, yy (D.)';
    }
    return $date;
}

/**
 * Function to get the company ID
 * @return string
 */
function CompanyId($connection) {
    $user = $connection->query("SELECT * FROM users where parent_id=0")->fetch();
    if (!empty($user)) {
        return ['msg' => 'Record retrieved Successfully.', 'data' => $user['id'], 'code' => 200];
    }
    return ['msg' => 'No Record Found!', 'code' => 400];
}

/**
 * function to get username
 * @param null $id
 * @param $table
 * @return string
 */
function userName($id = null, $connection,$table=null) {
    if ($id === null) {
        $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
    }
    $table = (!empty($table))?$table:'users';

    $sql = "SELECT * FROM $table WHERE id=" . $id;
    $data = $connection->query($sql)->fetch();
    //prefrence
    $sql2 = "SELECT * FROM company_preference WHERE user_id=1";
    $prefrenceData = $connection->query($sql2)->fetch();
    $name = !empty($_SESSION[SESSION_DOMAIN]['default_name']);
    //user name
    if (!empty($data) && count($data) > 0) {
        $nickName = !empty($data['nick_name']) ? $data['nick_name'] : null;
        $firstName = !empty($data['first_name']) ? $data['first_name'] : null;
        $lastName = !empty($data['last_name']) ? $data['last_name'] : null;
        if (!empty($data['nick_name'])) {
            if (!empty($prefrenceData['show_last_name']) && $prefrenceData['show_last_name'] == '1') {
                $name = ucfirst($lastName) . ' ' . ucfirst($nickName);
            } else {
                $name = ucfirst($nickName) . ' ' . ucfirst($lastName);
            }
        } else {
            if (!empty($prefrenceData['show_last_name']) && $prefrenceData['show_last_name'] == '1') {
                $name = ucfirst($lastName) . ' ' . ucfirst($firstName);
            } else {
                $name = ucfirst($firstName) . ' ' . ucfirst($lastName);
            }
        }
    }
    return $name;
}

/**
 * function to update name with username fucnction and update in DB
 * @param $id
 * @param $connection
 * @return array
 */
function updateUsername($id,$connection){
    $username = username($id,$connection);
    $data =[];
    $data['name'] = $username;
    $columnValue = createSqlUpdateCase($data);
    $data = $connection->prepare("UPDATE users SET " . $columnValue['columnsValuesPair'] ."WHERE id =".$id)->execute($columnValue['data'] );
    if($data){
        return ['msg' => 'Name updated Successfully.', 'data' => $username, 'code' => 200];
    } else {
        return ['msg' => 'error.', 'data' => $username, 'code' => 500];
    }
}

/**
 * @param null $id
 * @param $connection
 * @return string
 */
function lastNameFirst($email, $connection) {
    $sql = "SELECT * FROM users WHERE email=" . "'" . $email . "'";
    $data = $connection->query($sql)->fetch();
    $name = $_SESSION[SESSION_DOMAIN]['default_name'];
    //user name
    if(!empty($data) && count($data) > 0){
        $firstName = !empty($data['first_name'])?$data['first_name']:null;
        $lastName = !empty($data['last_name'])?$data['last_name']:null;
        if(!empty($data['last_name'])){
            $name = ucfirst($lastName).', '.ucfirst($firstName);
        }
    }
    return $name;
}

/**
 * Function to update custom field is_editable and is_updateble
 * @param $connection
 * @param $columns
 * @param $id
 * @return bool
 */
function updateCustomField($connection, $columns, $id) {
    $data = $connection->prepare("UPDATE custom_fields SET " . $columns['columnsValuesPair'] . " where id=" . $id)->execute();
    if ($data) {
        return true;
    }
    return false;
}

/**
 * Function to update all is_default to 0
 * @param $connection
 * @param $columns
 * @param $table
 * @return bool
 */
function updateDefaultField($connection, $columns, $table) {
    $data = $connection->prepare("UPDATE " . $table . " SET " . $columns['columnsValuesPair'])->execute();
    if ($data) {
        return true;
    }
    return false;
}

/**
 * function to get single record from table
 * @param $connection
 * @param $id
 * @param $table
 * @return array
 */
function getSingleRecord($connection, $condition, $table) {
    $data = $connection->query("SELECT * FROM " . $table . " WHERE " . $condition['column'] . "= '" . $condition['value'] . "' AND deleted_at IS NULL")->fetch();
    if (!empty($data)) {
        return ['msg' => 'Record retrieved Successfully.', 'data' => $data, 'code' => 200];
    }
    return ['msg' => 'No Record Found!', 'code' => 400];
}

/**
 * function to get single record from table
 * @param $connection
 * @param $id
 * @param $table
 * @return array
 */
function getSingleWithAndConditionRecord($connection, $condition, $table,$and_condition) {
    $data = $connection->query("SELECT * FROM " . $table . " WHERE " . $condition['column'] . "= '" . $condition['value'] . "' AND " .$and_condition['column']."= '". $and_condition['value']."' AND deleted_at IS NULL")->fetch();

//    dd("SELECT * FROM " . $table . " WHERE " . $condition['column'] . "= '" . $condition['value'] . "' AND " .$and_condition['column']."= '". $and_condition['value']."' AND deleted_at IS NULL");
    if (!empty($data)) {
        return ['msg' => 'Record retrieved Successfully.', 'data' => $data, 'code' => 200];
    }
    return ['msg' => 'No Record Found!', 'code' => 400];
}

/**
 * Function for checking data already exists or not
 * @param $connection
 * @param $table
 * @param $column_name
 * @param $column_value
 * @param $edit_id
 * @return array
 */

function checkNameAlreadyExists($connection, $table, $column_name, $column_value, $edit_id) {
    try {
        if ($edit_id) {
            $data = $connection->query("SELECT * FROM " . $table . " WHERE " . $column_name . " = '" . $column_value . "' AND id != " . $edit_id)->fetch();
        } else {
            $data = $connection->query("SELECT * FROM " . $table . " WHERE " . $column_name . " = '" . $column_value . "'")->fetch();
        }


        if ($data) {
            return ['is_exists' => 1, 'data' => $data];
        } else {
            return ['is_exists' => 0];
        }
    } catch (PDOException $e) {
        return json_encode(array('code' => 503, 'status' => 'failed', 'message' => $e->getMessage()));
    }
}

function checkMultipleDuplicates($companyConnection, $email = "", $phoneNumber = "", $ssn, $driveLicence = "", $propertyParcel = "",$vin =""){
    try{
        if ($email != ""){
            $getData = $companyConnection->query('SELECT email FROM users where email="'.$email.'"')->fetch();
            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Email, this email already Exists!"));
            }
            // return true;
        }
        if ($phoneNumber != ""){
            $getData=[];
            $getData1 = $companyConnection->query('SELECT phone_number FROM users where phone_number="'.$phoneNumber.'"')->fetch();
            if(!empty($getData1)){
                array_push($getData,$getData1);
            }
            $getData2 = $companyConnection->query('SELECT phone_number FROM general_property')->fetchAll();
            for($i=0;$i<count($getData2);$i++){
                if (!empty($getData2[$i])){
                    $prop_phone=unserialize($getData2[$i]['phone_number']);
                    if(is_array($prop_phone)){
                        for($j=0;$j<count($prop_phone);$j++){
                            if($prop_phone[$j]==$phoneNumber){
                                array_push($getData,$prop_phone[$j]);
                            }
                        }
                    }
                }
            }
            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Phone Number, this number already Exists!"));
            }else{
            }
        }
        if (!empty($ssn)){
            if (is_array($ssn)){
                foreach ($ssn as $ss){
                    $regVariable = '.*;s:[0-9]+:"'.$ss.'".*';
                    $regVariable = "'".$regVariable."'";

                    $getData = $companyConnection->query("SELECT * FROM users WHERE ssn_sin_id REGEXP ".$regVariable)->fetch();
                    //  dd("SELECT * FROM users WHERE ssn_sin_id REGEXP ".$regVariable);
                    if (!empty($getData)){
                        return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate SSN/SIN/ID, this Number already Exists!"));
                    }
                }
            } else{
                $regVariable = '.*;s:[0-9]+:"'.$ssn.'".*';
                $getData = $companyConnection->query("SELECT * FROM users WHERE ssn_sin_id REGEXP $regVariable")->fetch();
                if (!empty($getData)){
                    return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate SSN/SIN/ID, this Number already Exists!"));
                }
            }
        }
        if ($driveLicence != ""){
            $getData = $companyConnection->query('SELECT id FROM tenant_details WHERE tenant_license_number ="'.$driveLicence.'"')->fetch();
            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Driver’s License Number, this number already Exists!"));
            }else{
                $getData1 = $companyConnection->query('SELECT id FROM employee_details WHERE employee_license_number ="'.$driveLicence.'"')->fetch();
                if (!empty($getData1)){
                    return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Driver’s License Number, this number already Exists!"));
                }
                //  return true;
            }
        }
        if ($propertyParcel != ""){
            $getData = $companyConnection->query('SELECT id FROM general_property where property_parcel_number="'.$propertyParcel.'"')->fetch();
            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Property Parcel Number, this number already Exists!"));
            }
            //  return true;
        }
        if ($vin != ""){
            $getData=[];
            $getData1 = $companyConnection->query('SELECT id FROM tenant_vehicles where vin="'.$vin.'"')->fetch();
            if(!empty($getData1)){
                array_push($getData,$getData1);
            }
            $getData2 = $companyConnection->query('SELECT id FROM company_manage_vehicle where vin="'.$vin.'"')->fetch();
            if(!empty($getData2)){
                array_push($getData,$getData2);
            }
            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Vehicle ID Number, this VIN number already Exists!"));
            }
            //  return true;
        }
    } catch (PDOException $e) {
        return json_encode(array('code' => 503, 'status' => 'failed', 'message' => $e->getMessage()));
    }
}

function checkMultipleDuplicatesEdit($companyConnection, $email = "", $phoneNumber = "", $ssn, $driveLicence = "", $propertyParcel = "",$vin ="",$id) {
    try{
        if ($email != ""){
            $getData = $companyConnection->query('SELECT email FROM users where email="'.$email.'" AND id!='.$id)->fetch();

            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Email, this email already Exists!"));
            }
            // return true;
        }
        if ($phoneNumber != ""){
            $getData=[];
            $getData1 = $companyConnection->query('SELECT phone_number FROM users where phone_number="'.$phoneNumber.'" AND id!='.$id)->fetch();
            // dd($getData1);
            if(!empty($getData1)){
                array_push($getData,$getData1);
            }
            $getData2 = $companyConnection->query('SELECT phone_number FROM general_property WHERE id!='.$id)->fetchAll();
            for($i=0;$i<count($getData2);$i++){
                if (!empty($getData2[$i])){
                    $prop_phone=unserialize($getData2[$i]['phone_number']);
                    if(is_array($prop_phone)){
                        for($j=0;$j<count($prop_phone);$j++){
                            if($prop_phone[$j]==$phoneNumber){
                                array_push($getData,$prop_phone[$j]);
                            }
                        }
                    }
                }
            }
            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Phone Number, this number already Exists!"));
            }
        }
        if (!empty($ssn)){
            if (is_array($ssn)){
                foreach ($ssn as $ss){
                    $regVariable = '.*;s:[0-9]+:"'.$ss.'".*';
                    $regVariable = "'".$regVariable."'";

                    $getData = $companyConnection->query("SELECT * FROM users WHERE ssn_sin_id REGEXP ".$regVariable." AND id!=".$id)->fetch();
                    //  dd("SELECT * FROM users WHERE ssn_sin_id REGEXP ".$regVariable);
                    if (!empty($getData)){
                        return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate SSN/SIN/ID, this Number already Exists!"));
                    }
                }
            } else{
                $regVariable = '.*;s:[0-9]+:"'.$ssn.'".*';
                $getData = $companyConnection->query("SELECT * FROM users WHERE ssn_sin_id REGEXP ".$regVariable." AND id!=".$id )->fetch();
                if (!empty($getData)){
                    return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate SSN/SIN/ID, this Number already Exists!"));
                }
            }
        }
        if ($driveLicence != ""){
            $getData = $companyConnection->query('SELECT id FROM tenant_details WHERE tenant_license_number ="'.$driveLicence.'" AND id!='.$id)->fetch();
            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Driver’s License Number, this number already Exists!"));
            }else{
                $getData1 = $companyConnection->query('SELECT id FROM employee_details WHERE employee_license_number ="'.$driveLicence.'" AND id!='.$id)->fetch();
                if (!empty($getData1)){
                    return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Driver’s License Number, this number already Exists!"));
                }
                //  return true;
            }
        }
        if ($propertyParcel != ""){
            $getData = $companyConnection->query('SELECT id FROM general_property where property_parcel_number="'.$propertyParcel.'" AND id!='.$id)->fetch();
            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Property Parcel Number, this number already Exists!"));
            }
            //  return true;
        }
        if ($vin != ""){
            $getData=[];
            $getData1 = $companyConnection->query('SELECT id FROM tenant_vehicles where vin="'.$vin.'" AND id!='.$id)->fetch();
            if(!empty($getData1)){
                array_push($getData,$getData1);
            }
            $getData2 = $companyConnection->query('SELECT id FROM company_manage_vehicle where vin="'.$vin.'" AND id!='.$id)->fetch();
            if(!empty($getData2)){
                array_push($getData,$getData2);
            }
            if (!empty($getData)){
                return (array('code' => 503, 'status' => 'failed', 'message' => "Duplicate Vehicle ID Number, this VIN number already Exists!"));
            }
            //  return true;
        }
    } catch (PDOException $e) {
        return json_encode(array('code' => 503, 'status' => 'failed', 'message' => $e->getMessage()));
    }

}
function checkSerializeOrNot($ssn){
    $data = @unserialize($ssn);
    if($data !== false || $ssn === 'b:0;')
        return true;
    else
        return false;
}
/**
 * Function for checking data already exists or not
 * @param $connection
 * @param $table
 * @param $column_name
 * @param $column_value
 * @param $edit_id
 * @return array
 */
function checkDuplicateRecord($connection, $table, $columns,$where,$ssn=array(), $edit_id) {
    try {

        $columnsString = '';
        $whereSting = " WHERE (";
        if(!empty($columns)){
            foreach ($columns as $key =>$value){
                $columnsString .= $value.',';
            }
            $columnsString = str_replace_last(',', '', $columnsString);
        }
        foreach ($where as $key =>$value) {
            if ($key > 0) $whereSting .= ' AND';
            if($value['column']=='dob' &&  empty($value['value'])){
                $whereSting .= " " . $value['table'] . "." . $value['column'] . " IS NULL";
            }else {
                $whereSting .= " " . $value['table'] . "." . $value['column'] . " " . $value['condition'] . " '" . $value['value'] . "'";
            }
        }
        $whereSting .= ")";
        $query =  "SELECT ".$columnsString." FROM ".$table." ".$whereSting;
        if ($edit_id) {
            $data = $connection->query($query. " AND id != " . $edit_id)->fetch();
            if(empty($data)){

                $ssn_ids_new_array = [];
                $query1 = "Select ssn_sin_id,id FROM users WHERE deleted_at is NULL AND id != " . $edit_id;

                $data1 = $connection->query($query1)->fetchAll();
                if(!empty($data1['ssn_sin_id'])){
                    foreach($data1 as $key1 => $ssn_ids_array){
                        $ssn_ids_new_array= unserialize($ssn_ids_array["ssn_sin_id"]);
                        if(!empty($ssn_ids_array["ssn_sin_id"] && !empty($ssn) && is_array($ssn))){
                            $ssn_ids_new_array= unserialize($ssn_ids_array["ssn_sin_id"]);
                            if(count(array_intersect($ssn, $ssn_ids_new_array)) > 0){
                                return ['is_exists' => 1, 'data' => $data];
                            }
                        }
                    }}
                return ['is_exists' => 0, 'data' => $data];
            }else{
                return ['is_exists' => 1];
            }
        } else {
            if (isset($ssn[0]) && $ssn[0] == ""){
                return ['is_exists' => 0, 'data' => ''];
            }
            $data = $connection->query($query)->fetch();
            if(empty($data)){
                $ssn_ids_new_array = [];
                $query1 = "Select ssn_sin_id,id FROM users WHERE deleted_at is NULL";
                $data1 = $connection->query($query1)->fetchAll();
                foreach($data1 as $key1 => $ssn_ids_array){

                    if(!empty($ssn_ids_array["ssn_sin_id"]) && !empty($ssn) && is_array($ssn)){
                        $ssn_ids_new_array= unserialize($ssn_ids_array["ssn_sin_id"]);
                        if (count(array_intersect($ssn, $ssn_ids_new_array)) > 0) {
                            return ['is_exists' => 1, 'data' => $data];
                            // all of $target is in $haystack
                        }
                    }
                }
                return ['is_exists' => 0, 'data' => $data];
            }else{
                return ['is_exists' => 1];
            }
        }
    } catch (PDOException $e) {
        return json_encode(array('code' => 503, 'status' => 'failed', 'message' => $e->getMessage()));
    }
}

function in_array_all($needles, $haystack) {
    //dd($needles);
//    dd($haystack);
    dd(array_diff($needles, $haystack));
    return empty(array_diff($needles, $haystack));
}



/**
 * Function for fetching data based on id
 * @param $connection
 * @param $table
 * @param $id
 * @return array
 */
function getDataById($connection, $table, $id) {
    try {
        $property_subtype_id = $id;
        $data = $connection->query("SELECT * FROM " . $table . " WHERE id =" . $property_subtype_id)->fetch();
        return ['code' => 200, 'status' => 'success', 'data' => $data];
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * Function for fetching data based on id
 * @param $connection
 * @param $table
 * @param $id
 * @return array
 */
function getCurrencyData($connection) {
    try {
        $data = $connection->query("SELECT * FROM default_currency")->fetchAll();
        return ['code' => 200, 'status' => 'success', 'data' => $data];
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * Function for fetching Role based on user_type
 * @param $role
 * @return string
 */
function getRole($role) {
    try {
        switch ($role) {
            case "0":
                $roleName = 'Super Admin';
                break;
            case "1":
                $roleName = 'Company User';
                break;
            case "2":
                $roleName = 'Tenant';
                break;
            case "3":
                $roleName = 'Vendor';
                break;
            case "4":
                $roleName = 'Owner';
                break;
            case "5":
                $roleName = 'Contact';
                break;
            case "6":
                $roleName = 'Guest';
                break;
            case "7":
                $roleName = 'Rental';
                break;
            case "8":
                $roleName = 'Employee';
                break;
            case "9":
                $roleName = 'Manager';
                break;
            default:
                $roleName = 'None';
        }
        return $roleName;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * Function for fetching company_logo
 * @param $role
 * @return string
 */
function getCompanyLogo($connection) {
    try {
        $company_id = CompanyId($connection);
        $company = getSingleRecord($connection, ['column' => 'id', 'value' => @$company_id['data']], 'users');
        if ($company['code'] == 200 && !empty($company['data']['company_logo'])) {
            $company_logo = '/' . $company['data']['company_logo'];
        } else {
            $company_logo = '/images/logo.png';
        }
        return $company_logo;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to convert date time according to selected timezone
 * @param null $id
 * @param $date
 * @param $connection
 * @return false|string
 * @throws Exception
 */
function convertTimeZoneDefaultSeting($id = null, $date, $connection) {
    if ($id == null) {
        $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
    }
    $newdate = new DateTime($date);
    $newdate->setTimezone(new DateTimeZone('UTC'));
    $newdate->format('Y-m-d H:i:s'); // 2011-11-10 15:17:23 -0500
    $cdate = date_timestamp_get($newdate);
    $default_setting_date = $connection->query("SELECT * FROM company_time_setting JOIN timezone ON company_time_setting.timezone=timezone.id WHERE user_id=$id")->fetch();

    if (!empty($default_setting_date)) {
        if ($default_setting_date['hours_calc'] == 'sub') {
            $cdate = $cdate - ( $default_setting_date['hours_gap'] * 60 * 60);
        } else if ($default_setting_date['hours_calc'] == 'add') {
            $cdate = $cdate + ( $default_setting_date['hours_gap'] * 60 * 60);
        } else {
            $cdate = ( $default_setting_date['hours_gap'] * 60 * 60);
        }
    }
    $timestamp_date = date("Y-m-d H:i:s", $cdate);
    return $timestamp_date;
}

/**
 * function to get default pagination
 * @param null $id
 * @param $connection
 * @return array
 */
function pagination($id = null, $connection) {
    try {
        if ($id == null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $data = $connection->query("SELECT * FROM default_settings WHERE user_id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['page_size'];
        } else {
            return '10';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get default rent
 * @param null $id
 * @param $connection
 * @return string
 */
function defaultRent($id = null, $connection) {
    try {
        if ($id == null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $data = $connection->query("SELECT * FROM default_settings WHERE user_id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['default_rent'];
        } else {
            return '0.00';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get default notice period
 * @param null $id
 * @param $connection
 * @return string
 */
function defaultNoticePeriod($id = null, $connection) {
    try {
        if ($id == null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $data = $connection->query("SELECT * FROM default_settings WHERE user_id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['notice_period'];
        } else {
            return '0';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get default application fee
 * @param null $id
 * @param $connection
 * @return string
 */
function defaultApplicationfee($id = null, $connection) {
    try {
        if ($id == null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $data = $connection->query("SELECT * FROM default_settings WHERE user_id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['application_fees'];
        } else {
            return '0.00';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get default zip code
 * @param null $id
 * @param $connection
 * @return string
 */
function defaultZipCode($id = null, $connection) {
    try {
        if ($id == null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $data = $connection->query("SELECT * FROM default_settings WHERE user_id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['zip_code'];
        } else {
            return '10001';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get default city
 * @param null $id
 * @param $connection
 * @return string
 */
function defaultCity($id = null, $connection) {
    try {
        if ($id == null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $data = $connection->query("SELECT * FROM default_settings WHERE user_id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['city'];
        } else {
            return 'New york';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get default currency
 * @param null $id
 * @param $connection
 * @return string
 */
function defaultCurrency($id = null, $connection) {
    try {
        if ($id == null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $data = $connection->query("SELECT * FROM default_settings WHERE user_id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['currency'];
        } else {
            return '174';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get default currency symbol
 * @param $id
 * @param $connection
 * @return string
 */
function defaultCurrencySymbol($id, $connection) {
    try {
        $data = $connection->query("SELECT * FROM default_currency WHERE id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['symbol'];
        } else {
            return 'US$';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get default currency symbol
 * @param $id
 * @param $connection
 * @return string
 */
function defaultPaymentMethod($id = null, $connection) {
    try {
        if ($id == null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $data = $connection->query("SELECT * FROM default_settings WHERE user_id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['payment_method'];
        } else {
            return '1';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function defaultPropertySize($id = null, $connection) {
    try {
        if ($id == null) {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $data = $connection->query("SELECT * FROM default_settings WHERE user_id =" . $id)->fetch();
        if (!empty($data)) {
            return $data['property_size'];
        } else {
            return '1';
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to count record in table
 * @param $connection
 * @param $table
 * @return mixed
 */
function getRecordCount($connection, $table) {
    try {
        $data = $connection->query("SELECT COUNT(*) FROM $table WHERE deleted_at IS NULL")->fetch();
        return $data['COUNT(*)'];
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to print formated data
 * @param $data
 */
function dd($data) {
    try {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        die;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

/**
 * function to get dynamic domain name
 * @return mixed
 */
function getDomain() {
    try {
        $domain = explode('.', $_SERVER['HTTP_HOST']);
        return $domain[0];
    } catch (PDOException $e) {

    }
}

function lastLogin($id = null, $connection) {
//    $db = new DBConnection();
//    $dbDetails = $connection->query("SELECT * FROM users where id=" . $id)->fetch();
//    $connection = DBConnection::dynamicDbConnection($dbDetails['host'], $dbDetails['database_name'], $dbDetails['db_username'], $dbDetails['db_password']);
    $data = $connection->query("SELECT * FROM audit_trial where action='login' order by id desc ")->fetch();

    return dateFormatUser($data['created_at'], $id, $connection);
}

/**
 * Column value pair for update query
 * @param $data
 * @return array
 */
function createSqlUpdateCase($data) {
    $columnsValuesPair = '';
    $dataPair = [];
    foreach ($data as $key => $value) {
        $columnsValuesPair .= $key . "=? ,";

        if(empty($value) && $value != 0 ) $value = NULL;
        array_push($dataPair,$value);
    }
    $columnsValuesPair = substr_replace($columnsValuesPair, "", -1);
    $sqlData = ['columnsValuesPair' => $columnsValuesPair, 'data' => $dataPair];
    return $sqlData;
}

/**
 * Convert bytes to the unit specified by the $to parameter.
 *
 * @param integer $bytes The filesize in Bytes.
 * @param string $to The unit type to convert to. Accepts K, M, or G for Kilobytes, Megabytes, or Gigabytes, respectively.
 * @param integer $decimal_places The number of decimal places to return.
 *
 * @return integer Returns only the number of units, not the type letter. Returns 0 if the $to unit type is out of scope.
 *
 */
function isa_convert_bytes_to_specified($bytes, $to, $decimal_places = 1) {
    $formulas = array(
        'K' => number_format($bytes / 1024, $decimal_places),
        'M' => number_format($bytes / 1048576, $decimal_places),
        'G' => number_format($bytes / 1073741824, $decimal_places)
    );
    return isset($formulas[$to]) ? $formulas[$to] : 0;
}

/**
 * function to replace last occurance from a string
 * @param $search
 * @param $replace
 * @param $subject
 * @return mixed
 */
function str_replace_last($search, $replace, $subject) {
    $pos = strrpos($subject, $search);
    if ($pos !== false) {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }
    return $subject;
}

/**
 * @param $from
 * @param $to
 * @param $content
 * @return string|string[]|null
 */
function str_replace_first($from, $to, $content)
{
    $from = '/'.preg_quote($from, '/').'/';
    return preg_replace($from, $to, $content, 1);
}

/**
 * function to get user specified time format
 * @param $time
 * @param null $id
 * @return false|string
 */
function timeFormat($time, $id = null, $connection) {

    if (empty($id)) {
        $id ='1';
    }
    $sql = "SELECT * FROM default_date_clock_settings WHERE user_id=".$id;

    $userFormat = $connection->query($sql)->fetch();

    if (!empty($userFormat)) {
        if ($userFormat['default_clock_format'] == '12') {
            //change current time to 12 hour format
            $timeFormat = date("g:i A", strtotime($time));
        } elseif ($userFormat['default_clock_format'] == '24') {
            $timeFormat = date("H:i", strtotime($time));
        }
    } else {
        $timeFormat = date("g:i A", strtotime($time));
    }
    return $timeFormat;
}

/**
 * @param $time
 * @return mixed
 */
function mySqlTimeFormat($time) {
    $time = date('H:i:s', strtotime($time));
    return $time;
}

/**
 * function to build select query string
 * @param $columns
 * @param $table
 * @param null $joins
 * @param $where
 * @return string
 */
function selectQueryBuilder($columns,$table,$joins=null,$where,$orderBy=null){
    $columnsString = '';
    $join = '';
    $whereSting = " WHERE (";
    $orderString = '';
    if(!empty($columns)){
        foreach ($columns as $key =>$value){
            $columnsString .= $value.',';
        }
        $columnsString = str_replace_last(',', '', $columnsString);
    }
    if(!empty($joins)){
        foreach ($joins as $key =>$value){
            if(empty($value['type'])) $value['type'] = 'LEFT';
            $join .= " ".$value['type']." JOIN ".$value['on_table']." as ".$value['as']." ON ".$value['table'].".".$value['column']."=".$value['as'].".".$value['primary'];
        }
    }

    foreach ($where as $key =>$value) {
        $type = (isset($value['type']) && !empty($value['type']))? $value['type']:'AND';
        if ($key > 0) $whereSting .= ' '.$type;
        if ($value['condition'] == 'IS NULL') {
            $whereSting .= " " . $value['table'] . "." . $value['column'] . " " . $value['condition'];
        } else if ($value['condition'] == 'IS NOT NULL') {
            $whereSting .= " " . $value['table'] . "." . $value['column'] . " " . $value['condition'];
        } else if ($value['condition'] == 'IN') {
            $where .= " " . $table . "." . $value['column'] . " ".$value['condition']." " . $value['value'] . "";
        } else {
            $whereSting .= " " . $value['table'] . "." . $value['column'] . " " . $value['condition'] . " '" . $value['value'] . "'";
        }
    }
    $whereSting .= ")";

    if(!empty($orderBy)){
        $orderString = 'ORDER BY '.$orderBy["column"].' '.$orderBy["sort"];
    }

    return "SELECT ".$columnsString." FROM ".$table." ".$join." ".$whereSting." ".$orderString;
}

/**
 * function to unserialize data
 * @param $data
 * @return mixed
 */
function getUnserializedData($data,$view='false'){
    if(empty($data)) return $data;
    foreach ($data as $key=>$value){
        $unserializedData = @unserialize($value);
        if ($unserializedData !== false) {
            if($view=='true'){
                $data[$key] = !empty($unserializedData)?$unserializedData:'N/A';
            } else {
                $data[$key] = $unserializedData;
            }
        } else {
            if($view=='true'){
                $data[$key] = !empty($value)?$value:'N/A';

            }
        }
    }
    return $data;
}

/**
 * @param $val
 * @return int|string
 */
function buildingStatusFormatter ( $val ){
    $val = strtolower($val);
    if ($val == 'vacant available')
        return 1;
    else if($val == "unrentable")
        return 2;
    else if($val == "occupied")
        return 4;
    else if($val == "notice available")
        return 5;
    else if($val == "vacant rented")
        return 6;
    else if($val == "notice rented")
        return 7;
    else if($val == "under make ready")
        return 8;
    else
        return '';
}

/**
 * @param $val
 * @return int|string
 */
function taskReminderStatusSearch ( $val ){
    $val = strtolower($val);
    if ($val == 'not assigned')
        return 1;
    else if($val == "not started")
        return 2;
    else if($val == "in progress")
        return 3;
    else if($val == "completed")
        return 4;
    else if($val == "canceled and resigned")
        return 5;
    else
        return '';
}

/**
 * @param $val
 * @return int|string
 */
function phoneCallLogNeededSearch ( $val ){
    $val = strtolower($val);
    if ($val == 'no')
        return 1;
    else if($val == "yes")
        return 2;
    else
        return '';
}

/**
 * @param $val
 * @return int|string
 */
function phoneCallLogIncomingOutgoingSearch ( $val ){
    $val = strtolower($val);
    if ($val == 'incoming')
        return 1;
    else if($val == "outgoing")
        return 2;
    else
        return '';
}

/**
 * @param $val
 * @return int|string
 */
function reportingCode( $val ){
    $val = strtolower($val);
    if ($val == 'c')
        return 1;
    else if($val == "l")
        return 2;
    else if($val == "m")
        return 3;
    else if($val == "b")
        return 4;
    else
        return '';
}

/**
 * Function for fetching data based on id
 * @param $connection
 * @param $template_key
 * @return array
 */
function getEmailTemplateData($connection, $template_key) {
    try {
        $sql = "SELECT `template_html` FROM  emailtemplatesadmin WHERE template_key='$template_key'";
        $emailTemplateVal = $connection->query($sql)->fetch();

        $html= "";
        //$html .= '<div id="tinymce" class="mceContentBody " contenteditable="true" dir="ltr" style="font-size: 16px;"><p>&nbsp;</p>'. $emailTemplateVal["template_html"] .'</div>';
        $html .= $emailTemplateVal["template_html"];
        return $html;

    } catch (PDOException $e) {
        echo $e->getMessage();
    }


}

/*
 * function to get salutation
 */
function getSalutation($salutation){
    $salutation_value = '';
    if($salutation == "1"){
        $salutation_value = "Dr.";
    }else if($salutation == "2"){
        $salutation_value = "Mr.";
    }else if($salutation == "3"){
        $salutation_value = "Mrs.";
    }else if($salutation == "4"){
        $salutation_value = "Mr. & Mrs.";
    }else if($salutation == "5"){
        $salutation_value = "Ms.";
    }else if($salutation == "6"){
        $salutation_value = "Sir";
    }else if($salutation == "7"){
        $salutation_value = "Madam";
    }
    return  $salutation_value;
}

/*
 * function to get salutation
 */
function getSalutationId($salutation){
    $salutation_value = '';
    if($salutation == "Dr."){
        $salutation_value = "1";
    }else if($salutation == "Mr."){
        $salutation_value = "2";
    }else if($salutation == "Mrs."){
        $salutation_value = "3";
    }else if($salutation == "Mr. & Mrs."){
        $salutation_value = "4";
    }else if($salutation == "Ms."){
        $salutation_value = "5";
    }else if($salutation == "Sir"){
        $salutation_value = "6";
    }else if($salutation == "Madam"){
        $salutation_value = "7";
    }
    return  $salutation_value;
}


/**
 * function for get country code value
 * @author Deepak
 * @return array
 */
function getCountryCodeValue($connection,$country_code_id=NULL) {
    try {
        $county_code= '';
        if(!empty($country_code_id)) {
            $query = $connection->query("SELECT code FROM countries WHERE id=" . $country_code_id);
            $data = $query->fetch();
            $country_code = $data['code'];
        }else{
            $country_code = 'N/A';
        }
        return $country_code;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }


}

/**
 * function for get country code value
 * @author Deepak
 * @return array
 */
function getCredentialType($connection,$credential_type=NULL) {
    try {
        $credential_type_name= '';
        if(!empty($credential_type)) {
            $query = $connection->query("SELECT credential_type FROM tenant_credential_type WHERE id=" . $credential_type);
            $data = $query->fetch();
            $credential_type_name = $data['credential_type'];
        }else{
            $credential_type_name = 'N/A';
        }

        return $credential_type_name;

    } catch (PDOException $e) {
        echo $e->getMessage();
    }


}


/*
* function to get relation
*/
function getRelation($relation){
    // echo $relation;
    $relation_value = '';
    switch ($relation) {
        case 1:
            $relation_value ="Daughter";
            break;
        case 2:
            $relation_value ="Father";
            break;
        case 3:
            $relation_value ="Friend";
            break;
        case 4:
            $relation_value ="Mother";
            break;
        case 5:
            $relation_value ="Owner";
            break;
        case 6:
            $relation_value ="Partner";
            break;
        case 7:
            $relation_value ="Son";
            break;
        case 8:
            $relation_value ="Spouse";
            break;
        case 9:
            $relation_value ="Other";
            break;
        default:
            $relation_value ="N/A";
    }
    return $relation_value;
}


function noticePeriod( $val ){
    if ($val == 1)
        return '1 Month';
    else if($val == 2)
        return '2 Month';
    else if($val == 3)
        return '3 Month';
    else if($val == 4)
        return '5 Days';
    else
        return '';
}

function emergencyRelation( $val ){
    if ($val == 1)
        return 'Daughter';
    else if($val == 2)
        return 'Father';
    else if($val == 3)
        return 'Friend';
    else if($val == 4)
        return 'Mother';
    else if($val == 5)
        return 'Owner';
    else if($val == 6)
        return 'Partner';
    else if($val == 7)
        return 'Son';
    else if($val == 8)
        return 'Spouse';
    else if($val == 9)
        return 'Other';
    else
        return '';
}

function formatPhoneNum($phone){
    $phone = preg_replace("/[^0-9]*/",'',$phone);
    if(strlen($phone) != 10) {
        return 'lengthError';
    }
    $sArea = substr($phone,0,3);
    $sPrefix = substr($phone,3,3);
    $sNumber = substr($phone,6,4);
    $phone = $sArea."-".$sPrefix."-".$sNumber;
    return($phone);
}

function checkPermissionsNew($parent_module,$child,$subChild,$permissionModule,$connection) {
    $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
    $userData = getSingleRecord($connection,['column'=>'id','value'=>$user_id],'users');
    if ($user_id != '1' && $userData['data']['role'] != '1') {
        if (!empty($userData['data']['role'])) {
            $role_permissions = getSingleRecord($connection,['column'=>'id','value'=>$userData['data']['role']],'company_user_roles');
            $access = 1;
            if($role_permissions['code'] == 200) {
                $role_permissions_data = unserialize($role_permissions['data']['role']);
                $preData = getPermissionData($role_permissions_data,'#');

                $checkPermissionModule = $preData[$parent_module];
                if(isset($preData['selected']) && $preData['selected'] == 'all'){
                    return 1;
                }

                if($preData[$parent_module]['selected'] == 'true'){
                    return 1;
                }

                if($checkPermissionModule['child']['length'] > 0 && $checkPermissionModule['child'][$child]['selected'] == 'true'){
                    return 1;
                } else if($checkPermissionModule['child']['length'] > 0){
                    $checkPermissionModule = $checkPermissionModule['child'][$child];
                }

                if($checkPermissionModule['child']['length'] > 0 && $checkPermissionModule['child'][$subChild]['selected'] == 'true'){
                    return 1;
                } else if($checkPermissionModule['child']['length'] > 0){
                    $checkPermissionModule = $checkPermissionModule['child'][$subChild];
                }

                if($checkPermissionModule['child'][$permissionModule]['selected'] == 'true'){
                    return 1;
                } else {
                    return 0;
                }
            }
            return $access;
        } else {
            return 'No Role Assigned Yet!';
        }
    } else {
        return true;
    }
}




/**
 * CheckPermission of Company user.
 * @return string
 */
function checkPermissions($module, $parent = null, $child = null, $grandChild = null) {
    $user = Auth::User();
    if ($user->parent_id !== null) {
        if (!empty($user->role)) {
            $role_permissions = CompanyUserRole::find($user->role);
            //check if user has permission to access
            $access = false;
            $moduleId = null;
            $parentId = null;
            $childId = null;
            $module = strtolower($module);
            foreach ($role_permissions->role as $key => $item) {
                $new = str_replace("\n", "", $item['text']);
                $new = preg_replace('#[^\w()/.%\-&]#', "", $new);
                $new = strtolower($new);
                //checking module
                if ($new == $module && $item['state']['selected']) {
                    $access = true;
                    break;
                } elseif ($new == $module) {
                    $moduleId = $item['id'];
                }
                //checking parent access
                if (!empty($moduleId) && empty($parentId) && empty($childId) && !empty($parent)) {
                    $parent = strtolower($parent);
                    if ($moduleId == $item['parent'] && $new == $parent && $item['state']['selected']) {
                        $access = true;
                        break;
                    } elseif ($new == $parent) {
                        $parentId = $item['id'];
                    }
                }
                //checking child access
                if (!empty($parentId) && empty($childId) && !empty($child)) {
                    $child = strtolower($child);
                    if ($parentId == $item['parent'] && $new == $child && $item['state']['selected']) {
                        $access = true;
                        break;
                    } elseif ($new == $child) {
                        $childId = $item['id'];
                    }
                }
                //checking grandChild access
                if (!empty($childId) && !empty($grandChild)) {
                    $grandChild = strtolower($grandChild);
                    if ($childId == $item['parent'] && $new == $grandChild && $item['state']['selected']) {
                        $access = true;
                        break;
                    }
                    $access = false;
                }
            }
            return $access;
        } else {
            return 'No Role Assigned Yet!';
        }
    } else {
        return true;
    }
}

function calculateTimeDiff($start,$end){
    try{
        $startTime = strtotime($start);
        $endTime = strtotime($end);
        $diff = ($endTime - $startTime);
        return $diff;
    } catch(Exception $exception){
        dd($exception);
    }
}

function combineAddress($addressArray){
    try{
        $address = '';
        foreach ($addressArray as $key=>$value){
            if(!empty($value)) {
                $address .= $value . ',';
            }
        }
        $address = str_replace_last(',','',$address);
        return $address;
    } catch (Exception $exception) {

    }
}

function checkPropertyAddress($address,$id=null,$connection){
    try{
        if(empty($id)) {
            $select = "SELECT address_list FROM general_property WHERE address_list IS NOT NULL AND address_list='" . $address . "'";
        } else {
            $select = "SELECT address_list FROM general_property WHERE address_list IS NOT NULL AND address_list='" . $address . "' AND id !=".$id;
        }
        return $connection->query($select)->fetch();
    } catch(Exception $exception) {
        dd($exception);
    }
}

function getJsTimezone(){
    try {
        $url = "http://".$_SERVER['HTTP_HOST'].'/getTimeZone';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($server_output);
    } catch(Exception $exception){

    }
}


/**
 * function to update name with username fucnction and update in DB
 * @param $id
 * @param $connection
 * @return array
 */
function saveStripeCustomerId($connection,$data,$id){

    $columnValue = createSqlUpdateCase($data);
    $data = $connection->prepare("UPDATE users SET " . $columnValue['columnsValuesPair'] ."WHERE id =".$id)->execute($columnValue['data'] );
    if($data){
        return ['msg' => 'Customer Id updated successfully', 'code' => 200];
    } else {
        return ['msg' => 'error.', 'code' => 500];
    }
}


/**
 * function to save user account detail
 * @param $id
 * @param $connection
 * @return array
 */
function updateUserAccountDetail($connection,$stripe_account_array,$company_id){
    $address1 = "address_full_match"; /*$fdata['faddress1'];*/
    $address2 = ""; /*$fdata['faddress2'];*/
    $sqlData = createSqlColValPair($stripe_account_array);
    $query = "UPDATE user_account_detail SET ".$sqlData['columnsValuesPair']." where user_id=$company_id";
    $stmt = $connection->prepare($query);
    $stmt->execute();
}

/**
 * function to save user account detail
 * @param $id
 * @param $connection
 * @return array
 */
function saveUserAccountDetail($connection,$stripe_account_array){
    $address1 = "address_full_match"; /*$fdata['faddress1'];*/
    $address2 = ""; /*$fdata['faddress2'];*/
    $sqlData1 = createSqlColVal($stripe_account_array);
    $query2 = "INSERT INTO user_account_detail (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
    $stmt2 = $connection->prepare($query2);
    $stmt2->execute($stripe_account_array);
}

/**
 * function change subscription date
 * @param $date
 * @param null $id
 * @param $connection
 * @return false|string
 */
function subscriptionDate($date){
//    dd(date('M Y',strtotime($date)));
    return date('M Y',strtotime($date));
}

/**
 * function to normalize phone number with ['-']
 * @param $number
 * @return mixed
 */
function normalizeNumberFormat($number){
    return str_replace('-', '', $number);
}

function getCustomerDetails($connection,$id=null){
    try {
        if(empty($id)){
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        }
        $userData = $connection->query("SELECT first_name,last_name,company_name,phone_number,address1,address2,zipcode,city,state,country FROM users WHERE id=$id")->fetch();

        if(empty($userData)){
            return ['msg' => 'error', 'code' => 400, 'message' =>ERROR_NO_RECORD_FOUND];
        }
        return ['msg' => 'success', 'code' => 200, 'message' =>SUCCESS_FETCHED, 'data'=>$userData];
    } catch(Exception $exception){
        return ['msg' => 'error.', 'code' => 500, 'message' =>$exception->getMessage()];
    }
}


/**
 * function to save user account detail
 * @param $id
 * @param $connection
 * @return array
 */
function saveLedgerEnteries($connection,$ledger_data){
    $sqlData1 = createSqlColVal($ledger_data);
    $query2 = "INSERT INTO accounting_ledger (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
    $stmt2 = $connection->prepare($query2);
    $stmt2->execute($ledger_data);
}

/**
 * function to convert number to float value
 * @param $amount
 * @return float
 */
function convertToFloat($amount){
    $amount = (string) $amount;
    $amount = str_replace(",", "", $amount);
    return floatval($amount);
}

function getUserType($user_id){
    $getData = $this->companyConnection->query("SELECT user_type FROM users where id='$user_id'")->fetch();
    return  $getData['user_type'];

}

function getPropertyId($connection,$user_id){
    $getType = $connection->query("SELECT user_type FROM users where id='$user_id'")->fetch();

    if($getType['user_type']==2) {
        $getData =  $connection->query("SELECT property_id FROM tenant_property WHERE user_id ='" . $user_id . "'")->fetch();
    }
    if($getType['user_type']==4) {
        $getData =  $connection->query("SELECT property_id FROM owner_property_owned WHERE user_id ='" . $user_id . "'")->fetch();
    }
    return $property_id = $getData['property_id'];
}

function getPropertyBankId($connection, $property_id){
    $getData =  $connection->query("SELECT * FROM property_bank_details WHERE property_id ='" . $property_id . "' and is_default='1'")->fetch();
    if(empty($getData)) {
        return '';
    } else {
        return $bank_id = $getData['bank_id'];
    }

}
function getBankSourceId($connection, $property_id){
    $getData =  $connection->query("SELECT * FROM property_bank_details WHERE property_id ='" . $property_id . "' and is_default='1'")->fetch();
    if(empty($getData)) {
        return '';
    } else {
        return $bank_id = $getData['source_id'];
    }
}

function getAutoTransactionId($connection){
    $data = $connection->query("SELECT * FROM transactions where id = (select MAX(id) from transactions)")->fetch();
//    dd($data['auto_transaction_id']);
    if (!empty($data)){
        return $data['auto_transaction_id']+1;
    } else {
        return 100000;
    }
}

/**
 *  Insert Notification
 */
function insertNotification($connection,$module_id,$notificationTitle,$action,$descriptionNotifi,$module_type,$alert_type){
    try{
        $datauserType = $connection->query("SELECT id,user_type FROM users WHERE id=$module_id")->fetch();
        $id = $datauserType['id'];/*get id from users table*/
        $userType = $datauserType['user_type'];/*get user_type for identify module*/


/*Get user check from admin/alert users start*/
        $getRoleSettting  = $connection->query("SELECT ua.send_to_users FROM user_alerts as ua where ua.alert_name = '$notificationTitle'")->fetch();
        $getAllRollChecked = unserialize($getRoleSettting['send_to_users']);


/*get manager when check set PM from alert user start*/
        if(!empty($getAllRollChecked[1])){
            if(!empty($datauserType)){
                if($userType == 2){
                    $prop_Id =$connection->query("SELECT property_id FROM tenant_property WHERE user_id=$id")->fetch();
                    $propId = $prop_Id['property_id'];

                    /*get manager id start*/
                    $managerId =$connection->query("SELECT manager_id FROM `general_property` WHERE id='$propId'")->fetch();
                    $manager_Id = unserialize($managerId['manager_id']);

                    if(!empty($manager_Id) > 1){
                        $man_Id = $manager_Id[0];
                    }else if(!empty($manager_Id) == 1){
                        $man_Id = $manager_Id;
                    }else{
                        $man_Id = 1;
                    }
                    /*get manager id end*/

                }
                elseif($userType == 3){
                    $prop_Id =$connection->query("SELECT property_id FROM owner_blacklist_vendors WHERE vendor_id=$id")->fetch();
                    $propId = $prop_Id['property_id'];

                    /*get manager id start*/
                    $managerId =$connection->query("SELECT manager_id FROM `general_property` WHERE id='$propId'")->fetch();
                    $manager_Id = unserialize($managerId['manager_id']);

                    if(!empty($manager_Id) > 1){
                        $man_Id = $manager_Id[0];
                    }else if(!empty($manager_Id) == 1){
                        $man_Id = $manager_Id;
                    }else{
                        $man_Id = 1;
                    }
                    /*get manager id end*/
                }
                elseif($userType == 4){
                    $propertyId = $connection->query("SELECT property_id FROM owner_details WHERE user_id=$id")->fetch();
                    $prop_id = unserialize($propertyId['property_id']);
                    $propids = [];

                    if(!empty($prop_id)){
                        foreach ($prop_id as $key => $value) {
                            $val = $value;
                            array_push($propids,$val);
                        }
                        $propId = implode(",", $propids);
                    }
                    /*get manager id start*/
                    $managerId =$connection->query("SELECT manager_id FROM `general_property` WHERE id='$propId'")->fetch();
                    $manager_Id = unserialize($managerId['manager_id']);

                    if(!empty($manager_Id) > 1){
                        $man_Id = $manager_Id[0];
                    }else if(!empty($manager_Id) == 1){
                        $man_Id = $manager_Id;
                    }else{
                        $man_Id = 1;
                    }
                    /*get manager id end*/
                    //$propId = $prop_Id['property_id'];
                }
                elseif($userType == 6){
                    $getunitId = $connection->query("SELECT unit_id FROM lease_guest_card WHERE user_id=$id")->fetch();
                    $unitId = $getunitId['unit_id'];
                    $prop_Id = $connection->query("SELECT property_id FROM unit_details WHERE id=$unitId")->fetch();
                    $propId = $prop_Id['property_id'];

                    /*get manager id start*/
                    $managerId =$connection->query("SELECT manager_id FROM `general_property` WHERE id='$propId'")->fetch();
                    $manager_Id = unserialize($managerId['manager_id']);

                    if(!empty($manager_Id) > 1){
                        $man_Id = $manager_Id[0];
                    }else if(!empty($manager_Id) == 1){
                        $man_Id = $manager_Id;
                    }else{
                        $man_Id = 1;
                    }
                    /*get manager id end*/

                }
                elseif($userType == 7){
                    $prop_Id =$connection->query("SELECT tp.property_id FROM tenant_lease_details as tld join tenant_property as tp WHERE tp.user_id=$id")->fetch();
                    $propId = $prop_Id['property_id'];

                    /*get manager id start*/
                    $managerId =$connection->query("SELECT manager_id FROM `general_property` WHERE id='$propId'")->fetch();
                    $manager_Id = unserialize($managerId['manager_id']);

                    if(!empty($manager_Id) > 1){
                        $man_Id = $manager_Id[0];
                    }else if(!empty($manager_Id) == 1){
                        $man_Id = $manager_Id;
                    }else{
                        $man_Id = 1;
                    }
                    /*get manager id end*/
                }
                elseif($userType == 9){
                    $prop_Id =$connection->query("SELECT property_id FROM short_term_rental WHERE user_id = $id")->fetch();
                    $propId = $prop_Id['property_id'];

                    /*get manager id start*/
                    $managerId =$connection->query("SELECT manager_id FROM `general_property` WHERE id='$propId'")->fetch();
                    $manager_Id = unserialize($managerId['manager_id']);

                    if(!empty($manager_Id) > 1){
                        $man_Id = $manager_Id[0];
                    }else if(!empty($manager_Id) == 1){
                        $man_Id = $manager_Id;
                    }else{
                        $man_Id = 1;
                    }
                    /*get manager id end*/
                }
                elseif($userType == 10){
                    $prop_Id =$connection->query("SELECT prop_id FROM company_rental_applications WHERE user_id=$id")->fetch();
                    $propId = $prop_Id['prop_id'];

                    /*get manager id start*/
                    $managerId =$connection->query("SELECT manager_id FROM `general_property` WHERE id='$propId'")->fetch();
                    $manager_Id = unserialize($managerId['manager_id']);

                    if(!empty($manager_Id) > 1){
                        $man_Id = $manager_Id[0];
                    }else if(!empty($manager_Id) == 1){
                        $man_Id = $manager_Id;
                    }else{
                        $man_Id = 1;
                    }
                    /*get manager id end*/
                }
                else{
                    $propId = "";
                    $man_Id = 1;
                }
            }

            /*insert data to notification on basis of pm start*/
            if(is_array($propId) && count( $prop_Id ) > 1 ){
                for($i=0;$i<count( $prop_Id );$i++){
                    $data['property_id'] = $propId[$i];
                    $data['module_type'] = $module_type;
                    $data['module_title'] = $notificationTitle;
                    $data['module_id'] = $module_id;
                    $data['alert_type'] = $alert_type;
                    $data['action'] = $action;
                    $data['notifi_description'] = $descriptionNotifi;
                    $data['user_id'] = $man_Id;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO notification_apex (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $connection->prepare($query);
                    $stmt->execute($data);
                }
            }
            else{
                $data['property_id'] = $propId;
                $data['module_type'] = $module_type;
                $data['module_title'] = $notificationTitle;
                $data['module_id'] = $module_id;
                $data['alert_type'] = $alert_type;
                $data['action'] = $action;
                $data['notifi_description'] = $descriptionNotifi;
                $data['user_id'] = $man_Id;
                $data['created_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO notification_apex (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $connection->prepare($query);
                $stmt->execute($data);

            }
            /*insert data to notification on basis of pm start*/

        }
        /*get manager when check set PM from alert user end*/


        if(!empty($datauserType)){
                if($userType == 2){
                    $prop_Id =$connection->query("SELECT property_id FROM tenant_property WHERE user_id=$id")->fetch();
                    $propId = $prop_Id['property_id'];

                }elseif($userType == 3){
                    $prop_Id =$connection->query("SELECT property_id FROM owner_blacklist_vendors WHERE vendor_id=$id")->fetch();
                    $propId = $prop_Id['property_id'];
                }elseif($userType == 4){
                    $propertyId = $connection->query("SELECT property_id FROM owner_details WHERE user_id=$id")->fetch();
                    $prop_id = unserialize($propertyId['property_id']);
                    $propids = [];
                    if(!empty($prop_id)){
                        foreach ($prop_id as $key => $value) {
                            $val = $value;
                            array_push($propids,$val);
                        }
                        $propId = implode(",", $propids);
                    }
                    //$propId = $prop_Id['property_id'];
                }elseif($userType == 6){
                    $getunitId = $connection->query("SELECT unit_id FROM lease_guest_card WHERE user_id=$id")->fetch();
                    $unitId = $getunitId['unit_id'];
                    $prop_Id = $connection->query("SELECT property_id FROM unit_details WHERE id=$unitId")->fetch();
                    $propId = $prop_Id['property_id'];

                }elseif($userType == 7){
                    $prop_Id =$connection->query("SELECT tp.property_id FROM tenant_lease_details as tld join tenant_property as tp WHERE tp.user_id=$id")->fetch();
                    $propId = $prop_Id['property_id'];

                }elseif($userType == 9){
                    $prop_Id =$connection->query("SELECT property_id FROM short_term_rental WHERE user_id = $id")->fetch();
                    $propId = $prop_Id['property_id'];

                }elseif($userType == 10){
                    $prop_Id =$connection->query("SELECT prop_id FROM company_rental_applications WHERE user_id=$id")->fetch();
                    $propId = $prop_Id['prop_id'];

                }else{
                    $propId = "";
                }
            }

        /*Setting from admin/alert check admin, pm etc start*/
        if(is_array($getAllRollChecked) && count( $getAllRollChecked ) > 1 ){
            foreach($getAllRollChecked as $key => $value) {
                $getIdsSettting  = $connection->query("SELECT u.id FROM users as u where u.role = '$value'")->fetch();
                $see_id = $getIdsSettting['id'];

                if(is_array($propId) && count( $prop_Id ) > 1 ){
                    for($i=0;$i<count( $prop_Id );$i++){
                        $data['property_id'] = $propId[$i];
                        $data['module_type'] = $module_type;
                        $data['module_title'] = $notificationTitle;
                        $data['module_id'] = $module_id;
                        $data['alert_type'] = $alert_type;
                        $data['action'] = $action;
                        $data['notifi_description'] = $descriptionNotifi;
                        $data['user_id'] = $see_id;
                        $data['created_at'] = date('Y-m-d H:i:s');
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO notification_apex (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                        $stmt = $connection->prepare($query);
                        $stmt->execute($data);
                    }
                }
                else{
                    $data['property_id'] = $propId;
                    $data['module_type'] = $module_type;
                    $data['module_title'] = $notificationTitle;
                    $data['module_id'] = $module_id;
                    $data['alert_type'] = $alert_type;
                    $data['action'] = $action;
                    $data['notifi_description'] = $descriptionNotifi;
                    $data['user_id'] = $see_id;
                    $data['created_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO notification_apex (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $connection->prepare($query);
                    $stmt->execute($data);

                }
            }
        }
        /*Setting from admin/alert check admin, pm etc end*/
        else{
            /*Setting from admin/alert nothing is select Start*/
            $getIdsSettting  = $connection->query("SELECT u.id FROM users as u where u.id = '1'")->fetch();
            $see_id = $getIdsSettting['id'];

            if(is_array($propId) && count( $prop_Id ) > 1 ){
                for($i=0;$i<count( $prop_Id );$i++){
                    $data['property_id'] = $propId[$i];
                    $data['module_type'] = $module_type;
                    $data['module_title'] = $notificationTitle;
                    $data['module_id'] = $module_id;
                    $data['alert_type'] = $alert_type;
                    $data['action'] = $action;
                    $data['notifi_description'] = $descriptionNotifi;
                    $data['user_id'] = $see_id;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO notification_apex (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $connection->prepare($query);
                    $stmt->execute($data);
                }
            }
            else{
                $data['property_id'] = $propId;
                $data['module_type'] = $module_type;
                $data['module_title'] = $notificationTitle;
                $data['module_id'] = $module_id;
                $data['alert_type'] = $alert_type;
                $data['action'] = $action;
                $data['notifi_description'] = $descriptionNotifi;
                $data['user_id'] = $see_id;
                $data['created_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO notification_apex (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $connection->prepare($query);
                $stmt->execute($data);

            }
        }
        /*Setting from admin/alert nothing is select Start*/



        return ['status'=>'success','code'=>200,'data'=>$data];
    }catch (Exception $exception)
    {
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        printErrorLog($exception->getMessage());
    }

}

/**
 * Return floated value for $amount
 * @param $amount
 * @return float
 */
function floatFormatter($amount){
    $amount = (!empty($amount) || $amount != '') ? $amount : '0';
    $amount = (float)$amount;
    $amount = number_format($amount, 2, '.', '');
    return $amount;
}



/**
 *
 * @param null $userId
 * @return array
 */
function sendMail($connection,$descriptionNotifi,$notificationTitle)
{
    try{
        $server_name = 'https://'.$_SERVER['HTTP_HOST'];
        /*$userId = (isset($userId) ? $userId : $_REQUEST['id']);*/
        $userIdAdmin = $connection->query("SELECT name,email FROM users where user_type='1' and role='1'")->fetch();;
        //$user_details = $connection->query("SELECT * FROM users where id=".$userId)->fetch();
        //$user_name = userName($user_details['id'], $this->companyConnection);
        /* $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/welcomeUser.php');
         $body = str_replace("#name#",$user_name,$body);
         $body = str_replace("#email#",$user_details['email'],$body);
         $body = str_replace("#password#",$user_details['actual_password'],$body);
         $body = str_replace("#logo#",SITE_URL.'/company/images/logo.png',$body);
         $body = str_replace("#website#",$server_name,$body);*/
        $userIdPm = $connection->query("SELECT name,email FROM users where user_type='1' and role='2'")->fetch();
        $compName = "ccxz";
        $phone_number = "42344234";
        $urlSite = SITE_URL.'/company/images/logo.png';
        $html = '<table align="center" border="0" width="640" style="border:1px solid #ddd;" cellpadding="0" cellspacing="0"><tbody><tr style="background-color:#00b0f0;height:20px;"><td></td>
                        </tr><tr style="border-collapse:collapse;background-color:#fff;"><td align="center" style="font-family:\'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;border-collapse:collapse;">
                        <div style="margin-top:1px;margin-bottom:10px;">
                            <img src="'.$urlSite.'" height="60" width="160"></img>
                        </div>
                        </td>
                        </tr><tr style="background-color:#00b0f0;height:20px;"><td></td>
                        </tr><tr><td style="padding:20px;">
                        <p style="font-family:arial;">Dear '.$userIdAdmin['name'].',</p>
                        <p style="font-family:arial;">Please click on the link below to Electronically sign the document sent to you :-</p>
                        <p style="font-family:arial;">'.$notificationTitle.'</p>
                        <p style="font-family:arial;">Thanks,</p>                      
                        </td>
                        </tr><tr><td align="center" bgcolor="#05a0e4" style="padding:10px;font-size:12px;color:#ffffff;font-weight:bold;">
                        '.$compName.'  <a style="color:#fff;text-decoration:none;">
                        &nbsp; ●'.$phone_number.'</a> </td>
                        </tr></tbody></table>';

        $request['action']  = 'SendMailPhp';
        $request['to[]']    = "gff";
        $request['subject'] = $notificationTitle;
        $request['message'] = $html;
        $request['portal']  = '1';
        curlRequest($request);

        return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully'];
    }catch (Exception $exception) {
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        printErrorLog($exception->getMessage());
    }
}

function parse_number($number, $dec_point=null) {
    $number = str_replace(',','',$number);
    return floatval($number);
}

/**
 * function to get default bank account name of property
 * @param $data
 * @return string
 */
function property_default_bank_account($connection,$data){
    try{
        $sqlQuery = 'SELECT * FROM property_bank_details WHERE property_id='.$data.' AND is_default="1"';
        $data = $connection->query($sqlQuery)->fetch();
        if(!empty($data)){
            return $data['bank_name'];
        } else{
            $sqlQuery = 'SELECT * FROM company_accounting_bank_account WHERE is_default="1"';
            $data = $connection->query($sqlQuery)->fetch();
            if(!empty($data)){
                return $data['bank_name'];
            } else {
                return '';
            }
        }
    } catch(Exception $exception) {
        dd($exception);
    }
}

/**
 * function to get default bank account id of property
 * @param $data
 * @return string
 */
function property_default_bank_account_id($connection,$data){
    try{
        $sqlQuery = 'SELECT * FROM property_bank_details WHERE property_id='.$data.' AND is_default="1"';
        $data = $connection->query($sqlQuery)->fetch();
        if(!empty($data)){
            return $data['bank_id'];
        } else{
            $sqlQuery = 'SELECT * FROM company_accounting_bank_account WHERE is_default="1"';
            $data = $connection->query($sqlQuery)->fetch();
            if(!empty($data)){
                return $data['id'];
            } else {
                return '';
            }
        }
    } catch(Exception $exception) {
        dd($exception);
    }
}



/**
 * function to get the carrier from phone number
 * @param $ext
 * @param $phone_number
 * @param $connection
 * @return array
 */
function getPhoneCarrier($ext,$phone_number,$connection){
    try{
        if($ext == '+1') {
            $phone_number = str_replace('-', '', $phone_number);
            $userData = 'SELECT id,sms_carrier,name FROM users WHERE phone_number="'.$phone_number.'"';
            $userData = $connection->query($userData)->fetch();
            if(!empty($userData) && (isset($userData['sms_carrier']) && !empty($userData['sms_carrier']))){
                return  ['status' => SUCCESS, 'code' => SUCCESS_CODE, 'sms_gateway' =>$userData['sms_carrier']];
            }
            $ch = curl_init();
            curl_setopt_array($ch, array(
                CURLOPT_URL => 'http://apilayer.net/api/validate?access_key='.API_LAYER.'&number='.$phone_number,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 200000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET'
            ));
            $server_output = curl_exec($ch);
            $server_output = json_decode($server_output);
            if(!empty($server_output) && !empty($server_output->carrier)){
                $sdata = strtolower($server_output->carrier);
                $sql = 'SELECT sms_gateway FROM carrier WHERE LOWER(carrier)="'.$sdata.'"';
                $data = $connection->query($sql)->fetch();
                if(!empty($data['sms_gateway'])){
                    if(!empty($userData)){
                        $data = [];
                        $data['sms_carrier'] = $data['sms_gateway'];
                        $sqlData = createSqlColValPair($data);
                        $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where phone_number='$phone_number'";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                    }
                    return  ['status' => SUCCESS, 'code' => SUCCESS_CODE, 'sms_gateway' =>$data['sms_gateway']];
                }
            }
            return  ['status' => ERROR, 'code' => ERROR_CODE, 'sms_gateway' =>'No Sms Gateway Specified!'];
        }
        return  ['status' => ERROR, 'code' => ERROR_CODE, 'sms_gateway' =>'No Sms Gateway Specified!'];
    } catch(Exception $exception){
        return ['msg' => ERROR_SERVER_MSG, 'code' => 500, 'message' =>$exception->getMessage()];
    }
}

function checkPermission($user_id,$role,$connection){
    try{
        if ($user_id != '1' && $role != '1') {
            if (!empty($role)) {
                $role_permissions = getSingleRecord($connection,['column'=>'id','value'=>$role],'company_user_roles');
                if($role_permissions['code'] == 200) {
                    $role_permissions_data = unserialize($role_permissions['data']['role']);
                    //check if user has permission to access
                    $data = getPermissionData($role_permissions_data,'#');
                    $_SESSION[SESSION_DOMAIN]['permission'] = json_encode($data);
                }
                return;
            } else {
                return 'No Role Assigned Yet!';
            }
        } else {
            $_SESSION[SESSION_DOMAIN]['permission'] = json_encode(['selected'=>'all']);
            return true;
        }
    } catch (Exception $exception) {
        return array('code' => ERROR_CODE, 'status' => ERROR,'message' => $exception->getMessage());
    }
}


/**
 * function to make permission array (reccursive function)
 * @param $sortedData
 * @param $parent_id
 * @return array
 */
function getPermissionData($sortedData,$parent_id){
    try {
        $returnData = [];
        foreach ($sortedData as $value) {
            $child = [];
            if ($value->parent == $parent_id) {
                $children = getPermissionData($sortedData, $value->id);
                if ($children) {
                    $child = $children;
                    $child['length'] = count($child);
                }
                $returnData[trim($value->text)] = ['selected'=>(empty($value->state->selected))?'false':'true','child'=>$child];
            }
        }
        return $returnData;
    } catch (Exception $exception) {
        return array('code' => ERROR_CODE, 'status' => ERROR,'message' => $exception->getMessage());
    }
}