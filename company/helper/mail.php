<?php

//header("content-type: application/json");
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:23 PM
 */
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    include_once ('../../constants.php');
  //  $url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

if (isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] != '')) {
    $url = BASE_URL . "Dashboard/Dashboard";
    header('Location: ' . $url);
} else {
    // write your code here
}

require_once($_SERVER['DOCUMENT_ROOT'] . "../vendor/autoload.php");
try {
//    die('dfdsf');
    $data = json_decode(file_get_contents('php://input'), true);
    // Create the SMTP Transport
    $transport = (new Swift_SmtpTransport(SMTP_HOST, 465, 'ssl'))
            ->setUsername(SMTP_USERNAME)
            ->setPassword(SMTP_PASSWORD);
    // Create the Mailer using your created Transport
    $mailer = new Swift_Mailer($transport);
    // Create a message
    $message = new Swift_Message();
    // Set a "subject"
    $message->setSubject($data['subject']);
    // Set the "From address"
    $message->setFrom([$data['to'] => 'raghav']);
    // Set the "To address" [Use setTo method for multiple recipients, argument should be array]
    $message->setTo([$data['from'] => 'Apexlink']);
    // Add "CC" address [Use setCc method for multiple recipients, argument should be array]
    // $message->addCc['chughraghav@seasiainfotech.com'('recipient@gmail.com', 'recipient name');
    // Add "BCC" address [Use setBcc method for multiple recipients, argument should be array]
    // $message->addBcc('recipient@gmail.com', 'recipient name');
    // Add an "Attachment" (Also, the dynamic data can be attached)
    // $attachment = Swift_Attachment::fromPath('example.xls');
    // $attachment->setFilename('report.xls');
    //$message->attach($attachment);
    // Add inline "Image"
    //$inline_attachment = Swift_Image::fromPath('nature.jpg');
    // $cid = $message->embed($inline_attachment);
    // Set the plain-text "Body"
    //$message->setBody("This is the plain text body of the message.\nThanks,\nAdmin");
    $message->setBody($data['message'], "text/html");
    // Set a "Body"//
    //$message->addPart('This is the HTML version of the message.<br>Example of inline image:<br><img src="'.$cid.'" width="200" height="200"><br>Thanks,<br>Admin', 'text/html');
    // Send the message
    $result = $mailer->send($message);
//    echo $result;
    if ($result) {

        echo json_encode(array('status' => 'success', 'message' => $data['success_message']));
    } else {
        echo json_encode(array('status' => 'error', 'message' => 'Mail not send successfully'));
    }
} catch (Exception $e) {

    echo $e->getMessage();
}
?>
