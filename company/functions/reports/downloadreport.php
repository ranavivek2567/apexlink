<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
//include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
//include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AllreportsAjax extends DBConnection
{

    public function __construct()
    {

        parent::__construct();
        $this->reportHtmlfull();
//        $action = $_REQUEST['action'];
//        echo json_encode($this->$action());

    }


    public function reportHtmlfull()
    {
//        dd( $_SESSION[SESSION_DOMAIN]);
        $column = $_SESSION[SESSION_DOMAIN]['report_columns'] ;
        //   print_r($column);die;
        $layout=$_SESSION[SESSION_DOMAIN]['layout'];
        if(isset($_SESSION[SESSION_DOMAIN]['print_envelope_by'])){
            $print_envelope_by= $_SESSION[SESSION_DOMAIN]['print_envelope_by'];
        }
        //dd($layout);
        $dbquery = $_SESSION[SESSION_DOMAIN]['report_db_query'] ;
        /* dd($dbquery);die;*/
        $filter_html=$_SESSION[SESSION_DOMAIN]['htmlFilter'];
        $title1 = $_SESSION[SESSION_DOMAIN]['report_title'] ;
        //print_r($title1);
        $title=str_replace("Filters","",$title1);
        //       print_r($title);die;
        $filter_data=$_SESSION[SESSION_DOMAIN]['filter_data'] ;

//        dd($filter_data);
        $imgg=COMPANY_DIRECTORY_URL.'/images/logo.png';
        $pagination= $_SESSION[SESSION_DOMAIN]['pagination'];
        //$layout="5";

        $column_name=(explode("," ,$column));


        if (!empty($current_date)){
            $filter_date = $current_date;
        } elseif (!empty($start_date) && !empty($end_date)){
            $filter_date = $start_date .' - '. $end_date;
        } elseif (!empty($start_date) ){
            $filter_date = $start_date;
        } elseif (!empty($end_date)){
            $filter_date = $end_date;
        } else{
            $filter_date = '';
        }


        $page="portfolio";
        try {
            // Find out how many items are in the table
// print_r($dbquery);die;
            $query = $this->companyConnection->query($dbquery);
            $result = $query->fetchAll();
            $total = count($result);
            //  print_r($result);
            // print_r($total);die;
            // How many items to list per page
            if(isset($filter_data) && !empty($filter_data)) {
                if(isset($_GET['page']) && $_GET['page'] > 1){
                    $page  = (int)$_GET['page'] - 1;
                }else{
                    $page  = 0;
                }
                if(is_array($filter_data)){
                    $count=count($filter_data);
                }else{

                    $count=1;
                }
          //      print_r($filter_data);die();
                if(!is_array($filter_data)){

                    $this->notarrayall($total,$count,$page,$column,$dbquery,$filter_html,$title,$filter_data,$pagination,$layout,$column_name,$result,$filter_date);
                }else {
                    //print_r("hello");die;

                    $grandtotal=[];
                    $grand_total_sq_ft=[];
                    $grand_total_total_mart=[];
                    $grand_total_total_curr=[];
                    $grand_total_total_dep=[];
                    $grand_total_total_past_due=[];
                    $grand_total_total_nfs_count=[];
                    $grand_total_total_late_count=[];
                    $grand_total_occ_status=[];

                    $html='';
                    for ($i = $page; $i < $count; $i++) {

                        /* $query1 = $this->companyConnection->query($dbquery . ' AND gp.id =' . "'" . $filter_data[$i] . "'");
                         $pagin = $query1->fetchAll();*/
                        $query2 = $this->companyConnection->query("SELECT property_name,portfolio_id, CONCAT(address1,' ',address2,' ',address3,', ',state,' ',zipcode) AS address FROM general_property WHERE id=" . $filter_data[$i]);
                        $table_name = $query2->fetch();
                        $table_title = $table_name['property_name'].' - '.$table_name['address'];
                        if (isset($table_name['portfolio_id']) && $table_name['portfolio_id'] != '') {
                            $portfolio_id = $table_name['portfolio_id'];
                        } else {
                            $portfolio_id = '0';
                        }
                        $query3 = $this->companyConnection->query("SELECT portfolio_name FROM company_property_portfolio WHERE id=" . $portfolio_id);
                        $table_name2 = $query3->fetch();
//                        $table_title2 = $table_name2['portfolio_name'];
                        $table_title2 = '';
                        $setlimit = 0;
                        $limit = $setlimit;
                        /*  else{
                                  $limit = 10;
                              }*/
                        // How many pages will there be
                        //$pages = ceil($total / $limit);
                        if (is_array($filter_data) && $layout != "MailingLabel" && $layout != "MailingLabel2" && $layout != "DirectRedirection") {
                            $pages = count($filter_data);

                        } elseif ($layout == "MailingLabel" || $layout == "MailingLabel2" || $layout == "DirectRedirection") {

                            $testpage = count($result);
                            // print_r($query);
                            if ($testpage < 10) {
                                $pages = '1';
                                $offset = $page * 9;
                            } else {
                                $testpage1 = $testpage / 10;
                                if (strpos($testpage1, '.')) {
                                    $test = explode('.', $testpage1);
                                    $pages = $test[0] + 1;
                                    $offset = $page * 9;

                                    //print_r($pages);die;
                                } else {
                                    $pages = $testpage1;
                                    $offset = $page * 9;
                                }
                            }
                        } else {
                            $pages = 1;
                        }
                        // What page are we currently on?
                        $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
                            'options' => array(
                                'default' => 1,
                                'min_range' => 1,
                            ),
                        )));
                        // Calculate the offset for the query
                        // $offset = ($page - 1) * $limit;
                        // Some information to display to the user
                        //$start = $offset + 1;
                        //$end = min(($offset + $limit), $total);
                        // The "back" link
                        $prevlink = ($page > 1) ? '<a href="?page=1" title="First page">&laquo;</a> <a href="?page=' . ($page - 1) . '" title="Previous page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';
                        // The "forward" link
                        $nextlink = ($page < $pages) ? '<a href="?page=' . ($page + 1) . '" title="Next page">&rsaquo;</a> <a href="?page=' . $pages . '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';
                        // Display the paging information
                        /*  echo '<div id="paging"><p>', $prevlink, ' Page ', $page, ' of ', $pages, ' pages, displaying ', $start, '-', $end, ' of ', $total, ' results ', $nextlink, ' </p></div>';*/

                        // $html .= '<section class="main-content">';
                        // $html .= include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
//            $html.= '<section class="main-content">

                        // Prepare the paged query
                        if (strpos($dbquery, 'gp.') == false && $layout != "PrintEnvelope" && $layout != "PrintEnvelope2" && $layout != "MailingLabel" && $layout != "MailingLabel2" && $layout != "") {
                            $qry = $dbquery . ' ORDER BY name ASC ;';

                        } elseif (strpos($dbquery, 'gp.') == false && $layout != "PrintEnvelope" && $layout != "PrintEnvelope2" && $layout == "MailingLabel" && $layout != "MailingLabel2" && $layout != "") {
                            $qry = $dbquery . ' ORDER BY name ASC LIMIT 9 OFFSET ' . $offset . ';';
                            // print_r($qry);die;
                            // print_r("hello");die;
                        } elseif (strpos($dbquery, 'gp.') == false && $layout != "PrintEnvelope" && $layout != "PrintEnvelope2" && $layout != "MailingLabel" && $layout == "MailingLabel2" && $layout != "") {
                            $qry = $dbquery . ' ORDER BY name ASC LIMIT 9 OFFSET ' . $offset . ';';

                        } elseif (strpos($dbquery, 'gp.') == false && $layout != "PrintEnvelope" && $layout != "PrintEnvelope2" && $layout != "MailingLabel" && $layout == "MailingLabel2" && $layout != "") {
                            $qry = $dbquery . ' ORDER BY name ASC LIMIT 9 OFFSET ' . $offset . ';';

                        } elseif ($layout == "PrintEnvelope" || $layout == "PrintEnvelope2" && $layout != "") {
//                            dd($filter_data);

                            if (isset($print_envelope_by) && !empty($print_envelope_by)) {
                                $qry = $dbquery ;
                             //   echo "<pre>";print_r($qry);
                            } else {
                                $qry = $dbquery . 'AND u.id = '.$filter_data[$i].' ORDER BY name ASC ;';


                              //  echo "<prerrr>";print_r($qry);die();
                            }
                            /*   }
                               elseif ($layout!="PrintEnvelope" && $layout!="PrintEnvelope2" && $layout==""){
                                   if(strpos("WHERE", $dbquery)){
                                   $qry = $dbquery . ' AND u.id=' . $filter_data[$i] . ' ORDER BY name ASC ;';

                                   }else{
                                   $qry = $dbquery . ' WHERE u.id=' . $filter_data[$i] . ' ORDER BY name ASC ;';

                                   }*/
                            //print_r($qry);
                        } else {

                            $qry = $dbquery . ' AND gp.id=' . $filter_data[$i] . ' ORDER BY name ASC ;';
                            // print_r($qry);die;

                        }


                        $stmt = $this->companyConnection->query($qry)->fetchAll();

                        $arryKeys=(array_keys($stmt[0]));
                        $amt=array_values(preg_grep("/amt/",$arryKeys));
                        for($h=0;$h<count($amt);$h++){

                            if (isset($stmt[$i][$amt[$h]])  && !empty($stmt[$i][$amt[$h]])) {
                                //  $stmt[$i][$amt[$h]]=str_replace('.00','',$stmt[$i][$amt[$h]]);
                                $stmt[$i][$amt[$h]]=str_replace(',','',$stmt[$i][$amt[$h]]);
                                $stmt[$i][$amt[$h]]=number_format($stmt[$i][$amt[$h]]);
                                // print_r("hello");
                                $stmt[$i][$amt[$h]]=$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].$stmt[$i][$amt[$h]].'.00';
                            }
                        }
                        for ($j = 0; $j < count($stmt); $j++) {
                            // print_r(count($stmt));die;
                            $arryKeys=(array_keys($stmt[0]));
                            $amt=array_values(preg_grep("/amt/",$arryKeys));
                            for($h=0;$h<count($amt);$h++){

                                // print_r(($amt));die;
                                if (isset($stmt[$j][$amt[$h]])  && !empty($stmt[$j][$amt[$h]])) {
                                    if (isset($stmt[$i][$amt[$h]]) && $stmt[$j][$amt[$h]] != '') {
                                        $stmt[$j][$amt[$h]] = str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'], '', $stmt[$j][$amt[$h]]);
                                        $stmt[$j][$amt[$h]] = str_replace(',', '', $stmt[$j][$amt[$h]]);
                                        $stmt[$j][$amt[$h]] = str_replace(' ', '', $stmt[$j][$amt[$h]]);
                                        $stmt[$j][$amt[$h]] = number_format($stmt[$j][$amt[$h]]);
                                        $stmt[$j][$amt[$h]] = $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . $stmt[$j][$amt[$h]] . '.00';
                                    }
                                }
                            }

                            if (isset($stmt[$j]['status']) && !empty($stmt[$j]['status'])) {
                                if ($title == 'Rent Roll ') {
                                    if ($stmt[$j]['status'] == '1') {
                                        $stmt[$j]['status'] = str_replace('1', 'Vacant--Available', $stmt[$j]['status']);
                                    } else if ($stmt[$j]['status'] == '2') {
                                        $stmt[$j]['status'] = str_replace('2', 'Unrentable', $stmt[$j]['status']);
                                    } else if ($stmt[$j]['status'] == '4') {
                                        $stmt[$j]['status'] = str_replace('4', 'Occupied', $stmt[$j]['status']);
                                    } else if ($stmt[$j]['status'] == '5') {
                                        $stmt[$j]['status'] = str_replace('5', 'Notice--Available', $stmt[$j]['status']);
                                    } else if ($stmt[$j]['status'] == '6') {
                                        $stmt[$j]['status'] = str_replace('6', 'Vacant--Rented', $stmt[$j]['status']);
                                    } else if ($stmt[$j]['status'] == '7') {
                                        $stmt[$j]['status'] = str_replace('7', 'Notice--Rented', $stmt[$j]['status']);
                                    } else if ($stmt[$j]['status'] == '8') {
                                        $stmt[$j]['status'] = str_replace('8', 'Under--Make--Ready', $stmt[$j]['status']);
                                    }
                                } else {
                                    if ($stmt[$j]['status'] == '1' || $stmt[$i]['status'] == '3') {
                                        $stmt[$j]['status'] = str_replace('1', 'Active', $stmt[$j]['status']);
                                    }
                                    if ($stmt[$j]['status'] == '0') {
                                        $stmt[$j]['status'] = str_replace('0', 'Inactive', $stmt[$j]['status']);
                                    }
                                }
                            }

                            if (isset($stmt[$j]['vehicle_user_type']) && !empty($stmt[$j]['vehicle_user_type'])) {
                                if ($stmt[$j]['vehicle_user_type'] == '8') {
                                    $stmt[$j]['vehicle_user_type'] = str_replace('8', 'Employee', $stmt[$j]['vehicle_user_type']);
                                } else if ($stmt[$j]['vehicle_user_type'] == '2') {
                                    $stmt[$j]['vehicle_user_type'] = str_replace('2', 'Tenant', $stmt[$j]['vehicle_user_type']);
                                } else {
                                    $stmt[$j]['vehicle_user_type'] = str_replace($stmt[$j]['vehicle_user_type'], 'Company', $stmt[$j]['vehicle_user_type']);
                                }
                            }

                        }

                        // Bind the query params
                        // $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
                        // $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
                        //$stmt->execute();
                        // Do we have any results?
                        $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                        $company_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'users');
                        if (!empty($stmt)) {
                            if ($layout == "PrintEnvelope") {
                                //   print_r($layout);die;
                                $html .= '
<table width="100%"  style=" line-height: 20px; font-size: 8pt; font-style: normal; font-family: Arial; font-weight: 400; margin-top: 40px;">
    <tbody>
    
        <tr>
            <td>' . $company_data['data']['company_name'] . '</td>
        </tr>
        <tr>
            <td>' . $company_data['data']['address1'] . ' ' . $company_data['data']['address2'] . ' ' . $company_data['data']['address3'] . ' ' . $company_data['data']['address4'] . '</td>
        </tr>
        <tr>
            <td>' . $company_data['data']['city'] . ', ' . $company_data['data']['city'] . $company_data['data']['zipcode'] . '</td>
        </tr>
    </tbody>
</table>
<table width="60%" align="right" style="line-height: 20px; font-size: 8pt; font-style: normal; font-family: Arial; font-weight: 400;">
<tbody>';
                                // print_r(count($stmt));die;
                                for ($k = 0; $k < count($stmt); $k++) {
                                    $implode = (implode(' ', $stmt[$k]));
                                    $columns = explode(' ', $implode);
                                    for ($j = 0; $j < count($columns); $j++) {
                                        $dateReplace = $this->valid_date($columns[$j]);
                                        if (preg_match("--", $columns[$j])) {
                                            $columns[$j] = str_replace('--', ' ', $columns[$j]);
                                            $columns[$j] = str_replace(',', ',' . ' ', $columns[$j]);
                                        }
                                        if ($dateReplace == 1) {
                                            $columns[$j] = dateFormatUser($columns[$j], null, $this->companyConnection);
                                        }
                                        if ($columns[$j] != '') {
                                            $html .= '
                           <tr><td width="50%">
                                ' . $columns[$j];
                                        }
                                    }
                                    $html .= '</td></tr>';
                                }
                                $html .= '
    
    </tbody>
</table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
';
                                //   echo($html);
                                //  return;
                            } else if ($layout == "PrintEnvelope2") {

                                $html .= '
<table cellpadding="0" cellspacing="0" width="100%" style=" line-height: 23px; font-size: 13pt; margin-top: 40px;">
    <tbody>
        <tr>
            <td>' . $company_data['data']['company_name'] . '</td>
        </tr>
        <tr>
            <td>' . $company_data['data']['address1'] . ' ' . $company_data['data']['address2'] . ' ' . $company_data['data']['address3'] . ' ' . $company_data['data']['address4'] . '</td>
        </tr>
        <tr>
            <td>' . $company_data['data']['city'] . ', ' . $company_data['data']['city'] . $company_data['data']['zipcode'] . '</td>
        </tr>
    </tbody>
</table>


<table cellpadding="0" cellspacing="0" width="60%" align="right" style=" line-height: 23px; font-size: 13pt;">
    <tbody>
        <tr><td width="50%">&nbsp;</td>';

                                for ($k = 0; $k < count($stmt); $k++) {
                                    $implode = (implode(' ', $stmt[$k]));
                                    $columns = explode(' ', $implode);
                                    for ($j = 0; $j < count($columns); $j++) {
                                        $dateReplace = $this->valid_date($columns[$j]);
                                        if (preg_match("--", $columns[$j])) {
                                            $columns[$j] = str_replace('--', ' ', $columns[$j]);
                                            $columns[$j] = str_replace(',', ',' . ' ', $columns[$j]);
                                        }
                                        if ($dateReplace == 1) {
                                            $columns[$j] = dateFormatUser($columns[$j], null, $this->companyConnection);
                                        }
                                        if ($columns[$j] != '') {
                                            $html .= '
                           <tr><td width="50%">
                                ' . $columns[$j];
                                        }
                                    }
                                    $html .= '</td></tr>';
                                }
                                $html .= '</tr>';
                                $html .= '
          
  
    </tbody>
</table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


';
                                //echo($html);
                                //return;
                            } else if ($layout == "MailingLabel") {

                                $html .= ' <table cellpadding="0" cellspacing="0" width="100%" style=" line-height: 23px; font-size: 13pt; margin-top: 40px;">
                                <tbody> <tr>                                         <td style="padding-bottom: 20px; ">
';

                                for ($j = 0; $j < count($stmt); $j++) {
                                    foreach ($stmt[$j] as $k => $v) {
                                        $stmt[$j][$k] = str_replace('--', ' ', $v);
                                        $stmt[$j]['tenant_add'] = str_replace(',', ',' . ' ', $v);
                                    }

                                    $html .= '<p style="margin: 0; min-height: 120px; width: 33.33%; float: left; margin-bottom: 20px; line-height: 20px; font-size: 13px;">
                                               <span>' . $stmt[$j]['name'] . '</span><br>
                                               <span>' . $stmt[$j]['address1'] . '</span>';
                                    if ($stmt[$j]['address1'] != '') {
                                        $html .= '<br>';
                                    }
                                    $html .= '<span>' . $stmt[$j]['address2'] . '</span>';
                                    if ($stmt[$j]['address2'] != '') {
                                        $html .= '<br>';
                                    }
                                    $html .= '<span>' . $stmt[$j]['address3'] . '</span>';
                                    if ($stmt[$j]['address3'] != '') {
                                        $html .= '<br>';
                                    }
                                    $html .= '<span>' . $stmt[$j]['address4'] . '</span>';
                                    if ($stmt[$j]['address4'] != '') {
                                        $html .= '<br>';
                                    }
                                    $html .= '<span>' . $stmt[$j]['tenant_add'] . '</span>
                                              </p>';
                                }
                                $html .= '                                         </td>
</tr></tbody></table>';
                                //  echo($html);
                                // return;

                            } else if ($layout == "MailingLabel2") {

                                $html .= ' <table cellpadding="0" cellspacing="0" width="100%" style=" line-height: 23px; font-size: 13pt; margin-top: 40px;">
                                <tbody> <tr>                                         <td style="padding-bottom: 20px; ">
';
                                for ($j = 0; $j < count($stmt); $j++) {
                                    foreach ($stmt[$j] as $k => $v) {
                                        $stmt[$j][$k] = str_replace('--', ' ', $v);
                                        $stmt[$j]['tenant_add'] = str_replace(',', ',' . ' ', $v);
                                    }

                                    $html .= '<p style="margin: 0; min-height: 120px; width: 50%; float: left; margin-bottom: 20px; line-height: 20px; font-size: 13px;">
                                               <span>' . $stmt[$j]['name'] . '</span><br>
                                               <span>' . $stmt[$j]['address1'] . '</span>';
                                    if ($stmt[$j]['address1'] != '') {
                                        $html .= '<br>';
                                    }
                                    $html .= '<span>' . $stmt[$j]['address2'] . '</span>';
                                    if ($stmt[$j]['address2'] != '') {
                                        $html .= '<br>';
                                    }
                                    $html .= '<span>' . $stmt[$j]['address3'] . '</span>';
                                    if ($stmt[$j]['address3'] != '') {
                                        $html .= '<br>';
                                    }
                                    $html .= '<span>' . $stmt[$j]['address4'] . '</span>';
                                    if ($stmt[$j]['address4'] != '') {
                                        $html .= '<br>';
                                    }
                                    $html .= '<span>' . $stmt[$j]['tenant_add'] . '</span>
                                              </p>';
                                }
                                $html .= '                                         </td>
</tr></tbody></table>';
                                // echo($html);
                                //   return;

                            } else if ($layout == "3") {
                                $html .= '<div class="grid-outer htmldownloadd" id="htmldownloadd">
                        <div class="table-responsive Lease-table">
                        <table width="100%" border="0">
                            <tr>
                                <td><img style="width: 100px; height: 70px;" src="'.$imgg.'" alt=""> </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; font-weight: 700; font-size: 16px;">' . $title . '</td>
                            </tr>
                             <tr>
                                <td style="text-align: center; font-weight: 700; padding: 20px 0; font-size: 16px;">' . ($_SESSION[SESSION_DOMAIN]['formated_date']) . '</td>
                            </tr>
                            <tr>
                            <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; background: #262750; color: #fff; padding: 5px; font-size: 13px; font-weight: 700;" class="lease-hading"> ' . $table_title2 . '<br> ' . $table_title . '</td>
                            </tr>
                        </table>
                                               
                        <table width="100%" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; font-size: 12px;" class="table table-hover table-dark reports_table">
                        <thead>
                           <tr >';
                                for ($i = 0; $i < count($column_name); $i++) {
                                    $html .= '<th style="text-align: center; vertical-align: top;  background: #d3d3d3; color: #000000; border-right: 1px solid #ddd; font-size: 10px; padding: 10px 5px;" scope="col">' . $column_name[$i] . '</th>';
                                }
                                $html .= '</tr>
                        </thead>
                        <tbody>
                        
                        
                           ';
                                for ($i = 0; $i < count($stmt); $i++) {
                                    $html .= '<tr>';
                                    $implode = (implode(' ', $stmt[$i]));
                                    $columns = explode(' ', $implode);
                                    for ($j = 0; $j < count($columns); $j++) {
                                        $dateReplace = $this->valid_date($columns[$j]);
                                        if (preg_match("--", $columns[$j])) {
                                            $columns[$j] = str_replace('--', ' ', $columns[$j]);
                                        }
                                        if ($dateReplace == 1) {
                                            $columns[$j] = dateFormatUser($columns[$j], null, $this->companyConnection);
                                        }
                                        $html .= ' <td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 10px;"><a style=" color: #05A0E4; font-weight: 700;" class="grid-link" href="javascript:;">' . $columns[$j] . '</a></td>                                                            
                                ';
                                    }
                                    $html .= '</tr>';
                                }
                                $html .= '  </tbody>                           
                        </table>  ';
                                $html .= '
    <table style="width: 100%; margin-top: 100px">
        <tbody>
            <tr style="width: 22%; display: inline-block; margin: 0 10px;">
                <td style="width: 100%; display: inline-block;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">SEX</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">FEMALE</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">MALE</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">OTHER</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px; font-weight: bold;">TOTAL</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>     
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="width: 22%; display: inline-block; vertical-align: top; margin: 0 10px;">
                <td style="width: 100%; display: inline-block;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">ETHINICITY</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 75%; border: 1px solid black; padding-left: 5px;">American Indian/Alaskan Native</td>
                                <td style="width: 25%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Asian</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Black (not Hispanic or Latino)</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Hispanic or Latino</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Native Hawaiian / Pacific Islande</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Two or More Races (not Hispanice or Latino)</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Not Sure</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Other</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">KJK</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px; font-weight: bold">Total</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                                 
                        </tbody>
                    </table>
                </td>
            </tr>
            
            <tr style="width: 22%; display: inline-block; vertical-align: top; margin: 0 10px;">
                <td style="width: 100%; display: inline-block;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">Veteran Status</th>
 
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 75%; border: 1px solid black; padding-left: 5px;">Newly Seperated Veteran</td>
                                <td style="width: 25%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Veteran of Vietnam Era</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Special Disabled Veteran</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Veteran</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Other Protected Veteran</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Rgdg</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">ggg</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">LI</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">KJK</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">TEST</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>       
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px; font-weight: bold">Total</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>                         
                        </tbody>
                    </table>
                </td>
            </tr>
            
            <tr style="width: 22%; display: inline-block; vertical-align: top; margin: 0 10px;">
                <td style="width: 100%; display: inline-block;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">Marital Status</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 75%; border: 1px solid black; padding-left: 5px;">21212</td>
                                <td style="width: 25%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">GgGgh</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Married</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Married</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Single</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Test</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px; font-weight: bold">Total</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>                         
                        </tbody>
                    </table>
                </td>
            </tr>
            
        </tbody>
</table>
    ';
                            } else {
//echo "<pre>";print_r("kk");die;
                                /* if (!empty($stmt)) {*/
                                if (!empty($stmt)) {
                                    $imgg=COMPANY_DIRECTORY_URL.'/images/logo.png';
                                    $html .= '<html><body><div><div><div class="grid-outer htmldownloadd" id="htmldownloadd">
                        <div class="table-responsive Lease-table">';
                                    $html .= '
                        <table width="100%" border="0">';
                            if($i==0){
                                $html.= '
                            <tr>
                                <td><img style="width: 100px; height: 70px;" src="'.$imgg.'" alt=""> </td>
                            </tr>';

                                    }
                           $html.='<tr>
                                <td style="text-align: center; font-weight: 700; font-size: 16px;">' . $title . '</td>
                            </tr>
                             <tr>
                                <td style="text-align: center; font-weight: 700; padding: 20px 0; font-size: 16px;">' . $filter_date . '</td>
                            </tr>
                            <tr>
                            <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; background: #262750; color: #fff; padding: 5px; font-size: 13px; font-weight: 700;" class="lease-hading"> ' . $table_title2 . '<br> ' . $table_title . '</td>
                            </tr>
                        </table>
                                               
                        <table width="100%" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; font-size: 12px;" class="table table-hover table-dark reports_table">
                        <thead>
                           <tr >';
                                    // print_r($column_name);
                                    for ($l = 0; $l < count($column_name); $l++) {

                                        $html .= '<th style="text-align: center; vertical-align: top;  background: #d3d3d3; color: #000000; border-right: 1px solid #ddd; font-size: 10px; padding:5px;" scope="col">' . $column_name[$l] . '</th>';
                                    }
                                    $html .= '</tr>
                        </thead>
                        <tbody>
                        
                        
                           ';

                                    for ($y = 0; $y < count($stmt); $y++) {

                                        // print_r($stmt[$i]['amt']);
                                        $var = str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'], '', $stmt[$y]['amt']);
                                        $var = str_replace(',', '', $var);
                                        //print_r($var);die;
                                        $html .= '<tr>';
                                        $implode = (implode(' ', $stmt[$y]));
                                        $columns = explode(' ', $implode);

                                        for ($j = 0; $j < count($columns); $j++) {
                                            $dateReplace = $this->valid_date($columns[$j]);
                                            if (preg_match("--", $columns[$j])) {
                                                $columns[$j] = str_replace('--', ' ', $columns[$j]);
                                            }
                                            if ($dateReplace == 1) {
                                                $columns[$j] = dateFormatUser($columns[$j], null, $this->companyConnection);
                                            }
                                            $html .= ' <td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 10px;"><a style=" font-size: 7pt; color: #000000;text-decoration: none; font-weight: 400;" class="" href="javascript:;">' . $columns[$j] . '</a></td>                                                            
                                ';
                                        }
                                        $html .= '</tr>';

                                    }

                                    if($title=='Rent Roll '){
                                        $occ_status=[];
                                        $square_feet=[];
                                        $market_rent=[];
                                        $current_rent=[];
                                        $deposit=[];
                                        $amtlast=[];
                                        $nfs_count=[];
                                        $late_count=[];
                                        for($occ=0;$occ<count($stmt);$occ++){
                                            array_push($square_feet,$stmt[$occ]['square_ft']);
                                            array_push($late_count,$stmt[$occ]['late_count']);
                                            $total_market_rent=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$stmt[$occ]['amt']);
                                            $total_market_rent=str_replace(',','',$total_market_rent);
                                            $total_market_rent=str_replace('.00','',$total_market_rent);
                                            array_push($market_rent,$total_market_rent);

                                            $total_current_rent=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$stmt[$occ]['amt1']);
                                            $total_current_rent=str_replace(',','',$total_current_rent);
                                            $total_current_rent=str_replace('.00','',$total_current_rent);
                                            array_push($current_rent,$total_current_rent);

                                            $total_deposit=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$stmt[$occ]['amt2']);
                                            $total_deposit=str_replace(',','',$total_deposit);
                                            $total_deposit=str_replace('.00','',$total_deposit);
                                            array_push($deposit,$total_deposit);

                                            $past_due=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$stmt[$occ]['amt3']);
                                            $past_due=str_replace(',','',$past_due);
                                            $past_due=str_replace('.00','',$past_due);
                                            array_push($amtlast,$past_due);

                                            if($stmt[$occ]['status']=="Occupied"){
                                                array_push($occ_status,$stmt[$occ]['status']);

                                            }
                                            // print_r($stmt[$occ]);die();
                                        }
                                        // print_r($stmt);die;
                                        $occupied_per=(count($occ_status))/count($stmt)*100;
                                        array_push($grand_total_occ_status,count($occ_status));
                                        $occupied_per=number_format((float)$occupied_per,2,'.','');
                                        $occupied_per='Occupied:'.$occupied_per.'%';
                                        $total_mart=array_sum($market_rent);
                                        array_push($grand_total_total_mart,$total_mart);
                                        $total_mart=$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].number_format($total_mart).'.00';
                                        $total_curr=array_sum($current_rent);
                                        array_push($grand_total_total_curr,$total_curr);
                                        $total_curr=$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].number_format($total_curr).'.00';
                                        $total_dep=array_sum($deposit);
                                        array_push($grand_total_total_dep,$total_dep);
                                        $total_dep=$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].number_format($total_dep).'.00';
                                        $total_sq_ft=array_sum($square_feet);
                                        array_push($grand_total_sq_ft,$total_sq_ft);
                                        $total_sq_ft=$total_sq_ft.'Sq.Ft.';
                                        $total_past_due=array_sum($amtlast);
                                        array_push($grand_total_total_past_due,$total_past_due);
                                        $total_past_due=$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].number_format($total_past_due).'.00';
                                        $total_nfs_count=array_sum($nfs_count);
                                        array_push($grand_total_total_nfs_count,$total_nfs_count);
                                        $total_late_count=array_sum($late_count);
                                        array_push($grand_total_total_late_count,$total_late_count);

                                        $html.='<tr><td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: '.count($stmt).' </a></td>
                                       
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$occupied_per.'</a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$total_sq_ft.'</a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$total_mart.'</a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$total_curr.'</a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$total_dep.'</a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> </a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> </a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> </a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"></a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$total_past_due.'</a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$total_nfs_count.'</a></td>
                                        <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$total_late_count.'</a></td>
                                       </tr>
                                      ';
                                        array_push($grandtotal,count($stmt));
                                        $grandsatus=array_sum($grand_total_occ_status)/array_sum($grandtotal)*100;
                                        $grandsatus=number_format((float)$grandsatus,2,'.','');
                                        $grandsatus='Occupied:'.$grandsatus.'%';
                                        //  print_r($count);die;
                                        if($i==$count-1){
                                            $html.='<tr><td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> Grand Total: '.number_format(array_sum($grandtotal)).'</a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$grandsatus.'</a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.number_format(array_sum($grand_total_sq_ft)).'Sq.Ft.'.'</a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].number_format(array_sum($grand_total_total_mart)).'.00'.'</a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].number_format(array_sum($grand_total_total_curr)).'.00'.'</a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].number_format(array_sum($grand_total_total_dep)).'.00'.'</a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> </a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> </a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> </a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> </a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].number_format(array_sum($grand_total_total_past_due)).'.00'.'</a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.array_sum($grand_total_total_nfs_count).'</a></td>
<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;"> '.array_sum($grand_total_total_late_count).'</a></td>
                                       </tr>';
                                        }
                                    }else{
                                        $html.='<tr><td  colspan="14"  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: '.count($stmt).' </a></td>
                                       </tr>';
                                    }



                                    $html .= '  </tbody>                           
                        </table>  ';


                                }
                                //print_r($html);die();
                            }
                        }
                    }

                    if ($layout != 'PrintEnvelope') {
                        if ($layout != 'PrintEnvelope2') {
                            $html .= '                                                
                       <table style="margin-top: 0px; width: 100%;">
                            <tr>
                                <td style="text-align: right;">' . $_SESSION[SESSION_DOMAIN]['default_name'] . '</td>
                            </tr>
                           
                            <tr>
                        
                                <td>Printed On: '.' ' . date('F d, Y (D) h:i a', strtotime($_SESSION[SESSION_DOMAIN]['time_reports'])) . '</td>
                            </tr>
                        </table>
                        </div></div></div></div>'
                            ;
                        }
                    }
                    $html.='</body></html>';

                    return print_r($html); die;
                    //echo json_encode (array($html));die;
                    // print_r($html);
                }
            }

        } catch (Exception $e) {
            echo '<p>', $e->getMessage(), '</p>';
        }
    }



    public function notarrayall($total,$count,$page,$column,$dbquery,$filter_html,$title,$filter_data,$pagination,$layout,$column_name,$result,$filter_date )
    {
        $imgg=COMPANY_DIRECTORY_URL.'/images/logo.png';
        /* $query1 = $this->companyConnection->query($dbquery . ' AND gp.id =' . "'" . $filter_data[$i] . "'");
         $pagin = $query1->fetchAll();*/
        /* $query1 = $this->companyConnection->query($dbquery . ' AND gp.id =' . "'" . $filter_data[$i] . "'");
         $pagin = $query1->fetchAll();*/
        /* $query1 = $this->companyConnection->query($dbquery . ' AND gp.id =' . "'" . $filter_data[$i] . "'");
         $pagin = $query1->fetchAll();*/
        if (is_int($filter_data)) {
            $query2 = $this->companyConnection->query("SELECT property_name,portfolio_id FROM general_property WHERE id=" . $filter_data);
            $table_name = $query2->fetch();
            $table_title = $table_name['property_name'];
            if (isset($table_name['portfolio_id']) && $table_name['portfolio_id'] != '') {
                $portfolio_id = $table_name['portfolio_id'];
            } else {
                $portfolio_id = '0';
            }
          //  $query3 = $this->companyConnection->query("SELECT portfolio_name FROM company_property_portfolio WHERE id=" . $portfolio_id);
            //$table_name2 = $query3->fetch();
            $table_title2 = $table_name2['portfolio_name'];
        } else {
            $table_title2 = " ";
            $table_title = " ";
        }
        $setlimit = 0;
        $limit = $setlimit;
        /*  else{
                  $limit = 10;
              }*/
        // How many pages will there be
        //$pages = ceil($total / $limit);
        /* print_r($layout);die;*/
        if (is_array($filter_data) && $layout != "MailingLabel" && $layout != "MailingLabel2" && $layout != "DirectRedirection") {
            $pages = count($filter_data);

        } elseif ($layout == "MailingLabel" || $layout == "MailingLabel2" || $layout == "DirectRedirection") {

            $testpage = count($result);
            // print_r($result);die;
            if ($testpage < 10) {
                $pages = '1';
                $offset = $page * 9;
                //print_r("helllo");die;
            } else {
                $testpage1 = $testpage / 10;
                if (strpos($testpage1, '.')) {
                    $test = explode('.', $testpage1);
                    $pages = $test[0] + 1;
                    $offset = $page * 9;

                    //print_r($pages);die;
                } else {
                    $pages = $testpage1;
                    $offset = $page * 9;
                }
            }
        } else {
            $pages = 1;
        }
        // What page are we currently on?
        $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
            'options' => array(
                'default' => 1,
                'min_range' => 1,
            ),
        )));
        // Calculate the offset for the query
        // $offset = ($page - 1) * $limit;
        // Some information to display to the user
        //$start = $offset + 1;
        //$end = min(($offset + $limit), $total);
        // The "back" link
        $prevlink = ($page > 1) ? '<a href="?page=1" title="First page">&laquo;</a> <a href="?page=' . ($page - 1) . '" title="Previous page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';
        // The "forward" link
        $nextlink = ($page < $pages) ? '<a href="?page=' . ($page + 1) . '" title="Next page">&rsaquo;</a> <a href="?page=' . $pages . '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';
        // Display the paging information
        /*  echo '<div id="paging"><p>', $prevlink, ' Page ', $page, ' of ', $pages, ' pages, displaying ', $start, '-', $end, ' of ', $total, ' results ', $nextlink, ' </p></div>';*/
        $html = '';


//            $html.= '<section class="main-content">
        // Prepare the paged query
        //print_r($layout);die;

        if (strpos($dbquery, 'gp.') == false && $layout != "PrintEnvelope" && $layout != "PrintEnvelope2" && $layout != "MailingLabel" && $layout != "MailingLabel2" && $layout != "DirectRedirection") {
            $qry = $dbquery . ' ORDER BY name ASC ;';
            $qry1 = $dbquery ;
        } elseif (strpos($dbquery, 'gp.') == false && $layout != "PrintEnvelope" && $layout != "PrintEnvelope2" && $layout != "DirectRedirection" && $layout == "MailingLabel" && $layout != "MailingLabel2") {
            $qry = $dbquery . ' ORDER BY name ASC ;';
            $qry1 = $dbquery ;
            //echo "<preffffww>";print_r($qry);
            // print_r($qry);die();
            // print_r($qry);die;
        } elseif (strpos($dbquery, 'gp.') == false && $layout != "PrintEnvelope" && $layout != "PrintEnvelope2" && $layout != "MailingLabel" && $layout != "DirectRedirection" && $layout == "MailingLabel2") {
            $qry = $dbquery . ' ORDER BY name ASC;';
            $qry1 = $dbquery ;
           // echo "<preffrrff>";print_r($qry);;
            //print_r($qry);die;
        } elseif ($layout != "PrintEnvelope" && $layout != "PrintEnvelope2" && $layout != "MailingLabel" && $layout != "MailingLabel2" && $layout == "DirectRedirection") {
            // print_r("hello");die;
            $qry = $dbquery . ' ORDER BY name ASC;';
            $qry1 = $dbquery ;
          //  echo "<preffffff>";print_r($qry);
            // print_r($qry);die;
        } elseif ($layout == "PrintEnvelope" || $layout == "PrintEnvelope2") {

//            $qry = $dbquery . ' AND u.id=' . $filter_data . ' ORDER BY name ASC ;';

            if (isset($print_envelope_by) && !empty($print_envelope_by)) {
                $qry = $dbquery . ' AND ' . $print_envelope_by . '.id=' . $filter_data;

                $qry1 = $dbquery ;
            } else {
                $qry = $dbquery . ' AND u.id=' . $filter_data . ' ORDER BY name ASC ;';
              //  echo "<prefff>";print_r($qry);
                $qry1 = $dbquery ;
            }

        } else {

            $qry = $dbquery . ' AND gp.id=' . $filter_data . ' ORDER BY name ASC ;';
            $qry1 = $dbquery ;
          //  echo "<preff>";print_r($qry);
            //   print_r($qry);die;
        }



        $stmt = $this->companyConnection->query($qry)->fetchAll();

      // echo "<preff>";print_r($stmt);
        $stmt1 = $this->companyConnection->query($qry1)->fetchAll();


        $arryKeys=(array_keys($stmt[0]));
        $amt=array_values(preg_grep("/amt/",$arryKeys));
        $totalmt1=[];
        $totalmt2=[];
        $totalmt3=[];
        $totalmt4=[];
        $totalunit=[];
        $totalten=[];
        $totaltencon3=[];
        $totaltencon2=[];
        $totaltencon1=[];
        $totalunitfeature=[];
        $totalunitcount=[];
        $totalstarting_mileage=[];
        $totalowner1=[];
        $totalowner2=[];
        $totalownerwork1=[];
        $totalvendorwork1=[];
        $totalInventoryDetail=[];
        $totalvendor1099=[];
        $totalvendorsumm=[];
        for ($i = 0; $i < count($stmt); $i++){
        for($h=0;$h<count($amt);$h++){
            // print_r(($amt));die;
            if (isset($stmt[$i][$amt[$h]])  && !empty($stmt[$i][$amt[$h]])) {
                //  $stmt[$i][$amt[$h]]=str_replace('.00','',$stmt[$i][$amt[$h]]);
                $stmt[$i][$amt[$h]]=str_replace(',','',$stmt[$i][$amt[$h]]);
                $stmt[$i][$amt[$h]]=number_format($stmt[$i][$amt[$h]]);
                // print_r("hello");
                $stmt[$i][$amt[$h]]=$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].$stmt[$i][$amt[$h]].'.00';
            }
        }
        }
        for ($i = 0; $i < count($stmt); $i++) {
            if (isset($stmt[$i]['status']) && !empty($stmt[$i]['status'])) {
                if ($stmt[$i]['status'] == '1') {
                    $stmt[$i]['status'] = str_replace('1', 'Active', $stmt[$i]['status']);
                }
                if ($stmt[$i]['status'] == '0') {
                    $stmt[$i]['status'] = str_replace('0', 'Inactive', $stmt[$i]['status']);
                }
            }
        }
        // Bind the query params
        // $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        // $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        //$stmt->execute();
        // Do we have any results?
        $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $company_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'users');
        if (!empty($stmt)) {
            if ($layout == "PrintEnvelope") {
                //   print_r($stmt);die;
                $html .= '<html><body>
<table width="100%"  style=" line-height: 20px; font-size: 8pt; font-style: normal; font-family: Arial; font-weight: 400; margin-top: 40px;">
    <tbody>
    
        <tr>
            <td>' . $company_data['data']['company_name'] . '</td>
        </tr>
        <tr>
            <td>' . $company_data['data']['address1'] . ' ' . $company_data['data']['address2'] . ' ' . $company_data['data']['address3'] . ' ' . $company_data['data']['address4'] . '</td>
        </tr>
        <tr>
            <td>' . $company_data['data']['city'] . ', ' . $company_data['data']['city'] . $company_data['data']['zipcode'] . '</td>
        </tr>
    </tbody>
</table>
<table width="60%" align="right" style="line-height: 20px; font-size: 8pt; font-style: normal; font-family: Arial; font-weight: 400;">
<tbody>';
                for ($i = 0; $i < count($stmt); $i++) {
                    $implode = (implode(' ', $stmt[$i]));
                    $columns = explode(' ', $implode);
                    for ($j = 0; $j < count($columns); $j++) {
                        $dateReplace = $this->valid_date($columns[$j]);
                        if (preg_match("--", $columns[$j])) {
                            $columns[$j] = str_replace('--', ' ', $columns[$j]);
                            $columns[$j] = str_replace(',', ',' . ' ', $columns[$j]);
                        }
                        if ($dateReplace == 1) {
                            $columns[$j] = dateFormatUser($columns[$j], null, $this->companyConnection);
                        }
                        if ($columns[$j] != '') {
                            $html .= '
                           <tr><td width="50%">
                                ' . $columns[$j];
                        }
                    }
                    $html .= '</td></tr>';
                }
                $html .= '
    
    </tbody>
</table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    
';
                //echo($html);
                //return;
            } else if ($layout == "PrintEnvelope2") {

                $html .= '
<table cellpadding="0" cellspacing="0" width="100%" style=" line-height: 23px; font-size: 13pt; margin-top: 40px;">
    <tbody>
        <tr>
            <td>' . $company_data['data']['company_name'] . '</td>
        </tr>
        <tr>
            <td>' . $company_data['data']['address1'] . ' ' . $company_data['data']['address2'] . ' ' . $company_data['data']['address3'] . ' ' . $company_data['data']['address4'] . '</td>
        </tr>
        <tr>
            <td>' . $company_data['data']['city'] . ', ' . $company_data['data']['city'] . $company_data['data']['zipcode'] . '</td>
        </tr>
    </tbody>
</table>


<table cellpadding="0" cellspacing="0" width="60%" align="right" style=" line-height: 23px; font-size: 13pt;">
    <tbody>
        <tr><td width="50%">&nbsp;</td>';

                for ($i = 0; $i < count($stmt); $i++) {
                    $implode = (implode(' ', $stmt[$i]));
                    $columns = explode(' ', $implode);
                    for ($j = 0; $j < count($columns); $j++) {
                        $dateReplace = $this->valid_date($columns[$j]);
                        if (preg_match("--", $columns[$j])) {
                            $columns[$j] = str_replace('--', ' ', $columns[$j]);
                            $columns[$j] = str_replace(',', ',' . ' ', $columns[$j]);
                        }
                        if ($dateReplace == 1) {
                            $columns[$j] = dateFormatUser($columns[$j], null, $this->companyConnection);
                        }
                        if ($columns[$j] != '') {
                            $html .= '
                           <tr><td width="50%">
                                ' . $columns[$j];
                        }
                    }
                    $html .= '</td></tr>';
                }
                $html .= '</tr>';
                $html .= '
          
  
    </tbody>
</table><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


';
                //echo($html);
                // return;
            } else if ($layout == "MailingLabel") {
                //print_r(count($stmt));die;
                $html = '<html><body><table cellpadding="0" cellspacing="0" width="100%" style=" line-height: 23px; font-size: 13pt; margin-top: 40px;"><tbody>';

                for ($j = 0; $j < count($stmt); $j++) {

                    foreach ($stmt[$j] as $k => $v) {
                        $stmt[$j][$k] = str_replace('--', '', $v);
                        $stmt[$j]['tenant_add'] = str_replace(',', ',' . '', $v);
                    }

                   // $html .= '<p style="margin: 0; min-height: 120px; float: left; width: 33.33%; margin-bottom: 20px; line-height: 20px; font-size: 13px;"><span>' . $stmt[$j]['name'] . '</span><br><span>' . $stmt[$j]['address1'] . '</span>';




                   // if($stmt[$j]['name'] != '' && $stmt[$j]['address1'] != '') {
                    //    if ($j != 0) {
                            if (($j % 3) == 0) {
                                if ($j != 0) {
                                    $html .= '</tr>';
                                }
                                $html .= '<tr>';
                            }
                     //   }

                        $html .= '<td style="padding-bottom: 20px;" width="33.33%"><p style="margin: 0; min-height: 120px; margin-bottom: 20px; line-height: 20px; font-size: 13px;"><span>' . $stmt[$j]['name'] . '</span><br><span>' . $stmt[$j]['address1'] . '</span>';
                        if ($stmt[$j]['address1'] != '') {
                            $html .= '<br>';
                        }
                        $html .= '<span>' . $stmt[$j]['address2'] . '</span>';
                        if ($stmt[$j]['address2'] != '') {
                            $html .= '<br>';
                        }
                        $html .= '<span>' . $stmt[$j]['address3'] . '</span>';
                        if ($stmt[$j]['address3'] != '') {
                            $html .= '<br>';
                        }
                        $html .= '<span>' . $stmt[$j]['address4'] . '</span>';
                        if ($stmt[$j]['address4'] != '') {
                            $html .= '<br>';
                        }
                        $html .= '<span>' . $stmt[$j]['tenant_add'] . '</span></p></td>';
                 //   }
                 //   $html .='</n>';



                }


                $html .= '</tbody></table></body></html>';

                 echo($html);
                 return;
            } else if ($layout == "MailingLabel2") {

                $html .= ' <html><body><table cellpadding="0" cellspacing="0" width="100%" style=" line-height: 23px; font-size: 13pt; margin-top: 40px;">
                                <tbody>
';
                for ($j = 0; $j < count($stmt); $j++) {

                    foreach ($stmt[$j] as $k => $v) {
                        $stmt[$j][$k] = str_replace('--', ' ', $v);
                        $stmt[$j]['tenant_add'] = str_replace(',', ',' . ' ', $v);
                    }

                        if (($j % 2) == 0) {
                            if ($j != 0) {
                                $html .= '</tr>';
                            }
                            $html .= '<tr>';
                        }
                  
                    $html .= '<td style="padding-bottom: 20px;" width="33.33%"><p style="margin: 0; min-height: 120px; margin-bottom: 20px; line-height: 20px; font-size: 13px;"><span>' . $stmt[$j]['name'] . '</span><br><span>' . $stmt[$j]['address1'] . '</span>';
                    if ($stmt[$j]['address1'] != '') {
                        $html .= '<br>';
                    }
                    $html .= '<span>' . $stmt[$j]['address2'] . '</span>';
                    if ($stmt[$j]['address2'] != '') {
                        $html .= '<br>';
                    }
                    $html .= '<span>' . $stmt[$j]['address3'] . '</span>';
                    if ($stmt[$j]['address3'] != '') {
                        $html .= '<br>';
                    }
                    $html .= '<span>' . $stmt[$j]['address4'] . '</span>';
                    if ($stmt[$j]['address4'] != '') {
                        $html .= '<br>';
                    }
                    $html .= '<span>' . $stmt[$j]['tenant_add'] . '</span></p></td>';
                }
                    $html .= '</tbody></table></body></html>';

                echo($html);
                return;

            } else if ($layout == "3") {
                $html .= '<div class="grid-outer htmldownloadd" id="htmldownloadd">
                        <div class="table-responsive Lease-table">
                        <table width="100%" border="0">
                           <tr>
                              <td>
                                 <img style="width: 100px; height: 70px;" src="'.$imgg.'" alt="">
                              </td>
                           </tr>
                           <tr>
                              <td style="text-align: center; font-weight: 700; font-size: 16px;">' . $title . '</td>
                           </tr>
                           <tr>
                              <td style="text-align: center; font-weight: 700; padding: 20px 0; font-size: 16px;">' . ($_SESSION[SESSION_DOMAIN]['formated_date']) . '</td>
                           </tr>
                           <tr>
                              <td style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; background: #262750; color: #fff; padding: 5px; font-size: 13px; font-weight: 700;" class="lease-hading"> ' . $table_title2 . '<br> ' . $table_title . '</td>
                           </tr>
                        </table>
                        <table width="100%" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; font-size: 12px;" class="table table-hover table-dark reports_table">
                        <thead>
                           <tr >';
                for ($i = 0; $i < count($column_name); $i++) {
                    $html .= '<th style="text-align: center; vertical-align: top;  background: #d3d3d3; color: #000000; border-right: 1px solid #ddd; font-size: 10px; padding: 10px 5px;" scope="col">' . $column_name[$i] . '</th>';
                }
                $html .= '</tr>
                        </thead>
                        <tbody>                       
                        
                           ';
                for ($i = 0; $i < count($stmt); $i++) {
                    $html .= '<tr>';
                    $implode = (implode(' ', $stmt[$i]));
                    $columns = explode(' ', $implode);
                    for ($j = 0; $j < count($columns); $j++) {
                        $dateReplace = $this->valid_date($columns[$j]);
                        if (preg_match("--", $columns[$j])) {
                            $columns[$j] = str_replace('--', ' ', $columns[$j]);
                        }
                        if ($dateReplace == 1) {
                            $columns[$j] = dateFormatUser($columns[$j], null, $this->companyConnection);
                        }
                        $html .= '<td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 10px;"><a style=" color: #05A0E4; font-weight: 700;" class="grid-link" href="javascript:;">' . $columns[$j] . '</a></td>';
                    }
                    $html .= '</tr>';
                }
                $html .= '  </tbody>                           
                        </table>  ';
                $html .= '
    <table style="width: 100%; margin-top: 100px">
        <tbody>
            <tr style="width: 22%; display: inline-block; margin: 0 10px;">
                <td style="width: 100%; display: inline-block;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">SEX</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">FEMALE</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">MALE</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">OTHER</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px; font-weight: bold;">TOTAL</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>     
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr style="width: 22%; display: inline-block; vertical-align: top; margin: 0 10px;">
                <td style="width: 100%; display: inline-block;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">ETHINICITY</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 75%; border: 1px solid black; padding-left: 5px;">American Indian/Alaskan Native</td>
                                <td style="width: 25%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Asian</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Black (not Hispanic or Latino)</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Hispanic or Latino</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Native Hawaiian / Pacific Islande</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Two or More Races (not Hispanice or Latino)</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Not Sure</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Other</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">KJK</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px; font-weight: bold">Total</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                                 
                        </tbody>
                    </table>
                </td>
            </tr>
            
            <tr style="width: 22%; display: inline-block; vertical-align: top; margin: 0 10px;">
                <td style="width: 100%; display: inline-block;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">Veteran Status</th>
 
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 75%; border: 1px solid black; padding-left: 5px;">Newly Seperated Veteran</td>
                                <td style="width: 25%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Veteran of Vietnam Era</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Special Disabled Veteran</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Veteran</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Other Protected Veteran</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Rgdg</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">ggg</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">LI</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">KJK</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">TEST</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>       
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px; font-weight: bold">Total</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>                         
                        </tbody>
                    </table>
                </td>
            </tr>
            
            <tr style="width: 22%; display: inline-block; vertical-align: top; margin: 0 10px;">
                <td style="width: 100%; display: inline-block;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">Marital Status</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 75%; border: 1px solid black; padding-left: 5px;">21212</td>
                                <td style="width: 25%; border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">GgGgh</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Married</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Married</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Single</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Test</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px; font-weight: bold">Total</td>
                                <td style="border: 1px solid black; padding-left: 5px;">0</td>
                            </tr>                         
                        </tbody>
                    </table>
                </td>
            </tr>
            
        </tbody>
</table>
    ';
            } else {
                /* if (!empty($stmt)) {*/
                // echo "<preee>";print_r("kkff");die;
            //print_r($stmt);die;
                $html .= '<html><body><div class="grid-outer htmldownloadd" id="htmldownloadd">
                        <div class="table-responsive Lease-table">
                        <table width="100%" border="0">
                           <tr>
                              <td>
                                 <img style="width: 100px; height: 70px;" src="'.$imgg.'" alt=""> 
                              </td>
                           </tr>
                          <tr>
                                <td style="text-align: center; font-weight: 700; font-size: 16px;">' . $title . '</td>
                            </tr>
                             <tr>
                                <td style="text-align: center; font-weight: 700; padding: 20px 0; font-size: 16px;">' . $filter_date . '</td>
                            </tr>
                           
                        </table>
                                               
                        <table width="100%" style="font-family: Helvetica Neue,Helvetica,Arial,sans-serif; font-size: 12px;" class="table table-hover table-dark reports_table">
                        <thead>
                           <tr >';
                for ($t = 0; $t < count($column_name); $t++) {
                    $html .= '<th style="text-align: center; vertical-align: top;  background: #262750; color: #fff; border-top: 1px solid #262750; border-right: 1px solid #262750; font-size: 10px; padding: 5px;" scope="col">' . $column_name[$t] . '</th>';

                }

                $html .= '</tr>
                        </thead>
                        <tbody>
                           ';

                //print_r($title);die();
                if($title=='Property Group '){

                    $html .= '<tr>';

                    $test=[];
                    for($z = 0; $z < count($stmt); $z++){
                        if(isset($stmt[$z]['property_group']) && !empty($stmt[$z]['property_group'])) {
                            $property_group = unserialize($stmt[$z]['property_group']);
                            $property_group = implode(',', $property_group);
                            array_push($test, $property_group);

                        }
                    }
                    $property_group = implode(',', $test);
                    $count=explode(',',$property_group);

                    $whatIWant = substr($dbquery, strpos($dbquery, "WHERE")+5);
                    $group_quer="SELECT REPLACE(cpg.group_name,' ','--') AS name, REPLACE(gp.property_name,' ','--') AS prop_name,CONCAT(REPLACE(gp.address1,' ','--'), '--', REPLACE(gp.address2,' ','--'),'--',REPLACE(gp.address3,' ','--'), '--', REPLACE(gp.address4,' ','--')) AS address FROM general_property gp JOIN company_property_groups cpg ON gp.id IN (".$property_group.") WHERE ".$whatIWant;
                    $stmt6 = $this->companyConnection->query($group_quer)->fetchAll();
                    // print_r($stmt6);die;
                    for($kg=0;$kg<count($stmt6);$kg++) {
                        $implode = (implode(' ', $stmt6[$kg]));
                        $columns = explode(' ', $implode);

                        for ($j = 0; $j < count($columns); $j++) {
                            $dateReplace = $this->valid_date($columns[$j]);
                            if (preg_match("--", $columns[$j])) {
                                $columns[$j] = str_replace('--', ' ', $columns[$j]);
                            }
                            if ($dateReplace == 1) {
                                $columns[$j] = dateFormatUser($columns[$j], null, $this->companyConnection);
                            }
                            $html .= ' <td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 3px;
    padding-top: 3px;">
                                    <a style=" color: #000000; font-size: 8pt; font-weight: 400; text-decoration: none;" class="" href="javascript:;">' . $columns[$j] . '</a></td>                                                            
                                ';
                        }
                        $html .= '</tr>';
                    }
                    if($page==$pages){
                        $html.='<tr><td  colspan="13"  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #05A0E4; font-weight: 700;" class="" href="javascript:;">Total: '.(count($count)+1).' </a></td>
                                       </tr>';
                    } }
                else {

                    for ($ii = 0; $ii < count($stmt); $ii++) {
                        $html .= '<tr>';
                        $implode = (implode(' ', $stmt[$ii]));
                        $columns = explode(' ', $implode);

                        for ($j = 0; $j < count($columns); $j++) {
                            $dateReplace = $this->valid_date($columns[$j]);
                            if (preg_match("--", $columns[$j])) {
                                $columns[$j] = str_replace('--', ' ', $columns[$j]);
                            }
                            if ($dateReplace == 1) {
                                $columns[$j] = dateFormatUser($columns[$j], null, $this->companyConnection);
                            }
                            $html .= ' <td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 3px;
    padding-top: 3px;">
                                    <a style=" color: #000000; font-size: 8pt; font-weight: 400; text-decoration: none;" class="" href="javascript:;">' . $columns[$j] . '</a></td>    
                                                                                            
                                ';
                        }

                    }
                  //  print_r($page);print_r($pages);die();
                    if (1 == 1) {

                        if ($title == "Portfolio ") {
                            $stmt2 = $this->companyConnection->query($dbquery . ' GROUP BY cpp.portfolio_name')->fetchAll();
                            foreach ($stmt1 as $key=>$value){

                                if(isset($value['Total_units']) && !empty($value['Total_units'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['Total_units']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalunitcount,$abc);



                                }
                            }

                            $html .= ' <tr><td colspan="3" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'. number_format(array_sum($totalunitcount)).'  </a></td>

 </tr>';

                        }  else if($title=="Rent Roll Consolidated ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;
                            foreach ($stmt as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalmt1,$abc);

                                    $abc1=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt1']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc1= str_replace('.00','',$abc1);
                                    $abc1=str_replace(',','',$abc1);
                                    array_push($totalmt2,$abc1);

                                    $abc2=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt2']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc2= str_replace('.00','',$abc2);
                                    $abc2=str_replace(',','',$abc2);
                                    array_push($totalmt3,$abc2);

                                    $abc3=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt3']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc3= str_replace('.00','',$abc3);
                                    $abc3=str_replace(',','',$abc3);
                                    array_push($totalmt4,$abc3);
                                }
                            }

                            $html .= ' <tr><td colspan="5" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalmt1)). '.00'.'  </a></td>
 <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalmt2)). '.00'.'  </a></td>
 <td colspan="5" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalmt3)). '.00'.'  </a></td>
 <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalmt4)). '.00'.'  </a></td>
 </tr>';


                        }

                        else if($title=="Tenant List Consolidated ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;
                            foreach ($stmt1 as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totaltencon1,$abc);

                                    $abc1=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt1']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc1= str_replace('.00','',$abc1);
                                    $abc1=str_replace(',','',$abc1);
                                    array_push($totaltencon2,$abc1);

                                    $abc2=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt2']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc2= str_replace('.00','',$abc2);
                                    $abc2=str_replace(',','',$abc2);
                                    array_push($totaltencon3,$abc2);

                                }
                            }

                            $html .= ' <tr><td colspan="7" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totaltencon1)). '.00'.'  </a></td>
 <td  style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totaltencon2)). '.00'.'  </a></td>
 <td colspan="5" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totaltencon3)). '.00'.'  </a></td>
 
 </tr>';
                        }


                        else if($title=="Unit Features ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;
                            foreach ($stmt1 as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalunitfeature,$abc);

                                }
                            }

                            $html .= ' <tr><td colspan="8" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalunitfeature)). '.00'.'  </a></td>

 </tr>';
                        }
                        else if($title=="Tenant List ") {

                            //  print_r("hello");die;
                             //print_r($title);die;
                            foreach ($stmt1 as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalten,$abc);

                                }
                            }

                            $html .= ' <tr><td colspan="4" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalten)). '.00'.'  </a></td>
 </tr>';
                        }
                        else if($title=="Unit List ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;

                            foreach ($stmt1 as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $ab=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $ab= str_replace('.00','',$ab);
                                    $ab=str_replace(',','',$ab);
                                    array_push($totalunit,$ab);

                                }
                            }

                            $html .= ' <tr><td colspan="8" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #05A0E4; font-weight: 700;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalunit)). '.00'.'  </a></td>
 </tr>';
                        }
                        else if($title=="Employee / Company Vehicles ") {
                            //print_r(starting_mileage);die;

                            foreach ($stmt as $key=>$value){
                                if(isset($value['starting_mileage']) && !empty($value['starting_mileage'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['starting_mileage']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalstarting_mileage,$abc);                            }
                            }                        $html .= ' <tr><td colspan="10" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">' . number_format(array_sum($totalstarting_mileage)).'  </a></td>
 </tr>';                    }


                        else if($title=="Owner Withholdings ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;
                            foreach ($stmt as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalowner2,$abc);                                $abc1=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt1']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc1= str_replace('.00','',$abc1);
                                    $abc1=str_replace(',','',$abc1);
                                    array_push($totalowner1,$abc1);                            }
                            }                        $html .= ' <tr><td colspan="3" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalowner2)). '.00'.'  </a></td>
 <td colspan="3" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalowner1)). '.00'.'  </a></td>
 </tr>';
                        }

                        else if($title=="Owner Work Orders ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;
                            foreach ($stmt1 as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalownerwork1,$abc);                            }
                            }                        $html .= ' <tr><td colspan="11" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalownerwork1)). '.00'.'  </a></td>
</tr>';
                        }


                        else if($title=="Vendor Work Order ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;
                            foreach ($stmt1 as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalvendorwork1,$abc);                            }
                            }                        $html .= ' <tr><td colspan="10" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalvendorwork1)). '.00'.'  </a></td>
</tr>';
                        }


                        else if($title=="Inventory Detail ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;
                            foreach ($stmt1 as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalInventoryDetail,$abc);                            }
                            }                        $html .= ' <tr><td colspan="10" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalInventoryDetail)). '.00'.'  </a></td>
</tr>';
                        }

                        else if($title=="Vendor 1099 Detail ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;
                            foreach ($stmt1 as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalvendor1099,$abc);                            }
                            }                        $html .= ' <tr><td colspan="8" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalvendor1099)). '.00'.'  </a></td> </tr>';
                        }

                        else if($title=="Vendor 1099 Summary ") {
                            //  print_r($count);die;
                            // print_r($stmt[$i-1]['amt']);die;
                            foreach ($stmt1 as $key=>$value){
                                if(isset($value['amt']) && !empty($value['amt'])) {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc= str_replace('.00','',$abc);
                                    $abc=str_replace(',','',$abc);
                                    array_push($totalvendorsumm,$abc);
                                }
                            }                        $html .= ' <tr><td colspan="4" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000;
; font-weight: 700; text-decoration: none;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalvendorsumm)). '.00'.'  </a></td>
 </tr>';
                        }


                        else if(trim($title)=="Employee EEO") {
                          //print_r($count);die;
                        // print_r($stmt[$i-1]['amt']);die;
                        $martialStatus = [];
                        $veteranStatus = [];
                        $ethnicityStatus = [];
                        $maleFemaleStatus = [];
                        foreach ($stmt1 as $key1=>$value1){
                          //  print_r($value1);die;
                            foreach ($value1 as $key=>$value) {
                          //      print_r($value);die;
                                if ($key == 'amt') {
                                    //print_r($stmt[$i-1]['amt']);die;
                                    $abc = str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'], '', $value);
                                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
                                    $abc = str_replace('.00', '', $abc);
                                    $abc = str_replace(',', '', $abc);
                                    array_push($totalvendorsumm, $abc);
                                } else if ($key == 'martial') {
                                 //   print_r('sdgsdg');die;
                                    array_push($martialStatus, $value);
                                } else if ($key == 'veteran') {
                                    $value = str_replace('--', ' ', $value);
                                    array_push($veteranStatus, $value);
                                } else if ($key == 'title') {
                                    $value = str_replace('--', ' ', $value);
                                    array_push($ethnicityStatus, trim($value));
                                } else if($key == "if(u.gender = 1, 'Male', 'Female')") {
                                    array_push($maleFemaleStatus, trim($value));
                                }
                            }
                        }
                        $countsMartialStatus = array_count_values($martialStatus);
                        $countsVeteranStatus = array_count_values($veteranStatus);
                        $countsEthnicityStatus = array_count_values($ethnicityStatus);
                        $countsMaleFemaleStatus = array_count_values($maleFemaleStatus);
                      //  print_r($maleFemaleStatus);die;

                        $html .= ' <table style="width: 100%; margin-top: 100px">
        <tbody>
            <tr style=" margin: 0 10px;">
                <td style="width: 25%; padding-right: 1%;  vertical-align: top; ">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">SEX</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">FEMALE</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">'.$countsMaleFemaleStatus["Female"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">MALE</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">'.$countsMaleFemaleStatus["Male"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">OTHER</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">'.$countsMaleFemaleStatus["Other"].'</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px; font-weight: bold;">TOTAL</td>
                                <td style="width: 50%; border: 1px solid black; padding-left: 5px;">'.count($maleFemaleStatus).'</td>
                            </tr>     
                        </tbody>
                    </table>
                </td>
                 <td style="width: 25%; padding-right: 1%; vertical-align: top;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">ETHINICITY</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 75%; border: 1px solid black; padding-left: 5px;">American Indian/Alaskan Native</td>
                                <td style="width: 25%; border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["American Indian/Alaskan Native"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Asian</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["Asian"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Black (not Hispanic or Latino)</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["Black (not Hispanic or Latino)"].'</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Hispanic or Latino</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["Hispanic or Latino"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Native Hawaiian / Pacific Islander</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["Native Hawaiian / Pacific Islander"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Two or More Races (not Hispanice or Latino)</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["Two or More Races (not Hispanice or Latino)"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Anglo indian</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["Anglo indian"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">White (not Hispanic or Latino)</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["White (not Hispanic or Latino)"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Not Sure</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["Not Sure"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Other</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsEthnicityStatus["Other"].'</td>
                            </tr>
                           
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px; font-weight: bold">Total</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.count($ethnicityStatus).'</td>
                            </tr>
                                 
                        </tbody>
                    </table>
                </td>
                    <td style="width: 25%; padding-right: 1%; vertical-align: top;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">Veteran Status</th>
 
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="width: 75%; border: 1px solid black; padding-left: 5px;">Newly Separated Veteran</td>
                                <td style="width: 25%; border: 1px solid black; padding-left: 5px;">'.$countsVeteranStatus["Newly Separated Veteran"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Veteran of Vietnam Era</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsVeteranStatus["Veteran of Vietnam Era"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Special Disabled Veteran</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsVeteranStatus["Special Disabled Veteran"].'</td>
                            </tr> 
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Veteran</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsVeteranStatus["Veteran"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Other Protected Veterans</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsVeteranStatus["Other Protected Veterans"].'</td>
                            </tr>
                            
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px; font-weight: bold">Total</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.count($veteranStatus).'</td>
                            </tr>                         
                        </tbody>
                    </table>
                </td>
                                <td style="width: 25%; padding-right: 1%; vertical-align: top;">
                    <table style="width: 100%;">
                        <thead>
                           <tr style="height: 22px; border: 1px solid black;"">
                                <th colspan="2" style="text-align: center; font-weight: bold; line-height: 22px;">Marital Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Married</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsMartialStatus["Married"].'</td>
                            </tr>
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px;">Single</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.$countsMartialStatus["Single"].'</td>
                            </tr>
                           
                            <tr style="height: 22px; line-height: 22px;">
                                <td style="border: 1px solid black; padding-left: 5px; font-weight: bold">Total</td>
                                <td style="border: 1px solid black; padding-left: 5px;">'.count($martialStatus).'</td>
                            </tr>                         
                        </tbody>
                    </table>
                </td>


            </tr>
          
            
        </tbody>
</table>';
                    }


                        else {
                            $html .= '<tr><td colspan="'.count($column_name).'" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 1px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #000000; font-weight: 700; text-decoration: none;" class="" href="javascript:;">Total: ' . count($stmt1) . ' </a></td>';


                            $html.='</tr>';
                        }
                    }
                }
                //echo "<pre>"; print_r($html);die();

                $html .= '  </tbody>                           
                        </table> 
                   ';
                if ($layout != 'PrintEnvelope') {
                    if ($layout != 'PrintEnvelope2') {
                        //  echo "<prde>";print_r($pages);
                        $html .= '
                       
                          
                       <table style="margin-top: 0px; width: 100%;">
                            <tr>
                                <td style="text-align: right; padding-top: 5px;">' . $_SESSION[SESSION_DOMAIN]['default_name'] . '</td>
                            </tr>
                            
                            <tr>
                        
                                <td>Printed On:' . date('F d,Y (D) h:i a', strtotime($_SESSION[SESSION_DOMAIN]['time_reports'])) . '</td>
                            </tr>
                          
                        </table>';
                    }
                }

                // echo "<prerr>"; return print_r($html); die;
                $html .= '</div>
                        </div>
                  </body></html>
  
';
                // echo $html;
                //echo include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
                // Define how we want to fetch the results
                //$stmt->setFetchMode(PDO::FETCH_ASSOC);
                /* $iterator = new IteratorIterator($stmt);
                 // Display the results
                 foreach ($iterator as $row) {
                     echo '<p>', $stmt['first_name'], '</p>';
                 }*/


                //print_r($_SESSION[SESSION_DOMAIN]['default_name']);die();


            }
        }
return print_r($html);
    }




    function valid_date($date) {
        return (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date));
    }

}
$reports = new AllreportsAjax();