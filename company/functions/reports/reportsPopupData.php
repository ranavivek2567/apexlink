<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
ini_set('memory_limit', -1);
ini_set('upload_max_filesize', '2000M');
ini_set('post_max_size', '2000M');
ini_set('max_execution_time', '0');
ini_set('max_file_uploads', '8');

include(ROOT_URL . "/config.php");
require_once( COMPANY_DIRECTORY_URL.'/library/dompdf/src/FontMetrics.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class reportsPopupData extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }


    public function fetchModalData(){
        try{
            $table = $_POST['table']['main'];
            if ($_POST['field_name'] == 'status'){
                // dd($_POST);
                if(!empty($_POST['default_data'])){
                    $data = '<option value="">Select</option>';
                    foreach($_POST['default_data'] as $key=>$value){
                        $selected = '';
                        if($key == '0') $selected = 'selected';
                        $data .= '<option value="'.$value["id"].'" '.$selected.'>'.$value["name"].'</option>';
                    }
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
                    'data' => $data,
                    'changed_column' => $_POST['changed_column'],
                    'field_name' => 'status'
                );
            }
//            dd($_POST['field_name']);
            if ($_POST['field_name'] == 'owner_type[]'){
                $data = '<option value="Employee">Employee</option>';
                $data .= '<option value="Tenant">Tenant</option>';
                $data .= '<option value="Company">Company</option>';
                return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
                    'data' => $data,
                    'field_name' => 'owner_type[]'
                );
            }
            $joinQuery = '';
            if(isset($_POST['table']['joins1']) && !empty($_POST['table']['joins1']) && $_POST['table']['joins1'] != null) {
                $join = $_POST['table']['joins1'];
                foreach($join as $k => $v) {
                    if(empty($v['type'])) $v['type'] = 'LEFT';
                    $joinQuery .= " ".$v['type']." JOIN ".$v['on_table']." as ".$v['as']." ON ".$v['table'].".".$v['column']."=".$v['as'].".".$v['primary'];
                }
            }
            else{
                $joinQuery="";
            }


            $where = isset($_POST['table']['where']) ? $_POST['table']['where'] : null;
            $field_name = $_POST['field_name'];

            $columns = '';
            foreach ($_POST['columns'] as $key => $val) {
                $columns .= $table.'.'.$val . ',';
            }
            $columns = trim($columns, ',');
            $whereSting = "";
            if (!empty($where)) {
                $whereSting = " WHERE (";
                foreach ($where as $key => $value) {
                    $type = (isset($value['type']) && !empty($value['type'])) ? $value['type'] : 'AND';
                    if ($key > 0) $whereSting .= ' ' . $type;
                    if ($value['condition'] == 'IS NULL') {
                        $whereSting .= " " . $value['table'] . "." . $value['column'] . " " . $value['condition'];
                    } else if ($value['condition'] == 'IS NOT NULL') {
                        $whereSting .= " " . $value['table'] . "." . $value['column'] . " " . $value['condition'];
                    } else if ($value['condition'] == 'IN') {
                        $where .= " " . $table . "." . $value['column'] . " " . $value['condition'] . " " . $value['value'] . "";
                    } else {
                        $whereSting .= " " . $value['table'] . "." . $value['column'] . " " . $value['condition'] . " '" . $value['value'] . "'";
                    }
                }
                $whereSting .= ")";
            }


            $query = "SELECT " . $columns . " FROM " . $table . " ".$joinQuery." " . $whereSting;
//dd($query);
            $data = $this->companyConnection->query($query)->fetchAll();


            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
                'data' => $data,
                'field_name' => $field_name
            );

        }  catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

    }

    public function fetchModalDataOnChange(){
        try{
            // dd($_POST);
            $sortedData = $this->matchColumns($_POST['obj']['columnsData'],$_POST['obj']['tableDetail']);
            //dd($sortedData);
            $joinData = [];
            $whereSting = "";
            $columns = "";
            $columnData = [];
            $where = "";
            if(!empty($sortedData)) {
                $countD = 0;
                $where = ' WHERE (';
                //   dd($sortedData);
                foreach ($sortedData as $key => $value) {

                    if ((!empty($value['value']) || $value['value'] == 0) && $value['value'] != '' && $value['value'] != 'All') {
                        if (isset($value['tableDetails']['onChange'])) {
                            $joinData = $value['tableDetails']['onChange'];
                        }
//                        dd($value);
                        if (!empty($value['columns']) && (!empty($value['value']) || $value['value'] == 0) && !empty($value['tableDetails']['main'])) {

                            foreach ($value['columns'] as $key2 => $value2) {
                                $as = '';
                                if ($key2 == 0) {
                                    $as = ' AS ' . $value['tableDetails']['main'] . '_' . $value2;
                                    $name = $value['tableDetails']['main'] . '_' . $value2;
                                } else {
                                    $name = $value2;
                                }
                                $columns .= $value['tableDetails']['main'] . '.' . $value2 . $as . ', ';
                                $dataColumn = ['table' => $value['tableDetails']['main'], 'name' => $name, 'column' => $value2, 'id' => $value['id']];
                                array_push($columnData, $dataColumn);
                            }
                            if($key == 0) continue;
                            if ($countD > 0) $where .= ' AND';
                            if($value['tableDetails']['main'] != 'users' || $value['id'] == 'status') {
                                $countD++;
                                if (is_array($value['value'])) {
                                    //  $where .= $this->buildArrayString($value['tableDetails']['main'],$value['columns'][0],$value['value']);
                                    $inString = implode(",", $value['value']);
                                    $where .= " " . $value['tableDetails']['main'] . "." . $value['columns'][0] . " IN ( " . $inString . ")";
                                } else {
                                    $where .= " " . $value['tableDetails']['main'] . "." . $value['columns'][0] . " = '" . $value['value'] . "'";
                                }
                            }
                        }
                    }

                    if(!empty($value['tableDetails']['whereOther'])){
                        // dd($value['tableDetails']['whereOther']);
                        foreach($value['tableDetails']['whereOther'] as $key=>$value){
                            if(!empty($value['value']) || $value['value'] == 0) {
                                $where .= ' AND';
                                if (is_array($value['value'])) {
                                    $inString = implode(",", $value['value']);
                                    $where .= " " . $value['table'] . "." . $value['column'] . " IN ( " . $inString . ")";
                                } else {
                                    if ($value['condition'] == 'IS NULL') {
                                        $where .= " " . $value['table'] . "." . $value['column'] . " " . $value['condition'];
                                    } else {
                                        $where .= " " . $value['table'] . "." . $value['column'] . " " . $value['condition'] . " '" . $value['value'] . "'";
                                    }
                                }

                            }
                        }
                        //  $where .= str_replace_first(' ', 'D', $where);
                    }

                }
                //
                //
                $where .= ' )';

            }
            // dd($where);
            $columns = str_replace_last(',', '', $columns);
            $joinQuery = '';
            if(!empty($joinData)){
                foreach ($joinData['joins'] as $key =>$value){
                    if(empty($value['type'])) $value['type'] = 'LEFT';
                    $joinQuery .= " ".$value['type']." JOIN ".$value['on_table']." as ".$value['as']." ON ".$value['table'].".".$value['column']."=".$value['as'].".".$value['primary'];
                }
            }
            $where = str_replace_last('WHERE ( )', '', $where);
            //dd($columns);
            if(!empty($columns)){

                if(!empty($where)) {
                    $query = "SELECT " . $columns . ",general_property.id as property_id, general_property.property_name FROM " . $joinData['table'] . " " . $joinQuery . " " . $where;
                } else {
                    $query = "SELECT " . $columns . ",general_property.id as property_id, general_property.property_name FROM " . $joinData['table'] . " " . $joinQuery;
                }
            } else {
                if(!empty($where)) {
                    $query = "SELECT general_property.id as property_id, general_property.property_name FROM ".$joinData['table']." ".$joinQuery." ".$where;
                } else {
                    $query = "SELECT general_property.id as property_id, general_property.property_name FROM ".$joinData['table']." ".$joinQuery;
                }
            }

//            dd($query);

            $data = $this->companyConnection->query($query)->fetchAll();

            $userData = ['table'=>'users','name'=>'users_id','column'=>'id','id'=>'tenant_id'];
            array_push($columnData,$userData);
            $userData1 = ['table'=>'users','name'=>'name','column'=>'name','id'=>'tenant_id'];
            array_push($columnData,$userData1);
            $userData2 = ['table'=>'general_property','name'=>'property_id','column'=>'id','id'=>'property_id'];
            array_push($columnData,$userData2);
            $userData3 = ['table'=>'general_property','name'=>'property_name','column'=>'property_name','id'=>'property_id'];
            array_push($columnData,$userData3);
            $userData = ['table'=>'users','name'=>'users_id','column'=>'id','id'=>'tax_payer_name_id'];
            array_push($columnData,$userData);
            $userData1 = ['table'=>'users','name'=>'tax_payer_name','column'=>'tax_payer_name','id'=>'tax_payer_name_id'];
            array_push($columnData,$userData1);
            $dataArray = [];

            if(!empty($data)){
                foreach ($data as $key=>$value){
                    foreach ($columnData as $key1 => $value1) {
                        foreach ($value as $key2=>$value2) {
                            if ($key2 == $value1['name']){
                                $columnData[$key1]['value'] = $value2;
                                break;
                            }
                        }
                    }
                    array_push($dataArray,$columnData);
                }
            }

            $newArray = [];
            $logdata = [];
            if(!empty($dataArray)){
                foreach ($dataArray as $key=>$value){
                    foreach ($value as $key1=>$value1){
                        $newArray[$key][$value1['id']][$value1['column']]=(isset($value1['value']) && !empty($value1['value']))?$value1['value']:'';
                    }
                }

                foreach ($newArray as $key=>$value){
                    foreach ($value as $key1=>$value1){
                        if(!empty($value1)) {
                            $logdata[$key1][] = $value1;
                            // array_push($logdata[$key1],$value1);
                            $logdata[$key1] = array_map("unserialize", array_unique(array_map("serialize", $logdata[$key1])));
                        }
                    }
                }
            }

            return array('code' => 200,'status' => 'success','message' => 'Record fetched successfully !!!', 'data' => $logdata);
        }  catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

    }

    public function matchColumns($columnData,$tableData){
        $sortData = [];
        $joins = null;
        if(!empty($columnData)){
            // dd($columnData);
            foreach ($columnData as $key=>$value){
                foreach ($tableData['data'] as $key2=>$value2){
                    if(isset($value2['field_name']) || (isset($value2['table']['main']) && $key == $value2['table']['main'])){
                        if(isset($value2['field_name'])) {
                            $str_pos = strpos($value2['field_name'], "[");
                            if (empty($str_pos)) {
                                $filed_name = $value2['field_name'];
                            } else {
                                $filed_name = substr($value2['field_name'], 0, strpos($value2['field_name'], "["));
                            }
                        } else {
                            $filed_name = '';
                        }
                        if($key == $filed_name || (isset($value2['table']['main']) && $key == $value2['table']['main'])) {

                            $onChange = isset($value2['onChange']) ? $value2['onChange'] : null;
                            $data = ['tableDetails' => $value2['table'], 'columns' => $value2['column'], 'value' => $value, 'onChange' => $onChange,'id' => $tableData['field'][$key2]['id']];
                            array_push($sortData, $data);
                        }
                    }
                }
            }
            if(!empty($joins)) array_push($sortData,['joins'=>$joins]);
        }
        return $sortData;
    }

    /**
     * function to build Or where query
     */
    public function buildArrayString($table,$column,$array){
        $where = '';
        foreach ($array as $key=>$value){
            $where .= " " . $table . "." . $column . " = " . $value ." OR";
        }
        $where = str_replace_last('OR', '', $where);
        return $where;
    }




    public function downloadword(){
        try{
            $html=$_POST['html'];

            //$html = file_get_contents('html path');
            $phpWord = new PhpWord();
            $section = $phpWord->addSection();
            $html = '<p><strong>You html here</strong></p>';
            Html::addHtml($section, $html);

            $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
            $cacheDir = '/temp_directory_of_your_project/';
            $objWriter->save($cacheDir. 'helloWorld.docx');
            return ['status'=>'success','code'=>200, 'data'=> $html];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function getPdfContent(){
        try{
            //  $data['record']='';
            $html=$_POST['htmls'];
            //   print_r($html);die();

            $pdf = $this->createHTMLToPDF($html);

            $data['record'] = $pdf['data'];
//print_r($data);die();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }

    }
    public function createHTMLToPDF($report){

        //$html= json_encode(array('htmls' => $_POST['htmls']));
        $dompdf = new Dompdf();
        $dompdf->loadHtml($report);
        // print_r($dompdf);die;
        //  print_r($dompdf);die();
        $dompdf->render();

        //$domain = getDomain();
        $path = "uploads/";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'/Ticket.pdf';
        $output = $dompdf->output();

        file_put_contents($fileUrl, $output);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'/Ticket.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
    }

    public function sendData(){
        try{
            $formData = $_POST;
            $time=$_POST['dateformats'];
            if(isset($_POST['htmlFilter'])) {
                $htmlFilter = $_POST['htmlFilter'];
            } else{
                $htmlFilter = '';
            }

            $and_condition = '';
            $layout = '';
            $envelope_size = '';
            $print_envelope_by = '';
            $date_condition = '';

//            dd($formData);
            $report_date_filter_table = '';
            if (isset($formData['report_date_filter_table']) && !empty($formData['report_date_filter_table'])){
                $report_date_filter_table = $formData['report_date_filter_table'];
            }
            $current_date = (isset($formData['current_date']) && !empty($formData['current_date'])) ? $formData['current_date'] : '';
            $sql_current_date = !empty($current_date) ? mySqlDateFormat($current_date,null,$this->companyConnection) : '';
            if (!empty($sql_current_date)){
                $date_condition = ' AND '.$report_date_filter_table.'.updated_at <= "' . $sql_current_date .'"';
            }

            $start_date = (isset($formData['start_date']) && !empty($formData['start_date'])) ? $formData['start_date'] : '';
            $sql_start_date = !empty($start_date) ? mySqlDateFormat($start_date,null,$this->companyConnection) : '';
            $end_date = (isset($formData['end_date']) && !empty($formData['end_date'])) ? $formData['end_date'] : '';
            $sql_end_date = !empty($end_date) ? mySqlDateFormat($end_date,null,$this->companyConnection) : '';

            if ($report_date_filter_table == 'gp'){
                $filter_col = 'update_at';
            } else {
                $filter_col = 'updated_at';
            }

            if (!empty($sql_start_date) && !empty($sql_end_date)){
                $date_condition = ' AND '.$report_date_filter_table.'.'.$filter_col.' BETWEEN "'.$sql_start_date.'" AND "'.$sql_end_date.'"';
            } else if(!empty($sql_start_date) ){
                $date_condition = ' AND '.$report_date_filter_table.'.'.$filter_col.' >= "' . $sql_start_date.'"';
            }else if(!empty($sql_end_date) ){
                $date_condition = ' AND '.$report_date_filter_table.'.'.$filter_col.' <= "' . $sql_end_date.'"';
            }
            $pagination = '';
            if($formData['report_url'] == '/Reporting/PrintEnvelopeReport'){
                $layout = 'PrintEnvelope';
                $envelope_size = ($formData['envelope_size'] == 1) ? 'PrintEnvelope' : 'PrintEnvelope2';
            }
            if(isset($formData['mailing_label_type']) && !empty($formData['mailing_label_type'])){
                $layout = 'MailingLabel';
                $envelope_size = ($formData['mailing_label_type'] == 1) ? 'MailingLabel' : 'MailingLabel2';

            }
//            dd($layout);
            if (!isset($formData['tenant']) && !isset($formData['owner'])) {
                if (isset($formData['move_out'])){
                    $and_condition = '';
                }  else{
                    // $and_condition = ' WHERE tld.record_status=1';
                }
            }else {
                if ($layout == 'PrintEnvelope') {
                    $and_condition = '';
                } else If ($layout == 'MailingLabel') {
                    $and_condition = '';
                } else {
                    if (isset($formData['tenant'])) {
                        if (is_array($formData['tenant'])) {
                            $in_string = implode(",", $formData['tenant']);
                            $and_condition .= ' AND u.id IN (' . $in_string . ')';
                        } else {
                            $and_condition .= ' AND u.id = ' . $formData['tenant'];
                        }
                    }

                    if (isset($formData['owner'])) {
                        if (is_array($formData['owner'])) {
                            $in_string = implode(",", $formData['owner']);
                            $and_condition .= ' AND u.id IN (' . $in_string . ')';
                        } else {
                            $and_condition .= ' AND u.id = ' . $formData['owner'];
                        }
                    }
                }
            }



            if(isset($formData['vendor']) && $layout == '') {
                if (isset($formData['vendor']) && empty($formData['vendor'])){
                    $filter_data = 'all';
                } else {
                    $filter_data = 'DirectRedirection';
                    if (is_array($formData['vendor'])) {
                        $in_string = implode(",", $formData['vendor']);
                        $and_condition .= ' WHERE u.id IN  (' . $in_string . ')';
                    } else {
                        $and_condition .= ' WHERE u.id = ' . $formData['vendor'];
                    }
                }
                $envelope_size = 'DirectRedirection';
                $formData['property'] = '';
                $formData['portfolio'] = '';
                $pagination = '20';
                /*} else if (isset($formData['portfolio']) && $layout == '') {
                    $filter_data = $formData['portfolio'];
                    $pagination = 'portfolio';*/
            } else if(isset($formData['property']) && $layout == '') {
                if ($formData['report_url'] == '/Reporting/Portfolio') {
                    $filter_data = $formData['portfolio'];
                    $pagination = 'property';

                    if (isset($formData['property'])) {
                        if (is_array($formData['property'])) {
                            $in_string = implode(",", $formData['property']);
                            $and_condition .= ' Where gp.id IN (' . $in_string . ')';
                        } else {
                            $and_condition .= ' Where gp.id = ' . $formData['property'];
                        }
                    }

                } else {
                    $filter_data = $formData['property'];
                    $pagination = 'property';
                }
            } else if(isset($formData['owner']) && $layout == '') {
                $filter_data = '1';
                $pagination = 'property';
                $envelope_size = 'DirectRedirection';
            } else if($layout == 'MailingLabel'){
                $filter_data ='1';
                if (isset($formData['property'])) {
                    if (is_array($formData['property'])) {
                        $in_string = implode(",", $formData['property']);
                        $and_condition .= ' WHERE gpm.id IN  (' . $in_string . ')';
                    } else {
                        $and_condition .= ' WHERE gpm.id = ' . $formData['property'];
                    }
                } else {
                    $and_condition = '';
                }

                $pagination = '';
            }else if($layout == 'PrintEnvelope'){
                if (isset($formData['tenant'])){
                    $filter_data_by = $formData['tenant'];
                } else if (isset($formData['owner'])){
                    $filter_data_by = $formData['owner'];
                } else if(isset($formData['vendor'])){
                    $filter_data_by = $formData['vendor'];
                }else if(isset($formData['property'])){
                    if (isset($formData['unit'])){
                        $print_envelope_by = 'ud';
                        $filter_data_by = $formData['unit'];
                    } else if (isset($formData['building'])){
                        $print_envelope_by = 'bd';
                        $filter_data_by = $formData['building'];
                    } else {
                        $print_envelope_by= 'gp';
                        $filter_data_by = $formData['property'];
                    }

                }else {
                    $filter_data_by = $formData['contact'];
                }
                $filter_data = $filter_data_by;
                $pagination = '';

            } else{
                $filter_data = 10;
                $pagination = '';
            }

//            dd($formData);
//            if ($formData['report_url'] == '/Reporting/Owner_WithHoldingReport'){
//                $filter_data = $formData['taxpayer_name'];
//            }

//            dd($filter_data);



            if ($formData['report_url'] == '/Reporting/TenantListReport' || $formData['report_url'] == '/Reporting/TenantListConsolidatedReport'
                || $formData['report_url'] == '/Reporting/RentRollConsolidatedReport' || $formData['report_url'] == '/Reporting/Tenant_InsuranceReport'
                ||$formData['report_url'] == '/Reporting/InventoryReport' || $formData['report_url'] == '/Reporting/PropertyListingReport'
                || $formData['report_url'] == '/Reporting/TenantPetDetailReport'|| $formData['report_url'] == '/Reporting/UnitListReport'
                || $formData['report_url'] == '/Reporting/UnitFeatureReport'|| $formData['report_url'] == '/Reporting/Owner_ListingReport'
                || $formData['report_url'] == '/Reporting/LostItemsReport'|| $formData['report_url'] == '/Reporting/FoundItemsReport'
                || $formData['report_url'] == '/Reporting/MatchedItemsReport' || $formData['report_url'] == '/Reporting/LostItemsReport'
                || $formData['report_url'] == '/Reporting/VendorList'|| $formData['report_url'] == '/Reporting/EmployeeListReport'
                || $formData['report_url'] == '/Reporting/EmployeeEEOReport' || $formData['report_url'] == '/Reporting/PropertyGroup'
                || $formData['report_url'] == '/Reporting/Portfolio' || $formData['report_url'] == '/Reporting/InventoryDetails'
                || $formData['report_url'] == '/Reporting/Owner_WithHoldingReport'|| $formData['report_url'] == '/Reporting/InventorySummary'
                || $formData['report_url'] == '/Reporting/InventoryReordering') {
                $filter_data = 'DirectRedirection';
                $envelope_size = 'DirectRedirection';
                $pagination = 'property';
            }

            $groupBy_condition = '';
            if(isset($formData['db_query_after_where']) && $formData['db_query_after_where']!='undefined') {
                $groupBy_condition = $formData['db_query_after_where'];
            } elseif ($formData['db_query_after_where']=='undefined'){
                $groupBy_condition = '';
            }

            $report_columns = '';
            if(isset($formData['alternate_report_columns']) && $formData['alternate_report_columns']!='undefined') {
                if (isset($formData['pet_detail']) && $formData['pet_detail'] == '1'){
                    $report_columns = $formData['alternate_report_columns'];
                } else {
                    $report_columns = $formData['report_columns'];
                }
            } else {
                $report_columns = $formData['report_columns'] ;
            }


//            dd($formData);
            $report_url = $formData['report_url'];
//            dd($formData['status']);
            $status = $formData['status'];
            switch ($report_url) {
                //Tenant Reports starts
                case '/Reporting/RentRollConsolidatedReport':
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/TenantListReport':
                    $and_condition = $this->propertyFilter($formData);
                    if ($status == '1') {
                        $and_condition .=  " AND td.status IN ('1','2','3','4')";
                    }
                    if ($status == '0') {
                        $and_condition .=    " AND td.status IN ('0','5','6')";
                    }
                    $and_condition .= (isset($formData['move_out']) && $formData['move_out'] == 'on') ?  ' AND u.record_status = 0' : '';
                    break;
                case '/Reporting/TenantListConsolidatedReport':
                    $and_condition = $this->propertyFilter($formData);
                    if ($status == '1') {
                        $and_condition .=  " AND td.status IN ('1','2','3','4')";
                    }
                    if ($status == '0') {
                        $and_condition .=    " AND td.status IN ('0','5','6')";
                    }
                    $and_condition .= (isset($formData['move_out']) && $formData['move_out'] == 'on') ?  ' AND u.record_status = 0' : '';
                    break;
                case '/Reporting/Tenant_InsuranceReport':
                    if (isset($formData['tenant_insurance']) && $formData['tenant_insurance'] == 'yes') {
                        $formData['report_db_query'] = "SELECT REPLACE(u.name,' ','--') AS name, REPLACE(bd.building_name,' ','--') AS build_name,concat(ud.unit_prefix,'-',ud.unit_no),DATE_FORMAT(u.created_at,'%Y-%m-%d') as available_date,REPLACE(tri.policy_no,' ', '--'), 'Yes' AS has_insurance FROM users u RIGHT JOIN tenant_renter_insurance tri ON u.id=tri.user_id JOIN tenant_property tp ON u.id=tp.user_id JOIN unit_details ud ON tp.unit_id=ud.id JOIN building_detail bd ON tp.building_id=bd.id LEFT JOIN general_property gp ON gp.id=bd.property_id WHERE u.user_type = '2'";
                    } else  if (isset($formData['tenant_insurance']) && $formData['tenant_insurance'] == 'no') {
                        $formData['report_db_query'] = "SELECT REPLACE(u.name,' ','--') AS name, REPLACE(bd.building_name,' ','--') AS build_name,concat(ud.unit_prefix,'-',ud.unit_no),DATE_FORMAT(u.created_at,'%Y-%m-%d') as available_date,REPLACE(tri.policy_no,' ', '--'), 'No' AS has_insurance FROM users u LEFT JOIN tenant_renter_insurance tri ON u.id=tri.user_id JOIN tenant_property tp ON u.id=tp.user_id JOIN unit_details ud ON tp.unit_id=ud.id JOIN building_detail bd ON tp.building_id=bd.id LEFT JOIN general_property gp ON gp.id=bd.property_id WHERE u.user_type = '2' AND tri.created_at IS NULL";
                    } else {
                        $formData['report_db_query'] = "SELECT REPLACE(u.name,' ','--') AS name, REPLACE(bd.building_name,' ','--') AS build_name,concat(ud.unit_prefix,'-',ud.unit_no),DATE_FORMAT(u.created_at,'%Y-%m-%d') as available_date,REPLACE(tri.policy_no,' ', '--'),if(tri.created_at IS NULL, 'No','Yes') as has_insurance FROM users u LEFT JOIN tenant_renter_insurance tri ON u.id=tri.user_id JOIN tenant_property tp ON u.id=tp.user_id JOIN unit_details ud ON tp.unit_id=ud.id JOIN building_detail bd ON tp.building_id=bd.id LEFT JOIN general_property gp ON gp.id=bd.property_id WHERE u.user_type = '2'";
                    }
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/TenantPetDetailReport':
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/TenantMailingLabelReport':
                    if ($status == '1') {
                        $and_condition .=  " AND td.status IN ('1','2','3','4')";
                    }
                    if ($status == '0') {
                        $and_condition .=    " AND td.status IN ('0','5','6')";
                    }
                    break;
                //Tenant Reports Ends

                //Maintenance Reports Starts
                case '/Reporting/PurchaseOrderReport':
                    if ($formData['status'] == '1') {
                        $and_condition .=  " AND po.status ='1'";
                    }
                    if ($formData['status'] == '0') {
                        $and_condition .=    " AND po.status ='0'";
                    }
                    break;
                //Maintenance Reports Starts


                //Property Reports Starts
                case '/Reporting/UnitListReport':
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/UnitFeatureReport':
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/PropertyGroup':
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/Portfolio':
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/LostItemsReport':
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/FoundItemsReport':
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/MatchedItemsReport':
                    $and_condition = $this->propertyFilter($formData);
                    break;
                case '/Reporting/VendorList':
                    if ($status == '1') {
                        $and_condition .=  ' AND u.status ="1"';
                    }
                    if ($status == '0') {
                        $and_condition .=   ' AND u.status = "0"';
                    }
                    break;
                case '/Reporting/VendorInsurance':
                    if ($formData['vendor'] !=0) {
                        $and_condition .= ' AND u.id = ' . $formData['vendor'];
                    }
                    break;
                case '/Reporting/VendorMailingLabelReport':
                    if ($formData['status'] == '1') {
                        $and_condition .=  " AND u.status ='1'";
                    }
                    if ($formData['status'] == '0') {
                        $and_condition .=    " AND u.status ='0'";
                    }
                    break;
                case '/Reporting/Owner_WithHoldingReport':
                    if (is_array($formData['taxpayer_name'])) {
                        $in_string = implode(",", $formData['taxpayer_name']);
                        $and_condition = ' AND u.id IN  (' . $in_string . ')';
                    } else {
                        $and_condition = ' AND u.id = ' . $formData['taxpayer_name'];
                    }
                    if ($formData['status'] == '1') {
                        $and_condition .=  " AND u.status ='1'";
                    }
                    if ($formData['status'] == '0') {
                        $and_condition .=    " AND u.status ='0'";
                    }
                    break;
                //Property Reports Ends

                //Inventory Reports Starts
                case '/Reporting/InventoryDetails':
                    $and_condition = $this->propertyFilter($formData );
                    break;
                case '/Reporting/InventorySummary':
                    if ($formData['status'] == '1') {
                        $and_condition .=  " AND mi.deleted_at IS NULL";
                    }
                    if ($formData['status'] == '0') {
                        $and_condition .=    " AND mi.deleted_at IS NOT NULL";
                    }
                    break;
                case '/Reporting/InventoryReordering':
                    if ($formData['status'] == '1') {
                        $and_condition .=  "  AND mi.deleted_at IS NULL";
                    }
                    if ($formData['status'] == '0') {
                        $and_condition .=    " AND mi.deleted_at IS NOT NULL";
                    }
                    break;
                //Inventory Reports Ends
                case '':

                    break;

                default:
            }

//            dd($and_condition);
            $reqQuery = $formData['report_db_query'].$and_condition.' '.$date_condition.' '.$groupBy_condition;
//            dd($reqQuery);
            $_SESSION[SESSION_DOMAIN]['report_columns'] = $report_columns ;
            $_SESSION[SESSION_DOMAIN]['report_db_query'] = $reqQuery;
            $_SESSION[SESSION_DOMAIN]['report_title'] = $_POST['title'];
            $_SESSION[SESSION_DOMAIN]['filter_data'] = $filter_data;
            $_SESSION[SESSION_DOMAIN]['pagination'] = $pagination;
            $_SESSION[SESSION_DOMAIN]['modal_filter_data'] = $formData['modal_filter_data'];
            $_SESSION[SESSION_DOMAIN]['htmlFilter'] = $htmlFilter;
            $_SESSION[SESSION_DOMAIN]['layout'] = $envelope_size;
            $_SESSION[SESSION_DOMAIN]['print_envelope_by'] = $print_envelope_by;
            $_SESSION[SESSION_DOMAIN]['current_date'] = $current_date;
            $_SESSION[SESSION_DOMAIN]['start_date'] = $start_date;
            $_SESSION[SESSION_DOMAIN]['time_reports'] = $time;
            $_SESSION[SESSION_DOMAIN]['end_date'] = $end_date;

            $data = $formData['report_url'];
            return array('code' => 200, 'status' => 'success', 'url' => $data, 'message' => 'Record fetched successfully!');
        }   catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    function propertyFilter($propertyData){
        $returnPropertyData = '';
        if (isset($propertyData['property']) && !empty($propertyData['property'])) {
            if (is_array($propertyData['property'])) {
                $in_string = implode(",", $propertyData['property']);
                $returnPropertyData = ' AND gp.id IN  (' . $in_string . ')';
            } else {
                $returnPropertyData = ' AND gp.id = ' . $propertyData['property'];
            }
        }
        return $returnPropertyData;
    }

    public function sendDirectData(){
        try{
            $and_condition = '';
            $pagination = '';
            $time=$_POST['dateformats'];

            if ($_POST['title'] == 'Property Listing Report'){
                $db_query_after_where =' GROUP BY gp.property_name';
                $report_url = '/Reporting/PropertyListingReport';
                $report_columns = "Property Name,Property Address,Property Owner Name";
                $formData['report_db_query'] ="SELECT REPLACE(gp.property_name,' ','--') AS prop_name, CONCAT(REPLACE(gp.address1,' ','--'), '--', REPLACE(gp.address2,' ','--'),'--',REPLACE(gp.address3,' ','--'), '--', REPLACE(gp.address4,' ','--')) AS address , GROUP_CONCAT(REPLACE(u.name, ' ', '--'), '--','(',opo.property_percent_owned,'.00%)') AS name FROM general_property gp LEFT JOIN owner_property_owned opo ON opo.property_id = gp.id LEFT JOIN users u ON opo.user_id = u.id ";
            } else if ($_POST['title'] == 'Vendor List Consolidated'){
                $db_query_after_where ='';
                $report_url = '/Reporting/VendorListConsolidate';
                $report_columns = "Vendor Name,Phone Number,Email,Address,Term";
                $formData['report_db_query'] ="SELECT REPLACE(name,' ','--') AS name,u.phone_number,u.email,concat(REPLACE(u.address1,' ','--'),'--',REPLACE(u.address2,' ','--'),'--',REPLACE(u.address3,' ','--'),'--',REPLACE(u.address4,' ','--')) AS address,REPLACE(cvt.vendor_type,' ','--') as term FROM users u JOIN vendor_additional_detail vad on u.id=vad.vendor_id JOIN company_vendor_type cvt ON cvt.id=vad.vendor_type_id";
            } else {
                $db_query_after_where =' GROUP BY gp.property_name';
                $report_url = '/Reporting/PropertyListingReport';
                $report_columns = array('Property Name','Property Address','Property Owner Name');
                $formData['report_db_query'] ="SELECT REPLACE(gp.property_name,' ','--') AS prop_name, CONCAT(REPLACE(gp.address1,' ','--'), '--', REPLACE(gp.address2,' ','--'),'--',REPLACE(gp.address3,' ','--'), '--', REPLACE(gp.address4,' ','--')) AS address , GROUP_CONCAT(REPLACE(u.name, ' ', '--'), '--','(',opo.property_percent_owned,'.00%)') AS name FROM general_property gp LEFT JOIN owner_property_owned opo ON opo.property_id = gp.id LEFT JOIN users u ON opo.user_id = u.id ";
            }


            $groupBy_condition = '';
            if(isset($db_query_after_where) && $db_query_after_where!='undefined') {
                $groupBy_condition = $db_query_after_where;
            } elseif ($db_query_after_where=='undefined'){
                $groupBy_condition = '';
            }

            $filter_data = 'DirectRedirection';
            $envelope_size = 'DirectRedirection';
            $pagination = 'property';

            $reqQuery = $formData['report_db_query'].$and_condition.' '.$groupBy_condition;
            $_SESSION[SESSION_DOMAIN]['report_columns'] = $report_columns;
            $_SESSION[SESSION_DOMAIN]['report_db_query'] = $reqQuery;
            $_SESSION[SESSION_DOMAIN]['report_title'] = $_POST['title'];
            $_SESSION[SESSION_DOMAIN]['filter_data'] = 'DirectRedirection';
            $_SESSION[SESSION_DOMAIN]['pagination'] = $pagination;
            $_SESSION[SESSION_DOMAIN]['modal_filter_data'] = '';
            $_SESSION[SESSION_DOMAIN]['htmlFilter'] = '';
            $_SESSION[SESSION_DOMAIN]['time_reports'] = $time;
            $_SESSION[SESSION_DOMAIN]['layout'] = 'DirectRedirection';
            $data = $report_url;
            return array('code' => 200, 'status' => 'success', 'url' => $data, 'message' => 'Record fetched successfully!');
        }   catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }




    public function searchEmailForReport()
    {
        try{
            //$keyValueData = $_POST['keyValue'];
            // print_r($keyValueData);die;

            /*$query = $this->companyConnection->query("SELECT id, CONCAT( first_name,' ', last_name ) AS username,email FROM users  WHERE first_name like '%{$keyValueData}%'");*/
            $query = $this->companyConnection->query("SELECT id, CONCAT( first_name,' ', last_name ) AS username,email FROM users");
            $settings = $query->fetchAll();


            //print_r($settings);die;
            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function sendMail()
    {
        try{
            $hello = [];
            $hello = $_POST['UserTo'];
            $to = (isset($hello) && !empty($hello)) ? $hello : '';
            $data = ['reportTo' => $to];
            $required_array = ['reportTo'];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
//            dd($err_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $server_name = 'https://' . $_SERVER['HTTP_HOST'];
                $domain = explode('.', $_SERVER['HTTP_HOST']);


                $toUsers = $_POST['UserTo'];
                $ccUsers = $_POST['UserCc'];
                $bccUsers = $_POST['UserBcc'];
                $reportName = $_POST['reportName'];
                if (!empty($toUsers) && empty($ccUsers) && empty($bccUsers)) {
                    $allUsers = $toUsers;
                } else if (empty($toUsers) && !empty($ccUsers) && empty($bccUsers)) {
                    $allUsers = $ccUsers;
                } else if (empty($toUsers) && empty($ccUsers) && !empty($bccUsers)) {
                    $allUsers = $ccUsers;
                } else if (!empty($toUsers) && !empty($ccUsers) && empty($bccUsers)) {
                    $allUsers = array_merge($toUsers, $ccUsers);
                } else if (empty($toUsers) && !empty($ccUsers) && !empty($bccUsers)) {
                    $allUsers = array_merge($ccUsers, $bccUsers);
                } else if (!empty($toUsers) && empty($ccUsers) && !empty($bccUsers)) {
                    $allUsers = array_merge($toUsers, $bccUsers);
                } else if (!empty($toUsers) && !empty($ccUsers) && !empty($bccUsers)) {
                    $allUsers = array_merge($toUsers, $ccUsers, $bccUsers);
                } else {
                    echo "no data";
                }

                $username = $_SESSION[SESSION_DOMAIN]['name'];
                $compName = $_SESSION[SESSION_DOMAIN]['company_name'];
                $current_date = dateFormatUser(date('Y-m-d'), null, $this->companyConnection);


                $body = getEmailTemplateData($this->companyConnection, 'Reporting_Key');
                $body = str_replace("{UserName}", $username, $body);
                $body = str_replace("{CompanyName}", $compName, $body);
                $body = str_replace("{ReportName}", $reportName, $body);
                $body = str_replace("{Date}", $current_date, $body);
                // $subdomain = array_shift($domain);
                // $user_detail = $SESSION['SESSION'.$subdomain];

                $request['action'] = 'SendMailPhp';
                $request['to'] = $allUsers;
                $request['subject'] = $reportName;
                $request['message'] = $body;
                /*echo '<pre>';print_r($request['message']);die;*/
                $request['portal'] = '1';
                curlRequest($request);
                return ['status' => 'success', 'code' => 200, 'data' => $request, 'message' => 'The Email was send successfully'];
            }
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
}
$reportsPopupData = new reportsPopupData();
