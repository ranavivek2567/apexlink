<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
ini_set('memory_limit', -1);
ini_set('upload_max_filesize', '2000M');
ini_set('post_max_size', '2000M');
ini_set('max_execution_time', '0');
ini_set('max_file_uploads', '8');

include(ROOT_URL . "/config.php");
require_once( COMPANY_DIRECTORY_URL.'/library/dompdf/src/FontMetrics.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class balanceSheetReport extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }


    public function fetchModalData(){
        try{
            
            $query = "SELECT * FROM company_property_portfolio WHERE deleted_at IS NULL AND status = '1'";
            $portfolio_data = $this->companyConnection->query($query)->fetchAll();
            
            $portfolio_ddl = '<option value="">Select</option>';
            foreach($portfolio_data as $key => $val){
                $portfolio_ddl .= '<option value="'.$val["id"].'">'.$val["portfolio_name"].'</option>';
            }
            
            $queryProperty = "SELECT * FROM general_property WHERE deleted_at IS NULL AND status = '1'";
            $property_data = $this->companyConnection->query($queryProperty)->fetchAll();
            
            $property_ddl = '<option value="">Select</option>';
            foreach($property_data as $key => $val){
                $property_ddl .= '<option value="'.$val["id"].'">'.$val["property_name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
                'portfolio_ddl' => $portfolio_ddl,
                'property_ddl' => $property_ddl,
            );

        }  catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

    }

    public function fetchModalDataOnChange(){

        try{
            $id =$_POST["id"];
            $id=(implode("','",$id));
            $id="'".$id."'";
            // return array( "id"=>$id);
            $queryProperty = "SELECT id, property_name FROM general_property WHERE  deleted_at IS NULL AND status = '1' AND portfolio_id IN(".$id.")";
            $property_data = $this->companyConnection->query($queryProperty)->fetchAll();
            //dd($property_data);
           
            $property_ddl = '<option value="">Select</option>';
            foreach($property_data as $key => $val){
                $property_ddl .= '<option value="'.$val["id"].'">'.$val["property_name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
            'property_ddl' => $property_ddl,
        );
           
        } 
         catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }


    public function balanceSheetViewData(){
        try{
            $portfolio = $_POST["portfolio"];
            $property = $_POST["property"];
            $_SESSION[SESSION_DOMAIN]['property_id'] = $property;
            $_SESSION[SESSION_DOMAIN]["portfolio_id"] = $portfolio;
            // print_r($_SESSION[SESSION_DOMAIN]["portfolio_id"]);
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully New!!!',
            );
        }
        catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
}
$balanceSheetReport = new balanceSheetReport();
