<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
ini_set('memory_limit', -1);
ini_set('upload_max_filesize', '2000M');
ini_set('post_max_size', '2000M');
ini_set('max_execution_time', '0');
ini_set('max_file_uploads', '8');

include(ROOT_URL . "/config.php");
require_once( COMPANY_DIRECTORY_URL.'/library/dompdf/src/FontMetrics.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class commonReportsFunction extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }


    public function fetchModalData(){
        try{

//            dd($_POST);
            $id = isset($_POST['id']) && !empty($_POST['id']);
            $query = "SELECT * FROM company_property_portfolio WHERE deleted_at IS NULL AND status = '1'";
            $portfolio_data = $this->companyConnection->query($query)->fetchAll();
            
            $portfolio_ddl = '';
            foreach($portfolio_data as $key => $val){
                $portfolio_ddl .= '<option value="'.$val["id"].'">'.$val["portfolio_name"].'</option>';
            }
            $queryProperty = "SELECT * FROM general_property WHERE deleted_at IS NULL AND status = '1'";
            $property_data = $this->companyConnection->query($queryProperty)->fetchAll();
            
            $property_ddl = '';
            foreach($property_data as $key => $val){
                $property_ddl .= '<option value="'.$val["id"].'">'.$val["property_name"].'</option>';
            }

            $queryVendor = "SELECT u.id, u.name FROM users AS u WHERE user_type = '3' AND deleted_at IS NULL AND status = '1'";
            $vendor_data = $this->companyConnection->query($queryVendor)->fetchAll();
            $vendor_ddl = '';
            foreach($vendor_data as $key => $val){
                $vendor_ddl .= '<option value="'.$val["id"].'">'.$val["name"].'</option>';
            }

            $queryTenant = "SELECT users.id, users.name FROM users LEFT JOIN tenant_property ON users.id=tenant_property.user_id LEFT JOIN tenant_details ON users.id=tenant_details.user_id LEFT JOIN tenant_lease_details ON users.id=tenant_lease_details.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '1')";
            $tenant_data = $this->companyConnection->query($queryTenant)->fetchAll();
            $tenant_ddl = '';
            foreach($tenant_data as $key => $val){
                $tenant_ddl .= '<option value="'.$val["id"].'">'.$val["name"].'</option>';
            }

            $queryOwner = "SELECT users.id, users.name, owner_details.status FROM users LEFT JOIN owner_details ON users.id=owner_details.user_id WHERE ( users.deleted_at IS NULL AND users.status = '1' AND users.user_type = '4')";
            $owner_data = $this->companyConnection->query($queryOwner)->fetchAll();
            $owner_ddl = '';
            foreach($owner_data as $key => $val){
                if ($id && $id == $val["id"]){
                    $owner_ddl .= '<option selected value="'.$val["id"].'">'.$val["name"].'</option>';
                } else {
                    $owner_ddl .= '<option value="'.$val["id"].'">'.$val["name"].'</option>';
                }

            }


            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
                'portfolio_ddl' => $portfolio_ddl,
                'property_ddl' => $property_ddl,
                'vendor_ddl' => $vendor_ddl,
                'tenant_ddl' => $tenant_ddl,
                'owner_ddl' => $owner_ddl
            );
            
        

        }  catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

    }

     public function fetchModalBalanceDetailOnchange(){

        try{
            $id =$_POST["id"];
            $id=(implode("','",$id));
            $id="'".$id."'";
            // return array( "id"=>$id);
            $queryProperty = "SELECT id, property_name FROM general_property WHERE  deleted_at IS NULL AND status = '1' AND portfolio_id IN(".$id.")";
            $property_data = $this->companyConnection->query($queryProperty)->fetchAll();
            //dd($property_data);
           
            $property_ddl = '<option value="">Select</option>';
            foreach($property_data as $key => $val){
                $property_ddl .= '<option value="'.$val["id"].'">'.$val["property_name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
            'property_ddl' => $property_ddl,
        );
           
        } 
         catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }



    public function onchangePropertyTenant(){
        try{
            $id = $_POST["id"];
            $id = implode("','",$id);
            $id = "'.$id.'";
            $queryTenant = "SELECT users.id, users.name,general_property.id FROM users LEFT JOIN tenant_property ON users.id=tenant_property.user_id LEFT JOIN tenant_details ON users.id=tenant_details.user_id LEFT JOIN tenant_lease_details ON users.id=tenant_lease_details.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '1') AND general_property.id IN(".$id.")";
            $tenant_data = $this->companyConnection->query($queryTenant)->fetchAll();
            //dd($property_data);
            $tenant_ddl = '<option value="">Select</option>';
            foreach($tenant_data as $key => $val){
                $tenant_ddl .= '<option value="'.$val["id"].'">'.$val["name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
            'tenant_ddl' => $tenant_ddl,
        );

    }
    catch (Exception $exception) {
        return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        printErrorLog($exception->getMessage());
    }

 }


///On Property Change Show Vendors
     public function onchangePropertyVendor(){
        try{
            $id =$_POST["id"];
            $id=(implode("','",$id));
            $id="'".$id."'";
            $queryVendor = "SELECT u.name,u.id,u.status FROM `owner_blacklist_vendors` as obv JOIN users as u ON u.id = obv.vendor_id WHERE obv.property_id IN (".$id.") and u.status ='1'";
            $vendor_data = $this->companyConnection->query($queryVendor)->fetchAll();
            //dd($property_data);

            $vendor_ddl = '<option value="">Select</option>';
            foreach($vendor_data as $key => $val){
                $vendor_ddl .= '<option value="'.$val["id"].'">'.$val["name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
            'vendor_ddl' => $vendor_ddl,
        );
        }
        catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

     }

     public function onchangeStatusVendor(){
        try{
            $id =$_POST["id"];
            if($id=='All'){
            $statusVendor = "SELECT vendor_additional_detail.vendor_id,  users.name FROM vendor_additional_detail LEFT JOIN users ON vendor_additional_detail.vendor_id=users.id LEFT JOIN company_vendor_type ON vendor_additional_detail.vendor_type_id=company_vendor_type.id WHERE ( vendor_additional_detail.deleted_at IS NULL AND users.user_type = '3' AND users.deleted_at IS  NULL)";

        }else{
            $statusVendor = "SELECT vendor_additional_detail.vendor_id,  users.name FROM vendor_additional_detail LEFT JOIN users ON vendor_additional_detail.vendor_id=users.id LEFT JOIN company_vendor_type ON vendor_additional_detail.vendor_type_id=company_vendor_type.id WHERE ( vendor_additional_detail.deleted_at IS NULL AND users.user_type = '3' AND users.deleted_at IS  NULL) AND users.status= '$id'";
        }
            $status_data = $this->companyConnection->query($statusVendor)->fetchAll();
            $status_ddl = '<option value="">Select</option>';
            foreach($status_data as $key => $val){
                $status_ddl .= '<option value="'.$val["vendor_id"].'">'.$val["name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!','status_ddl' => $status_ddl);
        }
        catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

     }

     // onchange Property Status Starts
      public function onchangeStatusProperty(){
        try{
            $id =$_POST["id"];
            $statusProperty = "SELECT general_property.id, general_property.property_name, general_property.status FROM general_property LEFT JOIN company_property_type ON general_property.property_type=company_property_type.id WHERE ( general_property.deleted_at IS NULL) AND general_property.status= '$id'";
            $propstatus_data = $this->companyConnection->query($statusProperty)->fetchAll();
            $propstatus_ddl = '<option value="">Select</option>';
            foreach($propstatus_data as $key => $val){
                $propstatus_ddl .= '<option value="'.$val["id"].'">'.$val["property_name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!','propstatus_ddl' => $propstatus_ddl);
        }
        catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

     }
     // onchange Property Status Ends

     ///On Get All Banks Name Data
     public function fetchBankName(){
        try{

            $queryBank = "SELECT bank.id, bank.bank_name FROM `company_accounting_bank_account` as bank WHERE `status` = '1' AND deleted_at IS NULL";
            $bank_data = $this->companyConnection->query($queryBank)->fetchAll();
            $bank_ddl = '<option value="">Select</option>';
            foreach($bank_data as $key => $val){
                $bank_ddl .= '<option value="'.$val["id"].'">'.$val["bank_name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
            'bank_ddl' => $bank_ddl,
        );

        }
        catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function applicantName(){
        try{

             $applicant_name = "SELECT users.id, users.name,company_rental_applications.status FROM users LEFT JOIN company_rental_applications ON users.id=company_rental_applications.user_id LEFT JOIN general_property ON company_rental_applications.prop_id=general_property.id LEFT JOIN unit_details ON company_rental_applications.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '10' AND users.record_status = '1')";
            $applicant_data = $this->companyConnection->query($applicant_name)->fetchAll();
           $applicant_ddl = '';
            foreach($applicant_data as $key => $val){
                $applicant_ddl .= '<option value="'.$val["id"].'">'.$val["name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!',
            'applicant_ddl' => $applicant_ddl,
        );

        }
        catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    
    public function balanceDetailData(){
        try{

            $portfolio_data = $_POST["portfolio"];
            $property_data = $_POST["property"];
            $_SESSION[SESSION_DOMAIN]['property_id'] = $portfolio_data;
            $_SESSION[SESSION_DOMAIN]["portfolio_id"] = $property_data;
           
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully New!!!',
            );
        }
        catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    
//OnChange of Status getting a Tenant
    public function onChangeStatus(){
        try{
            $id =$_POST["id"];
            //$queryStatus = "SELECT general_property.id, general_property.property_name, general_property.status FROM general_property LEFT JOIN company_property_type ON general_property.property_type=company_property_type.id WHERE ( general_property.deleted_at IS NULL) AND general_property.status= '$id'";
            $queryStatus = "SELECT users.id, users.name, tenant_details.status FROM users LEFT JOIN tenant_property ON users.id=tenant_property.user_id LEFT JOIN tenant_details ON users.id=tenant_details.user_id LEFT JOIN tenant_lease_details ON users.id=tenant_lease_details.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id WHERE ( users.deleted_at IS NULL AND users.user_type = '2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '$id')";
            $tenant_data = $this->companyConnection->query($queryStatus)->fetchAll();
            $tenant_ddl = '<option value="">Select</option>';
            foreach($tenant_data as $key => $val){
                $tenant_ddl .= '<option value="'.$val["id"].'">'.$val["name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!','tenant_ddl' => $tenant_ddl);
        }
        catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }   
    }

    public function onChangePropertyOwner(){
        try{
            // dd($_POST);
            // $id = $_POST["id"];
            if(isset($_POST["id"]) && !empty($_POST["id"])){
                $ids_array=(implode("','",$_POST["id"]));
                $ids_array="'".$ids_array."'";
                $queryOwner = "SELECT u.id, u.name FROM users AS u LEFT JOIN owner_property_owned as opo ON u.id=opo.user_id LEFT JOIN general_property AS gp ON opo.property_id=gp.id  WHERE u.deleted_at IS NULL AND gp.id IN ($ids_array)";
                $owner_data = $this->companyConnection->query($queryOwner)->fetchAll();
                $owner_ddl = '<option value="">Select</option>';
                foreach($owner_data as $key => $val){
                    $owner_ddl .= '<option value="'.$val["id"].'">'.$val["name"].'</option>';
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!','owner_ddl' => $owner_ddl);
            } else {
                $owner_ddl = '<option value="">Select</option>';
            }
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }   
    }
    
    //On Change Satus of Owner get Owner Name
    public function onChangeStatusOwner(){
        try{
            $id =$_POST["id"];
            $queryStatus = "SELECT users.id, users.name, owner_details.status FROM users LEFT JOIN owner_details ON users.id=owner_details.user_id WHERE ( users.deleted_at IS NULL AND users.status = '$id' AND users.user_type = '4')";
            $owner_data = $this->companyConnection->query($queryStatus)->fetchAll();
    
            //dd($queryStatus);
            $owner_ddl = '<option value="">Select</option>';
            foreach($owner_data as $key => $val){
                $owner_ddl .= '<option value="'.$val["id"].'">'.$val["name"].'</option>';
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record fetched successfully !!!','owner_ddl' => $owner_ddl);
        }
        catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }   
    }
}



$commonReportsFunction = new commonReportsFunction();
