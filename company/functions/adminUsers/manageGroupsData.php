<?php


include(ROOT_URL."/config.php");
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");
class manageGroups extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    public function usersData(){
        try {

            $query = $this->companyConnection->query("SELECT name,id,email,phone_number from users where user_type='1' AND (role='1' OR role='2')");
            $data = $query->fetchAll();
            $html ="";
            foreach ($data as $key=>$value){
                $html .="<option value='".$value['id']."'>".$value['name']."</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html'=>$html,'data'=>$data);
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function update_permissions(){
        try {
            $mem=$_POST['checked_val'];
            $members=implode(',',$mem);
            $id=$_POST['id'];

            $query = $this->companyConnection->query("SELECT members from calendar_groups where id=".$id);
            $data = $query->fetch();
           if(empty($data)){
            $data1['members'] =$members;
            $data1['description'] =$_POST['description'];
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE calendar_groups SET " . $sqlData['columnsValuesPair'] . " where id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
           }else if(!empty($data)){
              $new_arr= explode(',',$data['members']);

              for($i=0;$i<count($mem);$i++){
                  array_push($new_arr,$mem[$i]);
              }
               $new_arr=implode(',',$new_arr);
               $data1['members'] =$new_arr;
               $data1['description'] =$_POST['description'];
               $data1['updated_at'] = date('Y-m-d H:i:s');
               $sqlData = createSqlColValPair($data1);
               $query = "UPDATE calendar_groups SET " . $sqlData['columnsValuesPair'] . " where id=".$id;
               $stmt = $this->companyConnection->prepare($query);
               $stmt->execute();

           }
            return array('code' => 200, 'status' => 'success', 'message' => 'Records Updated successfully!');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

}

$TenantShortTermAjax = new manageGroups();

