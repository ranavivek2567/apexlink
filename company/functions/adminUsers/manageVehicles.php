<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class ManageVehicleAjax extends DBConnection {

    /**
     * UserAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function insert(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            /*Required variable array*/
            $required_array = [ ];
            /* Max length variable array */
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = ['starting_mileage'];
            /*Server side validation*/
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            /*Checking server side validation*/
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                if(isset($data['vehicle_edit_id']) || empty($data['vehicle_edit_id'])) unset($data['vehicle_edit_id']);
                /*check if vehicle number field already exists or not*/
                $duplicate = checkNameAlreadyExists($this->companyConnection , 'company_manage_vehicle', 'vehicle', $data['vehicle'],'');
                if($duplicate['is_exists'] == 1){
                    if(!empty($duplicate['data']['id']) ) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Vehicle number already exists!');

                    }
                }

                $data['user_id']    = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['date_purchased'] = (empty( $data['date_purchased'])) ? null : mySqlDateFormat($data['date_purchased'],$_SESSION[SESSION_DOMAIN]['cuser_id'],$this->companyConnection);
                $data['created_at'] =  date('Y-m-d H:i:s');
                $data['updated_at'] =  date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_manage_vehicle (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record created successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update vehicle by id
     * @return array
     * @throws Exception
     */
    public function update()
    {
        try{
            $data = $_POST['form'];
            $data = postArray($data);
            $vehicle_edit_id = $data['vehicle_edit_id'];
            /*Required variable array*/
            $required_array = ['vehicle'];
            /*Max length variable array*/
            $maxlength_array = [];

            /*Number variable array*/
            $number_array = ['starting_mileage'];
            /*Server side validation*/
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array, $vehicle_edit_id, 'company_manage_vehicle');
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['vehicle_edit_id'])){
                    unset($data['vehicle_edit_id']);
                }

                $data['user_id']    = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['date_purchased'] = (empty( $data['date_purchased'])) ? '' : mySqlDateFormat($data['date_purchased'],$_SESSION[SESSION_DOMAIN]['cuser_id'],$this->companyConnection);
                $data['updated_at'] =  date('Y-m-d H:i:s');

                /*check if vehicle number field already exists or not*/
                $duplicate = checkNameAlreadyExists($this->companyConnection , 'company_manage_vehicle', 'vehicle', $data['vehicle'],$vehicle_edit_id);
                if($duplicate['is_exists'] == 1){
                    if(empty($duplicate['data']['id']) || $vehicle_edit_id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Vehicle number already exists!');

                    }
                }

                /*Save Data in Company Database*/
                $sqlData = createSqlUpdateCase($data);
                $query = "UPDATE company_manage_vehicle SET ".$sqlData['columnsValuesPair']." where id='$vehicle_edit_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($sqlData['data']);
                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully.');
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e);
        }
    }

    /**
     * Get vehicle detail by id
     * @return mixed
     */
    public function view(){
        $id = $_POST['id'];
        $query = $this->companyConnection->query("SELECT * FROM company_manage_vehicle WHERE id='$id'");
        $user = $query->fetch();
        if(!empty($user)){
            $user['date_purchased'] = dateFormatUser($user['date_purchased'],null,$this->companyConnection);
        }
        return $user;
    }

    /**
     * Delete vehicle by id
     * @return array
     */
    public function delete() {
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE company_manage_vehicle SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

}

$manageVehicleAjax = new ManageVehicleAjax();
