<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class manageUsers extends DBConnection {

    /**
     * manageUserRoles constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Insert data to company users table
     * @return array|void
     * @throws Exception
     */
    public function insert(){
        try {
            $data = $_POST['form'];
            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data);

            /*Required variable array*/
            $required_array = ['first_name','last_name','email','role','status','work_phone','mobile_number'];
            $maxlength_array = [];
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                if(isset($data['edit_user_id']) || empty($data['edit_user_id'])){
                    unset($data['edit_user_id']);
                }

                $custom_field_hidden_id = (isset($data['id']) && !empty($data['id']))?$data['id']:Null;
                if(isset($custom_field_hidden_id)) unset($data['id']);
                if(!empty($custom_field)){
                    foreach ($custom_field as $key=>$value){
                        unset($data[$value['name']]);
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }

                $domain = explode('.', $_SERVER['HTTP_HOST']);
                $domain = array_shift($domain);
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['parent_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['created_by'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['custom_fields'] = (count($custom_field)>0) ? serialize($custom_field):Null;
                $data['admin_user_id'] = (isset($data['admin_user_id']))? $data['admin_user_id']: Null;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $password = randomString();
                $data['actual_password'] = $password;
                $data['password'] = md5($password);
                $data['domain_name'] = $domain;
                $data['user_type'] = 1;

                /*check if email field already exists or not*/
                $duplicate = checkNameAlreadyExists($this->companyConnection , 'users', 'email', $data['email'],$custom_field_hidden_id);
                if($duplicate['is_exists'] == 1){
                    if(empty($duplicate['data']['id']) || $custom_field_hidden_id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Email already exists!');
                    }
                }

                /*Save Data in Company Database*/
                if (isset($data['id'])){
                    unset($data['id']);
                }
                $sqlData = createSqlColVal($data);
                $custom_data = ['is_deletable' => '0','is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if(count($custom_field)> 0){
                    foreach ($custom_field as $key=>$value){
                        updateCustomField($this->companyConnection ,$updateColumn,$value['id']);
                    }
                }

                if(empty($custom_field_hidden_id)) {
                    $query = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $lastInsertId = $this->companyConnection->lastInsertId();
                    $this->sendMail($lastInsertId);
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record added successfully.');
                }
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e);
        }
    }


    /**
     *
     * @param null $userId
     * @return array
     */
    public function sendMail($userId = null)
    {
        try{
            $server_name = 'https://'.$_SERVER['HTTP_HOST'];
            $userId = (isset($userId) ? $userId : $_REQUEST['id']);
            $user_details = $this->companyConnection->query("SELECT * FROM users where id=".$userId)->fetch();
            $user_name = userName($user_details['id'], $this->companyConnection);
            $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/welcomeUser.php');
            $body = str_replace("#name#",$user_name,$body);
            $body = str_replace("#email#",$user_details['email'],$body);
            $body = str_replace("#password#",$user_details['actual_password'],$body);
            $body = str_replace("#logo#",SITE_URL.'/company/images/logo.png',$body);
            $body = str_replace("#website#",$server_name,$body);

            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $user_details['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal']  = '1';
            curlRequest($request);

            return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Update data to company user roles table
     * @return array|void
     * @throws Exception
     */
    public function update()
    {
        try{
            $data = $_POST['form'];
            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data);
            $edit_user_id = $data['edit_user_id'];
            /*Required variable array*/
            $required_array = ['first_name','last_name','email','role','work_phone','mobile_number'];
            /* Max length variable array */
            $maxlength_array = [ ];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array, $edit_user_id, 'users');
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                /*declaring additional variables*/
                if(!empty($custom_field)){
                    foreach ($custom_field as $key=>$value){
                        unset($data[$value['name']]);
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                if(isset($data['edit_user_id'])){
                    unset($data['edit_user_id']);
                }

                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['status'] = (isset($data['status'])) ? $data['status'] : '0';
                $data['parent_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['custom_fields'] = (count($custom_field)>0) ? serialize($custom_field):NULL;
                $data['admin_user_id'] = (isset($data['admin_user_id']))? $data['admin_user_id']: NULL;
                $data['updated_at'] = date('Y-m-d H:i:s');

                /*check if custom field already exists or not*/
                $duplicate = checkNameAlreadyExists($this->companyConnection , 'users', 'email', $data['email'],$edit_user_id);
                if($duplicate['is_exists'] == 1){
                    if(empty($duplicate['data']['id']) || $edit_user_id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Email already exists!');
                    }
                }

                /*Save Data in Company Database*/
                $custom_data = ['is_deletable' => '0','is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if(count($custom_field)> 0){
                    foreach ($custom_field as $key=>$value){
                        updateCustomField($this->companyConnection ,$updateColumn,$value['id']);
                    }
                }

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id='$edit_user_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully.');


            }
        } catch (PDOException $e) {
            print_r($e);
            return array('code' => 400, 'status' => 'failed','message' => $e);
        }
    }

    /**
     * Get all user roles
     * @return array
     */
    public function getAllUserRoles() {
        try {
            if(isset($_POST['role_id'])){
                $role_id = $_POST['role_id'];
            } else {
                $role_id = '';
            }
            if(isset($role_id) && !empty($role_id)){
                $data = getDataById($this->companyConnection, 'company_user_roles', $role_id);
                $data['data']['role'] = json_decode($data['data']['role']);
                return $data;
            } else {
                $data =$this->companyConnection->query("SELECT * FROM company_user_roles WHERE status='1' AND deleted_at IS NULL")->fetchAll();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'All user\'s roles fetched successfully.');
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Get all users
     * @return array
     */
    public function getAllUsers() {
        try {
            $data =$this->companyConnection->query("SELECT * FROM users WHERE status='1' AND deleted_at IS NULL AND id != '1'")->fetchAll();
            foreach ($data as $key => $value) {
                $data[$key]['user_name'] = userName($value['id'], $this->companyConnection);
            }
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'All users fetched successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Delete User
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE users SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update User Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];
            $updated_at = date('Y-m-d H:i:s');

            $sql = "UPDATE users SET status=?, updated_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $updated_at, $id]);

            if($status == '1'){
                $status_message='Record activated successfully.';
            }
            if($status == '0'){
                $status_message='Record deactivated successfully.';
            }

            if($stmt){
                return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $status_message);
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Get user by id
     * @return array
     */
    public function getUserById() {
        try {
            $id = $_POST['id'];
            $data = getDataById($this->companyConnection, 'users', $id);
            if(!empty($data['data']) && count($data['data']) > 0){
                $custom_data = !empty($data['data']['custom_fields']) ? unserialize($data['data']['custom_fields']) : null;
                if(!empty($custom_data)){
                    foreach ($custom_data as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_data[$key]['default_value'] = dateFormatUser($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_data[$key]['value'] = dateFormatUser($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                $data['custom_data'] = $custom_data;
                return $data;
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    public function restoreManageUser()
    {

        try {

            $deleted_at['deleted_at'] = NULL ;
            $sqlData = $this->createSqlColValPair($deleted_at);

            $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." WHERE id =".$_POST['cuser_id'];
            $stmt1 =$this->companyConnection->prepare($query);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $query, 'message' => 'Records restored successfully');


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


}



$manageUsers = new manageUsers();
