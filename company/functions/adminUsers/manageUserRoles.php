<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class manageUserRoles extends DBConnection {

    /**
     * manageUserRoles constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Insert data to company user roles table
     * @return array
     */
    public function insert()
    {
        try {
            $data = $_POST['form'];

            $required_array = ['role_name'];
            /* Max length variable array */
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];

            /*Server side validation*/
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }

                if (empty($data['checked_ids'])){
                    return array('code' => 503, 'status' => 'error', 'message' => 'Please grant access to at least one module.');
                }
                if(isset($data['edit_user_role_id'])){
                    unset($data['edit_user_role_id']);
                }

                if(isset($data['checked_ids'])){
                    unset($data['checked_ids']);
                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['status'] = 1;
                $data['is_editable'] = 1;
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['role'] = serialize(json_decode($data['role']));

                $checkExists =  checkNameAlreadyExists($this->companyConnection,'company_user_roles', 'role_name', $data['role_name'], '');
                if($checkExists['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Role name already exists.');
                }

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_user_roles (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update data to company user roles table
     * @return array
     */
    public function update()
    {
        try {
            $data = $_POST['form'];
            $edit_user_role_id = $data['edit_user_role_id'];

            $required_array = ['role_name'];
            /* Max length variable array */
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];

            /*Server side validation*/
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }

                if(isset($data['edit_user_role_id']) && !empty($data['edit_user_role_id'])){
                    unset($data['edit_user_role_id']);
                }

                if (empty($data['checked_ids'])){
                    return array('code' => 503, 'status' => 'error', 'message' => 'Please grant access to at least one module.');
                }

                if(isset($data['checked_ids'])){
                    unset($data['checked_ids']);
                }

                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['is_editable'] = 1;
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $roles = serialize(json_decode($data['role']));

                $checkExists =  checkNameAlreadyExists($this->companyConnection,'company_user_roles', 'role_name', $data['role_name'], $edit_user_role_id);
                if($checkExists['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Role name already exists.');
                }
                $query = "UPDATE company_user_roles SET role_name=?, updated_at=?, role=? WHERE id=?";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute([$data['role_name'], $data['updated_at'], $roles, $edit_user_role_id]);
                checkPermission($_SESSION[SESSION_DOMAIN]['cuser_id'],$_SESSION[SESSION_DOMAIN]['role'],$this->companyConnection);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record updated successfully.');
            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Get all user roles
     * @return array
     */
    public function getAllUserRoles() {
        try {
            if(isset($_POST['role_id'])){
                $role_id = $_POST['role_id'];
            } else {
                $role_id = '';
            }
            if(isset($role_id) && !empty($role_id)){
                $data = getDataById($this->companyConnection, 'company_user_roles', $role_id);
                $data['data']['role'] = unserialize($data['data']['role']);
                return $data;
            } else {
                $data =$this->companyConnection->query("SELECT * FROM company_user_roles WHERE status='1' AND deleted_at IS NULL")->fetchAll();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'All user\'s roles fetched successfully.');
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Delete User Roles
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['role_id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE company_user_roles SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update User Role Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['role_id'];
            $status = $_POST['status'];
            $updated_at = date('Y-m-d H:i:s');

            $sql = "UPDATE company_user_roles SET status=?, updated_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $updated_at, $id]);

            if($status == '1'){
                $status_message='Record activated successfully.';
            }
            if($status == '0'){
                $status_message='Record deactivated successfully.';
            }

            if($stmt){
                return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $status_message);
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    public function restoreManageUserRole()
    {

        try {

            $deleted_at['deleted_at'] = NULL ;
            $sqlData = $this->createSqlColValPair($deleted_at);

            $query = "UPDATE company_user_roles SET ".$sqlData['columnsValuesPair']." WHERE id =".$_POST['cuser_id'];
            $stmt1 =$this->companyConnection->prepare($query);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $query, 'message' => 'Records restored successfully');


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }



}



$manageUserRoles = new manageUserRoles();
