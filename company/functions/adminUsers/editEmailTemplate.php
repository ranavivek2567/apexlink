<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class editEmailTemplate extends DBConnection {

    /**
     * manageUserRoles constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function emailTemplateDataDetails()
    {
        try{
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM emailtemplatesadmin WHERE id =".$id);
            // echo "<pre>";print_r($query);die;
            $settings = $query->fetch();
            $settingsAll['templateTitle'] = $settings;

            $template_key = $settings['template_key'];
            //$query1 = $this->companyConnection->query("SELECT * FROM tags_emailTemplate as t LEFT JOIN emailTemplatesAdmin as e ON t.template_key = e.template_key  WHERE t.template_key='$template_key'");
            $query1 = $this->companyConnection->query("SELECT * FROM tags_emailtemplate  WHERE template_key='$template_key'");
            // echo "<pre>";print_r($query1);die;
            $settingsAll['templateTags'] = $query1->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settingsAll, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function updateTemplateEdit(){
        try{
            $template_key =$_POST['template_key'];
            $template_html =  $_POST['template_html'];
            $updated_at = date('Y-m-d H:i:s');
            //dd($updated_at);

            $sql = "UPDATE emailtemplatesadmin SET template_html=?,updated_at=? WHERE template_key=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$template_html,$updated_at,$template_key]);
            //dd($stmt);
            return ['status'=>'success','code'=>200,'data'=>'Template added successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }



}



$editEmailTemplate = new editEmailTemplate();
