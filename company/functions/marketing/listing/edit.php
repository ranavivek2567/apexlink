<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class edit extends DBConnection {

    /**
     * propertyCustomfield constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function getProperty() {
        try {
            $id=$_POST['id'];
            $queryFetch = $this->companyConnection->query("SELECT posting_type,garage_available,property_for_sale,is_short_term_rental,property_year,state,address1,city,country,property_year,no_of_buildings,no_of_units,
                          vacant,description,pet_friendly,property_style,property_type FROM general_property WHERE id =$id");
            $checkData = $queryFetch->fetch();

            $amenties= $this->companyConnection->query("SELECT amenities FROM general_property WHERE id = $id");
            $checkamenties = $amenties->fetch();
            $unserAmen=unserialize($checkamenties['amenities']);

            if(!empty($unserAmen)) {
                foreach ($unserAmen as $amentites) {
                    $amentiesdata = $this->companyConnection->query("SELECT name FROM company_property_amenities WHERE id = $amentites");
                    $checkData['amenties'][] = $amentiesdata->fetch();
                }
            }

            return array('data' => $checkData, 'status' => 'success','amentiesData'=>$unserAmen);
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function updateProperty() {
        try {
            $id=$_POST['id'];
            $data = $_POST['form'];
            $data = postArray($data);

            $data['property_year'] = (!empty($data['property_year'])) ? $data['property_year'] : '0';
            $sqlData = createSqlColValPair($data);
            //Save Data in Company Database
            $query = "UPDATE general_property SET " . $sqlData['columnsValuesPair'] . " where id=$id";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();



            $getLatLong=$this->getLatLong($id);

            return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record Updated successfully');
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function fetchAllAmenities() {
        $html = '';

        $sql = "SELECT * FROM   company_property_amenities";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html .="<div id='selectAllamennity' class='commonCheckboxAmenClass'><input type='checkbox' value='selectall'  class='all'>Select all</div>";
        foreach ($data as $d) {
            $name = $d['name'];
            $idd = $d['id'];
            $html.= "<div class='commonCheckboxAmentiesClass'><input class='inputAllamenity' type='checkbox' value='$idd'  id='$idd' name='amenities[]'> $name </div>";
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function updateAmenties() {
        try {
            $id=$_POST['id'];
            if(!empty($_POST['form'])){
                $data = $_POST['form'];
                $data = postArray($data);
            }else{
                $data = null;
            }


            $data['amenities'] = (!empty($data['amenities'])) ? serialize((array) $data['amenities']) : '';
            //Save Data in Company Database
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE general_property SET " . $sqlData['columnsValuesPair'] . " where id=$id";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function getLatLong($id){
        try {
            $propertyAddress = "SELECT address1,address2,address_list,address3,address4 FROM  general_property WHERE id=".$id;
            $res = $this->companyConnection->query($propertyAddress)->fetch();

            if(!empty($res['address1']) || !empty($res['address2']) || !empty($res['address3']) || !empty($res['address4'])) {
                $address = $res['address1'] . '' . $res['address2'] . '' . $res['address3'] . '' . $res['address4'];
                $formattedAddr = str_replace(' ', '+', $address);

                $api_maps = 'https://maps.googleapis.com/maps/';

                $api_key = 'AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww';
                $url = $api_maps . 'api/geocode/json?address=' . $formattedAddr . '&key=' . $api_key;
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $curl_response = curl_exec($curl);
                curl_close($curl);
                $response_a = json_decode($curl_response);
                if(!empty($response_a)){
                    $lat = $response_a->results[0]->geometry->location->lat;
                    $long = $response_a->results[0]->geometry->location->lng;
                }else{
                    $lat = null;
                    $long = null;
                }
                $mapArr = [];
                $mapArr['property_id'] = $id;
                $mapArr['address'] = $address;
                $mapArr['latitude'] = $lat;
                $mapArr['longitude'] = $long;
                $mapArr['created_at'] = date('Y-m-d H:i:s');
                $mapArr['updated_at'] = date('Y-m-d H:i:s');


                $mapCheck = "SELECT property_id FROM  map_address WHERE property_id=".$id;
                $resCheck = $this->companyConnection->query($mapCheck)->fetch();
                if(empty($resCheck)){
                    $renovationData = createSqlColVal($mapArr);
                    $query1 = "INSERT INTO map_address (" . $renovationData['columns'] . ") VALUES (" . $renovationData['columnsValues'] . ")";

                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute($mapArr);
                }else{

                    $renovationData = createSqlUpdateCase($mapArr);
                    $query = "UPDATE map_address SET " . $renovationData['columnsValuesPair'] . " where property_id=".$id;

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($renovationData['data']);

                }

          }

            return "done";
        } catch (Exception $e){
            dd($e);
        }

    }




}

$edit = new edit();
?>