<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class viewEdit extends DBConnection {

    /**
     * propertyCustomfield constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function getProperty() {
        try {
            $id=$_POST['id'];
            $queryFetch = $this->companyConnection->query("SELECT gp.posting_type,gp.garage_available,gp.property_for_sale,gp.property_year, gp.state, gp.address1, gp.city, gp.country, gp.property_year, gp.no_of_buildings, gp.no_of_units, gp.vacant, gp.description, cpt.property_type, cps.property_style,cpf.pet_friendly FROM general_property AS gp JOIN company_property_type AS cpt ON gp.property_type = cpt.id JOIN company_property_style AS cps ON gp.property_style = cps.id JOIN company_pet_friendly AS cpf ON gp.pet_friendly = cpf.id WHERE gp.id =$id");
            $checkData = $queryFetch->fetch();

            $amenties= $this->companyConnection->query("SELECT amenities FROM general_property WHERE id = $id");
            $checkamenties = $amenties->fetch();
            $unserAmen=unserialize($checkamenties['amenities']);
            if(!empty($unserAmen)) {
                foreach ($unserAmen as $amentites) {
                    $amentiesdata = $this->companyConnection->query("SELECT name FROM company_property_amenities WHERE id = $amentites");
                    $checkData['amenties'][] = $amentiesdata->fetch();
                }
            }
            $getTenantDetail=$this->getTenantDetail($id);
            return array('data' => $checkData, 'status' => 'success','getTenantDetail'=>$getTenantDetail);
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function getTenantDetail($id){
        $Tdetail= $this->companyConnection->query("SELECT u.name,u.email,u.phone_number FROM tenant_property as tp JOIN users as u on u.id=tp.user_id WHERE tp.property_id=$id");
        $TdetailData = $Tdetail->fetchAll();
        $html='';
        foreach($TdetailData as $Td){
            $html.='<tr><td>'.$Td['name'].'</td>';
            $html.='<td>'.$Td['email'].'</td>';
            $html.='<td>'.$Td['phone_number'].'</td></tr>';
        }
        return $html;
    }
    public function onLoadUnitListing() {
        try {
            $id=$_POST['id'];
            $Udetail1= $this->companyConnection->query("SELECT id,building_unit_status,unit_no,base_rent,bedrooms_no,bathrooms_no,amenities FROM unit_details WHERE property_id= $id");
            $unitData1= $Udetail1->fetchAll();
            $page = $_POST['pagination'];
            $count = count($unitData1);
            if($page == '0'){
                $offset = 0;
            } else {
                $offset = $page*5-5;
            }
            if($offset > $count) $offset = $count;
            $Udetail= $this->companyConnection->query("SELECT id,building_unit_status,unit_no,base_rent,bedrooms_no,bathrooms_no,amenities FROM unit_details WHERE property_id= $id  LIMIT $offset,5");

            $marketing_list_data = $Udetail->fetchAll();
            $pages = count($unitData1)/5;
            if (strpos($pages, '.')) {
                $pages = explode('.', $pages);
                $total_pages = $pages[0] + 1;
            } else {
                $total_pages = $pages;
            }
            $amndata=[];
            if(!empty($marketing_list_data)) {
                foreach ($marketing_list_data as $unit) {
                    if (!empty($unit['amenities'])) {
                        $unserAmen = unserialize($unit['amenities']);
                        if (!empty($unserAmen)) {
                            foreach ($unserAmen as $amentites) {

                                $amentiesdata = $this->companyConnection->query("SELECT name FROM company_property_amenities WHERE id = $amentites");
                                $amndata[] = $amentiesdata->fetch();

                            }
                        }
                    }

                }
            }

            return ['status'=>'success','code'=>200,'marketing_list_data'=>$marketing_list_data,'total_pages'=>$total_pages,'amndata'=>$amndata];
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }





}

$viewEdit = new viewEdit();
?>