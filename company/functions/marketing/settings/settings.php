<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class settings extends DBConnection {

    /**
     * propertyCustomfield constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function addContactDetail() {
        try {
            $data = $_POST;
            $file=$_FILES;


            unset($data['action']);
            unset($data['class']);
//            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                if(!empty($file)) {
                    if(!empty($file['logo']['tmp_name']))
                    {
                        $domain = getDomain();
                        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
                        $file_name = $file['logo']['name'];
                        $file_tmp = $file['logo']['tmp_name'];
                        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                        $name = time() . uniqid(rand());
                        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                        //Check if the directory already exists.
                        if (!is_dir(ROOT_URL . '/' . $path)) {
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/' . $path, 0777, true);
                        }
                        move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                        $data['logo']=$path.'/'.$name.'.'.$ext;
                }
            }

                    $data['user_id']=$_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    if(isset($data['publish']) && $data['publish'] == 'on'){
                        $data['publish'] ='1';
                    }else{
                        $data['publish'] ='0';
                    }
                if (!empty($data['address'])) {
                    $address = $data['address'];

                    $formattedAddr = str_replace(' ', '+', $address);

                    $api_maps = 'https://maps.googleapis.com/maps/';

                    $api_key = 'AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww';
                    $url = $api_maps . 'api/geocode/json?address=' . $formattedAddr . '&key=' . $api_key;
                    $curl = curl_init($url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    $curl_response = curl_exec($curl);
                    curl_close($curl);
                    $response_a = json_decode($curl_response);

                    if (!empty($response_a)) {
                        $data['latitude'] = $response_a->results[0]->geometry->location->lat;
                        $data['longitude']  = $response_a->results[0]->geometry->location->lng;
                    } else {
                        $data['latitude']  = null;
                        $data['longitude']  = null;
                    }
                }
                   $user_data =  $this->companyConnection->query("SELECT * FROM marketing_contact_detail WHERE user_id=".$data['user_id'])->fetch();

                    if(empty($user_data)) {

                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO marketing_contact_detail (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                        $id = $this->companyConnection->lastInsertId();

                        return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');

                    }else{

                        $sqlData = createSqlUpdateCase($data);
                        $query = "UPDATE marketing_contact_detail SET " . $sqlData['columnsValuesPair'] . " where user_id=".$data['user_id'];
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($sqlData['data']);


                        return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                    }

            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function getMarketingDetailAjax(){
        $id = $_POST['id'];
        $user_data =  $this->companyConnection->query("SELECT * FROM marketing_contact_detail WHERE user_id=".$id)->fetch();

            return ['status'=>'success','code'=>200,'data'=>$user_data];

    }





}

$settings = new settings();
?>