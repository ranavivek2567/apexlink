<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class campaign extends DBConnection {

    /**
     * propertyCustomfield constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function addCampaign() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                    $data['start_date']=!empty($data['start_date']) ? mySqlDateFormat($data['start_date'], null, $this->companyConnection) : NULL;
                    $data['end_date']=!empty($data['end_date']) ? mySqlDateFormat($data['end_date'], null, $this->companyConnection) : NULL;

                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $id = isset($_POST['id'])?$_POST['id']:null;
                    if($id == null){
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO campaign (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                        $id = $this->companyConnection->lastInsertId();
                        return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');

                    }else{
                        $sqlData = createSqlUpdateCase($data);
                        $query = "UPDATE campaign SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($sqlData['data']);
                        return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                    }

            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function deleteCampaign() {
        try {
            $id = $_POST['id'];
            $query1 = "DELETE FROM campaign WHERE id=" . $id;
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute();
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Deleted Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function getCampaign() {
        try {
            $id = $_POST['id'];
            //check if custom field already exists or not
            $sql = "SELECT * FROM campaign WHERE id=".$id ;
            $data = $this->companyConnection->query($sql)->fetch();
            $data['start_date']=dateFormatUser($data['start_date'], null, $this->companyConnection);
            $data['end_date']=dateFormatUser($data['end_date'], null, $this->companyConnection);
            if (!empty($data) && count($data) > 0) {
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
            } else {
                return array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }



}

$campaign = new campaign();
?>