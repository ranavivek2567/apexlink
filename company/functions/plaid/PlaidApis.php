<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
include_once(ROOT_URL . "/helper/globalHelper.php");

class PlaidApis extends DBConnection
{
    /**
     * PlaidApis constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to exchange public token to get plaid access token (CURL)
     */
    private function exchangeToken(){
        try {
            $object = $_REQUEST['obj'];
            $public_token = $object['public_token'];
            $account_id = $object['metadata'][1]['id'];
            $id = $object['id'];
            $headers[] = 'Content-Type: application/json';
            $params = [
                'client_id' => PLAID_CLIENT_ID,
                'secret' => PLAID_SECRET_SANDBOX,
                'public_token' => $public_token,
            ];


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://sandbox.plaid.com/item/public_token/exchange");
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_TIMEOUT, 80);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            if(!$result = curl_exec($ch)) {
                trigger_error(curl_error($ch));
            }
            curl_close($ch);

            $jsonParsed = json_decode($result);

            $btok_params = [
                'client_id' => PLAID_CLIENT_ID,
                'secret' => PLAID_SECRET_SANDBOX,
                'access_token' => $jsonParsed->access_token,
                'account_id' => $account_id
            ];


            $data = $this->createBankAccountToken($headers,$btok_params,$id);
            return $data;
        } catch (Exception $e){
            return array(['status' => 'error', 'code' => 400 ,'message' => $e->getMessage()]);
        }
    }

    /**
     * function to get bank account token with plaid access token (CURL)
     * @param $headers
     * @param $btok_params
     * @param $id
     * @return array
     */
    private function createBankAccountToken($headers,$btok_params,$id){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://sandbox.plaid.com/processor/stripe/bank_account_token/create");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($btok_params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if(!$result = curl_exec($ch)) {
            trigger_error(curl_error($ch));
        }
        curl_close($ch);

        return $this->attachSourceToCustomer(json_decode($result),$id);
    }

    /**
     * function to attach bank account to stripe customer
     * @param $obj
     * @param $id
     * @return array
     */
    private function attachSourceToCustomer($obj,$id){
        try{
            $data_id = ($id == 0)?$_SESSION[SESSION_DOMAIN]['cuser_id']:$id;
            $userData = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $data_id], 'users');
            if($userData['code'] == 200){
                if(empty($userData['data']['stripe_customer_id'])){
                    $customerInfo = createCustomer($userData['data']);
                    if($customerInfo['code'] == 200){
                        saveStripeCustomerId($this->companyConnection,['stripe_customer_id'=>$customerInfo['account_data']],$data_id);
                        $response = addStripeSourceBank($customerInfo['account_data'],$obj->stripe_bank_account_token);
                    }
                } else {
                    $data['customer_id'] = $userData['data']['stripe_customer_id'];
                    $customerInfo = getCustomer($data);
                    if($customerInfo['code'] == 200){
                        $response = addStripeSourceBank($customerInfo['account_data'],$obj->stripe_bank_account_token);
                    }
                }
                $response = !empty($response)?$response:$customerInfo;
                return $response;
            }
            return array(['status' => 'error', 'code' => 400 ,'message' => 'User not found!']);
        } catch(Exception $exception){
            return array(['status' => 'error', 'code' => 400 ,'message' => $exception->getMessage()]);
        }
    }

}

$propertyDetail = new PlaidApis();
?>