<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class userAlert extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }
    /**
     * function for fetching listing view data
     * @return array
     */
    public function userAlertEdit(){
        try {
            $html = '';
            $htmlowner = '';
            $htmlvendor = '';
            $htmllease = '';
            $htmlpayment = '';
            $htmlcommunication = '';
            $htmlbusiness = '';
            $htmlmaintenance = '';
            $htmllead = '';
            $dataselected = $this->companyConnection->query("SELECT * FROM `user_alerts` WHERE `module_type`='tenant'")->fetchAll();
            $dataowner = $this->companyConnection->query("SELECT * FROM `user_alerts` WHERE `module_type`='owner'")->fetchAll();
            $datavendor = $this->companyConnection->query("SELECT * FROM `user_alerts` WHERE `module_type`='vendor'")->fetchAll();
            $datalease = $this->companyConnection->query("SELECT * FROM `user_alerts` WHERE `module_type`='lease'")->fetchAll();
            $datapayment = $this->companyConnection->query("SELECT * FROM `user_alerts` WHERE `module_type`='payment'")->fetchAll();
            $datacommunication = $this->companyConnection->query("SELECT * FROM `user_alerts` WHERE `module_type`='communication'")->fetchAll();
            $databusiness = $this->companyConnection->query("SELECT * FROM `user_alerts` WHERE `module_type`='business'")->fetchAll();
            $datamaintenance = $this->companyConnection->query("SELECT * FROM `user_alerts` WHERE `module_type`='maintenance'")->fetchAll();
            $datalead = $this->companyConnection->query("SELECT * FROM `user_alerts` WHERE `module_type`='lead'")->fetchAll();
            foreach($dataselected as $key => $value) {
                $statustenant =$value['status'];
                $send_to_users_unserialzied_data ='';
                $string ='';
                $string_products ='';
                $dataRoles ='';
                $role_name = '';
                $role_name2 = '';

                $html .="<tr class='clsUserAlert'>";
                $html .="<td>".$value["alert_name"]."</td>";
                if(!empty($value["send_to_users"])) {

                    $send_to_users_unserialzied_data = unserialize($value["send_to_users"]);
                    if (is_array($send_to_users_unserialzied_data))
                        $send_to_users_unserialzied_datas = implode(",",$send_to_users_unserialzied_data);
                    $dataRoles = $this->companyConnection->query("SELECT role_name FROM `company_user_roles` WHERE id IN (".$send_to_users_unserialzied_datas.")")->fetchAll();
                    foreach($dataRoles as $role) {
                        $role_name.= $role['role_name'] .',';
                        $role_name2 = str_replace_last(',','',$role_name);
                    }
                }
                $html .="<td>".$role_name2."</td>";
                $html .="<td>".$value["alert_type"]."</td>";
                $html .="<td>".$value["description"]."</td>";
                if($value['status'] == '1')
                {
                    $statustenant = "Active";
                }
                else
                {
                    $statustenant = "Inactive";
                }
                $html .="<td>".$statustenant."</td>";
                $html .="<td>";
                $html .="<select data_id=".$value['id']." module_type='tenant' alert_name='".$value["alert_name"]."' class='form-control select_options'>";
                $html .="<option value='select'>Select</option>";
                $html .="<option value='Edit'>Edit</option>";
                $html .="<option value='Preview'>Preview</option>";
                $html .="<option value='testMail'>Test Mail</option>";
                $html .="</select>";
                $html .="</td>";
                $html .="</tr>";
            }


            foreach($dataowner as $key => $value) {
                $html2_owner ='';
                $string_product_owner ='';
                $dataRoles_owner ='';
                $role_name_owner = '';
                $role_name2_owner = '';
                $htmlowner .="<tr class='clsUserAlertOwner'>";
                $htmlowner .="<td>".$value["alert_name"]."</td>";
                if(!empty($value["send_to_users"])) {
                    $html2_owner = unserialize($value["send_to_users"]);
                    $string_product_owner = implode(',',$html2_owner);
                    $dataRoles_owner = $this->companyConnection->query("SELECT role_name FROM `company_user_roles` WHERE id IN (".$string_product_owner.")")->fetchAll();
                    foreach($dataRoles_owner as $role) {
                        $role_name_owner.= $role['role_name'] .',';
                        $role_name2_owner = str_replace_last(',','',$role_name_owner);
                    }
                }
                $htmlowner .="<td>".$role_name2_owner."</td>";
                $htmlowner .="<td>".$value["alert_type"]."</td>";
                $htmlowner .="<td>".$value["description"]."</td>";
                if($value['status'] == '1')
                {
                    $statusowner = "Active";
                }
                else
                {
                    $statusowner = "Inactive";
                }
                $htmlowner .="<td>".$statusowner."</td>";
                $htmlowner .="<td>";
                $htmlowner .="<select data_id=".$value['id']." module_type='owner' alert_name='".$value["alert_name"]."' class='form-control select_options'>";
                $htmlowner .="<option value='select'>Select</option>";
                $htmlowner .="<option value='Edit'>Edit</option>";
                $htmlowner .="<option value='Preview'>Preview</option>";
                $htmlowner .="<option value='testMail'>Test Mail</option>";
                $htmlowner .="</select>";
                $htmlowner .="</td>";
                $htmlowner .="</tr>";
            }

            foreach($datavendor as $key => $value) {
                $html2_vendor ='';
                $string_product_vendor ='';
                $dataRoles_vendor ='';
                $role_name_vendor = '';
                $role_name2_vendor = '';
                $htmlvendor .="<tr class='clsUserAlertVendor'>";
                $htmlvendor .="<td>".$value["alert_name"]."</td>";
                if(!empty($value["send_to_users"])) {
                    $html2_vendor = unserialize($value["send_to_users"]);
                    $string_product_vendor = implode(',',$html2_vendor);
                    $dataRoles_vendor = $this->companyConnection->query("SELECT role_name FROM `company_user_roles` WHERE id IN (".$string_product_vendor.")")->fetchAll();
                    foreach($dataRoles_vendor as $role) {
                        $role_name_vendor.= $role['role_name'] .',';
                        $role_name2_vendor = str_replace_last(',','',$role_name_vendor);
                    }
                }
                $htmlvendor .="<td>".$role_name2_vendor."</td>";
                $htmlvendor .="<td>".$value["alert_type"]."</td>";
                $htmlvendor .="<td>".$value["description"]."</td>";
                if($value['status'] == '1')
                {
                    $statusvendor = "Active";
                }
                else
                {
                    $statusvendor = "Inactive";
                }
                $htmlvendor .="<td>".$statusvendor."</td>";
                $htmlvendor .="<td>";
                $htmlvendor .="<select data_id=".$value['id']." module_type='vendor' alert_name='".$value["alert_name"]."' class='form-control select_options'>";
                $htmlvendor .="<option value='select'>Select</option>";
                $htmlvendor .="<option value='Edit'>Edit</option>";
                $htmlvendor .="<option value='Preview'>Preview</option>";
                $htmlvendor .="<option value='testMail'>Test Mail</option>";
                $htmlvendor .="</select>";
                $htmlvendor .="</td>";
                $htmlvendor .="</tr>";
            }

            foreach ($datalease as $key => $value){
                $html2_lease ='';
                $string_product_lease ='';
                $dataRoles_lease ='';
                $role_name_lease = '';
                $role_name2_lease = '';
                $htmllease .="<tr class='clsUserAlertLease'>";
                $htmllease .="<td>".$value["alert_name"]."</td>";
                if(!empty($value["send_to_users"])) {
                    $html2_lease = unserialize($value["send_to_users"]);
                    $string_product_lease = implode(',',$html2_lease);
                    $dataRoles_lease = $this->companyConnection->query("SELECT role_name FROM `company_user_roles` WHERE id IN (".$string_product_lease.")")->fetchAll();
                    foreach($dataRoles_lease as $role) {
                        $role_name_lease.= $role['role_name'] .',';
                        $role_name2_lease = str_replace_last(',','',$role_name_lease);
                    }
                }
                $htmllease .="<td>".$role_name2_lease."</td>";
                $htmllease .="<td>".$value["alert_type"]."</td>";
                $htmllease .="<td>".$value["description"]."</td>";
                if($value['status'] == '1')
                {
                    $statuslease = "Active";
                }
                else
                {
                    $statuslease = "Inactive";
                }
                $htmllease .="<td>".$statuslease."</td>";
                $htmllease .="<td>";
                $htmllease .="<select data_id=".$value['id']." module_type='lease' alert_name='".$value["alert_name"]."' class='form-control select_options'>";
                $htmllease .="<option value='select'>Select</option>";
                $htmllease .="<option value='Edit'>Edit</option>";
                $htmllease .="<option value='Preview'>Preview</option>";
                $htmllease .="<option value='testMail'>Test Mail</option>";
                $htmllease .="</select>";
                $htmllease .="</td>";
                $htmllease .="</tr>";
            }
            foreach($datamaintenance as $key =>$value){
                $html2_maintenance ='';
                $string_product_maintenance ='';
                $dataRoles_maintenance ='';
                $role_name_maintenance = '';
                $role_name2_maintenance = '';
                $htmlmaintenance .="<tr class='clsUserAlertMaintenance'>";
                $htmlmaintenance .="<td>".$value["alert_name"]."</td>";
                if(!empty($value["send_to_users"])) {
                    $html2_maintenance = unserialize($value["send_to_users"]);
                    $string_product_maintenance = implode(',',$html2_maintenance);
                    $dataRoles_maintenance = $this->companyConnection->query("SELECT role_name FROM `company_user_roles` WHERE id IN (".$string_product_maintenance.")")->fetchAll();
                    foreach($dataRoles_maintenance as $role) {
                        $role_name_maintenance.= $role['role_name'] .',';
                        $role_name2_maintenance = str_replace_last(',','',$role_name_maintenance);
                    }
                }
                $htmlmaintenance .="<td>".$role_name2_maintenance."</td>";
                $htmlmaintenance .="<td>".$value["alert_type"]."</td>";
                $htmlmaintenance .="<td>".$value["description"]."</td>";
                if($value['status'] == '1')
                {
                    $statusmaintenance = "Active";
                }
                else
                {
                    $statusmaintenance = "Inactive";
                }
                $htmlmaintenance .="<td>".$statusmaintenance."</td>";
                $htmlmaintenance .="<td>";
                $htmlmaintenance .="<select data_id=".$value['id']." module_type='maintenance' alert_name='".$value["alert_name"]."' class='form-control select_options'>";
                $htmlmaintenance .="<option value='select'>Select</option>";
                $htmlmaintenance .="<option value='Edit'>Edit</option>";
                $htmlmaintenance .="<option value='Preview'>Preview</option>";
                $htmlmaintenance .="<option value='testMail'>Test Mail</option>";
                $htmlmaintenance .="</select>";
                $htmlmaintenance .="</td>";
                $htmlmaintenance .="</tr>";
            }
            foreach($datalead as $key=>$value){
                $html2_lead ='';
                $string_product_lead ='';
                $dataRoles_lead ='';
                $role_name_lead = '';
                $role_name2_lead = '';
                $htmllead .="<tr class='clsUserAlertLead'>";
                $htmllead .="<td>".$value["alert_name"]."</td>";
                if(!empty($value["send_to_users"])) {
                    $html2_lead = unserialize($value["send_to_users"]);
                    $string_product_lead = implode(',',$html2_lead);
                    $dataRoles_lead = $this->companyConnection->query("SELECT role_name FROM `company_user_roles` WHERE id IN (".$string_product_lead.")")->fetchAll();
                    foreach($dataRoles_lead as $role) {
                        $role_name_lead.= $role['role_name'] .',';
                        $role_name2_lead = str_replace_last(',','',$role_name_lead);
                    }
                }
                $htmllead .="<td>".$role_name2_lead."</td>";
                $htmllead .="<td>".$value["alert_type"]."</td>";
                $htmllead .="<td>".$value["description"]."</td>";
                if($value['status'] == '1')
                {
                    $statuslead = "Active";
                }
                else
                {
                    $statuslead = "Inactive";
                }
                $htmllead .="<td>".$statuslead."</td>";
                $htmllead .="<td>";
                $htmllead .="<select data_id=".$value['id']." module_type='lead' alert_name='".$value["alert_name"]."' class='form-control select_options'>";
                $htmllead .="<option value='select'>Select</option>";
                $htmllead .="<option value='Edit'>Edit</option>";
                $htmllead .="<option value='Preview'>Preview</option>";
                $htmllead .="<option value='testMail'>Test Mail</option>";
                $htmllead .="</select>";
                $htmllead .="</td>";
                $htmllead .="</tr>";
            }
            foreach($datapayment as $key=> $value){
                $html2_payment ='';
                $string_product_payment ='';
                $dataRoles_payment ='';
                $role_name_payment = '';
                $role_name2_payment = '';
                $htmlpayment .="<tr class='clsUserAlertPayment'>";
                $htmlpayment .="<td>".$value["alert_name"]."</td>";
                if(!empty($value["send_to_users"])) {
                    $html2_payment = unserialize($value["send_to_users"]);
                    $string_product_payment = implode(',',$html2_payment);
                    $dataRoles_payment = $this->companyConnection->query("SELECT role_name FROM `company_user_roles` WHERE id IN (".$string_product_payment.")")->fetchAll();
                    foreach($dataRoles_payment as $role) {
                        $role_name_payment.= $role['role_name'] .',';
                        $role_name2_payment = str_replace_last(',','',$role_name_payment);
                    }
                }
                $htmlpayment .="<td>".$role_name2_payment."</td>";
                $htmlpayment .="<td>".$value["alert_type"]."</td>";
                $htmlpayment .="<td>".$value["description"]."</td>";
                if($value['status'] == '1')
                {
                    $statuspayment = "Active";
                }
                else
                {
                    $statuspayment = "Inactive";
                }
                $htmlpayment .="<td>".$statuspayment."</td>";
                $htmlpayment .="<td>";
                $htmlpayment .="<select data_id=".$value['id']." module_type='payment' alert_name='".$value["alert_name"]."' class='form-control select_options'>";
                $htmlpayment .="<option value='select'>Select</option>";
                $htmlpayment .="<option value='Edit'>Edit</option>";
                $htmlpayment .="<option value='Preview'>Preview</option>";
                $htmlpayment .="<option value='testMail'>Test Mail</option>";
                $htmlpayment .="</select>";
                $htmlpayment .="</td>";
                $htmlpayment .="</tr>";
            }

            foreach($datacommunication as $key => $value){
                $html2_communication ='';
                $string_product_communication ='';
                $dataRoles_communication ='';
                $role_name_communication = '';
                $role_name2_communication = '';
                $htmlcommunication .="<tr class='clsUserAlertCommunication'>";
                $htmlcommunication .="<td>".$value["alert_name"]."</td>";
                if(!empty($value["send_to_users"])) {
                    $html2_communication = unserialize($value["send_to_users"]);
                    $string_product_communication = implode(',',$html2_communication);
                    $dataRoles_communication = $this->companyConnection->query("SELECT role_name FROM `company_user_roles` WHERE id IN (".$string_product_communication.")")->fetchAll();
                    foreach($dataRoles_communication as $role) {
                        $role_name_communication.= $role['role_name'] .',';
                        $role_name2_communication = str_replace_last(',','',$role_name_communication);
                    }
                }
                $htmlcommunication .="<td>".$role_name2_communication."</td>";
                $htmlcommunication .="<td>".$value["alert_type"]."</td>";
                $htmlcommunication .="<td>".$value["description"]."</td>";
                if($value['status'] == '1')
                {
                    $statuscommunication = "Active";
                }
                else
                {
                    $statuscommunication = "Inactive";
                }
                $htmlcommunication .="<td>".$statuscommunication."</td>";
                $htmlcommunication .="<td>";
                $htmlcommunication .="<select data_id=".$value['id']." module_type='communication' alert_name='".$value["alert_name"]."' class='form-control select_options'>";
                $htmlcommunication .="<option value='select'>Select</option>";
                $htmlcommunication .="<option value='Edit'>Edit</option>";
                $htmlcommunication .="<option value='Preview'>Preview</option>";
                $htmlcommunication .="<option value='testMail'>Test Mail</option>";
                $htmlcommunication .="</select>";
                $htmlcommunication .="</td>";
                $htmlcommunication .="</tr>";
            }

            foreach($databusiness as $key =>$value){
                $html2_business ='';
                $string_product_business ='';
                $dataRoles_business ='';
                $role_name_business = '';
                $role_name2_business = '';
                $htmlbusiness .="<tr class='clsUserAlertBusiness'>";
                $htmlbusiness .="<td>".$value["alert_name"]."</td>";
                if(!empty($value["send_to_users"])) {
                    $html2_business = unserialize($value["send_to_users"]);
                    $string_product_business = implode(',',$html2_business);
                    $dataRoles_business = $this->companyConnection->query("SELECT role_name FROM `company_user_roles` WHERE id IN (".$string_product_business.")")->fetchAll();
                    foreach($dataRoles_business as $role) {
                        $role_name_business.= $role['role_name'] .',';
                        $role_name2_business = str_replace_last(',','',$role_name_business);
                    }
                }
                $htmlbusiness .="<td>".$role_name2_business."</td>";
                $htmlbusiness .="<td>".$value["alert_type"]."</td>";
                $htmlbusiness .="<td>".$value["description"]."</td>";
                if($value['status'] == '1')
                {
                    $statusbusiness = "Active";
                }
                else
                {
                    $statusbusiness = "Inactive";
                }
                $htmlbusiness .="<td>".$statusbusiness."</td>";
                $htmlbusiness .="<td>";
                $htmlbusiness .="<select data_id=".$value['id']." module_type='business' alert_name='".$value["alert_name"]."' class='form-control select_options'>";
                $htmlbusiness .="<option value='select'>Select</option>";
                $htmlbusiness .="<option value='Edit'>Edit</option>";
                $htmlbusiness .="<option value='Preview'>Preview</option>";
                $htmlbusiness .="<option value='testMail'>Test Mail</option>";
                $htmlbusiness .="</select>";
                $htmlbusiness .="</td>";
                $htmlbusiness .="</tr>";

            }
            return array('code' => 200,'status'=>'success' ,'html' => $html,'owner'=> $htmlowner,'vendor'=> $htmlvendor,'lease'=>$htmllease,'payment'=>$htmlpayment,'communication'=>$htmlcommunication,'business'=>$htmlbusiness,'maintenance'=>$htmlmaintenance,'lead'=>$htmllead);
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }

    /**
     * function for fetching tenant view data
     * @return array
     */
    public function view() {
        try {
            $id = $_POST['data_id'];
            $data = getDataById($this->companyConnection, 'user_alerts', $id);
            if(!empty($data['data']) && count($data['data']) > 0){
                if(!empty($data['data']['send_to_users'])) {
                    $data['data']['send_to_users'] = unserialize($data['data']['send_to_users']);
                }
                return $data;

            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function userRoles(){
        try {
            $dataRoles = $this->companyConnection->query("SELECT id,role_name FROM company_user_roles")->fetchAll();
            return array('code' => 200, 'status' => 'success', 'data' => $dataRoles, 'message' => 'User roles fetched successfully');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update data of useralert listing
     * @return array
     */
    public function update()
    {

        try {
            $data = $_POST;
            $alert_edit_id = $data['alert_edit_id'];
            //Required variable array
            $required_array = ['alert_name', 'description','subject','send_to_users,','status','no_of_days_before','alert_message'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['action']) && !empty($data['action'])){
                    unset($data['action']);
                }
                if(isset($data['class']) && !empty($data['class'])){
                    unset($data['class']);
                }
                if(isset($data['alert_edit_id']) || !empty($data['alert_edit_id'])){
                    unset($data['alert_edit_id']);
                }

                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                 if(isset($data['send_to_users']) && !empty($data['send_to_users'])) {
                     $data['send_to_users'] = serialize($data['send_to_users']);
                 }else{
                     $data['send_to_users'] = '';
                 }
                if(isset($data['is_default']) && $data['is_default'] == 1){
                    $update_data['is_default'] = 0;
                    $sqlData = createSqlColValPair($update_data);
                    $query = "UPDATE user_alerts SET ".$sqlData['columnsValuesPair'];
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                }

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE user_alerts SET ".$sqlData['columnsValuesPair']." where id='$alert_edit_id'";
                $stmt =$this->companyConnection->prepare($query);

                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record updated successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * testmail view
     */
    public function testmail(){
        try
        {
            $data= $_POST;
            $email = $data['email'];
            $module_type = $data['module_type'];
            $alert_name = $data['alert_name'];
            $server_name = 'https://'.$_SERVER['HTTP_HOST'];
            switch ($alert_name){
                case 'Vendor Portal Created':
                    $alert_name = str_replace(' ', '_', $alert_name);
//                    $url = COMPANY_SUBDOMAIN_URL."/views/useralert_email/tenant_test_mail.php?alert_name=".$alert_name;
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/vendor/VendorPortalCreated.php");
                    //print_r($url); die;
//                    $body = file_get_contents($url);
                    break;
                case 'Vendor WorkOrder Alert':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/vendor/VendorWorkOrderAlert.php");
                    break;
                case 'Vendor Added a Comment':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/vendor/VendorAddedAComment.php");
                    break;
                case 'Vendor Work Order Canceled':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/vendor/VendorWorkOrderCanceled.php");

                    break;
                case 'Owner Account Created':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/owner/OwnerAccountCreated.php");

                    break;
                case 'Owner Work Order Alert':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/owner/OwnerWorkOrderAlert.php");

                    break;
                case 'Owner Added a Comment':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/owner/OwnerAddedAComment.php");

                    break;
                case 'Ticket Disapprove':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/owner/TicketDisapprove.php");

                    break;
                case 'New Prospect':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lead/NewProspect.php");

                    break;
                case 'New Applicant':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lead/NewApplicant.php");

                    break;
                case 'Electronic Payment Failed':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/payment/ElectronicPaymentFailed.php");

                    break;
                case 'Tenant Conversation Posted to Portal':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/communication/TenantConversationPostedToPortal.php");

                    break;
                case 'Document Posted to Portal':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/communication/DocumentPostedToPortal.php");

                    break;
                case 'Property Insurance is Expiring':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/business/PropertyInsuranceIsExpiring.php");

                    break;
                case 'Owner’s Conversation Postal to Portal':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/business/Owner’sConversationPostalToPortal.php");

                    break;
                case 'Pending Deposit to Account':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/business/PendingDepositToAccount.php");

                    break;
                case 'Work Order Created':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/maintenance/WorkOrderCreated.php");

                    break;
                case 'Work Order Changed':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/maintenance/WorkOrderChanged.php");

                    break;
                case 'Work Order Completed':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/maintenance/WorkOrderCompleted.php");

                    break;
                case 'Work Order Canceled':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/maintenance/WorkOrderCanceled.php");

                    break;
                case 'Ticket Canceled':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/maintenance/TicketCanceled.php");

                    break;
                case 'Work Order Cancel':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/maintenance/WorkOrderCancel.php");

                    break;
                case 'Start Date':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/StartDate.php");

                    break;
                case 'Schedule Move Out':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/ScheduleMoveOut.php");

                    break;
                case 'Auto Charge Start':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/AutoChargeStart.php");

                    break;
                case 'Auto Charge End':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/AutoChargeEnd.php");

                    break;
                case 'Lease Signed':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/LeaseSigned.php");

                    break;
                case 'First Notice of Lease Expiration':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/FirstNoticeofLeaseExpiration.php");

                    break;
                case 'Second Notice of Lease Expiration':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/SecondNoticeofLeaseExpiration.php");

                    break;
                case 'First Past Due Tenant Notification':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/FirstPastDueTenantNotification.php");

                    break;
                case 'Second Past Due Tenant Notification':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/SecondPastDueTenantNotification.php");

                    break;
                case 'Received Tenant Notice to Vacate':
                $alert_name = str_replace(' ', '_', $alert_name);
                $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/ReceivedTenantNoticetoVacate.php");

                break;
                case 'New Tenant Notification':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/NewTenantNotification.php");

                    break;
                case 'Insufficient Fund Notification':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/lease/InsufficientFundNotification.php");

                    break;
                case 'T/Portal Login Request Alert':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/PortalLoginRequestAlert.php");

                    break;
                case 'Tenant Information Update':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/TenantInformationUpdate.php");

                    break;
                case 'Tenant Make Payment':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/TenantMakePayment.php");

                    break;
                case 'Tenant Give Notice':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/TenantGiveNotice.php");

                    break;
                case 'Send Tenant Monthly Rent Invoices':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/SendTenantMonthlyRentInvoices.php");

                    break;
                case 'Tenant Added Comment':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/TenantAddedComment.php");

                    break;
                case 'Tenant Rental Ins. Purchased':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/TenantRentalInsPurchased.php");

                    break;
                case 'Tenant Rental Ins. Expiring':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/TenantRentalInsExpiring.php");

                    break;
                case 'Tenant Work Order Confirmation':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/TenantWorkOrderConfirmation.php");

                    break;
                case 'Tenant Ticket Canceled':
                    $alert_name = str_replace(' ', '_', $alert_name);
                    $body = file_get_contents(COMPANY_DIRECTORY_URL."/views/useralert_email/tenant/TenantTicketCanceled.php");

                    break;

                default:
            }
//            $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/welcomeUser.php');
            $body1 = str_replace("#alert_name#",$alert_name,$body);
            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $email;
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body1;
            $request['portal']  = '1';
            curlRequest($request);
            return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

}
$userAlert = new userAlert();
?>
