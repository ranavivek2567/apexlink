<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class utilityBilling extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Get data on add new announcement page load
     * @return array
     */
    public function getInitialData(){
        try{
            $data = [];
            $data['propertyList'] = $this->companyConnection->query("SELECT distinct gp.id,gp.property_id, gp.property_name FROM tenant_property tp JOIN general_property gp on gp.id = tp.property_id JOIN tenant_lease_details tld ON tp.user_id = tld.user_id WHERE gp.id = tp.property_id AND tld.record_status = '1'")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Common function to get data by id
     * @return array
     */
    public function getDataByID(){
        $id = $_POST['id'];
        $prev_id = $_POST['prev_id'];
        $data_type = $_POST['data_type'];
        $userData = '';
        $table = '';
        switch ($data_type) {
            case "property":
                $table = 'building_detail';
                $query = "SELECT DISTINCT bd.id, bd.building_name FROM tenant_property tp JOIN building_detail bd ON bd.id = tp.building_id JOIN tenant_lease_details tld ON tld.user_id = tp.user_id WHERE tld.record_status = '1' AND tp.property_id = ".$id;
                break;
            case "building":
                $table = 'unit_details';
                $query = "SELECT DISTINCT ud.id, ud.unit_prefix, ud.unit_no FROM unit_details ud JOIN tenant_property tp ON ud.id = tp.unit_id JOIN tenant_lease_details tld ON tld.user_id = tp.user_id WHERE tld.record_status = '1' AND tp.building_id = ".$id;
                break;
            case "unit":
                $table = 'users';
                $query = "SELECT u.id, u.name  FROM users u LEFT JOIN tenant_property t ON u.id=t.user_id WHERE t.unit_id=$id AND t.record_status = '1'";
                break;
            default:
                $table = '';
                $query = '';
                break;
        }

        if (!empty($query)) {
            $userData = $this->companyConnection->query($query)->fetchAll();
        }
        return array('code' => 200, 'status' => 'success', 'data' => $userData,'message' => 'Data loaded successfully!');
    }

    /**
     * Create/Update/Copy/Publish an announcement
     * @return array
     * @throws Exception
     */
    public function addUtilityBills() {
        try {
            $data = $_POST;
            $firstDayNextMonth = date('Y-m-d H:i:s', strtotime('first day of next month'));
            $lastDayNextMonth = date('Y-m-t',strtotime('last day of next month'));
            for ($i = 0;$i < count($data['user_id']);$i++){
                $tenant_id = $data['user_id'][$i];
                $query = "SELECT tld.id, tld.end_date  FROM tenant_lease_details tld WHERE tld.user_id = $tenant_id AND tld.record_status = '1'";
                $tenantData = $this->companyConnection->query($query)->fetch();
//                print_r();die();
                $queryData['user_id']          =  $data['user_id'][$i];
                $queryData['utility_type']     =  $data['utility'][$i];
                $queryData['start_date']       =  $firstDayNextMonth;
                $queryData['end_date']         =  $tenantData['end_date'];
                $queryData['utility_amount']   =  $data['utility_amount'][$i];
                $queryData['status']           =  'Pending';
                $queryData['created_at']       = date('Y-m-d H:i:s');
                $queryData['updated_at']       = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($queryData);
                $query = "INSERT INTO utility_billing (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($queryData);

                $queryData1['user_id']          =  $data['user_id'][$i];
                $queryData1['charge_code']      =  $data['utility'][$i];
                $queryData1['start_date']       =  $firstDayNextMonth;
                $queryData1['end_date']         =  $tenantData['end_date'];
                $queryData1['amount']           =  $data['utility_amount'][$i];
                $queryData1['status']           =  'Pending';
                $queryData1['record_status']    =  '1';
                $queryData1['created_at']       = date('Y-m-d H:i:s');
                $queryData1['updated_at']       = date('Y-m-d H:i:s');

                $sqlData1 = createSqlColVal($queryData1);
                $query1 = "INSERT INTO tenant_charges (".$sqlData1['columns'].") VALUES (".$sqlData1['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query1);
                $stmt->execute($queryData1);

            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Records created successfully');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function editUtilityBills() {
        try {
            $data = $_POST;
            $utility_bill_id               =  $data['utility_bill_id'];

            $queryData['start_date']       =  mySqlDateFormat($data['start_date'], null, $this->companyConnection);
            $queryData['end_date']         =  mySqlDateFormat($data['end_date'], null, $this->companyConnection);
            $queryData['utility_amount']   =  $data['edit_utility_amount'];
            $queryData['updated_at']       =  date('Y-m-d H:i:s');

            $sqlData = createSqlColValPair($queryData);
            $query = "UPDATE utility_billing SET ".$sqlData['columnsValuesPair']." where id=".$utility_bill_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            $queryData1['start_date']       =  mySqlDateFormat($data['start_date'], null, $this->companyConnection);
            $queryData1['end_date']         =  mySqlDateFormat($data['end_date'], null, $this->companyConnection);
            $queryData1['amount']           =  $data['edit_utility_amount'];
            $queryData1['updated_at']       =  date('Y-m-d H:i:s');

            $sqlData1 = createSqlColValPair($queryData1);
            $query1 = "UPDATE tenant_charges SET ".$sqlData1['columnsValuesPair']." where id=".$utility_bill_id;
            $stmt = $this->companyConnection->prepare($query1);
            $stmt->execute();

            return array('code' => 200, 'status' => 'success', 'message' => 'Records created successfully');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * Get announcement by id
     * @return array
     */
    public function delete(){
        try {
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM utility_billing WHERE id = '$id'");
            $utilityData = $query->fetch();
            if($utilityData) {
                $update_data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($update_data);
                $query = "UPDATE utility_billing SET ".$sqlData['columnsValuesPair']." where id='$id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('status' => 'success', 'data' => $utilityData, 'code' => 200);
            }
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getDataByIdUtility(){
        try {
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM utility_billing WHERE id = '$id'");
            $utilityData = $query->fetch();
            if ($utilityData){
                $utilityData['start_date'] = dateFormatUser($utilityData['start_date'], null, $this->companyConnection);
                $utilityData['end_date'] = dateFormatUser($utilityData['end_date'], null, $this->companyConnection);
            }
            return array('status' => 'success', 'data' => $utilityData, 'code' => 200, 'message' => 'Record fetched successfully.');
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


}
$utilityBilling = new utilityBilling();