<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
error_reporting(E_ALL);
ini_set("display_errors",1);
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class processManagementFee extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Common function to get data by id
     * @return array
     */
    public function getDataByID(){
        try {
            $id = $_POST['id'];
            $prev_data = $_POST['prev_data'];
            $data_type = $_POST['data_type'];
            $extra_array = isset($_POST['extra_array']) && !empty($_POST['extra_array']) ? $_POST['extra_array'] : '';
            $module_type = isset($_POST['module_type']) && !empty($_POST['module_type']) ? $_POST['module_type'] : '';
            $userData = '';
            $table = '';
            switch ($data_type) {
                case "get_vendor_address":
                    $query1 = "SELECT id, name, address1, address2, address3, address4, zipcode, city, state, country FROM users WHERE id = ".$id;
                    $data['vendor_data'] = $this->companyConnection->query($query1)->fetch();
                    break;
                case "all_PMC_vendors":
                    $query1 = "SELECT u.id, u.name FROM users u JOIN vendor_additional_detail vad ON vad.vendor_id = u.id WHERE u.deleted_at IS NULL AND u.status = '1' AND vad.is_pmc = '1'";
                    $data['vendor_list'] = $this->companyConnection->query($query1)->fetchAll();
                    break;
                case "all_properties_list_run_management":
                    $process_management_fee_date = $prev_data;
                    $current_date = '01-'.$process_management_fee_date;
                    $current_date = date('Y-m-d', strtotime($current_date));
                    $start_date = date('Y-m-01', strtotime($current_date));
                    $end_date = date('Y-m-t', strtotime($current_date));

                    $query1 = "SELECT * FROM process_management_fee WHERE processed_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' AND paid_status = 1";
                    $already_paid_data = $this->companyConnection->query($query1)->fetchAll();
//                    dd($exist_data);
                    if ($module_type != 'paid_fee') {
                        if (!empty($already_paid_data)) {
                            return array('code' => 201, 'status' => 'success', 'message' => 'Process Management Fee for this month is already paid!');
                        }
                    }
                    if (!empty($extra_array)) {
                        $query1 = "SELECT gp.id as gp_prop_id, gp.property_name, mf.* FROM general_property gp LEFT JOIN management_fees mf ON mf.property_id = gp.id WHERE gp.deleted_at IS NULL AND gp.status = '1' AND gp.created_at <= '" . $current_date . "' AND gp.id IN ( $extra_array ) ORDER BY  gp.property_name";
                    } else{
                        $query1 = "SELECT gp.id as gp_prop_id, gp.property_name, mf.* FROM general_property gp LEFT JOIN management_fees mf ON mf.property_id = gp.id WHERE gp.deleted_at IS NULL AND gp.status = '1' AND gp.created_at <= '" . $current_date . "' ORDER BY  gp.property_name";
                    }
                    $data['property_listing'] = $this->companyConnection->query($query1)->fetchAll();
                    foreach ($data['property_listing'] as $key => $value){
                        $property_id = $value['gp_prop_id'];
                        $query1 = "SELECT * FROM process_management_fee WHERE property_id = $property_id AND processed_date BETWEEN '".$start_date."' AND '".$end_date."'";
                        $exist_data = $this->companyConnection->query($query1)->fetch();
                        $data['property_listing'][$key]['process_management_fee'] = $exist_data['process_management_fee'];
                    }
                    break;
                case "all_property_listing":
                    $query1 = "SELECT gp.id, gp.property_id, gp.property_name FROM general_property gp LEFT JOIN management_fees mf ON mf.property_id = gp.id WHERE gp.deleted_at IS NULL ORDER BY gp.property_name";
                    $data['property_listing'] = $this->companyConnection->query($query1)->fetchAll();
                    break;
                case "all_property_listing_ddl":
                    $query1 = "SELECT gp.id, gp.property_id, gp.property_name FROM general_property gp WHERE gp.status = '1' AND gp.deleted_at IS NULL ORDER BY gp.property_name";
                    $data['property_listing'] = $this->companyConnection->query($query1)->fetchAll();
                    break;
                case "all_bank_accounts":
                    $extra_array = $_POST['extra_array'];
                    $bank_name_data = [];
                    foreach ($extra_array as $key => $val){
                        $bank_name_data[$key]['property_id'] = $val;
                        $bank_name_data[$key]['bank_name'] = property_default_bank_account($this->companyConnection,$val);
                        $bank_name_data[$key]['property_amount'] = $prev_data[$key];
                    }
                    $data = $bank_name_data;
                    break;
                default:
                    $table = '';
                    $query = '';
                    break;
            }

            if (!empty($query)) {
                $data = $this->companyConnection->query($query)->fetchAll();
            }
            return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Data loaded successfully!');
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    public function updateProcessManagement(){
        try {
            $data = postArray($_POST['data']);
            $property_id = $_POST['property_id'];
//            dd($data);

            $sqlData['management_type'] = $data['management_type'];
            if ($sqlData['management_type'] == 'Flat') {
                $sqlData['management_value'] = $data['management_value_flat'];
            } else {
                $sqlData['management_value'] = $data['management_value_percent'];
            }
            $sqlData['minimum_management_fee'] = ($sqlData['management_type'] == 'Percentage and a min. set fee') ? $data['minimum_management_fee'] : '';


            foreach ($property_id as $key => $value){
                $getDataQuery = "SELECT * FROM management_fees where property_id=".$value;
                $alreadyExistData = $this->companyConnection->query($getDataQuery)->fetch();
                if (empty($alreadyExistData) ){
                    $sqlData['property_id'] = $value;
                    $sqlData['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData['created_at'] = date('Y-m-d H:i:s');
                    $sqlQueryData = createSqlColVal($sqlData);
                    $query = "INSERT INTO management_fees(" . $sqlQueryData['columns'] . ") VALUES (" . $sqlQueryData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData);
                } else {
                    $sqlData['updated_at'] = date('Y-m-d H:i:s');
                    $sqlDataQ = createSqlColValPair($sqlData);
                    $query = "UPDATE management_fees SET ".$sqlDataQ['columnsValuesPair']." where property_id= $value";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                }

            }
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records updated successfully.');
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    /**
     * Save run management fee for properties
     * @return array|void
     */
    public function saveRunManagementFee(){
        try {
            $data = isset($_POST['data']) ? $_POST['data'] : '';
            $process_management_fee_date = $_POST['process_management_fee_date'];
            $process_management_fee_date = '01-'.$process_management_fee_date;
            $start_date = date('Y-m-01', strtotime($process_management_fee_date));

            $end_date = date('Y-m-t', strtotime($process_management_fee_date));

            if ($data) {
                $query1 = "SELECT * FROM process_management_fee WHERE processed_date BETWEEN '".$start_date."' AND '".$end_date."'";
                $exist_data = $this->companyConnection->query($query1)->fetchAll();

                foreach ($data as $key => $value) {
                    $sqlData['property_id']             = $value['property_id'];
                    $sqlData['income']                  = $value['income'];
                    $sqlData['process_management_fee']  = $value['fee'];
                    $sqlData['processed_date']          = date('Y-m-10 H:i:s', strtotime($process_management_fee_date));
                    $sqlData['paid_status']             = 0;
                    $sqlData['created_at']              = date('Y-m-d H:i:s');
                    $sqlData['updated_at']              = date('Y-m-d H:i:s');
                    $start_date = date('Y-m-01', strtotime($process_management_fee_date));

                    if (!empty($exist_data)){
                        $sqlQueryData = createSqlColValPair($sqlData);
                        $query = "UPDATE process_management_fee SET ".$sqlQueryData['columnsValuesPair']." where property_id=".$value['property_id']." AND processed_date BETWEEN '".$start_date."' AND '".$end_date."' AND paid_status = 0";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                    } else {
                        $sqlQueryData = createSqlColVal($sqlData);
                        $query = "INSERT INTO process_management_fee(" . $sqlQueryData['columns'] . ") VALUES (" . $sqlQueryData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($sqlData);
                    }
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records updated successfully.');
            } else {
                return;
            }

        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    /**
     * Update PMC vendor's address
     * @return array
     */
    public function updatePMCAddress(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);

            $sqlData['zipcode']        = $data['zipcode'];
            $sqlData['country']        = $data['country'];
            $sqlData['state']          = $data['state'];
            $sqlData['city']           = $data['city'];
            $sqlData['address1']       = $data['address1'];
            $sqlData['address2']       = $data['address2'];
            $sqlData['address3']       = $data['address3'];
            $sqlData['address4']       = $data['address4'];
            $sqlData['updated_at']     = date('Y-m-d H:i:s');


            $sqlQueryData = createSqlColValPair($sqlData);
            $query = "UPDATE users SET ".$sqlQueryData['columnsValuesPair']." where id=".$data['user_id'];
//            dd($data);
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record updated successfully.');
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    public function checkForReceivePayment(){
        try {
            $receivePaymentCheck = $this->checkReceivePayment();    /*this is to check if PMC Vendor is eligible for payment or not*/
            if($receivePaymentCheck['account_status'] == 'false') {
                return array('status'=>'false','message'=>'This PMC Vendor Account is not verified!');
            }
        } catch (Exception $e){
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function checkReceivePayment($user_id){
        try {
            $request = [];
            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $user_id = (isset($user_id) && !empty($user_id))? $user_id : $_POST['user_id'];
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $user_id], 'users');
            if(!empty($user_data["data"])){
//                dd($user_data["data"]);
                if(!empty($user_data["data"]["stripe_account_id"])) {
                    $account_id = $user_data["data"]["stripe_account_id"];
                    $request["account_id"] = $account_id;
                    $stripe_account_array["stripe_account_id"] = $account_id;
                    $connected_account_detail = getConnectedAccount($request);
                    if($connected_account_detail['account_data']["business_type"]=="company"){
                        $get_person_detail = getPerson($request);
                        $status = (isset($get_person_detail["person_detail"]["data"][0]["verification"]["status"]) && $get_person_detail["person_detail"]["data"][0]["verification"]["status"] == "verified"  && empty($connected_account_detail["account_data"]["company"]["requirements"]["current_deadline"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["currently_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["disabled_reason"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["pending_verification"]) ) ? 'Verified' : "false";
                    }else {
                        $status = (isset($connected_account_detail["account_data"]["individual"]["verification"]["status"]) && $connected_account_detail["account_data"]["individual"]["verification"]["status"] == "verified" && empty($connected_account_detail["account_data"]["individual"]["requirements"]["currently_due"]) && empty($connected_account_detail["account_data"]["individual"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["pending_verification"])) ? 'Verified' : "false";
                    }
                } else{
                    $connected_account_detail["account_data"] = '';
                    $status =  "false";
                }
            } else{
                $status =  "false";
                $connected_account_detail["account_data"] = '';
            }
            return array('code' => 200, 'account_status' => $status, 'status' => 'success','account_data'=>$connected_account_detail["account_data"],   'message' => 'Account status fetched successfully.');
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }


    public function createPaymentOnSubmit(){
        try {
//            dd($_POST);
            $data_array = $_POST['data'];
            $payment_type = $_POST['payment_type'];
            $total_amount_to_pay = parse_number($_POST['total_amount_to_pay']);

            if ($total_amount_to_pay > 0) {
                if ($_POST['payment_type'] == 'Check') {
                    $this->addTransactionData($data_array);
                } else {
                    $total_stripe_fee_amount_to_deduct = $total_amount_to_pay * (2.9 / 100);
                    $total_stripe_fee_amount_to_deduct = round($total_stripe_fee_amount_to_deduct, 2) + .30;
                    $total_amount_to_deduct = $total_amount_to_pay + $total_stripe_fee_amount_to_deduct;
                    $vendor_id = $_POST['vendor_id'];
                    $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];

                    $getAccountId = $this->companyConnection->query("SELECT stripe_account_id FROM users WHERE id =" . $vendor_id)->fetch();
                    $destination_account_id = $getAccountId['stripe_account_id'];

                    $receivePaymentCheck = $this->checkReceivePayment($vendor_id);    /*this is to check if PMC Vendor is eligible for payment or not*/
                    if($receivePaymentCheck['account_status'] == 'false') {
                        return array('status'=>'false','message'=>'This PMC Vendor Account is not verified!');
                    }

                    $getUserAccountData = $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id =" . $user_id)->fetch();
                    $source_customer_id = $getUserAccountData['stripe_customer_id'];

                    $customer_id["customer_id"] = $source_customer_id;
                    $customerData = getCustomer($customer_id);
                    $customer_source = $customerData['customer_data']->default_source;

                    if ($_POST['payment_type']  == 'Credit Card/Debit Card' || $_POST['payment_type']  == 'ACH') {
                        $getData = $this->companyConnection->query("SELECT stripe_customer_id FROM users where id='$user_id'")->fetch();
                        $customer_id = $getData['stripe_customer_id'];
                        if ($customer_id == "") {
                            return array('status' => 'false', 'message' => "Please add $payment_type Information first!");
                        }

                        $checkDefaultSource = $this->checkDefaultSource($customer_id, $payment_type);
                        if ($checkDefaultSource['status'] == 'false') {
                            return array('status' => 'false', 'message' => $checkDefaultSource['message']);
                        }
                    }

                    if (isset($destination_account_id) && !empty($destination_account_id)) {
                        $chargeData = [];
                        $chargeData['amount'] = $total_amount_to_deduct * 100;
                        $chargeData['currency'] = 'usd';
                        $chargeData['token'] = $customer_source;
                        $chargeData['customer_id'] = $source_customer_id;
                        $chargeData['destination_account_id'] = $destination_account_id;
                        $stripe_payment_response = createCharge($chargeData);
                        if ($stripe_payment_response['code'] == 200) {
                            $this->addTransactionData($data_array);
                        }
                    }
                }
            }
            return array('code' => 200, 'status' => 'true','message' => 'Payment has been done successfully...........');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function checkDefaultSource($customer_id,$payment_type){
        try {
            $customer = \Stripe\Customer::retrieve($customer_id);

            if($payment_type=='Credit Card/Debit Card') {
                $sources = $customer['default_source'];
                if (strpos($sources, 'card') === false) {
                    return array('status'=>'false','message'=>'Please set card as a default source');
                } else {
                    $card_id = $customer['default_source'];
                    $card_data = \Stripe\Customer::retrieveSource($customer_id, $card_id);
                    $card_data['exp_month'] = date("F", mktime(0, 0, 0, $card_data['exp_month'], 10));
                    return array('status'=>'true', 'data'=> $card_data, 'payment_type'=>'card');
                }
            } else if($payment_type=='ACH') {
                $sources = $customer['default_source'];
                if (strpos($sources, 'ba_') === false) {
                    return array('status'=>'false','message'=>'Please set ACH account as a default source');
                } else {
                    $ach_id = $customer['default_source'];
                    $ach_data = \Stripe\Customer::retrieveSource($customer_id, $ach_id);
                    return array('status'=>'true', 'data'=> $ach_data, 'payment_type'=>'ACH');
                }
            } else {
                return array('status'=>'true');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function addTransactionData($value){
        try{
//            dd($_POST);
            $data_array = $value;
            if ($_POST['payment_type'] == 'Cash'){
                $payment_type = 'cash';
//                $reference_no = $_POST['reference_number'];
            } else if ($_POST['payment_type'] == 'Check'){
                $payment_type = 'check';
            } else if ($_POST['payment_type'] == 'ACH'){
                $payment_type = 'ACH';
            } else {
                $payment_type = 'card';
//                $check_number = $_POST['check_number'];
            }

            /*Save data in transactions table*/
            foreach ($data_array as $key => $value) {
                $check_number = isset($value['check_number']) ? $value['check_number'] : randomTokenString(9);
                $data['auto_transaction_id'] = getAutoTransactionId($this->companyConnection);
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['receiver_id'] = $_POST['vendor_id'];
                $data['property_id'] = $value['property_id'];
                $data['user_type'] = 'PM';
                $data['amount'] = parse_number($value['fee']);
                $data['payment_type'] = $payment_type;
                $data['type'] = 'PMC_FEE';
//                $data['reference_no'] = $reference_no;
                $data['check_number'] = $check_number;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
            }
            /*Save data in transactions table*/

            /*Save update data in process management fee table*/
            $data = isset($_POST['data']) ? $_POST['data'] : '';
            $process_management_fee_date = $_POST['process_management_fee_date'];
            $process_management_fee_date = '01-'.$process_management_fee_date;
            $start_date = date('Y-m-01', strtotime($process_management_fee_date));

            $end_date = date('Y-m-t', strtotime($process_management_fee_date));

            if ($data) {
                $query1 = "SELECT * FROM process_management_fee WHERE processed_date BETWEEN '" . $start_date . "' AND '" . $end_date . "'";
                $exist_data = $this->companyConnection->query($query1)->fetchAll();
//                dd($exist_data);

                foreach ($data as $key => $value) {
                    $sqlData['property_id'] = $value['property_id'];
                    $sqlData['income'] = $value['income'];
                    $sqlData['process_management_fee'] = $value['fee'];
                    $sqlData['processed_date'] = date('Y-m-10 H:i:s', strtotime($process_management_fee_date));
                    $sqlData['paid_status'] = 1;
                    $sqlData['created_at'] = date('Y-m-d H:i:s');
                    $sqlData['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData['paid_date'] = date('Y-m-d H:i:s');
                    $start_date = date('Y-m-01', strtotime($process_management_fee_date));

                    if (!empty($exist_data)) {
                        $sqlQueryData = createSqlColValPair($sqlData);
                        $query = "UPDATE process_management_fee SET " . $sqlQueryData['columnsValuesPair'] . " where property_id=" . $value['property_id'] . " AND processed_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' AND paid_status = 0";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                    } else {
                        $sqlQueryData = createSqlColVal($sqlData);
                        $query = "INSERT INTO process_management_fee(" . $sqlQueryData['columns'] . ") VALUES (" . $sqlQueryData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($sqlData);
                    }
                }
            }
            /*Save update data in process management fee table*/
        }
        catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }

}
$processManagementFee = new processManagementFee();