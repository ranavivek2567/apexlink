<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class allocationOrder extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     * Update data of unit type
     * @return array
     */
    public function update()
    {
        try {
            $data = $_POST['form'];
            $data = postArray($data);

            $required_array = ['eft_generate_date'];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [ ];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }

                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['warning_past_status'] = isset($data['warning_past_status']) ? $data['warning_past_status'] : 0 ;
                $data['warning_future_status'] = isset($data['warning_future_status']) ? $data['warning_future_status'] : 0 ;

                $data['warn_transaction_days_past'] = (isset($data['warn_transaction_days_past']) && ($data['warn_transaction_days_past'] == 0)) ? '' : $data['warn_transaction_days_past'] ;
                $data['warn_transaction_days_future'] = (isset($data['warn_transaction_days_future']) && ($data['warn_transaction_days_future'] ==0)) ? '' : $data['warn_transaction_days_future']  ;

                $query = $this->companyConnection->query("SELECT * FROM company_accounting_preferences");
                $checkRecordExistOrNot = $query->fetch();

                if(empty($checkRecordExistOrNot)){
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    //Save Data in Company Database
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO company_accounting_preferences (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');
                } else {
                    $edit_id = $checkRecordExistOrNot['id'];
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    //Update Data in Company Database
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE company_accounting_preferences SET ".$sqlData['columnsValuesPair']." where id='$edit_id'";
                    $stmt =$this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record updated successfully.');
                }

            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function updatePriorityOfChargeCode(){

        try {
//            dd($_POST); die();
            if(isset($_POST['priority']) && !empty($_POST['priority'])) {
                foreach ($_POST['priority'] as $key => $value) {
                    $sql = "UPDATE company_accounting_charge_code SET priority=? WHERE id=?";
                    $stmt = $this->companyConnection->prepare($sql);
                    $stmt->execute([$key + 1, $value]);
                }
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully.');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }
    /**
     * function for fetching account preference data for edit
     * @return array
     */
    public function chargeCodeList(){

        try {
            $query = $this->companyConnection->query("SELECT * FROM company_accounting_charge_code WHERE status = '1' ORDER BY Priority ASC");
            $chargeCodeData =  $query->fetchAll();
            return array('code' => 200, 'status' => 'success', 'data' => $chargeCodeData, 'message' => 'Record fetched successfully.');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }


}

$allocationOrder = new allocationOrder();