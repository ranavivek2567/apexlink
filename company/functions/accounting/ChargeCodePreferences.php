<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class ChargeCodePreferencesAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     * Insert data to company charge code preferences table
     * @return array
     */
    public function insert()
    {
        try {
            $data = $_POST['form'];
            $required_array = ['charge_code_type','charge_code','frequency'];
            /*Max length variable array*/
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/

            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            /*Checking server side validation*/
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['charge_code_pref_id'])){
                    unset($data['charge_code_pref_id']);
                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['status'] = 1;
                $data['is_editable'] = 1;
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];

                if($data['is_default'] == 1){
                    $update_data['is_default'] = 0;
                    $sqlData = createSqlColValPair($update_data);
                    $query = "UPDATE company_accounting_charge_code_preferences SET ".$sqlData['columnsValuesPair'];
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                }

                /*Save Data in Company Database*/
                if(isset($data['unit_type_id']) && !empty($data['unit_type_id'])){
                    foreach ($data['unit_type_id'] as $key => $val){
                        $data['unit_type_id'] = $val;
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO company_accounting_charge_code_preferences (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                    }
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record added successfully.');
                } else {
                    if(isset($data['is_default']) && $data['is_default'] == 1){
                        $update_data['is_default'] = 0;
                        $sqlData = createSqlColValPair($update_data);
                        $query = "UPDATE company_accounting_charge_code_preferences SET ".$sqlData['columnsValuesPair'];
                        $stmt1 =$this->companyConnection->prepare($query);
                        $stmt1->execute();
                    }

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO company_accounting_charge_code_preferences (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record added successfully.');
                }
            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update data of charge code preferences
     * @return array
     */
    public function update()
    {
        try {
            $data = $_POST['form'];

            $charge_code_pref_id = $data['charge_code_pref_id'];
            /*Required variable array*/
            $required_array = ['charge_code_type','charge_code','frequency'];
            /*Max length variable array*/
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            /*Checking server side validation*/
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['charge_code_pref_id']) && !empty($data['charge_code_pref_id'])){
                    unset($data['charge_code_pref_id']);
                }

                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];

                if(isset($data['unit_type_id']) && !empty($data['unit_type_id'])){
                    $oldData = $this->view($charge_code_pref_id);
                    foreach ($oldData['AllChargeCode'] as $key => $val){
                        $deleted_at = date('Y-m-d H:i:s');
                        $sql = "UPDATE company_accounting_charge_code_preferences SET deleted_at=? WHERE id=?";
                        $stmt= $this->companyConnection->prepare($sql);
                        $stmt->execute([$deleted_at,$val['id']]);
                    }
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['status'] = 1;
                    $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    foreach ($data['unit_type_id'] as $key => $val){
                        $data['unit_type_id'] = $val;
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO company_accounting_charge_code_preferences (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                    }
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record unit updated successfully.');
                } else {
                    if(isset($data['is_default']) && $data['is_default'] == 1){
                        $update_data['is_default'] = 0;
                        $sqlData = createSqlColValPair($update_data);
                        $query = "UPDATE company_accounting_charge_code_preferences SET ".$sqlData['columnsValuesPair'];
                        $stmt1 =$this->companyConnection->prepare($query);
                        $stmt1->execute();
                    }

                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE company_accounting_charge_code_preferences SET ".$sqlData['columnsValuesPair']." where id='$charge_code_pref_id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record type updated successfully.');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for fetching charge code preferences data
     * @return array
     */
    public function view($charge_code_pref_id = null){

        try {
            if (isset($charge_code_pref_id) && !empty($charge_code_pref_id)){
                $id = $charge_code_pref_id;
            } else {
                $id = $_POST['id'];
            }
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            if(isset($login_user_id) && !empty($login_user_id))
            {
                $query = $this->companyConnection->query("SELECT * FROM company_accounting_charge_code_preferences WHERE id='$id'");
                $data = $query->fetch();
                $chargeCodeId = $data['charge_code'];
                $chargeCodeType = $data['charge_code_type'];
                $frequency = $data['frequency'];
                $query = $this->companyConnection->query("SELECT * FROM company_accounting_charge_code_preferences WHERE charge_code='".$chargeCodeId."' AND charge_code_type='".$chargeCodeType."' AND frequency='".$frequency."' AND status != '2' AND status != '0' AND deleted_at IS NULL");
                $checkExistingChargeCode = $query->fetchAll();

//                dd($checkExistingChargeCode);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'AllChargeCode'=> $checkExistingChargeCode,'message' => 'The record fetched successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }

    /**
     * Update Charge Code Preferences Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];
            $updated_at = date('Y-m-d H:i:s');

            $checkSetDefault = $this->companyConnection->prepare("SELECT * FROM company_accounting_charge_code_preferences WHERE id=?");
            $checkSetDefault->execute([$id]);
            $checkSetDefault = $checkSetDefault->fetch();

            if(isset($checkSetDefault) || !empty($checkSetDefault)){
                $isDefault = $checkSetDefault['is_default'];

                if($isDefault == 1 && $status == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'A default value cannot be deactivated.');
                }
            }
            $sql = "UPDATE company_accounting_charge_code_preferences SET status=?, updated_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $updated_at, $id]);

            if($status == '1'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Record activated successfully.';
            }
            if($status == '0'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Record deactivated successfully.';
            }

            if($stmt){
                return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $_SESSION[SESSION_DOMAIN]["status_message"]);
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Delete charge code preferences
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE company_accounting_charge_code_preferences SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * cahrgecode list data
     */
    public  function getAllChargeCodeList(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM company_accounting_charge_code WHERE status = '1' ORDER BY charge_code ASC");
            $data = $query->fetchAll();
        // print_r($data);
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Unittype list data
     */
    public  function getAllUnittypeList(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM company_unit_type WHERE status = '1'");
            $data = $query->fetchAll();
            // print_r($data);
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }


    /**
     * Restore trashed records
     * @return array
     */
    public function restoreChargeCodePreference()
    {
        try {
            $id = $_POST['data_id'];
            $data = NULL;
            $sql = "UPDATE company_accounting_charge_code_preferences SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record restored successfully.');
        } catch (PDOException $e) {
            return $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
}

$ChargeCodePreferencesAjax = new ChargeCodePreferencesAjax();