<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class RecurringChecks extends DBConnection
{


    /**
     * Invoice constructor.
     */
    public function __construct()
    {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function add(){
        try{
            unset($_POST['action']);
            unset($_POST['class']);
            $check_data = $_POST;
            //  print_r($check_data);
            $check_data['date'] = empty($_POST['Date'])?NULL:mySqlDateFormat($_POST['Date'],null,$this->companyConnection);

            $check_data["user_id"] = (isset($check_data['payOrderOff']) && !empty($check_data['payOrderOff'])? $check_data['payOrderOff'] :"");
            $check_data["other_address"] = (isset($check_data['address']) && !empty($check_data['address'])? $check_data['address'] :"");
            $check_data["amount"] = (isset($check_data['afn']) && !empty($check_data['afn'])? $check_data['afn'] :"");

            unset($check_data['Date']);
            unset($check_data['payOrderOff']);
            unset($check_data['address']);
            unset($check_data['afn']);

            if(!empty($check_data["user_id"]))
                $check_data["other_name"] = '';

            if(!empty($check_data["other_name"]))
                $check_data["user_id"] = '';

                $sqlData = createSqlColVal($check_data);
            $query = "INSERT INTO accounting_banking (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($check_data);
            return array('code' => 200, 'status' => 'success','message' => 'Record added successfully.');
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    public function view(){
        try{
            $id =!empty($_POST['id'])?$_POST['id']:'';
            $data = $this->companyConnection->query("SELECT ab.*,cba.bank_name,gp.property_name,users.name FROM general_property gp,company_accounting_bank_account cba,`accounting_banking` ab LEFT JOIN users ON ab.user_id = users.id WHERE gp.id=ab.property_id AND cba.id=ab.bank_id AND ab.id=$id")->fetch();
            return array('data' => $data, 'status' => 'success','code' => 200);
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function delete(){
        try{
            $id = !empty($_POST['id'])?$_POST['id']:'';
            $date = date('Y-m-d H:i:s');
            $query = "UPDATE `accounting_banking` set deleted_at = '$date'  WHERE id = $id";
            $stmt = $this->companyConnection->prepare($query);
            $recurr_delete  = $stmt->execute();
            $return = array('code' => 200, 'status' => 'success','data' => $recurr_delete, 'message' => 'Recurring check Record delete successfully.');
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function update(){
        try{
            unset($_POST['action']);
            unset($_POST['class']);
            $id = !empty($_POST['check_id'])?$_POST['check_id']:'';
            $data = $_POST;
            unset($data['check_id']);
            $data['date'] = empty($_POST['Date'])?NULL:mySqlDateFormat($_POST['Date'],null,$this->companyConnection);
            $data["user_id"] = (isset($data['payOrderOff']) && !empty($data['payOrderOff'])? $data['payOrderOff'] :"");
            $data["other_address"] = (isset($data['address']) && !empty($data['address'])? $data['address'] :"");
            $data["amount"] = (isset($data['afn']) && !empty($data['afn'])? $data['afn'] :"");
            $data["property_id"] = (isset($data['property_id']) && !empty($data['property_id'])? $data['property_id'] :"");
            $data["building_id"] = (isset($data['building_id']) && !empty($data['building_id'])? $data['building_id'] :"");
            $data["unit_id"] = (isset($data['unit_id']) && !empty($data['unit_id'])? $data['unit_id'] :"");
            $data["bank_id"] = (isset($data['bank_id']) && !empty($data['bank_id'])? $data['bank_id'] :"");
            $data["frequency"] = (isset($data['frequency']) && !empty($data['frequency'])? $data['frequency'] :"");
            $data["endingbalance"] = (isset($data['endingbalance']) && !empty($data['endingbalance'])? $data['endingbalance'] :"");
            $data["duration"] = (isset($data['duration']) && !empty($data['duration'])? $data['duration'] :"");
            $data["userType"] = (isset($data['userType']) && !empty($data['userType'])? $data['userType'] :"");
            $data["memo"] = (isset($data['memo']) && !empty($data['memo'])? $data['memo'] :"");
            //print_r($check_data["user_id"]); die;
            if(!empty($data["user_id"]))
                $data["other_name"] = '';

            if(!empty($data["other_name"]))
                $data["user_id"] = '';
            unset($data['Date']);
            unset($data['payOrderOff']);
            unset($data['address']);
            unset($data['afn']);

            $sqlData = createSqlColValPair($data);
            $query = "UPDATE accounting_banking SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success','message' => 'Record updated successfully.');
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getTenantAddress(){
        try{
            unset($_POST['RecurringChecks']);
            unset($_POST['getTenantAddress']);
            $property_id = $_POST['property_id'];
            $getdata = $this->companyConnection->query("SELECT address1,address2,address3,address4,state,city,zipcode,country FROM general_property where id = '$property_id'")->fetch();
         // print_r($getdata); die;
            return array('code' => 200, 'status' => 'success', 'data' =>  $getdata, 'message' => 'Address get successfully.');
        } catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
}

$RecurringChecks = new RecurringChecks();