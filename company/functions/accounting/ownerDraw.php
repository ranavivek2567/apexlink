<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class ownerDraw extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getOwnerDataForComboGrid() {
        try {
            $search = @$_REQUEST['q'];
            $sql = "SELECT * FROM users  where user_type='4' AND status = '1' and first_name LIKE '%$search%' order by name asc";
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);
            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['name'];
                    $data['rows'][$key]['company_name'] = $user_data['company_name'];
                    $data['rows'][$key]['phone_number'] = $user_data['phone_number'];
                    $data['rows'][$key]['address1'] = $user_data['address1'] . ', ' . $user_data['address2'] . ' ' . $user_data['address3'];
                    $data['rows'][$key]['email'] = $user_data['email'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }


    /**
     * Common function to get data by id
     * @return array
     */
    public function getDataByID(){
        $id = $_POST['id'];
        $prev_id = $_POST['prev_id'];
        $data_type = $_POST['data_type'];
        $userData = '';
        $table = '';
        switch ($data_type) {
            case "owner":
                $query1 = "SELECT property_id FROM owner_property_owned WHERE user_id = ".$id;
                $propertyData = $this->companyConnection->query($query1)->fetchAll();
                $data = [];
                foreach ($propertyData as $key=>$value){
                    if(!empty($value['property_id'])){
                        $data['portfolio_data'][$key] = $this->companyConnection->query("SELECT DISTINCT cpp.id, cpp.portfolio_name FROM general_property gp JOIN company_property_portfolio cpp ON gp.portfolio_id = cpp.id WHERE gp.id = ".$value['property_id'])->fetch();
                    }
                }

                $outputArray = array();
                $outputArray2 = array();

                foreach($data['portfolio_data'] as $inputArrayItem) {
                    foreach($outputArray as $outputArrayItem) {
                        if($inputArrayItem['id'] == $outputArrayItem) {
                            continue 2;
                        }
                    }
                    $outputArray[] = $inputArrayItem['id'];
                    $outputArray2[] = $inputArrayItem;
                }
                $data['portfolio_data'] = $outputArray2;

                break;
            case "portfolio":
                $owner_id = $prev_id;
                $query1 = "SELECT property_id FROM owner_property_owned WHERE user_id = ".$owner_id;
                $propertyData = $this->companyConnection->query($query1)->fetchAll();

                foreach ($propertyData as $key=>$value){
                    if(!empty($value['property_id'])){
                        $property_data = $this->companyConnection->query("SELECT gp.id, gp.property_name FROM general_property gp JOIN company_property_portfolio cpp ON gp.portfolio_id = cpp.id WHERE cpp.id = $id AND gp.id = ".$value['property_id'])->fetch();
                        $data['property_data'][$key] = $property_data;
                    }
                }
//                    dd($data);
                break;
            case "property":
                $query1 = "SELECT caba.id, caba.bank_name, caba.bank_account_number FROM property_bank_details pbd JOIN company_accounting_bank_account caba ON pbd.account_name = caba.id WHERE pbd.property_id = $id";
                $data['bank_account_data'] = $this->companyConnection->query($query1)->fetchAll();
                break;
            case "all_owner_listing":
                $query1 = "SELECT u.id, u.name FROM users u  JOIN owner_details od ON od.user_id = u.id WHERE u.user_type = '4' AND od.deleted_at IS NULL AND od.status = '1' ORDER BY u.name";
                $data['owner_listing'] = $this->companyConnection->query($query1)->fetchAll();
                break;
            case "all_property_listing":
                $query1 = "SELECT id,property_id, property_name FROM general_property WHERE status = 1 AND deleted_at IS NULL ORDER BY property_name";
                $data['property_listing'] = $this->companyConnection->query($query1)->fetchAll();
                break;
            default:
                $table = '';
                $query = '';
                break;
        }

        if (!empty($query)) {
            $data = $this->companyConnection->query($query)->fetchAll();
        }
        return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Data loaded successfully!');
    }


    public function checkForPayment(){
        try {
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
//            $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
            $payment_type = $_POST['payment_type'];

//            dd($payment_type);
            if ($payment_type == 'Check') {
                return;
            }
          /*  if ($payment_type == 'Check') {
                $request=[];
                $getData = $this->companyConnection->query("SELECT stripe_customer_id FROM users where id=".$_SESSION[SESSION_DOMAIN]['cuser_id'])->fetch();
                $request["customer_id"] = $getData['stripe_customer_id'];
//                dd($request);
                if(empty($request["customer_id"])){
                    $getDetailUser= $this->createCustomer($_SESSION[SESSION_DOMAIN]['cuser_id']);
                    $request["customer_id"] = $getDetailUser;
                    $getDetailUser= getCustomer($request);
                }else{
                    $getDetailUser= getCustomer($request);
                }
//                dd($getDetailUser);
                $sources = $getDetailUser['customer_data']['default_source'];
//                dd($sources);
                if (empty($sources)){
                    return array('status' => 'false', 'message' => "Please set payment details first!");
                } else {
                    return array('code'=>200, 'status' => 'true', 'message' => "Payment mode already exist!");
                }
            }*/

            if ($payment_type == 'Credit Card/Debit Card' || $payment_type == 'ACH') {
                $getData = $this->companyConnection->query("SELECT stripe_customer_id FROM users where id='$user_id'")->fetch();
                $customer_id = $getData['stripe_customer_id'];
                if ($customer_id == "") {
                    return array('status' => 'false', 'message' => "Please add $payment_type Information first!");
                }

                $checkDefaultSource = $this->checkDefaultSource($customer_id, $payment_type);
                if ($checkDefaultSource['status'] == 'false') {
                    return array('status' => 'false', 'message' => $checkDefaultSource['message']);
                } else {
                    return array('status' => 'true', 'customer_id' => $customer_id, 'payment_type'=>$checkDefaultSource['payment_type'], 'data'=>$checkDefaultSource['data']);
                }
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }
    public function createCustomer($user_id){
        try {
            $getData =  $this->companyConnection->query("SELECT stripe_customer_id,email FROM users WHERE id ='" . $user_id . "'")->fetch();
            $email = $getData['email'];
            if($getData['stripe_customer_id']=='') {
                $customer    =     \Stripe\Customer::create(array(
                    'email' => $email
                ));
                $customer_id =        $customer['id'];
                $updateData =  "UPDATE users SET stripe_customer_id='$customer_id' where id='$user_id'";
                $stmt = $this->companyConnection->prepare($updateData);
                $stmt->execute();
                return $customer_id;
            } else {
                return $getData['stripe_customer_id'];
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function checkDefaultSource($customer_id,$payment_type){
        try {
            $customer = \Stripe\Customer::retrieve($customer_id);

            if($payment_type=='Credit Card/Debit Card') {
                $sources = $customer['default_source'];
                if (strpos($sources, 'card') === false) {
                    return array('status'=>'false','message'=>'Please set card as a default source');
                } else {
                    $card_id = $customer['default_source'];
                    $card_data = \Stripe\Customer::retrieveSource($customer_id, $card_id);
                    $card_data['exp_month'] = date("F", mktime(0, 0, 0, $card_data['exp_month'], 10));
                    return array('status'=>'true', 'data'=> $card_data, 'payment_type'=>'card');
                }
            } else if($payment_type=='ACH') {
                $sources = $customer['default_source'];
                if (strpos($sources, 'ba_') === false) {
                    return array('status'=>'false','message'=>'Please set ACH account as a default source');
                } else {
                    $ach_id = $customer['default_source'];
                    $ach_data = \Stripe\Customer::retrieveSource($customer_id, $ach_id);
//                dd($ach_data);
                    return array('status'=>'true', 'data'=> $ach_data, 'payment_type'=>'ACH');
                }
            } else {
                return array('status'=>'true');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function createPaymentOnSubmit(){
        try {
            $data_array = $_POST['data_array'];
            foreach ($data_array as $key => $val) {

                $paid_amount = parse_number($val['owner_draw_amount']);
//                if ($paid_amount > 0) {
                    if ($_POST['payment_type'] == 'Check' || $_POST['payment_type'] == 'Cash') {
                        $this->addTransactionData($val);
                    } else {
                        $total_stripe_fee_amount_to_deduct = $paid_amount * (2.9 / 100);
                        $total_stripe_fee_amount_to_deduct = round($total_stripe_fee_amount_to_deduct, 2) + .30;
                        $total_amount_to_deduct = $paid_amount + $total_stripe_fee_amount_to_deduct;
                        $owner_id = $val['owner_id'];
                        $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                        $getUserAccountData = $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id =" . $user_id)->fetch();
                        $source_customer_id = $getUserAccountData['stripe_customer_id'];

                        $customer_id["customer_id"] = $source_customer_id;
                        $customerData = getCustomer($customer_id);
                        $customer_source = $customerData['customer_data']->default_source;
//                        dd($customer_source);
                        $getAccountId = $this->companyConnection->query("SELECT stripe_account_id FROM users WHERE id =" . $owner_id)->fetch();
                        $destination_account_id = $getAccountId['stripe_account_id'];

                        if (isset($destination_account_id) && !empty($destination_account_id)) {
                            $chargeData = [];
                            $chargeData['amount'] = $total_amount_to_deduct * 100;
                            $chargeData['currency'] = 'usd';
                            $chargeData['token'] = $customer_source;
                            $chargeData['customer_id'] = $source_customer_id;
                            $chargeData['destination_account_id'] = $destination_account_id;
                            $stripe_payment_response = createCharge($chargeData);
                            if ($stripe_payment_response['code'] == 200) {
                                $this->addTransactionData($val);
                            }
                        }
                    }
//                }
            }
            return array('code' => 200, 'status' => 'true','message' => 'Payment has been done successfully...........');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function addTransactionData($value){
        try{
//            dd($value);
            $check_number = '';
            $data_array = $_POST['data_array'];
            if ($_POST['payment_type'] == 'Cash'){
                $payment_type = 'cash';
//                $reference_no = $_POST['reference_number'];
            } else if ($_POST['payment_type'] == 'Check'){
                $check_number = isset($value['check_number']) ? $value['check_number'] : randomTokenString(9);
                $payment_type = 'check';
            } else if ($_POST['payment_type'] == 'ACH'){
                $payment_type = 'ACH';
            } else {
                $payment_type = 'card';
//                $check_number = $_POST['check_number'];
            }

            /*Save data in transactions table*/
//            foreach ($data_array as $key => $value) {
                $data['auto_transaction_id'] = getAutoTransactionId($this->companyConnection);
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['receiver_id'] = $value['owner_id'];
                $data['property_id'] = $value['property_id'];
                $data['user_type'] = 'OWNER';
                $data['amount'] = parse_number($value['owner_draw_amount']);
                $data['payment_type'] = $payment_type;
                $data['type'] = 'OWNER_DRAW';
                $data['bank_id'] = $value['bank_name_id'];
                $data['check_number'] = $check_number;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                /*Save data in transactions table*/

                /*Save data in owner_draw table*/
                $draw_data['owner_id'] = $value['owner_id'];
                $draw_data['property_id'] = $value['property_id'];
//                $draw_data['account_number_id'] = $value['account_number_id'];
                $draw_data['chart_of_account_id'] = 43;                     //In 'company_chart_of_accounts' table Owner Draw id is 43
                $draw_data['amount'] = $value['owner_draw_amount'];
                $draw_data['transaction_type'] = $_POST['payment_type'];
                $draw_data['created_at'] = date('Y-m-d H:i:s');
                $draw_data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData1 = createSqlColVal($draw_data);
                $qry = "INSERT INTO `owner_draw` (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($qry);
                $stmt1->execute($draw_data);
//            }
            /*Save data in owner_draw table*/
        }
        catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    public function createCharge($customer_id,$account_id,$user_id,$chargeType){
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        try {
            $paid_amount = $_POST['total_amount'];
            $paid_amount = parse_number($paid_amount);

            if ($customer_id == "") {
                return array('status' => 'false', 'message' => "Stripe account not created for this user!");
            }
//            dd($account_id);
            $property_id = $_POST['property_id'];
            $bank_id = getBankSourceId($this->companyConnection, $property_id);
            if ($bank_id == ''){
                $bank_id = $account_id;
            }
            $charge = \Stripe\Charge::create([
                "amount" => $paid_amount * 100,
                "currency" => 'usd',
                "customer"=> $customer_id,
                "destination" => array("account"  => $bank_id)
            ]);

            $charge_id = $charge['id'];
            $transaction_id = $charge['balance_transaction'];
            $stripe_status  = $charge['status'];
            $payment_response  = 'Payment has been done successfully.';

            if ($stripe_status == 'succeeded'){
                $contribution_data['date'] = mySqlDateFormat($_POST['contribution_date'],null,$this->companyConnection);
//                $contribution_data['date'] = $_POST['contribution_date'];
                $contribution_data['owner_id'] = $user_id;
                $contribution_data['portfolio_id'] = $_POST['portfolio_id'];
                $contribution_data['property_id'] = $_POST['property_id'];
                $contribution_data['bank_account_id'] = $_POST['bank_account'];
                $contribution_data['chart_of_account_id'] = $_POST['chart_of_account_id'];
                $contribution_data['amount'] = $_POST['total_amount'];
                $contribution_data['transation_type'] = $_POST['transaction_type'];
                $contribution_data['check_ref'] = $_POST['reference_number'];
                $contribution_data['remarks'] = $_POST['remarks'];
                $contribution_data['created_at'] = date('Y-m-d H:i:s');
                $contribution_data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData1 = createSqlColVal($contribution_data);
                $qry = "INSERT INTO `owner_contribution` (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($qry);
                $stmt1->execute($contribution_data);
            }

            $data['charge_id'] = $charge_id;
            $data['auto_transaction_id'] = getAutoTransactionId($this->companyConnection);
            $data['transaction_id'] = $transaction_id;
            $data['user_id'] = $user_id;
            $data['bank_id'] = getPropertyBankId($this->companyConnection, $property_id);
            $data['property_id'] = $_POST['property_id'];
            $data['user_type'] = 'OWNER';
            $data['amount'] = $paid_amount;
            $data['type'] = 'OWNER_CONTRIBUTION';
            $data['payment_type'] =  $_POST['payment_type'];
            $data['stripe_status'] = $stripe_status;
            $data['stripe_response'] = $charge;
            $data['payment_response'] = $payment_response;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('code' => 200, 'status' => 'true','message' => 'Charge added successfully!');
        }
        catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }



}
$ownerDraw = new ownerDraw();