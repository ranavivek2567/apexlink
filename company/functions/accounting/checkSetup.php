<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class checkSetup extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     * Insert data to company unit type table
     * @return array
     */
    public function insert()
    {
        try {
            $company_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $cheque_element = json_encode(@$_REQUEST['cheque_element']);
            $query = $this->companyConnection->query("SELECT count(*) as cheque_count FROM cheque_template WHERE company_id=".$company_id);
            $chequeTemplate = $query->fetch();
            $updated_at = date('Y-m-d H:i:s');
            $created_at = date('Y-m-d H:i:s');
            if($chequeTemplate['cheque_count'] >= 1)
            {
                $sql = "UPDATE cheque_template SET type=?, elements=?, updated_at=? WHERE company_id=?";
                $stmt= $this->companyConnection->prepare($sql);
                $stmt->execute([$_REQUEST['cheque_type'], $cheque_element,$updated_at, $company_id]);

            } else {
                $data['company_id'] = $company_id;
                $data['type']       = $_REQUEST['cheque_type'];
                $data['elements']   = $cheque_element;
                $data['created_at'] = $created_at;
                $data['updated_at'] = $updated_at;

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO cheque_template (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');

        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }


    /**
     * function for fetching unit type data
     * @return array
     */

    public function getChequeElement()
    {
        try {
            $company_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $query = $this->companyConnection->query("SELECT elements,type FROM cheque_template WHERE company_id=".$company_id);
            $data = $query->fetch();

            if(!isset($data['type']) && $data['type'] == null || $data['type'] == 'null')
            {
                $data['type'] = 1;
            }
            if(isset($data['elements']) && $data['elements'] == null || $data['elements'] == 'null')
            {
                return $data = array('type'=>$data['type'],'elements'=>array("1","2","3","4","5","6","7","8","9"));

            }
            $data['elements'] = json_decode(@$data['elements']);
            $data = array('type'=>@$data['type'],'elements'=>@$data['elements']);

            return $data;


        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }
}

$checkSetup = new checkSetup();