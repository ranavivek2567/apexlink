<?php

/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */


include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
include_once( ROOT_URL."/helper/globalHelper.php");

class accounting extends DBConnection
{

    public function __construct()
    {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }



    public function getReceiableData()
    {

        //dd('hio');
        $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';

        $html = '';
        $html .= "<table class='table  table-bordered' border= '1px'>";
        $html .= "<tr>
        <th>Name</th>
        <th>Payment Type</th>
        <th>Check#</th>
        <th>Ref#</th>
        <th>Date#</th>
        <th>Memo</th>
        <th>Amount($default_symbol)</th>
        <th>Overpay/Underpay#</th>
        </tr>";
        $type = $_POST['type'];
        if($type=='user')
        {
            $user_id = $_POST['user_id'];
            $getData =  $this->companyConnection->query("SELECT * FROM accounting_manage_charges where user_id='$user_id'")->fetch();
        }
        else
        {
            $invoice_id = $_POST['invoice_id'];
            $getData =  $this->companyConnection->query("SELECT * FROM accounting_manage_charges where invoice_id='$invoice_id'")->fetch();
        }

        $name = $_POST['username'];

        $total_amount = $getData['total_amount'];
        $total_amount_paid = $getData['total_amount_paid'];
        $total_due_amount = $getData['total_due_amount'];
        $total_due_amount = number_format($total_due_amount,2);
        $total_refunded_amount = $getData['total_refunded_amount'];
        $overpay_underpay = $getData['overpay_underpay'];
        $overpay_underpay = number_format($overpay_underpay,2);
        if($overpay_underpay=='')
        {
            $overpay_underpay = 0;
        }

        $dueAndOverPay = str_replace(',', '', $total_due_amount) + str_replace(',', '',$overpay_underpay);

        $totalAmtPaidByUser = str_replace(',', '', $total_amount)+str_replace(',', '', $overpay_underpay);

        $status = $getData['status'];
        $date = date('Y-m-d H:i:s');
        $dateFormat = dateFormatUser($date,null,$this->companyConnection);
        $html .= "<tr>
        <td><select class='form-control'>";
        if($type=='user')
        {
            $html .="<option value='$user_id'>$name</option></select></td>";
        }
        else
        {
            $html .="<option value='$invoice_id'>$name</option></select></td>";
        }

        $html .="<td><select class='paymnt_type form-control'>
        <option value='check'>Check</option>
        <option value='cash'>Cash</option>
        <option value='money-order'>Money Order</option>";
        if($type=='user')
        {
            $html.="<option value='card'>Credit Card\Debit card</option>
        <option value='ACH'>ACH</option></td>";
        }
        if($type=='user')
        {
            $html.="<td><input type='text' class='check_$user_id form-control'></td>
        <td><input type='text' class='ref_$user_id form-control'></td>
        <td><input type='text' name='paymentDate_$user_id' class='paymentDate form-control' value='$dateFormat'></td>
        <td><textarea class='memo_$user_id'></textarea></td>
        <td><input type='text' name='paidAmt' class='paidAmt numberonly form-control amount_num' value='".$total_due_amount."'></td>
        <td><input type='text' class='overUnderPay numberonly form-control amount_num' value='".$overpay_underpay."' readonly></td>
        </tr> <input type='hidden' class='hiddenDueAmount form-control' value='$total_due_amount'>
         <input type='hidden' class='hiddenOverPay form-control' value='$overpay_underpay'>
         <input type='hidden' class='hiddenUncheckedAmt form-control' value='0'>

         <input type='hidden' class='hiddenUser_id form-control' value='$user_id'>

         <input type='hidden' class='hiddenChargeType form-control' value='$type'>";
        }
        else
        {

            $html.="<td><input type='text' class='check_$invoice_id form-control'></td>
        <td><input type='text' class='ref_$invoice_id form-control'></td>
        <td><input type='text' name='paymentDate_$invoice_id' class='paymentDate form-control' value='$dateFormat'></td>
        <td><textarea class='memo_$invoice_id'></textarea></td>
        <td><input type='text' name='paidAmt' class='paidAmt amount_num numberonly form-control' value='".$total_due_amount."'></td>
        <td><input type='text' class='overUnderPay numberonly form-control' value='".$overpay_underpay."' readonly></td>
        </tr> <input type='hidden' class='hiddenDueAmount form-control' value='$total_due_amount'>
         <input type='hidden' class='hiddenOverPay form-control' value='$overpay_underpay'>
         <input type='hidden' class='hiddenUncheckedAmt form-control' value='0'>

         <input type='hidden' class='hiddenUser_id form-control' value='$invoice_id'>

         <input type='hidden' class='hiddenChargeType form-control' value='$type'>";

        }



        echo $html;
        exit;

    }




    public function getAllChargesData()
    {
        $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';
        $type = $_POST['type'];
        $html =  "";
        $html .= "<table class='table  table-bordered' border= '1px'>";
        $html .= "<tr>
        <th>Date</th>
        <th>Description</th>
        <th>Charge Code</th>
        <th>Original Amount($default_symbol)</th>
        <th>Amount paid($default_symbol)</th>
        <th>Amount Due($default_symbol)</th>
        <th>Amount Waived off($default_symbol)</th>
        <th>Waive off comment</th>
        <th>Waive off Amount($default_symbol)</th>
        <th>Current Payment($default_symbol)</th>
        <th>Priority</th>
        <th>Pay</th>


        </tr>";
        if($type=='user')
        {
            $user_id = $_POST['user_id'];
            $idByType = $user_id;
            $getData =  $this->companyConnection->query("SELECT tc.id,tc.created_at,tc.amount,tc.amount_due,tc.amount_paid,tc.amount_refunded,tc.start_date,tc.end_date,tc.waive_of_amount,tc.waive_of_comment,tc.status,ca.description,ca.charge_code,ca.priority FROM tenant_charges as tc inner join company_accounting_charge_code as ca on tc.charge_code = ca.id   where tc.user_id='$user_id'")->fetchAll();

        }
        else
        {
            $invoice_id = $_POST['invoice_id'];
            $idByType = $invoice_id;
            $getData =  $this->companyConnection->query("SELECT tc.id,tc.created_at,tc.amount,tc.amount_due,tc.amount_paid,tc.amount_refunded,tc.start_date,tc.end_date,tc.waive_of_amount,tc.waive_of_comment,tc.status,ca.description,ca.charge_code,ca.priority FROM tenant_charges as tc inner join company_accounting_charge_code as ca on tc.charge_code = ca.id   where tc.invoice_id='$invoice_id'")->fetchAll();

        }
        $html .= "<input type='hidden' name='typeofcharge' value='$type'>
                <input type='hidden' name='idbytype' value='$idByType'>";
        foreach($getData as $data)
        {
            $created_at = $data['created_at'];
            $date = dateFormatUser($created_at,null,$this->companyConnection);
            $description = $data['description'];
            $amount_due = $data['amount_due'];
            $amount_due = number_format($amount_due,2);
            if($amount_due=='')
            {
                $amount_due = 0;
            }

            $totalamount = $data['amount'];
            $totalamount = number_format($totalamount,2);
            $amount_paid = $data['amount_paid'];
            $amount_paid = number_format($amount_paid,2);
            if($amount_paid=='')
            {
                $amount_paid = 0;
            }
            $amount_refunded = $data['amount_refunded'];
            $status = $data['status'];
            $charge_code = $data['charge_code'];
            $waive_of_amount = $data['waive_of_amount'];
            $waive_of_amount = number_format($waive_of_amount,2);
            if($waive_of_amount=='')
            {
                $waive_of_amount = 0;
            }
            $waive_of_comment = $data['waive_of_comment'];
            $chargeId = $data['id'];

            if($status=='1')
            {
                $disabled = 'disabled=disabled';
            }
            else
            {
                $disabled = '';
            }
            $priority = $data['priority'];

            $html .= "<tr>
       
        <td>$date</td>
        <td>$description</td>
        <td>$charge_code</td>
        <td>$default_symbol$totalamount</td>
        <td class='amount_num'>$default_symbol$amount_paid</td>
        <td class= 'amount_num current_due_amt_".$chargeId."'>$default_symbol$amount_due</td>
        <td>$default_symbol$waive_of_amount</td>
        <td><textarea class='waiveOfComment_".$chargeId."' name='waiveOfComment[]' $disabled>$waive_of_comment</textarea></td>
        <td><input type='text' class='waiveOfAmount_".$chargeId." numberonly amount_num' value='$waive_of_amount' name='waiveOfAmount[]' $disabled></td>
        <td><input type='text' class='currentPayment_".$chargeId." numberonly amount_num' value='$amount_due' name='currentPayment[]' $disabled></td>
        <td>$priority</td>";



            if($status=='1')
            {
                $html .= "<td><input type='checkbox' name='pay[]' disabled='disabled' checked='checked' value='$chargeId' class='pay_checkbox'></td>";

            }
            else
            {
                $html .= "<td><input type='checkbox' name='pay[]' checked='checked' value='$chargeId' class='pay_checkbox'></td>";
            }


            $html .=   "</tr>";

        }


        $html .= "</table>";
        $html .=   "<div class='footer-btn-outer'> <input type='submit' name='applyAccountingCalculations' class='submitBtn blue-btn' value='Save & New'>";
        $html .=   "<input type='submit' name='applyAccountingCalculations' class='submitBtn blue-btn' value='Save'>";
        $html .=   "<input type='button' class='CancelBtn blue-btn' value='Cancel'>";
        $html .=   "<input type='button'  class='reallocate blue-btn' value='Re-allocate'></div>";
        echo $html;
        exit;

    }



    public function updateAllCharges()
    {

        $chargeType = $_POST['typeofcharge']; /*other or users*/
        $idByType = $_POST['idbytype'];
        $payment_type = $_POST['paymnt_type'];
        $paid_amount = $_POST['paid_amount'];
        $hiddenChargeType = $_POST['hiddenChargeType'];
        if($payment_type=='card' || $payment_type=='ACH')
        {

            $checkPayment =  $this->checkForPayment($idByType,$payment_type);
            if($checkPayment['status']=='false')
            {
                return array('status'=>'false','message'=>$checkPayment['message']);
            }
            else
            {
                $customer_id = $checkPayment['customer_id'];
            }

            $receivePaymentCheck = $this->checkForReceivePayment();    /*this is to check if PM is Eligible for payment or not*/
            if($receivePaymentCheck['account_status']=='false')
            {
                return array('status'=>'false','message'=>'Currently PM Account is not verified');
            }
            else
            {
                $account_id = $receivePaymentCheck['account_data']['id'];

            }
        }

        if(isset($_POST['pay']))
        {


            $count = count($_POST['pay']);





            $wrongPaymentCheck = $this->wrongPaymentCheck();
            if($wrongPaymentCheck['status']=='false')
            {
                return array('status'=>'false','message'=>$wrongPaymentCheck['message']);
            }




            for($i=0; $i<$count; $i++)
            {
                $chargeTableId = $_POST['pay'][$i]; /*id of charge table*/

                $getData =  $this->companyConnection->query("SELECT * FROM tenant_charges where id='$chargeTableId'")->fetch();



                $currentPayment = $_POST['currentPayment'][$i];
                $currentPayment =      str_replace(',', '', $currentPayment);

                $waveAmt =  $_POST['waiveOfAmount'][$i];
                $waveAmt =      str_replace(',', '', $waveAmt);
                $amt = $getData['amount'];
                $amt =      str_replace(',', '', $amt);
                $waiveComment = $_POST['waiveOfComment'][$i];

                if($waveAmt=='')
                {
                    $waveAmt = 0;
                }
                $alreadyPaid = $getData['amount_paid'];
                if($alreadyPaid=='')
                {
                    $alreadyPaid = 0;
                }
                $currentAmtPaid = $alreadyPaid + $currentPayment;


                $currentDueAmt = $amt - $currentPayment - $waveAmt;
                $data['amount_due'] = $currentDueAmt;
                $data['amount_paid'] = $currentAmtPaid;
                $data['waive_of_amount'] = $waveAmt;

                if($amt>$currentAmtPaid + $waveAmt)
                {
                    $data['status'] = 0;
                }
                else if($amt=$currentAmtPaid + $waveAmt)
                {
                    $data['status'] = 1;
                }



                $sqlData = createSqlColValPair($data);


                $query = "UPDATE tenant_charges SET " . $sqlData['columnsValuesPair'] . " where id=" . $chargeTableId;


                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();


            }


        }

        if(!isset($_POST['pay']) && $paid_amount<=1)
        {
            return array('status'=>'false','message'=>'There must be atleast one charge to pay');

        }

        if($payment_type=='card' || $payment_type=='ACH' && $paid_amount>=1)
        {
            $this->createCharge($customer_id,$account_id,$paid_amount,$payment_type,$idByType,$chargeType);
        }
        else
        {
            $this->addTransactionData($paid_amount,$payment_type,$idByType,$chargeType);

        }

        $this->addChargeAmount($idByType,$chargeType);
        $this->changeInvoiceStatus($chargeType,$idByType);

        $getData1 =  $this->companyConnection->query("SELECT user_type FROM users where id=".$_POST['idbytype'])->fetch();

     /*     if($getData1['user_type']=='2'){
            for($i=0; $i<count($_POST['pay']); $i++){
                $now = new \DateTime('now');
                $month = $now->format('m');
                $year = $now->format('Y');
                $journal_entry_data['transaction_id'] = (isset($data['']) && !empty($data[''])?$data[''] :'');
                $journal_entry_data['transaction_type'] = (isset($_POST['paymnt_type']) && !empty($_POST['paymnt_type'])?$_POST['paymnt_type'] :'cash');
                $journal_entry_data['module_type'] = (isset($data['module_type']) && !empty($data['module_type'])?$data['module_type'] :'tenant_charges');
                $journal_entry_data['module_id'] = (isset($_POST['pay'][$i]) && !empty($_POST['pay'][$i])?$_POST['pay'][$i] :1);
                $journal_entry_data['from_user'] = (isset($_POST['idbytype']) && !empty($_POST['idbytype'])?$_POST['idbytype'] :1);
                $journal_entry_data['to_user'] = (isset($data['to_user']) && !empty($data['to_user'])?$data['to_user'] :1);
                $journal_entry_data['debit'] = (isset($data['debit']) && !empty($data['debit'])?$data['debit'] :0);
                $journal_entry_data['credit'] = (isset($_POST['currentPayment'][$i]) && !empty($_POST['currentPayment'][$i])?$_POST['currentPayment'][$i] :NULL);
                $journal_entry_data['current_month_year'] = $year.'-'.$month;
                $journal_entry_data['created_at'] = date('Y-m-d H:i:s');
                saveLedgerEnteries($this->companyConnection,$journal_entry_data);  // call  this function for saving enteries
            }
        }*/


        return array('status'=>'true','message'=>'Payment has been done successfully.');



    }



    public function addTransactionData($paid_amount,$payment_type,$user_id,$chargeType)
    {

        try{
            if($chargeType=="other")
            {
                
              $bank_id = $this->getBankIdForOther();
              $type = 'other';
            }
            else
            {
                $data['user_id'] = $user_id;
               $propertyId = $this->getPropertyId($user_id); 
               $bank_id = $this->getBankId($propertyId);
               $type = $this->getUserType($user_id);
               
                $data['property_id'] = $propertyId;
              
            }

             $data['bank_id'] = $bank_id;

            
            if($type=='2')
            {
                $data['user_type'] = 'TENANT';
            }
            else if($type=='4')
            {
                $data['user_type'] = 'OWNER';
            }
            else
            {
                $data['user_type'] = 'OTHER';
            }

            

            $data['amount'] = $paid_amount;
           
            $data['total_charge_amount'] = $paid_amount;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            
          
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('code' => 400, 'status' => 'true','message' => 'charge added successfully');

        }
        catch (Exception $e){
            dd($e);
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }

    }


    public function getBankIdForOther()
    {
       $getData = $this->companyConnection->query("SELECT * FROM company_accounting_bank_account where is_default='1'")->fetch(); 
       if(empty($getData))
       {
        return '0';
       }
       else
       {
        return $getData['id'];
       }

    }

    public function createCharge($customer_id,$account_id,$paid_amount,$payment_type,$user_id,$chargeType)
    {
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        try {
            $charge = \Stripe\Charge::create([
                "amount" => $paid_amount * 100,
                "currency" => 'usd',
                "customer"=> $customer_id,
                "destination" => array("account"  => $account_id)
            ]);



            $charge_id = $charge['id'];
            $transaction_id = $charge['balance_transaction'];
            $stripe_status  = $charge;
            $payment_response  = 'Payment has been done successfully.';



            $data['transaction_id'] = $transaction_id;
            $data['charge_id'] = $charge_id;
            $data['user_id'] = $user_id;
            $type = $this->getUserType($user_id);
            $propertyId = $this->getPropertyId($user_id); 
            $bank_id = $this->getBankId($propertyId);
            $data['bank_id'] = $bank_id;
            $data['property_id'] = $propertyId;

            if($type=='2')
            {
                $data['user_type'] = 'TENANT';
            }
            else if($type=='4')
            {
                $data['user_type'] = 'OWNER';
            }
            else
            {
                $data['user_type'] = 'TENANT';
            }

            $data['amount'] = $paid_amount;
            $data['payment_type'] = $payment_type;
            //  $data['prorated_payment_days'] = $days;
            $data['stripe_status'] = $stripe_status;
            $data['payment_response'] = $payment_response;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('code' => 400, 'status' => 'true','message' => 'charge added successfully');

        }
        catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }



    }



    public function changeInvoiceStatus($chargeType,$id)
    {


        if($chargeType=='invoice')
        {
            $payedInvoices  = $this->companyConnection->query("SELECT * FROM tenant_charges where invoice_id='$id' and status='1'")->fetchAll();
            $countPayedInvoice = count($payedInvoices);
            $invoices = $this->companyConnection->query("SELECT * FROM tenant_charges where invoice_id='$id'")->fetchAll();
            $countInvoices = count($invoices);

            if($countInvoices==$countPayedInvoice)
            {
                $data['status'] = 1;
                $sqlData = createSqlColValPair($data);

                $query = "UPDATE accounting_invoices SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

            }


        }
        else
        {
            $getInvoices  = $this->companyConnection->query("SELECT id FROM accounting_invoices where invoice_to='$id'")->fetchAll();

            if(!empty($getInvoices))
            {

                foreach($getInvoices as $invoice)
                {
                    $invoiceId = $invoice['id'];
                    $payedInvoices  = $this->companyConnection->query("SELECT * FROM tenant_charges where invoice_id='$invoiceId' and status='1'")->fetchAll();
                    $countPayedInvoice = count($payedInvoices);
                    $invoices = $this->companyConnection->query("SELECT * FROM tenant_charges where invoice_id='$invoiceId'")->fetchAll();
                    $countInvoices = count($invoices);

                    if($countInvoices==$countPayedInvoice)
                    {
                        $data['status'] = 1;
                        $sqlData = createSqlColValPair($data);

                        $query = "UPDATE accounting_invoices SET " . $sqlData['columnsValuesPair'] . " where id=" . $invoiceId;
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();

                    }



                }

            }
        }



    }





    public function getUserType($user_id)
    {
        $getData = $this->companyConnection->query("SELECT user_type FROM users where id='$user_id'")->fetch();
        return  $getData['user_type'];

    }



    public function checkForReceivePayment()
    {
        try {
            //Save Data in Company Database
            $request = [];

            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $_SESSION[SESSION_DOMAIN]['cuser_id']], 'users');
            if(!empty($user_data["data"])){
                if(!empty($user_data["data"]["stripe_account_id"])) {
                    $account_id = $user_data["data"]["stripe_account_id"];
//                        $account_id = 'acct_1Fb1FXIYilvAmUaE';
                    $request["account_id"] = $account_id;
                    $stripe_account_array["stripe_account_id"] = $account_id;
                    $connected_account_detail = getConnectedAccount($request);
                    if($connected_account_detail['account_data']["business_type"]=="company"){
                        $get_person_detail = getPerson($request);
                        $status = (isset($get_person_detail["person_detail"]["data"][0]["verification"]["status"]) && $get_person_detail["person_detail"]["data"][0]["verification"]["status"] == "verified"  && empty($connected_account_detail["account_data"]["company"]["requirements"]["current_deadline"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["currently_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["disabled_reason"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["company"]["requirements"]["pending_verification"]) ) ? 'Verified' : "false";
                    }else {
                        $status = (isset($connected_account_detail["account_data"]["individual"]["verification"]["status"]) && $connected_account_detail["account_data"]["individual"]["verification"]["status"] == "verified" && empty($connected_account_detail["account_data"]["individual"]["requirements"]["currently_due"]) && empty($connected_account_detail["account_data"]["individual"]["requirements"]["eventually_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["past_due"])&& empty($connected_account_detail["account_data"]["individual"]["requirements"]["pending_verification"])) ? 'Verified' : "false";
                    }
                }else{
                    $connected_account_detail["account_data"] = '';
                    $status =  "false";
                }
            }else{
                $status =  "false";
                $connected_account_detail["account_data"] = '';
            }

            return array('code' => 200, 'account_status' => $status, 'status' => 'success','account_data'=>$connected_account_detail["account_data"],   'message' => 'Account status fetched successfully.');


        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }





    }



    public function checkForPayment($user_id,$payment_type)
    {
        $getData = $this->companyConnection->query("SELECT stripe_customer_id FROM users where id='$user_id'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        if($customer_id=="")
        {
            return array('status'=>'false','message'=>"Please add $payment_type Information");
        }

        $checkDefaultSource = $this->checkDefaultSource($customer_id,$payment_type);
        if($checkDefaultSource['status']=='false')
        {
            return array('status'=>'false','message'=>$checkDefaultSource['message']);
        }else
        {
            return array('status'=>'true','customer_id'=>$customer_id);
        }



    }


    public function checkDefaultSource($customer_id,$payment_type)
    {

        $customer = \Stripe\Customer::retrieve($customer_id);
        if($payment_type=='card')
        {
            $sources = $customer['default_source'];

            if (strpos($sources, 'card') === false) {
                return array('status'=>'false','message'=>'Please set card as a default source');

            }
        }
        else if($payment_type=='ACH')
        {
            $sources = $customer['default_source'];
            if (strpos($sources, 'ba_') === false) {
                return array('status'=>'false','message'=>'Please set ACH account as a default source');

            }
        }
        else
        {
            return array('status'=>'true');
        }





    }




    public function addChargeAmount($id,$type)
    {
        if($type=='user')
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE user_id ='" . $id . "'")->fetch();
        }
        else
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE invoice_id ='" . $id . "'")->fetch();
        }



        if($getCharges['total_amount']=='')
        {
            $totalAmount = 0;
            $data['total_amount'] = $totalAmount;
        }
        else
        {
            $totalAmount = $getCharges['total_amount'];
            $totalAmount =    str_replace(',', '', $totalAmount);
            $data['total_amount'] = $totalAmount;

        }


        if($getCharges['amount_paid']=='')
        {
            $amount_paid = 0;
            $data['total_amount_paid'] = $amount_paid;
        }
        else
        {
            $amount_paid = $getCharges['amount_paid'];
            $amount_paid =    str_replace(',', '', $amount_paid);
            $data['total_amount_paid'] = $amount_paid;
        }

        if($getCharges['amount_refunded']=='')
        {
            $amount_refunded = 0;
            $data['total_refunded_amount'] = $amount_refunded;
        }
        else
        {
            $amount_refunded = $getCharges['amount_refunded'];
            $amount_refunded =    str_replace(',', '', $amount_refunded);
            $data['total_refunded_amount'] = $amount_refunded;
        }

        if($getCharges['waive_of_amount']=='')
        {
            $waive_of_amount = 0;
            $data['total_waive_amount'] = $waive_of_amount;
        }
        else
        {
            $waive_of_amount = $getCharges['waive_of_amount'];
            $waive_of_amount =    str_replace(',', '', $waive_of_amount);
            $data['total_waive_amount'] = $waive_of_amount;
        }

        $data['overpay_underpay'] = str_replace(',', '', $_POST['overPay']);

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');




        $data['total_due_amount'] = str_replace(',', '', $getCharges['amount_due']);

        $sqlData = createSqlColValPair($data);
        if($type=='user')
        {
            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }
        else
        {

            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where invoice_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }


    }


    public function wrongPaymentCheck()
    {
        $count = count($_POST['pay']);
        $idByType = $_POST['idbytype'];
        $chargeType = $_POST['typeofcharge'];




        for($i=0; $i<$count; $i++)
        {
            $chargeTableId = $_POST['pay'][$i]; /*id of charge table*/

            $getData =  $this->companyConnection->query("SELECT * FROM tenant_charges where id='$chargeTableId'")->fetch();



            $currentPayment = $_POST['currentPayment'][$i];
            $currentPayment =    str_replace(',', '', $currentPayment);

            $waveAmt =  $_POST['waiveOfAmount'][$i];
            $waveAmt =    str_replace(',', '', $waveAmt);
            $amt = $getData['amount'];
            $amt = str_replace(',', '', $amt);
            $waiveComment = $_POST['waiveOfComment'][$i];

            if($waveAmt=='')
            {
                $waveAmt = 0;
            }
            $alreadyPaid = $getData['amount_paid'];
            $alreadyPaid = str_replace(',', '', $alreadyPaid);
            if($alreadyPaid=='')
            {
                $alreadyPaid = 0;
            }
            $currentAmtPaid = $alreadyPaid + $currentPayment;



            if($amt>$currentAmtPaid+$waveAmt)
            {
                return array('status'=>'false','message'=>'Currently you are paying less amount');
            }
            else if($amt<$currentAmtPaid+$waveAmt)
            {
                return array('status'=>'false','message'=>'Currently you are paying greater amount');
            }

        }

    }


    public function checkForCard()
    {
        $user_id = $_POST['user_id'];
        $type = $_POST['type'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users where id='$user_id'")->fetch();
        if($getData['stripe_customer_id']=='')
        {
            return array('status'=>'false','message'=>"This user has not entered his $type infomation");
        }
        else
        {
            return array('status'=>'true','message'=>"this user has customer id");
        }


    }



    public function checkForACH()
    {
        $user_id = $_POST['user_id'];
        $type = $_POST['type'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users where id='$user_id'")->fetch();
        if($getData['stripe_customer_id']=='')
        {
            return array('status'=>'false','message'=>"This user has not entered his $type infomation");
        }
        else
        {
            return array('status'=>'true','message'=>"this user has customer id");
        }

    }

    public function getPropertyListing()
    {
        $html ='';
        $getData =  $this->companyConnection->query("SELECT * FROM general_property")->fetchAll();
        $html .= '<option value="all">Select Property</option>';
        foreach($getData as $data)
        {
            $html .= '<option value="'.$data['id'].'">'.$data['property_name'].'</option>';
        }
        echo $html;
        exit;

    }


    public function getMultipaymentList()
    {
        $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';
        $a="";
    if(isset($_POST['a'])) $a=$_POST['a'];
        $html =  "";
        $html .= "<table class='table  table-bordered' border= '1px'>";
        $html .= "<tr>
        <th>Payor Name</th>
        <th>Payment Type</th>
        <th>Check/Account Number</th>
        <th>Reference Number</th>
        <th>Date</th>
        <th>Memo</th>
        <th>Balance Amount($default_symbol)</th>
        <th>Waive off Amount($default_symbol)</th>
        <th>Current Payment($default_symbol)</th>
        <th>Overpay/Underpay($default_symbol)</th>
        <th>Action</th>
        </tr>";
      $getData="";
    if($a=="" || $a=='undefined'){
    $getData =  $this->companyConnection->query("SELECT u.id as user_id,u.name,u.first_name,u.last_name,u.name,u.id,amc.total_due_amount,amc.overpay_underpay FROM accounting_manage_charges as amc join users as u on u.id=amc.user_id where amc.total_due_amount!='0' ORDER BY u.name")->fetchAll();
    }
    elseif ($a!==""){
        $getData =  $this->companyConnection->query("SELECT u.id as user_id,u.name,u.first_name,u.last_name,u.name,u.id,amc.total_due_amount,amc.overpay_underpay FROM accounting_manage_charges as amc LEFT join users as u on u.id=amc.user_id LEFT JOIN accounting_invoices ai ON amc.invoice_id=ai.id where (u.name like '%$a%' or amc.total_due_amount like '%$a%' or amc.overpay_underpay like '%$a%' or u.id like '%$a%' OR ai.invoice_number LIKE '%$a%') and (amc.total_due_amount !='0') ORDER BY u.name")->fetchAll();
    }

    $numberOfPayer = $this->companyConnection->query("SELECT u.first_name,u.last_name,u.id,amc.total_due_amount,amc.overpay_underpay FROM accounting_manage_charges as amc join users as u on u.id=amc.user_id where amc.total_due_amount!='0'")->fetchAll();
     $totalNumberOfPayer = count($numberOfPayer);

     $amountData = $this->companyConnection->query("SELECT sum(total_waive_amount) as total_waive_amount,sum(total_due_amount) as total_due_amount,sum(total_amount_paid) as total_amount_paid,sum(overpay_underpay) as overpay_underpay,sum(waive_on_total_amount) as waive_on_total_amount FROM accounting_manage_charges where user_id!=''")->fetch();

     $totalDueAmount = $amountData['total_due_amount'];
     if($totalDueAmount==0 || $totalDueAmount=='')
     {
     $totalDueAmount = "0.0"; 
     }
     else
     {
     $totalDueAmount = number_format($totalDueAmount,2);
     }

     
     $totalAmtPaid   = $amountData['total_amount_paid'];
     $totalAmtPaid   = number_format($totalAmtPaid,2);

     $total_waive_amount = $amountData['total_waive_amount'] + $amountData['waive_on_total_amount'];
     if($total_waive_amount==0 || $total_waive_amount=="")
     {
      $total_waive_amount = "0.0";
     }
     else
     {
       $total_waive_amount = number_format($total_waive_amount,2);
     }

     
     $totalOverPay = $amountData['overpay_underpay'];
     $totalOverPay = number_format($totalOverPay,2);



        foreach($getData as $data)
        {

            //$payer_name = $data['first_name'].' '.$data['last_name'];
            $payer_name =  userName($data['user_id'], $this->companyConnection);
            $overpay_underpay = $data['overpay_underpay'];
            if($overpay_underpay==0 || $overpay_underpay=="")
            {
            $overpay_underpay = "0.0";    
            }
            else
            {
            $overpay_underpay = number_format($overpay_underpay,2);    
            }
            
            $date = date('Y-m-d H:i:s');
            $dateFormat = dateFormatUser($date,null,$this->companyConnection);
            $user_id = $data['id'];
            $total_due_amount = $data['total_due_amount'];
            if($total_due_amount==0)
            {
              $total_due_amount = "0.0";
            }else
            {
                $total_due_amount = number_format($total_due_amount,2);
            }
            
       

     $html .="<tr>
        <td class='payer_name'><input type='checkbox' data-user_id='$user_id' class='payer_id unchecked'>$payer_name</td>
        <td>
        <select class='paymnt_type_$user_id form-control disableAll enableDisable_$user_id'>
        <option value='check'>Check</option>
        <option value='cash'>Cash</option>
        <option value='money-order'>Money Order</option>
        <option value='card'>Credit Card\Debit card</option>
      </td>
        <td><input type='text' class='check_$user_id form-control disableAll enableDisable_$user_id'></td>
        <td><input type='text' class='ref_$user_id form-control disableAll enableDisable_$user_id'></td>
        <td><input type='text' name='paymentDate' class='paymentDate_$user_id form-control disableAll enableDisable_$user_id' value='$dateFormat'></td>
        <td><textarea class='memo disableAll enableDisable_$user_id'></textarea></td>
        <td><input type='text' name='balance' class='balance_$user_id numberonly amount_num form-control disableAll enableDisable_$user_id' value='".$total_due_amount."'></td>
        <td><input type='text' class='waive_of_on_total_$user_id numberonly form-control disableAll enableDisable_$user_id waive_of_on_total amount_num' value='0.0'></td>
         <td><input type='text' name='paidAmt' class='paidAmt_$user_id numberonly amount_num form-control disableAll enableDisable_$user_id paidAmt' value='".$total_due_amount."'></td>
        <td><input type='text' class='overUnderPay_$user_id numberonly form-control disableAll enableDisable_$user_id amount_num' value='".$overpay_underpay."' readonly></td>
        <td class='actionbutton_$user_id forNoAction'><input type='button' value='No Action' class='disableAll enableDisable_$user_id'></td>
        </tr>";


        }
        $html .= "<tr>
        <td></td>
        <td style='font-weight:bold; font-size:13px;'>Total # of Payers : $totalNumberOfPayer</td>
        <td></td>
        <td></td>
        <td></td>
        <td style='font-weight:bold; font-size:13px;'>Total:</td>
        <td style='font-weight:bold; font-size:13px;'>$default_symbol$totalDueAmount</td>
        <td style='font-weight:bold; font-size:13px;'>$default_symbol$total_waive_amount</td>
        <td style='font-weight:bold; font-size:13px;'>$default_symbol$totalAmtPaid</td>
        <td style='font-weight:bold; font-size:13px;'>$default_symbol$totalOverPay</td>
        <td></td>
                </tr>";
        $html .= "<input type='hidden' class='hiddenOverPay form-control' value='0'>
         <input type='hidden' class='hiddenUncheckedAmt form-control' value='0'>
         <input type='hidden' class='checkedUserid form-control' value='0'>";
        $html .= "<input type='hidden' class='paymentStatus form-control' value='notReadyForPay'>";

        echo $html;
        exit;


    }



    public function updateChargeByMultipay()
    {
        $user_id = $_POST['user_id'];
        $paidAmt = $_POST['paidAmount'];
        $payment_type = $_POST['payment_type'];
        $payment_module = $_POST['payment_module'];
        $waiveAmt = $_POST['waiveAmt'];

        if($payment_type=='card' || $payment_type=='ACH')
        {

            $checkPayment =  $this->checkForPayment($user_id,$payment_type);
            if($checkPayment['status']=='false')
            {
                return array('status'=>'false','message'=>$checkPayment['message']);
            }
            else
            {
                $customer_id = $checkPayment['customer_id'];
            }

            $receivePaymentCheck = $this->checkForReceivePayment();    /*this is to check if PM is Eligible for payment or not*/
            if($receivePaymentCheck['account_status']=='false')
            {
                return array('status'=>'false','message'=>'Currently PM Account is not verified');
            }
            else
            {
                $account_id = $receivePaymentCheck['account_data']['id'];

            }
        }

        $overUnderPay = $_POST['overPay'];
        if($overUnderPay<0)
        {
            return array('status'=>'false','message'=>'Currently you are paying less amount');
        }

        $getData =  $this->companyConnection->query("SELECT * FROM tenant_charges where user_id='$user_id' and (status='0' OR status=null)")->fetchAll();
        if(empty($getData) && $overUnderPay==0)
        {
            return array('status'=>'false','message'=>'Currently you have no charge to pay');
        }
        else{

            foreach($getData as $data)
            {
                $currentDueAmt = $data['amount_due'];
                $chargeTableId = $data['id']; /*id of charge table*/
                $data1['amount_due'] = 0;
                $data1['amount_paid'] = $currentDueAmt;
                $data1['waive_of_amount'] = 0;
                $data1['status'] = 1;



                $sqlData1 = createSqlColValPair($data1);
                $query1 = "UPDATE tenant_charges SET " . $sqlData1['columnsValuesPair'] . " where id=" . $chargeTableId;

                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute();
            }

            $data3['status'] = 1;
            $sqlData3 = createSqlColValPair($data3);

            $query3= "UPDATE accounting_invoices SET " . $sqlData3['columnsValuesPair'] . " where invoice_to=" . $user_id;
            $stmt3 = $this->companyConnection->prepare($query3);
            $stmt3->execute();




        }

        if($payment_type=='card' || $payment_type=='ACH' && $paid_amount>=1)
        {
            $this->createCharge($customer_id,$account_id,$paidAmt,$payment_type,$user_id,'user');
        }
        else
        {
            $this->addTransactionData($paidAmt,$payment_type,$user_id,'user');
        }
        $this->addChargeAmount($user_id,'user');
        return array('status'=>'true','message'=>'Payment has been done successfully.');
        //$this->changeInvoiceStatus($chargeType,$idByType);

    }


    public function getProperties()
    {
        $data =  $this->companyConnection->query("SELECT property_name,id FROM general_property")->fetchAll();
        $html = "<option value=''>Select</option>";
        foreach($data as $properties)
        {
            $id = $properties['id'];
            $name = $properties['property_name'];
            $html .= "<option value=".$id.">".$name."</option>";
        }

        return array('property'=>$html);

    }


    public function getBuildings(){
        try{
            $property_id = $_REQUEST['property_id'];
            $query1 = "SELECT * FROM building_detail Where property_id = $property_id";
            $buildingData = $this->companyConnection->query($query1)->fetchAll();
            $html = "<option value=''>Select</option>";
            foreach($buildingData as $building)
            {
                $id = $building['id'];
                $name = $building['building_name'];
                $html .= "<option value=".$id.">".$name."</option>";
            }
            return array('building'=>$html);

        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }


    public function getUnits()
    {
        $building_id = $_POST['building_id'];
        $query1 = "SELECT id,unit_prefix,unit_no FROM unit_details Where building_id = $building_id";
        $buildingData = $this->companyConnection->query($query1)->fetchAll();
        $html = "<option value=''>Select</option>";
        foreach($buildingData as $building)
        {
            $id = $building['id'];
            $unit_prefix = $building['unit_prefix'];
            $unit_no = $building['unit_no'];
            $html .= "<option value=".$id.">".$unit_prefix."-".$unit_no."</option>";
        }
        return array('unit'=>$html);





    }


    public function getUserNameByType()
    {
        $type = $_POST['type'];
        $property_id = $_POST['property_id'];
        if($type==4)
        {
            $query1 = "SELECT u.id,u.first_name FROM users as u inner join owner_property_owned as op on op.user_id=u.id Where u.user_type = '$type'";
        }
        else if($type==2)
        {
            $query1 = "SELECT u.id,u.first_name FROM users as u inner join tenant_property as tp on tp.user_id=u.id Where u.user_type = '$type'";
        }
        else
        {
            $query1 = "SELECT id,first_name FROM users Where user_type = '$type'";
        }

        $getData = $this->companyConnection->query($query1)->fetchAll();
        $html = "<option value=''>Select</option>";
        if(!empty($getData))
        {
            foreach($getData as $data)
                $html .="<option value=".$data['id'].">".$data['first_name']."</option>";

        }
        echo $html;
        exit;


    }


    public function getBankListByProperty()
    {
        $property_id = $_POST['property_id'];
        $getData = $this->companyConnection->query("SELECT ca.id,ca.bank_name FROM company_accounting_bank_account as ca join general_property as gp on gp.portfolio_id=ca.portfolio where gp.id = '$property_id'")->fetchAll();
        $htmlBank = "";
        if(!empty($getData))
            {
                if(count($getData)!='1')
                {
             $htmlBank .= "<option value=''>Please Select</option>";
                }
                

                if(!empty($getData))
                {
                    foreach($getData as $data)
                        $htmlBank .="<option value=".$data['id'].">".$data['bank_name']."</option>";

            }    
          }
          else
          {
            $htmlBank = "<option value=''>Please Select</option>"; 
          }

        
        echo $htmlBank;
        exit;

    }


    public function getBankList()
    {

        $getData = $this->companyConnection->query("SELECT ca.id,ca.bank_name FROM company_accounting_bank_account as ca")->fetchAll();
        $htmlBank = "<option value=''>Please Select</option>";

        if(!empty($getData))
        {
            foreach($getData as $data)
                $htmlBank .="<option value=".$data['id'].">".$data['bank_name']."</option>";

        }
        echo $htmlBank;
        exit;

    }






    public function getCheckNumber()
    {

        $bank_id = $_POST['bank_id'];
        $getData = $this->companyConnection->query("SELECT last_used_check_number FROM company_accounting_bank_account where id = '$bank_id'")->fetch();
        echo $getData['last_used_check_number'];
        exit;

    }

    public function getAddress()
    {
        $type = $_POST['type'];
        $property_id = $_POST['property_id'];
        $user_id = $_POST['user_id'];
        if($type==2)
        {
            /* echo "SELECT address1,address2,address3,address4 FROM general_property where id = '$property_id'";
             exit;*/
            if(!empty($property_id)){

            $getdata = $this->companyConnection->query("SELECT address1,address2,address3,address4 FROM general_property where id = '$property_id'")->fetch();
            echo $getdata['address1'].' '.$getdata['address2'].' '.$getdata['address3'].' '.$getdata['address4'];
            exit;
            }
            else
            {
               return array('status'=>'false','message'=>'Please select property');
            }
        }
        else
        {
            $getdata = $this->companyConnection->query("SELECT address1,address2,address3 FROM users where id = '$user_id'")->fetch();
            echo $getdata['address1'].' '.$getdata['address2'].' '.$getdata['address3'];
            exit;
        }
    }


    public function createCheckForbank()
    {
        $checkNumber = $_POST['checkNumber'];

        $getData =  $this->companyConnection->query("SELECT check_number FROM accounting_banking where check_number='$checkNumber'")->fetch();
        if(!empty($getData))
        {
            return array('status'=>'false','message'=>"This Check Number is already exist");
        }
        $property_id =$_POST['property'];

        $getPropertyName = $this->companyConnection->query("SELECT property_name FROM general_property where id='$property_id'")->fetch();
        $property_name = $getPropertyName['property_name'];


        $data['property_id'] = $_POST['property'];
        $data['building_id'] = $_POST['building'];
        $data['unit_id'] = $_POST['unit'];
        $data['bank_id'] = $_POST['selectBank'];
        $data['userType'] = $_POST['userType'];
        
        if($_POST['userType']==0)
        {
        $data['user_id'] = 0;
        $data['other_name'] = $_POST['payOrderOff'];
        }
        else
        {
         $data['user_id'] = $_POST['payOrderOff'];
        }    
        
       
        $data['date'] =  mySqlDateFormat($_POST['Date'], null, $this->companyConnection);
        $data['memo'] = $_POST['memo'];
        $amt = $_POST['money'];
        $data['amount'] = str_replace(',', '', $amt);
        $data['check_number'] = $_POST['checkNumber'];
        $data['other_address'] = $_POST['address'];
        $sqlData = createSqlColVal($data);
        $query = "INSERT INTO accounting_banking(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($data);
        $id = $this->companyConnection->lastInsertId();


        return array('status'=>'true','message'=>"Check has been Created Successfully",'last_id'=>$id,'property_name'=>$property_name,'user_type'=>$_POST['userType']);




    }



    public function getCheckData()
    {
        $last_id = $_POST['last_id'];
        $property_name = $_POST['property_name'];
        $getData =  $this->companyConnection->query("SELECT * FROM accounting_banking as ab join company_accounting_bank_account as ca on ca.id=ab.bank_id join users as u on ca.user_id=u.id where ab.id='$last_id'")->fetch();
        if($_POST['user_type']==2)
        {
            $user_type = 'Tenant';
        }
        else if($_POST['user_type']==3)
        {
            $user_type = 'Vendor';
        }
        else if($_POST['user_type']==4)
        {
            $user_type = 'Owner';
        }
        else if($_POST['user_type']==0)
        {
            $user_type = 'Other';
        }
        $user_name = $getData['first_name'];
        $html = "";
        $html .= "<tr>";
        $html .= "<td>".$getData['check_number']."</td>";
        $html .= "<td>".$getData['branch_code']."</td>";
        $html .= "<td>".$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].$getData['amount']."</td>";
        $html .= "<td>".$getData['memo']."</td>";
        $html .= "<td>$user_type</td>";
        $html .= "<td>".$user_name."</td>";
        $html .= "<td>".$getData['other_address']."</td>";
        $html .= "<td>".$property_name."</td>";
        $html .= "</tr>";

        echo $html;
        exit;


    }



    public function printCheckData()
    {
        $check_id = $_POST['check_id'];
        $userType = $_POST['user_type'];
        if($userType==0)
        {
           $getData =  $this->companyConnection->query("SELECT amount,check_number,other_address,other_name as name,user_id  FROM accounting_banking  where id='$check_id'")->fetch();   
        }
        else
        {

        $getData =  $this->companyConnection->query("SELECT ab.check_number,ab.amount,ab.other_address,u.id,u.first_name,u.last_name FROM accounting_banking as ab join company_accounting_bank_account as ca on ca.id=ab.bank_id join users as u on ab.user_id=u.id where ab.id='$check_id'")->fetch();
       // dd($getData);
       
        }
        
       
       
        return array("data"=>$getData,'status'=>'true','user_type'=>$userType);


    }




    public function getBankId($property_id)
    {
      $getData =  $this->companyConnection->query("SELECT * FROM property_bank_details WHERE property_id ='" . $property_id . "' and is_default='1'")->fetch();
      if(empty($getData))
      {
        return '';
      }
      else
      {
       return $bank_id = $getData['bank_id'];
      }
       
    }


    public function getPropertyId($user_id)
    {
         $getType = $this->companyConnection->query("SELECT user_type FROM users where id='$user_id'")->fetch();
         
       if($getType['user_type']==2)
       {
       $getData =  $this->companyConnection->query("SELECT property_id FROM tenant_property WHERE user_id ='" . $user_id . "'")->fetch();
       }
       else if($getType['user_type']==4)
       {
        $getData =  $this->companyConnection->query("SELECT property_id FROM owner_property_owned WHERE user_id ='" . $user_id . "'")->fetch();
       }

       return $property_id = $getData['property_id'];
    }
















    /**
     * Insert data to company amenities table
     * @return array
     */




}


$UnitTypeAjax = new accounting();