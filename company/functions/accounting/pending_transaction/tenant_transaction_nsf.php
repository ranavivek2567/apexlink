<?php
/* Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class AjaxPendingGLPosting extends DBConnection{
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function updateChargesStatus(){
        try {
        	if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
        		$data1 = $_POST['user_id'];
        		foreach ($data1 as $value) {
	            	$data['status']="1";
	            	$sqlData = createSqlColValPair($data);
	            	$query = "UPDATE tenant_charges SET " . $sqlData['columnsValuesPair'] ." where id=".$value;
                    $stmt = $this->companyConnection->prepare($query);
            		$stmt->execute();

            		$data['charges'] =$this->companyConnection->query("SELECT id,user_id,amount FROM tenant_charges WHERE id=$value")->fetch();
            		$user_id = $data['charges']['user_id'];
            		$amount = $data['charges']['amount'];
            		$this->saveLedger($user_id, $amount);
        		}
        		return ['code'=>200, 'status'=>'success', 'data'=>$data1, 'message' => 'Your post was successful.'];
        	}else{
        		return array('code' => 504, 'status' => 'error','message' => 'Please select atleast one post.');	
        	}
        } catch (PDOException $e) {
            return array('code' => 504, 'status' => 'error','message' => $e->getMessage());
        }
    }

    public function updateSecurityDeposit(){
        try {
            if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
                $data1 = $_POST['user_id'];
                foreach ($data1 as $value) {
                    $data['payment_status']="1";
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] ." where id=".$value;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                }
                return ['code'=>200, 'status'=>'success', 'data'=>$data1, 'message' => 'Your post was successful.'];
            }else{
                return array('code' => 504, 'status' => 'error','message' => 'Please select atleast one post.');    
            }
        } catch (PDOException $e) {
            return array('code' => 504, 'status' => 'error','message' => $e->getMessage());
        }
    }

    public function updatePendingPayment(){
        try {
            if (isset($_POST['user_id']) && !empty($_POST['user_id'])) {
                $data1 = $_POST['user_id'];
                foreach ($data1 as $value) {
                    $data['payment_status']="SUCCESS";
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE transactions SET " . $sqlData['columnsValuesPair'] ." where id=".$value;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                }
                return ['code'=>200, 'status'=>'success', 'data'=>$data1, 'message' => 'Your post was successful.'];
            }else{
                return array('code' => 504, 'status' => 'error','message' => 'Please select atleast one post.');    
            }
        } catch (PDOException $e) {
            return array('code' => 504, 'status' => 'error','message' => $e->getMessage());
        }
    }

    public function getBankDetails(){
        try {
            $data_bank =$this->companyConnection->query("SELECT id,bank_name FROM company_accounting_bank_account")->fetchAll();
            if (!empty($data_bank)) {
                return ['code'=>200, 'status'=>'success', 'data'=>$data_bank, 'message' => 'Record fetched.'];
            }else{
                return array('code' => 503, 'status' => 'error');    
            }
        } catch (PDOException $e) {
            return array('code' => 504, 'status' => 'error','message' => $e->getMessage());
        }
    }

    public function getBankDetail(){
        try {
            $id = $_POST['id'];
            $data_bank =$this->companyConnection->query("SELECT * FROM company_accounting_bank_account WHERE id=$id")->fetch();
            if (!empty($data_bank)) {

                $bankBalance =  $this->getBankBalance($bankId,$transactionType);
                if ($bankBalance['status'] == 'success') {
                    $data_bank['balance'] = $bankBalance['data']['total_amount'];
                }
                return ['code'=>200, 'status'=>'success', 'data'=>$data_bank, 'message' => 'Record fetched.'];
            }else{
                return array('code' => 503, 'status' => 'error');    
            }
        } catch (PDOException $e) {
            return array('code' => 504, 'status' => 'error','message' => $e->getMessage());
        }
    }

    public function getBankBalance($bankId,$transactionType){
        try {
            $data_bank =$this->companyConnection->query("SELECT SUM(total_amount) as total_amount FROM transactions WHERE bank_id=$bankId AND user_type=$transactionType")->fetchAll();
            if (!empty($data_bank)) {
                return ['code'=>200, 'status'=>'success', 'data'=>$data_bank, 'message' => 'Record fetched.'];
            }else{
                return array('code' => 503, 'status' => 'error');    
            }
        } catch (PDOException $e) {
            return array('code' => 504, 'status' => 'error','message' => $e->getMessage());
        }
    }

    public function saveLedger($userIds){
    	$journal_entry_data['transaction_id'] = (isset($data['transaction_id']) && !empty($data['transaction_id'])?$data['transaction_id'] :NULL);
		$journal_entry_data['transaction_type'] = (isset($data['transaction_type']) && !empty($data['transaction_type'])?$data['transaction_type'] :'TENANT_NSF');
		$journal_entry_data['module_type'] = (isset($data['module_type']) && !empty($data['module_type'])?$data['module_type'] :'TENANT_NSF');
		$journal_entry_data['module_id'] = (isset($data['module_id']) && !empty($data['module_id'])?$data['module_id'] :1);
		$journal_entry_data['from_user'] = (isset($userIds) && !empty($userIds)?$userIds :1);
		$journal_entry_data['to_user'] = (isset($data['to_user']) && !empty($data['to_user'])?$data['to_user'] :1);
		$journal_entry_data['debit'] = (isset($data['debit']) && !empty($data['debit'])?$data['debit'] :NULL);
		$journal_entry_data['credit'] = (isset($data['credit']) && !empty($data['credit'])?$data['credit'] :NULL);
		$journal_entry_data['current_month_year'] = (isset($data['current_month_year']) && !empty($data['current_month_year'])?$data['current_month_year'] :'2019-10');
		$journal_entry_data['created_at'] = date('Y-m-d H:i:s');
		saveLedgerEnteries($this->companyConnection,$journal_entry_data);  
    }



}
$ajaxPendingPosting = new AjaxPendingGLPosting();