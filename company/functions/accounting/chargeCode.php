<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class ChargeCodeAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     * Insert data to company AccountChargeCode table
     * @return array
     */
    public function insert()
    {
        try {
            $data = $_POST['form'];

            $required_array = ['debit_account','credit_account','charge_code','description'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['charge_code_id'])){
                    unset($data['charge_code_id']);
                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['is_editable'] = 1;
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];

                $checkExists =  checkNameAlreadyExists($this->companyConnection,'company_accounting_charge_code', 'charge_code', $data['charge_code'], '');
                if($checkExists['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Charge Code already exists.');
                }

//                $checkLastPriorityNumber = 'SELECT fields FROM table ORDER BY id DESC LIMIT 1;';
                $query = $this->companyConnection->query("SELECT priority FROM company_accounting_charge_code ORDER BY id DESC LIMIT 1;");
                $checkLastPriorityNumber = $query->fetch();
//                dd($checkLastPriorityNumber['priority']);
                $data['priority'] = $checkLastPriorityNumber['priority']+1;

                if($data['is_default'] == 1){
                    $update_data['is_default'] = 0;
                    $sqlData = createSqlColValPair($update_data);
                    $query = "UPDATE company_accounting_charge_code SET ".$sqlData['columnsValuesPair'];
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                }

                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_accounting_charge_code (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update data of AccountChargeCode
     * @return array
     */
    public function update()
    {
        try {
            $data = $_POST['form'];

            $charge_code_id = $data['charge_code_id'];
            //Required variable array
            $required_array = ['debit_account','credit_account','charge_code','description','status'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['charge_code_id']) && !empty($data['charge_code_id'])){
                    unset($data['charge_code_id']);
                }

                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $checkExists =  checkNameAlreadyExists($this->companyConnection,'company_accounting_charge_code', 'charge_code', $data['charge_code'], $charge_code_id);
                if($checkExists['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Charge Code already exists.');
                }

                $unitTypeData = getDataById($this->companyConnection, 'company_accounting_charge_code', $charge_code_id);

                $checkStatus = $unitTypeData['data']['status'];
                if($checkStatus == 0 && $data['is_default']){
                    return array('code' => 503,'status' => 'error', 'message' => 'Deactivate value can\'t be set as default.');
                }

                $checkIsDefault = $unitTypeData['data']['is_default'];
                if($checkIsDefault == 1 && $data['is_default'] == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'One default value is required.');
                }

                if(isset($data['is_default']) && $data['is_default'] == 1){
                    $update_data['is_default'] = 0;
                    $sqlData = createSqlColValPair($update_data);
                    $query = "UPDATE company_accounting_charge_code SET ".$sqlData['columnsValuesPair'];
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                }

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE company_accounting_charge_code SET ".$sqlData['columnsValuesPair']." where id='$charge_code_id'";
                $stmt =$this->companyConnection->prepare($query);

                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record updated successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for fetching AccountChargeCode data
     * @return array
     */
    public function view(){

        try {
            $id = $_POST['id'];
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            if(isset($login_user_id) && !empty($login_user_id))
            {
                return getDataById($this->companyConnection, 'company_accounting_charge_code', $id);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }

    /**
     * Update AccountChargeCode Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];

            $checkSetDefault = $this->companyConnection->prepare("SELECT * FROM company_accounting_charge_code WHERE id=?");
            $checkSetDefault->execute([$id]);
            $checkSetDefault = $checkSetDefault->fetch();

            if(isset($checkSetDefault) || !empty($checkSetDefault)){
                $isDefault = $checkSetDefault['is_default'];

                if($isDefault == 1 && $status == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'A default value cannot be deactivated.');
                }
            }
            $sql = "UPDATE company_accounting_charge_code SET status=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $id]);

            if($status == '1'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Record activated successfully.';
            }
            if($status == '0'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Record deactivated successfully.';
            }

            if($stmt){
                return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $_SESSION[SESSION_DOMAIN]["status_message"]);
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Delete AccountChargeCode
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE company_accounting_charge_code SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Get all account charge code data
     * @return array
     */
    public  function getAllChargeCodeList(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM company_accounting_charge_code WHERE status = '1'");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * get account credit data
     * @return array
     */
    public  function getAllCreditList(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM company_credit_accounts");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * get account debit data
     * @return array
     */

    public  function getAllDebitList(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM company_debit_accounts");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Export Sample Excel For AccountChargeCode
     */
    public function exportSampleExcel()
    {
        $file_url = COMPANY_SITE_URL . "/excel/ChargeCode.xlsx";
        if (ob_get_contents()) ob_end_clean();
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
        readfile($file_url); // do the double-download-dance (dirty but worky)
        die();
    }

    /**
     * Export Excel For AccountChargeCode
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function exportExcel(){

        include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");

        $objPHPExcel = new PHPExcel();
        $table = $_REQUEST['table'];

        $query1 = "SELECT $table.*,company_credit_accounts.credit_accounts,company_debit_accounts.debit_accounts FROM $table LEFT JOIN company_credit_accounts ON $table.credit_account=company_credit_accounts.id LEFT JOIN company_debit_accounts ON $table.debit_account=company_debit_accounts.id";
        $stmt = $this->companyConnection->query($query1);
        //$data = $stmt->fetchAll();

        //Set header with temp array
        $columnHeadingArr =array("ChargeCode", "Description","credit_accounts","debit_accounts");
        //take new main array and set header array in it.
        $sheet =array($columnHeadingArr);
        while ($unit_type_data = $stmt->fetch(PDO::FETCH_ASSOC))
        {

            $tmpArray =array();
            $charge_code = $unit_type_data['charge_code'];
            array_push($tmpArray,$charge_code);

            $description = $unit_type_data['description'];
            array_push($tmpArray,$description);

            $credit_account = $unit_type_data['credit_accounts'];
            array_push($tmpArray,$credit_account);

            $debit_account = $unit_type_data['debit_accounts'];
            array_push($tmpArray,$debit_account);

            array_push($sheet,$tmpArray);
        }
        $worksheet = $objPHPExcel->getActiveSheet();
        foreach($sheet as $row => $columns) {
            foreach($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
        //make first row bold
//        $objPHPExcel->getActiveSheet()->getStyle("A1:B1:C1:D1:E1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="ChargeCode.xlsx"');
        $objWriter->save('php://output');
        die();
    }

    /**
     * Import Unit type Excel
     * @return array
     * @throws PHPExcel_Exception
     */
    public function importExcel(){
        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if(in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if($file_size < 3000) {
                        $file = "uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx/ .csv) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                                printErrorLog($e->getMessage());
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the first element of 'A1'
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();

                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestDataColumn();

                            //Loop through each row of the worksheet
                            for ($row = 2; $row <= $total_rows; $row++) {

                                $skip_record = true;
                                $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);

                                if ($first_row_header == 'ChargeCode') {

                                    foreach ($single_row[0] as $key => $value) {
                                        if($key == 0){
                                            $chargeCodeData = getSingleRecord($this->companyConnection, ['column' => 'charge_code', 'value' => $value], 'company_accounting_charge_code');
                                            if($chargeCodeData['code'] == 400){
                                                $single_row[0][$key] = $value;
                                            } else {
                                                $skip_record = false;
                                                continue;
                                            }
                                        }
                                        if( $key == 2){
                                            $debit_account_data = getSingleRecord($this->companyConnection, ['column' => 'credit_accounts', 'value' => $value], 'company_credit_accounts');
                                            if($debit_account_data['code'] == 200){
                                                $single_row[0][$key] = $debit_account_data['data']['id'];
                                            } else {
                                                $single_row[0][$key] = null;
                                            }
                                            continue;
                                        }

                                        if($key == 3){
                                            $credit_account_data = getSingleRecord($this->companyConnection, ['column' => 'debit_accounts', 'value' => $value], 'company_debit_accounts');
                                            if($credit_account_data['code'] == 200){
                                                $single_row[0][$key] = $credit_account_data['data']['id'];
                                            } else {
                                                $single_row[0][$key] = null;
                                            }
                                            continue;
                                        }
                                        $single_row[0][$key] = $value;

                                    }

                                    if($skip_record) {
                                        $insert = [];
                                        $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                        $insert['debit_account'] = $single_row[0][3];
                                        $insert['credit_account'] = $single_row[0][2];
                                        $insert['charge_code'] = $single_row[0][0];
                                        $insert['description'] = $single_row[0][1];
                                        $insert['status'] = '1';
                                        $insert['is_default'] = '0';
                                        $insert['created_at'] = date('Y-m-d H:i:s');
                                        $insert['updated_at'] = date('Y-m-d H:i:s');
                                        $sqlData = createSqlColVal($insert);
                                        $query = "INSERT INTO company_accounting_charge_code (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                        $stmt = $this->companyConnection->prepare($query);
                                        $stmt->execute($insert);
                                    }
                                } else {
                                    return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data'];
                                    printErrorLog($e->getMessage());
                                }
                            }
                            return ['status'=>'success','code'=>200,'message'=>'File uploaded successfully!'];
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }

    /**
     * Import AccountChargeCode Excel
     * @return array
     * @throws PHPExcel_Exception
     */
    public function importExcedl(){
        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if(in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if($file_size < 3000) {
                        $file = "uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx/ .csv) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                                printErrorLog($e->getMessage());
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the first element of 'A1'
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestDataColumn();
                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $extra_columns = ",`user_id`, `is_default`, `status`, `is_editable`, `created_at`, `updated_at`";
                            $is_default = 0;
                            $status = 1;
                            $is_editable = 1 ;
                            $extra_values = ",'" . $login_user_id . "','" . $is_default . "','" . $status . "','". $is_editable . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
                            $query = "insert into `company_accounting_charge_code` (`charge_code`, `description`, `credit_account`, `debit_account`$extra_columns) VALUES ";
                           // dd($query);
                            //Loop through each row of the worksheet
                            for ($row = 2; $row <= $total_rows; $row++) {
                                //Read a single row of data and store it as a array.
                                //This line of code selects range of the cells like A1:D1
                                $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
                                //Creating a dynamic query based on the rows from the excel file
                                $data =$this->companyConnection->query("SELECT charge_code FROM company_accounting_charge_code")->fetchAll();
                               // dd($data);

                                if ($first_row_header == 'ChargeCode') {
                                    $query .= "(";
                                    //Print each cell of the current row
                                    foreach ($single_row[0] as $key => $value) {
                                        $query .= "'" . $value . "',";
                                        foreach ($data as $key1 => $value1) {
                                            if (in_array($value, $value1)) {
//                                                echo '<pre>'; print_r($value); die('<<<<<===$value>>>>');
                                                return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data'];
                                                printErrorLog($e->getMessage());
                                            }
                                        }
                                    }

                                    $query = substr($query, 0, -1);
                                    $query .= $extra_values . "),";
                                } else {
                                    return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data'];
                                    printErrorLog($e->getMessage());
                                }
                            }
                            $query = substr($query, 0, -1);
                            try {
                                $stmt = $this->companyConnection->prepare($query);
                            } catch (Exception $e) {
                                return ['status'=>'failed','code'=>503,'message'=>'Error in importing the data'];
                                printErrorLog($e->getMessage());
                            }
                            $executed_query = $stmt->execute();
                            if ($executed_query) {
                                unlink($file);
                                return ['status'=>'success','code'=>200,'message'=>'Data imported successfully.'];
                            }else{
                                return ['status'=>'success','code'=>200,'message'=>'There is some error in your file.'];
                            }
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    public function restoreChargeCode()
    {

        try {
            $data = $_POST['cuser_id'];
            //  $data = $this->postArray($data);
            //print_r($data); exit;
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];

            $data = array();
            $data['deleted_at'] = NULL ;
            //$data['id'] = $_POST['cuser_id'];

            $deleted_at['deleted_at'] = NULL ;
            $sqlData = $this->createSqlColValPair($deleted_at);

            $query = "UPDATE company_accounting_charge_code SET ".$sqlData['columnsValuesPair']." WHERE id =".$_POST['cuser_id'];

            $stmt1 =$this->companyConnection->prepare($query);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records restored successfully');


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


}

$ChargeCodeAjax = new ChargeCodeAjax();