<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");

class Invoice extends DBConnection {


    /**
     * Invoice constructor.
     */
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    /**
     * add page for invoice
     * @return array
     */
    public function add()
    {
        try {
            $redirect = VIEW_COMPANY."company/accounting/invoices/add.php";
            return require $redirect;

        }

        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }


    /**
     * function to show all vendors
     * @return array
     */
    public function getAllTenants() {
        try {

            $search = @$_REQUEST['q'];
            if(empty($search)){
                $sql = "SELECT u.id,u.user_type,u.name,p.city,p.state,p.country,p.zipcode,tp.property_id,tp.unit_id,p.property_name,p.property_id,p.address1,p.address2,p.address3,ud.floor_no,td.user_id,td.record_status,tld.user_id,tld.record_status FROM users as u LEFT JOIN tenant_property tp ON tp.user_id= u.id LEFT JOIN general_property as p ON p.id= tp.property_id LEFT JOIN unit_details as ud ON ud.id= tp.unit_id LEFT JOIN tenant_details as td ON td.user_id= u.id LEFT JOIN tenant_lease_details as tld ON tld.user_id= u.id where u.user_type IN ('2') AND u.status = '1' AND td.record_status = '1' AND tld.record_status='1'  order by u.user_type DESC";
            } else {
                $sql = "SELECT u.id,u.user_type,u.name,p.city,p.state,p.country,p.zipcode,tp.property_id,tp.unit_id,p.property_name,p.property_id,p.address1,p.address2,p.address3,ud.floor_no,td.user_id,td.record_status,tld.user_id,tld.record_status FROM users as u LEFT JOIN tenant_property tp ON tp.user_id= u.id LEFT JOIN general_property as p ON p.id= tp.property_id LEFT JOIN unit_details as ud ON ud.id= tp.unit_id LEFT JOIN tenant_details td ON td.user_id= u.id LEFT JOIN tenant_lease_details as tld ON tld.user_id= u.id where u.user_type IN ('2') AND u.status = '1' AND td.record_status = '1' AND tld.record_status='1'  and name LIKE '%$search%' order by user_type DESC";
            }

            $users_data = $this->companyConnection->query($sql)->fetchAll();
//            dd($users_data);
            $users_count = sizeOf($users_data);
            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['name'];
                    $data['rows'][$key]['floor_no'] = $user_data['floor_no'];
                    $data['rows'][$key]['property_id'] = $user_data['property_id'];
                    $data['rows'][$key]['property_name'] = $user_data['property_name'];
                    $data['rows'][$key]['address1'] = $user_data['address1'];
                    $data['rows'][$key]['address2'] = $user_data['address2'];
                    $data['rows'][$key]['address3'] = $user_data['address3'];
                    $data['rows'][$key]['city'] = $user_data['city'];
                    $data['rows'][$key]['state'] = $user_data['state'];
                    $data['rows'][$key]['country'] = $user_data['country'];
                    $data['rows'][$key]['zipcode'] = $user_data['zipcode'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    /**
     * function to show all owners
     * @return array
     */
    public function getAllOwners() {
        try {

            $search = @$_REQUEST['q'];
            if(empty($search)){
                $sql = "SELECT u.id,u.user_type,u.name,opo.user_id,opo.property_id as property_owned_id,opo.property_percent_owned,p.property_name,p.property_id FROM owner_property_owned as opo LEFT JOIN users u ON u.id= opo.user_id LEFT JOIN general_property as p ON p.id= opo.property_id order by u.name ASC";
            } else {
                $sql = "SELECT u.id,u.user_type,u.name,opo.user_id,opo.property_id as property_owned_id,opo.property_percent_owned,p.property_name,p.property_id FROM owner_property_owned as opo LEFT JOIN users u ON u.id= opo.user_id LEFT JOIN general_property as p ON p.id= opo.property_id WHERE u.name LIKE '%$search%'";
            }
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);
            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['name'];
                    $data['rows'][$key]['floor_no'] = $user_data['floor_no'];
                    $data['rows'][$key]['property_id'] = $user_data['property_id'];
                    $data['rows'][$key]['property_name'] = $user_data['property_name'];
                    $data['rows'][$key]['address1'] = $user_data['address1'];
                    $data['rows'][$key]['address2'] = $user_data['address2'];
                    $data['rows'][$key]['address3'] = $user_data['address3'];
                    $data['rows'][$key]['city'] = $user_data['city'];
                    $data['rows'][$key]['state'] = $user_data['state'];
                    $data['rows'][$key]['country'] = $user_data['country'];
                    $data['rows'][$key]['zipcode'] = $user_data['zipcode'];
                    $data['rows'][$key]['property_owned_id'] = $user_data['property_owned_id'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    /**
     * function to show all charge codes
     * @return array
     */
    public function getCharges(){

        try{
            $data = [];
            $data['charges'] = $this->companyConnection->query("SELECT id,charge_code,description FROM company_accounting_charge_code")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Insert data to company unit type table
     * @return array
     */
    public function insert()
    {
        try {
            $data = $_POST;
            //dd($data);
            $required_array = [''];
            /* Max length variable array */
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            /*Checking server side validation*/
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $user_type = 0;
                if($data["user_radio"] == "tenant_radio"){
                    $user_type = 2;
                }else if($data["user_radio"] == "owner_radio"){
                    $user_type = 4;
                }
                $invoice_number = substr(str_shuffle("0123456789"), 0, 7);
                // $number = str_replace(',', '', $_POST['amount'][$i]);
                $user_id = (isset($data['user_id']) && !empty($data['user_id'])? $data['user_id'] : NULL);
                $invoice_data['invoice_to'] =  (isset($data['user_id']) && !empty($data['user_id'])? $data['user_id'] : NULL);
                $invoice_data['invoice_from'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $invoice_data['user_type'] = $user_type;
                // $invoice_data['total_amount'] = $number;
                //$invoice_data['amount_due'] = $number;
                $invoice_data["invoice_number"] = $invoice_number;
                $invoice_data["other_name"] = (isset($data['other_name']) && !empty($data['other_name'])? $data['other_name'] :"");
                $invoice_data["other_address"] = (isset($data['address']) && !empty($data['address'])? $data['address'] :"");
                $invoice_data["email_invoice"] = (isset($data['email_invoice']) && !empty($data['email_invoice'])? "1" :"0");
                $invoice_data["no_late_fee"] = (isset($data['no_late_fee']) && !empty($data['no_late_fee'])? "1" :"0");
                $invoice_data['invoice_date'] = empty($_POST['invoice_date'])?NULL:mySqlDateFormat($_POST['invoice_date'],null,$this->companyConnection);
                $invoice_data['late_date'] = empty($_POST['late_date'])?NULL:mySqlDateFormat($_POST['late_date'],null,$this->companyConnection);
                $invoice_data["property_id"] = (isset($data['property_id']) && !empty($data['property_id'])? $data['property_id'] :NULL);
                $invoice_data['status'] = "0";
                $invoice_data['created_at'] = date('Y-m-d H:i:s');
                $invoice_data['updated_at'] = date('Y-m-d H:i:s');
                // dd(mySqlDateFormat($_POST['invoice_date'],null,$this->companyConnection));
                /*Save Data in Company Database*/
                $sqlData = createSqlColVal($invoice_data);
                $query = "INSERT INTO accounting_invoices (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($invoice_data);
                $last_insert_id = $this->companyConnection->lastInsertId();
                if($last_insert_id){
                    //Save Data into elastic search documents
                    $invoiceData = $invoice_data;
                    $invoiceData['id'] = $last_insert_id;
                    $ElasticSearchSave = insertDocument('INVOICE','ADD',$invoiceData,$this->companyConnection);
                    $this->addInvoiceDetails($data,$last_insert_id);
                    $this->addChargeAmount($user_id,$last_insert_id);
                }

                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }


    public function addInvoiceDetails($data,$invoice_id){
        $charge_data = [];
        $user_type = 0;
        if($data["user_radio"] == "tenant_radio"){
            $user_type = 2;
        }else if($data["user_radio"] == "owner_radio"){
            $user_type = 4;
        }
        $charges = $_POST['charge_code'];
        $i=0;
        foreach($charges as $charge)
        {
            $number = str_replace(',', '', $_POST['amount'][$i]);
            $charge_data['user_id'] =  (isset($data['user_id']) && !empty($data['user_id'])? $data['user_id'] : 0);
            $charge_data['user_type'] = $user_type;
            $charge_data['invoice_id'] = $invoice_id;
            $charge_data['invoice_from'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $charge_data['charge_code'] = $charge;
            $charge_data['amount'] = $number;
            $charge_data['amount_due'] =  $number;
            $charge_data['start_date'] = empty($_POST['invoice_date'])?NULL:mySqlDateFormat($_POST['invoice_date'],null,$this->companyConnection);
            $charge_data['end_date'] = empty($_POST['late_date'])?NULL:mySqlDateFormat($_POST['late_date'],null,$this->companyConnection);
            $charge_data['status'] = 0;
            $charge_data['frequency'] = 'One Time';
            $charge_data['created_at'] = date('Y-m-d H:i:s');
            $charge_data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($charge_data);
            $query = "INSERT INTO tenant_charges(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($charge_data);
            $i++;
        }

        return array('status' => 'success',  'message' => 'Data added successfully.','table'=>'tenant_charges');
    }


    public function addChargeAmount($user_id,$invoice_id)
    {
        if(!empty($user_id)) {

            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded , sum(amount_due) as amount_due FROM tenant_charges WHERE user_id ='" . $user_id . "'")->fetch();
        }else{
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded , sum(amount_due) as amount_due FROM tenant_charges WHERE invoice_id ='" . $invoice_id . "'")->fetch();
        }

        if($getCharges['total_amount']=='')
        {
            $totalAmount = 0;
            $data['total_amount'] = $totalAmount;
        }
        else
        {
            $totalAmount = $getCharges['total_amount'];
            $data['total_amount'] = $totalAmount;
        }
        if($getCharges['amount_paid']=='')
        {
            $amount_paid = 0;
            $data['total_amount_paid'] = $amount_paid;
        }
        else
        {
            $amount_paid = $getCharges['amount_paid'];
            $data['total_amount_paid'] = $amount_paid;
        }


        if($getCharges['amount_refunded']=='')
        {
            $amount_refunded = 0;
            $data['total_refunded_amount'] = $amount_refunded;
        }
        else
        {
            $amount_refunded = $getCharges['amount_refunded'];
            $data['total_refunded_amount'] = $amount_refunded;
        }
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        if(!empty($user_id)) {
            $checkManageCharge = $this->companyConnection->query("SELECT *  FROM accounting_manage_charges WHERE user_id ='" . $user_id . "'")->fetch();
        }else{
            $checkManageCharge = $this->companyConnection->query("SELECT *  FROM accounting_manage_charges WHERE invoice_id ='" . $invoice_id . "'")->fetch();
        }
        if(empty($checkManageCharge))
        {

            $data['user_id'] = $user_id;
//            if(empty($user_id)) {
            $data['invoice_id'] = $invoice_id;
//            }
            $data['total_due_amount'] = $totalAmount;
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO accounting_manage_charges(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
        }
        else
        {


            if(empty($user_id)){
                $data['total_due_amount'] = $getCharges["amount_due"];
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where invoice_id=" . $invoice_id;
            }else{
                $data['user_id'] = $user_id;
                $data['total_due_amount'] = $getCharges["amount_due"];
                $data['invoice_id'] = $invoice_id;
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $user_id;

            }
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }
    }

    /**
     * function to add custom field.
     * @return array|void
     */
    public function View(){
        try {
            $renovation_detail_html="";
            $data = [];
            $custom_data=[];
            $id= $_GET["id"];
            $data = $this->companyConnection->query("SELECT i.id,i.invoice_number,i.invoice_date,i.property_id as owner_property_id,i.late_date,i.invoice_to,i.other_name,i.user_type,i.total_amount,i.amount_due,i.amount_paid,i.status,u.name as invoice_to ,tp.property_id,gp.property_name,ud.floor_no,ud.unit_prefix,ogp.property_name as owner_property_name FROM accounting_invoices as i  LEFT JOIN users as u ON u.id=i.invoice_to LEFT JOIN tenant_property as tp ON tp.user_id=i.invoice_to LEFT JOIN general_property as gp ON gp.id=tp.property_id LEFT JOIN general_property as ogp ON ogp.id=i.property_id LEFT JOIN unit_details as ud ON ud.id=tp.unit_id WHERE i.id=".$id)->fetch();
            if(!empty($data)){
                if($data["user_type"] == 4){
                    $data["property_name"] =  $data["owner_property_name"];
                }
                if( isset($data['late_date']) && !empty($data["late_date"] && $data["status"] == 0)){
                    // Declare and define two dates
                    $late_date = strtotime($data['late_date']);
                    //$invoice_date = strtotime($data['invoice_date']);
                    $invoice_date = strtotime(date("Y-m-d"));
                    // Formulate the Difference between two dates
                    $diff = ($late_date - $invoice_date)/60/60/24; ;
                    if($diff < 0 && $data['status'] == 0){
                        $day = ($diff < 1)?'Days': 'Day';
                        $status = "Late (".abs($diff).' '.$day.")";
                    }elseif($diff > 0 && $data['status'] == 0){
                        $status = 'Unpaid';
                    }elseif($diff == 0 && $data['status'] == 0){
                        $status = 'Unpaid';
                    }
                }elseif($data["status"] == 0){
                    $status = 'Unpaid';

                }elseif ($data["status"] == 1){
                    $status = 'Paid';

                }


                $data['invoice_date'] = dateFormatUser($data['invoice_date'], $id, $this->companyConnection);
                $data['late_date'] = dateFormatUser($data['late_date'], $id, $this->companyConnection);
                $data['user_type'] = (isset($data['user_type'])  && $data['user_type'] == '0' )?'Other':getRole($data['user_type']);
                $data['total_amount'] = (isset($data['total_amount'])  && $data['total_amount'] != '0' )? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].''.number_format($data['total_amount']):'--';
                // $data['amount_due'] = (isset($data['amount_due'])  && $data['amount_due'] != '0' )? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].' '. $data['amount_due']:'--';
                // $data['amount_paid'] = (isset($data['amount_paid'])  && $data['amount_paid'] != '0' )? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].' '. $data['amount_paid']:'--';
                $data['unit'] = $data['unit_prefix'] .'-'.$data['floor_no'];
                $data['status'] = $status;

                $data1 = $this->companyConnection->query("SELECT tc.id,tc.charge_code,tc.amount,tc.amount_due,tc.amount_paid,cc.charge_code,cc.description FROM tenant_charges as tc LEFT JOIN company_accounting_charge_code as cc ON cc.id=tc.charge_code WHERE tc.invoice_id=".$data["id"])->fetchAll();
                $amount = 0;
                $amount_paid= 0;
                foreach ($data1 as $key => $charges_data) {
                    $amount += $charges_data['amount'];
                    $amount_paid += $charges_data['amount_paid'];

                }
                $data['total_amount'] = (isset($amount)  && $amount != 0 )? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].''. number_format($amount,2):'$0.00';
                $data['amount_due'] = $amount -$amount_paid;
                $data['amount_due'] = (isset($data['amount_due'])  && $data['amount_due'] != 0 )? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].''. number_format($data['amount_due'],2):'$0.00';
                $data['amount_paid'] = (isset($amount_paid)  && $amount_paid != 0 )? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].''. number_format($amount_paid,2):'$0.00';
                return array('code'=>200,'status' => 'success',  'message' => 'Data fetched successfully','data'=>$data,'charge_data'=>$data1);
            }else{
                return array('code'=>400,'status' => 'error',  'message' => 'Unable to fetch records.','table'=>'accounting_invoices');
            }

        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            return;
        }
    }


    /**
     * function to add custom field.
     * @return array|void
     */
    public function Edit(){
        try {
            $renovation_detail_html="";
            $data = [];
            $custom_data=[];
            $id= $_GET["id"];
            $data = $this->companyConnection->query("SELECT i.id,i.invoice_number,i.invoice_date,i.late_date,i.invoice_to,i.other_name,i.user_type,i.total_amount,i.amount_due,i.property_id,i.amount_paid,i.status,i.email_invoice,i.no_late_fee,i.other_address,u.id as invoice_to_id,u.name as invoice_to ,u.address1,u.address2,u.address3,u.city,u.state,u.zipcode,gp.address1 as p_address1,gp.address2 as p_address2,gp.address3 as p_address3,gp.city as p_city,gp.state as p_state,gp.zipcode as p_zipcode,gp.country as p_country  ,tp.property_id,gp.property_name,ud.floor_no,ud.unit_prefix,ogp.property_name as owner_property_name  FROM accounting_invoices as i  LEFT JOIN users as u ON u.id=i.invoice_to LEFT JOIN tenant_property as tp ON tp.user_id=i.invoice_to LEFT JOIN general_property as gp ON gp.id=tp.property_id LEFT JOIN general_property as ogp ON ogp.id=i.property_id LEFT JOIN unit_details as ud ON ud.id=tp.unit_id WHERE i.id=".$id)->fetch();
            if(!empty($data)){
                if($data["user_type"] == 4){
                    $data["property_name"] =  $data["owner_property_name"];
                }
                $data['invoice_date'] = dateFormatUser($data['invoice_date'], $id, $this->companyConnection);
                $data['late_date'] = dateFormatUser($data['late_date'], $id, $this->companyConnection);
                $data['user_type'] = $data['user_type'];
                $data['total_amount'] = (isset($data['total_amount'])  && $data['total_amount'] != '0' )? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].' '. $data['total_amount']:'--';
                $data['amount_due'] = (isset($data['amount_due'])  && $data['amount_due'] != '0' )? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].' '. $data['amount_due']:'--';
                $data['amount_paid'] = (isset($data['amount_paid'])  && $data['amount_paid'] != '0' )? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].' '. $data['amount_paid']:'--';
                $data['unit'] = $data['unit_prefix'] .'-'.$data['floor_no'];

                if($data["user_type"]==2){
                    $data["address1"] = $data["p_address1"];
                    $data["address2"] = $data["p_address2"];
                    $data["address3"] = $data["p_address3"];
                    $data["state"] = $data["p_state"];
                    $data["city"] = $data["p_city"];
                    $data["zipcode"] = $data["p_zipcode"];
                    $data["country"] = $data["p_country"];
                }elseif($data["user_type"]==4){
                    $data["address1"] = $data["address1"];
                    $data["address2"] = $data["address2"];
                    $data["address3"] = $data["address3"];
                    $data["state"] = $data["state"];
                    $data["city"] = $data["city"];
                    $data["zipcode"] = $data["zipcode"];
                    $data["country"] = $data["country"];
                }


                $data1 = $this->companyConnection->query("SELECT tc.id,tc.charge_code,tc.amount,cc.id as charge_id,cc.charge_code,cc.description FROM tenant_charges as tc LEFT JOIN company_accounting_charge_code as cc ON cc.id=tc.charge_code WHERE tc.invoice_id=".$data["id"])->fetchAll();
                return array('code'=>200,'status' => 'success',  'message' => 'Data fetched successfully','data'=>$data,'charge_data'=>$data1);
            }else{
                return array('code'=>400,'status' => 'error',  'message' => 'Unable to fetch records.','table'=>'accounting_invoices');
            }

        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            return;
        }
    }

    /**
     * Update Unit Type Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];
            $sql = "UPDATE company_accounting_tax_setup SET status=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $id]);

            if($status == '1'){
                $_SESSION["status_message"]='Record activated successfully.';
            }
            if($status == '0'){
                $_SESSION["status_message"]='Record deactivated successfully.';
            }

            if($stmt){
                return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $_SESSION["status_message"]);
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Delete Unit Type
     * @return array
     */
    public function deleteInvoices(){
        try{
            $ids = $_POST['ids'];
            $invoice_ids = $_POST['ids'];
            if (!is_array($ids))
                $ids = array($ids); // if it is just one id not in an array, put it in an array so the rest of the code work for all cases
            $ids = array_map([$this->companyConnection, 'quote'], $ids); // filter elements for SQL injection
            if(!empty($ids)){
                foreach ($invoice_ids as $key => $value) {
                    $this->calculateDeleteInvoiceData($value);
                }
            }
            $this->companyConnection->exec('DELETE FROM accounting_invoices WHERE id IN (' . implode(', ', $ids) . ')');
            $this->companyConnection->exec('DELETE FROM tenant_charges WHERE invoice_id IN (' . implode(', ', $ids) . ')');
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function calculateDeleteInvoiceData($value){
        $invoice_id= $value;
        $user_id = $this->companyConnection->query("SELECT invoice_to from accounting_invoices where id ='" . $invoice_id . "'")->fetch();
        $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded , sum(amount_due) as amount_due FROM tenant_charges WHERE invoice_id ='" . $invoice_id . "'")->fetch();
        if(!empty($getCharges)){
            $total_amount = (isset($getCharges["total_amount"] )&& !empty($getCharges["total_amount"])) ? $getCharges["total_amount"]:0;
            $amount_paid = (isset($getCharges["amount_paid"] )&& !empty($getCharges["amount_paid"])) ? $getCharges["amount_paid"]:0;
            $amount_due = (isset($getCharges["amount_due"] )&& !empty($getCharges["amount_due"])) ? $getCharges["amount_due"]:0;
            if(!empty($user_id["invoice_to"])) {
                $sql = "UPDATE accounting_manage_charges SET total_amount=total_amount -" . $total_amount . ",total_amount_paid = total_amount_paid-" . $amount_paid . ",total_due_amount=total_due_amount-" . $amount_due . "  WHERE user_id=" . $user_id["invoice_to"];
            }else{
                $sql = "UPDATE accounting_manage_charges SET total_amount=total_amount -" . $total_amount . ",total_amount_paid = total_amount_paid-" . $amount_paid . ",total_due_amount=total_due_amount-" . $amount_due . "  WHERE invoice_id=" . $invoice_id;
                $delete_invoices_data =$this->deleteRecords('accounting_manage_charges',$invoice_id);
            }
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute();

        }
    }



//    public function createSqlColValPair($data) {
//        $columnsValuesPair = '';
//        foreach ($data as $key=>$value){
//            if($key == 'deleted_at'){
//                $columnsValuesPair .=  $key."=NULL";
//            }else{
//                $columnsValuesPair .=  $key."='".$value."',";
//                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
//            }
//        }
//
//        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
//        return $sqlData;
//    }


    /**
     * Insert data to company unit type table
     * @return array
     */
    public function update()
    {
        try {
            $data = $_POST;
            //  dd($data);
            $required_array = [''];
            /* Max length variable array */
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            /*Checking server side validation*/
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $user_id = (isset($data['user_id']) && !empty($data['user_id'])? $data['user_id'] : NULL);
                $invoice_id = (isset($data['invoice_id']) && !empty($data['invoice_id'])? $data['invoice_id'] : NULL);
                $invoice_data["other_name"] = (isset($data['other_name']) && !empty($data['other_name'])? $data['other_name'] :"");
                $invoice_data["other_address"] = (isset($data['address']) && !empty($data['address'])? $data['address'] :"");
                $invoice_data["email_invoice"] = (isset($data['email_invoice']) && !empty($data['email_invoice'])? "1" :"0");
                $invoice_data["no_late_fee"] = (isset($data['no_late_fee']) && !empty($data['no_late_fee'])? "1" :"0");
                $invoice_data['invoice_date'] = empty($_POST['invoice_date'])?NULL:mySqlDateFormat($_POST['invoice_date'],null,$this->companyConnection);
                $invoice_data['late_date'] = empty($_POST['late_date'])?NULL:mySqlDateFormat($_POST['late_date'],null,$this->companyConnection);
                $invoice_data['updated_at'] = date('Y-m-d H:i:s');
                /*Save Data in Company Database*/
                $invoice_data['updated_at'] = date('Y-m-d H:i:s');
                /*Save Data in Company Database*/
                $sqlData = createSqlUpdateCase($invoice_data);
                $query = "UPDATE accounting_invoices SET " . $sqlData['columnsValuesPair'] . " where id='$invoice_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($invoice_data);
                if($stmt->execute($sqlData['data'])) {
                    //Save Data into elastic search documents
                    $invoiceData = $invoice_data;
                    $invoiceData['id'] = $invoice_id;
                    $ElasticSearchSave = insertDocument('INVOICE','UPDATE',$invoiceData,$this->companyConnection);
                    $delete_invoices_data =$this->deleteRecords('tenant_charges',$invoice_id);
                    $this->addInvoiceDetails($data,$invoice_id);
                    $this->addChargeAmount($user_id,$invoice_id);
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                } else {
                    return array('code' => 400, 'status' => 'error', 'message' => 'Internal Server Error!');

                }
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  Delete Vehicle detail
     */
    public function deleteRecords($table,$invoice_id){

        try{

            $count=$this->companyConnection->prepare("DELETE FROM $table WHERE invoice_id=:id");
            $count->bindParam(":id",$invoice_id,PDO::PARAM_INT);
            $count->execute();
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];


        }catch (Exception $exception)
        {
            echo json_encode( ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()]);
            printErrorLog($exception->getMessage());
        }
    }

    /*
* function for fetch property manageers and admin
* */
    public function getAllInvoices() {

        $ids = $_POST['ids'];
        if (!is_array($ids))
            $ids = array($ids); // if it is just one id not in an array, put it in an array so the rest of the code work for all cases
        $ids = array_map([$this->companyConnection, 'quote'], $ids); // filter elements for SQL injection
        $html = '';
        $data = $this->companyConnection->query("SELECT i.id,i.invoice_number,i.invoice_date,i.late_date,i.invoice_to,i.other_name,i.user_type,i.total_amount,i.amount_due,i.amount_paid,i.status,i.email_invoice,i.no_late_fee,i.other_address,u.id as invoice_to_id,u.name as invoice_to ,u.address1,u.address2,u.address3,u.city,u.state,u.zipcode  ,tp.property_id,gp.property_name,ud.floor_no,ud.unit_prefix,bd.building_name FROM accounting_invoices as i  LEFT JOIN users as u ON u.id=i.invoice_to LEFT JOIN tenant_property as tp ON tp.user_id=i.invoice_to LEFT JOIN general_property as gp ON gp.id=tp.property_id LEFT JOIN building_detail as bd ON bd.id=tp.building_id LEFT JOIN unit_details as ud ON ud.id=tp.unit_id  WHERE i.id IN (" . implode(', ', $ids) . ")")->fetchAll();



        $html.= '<body style="padding-top:10px; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif; font-size:13px; line-height:19px; margin:0;">';
        foreach ($data as $key => $invoice_data) {
            $amount =0.00;
            $amount_paid =0.00;
            $invoice_to_name = '';
            if($invoice_data['user_type']== 0 || $invoice_data == NULL){
                $invoice_to_name = $invoice_data['other_name'];
            }else{
                $invoice_to_name = $invoice_data['invoice_to'];
            }
            $invoice_date = dateFormatUser($invoice_data['invoice_date'],null, $this->companyConnection);
            $late_date = dateFormatUser($invoice_data['late_date'], null, $this->companyConnection);

            if(empty($late_date)){
                $due_date =  $late_date;
            }else{
                $due_date =  $invoice_date;
            }
            $unit = '';
            if(!empty( $invoice_data['unit_prefix']) || !empty( $invoice_data['floor_no'])) {
                $unit = $invoice_data['unit_prefix'] . '-' . $invoice_data['floor_no'];
            }

            $city_state_zip = '';
            if($invoice_data['user_type']== 0 || $invoice_data == NULL){

            }else{
                if(!empty( $invoice_data['city']) || !empty( $invoice_data['state'])|| !empty( $invoice_data['zipcode'])) {
                    $city_state_zip = $invoice_data['city'] . ',' . $invoice_data['state'] . ' '.$invoice_data['zipcode'] ;
                }
            }
            $month_year = '';
            if(!empty($invoice_data['invoice_date'])){
                $month_year = explode("-",$invoice_data['invoice_date']);
                $service_period = $month_year[0].$month_year[1];
            }

            $data1 = $this->companyConnection->query("SELECT tc.id,tc.charge_code,tc.amount,tc.amount_due,tc.amount_paid,cc.id as charge_id,cc.charge_code,cc.description FROM tenant_charges as tc LEFT JOIN company_accounting_charge_code as cc ON cc.id=tc.charge_code WHERE tc.invoice_id=".$invoice_data["id"])->fetchAll();

            $sql = "SELECT sum(amount) as amount_sum, count(*) as record_count FROM tenant_charges WHERE invoice_id='".$invoice_data["id"]."' AND status = 1";

            $query = $this->companyConnection->query($sql);
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $sum = 0.00;
            if ($row) {

                $sum = (!empty($row['amount_sum']))?$row['amount_sum']:0.00;
                $count = $row['record_count'];
            }
            $html.='<table width="680" align="center" cellspacing="0" cellpadding="0">
  <tr>

    
    <td align="left" style="padding: 0 0 10px 0; vertical-align: bottom;">
    <p style="text-align: right; margin: 0; font-weight: bold;" >'.$invoice_data["property_name"].'</p>
      <p style="text-align: right; margin: 0; font-weight: bold;">Marco Island, FL 34145</p>
      <p style="text-align: right; margin: 0; font-weight: bold;">123-456-7890 - <a href="javascript:;">sonny@apexlink.com</a></p>
    </td>
  </tr>
</table>
<table width="680" align="center" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="4" align="center" style="background: #00359d; color: #fff;">
      <h4 style="margin: 5px 0; font-weight: 700;">TENANT INVOICE</h4>
    </td>
  </tr>
   <tr>
    <td style="font-weight: bold; padding: 20px 10px 5px 10px; font-size: 16px;">Tenant: </td>
    <td style=" padding: 20px 10px 5px 10px; font-size: 16px;">'. $invoice_to_name.'</td>
    <td style="font-weight: bold; padding: 20px 10px 5px 10px; font-size: 16px;">Invoice Date: </td>
    <td style=" padding: 20px 10px 5px 10px; font-size: 16px;">'.$invoice_date.'</td>
  </tr>
  <tr>
    <td style="font-weight: bold; padding: 5px 10px 5px 10px; font-size: 16px;">Address: </td>
    <td style=" padding: 5px 10px 5px 10px; font-size: 16px;">'.$invoice_data["address1"].'</br>'.$invoice_data["address2"].'</br>'.$invoice_data["address3"].'</td>
    <td style="font-weight: bold; padding: 5px 10px 5px 10px; font-size: 16px;">Invoice Number: </td>
    <td style=" padding: 5px 10px 5px 10px; font-size: 16px;">'.$invoice_data["invoice_number"].' </td>
  </tr>
  <tr>
    <td style="font-weight: bold; padding: 5px 10px 5px 10px; font-size: 16px;">Buinding # </td>
    <td style=" padding: 5px 10px 5px 10px; font-size: 16px;">'.$invoice_data["building_name"].' </td>
    <td style="font-weight: bold; padding: 5px 10px 5px 10px; font-size: 16px;">Due Date: </td>
    <td style=" padding: 5px 10px 5px 10px; font-size: 16px;">'.$late_date.' </td>
  </tr>
  <tr>
    <td style="font-weight: bold; padding: 5px 10px 5px 10px; font-size: 16px;">Unit # </td>
    <td style="padding: 5px 10px 5px 10px; font-size: 16px;">'.$unit.'</td>
 
  </tr>
  <tr>
    <td style="font-weight: bold; padding: 5px 10px 20px 10px; font-size: 16px;">City, State Zip</td>
    <td style="padding: 5px 10px 20px 10px; font-size: 16px;">'.$city_state_zip.'</td>
 
  </tr>
</table>
<table width="680" align="center" style="margin-bottom: 20px; " cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="4" align="center" style="background: #00359d; color: #fff;">
      <h4 style="margin: 5px 0; font-weight: 700;">CHARGES</h4>
    </td>
  </tr>
  <tr style="background: #e5f6fe;">
    <th style="text-align: center; font-size: 15px; padding: 5px; font-weight: bold; border-left: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333;">Property </th>
    <th style="text-align: center; font-size: 15px; padding: 5px; font-weight: bold; border-right: 1px solid #333; border-bottom: 1px solid #333;">Description</th>
    <th style="text-align: center; font-size: 15px; padding: 5px; font-weight: bold; border-right: 1px solid #333; border-bottom: 1px solid #333;">Service Period</th>
    <th style="text-align: center; font-size: 15px; padding: 5px; font-weight: bold; border-bottom: 1px solid #333; border-right: 1px solid #333;">Amount</th>
  </tr>';
            foreach ($data1 as $key => $charges_data) {
                $amount += $charges_data['amount'];
                $amount_paid += $charges_data['amount_paid'];
                $html .= '<tr>
    <td style="font-size: 16px; padding: 5px; border-left: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333;">'.$invoice_data["property_name"].' </td>
    <td style="font-size: 16px; padding: 5px; border-right: 1px solid #333; border-bottom: 1px solid #333;">'.$charges_data["description"] .'</td>
    <td style="font-size: 16px; padding: 5px; border-right: 1px solid #333; border-bottom: 1px solid #333;">'.$service_period.'</td>
    <td align="right" style="font-size: 16px; padding: 5px; border-right: 1px solid #333; border-bottom: 1px solid #333;">'.$charges_data["amount"].' </td>
  </tr>';
            }
            $html.='<tr>
    <td align="right" style="font-size: 16px; padding: 5px;" colspan="3"> </td>
    <td align="right" style="font-size: 16px; padding: 5px; background: #e5f6fe; border-left: 1px solid #333; border-bottom: 1px solid #333;">Total:'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].$amount .'</td>
  </tr>
    <tr>
      <td colspan="4" height="20"></td>
    </tr>
  <tr> 
    <td align="right" colspan="2" style="font-size: 16px; padding: 5px;"> </td>
    <td align="right" style="font-size: 16px; padding: 5px; border-left: 1px solid #333; border-top: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333;">Tax</td>
    <td align="right" style="font-size: 16px; padding: 5px; background: #e5f6fe; border-top: 1px solid #333; border-bottom: 1px solid #333;">('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].'0.00) </td>
  </tr>
  <tr>
    <td align="right" colspan="2" style="font-size: 16px; padding: 5px; border-right: 1px solid #333; "> </td>
    <td align="right" style="font-size: 16px; padding: 5px; border-right: 1px solid #333; border-bottom: 1px solid #333;">Total Amount</td>
    <td align="right" style="font-size: 16px; padding: 5px; background: #e5f6fe; border-bottom: 1px solid #333;">('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].$amount.') </td>
  </tr> 
</table>
<table width="680" align="center" style="margin-bottom: 20px;"  cellspacing="0" cellpadding="5">
    <thead>
      <tr>
        <th colspan="4" style="background: #00359d; color: #fff;">
          <h4 style="margin:5px 0; text-align: center; font-weight: 700;"">
            CREDITS
          </h4>
        </th>
      </tr>
      <tr style="background: #e5f6fe;">
        <th style="text-align: center; font-size: 15px; padding: 5px; font-weight: bold; border-left: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333;">Property </th>
        <th style="text-align: center; font-size: 15px; padding: 5px; font-weight: bold; border-left: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333;">Description</th>
        <th style=" text-align: center;font-size: 15px; padding: 5px; font-weight: bold; border-left: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333;">Service Period</th>
        <th style=" text-align: center;font-size: 15px; padding: 5px; font-weight: bold; border-left: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333;">Amount</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td align="right" style="font-size: 16px; padding: 5px;" colspan="3"> </td>
        <td align="right" style="font-size: 16px; padding: 5px; border-left: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333; background: #e5f6fe;">Total:'. $_SESSION[SESSION_DOMAIN]['default_currency_symbol'].$sum.'</td>
      </tr>
      <tr>
        <td align="right" colspan="2" style="font-size: 16px; padding: 5px;"> </td>
        <td align="right" style="font-size: 16px; padding: 5px; border-top: 1px solid #333; border-left: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333;">Total Paid Amount:</td>
        <td align="right" style="font-size: 16px; padding: 5px; border-left: 1px solid #333; border-right: 1px solid #333; border-bottom: 1px solid #333; background: #e5f6fe;">('.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].$amount_paid.') </td>
      </tr> 
    </tbody>
</table>
<table width="680" align="center" border="1" style="margin-bottom: 20px;"  cellspacing="0" cellpadding="5">
    <thead>
      <tr>
        <th colspan="4" style="background: #00359d; color: #fff;">
          <h4 style="margin:5px 0; text-align: center; font-weight: 700;"">
            TOTAL DUE
          </h4>
        </th>
      </tr>
      <tr style="background: #e5f6fe;">
        <th style="font-size: 15px; font-weight: bold; padding: 5px; text-align: right; border-bottom: 1px solid #333;  border-left: 1px solid #333;  border-right: 1px solid #333;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].($amount - $amount_paid).'</th>
      </tr>
    </thead>
 
</table>
<table width="680" align="center" border="0"  cellspacing="0" cellpadding="5">
  <tr>
    <td colspan="2" style="border-top: 1px dashed #000; color: #fff; padding: 10px 1px 1px 1px; text-align: center;">
      
    </td>
  </tr>
  <tr>
    <td style="color: #000; font-style: italic; font-size: 16px; padding: 10px 0;">
      Please detach and send coupon with payment.
    </td>
    <td>
      
    </td>
  </tr>
  <tr>
    <td style="color: #000; font-style: italic; font-size: 16px; padding: 10px 0;">
      <p>Invoice Number :'.$invoice_data["invoice_number"].'</p>
      <p class="invoice_to_name">'.$invoice_to_name.'</p>
      <p class="adress">'.$invoice_data["address1"].'</br>'.$invoice_data["address2"].'</p>
      <p class="city_state_zip">'.$city_state_zip.'</p>
      
      
    </td>
    <td>
      <div style="border: 1px solid #333; padding: 20px;">
        <table width="100%" align="center" border="0"  cellspacing="0" cellpadding="5">
          <tbody>
            <tr>
              <td>Amount Due:</td>
              <td>'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].($amount - $amount_paid).'</td>
            </tr>
            <tr>
              <td>Due Date:</td>
              <td>'.$due_date.'</td>
            </tr>
            <tr>
              <td>Enter Amount of Payment Enclosed.</td>
              <td><input style="border: 1px solid #000; padding: 10px;" type="text" value="'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'].$amount.'"/></td>
            </tr>
           
          </tbody>
        </table>
      </div>
    </td>
  </tr>
   <tr>';
            if($invoice_data['status']== 1) {
                $html .= '<td class="pull-left" ><img width="150" height="79" class="pull-left" src="' . COMPANY_SITE_URL . '/images/paid.jpg"/></td>';
            }
            $html .='<td>
            <p>'.$invoice_data["property_name"].'</p>
            <p>'.$city_state_zip.'</p>
        </td>
    </tr>
</table>';
        }
        $html.='</body>';
        return array('data' => $html, 'status' => 'success','code' => 200);
    }


    /*
* function for fetch property manageers and admin
* */
    public function sendInvoiceEmail() {
        $amount =0.00;
        $ids = $_POST['ids'];
        if (!is_array($ids))
            $ids = array($ids); // if it is just one id not in an array, put it in an array so the rest of the code work for all cases
        $ids = array_map([$this->companyConnection, 'quote'], $ids); // filter elements for SQL injection
        $html = '';
        $data = $this->companyConnection->query("SELECT i.id,i.invoice_number,i.invoice_date,i.late_date,i.invoice_to,i.other_name,i.user_type,i.total_amount,i.amount_due,i.amount_paid,i.status,i.email_invoice,i.no_late_fee,i.other_address,u.id as invoice_to_id,u.email,u.name as invoice_to ,u.address1,u.address2,u.address3,u.city,u.state,u.zipcode  ,tp.property_id,gp.property_name,ud.floor_no,ud.unit_prefix,bd.building_name FROM accounting_invoices as i  LEFT JOIN users as u ON u.id=i.invoice_to LEFT JOIN tenant_property as tp ON tp.user_id=i.invoice_to LEFT JOIN general_property as gp ON gp.id=tp.property_id LEFT JOIN building_detail as bd ON bd.id=tp.building_id LEFT JOIN unit_details as ud ON ud.id=tp.unit_id  WHERE i.id IN (" . implode(', ', $ids) . ")")->fetchAll();


        foreach ($data as $key => $invoice_data) {
            $invoice_to_name = '';
            if($invoice_data['user_type']== 0 || $invoice_data == NULL){
                $invoice_to_name = $invoice_data['other_name'];
            }else{
                $invoice_to_name = $invoice_data['invoice_to'];
            }
            $invoice_date = dateFormatUser($invoice_data['invoice_date'],null, $this->companyConnection);
            $late_date = dateFormatUser($invoice_data['late_date'], null, $this->companyConnection);

            if(empty($late_date)){
                $due_date =  $late_date;
            }else{
                $due_date =  $invoice_date;
            }
            $unit = '';
            if(!empty( $invoice_data['unit_prefix']) || !empty( $invoice_data['floor_no'])) {
                $unit = $invoice_data['unit_prefix'] . '-' . $invoice_data['floor_no'];
            }

            $city_state_zip = '';
            if($invoice_data['user_type']== 0 || $invoice_data == NULL){

            }else{
                if(!empty( $invoice_data['city']) || !empty( $invoice_data['state'])|| !empty( $invoice_data['zipcode'])) {
                    $city_state_zip = $invoice_data['city'] . ',' . $invoice_data['state'] . ' '.$invoice_data['zipcode'] ;
                }
            }
            $month_year = '';
            if(!empty($invoice_data['invoice_date'])){
                $month_year = explode("-",$invoice_data['invoice_date']);
                $service_period = $month_year[0].$month_year[1];
            }

            $data1 = $this->companyConnection->query("SELECT tc.id,tc.charge_code,tc.amount,cc.id as charge_id,cc.charge_code,cc.description FROM tenant_charges as tc LEFT JOIN company_accounting_charge_code as cc ON cc.id=tc.charge_code WHERE tc.invoice_id=".$invoice_data["id"])->fetchAll();

            $sql = "SELECT sum(amount) as amount_sum, count(*) as record_count FROM tenant_charges WHERE invoice_id='".$invoice_data["id"]."' AND status = 1";

            $query = $this->companyConnection->query($sql);
            $row = $query->fetch(PDO::FETCH_ASSOC);
            $sum = 0.00;
            if ($row) {

                $sum = (!empty($row['amount_sum']))?$row['amount_sum']:0.00;
                $count = $row['record_count'];
            }
            $html.='<html>
<head>
<meta charset="utf-8">
<title>Apexlink</title>

</head>

<body style="padding-top:10px; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif; font-size:13px; line-height:19px; margin:0;">

<table width="680" align="center" cellspacing="0" cellpadding="0">
  <tr>
    <td style="background: #00b0f0; height: 30px;">
      
    </td>
  </tr>
  <tr>
    <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
      <img width="200" src="'.COMPANY_SITE_URL.'/images/logo.png"/>
    </td>
  </tr>
  <tr>
    <td style="background: #00b0f0; height: 30px;">
      
    </td>
  </tr>
  <tr>
    <td style="border-left: 1px solid #333; border-right: 1px solid #00b0f0; padding: 0 20px 50px 20px">
         <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <th width="60%" align="left">
              <p style=" margin: 0; padding: 0; font-weight: bold;">ApexLink Inc. </p>
              <p style="margin: 0; padding: 0; font-weight: bold;">Marco Island</p>
              <p style="margin: 0; padding: 0; font-weight: bold;">FL 34145</p>
            </th>
            <th align="left">
            <h2 style="margin: 10px 0 0 0; padding: 0;">Bill To: </h2>
            <h2 style="margin: 0; padding: 0;">'.$invoice_to_name.'</h2> 
              <p style="margin: 0; padding: 0; font-weight: bold;">'.$invoice_data["property_name"].'</p>
              <p style="margin: 0; padding: 0; font-weight: bold;">'.$invoice_data["address1"].'</br>'.$invoice_data["address2"].'</br>'.$invoice_data["address3"].' </p>

            </th>
          </tr>
        </tbody>
      </table>

          <p style="font-size: 20px; color: #333; font-weight: bold; padding: 20px 0; text-align: center;"> Invoice
        </p>
      
      <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <th align="left">
              <p style=" font-weight: bold;">Invoice Number </p>
              <p style="font-weight: normal;">'.$invoice_data["invoice_number"].'</p>
            </th>
            <th align="left"> 
              <p  style=" font-weight: bold;">Date </p>
              <p style="font-weight: normal;">'.$invoice_data["invoice_date"].'</p> 
            </th>
          </tr>
        </tbody>
      </table>
      <table width="100%" style="border: 1px solid #ddd;" cellspacing="0" cellpadding="5">
        <thead>
          <tr>
            <th style="border-right: 1px solid #ececec; background-color: #dfdfdf; text-align: left;">Charge Date: </th>
            <th style="border-right: 1px solid #ececec; background-color: #dfdfdf; text-align: left;">Charge Code: </th>
            <th style="border-right: 1px solid #ececec; background-color: #dfdfdf; text-align: left;">Description: </th>
            <th style=" border-right: 1px solid #ddd;background-color: #dfdfdf; text-align: left;">Amount: </th>

          </tr>
        </thead>
        <tbody>';

            foreach ($data1 as $key => $charges_data) {
                $amount += $charges_data['amount'];

                $html .= '<tr><td style="border-right: 1px solid #ececec; border-bottom: 1px solid #ececec;">'.$invoice_data["invoice_date"].'</td>
          <td style="border-right: 1px solid #ececec; border-bottom: 1px solid #ececec;">'.$charges_data["charge_code"].'</td>
          <td style="border-right: 1px solid #ececec; border-bottom: 1px solid #ececec;">'.$charges_data["description"].'</td>
          <td style="border-bottom: 1px solid #ececec;">'.$charges_data["amount"].'</td>
        </tr>';
            }
            $html .= '<tr>   <td style="font-weight: bold;" colspan="3" align="right">Grand Total </td>
          <td style="font-weight: bold;">'.$amount.'</td>
        </tr>
       
       
      </tbody>
      </table>
      <p style="margin-bottom: 0;">Thank You</p>
    </td>
  </tr>
  <tr>
    <td style="background: #05a0e4; font-weight: bold; color: #fff; padding: 10px; text-align: center;">
      Apexlink Property Manager. support@apexlink.com. 772-212-1950
      
    </td>
  </tr>
</table>

</body>
</html>';
            $this->SendInvoiceMail($html,$invoice_data['email']);
        }
        // dd($result);
        //  return $result;
        return array('status' => 'success','code' => 200);
        exit;
    }


    /*function to send complaint mail */
    public function SendInvoiceMail($html,$email)
    {
        // dd($email);
        try{
            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $email;
            $request['subject'] = 'ApexLink Admin :: Send Tenant Monthly Rent Receipt';
            $request['message'] = $html;
            $request['portal']  = '1';
            curlRequest($request);
            //return ['status'=>'success','code'=>200,'data'=>$request];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }


    public function getTotalAmountDue(){
        try {
            $data ="";
            $invoice_id= $_POST["invoice_id"];
            $type= $_POST["type"];
            $user_id= $_POST["user_id"];
            if($type == 'user') {
                $data = $this->companyConnection->query("SELECT * FROM accounting_manage_charges WHERE user_id=" . $user_id)->fetch();
            }else if($type == 'other' ){
                $data = $this->companyConnection->query("SELECT * FROM accounting_manage_charges WHERE invoice_id=" . $invoice_id)->fetch();
            }
            if(!empty($data)){
            return array('data' => $data , 'status' => 'success','code' => 200);
            }else{
                return array( 'status' => 'failed','code' => 400,'message'=>'Unable to get the record');
            }
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }




}



$Invoice = new Invoice();