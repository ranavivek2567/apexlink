<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");

class buildingDetail extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  function for create a property
     */
    public function addBuilding() {
        try {
            $files = $_FILES;
            $data = $_POST['form'];

            if (isset($data[0]['name']) && $data[0]['name'] == "tenantbuildingpopup"){
                $data = $data;
                unset($data[0]);
            } else {
                $data = serializeToPostArray($data);
            }

            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $popup = postArray($data,"true");
            $data = postArray($data,"false");
            $seprate_unit =false;
            $property_edit_id = $data['property_id'];
            $building_edit_id = $data['building_id'];
            if(isset($data['phone_number']))
            {
                foreach($data['phone_number'] as $key=>$value)
                {

                    if(is_null($value) || $value == '')
                        unset($data['phone_number'][$key]);
                }
            }

            if(isset($data['key_access_codes_info']))
            {
                foreach($data['key_access_codes_info'] as $key=>$value)
                {

                    if(is_null($value) || $value == '')
                        unset($data['key_access_codes_info'][$key]);
                }
            }

            if(isset($data['key_access_code_desc']))
            {
                foreach($data['key_access_code_desc'] as $key=>$value)
                {

                    if(is_null($value) || $value == '')
                        unset($data['key_access_code_desc'][$key]);
                }
            }



            if(isset($data['fax_number'])){
                foreach($data['fax_number'] as $key1=>$value1)
                {
                    if(is_null($value1) || $value1 == '')
                        unset($data['fax_number'][$key1]);
                }
            }

            if (isset($data['tenantbuildingpopup']) && $data['tenantbuildingpopup'] != ""){
                unset($data['tenantbuildingpopup']);
            }


            /*unset the custom keys */
            $custom_keys=[];
            if (!empty($custom_field)){
                $custom_data = json_decode(stripslashes($custom_field));
                $custom_field = [];
                foreach ($custom_data as $key => $value) {
                    $custom_keys[$key] = $value->name;
                    array_push($custom_field,(array) $value);
                }
            }
            $removeKeys = $custom_keys;

            if (!empty($removeKeys)){
                foreach($removeKeys as $key) {
                    unset($data[$key]);
                }
            }
            foreach ($data AS $key => $value) {
                if (stristr($key, 'photoVideoName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }

            foreach ($data AS $key => $value) {

                if (stristr($key, 'imgName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }
            /*unset the custom keys */

            if(isset($data["no_seprate_unit_checkbox"])){
                unset($data["no_seprate_unit_checkbox"]);
                $seprate_unit = true;
            }else{
                $seprate_unit= false;
            }


            /*Required variable array*/
            $required_array = ['building_id', 'building_name'];
            /*Max length variable array*/
            $maxlength_array = ['building_id' => 20, 'building_name' => 30];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                /*declaring additional variables*/
                $id = (isset($data['form_building_edit_id']) && !empty($data['form_building_edit_id'])) ? $data['form_building_edit_id'] : null;
                $property_id = $data['property_id'];
                $property_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $data['property_id']], 'general_property');



                if (!empty($id))
                    unset($data['id']);
                if(!empty($custom_field)){
                    foreach ($custom_field as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                $data['phone_number'] = (isset($data['phone_number']) && !empty($data['phone_number'])?serialize($data['phone_number']):'');
                $data['amenities'] = (isset($data['amenities']) && !empty($data['amenities']))? serialize(@$data['amenities']):'';
                $data['property_id'] = $data['property_id'];
                $data['linked_units'] = 0;
                $data['key_access_codes_info'] = (!empty($data['key_access_codes_info'])) ? serialize((array) $data['key_access_codes_info']) : '';
                $data['key_access_code_desc'] = (isset($data['key_access_code_desc']) && !empty($data['key_access_code_desc'])) ? serialize((array) $data['key_access_code_desc']) : '';
                $data['fax_number'] = (isset($data['fax_number']) && !empty($data['fax_number']))? serialize($data['fax_number']):'';
                $data['custom_field'] = (count($custom_field)>0) ? serialize($custom_field):null;
                $data['status'] = '1';
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data["pet_friendly"] = (isset($data['pet_friendly']) && !empty($data['pet_friendly'])) ? $data['pet_friendly'] : 1;
                $data["smoking_allowed"] = (isset($data['smoking_allowed']) && !empty($data['smoking_allowed'])) ? $data['smoking_allowed'] : 0;

                /*Save Data in Company Database*/
                $sqlData = createSqlColVal($data);
                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_field) > 0) {
                    foreach ($custom_field as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }
                if (empty($id)) {
                    $building_count = $this->companyConnection->query("SELECT COUNT(*) FROM building_detail WHERE property_id ='".$data['property_id']."' AND deleted_at IS NULL")->fetch();
                    $building_count = $building_count['COUNT(*)'];
                    if($property_data['data']['no_of_buildings'] == $building_count ||$building_count > $property_data['data']['no_of_buildings']   ){
                        return array('code' => 502, 'status' => 'error', 'message' => 'Number of Building exceeds Maximum limit !');
                    }

                    if($seprate_unit == true){
                        $this->AddUnit($data,$id,$data['property_id']);
                    }


                    $duplicate = checkNameAlreadyExists($this->companyConnection, 'building_detail', 'building_name', $data['building_name'], "");
                    if ($duplicate['is_exists'] == 1) {
                        if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                            return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Building name allready exists!');
                        }
                    }

                    if (getRecordCount($this->companyConnection, 'building_detail') == 0)
                        ;
                    $query = "INSERT INTO building_detail (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    $elasticSearchData = $data;
                    $elasticSearchData['id'] = $id;
                    $ElasticSearchSave = insertDocument('BUILDING','ADD',$elasticSearchData,$this->companyConnection);

                    if($id){
                        $building_count_after_insertion = $this->companyConnection->query("SELECT COUNT(*) FROM building_detail WHERE property_id ='".$data['property_id']."' AND deleted_at IS NULL")->fetch();
                        $building_count_after_insertion = $building_count_after_insertion['COUNT(*)'];
                        if(!empty($files)) {
                            $domain = getDomain();
                            $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');
                            foreach ($files as $key => $value) {
                                $file_name = $value['name'];
                               /* $fileData = getSingleRecord($this->companyConnection ,['column'=>'file_name','value'=>$file_name], 'building_file_uploads');*/
                                $fileData = getSingleWithAndConditionRecord($this->companyConnection ,['column'=>'file_name','value'=>$file_name], 'building_file_uploads',['column'=>'building_id','value'=>$id]);
                                if($fileData['code'] == '200'){
                                    return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                                }
                                $file_tmp = $value['tmp_name'];
                                $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                                $name = time() . uniqid(rand());
                                $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                /*Check if the directory already exists.*/
                                if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                    /*Directory does not exist, so lets create it.*/
                                    mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                                }
                                move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                                $data = [];
                                $data['building_id'] = $id;
                                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                $data['file_name'] = $file_name;
                                $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                                $data['file_location'] = $path . '/' . $name . '.' . $ext;
                                $data['file_extension'] = $ext;
                                $data['codec'] = $value['type'];
                                if(strstr($value['type'], "video/")){
                                    $type = 3;

                                }else if(strstr($value['type'], "image/")){
                                    $type = 1;
                                }else{
                                    $type = 2;
                                }
                                $data['file_type'] = $type;
                                $data['created_at'] = date('Y-m-d H:i:s');
                                $data['updated_at'] = date('Y-m-d H:i:s');

                                $sqlData = createSqlColVal($data);
                                $query = "INSERT INTO building_file_uploads (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                $stmt = $this->companyConnection->prepare($query);
                                $stmt->execute($data);
                            }
                        }

                        if(isset($popup["key_id"] ) && !empty($popup["key_id"]))
                            $this->addBuildingKeys($popup, $id);
                        if(isset($popup["track_key_holder"] ) && !empty($popup["track_key_holder"]))
                            $this->addTrackKeys($popup, $id);

                    }
                    if(!empty($data['last_renovation_date']))
                        $this->addRenovationDetail($data, $id);
                    return array('code' => 200, 'status' => 'success','last_insert_id'=>$id,'property_data'=>$property_data,'building_count_after_insertion'=>$building_count_after_insertion, 'data' => $stmt, 'message' => 'Record added successfully');
                } else {
                    /*unset fields */
                    $removeKeys = array('form_building_edit_id', 'property_id','building_id','building_id','key_id','key_description','total_keys','checkout_keys','checkout_desc','edit_track_key_id','flag_by','date','flag_name','country_code','flag_phone_number','flag_reason','completed','flag_note','edit_complaint_id','complaint_id','complaint_date','complaint_note','other_notes','edit_key_id','key_holder','default_login_user_name','default_login_user_phone_number');

                    foreach($removeKeys as $key) {
                        unset($data[$key]);
                    }

                    $building_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'building_detail');
                    if ($building_data['code'] == 400) {
                        echo json_encode(array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!'));
                        return;
                    }

                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE building_detail SET " . $sqlData['columnsValuesPair'] . " where id='$id'";

                    $stmt = $this->companyConnection->prepare($query);
                    $updatebuilding=  $stmt->execute();

                    $elasticSearchData = $data;
                    $elasticSearchData['id'] = $id;
                    $elasticSearchData['property_id'] = $property_edit_id;
                    $elasticSearchData['building_id'] = $building_edit_id;
                    $ElasticSearchSave = insertDocument('BUILDING','UPDATE',$elasticSearchData,$this->companyConnection);

                    if($updatebuilding){

                        if(!empty($files)) {
                            $domain = getDomain();
                            $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');
                            foreach ($files as $key => $value) {
                                $file_name = $value['name'];
                                $fileData = getSingleWithAndConditionRecord($this->companyConnection ,['column'=>'file_name','value'=>$file_name], 'building_file_uploads',['column'=>'building_id','value'=>$id]);
                                if($fileData['code'] == '200'){
                                    return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                                }
                                $file_tmp = $value['tmp_name'];
                                $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                                $name = time() . uniqid(rand());
                                $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                /*Check if the directory already exists.*/
                                if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                    /*Directory does not exist, so lets create it.*/
                                    mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                                }
                                move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                                $data = [];
                                $data['building_id'] = $id;
                                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                $data['file_name'] = $file_name;
                                $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                                $data['file_location'] = $path . '/' . $name . '.' . $ext;
                                $data['file_extension'] = $ext;
                                $data['codec'] = $value['type'];
                                if(strstr($value['type'], "video/")){
                                    $type = 3;

                                }else if(strstr($value['type'], "image/")){
                                    $type = 1;
                                }else{
                                    $type = 2;
                                }
                                $data['file_type'] = $type;
                                $data['created_at'] = date('Y-m-d H:i:s');
                                $data['updated_at'] = date('Y-m-d H:i:s');

                                $sqlData = createSqlColVal($data);
                                $query = "INSERT INTO building_file_uploads (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                $stmt = $this->companyConnection->prepare($query);
                                $stmt->execute($data);
                            }
                        }
                    }
                    return array('code' => 200, 'status' => 'success','property_id'=>$property_id, 'message' => 'Record updated Successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    private function addBuildingKeys($data, $id){
        $key_data = [];
        $key_data["building_id"] = $id;
        $key_data["key_id"] = $data["key_id"];
        $key_data["key_description"] = $data["key_description"];
        $key_data["total_keys"] = (isset($data['total_keys']) && !empty($data['total_keys'])) ? $data['total_keys'] : 0;
        $key_data["status"] = 0;
        $key_data["created_at"] = date('Y-m-d H:i:s');
        $key_data["updated_at"] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($key_data);

        $query = "INSERT INTO building_keys (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($key_data);
        $id = $this->companyConnection->lastInsertId();
    }

    /*
     * function to add unit
     */
    private function addUnit($data, $building_id,$property_id){
        $unit_data = [];
        $unit_data["building_id"] = $building_id;
        $unit_data["property_id"] = $property_id;
        $unit_data["user_id"] = $_SESSION[SESSION_DOMAIN]['cuser_id'];;
        $unit_data["floor_no"] = 1;
        $unit_data["unit_no"] = 1;
        $unit_data["unit_prefix"] = $data["building_name"];
        $unit_data["unit_type_id"] = $data["unit_type"];
        $unit_data["base_rent"] = $data["base_rent"];
        $unit_data["market_rent"] = $data["market_rent"];
        $unit_data["security_deposit"] = $data["security_deposit"];
        $unit_data["smoking_allowed"] = $data["smoking_allowed"];

        $unit_data["bedrooms_no"] = 1;
        $unit_data["bathrooms_no"] = 1;
        $unit_data["total_keys"] = (isset($data['total_keys']) && !empty($data['total_keys'])) ? $data['total_keys'] : 0;
        $unit_data["created_at"] = date('Y-m-d H:i:s');
        $unit_data["updated_at"] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($unit_data);
        /*Save Data in Company Database*/

        $query = "INSERT INTO unit_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($unit_data);
        $id = $this->companyConnection->lastInsertId();
    }

    private function addTrackKeys($data, $id){
        $key_data = [];
        $key_data["building_id"] = $id;
        $key_data["key_holder"] = $data["track_key_holder"];
        $key_data["email"] = (isset($data['email']) && !empty($data['email'])) ? $data['email'] : '';
        $key_data["company_name"] = (isset($data['company_name']) && !empty($data['company_name'])) ? $data['company_name'] : '';
        $key_data["phone"] = (isset($data['company_name']) && !empty($data['phone'])) ? $data['phone'] : '';
        $key_data["Address1"] = (isset($data['Address1']) && !empty($data['Address1'])) ? $data['Address1'] : '';
        $key_data["Address2"] = (isset($data['Address2']) && !empty($data['Address2'])) ? $data['Address2'] : '';
        $key_data["Address3"] = (isset($data['Address3']) && !empty($data['Address3'])) ? $data['Address3'] : '';
        $key_data["Address4"] = (isset($data['Address4']) && !empty($data['Address4'])) ? $data['Address4'] : '';
        $key_data["key_number"] = (isset($data['key_number']) && !empty($data['key_number'])) ? $data['key_number'] : '';
        $key_data["key_quality"] = (isset($data['key_quality']) && !empty($data['key_quality'])) ? $data['key_quality'] : '';
        $key_data["pick_up_date"] = (isset($data['pick_up_date']) && !empty($data['pick_up_date'])) ?  mySqlDateFormat($data['pick_up_date'],null,$this->companyConnection) : NULL ;
        $key_data["pick_up_time"] = (isset($data['pick_up_time']) && !empty($data['pick_up_time'])) ? mySqlTimeFormat($data['pick_up_time']) : '';
        $key_data["return_date"] = (isset($data['return_date']) && !empty($data['return_date'])) ? mySqlDateFormat($data['return_date'],null,$this->companyConnection) : NULL;
        $key_data["return_time"] = (isset($data['return_time']) && !empty($data['return_time'])) ? mySqlTimeFormat($data['return_time']) : '';
        $key_data["key_designator"] = (isset($data['key_designator']) && !empty($data['key_designator'])) ? $data['key_designator'] : '';
        $key_data["created_at"] = date('Y-m-d H:i:s');
        $key_data["updated_at"] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($key_data);
        /*Save Data in Company Database*/

        $query = "INSERT INTO building_track_key (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($key_data);
        $id = $this->companyConnection->lastInsertId();
    }

    private function addRenovationDetail($data, $id){
        $renovation_data = [];
        $renovation_data["object_id"] = $id;
        $renovation_data["object_type"] = 'building';
        $renovation_data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $renovation_data['status'] = 1;
        $renovation_data["last_renovation_date"] = (isset($data['last_renovation_date']) && !empty($data['last_renovation_date'])) ? mySqlDateFormat($data['last_renovation_date'],null,$this->companyConnection) : NULL;
        $renovation_data["last_renovation_time"] = (isset($data['last_renovation_time']) && !empty($data['last_renovation_time'])) ? mySqlTimeFormat($data['last_renovation_time']) : '';
        $renovation_data["last_renovation_description"] = (isset($data['last_renovation_description']) && !empty($data['last_renovation_description'])) ? $data['last_renovation_description'] : '';
        $renovation_data["created_at"] = date('Y-m-d H:i:s');
        $renovation_data["updated_at"] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($renovation_data);
        /*Save Data in Company Database*/

        $query = "INSERT INTO  renovation_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($renovation_data);
        $id = $this->companyConnection->lastInsertId();
    }

    public function deleteKey() {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'building_keys');

            $sql = "UPDATE building_keys SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            return ['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.'];

        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function deleteTrackKey() {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'building_track_key');

            $sql = "UPDATE building_track_key SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            return ['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.'];
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function getKey() {
        try {
            $id = $_POST['id'];
            $sql = "SELECT * FROM key_tracker WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
            } else {
                return array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }

    /**
     *  @return array|void
     * function to get login company data
     */

    public function getCompanyData() {
        try {
            $id= $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $building_id = $_POST['building_id'];
            $data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'users');
            $building_detail = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $building_id], 'building_detail');
            return array('status' => 'success', 'code' => 200, 'data' => $data,'building'=>$building_detail, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    /**
     * @return array|void
     * function to return complaints envelope data
     */
    public function getComplaintsData() {
        try {

            $complaint_ids= $_POST['complaint_ids'];
            $complaint_idss  = str_repeat('?,', count($complaint_ids) - 1) . '?';
            $stmt = $this->companyConnection->prepare("Select * FROM `complaints` as c 
            LEFT JOIN building_detail as b ON b.id=c.object_id AND c.module_type='building' 
            LEFT JOIN complaint_types as ct ON ct.id=c.complaint_type_id
            WHERE c.id IN ($complaint_idss)");
            $stmt->execute($complaint_ids);
            $data = $stmt->fetchAll();
            $stmt = $this->companyConnection->prepare("Select * FROM `building_detail` WHERE `id` IN ($complaint_idss)");
            $print_complaints_html = '';
            $company_logo = SITE_URL."company/images/logo.png";
            foreach($data as $key => $value) {
                $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
    <tr>
      <td style="background: #00b0f0; height: 30px;">
        
      </td>
    </tr>
    <tr>
      <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
        <img width="200" src="'.$company_logo.'"/>
      </td>
    </tr>
    <tr>
      <td style="background: #00b0f0; height: 30px;">
        
      </td>
    </tr>
    <tr>
      <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
        <table width="100%" align="center" cellspacing="0" cellpadding="0">
          <tr>
            <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> Complaint by Building </td>
          </tr>
        </table>
        <table width="100%" align="center" border="1" cellspacing="0" cellpadding="5">
          <tr>
            <td>Address: </td>
            <td>' .$value["building_name"].'</td>
          </tr>
          <tr>
            <td>Name: </td>
            <td>'.$value["address"].'</td>
          </tr>
          <tr>
            <td>Complaint Type: </td>
            <td>'.$value["complaint_type"].'</td>
          </tr>
          <tr>
            <td>Description: </td>
            <td>'.$value['complaint_note'].'</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="background: #585858; color: #fff; padding: 10px; text-align: center;">
        Apexlink.apexlink@yopmail.com
        
      </td>
    </tr>
  </table>';
            }


            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }



    /**
     * Get Building Detail
     */
    public function getBuildingDetail() {

        try {
            $id= $_POST["id"];
            $renovation_detail_html="";
            $school_district_html ="";
            $building_images_slider="";

            $sql = "SELECT building_detail.*,company_pet_friendly.pet_friendly as pet_friendly, general_property.property_id as property_id, building_detail.property_id as building_property_id, general_property.property_name as property_name, general_property.school_district_municipality as property_school_district_municipality, general_property.school_district_code as property_school_district_code, general_property.school_district_notes as property_school_district_notes , building_detail.address as building_address 
                    FROM building_detail 
                    LEFT JOIN renovation_details ON building_detail.id=renovation_details.object_id AND renovation_details.object_type='building'
                    LEFT JOIN general_property ON building_detail.property_id=general_property.id
                    LEFT JOIN company_pet_friendly ON building_detail.pet_friendly=company_pet_friendly.id
                    WHERE building_detail.id=".$id ;

            $stmt = $this->companyConnection->prepare($sql);
            $data = [];
            $images_data =[];
            $renovation_detail_data =[];
            $custom_data=[];
            if($stmt->execute())
            {
                $data =$stmt->fetch();
                $stmt->closeCursor();

                if(!empty($data)){

                    if(!empty($data["custom_field"])){
                        $custom_data = unserialize($data["custom_field"]);
                    }
                    if(isset($data["phone_number"]) && !empty($data["phone_number"])) {

                        $phone_number = unserialize($data["phone_number"]);
                        $data["phone_number"] = implode($phone_number, ", ");
                    }else{
                        $data["phone_number"] = "";
                    }
                    if(isset($data["fax_number"]) && !empty($data["fax_number"])) {
                        $fax_number = unserialize($data["fax_number"]);
                        $data["fax_number"]= implode($fax_number,", ");
                    }else{
                        $data["fax_number"] = "";
                    }

                    if(isset($data["amenities"]) && !empty($data["amenities"])) {
                        $newamenities='';
                        $amenities = unserialize($data["amenities"]);
                        if(!empty($amenities)){
                            foreach ($amenities as $key=>$value){
                                $query = 'SELECT name FROM company_property_amenities WHERE id='.$value;
                                $groupData = $this->companyConnection->query($query)->fetch();
                                $name = $groupData['name'];
                                $newamenities .= $name.',';
                            }
                            $data["amenities"] = $newamenities;
                            $data["saved_amenities"] = $amenities;
                        }else{
                            $data["amenities"] = "N/A";
                            $data["saved_amenities"] = [];
                        }
                    }else{

                        $data["amenities"] = "N/A";
                        $data["saved_amenities"] = [];
                    }



                    if($data["smoking_allowed"] == 0){
                        $data["smoking_allowed"] = "No";
                    }else{
                        $data["smoking_allowed"] = "Yes";
                    }
                    $sql1 = "SELECT * FROM building_file_uploads WHERE file_type=1 AND building_id=".$id ;
                    $stmt1 = $this->companyConnection->prepare($sql1);

                    if($stmt1->execute())
                    {
                        $images_data =$stmt1->fetchAll();
                    }
                    $images_count = count($images_data);
                    if($images_count<=3){
                        $newdata =  array (
                            'file_name' => 'no-image.png',
                            'file_location' => 'images/no-image.png'
                        );

                        for($i=$images_count ;$i<3;$i++){
                            $images_data[$i] =   $newdata;
                        }



                    }
                    $school_district_muncipiality=[];
                    $school_district_code=[];
                    $school_district_notes=[];
                    $result=[];
                    if(isset($data["property_school_district_municipality"]) && !empty($data["property_school_district_municipality"]))
                        $school_district_muncipiality = unserialize($data["property_school_district_municipality"]);
                    if(isset($data["property_school_district_code"]) && !empty($data["property_school_district_code"]))
                        $school_district_code = unserialize($data["property_school_district_code"]);
                    if(isset($data["property_school_district_notes"]) && !empty($data["property_school_district_notes"]))
                        $school_district_notes = unserialize($data["property_school_district_notes"]);
                    $result = array_map(function ($name, $type, $price) {
                        return array_combine(
                            ['school_district', 'code', 'notes'],
                            [$name, $type, $price]
                        );
                    }, $school_district_muncipiality, $school_district_code, $school_district_notes);
                    if(!empty($result)) {
                        foreach ($result as $key1 => $value1) {
                            $school_district_html .= "<tr>";
                            $school_district_html .= "<td>" .$value1["school_district"]."</td>";
                            $school_district_html .= "<td>" . $value1["code"] . "</td>";
                            $school_district_html .= "<td>" . $value1["notes"] . "</td>";
                            $school_district_html .= "</tr>";
                        }
                    }

                    /*query to fetch renovation details */

                    $sql2 = "SELECT * FROM renovation_details WHERE object_type='building' AND object_id=".$id ;
                    $stmt2 = $this->companyConnection->prepare($sql2);
                    if($stmt2->execute())
                    {
                        $renovation_detail_data =$stmt2->fetchAll();
                    }
                    foreach($renovation_detail_data as $key=>$value) {
                        if(!empty($value['last_renovation_date'])){

                            $renovation_date = dateFormatUser($value['last_renovation_date'],null,$this->companyConnection);
                        }else{
                            $renovation_date ="";
                        }

                        if(!empty($value['last_renovation_time'])){
                            $renovation_time = timeFormat($value['last_renovation_time'], null,$this->companyConnection);
                        }else{
                            $renovation_time ="";
                        }
                        $renovation_detail_html .= "<tr>";
                        $renovation_detail_html .= "<td>".$renovation_date."</td>";
                        $renovation_detail_html .= "<td>".$renovation_time."</td>";
                        $renovation_detail_html .= "<td>".$value['last_renovation_description']."</td>";
                        $renovation_detail_html .= "</tr>";
                    }

                    $string = '';
                    if(!empty($data['key_access_codes_info'])) {
                        $string = '';
                        $key_info_data = unserialize($data['key_access_codes_info']);
                        $key_description_data = unserialize($data['key_access_code_desc']);
                     /*   $key_info_new_data = array_combine($key_description_data, $key_info_data);*/
                        $info_data_count = count($key_info_data);
                        $i=1;
                        foreach ($key_info_data as $key => $value) {

                            $info_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $value], 'company_key_access');
                            $string .= $info_data['data']['key_code'] . ' : ' . $key_description_data[$key];
                            $string .= ($i < $info_data_count) ? ' , ' : ' ';
                            $i++;

                        }
                    }


                    return array('status' => 'success', 'code' => 200, 'data' => @$data,'renovation_detail'=>@$renovation_detail_html,'school_district_muncipiality_data'=>@$school_district_html,'images_data'=>@$images_data,'images_html'=>@$building_images_slider,'custom_data'=>$custom_data,'key_access_codes_info_string'=>@$string, 'message' => 'Record fetched successfully.');
                }
                else{
                    return array('status' => 'success', 'code' => 200, 'data' => [],'school_district_muncipiality_data'=>[], 'message' => 'Record fetched successfully.');
                }
            }

        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    /**
     *  Deactivate Building
     */
    public function deactivate(){
        try{
            $building_id = $_REQUEST['id'];
            $status= 0;
            $sql = "UPDATE building_detail SET status=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status,$building_id]);
            return ['status'=>'success','code'=>200,'message'=>'Building deactivated successfully.'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  Delete Building
     */
    public function delete(){
        try{
            $building_id = $_REQUEST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE building_detail SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$building_id]);
            return ['status'=>'success','code'=>200,'data'=>'Building deleted successfully.'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  Delete Building
     */
    public function deleteComplaint(){
        try{
            $building_id = $_REQUEST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE complaints SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$building_id]);
            return ['status'=>'success','code'=>200,'data'=>'Complaint deleted successfully.'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }




    /**
     *  Delete Building
     */
    public function deleteFile(){
        try{
            $file_id= $_REQUEST['id'];
            $data = date('Y-m-d H:i:s');
            $data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $file_id], 'building_file_uploads');
            if(!empty($data["data"])){
                $count=$this->companyConnection->prepare("DELETE FROM building_file_uploads WHERE id=:id");
                $count->bindParam(":id",$file_id,PDO::PARAM_INT);
                $count->execute();
                return ['status'=>'success','code'=>200,'message'=>'File deleted successfully.'];
            }else{
                return ['status'=>'failed','code'=>503,'message'=>'File not deleted successfully'];
            }

        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  Activate Company
     */
    public function activate(){
        try{
            $building_id = $_REQUEST['id'];
            $status = 1;
            $sql = "UPDATE building_detail SET status=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status,$building_id]);
            return ['status'=>'success','code'=>200,'data'=>'Property type activated successfully.'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }


    /**
     * function to add new renovation
     *
     *
     */

    public function  addNewRenovation(){
        try {
            $data = $_POST['form'];
            $data = postArray($data,"false");
            $renovation_data = [];
            $renovation_data["object_id"] = $data["edit_building_id"];
            $renovation_data["object_type"] = 'building';
            $renovation_data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $renovation_data['status'] = 1;
            $renovation_data["last_renovation_date"] = (isset($data['new_renovation_date']) && !empty($data['new_renovation_date'])) ? mySqlDateFormat($data['new_renovation_date'],null,$this->companyConnection) : NULL;
            $renovation_data["last_renovation_time"] = (isset($data['new_renovation_time']) && !empty($data['new_renovation_time'])) ? mySqlTimeFormat($data['new_renovation_time']) : '';
            $renovation_data["last_renovation_description"] = (isset($data['new_renovation_description']) && !empty($data['new_renovation_description'])) ? $data['new_renovation_description'] : '';
            $renovation_data["created_at"] = date('Y-m-d H:i:s');
            $renovation_data["updated_at"] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($renovation_data);

            $query = "INSERT INTO  renovation_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($renovation_data);
            $id = $this->companyConnection->lastInsertId();
            if($id){
                return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully');
            }else{
                return array('code' => 400, 'status' => 'error', 'message' => 'Record Not Added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $e->getMessage()));
            return;
        }

    }

    /**
     * function to add new renovation
     *
     *
     */

    public function  addNewComplaint(){
        try {
            $data = $_POST['form'];
            $data = postArray($data,"false");
            $building_id = $_POST["edit_building_id"];
            $complaint_id = $data["edit_complaint_id"];
            if(isset($complaint_id) && empty($complaint_id)) {
                $complaint_data = [];
                $complaint_data["object_id"] = $building_id;
                $complaint_data["module_type"] = 'building';
                $complaint_data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $complaint_data['status'] = 1;
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : NULL;
                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : NULL;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["created_at"] = date('Y-m-d H:i:s');
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($complaint_data);

                $query = "INSERT INTO  complaints (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($complaint_data);
                $id = $this->companyConnection->lastInsertId();
                if ($id) {
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully');
                } else {
                    return array('code' => 400, 'status' => 'error', 'message' => 'Record Not Added successfully');
                }
            } else{
                $complaint_data = [];
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : NULL;
                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : NULL;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($complaint_data);
                $query = "UPDATE complaints SET " . $sqlData['columnsValuesPair'] . " where id='$complaint_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');

            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $e->getMessage()));
            return;
        }

    }

    public function getComplaintDetail() {
        try {
            $id = $_POST['id'];
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'complaints');
            if(!empty($record["data"]["complaint_date"])){
                $record["data"]["complaint_date"] =  dateFormatUser($record["data"]["complaint_date"],null,$this->companyConnection);
            }
            return ['status' => 'success', 'code' => 200,'data'=>$record, 'message' => 'Record deleted successfully.'];
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }


    public function addKeytag() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            /*Required variable array*/
            $required_array = ['key_tag', 'description', 'total_keys'];
            /*Max length variable array*/
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                if(isset($data['edit_key_id']) && empty($data['edit_key_id']) ):

                    $id = (isset($data['edit_key_id']) && !empty($data['edit_key_id'])) ? $data['edit_key_id'] : null;
                    $duplicate = checkNameAlreadyExists($this->companyConnection, 'building_keys', 'key_id', $data['key_id'], $id);
                    if ($duplicate['is_exists'] == 1) {
                        if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                            return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Key Tag allready exists!');
                        }
                    }
                    $keys_data = [];
                    $keys_data["key_id"] = (isset($data['key_id']) && !empty($data['key_id'])) ? $data['key_id'] : '';
                    $keys_data["key_description"] = (isset($data['key_description']) && !empty($data['key_description'])) ? $data['key_description'] : NULL;
                    $keys_data["total_keys"] = (isset($data['total_keys']) && !empty($data['total_keys'])) ? $data['total_keys'] : NULL;
                    $keys_data["updated_at"] = date('Y-m-d H:i:s');
                    $keys_data['building_id'] = $_POST['edit_building_id'];
                    $keys_data['available_keys'] = $data['total_keys'];
                    $keys_data['status'] = 1;
                    unset($data['id']);
                    $sqlData = createSqlColVal($keys_data);
                    /*Save Data in Company Database*/
                    $query = "INSERT INTO  building_keys (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($keys_data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                else:

                    $chekout_sql = 'SELECT SUM(checkout_keys) AS total_checkout_keys FROM building_key_checkout WHERE key_id='.$data['edit_key_id'];
                    /* $row = $chekout_sql->fetch(PDO::FETCH_ASSOC);*/

                    $stmt = $this->companyConnection->prepare($chekout_sql);
                    $stmt->execute();
                    $total_checkout_keys=0;
                    while($row1 = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $total_checkout_keys += $row1['total_checkout_keys'];
                    }

                    /*$checkout_keys = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $data['edit_key_id']], 'building_keys');
                    $data['total_keys'] = $key_data['data']['available_keys'];*/
                    $id = (isset($data['edit_key_id']) && !empty($data['edit_key_id'])) ? $data['edit_key_id'] : null;
                    $duplicate = checkNameAlreadyExists($this->companyConnection, 'building_keys', 'key_id', $data['key_id'], $id);
                    if ($duplicate['is_exists'] == 1) {
                        if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                            return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Key Tag allready exists!');
                        }
                    }
                    $keytag = $data['key_id'];
                    $keydesc = $data['key_description'];
                    $keytotal = $data['total_keys'];
                    $availablekeys = $data['total_keys'] - $total_checkout_keys;
                    $updated_at = date('Y-m-d H:i:s');
                    if($availablekeys < 0){
                        return array('code' => 400, 'status' => 'error', 'message' => 'You cannot update the key count less than the allocated keys.');
                    }
                    $query = "UPDATE building_keys SET key_id='$keytag',key_description='$keydesc',total_keys='$keytotal',available_keys='$availablekeys', updated_at='$updated_at' where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated Successfully');
                endif;
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function getTrackKey() {
        try {
            $id = $_POST['id'];
            /*check if custom field allready exists or not*/
            $sql = "SELECT * FROM building_track_key WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
            } else {
                return array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }

    public function getbuildingKey() {
        try {
            $id = $_POST['id'];
            /*check if custom field allready exists or not*/
            $sql = "SELECT * FROM building_keys WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
            } else {
                return array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }

    public function keyCheckout() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $building_id = $_POST['building_id'];
            $id = $_POST['id'];
            /*Required variable array*/
            $required_array = [];
            /*Max length variable array*/
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            $queryFetch = $this->companyConnection->query("SELECT * FROM  building_keys WHERE id=$id");
            $key = $queryFetch->fetch();
            if ($key['available_keys'] >= $data['checkout_keys']) {
                $data['available_keys'] = $key['available_keys'] - $data['checkout_keys'];
                $data['key_id'] = $id;
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO   building_key_checkout (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $queryupd = "UPDATE  building_keys SET available_keys='" . $data['available_keys'] . "'  where id=$id";
                $stmtupd = $this->companyConnection->prepare($queryupd);
                $stmtupd->execute();

                $queryupd1 = "UPDATE  building_key_checkout SET available_keys='" . $data['available_keys'] . "'  where key_id=$id";
                $stmtupd1 = $this->companyConnection->prepare($queryupd1);
                $stmtupd1->execute();

                $queryFetchcheckout = $this->companyConnection->query("SELECT * FROM  building_key_checkout WHERE key_id=" . $key['id'] . "");
                $keycheckout = $queryFetchcheckout->fetch();
                if (!empty($keycheckout)) {
                    $querystatus = "UPDATE  building_keys SET status='1'  where id=$id";
                    $stmtstatus = $this->companyConnection->prepare($querystatus);
                    $stmtstatus->execute();
                }

                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated Successfully');
            } else {
                return array('code' => 500, 'status' => 'success', 'message' => 'Cannot checkout more than available keys');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function getReturnkeyData() {
        try {
            $html = '';
            $id = $_POST['building_id'];
            $keytracker = "SELECT * FROM  building_keys WHERE building_id=$id";
            $reskeytracker = $this->companyConnection->query($keytracker)->fetch();
            if ($reskeytracker['total_keys'] == $reskeytracker['available_keys']) {
                $querystatus = "UPDATE  building_keys SET status='0' WHERE building_id=$id";
                $stmtstatus = $this->companyConnection->prepare($querystatus);
                $stmtstatus->execute();
            }

            $keycheckout = "SELECT * FROM  building_key_checkout WHERE key_id='" . $reskeytracker['id'] . "'  and checkout_keys !=0";
            $reskeycheckout = $this->companyConnection->query($keycheckout)->fetchAll();
            foreach ($reskeycheckout as $keydetail) {
                $keyholderdetail = $this->companyConnection->query("SELECT * FROM  users WHERE id='" . $keydetail['key_holder'] . "'");
                $reskey = $keyholderdetail->fetch();
                $keydetailid = $keydetail['id'];
                $name = ucfirst($reskey['name']);
                $keydetailcheckoutkeys = $keydetail['checkout_keys'];

                $html .="<tr class='clsCheckOut'>";
                $html .="<td class='text-center'>";
                $html .="<input type='checkbox' class='chkKeyReturn' value=$keydetailid >";
                $html .="</td>";
                $html .="<td>$name</td><td class='clstd2'>Staff</td><td> $keydetailcheckoutkeys</td></tr>";
            }

            return array('code' => 200, 'html' => $html);
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function returnCheckout() {
        try {
            $val = $_POST['val'];
            $building_id = $_POST['building_id'];
            /*Required variable array*/
            $required_array = [];
           /* Max length variable array*/
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = [];




            foreach ($val as $returnkey) {
                $queryreturnkey = $this->companyConnection->query("SELECT * FROM  building_key_checkout WHERE id=$returnkey");
                $resreturnkey = $queryreturnkey->fetch();
                $keytrackerid = $resreturnkey['key_id'];
                $fetchkeytracker = $this->companyConnection->query("SELECT * FROM  building_keys WHERE id=$keytrackerid");
                $Reskeytracker = $fetchkeytracker->fetch();

                $Latestavailkey = $resreturnkey['checkout_keys'] + $Reskeytracker['available_keys'];
                $updkeycheckout = "UPDATE  building_key_checkout SET available_keys=$Latestavailkey ,checkout_keys=0 where id=$returnkey";
                $stmtupd = $this->companyConnection->prepare($updkeycheckout);
                $stmtupd->execute();

                $keyid = $resreturnkey['key_id'];
                $updkeytracker = "UPDATE  building_keys SET available_keys=$Latestavailkey  where id=$keyid";
                $stmttrackerupd = $this->companyConnection->prepare($updkeytracker);
                $stmttrackerupd->execute();

                if ($Reskeytracker['total_keys'] == $Reskeytracker['available_keys']) {
                    $querystatus = "UPDATE  building_keys SET status='0' WHERE id=$keytrackerid";
                    $stmtstatus = $this->companyConnection->prepare($querystatus);
                    $stmtstatus->execute();
                }
            }
            return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }


    public function editTrackKeys(){
        try {
            $data = $_POST['form'];
            $data = postArray($data,"true");
            $building_id = $_POST["edit_building_id"];

            //Required variable array
            $required_array = [];
            //Max length variable array
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['edit_track_key_id']) && empty($data['edit_track_key_id']) ):
                    $key_data = [];
                    $key_data["building_id"] = $building_id;
                    $key_data["key_holder"] = $data["track_key_holder"];
                    $key_data["email"] = (isset($data['email']) && !empty($data['email'])) ? $data['email'] : '';
                    $key_data["company_name"] = (isset($data['company_name']) && !empty($data['company_name'])) ? $data['company_name'] : '';
                    $key_data["phone"] = (isset($data['company_name']) && !empty($data['phone'])) ? $data['phone'] : '';
                    $key_data["Address1"] = (isset($data['Address1']) && !empty($data['Address1'])) ? $data['Address1'] : '';
                    $key_data["Address2"] = (isset($data['Address2']) && !empty($data['Address2'])) ? $data['Address2'] : '';
                    $key_data["Address3"] = (isset($data['Address3']) && !empty($data['Address3'])) ? $data['Address3'] : '';
                    $key_data["Address4"] = (isset($data['Address4']) && !empty($data['Address4'])) ? $data['Address4'] : '';
                    $key_data["key_number"] = (isset($data['key_number']) && !empty($data['key_number'])) ? $data['key_number'] : '';
                    $key_data["key_quality"] = (isset($data['key_quality']) && !empty($data['key_quality'])) ? $data['key_quality'] : '';
                    $key_data["pick_up_date"] = (isset($data['pick_up_date']) && !empty($data['pick_up_date'])) ?  mySqlDateFormat($data['pick_up_date'],null,$this->companyConnection) : NULL ;
                    $key_data["pick_up_time"] = (isset($data['pick_up_time']) && !empty($data['pick_up_time'])) ? mySqlTimeFormat($data['pick_up_time']) : '';
                    $key_data["return_date"] = (isset($data['return_date']) && !empty($data['return_date'])) ? mySqlDateFormat($data['return_date'],null,$this->companyConnection) : NULL;
                    $key_data["return_time"] = (isset($data['return_time']) && !empty($data['return_time'])) ? mySqlTimeFormat($data['return_time']) : '';
                    $key_data["key_designator"] = (isset($data['key_designator']) && !empty($data['key_designator'])) ? $data['key_designator'] : '';
                    $key_data["created_at"] = date('Y-m-d H:i:s');
                    $key_data["updated_at"] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($key_data);
                    //Save Data in Company Database

                    $query = "INSERT INTO building_track_key (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($key_data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                else:
                    $track_key_id = (isset($data['edit_track_key_id']) && !empty($data['edit_track_key_id'])) ? $data['edit_track_key_id'] : null;
                    $key_data = [];
                    $key_data["key_holder"] = $data["track_key_holder"];
                    $key_data["email"] = (isset($data['email']) && !empty($data['email'])) ? $data['email'] : '';
                    $key_data["company_name"] = (isset($data['company_name']) && !empty($data['company_name'])) ? $data['company_name'] : '';
                    $key_data["phone"] = (isset($data['company_name']) && !empty($data['phone'])) ? $data['phone'] : '';
                    $key_data["Address1"] = (isset($data['Address1']) && !empty($data['Address1'])) ? $data['Address1'] : '';
                    $key_data["Address2"] = (isset($data['Address2']) && !empty($data['Address2'])) ? $data['Address2'] : '';
                    $key_data["Address3"] = (isset($data['Address3']) && !empty($data['Address3'])) ? $data['Address3'] : '';
                    $key_data["Address4"] = (isset($data['Address4']) && !empty($data['Address4'])) ? $data['Address4'] : '';
                    $key_data["key_number"] = (isset($data['key_number']) && !empty($data['key_number'])) ? $data['key_number'] : '';
                    $key_data["key_quality"] = (isset($data['key_quality']) && !empty($data['key_quality'])) ? $data['key_quality'] : '';
                    $key_data["pick_up_date"] = (isset($data['pick_up_date']) && !empty($data['pick_up_date'])) ?  mySqlDateFormat($data['pick_up_date'],null,$this->companyConnection) : NULL ;
                    $key_data["pick_up_time"] = (isset($data['pick_up_time']) && !empty($data['pick_up_time'])) ? mySqlTimeFormat($data['pick_up_time']) : '';
                    $key_data["return_date"] = (isset($data['return_date']) && !empty($data['return_date'])) ? mySqlDateFormat($data['return_date'],null,$this->companyConnection) : NULL;
                    $key_data["return_time"] = (isset($data['return_time']) && !empty($data['return_time'])) ? mySqlTimeFormat($data['return_time']) : '';
                    $key_data["key_designator"] = (isset($data['key_designator']) && !empty($data['key_designator'])) ? $data['key_designator'] : '';
                    $key_data["updated_at"] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($key_data);
                    $query = "UPDATE building_track_key SET " . $sqlData['columnsValuesPair'] . " where id='$track_key_id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                endif;
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    /*function to send complaint mail */
    public function SendComplaintMail()
    {
        $data= $_POST;
        $body =$data['data_html'];
        $domain = explode('.', $_SERVER['HTTP_HOST']);
        $subdomain = array_shift($domain);
        $user_detail = $_SESSION['SESSION_'.$subdomain];
        try{
            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $user_detail['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal']  = '1';
            curlRequest($request);
            return ['status'=>'success','code'=>200,'data'=>$request];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /*inspection add area*/
    public function addInspectionAreaModule(){
        try{
            $dataAll=[];
            $data = $_POST['form'];
            $data = postArray($data);
            $data1['inspection_area'] = $data['inspection_area_name'];
            $data1['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data1['object_id'] = $_REQUEST['id'];
            $data1['parent_id'] = 0;
            $data1['object_type'] = 'Building Inspection';
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $dataAll['parent'] =$data1;
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO inspection_area (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            $inspectionArea_id = $this->companyConnection->lastInsertId();
            $subAreaData1 = [];

            $subInspectionAreaName=$data['sub_inspection_area_name'];

            if (!empty($subInspectionAreaName) ) {
                if(is_array ($subInspectionAreaName)){
                    if (count($subInspectionAreaName) > 0) {
                        for ($i = 0; $i < count($subInspectionAreaName); $i++) {
                            $subAreaData['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $subAreaData['inspection_area'] = $subInspectionAreaName[$i];
                            $subAreaData['parent_id'] = $inspectionArea_id;
                            $subAreaData['object_id'] = $data['object_id'];
                            $subAreaData['object_type'] = $data['object_type'];
                            $subAreaData['created_at'] = date('Y-m-d H:i:s');
                            $subAreaData['updated_at'] = date('Y-m-d H:i:s');
                            $sqlData = createSqlColVal($subAreaData);
                            $query = "INSERT INTO inspection_area (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                            $stmt = $this->companyConnection->prepare($query);
                            $stmt->execute($subAreaData);
                            $subAreaData1[] = $subAreaData;

                        }
                    }
                }else{
                    $subAreaData['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $subAreaData['inspection_area'] = $subInspectionAreaName;
                    $subAreaData['parent_id'] = $inspectionArea_id;
                    $subAreaData['object_id'] = $data['object_id'];
                    $subAreaData['object_type'] = $data['object_type'];
                    $subAreaData['created_at'] = date('Y-m-d H:i:s');
                    $subAreaData['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($subAreaData);
                    $query = "INSERT INTO inspection_area (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($subAreaData);
                    $subAreaData1 = $subAreaData;

                }
                $dataAll['child'] =$subAreaData1;
            }

            return ['status'=>'success','code'=>200,'data'=>$dataAll,'message'=>'Record Saved successfully'];



        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }



    /**
     *  Activate Company
     */
    public function addInspectionArea(){
        try{
            $dataAll=[];
            $data = $_POST['form'];
            $data = postArray($data);
            $data1['inspection_area'] = $data['inspection_area_name'];
            $data1['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data1['object_id'] = $_REQUEST['id'];
            $data1['parent_id'] = 0;
            //$data1['object_type'] = 'Building Inspection';
            $data1['object_type'] = $data['object_type'];
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $dataAll['parent'] =$data1;
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO inspection_area (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            $inspectionArea_id = $this->companyConnection->lastInsertId();
            $subAreaData1 = [];

            $subInspectionAreaName=$data['sub_inspection_area_name'];

            if (!empty($subInspectionAreaName)) {
                $subInspectionArea = count($subInspectionAreaName);

                for ($i = 0; $i < $subInspectionArea; $i++) {
                    $subAreaData['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $subAreaData['inspection_area'] = $subInspectionAreaName[$i];
                    $subAreaData['parent_id'] = $inspectionArea_id;
                    $subAreaData['object_id'] = $data['object_id'];
                    $subAreaData['object_type'] = $data['object_type'];
                    $subAreaData['created_at'] = date('Y-m-d H:i:s');
                    $subAreaData['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($subAreaData);
                    $query = "INSERT INTO inspection_area (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($subAreaData);
                    $subAreaData1[] = $subAreaData;

                }
                $dataAll['child'] =$subAreaData1;
            }

            return ['status'=>'success','code'=>200,'data'=>$dataAll,'message'=>'Record Saved successfully'];



        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

        public function viewInspectionArea()
        {
            try{
                $query = $this->companyConnection->query("SELECT * FROM inspection_area where  parent_id='0'");
                $viewInspectionDate = $query->fetchAll();


                $query1 = $this->companyConnection->query("SELECT * FROM inspection_area where  parent_id!='0'");
                $viewSubInspectionDate = $query1->fetchAll();

                $html="";
                foreach ($viewInspectionDate as $key => $value) {

                    $countParentId = $value['parent_id'];

                    $html .='<div class="inspection-sub-child">' .
                                '<div class="col-sm-12">' .
                                    '<div class="row">' .
                                        '<div class="col-xs-2">' .
                                            '<div class="check-input-outer">' .
                                                '<input class="checkboxInspection" type="checkbox" value="'.$value['inspection_area'].'" name="inspection_area[inspectionName][]"><label>'.$value['inspection_area'].'</label>' .
                                            '</div>' .
                                        '</div>' .
                                        '<div class="col-xs-1">' .
                                            '<select id="rating" name="inspection_area[ratingParent][]" class="form-control rating_parent">' .
                                                '<option value="1">A</option><br>' .
                                                '<option value="2">B</option><br>' .
                                                '<option value="3">C</option>' .
                                            '</select>' .
                                        '</div>' .
                                    '</div>' .
                                //'</div>' .
                            '</div>' ;
                            foreach ($viewSubInspectionDate as $key1 =>$value1) {
                                if($value['id'] == $value1['parent_id']) {
                                    $html .='<div class="col-xs-6 inner-checkbox-cls bck-new-inspection">' .
                                        '<div class="col-xs-5">' .
                                        '<div class="check-input-outer">' .
                                        '<input type="checkbox" class="checkboxInspection" value="'.$value1['inspection_area'].'" name="inspection_area['.$key.'][sub_inspection][name][]" disabled="disabled"><label>'.$value1['inspection_area'].'</label>' .
                                        '</div>' .
                                        '</div>' .
                                        '<div class="col-xs-2">' .
                                        '<select id="rating" name="inspection_area['.$key.'][sub_inspection][ratingChild][]" class="form-control rating">' .
                                        '<option value="1">A</option>' .
                                        '<option value="2">B</option>' .
                                        '<option value="3">C</option>' .
                                        '</select>' .
                                        '</div>'.
                                        '</div>';
                                }
                            }
                    if($countParentId == 0){
                        $html .='</div>';
                    }else{
                        $html .='</div>';
                    }
                }

                return ['code'=>200, 'status'=>'success', 'data'=>$html];
            }catch (Exception $exception) {
                echo $exception->getMessage();
                printErrorLog($exception->getMessage());
            }
        }

    public function getInspectionNameList()
    {
        try{
            $sql = "SELECT * FROM inspection_list_property ORDER BY inspection_name ASC";
            $data = $this->companyConnection->query($sql)->fetchAll();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }


    public function getPropertyBuildingDetails()
    {
        try{
            $id1 = $_POST['id1'];
            $query = $this->companyConnection->query("SELECT * FROM building_detail as b LEFT JOIN general_property as g ON g.id = b.property_id  WHERE b.id='$id1'");
            $settings1 = $query->fetch();
            return array('code' => 200, 'status' => 'success', 'data' => $settings1, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getPropertyUnitDetails()
    {
        try{
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM unit_details WHERE id='$id'");
            $settings = $query->fetch();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getPropertyDetails()
    {
        try{
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM general_property WHERE id='$id'");
            $settings1 = $query->fetch();
            return array('code' => 200, 'status' => 'success', 'data' => $settings1, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  function for create a property
     */
    public function addInspectionData() {
        try {

            $files = $_FILES;

            $data = $_POST['form'];

            $data = serializeToPostArray($data);

            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data,"false");

            /*unset the custom keys */
            $custom_keys=[];
            $custom_data = json_decode(stripslashes($custom_field));
            $custom_field = [];
            foreach ($custom_data as $key => $value) {
                $custom_keys[$key] = $value->name;
                array_push($custom_field,(array) $value);
            }
            $removeKeys = $custom_keys;

            foreach($removeKeys as $key) {
                unset($data[$key]);
            }
            foreach ($data AS $key => $value) {
                if (stristr($key, 'photoVideoName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }

            foreach ($data AS $key => $value) {

                if (stristr($key, 'imgName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }
            /*unset the custom keys */


            //Required variable array
            $required_array = ['inspection_type', 'inspection_name'];
            //Max length variable array
            $maxlength_array = ['inspection_type' => 20, 'inspection_name' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id))
                    unset($data['id']);
                if(!empty($custom_field)){
                    foreach ($custom_field as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }

                $data["inspection_date"] = (isset($data['inspection_date']) && !empty($data['inspection_date'])) ? mySqlDateFormat($data['inspection_date'],null,$this->companyConnection) : NULL;
                $data['inspection_type'] = $data['inspection_type'];

                /*Inspection Area*/
                $addInspectionarea = $data['inspection_area'];
                $parent_array = $addInspectionarea['inspectionName'];
                $parent_rating = $addInspectionarea['ratingParent'];
                unset($addInspectionarea['inspectionName']);
                unset($addInspectionarea['ratingParent']);
                $new_subinspection_arry= array_values($addInspectionarea);

                $inspection_data=[];

                if(isset($parent_array) && !empty($parent_array)){
                    foreach($parent_array as $key =>$value){
                        $inspection_data[$key]['name']= $value;
                        $inspection_data[$key]['rating']= $parent_rating[$key];
                        if(isset($new_subinspection_arry[$key]['sub_inspection']['name']) && !empty($new_subinspection_arry[$key]['sub_inspection']['name'])) {
                            foreach ($new_subinspection_arry[$key]['sub_inspection']['name'] as $key1 => $value1) {
                                $inspection_data[$key]['subInspection'][$key1]['name'] = $value1;
                                $inspection_data[$key]['subInspection'][$key1]['rating']=$addInspectionarea[$key]['sub_inspection']['ratingChild'][$key1];
                            }
                        }


                    }

                }

                $inspection_area = $inspection_data;
                $inspectionAreaSerialize = serialize($inspection_area);
                $data['inspection_area'] = $inspectionAreaSerialize;
                /*Inspection Area*/



                $data['property_name'] = $data['property_name'];

                if (empty($data['property_address']))
                {
                    $data['property_address'] = "N/A";
                }else{
                    $data['property_address'] = $data['property_address'];
                }

                if (empty($data['building_name']))
                {
                    $data['building_name'] = "N/A";
                }else{

                    $data['building_name'] = $data['building_name'];
                }

                if (empty($data['building_address']))
                {
                    $data['building_address'] = "N/A";
                }else{
                    $data['building_address'] = $data['building_address'];
                }

                if (empty($data['unit_name'])){
                    $data['unit_name'] = "N/A";
                }else{
                    $data['unit_name'] = $data['unit_name'];
                }

                $data['inspection_name'] = $data['inspection_name'];
                $data['notes'] = $data['notes'];
                $data['object_type'] = $data['object_type'];
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['object_id'] = $_POST['building_id'];
                $data['custom_field'] = (count($custom_field)>0) ? serialize($custom_field):null;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_field) > 0) {
                    foreach ($custom_field as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }
                if (empty($id)) {


                    if (getRecordCount($this->companyConnection, 'property_inspection') == 0);
                    $query = "INSERT INTO property_inspection (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    if($id){

                        if(!empty($files)) {
                            $domain = getDomain();
                            $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');
                            foreach ($files as $key => $value) {
                                $file_name = $value['name'];
                                $fileData = getSingleRecord($this->companyConnection ,['column'=>'file_name','value'=>$file_name], ' inspection_photos');
                                if($fileData['code'] == '200'){
                                    return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                                }
                                $file_tmp = $value['tmp_name'];
                                $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                                $name = time() . uniqid(rand());
                                $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                //Check if the directory already exists.
                                if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                    //Directory does not exist, so lets create it.
                                    mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                                }
                                move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                                $data = [];
                                $data['inspection_id'] = '12345';
                                $data['module_type'] = 'Property';
                                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                $data['file_name'] = $file_name;
                                $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                                $data['file_location'] = $path . '/' . $name . '.' . $ext;
                                //$data['file_extension'] = $ext;
                                //$data['codec'] = $value['type']    ;
                                /* if(strstr($value['type'], "video/")){
                                     $type = 3;
                                 }else if(strstr($value['type'], "image/")){
                                     $type = 1;
                                 }else{
                                     $type = 2;
                                 }
                                 $data['file_type'] = $type;*/
                                $data['created_at'] = date('Y-m-d H:i:s');
                                $data['updated_at'] = date('Y-m-d H:i:s');

                                $sqlData = createSqlColVal($data);
                                $query = "INSERT INTO inspection_photos (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                $stmt = $this->companyConnection->prepare($query);
                                $stmt->execute($data);
                            }
                        }

                        if(isset($popup["key_id"] ) && !empty($popup["key_id"]))
                            $this->addBuildingKeys($popup, $id);

                    }


                    return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record Added successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    /**
     *  function for create a property
     */


    public function editInspectionData() {
        try {

            $editId =  $_POST['id'];

            $files = $_FILES;
            $data = $_POST['form'];
            $data = serializeToPostArray($data);

            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data,"false");

            /*unset the custom keys */
            $custom_keys=[];
            $custom_data = json_decode(stripslashes($custom_field));
            $custom_field = [];
            foreach ($custom_data as $key => $value) {
                $custom_keys[$key] = $value->name;
                array_push($custom_field,(array) $value);
            }
            $removeKeys = $custom_keys;

            foreach($removeKeys as $key) {
                unset($data[$key]);
            }
            foreach ($data AS $key => $value) {
                if (stristr($key, 'photoVideoName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }

            foreach ($data AS $key => $value) {

                if (stristr($key, 'imgName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }
            /*unset the custom keys */


            //Required variable array
            $required_array = ['inspection_type', 'inspection_name'];
            //Max length variable array
            $maxlength_array = ['inspection_type' => 20, 'inspection_name' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id))
                    unset($data['id']);
                if(!empty($custom_field)){
                    foreach ($custom_field as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }

                $data["inspection_date"] = (isset($data['inspection_date']) && !empty($data['inspection_date'])) ? mySqlDateFormat($data['inspection_date'],null,$this->companyConnection) : NULL;
                $data['inspection_type'] = $data['inspection_type'];

                /*Inspection Area*/
                $addInspectionarea = $data['inspection_area'];
                $parent_array = $addInspectionarea['inspectionName'];
                $parent_rating = $addInspectionarea['ratingParent'];
                unset($addInspectionarea['inspectionName']);
                unset($addInspectionarea['ratingParent']);
                $new_subinspection_arry= array_values($addInspectionarea);

                $inspection_data=[];

                if(isset($parent_array) && !empty($parent_array)){
                    foreach($parent_array as $key =>$value){
                        $inspection_data[$key]['name']= $value;
                        $inspection_data[$key]['rating']= $parent_rating[$key];
                        if(isset($new_subinspection_arry[$key]['sub_inspection']['name']) && !empty($new_subinspection_arry[$key]['sub_inspection']['name'])) {
                            foreach ($new_subinspection_arry[$key]['sub_inspection']['name'] as $key1 => $value1) {
                                $inspection_data[$key]['subInspection'][$key1]['name'] = $value1;
                                $inspection_data[$key]['subInspection'][$key1]['rating']=$addInspectionarea[$key]['sub_inspection']['ratingChild'][$key1];
                            }
                        }


                    }

                }

                $inspection_area = $inspection_data;
                $inspectionAreaSerialize = serialize($inspection_area);
                $data['inspection_area'] = $inspectionAreaSerialize;
                /*Inspection Area*/



                $data['property_name'] = $data['property_name'];

                if (empty($data['property_address']))
                {
                    $data['property_address'] = "N/A";
                }else{
                    $data['property_address'] = $data['property_address'];
                }

                if (empty($data['building_name']))
                {
                    $data['building_name'] = "N/A";
                }else{

                    $data['building_name'] = $data['building_name'];
                }

                if (empty($data['building_address']))
                {
                    $data['building_address'] = "N/A";
                }else{
                    $data['building_address'] = $data['building_address'];
                }

                if (empty($data['unit_name'])){
                    $data['unit_name'] = "N/A";
                }else{
                    $data['unit_name'] = $data['unit_name'];
                }

                $data['inspection_name'] = $data['inspection_name'];
                $data['notes'] = $data['notes'];
                $data['object_type'] = $data['object_type'];
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['object_id'] = $_REQUEST['id'];
                $data['custom_field'] = (count($custom_field)>0) ? serialize($custom_field):null;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                //Save Data in Company Database
               // $sqlData = createSqlColVal($data);
                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_field) > 0) {
                    foreach ($custom_field as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }
                if (empty($id)) {



                    /* $duplicate = checkNameAlreadyExists($this->companyConnection, 'property_inspection', 'building_name', $data['building_name'], "");
                     if ($duplicate['is_exists'] == 1) {
                         if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                             return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Building name allready exists!');
                         }
                     }*/


                    /*$chdata = getRecordCount($this->companyConnection, 'property_inspection');
                    */


                    if (getRecordCount($this->companyConnection, 'property_inspection') == 0){
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO property_inspection (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                        $id = $this->companyConnection->lastInsertId();
                    }else{
                        $sqlData = createSqlColValPair($data);
                        $query = "UPDATE property_inspection SET ".$sqlData['columnsValuesPair']." where id='$editId'";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                    }
                    if($stmt){

                        if(!empty($files)) {
                            $domain = getDomain();
                            $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');
                            foreach ($files as $key => $value) {
                                $file_name = $value['name'];
                                $fileData = getSingleRecord($this->companyConnection ,['column'=>'file_name','value'=>$file_name], ' inspection_photos');
                                if($fileData['code'] == '200'){
                                    return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                                }
                                $file_tmp = $value['tmp_name'];
                                $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                                $name = time() . uniqid(rand());
                                $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                /*Check if the directory already exists.*/
                                if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                   /*Directory does not exist, so lets create it.*/
                                    mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                                }
                                move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                                $data = [];
                                $data['inspection_id'] = '12345';
                                $data['module_type'] = 'Property';
                                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                $data['file_name'] = $file_name;
                                $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                                $data['file_location'] = $path . '/' . $name . '.' . $ext;
                                $data['created_at'] = date('Y-m-d H:i:s');
                                $data['updated_at'] = date('Y-m-d H:i:s');

                                $sqlData = createSqlColVal($data);
                                $query = "INSERT INTO inspection_photos (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                $stmt = $this->companyConnection->prepare($query);
                                $stmt->execute($data);
                            }
                        }

                        if(isset($popup["key_id"] ) && !empty($popup["key_id"]))
                            $this->addBuildingKeys($popup, $id);

                    }
                    if(!empty($data['last_renovation_date']) || !empty($data['last_renovation_time']) ||  !empty($data['description']) )
                        $this->addRenovationDetail($data, $id);


                    return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    /*get all property & building details*/

    public function PropertyBuildingDetails()
    {
        try{
            $query = $this->companyConnection->query("SELECT * FROM general_property");
            $settings = $query->fetchAll();
            $dataAl1['propertyDetails'] =$settings;


            $query = $this->companyConnection->query("SELECT * FROM building_detail ");
            $settings1 = $query->fetchAll();
            $dataAl1['buildingDetails'] =$settings1;

            return array('code' => 200, 'status' => 'success', 'data' => $dataAl1, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getInspectionBuildingDetails()
    {
        try{
            $property_id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM building_detail WHERE property_id =".$property_id);
            $settings = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getInspectionBuildingDetails1()
    {
        try{
            $property_id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM building_detail WHERE property_id =".$property_id);
            $settings = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }
    /*get all property & building details*/

    public function getInspectionUnitDetails()
    {
        try{
            $building_id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM unit_details WHERE building_id =".$building_id);
            $settings = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getInspectionUnitDetails1()
    {
        try{
            $building_id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM unit_details WHERE building_id =".$building_id);
            $settings = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }
    /*get all property & building details*/

    /*delete data from inspection table select option*/
    public function deleteDataInspection() {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'property_inspection');

            $sql = "UPDATE property_inspection SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            echo json_encode(['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.']);
            return;
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }
    /*delete data from inspection table select option*/

        public function getInspectionListView() {
              try{
                    $id = $_POST['id'];
                    $sql = "SELECT * FROM  property_inspection WHERE id=".$id;
                    $data = $this->companyConnection->query($sql)->fetch();
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');

            }catch (Exception $exception){
                echo $exception->getMessage();
                printErrorLog($exception->getMessage());
            }
        }


    public function getInspectionCustomFeildView() {
        try{
            $id = $_POST['id'];
            $sql = "SELECT custom_field FROM  property_inspection WHERE id=".$id;
            $data = $this->companyConnection->query($sql)->fetch();
            $unserializeData = getUnserializedData($data,'true');
            return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getInspectionEdit() {
        try{
            $id = $_POST['id'];
            $sql = "SELECT * FROM  property_inspection WHERE id=".$id;
            $data = $this->companyConnection->query($sql)->fetch();

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function editInspectionareaView() {
        try{
            $id = $_POST['id'];
            $sql = "SELECT inspection_area FROM  property_inspection WHERE id=".$id;
            $data = $this->companyConnection->query($sql)->fetch();
            $unserializeData = getUnserializedData($data,'true');
            return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function deleteFileInspection(){
        try {
            $file_id= $_REQUEST['id'];
            $data = date('Y-m-d H:i:s');
            $data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $file_id], 'inspection_photos');
            if(!empty($data["data"])){
                $count=$this->companyConnection->prepare("DELETE FROM inspection_photos WHERE id=:id");
                $count->bindParam(":id",$file_id,PDO::PARAM_INT);
                $count->execute();
                return ['status'=>'success','code'=>200,'message'=>'File deleted successfully.'];
            }else{
                return ['status'=>'failed','code'=>503,'message'=>'File not deleted successfully'];
            }
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
        }
    }


}


$buildingDetail = new buildingDetail();
?>