<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/functions/FlashMessage.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class Customfield extends DBConnection {

    /**
     * CustomFields constructor.
     */
    public function __construct() {
        parent::__construct();
        $function = FlashMessage::render();
        $this->$function();
    }

    /**
     * @return array|void
     * @throws Exception
     */
    public function add(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = ['field_name'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $comp_id = CompanyId($this->companyConnection);
                $module = (isset($data['module']) && !empty($data['module']))?$data['module']:null;
                $data['company_id'] = $comp_id['data'];
                if($data['data_type'] == 'date') {
                    $data['default_value'] = empty($data['default_value'])?null:mySqlDateFormat($data['default_value'],null,$this->companyConnection);
                } else {
                    $data['default_value'] = empty($data['default_value'])?null:$data['default_value'];
                }
                $data['is_deletable'] = '1';
                $data['is_editable'] = '1';
                $data['module'] = (isset($data['module']) && !empty($data['module']))?$data['module']:null;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                //check if custom field allready exists or not
                $duplicate = "SELECT * FROM custom_fields WHERE field_name='".$data['field_name']."' AND module='".$module."' AND deleted_at IS NULL";
                $duplicateData = $this->companyConnection->query($duplicate)->fetch();
                if(!empty($duplicateData) && count($duplicateData) > 0){
                    if(empty($data['id']) || $data['id'] != $duplicateData['id']) {
                        echo json_encode(array('code' => 500, 'status' => 'error', 'data' => $duplicateData, 'message' => 'Record already exists!'));
                        return;
                    }
                }

                if(empty($data['id'])) {
                    unset($data['id']);
                    //Save Data in Company Database
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO custom_fields (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    echo json_encode(array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record added successfully'));
                    return;
                } else {
                    $id = $data['id'];
                    unset($data['id']);
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE custom_fields SET ".$sqlData['columnsValuesPair']." where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    echo json_encode(array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully'));
                    return;
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }

    /**
     * function to return all the custom fields.
     */
    public function get(){
        try {
            $module = (isset($_POST['module']) && !empty($_POST['module']))?$_POST['module']:'';
            //check if custom field allready exists or not
            if(empty($module)) {
                $sql = "SELECT * FROM custom_fields WHERE deleted_at IS NULL";
            } else {
                $sql = "SELECT * FROM custom_fields WHERE module='$module' AND deleted_at IS NULL";
            }

            $data = $this->companyConnection->query($sql)->fetchAll();
            if(!empty($data) && count($data) > 0){
                foreach ($data as $key=>$value){
                    if($value['data_type'] == 'date' && !empty($value['default_value'])) $data[$key]['default_value'] = dateFormatUser($value['default_value'],null,$this->companyConnection);
                    continue;
                }
                echo json_encode(array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record retrieved successfully'));
            } else {
                echo json_encode(array('code' => 400, 'status' => 'success','message' => 'No Record Found!'));
            }
            return;
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }

    /**
     * function to get single record of custom field to edit.
     */
    public function edit(){
        try {
            $id = $_POST['id'];
            //check if custom field allready exists or not
            $sql = "SELECT * FROM custom_fields WHERE id=".$id." AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if(!empty($data) && count($data) > 0){
                echo json_encode(array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record retrieved successfully'));
            } else {
                echo json_encode(array('code' => 200, 'status' => 'success','message' => 'No Record Found!'));
            }
            return;
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }

    /**
     * function to delete custom field.
     */
    public function delete(){
        try {
            $id = $_POST['id'];
            //check if custom field allready exists or not
            $deleted_at = date('Y-m-d H:i:s');
            $sql = "DELETE FROM custom_fields where id=".$id;
            $data = $this->companyConnection->prepare($sql);
            $data->execute();
            if($data){
                echo json_encode(array('code' => 200, 'status' => 'success','message' => 'Record Deleted successfully'));
            } else {
                echo json_encode(array('code' => 200, 'status' => 'success','message' => 'No Record Found!'));
            }
            return;
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }

}

$user = new Customfield();
