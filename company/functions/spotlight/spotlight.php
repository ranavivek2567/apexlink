<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class Spotlight extends DBConnection{
    public function __construct() {

        parent::__construct();
        $labels =array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
        $count = count($labels);
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function labelData(){
        $months =array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
        return $months;
    }

    public function is_in_array($array, $key, $key_value){
        $within_array = 'no';
        foreach( $array as $k=>$v ){
            if( is_array($v) ){
                $within_array = $this->is_in_array($v, $key, $key_value);
                if( $within_array == 'yes' ){
                    break;
                }
            } else {
                if( $v == $key_value && $k == $key ){
                    $within_array = 'yes';
                    break;
                }
            }
        }
        return $within_array;
    }

    public function getBirthdayData(){
        try{
            $Birthdaylist = $this->companyConnection->query("SELECT  DATE_FORMAT(dob, '%b') AS month, COUNT(id) as total FROM users WHERE dob <> '' GROUP BY DATE_FORMAT(dob, '%b') ORDER BY DATE_FORMAT(dob, '%m') ASC")->fetchAll();
            $BirthdayData=[];
            // $labels =array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
            $labels = $this->labelData();
            $count = count($labels);
            // echo '<pre>'; print_r($count);die;
         //   print_r($Birthdaylist)
            for ($i = 0; $i < $count ; $i++){
                $response = $this->is_in_array($Birthdaylist, 'month', $labels[$i]);
                if ($response == "yes"){
                    foreach($Birthdaylist as $val){
                        if($labels[$i] == $val['month']){
                            $mnthcount =   $val['total'];
                        }
                    }
                    $BirthdayData['month'][] = $labels[$i];
                    $BirthdayData['data'][] = $mnthcount;
                } else{
                    $BirthdayData['month'][] = $labels[$i];
                    $BirthdayData['data'][] = 0;
                }
            }
            $maxValue =  max($BirthdayData['data']);

            if($maxValue < 10) {
                $maxValue = 10;
                $half =  2 ;
            } else if($maxValue > 50){
                $maxValue = 100;
                $half =  20 ;
            }  else if($maxValue > 10 && $maxValue < 50){
                $maxValue = 50;
                $half =  10 ;
            }  else if($maxValue > 100 && $maxValue < 500){
                $maxValue = 500;
                $half =  100 ;
            }  else if($maxValue > 500 && $maxValue < 1000){
                $maxValue = 1000;
                $half =  200 ;
            }

            $return = array('code' => 200,'status' => 'success','maxValue' => $maxValue,'halfvalue'=> $half, 'data' => $BirthdayData, 'message' => "Birthday record fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

    }
    public function getTaskReminderData(){
        try{
            $StatusArray =[1,2,3,4,5];
            foreach ($StatusArray as  $val) {
                if($val==1){
                    $name =  'NotAssigned';
                }else if($val==2){
                    $name = 'NotStarted';
                }else if($val==3){
                    $name = 'InProgress';
                }else if($val==4){
                    $name = 'Completed';
                }else if($val==5){
                    $name = 'CancelledandResigned';
                }
                $TaskReminderData[$name] = $this->companyConnection->query("SELECT DATE_FORMAT(created_at, '%b') AS month, COUNT(id) as total FROM task_reminders WHERE created_at <> '' AND status='$val' GROUP BY DATE_FORMAT(created_at, '%b') ORDER BY DATE_FORMAT(created_at, '%m') ASC")->fetchAll();

            }
            $labels = $this->labelData();
            $count = count($labels);
            foreach ($TaskReminderData as $key => $val) {
                for ($i = 0; $i < $count; $i++) {
                    $response = $this->is_in_array($TaskReminderData[$key], 'month', $labels[$i]);
                    if ($response == "yes") {
                        foreach ($TaskReminderData[$key] as $val) {
                            if ($labels[$i] == $val['month']) {
                                $mnthcount = $val['total'];
                            }
                        }
                        $TaskRemindData[$key]['data'][] = $mnthcount;
                    } else {
                        $TaskRemindData[$key]['data'][] = 0;
                    }
                }
            }
            $max =[];
            foreach ($TaskRemindData as $key => $val){
               $maxval = max($val['data']);
                array_push($max,$maxval);
            }

             $maxValue =  max($max);

            if($maxValue < 10) {
                $maxValue = 10;
                $half =  2 ;
            } else if($maxValue > 50){
                $maxValue = 100;
                $half =  20 ;
            }  else if($maxValue > 10 && $maxValue < 50){
                $maxValue = 50;
                $half =  10 ;
            }  else if($maxValue > 100 && $maxValue < 500){
                $maxValue = 500;
                $half =  100 ;
            }  else if($maxValue > 500 && $maxValue < 1000){
                $maxValue = 1000;
                $half =  200 ;
            }

            $return = array('code' => 200,'status' => 'success','maxValue' => $maxValue,'halfvalue'=> $half,'data' => $TaskRemindData, 'message' => "TaskReminder record fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getWorkOrderData(){
        try{
            $WorkOrderData = [];
            $Range = [];
            $WorkData=[];

            $start=0; $end=0; $range2 = '';
            $StatusFields =['Open','Closed','Completed','Completed, no need to bill','Scheduled'];
            $StatusIDarray =['Open'=>1,'Closed'=>2,'Completed'=>6,'Completednoneedtobill'=>7,'Scheduled'=>13];
            $rangeArray = [0,30,60,90];
            for($i=0;$i< count($rangeArray);$i++) {
                if($i > 0){
                    if($i == 3){
                        $start = $end;
                        $end =  '';
                    }
                    else{
                        $start = $end;
                        $end =  date('Y-m-d',strtotime($start."+30 days"));
                    }

                }else{
                    $start= date('Y-m-d');
                    $end=   date('Y-m-d',strtotime("+30 days"));
                }
                if($i > 0) {
                    $range1 =  $range2;
                    $range2 =  $range1+ 30;
                }
                else{
                    $range2 = $i+30;
                    $range1 = $i;
                }
                $makeRange = $range1.'-'.$range2;
                array_push($Range,$makeRange);
                foreach ($StatusIDarray as $key => $val) {
                    $sql ="SELECT DATE_FORMAT(wo.created_on, '%b') AS month, COUNT(wo.id) as total FROM work_order as wo,company_workorder_status as cws WHERE cws.id= wo.status_id AND cws.id = '$val'  AND wo.created_on <> '' AND DATE(wo.created_on) >= '$start' ";
                    if(!empty($end))
                        $sql .="  AND DATE(wo.created_on) < '$end'  ";
                    $sql .="GROUP BY DATE_FORMAT(wo.created_on, '%b') ORDER BY DATE_FORMAT(wo.created_on, '%m') ASC";
                    $WorkOrderData[$makeRange][$key] = $this->companyConnection->query("$sql")->fetchAll();
                }
            }

            $statusCount = count($StatusIDarray);
            $j=0;
            $i=0;
            for($h=0;$h< count($Range);$h++){
                foreach ($WorkOrderData[$Range[$h]] as $key => $val) {
                    if(!empty($val)){
                        $WorkData[$key][] = $val[0]['total'];
                    }else{
                        $WorkData[$key][] = 0;
                    }
                }
            }
            $maxvalue = [];
            foreach ($WorkData as $key => $value) {
                array_push($maxvalue,max($value));
            }
            $maxValue = max($maxvalue);
            if($maxValue < 10) {
                $maxValue = 10;
                $half =  2 ;
            } else if($maxValue > 50){
                $maxValue = 100;
                $half =  20 ;
            }  else if($maxValue > 10 && $maxValue < 50){
                $maxValue = 50;
                $half =  10 ;
            }  else if($maxValue > 100 && $maxValue < 500){
                $maxValue = 500;
                $half =  100 ;
            }  else if($maxValue > 500 && $maxValue < 1000){
                $maxValue = 1000;
                $half =  200 ;
            }

            $return = array('code' => 200,'status' => 'success', 'maxValue' => $maxValue,'halfvalue'=> $half,'data' => $WorkData, 'message' => "WorkOrder record fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getComplaintData(){
        try{
            $Complaints = $this->companyConnection->query("SELECT u.user_type as ID,Count(c.id) as total FROM `users` as u,complaints as c WHERE u.id = c.object_id GROUP By c.object_id")->fetchAll();
            foreach ($Complaints as $key => $val) {

                $complaint =[];
                if ($val['ID'] == 2) {
                    array_push($complaint,$val['total']);
                } else{
                    array_push($complaint,0);
                }
                if ($val['ID'] == 3) {
                    array_push($complaint,$val['total']);
                }else{
                    array_push($complaint,0);
                }
                if($val['ID'] == 4) {
                    array_push($complaint,$val['total']);
                } else{
                    array_push($complaint,0);
                }
            }
            $maxValue =  max($complaint);
            if($maxValue < 10) {
                $maxValue = 10;
                $half =  2 ;
            } else if($maxValue > 50) {
                $maxValue = 100;
                $half = 20;
            }else if($maxValue > 10 && $maxValue < 20){
                $maxValue = 20;
                $half =  5 ;
            }  else if($maxValue > 20 && $maxValue < 50){
                $maxValue = 50;
                $half =  10 ;
            }  else if($maxValue > 100 && $maxValue < 500){
                $maxValue = 500;
                $half =  100 ;
            }  else if($maxValue > 500 && $maxValue < 1000){
                $maxValue = 1000;
                $half =  200 ;
            }

            $half = round($half);
            $return = array('code' => 200,'status' => 'success','maxValue' => $maxValue,'halfvalue'=> $half, 'data' => $complaint, 'message' => " DelinQuency record fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function checkDateDuplicateCountFormat($dateArray){
        $newArray = [];
        foreach ($dateArray as $dateArr){
            $mnth = date("Y-M-d", strtotime($dateArr));
            $explode = explode('-',$mnth);
            $newArray[] = $explode[1];
        }
        return $newArray;
    }

    public  function  getCredentialExpiration(){
        try {
            $CredentialExpiration = $this->companyConnection->query("SELECT DATE_FORMAT(tc.expire_date, '%b') AS month,COUNT(tc.id) as total FROM `tenant_credential` as tc,users as u WHERE u.`id` = tc.`user_id` GROUP BY DATE_FORMAT(tc.expire_date, '%b')  ORDER BY DATE_FORMAT(tc.expire_date, '%m')")->fetchAll();
            $OwnerCredentialExp = $this->companyConnection->query("SELECT owner_credential_name,owner_credential_type,owner_credential_expiration_date FROM `owner_details` as tc,users as u WHERE u.`id` = tc.`user_id`")->fetchAll();
            $OwnerCredent = [];
            if (!empty($OwnerCredentialExp)) {
                $newArray = [];
                foreach ($OwnerCredentialExp as $OCE) {
                    $OwneCrreExp = unserialize($OCE['owner_credential_expiration_date']);
                    if (empty($newArray)) {
                        $newArray = $this->checkDateDuplicateCountFormat($OwneCrreExp);
                    } else {
                        $newArray = array_merge($newArray, $this->checkDateDuplicateCountFormat($OwneCrreExp));
                    }

                }
                if (!empty($newArray))
                    $vals = array_count_values($newArray);
                $finalArray = [];
                if (!empty($vals)) {
                    foreach ($vals as $key => $val) {
                        $innerArray = [];
                        $innerArray['month'] = $key;
                        $innerArray['total'] = $val;
                        $finalArray[] = $innerArray;
                    }
                }
            }
            if(empty($finalArray))
                $finalArray =[];

            $latest_array = array_merge($CredentialExpiration, $finalArray);
            $newArray = array();
            // New datas will be put in this new array.
            if (!empty($latest_array)) {
                foreach ($latest_array as $nameAndCount) {
                    foreach ($nameAndCount as $index => $name) {
                        if ($index == 'month') {
                            $monthname = $name;
                        }
                        if ($index == 'total') {
                            if (array_key_exists($monthname, $newArray)) {
                                $newArray[$monthname] += $name;
                            } else {
                                $newArray[$monthname] = $name;
                            }
                        }
                    }
                }
            }
            if (!empty($newArray)) {
                foreach ($newArray as $key => $val) {
                    $newArray2['month'] = $key;
                    $newArray2['total'] = $val;
                    $newArray3[] = $newArray2;
                }
            }
            $labels = $this->labelData();
            $count = count($labels);

            if(empty($newArray3))
                $newArray3 =[];

            for ($i = 0; $i < $count; $i++) {
                $response = $this->is_in_array($newArray3, 'month', $labels[$i]);
                if ($response == "yes") {
                    foreach ($newArray3 as $val) {
                        if ($labels[$i] == $val['month']) {
                            $mnthcount = $val['total'];
                        }
                    }
                    $CredentExp['data'][] = $mnthcount;
                } else {
                    $CredentExp['data'][] = 0;
                }
            }

            $return = array('code' => 200,'status' => 'success', 'data' => $CredentExp, 'message' => "Credential Expiration record fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getInventoryExpiration(){
        try {
            $IventoryExpiration = $this->companyConnection->query("SELECT DATE_FORMAT(expected_deliver_date, '%b') AS month,COUNT(id) as total FROM `maintenance_inventory` WHERE purchase_date <> '' GROUP BY DATE_FORMAT(expected_deliver_date, '%b') ORDER BY DATE_FORMAT(expected_deliver_date, '%m')")->fetchAll();
            $InventoryExp = [];
            $labels = $this->labelData();
            $count = count($labels);
            // echo '<pre>'; print_r($count);die;
            for ($i = 0; $i < $count ; $i++){
                $response = $this->is_in_array($IventoryExpiration, 'month', $labels[$i]);
                if ($response == "yes"){
                    foreach($IventoryExpiration as $val){
                        if($labels[$i] == $val['month']){
                            $mnthcount =   $val['total'];
                        }
                    }
                    $InventoryExp['data'][] = $mnthcount;
                } else{
                    $InventoryExp['data'][] = 0;
                }
            }
            $maxValue =  max($InventoryExp['data']);

            if($maxValue < 10) {
                $maxValue = 10;
                $half =  2 ;
            } else if($maxValue > 50){
                $maxValue = 100;
                $half =  20 ;
            }  else if($maxValue > 10 && $maxValue < 50){
                $maxValue = 50;
                $half =  10 ;
            }  else if($maxValue > 100 && $maxValue < 500){
                $maxValue = 500;
                $half =  100 ;
            }  else if($maxValue > 500 && $maxValue < 1000){
                $maxValue = 1000;
                $half =  200 ;
            }
            $return = array('code' => 200,'status' => 'success', 'halfvalue'=>$half,'maxValue'=>$maxValue,'data' => $InventoryExp, 'message' => "Inventory Expiration record fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getAnnouncementData(){
        try {
        if(!empty($_POST['formData'])) {
               $data1= postArray($_POST['formData'], 'true');
            }
            if(isset($data1['years']) && $data1['years'] != ''){
                $countArray = [0, 1, 2, 3, 4];
                $count = count($countArray);
                $week_end_day = '';
                for ($i = 0; $i < $count; $i++) {

                    if ($i > 0) {
                        $week_start_day = date('Y-m-d', strtotime($week_end_day . '+ 1days'));
                        $week_end_day = date('Y-m-d', strtotime($week_start_day . '+ 6days'));
                    } else {
                        $d = strtotime($data1['years'].'-'.$data1['annoMonth'].'-01');
                        $week_start_day = date("Y-m-d",$d);
                        $end_week = strtotime("+1 week",strtotime($week_start_day));
                        $week_end_day = date("Y-m-d",$end_week);
                    }

                    if ($i == 0) {
                        $name = 'Week1';
                    } elseif ($i == '1') {
                        $name = 'Week2';
                    } elseif ($i == '2') {
                        $name = 'Week3';
                    } elseif ($i == '3') {
                        $name = 'Week4';
                    } elseif ($i == '4') {
                        $name = 'Week5';
                    }
                    $Announcement[$name] = $this->companyConnection->query("SELECT COUNT(id) as total FROM `announcements` WHERE start_date <> ''  AND  start_date BETWEEN '" . $week_start_day . "' AND '" . $week_end_day . "' GROUP BY DATE_FORMAT(start_date, '%b') ORDER BY DATE_FORMAT(start_date, '%m')")->fetchAll();

                }
            } else {
                $countArray = [0, 1, 2, 3, 4];
                $count = count($countArray);
                $week_end_day = '';
                for ($i = 0; $i < $count; $i++) {

                    if ($i > 0) {
                        $week_start_day = date('Y-m-d', strtotime($week_end_day . '+ 1days'));
                        $week_end_day = date('Y-m-d', strtotime($week_start_day . '+ 6days'));
                    } else {
                        $current_day = date('w');
                        $week_start_day = date('Y-m-d', strtotime('-' . $current_day . ' days'));
                        $week_end_day = date('Y-m-d', strtotime('+' . (6 - $current_day) . ' days'));
                    }

                    if ($i == 0) {
                        $name = 'Week1';
                    } elseif ($i == '1') {
                        $name = 'Week2';
                    } elseif ($i == '2') {
                        $name = 'Week3';
                    } elseif ($i == '3') {
                        $name = 'Week4';
                    } elseif ($i == '4') {
                        $name = 'Week5';
                    }
                    $Announcement[$name] = $this->companyConnection->query("SELECT COUNT(id) as total FROM `announcements` WHERE start_date <> ''  AND  start_date BETWEEN '" . $week_start_day . "' AND '" . $week_end_day . "' GROUP BY DATE_FORMAT(start_date, '%b') ORDER BY DATE_FORMAT(start_date, '%m')")->fetchAll();
                }
            }
            $Annouce = [];
            foreach ($Announcement as $key => $value) {
                if (!empty($value[0]['total'])) {
                    array_push($Annouce, $value[0]['total']);
                } else {
                    array_push($Annouce, 0);
                }
            }
            $maxValue = max($Annouce);

            if($maxValue < 10) {
                $maxValue = 10;
                $half =  2 ;
            } else if($maxValue > 50){
                $maxValue = 100;
                $half =  20 ;
            }  else if($maxValue > 10 && $maxValue < 50){
                $maxValue = 50;
                $half =  10 ;
            }  else if($maxValue > 100 && $maxValue < 500){
                $maxValue = 500;
                $half =  100 ;
            }  else if($maxValue > 500 && $maxValue < 1000){
                $maxValue = 1000;
                $half =  200 ;
            }

            $return = array('code' => 200, 'status' => 'success', 'data' => $Annouce, 'halfvalue'=>$half,'maxValue'=>$maxValue,'message' => "AnnouncementData record fetched");
            echo json_encode($return);
            die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getVacentoccupaidUnit(){
        try {
            if(isset($_POST['formData']) && $_POST['formData'] != ''){
                $data1= postArray($_POST['formData'], 'true');
                $portfolioId = $data1['portfolio_List'];
                $propertyId = $this->companyConnection->query("SELECT gp.id FROM general_property as gp where portfolio_id ='$portfolioId'")->fetchAll();

                $arrayData = [];
                $abc=[];

               foreach ($propertyId as $row){
                   $arrayData = $row['id'];
                   array_push($abc ,$arrayData);

                }
                $propId = implode(',', $abc);
                $vacantUnit = $this->companyConnection->query("SELECT count(unit_no) AS vacent FROM `unit_details` WHERE building_unit_status ='1' and property_id  IN ('".$propId."')")->fetch();
                $occupiadUnit = $this->companyConnection->query("SELECT count(unit_no) AS occupaid FROM `unit_details` WHERE building_unit_status ='4'and property_id  IN ('".$propId."')")->fetch();
                $totalUnit = $vacantUnit['vacent'] + $occupiadUnit['occupaid'];


                 /*$new_width1 = ($vacaentUnit['vacent'] / 100) * $totalUnit;*/
                if($vacantUnit != '0' && $totalUnit != '0'){
                    $percentVacant = $vacantUnit['vacent']/$totalUnit;
                    $percent_vacant = number_format( $percentVacant * 100 );
                }else{
                    $percent_vacant = 0;
                }

                if($occupiadUnit != '0' && $totalUnit != '0'){
                    $percentoccupiad = $occupiadUnit['occupaid']/$totalUnit;
                    $percent_occupiad = number_format( $percentoccupiad * 100 );
                }else{
                    $percent_occupiad = 0;
                }

                $totaData = array($percent_vacant,$percent_occupiad);
                //echo '<pre>';print_r($totaData); die;
                return array('code' => 200, 'status' => 'success', 'dataVacant' => $percent_vacant,'dataOccupiad' => $percent_occupiad, 'message' => 'Record fetched successfully');
            }else{
                //dd('there');
                $vacantUnit1 = $this->companyConnection->query("SELECT count(unit_no) AS vacent FROM `unit_details` WHERE building_unit_status ='1'")->fetch();
                $occupiadUnit1 = $this->companyConnection->query("SELECT count(unit_no) AS occupaid FROM `unit_details` WHERE building_unit_status ='4'")->fetch();
                $totalUnit1 = $vacantUnit1['vacent'] + $occupiadUnit1['occupaid'];

                /*$new_width1 = ($vacaentUnit['vacent'] / 100) * $totalUnit;*/

                $percentVacant = $vacantUnit1['vacent']/$totalUnit1;
                $percent_vacant = number_format( $percentVacant * 100 );

                $percentoccupiad = $occupiadUnit1['occupaid']/$totalUnit1;
                $percent_occupiad = number_format( $percentoccupiad * 100 );
                $totaData = array($percent_vacant,$percent_occupiad);
                return array('code' => 200, 'status' => 'success', 'dataVacant' => $percent_vacant,'dataOccupiad' => $percent_occupiad, 'message' => 'Record fetched successfully');
            }
//
            //dd($totaData);

        }catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getTenantInsuranceData(){
        try {
            $activeSatus = $this->companyConnection->query("SELECT count(status) AS activeStatus FROM `tenant_renter_insurance` WHERE expire_date >= CURDATE()")->fetch();
            $inActiveSatus = $this->companyConnection->query("SELECT count(status) AS inActiveStatus FROM `tenant_renter_insurance` WHERE expire_date < CURDATE()")->fetch();

                $active = !empty($activeSatus['activeStatus'])? $activeSatus['activeStatus']:0;
                $Inactive = !empty($inActiveSatus['inActiveStatus'])? $inActiveSatus['inActiveStatus']:0;

                $totalActive = $active + $Inactive;
                if(!empty($totalActive)){

                $percentActive = $activeSatus['activeStatus'] / $totalActive;
                $percent_active = number_format($percentActive * 100);
                //dd($percent_active);
                $percentInactive = $inActiveSatus['inActiveStatus'] / $totalActive;
                $percent_inactive = number_format($percentInactive * 100);
            }  else {
                $percent_active = 0;
                $percent_inactive = 100;
            }


            return array('code' => 200, 'status' => 'success', 'dataActive' => $percent_active,'dataInactive' => $percent_inactive, 'message' => 'Record fetched successfully');

        }catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getLeaseStatus(){
        try {
            $activeSatus = $this->companyConnection->query("SELECT count(record_status) AS activeStatus FROM `tenant_lease_details` WHERE record_status='1'")->fetch();
            $inActiveSatus = $this->companyConnection->query("SELECT count(record_status) AS inActiveStatus FROM `tenant_lease_details` WHERE record_status='0'")->fetch();

            $active = !empty($activeSatus['activeStatus'])? $activeSatus['activeStatus']:0;
            $Inactive = !empty($inActiveSatus['inActiveStatus'])? $inActiveSatus['inActiveStatus']:0;

            $totalActive = $active + $Inactive;
            if(!empty($totalActive)) {

            $percentActive = $activeSatus['activeStatus']/$totalActive;
            $percent_active = number_format( $percentActive * 100 );
            //dd($percent_active);
            $percentInactive = $inActiveSatus['inActiveStatus']/$totalActive;
            $percent_inactive = number_format( $percentInactive * 100 );
        }  else {
            $percent_active = 0;
            $percent_inactive = 100;
        }


            return array('code' => 200, 'status' => 'success', 'dataActive' => $percent_active,'dataInactive' => $percent_inactive, 'message' => 'Record fetched successfully');

        }catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getPropertyInsurance(){
        try {
            $activeSatus = $this->companyConnection->query("SELECT count(status) AS activeStatus FROM `property_insurance` WHERE end_date >= CURDATE()")->fetch();
            $inActiveSatus = $this->companyConnection->query("SELECT count(status) AS inActiveStatus FROM `property_insurance` WHERE end_date < CURDATE()")->fetch();

            $active = !empty($activeSatus['activeStatus'])? $activeSatus['activeStatus']:0;
            $Inactive = !empty($inActiveSatus['inActiveStatus'])? $inActiveSatus['inActiveStatus']:0;

            $totalActive = $active + $Inactive;
            if(!empty($totalActive)){
            $percentActive = $activeSatus['activeStatus']/$totalActive;
            $percent_active = number_format( $percentActive * 100 );
            //dd($percent_active);
            $percentInactive = $inActiveSatus['inActiveStatus']/$totalActive;
            $percent_inactive = number_format( $percentInactive * 100 );
        }  else {
            $percent_active = 0;
            $percent_inactive = 100;
        }

            return array('code' => 200, 'status' => 'success', 'dataActive' => $percent_active,'dataInactive' => $percent_inactive, 'message' => 'Record fetched successfully');

        }catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getMoveInData(){
        try {
            $midDate = date("Y-m-d");

            $currentSunday = date( 'Y-m-d', strtotime( 'previous sunday this week' ) );
            $nextSunday = date( 'Y-m-d', strtotime( 'next sunday this week' ) );

            $weekDate =  strtotime("-1 week", strtotime($currentSunday));
            $oneWeekAgoDate =date('Y-m-d',$weekDate);

            $weekDate1 = strtotime("-2 week", strtotime($currentSunday));
            $twoWeekAgoDate =date('Y-m-d',$weekDate1);

            $weekDate2 = strtotime("+1 week", strtotime($nextSunday));
            $nextWeekDate =date('Y-m-d',$weekDate2);

            $weekDate3 = strtotime("+2 week", strtotime($nextSunday));
            $twoWeekHeadDate =date('Y-m-d',$weekDate3);

            $oneWeekAgo = $this->companyConnection->query("SELECT count(actual_move_in) AS ami FROM `tenant_move_in` tmi WHERE tmi.actual_move_in >= '$oneWeekAgoDate' and tmi.actual_move_in <= '$midDate'");
            $totalData['weekago'] = $oneWeekAgo->fetch();

            $currentWeek = $this->companyConnection->query("SELECT count(actual_move_in) AS ami FROM `tenant_move_in` tmi WHERE tmi.actual_move_in >= '$currentSunday' and tmi.actual_move_in <= '$midDate'");
            $totalData['currentWeek'] = $currentWeek->fetch();


            $twoWeekAgo = $this->companyConnection->query("SELECT count(actual_move_in) AS ami FROM `tenant_move_in` tmi WHERE tmi.actual_move_in >= '$twoWeekAgoDate' and tmi.actual_move_in <= '$midDate'");
            $totalData['twoWeekAgo'] = $twoWeekAgo->fetch();

            $nextWeek = $this->companyConnection->query("SELECT count(actual_move_in) AS ami FROM `tenant_move_in` tmi WHERE tmi.actual_move_in >= '$midDate' and tmi.actual_move_in <= '$nextWeekDate'");
            $totalData['nextWeek'] = $nextWeek->fetch();

            $twoWeekHead = $this->companyConnection->query("SELECT count(actual_move_in) AS ami FROM `tenant_move_in` tmi WHERE tmi.actual_move_in >= '$midDate' and tmi.actual_move_in <= '$twoWeekHeadDate' ");
            $totalData['twoWeekHead'] = $twoWeekHead->fetch();
            /*echo '<pre>';print_r($twoWeekHead);die;*/
            $maxDataMoIn = max($totalData['weekago']['ami'],$totalData['currentWeek']['ami'],$totalData['twoWeekAgo']['ami'],$totalData['nextWeek']['ami'],$totalData['twoWeekHead']['ami']);
            return array('code' => 200, 'status' => 'success', 'data' => $totalData, 'maxData' => $maxDataMoIn,'message' => 'Move In Record fetched successfully');

        }catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    public function getMoveOutData()
    {
        try {
            $midDate = date("Y-m-d");

            $currentSunday = date('Y-m-d', strtotime('previous sunday this week'));
            $nextSunday = date('Y-m-d', strtotime('next sunday this week'));

            $weekDate = strtotime("-1 week", strtotime($currentSunday));
            $oneWeekAgoDate = date('Y-m-d', $weekDate);

            $weekDate1 = strtotime("-2 week", strtotime($currentSunday));
            $twoWeekAgoDate = date('Y-m-d', $weekDate1);

            $weekDate2 = strtotime("+1 week", strtotime($nextSunday));
            $nextWeekDate = date('Y-m-d', $weekDate2);

            $weekDate3 = strtotime("+2 week", strtotime($nextSunday));
            $twoWeekHeadDate = date('Y-m-d', $weekDate3);

            $oneWeekAgo = $this->companyConnection->query("SELECT count(actualMoveOutDate) AS amod FROM `moveouttenant` tmi WHERE tmi.actualMoveOutDate >= '$oneWeekAgoDate' and tmi.actualMoveOutDate <= '$midDate'");
            $totalData['weekago'] = $oneWeekAgo->fetch();


            $currentWeek = $this->companyConnection->query("SELECT count(actualMoveOutDate) AS amod FROM `moveouttenant` tmi WHERE tmi.actualMoveOutDate >= '$currentSunday' and tmi.actualMoveOutDate <= '$midDate'");
            $totalData['currentWeek'] = $currentWeek->fetch();


            $twoWeekAgo = $this->companyConnection->query("SELECT count(actualMoveOutDate) AS amod FROM `moveouttenant` tmi WHERE tmi.actualMoveOutDate >= '$twoWeekAgoDate' and tmi.actualMoveOutDate <= '$midDate'");
            $totalData['twoWeekAgo'] = $twoWeekAgo->fetch();

            $nextWeek = $this->companyConnection->query("SELECT count(actualMoveOutDate) AS amod FROM `moveouttenant` tmi WHERE tmi.actualMoveOutDate >= '$midDate' and tmi.actualMoveOutDate <= '$nextWeekDate'");
            $totalData['nextWeek'] = $nextWeek->fetch();

            $twoWeekHead = $this->companyConnection->query("SELECT count(actualMoveOutDate) AS amod FROM `moveouttenant` tmi WHERE tmi.actualMoveOutDate >= '$midDate' and tmi.actualMoveOutDate <= '$twoWeekHeadDate' ");
            $totalData['twoWeekHead'] = $twoWeekHead->fetch();

            $maxData = max($totalData['weekago']['amod'], $totalData['currentWeek']['amod'],$totalData['twoWeekAgo']['amod'],$totalData['nextWeek']['amod'],$totalData['twoWeekHead']['amod']);
            //$maxData = max(2, 4,0,6,5);

           // echo '<pre>';print_r( $maxData );die;

            return array('code' => 200, 'status' => 'success', 'data' => $totalData, 'maxData' => $maxData, 'message' => 'Move Out Record fetched successfully');

        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /* data filter by filters function */
    public function getAllProperties()
    {
        try {
            $Properties= $this->companyConnection->query("SELECT id,property_name FROM `general_property` gp")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $Properties, 'message' => "Properties record fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    public function getPropertiesCatSubcat(){
        try {
            $data['Properties']=   $this->companyConnection->query("SELECT id,property_name FROM `general_property` gp")->fetchAll();
            $data['category']= $this->companyConnection->query("SELECT * FROM `company_maintenance_subcategory` WHERE sub_category=''")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $data, 'message' => "Categories record fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getpropertyVacantData()
    {
        try {
            $propertyUnit = $this->companyConnection->query("SELECT gp.id ,COUNT(ud.id) as totalUnit from general_property gp JOIN unit_details ud on ud.property_id = gp.id GROUP BY gp.id");
            $totalData = $propertyUnit->fetchall();
            $propertyUnit = $this->companyConnection->query("SELECT gp.id ,COUNT(ud.id) as vacantUnit from general_property gp JOIN unit_details ud on ud.property_id = gp.id WHERE ud.building_unit_status = 1 GROUP BY gp.id");
            $totalUnit = $propertyUnit->fetchall();

            $mergedData = array_merge($totalData,$totalUnit);
            $abc = [];
            $efg = [];
            $res = [];
            $xyz = [];
            $percent_active = [];
            for($i=0;$i<count($mergedData);$i++){
                if(isset($mergedData[$i]['vacantUnit']) && !empty($mergedData[$i]['vacantUnit']) ){
                    array_push($abc,$mergedData[$i]['vacantUnit']);
                }
            };

            for($j=0;$j<count($mergedData);$j++){
                if(isset($mergedData[$j]['totalUnit']) && !empty($mergedData[$j]['totalUnit']) ){
                    array_push($efg,$mergedData[$j]['totalUnit']);
                }
            };

            for($k=0;$k<count($abc);$k++){
                $res = $abc[$k]/$efg[$k];
                $percent_active = number_format( $res * 100 );
                if($percent_active >= '5') {
                    array_push($xyz,$percent_active);
                }

            };
            $rts=(count($xyz)/count($totalData));
            $ress = number_format($rts*100);
            $remaining = 100 - $ress;
           // echo '<pre>';print_r($xyz);print_r($total+$ress);die;
            return array('code' => 200, 'status' => 'success', 'vacantOver' => $ress,'vacantBelow' => $remaining, 'message' => 'Record fetched successfully');
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getAllCredentialtype()
    {
        try {
            $CredentType = $this->companyConnection->query("SELECT * FROM tenant_credential_type")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $CredentType, 'message' => "Credential Types fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public  function getSubCategorybyCat(){
        try {
            $id = implode(',',$_POST['id']);
       //     echo "SELECT * FROM `company_maintenance_subcategory` WHERE parent_id IN ($id)";
            $subCategory = $this->companyConnection->query("SELECT * FROM `company_maintenance_subcategory` WHERE parent_id IN ($id)")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $subCategory, 'message' => "Sub Categories fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getComplaintTypes(){
        try {
            $ComplaintTypes = $this->companyConnection->query("SELECT * FROM `complaint_types` ")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $ComplaintTypes, 'message' => "Complaint Types fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getportfolioData(){
        try {
            $portfolioDataList = $this->companyConnection->query("SELECT portfolio_name,id FROM `company_property_portfolio` WHERE status = '1'")->fetchAll();
            return  array('code' => 200,'status' => 'success', 'data' => $portfolioDataList, 'message' => "Portfolio fetched successfully!");
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public  function GetUsersByUserType(){
        try {
            $usertype = $_POST['usertype'];
            $data = $this->companyConnection->query("SELECT * FROM `users` WHERE user_type='$usertype' ")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $data, 'message' => "Tenant list fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    public  function getPortfolios(){
        try {
            $data['portfolio'] = $this->companyConnection->query("SELECT * FROM `company_property_portfolio` ")->fetchAll();
            $data['Properties']=   $this->companyConnection->query("SELECT id,property_name FROM `general_property` gp")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $data, 'message' => "Complaint Types fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function gePropertyAndManager(){
        try {
            $data['propertyManager'] = $this->companyConnection->query("SELECT * FROM `users` WHERE user_type=2 AND role=2 ")->fetchAll();
            $data['Properties']=   $this->companyConnection->query("SELECT id,property_name FROM `general_property` gp")->fetchAll();
            $return = array('code' => 200,'status' => 'success', 'data' => $data, 'message' => "Complaint Types fetched");
            echo json_encode($return); die();
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getPaidUnpaidInvoices(){
        try {
            $activeSatus = $this->companyConnection->query("SELECT count(status) AS activeStatus FROM `accounting_invoices` WHERE status='1'")->fetch();
            $inActiveSatus = $this->companyConnection->query("SELECT count(status) AS inActiveStatus FROM `accounting_invoices` WHERE status='0'")->fetch();

            $active = !empty($activeSatus['activeStatus'])? $activeSatus['activeStatus']:0;
            $Inactive = !empty($inActiveSatus['inActiveStatus'])? $inActiveSatus['inActiveStatus']:0;

            $totalActive = $active + $Inactive;
            if(!empty($totalActive)){
                $totalActive = $activeSatus['activeStatus'] + $inActiveSatus['inActiveStatus'];

                $percentActive = $activeSatus['activeStatus'] / $totalActive;
                $percent_active = number_format($percentActive * 100);
                $percentInactive = $inActiveSatus['inActiveStatus'] / $totalActive;
                $percent_inactive = number_format($percentInactive * 100);
            } else {
                $percent_active = 0;
                $percent_inactive = 100;
            }

            return array('code' => 200, 'status' => 'success', 'dataActive' => $percent_active,'dataInactive' => $percent_inactive, 'message' => 'Invoices fetched successfully');
        } catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getPaidUnpaidBills(){
        try {
           $activeSatus = $this->companyConnection->query("SELECT count(status) AS activeStatus FROM `company_bills` WHERE status='1'")->fetch();
            $inActiveSatus = $this->companyConnection->query("SELECT count(status) AS inActiveStatus FROM `company_bills` WHERE status='0'")->fetch();

             $active = !empty($activeSatus['activeStatus'])? $activeSatus['activeStatus']:0;
             $Inactive = !empty($inActiveSatus['inActiveStatus'])? $inActiveSatus['inActiveStatus']:0;

                $totalActive = $active + $Inactive;
            if(!empty($totalActive)){
                $percentActive = $activeSatus['activeStatus'] / $totalActive;
                $percent_active = number_format($percentActive * 100);
                $percentInactive = $inActiveSatus['inActiveStatus'] / $totalActive;
                    if(!empty($percentInactive)){
                        $percent_inactive = number_format($percentInactive * 100);
                    }
                    else{
                        $percent_inactive = 0;
                    }
            } else {
                $percent_active = 0;
                $percent_inactive = 100;
            }

            return array('code' => 200, 'status' => 'success', 'dataActive' => $percent_active,'dataInactive' => $percent_inactive, 'message' => 'Bills record fetched successfully');
        } catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getNotifications(){
        try {
            /*  $activeSatus = $this->companyConnection->query("SELECT count(status) AS activeStatus FROM `accounting_invoices` WHERE status='1'")->fetch();
              $inActiveSatus = $this->companyConnection->query("SELECT count(status) AS inActiveStatus FROM `accounting_invoices` WHERE status='0'")->fetch();

              if(!empty($activeSatus['activeStatus']) &&  $inActiveSatus['inActiveStatus']) {
                  $totalActive = $activeSatus['activeStatus'] + $inActiveSatus['inActiveStatus'];

                  $percentActive = $activeSatus['activeStatus'] / $totalActive;
                  $percent_active = number_format($percentActive * 100);
                  $percentInactive = $inActiveSatus['inActiveStatus'] / $totalActive;
                  $percent_inactive = number_format($percentInactive * 100);
              } else {*/
         //   $percent_active = 0;
         //   $percent_inactive = 100;
            //}
            $data = [];

            return array('code' => 200, 'status' => 'success', 'data' => $data ,'message' => 'Bills record fetched successfully');
        } catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getReceiveablePayable(){
        try {
            /*  $activeSatus = $this->companyConnection->query("SELECT count(status) AS activeStatus FROM `accounting_invoices` WHERE status='1'")->fetch();
              $inActiveSatus = $this->companyConnection->query("SELECT count(status) AS inActiveStatus FROM `accounting_invoices` WHERE status='0'")->fetch();

              if(!empty($activeSatus['activeStatus']) &&  $inActiveSatus['inActiveStatus']) {
                  $totalActive = $activeSatus['activeStatus'] + $inActiveSatus['inActiveStatus'];

                  $percentActive = $activeSatus['activeStatus'] / $totalActive;
                  $percent_active = number_format($percentActive * 100);
                  $percentInactive = $inActiveSatus['inActiveStatus'] / $totalActive;
                  $percent_inactive = number_format($percentInactive * 100);
              } else {*/
            $percent_active = 0;
            $percent_inactive = 100;
            //}
            return array('code' => 200, 'status' => 'success', 'dataActive' => $percent_active,'dataInactive' => $percent_inactive, 'message' => 'ReceiveablePayable record fetched successfully');
        } catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function downloadsvg(){
        $image = $_POST['image'];
        $file = 'invoice.jpeg';
        $data=$this->base64_to_jpeg($image);
        $decoded = base64_decode($data[1]);
        $path = "uploads/";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'/invoice.svg';
        // imagejpeg($decoded,$file.'.jpg',100);
        /*dd($decoded);*/
        file_put_contents($fileUrl, $decoded);
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'/invoice.jpg';
            return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
            exit;
        }
    }
    function base64_to_jpeg($base64_string) {
        // open the output file for writing

        $data = explode( ',', $base64_string );
        // we could add validation here with ensuring count( $data ) > 1

        return $data;
    }

    function restoreSettingSpot(){
        try{
            $data2 = $_POST['formData'];
            $data=postArray($data2);

            $newSql = "SELECT * FROM dashbordspotsetting";
            $getDataSig =  $this->companyConnection->query($newSql)->fetch();
            if ($getDataSig != ""){
                $data1['nineSpotChart']= isset($data['nineSpotChart'])? $data['nineSpotChart']:0;
                $data1['elevenSpotChart']= isset($data['elevenSpotChart'])? $data['elevenSpotChart']:0;
                $data1['tenSpotChart']= isset($data['tenSpotChart'])? $data['tenSpotChart']:0;
                $data1['firstSpotChart']= isset($data['firstSpotChart'])? $data['firstSpotChart']:0;
                $data1['secSpotChart'] = isset($data['secSpotChart']) ? $data['secSpotChart']:0;
                $data1['thirdSpotChart'] = isset($data['thirdSpotChart'])? $data['thirdSpotChart']:0;
                $data1['forthSpotChart'] = isset($data['forthSpotChart'])? $data['forthSpotChart']:0;
                $data1['twelveSpotChart'] = isset($data['twelveSpotChart'])? $data['twelveSpotChart']:0;
                $data1['fifthSpotChart'] = isset($data['fifthSpotChart']) ? $data['fifthSpotChart']:0;
                $data1['thirteenSpotChart'] = isset($data['thirteenSpotChart'])? $data['thirteenSpotChart']:0;
                $data1['sixSpotChart'] = isset($data['sixSpotChart'])? $data['sixSpotChart']:0;
                $data1['fourteenSpotChart'] = isset($data['fourteenSpotChart'])? $data['fourteenSpotChart']:0;
                $data1['sevenSpotChart'] = isset($data['sevenSpotChart'])? $data['sevenSpotChart']:0;
                $data1['fifteenSpotChart'] = isset($data['fifteenSpotChart'])? $data['fifteenSpotChart']:0;
                $data1['eightSpotChart'] = isset($data['eightSpotChart'])? $data['eightSpotChart']:0;
                $data1['sixteenSpotChart'] = isset($data['sixteenSpotChart'])? $data['sixteenSpotChart']:0;
                $data1['seventeenSpotChart'] = isset($data['seventeenSpotChart'])? $data['seventeenSpotChart']:0;
                $data1['eighteenSpotChart'] = isset($data['eighteenSpotChart'])? $data['eighteenSpotChart']:0;
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data1);
                $query = "UPDATE `dashbordspotsetting` SET " . $sqlData['columnsValuesPair'] ;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $id = $this->companyConnection->lastInsertId();

                return ['code'=>200, 'status'=>'success', 'data'=>$data1, 'id'=>$id, 'message'=> 'The record updated successfully.'];
            }else{
                $data1['nineSpotChart']= isset($data['nineSpotChart'])? $data['nineSpotChart']:0;
                $data1['elevenSpotChart']= isset($data['elevenSpotChart'])? $data['elevenSpotChart']:0;
                $data1['tenSpotChart']= isset($data['tenSpotChart'])? $data['tenSpotChart']:0;
                $data1['firstSpotChart']= isset($data['firstSpotChart'])? $data['firstSpotChart']:0;
                $data1['secSpotChart'] = isset($data['secSpotChart']) ? $data['secSpotChart']:0;
                $data1['thirdSpotChart'] = isset($data['thirdSpotChart'])? $data['thirdSpotChart']:0;
                $data1['forthSpotChart'] = isset($data['forthSpotChart'])? $data['forthSpotChart']:0;
                $data1['twelveSpotChart'] = isset($data['twelveSpotChart'])? $data['twelveSpotChart']:0;
                $data1['fifthSpotChart'] = isset($data['fifthSpotChart']) ? $data['fifthSpotChart']:0;
                $data1['thirteenSpotChart'] = isset($data['thirteenSpotChart'])? $data['thirteenSpotChart']:0;
                $data1['sixSpotChart'] = isset($data['sixSpotChart'])? $data['sixSpotChart']:0;
                $data1['fourteenSpotChart'] = isset($data['fourteenSpotChart'])? $data['fourteenSpotChart']:0;
                $data1['sevenSpotChart'] = isset($data['sevenSpotChart'])? $data['sevenSpotChart']:0;
                $data1['fifteenSpotChart'] = isset($data['fifteenSpotChart'])? $data['fifteenSpotChart']:0;
                $data1['eightSpotChart'] = isset($data['eightSpotChart'])? $data['eightSpotChart']:0;
                $data1['sixteenSpotChart'] = isset($data['sixteenSpotChart'])? $data['sixteenSpotChart']:0;
                $data1['seventeenSpotChart'] = isset($data['seventeenSpotChart'])? $data['seventeenSpotChart']:0;
                $data1['eighteenSpotChart'] = isset($data['eighteenSpotChart'])? $data['eighteenSpotChart']:0;
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO `dashbordspotsetting` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $id = $this->companyConnection->lastInsertId();

                return ['code'=>200, 'status'=>'success', 'data'=>$data1, 'id'=>$id, 'message'=> 'The record updated successfully.'];
            }
        }catch (Exception $exception){
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getSettingSpot(){
        try {
            $query = $this->companyConnection->query("SELECT * FROM dashbordspotsetting");
            $settings = $query->fetch();
            if($settings){
                return array('status' => 'success', 'message'=>'The record updated successfully.', 'data' => $settings,'code' => 200);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found', 'code' => 400);
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getreceivablePayable(){
        try {
            if(isset($_POST['formData']) && $_POST['formData'] != '') {
                $data1 = postArray($_POST['formData'], 'true');
                $portfolioId = $data1['portfolio_List'];
                $propertyId = $this->companyConnection->query("SELECT gp.id FROM general_property as gp where portfolio_id ='$portfolioId'")->fetchAll();
                $tenantIds = [];
                if(!empty($propertyId)) {
                    for ($i = 0; $i < count($propertyId); $i++) {
                        $tenantId = $this->companyConnection->query("SELECT tp.user_id FROM tenant_property as tp where property_id ='" . $propertyId[$i]['id'] . "'AND record_status = '1'")->fetch();
                        if (!empty($tenantId)) {
                            array_push($tenantIds, $tenantId);
                        }
                    }
                }
                $arrayData = [];
                $abc = [];
                if (!empty($tenantIds)) {
                    foreach ($tenantIds as $row) {
                        $arrayData = $row['user_id'];

                        array_push($abc, $arrayData);

                    }

                }
               // $tenId = implode(',', $abc);
                $tenId = implode("', '",  $abc);
                $amount = "SELECT SUM(tc.amount) FROM tenant_charges as tc  WHERE status ='0' and user_id  IN ('".$tenId."')";
                 $totalreceivable =   $this->companyConnection->query($amount)->fetch();

                $this->getuserDueAmount($tenantIds,$tenId);



            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getuserDueAmount($tenant_ids,$tenId) {
        try{

            /*dd($tenId);*/
            $tenantOneId = [];
            if(!empty($tenant_ids)) {
                for ($i = 0; $i < count($tenant_ids); $i++) {
                    $OneTimePayment = $this->companyConnection->query("SELECT t.id,t.user_id,t.type FROM transactions as t where t.user_id =  " . $tenant_ids[$i]['user_id'])->fetch();
                    if (!empty($OneTimePayment)) {
                        array_push($tenantOneId, $OneTimePayment);
                    }

                    if($OneTimePayment['type'] == '' || empty($OneTimePayment['type'])){
                      $getAmountTenantLease = "SELECT  SUM(tld.security_deposite) as securityDeposite, SUM(tld.rent_amount) as rentAmount,SUM(tld.cam_amount) as cam_amount FROM tenant_lease_details as tld  WHERE record_status ='1' and user_id  IN ('".$tenId."')";
                        $totalreceivable =   $this->companyConnection->query($getAmountTenantLease)->fetch();
                        dd($totalreceivable);
                    }

                }
            }



        }catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public  function getNotificationData(){
        try {
            if(!empty($_POST['formData'])) {
                $data1= postArray($_POST['formData'], 'true');
            }
            if(isset($data1['years']) && $data1['years'] != ''){
                $d = strtotime($data1['years'].'-'.$data1['annoMonth'].'-01');
                $no_of_days_in_month = date("t",$d);
                $no_of_weeks = (ceil(intval($no_of_days_in_month)/7));
                $week_inc = 0;
                /*$curr_month_year = date("m-Y");*/
                $curr_month_year = date("m-Y",$d);

                //$week_total = [];
                for($i = 1; $i <= $no_of_weeks; $i++){
                    if($week_inc + 7 > $no_of_days_in_month)
                        $day = $no_of_days_in_month;
                    else
                        $day = $week_inc + 7;

                    $week_arr[$i]['start'] = ($week_inc+1)."-".$curr_month_year;
                    $week_arr[$i]['end'] = $day."-".$curr_month_year;
                    $week_start_day = date("Y-m-d",strtotime($week_arr[$i]['start']));
                    $week_end_day = date("Y-m-d",strtotime($week_arr[$i]['end']));

                    if ($i == 0) {
                        $name = 'Week1';
                    } elseif ($i == '1') {
                        $name = 'Week2';
                    } elseif ($i == '2') {
                        $name = 'Week3';
                    } elseif ($i == '3') {
                        $name = 'Week4';
                    } elseif ($i == '4') {
                        $name = 'Week5';
                    }

                    $notification[$name] = $this->companyConnection->query("SELECT COUNT(id) as total FROM `notification_apex` WHERE created_at <> ''  AND  created_at BETWEEN '" . $week_start_day . "' AND '" . $week_end_day . "' GROUP BY DATE_FORMAT(created_at, '%b') ORDER BY DATE_FORMAT(created_at, '%m')")->fetchAll();
                    $week_inc += 7;
                }
            } else {

                $no_of_days_in_month = date("t");

                $no_of_weeks = (ceil(intval($no_of_days_in_month)/7));
                $week_inc = 0;

                $curr_month_year = date("m-Y");

                //$week_total = [];
                for($i = 1; $i <= $no_of_weeks; $i++){
                    if($week_inc + 7 > $no_of_days_in_month)
                        $day = $no_of_days_in_month;
                    else
                        $day = $week_inc + 7;

                    $week_arr[$i]['start'] = ($week_inc+1)."-".$curr_month_year;
                    $week_arr[$i]['end'] = $day."-".$curr_month_year;
                    $week_start_day = date("Y-m-d",strtotime($week_arr[$i]['start']));
                    $week_end_day = date("Y-m-d",strtotime($week_arr[$i]['end']));

                    if ($i == 0) {
                        $name = 'Week1';
                    } elseif ($i == '1') {
                        $name = 'Week2';
                    } elseif ($i == '2') {
                        $name = 'Week3';
                    } elseif ($i == '3') {
                        $name = 'Week4';
                    } elseif ($i == '4') {
                        $name = 'Week5';
                    }

                    $notification[$name] = $this->companyConnection->query("SELECT COUNT(id) as total FROM `notification_apex` WHERE created_at <> ''  AND  created_at BETWEEN '" . $week_start_day . "' AND '" . $week_end_day . "' GROUP BY DATE_FORMAT(created_at, '%b') ORDER BY DATE_FORMAT(created_at, '%m')")->fetchAll();
                    $week_inc += 7;

                }

            }
            $notifi = [];
            foreach ($notification as $key => $value) {
                if (!empty($value[0]['total'])) {
                    array_push($notifi, $value[0]['total']);
                } else {
                    array_push($notifi, 0);
                }
            }
            $maxValue = max($notifi);

            if($maxValue < 10) {
                $maxValue = 10;
                $half =  2 ;
            } else if($maxValue > 50){
                $maxValue = 100;
                $half =  20 ;
            }  else if($maxValue > 10 && $maxValue < 50){
                $maxValue = 50;
                $half =  10 ;
            }  else if($maxValue > 100 && $maxValue < 500){
                $maxValue = 500;
                $half =  100 ;
            }  else if($maxValue > 500 && $maxValue < 1000){
                $maxValue = 1000;
                $half =  200 ;
            }

            $return = array('code' => 200, 'status' => 'success', 'data' => $notifi, 'halfvalue'=>$half,'maxValue'=>$maxValue,'message' => "NotificationData record fetched");
            echo json_encode($return);
            die();
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }


}
$spotlight = new Spotlight();