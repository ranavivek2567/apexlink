<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};
class ContactEdit extends DBConnection
{

    public function __construct()
    {

        parent::__construct();
        $action = $_REQUEST['action'];

        echo json_encode($this->$action());
    }

    /**
     *  function for Referral Source
     */
    /*Emergency Contact Detail Starts*/


    /*Emergency Contact Details Ends*/

    public function getCredentialInfo($id)
    {
        $getCredentialInfo = $this->companyConnection->query("SELECT * FROM tenant_credential WHERE user_id ='" . $id . "'")->fetchAll();
        $getCredentialType = $this->companyConnection->query("SELECT * FROM tenant_credential_type")->fetchAll();

        $today = date('Y-m-d H:i:s');
        $todayDate = dateFormatUser($today, null, $this->companyConnection);


        $html = "";
        if (empty($getCredentialInfo)) {
            $html .= '<div class="row tenant-credentials-control">
                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Credential Name</label>
                                                        <input class="form-control capsOn" type="text"
                                                               id="credentialName" name="credential_name[]" maxlength="18">
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 apx-inline-popup credintials">
                                                        <label>Credential Type
                                                            <a class="pop-add-icon tenantCredentialType" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="credential_type[]">
                                                            <option value="">Select</option>
                                                            <option value="1">Bond</option>
                                                            <option value="2">Certification</option>
                                                        </select>
                                                        <div class="add-popup" id="tenantCredentialType1">
                                                            <h4>Add Credential Type</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Credential Type <em class="red-star">*</em></label>
                                                                        <input class="form-control credential_source" type="text" placeholder="Ex: License" name=""credential_type[]>
                                                                        <span class="red-star" id="credential_source"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Acquire Date</label>
                                                        <input class="form-control capsOn calander" type="text"
                                                               id="acquireDate" name="acquire_date[]" value=' . $todayDate . '>
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Expiration Date</label>
                                                        <input class="form-control capsOn calander" type="text"
                                                               id="expirationDate" name="expire_date[]" value=' . $todayDate . '>
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                </div>
                                                <div class="notice-period">
                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>Notice Period</label>
                                                        <select class="form-control add-input" id="term_plan"
                                                                name="notice_period[]">
                                                            <option value="">Select</option>
                                                            <option value="1">5 day</option>
                                                            <option value="2">1 Month</option>
                                                            <option value="3">2 Month</option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                        <a class="add-icon add-notice-period" href="javascript:;"><i
                                                                    class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                        <a class="add-icon remove-notice-period" style="display:none;">
                                                            <i class="fa fa-minus-circle " aria-hidden="true" style="display:block !important;"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>';
        } else {
            $i = 0;
            foreach ($getCredentialInfo as $credentialInfo) {

                $acquire_date = dateFormatUser($credentialInfo['acquire_date'], null, $this->companyConnection);

                $expire_date = dateFormatUser($credentialInfo['expire_date'], null, $this->companyConnection);

                $html .= '  <div class="row tenant-credentials-control">
                                                    <div class="col-sm-2">
                                                        <label>Credential Name</label>
                                                        <input class="form-control" type="text" name="credential_name[]" value="' . $credentialInfo['credential_name'] . '" maxlength="18">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Credential Type <a class="pop-add-icon credntl_popup"
                                                                                  href="javascript:;"><i
                                                                        class="fa fa-plus-circle"
                                                                        aria-hidden="true"></i></a></label>
                                                        <select class="form-control edit_general_credentials" name="credential_type[]">';

                foreach ($getCredentialType as $credentialType) {
                    if ($credentialType['id'] == $credentialInfo['credential_type']) {
                        $selected = "selected=selected";
                    } else {
                        $selected = "";
                    }

                    $html .= "<option value=" . $credentialType['id'] . " " . $selected . ">" . $credentialType['credential_type'] . "</option>";
                }


                $html .= '</select><div class="add-popup" id="tenantCredentialType1">
                                                                <h4>Add Credential Type</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>Credential Type <em class="red-star">*</em></label>
                                                                            <input class="form-control credential_source" type="text" placeholder="Ex: License">
                                                                            <span class="red-star" id="credential_source"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                $html .= '<div class="col-sm-2">
                                                        <label>Acquire Date</label>
                                                        <input class="form-control calander" type="text" value="' . $acquire_date . '" name="acquire_date[]">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Expiration Date </label>
                                                        <input class="form-control calander" type="text"  value="' . $expire_date . '" name="expire_date[]">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Notice Period </label>
                                                        <input class="form-control add-input calander" type="text" name="notice_period[]" value="' . $credentialInfo['notice_period'] . '">';
                if ($i != 0) {


                    $html .= '<a class="add-icon remove-notice-period"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                } else {
                    $html .= '<a class="add-icon add-notice-period" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>';

                    $html .= '<a class="add-icon remove-notice-period" style="display:none"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                }
                $html .= '</div>
                                                                    </div>';
                $i++;
            }

        }


        return $html;
    }


    /* Full Data For Edit Contact*/

    public function getContactEdit()
    {


        try {
            //dd("here");
            $id = $_POST['id'];

            $sql1 = "SELECT * FROM  users WHERE id=" . $id;
            $data0 = $this->companyConnection->query($sql1)->fetch();

            $ssn = unserialize($data0['ssn_sin_id']);
            $ssnHtml = '';
            if (is_array($ssn)) {
                foreach ($ssn as $keyssn => $ssnval) {
                    if ($keyssn == '0') {
                        $ssnHtml .= '<div class="col-xs-12 contact-ssn-row">
                        <div>
                            <label>SSN/SIN/ID <em class="red-star">*</em></label>
                            <input class="form-control fax error add-input" type="text" maxlength="12" placeholder="SSN/SIN/ID" fixedlength="12" id="contactt_ssn_sin_id" name="ssn_sin_id" value="' . $ssnval . '" aria-invalid="true">
                        </div>
                        <div class="ssn-add-remove-row" style="padding: 0px;float: left;">
                            <span class="glyphicon glyphicon-remove-sign clone-add"></span>
                            <span class="glyphicon glyphicon-plus-sign clone-add"></span>
                        </div>
                    </div>';
                    } else {
                        $ssnHtml .= '<div class="col-xs-12 contact-ssn-row">
                        <div>
                            <label>SSN/SIN/ID <em class="red-star">*</em></label>
                            <input class="form-control fax error add-input" type="text" maxlength="12" placeholder="SSN/SIN/ID" fixedlength="12" id="contactt_ssn_sin_id" name="ssn_sin_id" value="' . $ssnval . '" aria-invalid="true">
                        </div>
                        <div class="ssn-add-remove-row" style="padding: 0px;float: left;">
                            <span class="glyphicon glyphicon-remove-sign clone-add"></span>
                            <span class="glyphicon glyphicon-plus-sign clone-add" style="display: none;"></span>
                        </div>
                    </div>';
                    }
                }
            } else {
                if ($ssn == "") {
                    $ssnHtml .= '<div class="col-xs-12 contact-ssn-row">
                        <div>
                            <label>SSN/SIN/ID <em class="red-star">*</em></label>
                            <input class="form-control fax error add-input" type="text" maxlength="12" placeholder="SSN/SIN/ID" fixedlength="12" id="contactt_ssn_sin_id" name="ssn_sin_id" value="" aria-invalid="true">
                        </div>
                        <div class="ssn-add-remove-row" style="padding: 0px;float: left;">
                            <span class="glyphicon glyphicon-remove-sign clone-add"></span>
                            <span class="glyphicon glyphicon-plus-sign clone-add"></span>
                        </div>
                    </div>';
                }

            }
            $ssn = $ssnHtml;
            /* ssn id clone data fetch ends
            */
            $sql1 = "SELECT * FROM  tenant_details WHERE user_id=" . $id;
            $data1 = $this->companyConnection->query($sql1)->fetch();

            $email = $this->getGeneralEmails($data1['email1'], $data1['email2'], $data1['email3']);

//            $sql2 = "SELECT * FROM  emergency_details	 WHERE user_id=".$id;
//            $data2 = $this->companyConnection->query($sql2)->fetch();
            $emergencyContactINFO = $this->getEmergencyInfo($id);
            $PhoneTypeInfo = $this->getPhoneInfo($id);

            $sql3 = "SELECT * FROM  tenant_phone WHERE user_id=" . $id;
            $data3 = $this->companyConnection->query($sql3)->fetch();

            $credentialTypes = $this->getCredentialInfo($id);

            // $contactPhoneType = $this->getPhoneInfo($id);

            $sql4 = "SELECT * FROM  contact_notes WHERE user_id=" . $id;
            $data4 = $this->companyConnection->query($sql4)->fetch();

            /* Multiple notes starts*/
            $contact_notes = $data4['contact_notes'];

            $contact_notesHtml = '';
            $contactNotesHtml = '';
            if (is_array($contact_notes)) {
                foreach ($contact_notes as $keynotes => $contactnotesval) {
                    if ($keynotes == '0') {

                        $contactNotesHtml .= '<div class="row primary-notes-row">
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <textarea class="form-control add-input" name="contact_notes[]" id="contactt_notes">' . $contactnotesval . '</textarea>
                                        <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                        <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true" style="display:none;"></i></a>
                                        <span class="ffirst_nameErr error red-star"></span>
                                        <span class="term_planErr error red-star"></span>
                                    </div>
                                </div>';
                    } else {
                        $contactNotesHtml .= '<div class="row primary-notes-row">
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <textarea class="form-control add-input" name="contact_notes[]" id="contactt_notes">' . $contactnotesval . '</textarea>
                                        <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"  style="display:none;"></i></a>
                                        <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                        <span class="ffirst_nameErr error red-star"></span>
                                        <span class="term_planErr error red-star"></span>
                                    </div>
                                </div>';
                    }
                    $contact_notes = $contactNotesHtml;
                }
            } else {
                if ($contact_notes == "") {
                    $contactNotesHtml .= '<div class="row primary-notes-row">
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <textarea class="form-control add-input" name="contact_notes[]" id="contactt_notes"></textarea>
                                        <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                        <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"  style="display:none;"></i></a>
                                        <span class="ffirst_nameErr error red-star"></span>
                                        <span class="term_planErr error red-star"></span>
                                    </div>
                                </div>';
                    $contact_notes = $contactNotesHtml;
                } else {
                    $contactNotesHtml .= '<div class="row primary-notes-row">
                                    <div class="col-xs-12 col-sm-4 col-md-4">
                                        <textarea class="form-control add-input" name="contact_notes[]" id="contactt_notes">' . $contact_notes . '</textarea>
                                        <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                        <a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"  style="display:none;"></i></a>
                                        <span class="ffirst_nameErr error red-star"></span>
                                        <span class="term_planErr error red-star"></span>
                                    </div>
                                </div>';
                    $contact_notes = $contactNotesHtml;
                }
            }


            /* Multiple notes Ends*/

            $sql5 = "SELECT * FROM  tenant_credential WHERE user_id=" . $id;
            $data5 = $this->companyConnection->query($sql5)->fetch();


            return array('code' => 200, 'status' => 'success', 'data' => ['users' => $data0, 'tenantDetail' => $data1, 'tenant_credential' => $credentialTypes, 'emergencyContact' => $emergencyContactINFO, 'tenantPhone' => $data3, 'phoneInfo' => $PhoneTypeInfo, 'ssn' => $ssn, 'contactNotes' => $data4, 'credentials' => $data5, 'contactt_notes' => $contact_notes, 'email' => $email], 'message' => 'Record retrieved successfully');
        } catch (Exception $exception) {
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }


    public function getGeneralEmails($email1, $email2, $email3)
    {
        $html = '';
        $html .= '<div class="">
      <label>Email <em class="red-star">*</em></label>
      <div class="multipleEmail">
      <input class="form-control add-input" type="text" name="email[]" value="' . $email1 . '" maxlength="100">
     <a class="add-icon email-plus-sign" href="javascript:;"><i
                      class="fa fa-plus-circle"
                      aria-hidden="true"></i></a>
      <a class="add-icon email-remove-sign" href="javascript:;" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>                
      </div>';
        if ($email2 != "") {
            $html .= '
      <div class="multipleEmail">
      <input class="form-control add-input" type="text" name="email[]" value="' . $email2 . '" maxlength="100">
  <a class="add-icon email-remove-sign" href="javascript:;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
      </div>';
        }


        if ($email3 != "") {
            $html .= '
      <div class="multipleEmail">
      <input class="form-control add-input" type="text" name="email[]" value="' . $email3 . '" maxlength="100">
  <a class="add-icon email-remove-sign" href="javascript:;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
      </div>';
        }


        $html .= '</div>';

        return $html;


    }


    /*Get Emergency Contact Details*/

    public function UpdateContactDetails()
    {
        try {
            $id = $_POST['id'];
            //print_r($id);die;
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);

            $data = $_POST;
            $data1 = [];
            $data1['salutation'] = $data['salutation'];
            $data1['referral_source'] = $data['referral'];
            $data1['ssn_sin_id'] = serialize($data['ssn_sin_id']);
            $data1['first_name'] = $data['first_name'];
            if (!empty($data['dob'])) {
                $data1['dob'] = mySqlDateFormat($data['dob'], null, $this->companyConnection);

            } else {
                $data1['dob'] = null;
            }
            $data1['last_name'] = $data['last_name'];
            $data1['middle_name'] = $data['middle_name'];
            $data1['maiden_name'] = $data['maiden_name'];
            $data1['nick_name'] = $data['nick_name'];
            $data1['last_name'] = $data['last_name'];
            $data1['name'] = $data['first_name'] . ' ' . $data['last_name'];
            $data1['email'] = (!is_array($data['email'])) ? $data['email'] : $data['email'][0];
            $data1['phone_number'] = (!is_array($data['phoneNumber'])) ? $data['phoneNumber'] : $data['phoneNumber'][0];
            $data1['gender'] = $data['gender'];
            $data1['address1'] = $data['address1'];
            $data1['address2'] = $data['address2'];
            $data1['address3'] = $data['address3'];
            $data1['address4'] = $data['address4'];
            $data1['zipcode'] = $data['zipcode'];
            $data1['country'] = $data['country'];
            $data1['state'] = $data['state'];
            $data1['city'] = $data['city'];
            $data1['phone_number_note'] = $data['phone_number_note'];
            $data1['ethnicity'] = $data['title'];
            $data1['hobbies'] = (!empty($data['hobbies'])) ? serialize($data['hobbies']) : null;
            $data1['status'] = '1';
            $data1['user_type'] = '5';
            $data1['domain_name'] = $subdomain;
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
         //   print_r($data1);return;
            $sqlData = createSqlColValPair($data1);

            $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            if (isset($data['contact_notes']) != "") {

                $this->updateContactNotes($id, $data);
            }

            if (isset($data['emergency_contact_name']) != "") {

                $this->updateEmergencyContant($id, $data);
            }

            if (isset($data['credential_name']) != "") {

                $this->updateCredentialControl($id, $data);
            }

            if (isset($data['phoneType']) != "") {

                $this->updatePhoneInfo($id, $data);
            }

            return array('code' => 200, 'status' => 'success', 'message' => 'Data Retrieved Successfully.', 'table' => 'users', $id => 'id');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'users');
            printErrorLog($e->getMessage());
        }


        // }

    }


    /* Update Multiple Phone Clone */


    public function updateCredentialControl($id)
    {

        $count = $this->companyConnection->prepare("DELETE FROM tenant_credential WHERE  user_id=$id");
        $count->execute();

        try {
            if (isset($_POST['credential_name'])) {


                $i = 0;
                foreach ($_POST['credential_name'] as $k => $val1) {
                    // $acquire_date = $_POST['acquire_date'][$i];
                    // $expireDate = $_POST['expirationDate'][$i];
                    $data['user_id'] = $id;

                    $data['credential_name'] = $_POST['credential_name'][$k];
                    $data['credential_type'] = $_POST['credential_type'][$k];
                    // $data['acquire_date'] = mySqlDateFormat($acquire_date,null,$this->companyConnection);
                    //$data['expire_date'] = mySqlDateFormat($expireDate,null,$this->companyConnection);
                    $data['notice_period'] = $_POST['notice_period'][$k];
//                    print_r($data);
//                    die;
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_credential(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt1 = $this->companyConnection->prepare($query);
                    $stmt1->execute($data);

                    $i++;
                }
            }


            return array('status' => 'success', 'message' => 'Data Retrieved Successfully.', 'table' => 'tenant_credentials');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_credentials');
            printErrorLog($e->getMessage());
        }

    }

    public function updateContactNotes($id, $data)
    {
        $i = 0;
        $id = $_POST['id'];
        if (is_array($data['contact_notes'])) {

            foreach ($data['contact_notes'] as $k => $val1) {
                $data1['type'] = 1;
                $data1['record_status'] = 2;
                $data1['contact_notes'] = $data['contact_notes'][$k];
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data1);
                $query = "UPDATE contact_notes SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $i++;
            }
        } else {
            $id = $_POST['id'];
            $data1['type'] = 1;
            $data1['record_status'] = 2;
            $data1['contact_notes'] = $data['contact_notes'];
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE contact_notes SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }
        return "data updated";
    }


    public function updateEmergencyContant($id)
    {
        // print_r($id);die;
        $count = $this->companyConnection->prepare("DELETE FROM emergency_details WHERE  user_id=$id");
        $count->execute();
        try {


            $i = 0;
            foreach ($_POST['emergency_contact_name'] as $credentialName) {

                $data['user_id'] = $id;
                $data['parent_id'] = 0;
                $data['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $data['emergency_relation'] = $_POST['emergency_relation'][$i];
                $data['emergency_country_code'] = $_POST['emergency_country'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $data['emergency_email'] = $_POST['emergency_email'][$i];


                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;

            }

            return array('status' => 'success', 'message' => 'Data Retrieved Successfully.', 'table' => 'emergency_details');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'emergency_details');
            printErrorLog($e->getMessage());
        }


    }

    public function updatePhoneInfo($id)
    {
        try {
            $phoneType = $_POST['phoneType'];
            $count = $this->companyConnection->prepare("DELETE FROM tenant_phone WHERE  parent_id=0 and user_id=$id");
            $count->execute();
            $i = 0;
            foreach ($phoneType as $phone) {
                $data['user_id'] = $id;
                $data['parent_id'] = 0;
                $data['phone_type'] = $phone;
                $data['carrier'] = $_POST['carrier'][$i];
                $data['phone_number'] = $_POST['phoneNumber'][$i];
                $data['country_code'] = $_POST['countryCode'][$i];
                $data['user_type'] = 5; //0 value for main tenant
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $i++;
            }
            return array('status' => 'success', 'message' => 'Data Retrieved Successfully.', 'table' => 'tenant_phone');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'table' => "tenant_details");
            printErrorLog($e->getMessage());
        }
    }


    public function addFileLibrary($idd, $fileData)
    {
        try {
            $files = $fileData;

            $user_id = $idd;

            if (!empty($files)) {

                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
                foreach ($files as $key => $value) {

                    $file_name = $value['name'];
                    $fileData = getSingleRecord($this->companyConnection, ['column' => 'name', 'value' => $file_name], 'tenant_file_library');
                    if ($fileData['code'] == '200') {
                        return array('code' => 500, 'status' => 'warning', 'message' => 'Document allready exists.');
                    }
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    //Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    //  $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $data['user_id'] = $user_id;
                    $data['name'] = $file_name;
//                    $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                    $data['path'] = $path . '/' . $name . '.' . $ext;
                    $data['extension'] = $ext;
//                    $data['marketing_site'] = '0';
//                    $data['file_type'] = '2';
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_file_library (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents uploaded successfully.');
            }
        } catch (PDOException $e) {
            dd($e);
            return array('code' => 500, 'status' => 'failed', 'message' => $e->getMessage());
        }
    }


//    public function savupdateContactPropertyData($id,$data)
//    {
//        try{
//            $data1['user_id'] = $id;
//            $data1['record_status'] = 0;
//            $data1['contact'] = $id;
//            $data1['property_id'] = (!empty($data['property']) && $data['property'] != 'Select')? $data['property']:null;
//            $data1['building_id'] = (!empty($data['building']) && $data['building'] != 'Select')? $data['building']:null;
//            $data1['unit_id'] = (!empty($data['unit']) && $data['unit'] != 'Select')? $data['unit']:null;
//            $data1['created_at'] = date('Y-m-d H:i:s');
//            $data1['updated_at'] = date('Y-m-d H:i:s');
//            $sqlData = createSqlColValPair($data1);
//            $query = "UPDATE tenant_property SET " . $sqlData['columnsValuesPair'] ." where user_id=".$id;
//            $stmt = $this->companyConnection->prepare($query);
//            $stmt->execute();
//
//            return array('code' => 200, 'status' => 'success','message' => "Data");
//        }
//        catch (PDOException $e) {
//            dd($e);
//            return array('code' => 400, 'status' => 'failed','message' => "something Went wrong on tenant property");
//            printErrorLog($e->getMessage());
//        }
//
//    }


    public function Emails($email1, $email2, $email3)
    {
        $html = '';
        $html .= ' <div class="col-xs-12 primary-email-row">
                    <div >
                        <label>Email <em class="red-star">*</em> <!--<a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a> -->
                        </label>
                        <!--<input placeholder="Fax Number" name="unit_fax_no" type="text" class="form-control add-input" maxlength="12"/>-->
                        <input class="form-control fax error add-input" type="text" placeholder="Email" fixedlength="12" id="con_email" name="email[]" value="" aria-invalid="true">
                    </div>
                    <div class="email-add-remove-row" style="padding: 0px;float: left;">
                        <span class="glyphicon glyphicon-remove-sign clone-add"></span>
                        <span class="glyphicon glyphicon-plus-sign clone-add"></span>
                    </div>
                </div>';
        if ($email2 != "") {
            $html .= '
       <div class="col-xs-12 primary-email-row">
            <div >
                <label>Email <em class="red-star">*</em> <!--<a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a> -->
                </label>
                <!--<input placeholder="Fax Number" name="unit_fax_no" type="text" class="form-control add-input" maxlength="12"/>-->
                <input class="form-control fax error add-input" type="text" placeholder="Email" fixedlength="12" id="con_email" name="email[]" value="" aria-invalid="true">
            </div>
            <div class="email-add-remove-row" style="padding: 0px;float: left;">
                <span class="glyphicon glyphicon-remove-sign clone-add"></span>
                <span class="glyphicon glyphicon-plus-sign clone-add"></span>
            </div>
        </div>';
        }


        if ($email3 != "") {
            $html .= '
       <div class="col-xs-12 primary-email-row">
        <div >
            <label>Email <em class="red-star">*</em> <!--<a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a> -->
            </label>
            <!--<input placeholder="Fax Number" name="unit_fax_no" type="text" class="form-control add-input" maxlength="12"/>-->
            <input class="form-control fax error add-input" type="text" placeholder="Email" fixedlength="12" id="con_email" name="email[]" value="" aria-invalid="true">
        </div>
        <div class="email-add-remove-row" style="padding: 0px;float: left;">
            <span class="glyphicon glyphicon-remove-sign clone-add"></span>
            <span class="glyphicon glyphicon-plus-sign clone-add"></span>
        </div>
    </div>';
        }


        $html .= '</div>';

        return $html;

    }


//
    public function addAdditionalEmail($id, $data)
    {
        try {

            $data = $_POST['form'];
            $data = postArray($data);
            $data1['user_id'] = $id;
            $email = $data['email'];
            if (is_array($email)) {

                $Count = count($email);
                if ($Count == 3) {
                    $email1 = $email[0];
                    $email2 = $email[1];
                    $email3 = $email[2];
                } else if ($Count == 2) {
                    $email1 = $email[0];
                    $email2 = $email[1];
                    $email3 = "";
                } else {
                    $email1 = $email[0];
                    $email2 = "";
                    $email3 = "";
                }
                $data1['email1'] = $email1;
                $data1['email2'] = $email2;
                $data1['email3'] = $email3;
            } else {


                $data1['email'] = $data['email'];
//
            }

            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE tenant_details SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'message' => "Data");
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => "something Went wrong on tenant property");
            printErrorLog($e->getMessage());
        }

    }


//emergency detail

    public function getEmergencyInfo($tenant_id)
    {
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $tenant_id . "' and parent_id=0")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html = "";
        if (empty($getEmergencyInfo)) {
            $html .= '<div class="row tenant-emergency-contact">
                                                    <div class="form-outer">

                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Emergency Contact Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="emergency" name="emergency_contact_name[]" maxlength="18">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Relationship</label>
                                                            <select class="form-control" id="relationship"
                                                                    name="emergency_relation[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Daughter</option>
                                                                <option value="2">Father</option>
                                                                <option value="3">Friend</option>
                                                                <option value="4">Mother</option>
                                                                <option value="5">Owner</option>
                                                                <option value="6">Partner</option>
                                                                <option value="7">Son</option>
                                                                <option value="8">Spouse</option>
                                                                <option value="9">Other</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 countycodediv">
                                                            <label>Country Code</label>
                                                            <select class="form-control" name="emergency_country[]">
                                                                <option value="">Select</option>
                                                                <option value="1">test</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">

                                                            <label>Phone</label>
                                                            <input class="form-control capsOn phone_format" type="text"
                                                                   id="phoneNumber"
                                                                   name="emergency_phone[]" maxlenght="12">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="clearfix"></div>

                                                    </div>

                                               
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Email</label>
                                                            <input class="form-control capsOn" type="text" 
                                                                   name="emergency_email[]" maxlength="100">
                                                                      <a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                                     <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true" style="display:block !important;"></i></a>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    

                                                </div>';


        } else {
            $i = 0;
            foreach ($getEmergencyInfo as $emergencyInfo) {
                $html .= '
                                                <div class="row tenant-emergency-contact">
                                                    <div class="col-sm-2">
                                                        <label>Emergency Contact Name</label>
                                              <input class="form-control" type="text" value="' . $emergencyInfo['emergency_contact_name'] . '" name="emergency_contact_name[]" maxlength="18">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Relationship</label>
                                                        <select class="form-control" name="emergency_relation[]">';
                $relationArr = array("" => "Select", "1" => "Daughter", "2" => "Father", "3" => "Friend", "4" => "Mother", "5" => "Owner", "6" => "Partner", "7" => "Son", "8" => "Spouse", "9" => "Other");
                foreach ($relationArr as $key => $value) {
                    if ($key == $emergencyInfo['emergency_relation']) {
                        $html .= '<option value="' . $key . '" selected>' . $value . '</option>';
                    } else {
                        $html .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                }

                $html .= '</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="emergency_country[]">';
                foreach ($countryCode as $code) {
                    if ($code['id'] == $emergencyInfo['emergency_country_code']) {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] . "</option>";
                    } else {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "</option>";
                    }
                }
                $html .= '</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Phone Number</label>
                                            <input class="form-control phone_format" type="text" maxlength="12" name="emergency_phone[]" value="' . $emergencyInfo['emergency_phone_number'] . '">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Email</label>
                                                        <input class="form-control add-input" type="text" value="' . $emergencyInfo['emergency_email'] . '" name="emergency_email[]" maxlength="100">';

                if ($i != 0) {


                    $html .= '<a class="add-icon remove-emergency-contant"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                } else {
                    $html .= '<a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>';

                    $html .= '<a class="add-icon remove-emergency-contant" style="display:none"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                }


                $html .= '</div>
                                </div>';

                $i++;
            }


        }

        return $html;

    }

//phone Number div clone


    public function getPhoneInfo($id)

    {

        $getPhoneInfo = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE parent_id=0 and user_id ='" . $id . "'")->fetchAll();

        $getPhoneType = $this->companyConnection->query("SELECT * FROM phone_type")->fetchAll();

        $getCarrierInfo = $this->companyConnection->query("SELECT * FROM carrier")->fetchAll();

        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();

        $html = "";

        if (empty($getPhoneInfo)) {

            $html .= '<div class="row primary-tenant-phone-row" id="primary-tenant-phone-row-divId">

                                                <div class="form-outer">

                                                    <div class="col-sm-12 col-md-3 ">

                                                        <label>Phone Type</label>

                                                        <select class="form-control" name="phoneType[]">

                                                            <option value="">Select</option>

                                                            <option value="1">mobile</option>

                                                            <option value="2">work</option>

                                                            <option value="3">Fax</option>

                                                            <option value="4">Home</option>

                                                            <option value="5">Other</option>

                                                        </select>

                                                        <span class="term_planErr error red-star"></span>

                                                    </div>

                                                    <div class="col-sm-12 col-md-3 ">

                                                        <label>Carrier <em class="red-star">*</em></label>

                                                        <select class="form-control" name="carrier[]">';

            foreach ($getCarrierInfo as $carrierInfo) {

                $html .= "<option value=" . $carrierInfo['id'] . ">" . $carrierInfo['carrier'] . "</option>";

            }

            $html .= '</select>

                                                        <span class="term_planErr error red-star"></span>

                                                    </div>

                                                    <div class="col-sm-12 col-md-3 countycodediv">

                                                        <label>Country Code</label>

                                                        <select class="form-control" name="countryCode[]">';

            foreach ($countryCode as $code) {

                $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "</option>";

            }

            $html .= '</select>

                                                        <span class="term_planErr error red-star"></span>

                                                    </div>

                                                    <div class="col-sm-12 col-md-3 ">

                                                        <label>Phone Number <em class="red-star">*</em></label>

                                                        <input class="form-control capsOn add-input phone_format" type="text"

                                                               name="phoneNumber[]" maxlenght="12">

                                                        <a class="add-icon" href="javascript:;"><i

                                                                    class="fa fa-plus-circle"

                                                                    aria-hidden="true"></i></a>

                                                        <a class="add-icon" href="javascript:;"><i

                                                                    class="fa fa-times-circle" style="display:none"

                                                                    aria-hidden="true"></i></a>



                                                    </div>

                                                    <div class="clearfix"></div>

                                                </div>

                                            </div>';

        } else {

            $i = 0;

            foreach ($getPhoneInfo as $phoneInfo) {

                $html .= '<div class="row primary-tenant-phone-row" id="primary-tenant-phone-row-divId">

                                  <div class="col-sm-3">

                                      <label>Phone Type</label>

                                     <select class="form-control" name="phoneType[]">';

                foreach ($getPhoneType as $phoneType) {

                    if ($phoneType['id'] == $phoneInfo['phone_type']) {

                        $selected = "selected=selected";

                    } else {

                        $selected = "";

                    }

                    $html .= "<option value=" . $phoneType['id'] . " " . $selected . ">" . $phoneType['type'] . "</option>";

                }

                $html .= '</select>

                                     </div>

                                      <div class="col-sm-3">

                                      <label>Carrier <em class="red-star">*</em></label>

                                      <select class="form-control" name="carrier[]">';

                foreach ($getCarrierInfo as $carrierInfo) {

                    if ($carrierInfo['id'] == $phoneInfo['carrier']) {

                        $selected = "selected=selected";

                    } else {

                        $selected = "";

                    }

                    $html .= "<option value=" . $carrierInfo['id'] . " " . $selected . ">" . $carrierInfo['carrier'] . "</option>";

                }

                $html .= '</select></div>

                                  <div class="col-sm-3">

                                      <label>Country Code</label>

                                         <select class="form-control" name="countryCode[]">';

                foreach ($countryCode as $code) {

                    if ($code['id'] == $phoneInfo['country_code']) {

                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] . "</option>";

                    } else {

                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "</option>";

                    }

                }

                $html .= '</select></div>





                                  <div class="col-sm-3">

                                      <label>Phone Number</label>

                                      <input class="form-control add-input phone_format" maxlength="12" type="text" name="phoneNumber[]" value="' . $phoneInfo['phone_number'] . '">';

                if ($i == 0) {

                    $html .= '<a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';

                    $html .= '<a class="add-icon" href="javascript:;" ><i style="display:none" class="fa fa-times-circle" aria-hidden="true"></i></a>';

                }

                if ($i != 0) {

                    $html .= '<a class="add-icon" href="javascript:;" style=""><i class="fa fa-times-circle" aria-hidden="true"></i></a>';

                }

                $html .= '</div>

                              </div>';

                $i++;

            }

        }

        return $html;

    }

    public function file_library($id)
    {
        try {
            $files = $_FILES;
            $contact_id = $id;
            if (!empty($files)) {
                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
                foreach ($files as $key => $value) {
                    $file_name = $value['name'];
                    $fileData = getSingleRecord($this->companyConnection, ['column' => 'file_name', 'value' => $file_name], 'property_file_uploads');
                    if ($fileData['code'] == '200') {
                        return array('code' => 500, 'status' => 'warning', 'message' => 'Document allready exists.');
                    }
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    //Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    $data['user_id'] = $contact_id;
                    $data['name'] = $file_name;
                    $data['path'] = $path . '/' . $name . '.' . $ext;
                    $data['extension'] = $ext;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_file_library (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents uploaded successfully.');
            }
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed', 'message' => $e->getMessage());
        }
    }


//    public function deleteFile(){
//        try {
//            $id = $_POST['id'];
//            $data = getSingleRecord($this->companyConnection,['column'=>'id','value'=>$id],'property_file_uploads');
//            $query = "DELETE FROM tenant_file_library WHERE id=$id";
//            $stmt = $this->companyConnection->prepare($query);
//            if($stmt->execute()){
//                $path = ROOT_URL . '/company/'.$data['data']['file_location'];
//                unlink($path);
//                return array('code' => 200, 'status' => 'success', 'message' => 'Documents deleted successfully.');
//            } else {
//                return array('code' => 500, 'status' => 'error', 'message' => 'Error occured while deleting document.');
//            }
//        } catch (PDOException $e) {
//            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
//        }
//    }

/*Phone Number Clone Starts*/

}

$ContactEdit = new ContactEdit();
