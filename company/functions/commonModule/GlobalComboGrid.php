<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class GlobalComboGrid extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getPropertiesdata() {

        try {
            $search = @$_REQUEST['q'];
            if(empty($search)){
                $sql = "SELECT * FROM general_property  where status = '1' order by property_name ASC";
            } else {
                $sql = "SELECT * FROM general_property  where status = '1' and property_name LIKE '%$search%' order by property_name ASC";
            }
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);

            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['property_name'];
                    $data['rows'][$key]['address1'] = $user_data['address1'] . ', ' . $user_data['address2'] . ' ' . $user_data['address3'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    public function getBuildingdata() {

        try {
            $search = @$_REQUEST['q'];
            $property_id = $_REQUEST['property_id'];
            if(empty($search)){
                $sql = "SELECT * FROM building_detail  where status = '1' and property_id =$property_id  order by building_name ASC";
            } else {
                $sql = "SELECT * FROM building_detail  where status = '1' and property_id =$property_id and building_name LIKE '%$search%' order by building_name ASC";
            }
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);

            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['building_name'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    public function getUnitdata() {

        try {
            $search = @$_REQUEST['q'];
            $building_id = $_REQUEST['building_id'];
            if(empty($search)){
                $sql = "SELECT * FROM unit_details  where status = '1' and building_id =$building_id  order by unit_prefix ASC";
            } else {
                $sql = "SELECT * FROM unit_details  where status = '1' and building_id =$building_id and unit_prefix LIKE '%$search%' order by unit_prefix ASC";
            }
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);

            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['unit_prefix'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    public function getUserdata() {
        try {
            $search = @$_REQUEST['q'];
            $user_type = $_REQUEST['user_type'];
            $join = ' LEFT JOIN vendor_additional_detail ON users.id=vendor_additional_detail.id LEFT JOIN company_vendor_type ON vendor_additional_detail.vendor_type_id=company_vendor_type.id ';
            if(empty($search)){
                if($user_type == '3'){
                    $sql = "SELECT users.city,users.state,users.zipcode,users.id,users.country,users.name,users.email,users.address1,users.address2,users.address3,users.address4,vendor_additional_detail.vendor_rate,company_vendor_type.vendor_type FROM users ".$join." where users.status = '1' and users.user_type = '".$user_type."' order by name ASC";
                } else if($user_type == '2'){
                    $join = ' LEFT JOIN tenant_property ON users.id=tenant_property.id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN building_detail ON tenant_property.building_id=building_detail.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id ';
                    $sql = "SELECT users.id,users.name,users.email,general_property.property_name,general_property.id as property_id,building_detail.building_name,unit_details.unit_no FROM users ".$join." where users.status = '1' and users.user_type = '".$user_type."' order by name ASC";
                } else {
                    $sql = "SELECT users.city,users.state,users.zipcode,users.id,users.country,users.name,users.email,users.address1,users.address2,users.address3,users.address4 FROM users where users.status = '1' and users.user_type = '".$user_type."' order by name ASC";
                }
            } else {
                if($user_type == '3'){
                    $sql = "SELECT users.city,users.state,users.zipcode,users.country,users.id,users.name,users.email,vendor_additional_detail.vendor_rate,company_vendor_type.vendor_type,users.address1,users.address2,users.address3,users.address4 FROM users ".$join." where users.status = '1' and user_type = '".$user_type."' AND users.name LIKE '%$search%' order by users.name ASC";
                } else if($user_type == '2'){
                    $join = ' LEFT JOIN tenant_property ON users.id=tenant_property.id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN building_detail ON tenant_property.building_id=building_detail.id LEFT JOIN unit_details ON tenant_property.unit_id=unit_details.id ';
                    $sql = "SELECT users.id,users.name,users.email,general_property.property_name,general_property.id as property_id,building_detail.building_name,unit_details.unit_no FROM users ".$join." where users.status = '1' and user_type = '".$user_type."' AND users.name LIKE '%$search%' order by users.name ASC";
                } else {
                    $sql = "SELECT users.city,users.state,users.zipcode,users.country,users.id,users.name,users.email,users.address1,users.address2,users.address3,users.address4 FROM users where users.status = '1' and users.user_type = '".$user_type."' AND users.name LIKE '%$search%' order by users.name ASC";
                }

            }
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);
            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $value) {
                    if($user_type == '3') {
                        $data['total'] = $users_count;
                        $data['rows'][$key]['id'] = $value['id'];
                        $data['rows'][$key]['name'] = $value['name'];
                        $data['rows'][$key]['email'] = (isset($value['email']) && !empty($value['email'])) ? $value['email'] : 'N/A';
                        $data['rows'][$key]['vendor_type'] = (isset($value['vendor_type']) && !empty($value['vendor_type'])) ? $value['vendor_type'] : 'N/A';
                        $data['rows'][$key]['vendor_rate'] = (isset($value['vendor_rate']) && !empty($value['vendor_rate'])) ? $value['vendor_rate'] : 'N/A';
                        $data['rows'][$key]['city'] = $value['city'];
                        $data['rows'][$key]['state'] = $value['state'];
                        $data['rows'][$key]['zipcode'] = $value['zipcode'];
                        $data['rows'][$key]['country'] = $value['country'];
                        $address = '';
                        $addressFormated = $value['name'] . "\n\n";
                        $cityAddress = '';
                        $stateAddress = '';
                        $zipcode = '';
                        $country = '';
                        foreach ($value as $key1 => $value1) {
                            if ($key1 == 'address1' || $key1 == 'address2' || $key1 == 'address3') {
                                if (!empty($value1)) {
                                    $address .= $value1 . ',';
                                    $addressFormated .= $value1 . ",\n";
                                }
                                $data['rows'][$key][$key1] = $value1;
                            }
                            if (!empty($value1) && $key1 == 'city') {
                                $cityAddress = $value1 . ',';
                            }
                            if (!empty($value1) && $key1 == 'state') {
                                $stateAddress = $value1;
                            }
                            if (!empty($value1) && $key1 == 'zipcode') {
                                $zipcode = $value1;
                            }
                            if (!empty($value1) && $key1 == 'country') {
                                $country = $value1;
                            }
                        }
                        $addressFormated = $addressFormated . $cityAddress . $stateAddress . ' ' . $zipcode . "\n" . $country;
                        $addressFormated = str_replace_last(',\n', '', $addressFormated);
                        $data['rows'][$key]['address'] = str_replace_last(',', '', $address);
                        $data['rows'][$key]['addressFormat'] = $addressFormated;
                    } else if($user_type == '2'){
                        $data['total'] = $users_count;
                        $data['rows'][$key]['id'] = $value['id'];
                        $data['rows'][$key]['name'] = $value['name'];
                        $data['rows'][$key]['email'] = (isset($value['email']) && !empty($value['email'])) ? $value['email'] : 'N/A';
                        $data['rows'][$key]['property'] = (isset($value['property_name']) && !empty($value['property_name'])) ? $value['property_name'] : 'N/A';
                        $data['rows'][$key]['building'] = (isset($value['building_name']) && !empty($value['building_name'])) ? $value['building_name'] : 'N/A';
                        $data['rows'][$key]['unit'] = $value['unit_no'];
                        $data['rows'][$key]['property_id'] = $value['property_id'];
                    } else {
                        $data['total'] = $users_count;
                        $data['rows'][$key]['id'] = $value['id'];
                        $data['rows'][$key]['name'] = $value['name'];
                    }
                }
            }
           // dd($data);
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

}

$propertyDetail = new GlobalComboGrid();
?>