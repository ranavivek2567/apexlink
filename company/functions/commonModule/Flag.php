<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class Flag extends DBConnection {

    /**
     * Flag constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to create flag
     */
    public function create_flag(){
        try {
//            dd($_POST);
            $data = $_POST['form'];
            $data = postArray($data);
            $object_id = $_POST['object_id'];
            $object_type = $_POST['object_type'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
//                dd($object_type);

                $flag_id = !empty($data['id'])?$data['id']:null;
                unset($data['id']);
                $data['date'] = !empty($data['date'])?mySqlDateFormat($data['date'],null,$this->companyConnection):null;
                $data['object_type'] = $object_type;
                $data['object_id'] = $object_id;
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                if(isset($data['flag_country_code']) || !empty($data['flag_country_code'])){
                    $data['country_code'] = $data['flag_country_code'];
                    unset($data['flag_country_code']);
                }
                if(empty($flag_id)) {
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
//                    dd($data);
                    //Save Data in Company Database
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO flags (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($data);
                    $message = 'Record added successfully.';
                } else {
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $data['completed'] = ($data['completed'] == 0)?'0':'1';
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE flags SET ".$sqlData['columnsValuesPair']." WHERE id=".$flag_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($sqlData['data']);
                    $message = 'Record updated successfully.';
                }

                if($exe){
                    $updateData = [];
                    $updateData['update_at'] = date('Y-m-d H:i:s');
                    switch ($object_type) {
                        case 'property':
                            $sqlData = createSqlUpdateCase($updateData);
                            $query = "UPDATE general_property SET ".$sqlData['columnsValuesPair']." WHERE id=".$object_id;
                            $stmt = $this->companyConnection->prepare($query);
                            $stmt->execute($sqlData['data']);
                            break;
                        default:
                    }
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => $message);
                } else {
                    return array('code' => 500, 'status' => 'error', 'message' => 'Internal server error!');
                }

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * fuinction to retrive flag data
     * @return array
     */
    public function get_flags(){
        try {
           $id = $_POST['id'];
            $sql = "SELECT * FROM flags WHERE id=".$id." AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if(!empty($data) && count($data) > 0){
                if($data['object_type'] == 'property'){
                    $query = "SELECT general_property.property_name as name FROM general_property WHERE id =".$data['object_id'];
                    $data['flagged_for_name'] = $this->companyConnection->query($query)->fetch();
                } else if($data['object_type'] == 'building'){
                    $query = "SELECT building_detail.building_name as name FROM building_detail WHERE id = ".$data['object_id'];
                    $data['flagged_for_name'] = $this->companyConnection->query($query)->fetch();
                } else if($data['object_type'] == 'unit'){
                    $query = "SELECT unit_details.unit_prefix, unit_details.unit_no FROM unit_details WHERE id = ".$data['object_id'];
                    $dataQuery = $this->companyConnection->query($query)->fetch();
                    if (!empty($dataQuery['unit_prefix'])){
                        $data['flagged_for_name']['name'] = $dataQuery['unit_prefix'].'-'. $dataQuery['unit_no'];
                    } else {
                        $data['flagged_for_name']['name'] = $dataQuery['unit_no'];
                    }
                } else {
                    $query = "SELECT users.name as name FROM users WHERE id = ".$data['object_id'];
                    $data['flagged_for_name'] = $this->companyConnection->query($query)->fetch();
                }
                $data['date'] = (!empty($data['date']))?dateFormatUser($data['date'],null,$this->companyConnection):'';
                $flag_data = array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record retrieved successfully');
            } else {
                $flag_data = array('code' => 400, 'status' => 'error','message' => 'No Record Found!');
            }
            return $flag_data;
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * fuinction to soft delete flag data
     * @return array
     */
    public function deleteFlag(){
        try {
            $id = $_POST['id'];
            $data = [];
            $data['deleted_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE flags SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $exe = $stmt->execute($sqlData['data']);
            if($exe){
                $flag_data = array('code' => 200, 'status' => 'success','message' => 'Record deleted successfully.');
            } else {
                $flag_data = array('code' => 500, 'status' => 'error','message' => 'Internal Server Error!');
            }
            return $flag_data;
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * fuinction to update completed status
     * @return array
     */
    public function flagCompleted(){
        try {
            $id = $_POST['id'];
            $data = [];
            $data['completed'] = '1';
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE flags SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $exe = $stmt->execute($sqlData['data']);
            if($exe){
                $flag_data = array('code' => 200, 'status' => 'success','message' => 'Record updated successfully.');
            } else {
                $flag_data = array('code' => 500, 'status' => 'error','message' => 'Internal Server Error!');
            }
            return $flag_data;
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    /**
     * function to get the count of uncompleted records
     * @return array
     */
    public function getFlagCount(){
        try{
            $query = 'SELECT COUNT(id) FROM flags where completed = "0" AND deleted_at IS NULL';
            $data = $this->companyConnection->query($query)->fetch();
            $_SESSION[SESSION_DOMAIN]['total_flag_count'] = $data['COUNT(id)'];
            return array('code' => SUCCESS_CODE, 'status' => SUCCESS,'message' => SUCCESS_FETCHED,'count' => $data['COUNT(id)']);
        } catch (PDOException $exception){
            return array('code' => ERROR_SERVER_CODE, 'status' => ERROR,'message' => ERROR_SERVER_MSG);
        }
    }

}

$flag = new Flag();
?>