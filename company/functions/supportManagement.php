<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:30 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};

class supportManagement extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function sendMail()
    {
        try{

            $data = $_REQUEST['form'];
//            echo '<pre>'; print_r($data); die('ssss');
            $request = [];
            $body = [];
            $subject = '';
            if($data['mail_subject'] == 'support_request'){
                $logo = SUBDOMAIN_URL."/company/images/logo.png";
                $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/support_request.php');
                $body = str_replace("#logo#",$logo,$body);
                $body = str_replace("#name#",$data['name'],$body);
                $body = str_replace("#company#",$data['company_name'],$body);
                $body = str_replace("#phone_number#",$data['phone_number'],$body);
                $body = str_replace("#email#",$data['email'],$body);
                $body = str_replace("#subject#",$data['subject'],$body);
                $body = str_replace("#description#",$data['message'],$body);

                $subject = "New Support Request";
            } else if($data['mail_subject'] == 'support_suggestion'){
                $logo = SUBDOMAIN_URL."/company/images/logo.png";
                $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/support_suggestion.php');
                $body = str_replace("#logo#",$logo,$body);
                $body = str_replace("#name#",$data['name'],$body);
                $body = str_replace("#company_name#",$data['company_name'],$body);
                $body = str_replace("#phone_number#",$data['phone_number'],$body);
                $body = str_replace("#email#",$data['email'],$body);
                $body = str_replace("#subject#",$data['subject'],$body);
                $body = str_replace("#suggest_feature#",$data['message'],$body);

                $subject = "Suggest Features";
            }

            $supportTeam = $this->conn->query("SELECT email FROM `support_team` WHERE deleted_at IS NULL")->fetchAll(PDO::FETCH_COLUMN);
            if(is_array($supportTeam) && count($supportTeam) > 0) {
                $request['to']    = $supportTeam;
                $request['to']    = ['support@apexlink.com'];
            } else {
                $request['to']    =  ['support@apexlink.com'];
            }

            $request['action']  = 'SendMailPhp';
            $request['subject'] = $subject;
            $request['portal']  = '1';
            $request['message'] = $body;
//            curlRequest($request);


            $curl_response = curlRequest($request);
//            dd($curl_response->code);
            if (isset($curl_response)) {
                if (isset($curl_response->error)) {
                    $error = $curl_response->error;
                    if (isset($error->to)) {
                        return array('code' => 504, 'status' => 'error', 'data' => $data, 'message' => 'The email was not sent. Please try again later.');
                    }
                }
                if ($curl_response->code == '200') {
                    return ['code'=>200, 'status'=>'success', 'data'=>$data, 'message'=>'The email was sent successfully.'];
                } else {
                    return array('code' => 503, 'status' => 'error', 'data' => $data, 'message' => 'The email was not sent. Please try again later.');
                }
            } else {
                return array('code' => 503, 'status' => 'error', 'data' => $data, 'message' => 'The email was not sent. Please try again later.');
            }
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


}

$supportManagement = new supportManagement();
