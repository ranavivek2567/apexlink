<?php
include_once(ROOT_URL.'/config.php');
include_once (ROOT_URL.'/constants.php');
include_once(ROOT_URL."/company/helper/helper.php");

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
}

class DefaultSettingsAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        //echo $action; die;
        echo json_encode($this->$action());
    }

    /**
     *  function for listing of plans
     */
    public function viewSettings(){
        try {
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $query = $this->companyConnection->query("SELECT * FROM default_settings where  user_id='$user_id'");
            $settings = $query->fetch();
            if($settings){
                return array('status' => 'success', 'data' => $settings);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function for listing of plans
     */
    public function viewClockSettings(){
        try {
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $query = $this->companyConnection->query("SELECT * FROM default_date_clock_settings where user_id='$user_id'");
            $settings = $query->fetch();
            if($settings){
                return array('status' => 'success', 'data' => $settings);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function to create a clock settings for super admin
     */
    public function addClockSettings(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);

            $err_array = [];
//            $err_array = validation();
            //Checking server side validation
            if(!empty($err_array)){
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $user = getSingleRecord($this->companyConnection,['column'=>'user_id','value'=>$_SESSION[SESSION_DOMAIN]['cuser_id']],'default_date_clock_settings');
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];

                if($user['code'] == 200){
                    $id = $data['user_id'];
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE default_date_clock_settings SET ".$sqlData['columnsValuesPair']." where user_id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $msg = 'Date/timeSettings updated successfully';
                }else{
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO default_date_clock_settings (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $msg = 'Date/timeSettings added successfully';
                }

                //Update user date format throughout the website
                $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
                $data['formatted_date'] =$_SESSION[SESSION_DOMAIN]['formated_date'];

                $_SESSION[SESSION_DOMAIN]['datepicker_format'] = getDatePickerFormat($_SESSION[SESSION_DOMAIN]['cuser_id'],$this->companyConnection);
                $data['datepicker_format'] =$_SESSION[SESSION_DOMAIN]['datepicker_format'];

                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => $msg);
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function for listing of plans
     */
    public function fetchCountries(){
        try {
            $query = $this->companyConnection->query("SELECT * FROM default_currency");
            $settings = $query->fetchAll();
            if($settings){
                return array('status' => 'success', 'data' => $settings,'code' => 200);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found', 'code' => 400);
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update default settings
     * @return array
     */
    public function updateDefaultSettings(){
        try {
            $data = $_REQUEST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = ['zip_code'=>10,'country'=>30,'state'=>30,'city'=>30];
            //Number variable array
            $number_array = [];
            $companyData = CompanyId($this->companyConnection);
            $company_id = $companyData['data'];
            //Server side validation
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $user = getSingleRecord($this->companyConnection ,['column'=>'user_id','value'=>$company_id], 'default_settings');
                $user_id = $company_id;
                $data['user_id'] = $user_id;
                if($user['code'] == 400){
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO default_settings (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $msg = 'Record added successfully';
                } else {
                    //updating user table
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE default_settings SET " . $sqlData['columnsValuesPair'] . " where user_id='$company_id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $msg = 'Record updated successfully';
                }

                //Default setting session data
                $_SESSION[SESSION_DOMAIN]['default_notice_period'] = defaultNoticePeriod($company_id,$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_rent'] = defaultRent($company_id,$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_application_fee'] = defaultApplicationfee($company_id,$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_zipcode'] = defaultZipCode($company_id,$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_city'] = defaultCity($company_id,$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_currency'] = defaultCurrency($company_id,$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] = defaultCurrencySymbol($_SESSION[SESSION_DOMAIN]['default_currency'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['pagination'] = pagination($company_id,$this->companyConnection);
                $_SESSION[SESSION_DOMAIN]['payment_method'] = defaultPaymentMethod($company_id,$this->companyConnection);
                
                $_SESSION[SESSION_DOMAIN]['property_size'] = defaultPropertySize($company_id,$this->companyConnection);

                $defaultData['application_fees']= $data['application_fees'];
                $defaultData['rent_amount']= $data['default_rent'];
                $defaultData['zipcode']= $data['zip_code'];
                $defaultData['country']= $data['country'];
                $defaultData['state']= $data['state'];
                $defaultData['city']= $data['city'];
                $defaultData['currency']= $data['currency'];
                $defaultData['default_currency']= $data['currency'];
                $defaultData['default_payment_method']= $data['payment_method'];
                //updating company setting when updating default setting
                $this->updateCompnaySetup($defaultData);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => $msg);
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    /**
     * get default settings
     * @return array
     */
    public function getDefaultSettings(){
        try {
            $companyData = CompanyId($this->companyConnection);
            $company_id = $companyData['data'];
            $query = $this->companyConnection->query("SELECT default_currency.id AS currency_id,default_currency.symbol,default_settings.* FROM default_settings JOIN default_currency ON default_settings.currency=default_currency.id WHERE user_id=".$company_id);
            $settings = $query->fetch();
            if($settings){
                return array('status' => 'success', 'data' => $settings,'code' => 200);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found', 'code' => 400);
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }

    /**
     * add zip code master data
     * @return array|void
     */
    public function addZipCodeMaster(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = ['zip_code','city','state','county','country'];
            /* Max length variable array */
            $maxlength_array = ['zip_code'=>8,'city'=>30,'state'=>30,'county'=>30,'country'=>30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['zip_code_id']) && !empty($data['zip_code_id']))?$data['zip_code_id']:null;
                unset($data['zip_code_id']);

                //check if custom field allready exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection , 'zip_code_master', 'zip_code', $data['zip_code'],$id);
                if($duplicate['is_exists'] == 1){
                    if(empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Zip Code allready exists!');
                    }
                }
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                if(empty($id)) {
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO zip_code_master (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
                } else {
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE zip_code_master SET ".$sqlData['columnsValuesPair']." where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e));
            return;
        }
    }

    /**
     * get zip code master data
     * @return array
     */
    public function getZipCodeMaster(){
        try {
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM zip_code_master WHERE id=".$id);
            $settings = $query->fetch();
            if($settings){
                return array('status' => 'success', 'data' => $settings,'code' => 200);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found', 'code' => 400);
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());

        }
    }

    /**
     * Delete zip code master data
     * @return array
     */
    public function deleteZipCodeMaster(){
        try {
            $id = $_POST['id'];
            $sql = "DELETE FROM zip_code_master where id=".$id;
            $data = $this->companyConnection->prepare($sql);
            if($data->execute()){
                return array('code' => 200, 'status' => 'success','message' => 'Record Deleted successfully');
            } else {
                return array('code' => 400, 'status' => 'error','message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    /*Harjinder 08-05-19*/
    /**
     *
     * @return array
     */
    public function addSignature(){
        try {
            $companyData = CompanyId($this->companyConnection);
            $company_id = $companyData['data'];
            $data=[];
            $data['user_id'] = $company_id;
            $email_signature = urlencode($_POST['email_signature']);
            $data['email_signature'] = '"'.$email_signature.'"';
            $data['email_signature'] = $email_signature;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            // print_r('"'.$email_signature.'"'); exit;
            //Required variable array
            $required_array = ['email_signature'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $user = getSingleRecord($this->companyConnection ,['column'=>'user_id','value'=>$company_id], 'email_signature');
                if($user['code'] == 200) {
                    $id = $data['user_id'];
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE email_signature SET ".$sqlData['columnsValuesPair']." where user_id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $msg = 'Records Updated successfully';
                } else {
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO email_signature (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $msg = 'Signature has been added!';
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => $msg);
            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    /*Harjinder 08-05-19*/

    /**
     * add preferance settings
     * @return array
     */
    public function PreferanceCompanyAjax(){
        try {
            $companyData = CompanyId($this->companyConnection);
            $company_id = $companyData['data'];

            $data=[];
            $data['user_id'] = $_POST['id'];
            $data['notifications_to_me'] = isset($_POST['notifications_to_me'])?serialize(array($_POST['notifications_to_me'])):null;
            $data['show_last_name'] = isset($_POST['show_last_name'])?$_POST['show_last_name']:null;
            $data['birthday_notifications'] = isset($_POST['birthday_notifications'])?$_POST['birthday_notifications']:null;
            $data['inventory_alert'] = isset($_POST['inventory_alert'])?$_POST['inventory_alert']:null;
            $data['insurance_alert'] = isset($_POST['insurance_alert'])?$_POST['insurance_alert']:null;
            $data['activate_2fa'] = isset($_POST['activate_2fa'])?$_POST['activate_2fa']:'0';
            $data['in_touch'] = isset($_POST['in_touch'])?$_POST['in_touch']:null;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = $company_id;
                $data['user_id'] = $company_id;
                $activete =[];
                $activete['enabled_2fa'] = $data['activate_2fa'];
                $userData = createSqlColValPair($activete);
                $query = "UPDATE users SET ".$userData['columnsValuesPair']." where id='$id'";
                $userstmt = $this->companyConnection->prepare($query);
                $userstmt->execute();
                $user = getSingleRecord($this->companyConnection,['column'=>'user_id','value'=>$company_id],'company_preference');
                if($user['code'] == 200){
                    $id = $data['user_id'];
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE company_preference SET ".$sqlData['columnsValuesPair']." where user_id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $this->updateNameIndex();
                    //Update user name throughout the website
                    $_SESSION[SESSION_DOMAIN]['name'] = userName($_SESSION[SESSION_DOMAIN]['cuser_id'],$this->companyConnection);
                    $data['user_name'] = $_SESSION[SESSION_DOMAIN]['super_admin_name']? $_SESSION[SESSION_DOMAIN]['super_admin_name'] :  $_SESSION[SESSION_DOMAIN]['name'];
                    return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Records Updated successfully');
                }else{
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO company_preference (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $this->updateNameIndex();
                    //Update user name throughout the website
                    $_SESSION[SESSION_DOMAIN]['name'] = userName($_SESSION[SESSION_DOMAIN]['cuser_id'],$this->companyConnection);
                    $data['user_name'] = $_SESSION[SESSION_DOMAIN]['super_admin_name']? $_SESSION[SESSION_DOMAIN]['super_admin_name'] :  $_SESSION[SESSION_DOMAIN]['name'];
                    //Update user date format throughout the website
                    $_SESSION[SESSION_DOMAIN]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
                    $data['formatted_date'] =$_SESSION[SESSION_DOMAIN]['formated_date'];
                    return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Records Added successfully');
                }

            }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     *  function for updating Name of user column
     */
    public function updateNameIndex(){
        try {
            $users = $this->companyConnection->query("SELECT * FROM users WHERE id!=1")->fetchAll();
            //dd($users);
            if(!empty($users)) {
                foreach ($users as $key=>$value){
                    $id = $value['id'];
                    if($value['if_entity_name_display'] == '0') {
                        $data = [];
                        $data['name'] = userName($value['id'], $this->companyConnection, 'users');
                        // dd($data);
                        // $sqlData = createSqlUpdateCase($data);
                        $query = "UPDATE users SET name='".$data['name']."' where id=$id";
                        //dd($query);
                        $stmt = $this->companyConnection->prepare($query)->execute();

                    }
                }
            }

        } catch(PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function for listing of plans
     */
    public function viewPreferanceCompany(){
        try {
            $companyData = CompanyId($this->companyConnection);
            //echo "<pre>";print_r($companyData);echo "</pre>";
            $company_id = $companyData['data'];
            $user_id = $company_id;
            $query = $this->companyConnection->query("SELECT * FROM company_preference where user_id='$user_id'");
            $settings = $query->fetch();
            $data = array();
            $data['id'] = $settings['id'];
            $data['user_id'] = $settings['user_id'];
            $data['notifications_to_me'] = unserialize($settings['notifications_to_me']);
            $data['show_last_name'] = $settings['show_last_name'];
            $data['birthday_notifications'] = $settings['birthday_notifications'];
            $data['inventory_alert'] = $settings['inventory_alert'];
            $data['insurance_alert'] = $settings['insurance_alert'];
            $data['activate_2fa'] = $settings['activate_2fa'];
            $data['in_touch'] = $settings['in_touch'];


            if($data){
                return array('status' => 'success', 'data' => $data);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    /*Harjinder 08-05-19*/
    /**
     *  function for listing of plans
     */
    public function viewEmailSignature(){
        try {
            $companyData = CompanyId($this->companyConnection);

            $company_id = $companyData['data'];
            $user_id = $company_id;

            $query = $this->companyConnection->query("SELECT * FROM email_signature where  user_id='$user_id'");
            $settings = $query->fetch();
            $settings["email_signature"] = urldecode($settings["email_signature"]);
            $data = array();
            //$data['user_id'] =$query->user_id;
            if($settings){
                return array('status' => 'success', 'data' => $settings);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    /*Harjinder 08-05-19*/

    /**
     * function to update default setting on account setup update
     * @param $data
     * @return array|void
     */
    public function updateCompnaySetup($data){
        try{
            $companyData = CompanyId($this->companyConnection);
            $company_id = $companyData['data'];
            //updating user table
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id='$company_id'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record updated successfully');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }


}

$user = new DefaultSettingsAjax();
