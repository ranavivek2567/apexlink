<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */


include(ROOT_URL . "/config.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
class PropertyGroupsAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     *  Insert Data to Company Users
     */
    public function insert()
    {
        try {
            $data = $_POST['form'];
            $data = $this->postArray($data);

            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            //Required variable array
            $required_array = ['group_name'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $property_groups_already =  $this->checkNameAlreadyExists($data['group_name']);
                if($property_groups_already['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Group already exists');
                }
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }

                if(isset($data['property_groups_id'])){
                    unset($data['property_groups_id']);
                }
                $number_of_rows=  $this->companyConnection->query("SELECT * FROM `company_property_groups` WHERE deleted_at IS NULL")->fetchAll();
                    if(count($number_of_rows) == 0){
                        $is_default = 1;
                    }else{
                        $is_default = isset($data['is_default'])? $data['is_default'] :0;
                    }
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['group_name'] = (isset($data['group_name']) && !empty($data['group_name'])? $data['group_name'] :'');
                $data['description'] = (isset($data['description']) && !empty($data['description'])? $data['description'] :'');
                $data['is_default'] = $is_default;
                $data['status'] = 1;
                $migration = MigrationCompanySetup::createPropertyType($this->companyConnection);
                if($migration['code'] == 200)
                {
                    if(isset($data['is_default']) && $data['is_default'] ==1){
                        $upadte_data['is_default'] = 0;
                        $sqlData = $this->createSqlColValPair($upadte_data);
                        $query = "UPDATE company_property_groups SET ".$sqlData['columnsValuesPair'];
                        $stmt1 =$this->companyConnection->prepare($query);
                        $stmt1->execute();
                    }

                    //Save Data in Company Database
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO company_property_groups (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                } else {
                    return array('code' => 400, 'status' => 'failed','message' => "Something went wrong! Please contact to Administrator.");
                }

                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Records Added successfully');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  Insert Data to Update Users
     */
    public function update()
    {

        try {
            $data = $_POST['form'];
            $data = $this->postArray($data);
            $property_groups_id = $data['property_groups_id'];
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            //Required variable array
            $required_array = ['property_type'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $property_groups_already =  $this->checkNameAlreadyExists($data['group_name']);
                $property_groups_data =  $this->getGroupsData($property_groups_id);
                if(!isset($data["is_default"])) {
                    $checkDefault = $this->checkDefault($property_groups_id);
                    if( $checkDefault['data']['check_default_data']['is_default'] == 1 )  {
                        return array('code' => 503, 'status' => 'error', 'message' => 'One default value is required');
                    }
                }
                if($property_groups_data['data']['property_groups_data']['status'] == '0'  && isset($data['is_default']) ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Deactivated value cannot set as default');
                }

                if($property_groups_already['is_exists']==1 && $property_groups_already['data']['property_groups_data']['id'] != $property_groups_id  ) {
                            return array('code' => 503, 'status' => 'error', 'message' => 'Group already exists');
                }
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['property_groups_id'])){
                    unset($data['property_groups_id']);
                }




                $data1['updated_at'] = date('Y-m-d H:i:s');
                $data1['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data1['group_name'] = (isset($data['group_name']) && !empty($data['group_name'])? $data['group_name'] :'');
                $data1['description'] = (isset($data['description']) && !empty($data['description'])? $data['description'] :'');
                $data1['is_default'] = isset($data['is_default'])? 1 :0;

                if(isset($data['is_default']) && $data['is_default'] ==1){
                    $upadte_data['is_default'] = 0;
                    $sqlData = createSqlColPropertyGroup($upadte_data);
                    $query = "UPDATE company_property_groups SET ".$sqlData['columnsValuesPair'];
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                }else{
                    return array('code' => 500, 'status' => 'error', 'data' => $data1, 'message' => 'One default value is required');
                }

                $sqlData = createSqlColPropertyGroup($data1);

                $query = "UPDATE company_property_groups SET ".$sqlData['columnsValuesPair']." where id=$property_groups_id";
                $stmt =$this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'Records Updated successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    function checkNameAlreadyExists($group_name){

            $data['property_groups_data'] = $this->companyConnection->query("SELECT * FROM company_property_groups WHERE group_name ='".$group_name."'")->fetch();

            if($data['property_groups_data']){
                return ['is_exists' => 1,'data'=>$data];
            }
            else{
                return ['is_exists'=>0];
            }

    }


    /**
     *  function for fetching propertySetup type data
     */
    public function getGroupsData($property_groups_id)
    {
        try {
            $data = [];
                $data['property_groups_data'] = $this->companyConnection->query("SELECT * FROM company_property_groups WHERE id =".$property_groups_id)->fetch();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     *  function for fetching propertySetup type data
     */
    public function getPropertyGroupsData()
    {
        try {
            $data = [];
            $property_groups_id = $_REQUEST['id'];
            if(isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && !empty($_SESSION[SESSION_DOMAIN]['cuser_id']))
            {
                $data['property_groups_data'] =$this->companyConnection->query("SELECT * FROM company_property_groups WHERE id =".$property_groups_id)->fetch();
            }
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  function to Check Domain or Company Exists or Not
     */
    public function CheckDomainCompanyExist()
    {
        try{
            $company_name = $_REQUEST['company_name'];
            $type = $_REQUEST['type'];
            if($type == 'domain')
            {
                $data = $this->conn->query("SELECT * FROM users WHERE domain_name='$company_name' ")->fetch();
            } else {
                $data = $this->conn->query("SELECT * FROM users WHERE company_name='$company_name' ")->fetch();
            }
            if($data)
            {
                return ['status'=>'failed','code'=>204,'data'=>''];
            } else {
                return ['status'=>'success','code'=>200,'data'=>''];
            }
        }   catch (PDOException $e) {
            return ['status'=>'failed','code'=>503,'data'=>$e->getMessage()];
            printErrorLog($e->getMessage());
        }
    }





    /**
     *  Activate Company
     */
    public function activate(){
        try{
            $updated_at = date('Y-m-d H:i:s');
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $company_property_groups_id = $_REQUEST['id'];
            $sql = "UPDATE company_property_groups SET status=?, updated_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([1,$updated_at,$company_property_groups_id]);
            return ['status'=>'success','code'=>200,'data'=>'Company activated successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     *  Deactivate Company
     */
    public function deactivate(){
        try{
            $updated_at = date('Y-m-d H:i:s');
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $company_property_groups_id = $_REQUEST['id'];
            $property_groups_data =  $this->getGroupsData($company_property_groups_id);
            if($property_groups_data['data']['property_groups_data']['is_default'] == '1' ) {
                return array('code' => 503, 'status' => 'error', 'message' => 'A default value cannot be deactivated.');
            }
            $sql = "UPDATE company_property_groups SET status=?, updated_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([0,$updated_at,$company_property_groups_id]);
            return ['status'=>'success','code'=>200,'data'=>'Group deactivated successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     *  Delete Company
     */
    public function delete(){
        try{

            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $company_property_type = $_REQUEST['id'];
            $checkDefault = $this->checkDefault($company_property_type);
            if( $checkDefault['data']['check_default_data']['is_default'] == 1 )  {
                return array('code' => 503, 'status' => 'error', 'message' => 'Default value cannot be deleted');
            }
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE company_property_groups SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$company_property_type]);
            return ['status'=>'success','code'=>200,'message'=>'Group deleted successfully'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }


    /**
     *  Delete Company
     */
//    public function importExcel(){
//        if(isset($_FILES['file'])) {
//            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
//                $allowedExtensions = array("xls","xlsx");
//                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
//                if(in_array($ext, $allowedExtensions)) {
//                    $file_size = $_FILES['file']['size'] / 1024;
//                    if($file_size < 3000) {
//                        $file = "uploads/".$_FILES['file']['name'];
//                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
//                        if($isUploaded) {
//                            include(ROOT_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
//                            try {
//                                //Load the excel(.xls/.xlsx) file
//                                $objPHPExcel = PHPExcel_IOFactory::load($file);
//                            } catch (Exception $e) {
//                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
//                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
//                                printErrorLog($e->getMessage());
//                                die();
//                            }
//                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
//                            $sheet = $objPHPExcel->getSheet(0);
//                            //It returns the highest number of rows
//                            $total_rows = $sheet->getHighestRow();
//                            //It returns the highest number of columns
//                            $total_columns = $sheet->getHighestDataColumn();
//                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
//                            $extra_columns = ",`user_id`, `is_default`, `status`, `is_editable`, `created_at`, `updated_at`";
//                            $is_default = 0;
//                            $status = 1;
//                            $is_editable = 1 ;
//                            $extra_values = ",'" . $login_user_id . "','" . $is_default . "','" . $status . "','". $is_editable . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
//                            $query = "insert into `company_property_groups` (`group_name`, `description`$extra_columns) VALUES ";
//                            //Loop through each row of the worksheet
//                            for ($row = 2; $row <= $total_rows; $row++) {
//                                //Read a single row of data and store it as a array.
//                                //This line of code selects range of the cells like A1:D1
//                                $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
//                                //Creating a dynamic query based on the rows from the excel file
//                                $query .= "(";
//                                //Print each cell of the current row
//                                foreach ($single_row[0] as $key => $value) {
//                                    $query .= "'" . $value . "',";
//                                }
//                                $query = substr($query, 0, -1);
//                                $query .= $extra_values . "),";
//                            }
//                            $query = substr($query, 0, -1);
//                            try {
//                                $stmt = $this->companyConnection->prepare($query);
//                            } catch (Exception $e) {
//                                return ['status'=>'failed','code'=>503,'message'=>'Error in importing the data'];
//                                printErrorLog($e->getMessage());
//                                die();
//                            }
//                            $executed_query = $stmt->execute();
//                            if ($executed_query) {
//                                unlink($file);
//                                return ['status'=>'success','code'=>200,'message'=>'Data Imported Successfully'];
//                            }else{
//                                return ['status'=>'success','code'=>200,'message'=>'There is some error in your file'];
//                            }
//                        } else {
//                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
//                            //echo '<span class="msg">File not uploaded!</span>';
//                        }
//                    } else {
//                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
//                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
//                    }
//                } else {
//                    return ['status'=>'failed','code'=>503,'message'=>'* Only .xls/.xlsx files are accepted'];
//                   // echo '<span class="msg">This type of file not allowed!</span>';
//                }
//            } else {
//                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
//               // echo '<span class="msg">Select an excel file first!</span>';
//            }
//        }
//    }

    public function importExcel(){
        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if(in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if($file_size < 3000) {
                        $file = "uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx/ .csv) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                                printErrorLog($e->getMessage());
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the first element of 'A1'
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestDataColumn();
                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $extra_columns = ",`user_id`, `is_default`, `status`, `is_editable`, `created_at`, `updated_at`";
                            $is_default = 0;
                            $status = 1;
                            $is_editable = 1 ;
                            $extra_values = ",'" . $login_user_id . "','" . $is_default . "','" . $status . "','". $is_editable . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
                            $query = "insert into `company_property_groups` (`group_name`, `description`$extra_columns) VALUES ";
                            //Loop through each row of the worksheet
                            for ($row = 2; $row <= $total_rows; $row++) {
                                //Read a single row of data and store it as a array.
                                //This line of code selects range of the cells like A1:D1
                                $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
                                //Creating a dynamic query based on the rows from the excel file
                                $data =$this->companyConnection->query("SELECT group_name FROM company_property_groups")->fetchAll();

                                if ($first_row_header == 'GroupName') {
                                    $query .= "(";
                                    //Print each cell of the current row
                                    foreach ($single_row[0] as $key => $value) {
                                        $query .= "'" . $value . "',";
                                        foreach ($data as $key1 => $value1) {
                                            if (in_array($value, $value1)) {
                                                return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data'];
                                                printErrorLog($e->getMessage());
                                            }
                                        }
                                    }

                                    $query = substr($query, 0, -1);
                                    $query .= $extra_values . "),";
                                } else {
                                    return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data'];
                                    printErrorLog($e->getMessage());
                                }
                            }
                            $query = substr($query, 0, -1);
                            try {

                                $stmt = $this->companyConnection->prepare($query);

                            } catch (Exception $e) {
                                return ['status'=>'failed','code'=>503,'message'=>'Error in importing the data'];
                                printErrorLog($e->getMessage());
                            }
                            $executed_query = $stmt->execute();
                            if ($executed_query) {
                                unlink($file);
                                return ['status'=>'success','code'=>200,'message'=>'Data imported successfully.'];
                            }else{
                                return ['status'=>'success','code'=>200,'message'=>'There is some error in your file.'];
                            }
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }



    /**
     *  function to get post data array
     */
    public function postArray($post) {
        $data = [];
        foreach ($post as $key => $value) {
            if (!empty($value['value'])) {
                $dataValue = $this->test_input($value['value']);
            } else {
                $dataValue = $value['value'];
            }
            $data[$value['name']] = $dataValue;
        }
        return $data;
    }


    /**
     *  Server side validation function
     */
    public function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    /**
     *  Server side validation function
     */
    public function createSqlColVal($data) {
        $columns = '';
        $columnsValues = '';
        foreach ($data as $key => $value) {
            $columns .= $key . ',';
            $columnsValues .= ':' . "$key" . ',';
        }
        $columns = substr_replace($columns, "", -1);
        $columnsValues = substr_replace($columnsValues, "", -1);
        $sqlData = ['columns' => $columns, 'columnsValues' => $columnsValues];
        return $sqlData;
    }

    /**
     *  column value pair for update query
     */
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }




    /**
     *  Export Excel
     */
    public function exportExcel(){

        include(ROOT_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
        $objPHPExcel = new PHPExcel();
        $table = $_REQUEST['table'];
        $query1 = "SELECT * FROM $table";
        $stmt = $this->companyConnection->prepare($query1);
        $stmt->execute();
        //Set header with temp array
        $tmparray =array("Property Group Name","Description");
        //take new main array and set header array in it.
        $sheet =array($tmparray);
        while ($property_groups_data = $stmt->fetch(PDO::FETCH_ASSOC))
        {

            $tmparray =array();
            $property_groups = $property_groups_data['group_name'];
            array_push($tmparray,$property_groups);
            $description = $property_groups_data['description'];
            array_push($tmparray,$description);
            array_push($sheet,$tmparray);
        }
        $worksheet = $objPHPExcel->getActiveSheet();
        foreach($sheet as $row => $columns) {
            foreach($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
        //make first row bold
        $objPHPExcel->getActiveSheet()->getStyle("A1:B1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="PropertyGroup.xlsx"');
        $objWriter->save('php://output');
        die();
    }


    /**
     *  Export Excel
     */
    public function exportSampleExcel()
    {
        $file_url = COMPANY_SITE_URL . "/excel/PropertyGroup.xlsx";
        if (ob_get_contents()) ob_end_clean();
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
        readfile($file_url); // do the double-download-dance (dirty but worky)
        die();
    }

    /**
     *  function for fetching propertySetup type data
     */
    public function checkDefault($property_group_id)
    {
        try {
            $data = [];
            $data['check_default_data'] =$this->companyConnection->query("SELECT *  FROM company_property_groups WHERE id = '".$property_group_id."'")->fetch();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function restorepropertyType()
    {

        try {
            $data = $_POST['cuser_id'];
            //  $data = $this->postArray($data);
            //print_r($data); exit;
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];

            $data = array();
            $data['deleted_at'] = NULL ;
            //$data['id'] = $_POST['cuser_id'];

            $deleted_at['deleted_at'] = NULL ;
            $sqlData = $this->createSqlColValPair($deleted_at);

            $query = "UPDATE company_property_groups SET ".$sqlData['columnsValuesPair']." WHERE id =".$_POST['cuser_id'];

            $stmt1 =$this->companyConnection->prepare($query);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record restored successfully');


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

}

$property_groups = new PropertyGroupsAjax();

