<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/functions/FlashMessage.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class Portfolio extends DBConnection {

    /**
     * CustomFields constructor.
     */
    public function __construct() {
        parent::__construct();
        $function = FlashMessage::render();
        $this->$function();
    }

    /**
     * @return array|void
     * @throws Exception
     */
    public function add(){
        try {
            $data = $_POST['form'];
            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data);
            //Required variable array
            $required_array = ['portfolio_id','portfolio_name'];
            /* Max length variable array */
            $maxlength_array = ['portfolio_id'=>100,'portfolio_name'=>500];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id']))?$data['id']:null;
                if(!empty($id)) unset($data['id']);
                if(!empty($custom_field)){
                    foreach ($custom_field as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                $insert = [];
                $insert['portfolio_id'] = $data['portfolio_id'];
                $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $insert['portfolio_name'] = $data['portfolio_name'];
                $insert['is_default'] = isset($data['is_default'])? '1':'0';
                $insert['is_editable'] = '1';
                $insert['custom_field'] = (count($custom_field)>0) ? serialize($custom_field):null;
                $insert['status'] = '1';
                $insert['created_at'] = date('Y-m-d H:i:s');
                $insert['updated_at'] = date('Y-m-d H:i:s');

                //check if custom field allready exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection , 'company_property_portfolio', 'portfolio_name', $insert['portfolio_name'],$id);
                if($duplicate['is_exists'] == 1){
                    if(empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        echo json_encode(array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Portfolio allready exists!'));
                        return;
                    }
                }

                //Save Data in Company Database
                $sqlData = createSqlColVal($insert);
                $custom_data = ['is_deletable' => '0','is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if(count($custom_field)> 0){
                    foreach ($custom_field as $key=>$value){
                        updateCustomField($this->companyConnection ,$updateColumn,$value['id']);
                    }
                }


                if(empty($id)) {
                    if(getRecordCount($this->companyConnection,'company_property_portfolio') == 0) $insert['is_default'] = '1';
                    if($insert['is_default'] == 1){
                        $updateDafault = createSqlColValPair(['is_default' => 0]);
                        updateDefaultField($this->companyConnection ,$updateDafault, 'company_property_portfolio');
                    }
                    $query = "INSERT INTO company_property_portfolio (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($insert);
                    echo json_encode(array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully'));
                    return;
                } else {
                    $data =  getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$id], 'company_property_portfolio');
                    if($data['code'] == 400) {
                        echo json_encode(array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!'));
                        return;
                    } else {
                        if($data['data']['is_editable'] == '0'){
                            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => 'Cannot update Record!'));
                            return;
                        }
                        if($data['data']['is_default'] == '1' && $insert['is_default'] == '0'){
                            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => 'One default value is required!'));
                            return;
                        }
                    }
                    if($insert['is_default'] == 1){
                        $updateDafault = createSqlColValPair(['is_default' => 0]);
                        updateDefaultField($this->companyConnection ,$updateDafault, 'company_property_portfolio');
                    }
                    $sqlData = createSqlColValPair($insert);
                    $query = "UPDATE company_property_portfolio SET ".$sqlData['columnsValuesPair']." where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    echo json_encode(array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully'));
                    return;
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e));
            return;
        }
    }

    /**
     * function to return all the custom fields.
     */
    public function get(){
        try {
            $id = $_POST['id'];
            //check if custom field allready exists or not
            $sql = "SELECT * FROM company_property_portfolio WHERE id=".$id." AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if(!empty($data) && count($data) > 0){
                $custom_data = !empty($data['custom_field']) ? unserialize($data['custom_field']) : null;
                if(!empty($custom_data)){
                    foreach ($custom_data as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_data[$key]['default_value'] = dateFormatUser($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_data[$key]['value'] = dateFormatUser($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                echo json_encode(array('code' => 200, 'status' => 'success', 'data' => $data, 'custom_data'=>$custom_data,'message' => 'Record retrieved successfully'));
            } else {
                echo json_encode(array('code' => 400, 'status' => 'error','message' => 'No Record Found!'));
            }
            return;
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }

    /**
     * function to delete custom field.
     */
    public function delete(){
        //delete
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record =  getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$id], 'company_property_portfolio');
            if($record['code'] == 200){
                if($record['data']['is_default'] == '1') {
                    echo json_encode(['status'=>'error','code'=>500,'message'=>'A Default Set Value Cannot Be Deleted!']);
                    return;
                }
            }
            $sql = "UPDATE company_property_portfolio SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            echo json_encode(['status'=>'success','code'=>200,'message'=>'Record deleted successfully.']);
            return;
        } catch (Exception $exception) {
            echo json_encode(['status'=>'failed','code'=>503,'message'=>$exception->getMessage()]);
            return;
        }
    }

    /**
     * Update Unit Type Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];
            $updated_at = date('Y-m-d H:i:s');

            $checkSetDefault = $this->companyConnection->prepare("SELECT * FROM company_property_portfolio WHERE id=?");
            $checkSetDefault->execute([$id]);
            $checkSetDefault = $checkSetDefault->fetch();

            if(isset($checkSetDefault) || !empty($checkSetDefault)){
                $isDefault = $checkSetDefault['is_default'];

                if($isDefault == 1 && $status == 0){
                    echo json_encode(['code' => 503,'status' => 'error', 'message' => 'A default value cannot be deactivated.']);
                    return;
                }
            }
            $sql = "UPDATE company_property_portfolio SET status=?, updated_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $updated_at, $id]);

            if($status == '1'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Record activated successfully.';
            }
            if($status == '0'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Record deactivated successfully.';
            }

            if ($stmt) {
                echo json_encode(['code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $_SESSION[SESSION_DOMAIN]["status_message"]]);
                return;
            } else {
                echo json_encode(array('code' => 503,'status' => 'error', 'message' => 'No record found.'));
                return;
            }
        } catch (Exception $exception) {
            echo json_encode(['status'=>'error','code'=>503,'data'=>$exception->getMessage()]);
            return;
        }
    }

}

$user = new Portfolio();
