<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class UnitTypeAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    /**
     * Insert data to company unit type table
     * @return array
     */
    public function insert()
    {
        try {
            $data = $_POST['form'];

            $required_array = ['unit_type'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['unit_type_id'])){
                    unset($data['unit_type_id']);
                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['status'] = 1;
                $data['is_editable'] = 1;
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['unit_type'] = (isset($data['unit_type']) && !empty($data['unit_type'])? $data['unit_type'] :'');
                $data['description'] = (isset($data['description']) && !empty($data['description'])? $data['description'] :'');

                $checkExists =  checkNameAlreadyExists($this->companyConnection,'company_unit_type', 'unit_type', $data['unit_type'], '');
                if($checkExists['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Unit type already exists.');
                }
                if($data['is_default'] == 1){
                    $update_data['is_default'] = 0;
                    $sqlData = createSqlColValPair($update_data);
                    $query = "UPDATE company_unit_type SET ".$sqlData['columnsValuesPair'];
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                }

                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_unit_type (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Update data of unit type
     * @return array
     */
    public function update()
    {
        try {
            $data = $_POST['form'];

            $unit_type_id = $data['unit_type_id'];
            //Required variable array
            $required_array = ['unit_type'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($data['form_type']) && !empty($data['form_type'])){
                    unset($data['form_type']);
                }
                if(isset($data['unit_type_id']) && !empty($data['unit_type_id'])){
                    unset($data['unit_type_id']);
                }

                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['description'] = (isset($data['description']) && !empty($data['description'])? $data['description'] :'');

                $checkExists =  checkNameAlreadyExists($this->companyConnection,'company_unit_type', 'unit_type', $data['unit_type'], $unit_type_id);
                if($checkExists['is_exists']==1 ) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Unit type already exists.');
                }

                $unitTypeData = getDataById($this->companyConnection, 'company_unit_type', $unit_type_id);

                $checkStatus = $unitTypeData['data']['status'];
                if($checkStatus == 0 && $data['is_default']){
                    return array('code' => 503,'status' => 'error', 'message' => 'Deactivate value can\'t be set as default.');
                }

                $checkIsDefault = $unitTypeData['data']['is_default'];
                if($checkIsDefault == 1 && $data['is_default'] == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'One default value is required.');
                }

                if(isset($data['is_default']) && $data['is_default'] == 1){
                    $update_data['is_default'] = 0;
                    $sqlData = createSqlColValPair($update_data);
                    $query = "UPDATE company_unit_type SET ".$sqlData['columnsValuesPair'];
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                }

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE company_unit_type SET ".$sqlData['columnsValuesPair']." where id='$unit_type_id'";
                $stmt =$this->companyConnection->prepare($query);

                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record updated successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function for fetching unit type data
     * @return array
     */
    public function view(){

        try {
            $id = $_POST['id'];
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            if(isset($login_user_id) && !empty($login_user_id))
            {
                return getDataById($this->companyConnection, 'company_unit_type', $id);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }

    }

    /**
     * Update Unit Type Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];

            $checkSetDefault = $this->companyConnection->prepare("SELECT * FROM company_unit_type WHERE id=?");
            $checkSetDefault->execute([$id]);
            $checkSetDefault = $checkSetDefault->fetch();

            if(isset($checkSetDefault) || !empty($checkSetDefault)){
                $isDefault = $checkSetDefault['is_default'];

                if($isDefault == 1 && $status == 0){
                    return array('code' => 503,'status' => 'error', 'message' => 'A default value cannot be deactivated.');
                }
            }
            $sql = "UPDATE company_unit_type SET status=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$status, $id]);

            if($status == '1'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Record activated successfully.';
            }
            if($status == '0'){
                $_SESSION[SESSION_DOMAIN]["status_message"]='Record deactivated successfully.';
            }

            if($stmt){
                return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $_SESSION[SESSION_DOMAIN]["status_message"]);
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Delete Unit Type
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE company_unit_type SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Export Sample Excel For Unit Type
     */
    public function exportSampleExcel()
    {
        $file_url = COMPANY_SITE_URL . "/excel/UnitType.xlsx";
        if (ob_get_contents()) ob_end_clean();
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
        readfile($file_url); // do the double-download-dance (dirty but worky)
        die();
    }

    /**
     * Export Excel For Unit Type
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Reader_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function exportExcel(){

        include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");

        $objPHPExcel = new PHPExcel();
        $table = $_REQUEST['table'];

        $query1 = "SELECT * FROM $table";
        $stmt = $this->companyConnection->prepare($query1);
        $stmt->execute();
        //Set header with temp array
        $columnHeadingArr =array("Unit Type", "Description");
        //take new main array and set header array in it.
        $sheet =array($columnHeadingArr);
        while ($unit_type_data = $stmt->fetch(PDO::FETCH_ASSOC))
        {

            $tmpArray =array();
            $unit_type = $unit_type_data['unit_type'];
            array_push($tmpArray,$unit_type);
            $description = $unit_type_data['description'];
            array_push($tmpArray,$description);
            array_push($sheet,$tmpArray);
        }
        $worksheet = $objPHPExcel->getActiveSheet();
        foreach($sheet as $row => $columns) {
            foreach($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
        //make first row bold
        $objPHPExcel->getActiveSheet()->getStyle("A1:B1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="UnitType.xlsx"');
        $objWriter->save('php://output');
        die();
    }

    /**
     * Import Unit type Excel
     * @return array
     * @throws PHPExcel_Exception
     */
    public function importExcel(){
        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if(in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if($file_size < 3000) {
                        $file = "uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx/ .csv) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                                printErrorLog($e->getMessage());
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the first element of 'A1'
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestDataColumn();
                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $extra_columns = ",`user_id`, `is_default`, `status`, `is_editable`, `created_at`, `updated_at`";
                            $is_default = 0;
                            $status = 1;
                            $is_editable = 1 ;
                            $extra_values = ",'" . $login_user_id . "','" . $is_default . "','" . $status . "','". $is_editable . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
                            $query = "insert into `company_unit_type` (`unit_type`, `description`$extra_columns) VALUES ";
                            //Loop through each row of the worksheet
                            for ($row = 2; $row <= $total_rows; $row++) {
                                //Read a single row of data and store it as a array.
                                //This line of code selects range of the cells like A1:D1
                                $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
                                //Creating a dynamic query based on the rows from the excel file
                                $data =$this->companyConnection->query("SELECT unit_type FROM company_unit_type")->fetchAll();

                                if ($first_row_header == 'UnitType') {
                                    $query .= "(";
                                    //Print each cell of the current row
                                    foreach ($single_row[0] as $key => $value) {
                                        $query .= "'" . $value . "',";
                                        foreach ($data as $key1 => $value1) {
                                            if (in_array($value, $value1)) {
//                                                echo '<pre>'; print_r($value); die('<<<<<===$value>>>>');
                                                return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data'];
                                                printErrorLog($e->getMessage());
                                            }
                                        }
                                    }

                                    $query = substr($query, 0, -1);
                                    $query .= $extra_values . "),";
                                } else {
                                    return ['status' => 'failed', 'code' => 503, 'message' => 'Error in importing the data'];
                                    printErrorLog($e->getMessage());
                                }
                            }
                            $query = substr($query, 0, -1);
                            try {

                                $stmt = $this->companyConnection->prepare($query);

                            } catch (Exception $e) {
                                return ['status'=>'failed','code'=>503,'message'=>'Error in importing the data'];
                                printErrorLog($e->getMessage());
                            }
                            $executed_query = $stmt->execute();
                            if ($executed_query) {
                                unlink($file);
                                return ['status'=>'success','code'=>200,'message'=>'Data imported successfully.'];
                            }else{
                                return ['status'=>'success','code'=>200,'message'=>'There is some error in your file.'];
                            }
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }

    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    public function restorepropertyType()
    {

        try {
            $data = $_POST['cuser_id'];
            //  $data = $this->postArray($data);
            //print_r($data); exit;
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];

            $data = array();
            $data['deleted_at'] = NULL ;
            //$data['id'] = $_POST['cuser_id'];

            $deleted_at['deleted_at'] = NULL ;
            $sqlData = $this->createSqlColValPair($deleted_at);

            $query = "UPDATE company_unit_type SET ".$sqlData['columnsValuesPair']." WHERE id =".$_POST['cuser_id'];

            $stmt1 =$this->companyConnection->prepare($query);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record restored successfully');


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


}

$UnitTypeAjax = new UnitTypeAjax();