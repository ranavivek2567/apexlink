<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
use Elasticsearch\ClientBuilder;

require 'vendor/autoload.php';
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include( ROOT_URL .'/elastic_search.php');
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class ElasticAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    function searchingActive(){
        $domain = getDomain();
        $val=$_POST['val'];
        $availIndex=[];
        $indexes=array($domain."_property",$domain."_building",$domain."_unit",$domain."_tenant",$domain."_owner",$domain."_vendor",$domain."_contact",$domain."_waiting_list",$domain."_ticket",$domain."_employee",$domain."_purchase_order",$domain."_users");
        foreach ($indexes as $key=>$value){
            $indexName = $indexes[$key];
            $client = ClientBuilder::create()->build();
            $result= $client->indices()->exists(['index'=>$indexName]);
            if($result){
                array_push($availIndex,$indexName);
            }
        }
        $activeIndexes=(implode(',',$availIndex));
        $params = [
            "index"=> $activeIndexes,
            "from" => 0, "size" => 10000,
            'type' => 'my_type',
            'body' => [
              /*  'sort' => [
                    'name' => [
                        'order' => 'asc'
                    ]
                ],*/
                'query' => [
                    'query_string'=>[
                        "query"=> "*".$val."*",
                    ]
                ]
            ]
        ];
        $client = ClientBuilder::create()->build();
        $response = $client->search($params);
        $html='';
        $building_data=[];
        $property_data=[];
        $unit_data=[];
        $tenant_data=[];
        $owner_data=[];
        $vendor_data=[];
        $contact_data=[];
        $waitinglist_data=[];
        $tickets_data=[];
        $employee_data=[];
        $purchaseOrder_data=[];
        $users_data=[];
       // dd($response['hits']['hits']);
        for($i=0;$i<count($response['hits']['hits']);$i++){
            if($response['hits']['hits'][$i]['_index']==$domain.'_property' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($property_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_building' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($building_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_unit' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($unit_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_tenant' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($tenant_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_owner' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($owner_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_vendor' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($vendor_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_contact' && $response['hits']['hits'][$i]['_source']['status']=='1') {
                array_push($contact_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_waiting_list' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($waitinglist_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_ticket' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($tickets_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_employee' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($employee_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_purchase_order' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($purchaseOrder_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_users' && $response['hits']['hits'][$i]['_source']['status']=='1'){
                array_push($users_data,$response['hits']['hits'][$i]['_source']);
            }
        }
        if(!empty($property_data)){$html=$this->createTable($property_data,$html,'Property','property_detail');}
        if(!empty($building_data)){$html=$this->createTable($building_data,$html,'Building','building_details');}
        if(!empty($unit_data)){$html=$this->createTable($unit_data,$html,'Units','unit_detail');}
        if(!empty($tenant_data)){$html=$this->createTable($tenant_data,$html,'Tenants','users');}
        if(!empty($owner)){$html=$this->createTable($owner_data,$html,'Owners','users');}
        if(!empty($vendor_data)){$html=$this->createTable($vendor_data,$html,'Vendors','users');}
        if(!empty($contact_data)){$html=$this->createTable($contact_data,$html,'Contacts','users');}
        if(!empty($waitinglist_data)){$html=$this->createTable($waitinglist_data,$html,'Waiting List','users');}
        if(!empty($tickets_data)){$html=$this->createTable($tickets_data,$html,'Tickets','tenant_maintenance');}
        if(!empty($purchaseOrder_data)){$html=$this->createTable($purchaseOrder_data,$html,'Purchase Order','purchaseOrder');}
        if(!empty($employee_data)){$html=$this->createTable($employee_data,$html,'Employee','users');}
        if(!empty($users_data)){$html=$this->createTable($users_data,$html,'Users','users');}
        echo $html;die;
    }
    function searchingAll(){
        $domain = getDomain();
        $val=$_POST['val'];
        $availIndex=[];
        $indexes=array($domain."_property",$domain."_building",$domain."_unit",$domain."_tenant",$domain."_owner",$domain."_vendor",$domain."_contact",$domain."_waiting_list",$domain."_ticket",$domain."_employee",$domain."_purchase_order",$domain."_users");
        foreach ($indexes as $key=>$value){
            $indexName = $indexes[$key];
            $client = ClientBuilder::create()->build();
            $result= $client->indices()->exists(['index'=>$indexName]);
            if($result){
                array_push($availIndex,$indexName);
            }
        }
        $activeIndexes=(implode(',',$availIndex));
        $params = [
            "index"=> $activeIndexes,
            "from" => 0, "size" => 10000,
            'type' => 'my_type',
            'body' => [
              /*  'sort' => [
                    'name' => [
                        'order' => 'asc'
                    ]
                ],*/
                'query' => [
                    'query_string'=>[
                        "query"=> "*".$val."*",
                    ]
                ]
            ]
        ];
        $client = ClientBuilder::create()->build();
        $response = $client->search($params);
        $html='';
        $building_data=[];
        $property_data=[];
        $unit_data=[];
        $tenant_data=[];
        $owner_data=[];
        $vendor_data=[];
        $contact_data=[];
        $waitinglist_data=[];
        $tickets_data=[];
        $employee_data=[];
        $purchaseOrder_data=[];
        $users_data=[];
       // dd($response['hits']['hits']);
        for($i=0;$i<count($response['hits']['hits']);$i++){
            if($response['hits']['hits'][$i]['_index']==$domain.'_property'){
                array_push($property_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_building'){
                array_push($building_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_unit'){
                array_push($unit_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_tenant'){
                array_push($tenant_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_owner'){
                array_push($owner_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_vendor'){
                array_push($vendor_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_contact') {
                array_push($contact_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_waiting_list'){
                array_push($waitinglist_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_ticket'){
                array_push($tickets_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_employee'){
                array_push($employee_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_purchase_order'){
                array_push($purchaseOrder_data,$response['hits']['hits'][$i]['_source']);
            }
            if($response['hits']['hits'][$i]['_index']==$domain.'_users'){
                array_push($users_data,$response['hits']['hits'][$i]['_source']);
            }
        }
        if(!empty($property_data)){$html=$this->createTable($property_data,$html,'Property','property_detail');}
        if(!empty($building_data)){$html=$this->createTable($building_data,$html,'Building','building_details');}
        if(!empty($unit_data)){$html=$this->createTable($unit_data,$html,'Units','unit_detail');}
        if(!empty($tenant_data)){$html=$this->createTable($tenant_data,$html,'Tenants','users');}
        if(!empty($owner)){$html=$this->createTable($owner_data,$html,'Owners','users');}
        if(!empty($vendor_data)){$html=$this->createTable($vendor_data,$html,'Vendors','users');}
        if(!empty($contact_data)){$html=$this->createTable($contact_data,$html,'Contacts','users');}
        if(!empty($waitinglist_data)){$html=$this->createTable($waitinglist_data,$html,'Waiting List','users');}
        if(!empty($tickets_data)){$html=$this->createTable($tickets_data,$html,'Tickets','tenant_maintenance');}
        if(!empty($purchaseOrder_data)){$html=$this->createTable($purchaseOrder_data,$html,'Purchase Order','purchaseOrder');}
        if(!empty($employee_data)){$html=$this->createTable($employee_data,$html,'Employee','users');}
        if(!empty($users_data)){$html=$this->createTable($users_data,$html,'Users','users');}
        echo $html;die;
    }
    function createTable($data,$html,$heading,$tableName){
        $html.='   <div class="div-full"><h4>'.$heading.'</h4>
                    <div class="grid-outer"><div class="table-responsive"><div class="table-responsive table-popup" data-table="'.$tableName.'">
                            <table class="table1 table-hover table-dark hidefirstelastic">
                                <thead>
                                <tr>';
        $col=array_keys($data[0]);
        for ($cols = 0; $cols < count($col); $cols++) {
            $columns=str_replace('_',' ',$col[$cols]);
            if($cols==0 || $columns=='status'){
                $html .= '<th scope="col" style="display: none;">'.$columns.'</th>';
            }else{
                $html .= '<th scope="col">'.$columns.'</th>';
            }
        }
        $html .= '</tr></thead><tbody><tr>';
        for ($i = 0; $i < count($data); $i++) {
            $values = array_values($data[$i]);
            for ($val = 0; $val < count($values); $val++) {
                if($data[$i]['status']!='1'){
                    if($val==0){
                        $html .= '<td style="display: none;">' . $values[$val] . '</td>';
                    }else{
                        $html .= '<td style="color: #ff0000;">' . $values[$val] . '</td>';
                    }
                }else{
                if($val==0){
                    $html .= '<td style="display: none;">' . $values[$val] . '</td>';
                }else{
                    $html .= '<td>' . $values[$val] . '</td>';
                }
            }

            }
            $html .= '</tr>';
        }
        $html.='</tbody></table></div></div></div></div>';
        return $html;
    }
}
$elasticAjax = new ElasticAjax();