<?php
use Elasticsearch\ClientBuilder;
require 'vendor/autoload.php';
/**
 * Created by PhpStorm.
 * User: deepak
 * Date: 1/17/2019
 * Time: 3:30 PM
 */
include(ROOT_URL."/config.php");
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include( ROOT_URL .'/elastic_search.php');
//include_once ('constants.php');
//include_once (ROOT_URL.'/vendor/jqGridPHP-master/php/jqGridLoader.php');
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
};

/**
 * JqGrid Server Side Implementation
 * Class jqGrid
 */
class EasySearchGrid extends DBConnection
{
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to display jqGrid listing dynamic
     */
    public function listing_ajax()
    {
        try {
        //Required params
        $page = $_REQUEST['page']; // get the requested page
        $limit = $_REQUEST['rows']; // get how many rows we want to have into the grid
        $sidx = $_REQUEST['sidx']; // get index row - i.e. user click to sort
        $sord = $_REQUEST['sord']; // get the direction
        $tables = [''];


//        $response = [];
//        $response['page'] = $page;
//        $response['total'] = $total_pages;
//        $response['records'] = $count;
//        $response['rows'] = $json_data;
//        echo json_encode($response);
        die;
        } catch (Exception $exc){
            dd($exc);
        }
    }
}

$helper = new jqGrid();