
<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

include(ROOT_URL."/config.php");

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");

class Stripe extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];

        echo json_encode($this->$action());

    }



    public function oneTimePayment()
    {

        $papyment_data = serializeToPostArray($_REQUEST["payment_data"]);
        $company_description = $_SESSION[SESSION_DOMAIN]['company_name']." (".$_SESSION[SESSION_DOMAIN]['admindb_id'].") : Subscription prorated payment for the month of ".date("F");
        $token_id = $_REQUEST['token_id'];
        $currency = $_REQUEST['currency'];
        $amount = $_REQUEST['amount'];
        $number_of_days = $_REQUEST['days'];
        $plan_id = $_REQUEST['plan_id'];
        $days = $_REQUEST['days'];
        $user_id = $_REQUEST['user_id'];
        $user_type = $_REQUEST['user_type'];
        $term_plan = $_REQUEST['term_plan'];
        if($term_plan==4)
        {
            $plan = "MONTHLY";
        }
        else
        {
            $plan = "YEARLY";
        }



        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        try {

            $charge = \Stripe\Charge::create([
                "amount" => $amount * 100,
                "currency" => $currency,
                "source" => $token_id,
                'description' => $company_description,
            ]);


            $charge_id =$charge['id'];
            $transaction_id =$charge['balance_transaction'];
            $stripe_status  = $charge;
            $payment_response  = 'Payment has been done successfully.';
            $data['transaction_id'] = $charge_id;
            $data['charge_id'] = $transaction_id;
            $data['plan_id'] = $plan_id;
            $data['user_id'] = $user_id;
            $data['user_type'] = $user_type;
            $data['amount'] = $amount;
            $data['total_charge_amount'] = $amount;
            $data['prorated_payment_amount'] = $amount;
            $data['prorated_payment_days'] = $days;
            $data['stripe_status'] = $stripe_status;
            $data['term_plan'] = $plan;
            $data['payment_response'] = $payment_response;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);

            $qry1 = "UPDATE users SET payment_status='1',free_plan='0' where id=".$user_id;
            $stmt1 = $this->companyConnection->prepare($qry1);
            $stmt1->execute();


            $domainAll = getDomain();
            $qry2 = "UPDATE users SET payment_status='1',free_plan='0' where domain_name='".$domainAll."'";
            $stmt2 = $this->conn->prepare($qry2);
            $stmt2->execute();



            return array('code' => 200, 'status' => 'success','charge_id'=>$charge->id,'charge_data'=>$charge,'message' => "Payment has been done successfully.");

        } catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }


    }


//
//    public function tenantOneTimePayment()
//    {
//        $payment_data = serializeToPostArray($_REQUEST["payment_data"]);
//        $payment_data = postArray($payment_data);
//        //  dd($payment_data);
//      // dd($_REQUEST);
////        if($payment_data["payment_method"] == "one-time-payment"){
////            if(!empty($payment_data["payment_category"])){
////
////            }
////        }
//        $company_description = $_SESSION[SESSION_DOMAIN]['company_name']." (".$_SESSION[SESSION_DOMAIN]['admindb_id'].") : Subscription prorated payment for the month of ".date("F");
//        $token_id = $_REQUEST['token_id'];
//        $currency = $_REQUEST['currency'];
//        $amount = $_REQUEST['amount'];
//        $user_id = $_REQUEST['user_id'];
//        $user_type = $_REQUEST['user_type'];
//
//        if($payment_data['payment_method'] == "one-time-payment"){
//            if(isset($payment_data['payment_category']) && !empty($payment_data['payment_category'])){
//                foreach ($payment_data['payment_category'] as $key => $value)
//                {
//                    dd($value);
//                    if($value == 1){
//                        $this->payRent($payment_data,$_REQUEST);
//                    }
//                    else if($value == 2){
//                        $this->payUtilities($payment_data,$_REQUEST);
//                    }else if($value == 3){
//                        $this->payMisleneous($payment_data,$_REQUEST);
//                    }
//                    dd($value);
//                    die('rrr');
//                }
//
//            }else{
//                $this->deductPayment($amount,$currency,$token_id,$company_description);
//                $this->addCredit($amount);
//            }
//        }
//
//        die('fsdfs');
////        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
//        try {
////            $charge = \Stripe\Charge::create([
////                "amount" => $amount * 100,
////                "currency" => $currency,
////                "source" => $token_id,
////                'description' => $company_description,
////            ]);
//            $charge_id =$charge['id'];
//            $transaction_id =$charge['balance_transaction'];
//            $stripe_status  = $charge;
//            $payment_response  = 'Payment has been done successfully.';
//            $data['transaction_id'] = $charge_id;
//            $data['charge_id'] = $transaction_id;
//            $data['user_id'] = $user_id;
//            $data['user_type'] = $user_type;
//            $data['amount'] = $amount;
//            $data['total_charge_amount'] = $amount;
//            $data['stripe_status'] = $stripe_status;
//            $data['payment_response'] = $payment_response;
//            $data['auto_transaction_id'] = getAutoTransactionId($this->companyConnection);
//            $data['created_at'] = date('Y-m-d H:i:s');
//            $data['updated_at'] = date('Y-m-d H:i:s');
//            // dd($data);
//
//            $sqlData = createSqlColVal($data);
//            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
//
//            $stmt = $this->companyConnection->prepare($query);
//            $stmt->execute($data);
//
//            $qry1 = "UPDATE users SET payment_status='1',free_plan='0' where id=".$user_id;
//            $stmt1 = $this->companyConnection->prepare($qry1);
//            $stmt1->execute();
//
//
//            $domainAll = getDomain();
//            $qry2 = "UPDATE users SET payment_status='1',free_plan='0' where domain_name='".$domainAll."'";
//            $stmt2 = $this->conn->prepare($qry2);
//            $stmt2->execute();
//
//
//
//            return array('code' => 200, 'status' => 'success','charge_id'=>$charge->id,'charge_data'=>$charge,'message' => "Payment has been done successfully.");
//
//        } catch (Exception $e){
//            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
//        }
//
//
//    }


    public function tenantOneTimePayment()
    {
        $remaining_amount = 0;
        $payment_deduction = 0;
        $payment_data = serializeToPostArray($_REQUEST["payment_data"]);
        $payment_data = postArray($payment_data);
        $stripe_payment = 0;

        $company_description = $_SESSION[SESSION_DOMAIN]['company_name']." (".$_SESSION[SESSION_DOMAIN]['admindb_id'].") : Subscription prorated payment for the month of ".date("F");
        $token_id = $_REQUEST['token_id'];
        $currency = $_REQUEST['currency'];
        $amount = $_REQUEST['amount'];
        $total_stripe_fee_amount_to_deduct = $amount *(2.9/100);
        $total_stripe_fee_amount_to_deduct = round($total_stripe_fee_amount_to_deduct ,2)+.30;
        $total_amount_to_deduct = $amount + $total_stripe_fee_amount_to_deduct;
        $user_id = $_REQUEST['user_id'];
        $user_type = $_REQUEST['user_type'];
        $stripe_payment = $amount;
        $category_array =[];
        $getPropertyId = $this->getPropertyId($user_id);
        $getbankId = $this->getBankId($getPropertyId);

        $getConnectedAccount =  $this->getConnectedAccount();
        if(!empty($getConnectedAccount)) {

            $makeBankDefault = $this->makeBankDefault($getConnectedAccount,$getbankId);


            if ($payment_data['payment_method'] == "one-time-payment") {
                if (isset($payment_data['payment_category']) && !empty($payment_data['payment_category'])) {


                    $payment_deduction = $this->deductPayment($amount, $currency, $token_id, $company_description, $total_amount_to_deduct,$getConnectedAccount);
                    if ($payment_deduction["code"] == 200) {
                        $check_credit = $this->checkCredit($user_id);

                        //  $add_credit = $this->addCredit($amount,$total_amount_to_deduct,$user_id,$user_type,$payment_deduction['charge'],'selected');
                        foreach ($payment_data['payment_category'] as $key => $value) {
                            array_push($category_array, $value);
                            if ($value == 1) {

                                $remaining_amount = $this->payRent($payment_deduction, $user_id, $stripe_payment, $remaining_amount, $check_credit);
                            } else if ($value == 2) {

                                if (in_array(1, $category_array)) {
                                    $remaining_amount = $this->payUtilities($payment_deduction, $stripe_payment, $_REQUEST, $remaining_amount, '');
                                } else {
                                    $remaining_amount = $this->payUtilities($payment_deduction, $stripe_payment, $_REQUEST, '', $check_credit);
                                }
                            } else if ($value == 3) {
                                if (in_array(1, $category_array) || in_array(2, $category_array)) {
                                    $remaining_amount = $this->payMisleneous($payment_deduction, $stripe_payment, $_REQUEST, $remaining_amount, '');
                                } else {
                                    $remaining_amount = $this->payMisleneous($payment_deduction, $stripe_payment, $_REQUEST, '', $check_credit);
                                }
                            }
                        }
                        $this->UpdateCredit($user_id, $remaining_amount);
                        return array('code' => 200, 'status' => 'success', 'message' => 'Amount Deducted Successfully');
                    } else {
                        return array('code' => 400, 'status' => 'failed', 'message' => $payment_deduction["message"]);
                    }
                } else {
                    $payment_deduction = $this->deductPayment($amount, $currency, $token_id, $company_description, $total_amount_to_deduct,$getConnectedAccount);
                    if ($payment_deduction["code"] == 200) {
                        $payment_method = 'one_time';
                        $payment_module = 'tenant_rent';
                        $type = 'CREDIT';
                        // $add_transaction = $this->addTransaction($amount,$total_amount_to_deduct,$user_id,$user_type,$payment_deduction['charge']);
                        $add_credit = $this->addCredit($amount, $total_amount_to_deduct, $user_id, $user_type, $payment_deduction['charge'], 'not-selected');
                        $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, 0, $user_id, $payment_method, $type, $payment_module,$amount);
                        return array('code' => 200, 'status' => 'success', 'message' => 'Amount Deducted successfully');
                    } else {
                        return array('code' => 400, 'status' => 'failed', 'message' => $payment_deduction["message"]);
                    }
                }
            }
        }else{
            return array('code' => 400, 'status' => 'failed', 'message' => 'Set your connected account  details first');
        }

    }


    function deductPayment($amount,$currency,$token_id,$company_description,$total_amount_to_deduct,$getConnectedAccount){
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        try {
            $charge = \Stripe\Charge::create([
                "amount" => $total_amount_to_deduct * 100,
                "currency" => $currency,
                "source" => $token_id,
                'description' => $company_description,
                "transfer_data" => [
                    "destination" => $getConnectedAccount,
                ],

            ]);
            return array('code' => 200, 'status' => 'success','message' => 'Charge Deducted successfully' , 'charge' => $charge);
        } catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }


    function payRent($payment_deduction,$user_id,$stripe_payment,$remaining_amount,$check_credit){
        try{
            $credit =   $check_credit["credit"];
            $stripe_amount =   $check_credit["stripe_amount"];;
//          if($credit > 0){
            $totalrentpaid= $this->getTotalRentPaid($user_id);
            $gettotalrent =  $this->getNewRentAmountToBePaid($user_id);
            $totalrentpaid = (isset($totalrentpaid )&& !empty($totalrentpaid))?$totalrentpaid : '0';
            $total_rent = $gettotalrent["total_rent"];
            $rent_to_be_paid = $total_rent - $totalrentpaid;
            if ($rent_to_be_paid > 0) {
                $payment_method = 'one_time';
                $payment_module = 'tenant_rent';
                if($credit <= $rent_to_be_paid){
                    $transaction_amount = $credit;
                }else{
                    $transaction_amount = $rent_to_be_paid;
                }
                if(empty($totalrentpaid) || $totalrentpaid == 0){
                    $type = 'RENT_FIRSTTIME';
                }else{
                    $type = 'RENT';
                }
                if(($rent_to_be_paid > 0) && ($rent_to_be_paid > $stripe_payment) ){
                    if(empty($credit) || $credit ==0 ) {
                        $transaction_amount = $stripe_payment;
                        $transaction_credit = 0;
                        $stripe_payment = 0;
                        $credit_left = 0;
                    }elseif($credit > 0){
                        $transaction_amount = $stripe_payment;
                        $remaining_rent_after_deducting_stripe = $rent_to_be_paid - $stripe_payment;
                        if($remaining_rent_after_deducting_stripe > 0 ){
                            if($credit < $remaining_rent_after_deducting_stripe){
                                $transaction_credit = $credit;
                                $credit_left = 0;
                            }else{
                                $transaction_credit = $remaining_rent_after_deducting_stripe;
                                $credit_left = $credit - $remaining_rent_after_deducting_stripe;
                            }
                        }
                    }
                    $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $user_id, $payment_method, $type, $payment_module,$transaction_credit);
                    if ($addTransactionData["code"] == 200) {
                        return array('code' => 200, 'status' => 'success','transaction'=>$transaction_amount,'transaction_credit'=>$transaction_credit ,'stripe_payment'=>$stripe_payment ,'credit_left'=>$credit_left );
                    }
                }else{
                    $transaction_amount = $rent_to_be_paid;
                    $stripe_payment = $stripe_payment - $rent_to_be_paid;
                    $transaction_credit = 0;
                    $credit_left = $credit /*+$stripe_payment*/;
                    $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $user_id, $payment_method, $type, $payment_module,$transaction_credit);
                    if ($addTransactionData["code"] == 200) {
                        return array('code' => 200, 'status' => 'success','transaction'=>$transaction_amount,'transaction_credit'=>$transaction_credit ,'stripe_payment'=>$stripe_payment ,'credit_left'=>$credit_left );
                    }
                }
            }
        }catch (Exception $e){

        }
    }


    function payUtilities($payment_deduction,$stripe_payment,$data,$remaining_amount =NULL,$check_credit =NULL){
        try{
            if(isset($remaining_amount) && !empty($remaining_amount)){
                $total_amount = $remaining_amount["credit_left"] + $remaining_amount["stripe_payment"] ; /* + $data["amount"]*/;
            }else{
                $total_amount =   $check_credit["credit"]+ $stripe_payment;
            }
            $getAllUtilties = $this->getAllCharges();
            $sum_of_charges_amount = 0;
            if(!empty($getAllUtilties)){
                foreach($getAllUtilties as $key => $value){
                    if($value["amount"] <= $total_amount){
                        $sum_of_charges_amount = $sum_of_charges_amount + $value["amount"];
                        $total_amount = $total_amount - $value["amount"];
                        $change_charge_status =  $this->UpdateTenantChargeStatus($value["id"],$value["amount"]);
                    }else{
                        break;
                    }
                }
                $payment_method = 'one_time';
                $payment_module = 'tenant_charge';
                if(isset($remaining_amount) && !empty($remaining_amount)){
                    if($remaining_amount["stripe_payment"] >= $sum_of_charges_amount){
                        $transaction_amount = $sum_of_charges_amount;
                        $transaction_credit = 0;
                        $stripe_payment = $remaining_amount["stripe_payment"]-$sum_of_charges_amount;
                        $credit_left = $remaining_amount["credit_left"];
                    }else{

                        $remaining_charge_amount = $sum_of_charges_amount -$remaining_amount["stripe_payment"];

                        $transaction_amount = $remaining_amount["stripe_payment"];

                        if($remaining_charge_amount >=  $remaining_amount["credit_left"]){
                            $transaction_credit = $remaining_amount["credit_left"];
                            $stripe_payment = 0;
                            $credit_left = 0;
                        }else{
                            $transaction_credit = $remaining_charge_amount;
                            $stripe_payment = 0;
                            $credit_left = $remaining_amount["credit_left"] - $remaining_charge_amount;
                        }

                    }

                    // $transaction_credit =
                }else{
                    if($stripe_payment >= $sum_of_charges_amount){
                        $transaction_amount = $sum_of_charges_amount;
                        $stripe_payment  = $stripe_payment - $transaction_amount;
                        $transaction_credit = 0;
                        $credit_left = $check_credit["credit"];


                    }else{
                        $remaining_charge_amount = $sum_of_charges_amount - $stripe_payment;
                        $transaction_amount = $stripe_payment;
                        $stripe_payment = 0;
                        if($remaining_charge_amount >=  $check_credit["credit"] ){
                            $transaction_credit = $check_credit["credit"];
                            $credit_left = 0;
                        }else{
                            $transaction_credit = $check_credit["credit"]- $remaining_charge_amount;
                            $credit_left = $check_credit["credit"]- $remaining_charge_amount;
                        }


                    }

                    // $transaction_credit =
                }
                $type = 'CHARGES';
                $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $data["user_id"], $payment_method, $type, $payment_module,$transaction_credit);
                if ($addTransactionData["code"] == 200) {

                    return array('code' => 200, 'status' => 'success','transaction'=>$transaction_amount,'transaction_credit'=>$transaction_credit ,'stripe_payment'=>$stripe_payment ,'credit_left'=>$credit_left );
                }

                // $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $data["user_id"], $payment_method, $type, $payment_module,$new_credit);
                // $this->updateCredit($data["user_id"],$total_amount);
                //return $total_amount;
            }
        }catch (Exception $e){

        }
    }

    function  getAllCharges(){
        try{
            $getData = [];
            $user_id = $_POST['user_id'];
            $idByType = $user_id;
            $getData =  $this->companyConnection->query("SELECT tc.id,tc.created_at,tc.amount,tc.amount_due,tc.amount_paid,tc.amount_refunded,tc.start_date,tc.end_date,tc.waive_of_amount,tc.waive_of_comment,tc.status,ca.description,ca.charge_code,ca.priority FROM tenant_charges as tc inner join company_accounting_charge_code as ca on tc.charge_code = ca.id   where tc.user_id='$user_id' AND tc.status != 1 ")->fetchAll();
            if(!empty($getData)){
                return $getData;
            }else{
                return $getData;
            }
        }catch (Exception $e){

        }
    }

    function payMisleneous($payment_deduction,$stripe_payment,$data,$remaining_amount =NULL,$check_credit =NULL){
        try{

            $remaining_amount =  $this->paySecurity($payment_deduction,$stripe_payment,$data,$remaining_amount,$check_credit);

            if(!empty($remaining_amount)){
                $remaining_amount   =   $this->payCam($payment_deduction,$stripe_payment,$data,$remaining_amount,'');
            }else{
                $remaining_amount =      $this->payCam($payment_deduction,$stripe_payment,$data,'',$check_credit);
            }
            return $remaining_amount;
        }catch (Exception $e){

        }
    }



    public function paySecurity($payment_deduction,$stripe_payment,$data,$remaining_amount,$check_credit){
        try {
            $getSecurityCharges = $this->getTotalSecurityChargesToPaid($data["user_id"]);
            $security_remaining = $getSecurityCharges["security_remaining"];
            if(!empty($security_remaining)) {
                if (isset($remaining_amount) && !empty($remaining_amount)) {
                    $total_amount = $remaining_amount["credit_left"] + $remaining_amount["stripe_payment"]; /* + $data["amount"]*/;

                    if (($remaining_amount["stripe_payment"] >= $security_remaining)) {
                        $transaction_amount = $security_remaining;
                        $transaction_credit = 0 ;
                        $stripe_payment = $remaining_amount["stripe_payment"]-$security_remaining;
                        $credit_left = $remaining_amount["credit_left"];

                    } else {
                        $transaction_amount = $remaining_amount["stripe_payment"];
                        $security_remaining = $security_remaining - $remaining_amount["stripe_payment"];
                        $stripe_payment = 0;
                        if($security_remaining >  $remaining_amount["credit_left"]){
                            $transaction_credit = $remaining_amount["credit_left"];
                            $credit_left = 0;
                        }else{
                            $transaction_credit = $security_remaining;
                            $credit_left = $remaining_amount["credit_left"]-$security_remaining;
                        }
                    }
                } else {
                    $total_amount = $check_credit["credit"] + $stripe_payment;
                    if (($security_remaining >= $stripe_payment)) {
                        $transaction_amount = $stripe_payment;
                        $security_remaining  = $security_remaining- $stripe_payment;
                        $stripe_payment = 0;
                        if($security_remaining >  $check_credit["credit"]){
                            $transaction_credit = $check_credit["credit"];
                            $credit_left = 0;
                        }else{
                            $transaction_credit = $security_remaining;
                            $credit_left = $check_credit["credit"]-$security_remaining;
                        }
                    } else {
                        $transaction_amount = $security_remaining;
                        $stripe_payment = $stripe_payment - $security_remaining;
                        $transaction_credit = 0;
                        $credit_left = $check_credit["credit_left"];
                    }
                }
                $payment_method = 'one_time';
                $type = 'SECURITY';
                $payment_module = 'tenant_miscellaneous_charges';
                $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $data["user_id"], $payment_method, $type, $payment_module,$transaction_credit);
                if ($addTransactionData["code"] == 200) {
                    return array('code' => 200, 'status' => 'success','transaction'=>$transaction_amount,'transaction_credit'=>$transaction_credit ,'stripe_payment'=>$stripe_payment ,'credit_left'=>$credit_left );
                }
            }else{
                if(!empty($remaining_amount)){
                    return  $remaining_amount ;
                }else{
                    return  $check_credit;
                }
            }
        }catch (Exception $e){

        }
    }

    public function payCam($payment_deduction,$stripe_payment,$data,$remaining_amount,$check_credit){
        try {
            $getSecurityCharges = $this->getTotalSecurityChargesToPaid($data["user_id"]);
            $security_remaining = $getSecurityCharges["cam_remaining"];
            if(!empty($security_remaining)) {
                if (isset($remaining_amount) && !empty($remaining_amount)) {
                    $total_amount = $remaining_amount["credit_left"] + $remaining_amount["stripe_payment"]; /* + $data["amount"]*/;

                    if (($remaining_amount["stripe_payment"] >= $security_remaining)) {
                        $transaction_amount = $security_remaining;
                        $transaction_credit = 0 ;
                        $stripe_payment = $remaining_amount["stripe_payment"]-$security_remaining;
                        $credit_left = $remaining_amount["credit_left"];

                    } else {
                        $transaction_amount = $remaining_amount["stripe_payment"];
                        $security_remaining = $security_remaining - $remaining_amount["stripe_payment"];
                        $stripe_payment = 0;
                        if($security_remaining >  $remaining_amount["credit"]){
                            $transaction_credit = $remaining_amount["credit"];
                            $credit_left = 0;
                        }else{
                            $transaction_credit = $security_remaining;
                            $credit_left = $remaining_amount["credit_left"]-$security_remaining;
                        }

                        $stripe_payment = $stripe_payment - $security_remaining;
                        $transaction_credit = 0;
                        $credit_left = $check_credit["credit_left"];
                    }
                } else {
                    $total_amount = $check_credit["credit"] + $stripe_payment;
                    if (($security_remaining >= $stripe_payment)) {
                        $transaction_amount = $stripe_payment;
                        $security_remaining  = $security_remaining- $stripe_payment;
                        $stripe_payment = 0;
                        if($security_remaining >  $check_credit["credit"]){
                            $transaction_credit = $check_credit["credit"];
                            $credit_left = 0;
                        }else{
                            $transaction_credit = $security_remaining;
                            $credit_left = $check_credit["credit_left"]-$security_remaining;
                        }
                    } else {
                        $transaction_amount = $security_remaining;
                        $stripe_payment = $stripe_payment - $security_remaining;
                        $transaction_credit = 0;
                        $credit_left = $check_credit["credit_left"];
                    }
                }
                $payment_method = 'one_time';
                $type = 'CAM';
                $payment_module = 'tenant_miscellaneous_charges';
                $addTransactionData = $this->addTransactionsData($payment_deduction["charge"]["id"], $payment_deduction, $transaction_amount, $data["user_id"], $payment_method, $type, $payment_module,$transaction_credit);
                if ($addTransactionData["code"] == 200) {

                    return array('code' => 200, 'status' => 'success','transaction'=>$transaction_amount,'transaction_credit'=>$transaction_credit ,'stripe_payment'=>$stripe_payment ,'credit_left'=>$credit_left );
                }
            }else{
                if(!empty($remaining_amount)){
                    return  $remaining_amount  ;
                }else{
                    return  $check_credit;
                }
            }
        }catch (Exception $e){

        }
    }


    public function checkCredit($user_id){
        $getpreviouscredit = $this->companyConnection->query("select overpay_underpay as credit , stripe_payment   from accounting_manage_charges  where user_id ='".$user_id."'")->fetch();
        $previouscredit = (isset($getpreviouscredit['credit']) && !empty($getpreviouscredit['credit']) ) ? $getpreviouscredit['credit'] : 0;
        $stripe_amount = (isset($getpreviouscredit['stripe_payment']) && !empty($getpreviouscredit['stripe_payment']) ) ? $getpreviouscredit['stripe_payment'] : 0;
        return array('code' => 200, 'status' => 'success','credit'=> $previouscredit , 'stripe_amount'=>$stripe_amount);

    }

    public function addTransaction($amount,$total_amount_to_deduct,$user_id,$user_type,$charge){
        try {
            $charge_id = $charge['id'];
            $transaction_id = $charge['balance_transaction'];
            $stripe_status = $charge;
            $payment_response = 'Payment has been done successfully.';
            $data['transaction_id'] = $charge_id;
            $data['charge_id'] = $transaction_id;
            $data['user_id'] = $user_id;
            $data['user_type'] = $user_type;
            $data['amount'] = $total_amount_to_deduct; /* total amount to be deducted with stripe fees */
            $data['total_charge_amount'] = $amount;
            $data['stripe_status'] = $stripe_status;
            $data['payment_response'] = $payment_response;
            $data['auto_transaction_id'] = getAutoTransactionId($this->companyConnection);
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('code' => 200, 'status' => 'success', 'message' => "Record added successfully", 'table'=>'trasactions');

        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }

    }

    public function addCredit($amount,$total_amount_to_deduct,$user_id,$user_type,$charge,$category){
        try {
            $getpreviouscredit = $this->companyConnection->query("select overpay_underpay as credit from accounting_manage_charges  where user_id ='".$user_id."'")->fetch();
            $previouscredit = (isset($getpreviouscredit['credit']) && !empty($getpreviouscredit['credit']) ) ? $getpreviouscredit['credit'] : 0;
            $total_credit = $amount + $previouscredit;
            if($category == 'selected') {
                $query = "update accounting_manage_charges set overpay_underpay = $total_credit , stripe_payment = $amount  where user_id ='" . $user_id . "'";
            }else{
                $query = "update accounting_manage_charges set overpay_underpay = $total_credit where user_id ='" . $user_id . "'";
            }

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'message' => "Record added successfully", 'table'=>'accounting_manage_charges');
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }

    }

    public function addStripe($amount,$user_id){
        try {
            $query = "update accounting_manage_charges set stripe_payment = $amount where user_id ='".$user_id."'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'message' => "Record added successfully", 'table'=>'accounting_manage_charges');
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }

    }

//    public function updateSecurity($user_id){
//        try {
//            $getpreviouscredit = $this->companyConnection->query("select overpay_underpay as credit from accounting_manage_charges  where user_id ='".$user_id."'")->fetch();
//            $previouscredit = (isset($getpreviouscredit['credit']) && !empty($getpreviouscredit['credit']) ) ? $getpreviouscredit['credit'] : 0;
//            $total_credit = $amount + $previouscredit;
//            $query = "update accounting_manage_charges set overpay_underpay = $total_credit where user_id ='".$user_id."'";
//            $stmt = $this->companyConnection->prepare($query);
//            $stmt->execute();
//            return array('code' => 200, 'status' => 'success', 'message' => "Record added successfully", 'table'=>'accounting_manage_charges');
//        }catch(\Stripe\Error\RateLimit $e){
//            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
//        }
//
//    }





    public function addCardDetails()
    {
        $cardNumber = $_POST['card_number'];
        $exp_date = $_POST['exp_date'];
        if(isset($_POST['auto_pay']))
        {
            $autopay = 'ON';
        }
        else
        {
            $autopay = 'OFF';
        }




        $getDateFormat = explode('/',$exp_date);
        $expMonth = $getDateFormat[0];
        $expYear = $getDateFormat[1];
        $cvc = $_POST['cvc'];
        $holder_name = $_POST['holder_name'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $upateData =  "UPDATE users SET auto_pay='$autopay' where id='$user_id'";
        $stmt = $this->companyConnection->prepare($upateData);
        $stmt->execute();
        $customer_id = $this->createCustomer($user_id);
        try{
            $token = \Stripe\Token::create([
                'card' => [
                    'number'    => $cardNumber,
                    'exp_month' => $expMonth,
                    'exp_year'  => $expYear,
                    'cvc'       => $cvc,
                ],
            ]);
            $token_id = $token->id;
            $customer = \Stripe\Customer::retrieve($customer_id);
            $card = $customer->sources->create(array("source" => $token_id));
            $result['message']    = "Card Added successfully";
            $result['status']   = 'true';
            return  $result;
        }catch(\Stripe\Error\RateLimit $e){
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];
            echo  json_encode(array('status' => 'false', 'message' => $results['message'] ));
            exit;
        }
        catch(\Stripe\Error\Card $e){
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];
            echo  json_encode(array('status' => 409, 'message' => $results['message'] ));
            exit;

        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['message'] = 'Too many requests made to the API too quickly';
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;
        } catch (\Stripe\Error\InvalidRequest $e) {

            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Network communication with Stripe failed";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\Base $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Error";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        }

    }

    public function getAllCards()
    {
        $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Card Number</th>
            <th>Expiry Month</th>
            <th>Expiry Year</th>
            <th>Brand</th>
            <th>Default</th>
            <th style='width: 15%;'>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 10,
                    'object' => 'card',
                ]
            );

            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['last4']."</td>
            <td>".$card['exp_month']."</td>
            <td>".$card['exp_year']."</td>
            <td>".$card['brand']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }

                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "<option value='default'>Default</option></select></td>";
                }else{
                    $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='default'>Default</option></select></td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;



    }



    public function getAllBanks()
    {

        $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Bank Name</th>
            <th>Country</th>
            <th>Currency</th>
            <th>Last4</th>
            <th>Default</th>
            <th>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 1,
                    'object' => 'bank_account',
                ]
            );

            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['bank_name']."</td>
            <td>".$card['country']."</td>
            <td>".$card['currency']."</td>
            <td>************".$card['last4']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }

//            $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='delete'>Delete</option><option value='default'>Default</option></select></td>
//            </tr>";
                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "<option value='default'>Default</option></select></td>";
                }else{
                    $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='default'>Default</option></select></td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;


    }



    public function deleteCard()
    {
        $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        \Stripe\Customer::deleteSource(
            "$customer_id",
            "$card_id"
        );

        echo  json_encode(array('status' => 'true', 'message' => "card deleted Successfully" ));
        exit;


    }


    public function defaultCard()
    {

        $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        $customer = \Stripe\Customer::update("$customer_id", [
            'default_source' => "$card_id"
        ]);
        echo  json_encode(array('status' => 'true', 'message' => "Card has been set to default" ));
        exit;

    }


    public function addAccountDetails()
    {
        $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $routingNumber = $_POST['routing_number'];
        $accountNumber = $_POST['account_number'];
        $account_type = $_POST['account_type'];
        $holder_name = $_POST['holder_name'];
        $type = $_POST['account_type'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $getData =  $this->companyConnection->query("SELECT email,stripe_account_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $email = $getData['email'];

        if($getData['stripe_account_id']=='')
        {
            $account_id = $this->getAccount($email);
        }
        else
        {
            $account_id = $getData['stripe_account_id'];
        }

        $account = \Stripe\Account::retrieve($account_id);

        $token = \Stripe\Token::create(array(
            "bank_account" => array(
                "country" => "US",
                "currency" => "USD",
                "account_holder_name" => 'test',
                "account_holder_type" => "individual",
                "routing_number" => $routingNumber,
                "account_number" => $accountNumber
            )
        ));


        $ext_account =  $account->external_accounts->create(
            array(
                "external_account" => $token->id,
                "default_for_currency" => true
            ));


    }


    public function getAccount($email)
    {
        $account =   \Stripe\Account::create(array(
            "type" => "custom",
            "country" => "US",
            "email" => $email,
            "requested_capabilities" => ["card_payments", "transfers"]
        ));


        return   $account_id = $account->id;

    }


    public function getTenantZipCode()
    {

        $tenant_id = $_POST['id'];
        $getProperty = $this->companyConnection->query("SELECT property_id FROM tenant_property WHERE user_id ='" . $tenant_id . "'")->fetch();

        $property_id = $getProperty['property_id'];







        $getZipCode =  $this->companyConnection->query("SELECT zipcode FROM general_property WHERE id ='" . $property_id . "'")->fetch();
         $getTenantName =  $this->companyConnection->query("SELECT name FROM users WHERE id ='" . $tenant_id . "'")->fetch();

            return  array('zipcode'=>$getZipCode['zipcode'],'name'=>$getTenantName['name']);
        




    }



    public function addTenantCardPayment()
    {

        $user_id = $_POST['company_user_id'];

        $holderFirstName = $_POST['cfirst_name'];
        $holderLastName = $_POST['clast_name'];
        $card_number = $_POST['ccard_number'];
        $expiry_year = $_POST['cexpiry_year'];
        $expiry_month = $_POST['cexpiry_month'];
        $paid_amount = $_POST['paid_amount'];



        $cvv = $_POST['ccvv'];

        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

        $customer_id = $this->createCustomer($user_id);


        try{
            $token = \Stripe\Token::create([
                'card' => [
                    'number'    => $card_number,
                    'exp_month' => $expiry_month,
                    'exp_year'  => $expiry_year,
                    'cvc'       => $cvv,
                ],
            ]);



            $token_id = $token->id;
            $customer = \Stripe\Customer::retrieve($customer_id);
            $card = $customer->sources->create(array("source" => $token_id));
            $card_id = $card['id'];

            $result['message']    = "Card Added successfully";
            $result['status']   = 'true';

            $query = "UPDATE users SET updated_at='".date('Y-m-d H:i:s')."' where id=$user_id";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            //return  $result;

        }catch(\Stripe\Error\RateLimit $e){

            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 'false', 'message' => $results['message'] ));
            exit;
        }
        catch(\Stripe\Error\Card $e){

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 409, 'message' => $results['message'] ));
            exit;

        } catch (\Stripe\Error\RateLimit $e) {

            // Too many requests made to the API too quickly

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message'] = 'Too many requests made to the API too quickly';
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\InvalidRequest $e) {

            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Network communication with Stripe failed";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\Base $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Error";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        }

        return  $this->paymentProcess($customer_id,$user_id,$paid_amount,$card_id);


    }




    public function paymentProcess($customer_id,$user_id,$paid_amount,$card_id)
    {

        $paid_amount = round($paid_amount);


        $getProperty = $this->companyConnection->query("SELECT property_id FROM tenant_property WHERE user_id ='" . $user_id . "'")->fetch();

        $property_id = $getProperty['property_id'];


        /*        $getOwners = $this->companyConnection->query("SELECT * FROM owner_property_relation WHERE property_id ='" . $property_id . "'")->fetchAll();
                if(!empty($getOwners))
                {
                    foreach($getOwners as $owner)
                    {
                        $ownerId = $owner['owner_id'];
                        $percantages = $owner['percantage'];

                        $checkVerifications = $this->checkVerifications($ownerId);
                        if($checkVerifications=='false')
                        {
                            return array('status'=>'false','message'=>'Account Needs to be verified');
                        }

                    }


                    foreach($getOwners as $owner)
                    {
                        $ownerId = $owner['owner_id'];
                        $percantages = $owner['percantage'];

                        $chargeDeduction = $this->chargeDeduction($customer_id,$ownerId,$paid_amount,$percantages,$card_id,$user_id);
                        if($chargeDeduction['status']=='false')
                        {
                            return array('status'=>'false','message'=>$chargeDeduction['message']);
                        }

                    }

                    return array('status'=>'success','message'=>'Payment has been done successfully');
                }*/
        // else  /*payment given to manager */
        // {
        $checkVerifications = $this->checkVerifications(1);
        if($checkVerifications=='false')
        {
            return array('status'=>'false','message'=>'Account Needs to be verified');
        }
        $chargeDeduction = $this->chargeDeduction($customer_id,1,$paid_amount,100,$card_id,$user_id);
        if($chargeDeduction['status']=='false')
        {
            return array('status'=>'false','message'=>$chargeDeduction['message']);
        }






        return array('status'=>'success','message'=>'Payment has been done to manager');
        /*manager id is 1*/
        // }


    }



    public function checkVerifications($user_id)
    {
        $getAccountId = $this->companyConnection->query("SELECT stripe_account_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $account_id = $getAccountId['stripe_account_id'];
        if(!empty($account_id)) {
            $connected_account =\Stripe\Account::retrieve($account_id);
            if ($connected_account["business_type"] == "company") {
                $request["account_id"] =$account_id;
                $get_person_detail = getPerson($request);
                $status = (isset($get_person_detail["person_detail"]["data"][0]["verification"]["status"]) && $get_person_detail["person_detail"]["data"][0]["verification"]["status"] == "verified" && empty($connected_account["company"]["requirements"]["current_deadline"]) && empty($connected_account["company"]["requirements"]["currently_due"]) && empty($connected_account["company"]["requirements"]["disabled_reason"]) && empty($connected_account["company"]["requirements"]["eventually_due"]) && empty($connected_account["company"]["requirements"]["past_due"]) && empty($connected_account["company"]["requirements"]["pending_verification"])) ? 'true' : "false";
                return $status;
            } else {
                $status = (isset($connected_account["individual"]["verification"]["status"]) && $connected_account["individual"]["verification"]["status"] == "verified" && empty($connected_account["individual"]["requirements"]["currently_due"]) && empty($connected_account["individual"]["requirements"]["eventually_due"]) && empty($connected_account["individual"]["requirements"]["past_due"]) && empty($connected_account["individual"]["requirements"]["pending_verification"])) ? 'true' : "false";
                return $status;
            }
        }else{
            return 'false';
        }
    }




    public function chargeDeduction($customer_id,$user_id,$paid_amount,$percantages,$card_id,$sender_id)
    {

        $amount = $paid_amount*$percantages/100;
        $amount = round($amount, 2);
        $getPropertyId = $this->getPropertyId($sender_id);
        $getbankId = $this->getBankId($getPropertyId);
        $getAccountId = $this->companyConnection->query("SELECT stripe_account_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $account_id = $getAccountId['stripe_account_id'];

        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        try {

            $charge = \Stripe\Charge::create([
                "amount" => $paid_amount * 100,
                "currency" => 'usd',
                "customer"=> $customer_id,
                "source" => "$card_id",
                "destination" => array("account"  => $account_id)
            ]);
            $charge_id = $charge['id'];
            $transaction_id = $charge['balance_transaction'];
            $stripe_status  = $charge;
            $payment_response  = 'Payment has been done successfully.';
            $data1['transaction_id'] = $charge_id;
            $data1['charge_id'] = $transaction_id;
            $data1['user_id'] = $sender_id;
            $data1['user_type'] = 'TENANT';
            $data1['amount'] = $amount;
            $data1['total_charge_amount'] = $amount;
            $data1['prorated_payment_amount'] = $amount;
            $data1['payment_mode'] = "reccuring";
            $data1['type'] = "RENT_FIRSTTIME";
            $data1['property_id'] =$getPropertyId;
            $data1['bank_mode'] =$getbankId["bank_mode"];
            $data1['bank_id'] =$getbankId["bank_id"];
            $data1['payment_module'] = 'tenant_firsttime';
            $data1['stripe_status'] = $stripe_status;
            $data1['payment_response'] = $payment_response;
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');


            $qry1 = "UPDATE users SET payment_status='1' where id=".$sender_id;
            $stmt1 = $this->companyConnection->prepare($qry1);
            $stmt1->execute();
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            return array('code' => 200, 'status' => 'success','charge_id'=>$charge->id,'charge_data'=>$charge,'message' => "Payment has been done successfully.");

        } catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }


    }



    public function getBankId($property_id)
    {
        $getData =  $this->companyConnection->query("SELECT * FROM property_bank_details WHERE property_id ='" . $property_id . "' and is_default='1'")->fetch();
        if(!empty($getData )) {
            $bank_id["bank_id"] = $getData['bank_id'];
            $bank_id["bank_mode"] = 'PROPERTY';
            return $bank_id;
        }else{
            $getCompanyBankAccountData =  $this->companyConnection->query("SELECT * FROM company_accounting_bank_account WHERE user_id ='" . 1 . "' and is_default='1'")->fetch();
            $bank_id["bank_id"] = $getCompanyBankAccountData['id'];
            $bank_id["bank_mode"] = 'PM';
            return $bank_id;
        }
    }

    public function getConnectedAccount()
    {
        $getData =  $this->companyConnection->query("SELECT stripe_account_id FROM users WHERE id ='" . 1 . "' and status='1'")->fetch();
        return $stripe_connnected_account_id = $getData['stripe_account_id'];
    }


    public function getPropertyId($user_id)
    {
        $getType = $this->companyConnection->query("SELECT user_type FROM users where id='$user_id'")->fetch();
        if($getType["user_type"]==2)
        {
            $getData =  $this->companyConnection->query("SELECT property_id FROM tenant_property WHERE user_id ='" . $user_id . "'")->fetch();
        }
        else if($getType["user_type"]==4)
        {
            $getData =  $this->companyConnection->query("SELECT property_id FROM owner_property_owned WHERE user_id ='" . $user_id . "'")->fetch();
        }

        return $property_id = $getData['property_id'];
    }





    public function createCustomer($user_id)
    {

        $getData =  $this->companyConnection->query("SELECT stripe_customer_id,email FROM users WHERE id ='" . $user_id . "'")->fetch();
        $email = $getData['email'];
        if($getData['stripe_customer_id']=='')
        {
            $customer    =     \Stripe\Customer::create(array(
                'email' => $email
            ));
            $customer_id =        $customer['id'];

            $upateData =  "UPDATE users SET stripe_customer_id='$customer_id' where id='$user_id'";
            $stmt = $this->companyConnection->prepare($upateData);
            $stmt->execute();


            return $customer_id;
        }
        else
        {

            return $getData['stripe_customer_id'];
        }


    }
    public function addVendorCardDetails()
    {
        $cardNumber = $_POST['ccard_number'];
        $expMonth =  $_POST['cexpiry_month'];
        $expYear =  $_POST['cexpiry_year'];
        $cvc = $_POST['ccvv'];


        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $user_id = $_POST['vendor_id'];
        $customer_id = $this->createCustomer($user_id);
        try{
            $token = \Stripe\Token::create([
                'card' => [
                    'number'    => $cardNumber,
                    'exp_month' => $expMonth,
                    'exp_year'  => $expYear,
                    'cvc'       => $cvc,
                ],
            ]);
            $token_id = $token->id;
            $customer = \Stripe\Customer::retrieve($customer_id);
            $card = $customer->sources->create(array("source" => $token_id));
            $result['message']    = "Card Added successfully";
            $result['status']   = 'true';

            $query = "UPDATE users SET updated_at='".date('Y-m-d H:i:s')."' where id=$user_id";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();


            return (array('status' => 200, 'result' => $result['message']));

        }catch(\Stripe\Error\RateLimit $e){

            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            return (array('status' => 'false', 'message' => $results['message'] ));
        }
        catch(\Stripe\Error\Card $e){

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            return  (array('status' => 409, 'message' => $results['message'] ));

        } catch (\Stripe\Error\RateLimit $e) {

            // Too many requests made to the API too quickly

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message'] = 'Too many requests made to the API too quickly';
            return  (array('status' => 409, 'message' => $err['message']));

        } catch (\Stripe\Error\InvalidRequest $e) {

            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            return  (array('status' => 409, 'message' => $err['message']));

        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            return  (array('status' => 409, 'message' => $err['message'] ));

        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Network communication with Stripe failed";
            return  (array('status' => 409, 'message' => $err['message'] ));

        } catch (\Stripe\Error\Base $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Error";
            return  (array('status' => 409, 'message' => $err['message'] ));

        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";

            $body = $e->getJsonBody();
            $err  = $body['error'];

            return  (array('status' => 409, 'message' => $err['message'] ));
        }
    }
    public function addOwnerCardDetails()
    {
        $cardNumber = $_POST['ccard_number'];
        $expMonth =  $_POST['cexpiry_month'];
        $expYear =  $_POST['cexpiry_year'];

        $cvc = $_POST['ccvv'];
//        $holder_name = $_POST['holder_name'];

        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

        $user_id = $_POST['owner_id'];


        $customer_id = $this->createCustomer($user_id);

        try{
            $token = \Stripe\Token::create([
                'card' => [
                    'number'    => $cardNumber,
                    'exp_month' => $expMonth,
                    'exp_year'  => $expYear,
                    'cvc'       => $cvc,
                ],
            ]);

            $token_id = $token->id;
            $customer = \Stripe\Customer::retrieve($customer_id);
            $card = $customer->sources->create(array("source" => $token_id));
            $result['message']    = "Card Added successfully";
            $result['status']   = 'true';


            $query = "UPDATE users SET updated_at='".date('Y-m-d H:i:s')."' where id=$user_id";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            return  $result;

        }catch(\Stripe\Error\RateLimit $e){

            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 'false', 'message' => $results['message'] ));
            exit;
        }
        catch(\Stripe\Error\Card $e){

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 409, 'message' => $results['message'] ));
            exit;

        } catch (\Stripe\Error\RateLimit $e) {

            // Too many requests made to the API too quickly

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message'] = 'Too many requests made to the API too quickly';
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\InvalidRequest $e) {

            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Network communication with Stripe failed";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\Base $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Error";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        }
    }
    public function addVendorAccountDetails(){
        try {
            $data=$_POST;
            unset($data['action']);
            unset($data['class']);
            $request['business_type']='individual';
            $request['email']=$data['email'];
            $data['line1'] = "address_full_match"; /*$fdata['faddress1'];*/
            $accountId=createAccount($request);
            if ($accountId["code"] == 200 && $accountId["status"] == "success") {
//                if (isset($_POST['dob']) && $_POST['dob'] != "") {
//                    $data['dob'] = mySqlDateFormat($_POST['dob'],null,$this->companyConnection);
//                    $dateSplit=explode('-',$data['dob']);
//                    $data['year']=$dateSplit[0];
//                    $data['month']=$dateSplit[1];
//                    $data['day']=$dateSplit[2];
//
//                }else{
//                    $data['dob'] ='';
//                    $data['year']='';
//                    $data['month']='';
//                    $data['day']='';
//                }
                $stripe_account_array = ["email"=>$data['email'],"first_name" => $data['first_name'], "last_name" => $data['last_name'],"city" => $data['city'],
                    "state" => $data['state'], "line1" => $data['line1'], "line2" => $data['address_line2'],
                    'postal_code' => $data['postal_code'],'day' => $data['day'], 'month' => $data['month'], 'year' => $data['year'],
                    'ssn_last_4' => $data['ssn_last'],'phone' => $data['phone'],'country' => 'US', 'currency' => 'USD', 'routing_number' => $data["routing_number"],
                    'account_number' => $data["account_number"],'stripe_account_id'=> $accountId['account_id'],
                    'document_front'=> '','document_back'=> '',
                    'website_url'=> SUBDOMAIN_URL.'/','product_description'=> '','mcc'=>'7623','support_email'=>$data['email']];

                $data['user_id'] = $_POST['user_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $getUserAccountData =  $this->companyConnection->query("SELECT * FROM user_account_detail WHERE user_id ='" . $data['user_id'] . "'")->fetch();
                if(empty($getUserAccountData)){
                    $enableAccount = enableAccount($stripe_account_array);
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO user_account_detail(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    if (!empty($accountId['account_id'])) {
                        $upateData = "UPDATE users SET stripe_account_id= '" . $accountId['account_id'] . "' where id= " . $data['user_id'];
                        $stmt = $this->companyConnection->prepare($upateData);
                        $stmt->execute();

                    }
                    if($enableAccount["code"] == 400){
                        $account = \Stripe\Account::retrieve($accountId['account_id']);
                        $account->delete();
                    }
                    return $enableAccount;
                }else{

                    $enableAccount = enableAccount($stripe_account_array);
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE user_account_detail SET " . $sqlData['columnsValuesPair'] . " WHERE user_id=" . $data['user_id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    return array('status' => 'AccountDetailupdated', 'enableAccount' => $enableAccount);
                }


            }else{
                return $accountId;
            }



        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }
    public function getAccountDetail(){
        $user_id=$_POST['user_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_account_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        if(!empty($getData)){
            $request["account_id"]=$getData['stripe_account_id'];
            $getConnectedAccount=getConnectedAccount($request);
            if(isset($getConnectedAccount['account_data']['individual']['verification']) && $getConnectedAccount['account_data']['individual']['verification']['status']=='unverified'){
                $getUserAccountDetai =  $this->companyConnection->query("SELECT * FROM user_account_detail WHERE user_id ='" . $user_id . "'")->fetch();
                $dob=(!empty($getUserAccountDetai['dob'])) ? dateFormatUser($getUserAccountDetai['dob'], null, $this->companyConnection) : null;

                return array('status' => 'AccountDetail', 'data' => $getUserAccountDetai,'dob'=>$dob);
            }else if(isset($getConnectedAccount['account_data']['individual']['verification']) && $getConnectedAccount['account_data']['individual']['verification']['status']=='pending'){
                return array('code' => 400, 'status' => 'failed', 'message' => 'pending');
            }
            else if(isset($getConnectedAccount['account_data']['individual']['verification']) && $getConnectedAccount['account_data']['individual']['verification']['status']=='verified'){
                return array('code' => 400, 'status' => 'failed', 'message' => 'verified');
            }
        }


    }
//    vendor card functions
    public function addVendorsCardDetails()
    {
        $cardNumber = $_POST['card_number'];
        $exp_date = $_POST['exp_date'];


        $getDateFormat = explode('/',$exp_date);
        $expMonth = $getDateFormat[0];
        $expYear = $getDateFormat[1];

        $cvc = $_POST['cvc'];
        $holder_name = $_POST['holder_name'];
        if(isset($_POST['auto_pay']))
        {
            $autopay = 'ON';
        }
        else
        {
            $autopay = 'OFF';
        }
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

        $user_id =$_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'];
        $upateData =  "UPDATE users SET auto_pay='$autopay' where id='$user_id'";
        $stmt = $this->companyConnection->prepare($upateData);
        $stmt->execute();

        $customer_id = $this->createCustomer($user_id);

        try{
            $token = \Stripe\Token::create([
                'card' => [
                    'number'    => $cardNumber,
                    'exp_month' => $expMonth,
                    'exp_year'  => $expYear,
                    'cvc'       => $cvc,
                ],
            ]);

            $token_id = $token->id;
            $customer = \Stripe\Customer::retrieve($customer_id);
            $card = $customer->sources->create(array("source" => $token_id));
            $result['message']    = "Card Added successfully";
            $result['status']   = 'true';
            return  $result;

        }catch(\Stripe\Error\RateLimit $e){

            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 'false', 'message' => $results['message'] ));
            exit;
        }
        catch(\Stripe\Error\Card $e){

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 409, 'message' => $results['message'] ));
            exit;

        } catch (\Stripe\Error\RateLimit $e) {

            // Too many requests made to the API too quickly

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message'] = 'Too many requests made to the API too quickly';
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\InvalidRequest $e) {

            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Network communication with Stripe failed";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\Base $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Error";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        }







    }
    public function getAllVendorCards()
    {
        $user_id = $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Card Number</th>
            <th>Expiry Month</th>
            <th>Expiry Year</th>
            <th>Brand</th>
            <th>Default</th>
            <th>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 10,
                    'object' => 'card',
                ]
            );

            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['last4']."</td>
            <td>".$card['exp_month']."</td>
            <td>".$card['exp_year']."</td>
            <td>".$card['brand']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }
//
//                $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='delete'>Delete</option><option value='default'>Default</option></select></td>
//            </tr>";

                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "<option value='default'>Default</option></select></td>";
                }else{
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option><option value='default'>Default</option></select></td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;



    }

    public function getVendorBanks()
    {
        $user_id = $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Bank Name</th>
            <th>Country</th>
            <th>Currency</th>
            <th>Last4</th>
            <th>Default</th>
            <th>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 10,
                    'object' => 'bank_account',
                ]
            );
            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['bank_name']."</td>
            <td>".$card['country']."</td>
            <td>".$card['currency']."</td>
            <td>************".$card['last4']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }

//                $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='delete'>Delete</option><option value='default'>Default</option></select></td>
//            </tr>";

                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "<option value='default'>Default</option></select></td>";
                }else{
                    $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='default'>Default</option></select></td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;


    }
    public function deleteVendorCard()
    {
        $user_id =$_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        \Stripe\Customer::deleteSource(
            "$customer_id",
            "$card_id"
        );

        echo  json_encode(array('status' => 'true', 'message' => "card deleted Successfully" ));
        exit;


    }


    public function defaultVendorCard()
    {

        $user_id = $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        $customer = \Stripe\Customer::update("$customer_id", [
            'default_source' => "$card_id"
        ]);

        echo  json_encode(array('status' => 'true', 'message' => "Card has been set to default" ));
        exit;

    }


    //    owner card functions
    public function addOwnersCardDetails()
    {
        $cardNumber = $_POST['card_number'];
        $exp_date = $_POST['exp_date'];


        $getDateFormat = explode('/',$exp_date);
        $expMonth = $getDateFormat[0];
        $expYear = $getDateFormat[1];

        $cvc = $_POST['cvc'];
        $holder_name = $_POST['holder_name'];
        if(isset($_POST['auto_pay']))
        {
            $autopay = 'ON';
        }
        else
        {
            $autopay = 'OFF';
        }
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

        $user_id =$_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];

        $upateData =  "UPDATE users SET auto_pay='$autopay' where id='$user_id'";
        $stmt = $this->companyConnection->prepare($upateData);
        $stmt->execute();

        $customer_id = $this->createCustomer($user_id);

        try{
            $token = \Stripe\Token::create([
                'card' => [
                    'number'    => $cardNumber,
                    'exp_month' => $expMonth,
                    'exp_year'  => $expYear,
                    'cvc'       => $cvc,
                ],
            ]);

            $token_id = $token->id;
            $customer = \Stripe\Customer::retrieve($customer_id);
            $card = $customer->sources->create(array("source" => $token_id));
            $result['message']    = "Card Added successfully";
            $result['status']   = 'true';
            return  $result;

        }catch(\Stripe\Error\RateLimit $e){

            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 'false', 'message' => $results['message'] ));
            exit;
        }
        catch(\Stripe\Error\Card $e){

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 409, 'message' => $results['message'] ));
            exit;

        } catch (\Stripe\Error\RateLimit $e) {

            // Too many requests made to the API too quickly

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message'] = 'Too many requests made to the API too quickly';
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\InvalidRequest $e) {

            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Network communication with Stripe failed";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\Base $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Error";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        }







    }
    public function getAllOwnerCards()
    {
        $user_id = $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Card Number</th>
            <th>Expiry Month</th>
            <th>Expiry Year</th>
            <th>Brand</th>
            <th>Default</th>
            <th>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 10,
                    'object' => 'card',
                ]
            );
            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['last4']."</td>
            <td>".$card['exp_month']."</td>
            <td>".$card['exp_year']."</td>
            <td>".$card['brand']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }

//                $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='delete'>Delete</option><option value='default'>Default</option></select></td>
//            </tr>";
                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "<option value='default'>Default</option></select></td>";
                }else{
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option><option value='default'>Default</option></select></td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;



    }



    public function getOwnerCardsOnList()
    {
        $user_id = $_POST['owner_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Card Number</th>
            <th>Expiry Month</th>
            <th>Expiry Year</th>
            <th>Brand</th>
            <th>Default</th>
            <th>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 10,
                    'object' => 'card',
                ]
            );

            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['last4']."</td>
            <td>".$card['exp_month']."</td>
            <td>".$card['exp_year']."</td>
            <td>".$card['brand']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }

//                $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='delete'>Delete</option><option value='default'>Default</option></select></td>
//            </tr>";
                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "<option value='default'>Default</option></select></td>";
                }else{
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option><option value='default'>Default</option></select></td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;



    }


    public function getOwnerBankOnList()
    {
        $user_id = $_POST['owner_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Bank Name</th>
            <th>Country</th>
            <th>Currency</th>
            <th>Last4</th>
            <th>Default</th>
            <th>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 10,
                    'object' => 'bank_account',
                ]
            );

            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['bank_name']."</td>
            <td>".$card['country']."</td>
            <td>".$card['currency']."</td>
            <td>************".$card['last4']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }

//                $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='delete'>Delete</option><option value='default'>Default</option></select></td>
//            </tr>";

                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "<option value='default'>Default</option></select></td>";
                }else{
                    $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='default'>Default</option></select></td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;


    }


    public function getOwnerBanks()
    {
        $user_id = $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Bank Name</th>
            <th>Country</th>
            <th>Currency</th>
            <th>Last4</th>
            <th>Default</th>
            <th>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 10,
                    'object' => 'bank_account',
                ]
            );

            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['bank_name']."</td>
            <td>".$card['country']."</td>
            <td>".$card['currency']."</td>
            <td>************".$card['last4']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }

//                $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='delete'>Delete</option><option value='default'>Default</option></select></td>
//            </tr>";

                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "<option value='default'>Default</option></select></td>";
                }else{
                    $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='default'>Default</option></select></td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;


    }
    public function deleteOwnerCard()
    {
        $user_id =$_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        \Stripe\Customer::deleteSource(
            "$customer_id",
            "$card_id"
        );

        echo  json_encode(array('status' => 'true', 'message' => "card deleted Successfully" ));
        exit;


    }



    public function deleteOwnerCardOnList()
    {
        $user_id =$_POST['owner_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        \Stripe\Customer::deleteSource(
            "$customer_id",
            "$card_id"
        );

        echo  json_encode(array('status' => 'true', 'message' => "card deleted Successfully" ));
        exit;


    }


    public function defaultOwnerCard()
    {

        $user_id = $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        $customer = \Stripe\Customer::update("$customer_id", [
            'default_source' => "$card_id"
        ]);

        echo  json_encode(array('status' => 'true', 'message' => "Card has been set to default" ));
        exit;

    }



    public function defaultOwnerCardOnList()
    {

        $user_id = $_POST['owner_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        $customer = \Stripe\Customer::update("$customer_id", [
            'default_source' => "$card_id"
        ]);

        echo  json_encode(array('status' => 'true', 'message' => "Card has been set to default" ));
        exit;

    }




    public function getRentAmount()
    {

        $tenant_id = $_POST['id'];

        $getRentAmount =  $this->companyConnection->query("SELECT * from tenant_lease_details  WHERE user_id ='" . $tenant_id . "'")->fetch();

        $rent_amount = $getRentAmount['rent_amount'];
        $cam_amount = $getRentAmount['cam_amount'];
        $lease_date = $getRentAmount['start_date'];
        if($cam_amount=='')
        {
            $cam_amount = 0;
        }
        else
        {
            $cam_amount = $getRentAmount['cam_amount'];
        }
        $security_deposite = $getRentAmount['security_deposite'];
        if($security_deposite=="")
        {
            $security_deposite = 0;
        }
        else
        {
            $security_deposite = $getRentAmount['security_deposite'];
        }


        $getDates =  $this->companyConnection->query("SELECT * from tenant_move_in  WHERE user_id ='" . $tenant_id . "'")->fetch();


        $actualDate = $getDates['actual_move_in'];
        if($actualDate=='')
        {
            $proratedAmount = '0';
        }

        else if($actualDate<$lease_date)
        {

            $diff = strtotime($lease_date) - strtotime($actualDate);
            $days =  round($diff / (60 * 60 * 24));
            $totalDays = date("t", strtotime($actualDate));
            $oneDayAmount = $rent_amount/$totalDays;
            $proratedAmount = $oneDayAmount * $days;

        }
        else
        {
            $proratedAmount = '0';
        }

        /*        $getExtraCharges = $this->companyConnection->query("SELECT cacc.charge_code,tc.amount,tc.frequency from tenant_charges as tc join company_accounting_charge_code as cacc on cacc.id=tc.charge_code  WHERE tc.user_id ='" . $tenant_id . "' and frequency='One Time'")->fetchAll();

                $extraRate = 0;
                if(!empty($getExtraCharges))
                {
                    foreach($getExtraCharges as $charges)
                    {
                        $extraRate +=   $charges['amount'];
                    }
                }*/

        if($security_deposite=="")
        {
            $security_deposite = 0;
        }

        if($cam_amount=="")
        {
            $cam_amount = 0;
        }

        if($cam_amount=="")
        {
            $cam_amount = 0;
        }

        $proratedAmount = round($proratedAmount,2);

        $amountPaid = $rent_amount + $cam_amount + $security_deposite /*+ $extraRate*/;

        $amount_Paid = round($amountPaid,2);
        $amountPaid = number_format($amount_Paid,2);



        /*Due to changes on this function i am removing prorated amount from here and tenant has to change full rent amount for one day payment*/

        return array('camCharges'=>$cam_amount,'prorated_rates'=>$proratedAmount,'security_charges'=>$security_deposite,'amount_paid'=>$amountPaid,'stripe_amount'=>$amount_Paid);

    }













    public function addTenantCardDetails()
    {
        $cardNumber = $_POST['card_number'];
        $exp_date = $_POST['exp_date'];


        $getDateFormat = explode('/',$exp_date);
        $expMonth = $getDateFormat[0];
        $expYear = $getDateFormat[1];

        if(isset($_POST['auto_pay']))
        {
            $autopay = 'ON';
        }
        else
        {
            $autopay = 'OFF';
        }



        $cvc = $_POST['cvc'];
        $holder_name = $_POST['holder_name'];

        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

        $user_id = $_POST['tenant_id'];
        $upateData =  "UPDATE users SET auto_pay='$autopay' where id='$user_id'";
        $stmt = $this->companyConnection->prepare($upateData);
        $stmt->execute();







        $customer_id = $this->createCustomer($user_id);

        try{
            $token = \Stripe\Token::create([
                'card' => [
                    'number'    => $cardNumber,
                    'exp_month' => $expMonth,
                    'exp_year'  => $expYear,
                    'cvc'       => $cvc,
                ],
            ]);

            $token_id = $token->id;
            $customer = \Stripe\Customer::retrieve($customer_id);
            $card = $customer->sources->create(array("source" => $token_id));
            $card_id = $card->id;
            $customer = \Stripe\Customer::update("$customer_id", [
                'default_source' => "$card_id"
            ]);

            $result['message']    = "Card updated successfully";
            $result['status']   = 'true';
            return  $result;

        }catch(\Stripe\Error\RateLimit $e){

            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 'false', 'message' => $results['message'] ));
            exit;
        }
        catch(\Stripe\Error\Card $e){

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 409, 'message' => $results['message'] ));
            exit;

        } catch (\Stripe\Error\RateLimit $e) {

            // Too many requests made to the API too quickly

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message'] = 'Too many requests made to the API too quickly';
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\InvalidRequest $e) {

            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Network communication with Stripe failed";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\Base $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Error";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        }

    }







    public function getTenantAllCards()
    {
        $user_id = $_POST['tenant_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Card Number</th>
            <th>Expiry Month</th>
            <th>Expiry Year</th>
            <th>Brand</th>
            <th>Default</th>
            <th>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 1,
                    'object' => 'card',
                ]
            );

            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['last4']."</td>
            <td>".$card['exp_month']."</td>
            <td>".$card['exp_year']."</td>
            <td>".$card['brand']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }

//            $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='delete'>Delete</option><option value='default'>Default</option></select></td>
//            </tr>";
                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "</select></td>";
                }else{
                $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>
                       <option value='update'>Update</option>
                       
                       </select>
                       </td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;



    }



    public function getTenantAllBanks()
    {

        $user_id = $_POST['tenant_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
            <th>Bank Name</th>
            <th>Country</th>
            <th>Currency</th>
            <th>Last4</th>
            <th>Default</th>
            <th>Action</th>
            </tr>";

        if($getData['stripe_customer_id']!='')
        {
            $customer_id = $getData['stripe_customer_id'];
            \Stripe\Stripe::setApiKey(Live_Stripe_KEY);

            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 1,
                    'object' => 'bank_account',
                ]
            );

            $customer = \Stripe\Customer::retrieve($customer_id);
            $default_source = $customer['default_source'];
            $cards_count = (isset($cards['data']) && !empty($cards['data'])) ? count($cards['data']):0;
            foreach($cards['data'] as $card)
            {
                $html .= "<tr>
            <td>************".$card['bank_name']."</td>
            <td>".$card['country']."</td>
            <td>".$card['currency']."</td>
            <td>************".$card['last4']."</td>";
                if($default_source==$card['id'])
                {
                    $html .="<td>True</td>";
                }
                else
                {
                    $html .="<td>False</td>";
                }

//            $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='delete'>Delete</option><option value='default'>Default</option></select></td>
//            </tr>";
                if($cards_count > 1){
                    $html .= "<td><select class='cardAction form-control' data-cardId = ".$card['id']."><option value=''>Select</option>";
                    if($default_source!=$card['id'])
                    {
                        $html .= "<option value='delete'>Delete</option>";
                    }
                    $html .= "<option value='default'>Default</option></select></td>";
                }else{
                    $html .= "<td><select class='cardAction' data-cardId = ".$card['id']."><option value=''>Select</option><option value='default'>Default</option></select></td>";
                }
                $html .= "</tr>";

            }
        }
        $html .= "</table>";

        echo $html;
        exit;


    }



    public function deleteTenantCard()
    {
        $user_id = $_POST['tenant_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        \Stripe\Customer::deleteSource(
            "$customer_id",
            "$card_id"
        );

        echo  json_encode(array('status' => 'true', 'message' => "card deleted Successfully" ));
        exit;


    }


    public function defaultTenantCard()
    {

        $user_id = $_POST['tenant_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        $customer_id = $getData['stripe_customer_id'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $card_id = $_POST['card_id'];

        $customer = \Stripe\Customer::update("$customer_id", [
            'default_source' => "$card_id"
        ]);

        echo  json_encode(array('status' => 'true', 'message' => "Card has been set to default" ));
        exit;

    }


    public function checkFirstPaymentStatus()
    {
        $user_id = $_REQUEST['user_id'];
        $getData =  $this->companyConnection->query("SELECT payment_status FROM users WHERE id ='" . $user_id . "'")->fetch();
        if($getData['payment_status']=='1')
        {
            return 1;
        }
        else
        {
            return 0;
        }

    }




    public function checkPaymentMethod()
    {
        $user_id = $_REQUEST['user_id'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users WHERE id ='" . $user_id . "'")->fetch();
        if($getData['stripe_customer_id']=='')
        {
            return array('status'=>'false','message'=>'Add Your payment method','type'=>'add');

        }
        else
        {

            $customer_id =  $getData['stripe_customer_id'];
            $customer = \Stripe\Customer::retrieve($customer_id);
            if(!empty($customer))
            {
                return array('status'=>'true','message'=>'Update your payment method','type'=>'update');
            }

        }
    }


    public function checkAutoPay()
    {
        $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $getData =  $this->companyConnection->query("SELECT auto_pay,zipcode FROM users WHERE id ='" . $user_id . "'")->fetch();

        echo  json_encode(array('status' => 'true', 'message' => "Card has been set to default" ,'data'=>$getData['auto_pay'],'zipcode'=>$getData['zipcode']));
        exit;
    }

    public function checkAutoPayVendor()
    {
        $user_id = $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'];;
        $getData =  $this->companyConnection->query("SELECT auto_pay,zipcode FROM users WHERE id ='" . $user_id . "'")->fetch();

        echo  json_encode(array('status' => 'true', 'message' => "Card has been set to default" ,'data'=>$getData['auto_pay'],'zipcode'=>$getData['zipcode']));
        exit;
    }
    public function checkAutoPayOwner()
    {
        $user_id =$_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];
        $getData =  $this->companyConnection->query("SELECT auto_pay,zipcode FROM users WHERE id ='" . $user_id . "'")->fetch();

        echo  json_encode(array('status' => 'true', 'message' => "Card has been set to default" ,'data'=>$getData['auto_pay'],'zipcode'=>$getData['zipcode']));
        exit;
    }


    public function checkTenantAutoPay()
    {
        $user_id = $_POST['tenant_id'];
        $getData =  $this->companyConnection->query("SELECT auto_pay,zipcode FROM users WHERE id ='" . $user_id . "'")->fetch();

        echo  json_encode(array('status' => 'true', 'message' => "Card has been set to default" ,'data'=>$getData['auto_pay'],'zipcode'=>$getData['zipcode']));
        exit;
    }

    public function smartMovePayment()
    {

        $token_id = $_REQUEST['token_id'];
        $currency = $_REQUEST['currency'];
        $amount = $_REQUEST['amount'];
        $user_id = $_REQUEST['user_id'];
        $user_type = $_REQUEST['user_type'];


        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        try {

            $charge = \Stripe\Charge::create([
                "amount" => $amount * 100,
                "currency" => $currency,
                "source" => $token_id,
            ]);


            $charge_id = $charge['id'];
            $transaction_id = $charge['balance_transaction'];
            $stripe_status = $charge;
            $payment_response = 'Payment has been done successfully.';

            $data['transaction_id'] = $charge_id;
            $data['charge_id'] = $transaction_id;
            $data['user_id'] = $user_id;
            $data['user_type'] = $user_type;
            $data['amount'] = $amount;
            $data['total_charge_amount'] = $amount;
            $data['prorated_payment_amount'] = $amount;
            $data['stripe_status'] = $stripe_status;
            $data['payment_response'] = $payment_response;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');


            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('code' => 200, 'status' => 'success', 'charge_id' => $charge->id, 'charge_data' => $charge, 'message' => "Payment has been done successfully.");

        } catch (Exception $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
        }
    }

    /*function to add new vendor card details */

    public function addNewVendorCardDetails()
    {
        $cardNumber = $_POST['ccard_number'];
        $expMonth =  $_POST['cexpiry_month'];
        $expYear =  $_POST['cexpiry_year'];
        $cvc = $_POST['ccvv'];
        \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
        $customer_id = $this->createNewCustomer();
        if(!empty($customer_id)) {
            try {
                $token = \Stripe\Token::create([
                    'card' => [
                        'number' => $cardNumber,
                        'exp_month' => $expMonth,
                        'exp_year' => $expYear,
                        'cvc' => $cvc,
                    ],
                ]);
                $token_id = $token->id;
                $customer = \Stripe\Customer::retrieve($customer_id);
                $card = $customer->sources->create(array("source" => $token_id));
                $result['message'] = "Card Added successfully";
                $result['status'] = 'true';
                return (array('status' => 200, 'result' => $result['message'],'customer_id'=>$customer_id));

            } catch (\Stripe\Error\RateLimit $e) {

                $body = $e->getJsonBody();
                $err = $body['error'];
                $results['status'] = $e->getHttpStatus();
                $results['type'] = $err['type'];
                $results['code'] = $err['code'];
                $results['param'] = $err['param'];
                $results['message'] = $err['message'];

                return (array('status' => 'false', 'message' => $results['message']));
            } catch (\Stripe\Error\Card $e) {

                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err = $body['error'];
                $results['status'] = $e->getHttpStatus();
                $results['type'] = $err['type'];
                $results['code'] = $err['code'];
                // param is '' in this case
                $results['param'] = $err['param'];
                $results['message'] = $err['message'];

                return (array('status' => 409, 'message' => $results['message']));

            } catch (\Stripe\Error\RateLimit $e) {

                // Too many requests made to the API too quickly

                $body = $e->getJsonBody();
                $err = $body['error'];

                $results['message'] = 'Too many requests made to the API too quickly';
                return (array('status' => 409, 'message' => $err['message']));

            } catch (\Stripe\Error\InvalidRequest $e) {

                // Invalid parameters were supplied to Stripe's API
                $body = $e->getJsonBody();
                $err = $body['error'];

                $results['message'] = "Invalid parameters were supplied to Stripe's API";
                return (array('status' => 409, 'message' => $err['message']));

            } catch (\Stripe\Error\Authentication $e) {

                $body = $e->getJsonBody();
                $err = $body['error'];

                return (array('status' => 409, 'message' => $err['message']));

            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed

                $body = $e->getJsonBody();
                $err = $body['error'];

                $results['message'] = "Network communication with Stripe failed";
                return (array('status' => 409, 'message' => $err['message']));

            } catch (\Stripe\Error\Base $e) {

                $body = $e->getJsonBody();
                $err = $body['error'];

                $results['message'] = "Error";
                return (array('status' => 409, 'message' => $err['message']));

            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                $results['message'] = "Something else happened, completely unrelated to Stripe";

                $body = $e->getJsonBody();
                $err = $body['error'];

                return (array('status' => 409, 'message' => $err['message']));
            }
        }else{
            return (array('status' => 400, 'message' => "Unbale to add payment method."));
        }
    }


    public function createNewCustomer()
    {

        $customer_id ='';
        try {
            $customer = \Stripe\Customer::create();
            $customer_id = $customer['id'];
            return $customer_id;
        } catch (Exception $e) {
            return $customer_id;
        }

    }




    public function setPaymentProcess()
    {

        $interval = $_POST['interval'];
        $user_id  = $_POST['user_id'];
        $amount   = $_POST['amount'];
        $count    = $_POST['count'];

        if(isset($_POST['paymentCategory']) || !empty($_POST['paymentCategory']))
        {
            $payment_category = $_POST['paymentCategory'];
            $checkExistCategory = $this->checkExistCategory($payment_category,$interval,$user_id);
        }
        else
        {
            return array('status'=>'false','message'=>'Please Select atleast one payment category');
        }



        if($checkExistCategory['status'] == 'false')
        {
            return array('status'=>'false','message'=>$checkExistCategory['message']);
        }

        if(!empty($payment_category))
        {

            foreach($payment_category as $category)
            {
                try
                {

                    if($interval=="monthly")
                    {
                        $date = $_POST['date'];
                        /*$date =  mySqlDateFormat($date,null,$this->companyConnection);
                        $date =  strtotime($date);
                        $date =  date('d',$date);*/
                    }
                    else
                    {
                        $date = $_POST['date'];
                    }


                    $data['user_id'] = $user_id;
                    $data['payment_type'] = $interval;
                    $data['payment_date'] = $date;
                    $data['payment_category'] = $category;  /*1 = rent, 2= utility, 3 = miscellaneous*/
                    $data['amount'] = $amount;
                    $data['interval_count'] = $count;
                    date_default_timezone_set('America/New_York');
                    if($interval=='weekly')
                    {
                        $date= date('Y-m-d');
                        $data['today'] =  $date;
                        $data['process_date'] = date("Y-m-d", strtotime("+1 week"));
                    }
                    else if($interval=='bi-weekly')
                    {
                        $date= date('Y-m-d');
                        $data['today'] =  $date;

                        $newDate =  date("Y-m-d", strtotime("+15 days"));
                        $data['process_date'] = $newDate;

                    }
                    else if($interval=='annual')
                    {
                        $date= date('Y-m-d');
                        $data['today'] =  $date;
                        $data['process_date']  = date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));

                    }





                    $getData =  $this->companyConnection->query("SELECT * FROM payment_interval WHERE user_id ='".$user_id."' and payment_type='".$interval."' and payment_category=$category")->fetch();

                    if(!empty($getData))
                    {
                        $data['status'] = 'active';
                        $sqlData = createSqlColValPair($data);
                        $query = "UPDATE payment_interval SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $user_id;
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();

                    }
                    else
                    {
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO payment_interval(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'].")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);


                    }
                }
                catch (Exception $e) {
                    return array('status' => 'failed', 'message' => $e->getMessage(),'line'=>$e->getLine());
                }
            }
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData1 = createSqlColValPair($data1);
            $query1 = "UPDATE users SET " . $sqlData1['columnsValuesPair'] . " where id=" . $user_id;
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute();

            return array('status' => 'success', 'message' => 'payment has been set successfully');
        }
    }



    public function checkExistCategory($payment_category,$interval,$user_id)
    {


        foreach($payment_category as $category)
        {

            $getData =  $this->companyConnection->query("SELECT * FROM payment_interval WHERE user_id ='".$user_id."' and status='active' and payment_category=$category and payment_type='$interval' and (interval_count!=interval_used_count)")->fetch();

            if(!empty($getData))
            {
                return array('status'=>'false','message'=>'You can not add same payment process with same payment interval and category');
            }
        }

    }


    public function getPaymentIntervals()
    {
        $tenant_id = $_POST['tenant_id'];
        $getData =  $this->companyConnection->query("SELECT * FROM payment_interval WHERE user_id ='" . $tenant_id . "' and status!='cancel'")->fetchAll();
        $html = "";
        $html .= "<table class='table' border= '1px'>";
        $html .= "<th>Payment Type</th>";
        $html .= "<th>Amount</th>";
        $html .= "<th>Interval count</th>";
        $html .= "<th>Payment Date</th>";
        $html .= "<th>Interval Done</th>";
        $html .= "<th>Category</th>";
        $html .= "<th>Status</th>";
        $html .= '</tr>';

        if(!empty($getData)) {
            foreach($getData as $data)
            {
                $category = $data['payment_category'];
                if($category==1)
                {
                    $cat_name = 'Rent';
                }
                else if($category==2)
                {
                    $cat_name = 'Utilities';
                }
                else if($category==3)
                {
                    $cat_name = 'Miscellaneous';
                }

                $html .= '<tr>';
                $html .= "<td>".ucfirst($data['payment_type'])."</td>";
                $html .= "<td>".$data['amount']."</td>";
                $html .= "<td>".$data['interval_count']."</td>";
                $html .= "<td>".$data['payment_date']."</td>";
                $html .= "<td>".$data['interval_used_count']."</td>";
                $html .= "<td>$cat_name</td>";
                $status = $data['status'];
                $id = $data['id'];




                $html .= "<td><select class='status form-control' data-id = '$id'><option value='active'>Active</option><option value='cancel'>Cancel</option></td>";
                $html .= '</tr>';
            }
        }
        $html .= '</table>';
        echo $html;
        exit;

    }







    public function getNewRentAmount()
    {

        $tenant_id = $_POST['id'];
        $total_amount_due = 0.00;
        $show_total_amount_due = 0.00;
        $getRentAmount =  $this->companyConnection->query("SELECT * from tenant_lease_details  WHERE user_id ='" . $tenant_id . "'")->fetch();
        $rent_amount = $getRentAmount['rent_amount'];
        $lease_date = $getRentAmount['start_date'];

        $getDates =  $this->companyConnection->query("SELECT * from tenant_move_in  WHERE user_id ='" . $tenant_id . "'")->fetch();
        $actualDate = $getDates['actual_move_in'];
        $get_total_charges_due = $this->getTotalChargesDue($tenant_id);
        $get_total_cam_cahrges = $this->getTotalCamChargesDue($tenant_id);

        if($actualDate=='')
        {

            $month_differnce = $this->getMonthDifference($lease_date);
            $total_rent = $month_differnce * $rent_amount;
            $rent = $this->getTotalRentPaid($tenant_id);
            $rent_to_be_paid = $total_rent-  $rent;
            $total_amount_due=   $this->getLeaseDateRentAmount($tenant_id,$total_rent);
            $show_total_amount_due = number_format($total_amount_due,2);
        }
        else if($actualDate<$lease_date)
        {

            $month_differnce = $this->getMonthDifference("2020-01-27");
            $total_rent = $month_differnce * $rent_amount;
            $total_amount_due = $this->getActualDateRentAmount($tenant_id,$total_rent);
            $rent = $this->getTotalRentPaid($tenant_id);
            $rent_to_be_paid = $total_rent-  $rent;
            $show_total_amount_due = number_format($total_amount_due,2);
        }
        else
        {
            $month_differnce = $this->getMonthDifference($lease_date);
            $total_rent = $month_differnce * $rent_amount;
            $total_amount_due=   $this->getLeaseDateRentAmount($tenant_id,$total_rent);
            $rent = $this->getTotalRentPaid($tenant_id);
            $rent_to_be_paid = $total_rent-  $rent;
            $show_total_amount_due = number_format($total_amount_due,2);
        }


        /*Due to changes on this function i am removing prorated amount from here and tenant has to change full rent amount for one day payment*/
        return array('total_amoun_due'=>$total_amount_due,'show_total_amount_due'=>$show_total_amount_due,'total_rent'=>$total_rent,'total_charges_due'=>$get_total_charges_due,'rent_to_be_paid'=>$rent_to_be_paid,'total_security_cam_charges'=>$get_total_cam_cahrges);

    }



    function  getActualDateRentAmount($tenant_id,$total_rent){
        $rent = $this->getTotalRentPaid($tenant_id);
        $rent_to_be_paid = $total_rent-  $rent;
        $get_total_charges_due = $this->getTotalChargesDue($tenant_id);
        $get_total_cam_cahrges = $this->getTotalCamChargesDue($tenant_id);
        $total_rent_charges_cam_security_due = $rent_to_be_paid+$get_total_charges_due+$get_total_cam_cahrges;
        return $total_rent_charges_cam_security_due;

    }

    function  getLeaseDateRentAmount($tenant_id,$total_rent){
        $rent = $this->getTotalRentPaid($tenant_id);
        $rent_to_be_paid = $total_rent-  $rent;
        $get_total_charges_due = $this->getTotalChargesDue($tenant_id);
        $get_total_cam_cahrges = $this->getTotalCamChargesDue($tenant_id);
        $total_rent_charges_cam_security_due = $rent_to_be_paid+$get_total_charges_due+$get_total_cam_cahrges;
        return $total_rent_charges_cam_security_due;
    }

    function getTotalRentPaid($user_id){
        $get_total_paid_rent_data =  $this->companyConnection->query("SELECT SUM(amount) as rent_amount ,SUM(credit) as credit,  (SUM(amount) + SUM(credit)) as paid_rent_amount  from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('RENT','RENT_FIRSTTIME') ")->fetch();
        if(!empty($get_total_paid_rent_data)){
            $total_paid_rent = $get_total_paid_rent_data["paid_rent_amount"];
        }else{
            $total_paid_rent = 0;
        }
        return $total_paid_rent;


    }


    function getTotalChargesDue($user_id){

        $get_total_charges_due =  $this->companyConnection->query("SELECT SUM(amount) as unpaid_charges  from tenant_charges  WHERE user_id ='" . $user_id . "' AND status ='0' ")->fetch();
        if(!empty($get_total_charges_due["unpaid_charges"])){
            $total_unpaid_charges = $get_total_charges_due["unpaid_charges"];
        }else{
            $total_unpaid_charges = 0;
        }
        return $total_unpaid_charges;


    }

    function getTotalCamChargesDue($user_id){
        $get_cam_security_charges =  $this->companyConnection->query("SELECT * from  tenant_lease_details  WHERE user_id ='" . $user_id . "'")->fetch();
        $cam_amount = 0.00;
        $security_deposit = 0.00;
        if(!empty($get_cam_security_charges)){
            $cam_amount = (isset($get_cam_security_charges["cam_amount"]) && !empty($get_cam_security_charges["cam_amount"])) ? $get_cam_security_charges["cam_amount"]:0.00;
            $security_deposit = (isset($get_cam_security_charges["security_deposite"]) && !empty($get_cam_security_charges["security_deposite"])) ? $get_cam_security_charges["security_deposite"]:0.00;
        }
        $get_total_paid_cam =  $this->companyConnection->query("SELECT  (SUM(amount) + SUM(credit)) as paid_cam_charges from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('CAM')")->fetch();
        $get_total_paid_security =  $this->companyConnection->query("SELECT (SUM(amount) + SUM(credit)) as paid_security_charges  from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('SECURITY')")->fetch();
        if(!empty($get_total_paid_cam)){
            $cam_amount = str_replace( ',', '', $cam_amount) - $get_total_paid_cam["paid_cam_charges"] ;
        }

        if(!empty($get_total_paid_security)){
            $security_deposit = str_replace( ',', '', $security_deposit) - $get_total_paid_security["paid_security_charges"] ;
        }
        $total_due_cam_security_charges = $cam_amount + $security_deposit;
        return $total_due_cam_security_charges;
    }


    function getMonthDifference($date){
        $today = date("Y-m-d");
        $date1=date_create($today);
        $date2=date_create($date);
        $diff=date_diff($date1,$date2);
        $month_differnce = $diff->m;
        return  $month_differnce;

    }



    public function changeIntervalStatus()
    {
        $interval_id = $_POST['interval_id'];
        try{
            $qry1 = "UPDATE payment_interval SET status='cancel' where id=".$interval_id;
            $stmt1 = $this->companyConnection->prepare($qry1);
            $stmt1->execute();
            return array('status'=>'true','message'=>'Interval has been canceled successfully');
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }



    public function addChargeAmount($id,$type)
    {
        if($type=='user')
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE user_id ='" . $id . "'")->fetch();
        }
        else
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE invoice_id ='" . $id . "'")->fetch();
        }



        if($getCharges['total_amount']=='')
        {
            $totalAmount = 0;
            $data['total_amount'] = $totalAmount;
        }
        else
        {
            $totalAmount = $getCharges['total_amount'];
            $totalAmount =    str_replace(',', '', $totalAmount);
            $data['total_amount'] = $totalAmount;

        }


        if($getCharges['amount_paid']=='')
        {
            $amount_paid = 0;
            $data['total_amount_paid'] = $amount_paid;
        }
        else
        {
            $amount_paid = $getCharges['amount_paid'];
            $amount_paid =    str_replace(',', '', $amount_paid);
            $data['total_amount_paid'] = $amount_paid;
        }

        if($getCharges['amount_refunded']=='')
        {
            $amount_refunded = 0;
            $data['total_refunded_amount'] = $amount_refunded;
        }
        else
        {
            $amount_refunded = $getCharges['amount_refunded'];
            $amount_refunded =    str_replace(',', '', $amount_refunded);
            $data['total_refunded_amount'] = $amount_refunded;
        }
        if($getCharges['waive_of_amount']=='')
        {
            $waive_of_amount = 0;
            $data['total_waive_amount'] = $waive_of_amount;
        }
        else
        {
            $waive_of_amount = $getCharges['waive_of_amount'];
            $waive_of_amount =    str_replace(',', '', $waive_of_amount);
            $data['total_waive_amount'] = $waive_of_amount;
        }
        $data['overpay_underpay'] = str_replace(',', '', $_POST['overPay']);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['total_due_amount'] = str_replace(',', '', $getCharges['amount_due']);
        $sqlData = createSqlColValPair($data);
        if($type=='user')
        {
            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }
        else
        {
            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where invoice_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }


    }

    function UpdateTenantChargeStatus ($id,$charge_amount){


        try {
            $chargeTableId = $id; /*id of charge table*/

            $getData =  $this->companyConnection->query("SELECT * FROM tenant_charges where id='$chargeTableId'")->fetch();



            $currentPayment = $charge_amount;
            $currentPayment =      str_replace(',', '', $currentPayment);

            $amt = $getData['amount'];
            $amt =      str_replace(',', '', $amt);
            $waveAmt = 0;
            $alreadyPaid = $getData['amount_paid'];
            if($alreadyPaid=='')
            {
                $alreadyPaid = 0;
            }
            $currentAmtPaid = $alreadyPaid + $currentPayment;
            $currentDueAmt = $amt - $currentPayment - $waveAmt;
            $data['amount_due'] = $currentDueAmt;
            $data['amount_paid'] = $currentAmtPaid;
            $data['waive_of_amount'] = $waveAmt;
            $data['status'] = 1;
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE tenant_charges SET " . $sqlData['columnsValuesPair'] . " where id=" . $chargeTableId;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'message' => "charge adjusted successfully");
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(),'line'=>$e->getLine());
        }

    }


    function UpdateCredit ($id,$remainingAmount){


        try {
            $data['overpay_underpay'] = $remainingAmount["credit_left"]+$remainingAmount["stripe_payment"];
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'message' => "charge adjusted successfully",'remaining_amount' =>$remainingAmount);
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(),'line'=>$e->getLine());
        }

    }


    function getTotalSecurityChargesToPaid($user_id){

        $get_cam_security_charges =  $this->companyConnection->query("SELECT * from  tenant_lease_details  WHERE user_id ='" . $user_id . "'")->fetch();
        $cam_amount = 0.00;
        $security_deposit = 0.00;
        if(!empty($get_cam_security_charges)){
            $cam_amount = (isset($get_cam_security_charges["cam_amount"]) && !empty($get_cam_security_charges["cam_amount"])) ? $get_cam_security_charges["cam_amount"]:0.00;
            $security_deposit = (isset($get_cam_security_charges["security_deposite"]) && !empty($get_cam_security_charges["security_deposite"])) ? $get_cam_security_charges["security_deposite"]:0.00;
        }
        $get_total_paid_cam =  $this->companyConnection->query("SELECT SUM(amount) as paid_cam_charges  from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('CAM')")->fetch();
        $get_total_paid_security =  $this->companyConnection->query("SELECT SUM(amount) as paid_security_charges  from transactions  WHERE user_id ='" . $user_id . "' AND user_type ='TENANT' AND payment_status = 'SUCCESS' AND type IN ('SECURITY')")->fetch();

        if(!empty($get_total_paid_cam)){
            $cam_amount = 0.00;
        }

        if(!empty($get_total_paid_security)){
            $security_deposit == 0.00;
        }
        $total_due_cam_security_charges = str_replace( ',', '', $cam_amount) + str_replace( ',', '', $security_deposit);
        return array('code' => 200, 'status' => 'success', 'message' => "Total cam an security charge fetched successfully",'security_remaining' => str_replace( ',', '', $security_deposit),'cam_remaining' => str_replace( ',', '', $cam_amount));

        //return $total_due_cam_security_charges;


    }


    function addTransactionsData($charge_id,$charge,$amount,$user_id,$payment_mode,$type,$payment_module,$credit){
        try {
            $getPropertyId = $this->getPropertyId($user_id);
            $getbankId = $this->getBankId($getPropertyId);
            $charge_id = $charge_id;
            $transaction_id =  generateTransactionId();
            $stripe_status  = $charge;
            $payment_response  = 'Payment has been done successfully.';
            $data1['transaction_id'] = $charge_id;
            $data1['charge_id'] = $transaction_id;
            $data1['user_id'] = $user_id;
            $data1['receiver_id'] = 1;
            $data1['user_type'] = 'TENANT';
            $data1['amount'] = $amount;
            $data1['total_charge_amount'] = $amount;
            $data1['prorated_payment_amount'] = $amount;
            $data1['bank_mode'] =$getbankId["bank_mode"];
            $data1['payment_mode'] = $payment_mode;
            $data1['type'] = "SECURITY";
            $data1['payment_module'] = $payment_module;
            $data1['payment_status'] = 'SUCCESS';
            $data1['type'] = $type;
            $data1['stripe_status'] = serialize($stripe_status);
            $data1['credit'] = $credit;
            $data1['property_id'] =$getPropertyId;
            $data1['bank_id'] =$getbankId["bank_id"];
            $data1['payment_response'] = $payment_response;
            $data1['auto_transaction_id'] = getAutoTransactionId($this->companyConnection);
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            return array('code' => 200, 'status' => 'success', 'message' => "Security deposited succesfully" );
        }catch(Exception $e){
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(),'line'=>$e->getLine());
        }
    }


    function getNewRentAmountToBePaid($id)
    {

        $tenant_id = $id;
        $total_amount_due = 0.00;
        $show_total_amount_due = 0.00;
        $getRentAmount =  $this->companyConnection->query("SELECT * from tenant_lease_details  WHERE user_id ='" . $tenant_id . "'")->fetch();
        $rent_amount = $getRentAmount['rent_amount'];
        $lease_date = $getRentAmount['start_date'];

        $getDates =  $this->companyConnection->query("SELECT * from tenant_move_in  WHERE user_id ='" . $tenant_id . "'")->fetch();
        $actualDate = $getDates['actual_move_in'];
        $get_total_charges_due = $this->getTotalChargesDue($tenant_id);

        if($actualDate=='')
        {

            $month_differnce = $this->getMonthDifference($lease_date);
            $total_rent = $month_differnce * $rent_amount;
            $total_amount_due=   $this->getLeaseDateRentAmount($tenant_id,$total_rent);
            $show_total_amount_due = number_format($total_amount_due,2);
        }
        else if($actualDate<$lease_date)
        {
            $month_differnce = $this->getMonthDifference("2020-01-27");
            $total_rent = $month_differnce * $rent_amount;
            $total_amount_due = $this->getActualDateRentAmount($tenant_id,$total_rent);
            $show_total_amount_due = number_format($total_amount_due,2);
        }
        else
        {
            $month_differnce = $this->getMonthDifference($lease_date);
            $total_rent = $month_differnce * $rent_amount;
            $total_amount_due=   $this->getLeaseDateRentAmount($tenant_id,$total_rent);
            $show_total_amount_due = number_format($total_amount_due,2);
        }


        /*Due to changes on this function i am removing prorated amount from here and tenant has to change full rent amount for one day payment*/
        return array('total_amoun_due'=>$total_amount_due,'show_total_amount_due'=>$show_total_amount_due,'total_rent'=>$total_rent,'total_charges_due'=>$get_total_charges_due);

    }

    function makeBankDefault($getConnectedAccount,$getbankId){
        try {
            \Stripe\Account::updateExternalAccount(
                $getConnectedAccount,
                $getbankId["bank_id"],
                ['default_for_currency' => true]
            );
            return array('code' => 200, 'status' => 'success', 'message' => "Security deposited succesfully" );
        }catch(Exception $e){
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(),'line'=>$e->getLine());
        }
    }


}








$UnitTypeAjax = new Stripe();