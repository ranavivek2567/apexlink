<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");

class ChatAjax extends DBConnection
{
    /**
     * ChatAjax constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getChatMembers(){
        try{
            $sql = "SELECT name,id,role FROM users WHERE user_type='1'" ;
            $stmt = $this->companyConnection->query($sql); // Prepare the statement
            $data = $stmt->fetchAll();
            return array('code' => 200, 'status' => 'success', 'data' => $data);
        } catch (PDOException $e) {
            dd($e);
        }
    }

    /**
     * function to save chat history
     */
    public function saveChatHistory(){
        try{
            $group = $this->checkGroup($_POST['id'],$_POST['sendTo']);
            $chatData = [];
            $chatData['user_id'] = $_POST['id'];
            $chatData['sender_id'] = $_POST['id'];
            $chatData['recevier_id'] = $_POST['sendTo'];
            $chatData['conversation_id'] = $group['group'];
            $chatData['message'] = $_POST['message'];
            $chatData['type'] = $_POST['type'];
            $chatData['date'] = date('d F, Y');
            $chatData['time'] = date('Y-m-d H:i:s');
            $chatData['created_at'] = date('Y-m-d H:i:s');
            $chatData['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($chatData);
            //Save Data in Company Database
            $query = "INSERT INTO  chat_history (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($chatData);
            return array('code' => 200, 'status' => 'success');
        } catch (PDOException $e) {
            dd($e);
        }
    }

    public function checkGroup($userId,$sendTo){
        try{
            $group = $userId.'_'.$sendTo;
            $reverse_group = $sendTo.'_'.$userId;
            $query = $this->companyConnection->query('SELECT id,conversation_id FROM chat_history WHERE conversation_id="'.$group.'"')->fetchAll();
            if(!empty($query)){
                return array('code' => 200, 'status' => 'success','group'=>$group);
            } else {
                $reverseQuery = $this->companyConnection->query('SELECT id,conversation_id FROM chat_history WHERE conversation_id="'.$reverse_group.'"')->fetchAll();
                if(!empty($reverseQuery)){
                    return array('code' => 200, 'status' => 'success','group'=>$reverse_group);
                } else {
                    return array('code' => 400, 'status' => 'error','group'=>$group);
                }
            }
        } catch (PDOException $e) {
            dd($e);
        }
    }

    public function getPrivateRoom(){
        try{
            return $this->checkGroup($_POST['id'],$_POST['sendTo']);
        } catch (PDOException $e) {
            dd($e);
        }
    }

    /**
     * function to get chat history
     */
    public function getChatHistory(){
        try{
            //Get chat history data
            $id = $_POST['id'];
            $sendTo = $_POST['sendTo'];
            $type = $_POST['type'];
            $group = $this->checkGroup($id,$sendTo);
            $group = $group['group'];
            $userDetail = $this->companyConnection->query('SELECT name,id,role FROM users WHERE id='.$id)->fetch();
            $sendToDetail = $this->companyConnection->query('SELECT name,id,role FROM users WHERE id='.$sendTo)->fetch();
            $query = "SELECT * FROM chat_history WHERE conversation_id='".$group."' AND type='".$type."' ORDER BY created_at asc";
            $data = $this->companyConnection->query($query)->fetchAll();

            if(!empty($data)) {
                foreach ($data as $key=>$value){
                    if(!empty($value['time'])){
                        $data[$key]['time'] = date('g:i A',strtotime(convertTimeZoneDefaultSeting(null,date('H:i:s',strtotime($value['time'])),$this->companyConnection)));
                    }
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'userDetail' => $userDetail, 'sendToDetail' => $sendToDetail );
            } else {
                return array('code' => 400, 'status' => 'success', 'data' => $data, 'userDetail' => $userDetail, 'sendToDetail' => $sendToDetail );
            }
        } catch (PDOException $e) {
            dd($e);
        }
    }

}

$propertyDetail = new ChatAjax();
?>