<?php
include(ROOT_URL."/config.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class FlashMessage {

    public static function render() {
        if (!isset($_SESSION[SESSION_DOMAIN]['messages'])) {
            return null;
        }
        $messages = $_SESSION[SESSION_DOMAIN]['messages'];
        unset($_SESSION[SESSION_DOMAIN]['messages']);
        return implode('<br/>', $messages);
    }

    public static function add($message) {
        if (!isset($_SESSION[SESSION_DOMAIN]['messages'])) {
            $_SESSION[SESSION_DOMAIN]['messages'] = array();
        }
        $_SESSION[SESSION_DOMAIN]['messages'][] = $message;
    }

}

$user = new FlashMessage();
