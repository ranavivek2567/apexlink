<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class InTouch extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function addType() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');
            //Required variable array
            $required_array = ['in_touch_type'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id))
                    unset($data['id']);
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'in_touch_type', '	in_touch_type', $data['in_touch_type'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Type already exists!');
                    }
                }
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['status'] ='1';
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO in_touch_type (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function addInTouchFormData(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id))
                    unset($data['id']);

                //Save Data in Company Database

                $data['subscribed_user']=(isset($data['subscribed_user']) && !empty($data['subscribed_user']) ? serialize($data['subscribed_user']) : null);
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['send_reminder']=$_POST['remindercheck'];

                $date = trim($data['optional_due_date']);

                if(!empty($date)) {
                    $format = explode("/", $date);
                    $year = $format[0];
                    $month = $format[1];
                    $day = $format[2];
                    $formatDate = $year . '-' . $month . '-' . $day;
                    $data['optional_due_date']=$formatDate;
                }
                $current_date_time = strtotime(date('Y-m-d H:i:s'));
                $intouch_date_time =strtotime($data['optional_due_date']);
//                echo $current_date_time;
//                dd($intouch_date_time);
                if($current_date_time >=  $intouch_date_time){
                    $data['status']='3';
                }
                if($current_date_time <  $intouch_date_time){
                    $data['status']='0';
                }
                if($_POST['completedMarkcheck'] == 1){

                    $data['status'] ='1';
                }
                $LoginUserName= $_SESSION[SESSION_DOMAIN]['name'];
                if(isset($_POST['intouchId']) && !empty($_POST['intouchId'])){
                    $sqlData = createSqlUpdateCase($data);
                    $query =  "UPDATE in_touch_detail SET " . $sqlData['columnsValuesPair'] . " where id=" .$_POST['intouchId'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    $notes = $data['notes'];
                    $createddate = date('Y-m-d H:i:s');

                    $in_touch_notes=[];
                    $in_touch_notes['notes']= $notes;
                    $in_touch_notes['in_touch_id']=$id;
                    $in_touch_notes['created_at']= $createddate;

                    $sqlDatanotes = createSqlColVal($in_touch_notes);
                    $query1 = "INSERT INTO in_touch_notes (" . $sqlDatanotes['columns'] . ") VALUES (" . $sqlDatanotes['columnsValues'] . ")";
//                    dd($query1);
                    $stm = $this->companyConnection->prepare($query1);
                    $stm->execute($in_touch_notes);

                    $queryhistory = "INSERT INTO in_touch_logs(username,action,action_time,description,in_touch_id) VALUES ('".$LoginUserName."','update','" . $createddate . "','Update-Modified In -Touch','" . $_POST['intouchId'] . "')";
                    $Resqueryhistory = $this->companyConnection->prepare($queryhistory);
                    $Resqueryhistory->execute();

                    return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record updated successfully');


                }   else {
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO in_touch_detail (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    $notes = $data['notes'];
                    $createddate = date('Y-m-d H:i:s');

                    $in_touch_notes=[];
                    $in_touch_notes['notes']= $notes;
                    $in_touch_notes['in_touch_id']=$id;
                    $in_touch_notes['created_at']= $createddate;

                    $sqlDatanotes = createSqlColVal($in_touch_notes);
                    $query1 = "INSERT INTO in_touch_notes (" . $sqlDatanotes['columns'] . ") VALUES (" . $sqlDatanotes['columnsValues'] . ")";
//                    dd($query1);
                    $stm = $this->companyConnection->prepare($query1);
                    $stm->execute($in_touch_notes);

                    $queryhistory = "INSERT INTO in_touch_logs(username,action,action_time,description,in_touch_id) VALUES ('".$LoginUserName."','insert','" . $createddate . "','Insert-Added In -Touch','" . $id . "')";
                    $Resqueryhistory = $this->companyConnection->prepare($queryhistory);
                    $Resqueryhistory->execute();

                    return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function fetchAllTouchType() {
        $html = '';
        $sql = "SELECT * FROM in_touch_type";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = "<option value=''>Select</option>";
        foreach ($data as $d) {

            $html.= "<option value='" . $d['id'] . "'>" . $d['in_touch_type'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function fetchAllAssignedUser() {
        $html = '';
        $sql = "SELECT * FROM users ORDER BY name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {

            $html.= "<option value='" . $d['id'] . "'>" . $d['name'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }
    public function getAddUserDetail(){
        $res=[];
        $datauserDD = (isset($_POST['datauser']) && !empty($_POST['datauser'])) ? $_POST['datauser'] : null;
       foreach($datauserDD as $k=>$v){
           $sql = "SELECT id,name FROM users WHERE id='".$v."'";
           $res[] = $this->companyConnection->query($sql)->fetch();
       }
        return array('res' => $res, 'status' => 'success');
    }
    public function getIntouchDetail(){
        $id=(isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : null;

        $table = 'in_touch_detail';
        $columns = [$table . '.*','users.name','in_touch_type.in_touch_type'];
        $joins = [[
            'type' => 'LEFT',
            'table' => 'in_touch_detail',
            'column' => 'assigned_user',
            'primary' => 'id',
            'on_table' => 'users',
            'as' => 'users'
        ], [
            'type' => 'LEFT',
            'table' => 'in_touch_detail',
            'column' => 'type',
            'primary' => 'id',
            'on_table' => 'in_touch_type',
            'as' => 'in_touch_type'
        ],
        ];
        $where = [[
            'table' => 'in_touch_detail',
            'column' => 'id',
            'condition' => '=',
            'value' => $id
        ]];
        $query = selectQueryBuilder($columns, $table, $joins, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $optional_due_date=date_format(date_create($data['optional_due_date']),'Y/m/d H:i');

        $trHtml='';
        $subscribed_user=(isset($data['subscribed_user']) && !empty($data['subscribed_user'])) ? unserialize($data['subscribed_user']) : null;
        if(!empty($subscribed_user)){
        foreach($subscribed_user as $subscribed_userone){

            $userSub = "SELECT * FROM users WHERE id=$subscribed_userone";
            $userSubRes = $this->companyConnection->query($userSub)->fetch();

            $trHtml .='<tr></tr><td>'.$userSubRes['name'].'</td>';
            $trHtml .='<td><a style="" rel="'.$userSubRes['id'].'" class="add-icon deleteUser" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a></td></tr>';
        }}
//       dd($id);
        $html='';
        if(isset($_POST['id']) && !empty($_POST['id'])) {
            $notes = "SELECT * FROM in_touch_notes WHERE in_touch_id=$id ORDER BY id DESC";
            $datanotes = $this->companyConnection->query($notes)->fetchAll();

            $LoginUserName = $_SESSION[SESSION_DOMAIN]['name'];

            if (!empty($datanotes)) {
                $j = count($datanotes);
                foreach ($datanotes as $datanotesone) {
                    $datacreatedat = (!empty($datanotesone['created_at'])) ? dateFormatUser($datanotesone['created_at'], null, $this->companyConnection) : '';
                    $html .= '<div class="form-row" style="margin-left:82px;"><div class="clsAddNotes"><b>' . $j . ' - ' . $LoginUserName . ' - ' . $datacreatedat . '</b><br><span id="InTouchNotesText1" notesid="1348">';
                    $html .= $datanotesone['notes'] . '</span></div></div>';
                    $j--;
                }
            }
        }

        return array('data' => $data, 'status' => 'success','html'=>$html,'subscribed_user'=>$subscribed_user,'trHtml'=>$trHtml,'optional_due_date'=>$optional_due_date);

    }
    public function getIntouchModule(){
        $id = (isset($_POST['tid']) && !empty($_POST['tid'])) ? $_POST['tid'] : null;

        if($_POST['category'] == 'Property') {
            $sql = "SELECT id,property_name FROM general_property WHERE id='" . $id . "'";
            $res = $this->companyConnection->query($sql)->fetch();
        }else if($_POST['category'] == 'Person'){
            $sql = "SELECT id,name FROM users WHERE id='" . $id . "'";
            $res = $this->companyConnection->query($sql)->fetch();
        }
        else if($_POST['category'] == 'WorkOrder'){
            $sql = "SELECT id,work_order_number FROM work_order WHERE id='" . $id . "'";
            $res = $this->companyConnection->query($sql)->fetch();
        }
        else if($_POST['category'] == 'Building'){
            $sql = "SELECT id,building_name FROM building_detail WHERE id='" . $id . "'";
            $res = $this->companyConnection->query($sql)->fetch();
        }
        else if($_POST['category'] == 'Unit'){
            $sql = "SELECT id,floor_no FROM unit_details WHERE 	property_id='" . $id . "'";
            $res = $this->companyConnection->query($sql)->fetch();
        }
        else if($_POST['category'] == 'Group'){
            $sql = "SELECT id,group_name FROM company_property_groups WHERE id='" . $id . "'";
            $res = $this->companyConnection->query($sql)->fetch();
        }
        else if($_POST['category'] == 'Portfolio'){
            $sql = "SELECT id,portfolio_name FROM company_property_portfolio WHERE id='" . $id . "'";
            $res = $this->companyConnection->query($sql)->fetch();
        }

        return array('res' => $res, 'status' => 'success');
    }

}
$InTouch = new InTouch();