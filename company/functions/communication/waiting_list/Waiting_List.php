<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class waitinglist extends DBConnection {


    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    public function getAllProperties(){
        try {

            $html = "";
            $query ="SELECT * FROM general_property ORDER BY property_name ASC";
            $datas = $this->companyConnection->query($query)->fetchAll();
            $html .="<option value=''>Select</option>";
            foreach ($datas as $data) {
                $html .= "<option value=" . $data['id'] . ">" . $data['property_name'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html' => $html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }

    }


    public function getAllBuildings(){
        try {
            $id=$_POST['id'];
            $html1 = "";
            $html = "";
            $building ="SELECT * FROM building_detail WHERE property_id = $id";
            $databuilding = $this->companyConnection->query($building)->fetchAll();
            $html1 .="<option value=''>Select</option>";
            foreach ($databuilding as $databuildingSingle) {
                $html1 .= "<option value=" . $databuildingSingle['id'] . ">" . $databuildingSingle['building_name'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html1' => $html1,'html' => $html,);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
 public function getAllUnits(){
        try {
            $id=$_POST['id'];
            $html1 = "";
            $html = "";

            $unit ="SELECT * FROM unit_details WHERE building_id = $id";
            $dataunits = $this->companyConnection->query($unit)->fetchAll();

            $html .="<option value=''>Select</option>";
            foreach ($dataunits as $dataunit) {
                $html .= "<option value=" . $dataunit['id'] . ">" .$dataunit['unit_prefix']."-". $dataunit['unit_no'] . "</option>";
            }


            return array('code' => 200, 'status' => 'success', 'html1' => $html1,'html' => $html,);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }



    public function newWaitinglist(){
        try {

            $formData=$_POST['formData'];
            $formData=postArray($formData);
            $data['name'] = $formData['first_name'].' '.$formData['last_name'] ;
            $data['first_name'] = $formData['first_name'] ;
            $data['company_name'] = $formData['company_name'] ;
            $data['middle_name'] =(isset($formData['middle_name']) && !empty($formData['middle_name']) ? $formData['middle_name'] : null);
            $data['last_name'] = $formData['last_name'] ;
            $data1['pet_id'] =(isset($formData['pets']) && !empty($formData['pets']) ? $formData['pets'] : null);
            $data['maiden_name'] =(isset($formData['maiden_name']) && !empty($formData['maiden_name']) ? $formData['maiden_name'] : null);
            $data['nick_name'] =(isset($formData['nickname']) && !empty($formData['nickname']) ? $formData['nickname'] : null);
            $data['phone_number'] =(isset($formData['phone']) && !empty($formData['phone']) ? $formData['phone'] : null);
            $data['email'] =(isset($formData['email']) && !empty($formData['email']) ? $formData['email'] : null);
            $data['address1'] =(isset($formData['address_1']) && !empty($formData['address_1']) ? $formData['address_1'] : null);
            $data['address2'] =(isset($formData['address_2']) && !empty($formData['address_2']) ? $formData['address_2'] : null);
            $data['address3'] =(isset($formData['address_3']) && !empty($formData['address_3']) ? $formData['address_3'] : null);
            $data['address4'] =(isset($formData['address_4']) && !empty($formData['address_4']) ? $formData['address_4'] : null);
            $data['zipcode'] =(isset($formData['postal_code']) && !empty($formData['postal_code']) ? $formData['postal_code'] : null);
            $data['state'] =(isset($formData['state']) && !empty($formData['state']) ? $formData['state'] : null);
            $data['country'] =(isset($formData['country']) && !empty($formData['country']) ? $formData['country'] : null);
            $data['city'] =(isset($formData['city']) && !empty($formData['city']) ? $formData['city'] : null);
            $data['notes'] =(isset($formData['note']) && !empty($formData['note']) ? $formData['note'] : null);
            $data['user_type'] ='11';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $data1['preferred_id'] = (isset($formData['preferred']) && !empty($formData['preferred']) ? $formData['preferred'] : null);
            $data1['property_id'] = (isset($formData['property']) && !empty($formData['property']) ? $formData['property'] : null);
            $data1['building_id'] = (isset($formData['building']) && !empty($formData['building']) ? $formData['building'] : null);
            $data1['unit_id'] = (isset($formData['unit']) && !empty($formData['unit']) ? $formData['unit'] : null);
            $data1['maiden_name'] =(isset($formData['maiden_name']) && !empty($formData['maiden_name']) ? $formData['maiden_name'] : null);
            $data1['nickname'] =(isset($formData['nickname']) && !empty($formData['nickname']) ? $formData['nickname'] : null);
            $data1['phone'] =(isset($formData['phone']) && !empty($formData['phone']) ? $formData['phone'] : null);
            $data1['email'] =(isset($formData['email']) && !empty($formData['email']) ? $formData['email'] : null);
            $data1['address_1'] =(isset($formData['address_1']) && !empty($formData['address_1']) ? $formData['address_1'] : null);
            $data1['address_2'] =(isset($formData['address_2']) && !empty($formData['address_2']) ? $formData['address_2'] : null);
            $data1['address_3'] =(isset($formData['address_3']) && !empty($formData['address_3']) ? $formData['address_3'] : null);
            $data1['address_4'] =(isset($formData['address_4']) && !empty($formData['address_4']) ? $formData['address_4'] : null);
            $data1['postal_code'] =(isset($formData['postal_code']) && !empty($formData['postal_code']) ? $formData['postal_code'] : null);
            $data1['state'] =(isset($formData['state']) && !empty($formData['state']) ? $formData['state'] : null);
            $data1['country'] =(isset($formData['country']) && !empty($formData['country']) ? $formData['country'] : null);
            $data1['city'] =(isset($formData['city']) && !empty($formData['city']) ? $formData['city'] : null);
            $data1['note'] =(isset($formData['note']) && !empty($formData['note']) ? $formData['note'] : null);
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');


            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO  users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

            $stmt = $this->companyConnection->prepare($query);

            $stmt->execute($data);

            $id = $this->companyConnection->lastInsertId();
            $data1['user_id'] = $id;
            $sqlData1 = createSqlColVal($data1);

            $query1 = "INSERT INTO  waiting_list (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";

            $stmt1 = $this->companyConnection->prepare($query1);

            $stmt1->execute($data1);

            return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function deletelist() {
        try {
            $data=[];

            $id = $_POST['id'];

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$id'";

//                $query = "DELETE FROM users WHERE id=".$data3['user_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }



    public function getlistdata() {

        try{
            $id = $_POST['id'];
            // $query = $this->companyConnection->query("SELECT user_id, unit_id, expected_move_in FROM lease_guest_card where user_id=".$user_id);

            //  $data1 = $query->fetch();

            $query = $this->companyConnection->query("SELECT waiting_list. *,
                                        general_property.id as property,
                                        building_detail.id as building,
                                        unit_details.id as unit
                                                           FROM waiting_list 
                          LEFT JOIN building_detail ON waiting_list.building_id=building_detail.id
                          LEFT JOIN general_property ON waiting_list.property_id=general_property.id
                          LEFT JOIN unit_details ON waiting_list.unit_id=unit_details.id
                            where waiting_list.user_id=".$id);
            $data = $query->fetch();


            $query1 = $this->companyConnection->query("SELECT users. *
                                                                             FROM users 
                                                                where users.id=".$id);
            $data1 = $query1->fetch();

//dd($data);


            return ['status'=>'success','code'=>200, 'data'=> $data,'data1'=>$data1];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function edit_list()

    {
        try {
            $id = $_POST['id'];
            $formData=$_POST['formData'];
            $formData=postArray($formData);
            $data['name'] = $formData['first_name'].' '.$formData['last_name'] ;
            $data['first_name'] = $formData['first_name'] ;
            $data['company_name'] = $formData['company_name'] ;
            $data['middle_name'] =(isset($formData['middle_name']) && !empty($formData['middle_name']) ? $formData['middle_name'] : null);
            $data['last_name'] = $formData['last_name'] ;
            $data1['pet_id'] =(isset($formData['pets']) && !empty($formData['pets']) ? $formData['pets'] : null);
            $data['maiden_name'] =(isset($formData['maiden_name']) && !empty($formData['maiden_name']) ? $formData['maiden_name'] : null);
            $data['nick_name'] =(isset($formData['nickname']) && !empty($formData['nickname']) ? $formData['nickname'] : null);
            $data['phone_number'] =(isset($formData['phone']) && !empty($formData['phone']) ? $formData['phone'] : null);
            $data['email'] =(isset($formData['email']) && !empty($formData['email']) ? $formData['email'] : null);
            $data['address1'] =(isset($formData['address_1']) && !empty($formData['address_1']) ? $formData['address_1'] : null);
            $data['address2'] =(isset($formData['address_2']) && !empty($formData['address_2']) ? $formData['address_2'] : null);
            $data['address3'] =(isset($formData['address_3']) && !empty($formData['address_3']) ? $formData['address_3'] : null);
            $data['address4'] =(isset($formData['address_4']) && !empty($formData['address_4']) ? $formData['address_4'] : null);
            $data['zipcode'] =(isset($formData['postal_code']) && !empty($formData['postal_code']) ? $formData['postal_code'] : null);
            $data['state'] =(isset($formData['state']) && !empty($formData['state']) ? $formData['state'] : null);
            $data['country'] =(isset($formData['country']) && !empty($formData['country']) ? $formData['country'] : null);
            $data['city'] =(isset($formData['city']) && !empty($formData['city']) ? $formData['city'] : null);
            $data['notes'] =(isset($formData['note']) && !empty($formData['note']) ? $formData['note'] : null);
            $data['user_type'] ='11';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $data1['preferred_id'] = (isset($formData['preferred']) && !empty($formData['preferred']) ? $formData['preferred'] : null);
            if(isset($formData['property']) && !empty($formData['property']) && $formData['building']!='null') {
                $data1['property_id'] = (isset($formData['property']) && !empty($formData['property']) ? $formData['property'] : null);
            }
            if(isset($formData['building']) && !empty($formData['building']) && $formData['building']!='null') {
                $data1['building_id'] = (isset($formData['building']) && !empty($formData['building']) ? $formData['building'] : null);
            }
            if(isset($formData['unit']) && !empty($formData['unit']) && $formData['building']!='null') {
                $data1['unit_id'] = (isset($formData['unit']) && !empty($formData['unit']) ? $formData['unit'] : null);
            }
            $data1['maiden_name'] =(isset($formData['maiden_name']) && !empty($formData['maiden_name']) ? $formData['maiden_name'] : null);
            $data1['nickname'] =(isset($formData['nickname']) && !empty($formData['nickname']) ? $formData['nickname'] : null);
            $data1['phone'] =(isset($formData['phone']) && !empty($formData['phone']) ? $formData['phone'] : null);
            $data1['email'] =(isset($formData['email']) && !empty($formData['email']) ? $formData['email'] : null);
            $data1['address_1'] =(isset($formData['address_1']) && !empty($formData['address_1']) ? $formData['address_1'] : null);
            $data1['address_2'] =(isset($formData['address_2']) && !empty($formData['address_2']) ? $formData['address_2'] : null);
            $data1['address_3'] =(isset($formData['address_3']) && !empty($formData['address_3']) ? $formData['address_3'] : null);
            $data1['address_4'] =(isset($formData['address_4']) && !empty($formData['address_4']) ? $formData['address_4'] : null);
            $data1['postal_code'] =(isset($formData['postal_code']) && !empty($formData['postal_code']) ? $formData['postal_code'] : null);
            $data1['state'] =(isset($formData['state']) && !empty($formData['state']) ? $formData['state'] : null);
            $data1['country'] =(isset($formData['country']) && !empty($formData['country']) ? $formData['country'] : null);
            $data1['city'] =(isset($formData['city']) && !empty($formData['city']) ? $formData['city'] : null);
            $data1['note'] =(isset($formData['note']) && !empty($formData['note']) ? $formData['note'] : null);
            $data1['created_at'] = date('Y-m-d H:i:s');


            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE waiting_list SET ".$sqlData['columnsValuesPair']." where user_id='$id'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            $sqlData1 = createSqlColValPair($data);
            $query1 = "UPDATE users SET ".$sqlData1['columnsValuesPair']." where id='$id'";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute();

            return ['status' => 'success', 'code' => 200, 'message' => 'Record updated successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function getFlagInfo(){
        try{
            $id = $_POST['id'];
            $sql = "SELECT tp.property_id,gp.owner_id,CONCAT( u.first_name, ' ', u.last_name ) AS fullname,
            u.phone_number,u.mobile_number FROM tenant_property tp JOIN general_property gp ON tp.property_id
             = gp.id JOIN users u ON gp.owner_id = u.id WHERE u.id=$id";
            $data['propid'] = $this->companyConnection->query($sql)->fetch();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
            return array('code' => 200, 'data'=> $data, 'status' => 'success', 'message' => 'Record fetched successfully.');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }
}

$workOrder = new waitinglist();
?>