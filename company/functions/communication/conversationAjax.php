<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class conversationAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    /**
     * Common function to get condition based data on page load
     * @return array
     */
    public function getInitialData(){
        $user_type = $_POST['user_type'];
        if ($user_type == 2){
            $query = "SELECT  * FROM users u JOIN tenant_lease_details ON u.id = tenant_lease_details.user_id JOIN tenant_details ON tenant_details.user_id = u.id WHERE ( u.record_status = '0' AND u.user_type = '2' AND tenant_details.record_status = '1' AND tenant_lease_details.record_status = '1') AND u.deleted_at IS NULL ORDER BY name";
        } else {
            $query = "SELECT * FROM users WHERE user_type='" . $user_type . "' AND status='1' ORDER BY name";
        }
        $html = '<option value="">Select</option>';
        $userData =$this->companyConnection->query($query)->fetchAll();
        foreach ($userData as $key => $value) {
            $html.= '<option value=' . $value['id'] . '>' . $value['name'] . '</option>';
        }
        $totalTenant = $this->companyConnection->query("SELECT count(*) as total_tenant FROM users where user_type='2'  AND status='1'")->fetch();
        $totalOwner  = $this->companyConnection->query("SELECT count(*) as total_owner FROM users where user_type='4'  AND status='1'")->fetch();
        $totalVendor = $this->companyConnection->query("SELECT count(*) as total_vendor FROM users where user_type='3'  AND status='1'")->fetch();


        return array('code' => 200, 'status' => 'success', 'data' => $html, 'totalTenant'=>$totalTenant, 'totalOwner'=>$totalOwner, 'totalVendor'=>$totalVendor);
    }

    /**
     * Get all problem category
     * @return array
     */
    public function getAllProblemCategory(){
        $html = '<option value="">Select</option>';
        $query = "SELECT * FROM conversation_problem_category";
        $userData =$this->companyConnection->query($query)->fetchAll();
        foreach ($userData as $key => $value) {
            $html.= '<option value=' . $value['id'] . '>' . $value['problem_category_name'] . '</option>';
        }

        return array('code' => 200, 'status' => 'success', 'data' => $html);
    }



    /**
     * Create/Update/Copy/Publish an announcement
     * @return array
     * @throws Exception
     */
    public function addConversation() {
        try {
            $active_tab = isset($_POST['active_tab']) ? $_POST['active_tab'] : '';
            $is_portal = isset($_POST['is_portal']) ? $_POST['is_portal'] : '';
            $domainSession = $_SESSION[SESSION_DOMAIN];
            $data = $_POST;

            //Required variable array
            $required_array = ['selected_user_id','problem_category','description'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
//                dd($active_tab);
                if ($active_tab == 'tenant_tab'){
                    $user_type = '1';
                } else if ($active_tab == 'owner_tab'){
                    $user_type = '2';
                } else {
                    $user_type = '3';
                }

                $portal_type = '';
                if (isset($is_portal) && ($is_portal == 'Tenant_Portal')){
                    $portal_type = isset($domainSession['Tenant_Portal']['tenant_portal_id']) && ($domainSession['Tenant_Portal']['tenant_portal_id'] != '') ? $domainSession['Tenant_Portal']['tenant_portal_id'] : '';
                }
                if (isset($is_portal) && ($is_portal == 'Owner_Portal')){
                    $portal_type = isset($domainSession['Owner_Portal']['portal_id']) && ($domainSession['Owner_Portal']['portal_id'] != '') ? $domainSession['Owner_Portal']['portal_id'] :'';
                }
                if (isset($is_portal) && ($is_portal == 'Vendor_Portal')){
                    $portal_type = isset($domainSession['Vendor_Portal']['vendor_portal_id']) && ($domainSession['Vendor_Portal']['vendor_portal_id'] != '') ? $domainSession['Vendor_Portal']['vendor_portal_id'] : '';
                }

                if (isset($portal_type) && !empty($portal_type)){
                    $queryData['user_id'] = isset($data['selected_user_id']) ? $data['selected_user_id'] : '';
                    $queryData['selected_user_id'] = $portal_type;
                } else {
                    $queryData['user_id'] = isset($domainSession['cuser_id']) ? $domainSession['cuser_id'] : '';
                    $queryData['selected_user_id'] = isset($data['selected_user_id']) ? $data['selected_user_id'] : '';
                }

                $queryData['user_type']        = isset($user_type) ? $user_type : '';
                $queryData['is_portal']        = isset($data['is_portal']) ? $data['is_portal'] : '';
                $queryData['problem_category'] = isset($data['problem_category']) ? $data['problem_category'] : '';
                $queryData['description']      = isset($data['description']) ? $data['description'] : '';

                $queryData['created_at']       = date('Y-m-d H:i:s');
                $queryData['updated_at']       = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($queryData);
                $query = "INSERT INTO conversation (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($queryData);

                //Send notification to tenant
                if ($active_tab == 'tenant_tab' && $is_portal == '' && empty($portal_type)){
                    $notificationTitle= "Tenant conversation posted to portal";
                    $module_type = "COMMUNICATION";
                    $alert_type = "Real Time";
                    $descriptionNotifi ='Tenant ID-'.$queryData['selected_user_id'].' has posted a Conversation to the Portal.';
                    insertNotification($this->companyConnection,$queryData['selected_user_id'],$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                }

                if ($active_tab == 'owner_tab' && $is_portal == '' && empty($portal_type)){
                    $data =  $this->companyConnection->query("SELECT name  FROM users where id=".$queryData['selected_user_id'])->fetch();
                    $notificationTitle= "Owner’s conversation postal to portal";
                    $module_type = "COMMUNICATION";
                    $alert_type = "Real Time";
                    $descriptionNotifi ='(Owner ID-'.$queryData['selected_user_id'].', Name -'.$data['name'].' ) has posted a Conversation to the Portal.';
                    insertNotification($this->companyConnection,$queryData['selected_user_id'],$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records created successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * Get announcement by id
     * @return array
     */
    public function getAllConversationByType(){
        try {
            $domainSession = $_SESSION[SESSION_DOMAIN];
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];

            $active_tab = isset($_POST['active_tab']) ? $_POST['active_tab'] : '';
            $conversation_id  = isset($_POST['conversation_id']) ? $_POST['conversation_id'] : '';
            $selected_manager = isset($_POST['selected_manager']) ? $_POST['selected_manager'] : '';
            $selected_user    = isset($_POST['selected_user']) ? $_POST['selected_user'] : '';
            $is_portal        = isset($_POST['is_portal']) ? $_POST['is_portal'] : '';

            $is_admin = "company_user_roles.role_name as login_user_role,";
            if($login_user_id == '1'){
                $is_admin = "Coalesce(company_user_roles.role_name, 'Admin') as login_user_role,";
            }
            if ($active_tab == 'tenant_tab'){
                $user_type = '1';
                $image_table = 'tenant_details';
                $image_column = 'tenant_image';
                $image_user_id = 'user_id';
            } else if ($active_tab == 'owner_tab'){
                $user_type = '2';
                $image_table = 'owner_details';
                $image_column = 'owner_image';
                $image_user_id = 'user_id';
            } else {
                $user_type = '3';
                $image_table = 'vendor_additional_detail';
                $image_column = 'image';
                $image_user_id = 'vendor_id';
            }

            $portal_type_id = '';
            if (isset($is_portal) && ($is_portal == 'Tenant_Portal')){
                $portal_type_id = isset($domainSession['Tenant_Portal']['tenant_portal_id']) && ($domainSession['Tenant_Portal']['tenant_portal_id'] != '') ? $domainSession['Tenant_Portal']['tenant_portal_id'] :'';;
            }
            if (isset($is_portal) && ($is_portal == 'Owner_Portal')){
                $portal_type_id = isset($domainSession['Owner_Portal']['portal_id']) && ($domainSession['Owner_Portal']['portal_id'] != '') ? $domainSession['Owner_Portal']['portal_id'] :'';
            }
            if (isset($is_portal) && ($is_portal == 'Vendor_Portal')){
                $portal_type_id = isset($domainSession['Vendor_Portal']['vendor_portal_id']) && ($domainSession['Vendor_Portal']['vendor_portal_id'] != '') ? $domainSession['Vendor_Portal']['vendor_portal_id'] : '';
            }

            if (!empty($conversation_id)){
                $sql_query = "SELECT conversation. *,
                            $is_admin
                            users.name as login_user_name,
                            users.user_type as main_user_type
                            FROM conversation 
                            LEFT JOIN users ON conversation.user_id=users.id
                            LEFT JOIN company_user_roles ON users.role=company_user_roles.id
                            WHERE conversation.parent_id=".$conversation_id;
            } else {
                $sql_query1 = "SELECT conversation. *,
                            $is_admin
                            users.name as login_user_name,
                            users.user_type as main_user_type,
                            conversation_problem_category.problem_category_name as problem_category_name,
                            user2.name as selected_user_name
                            FROM conversation 
                            LEFT JOIN users ON conversation.user_id=users.id
                            LEFT JOIN users as user2 ON conversation.selected_user_id=user2.id
                            LEFT JOIN company_user_roles ON users.role=company_user_roles.id
                            LEFT JOIN conversation_problem_category ON conversation.problem_category = conversation_problem_category.id
                            WHERE conversation.user_type =".$user_type;
                $sql_query1 = $sql_query1." AND conversation.parent_id IS NULL";
                if ($selected_manager && $selected_user){
                    $sql_query = $sql_query1." AND conversation.user_id =".$selected_manager." AND conversation.selected_user_id =".$selected_user;
                } else if($selected_manager) {
                    $sql_query = $sql_query1." AND conversation.user_id =".$selected_manager;
                } else if($selected_user) {
//                    if ($is_portal && isset($portal_type_id)){
//                        $sql_query = $sql_query1 . " AND conversation.selected_user_id =" . $selected_user." OR conversation.user_id =".$portal_type_id;
//                    } else {
                    $sql_query = $sql_query1 . " AND conversation.selected_user_id =" . $selected_user;
//                    }
                } else{
//                    dd($sql_query1);
                    $sql_query = $sql_query1;
                }
            }

//            if ($login_user_id != '1') {
//                $sql_query = $sql_query." AND conversation.user_id=".$login_user_id;
//            }

            if (isset($conversation_id) && !empty($conversation_id)){
                $sql_query = $sql_query." ORDER BY conversation.created_at ASC";
            } else {
                $sql_query = $sql_query." ORDER BY conversation.created_at DESC";
            }

            $data = $this->companyConnection->query($sql_query)->fetchAll();


            foreach ($data as $key => $val) {
                if ($val['comment_by'] == 'yes'){
                    $login_user = $val['user_id'];
                } else {
                    $login_user = isset($login_user_id) & !empty($login_user_id) ?  $login_user_id: $selected_user;
                }
                $data[$key]['created_date'] =  dateFormatUser($val['created_at'], $login_user, $this->companyConnection);
                $data[$key]['login_user_name'] =  userName($val['user_id'],$this->companyConnection);
                if(!empty($val['selected_user_id'])) {
                    $data[$key]['selected_user_name'] = userName($val['selected_user_id'], $this->companyConnection);
                }

                if (empty($conversation_id)) {
                    $fetch_image_query = "SELECT " . $image_column . " FROM " . $image_table . "  WHERE " . $image_user_id . "=" . $val['selected_user_id'];
                    $data[$key]['selected_user_image'] = $this->companyConnection->query($fetch_image_query)->fetch();

                    if ($val['comment_by'] == 'yes'){
                        $userId = $val['user_id'];
                    } else {
                        $userId = $val['selected_user_id'];
                    }
                    $fetch_user_role = "SELECT company_user_roles.role_name as login_user_role FROM users LEFT JOIN company_user_roles ON users.role=company_user_roles.id WHERE users.id = " . $userId;
                    $selected_user_role = $this->companyConnection->query($fetch_user_role)->fetch();

                    if(empty($selected_user_role) || $val['main_user_type'] == '1'){
                        $main_type_name = 'Admin';
                    } else if(!empty($selected_user_role)) {
                        $main_type_name = $selected_user_role['login_user_role'];
                    } else {
                        $main_type_name = getRole($val['main_user_type']);
                    }
                    if (isset($val['is_portal']) && !empty($val['is_portal'])){
//                        $data[$key]['user_type'] = $main_type_name;
//                        $data[$key]['login_user_role'] = $selected_user_role['login_user_role'];
                    } else {
                        if ($val['comment_by'] == 'yes') {
                            $data[$key]['user_type'] = $selected_user_role['login_user_role'];
                        }
                        $data[$key]['login_user_role'] = $main_type_name;//$this->fetchUserType($val['user_type']);
                    }
                } else {
                    $fetch_user_role = "SELECT company_user_roles.role_name as login_user_role FROM users LEFT JOIN company_user_roles ON users.role=company_user_roles.id WHERE users.id = " . $val['user_id'];
                    $selected_user_role = $this->companyConnection->query($fetch_user_role)->fetch();

                    if(empty($selected_user_role) || $val['main_user_type'] == '1'){
                        $main_type_name = 'Admin';
                    } else if(!empty($selected_user_role['login_user_role'])) {
                        $main_type_name = $selected_user_role['login_user_role'];
                    } else {
                        $main_type_name = getRole($val['main_user_type']);
                    }

                    if ($val['comment_by'] == 'yes'){
                        $data[$key]['login_user_role'] = $main_type_name;
                    }
//                    if ($val['comment_by'] == 'yes'){
//                        $data[$key]['login_user_role'] = $this->fetchUserType($val['user_type']);
//                    }


                    $fetch_image_query = "SELECT " . $image_column . " FROM " . $image_table . "  WHERE " . $image_user_id . "=" . $val['user_id'];
                    $data[$key]['selected_user_image'] = $this->companyConnection->query($fetch_image_query)->fetch();

                }
            }
            return array('status' => 'success', 'data' => $data, 'code' => 200);
        }catch (Exception $exception)
        {
            dd($exception);
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function fetchUserType($user_type) {
        if ($user_type == '1'){
            return 'Tenant';
        } else if ($user_type == '2'){
            return 'Owner';
        } else {
            return 'Vendor';
        }
    }

    public function sendCommentOnConversation() {
        try {
            $active_tab = isset($_POST['active_tab']) ? $_POST['active_tab'] : '';
            $is_portal = isset($_POST['is_portal']) ? $_POST['is_portal'] : '';
            $domainSession = $_SESSION[SESSION_DOMAIN];

            $data = $_POST;

            if ($active_tab == 'tenant_tab'){
                $user_type = '1';
            } else if ($active_tab == 'owner_tab'){
                $user_type = '2';
            } else {
                $user_type = '3';
            }
            $portal_type = '';
            $comment_by_portal = 'yes';
            if (isset($is_portal) && ($is_portal == 'Tenant_Portal')){
                $tenant_portal_id = isset($_POST['tenant_portal_id']) ? $_POST['tenant_portal_id'] : '';
                $portal_type = $tenant_portal_id;
                $comment_by_portal = 'yes';
            }
            if (isset($is_portal) && ($is_portal == 'Owner_Portal')){
                $portal_type = isset($domainSession['Owner_Portal']['portal_id']) && ($domainSession['Owner_Portal']['portal_id'] != '') ? $domainSession['Owner_Portal']['portal_id'] :'';
                $comment_by_portal = 'yes';
            }
            if (isset($is_portal) && ($is_portal == 'Vendor_Portal')){
                $portal_type = isset($domainSession['Vendor_Portal']['vendor_portal_id']) && ($domainSession['Vendor_Portal']['vendor_portal_id'] != '') ? $domainSession['Vendor_Portal']['vendor_portal_id'] : '';
                $comment_by_portal = 'yes';
            }

            if (isset($portal_type) && !empty($portal_type)){
                $queryData['user_id'] = $portal_type;
            } else {
                $queryData['user_id'] = isset($domainSession['cuser_id']) ? $domainSession['cuser_id'] : '';
            }
            $queryData['user_type']       = isset($user_type) ? $user_type : '';
            $queryData['parent_id']       = isset($data['conversation_id']) ? $data['conversation_id'] : '';
            $queryData['description']     = isset($data['comment_text']) ? $data['comment_text'] : '';
            $queryData['comment_by']      = $comment_by_portal;
            $queryData['created_at']      = date('Y-m-d H:i:s');
            $queryData['updated_at']      = date('Y-m-d H:i:s');

            $sqlData = createSqlColVal($queryData);
            $query = "INSERT INTO conversation (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($queryData);

            //Send notification to tenant
            if ($active_tab == 'tenant_tab' && $is_portal == ''){

            }

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records created successfully');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getUserPropertyManagerList(){
        try{
            $id = $_POST['id'];
            $active_tab = isset($_POST['active_tab']) ? $_POST['active_tab'] : '';
            if ($active_tab == 'tenant_tab') {
                $table = 'tenant_property';
                $user_id = 'user_id';
            } else if ($active_tab == 'vendor_tab'){
                $user_id = 'vendor_id';
                $table = 'work_order';
            }else {
                $user_id = 'user_id';
                $table = 'owner_property_owned';
            }
            $query = $this->companyConnection->query("Select property_id FROM ".$table." WHERE ".$user_id." = ".$id);
            $data = $query->fetchAll();
            $html = '<option value="">Select</option>';
            foreach ($data as $key => $val) {
                $query = $this->companyConnection->query("Select manager_id FROM general_property WHERE id = ".$val['property_id']);
                $manager_data = $query->fetch();
                $manager_data = (isset($manager_data['manager_id']) && !empty($manager_data['manager_id'])) ? unserialize($manager_data['manager_id']) : '';
                if (!empty($manager_data)){
                    foreach ($manager_data as $k => $v) {
                        $fullName = userName($v,$this->companyConnection);
                        $html.= '<option value=' . $v . '>' . $fullName . '</option>';
                    }
                }
            }
            return array('code' => 200, 'status' => 'success', 'data' => $html, 'message' => 'Record retrieved successfully.');

        }catch (Exception $exception){
            echo  $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function grtTotalUsersInConversation(){
        try {
            $query1 = 'SELECT DISTINCT user_id, users.user_type FROM conversation LEFT JOIN users on conversation.user_id=users.id WHERE user_id IS NOT NULL';
            $ownerArray1 = $this->companyConnection->query($query1)->fetchAll();

            $ownerArray2 = $this->companyConnection->query('SELECT DISTINCT selected_user_id AS user_id, users.user_type FROM conversation LEFT JOIN users on conversation.selected_user_id=users.id WHERE selected_user_id IS NOT NULL')->fetchAll();

            $combinedArray = array_merge($ownerArray1, $ownerArray2);
            $filteredArray = array_unique($combinedArray, SORT_REGULAR);

            $tenantCount = 0;
            $vendorCount = 0;
            $ownerCount  = 0;
            $data = [];
            $data['tenantCount'] = $tenantCount;
            $data['vendorCount'] = $vendorCount;
            $data['ownerCount'] = $ownerCount;
            foreach ($filteredArray as $key => $value) {

                if($value['user_type'] == '2') {
                    $tenantCount++;
                    $data['tenantCount'] = $tenantCount;
                }
                if($value['user_type'] == '3') {
                    $vendorCount++;
                    $data['vendorCount'] = $vendorCount;
                }
                if($value['user_type'] == '4'){
                    $ownerCount++;
                    $data['ownerCount'] = $ownerCount;
                }

            }
            return array('code' => 200, 'status' => 'success', 'data' => $data);
        } catch (Exception $exception){
            echo  $exception->getMessage();
            printErrorLog($exception->getMessage());
        }

    }

}
$conversationAjax = new conversationAjax();



