<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class announcementAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Get data on add new announcement page load
     * @return array
     */
    public function getInitialData(){
        try{
            $data = [];
            $data['propertyList'] = $this->companyConnection->query("SELECT id,property_id, property_name FROM general_property WHERE status = '1' ORDER BY property_name ASC")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Common function to get data by id
     * @return array
     */
    public function getDataByID(){
        $id = $_POST['id'];
        $prev_id = $_POST['prev_id'];
        $data_type = $_POST['data_type'];
        $userData = '';
        $table = '';
        switch ($data_type) {
            case "property":
                $table = 'building_detail';
                $query = "SELECT id, building_name FROM `$table` WHERE property_id = $id ORDER BY building_name ASC";
                break;
            case "building":
                $table = 'unit_details';
                $query = "SELECT id, unit_prefix, unit_no FROM `$table` WHERE building_id = ".$id ;
                break;
            case "users":
                $table = 'users';
                $sql = "SELECT id, name FROM `$table` WHERE user_type = '" . $id . "' && status = '1' ORDER BY name ASC";
                if ($id == 'all') {
                    if (isset($prev_id) && !empty($prev_id)) {
                        $tenantQuery = "SELECT u.id, u.name FROM users u LEFT JOIN tenant_property t ON u.id=t.user_id WHERE t.property_id=$prev_id ORDER BY u.name ASC";
                        $tenantQueryData = $this->companyConnection->query($tenantQuery)->fetchAll();

                        $ownerQuery = "SELECT  u.id, u.name  FROM users u LEFT JOIN owner_property_owned ON u.id=owner_property_owned.user_id WHERE owner_property_owned.property_id=$prev_id ORDER BY u.name ASC";
                        $ownerQueryData = $this->companyConnection->query($ownerQuery)->fetchAll();

                        $userData = array_merge($ownerQueryData,$tenantQueryData);
                        return array('code' => 200, 'status' => 'success', 'data' => $userData,'message' => 'Data loaded successfully!');
                    } else {
                        $query = "SELECT id, name FROM `$table` WHERE id != '1' && status = '1' ORDER BY name ASC";
                    }
                } else if($id == '2'){
                    if (isset($prev_id) && !empty($prev_id)) {
                        $query = "SELECT u.id, u.name  FROM users u LEFT JOIN tenant_property t ON u.id=t.user_id WHERE t.property_id=$prev_id ORDER BY u.name ASC";
                    } else {
                        $query = $sql;
                    }
                } else if($id == '4'){
                    if (isset($prev_id) && !empty($prev_id)) {
                        $query = "SELECT  u.id, u.name  FROM users u LEFT JOIN owner_property_owned o ON u.id=o.user_id WHERE o.property_id=$prev_id ORDER BY u.name ASC";
                    } else {
                        $query = $sql;
                    }
                } else {
                    $query = $sql;
                }
                break;
            default:
                $table = '';
                $query = '';
                break;
        }

        if (!empty($query)) {
            $userData = $this->companyConnection->query($query)->fetchAll();
        }
        return array('code' => 200, 'status' => 'success', 'data' => $userData,'message' => 'Data loaded successfully!');
    }

    /**
     * Create/Update/Copy/Publish an announcement
     * @return array
     * @throws Exception
     */
    public function addAnnouncement() {
        try {
            $edit_id = isset($_POST['edit_id']) ? $_POST['edit_id'] : '';
            $copy_id = isset($_POST['copy_id']) ? $_POST['copy_id'] : '';
            $publish_id = isset($_POST['publish_id']) ? $_POST['publish_id'] : '';
            $btn_value = isset($_POST['btn_value']) ? $_POST['btn_value'] : '';
            $data = $_POST;
            $timezone_offset =  $_REQUEST["timezone"];
            $timezone_name = timezone_name_from_abbr("", $timezone_offset*60, false);
            date_default_timezone_set($timezone_name);
            //Required variable array
            $required_array = ['announcement_for','user_name[]','title','start_date','end_date','start_time','end_time', 'description'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['end_time'] = date("H:i", strtotime($data['end_time']));

                $start_date =  mySqlDateFormat($data['start_date'], null, $this->companyConnection);
                $start_time =  date("H:i", strtotime($data['start_time']));
                $new_start_date_time  = date('Y-m-d H:i:s', strtotime("$start_date $start_time"));
                $str_start_date_to_time = strtotime($new_start_date_time);
                $current_date_time = $date = strtotime(date('Y-m-d H:i:s'));
                $current_date_time_diff = $str_start_date_to_time - $current_date_time;

                $end_date =  mySqlDateFormat($data['end_date'], null, $this->companyConnection);

                $end_time =  date("H:i", strtotime($data['end_time']));
                $new_end_date_time  = date('Y-m-d H:i:s', strtotime("$end_date $end_time"));
                $str_end_date_to_time = strtotime($new_end_date_time);
                $end_date_time_diff = $str_end_date_to_time - $str_start_date_to_time;
                if ($current_date_time_diff <= 0) {
                    return array('code' => 502, 'status' => 'error', 'message' => 'The time you have selected has past, please schedule a time in the future.');
                }
                if ($end_date_time_diff == 0) {
                    return array('code' => 502, 'status' => 'error', 'message' => 'Start Time and end time cannot be equal.');
                }
                if ($end_date_time_diff < 0) {
                    return array('code' => 502, 'status' => 'error', 'message' => 'End Time cannot be Less than start time.');
                }
                $queryData['announcement_for'] = isset($data['announcement_for']) ? $data['announcement_for'] : '';
                $queryData['property']        = isset($data['property']) ? $data['property'] : '';
                $queryData['building']        = isset($data['building']) ? $data['building'] : '';
                $queryData['unit']            = isset($data['unit']) ? $data['unit'] : '';
                $queryData['user_name']       = serialize($data['user_name']);
                $queryData['title']           = isset($data['title']) ? $data['title'] : '';
                $queryData['start_date']      = mySqlDateFormat($data['start_date'], null, $this->companyConnection);
                $queryData['end_date']        = mySqlDateFormat( $data['end_date'], null, $this->companyConnection);
                $queryData['start_time']      = date("H:i", strtotime($data['start_time']));
                $queryData['end_time']        = date("H:i", strtotime($data['end_time']));
                $queryData['description']     = isset($data['description']) ? $data['description'] : '';
                $queryData['is_file_library'] = isset($_FILES['file_library']) ? '1' : '0';
                $queryData['created_at']      = date('Y-m-d H:i:s');
                $queryData['updated_at']      = date('Y-m-d H:i:s');

                if (isset($edit_id) && !empty($edit_id)) {
                    $last_id = $edit_id;
                    if ($edit_id) {
                        if (isset($_FILES['file_library'])) {
                            $library_data = $this->insertFileLibrary($_FILES['file_library'], $last_id, $edit_id);
                            if($library_data['code'] != '200' || $library_data['status'] != 'success'){
                                return $library_data;
                            }
                        }
                    }



                    $sqlData = createSqlUpdateCase($queryData);
                    $query = "UPDATE announcements SET ".$sqlData['columnsValuesPair']." WHERE id=".$edit_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);

                } else if (isset($publish_id) && !empty($publish_id)) {

                    $last_id = $publish_id;
                    if ($publish_id) {
                        if (isset($_FILES['file_library'])) {
                            $library_data = $this->insertFileLibrary($_FILES['file_library'], $last_id, $publish_id);
                            if($library_data['code'] != '200' || $library_data['status'] != 'success'){
                                return $library_data;
                            }
                        }
                    }

                    $queryData['status']   = (isset($btn_value) && $btn_value == 'save_send_announcement_publish') ? '1' :'2';
                    $sqlData = createSqlUpdateCase($queryData);
                    $query = "UPDATE announcements SET ".$sqlData['columnsValuesPair']." WHERE id=".$publish_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);

                } else {
                    $queryData['status'] = (isset($btn_value) && $btn_value == 'save_announcement') ? '2' :'1';

                    $sqlData = createSqlColVal($queryData);
                    $query = "INSERT INTO announcements (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($queryData);
                    $last_id = $this->companyConnection->lastInsertId();

                    if ($last_id) {
//                        dd($_FILES['file_library']);
                        if ($copy_id) {
                            $this->insertFileLibrary($_FILES['file_library'], $last_id, '', $copy_id);
                        }
                        if (isset($_FILES['file_library'])) {
                            $this->insertFileLibrary($_FILES['file_library'], $last_id);
                        }
                    }
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records created successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * @param $files
     * @param $last_id
     * @param null $edit_id
     * @param null $copy_id
     * @param null $validation_check
     * @return array
     * @throws Exception
     */
    public function insertFileLibrary($files, $last_id,$edit_id = null, $copy_id = null, $validation_check=null){
        /*  dd($files);*/
        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = 'uploads/announcements/' . $_SESSION[SESSION_DOMAIN]['admindb_id'] . '/' . $last_id;
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($files) && !empty($files)) {
            if (isset($copy_id) && !empty($copy_id)){
//                $fileData = getSingleRecord($this->companyConnection, ['column' => 'announcement_id', 'value' => $copy_id],'announcement_file_library');
                $data = $this->companyConnection->query("SELECT * FROM announcement_file_library WHERE announcement_id = " .$copy_id." AND deleted_at IS NULL")->fetchAll();
                $data = $this->companyConnection->query("SELECT * FROM announcement_file_library WHERE announcement_id = " .$copy_id." AND deleted_at IS NULL")->fetchAll();
                foreach ($data as $k => $v) {
                    $queryData['announcement_id'] = isset($last_id) ? $last_id : '';
                    $queryData['type']          = isset($v['type']) ? $v['type'] : '';
                    $queryData['filename']      = isset($v['filename']) ? $v['filename'] : '';
                    $queryData['file_type']     = isset($v['file_type']) ? $v['file_type'] : '';
                    $queryData['file_location'] = isset($v['file_location']) ? $v['file_location'] : '';
                    $queryData['file_extension'] = isset($v['file_extension']) ? $v['file_extension'] : '';
                    $queryData['created_at']      = date('Y-m-d H:i:s');
                    $queryData['updated_at']      = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($queryData);
                    $query = "INSERT INTO announcement_file_library (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($queryData);
                }
            }
            if(!empty($files['name'][0])) {
                $countFiles = count($files['name']);
                if ($countFiles != 0) {
                    $i = 0;
                    if (isset($files)) {
                        foreach ($files['name'] as $key => $filename) {
                            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                            if (isset($filename[$i]) && $filename[$i] != "") {
                                $uniqueName = $files['name'][$key];
                                //   $uniqueName = $randomNumber . $files['name'][$key];
                                $data1['announcement_id'] = $last_id;
                                $data1['filename'] = $uniqueName;
                                $data1['file_location'] = $path . '/' . $uniqueName;

                                $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                                $data1['file_extension'] = $ext;

                                $fileData = [];
                                if (isset($edit_id) && !empty($edit_id)){
                                    $fileData = getSingleWithAndConditionRecord($this->companyConnection, ['column' => 'filename', 'value' => $uniqueName], 'announcement_file_library', ['column' => 'id', 'value' => $edit_id]);
                                }

//                                dd($fileData);
                                if(isset($fileData['code']) && $fileData['code'] == '200'){
                                    return array('code' => 500, 'status' => 'warning', 'message' => 'Document already exists.');
                                }
                                if(!$validation_check) {
                                    $sqlData1 = createSqlColVal($data1);
                                    $query1 = "INSERT INTO announcement_file_library(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
//                                    dd($query1);
                                    $stmt1 = $this->companyConnection->prepare($query1);
                                    $stmt1->execute($data1);
                                    $tmp_name = $files["tmp_name"][$key];
                                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                        //Directory does not exist, so lets create it.
                                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                                    }
                                    move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                                }
                            }
                            $i++;
                        }
                    }
                }
            }
        }
        return array('code' => 200,'status' => 'success', 'message' => 'Record added successfully.', 'table' => 'announcement_file_library');
    }

    /**
     * Get announcement by id
     * @return array
     */
    public function getAnnouncementById(){
        try {
            $announcement_id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM announcements WHERE id = '$announcement_id'");
            $announcementData = $query->fetch();
            if($announcementData) {
                $announcementData['start_date'] = dateFormatUser($announcementData['start_date'], null, $this->companyConnection);
                $announcementData['end_date'] = dateFormatUser($announcementData['end_date'], null, $this->companyConnection);
                $username =  unserialize($announcementData['user_name']);

                if(is_array($username)) {
                    $announcementData['user_name'] = $username;
                } else {
                    $announcementData['user_name'] = [$username];
                }
                return array('status' => 'success', 'data' => $announcementData, 'code' => 200);
            }
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    /**
     * Common function to update status
     * @return array
     */
    public function updateStatus(){
        try {
            $id = $_POST['id'];
            $status_type = $_POST['status_type'];
            $message = '';
            switch ($status_type) {
                case "delete_announcement":
                    $data['deleted_at'] = date('Y-m-d H:i:s') ;
                    $sqlData = createSqlColValPair($data);
                    $query1  = "UPDATE announcements SET ".$sqlData['columnsValuesPair']." WHERE id =".$id;
                    $message  = 'Record deleted successfully!';
                    break;
                case "delete_file_by_id":
                    $data['deleted_at'] = date('Y-m-d H:i:s') ;
                    $sqlData = createSqlColValPair($data);
                    $query1  = "UPDATE announcement_file_library SET ".$sqlData['columnsValuesPair']." WHERE id =".$id;
                    $message  = 'Record deleted successfully!';
                    break;
                default:
                    $query = '';
                    $query1 = '';
                    $message = 'Something went wrong!';
                    break;
            }

            if (!empty($query)) {
                $data = $this->companyConnection->query($query)->fetchAll();
            }
            if (!empty($query1)) {
                $stmt1 = $this->companyConnection->prepare($query1);
                $data = $stmt1->execute();
            }
            return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => $message);
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    /**
     * Update announcement status to the past .i.e inactive on every 30 seconds
     * @return array
     */
    public function checkAnnouncementStatus(){
        try{
            $timeZone = convertTimeZoneDefaultSeting('', date('Y-m-d H:i:s'), $this->companyConnection);
            $s = strtotime($timeZone);
            $date = date('Y-m-d', $s);
            $time = date('H:i:s', $s);

            $query = "UPDATE announcements SET status='0' WHERE status = '1' AND DATE(end_date)<'" . $date ."' OR TIME(end_time) < '" . $time."'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            exit;

        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Show announcement with respect to portal
     * @return array
     */
    public function showCompanyAnnouncement(){
        try{
            if (isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'])){
                $user_id = $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];
            } else if (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['portal_id'])){
                $user_id = $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['portal_id'];
            } else {
                $user_id = $_SESSION[SESSION_DOMAIN]['Tenant_Portal']['portal_id'];
            }
            if(isset($user_id)) {
                $announcement_ids = [ ];
//                if ($_SESSION[SESSION_DOMAIN]['skip_announcement_ids']) {
//                    $announcement_ids = $_SESSION[SESSION_DOMAIN]['skip_announcement_ids'];
//                }
                $announcement_ids = $_REQUEST['skip_announcement_ids'];
                $data = json_decode($announcement_ids, TRUE);
                $whereNotIn = '';
                if (!empty($data)) {
                    $skip_announcement_ids = implode(",", $data);
                    $whereNotIn = "AND id NOT IN ($skip_announcement_ids)";
                }
                $timeZone = convertTimeZoneDefaultSeting('', date('Y-m-d H:i:s'), $this->companyConnection);
                $s = strtotime($timeZone);
                $date = date('Y-m-d', $s);
                $time = date('H:i:s', $s);
                $query = "SELECT * FROM announcements  WHERE DATE(start_date) ='" . $date . "' AND DATE(end_date)='" . $date . "' OR TIME(start_time) <= '" . $time . "' AND TIME(end_time) >= '" . $time . "' AND status = '1' $whereNotIn";
                $liveAnnouncementData = $this->companyConnection->query($query)->fetchAll();
                //$user_id = !empty($_SESSION[SESSION_DOMAIN]['cuser_id']) ? $_SESSION[SESSION_DOMAIN]['cuser_id'] : null;
                if (count($liveAnnouncementData) != 0) {
                    foreach ($liveAnnouncementData as $key => $value) {
                        $announcement_id= $value["id"];
                        $user_name = unserialize($value["user_name"]);
                        $is_file_library = $value["is_file_library"];

                        if (in_array($user_id, $user_name)) {

                            $query1 = "SELECT * FROM announcements_user_status  WHERE user_id='" . $user_id . "' AND announcement_id = '" . $announcement_id . "'";
                            $user = $this->companyConnection->query($query1)->fetch();

                            if ($user != null) {
                                $liveAnnouncementData[$key]['seenStatus'] = 'seen';
                                $_SESSION[SESSION_DOMAIN]['skip_announcement_ids'] = $announcement_id;
                            } else {

                                if ($is_file_library == '1'){
                                    $queryData = "SELECT * FROM announcement_file_library  WHERE announcement_id = '" . $announcement_id . "'";
                                    $announcement_file_library = $this->companyConnection->query($queryData)->fetchAll();
                                    if (!empty($announcement_file_library)){
                                        $liveAnnouncementData[$key]['file_library'] = $announcement_file_library;
                                    }
                                }
                                $liveAnnouncementData[$key]['seenStatus'] = 'unseen';
                            }
                        } else {
                            return ['status'=>'success','code'=>503, 'message' => 'No record found!!!'];
                        }
                    }
                    return ['status'=>'success','code'=>200, 'message' => 'Record fetched successfully!', 'data' => $liveAnnouncementData ] ;
                }
            }
            return ['status'=>'failed','code'=>503, 'message' => 'No record found!!!'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Update announcement status as it is seen or not seen by the user
     * @return array
     */
    public function updateCompanyAnnouncementStatus(){
        try{
            $company_announcement_id = $_REQUEST['company_announcement_id'];
            if (isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'])){
                $user_id = $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];
            } else if (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['portal_id'])){
                $user_id = $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['portal_id'];
            } else {
                $user_id = $_SESSION[SESSION_DOMAIN]['Tenant_Portal']['portal_id'];
            }

            $data['user_id'] = $user_id;
            $data['announcement_id'] = $company_announcement_id;
            $data['status'] = 1;
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['created_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO announcements_user_status (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return ['status'=>'success','code'=>200,'data'=>$data];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function showAllMasterAnnouncement(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM announcements");
            $announcementData = $query->fetchAll();
            if($announcementData) {
                foreach ($announcementData as $k => $val) {
                    $announcementData[$k]['created_at'] = dateFormatUser($val['created_at'], null, $this->companyConnection);
                }
                return array('status' => 'success', 'data' => $announcementData, 'code' => 200);
            }

            return ['status'=>'failed','code'=>503, 'message' => 'No record found!!!'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }



}
$announcementAjax = new announcementAjax();