<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class  packageTrackerAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getPackageTrackerDetails()
    {
        try{
            $query = $this->companyConnection->query("SELECT * FROM packageTrackerCarrier_table");
            $settings = $query->fetchAll();
            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getLocationStoredDetails()
    {
        try{
            $query = $this->companyConnection->query("SELECT * FROM  locationStored_table ORDER BY locationName ASC ");
            $settings = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getUsersListname(){
        try{
            $html = '<option value="">Select</option>';
            $query = $this->companyConnection->query("Select id from users ORDER BY name ASC");
            $setting = $query->fetchAll();
            foreach ($setting as $d) {
                $fullName = userName($d['id'],$this->companyConnection);
                $html.= '<option value=' . $d['id'] . '>' . $fullName . '</option>';
            }


            $user_name = $_SESSION[SESSION_DOMAIN]['cuser_id'];

            return array('code'=>200,'status'=>'success','data'=>$html,'data1'=>$user_name,'message'=>'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog(($exception->getMessage()));
        }
    }


    public function getUsersname(){
        try{

            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT `packageOwnerName` FROM packageTracker_table WHERE id ='$id'");
            $settings = $query->fetch();
            $user_id = $settings['packageOwnerName'];
            $fullName = userName($user_id,$this->companyConnection);


            return array('code'=>200,'status'=>'success','data'=>$fullName,'message'=>'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog(($exception->getMessage()));
        }
    }

    public function getOwnerTypeListname(){
        try{
          //  print_r($_POST);

            $query1 = $this->companyConnection->query("Select show_last_name from company_preference WHERE id=".$_SESSION[SESSION_DOMAIN]['cuser_id']);
            $status = $query1->fetch();
            //     dd($status);
            if($status['show_last_name']=='0' || empty($status['show_last_name']))
            {
                $query = $this->companyConnection->query("Select id,first_name,name AS  tenantName from users WHERE user_type='2' and record_status = '0' ORDER BY first_name ASC");
                $setting['tenant'] = $query->fetchAll();
                $query = $this->companyConnection->query("Select id,first_name,name AS  ownerName from users WHERE user_type='4' ORDER BY first_name ASC");
                $setting['owner'] = $query->fetchAll();
                $query = $this->companyConnection->query("Select id,first_name,name AS  vendorName from users WHERE user_type='3' ORDER BY first_name ASC");
                $setting['vendor'] = $query->fetchAll();
                $query = $this->companyConnection->query("Select id,first_name,name AS  employeeName from users WHERE user_type='8' ORDER BY first_name ASC");
                $setting['employee'] = $query->fetchAll();
                $query = $this->companyConnection->query("Select id, name AS  otherName from users WHERE user_type NOT IN (2,3,4,8) ");
                $setting['other'] = $query->fetchAll();
            }else if( $status['show_last_name']=="1"){
                $query = $this->companyConnection->query("Select id,last_name,name AS  tenantName from users WHERE user_type='2' and record_status = '0' ORDER BY last_name ASC");
                $setting['tenant'] = $query->fetchAll();
                $query = $this->companyConnection->query("Select id,last_name,name AS  ownerName from users WHERE user_type='4' ORDER BY last_name ASC");
                $setting['owner'] = $query->fetchAll();
                $query = $this->companyConnection->query("Select id,last_name,name AS  vendorName from users WHERE user_type='3' ORDER BY last_name ASC");
                $setting['vendor'] = $query->fetchAll();
                $query = $this->companyConnection->query("Select id,last_name,name AS  employeeName from users WHERE user_type='8' ORDER BY last_name ASC");
                $setting['employee'] = $query->fetchAll();
                $query = $this->companyConnection->query("Select id, name AS  otherName from users WHERE user_type NOT IN (2,3,4,8) ");
                $setting['other'] = $query->fetchAll();
            }
            return array('code'=>200,'status'=>'success','data'=>$setting,'message'=>'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog(($exception->getMessage()));
        }
    }

    public function getOwnerDetails(){
        try{
            $id = $_POST['id'];
            $query = $this->companyConnection->query("Select email,phone_number from users where id ='$id' ");
            $setting = $query->fetch();

            return array('code'=>200,'status'=>'success','data'=>$setting,'message'=>'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog(($exception->getMessage()));
        }
    }

    public function insert(){
        try {
            $update = $_POST["update"];
            $updateId = $_POST["updateId"] ;
            $data1 = $_POST['form'];
            $data2 = postArray($data1);

           /* echo "<pre>";print_r($data2);die;*/
            /*Required variable array*/
            $required_array = ['packageOwnerName','ownerTypeEmail','country_code'];
            /* Max length variable array */
            $maxlength_array = [];
            /*Number variable array*/
            $number_array = ['starting_mileage'];
            /*Server side validation*/
            $err_array = validation($data2,$this->conn,$required_array,$maxlength_array,$number_array);
            /*Checking server side validation*/
            if (isset($data2['pcktrackList']) && $data2['pcktrackList'] !== "5") {
                if (checkValidationArray($err_array)) {
                    return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');

                }
                else {
                    if(!empty($update) && $update == '1'){
                        $data['pcktrackList'] = $data2['pcktrackList'];
                        if (isset($data2['pcktrackList']) && $data2['pcktrackList'] == "5"){
                            $data['othersData'] = $data2['packageAddOwnerName'];
                            $data['packageOwnerName'] = 0;
                        }else{
                            $data['packageOwnerName'] = $data2['packageOwnerName'];
                            $data['othersData'] = "";
                        }
                        $data['ownerTypeEmail'] = $data2['ownerTypeEmail'];
                        $data['country_code'] = $data2['country_code'];
                        $data['ownerType_phone_number'] = $data2['ownerType_phone_number'];
                        $data['parcelNumber'] = $data2['parcelNumber'];
                        $data['quantityOfPackages'] = isset($data2['quantityOfPackages']) ? $data2['quantityOfPackages'] : '';
                        $data['carrierName_id'] = $data2['carrierName_id'];
                        $data['packageSize'] = $data2['packageSize'];
                        $data['receivedByPackage'] = isset($data2['receivedByPackage']) ? $data2['receivedByPackage'] : '';
                        $data['locationName_id'] = $data2['locationName_id'];
                        $data['notesPackages'] = $data2['notesPackages'];
                        $data['trackerDatePackage'] = mySqlDateFormat($data2['trackerDatePackage'], null, $this->companyConnection);
                        //$data['IsPackageDelivered'] = "False";
                        $data['updated_at'] = date('Y-m-d H:i:s');
                        $sqlData = createSqlColValPair($data);
                        $query = "UPDATE packageTracker_table SET " . $sqlData['columnsValuesPair'] . " where id='$updateId'";
                        $stmt = $this->companyConnection->prepare($query);
                        // echo '<pre>';print_r($stmt);die;

                        $stmt->execute();

                        return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record updated successfully');
                    }else{
                        $data['pcktrackList'] = $data2['pcktrackList'];
                        if (isset($data2['pcktrackList']) && $data2['pcktrackList'] == "5"){
                            $data['othersData'] = $data2['packageAddOwnerName'];
                            $data['packageOwnerName'] = 0;
                        }else{
                            $data['packageOwnerName'] = $data2['packageOwnerName'];
                            $data['othersData'] = "";
                        }
                        $data['ownerTypeEmail'] = $data2['ownerTypeEmail'];
                        $data['country_code'] = $data2['country_code'];
                        $data['ownerType_phone_number'] = $data2['ownerType_phone_number'];
                        $data['parcelNumber'] = $data2['parcelNumber'];
                        $data['quantityOfPackages'] = isset($data2['quantityOfPackages']) ? $data2['quantityOfPackages'] : '';
                        $data['carrierName_id'] = $data2['carrierName_id'];
                        $data['packageSize'] = $data2['packageSize'];
                        $data['receivedByPackage'] = isset($data2['receivedByPackage']) ? $data2['receivedByPackage'] : '';
                        $data['locationName_id'] = $data2['locationName_id'];
                        $data['notesPackages'] = $data2['notesPackages'];
                        $data['trackerDatePackage'] = mySqlDateFormat($data2['trackerDatePackage'], null, $this->companyConnection);
                        $data['IsPackageDelivered'] = "False";
                        $data['created_at'] =  date('Y-m-d H:i:s');
                        $data['updated_at'] =  date('Y-m-d H:i:s');

                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO packageTracker_table (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                        $id = $this->companyConnection->lastInsertId();
                        return array('code' => 200, 'status' => 'success', 'data' => $data, 'id'=>$id, 'message' => 'Record created successfully.');
                    }
                }
            }
            else {
                if(!empty($update) && $update == '1'){
                    $data['pcktrackList'] = $data2['pcktrackList'];
                        $data['packageOwnerName'] = 0;
                    $data['othersData'] = $data2['packageAddOwnerName'];
                    $data['ownerTypeEmail'] = $data2['ownerTypeEmail'];
                    $data['country_code'] = $data2['country_code'];
                    $data['ownerType_phone_number'] = $data2['ownerType_phone_number'];
                    $data['parcelNumber'] = $data2['parcelNumber'];
                    $data['quantityOfPackages'] = isset($data2['quantityOfPackages']) ? $data2['quantityOfPackages'] : '';
                    $data['carrierName_id'] = $data2['carrierName_id'];
                    $data['packageSize'] = $data2['packageSize'];
                    $data['receivedByPackage'] = isset($data2['receivedByPackage']) ? $data2['receivedByPackage'] : '';
                    $data['locationName_id'] = $data2['locationName_id'];
                    $data['notesPackages'] = $data2['notesPackages'];
                    $data['trackerDatePackage'] = mySqlDateFormat($data2['trackerDatePackage'], null, $this->companyConnection);
                    //$data['IsPackageDelivered'] = "False";
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE packageTracker_table SET " . $sqlData['columnsValuesPair'] . " where id='$updateId'";
                    $stmt = $this->companyConnection->prepare($query);
                    // echo '<pre>';print_r($stmt);die;

                    $stmt->execute();

                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The Record updated successfully');
                }else{
                    $data['pcktrackList'] = $data2['pcktrackList'];
                    if (isset($data2['pcktrackList']) && $data2['pcktrackList'] == "5"){
                        $data['othersData'] = $data2['packageAddOwnerName'];
                        $data['packageOwnerName'] = 0;
                    }else{
                        $data['packageOwnerName'] = $data2['packageOwnerName'];
                        $data['othersData'] = "";
                    }
                    $data['ownerTypeEmail'] = $data2['ownerTypeEmail'];
                    $data['country_code'] = $data2['country_code'];
                    $data['ownerType_phone_number'] = $data2['ownerType_phone_number'];
                    $data['parcelNumber'] = $data2['parcelNumber'];
                    $data['quantityOfPackages'] = isset($data2['quantityOfPackages']) ? $data2['quantityOfPackages'] : '';
                    $data['carrierName_id'] = $data2['carrierName_id'];
                    $data['packageSize'] = $data2['packageSize'];
                    $data['receivedByPackage'] = isset($data2['receivedByPackage']) ? $data2['receivedByPackage'] : '';
                    $data['locationName_id'] = $data2['locationName_id'];
                    $data['notesPackages'] = $data2['notesPackages'];
                    $data['trackerDatePackage'] = mySqlDateFormat($data2['trackerDatePackage'], null, $this->companyConnection);
                    $data['IsPackageDelivered'] = "False";
                    $data['created_at'] =  date('Y-m-d H:i:s');
                    $data['updated_at'] =  date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO packageTracker_table (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'id'=>$id, 'message' => 'Record created successfully.');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function deletePackage(){
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'packageTracker_table');

            $sql = "UPDATE packageTracker_table SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            echo json_encode(['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.']);
            return;
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function editPackageData(){
        try {
            $id = isset($_POST['id']) ? $_POST['id'] : '';
            $query = $this->companyConnection->query("Select * from packageTracker_table where id ='$id'");
            $setting = $query->fetch();

            return(['status' => 'success', 'code' => 200, 'data'=>$setting, 'message' => 'Record reterived successfully.']);

        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function sendMail()
    {
        try{
            $favorite = $_POST['favorite'];
            $userld = $_POST['id'];
            $server_name = 'https://'.$_SERVER['HTTP_HOST'];
            //$userId = (isset($userId) ? $userId : $_REQUEST['id']);
            $user_details = $this->companyConnection->query("SELECT packageOwnerName, ownerTypeEmail,othersData,ownerType_phone_number FROM packageTracker_table where id='$userld'")->fetch();
            $userId = $user_details['packageOwnerName'];
            $emailAddress = $user_details['ownerTypeEmail'];
            $othersData=$user_details['othersData'];
            if($userId !=='0'){
            $query = $this->companyConnection->query("SELECT CONCAT(first_name,' ',last_name) AS  name, company_name, email,actual_password,phone_number FROM users where id='$userId'");
            $setting = $query->fetch();
            }
            if($userId=="0"){
                $setting['name']=$othersData;
            }
            elseif($userId!=="0"){
                $setting['name']= $setting['name'];
            }
//            ($userId !=='0') ? $setting['name']=$setting['name'] : $setting['name']=$othersData;
//            ($userId !=='0') ? $setting['email']=$setting['email'] : $setting['email']=$emailAddress;
            $phoneText = $user_details['ownerType_phone_number'];
            $stringTextMsg = str_replace("‐","",$phoneText);
            $testExt = "@yopmail.com";
            $sendMsgText = $stringTextMsg.$testExt;
            $urlSite = SITE_URL.'/company/images/logo.png';
            $imgLogo = "<img src=' ".$urlSite."' height='60' width='160'></img>";

         //  dd($_SESSION);
            if($favorite[0] == 'all'){
                $body = getEmailTemplateData($this->companyConnection, 'PakageDeliveredHere_Key');
                $body = str_replace("{username}",$setting['name'],$body);
                $body = str_replace("{Company Name}",$_SESSION[SESSION_DOMAIN]['company_name'].",",$body);
                $body = str_replace("{Companywebsite.com}","",$body);
                $body = str_replace(".","",$body);
                $body = str_replace("{Phone number}",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                $body = str_replace("{email}",$setting['email'],$body);
                $body = str_replace("{password}",$setting['actual_password'],$body);
                $body = str_replace("{CompanyLogo}",$imgLogo,$body);
                $body = str_replace("{website}",$server_name,$body);
                $request['action']  = 'SendMailPhp';
                $request['to']    = [$sendMsgText,$emailAddress];
                $request['subject'] = 'Package Carrier';
                $request['message'] = $body;
                $request['portal']  = '1';
                curlRequest($request);
                return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Text message send successfully'];
            }else if($favorite[0] == '1'){
                $body = getEmailTemplateData($this->companyConnection, 'PakageDeliveredHere_Key');
                $body = str_replace("{username}",$setting['name'],$body);
                $body = str_replace("{Company Name}",$_SESSION[SESSION_DOMAIN]['company_name'].",",$body);
                $body = str_replace("{Companywebsite.com}","",$body);
                $body = str_replace(".","",$body);
                $body = str_replace("{Phone number}",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                $body = str_replace("{email}",$setting['email'],$body);
                $body = str_replace("{password}",$setting['actual_password'],$body);
                $body = str_replace("{CompanyLogo}",$imgLogo,$body);
                $body = str_replace("{website}",$server_name,$body);
                $request['action']  = 'SendMailPhp';
                $request['to[]']    = $sendMsgText;
                $request['subject'] = 'Package Carrier';
                $request['message'] = $body;
                $request['portal']  = '1';
                curlRequest($request);
                return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully'];
            }else if($favorite[0] == '2'){
                $body = getEmailTemplateData($this->companyConnection, 'PakageDeliveredHere_Key');
                $body = str_replace("{username}",$setting['name'],$body);
                $body = str_replace("{Company Name}",$_SESSION[SESSION_DOMAIN]['company_name'].",",$body);
                $body = str_replace("{Companywebsite.com}","",$body);
                $body = str_replace(".","",$body);
                $body = str_replace("{Phone number}",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                $body = str_replace("{email}",$setting['email'],$body);
                $body = str_replace("{password}",$setting['actual_password'],$body);
                $body = str_replace("{CompanyLogo}",$imgLogo,$body);
                $body = str_replace("{website}",$server_name,$body);
                $request['action']  = 'SendMailPhp';
                $request['to[]']    = $emailAddress;
                $request['subject'] = 'Package Carrier';
                $request['message'] = $body;
                $request['portal']  = '1';
                curlRequest($request);
                return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully'];
            }else if ($favorite[0] == '1' && $favorite[1] == '2'){
                $body = getEmailTemplateData($this->companyConnection, 'PakageDeliveredHere_Key');
                $body = str_replace("{username}",$setting['name'],$body);
                $body = str_replace("{Company Name}",$_SESSION[SESSION_DOMAIN]['company_name'].",",$body);
                $body = str_replace("{Companywebsite.com}","",$body);
                $body = str_replace(".","",$body);
                $body = str_replace("{Phone number}",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                $body = str_replace("{email}",$setting['email'],$body);
                $body = str_replace("{password}",$setting['actual_password'],$body);
                $body = str_replace("{CompanyLogo}",$imgLogo,$body);
                $body = str_replace("{website}",$server_name,$body);
                $request['action']  = 'SendMailPhp';
                $request['to']    = [$sendMsgText,$emailAddress];
                $request['subject'] = 'Package Carrier';
                $request['message'] = $body;
                $request['portal']  = '1';
                curlRequest($request);
                return ['status'=>'success','code'=>200,'data'=>$request,'message' => ' send successfully'];
            }

        }catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    function UpdatePickedUp(){
        try {
            $user_id =   $_POST['id'];
            $data11 = $_POST['form'];
            $data1 = postArray($data11);

            $data['pickUpDate'] = (isset($data1['pickUpDatePackage']) && !empty($data1['pickUpDatePackage'])) ? mySqlDateFormat($data1['pickUpDatePackage'], null, $this->companyConnection) : '';
            $data['pickUpTime'] = date("y-m-d H:i:s", strtotime($data1['pickUpTimePackage']));
            //$data['PickedUpBy'] = (isset($data1['pickUpByPackage'] ) && !empty($data1['pickUpByPackage']))? : '';
            $data['PickedUpBy'] = $data['PickedUpBy'] = $data1['pickUpByPackage'];
            $data['notesPickedUp'] = (isset($data1['pickUpNotesPackage']) && !empty($data1['pickUpNotesPackage']))? : '';
            $data['IsPackageDelivered'] = "True";
            $data['updated_at'] =  date('Y-m-d H:i:s');

            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->conn, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'm essage' => 'Validation Errors!');
            } else {
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE packageTracker_table SET " . $sqlData['columnsValuesPair'] . " where id='$user_id'";
                $stmt = $this->companyConnection->prepare($query);

                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record saved successfully');
            }
        }catch(Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

}
$packageTrackerAjax = new packageTrackerAjax();