<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class groupMessageAjax extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    public function getUserList()
    {
        $user_type = $_POST['id'];
//        dd($user_type);
        if($user_type == '2'){
            $getUsers=$this->companyConnection->query("SELECT u.id,u.name FROM users as u join tenant_lease_details as tld on u.id=tld.user_id join tenant_details as td on td.user_id=u.id where u.user_type='2' and u.deleted_at is null and tld.record_status='1' and td.record_status='1'")->fetchAll();
        }else{
            $getUsers = $this->companyConnection->query("SELECT id,name FROM users where user_type='$user_type' and deleted_at is null")->fetchAll();
        }

        return $getUsers;
    }

    public function saveEmail()
    {
        try {
            $response = $_REQUEST;
            $to = [];

           // if (@$response['subrecepient']) {
                if(isset($response['subrecepient'][0]) && $response['subrecepient'][0] == 'All')
                {
                   unset($response['subrecepient'][0]);
                }
                $user_type =  $response['recepient'];

                if($user_type == 'all')
                {
                    $getUsers = $this->companyConnection->query("SELECT id,name,email,carrier,country_code,phone_number FROM users where user_type IN ('2','3','4','5','6','7','8')")->fetchAll();
                } else {
                    if(!isset($response['subrecepient']))
                    {
                        return array('code' => 503, 'status' => 'error', 'data' => '', 'message' => 'Please select atleast one recepient');
                    }
                    $all_mails = implode($response['subrecepient'],',');
                    $getUsers = $this->companyConnection->query("SELECT id,name,email,carrier,country_code,phone_number FROM users where id IN ($all_mails)")->fetchAll();
                }

                if($getUsers)
                {
                    if($response['gtype'] == '2')
                    {
                        $emails = [];
                      //  print_r($getUsers);
                        foreach ($getUsers as $key=>$value)
                        {
                            $email_data[] = '+'.$value['country_code'].$value['phone_number'];
                            $emails[] = $value['country_code'].$value['phone_number'].'@yopmail.com';
                            $name[] = $value['name'];
                        }

                        if(!$emails)
                        {
                           $counting = count($emails);
                           if($counting <= 0)
                           {
                               return array('code' => 503, 'status' => 'error', 'data' => '', 'message' => 'Please select atleast one email address');
                           }
                        }
                        $all_mails = implode($email_data,',');
                      //  print_r($all_mails); die;
                        $data['gtype'] = 'T';
                    } else {
                        $emails = [];
                        foreach ($getUsers as $key=>$value)
                        {
                            $emails[] = $value['email'];
                            $name[] = $value['name'];
                        }
                        $all_mails = implode($emails,',');
                        $data['gtype'] = 'E';
                    }




                }
          //  }
            if(!isset($all_mails))
            {
                return array('code' => 503, 'status' => 'error', 'data' => '', 'message' => 'Please select atleast one recepient');
            }

            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $user_data = getDataById($this->companyConnection, 'users', $user_id);

            $data['email_to'] = $all_mails;

            if($response['gtype'] == '1')
            {
                $data['email_subject'] = $response['subject'];
                $data['email_message'] = $response['mesgbody'];
            }else{
                $data['email_subject'] = '-';
                $data['email_message'] = $response['tmesgbody'];
            }

            $data['user_id']    = $user_id;
            $data['user_type']  = $user_data['data']['user_type'];
            $data['email_from'] = $_SESSION[SESSION_DOMAIN]['email'];
            $data['created_at'] = date('Y-m-d H:i:s');
            //   $to = explode(',',$data['email_to']);

            $edit_id = $response['edit_id'];

            if ($response['mail_type'] == 'send') {
                $request['action'] = 'SendMailPhp';
                $request['to'] = $emails;
                $request['subject'] = (isset($data['email_subject']))?$data['email_subject']:'text message';
                $request['message'] = $data['email_message'];
                $request['portal'] = '1';

                $curl_response = curlRequest($request);
                if (isset($curl_response)) {
                    if (isset($curl_response->error)) {
                        $error = $curl_response->error;
                        if (isset($error->to)) {
                            return array('code' => 503, 'status' => 'failed', 'data' => $data, 'message' => 'Please enter valid email.');
                        }
                        $data['status'] = '0';
                    }
                    if ($curl_response->code == '200') {
                        $data['status'] = '1';
                    } else {
                        $data['status'] = '0';
                    }
                } else {
                    $data['status'] = '0';
                }

            }
            if($name && count($name) > 0)
            {
                $name = implode($name,',');
            }
            $data['type'] = 'G';
            $data['user_name'] = $name;
            $data['selected_user_type'] = $response['recepient'];

            if ($edit_id) {
                $edit_id = $_REQUEST['edit_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE communication_email SET " . $sqlData['columnsValuesPair'] . " where id='$edit_id'";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();


            } else {

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO communication_email (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $lastInsertId = $this->companyConnection->lastInsertId();

            }

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record Saved successfully.');
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function getComposeForView()
    {
        try {
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM communication_email WHERE id=" . $id . " AND deleted_at IS NULL");
            $data = $query->fetch();
            $data['created_at'] = dateFormatUser($data['created_at'], null, $this->companyConnection);
            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function checkUserType()
    {
        try {
            dd('adasda');
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM communication_email WHERE id=" . $id . " AND deleted_at IS NULL");
            $data = $query->fetch();
            $data['created_at'] = dateFormatUser($data['created_at'], null, $this->companyConnection);
            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
}
$groupMessageAjax = new groupMessageAjax();
?>