<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class taskReminderAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getInitialData(){
        try{
            $data = [];
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['propertyList'] = $this->companyConnection->query("SELECT id,property_id, property_name FROM general_property WHERE status = '1' ORDER BY property_name ASC")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getDataByID(){
        $id = $_POST['id'];
        $prev_id = $_POST['prev_id'];
        $data_type = $_POST['data_type'];
        $userData = '';
        $table = '';
        $managers_id = [];
        switch ($data_type) {
            case "property":
                $data1=[];
                $data2=[];

                $table = 'building_detail';
                $query = "SELECT id, building_name FROM `$table` WHERE property_id = ".$prev_id." ORDER BY building_name ASC";
                $sql1 = "SELECT manager_id FROM  general_property WHERE id = '$prev_id'";
                $data1 = $this->companyConnection->query($sql1)->fetch();
                if(!empty($data1["manager_id"])) {
                    $managers_id = unserialize($data1['manager_id']);
                }
                break;
            case "building":
                $table = 'unit_details';
                $query = "SELECT id, unit_prefix, unit_no FROM `$table` WHERE property_id = ".$prev_id." && building_id = ".$id." && building_unit_status = '1'";
                break;
            case "users":
                $table = 'users';
                $sql = "SELECT id, name FROM `$table` WHERE user_type = '" . $id . "' && status = '1'";
                if ($id == 'all') {
                    if (isset($prev_id) && !empty($prev_id)) {
                        $tenantQuery = "SELECT u.id, u.name FROM users u LEFT JOIN tenant_property t ON u.id=t.user_id WHERE t.property_id=$prev_id";
                        $tenantQueryData = $this->companyConnection->query($tenantQuery)->fetchAll();

                        $ownerQuery = "SELECT  u.id, u.name  FROM users u LEFT JOIN owner_property_owned ON u.id=owner_property_owned.user_id WHERE owner_property_owned.property_id=$prev_id";
                        $ownerQueryData = $this->companyConnection->query($ownerQuery)->fetchAll();

                        $userData = array_merge($ownerQueryData,$tenantQueryData);

                        return array('code' => 200, 'status' => 'success', 'data' => $userData,'message' => 'Data loaded successfully!');
                    } else {
                        $query = "SELECT id, name FROM `$table` WHERE id != '1' && status = '1'";
                    }
                } else if($id == '2'){
                    if (isset($prev_id) && !empty($prev_id)) {
                        $query = "SELECT u.id, u.name  FROM users u LEFT JOIN tenant_property t ON u.id=t.user_id WHERE t.property_id=$prev_id";
                    } else {
                        $query = $sql;
                    }
                } else if($id == '4'){
                    if (isset($prev_id) && !empty($prev_id)) {
                        $query = "SELECT  u.id, u.name  FROM users u LEFT JOIN owner_property_owned o ON u.id=o.user_id WHERE o.property_id=$prev_id";
                    } else {
                        $query = $sql;
                    }
                } else {
                    $query = $sql;
                }
                break;
            default:
                $table = '';
                $query = '';
                break;
        }
        if (!empty($query)) {
            $userData = $this->companyConnection->query($query)->fetchAll();
        }
        return array('code' => 200, 'status' => 'success', 'data' => $userData,'property_manager_id'=> $managers_id,'message' => 'Data loaded successfully!');
    }

    /**
     * @return array
     * @throws Exception
     */
    public function addTaskReminder() {
        try {

            $edit_id = isset($_POST['task_reminder_edit_id']) ? $_POST['task_reminder_edit_id'] : '';
            $unit_ids= '';
            $data = $_POST;
            //Required variable array
            $required_array = ['title','property','building','unit','details'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if (isset($edit_id) && !empty($edit_id)) {
                    $due_date =  mySqlDateFormat($data['due_date'], null, $this->companyConnection);
                    $queryData['title'] = isset($data['title']) ? $data['title'] : '';
                    $queryData['property']        = isset($data['property']) ? $data['property'] : '';
                    $queryData['building']        = isset($data['building']) ? $data['building'] : '';
                    $units_data = '';
                    if(!empty($data["units"]) && isset($data["units"])){

                        //$unit_ids = implode(', ', $data["units"]);
                        $queryData['unit_ids']     =  serialize($data["units"]);


                        foreach($data["units"] as $units){
                            $unit_details = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$units], 'unit_details');

                            $units_data .= $unit_details["data"]["unit_prefix"] . ',';
                        }
                        if(!empty($units_data)){
                            $units_data = rtrim($units_data, ',');
                        }
                        $queryData['unit']            = $units_data;
                    }

                    $queryData['due_date']        = $due_date;
                    $queryData['assigned_to']     =  (isset($data['assigned_to']) && !empty($data['assigned_to']))  ? $data['assigned_to']: NULL;
                    $queryData['details']         =  isset($data['details']) ? $data['details'] : '';
                    $queryData['status']          = isset($data['status']) ? $data['status'] : '';
                    $queryData['updated_at']      = date('Y-m-d H:i:s');
                    $sqlData = createSqlUpdateCase($queryData);
                    $query = "UPDATE task_reminders SET ".$sqlData['columnsValuesPair']." WHERE id=".$edit_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                } else {

                  if(!empty($data['unit_ids'])){
                      $unit_ids = serialize(explode(',',$data['unit_ids']));
                  }

                    $due_date =  mySqlDateFormat($data['due_date'], null, $this->companyConnection);
                    $next_date =  mySqlDateFormat($data['next_date'], null, $this->companyConnection);
                    $queryData['title'] = isset($data['title']) ? $data['title'] : '';
                    $queryData['property']        = isset($data['property']) ? $data['property'] : '';
                    $queryData['building']        = isset($data['building']) ? $data['building'] : '';
                    $queryData['unit']            = isset($data['unit']) ? $data['unit']: '';
                    $queryData['due_date']        =   $due_date;
                    $queryData['assigned_to']     =  (isset($data['assigned_to']) && !empty($data['assigned_to'])) ? $data['assigned_to']: NULL;
                    //$queryData['unit_ids']     =  isset($data['unit_ids']) ? $data['unit_ids']: '';
                    $queryData['unit_ids']     =  $unit_ids;
                    $queryData['details']     = isset($data['details']) ? $data['details'] : '';
                    $queryData['status']          = isset($data['status']) ? $data['status'] : '';
                    $queryData['created_at']      = date('Y-m-d H:i:s');
                    $queryData['updated_at']      = date('Y-m-d H:i:s');
                    $queryData['user_id']      =  $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    if(isset($data["reccuring_task_checkbox"]) && !empty($data["reccuring_task_checkbox"])){
                        $queryData['recurring_task']        = isset($data['reccuring_task_checkbox']) ? 1 : '';
                        $queryData['frequency']        = isset($data['frequency']) ? $data['frequency'] : '';
                        $queryData['next_date']        = $next_date;
                    }
                    $sqlData = createSqlColVal($queryData);
                    $query = "INSERT INTO task_reminders (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($queryData);
                    $last_id = $this->companyConnection->lastInsertId();
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records created successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getAnnouncementById(){
        try {
            $announcement_id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM announcements WHERE id = '$announcement_id' AND status = '1' ");
            $announcementData = $query->fetch();
            if($announcementData) {
                $announcementData['start_date'] = dateFormatUser($announcementData['start_date'], null, $this->companyConnection);
                $announcementData['end_date'] = dateFormatUser($announcementData['end_date'], null, $this->companyConnection);
                $username =  unserialize($announcementData['user_name']);

                if(is_array($username)) {
                    $announcementData['user_name'] = $username;
                } else {
                    $announcementData['user_name'] = [$username];
                }
                return array('status' => 'success', 'data' => $announcementData, 'code' => 200);
            }
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    public function updateStatus(){
        try {
            $id = $_POST['id'];
            $status_type = $_POST['status_type'];
            $table = '';
            $message = '';
            switch ($status_type) {
                case "delete_announcement":
                    $data['deleted_at'] = date('Y-m-d H:i:s') ;
                    $sqlData = createSqlColValPair($data);
                    $query1  = "UPDATE announcements SET ".$sqlData['columnsValuesPair']." WHERE id =".$id;
                    $message  = 'Record deleted successfully!';
                    break;
                case "building":
                    $table = 'unit_details';
//                  $query = "SELECT id, unit_prefix, unit_no FROM `$table` WHERE property_id = ".$prev_id." && building_id = ".$id." && building_unit_status = '1'";
                    break;
                default:
                    $query = '';
                    $query1 = '';
                    $message = 'Something went wrong!';
                    break;
            }

            if (!empty($query)) {
                $data = $this->companyConnection->query($query)->fetchAll();
            }
            if (!empty($query1)) {
                $stmt1 = $this->companyConnection->prepare($query1);
                $data = $stmt1->execute();
            }
            return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => $message);
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /*
     * function for fetch property manageers and admin
     * */
    public function fetchPropertymanagers() {
        $html = '';
        $sql = "SELECT * FROM  users WHERE user_type = '1' AND deleted_at IS NULL order by first_name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html.= "<option value=''>Select</option>";
        foreach ($data as $d) {
            $fullname = ucfirst($d['first_name']) . ' ' . ucfirst($d['last_name']);
            $html.= '<option value=' . $d['id'] . '>' . $fullname . '</option>';
        }
        return array('data' => $html, 'status' => 'success');
    }

    /*
 * function to get task and reminder by id
 */

    public function getTaskReminderById(){
        $data = $_POST;
        try{
                $taskReminder = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$data['id']], 'task_reminders');
            $taskReminder["data"]["due_date"] =  (isset($taskReminder["data"]["due_date"]) && !empty($taskReminder["data"]["due_date"]))? dateFormatUser($taskReminder["data"]["due_date"],null,$this->companyConnection):'';
            $taskReminder["data"]["unit_ids"] =  (isset($taskReminder["data"]["unit_ids"]) && !empty($taskReminder["data"]["unit_ids"]))? unserialize($taskReminder["data"]["unit_ids"]):[];
            return array('status' => 'success', 'code'=>'200',  'message' => 'Data fetched successfully.','data'=>$taskReminder);
        }catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'emergency_details');
            printErrorLog($e->getMessage());
        }
    }

    /*
* function to delete task and reminder by id
*/
    public function deleteTaskReminder() {
        try {
            $data=[];
            $task_id = $_POST['task_id'];

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                //$query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$tenant_id'";
                $query = "DELETE FROM task_reminders WHERE id='$task_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
}



$taskReminderAjax = new taskReminderAjax();