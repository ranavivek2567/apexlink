<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class WatingList extends DBConnection {

    /**
     * Flag constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to create Wating List
     */
    public function createWatingList(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = ['first_name','last_name'];
            //Max length variable array
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array,$data['id'],'property_wating_list');
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $wating_id = !empty($data['id'])?$data['id']:null;
                $data['use_entity_name'] = (isset($data['use_entity_name']) && $data['use_entity_name']== 'on')?1:0;
                unset($data['id']);
                if(empty($wating_id))
                {
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    //Save Data in Company Database
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO property_wating_list (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($data);
                    $message = 'Record added successfully.';
                } else {
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE property_wating_list SET ".$sqlData['columnsValuesPair']." WHERE id=".$wating_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($sqlData['data']);
                    $message = 'Record updated successfully.';
                }
                if($exe){
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => $message);
                } else {
                    return array('code' => 500, 'status' => 'error', 'message' => 'Internal Server Error!');
                }

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * fuinction to retrive flag data
     * @return array
     */
    public function getWatingList(){
        try {
           $id = $_POST['id'];
            $sql = "SELECT * FROM property_wating_list WHERE id=".$id." AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if(!empty($data) && count($data) > 0){
                $watingList_data = array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record retrieved successfully');
            } else {
                $watingList_data = array('code' => 400, 'status' => 'error','message' => 'No Record Found!');
            }
            return $watingList_data;
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * fuinction to soft delete flag data
     * @return array
     */
    public function deleteWatingList(){
        try {
            $id = $_POST['id'];
            $data = [];
            $data['deleted_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE property_wating_list SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $exe = $stmt->execute($sqlData['data']);
            if($exe){
                $watingList_data = array('code' => 200, 'status' => 'success','message' => 'Record Deleted successfully.');
            } else {
                $watingList_data = array('code' => 500, 'status' => 'error','message' => 'Internal Server Error!');
            }
            return $watingList_data;
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }
}

$WatingList = new WatingList();
?>