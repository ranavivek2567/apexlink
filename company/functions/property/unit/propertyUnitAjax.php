 <?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class PropertyUnitAjax extends DBConnection{
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function fetchPropertyDetail(){

        try{
            $data = [];

            $id = $_POST['id'];
            //Property
            $property = "SELECT * FROM general_property WHERE id=" . $id;
            $property = $this->companyConnection->query($property)->fetch();

            $propertyAll = "SELECT * FROM general_property";
            $propertyAll = $this->companyConnection->query($propertyAll)->fetchAll();

            //Building
            $building = "SELECT count(*) as building_count FROM building_detail WHERE property_id=" . $id;
            $building = $this->companyConnection->query($building)->fetch();

            $buildingAll = "SELECT * FROM building_detail WHERE property_id=" . $id;
            $buildingAll = $this->companyConnection->query($buildingAll)->fetchAll();

            $countries = "SELECT * FROM countries";
            $countries = $this->companyConnection->query($countries)->fetchAll();

            //For checking building can add or not
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);

            $user_id = $_SESSION['SESSION_'.$subdomain];

            $default_settings = "SELECT * FROM default_settings WHERE user_id=" . $user_id['cuser_id'];
            $default_settings = $this->companyConnection->query($default_settings)->fetch();

            $data['building_disabled'] = (int)@$property['no_of_buildings'] - (int)@$building['building_count'];
            //$data['building_disabled'] = 0;
            $data['default_rent'] = $default_settings['default_rent'];
            $data['propertyAll'] = $propertyAll;
            $data['buildingAll'] = $buildingAll;
            $data['property_id'] = $property['property_id'];
            $data['countries'] = $countries;
            $data['user_name'] = $user_id['name'];

            //For Default Unit Type Add
            $default_unit_type = "SELECT * FROM company_unit_type WHERE user_id=" . $user_id['cuser_id'] . " and is_default='1'";
            $data['default_unit_type'] = $this->companyConnection->query($default_unit_type)->fetch();

            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    public function insertPropertyUnit()
    {
        try {

            $files = $_FILES;

            $data                       = $_POST['form'];
            $data                       = serializeToPostArray($data);

            $data                       = postArray($data);
            $data['market_rent']        = $_POST['market_rent'];
            $data['base_rent']          = $_POST['baseRent'];
            $data['security_deposit']   = $_POST['securityDeposit'];
            if(isset($_POST['square_ft']))
            {
                $data['square_ft']          =  null;
            }

            $array_data                 = [];
            $arr_length                 =  (isset($data['building_id']) && !empty($data['building_id'])?sizeof($data['building_id']):'');
            $arr_Plength                =  (isset($data['phone_number']) && !empty($data['phone_number'])?sizeof($data['phone_number']):'');
            $arr_Flength                =  (isset($data['fax_number']) && !empty($data['fax_number'])?sizeof($data['fax_number']):'');
            if(!isset($data['building_id']) && empty($data['building_id']))
            {
                $data['building_id_0'] = '';
            }
            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];

            /*unset the custom keys */
            $custom_keys=[];
            if (!empty($custom_field)){
                $custom_data = json_decode(stripslashes($custom_field));
                $custom_field = [];
                foreach ($custom_data as $key => $value) {
                    $custom_keys[$key] = $value->name;
                    array_push($custom_field,(array) $value);
                }
            }
            $removeKeys = $custom_keys;

            if (!empty($removeKeys)){
                foreach($removeKeys as $key) {
                    unset($data[$key]);
                }
            }

            foreach ($data AS $key => $value) {
                if (stristr($key, 'photoVideoName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }

            foreach ($data AS $key => $value) {

                if (stristr($key, 'imgName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }

            foreach($data as $akey=>$array_value){
                if(is_array($array_value))
                {
                    foreach($array_value as $k=>$v) {
                        $array_data[$akey.'_'.$k] = $v;
                    }
                }else{
                    $array_data[$akey] = $array_value;
                }
            }
            $required_keys = [];


            for($j=0; $j<$arr_Plength; $j++)
            {
                $required_keys[] = 'phone_number_'.$j;
            }

            for($k=0; $k<$arr_Flength; $k++)
            {
                $required_keys[] = 'fax_number_'.$k;
            }

            if(isset($array_data['unit_as'])&& ($array_data['unit_as'] == 1 || $array_data['unit_as'] == '1'))
            {
                $formrequiredkeys = ['floor_no_0','building_id_0','unit_no','bedrooms_no','unit_type_id','baseRent','market_rent','bathrooms_no','bedrooms_no'];
            } else {
                for($i=0; $i<$arr_length; $i++)
                {
                    $required_keys[] = 'building_id_'.$i;
                    $required_keys[] = 'unit_from_'.$i;
                    $required_keys[] = 'unit_to_'.$i;
                    $required_keys[] = 'floor_no_'.$i;
                }
                $formrequiredkeys = ['unit_no','bedrooms_no','unit_type_id','baseRent','market_rent','bathrooms_no','bedrooms_no'];
            }

            if(isset($data['property_id']))
            {
                $propery_id = @$data['property_id'];
                $query = $this->companyConnection->query("SELECT * FROM general_property where id = '$propery_id'");
                $queryData = $query->fetch();
                if(!$queryData)
                {
                    return array('code' => 400,'status' => 'error', 'data' => 'property_id_error', 'message' => 'Incorrect Property. Please select again.');
                }
            }

            $formrequiredkeys = array_merge($formrequiredkeys,$required_keys);

            $err_array = [];
            $err_array = validation($array_data,$this->companyConnection,$formrequiredkeys);
            // dd($array_data);
            if(!empty($err_array)){
                if($array_data['baseRent'] > $array_data['market_rent'])
                {
                    return array('code' => 400,'status' => 'error', 'data' => 'base_rent_error', 'message' => 'Please fill all required fields!');
                }
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Please fill all required fields!');
            } else {

                if(!empty($custom_field)){
                    foreach ($custom_field as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $unitTblData = $data;
                $unitTblData['unit_type_id'] = (isset($data['unitTypeID']) && $data['unitTypeID'] != "")?$data['unitTypeID']:"";
                $unitTblData['user_id'] =  $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $unitTblData['created_by'] =  $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $unitTblData['amenities'] = isset($data['amenities'])?serialize($data['amenities']):'';
                $unitTblData['custom_field'] = (count($custom_field)>0) ? serialize($custom_field):null;
                $unitTblData['key_access_codes'] = isset($unitTblData['key_access_codes_info'])?serialize($unitTblData['key_access_codes_info']):'';
                $unitTblData['key_access_codes_desc'] = isset($unitTblData['key_access_codes_desc'])?serialize($unitTblData['key_access_codes_desc']):'';


                $last_renovation = (isset($data['last_renovation_date'])?$data['last_renovation_date']:NULL);
                if($last_renovation)
                {
                    $unitTblData['last_renovation'] = mySqlDateFormat($last_renovation,null,$this->companyConnection);
                }

                $last_renovation_time = (isset($data['last_renovation_time'])?$data['last_renovation_time']:NULL);
                if($last_renovation_time)
                {
                    $unitTblData['last_renovation_time'] = mySqlTimeFormat($last_renovation_time,null,$this->companyConnection);
                }


                unset($unitTblData['building_id'],$unitTblData['floor_no'],$unitTblData['unit_prefix'],$unitTblData['unit_from'],$unitTblData['unit_to'],$unitTblData['phone_number'],$unitTblData['fax_number'],$unitTblData['fileshidden'],$unitTblData['unitTypeID'],$unitTblData['baseRent'],$unitTblData['securityDeposit'],$unitTblData['pet_friendly'],$unitTblData['last_renovation_date'],$unitTblData['key_access_codes_info'],$unitTblData['key'],$unitTblData['tag_key_name'],$unitTblData['tag_key_email'],$unitTblData['tag_key_phone'],$unitTblData['tag_key_address1'],$unitTblData['tag_key_address2'],$unitTblData['tag_key_address3'],$unitTblData['tag_key_address4'],$unitTblData['tag_key'],$unitTblData['tag_key_quantity'],$unitTblData['tag_key_pick_date'],$unitTblData['tag_key_pick_time'],$unitTblData['tag_key_return_date'],$unitTblData['tag_key_return_time'],$unitTblData['tag_key_designator'],$unitTblData['key_tag'],$unitTblData['key_desc'],$unitTblData['total_keys'],$unitTblData['tag_key_company'],$unitTblData['unit_no']);


                //Add Key and Tag Key Unit
                $key_data = [];
                $trackkey_data = [];
                if(isset($data['key']) && $data['key'] == 'keyUnit')
                {
                    $key_data['key_tag']        = $data['key_tag'];
                    $key_data['key_desc']       = $data['key_desc'];
                    $key_data['total_keys']     = $data['total_keys'];
                    $unitTblData['key_unit']    = 1;
                } else{
                    $trackkey_data['key_name']      = (isset($data['tag_key_name'])?$data['tag_key_name']:null);
                    $trackkey_data['email']         = (isset($data['tag_key_email'])?$data['tag_key_email']:null);
                    $trackkey_data['company_name']  = (isset($data['tag_key_company'])?$data['tag_key_company']:null);
                    $trackkey_data['phone']         = (isset($data['tag_key_phone'])?$data['tag_key_phone']:null);
                    $trackkey_data['Address1']      = (isset($data['tag_key_address1'])?$data['tag_key_address1']:null);
                    $trackkey_data['Address2']      = (isset($data['tag_key_address2'])?$data['tag_key_address2']:null);
                    $trackkey_data['Address3']      = (isset($data['tag_key_address3'])?$data['tag_key_address3']:null);
                    $trackkey_data['Address4']      = (isset($data['tag_key_address4'])?$data['tag_key_address4']:null);
                    $trackkey_data['key_number']    = (isset($data['tag_key'])?$data['tag_key']:null);
                    $trackkey_data['key_quality']   = (isset($data['tag_key_quantity'])?$data['tag_key_quantity']:null);
                    $trackkey_data['pick_up_date']  = (isset($data['tag_key_pick_date'])?$data['tag_key_pick_date']:null);
                    $trackkey_data['pick_up_time']  = (isset($data['tag_key_pick_time'])?$data['tag_key_pick_time']:null);
                    $trackkey_data['return_date']   = (isset($data['tag_key_return_date'])?$data['tag_key_return_date']:null);
                    $trackkey_data['return_time']   = (isset($data['tag_key_return_time'])?$data['tag_key_return_time']:null);
                    $trackkey_data['key_designator']= (isset($data['tag_key_designator'])?$data['tag_key_designator']:null);
                    $unitTblData['key_unit']        = 2;
                }
                //   dd($trackkey_data);

                /*Add Phone && Fax Number*/
                $phone1 = ''; $phone2 = ''; $phone3='';
                if(isset($data['phone_number']) && count($data['phone_number'])==1){
                    $phone1 = $data['phone_number'][0];
                } elseif(isset($data['phone_number']) && count($data['phone_number'])==2) {
                    $phone1 = $data['phone_number'][0];
                    $phone2 = $data['phone_number'][1];
                }elseif(isset($data['phone_number']) && count($data['phone_number'])==3){
                    $phone1 = $data['phone_number'][0];
                    $phone2 = $data['phone_number'][1];
                    $phone3 = $data['phone_number'][2];
                }

                $unitTblData['phone_number1'] = $phone1;
                $unitTblData['phone_number2'] = $phone2;
                $unitTblData['phone_number3'] = $phone3;


                $fax1 = ''; $fax2 = ''; $fax3 = '';
                if(isset($data['fax_number']) && count($data['fax_number'])==1){
                    $fax1 = $data['fax_number'][0];
                } elseif(isset($data['fax_number']) && count($data['fax_number'])==2) {
                    $fax1 = $data['fax_number'][0];
                    $fax2 = $data['fax_number'][1];
                }elseif(isset($data['fax_number']) && count($data['fax_number'])==3){
                    $fax1 = $data['fax_number'][0];
                    $fax2 = $data['fax_number'][1];
                    $fax3 = $data['fax_number'][2];
                }

                $unitTblData['fax_number1'] = $fax1;
                $unitTblData['fax_number2'] = $fax2;
                $unitTblData['fax_number3'] = $fax3;
                $unitTblData['status'] = 1;


                /*End Phone Number && Fax*/

                //Check Availability
                $unitAs = (isset($array_data['unit_as'])?$array_data['unit_as']:null);
                $unitFrom = (isset($array_data['unit_from'])?$array_data['unit_from']:null);
                $unitFrom = (isset($array_data['unit_to'])?$array_data['unit_to']:null);

                $check_availability = $this->checkAvailability($data['building_id'],$unitAs,$unitFrom,$unitFrom,'internal');

                if($check_availability['code'] == '400')
                {
                    return ['code' => 400, 'status' => 'error','message' => 'Unit Limit Reached', 'data' => 'unit_limit_reached'];
                }

                //End Check Availability
                $last_user_id = '';
//                $updateColumn = createSqlColValPair($custom_data);
//                if (count($custom_field) > 0) {
//                    foreach ($custom_field as $key => $value) {
//                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
//                    }
//                }


                if(isset($array_data['unit_as']) && $array_data['unit_as'] == 1 || isset($array_data['unit_as']) && $array_data['unit_as'] == '1')
                {
                    if (!empty($data['building_id'])){
                        foreach($data['building_id'] as $key=>$value)
                        {
                            $unitTblData['building_id'] = $value;
                            $unitTblData['floor_no']    =  $data['floor_no'][0] ;
                            $unitTblData['unit_prefix'] =  $data['unit_prefix'][0] ;
                            $unitTblData['unit_no'] =  $data['unit_no'][0] ;
                            $unit_check = (isset($data['unit_prefix'][$key]) && !empty($data['unit_prefix'][$key])) ? ($data['unit_prefix'][$key].'-'.$data['unit_no'][0]) : $data['unit_no'][0];
                            $unitTblData['unit_check'] = $unit_check;

                            $sqlData = createSqlColVal($unitTblData);
                            $query = "INSERT INTO unit_details (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                            $stmt = $this->companyConnection->prepare($query);
                            $stmt = $stmt->execute($unitTblData);
                            $lastinsert_id = $this->companyConnection->lastInsertId();
                            $last_insert_id[] = $lastinsert_id;
                            $data = $unitTblData;
                            $data['property_name'] = $queryData['property_name'];
                            $data['city'] = $queryData['city'];
                            $data['state'] = $queryData['state'];
                            $data['address'] = $queryData['address_list'];
                            $data['school_district_municipality'] = $queryData['school_district_municipality'];
                            $data['id'] = $lastinsert_id;
                            $ElasticSearchSave = insertDocument('UNIT','ADD',$data,$this->companyConnection);
                            $renovation_detail = $this->addUnitRenovationDetail($data,$lastinsert_id);
                            $addBuildingLinkedUnit = $this->addBuildingLinkedUnit($unitTblData['building_id'],$lastinsert_id);
                            if(isset($key_data['key_tag']) && !empty($key_data['key_tag']))
                            {
                                $keys_details = $this->addUnitKeys($key_data,$lastinsert_id);
                            }

                            if(isset($trackkey_data['key_name']) && !empty($key_data['key_name']))
                            {
                                $trackkey_details = $this->addTrackKeys($trackkey_data,$lastinsert_id);
                            }
                        }
                    }
                } else {
                    foreach($data['building_id'] as $key=>$value)
                    {
                        $unitTblData['building_id'] = $value;
                        $unitTblData['floor_no']    = (isset($data['floor_no'][$key])?$data['floor_no'][$key]:1);
                        $unitTblData['unit_prefix'] = $data['unit_prefix'][$key];
                        $unit_from  = (isset($data['unit_from'][$key])?$data['unit_from'][$key]:0);
                        $unit_to    = (isset($data['unit_to'][$key])?$data['unit_to'][$key]:0);
                        $unit_count = ((int)$unit_to - (int)$unit_from ) + (int)1;

                        for($k=0; $k<$unit_count; $k++) {
                            $unitNo = (int)$unit_from + $k;
                            $unitTblData['unit_no'] =  $unitNo;
                            $unit_check = (isset($unitTblData['unit_prefix']) && !empty($unitTblData['unit_prefix'])) ? ($unitTblData['unit_prefix'].'-'.$unitNo) : $unitNo;
                            $unitTblData['unit_check'] = $unit_check;
                            $sqlData = createSqlColVal($unitTblData);
                            $query = "INSERT INTO unit_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                            $stmt = $this->companyConnection->prepare($query);
                            $stmt = $stmt->execute($unitTblData);
                            $lastinsert_id = $this->companyConnection->lastInsertId();
                            $data = $unitTblData;
                            $data['property_name'] = $queryData['property_name'];
                            $data['city'] = $queryData['city'];
                            $data['state'] = $queryData['state'];
                            $data['address'] = $queryData['address_list'];
                            $data['school_district_municipality'] = $queryData['school_district_municipality'];
                            $data['id'] = $lastinsert_id;
                            $ElasticSearchSave = insertDocument('UNIT','ADD',$data,$this->companyConnection);
                            $last_insert_id[] = $lastinsert_id;
                            $renovation_detail = $this->addUnitRenovationDetail($data,$lastinsert_id);
                            $addBuildingLinkedUnit = $this->addBuildingLinkedUnit($unitTblData['building_id'],$lastinsert_id);

                            if(isset($key_data['key_tag']))
                            {
                                $keys_details = $this->addUnitKeys($key_data,$lastinsert_id);
                            }

                            if(isset($trackkey_data['key_name']))
                            {
                                $trackkey_details = $this->addTrackKeys($trackkey_data,$lastinsert_id);
                            }

                        }
                    }
                }
                //insert Unit count in general property
                $generalPropertyUnitCount = $this->addUnitCountToProperty($data['property_id']);
                if(isset($last_insert_id) && is_array($last_insert_id) && count($last_insert_id) > 0) {
                    if (!empty($files)) {
                        $domain = getDomain();
                        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');

                        foreach ($files as $key => $value) {
                            $file_name = $value['name'];
                            $fileDataNew = [];
                            $code2 = '';
                            if(is_array($last_insert_id)){
                                foreach ($last_insert_id as $value_id) {
                                    $query = $this->companyConnection->query("SELECT count(*) as unit_counts FROM unit_file_uploads where file_name='$file_name' and unit_id = '$value_id'");
                                    $queryData = $query->fetch();
                                    $unitimage_counts = (int)$queryData['unit_counts'];

                                    if ((int)$unitimage_counts > (int)0) {
                                        return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                                    }
                                }
                            } else {
                                $query = $this->companyConnection->query("SELECT count(*) as unit_counts FROM unit_file_uploads where file_name='$file_name' and unit_id = '$last_insert_id'");
                                $queryData = $query->fetch();
                                $unitimage_counts = (int)$queryData['unit_counts'];

                                if ((int)$unitimage_counts > (int)0) {
                                    return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                                }
                            }


                            $file_tmp = $value['tmp_name'];
                            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                            $name = time() . uniqid(rand());
                            $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            //Check if the directory already exists.
                            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                //Directory does not exist, so lets create it.
                                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                            }
                            move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                            $data = [];

                            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $data['file_name'] = $file_name;
                            $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K') . 'kb';
                            $data['file_location'] = $path . '/' . $name . '.' . $ext;
                            $data['file_extension'] = $ext;
                            $data['codec'] = $value['type'];
                            if (strstr($value['type'], "video/")) {
                                $type = 3;
                            } else if (strstr($value['type'], "image/")) {
                                $type = 1;
                            } else {
                                $type = 2;
                            }
                            $data['file_type'] = $type;
                            $data['created_at'] = date('Y-m-d H:i:s');
                            $data['updated_at'] = date('Y-m-d H:i:s');
                            if(is_array($last_insert_id)){
                                foreach ($last_insert_id as $value_id)
                                {
                                    $data['unit_id'] = $value_id;
                                    $sqlData = createSqlColVal($data);
                                    $query = "INSERT INTO unit_file_uploads (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                    $stmt = $this->companyConnection->prepare($query);
                                    $stmt->execute($data);
                                }
                            } else {
                                $data['unit_id'] = $last_insert_id;
                                $sqlData = createSqlColVal($data);
                                $query = "INSERT INTO unit_file_uploads (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                $stmt = $this->companyConnection->prepare($query);
                                $stmt->execute($data);

                            }


                        }
                    }
                }

                $data['last_insert_id'] = $lastinsert_id;


            }
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record Saved successfully');



        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    private function addBuildingLinkedUnit($buildingId=null, $last_id=null){
        $sql = "SELECT * FROM building_detail  WHERE id=" . $buildingId . "";
        $data = $this->companyConnection->query($sql)->fetch();
        $linked_units = (int)$data['linked_units'] + (int)1;

        $sql = "UPDATE building_detail SET linked_units=? WHERE id=?";
        $stmt= $this->companyConnection->prepare($sql);
        $stmt->execute([$linked_units, $buildingId]);

        //lastbuildingChecker
        $unit_details = "SELECT * FROM unit_details  WHERE id=" . $last_id . "";
        $unit_details = $this->companyConnection->query($unit_details)->fetch();

        $Rsql = "SELECT * FROM building_detail  WHERE id=" . $unit_details['building_id'] . "";
        $Rdata = $this->companyConnection->query($Rsql)->fetch();
        $RlinkedUnits = (int)$Rdata['linked_units'];
        if($RlinkedUnits > 0)
        {
            if($buildingId != $unit_details['building_id'])
            {
                $Rlinked_units = $RlinkedUnits - (int)1;

                $Rsql = "UPDATE building_detail SET linked_units=? WHERE id=?";
                $Rstmt= $this->companyConnection->prepare($Rsql);
                $Rstmt->execute([$Rlinked_units, $unit_details['building_id']]);
            }
        }
    }
    private function addUnitRenovationDetail($data=null, $id=null){

        if(isset($_REQUEST['editRenovation']))
        {
            $data = $_REQUEST['form'];
            $data = postArray($data);
            $id  = $edit_unit_id = $data['edit_unit_id'];
            $data['last_renovation_date'] = $Rdata['last_renovation'] = $data['new_renovation_date'];
            $data['last_renovation_time'] = $Rdata['last_renovation_time']  = $data['new_renovation_time'];
            $data['last_renovation_description'] = $Rdata['last_renovation_description']  = $data['new_renovation_description'];
            unset($data['new_renovation_date'],$data['new_renovation_time'],$data['new_renovation_description']);
        }

        $renovation_data = [];
        $renovation_data["object_id"] = $id;
        $renovation_data["object_type"] = 'unit';
        $renovation_data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $renovation_data['status'] = 1;
        $renovation_data["last_renovation_date"] = (isset($data['last_renovation_date']) && !empty($data['last_renovation_date'])) ? mySqlDateFormat($data['last_renovation_date'],null,$this->companyConnection) : NULL;
        $renovation_data["last_renovation_time"] = (isset($data['last_renovation_time']) && !empty($data['last_renovation_time'])) ? date("H:i", strtotime($data['last_renovation_time'])) : '';
        $renovation_data["last_renovation_description"] = (isset($data['last_renovation_description']) && !empty($data['last_renovation_description'])) ? $data['last_renovation_description'] : '';
        $renovation_data["created_at"] = date('Y-m-d H:i:s');
        $renovation_data["updated_at"] = date('Y-m-d H:i:s');


//        print_r($renovation_data); exit;
        $sqlData = createSqlColVal($renovation_data);
        //Save Data in Company Database

        $query = "INSERT INTO  renovation_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($renovation_data);
        $id = $this->companyConnection->lastInsertId();
        if(isset($_REQUEST['editRenovation']))
        {

            $sqlData = createSqlColValPair($Rdata);
            $Uquery = "UPDATE unit_details SET ".$sqlData['columnsValuesPair']." where id='$edit_unit_id'";

            $stmt = $this->companyConnection->prepare($Uquery);
            $stmt->execute();

            $query = $this->companyConnection->query("SELECT * FROM renovation_details where id='$id'");
            $settings = $query->fetch();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record Saved successfully');
        }
    }

    private function addUnitKeys($data, $id)
    {
        //  dd($data);
        $key_data = [];
        $key_data["unit_id"] = $id;
        $key_data["key_id"] = $data["key_tag"];
        $key_data["key_description"] = $data["key_desc"];
        $key_data["total_keys"] = (isset($data['total_keys']) && !empty($data['total_keys'])) ? $data['total_keys'] : 0;
        $key_data['available_keys'] = (isset($data['total_keys']) && !empty($data['total_keys'])) ? $data['total_keys'] : 0;
        $key_data["created_at"] = date('Y-m-d H:i:s');
        $key_data["updated_at"] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($key_data);
        //Save Data in Company Database

        $query = "INSERT INTO unit_keys (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($key_data);
        $id = $this->companyConnection->lastInsertId();
    }

    private function addTrackKeys($data, $id){
        $key_data = [];
        $key_data["unit_id"] = $id;
        $key_data["key_name"] = $data["key_name"];
        $key_data["email"] = (isset($data['email']) && !empty($data['email'])) ? $data['email'] : '';
        $key_data["company_name"] = (isset($data['company_name']) && !empty($data['company_name'])) ? $data['company_name'] : '';
        $key_data["phone"] = (isset($data['company_name']) && !empty($data['phone'])) ? $data['phone'] : '';
        $key_data["Address1"] = (isset($data['Address1']) && !empty($data['Address1'])) ? $data['Address1'] : '';
        $key_data["Address2"] = (isset($data['Address2']) && !empty($data['Address2'])) ? $data['Address2'] : '';
        $key_data["Address3"] = (isset($data['Address3']) && !empty($data['Address3'])) ? $data['Address3'] : '';
        $key_data["Address4"] = (isset($data['Address4']) && !empty($data['Address4'])) ? $data['Address4'] : '';
        $key_data["key_number"] = (isset($data['key_number']) && !empty($data['key_number'])) ? $data['key_number'] : '';
        $key_data["key_quality"] = (isset($data['key_quality']) && !empty($data['key_quality'])) ? $data['key_quality'] : '';
        $key_data["pick_up_date"] = (isset($data['pick_up_date']) && !empty($data['pick_up_date'])) ?  mySqlDateFormat($data['pick_up_date'],null,$this->companyConnection) : NULL ;
        $key_data["pick_up_time"] = (isset($data['pick_up_time']) && !empty($data['pick_up_time'])) ? mySqlTimeFormat($data['pick_up_time']) : '';
        $key_data["return_date"] = (isset($data['return_date']) && !empty($data['return_date'])) ? mySqlDateFormat($data['return_date'],null,$this->companyConnection) : NULL;
        $key_data["return_time"] = (isset($data['return_time']) && !empty($data['return_time'])) ? mySqlTimeFormat($data['return_time']) : '';
        $key_data["key_designator"] = (isset($data['key_designator']) && !empty($data['key_designator'])) ? $data['key_designator'] : '';
        $key_data["created_at"] = date('Y-m-d H:i:s');
        $key_data["updated_at"] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($key_data);
        //Save Data in Company Database

        $query = "INSERT INTO unit_track_key (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($key_data);
        $id = $this->companyConnection->lastInsertId();
    }

    private function addRenovationDetail($data, $id){
        $renovation_data = [];
        $renovation_data["object_id"] = $id;
        $renovation_data["object_type"] = 'building';
        $renovation_data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $renovation_data['status'] = 1;
        $renovation_data["last_renovation_date"] = (isset($data['last_renovation_date']) && !empty($data['last_renovation_date'])) ? mySqlDateFormat($data['last_renovation_date'],null,$this->companyConnection) : NULL;
        $renovation_data["last_renovation_time"] = (isset($data['last_renovation_time']) && !empty($data['last_renovation_time'])) ? date("H:i", strtotime($data['last_renovation_time'])) : '';
        $renovation_data["last_renovation_description"] = (isset($data['last_renovation_description']) && !empty($data['last_renovation_description'])) ? $data['last_renovation_description'] : '';
        $renovation_data["created_at"] = date('Y-m-d H:i:s');
        $renovation_data["updated_at"] = date('Y-m-d H:i:s');
//        print_r($renovation_data); exit;
        $sqlData = createSqlColVal($renovation_data);
        //Save Data in Company Database

        $query = "INSERT INTO  renovation_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($renovation_data);
        $id = $this->companyConnection->lastInsertId();
    }

    public function viewBuildingDetails(){
        try {
            //$companyData = CompanyId($this->companyConnection);

            //$company_id = $companyData['data'];
            //$user_id = $company_id;
            $building_id = 'KSW4GQ';

            $query = $this->companyConnection->query("SELECT * FROM building_detail where building_id='$building_id'");
            $settings = $query->fetch();
            //$settings["email_signature"] = urldecode($settings["email_signature"]);
            //$data = array();
            //$data['user_id'] =$query->user_id;
            //  echo "<pre>";print_r($settings);die;
            if($settings){
                return array('status' => 'success', 'data' => $settings);
            }else{
                return array('status' => 'error', 'message' => 'No Records Found');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function getpropertyDetails() {
        //    echo "<pre>";print_r($_POST);die;
        $id = $_POST['id'];
        //echo "<pre>";print_r($id);die;
        $sql = "SELECT * FROM general_property WHERE id=" . $id . "";
        $data = $this->companyConnection->query($sql)->fetch();
        $data['amenities'] = unserialize($data['amenities']);
        //echo "<pre>"; print_r($data);die;
        return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
        //return array('zip_code' => $data['zip_code'], 'city' => $data['city'], 'state' => $data['state'], 'country' => $data['country'], 'status' => 'success');
    }

    public function checkAvailability($building_data=null,$unit_as=null,$unit_from=null,$unit_to=null,$checkInternal=null) {
        $all_building_chek = [];

        if($checkInternal != 'internal')
        {
            $data                       = $_POST['form'];
            $data                       = serializeToPostArray($data);
            $data                       = postArray($data);
            $building_data              = $data['building_id'];
            $unit_as                    = $data['unit_as'];
            $unit_from                  = $data['unit_from'];
            $unit_to                    = $data['unit_to'];
        }

        foreach($building_data as $key=>$building_id)
        {
            if(isset($unit_as) && ($unit_as == 1 || $unit_as == '1'))
            {
                /**Single Field Checking**/
                //Building Limit Check
                $query = $this->companyConnection->query("SELECT id,no_of_units,building_id FROM building_detail where id='$building_id'");
                $buildingData = $query->fetch();

                //Unit Limit Check
                $build_id = $buildingData['id'];
                $query1 = $this->companyConnection->query("SELECT count(*) as unit_count FROM unit_details where building_id='$build_id'");
                $UnitData = $query1->fetch();
                $unitCount      = (isset($UnitData['unit_count']))       ? $UnitData['unit_count'] : 0;
                $buildingCount  = (isset($buildingData['no_of_units']))  ? $buildingData['no_of_units'] : 0;

                if($unitCount >= $buildingCount)
                {
                    return ['code' => 400,'status' => 'error', 'message' => 'Unit Limit Reached', 'data' => 'unit_limit_reached'];
                }
            } else {
                /**Group Fields Checking**/
                //Building Limit Check
                $query = $this->companyConnection->query("SELECT id,no_of_units,building_id FROM building_detail where id='$building_id'");
                $buildingData = $query->fetch();

                //Unit Limit Check
                $build_id = $buildingData['id'];
                $query1 = $this->companyConnection->query("SELECT count(*) as unit_count FROM unit_details where building_id='$build_id'");
                $UnitData = $query1->fetch();

                $unitCount      = (isset($UnitData['unit_count']))       ? $UnitData['unit_count'] : 0;
                $buildingCount  = (isset($buildingData['no_of_units']))  ? $buildingData['no_of_units'] : 0;

                if( $unitCount >= $buildingCount)
                {
                    return ['code' => 400,'status' => 'error', 'message' => 'Unit Limit Reached', 'data' => 'unit_limit_reached'];
                } else {
                    $unit_from  = (isset($unit_from[$key])?$unit_from[$key]:0);
                    $unit_to    = (isset($unit_to[$key])?$unit_to[$key]:0);
                    $unit_count = ((int)$unit_to - (int)$unit_from) + (int)1 ;
                    $pending_units = (int)$buildingData['no_of_units'] - (int)$UnitData;

                    if($unit_count >= $pending_units)
                    {
                        return ['code' => 400,'status' => 'error', 'message' => 'Unit Limit Reached', 'data' => 'unit_limit_reached'];
                    }

                    $all_building_chek[$building_id][] = $unit_count;
                }
            }
        }
        if(isset($all_building_chek))
        {
            $all_building_chek1 =  $value = array_sum($all_building_chek);
            $sum = 0;
            foreach($all_building_chek as $key => $values) {
                $sum_building = array_sum($values);
                $query = $this->companyConnection->query("SELECT no_of_units,building_id FROM building_detail where id='$key'");
                $buildingData = $query->fetch();
                $buildingCount  = (isset($buildingData['no_of_units']))  ? $buildingData['no_of_units'] : 0;
                if( $unitCount >= $buildingCount ) {
                    return ['code' => 503, 'message' => 'Unit Limit Reached', 'data' => 'unit_limit_reached'];
                }
            }

        }

        return ['code' => 200,'status'=>'success', 'message' => 'Unit Available'];
    }

    public function fetchUnitDetail()
    {
        try{

            $data = [];

            $id = $_POST['id'];

            //BuildingAll
            $unit_details = "SELECT * FROM unit_details where  id=" . $id;
            $unit_details = $this->companyConnection->query($unit_details)->fetch();

            //unitKeys
            $unit_keys = "SELECT * FROM unit_keys where unit_id=" . $id;
            $unit_keys = $this->companyConnection->query($unit_keys)->fetchAll();

            //unit Track keys
            $unit_track_keys = "SELECT * FROM unit_track_key where unit_id=" . $id;
            $unit_track_keys = $this->companyConnection->query($unit_track_keys)->fetchAll();

            //Property
            $property = "SELECT * FROM general_property WHERE id=" . $unit_details['property_id'];
            $property = $this->companyConnection->query($property)->fetch();

            $propertyAll = "SELECT * FROM general_property";
            $propertyAll = $this->companyConnection->query($propertyAll)->fetchAll();

            //Building
            $building = "SELECT count(*) as building_count FROM building_detail WHERE property_id=" . $unit_details['property_id'];
            $building = $this->companyConnection->query($building)->fetch();

            $buildingAll = "SELECT * FROM building_detail WHERE property_id=" . $unit_details['property_id'];
            $buildingAll = $this->companyConnection->query($buildingAll)->fetchAll();

            $buildingProperty = "SELECT * FROM building_detail";
            $buildingProperty = $this->companyConnection->query($buildingProperty)->fetchAll();


            $keyhtml = '';
            $sql_key = "SELECT * FROM company_key_access";
            $data_key = $this->companyConnection->query($sql_key)->fetchAll();
            $keyhtml.= " <option value=''>Select</option>";
            foreach ($data_key as $d) {
                $keyhtml.= '<option value=' . $d['id'] . '>' . $d['key_code'] . '</option>';
            }

            //For checking building can add or not
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
            $user_id = $_SESSION['SESSION_'.$subdomain];
            $default_settings = "SELECT * FROM default_settings WHERE user_id=" . $user_id['cuser_id'] . "";
            $default_settings = $this->companyConnection->query($default_settings)->fetch();

            $data['building_disabled'] = (int)@$property['no_of_buildings'] - (int)@$building['building_count'];

            $data['unitDetails'] = $unit_details;
            $data['keyhtml'] = $keyhtml;
            $data['unitDetails']['key_access_codes'] = @unserialize($unit_details['key_access_codes']);
            $data['unitDetails']['key_access_codes_desc'] = @unserialize($unit_details['key_access_codes_desc']);
            $data['unitDetails']['custom_field'] = @unserialize($unit_details['custom_field']);
            $data['unitDetails']['amenities'] = @unserialize($unit_details['amenities']);
            $data['unitDetails']['unit_keys'] = @$unit_keys;
            $data['unitDetails']['last_renovation'] = dateFormatUser($unit_details['last_renovation'],null,$this->companyConnection);
            $data['unitDetails']['last_renovation_time'] = timeFormat($unit_details['last_renovation_time'],null,$this->companyConnection);
            $data['unitDetails']['base_rent'] = number_format($data['unitDetails']['base_rent'],2);
            $data['unitDetails']['market_rent'] = number_format($data['unitDetails']['market_rent'],2);
            $data['unitDetails']['security_deposit'] = number_format($data['unitDetails']['security_deposit'],2);

            $data['unitDetails']['unit_track_keys'] = @$unit_track_keys;
            //$data['building_disabled'] = 0;
            $data['default_rent']       = $default_settings['default_rent'];
            $data['propertyAll']        = $propertyAll;
            $data['buildingAll']        = $buildingAll;
            $data['buildingProperty']   = $buildingProperty;
            $data['property_id']        = $property['property_id'];
            $data['property_detail']    = $property;
            //For Default Unit Type Add
            $default_unit_type = "SELECT * FROM company_unit_type WHERE user_id=" . $user_id['cuser_id'] . " and is_default='1'";
            $data['default_unit_type'] = $this->companyConnection->query($default_unit_type)->fetch();

            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Update Unit Status i.e. Activate/Deactivate
     * @return array
     */
    public function updateStatus(){
        try{
            $id = $_POST['id'];
            $status = $_POST['status'];
            $updated_at = date('Y-m-d H:i:s');
            $data = getDataById($this->companyConnection, 'unit_details', $id);
            if(!empty($data['data']) && count($data['data']) > 0){
                $sql = "UPDATE unit_details SET status=?, updated_at=? WHERE id=?";
                $stmt= $this->companyConnection->prepare($sql);
                $stmt->execute([$status, $updated_at, $id]);

                $message='';
                if($status == '1'){
                    $message='Record activated successfully.';
                }
                if($status == '0'){
                    $message ='Record deactivated successfully.';
                }

                if($stmt){
                    return array('code' => 200,'status' => 'success', 'data' => $stmt, 'message' => $message);
                }
            }else{
                return array('code' => 503,'status' => 'error', 'message' => 'No record found.');
            }
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Delete Unit
     * @return array
     */
    public function delete(){
        try{
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE unit_details SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$id]);
            return ['status'=>'success','code'=>200,'message'=>'Record deleted successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function deleteFile(){
        try{
            $file_id= $_REQUEST['id'];
            //   $data = date('Y-m-d H:i:s');
            $data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $file_id], 'unit_file_uploads');

            if(!empty($data["data"])){
                $count=$this->companyConnection->prepare("DELETE FROM unit_file_uploads WHERE id=:id");
                $count->bindParam(":id",$file_id,PDO::PARAM_INT);
                $count->execute();
                return ['status'=>'success','code'=>200,'message'=>'File deleted successfully.'];
            }else{
                return ['status'=>'failed','code'=>503,'message'=>'File not deleted successfully'];
            }

        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getBuildingNameOfUnit(){
        try {
            $query = $this->companyConnection->query("SELECT DISTINCT building_name_and_id FROM unit_details WHERE property_id=".$_POST['id']." AND deleted_at IS NULL");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=>$data, 'message'=>'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function fetchTrackKeyDetails()
    {
        try {
            $query = $this->companyConnection->query("SELECT * FROM unit_track_key WHERE id=".$_POST['id']." AND deleted_at IS NULL");
            $data = $query->fetch();
            $data['pick_up_date'] = dateFormatUser($data['pick_up_date'],null,$this->companyConnection);
            $data['pick_up_time'] = timeFormat($data['pick_up_time'],null,$this->companyConnection);
            $data['return_date'] = dateFormatUser($data['return_date'],null,$this->companyConnection);
            $data['return_time'] = timeFormat($data['return_time'],null,$this->companyConnection);
            return ['status'=>'success','code'=>200, 'data'=>$data, 'message'=>'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function fetchKeyDetails()
    {
        try {
            $query = $this->companyConnection->query("SELECT * FROM unit_keys WHERE id=".$_POST['id']." AND deleted_at IS NULL");
            $data = $query->fetch();

            return ['status'=>'success','code'=>200, 'data'=>$data, 'message'=>'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'error','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function editTrackKeys(){
        try {
            $data = $_POST['form'];
            $data = postArray($data,"true");
            $unit_id = $_POST["edit_unit_id"];

            //Required variable array
            $required_array = ['tag_key_name','tag_key_email','tag_key','tag_key_quantity','tag_key_pick_date','tag_key_pick_time'];
            //Max length variable array
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(empty($data['key_track_id']) ):
                    $key_data = [];
                    $key_data["unit_id"] = $unit_id;
                    $key_data["key_name"] = $data["tag_key_name"];
                    $key_data["email"] = (isset($data['tag_key_email']) && !empty($data['tag_key_email'])) ? $data['tag_key_email'] : '';
                    $key_data["company_name"] = (isset($data['tag_key_company']) && !empty($data['tag_key_company'])) ? $data['tag_key_company'] : '';
                    $key_data["phone"] = (isset($data['tag_key_phone']) && !empty($data['tag_key_phone'])) ? $data['tag_key_phone'] : '';
                    $key_data["Address1"] = (isset($data['tag_key_address1']) && !empty($data['tag_key_address1'])) ? $data['tag_key_address1'] : '';
                    $key_data["Address2"] = (isset($data['tag_key_address2']) && !empty($data['tag_key_address2'])) ? $data['tag_key_address2'] : '';
                    $key_data["Address3"] = (isset($data['tag_key_address3']) && !empty($data['tag_key_address3'])) ? $data['tag_key_address3'] : '';
                    $key_data["Address4"] = (isset($data['tag_key_address4']) && !empty($data['tag_key_address4'])) ? $data['tag_key_address4'] : '';
                    $key_data["key_number"] = (isset($data['tag_key']) && !empty($data['tag_key'])) ? $data['tag_key'] : '';
                    $key_data["key_quality"] = (isset($data['tag_key_quantity']) && !empty($data['tag_key_quantity'])) ? $data['tag_key_quantity'] : '';
                    $key_data["pick_up_date"] = (isset($data['tag_key_pick_date']) && !empty($data['tag_key_pick_date'])) ?  mySqlDateFormat($data['tag_key_pick_date'],null,$this->companyConnection) : NULL ;
                    $key_data["pick_up_time"] = (isset($data['tag_key_pick_time']) && !empty($data['tag_key_pick_time'])) ? mySqlTimeFormat($data['tag_key_pick_time']) : '';
                    $key_data["return_date"] = (isset($data['tag_key_return_date']) && !empty($data['tag_key_return_date'])) ? mySqlDateFormat($data['tag_key_return_date'],null,$this->companyConnection) : NULL;
                    $key_data["return_time"] = (isset($data['tag_key_return_time']) && !empty($data['tag_key_return_time'])) ? mySqlTimeFormat($data['tag_key_return_time']) : '';
                    $key_data["key_designator"] = (isset($data['tag_key_designator']) && !empty($data['tag_key_designator'])) ? $data['tag_key_designator'] : '';
                    $key_data["created_at"] = date('Y-m-d H:i:s');
                    $key_data["updated_at"] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($key_data);
                    //Save Data in Company Database

                    $query = "INSERT INTO unit_track_key (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($key_data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                else:
                    $track_key_id = (isset($data['key_track_id']) && !empty($data['key_track_id'])) ? $data['key_track_id'] : null;
                    $key_data = [];
                    $key_data["unit_id"] = $unit_id;
                    $key_data["key_name"] = $data["tag_key_name"];
                    $key_data["email"] = (isset($data['tag_key_email']) && !empty($data['tag_key_email'])) ? $data['tag_key_email'] : '';
                    $key_data["company_name"] = (isset($data['tag_key_company']) && !empty($data['tag_key_company'])) ? $data['tag_key_company'] : '';
                    $key_data["phone"] = (isset($data['tag_key_phone']) && !empty($data['tag_key_phone'])) ? $data['tag_key_phone'] : '';
                    $key_data["Address1"] = (isset($data['tag_key_address1']) && !empty($data['tag_key_address1'])) ? $data['tag_key_address1'] : '';
                    $key_data["Address2"] = (isset($data['tag_key_address2']) && !empty($data['tag_key_address2'])) ? $data['tag_key_address2'] : '';
                    $key_data["Address3"] = (isset($data['tag_key_address3']) && !empty($data['tag_key_address3'])) ? $data['tag_key_address3'] : '';
                    $key_data["Address4"] = (isset($data['tag_key_address4']) && !empty($data['tag_key_address4'])) ? $data['tag_key_address4'] : '';
                    $key_data["key_number"] = (isset($data['tag_key']) && !empty($data['tag_key'])) ? $data['tag_key'] : '';
                    $key_data["key_quality"] = (isset($data['tag_key_quantity']) && !empty($data['tag_key_quantity'])) ? $data['tag_key_quantity'] : '';
                    $key_data["pick_up_date"] = (isset($data['tag_key_pick_date'])) ?  mySqlDateFormat($data['tag_key_pick_date'],null,$this->companyConnection) : NULL ;
                    $key_data["pick_up_time"] = (isset($data['tag_key_pick_time']) && !empty($data['tag_key_pick_time'])) ? mySqlTimeFormat($data['tag_key_pick_time']) : '';
                    $key_data["return_date"] = (isset($data['tag_key_return_date']) && !empty($data['tag_key_return_date'])) ? mySqlDateFormat($data['tag_key_return_date'],null,$this->companyConnection) : NULL;
                    $key_data["return_time"] = (isset($data['tag_key_return_time']) && !empty($data['tag_key_return_time'])) ? mySqlTimeFormat($data['tag_key_return_time']) : '';
                    $key_data["key_designator"] = (isset($data['tag_key_designator']) && !empty($data['tag_key_designator'])) ? $data['tag_key_designator'] : '';
                    $key_data["updated_at"] = date('Y-m-d H:i:s');
//dd($key_data);
                    $sqlData = createSqlColValPair($key_data);
                    $query = "UPDATE unit_track_key SET " . $sqlData['columnsValuesPair'] . " where id='$track_key_id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                endif;
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function editKeys(){
        try {
            $data = $_POST['form'];
            $data = postArray($data,"true");
            $unit_id = $_POST["edit_unit_id"];

            //Required variable array
            $required_array = ['key_tag','total_keys'];
            //Max length variable array
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(empty($data['key_id']) ):


                    $key_data = [];
                    $key_data["unit_id"] = $unit_id;
                    $key_data["key_id"] = $data["key_tag"];
                    $key_data["key_description"] = (isset($data['key_desc']) && !empty($data['key_desc'])) ? $data['key_desc'] : '';
                    $key_data["total_keys"] = (isset($data['total_keys']) && !empty($data['total_keys'])) ? $data['total_keys'] : '';
                    $key_data['available_keys'] = $data['total_keys'];
                    $key_data["created_at"] = date('Y-m-d H:i:s');
                    $key_data["updated_at"] = date('Y-m-d H:i:s');
                    $key_data["status"] = 1;
                    $sqlData = createSqlColVal($key_data);
                    //Save Data in Company Database

                    $query = "INSERT INTO unit_keys (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($key_data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                else:

                    $chekout_sql = 'SELECT SUM(checkout_keys) AS total_checkout_keys FROM unit_key_checkout WHERE key_id='.$data['key_id'];
                    // $row = $chekout_sql->fetch(PDO::FETCH_ASSOC);

                    $stmt = $this->companyConnection->prepare($chekout_sql);
                    $stmt->execute();
                    $total_checkout_keys=0;
                    while($row1 = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $total_checkout_keys += $row1['total_checkout_keys'];
                    }

                    $track_key_id = (isset($data['key_id']) && !empty($data['key_id'])) ? $data['key_id'] : null;
                    $key_data = [];
                    $key_data["unit_id"] = $unit_id;
                    $key_data["key_id"] = $data["key_tag"];
                    $key_data["key_description"] = (isset($data['key_desc']) && !empty($data['key_desc'])) ? $data['key_desc'] : '';
                    $key_data["total_keys"] = (isset($data['total_keys']) && !empty($data['total_keys'])) ? $data['total_keys'] : '';
                    $availablekeys = $data['total_keys'] - $total_checkout_keys;
                    $key_data['available_keys'] = $availablekeys;
                    $key_data["updated_at"] = date('Y-m-d H:i:s');
                    $key_data["status"] = 1;
//dd($key_data);
                    $sqlData = createSqlColValPair($key_data);
                    $query = "UPDATE unit_keys SET " . $sqlData['columnsValuesPair'] . " where id='$track_key_id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                endif;
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addCheckoutDetail(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);

            $unit_id = $_POST['unit_id'];
            $id = $data['key_id'];
            //Required variable array
            $required_array = ['checkout_key_holder','checkout_keys'];
            //Max length variable array
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation\

            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            $queryFetch = $this->companyConnection->query("SELECT * FROM  unit_keys WHERE id=$id");
            $key = $queryFetch->fetch();

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                if ((int)$key['available_keys'] >= (int)$data['checkout_keys']) {
                    $data['available_keys'] = (int)$key['available_keys'] - (int)$data['checkout_keys'];
                    $data['key_id'] = (int)$id;
                    $data['key_holder'] = $data['checkout_key_holder'];
                    $data['checkout_desc'] = $data['checkout_description'];
                    $data['checkout_keys'] = $data['checkout_keys'];
                    // $data['available_keys'] = $data['checkout_description'];
//                $data['created_at'] = $data['checkout_description'];
//                $data['updated_at'] = $data['checkout_description'];

                    unset($data['checkout_key_holder'], $data['checkout_description']);
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO   unit_key_checkout (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);

                    $queryupd = "UPDATE  unit_keys SET available_keys='" . $data['available_keys'] . "'  where id=$id";
                    $stmtupd = $this->companyConnection->prepare($queryupd);
                    $stmtupd->execute();

                    $queryupd1 = "UPDATE  unit_key_checkout SET available_keys='" . $data['available_keys'] . "'  where key_id=$id";
                    $stmtupd1 = $this->companyConnection->prepare($queryupd1);
                    $stmtupd1->execute();

                    $queryFetchcheckout = $this->companyConnection->query("SELECT * FROM  unit_key_checkout WHERE key_id=" . $key['id'] . "");
                    $keycheckout = $queryFetchcheckout->fetch();
                    if (!empty($keycheckout)) {
                        $querystatus = "UPDATE  unit_keys SET status='1'  where id=$id";
                        $stmtstatus = $this->companyConnection->prepare($querystatus);
                        $stmtstatus->execute();
                    }

                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                } else {
                    return array('code' => 500, 'status' => 'error', 'message' => 'Cannot checkout more than available keys');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function fetchCheckoutDetails()
    {
        try {
            $unit_id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM unit_keys WHERE id=" . $unit_id . " AND deleted_at IS NULL");
            $data = $query->fetch();

            return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getUnitDetailForView(){
        try {
            $school_district_html = '';
            $renovation_detail_html = '';
            $custom_data = '';
            $edit_unit_id = $_POST['id'];
            $images_data =[];
            $sql = "SELECT unit_details.*,
                    company_pet_friendly.pet_friendly as pet_friendly, 
                    building_detail.building_name as building_name, 
                    general_property.property_id as random_property_id, 
                    general_property.id as property_id, 
                    unit_details.property_id as building_property_id, 
                    general_property.property_name as property_name,
                    company_unit_type.unit_type as unit_type_name,
                    general_property.school_district_municipality as property_school_district_municipality, 
                    general_property.school_district_code as property_school_district_code, 
                    general_property.school_district_notes as property_school_district_notes
                FROM unit_details 
                LEFT JOIN renovation_details ON unit_details.id=renovation_details.object_id AND renovation_details.object_type='unit'
                LEFT JOIN building_detail ON unit_details.building_id=building_detail.id
                LEFT JOIN general_property ON unit_details.property_id=general_property.id
                LEFT JOIN company_pet_friendly ON unit_details.pet_friendly_id=company_pet_friendly.id
                LEFT JOIN company_unit_type ON unit_details.unit_type_id=company_unit_type.id
                 WHERE unit_details.id=".$edit_unit_id ;
            $stmt = $this->companyConnection->prepare($sql);
            if($stmt->execute()) {
                $data = $stmt->fetch();
                if(isset($data) && !empty($data)){
                    if(isset($data["custom_field"]) && !empty($data["custom_field"])){
                        $custom_data = unserialize($data["custom_field"]);
                    }

                    $sql1 = "SELECT * FROM unit_file_uploads WHERE file_type=1 AND unit_id=".$edit_unit_id ;
                    $stmt1 = $this->companyConnection->prepare($sql1);
                    if($stmt1->execute())
                    {
                        $images_data =$stmt1->fetchAll();
                    }
                    $images_count = count($images_data);
                    if($images_count<=3){
                        $newdata =  array (
                            'file_name' => 'no-image.png',
                            'file_location' => 'images/no-image.png'
                        );
                        for($i=$images_count ;$i<3;$i++){
                            $images_data[$i] =   $newdata;
                        }
                    }

                    if(isset($data["amenities"]) && !empty($data["amenities"])) {
                        $newamenities='';
                        $amenities = unserialize($data["amenities"]);
                        if(!empty($amenities)){

                            foreach ($amenities as $key=>$value){
                                $query = 'SELECT name FROM company_property_amenities WHERE id='.$value;
                                $groupData = $this->companyConnection->query($query)->fetch();
                                $name = $groupData['name'];
                                $newamenities .= $name.',';
                            }
                            $data["amenities"] = $newamenities;
                            $data["saved_amenities"] = $amenities;
                        }else{
                            $data["amenities"] = "N/A";
                            $data["saved_amenities"] = [];
                        }
                    }else{

                        $data["amenities"] = "N/A";
                        $data["saved_amenities"] = [];
                    }

                    $data["smoking_allowed"] = ($data["smoking_allowed"] == 0) ? "No" : "Yes";

                    $school_district_code=[];
                    $school_district_notes=[];
                    $school_district_municipality=[];
                    if(isset($data["property_school_district_municipality"]) && !empty($data["property_school_district_municipality"]))
                        $school_district_municipality = unserialize($data["property_school_district_municipality"]);
                    if(isset($data["property_school_district_code"]) && !empty($data["property_school_district_code"]))
                        $school_district_code = unserialize($data["property_school_district_code"]);
                    if(isset($data["property_school_district_notes"]) && !empty($data["property_school_district_notes"]))
                        $school_district_notes = unserialize($data["property_school_district_notes"]);

                    $result = array_map(function ($name, $type, $price) {
                        return array_combine(
                            ['school_district', 'code', 'notes'],
                            [$name, $type, $price]
                        );
                    }, $school_district_municipality, $school_district_code, $school_district_notes);
                    if(!empty($result)) {
                        foreach ($result as $key1 => $value1) {
                            $school_district_html .= "<tr>";
                            $school_district_html .= "<td>" .$value1["school_district"]."</td>";
                            $school_district_html .= "<td>" . $value1["code"] . "</td>";
                            $school_district_html .= "<td>" . $value1["notes"] . "</td>";
                            $school_district_html .= "</tr>";
                        }
                    }

                    /*query to fetch renovation details */
                    $renovation_detail_data =[];
                    $sql2 = "SELECT * FROM renovation_details WHERE object_type='unit' AND object_id=".$edit_unit_id ;
                    $stmt2 = $this->companyConnection->prepare($sql2); // Prepare the statement
                    if($stmt2->execute())
                    {
                        $renovation_detail_data =$stmt2->fetchAll();
                    }
                    foreach($renovation_detail_data as $key=>$value) {
                        if(!empty($value['last_renovation_date'])){
                            $renovation_date = dateFormatUser($value['last_renovation_date'],null,$this->companyConnection);
                        }else{
                            $renovation_date ="";
                        }

                        if(!empty($value['last_renovation_time'])){
                            $renovation_time = timeFormat($value['last_renovation_time'], null,$this->companyConnection);
                        }else{
                            $renovation_time ="";
                        }
                        $renovation_detail_html .= "<tr>";
                        $renovation_detail_html .= "<td>".$renovation_date."</td>";
                        $renovation_detail_html .= "<td>".$renovation_time."</td>";
                        $renovation_detail_html .= "<td>".$value['last_renovation_description']."</td>";
                        $renovation_detail_html .= "</tr>";
                    }
                    $key_access_codes = 'N/A';
                    if(isset($data['key_access_codes'])|| isset($data['key_access_codes_desc'])) {
                        $key_access_codes = '';
                        $key_info_data = unserialize($data['key_access_codes']);
                        $key_description_data = unserialize($data['key_access_codes_desc']);
                        if(is_array($key_info_data))
                        {
                            $info_data_count = count($key_info_data);
                        } else {
                            $info_data_count = 0;
                        }

                        $i=1;
                        if(isset($key_info_data) && !empty($key_info_data)){
                            foreach ($key_info_data as $key => $value) {
                                $info_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $value], 'company_key_access');
                                if(isset($info_data['data']['key_code'])){
                                    $key_access_codes .= @$info_data['data']['key_code'] . ' : ' . $key_description_data[$key];
                                    $key_access_codes .= ($i < $info_data_count) ? ' , ' : ' ';
                                }
                                $i++;
                            }
                        }
                    }
                    $data['key_access_codes'] = $key_access_codes;

                    $phone_number1 =(isset($data["phone_number1"])) ? $data["phone_number1"] : '';
                    $phone_number2 = (isset($data["phone_number2"]) && !empty($data["phone_number2"]))? ', '.$data["phone_number2"] : '';
                    $phone_number3 = (isset($data["phone_number3"]) && !empty($data["phone_number3"]))? ', '.$data["phone_number3"] : '';
                    $phone_number = $phone_number1.$phone_number2.$phone_number3;
                    $data['phone_number'] = $phone_number;

                    $fax_number1 =(isset($data["fax_number1"])) ? $data["fax_number1"] : '';
                    $fax_number2 = (isset($data["fax_number2"]) && !empty($data["fax_number2"]))? ', '.$data["fax_number2"] : '';
                    $fax_number3 = (isset($data["fax_number3"]) && !empty($data["fax_number3"]))? ', '.$data["fax_number3"] : '';
                    $fax_number = $fax_number1.$fax_number2.$fax_number3;
                    $data['fax_number'] = $fax_number;
                    $data['description_html'] = isset($data['building_description'])? $data['building_description'] : 'N/A';
                    $data['notes_html'] = isset($data['building_unit_notes'])? $data['building_unit_notes'] : 'N/A';
                    $data['unit_name'] = isset($data['unit_prefix'])? $data['unit_prefix'].' - '.$data['unit_no'] : $data['unit_no'];
                    $data['base_rent'] = number_format($data['base_rent'],2);
                    $data['market_rent'] = number_format($data['market_rent'],2);
                    $data['security_deposit'] = number_format($data['security_deposit'],2);
                    return array(
                        'code' => 200,
                        'status' => 'success',
                        'data' => $data,
                        'images_data' => $images_data,
                        'custom_data'=>$custom_data,
                        'renovation_detail'=>$renovation_detail_html,
                        '$key_access_codes'=>$key_access_codes,
                        'school_district_municipality_data'=>$school_district_html,
                        'message' => 'Record fetched successfully.'
                    );
                }
            }
        } catch (Exception $exception) {
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * function to grt  a property detail
     */
    public function gerPropertyDetail() {
        try{
            $id = $_POST['id'];
            $sql = "SELECT * FROM general_property WHERE id=" . $id;
            $data = $this->companyConnection->query($sql)->fetch();
            $school_district_muncipiality = [];
            $school_district_code = [];
            $school_district_notes = [];
            $result = [];
            if (isset($data["school_district_municipality"]) && !empty($data["school_district_municipality"]))
                $school_district_muncipiality = unserialize($data["school_district_municipality"]);
            if (isset($data["school_district_code"]) && !empty($data["school_district_code"]))
                $school_district_code = unserialize($data["school_district_code"]);
            if (isset($data["school_district_notes"]) && !empty($data["school_district_notes"]))
                $school_district_notes = unserialize($data["school_district_notes"]);
            $result = array_map(function ($name, $type, $price) {
                return array_combine(
                    ['school_district', 'code', 'notes'], [$name, $type, $price]
                );
            }, $school_district_muncipiality, $school_district_code, $school_district_notes);
            $data['amenities'] = isset($data['amenities'])? unserialize($data['amenities']) : '';
            $get_default_settings =  getSingleRecord($this->companyConnection, ['column' => 'user_id', 'value' => $_SESSION[SESSION_DOMAIN]['cuser_id']], 'default_settings');
            return array('property_detail' => $data, 'school_district_muncipiality_data' => $result,'default_settings'=>$get_default_settings, 'status' => 'success');
        } catch (Exception $exception){
            return ['status' => 'error', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function deleteKey() {
        //delete
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'unit_keys');

            $sql = "UPDATE unit_keys SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            return ['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.'];

        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function deleteTrackKey() {
        //delete
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'unit_track_key');

            $sql = "UPDATE unit_track_key SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            return ['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.'];
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }


    /**
     * function to add new renovation
     *
     *
     */

    public function addNewComplaint(){
        try {
            $data = $_POST['form'];
//            dd($data);
            $data = postArray($data,"false");
            $building_id = $_POST["edit_unit_id"];
            $complaint_id = $data["edit_complaint_id"];
            if(isset($complaint_id) && empty($complaint_id)) {
                $complaint_data = [];
                $complaint_data["object_id"] = $building_id;
                $complaint_data["module_type"] = 'unit';
                $complaint_data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $complaint_data['status'] = 1;
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : NULL;

                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : NULL;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["created_at"] = date('Y-m-d H:i:s');
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($complaint_data);
                //Save Data in Company Database
                $query = "INSERT INTO  complaints (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($complaint_data);
                $id = $this->companyConnection->lastInsertId();
                if ($id) {
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully');
                } else {
                    return array('code' => 400, 'status' => 'error', 'message' => 'Record Not Added successfully');
                }
            } else{
                $complaint_data = [];
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : NULL;
                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : NULL;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');
//                dd($complaint_data);
                $sqlData = createSqlColValPair($complaint_data);
                $query = "UPDATE complaints SET " . $sqlData['columnsValuesPair'] . " where id='$complaint_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');

            }

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $e->getMessage()));
            return;
        }

    }

    public function getComplaintDetail() {
        try {
            $id = $_POST['id'];
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'complaints');
            if(!empty($record["data"]["complaint_date"])){
                $record["data"]["complaint_date"] =  dateFormatUser($record["data"]["complaint_date"],null,$this->companyConnection);
            }
            return ['status' => 'success', 'code' => 200,'data'=>$record, 'message' => 'Record deleted successfully.'];
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function deleteComplaint(){
        try{
            $building_id = $_REQUEST['id'];
            $data = date('Y-m-d H:i:s');
            $sql = "UPDATE complaints SET deleted_at=? WHERE id=?";
            $stmt= $this->companyConnection->prepare($sql);
            $stmt->execute([$data,$building_id]);
            return ['status'=>'success','code'=>200,'data'=>'Complaint deleted successfully.'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * @return array|void
     * function to return complaints envelope data
     */
    public function getComplaintsData() {
        //delete
        try {

            $complaint_ids= $_POST['complaint_ids'];
            $complaint_idss  = str_repeat('?,', count($complaint_ids) - 1) . '?';
            $stmt = $this->companyConnection->prepare("Select * FROM `complaints` as c 
            LEFT JOIN unit_details as b ON b.id=c.object_id AND c.module_type='unit' 
            LEFT JOIN complaint_types as ct ON ct.id=c.complaint_type_id
            LEFT JOIN general_property as gp ON gp.id=b.property_id
            WHERE c.id IN ($complaint_idss)");
            $stmt->execute($complaint_ids);
            $data = $stmt->fetchAll();


            $stmt = $this->companyConnection->prepare("Select * FROM `unit_details` WHERE `id` IN ($complaint_idss)");
            $print_complaints_html = '';
            $company_logo = SITE_URL."company/images/logo.png";
            foreach($data as $key => $value) {
                $address1 = (isset($value['address1']))?$value['address1']:'';
                $address2 = (isset($value['address2']))? ', '.$value['address2']:'';
                $address3 = (isset($value['address3']))? ', '.$value['address3']:'';
                $address4 = (isset($value['address4']))? ', '.$value['address4']:'';
                $address = $address1.$address2.$address3.$address4;
                //  dd($address);
                $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
    <tr>
      <td style="background: #00b0f0; height: 30px;">
        
      </td>
    </tr>
    <tr>
      <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
        <img width="200" src="'.$company_logo.'"/>
      </td>
    </tr>
    <tr>
      <td style="background: #00b0f0; height: 30px;">
        
      </td>
    </tr>
    <tr>
      <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
        <table width="100%" align="center" cellspacing="0" cellpadding="0">
          <tr>
            <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> Complaint by Unit </td>
          </tr>
        </table>
        <table width="100%" align="center" border="1" cellspacing="0" cellpadding="5">        
          <tr>
            <td>Name: </td>
            <td>'.$value["unit_check"].'</td>
          </tr>
            <tr>
            <td>Address: </td>
            <td>' .$address.'</td>
          </tr>
          <tr>
            <td>Complaint Type: </td>
            <td>'.$value["complaint_type"].'</td>
          </tr>
          <tr>
            <td>Description: </td>
            <td>'.$value['complaint_note'].'</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="background: #585858; color: #fff; padding: 10px; text-align: center;">
        Apexlink.apexlink@yopmail.com
        
      </td>
    </tr>
  </table>';
            }


            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    /*function to send complaint mail */
    public function SendComplaintMail()
    {
        $data= $_POST;
        $body =$data['data_html'];
        $domain = explode('.', $_SERVER['HTTP_HOST']);
        $subdomain = array_shift($domain);
        $user_detail = $_SESSION['SESSION_'.$subdomain];

        try{
            //  $userid = (isset($userid) ? $userid : $_REQUEST['id']);
            //$user_details = $this->conn->query("SELECT * FROM users where id=".$userid)->fetch();
            //$name = userName($user_details['id'], 'users');
            //$url = 'https://'.$user_details['domain_name'].'.'.COMPANY_DOMAIN_URL;
            //$body = file_get_contents(SUPERADMIN_DIRECTORY_URL.'/views/Emails/welcome.php');
            //$body = str_replace("#name#",ucfirst($name),$body);
            //$body = str_replace("#email#",$user_details['email'],$body);
            //$body = str_replace("#password#",$user_details['actual_password'],$body);
            //$body = str_replace("#logo#",SITE_URL.'/company/images/logo.png',$body);
            //$body = str_replace("#website#",$url,$body);
            $request['action']  = 'SendMailPhp';
            //$request['to[]']    = $user_details['email'];
            $request['to[]']    = $user_detail['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal']  = '1';
//          $request['attachments[]'] = SITE_URL.'uploads/email_attachments/WelcomeMailAttachment.docx';
            curlRequest($request);
            return ['status'=>'success','code'=>200,'data'=>$request];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function updatePropertyUnit()
    {
        try {

            $files = $_FILES;

            $data                       = $_POST['form'];
            $data                       = serializeToPostArray($data);
            $data                       = postArray($data);
            $data['market_rent']        = $_POST['market_rent'];
            $data['base_rent']          = $_POST['baseRent'];
            $data['security_deposit']   = $_POST['securityDeposit'];
            $array_data                 = [];
            $arr_length                 =  (isset($data['building_id']) && !empty($data['building_id'])?sizeof($data['building_id']):'');
            $arr_Plength                =  (isset($data['phone_number']) && !empty($data['phone_number'])?sizeof($data['phone_number']):'');
            $arr_Flength                =  (isset($data['fax_number']) && !empty($data['fax_number'])?sizeof($data['fax_number']):'');

            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];

            /*unset the custom keys */
            $custom_keys=[];
            if (!empty($custom_field)){
                $custom_data = json_decode(stripslashes($custom_field));
                $custom_field = [];
                foreach ($custom_data as $key => $value) {
                    $custom_keys[$key] = $value->name;
                    array_push($custom_field,(array) $value);
                }
            }
            $removeKeys = $custom_keys;

            if (!empty($removeKeys)){
                foreach($removeKeys as $key) {
                    unset($data[$key]);
                }
            }

            foreach ($data AS $key => $value) {
                if (stristr($key, 'photoVideoName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }

            foreach ($data AS $key => $value) {

                if (stristr($key, 'imgName') === FALSE) {
                    continue;
                } else {
                    unset($data[$key]);
                }
            }

            foreach($data as $akey=>$array_value){
                if(is_array($array_value))
                {
                    foreach($array_value as $k=>$v) {
                        $array_data[$akey.'_'.$k] = $v;
                    }
                }else{
                    $array_data[$akey] = $array_value;
                }
            }
            $required_keys = [];


            for($j=0; $j<$arr_Plength; $j++)
            {
                $required_keys[] = 'phone_number_'.$j;
            }

            for($k=0; $k<$arr_Flength; $k++)
            {
                $required_keys[] = 'fax_number_'.$k;
            }

            $formrequiredkeys = ['floor_no_0','building_id_0','unit_no','bedrooms_no','unit_type_id','baseRent','market_rent','market_rent','bathrooms_no','bedrooms_no'];
            if(isset($data['property_unit_id']))
            {
                $propery_id = @$data['property_unit_id'];
                $query = $this->companyConnection->query("SELECT * FROM general_property where id = '$propery_id'");
                $queryData = $query->fetch();
                unset($data['property_unit_id']);
                if(!$queryData)
                {
                    return array('code' => 400,'status' => 'error', 'data' => 'property_id_error', 'message' => 'Incorrect Property. Please select again.');
                }
            }

            $formrequiredkeys = array_merge($formrequiredkeys,$required_keys);

            $err_array = [];
            $err_array = validation($array_data,$this->companyConnection,$formrequiredkeys);

            if(!empty($err_array)){
                if($array_data['baseRent'] > $array_data['market_rent'])
                {
                    return array('code' => 400,'status' => 'error', 'data' => 'base_rent_error', 'message' => 'Please fill all required fields!');
                }
                return array('code' => 400,'status' => 'error', 'data' => $err_array, 'message' => 'Please fill all required fields!');
            } else {

                if(!empty($custom_field)){
                    foreach ($custom_field as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $unitTblData = $data;
                $unitTblData['unit_type_id'] = (isset($data['unitTypeID']) && $data['unitTypeID'] != "")?$data['unitTypeID']:"";
                $unitTblData['user_id'] =  $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $unitTblData['created_by'] =  $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $unitTblData['amenities'] = isset($data['amenities'])?serialize($data['amenities']):'';
                $unitTblData['custom_field'] = (count($custom_field)>0) ? serialize($custom_field):null;
                $unitTblData['key_access_codes'] = isset($unitTblData['key_access_codes_info'])?serialize($unitTblData['key_access_codes_info']):'';
                $unitTblData['key_access_codes_desc'] = isset($unitTblData['key_access_codes_desc'])?serialize($unitTblData['key_access_codes_desc']):'';


                $editUnitId = $unitTblData['editUnitId'];
                unset($unitTblData['complaint_type_options'],$unitTblData['building_id'],$unitTblData['floor_no'],$unitTblData['unit_prefix'],$unitTblData['unit_from'],$unitTblData['unit_to'],$unitTblData['phone_number'],$unitTblData['fax_number'],$unitTblData['fileshidden'],$unitTblData['unitTypeID'],$unitTblData['baseRent'],$unitTblData['securityDeposit'],$unitTblData['pet_friendly'],$unitTblData['last_renovation_date'],$unitTblData['key_access_codes_info'],$unitTblData['key'],$unitTblData['tag_key_name'],$unitTblData['tag_key_email'],$unitTblData['tag_key_phone'],$unitTblData['tag_key_address1'],$unitTblData['tag_key_address2'],$unitTblData['tag_key_address3'],$unitTblData['tag_key_address4'],$unitTblData['tag_key'],$unitTblData['tag_key_quantity'],$unitTblData['tag_key_pick_date'],$unitTblData['tag_key_pick_time'],$unitTblData['tag_key_return_date'],$unitTblData['tag_key_return_time'],$unitTblData['tag_key_designator'],$unitTblData['key_tag'],$unitTblData['key_desc'],$unitTblData['total_keys'],$unitTblData['tag_key_company'],$unitTblData['unit_no'],$unitTblData['checkout_key_holder'],$unitTblData['checkout_keys'],$unitTblData['checkout_description'],$unitTblData['key_id'],$unitTblData['id'],$unitTblData['flag_by'],$unitTblData['date'],$unitTblData['flag_name'],$unitTblData['country_code'],$unitTblData['flag_phone_number'],$unitTblData['flag_reason'],$unitTblData['completed'],$unitTblData['flag_note'],$unitTblData['edit_complaint_id'],$unitTblData['complaint_id'],$unitTblData['complaint_date'],$unitTblData['complaint_note'],$unitTblData['other_notes'],$unitTblData['editUnitId']);

                if(isset($data['key']) && $data['key'] == 'keyUnit') {
                    $unitTblData['key_unit'] = 1;
                } else {
                    $unitTblData['key_unit'] = 2;
                }
                /*Add Phone && Fax Number*/
                $phone1 = ''; $phone2 = ''; $phone3='';
                if(isset($data['phone_number']) && count($data['phone_number'])==1){
                    $phone1 = $data['phone_number'][0];
                } elseif(isset($data['phone_number']) && count($data['phone_number'])==2) {
                    $phone1 = $data['phone_number'][0];
                    $phone2 = $data['phone_number'][1];
                }elseif(isset($data['phone_number']) && count($data['phone_number'])==3){
                    $phone1 = $data['phone_number'][0];
                    $phone2 = $data['phone_number'][1];
                    $phone3 = $data['phone_number'][2];
                }

                $unitTblData['phone_number1'] = $phone1;
                $unitTblData['phone_number2'] = $phone2;
                $unitTblData['phone_number3'] = $phone3;


                $fax1 = ''; $fax2 = ''; $fax3 = '';
                if(isset($data['fax_number']) && count($data['fax_number'])==1){
                    $fax1 = $data['fax_number'][0];
                } elseif(isset($data['fax_number']) && count($data['fax_number'])==2) {
                    $fax1 = $data['fax_number'][0];
                    $fax2 = $data['fax_number'][1];
                }elseif(isset($data['fax_number']) && count($data['fax_number'])==3){
                    $fax1 = $data['fax_number'][0];
                    $fax2 = $data['fax_number'][1];
                    $fax3 = $data['fax_number'][2];
                }

                $unitTblData['fax_number1'] = $fax1;
                $unitTblData['fax_number2'] = $fax2;
                $unitTblData['fax_number3'] = $fax3;
                $unitTblData['status'] = 1;


                $unitTblData['building_id'] = $data['building_id'][0];
                $unitTblData['floor_no']    =  $data['floor_no'][0] ;
                $unitTblData['unit_prefix'] =  $data['unit_prefix'][0] ;
                $unitTblData['unit_no'] =  $data['unit_no'][0] ;

                $unit_check = (isset($data['unit_prefix'][0]) && !empty($data['unit_prefix'][0])) ? ($data['unit_prefix'][0].'-'.$data['unit_no'][0]) : $data['unit_no'][0];

                $unitTblData['unit_check'] =  $unit_check ;
                $dataUnit = $unitTblData;
                $dataUnit['property_name'] = $queryData['property_name'];
                $dataUnit['city'] = $queryData['city'];
                $dataUnit['state'] = $queryData['state'];
                $dataUnit['address'] = $queryData['address_list'];
                $dataUnit['school_district_municipality'] = $queryData['school_district_municipality'];

                //UPDATE LINKED UNITS IN BUILDING TABLE
                $addBuildingLinkedUnit = $this->addBuildingLinkedUnit($unitTblData['building_id'],$editUnitId);

                //Update Unit Data
                //dd($unitTblData);
                if(empty($unitTblData['pet_friendly_id'])){
                    unset($unitTblData['pet_friendly_id']);
                }
                $sqlData = createSqlUpdateCase($unitTblData);
                $query = "UPDATE unit_details SET " . $sqlData['columnsValuesPair'] . " where id='$editUnitId'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($sqlData['data']);

                $last_insert_id = $editUnitId;
                $dataUnit['id'] = $last_insert_id;
                $ElasticSearchSave = insertDocument('UNIT','UPDATE',$dataUnit,$this->companyConnection);
                $renovation_detail = $this->addUnitRenovationDetail($unitTblData,$last_insert_id);
                if($last_insert_id) {

                    if (!empty($files)) {
                        $domain = getDomain();
                        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');

                        foreach ($files as $key => $value) {
                            $file_name = $value['name'];
                            $fileDataNew = [];
                            $code2 = '';

                            $query = $this->companyConnection->query("SELECT count(*) as unit_counts FROM unit_file_uploads where file_name='$file_name' and unit_id = '$last_insert_id'");
                            $queryData = $query->fetch();
                            $unitimage_counts = (int)$queryData['unit_counts'];

                            if((int)$unitimage_counts > (int)0)
                            {
                                return array('code' => 500, 'status' => 'warning', 'message' => 'File allready exists.');
                            }

                            $file_tmp = $value['tmp_name'];
                            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                            $name = time() . uniqid(rand());
                            $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            //Check if the directory already exists.
                            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                //Directory does not exist, so lets create it.
                                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                            }
                            move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                            $data = [];
                            $data['unit_id'] = $last_insert_id;
                            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $data['file_name'] = $file_name;
                            $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K') . 'kb';
                            $data['file_location'] = $path . '/' . $name . '.' . $ext;
                            $data['file_extension'] = $ext;
                            $data['codec'] = $value['type'];
                            if (strstr($value['type'], "video/")) {
                                $type = 3;
                            } else if (strstr($value['type'], "image/")) {
                                $type = 1;
                            } else {
                                $type = 2;
                            }
                            $data['file_type'] = $type;
                            $data['created_at'] = date('Y-m-d H:i:s');
                            $data['updated_at'] = date('Y-m-d H:i:s');

                            $sqlData = createSqlColVal($data);
                            $query = "INSERT INTO unit_file_uploads (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                            $stmt = $this->companyConnection->prepare($query);
                            $stmt->execute($data);
                        }
                    }
                }

                $data['last_insert_id'] = $last_insert_id;

            }
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record updated successfully');



        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    /**
     *  @return array|void
     * function to get login company data
     */

    public function getUnitCompanyData() {
        try {
            $id= $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $unit_id = $_POST['unit_id'];
            $data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'users');
            $unit_detail = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $unit_id], 'unit_details');
            $building_id = $unit_detail['data']['building_id'];
            $query = $this->companyConnection->query("SELECT * FROM building_detail WHERE id=".$building_id);
            $building_detail = $query->fetch();
            return array('status' => 'success', 'code' => 200, 'data' => $data,'unit_detail'=>$unit_detail, 'building_detail'=>$building_detail, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function addUnitCountToProperty($property_id){
        $unit_count_after_insertion = $this->companyConnection->query("SELECT COUNT(*) FROM unit_details WHERE property_id ='".$property_id."' AND deleted_at IS NULL")->fetch();
        $unit_count_after_insertion = $unit_count_after_insertion['COUNT(*)'];
        $countArray =[];
        $countArray['unit_exist'] =$unit_count_after_insertion;
        $propertyCountData = createSqlUpdateCase($countArray);
        $generalPropertyCount = $this->companyConnection->prepare("UPDATE general_property SET ".$propertyCountData['columnsValuesPair']." WHERE id=".$property_id)->execute($propertyCountData['data']);
        return $unit_count_after_insertion;
    }

    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    public function restoreUnit()
    {

        try {

            $deleted_at['deleted_at'] = NULL ;
            $sqlData = $this->createSqlColValPair($deleted_at);

            $query = "UPDATE unit_details SET ".$sqlData['columnsValuesPair']." WHERE id =".$_POST['cuser_id'];
            $stmt1 =$this->companyConnection->prepare($query);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $query, 'message' => 'Records restored successfully');


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function numberFormatAjax()
    {

        try {
            $number = $_POST['data'];
            $number = number_format($number,2);

            return array('code' => 200, 'status' => 'success', 'data' => $number, 'message' => 'Data fetched successfully');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

}
$property_unit_Ajax = new PropertyUnitAjax();