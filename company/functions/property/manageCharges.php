<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class ManageCharges extends DBConnection {

    /**
     * Flag constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to create charge code
     * @return array
     */
    public function createChargeCode(){
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                // $data['property_id'] = $_POST['property_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['status'] = '1';
                //Save Data in Company Database
                if($data['charge_code_type'] == '1'){
                    $data_unit_id = is_array($data['unit_type_id'])?$data['unit_type_id']:(array) $data['unit_type_id'];
                    //check duplicate record
                    foreach ($data_unit_id as $key=>$value) {
                        $duplicateQuery = 'SELECT id FROM company_accounting_charge_code_preferences WHERE charge_code='.$data['charge_code'].' AND unit_type_id='.$value;
                        $duplicateData = $this->companyConnection->query($duplicateQuery)->fetch();
                        if(!empty($duplicateData)){
                            return array('code' => 403, 'status' => 'warning', 'message' => 'Record Allready Exists.');
                        }
                    }
                    foreach ($data_unit_id as $key=>$value) {
                        $insert = [];
                        $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                        $insert['charge_code'] = $data['charge_code'];
                        $insert['charge_code_type'] = $data['charge_code_type'];
                        $insert['unit_type_id'] = $value;
                        $insert['frequency'] = $data['frequency'];
                        $insert['status'] = $data['status'];
                        //  $insert['property_id'] = $_POST['property_id'];
                        $insert['created_at'] = date('Y-m-d H:i:s');
                        $insert['updated_at'] = date('Y-m-d H:i:s');
                        $sqlData = createSqlColVal($insert);
                        $query = "INSERT INTO company_accounting_charge_code_preferences (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $exe = $stmt->execute($insert);
                        $insert['property_id'] = $_POST['property_id'];
                        unset($insert['status']);
                        $sqlData = createSqlColVal($insert);
                        $query2 = "INSERT INTO property_manage_charge (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt2 = $this->companyConnection->prepare($query2);
                        $exe2 = $stmt2->execute($insert);
                    }
                } else {
                    unset($data['unit_type_id']);
                    $duplicateQuery = 'SELECT id FROM company_accounting_charge_code_preferences WHERE charge_code='.$data['charge_code'].' AND frequency='.$data['frequency'];
                    $duplicateData = $this->companyConnection->query($duplicateQuery)->fetch();
                    if(!empty($duplicateData)){
                        return array('code' => 403, 'status' => 'warning', 'message' => 'Record Allready Exists.');
                    }
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO company_accounting_charge_code_preferences (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($data);
                    unset($data['status']);
                    $data['property_id'] = $_POST['property_id'];
                    $sqlData = createSqlColVal($data);
                    $query2 = "INSERT INTO property_manage_charge (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt2 = $this->companyConnection->prepare($query2);
                    $exe2 = $stmt2->execute($data);
                }

                if($exe){
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record added successfully.');
                } else {
                    return array('code' => 500, 'status' => 'error', 'message' => 'Internal Server Error!');
                }
            }
        }
        catch (PDOException $e) {
            dd($e);
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * function to get manage charges list
     * @return array
     */
    public function getManageCharges(){
        $id = $_POST['property_id'];
//        $insertPrefrenceData = $this->getChargePrefrences($id);
        $table = 'property_manage_charge';
        $columns = [$table.'.id','company_accounting_charge_code.charge_code',$table.'.type',$table.'.frequency','company_unit_type.unit_type',$table.'.charge_code_type',$table.'.is_editable',$table.'.amount',$table.'.checked'];
        $where = [['table'=>'property_manage_charge','column'=>'property_id','condition'=>'=', 'value'=>$id,'type'=>'AND']];
        $joins = [
            [
                'type'      =>   'LEFT',
                'table'     =>   'property_manage_charge',
                'column'    =>   'charge_code',
                'primary'   =>   'id',
                'on_table'  =>   'company_accounting_charge_code',
                'as'        =>   'company_accounting_charge_code'
            ],
            [
                'type'      =>   'LEFT',
                'table'     =>   'property_manage_charge',
                'column'    =>   'unit_type_id',
                'primary'   =>   'id',
                'on_table'  =>   'company_unit_type',
                'as'        =>   'company_unit_type'
            ],
        ];
        $query = selectQueryBuilder($columns,$table,$joins,$where);
        $data = $this->companyConnection->query($query.' ORDER BY id asc')->fetchAll();
        $unitTypeCharge = [];
        $propertyTypeCharge = [];
        if(!empty($data)){
            foreach ($data as $key=>$value){
                $value['frequency'] = ($value['frequency']=='1')?'One Time':'Monthly';
                if($value['charge_code_type'] == '0'){
                    array_push($propertyTypeCharge,$value);
                } elseif($value['charge_code_type'] == '1'){

                    array_push($unitTypeCharge,$value);
                }
            }
            return array('code' => 200, 'status' => 'success', 'unitTypeCharge' => $unitTypeCharge,'propertyTypeCharge' => $propertyTypeCharge, 'message' => 'Record retrieved Successfully.');
        } else {
            return array('code' => 403, 'status' => 'warning', 'unitTypeCharge' => $unitTypeCharge,'propertyTypeCharge' => $propertyTypeCharge, 'message' => 'No Record Found!');
        }
    }

    /**
     * function to get manage charges list
     * @return array
     */
    public function getChargePrefrences(){
        try {
            $id = $_POST['property_id'];
            $table = 'company_accounting_charge_code_preferences';
            $columns = [$table.'.*','company_unit_type.unit_type',$table.'.charge_code_type',$table.'.status'];
            $where = [['table'=>'company_accounting_charge_code_preferences','column'=>'status','condition'=>'=', 'value'=>'1','type'=>'AND'],['table'=>'company_accounting_charge_code_preferences','column'=>'status','condition'=>'=', 'value'=>'2','type'=>'OR']];
            $joins = [
                [
                    'type'      =>   'LEFT',
                    'table'     =>   'company_accounting_charge_code_preferences',
                    'column'    =>   'unit_type_id',
                    'primary'   =>   'id',
                    'on_table'  =>   'company_unit_type',
                    'as'        =>   'company_unit_type'
                ],
            ];
            $query = selectQueryBuilder($columns,$table,$joins,$where);
            $data = $this->companyConnection->query($query.' ORDER BY id asc')->fetchAll();
            $unitTypeCharge = [];
            $propertyTypeCharge = [];
            if(!empty($data)){
                foreach ($data as $key=>$value){
                    $insert = [];
                    $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $insert['prefrence_id'] = $value['id'];
                    $insert['property_id'] = $id;
                    $insert['charge_code_type'] = $value['charge_code_type'];
                    $insert['type'] = $value['type'];
                    $insert['frequency'] = $value['frequency'];
                    $insert['unit_type_id'] = $value['unit_type_id'];
                    $insert['charge_code'] = $value['charge_code'];
                    $insert['created_at'] = date('Y-m-d H:i:s');
                    $insert['updated_at'] = date('Y-m-d H:i:s');
                    if($value['charge_code'] == '24'){
                        $insert['is_editable'] = '0';
                        $insert['checked'] = '1';

                    }

                    $sqlData = createSqlColVal($insert);

                    $query = "INSERT INTO property_manage_charge (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $exe = $stmt->execute($insert);


                    $query2 = 'SELECT charge_code FROM company_accounting_charge_code WHERE id='.$value['charge_code'];
                    $data2 = $this->companyConnection->query($query2)->fetch();
                    $value['charge_code'] = $data2['charge_code'];

                    $value['frequency'] = ($value['frequency']=='1')?'One Time':'Monthly';
                    if($value['charge_code_type'] == '0'){
                        array_push($propertyTypeCharge,$value);
                    } elseif($value['charge_code_type'] == '1'){

                        array_push($unitTypeCharge,$value);
                    }
                }
                return array('code' => 200, 'status' => 'success', 'unitTypeCharge' => $unitTypeCharge,'propertyTypeCharge' => $propertyTypeCharge, 'message' => 'Record retrieved Successfully.');
            } else {
                return array('code' => 403, 'status' => 'warning', 'unitTypeCharge' => $unitTypeCharge,'propertyTypeCharge' => $propertyTypeCharge, 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            return array('code' => 500, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    /**
     * function to save Manage Charges
     * @return array
     */
    public function saveManageCharges(){
        $id = $_POST['property_id'];
        $data = $_POST['data'];
        if(!empty($data)){
            foreach ($data as $key=>$value){
                if($value['status'] == '1'){
                    $insertData['amount'] = $value['amount'];
                    $insertData['checked'] = $value['status'];
                    $sqlData = createSqlUpdateCase($insertData);
                    $query = "UPDATE property_manage_charge SET ".$sqlData['columnsValuesPair']." WHERE id=".$value['id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                } else {
                    $query = "DELETE FROM property_manage_charge WHERE id=".$value['id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                }
            }
        }
        return array('code' => 200, 'status' => 'success', 'message' => 'Unit Charges Added Successfully.');
    }

}

$ManageCharges = new ManageCharges();
?>