<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");


class propertyDetail extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        $parent_module = 'Properties';
        $child = '';
        $subChild = '';
        $permissionModule = ['addProperty' => 'Add Property', 'addProperty' => 'Edit Property', 'activateProperty' => 'Deactivate/Activate Property'];
        if(array_key_exists($action,$permissionModule)) {
            if (checkPermissionsNew($parent_module, $child, $subChild, $permissionModule[$action], $this->companyConnection)) {
                echo json_encode($this->$action());
            } else {
                echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => "No Permission!"));
                die;
            }
        }
        echo json_encode($this->$action());


    }

    /**
     *  function for create a property
     */
    public function addProperty() {
        try {

            $data1 = $_POST['form'];
            $data = postArray($data1);
            //Required variable array
            $required_array = ['property_id', 'property_name'];
            /* Max length variable array */
            $maxlength_array = ['property_id' => 20, 'property_name' => 30];
            //Number variable array
            $number_array = ['fax_number', 'property_squareFootage'];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            unset($data['key_code']);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                // dd($data['amenities']);
                $data['manager_id'] = (!empty($data['manager_id'])) ? serialize((array) $data['manager_id']) : '';
                $data['attach_groups'] = (!empty($data['attach_groups'])) ? serialize((array) $data['attach_groups']) : '';
                $data['amenities'] = (!empty($data['amenities'])) ? serialize((array) $data['amenities']) : '';
                $data['property_tax_ids'] = (!empty($data['property_tax_ids'])) ? serialize((array) $data['property_tax_ids']) : '';
                $data['phone_number'] = (!empty($data['phone_number'])) ? serialize((array) $data['phone_number']) : '';

                $data['key_access_codes_info'] = (!empty($data['key_access_codes_info'])) ? serialize((array) $data['key_access_codes_info']) : '';

                $data['property_year'] = (!empty($data['property_year'])) ? $data['property_year'] : null;
                $data['school_district_municipality'] = (isset($data['school_district_municipality']) && !empty($data['school_district_municipality'])) ? serialize((array) $data['school_district_municipality']) : '';
                $data['school_district_code'] = (isset($data['school_district_code']) && !empty($data['school_district_code'])) ? serialize((array) $data['school_district_code']) : '';
                $data['school_district_notes'] = (isset($data['school_district_notes']) && !empty($data['school_district_notes'])) ? serialize((array) $data['school_district_notes']) : '';
                $data['key_access_code_desc'] = (isset($data['key_access_code_desc']) && !empty($data['key_access_code_desc'])) ? serialize((array) $data['key_access_code_desc']) : '';
                $data['online_listing'] = (isset($data['online_listing']) && !empty($data['online_listing'])) ?  $data['online_listing'] : '';

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['update_at'] = date('Y-m-d H:i:s');
                $unsePhone=unserialize($data['phone_number']);



//                $data['garage_available'] = (isset($data['garage_available']) && !empty($data['garage_available'])) ? serialize((array) $data['garage_available']) : '';

                $last_renovation_date = (isset($data['last_renovation_date']) && !empty($data['last_renovation_date']) ? mySqlDateFormat($data['last_renovation_date'], null, $this->companyConnection) : NULL);
                if(isset($_POST['property_clone_id']) && $_POST['property_clone_id'] == "yes"){
                    $data['clone_status'] = 1;
                    $data['clone_id'] =$_POST['property_clone_status'];
                }

                if((isset($data['property_for_sale']) && !empty($data['property_for_sale'])) &&  $data['property_for_sale']== 'Yes'){
                    $data['status']='4';
                } elseif((isset($data['property_for_sale']) && !empty($data['property_for_sale'])) &&  $data['property_for_sale']== 'No'){
                    $data['status']='1';
                }

                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $checkAddressId = isset($_POST['propertyEditid'])?$_POST['propertyEditid']:null;
                $propertyCompleteAddress = combineAddress([$data['address1'],$data['address2'],$data['address3'],$data['address4']]);
                $checkAddress = checkPropertyAddress($propertyCompleteAddress,$checkAddressId,$this->companyConnection);

                if(!empty($checkAddress)){
                    return array('code' => 400, 'status' => 'error', 'data' => $checkAddress, 'message' => 'Property Address already exists!');
                }
                if (!isset($_POST['propertyEditid'])) {
                    $id = null;
                    //check if custom field already exists or not
                    $duplicate = checkNameAlreadyExists($this->companyConnection, 'general_property', 'property_name', $data['property_name'], $id);
                    if ($duplicate['is_exists'] == 1) {
                        if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                            return array('code' => 400, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Property already exists!');
                        }
                    }
                    if(isset($data['property_parcel_number'])){
                        $pacelno=$data['property_parcel_number'];
                    }else{
                        $pacelno='';
                    }
                    $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, '',$unsePhone[0],'0','',$pacelno,'');
                    if(isset($duplicate_record_check)){
                        if($duplicate_record_check['code'] == 503){
                            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                            die();
                        }
                    }

                    $subscription_plan=$this->companyConnection->query("SELECT subscription_plan FROM `plans_history` WHERE status ='1'")->fetch();
                    $unitPlan = $this->conn->query("SELECT max_units FROM `plans` WHERE id =".$subscription_plan['subscription_plan'])->fetch();

                    $totalUnitsLeft=$this->companyConnection->query("SELECT sum(no_of_units) as no_of_units FROM `general_property` WHERE status ='1'")->fetch();
                    if($unitPlan['max_units'] < $totalUnitsLeft['no_of_units']){
                        return array('code' => 500, 'status' => 'warning', 'message' => 'Units Limit has been Exceed!');
                    }
                    if (getRecordCount($this->companyConnection, 'general_property') == 0);
                    $query = "INSERT INTO general_property (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();


                    $queryFetch = $this->companyConnection->query("SELECT * FROM general_property WHERE id='$id'");
                    $property = $queryFetch->fetch();
                    $insertRenovation = [];
                    $insertRenovation['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $insertRenovation['object_type'] = 'property';
                    $insertRenovation['object_id'] = $id;
                    $insertRenovation['status'] = '1';

                    $defaulttime = date('h:i:a');
                    $insertRenovation['last_renovation_time'] = (isset($data['last_renovation_time'])?substr($data['last_renovation_time'], 0, strrpos( $data['last_renovation_time'], ':')):$defaulttime);
                    $insertRenovation['last_renovation_time'] = (isset($data['last_renovation_time']) && !empty($data['last_renovation_time'])) ? mySqlTimeFormat($data['last_renovation_time']) : '';



                    $insertRenovation['last_renovation_date'] = (isset($data['last_renovation_date']) && !empty($data['last_renovation_date']) ? mySqlDateFormat($data['last_renovation_date'],null,$this->companyConnection) : null);
                    $insertRenovation['last_renovation_description'] = (isset($data['last_renovation_description']) && !empty($data['last_renovation_description']) ? $data['last_renovation_description'] : NULL);
                    $insertRenovation['created_at'] = date('Y-m-d H:i:s');
                    $insertRenovation['updated_at'] = date('Y-m-d H:i:s');
                    //add renovation details
                    $renovationData = createSqlColVal($insertRenovation);
                    $query1 = "INSERT INTO renovation_details (" . $renovationData['columns'] . ") VALUES (" . $renovationData['columnsValues'] . ")";
                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute($insertRenovation);
                    $getLatLong=$this->getLatLong($id);

                    return array('code' => 200, 'status' => 'success', 'pId' => $property['id'],'portfolio_id' => $property['portfolio_id'], 'data' => $stmt, 'message' => 'Record added successfully');
                } else {
                    $propertyEditid = $_POST['propertyEditid'];
                    $unsePhone=unserialize($data['phone_number']);
                    $duplicate_record_check=checkMultipleDuplicatesEdit($this->companyConnection,'',$unsePhone[0],'0','',$data['property_parcel_number'],'',$propertyEditid);
                    if(isset($duplicate_record_check)){
                        if($duplicate_record_check['code'] == 503){
                            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                            die();
                        }
                    }
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE general_property SET " . $sqlData['columnsValuesPair'] . " where id='$propertyEditid'";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);

                    $getLatLong=$this->getLatLong($propertyEditid);
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addPortfolioPopup() {
        try {
            $data = $_POST['form'];
            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data, 'true');
            //Required variable array
            $required_array = ['portfolio_id', 'portfolio_name'];
            /* Max length variable array */
            $maxlength_array = ['portfolio_id' => 20, 'portfolio_name' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id))
                    unset($data['id']);
                if (!empty($custom_field)) {
                    foreach ($custom_field as $key => $value) {
                        if ($value['data_type'] == 'date' && !empty($value['default_value']))
                            $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                        if ($value['data_type'] == 'date' && !empty($value['value']))
                            $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                        continue;
                    }
                }
                $insert = [];
                $insert['portfolio_id'] =  (isset($data['portfolio_id']) && !empty($data['portfolio_id'])) ? $data['portfolio_id'] : '';
                $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $insert['portfolio_name'] = $data['portfolio_name'];
                $insert['is_default'] = isset($data['is_default']) ? '1' : '0';
                $insert['is_editable'] = '1';
                $insert['custom_field'] = (count($custom_field) > 0) ? serialize($custom_field) : null;
                $insert['status'] = '1';
                $insert['created_at'] = date('Y-m-d H:i:s');
                $insert['updated_at'] = date('Y-m-d H:i:s');

                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_property_portfolio', 'portfolio_name', $insert['portfolio_name'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Portfolio already exists!');
                    }
                }

                //Save Data in Company Database
                $sqlData = createSqlColVal($insert);
                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_field) > 0) {
                    foreach ($custom_field as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }
                if (getRecordCount($this->companyConnection, 'company_property_portfolio') == 0)
                    $insert['is_default'] = '1';
                if ($insert['is_default'] == 1) {
                    $updateDafault = createSqlColValPair(['is_default' => 0]);
                    updateDefaultField($this->companyConnection, $updateDafault, 'company_property_portfolio');
                }
                $query = "INSERT INTO company_property_portfolio (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($insert);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addManagerPopup() {
        try {
            $data = $_POST['form'];
            $custom_field = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];
            $data = postArray($data, 'true');

            //Required variable array
            $required_array = ['first_name', 'last_name', 'email'];
            /* Max length variable array */
            $maxlength_array = ['first_name' => 30, 'last_name' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id))
                    unset($data['id']);
                //check if custom field already exists or not
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'users', 'email', $data['email'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Email already exists');
                    }
                }

                $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, $data['email'],'','0','','','');
                if(isset($duplicate_record_check)){
                    if($duplicate_record_check['code'] == 503){
                        echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                        die();
                    }
                }


                $domain = explode('.', $_SERVER['HTTP_HOST']);
                $domain = array_shift($domain);
                $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                $data['status'] = '1';
                $data['role'] = '2';
                $data['parent_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $password = randomString();
                $data['actual_password'] = $password;
                $data['password'] = md5($password);
                $data['domain_name'] = $domain;
                $data['user_type'] = 1;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
//                $this->sendMail($id);
                $getname = "SELECT * FROM users where role IN ('1','2')";
                $getdata = $this->companyConnection->query($getname)->fetchAll();

                return array('code' => 200, 'selected' => $id, 'getdata' => $getdata, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addAttachgroupPopup() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');

            //Required variable array
            $required_array = ['group_name', 'description'];
            /* Max length variable array */
            $maxlength_array = ['group_name' => 30, 'description' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_property_groups', 'group_name', $data['group_name'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Group Name already exists!');
                    }
                }
                $data['status']="1";
                //Save Data in Company Database
                $data['status']="1";
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_property_groups (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                $getname = "SELECT * FROM   company_property_groups";
                $getdata = $this->companyConnection->query($getname)->fetchAll();
                return array('code' => 200, 'selected' => $id, 'getdata' => $getdata, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addPropertytpePopup() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');

            //Required variable array
            $required_array = ['property_type', 'description'];
            /* Max length variable array */
            $maxlength_array = ['property_type' => 30, 'description' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_property_type', 'property_type', $data['property_type'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Property Type already exists!');
                    }
                }
//               $xmlFunction = $this->XML();
//                dd("test function");
                $data['status'] = 1;
                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO  company_property_type (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addPropertystylePopup() {
        try {

            $data = $_POST['form'];
            $data = postArray($data, 'true');

            //Required variable array
            $required_array = ['property_style', 'description'];
            /* Max length variable array */
            $maxlength_array = ['property_style' => 30, 'description' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_property_style', 'property_style', $data['property_style'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Property Style already exists!');
                    }
                }
                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO  company_property_style (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addPropertysubtypePopup() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');

            //Required variable array
            $required_array = ['property_subtype', 'description'];
            /* Max length variable array */
            $maxlength_array = ['property_subtype' => 30, 'description' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_property_subtype', 'property_subtype', $data['property_subtype'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Property Sub-Type already exists!');
                    }
                }
                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO   company_property_subtype (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addGarageavailPopup() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');
            //Required variable array
            $required_array = ['garage_avail'];
            /* Max length variable array */
            $maxlength_array = ['garage_avail' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_garage_avail', 'garage_avail', $data['garage_avail'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Name already exists!');
                    }
                }
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['update_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO   company_garage_avail (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function fetchZipcode() {
        $zip = $_POST['zip'];
        $sql = "SELECT * FROM zip_code_master WHERE zip_code=" . $zip . "";
        $data = $this->companyConnection->query($sql)->fetch();
        return array('zip_code' => $data['zip_code'], 'city' => $data['city'], 'state' => $data['state'], 'country' => $data['country'], 'status' => 'success');
    }

    public function fetchPortfolioname() {
        $html = '';
        $sql = "SELECT * FROM company_property_portfolio";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = "<option value=''>Select</option>";
        foreach ($data as $d) {

            $html.= "<option value='" . $d['id'] . "' " . (($d['is_default'] == '1') ? "selected" : "") . ">" . $d['portfolio_name'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function fetchPropertytype() {
        $html = '';
        $sql = "SELECT * FROM company_property_type";
        $data = $this->companyConnection->query($sql)->fetchAll();

        foreach ($data as $d) {
            $html.= '<option value="' . $d['id'] . '"' . (($d['is_default'] == '1') ? "selected" : "") . '>' . $d['property_type'] . '</option>';
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function fetchPropertystyle() {
        $html = '';
        $sql = "SELECT * FROM company_property_style";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html ='<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= '<option value="' . $d['id'] . '" ' . (($d['is_default'] == 1) ? "selected" : "") . '>' . $d['property_style'] . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function fetchPropertysubtype() {
        $html = '';
        $sql = "SELECT * FROM company_property_subtype";
        $data = $this->companyConnection->query($sql)->fetchAll();

        foreach ($data as $d) {
            $html.= '<option value="' . $d['id'] . '" ' . (($d['is_default'] == 1) ? "selected" : "") . '>' . $d['property_subtype'] . '</option>';
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function fetchGaragedata() {
        $html = '';
        $sql = "SELECT * FROM  company_garage_avail";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {
            $html.= '<option value=' . $d['id'] . '>' . $d['garage_avail'] . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function fetchAllmanagers() {
        $html = '';
        $sql = "SELECT * FROM  users WHERE role IN ('1','2') OR id=1 AND deleted_at IS NULL order by first_name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {
            $fullname = ucfirst($d['first_name']) . ' ' . ucfirst($d['last_name']);
            $html.= '<option data-id="'.$d['email'].'" value=' . $d['id'] . '>' . $fullname . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function fetchAllPropertyManagers() {
        $html = '<option value="">Select</option>';
        $sql = "SELECT * FROM  users WHERE role = '2' order by first_name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {
            $fullName = userName($d['id'],$this->companyConnection);
            $html.= '<option value=' . $d['id'] . '>' . $fullName . '</option>';
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function fetchPropertymanagers() {
        $html = '';
        $property_id = $_POST['property_id'];
        $sql1 = "SELECT manager_id FROM  general_property WHERE id = '$property_id'";
        $data1 = $this->companyConnection->query($sql1)->fetch();
        $managers_id = unserialize($data1['manager_id']);
        $manager_id = implode(",", $managers_id);
        $ids =  trim($manager_id,"'");



        $sql = "SELECT * FROM  users WHERE id IN ($ids) and role = '2' order by first_name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();

        foreach ($data as $d) {
            $fullname = ucfirst($d['first_name']) . ' ' . ucfirst($d['last_name']);
            $html.= '<option value=' . $d['id'] . '>' . $fullname . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function fetchAllAttachgroups() {
        $html = '';
        $sql = "SELECT * FROM   company_property_groups WHERE status='1'";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {
            $html.= '<option value=' . $d['id'] . '>' . $d['group_name'] . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function fetchOwners() {
        $html = '';
        if (!isset($_POST['property_id'])) {
            $sql = "SELECT * FROM  users WHERE user_type='4' AND status = '1' order by first_name ASC";
            $data = $this->companyConnection->query($sql)->fetchAll();
            $html ='<option value="default">Select</option>';
            foreach ($data as $d) {
                $fullname = ucfirst($d['first_name']) . ' ' . ucfirst($d['last_name']);
                $html.= '<option value=' . $d['id'] . '>' . $fullname . '</option>';
            }
        } else {
            $sql = "SELECT * FROM  users WHERE user_type='4' AND status = '1' order by first_name ASC";
            $data = $this->companyConnection->query($sql)->fetchAll();

            $linkowner = "SELECT * FROM  general_property WHERE id='" . $_POST['property_id'] . "' AND deleted_at IS NULL";
            $linkownerData = $this->companyConnection->query($linkowner)->fetch();

            $html ='<option value="default">Select</option>';
            foreach ($data as $d) {
                $fullname = ucfirst($d['first_name']) . ' ' . ucfirst($d['last_name']);
                $html.= '<option value=' . $d['id'] . '>' . $fullname . '</option>';
            }
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function addOwnertab() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = ['owner_percentowned', 'payment_type'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                if(!isset($_POST['propertyEditid'])){
                    $pid=$_POST['property_id'];
                }else{
                    $pid=$_POST['propertyEditid'];
                }

                $userArray = (array) $_POST['user_id'];
                $userPercentageOwner = (array) $data['owner_percentowned'];
                if(!empty($userArray)){
                    foreach ($userArray as $key=>$value){
                        $select_query = $this->companyConnection->query("SELECT * FROM owner_property_relation WHERE owner_id='".$value."' AND property_id='".$pid."' ")->fetch();
//                       dd($select_query);
                        if(!empty($select_query)){
                            return array('code' => 400, 'status' => 'error', 'message' => 'Owner is already asign to another property!');
                        }
                    }
                }

                $data['owner_id'] = !empty($_POST['user_id']) ? serialize((array) $_POST['user_id']) : '';
                $data['owner_percentowned'] = !empty($data['owner_percentowned']) ? serialize((array) $data['owner_percentowned']) : '';
                $data['vendor_1099_payer'] = !empty($data['vendor_1099_payer']) ? serialize($data['vendor_1099_payer']) : '';
                // $data['property_id'] = $_POST['property_id'];

                $sqlData = createSqlColValPair($data);
                //Save Data in Company Database
                $query = "UPDATE general_property SET " . $sqlData['columnsValuesPair'] . " where id=" .$pid;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                $this->addOwnerRelation($userArray,$userPercentageOwner,$pid);
                return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');

            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addOwnerRelation($users,$percentage,$property_id){
        try{
            $array_percent = [];
            foreach($percentage as $key => $value)
            {
                if($key%2 != 0) {
                    array_push($array_percent,$value);
                } else {
                    continue;
                }
            }
            $delete_query = $this->companyConnection->query("DELETE FROM owner_property_relation WHERE property_id=".$property_id)->execute();
            foreach ($users as $key=>$value){
                //Save Data in Company Database
                if($value != 'default') {
                    $data = [];
                    $data['owner_id'] = $value;
                    $data['property_id'] = $property_id;
                    $data['percentage'] = (isset($array_percent[$key]) && !empty($array_percent[$key])) ? $percentage[$key] : NULL;
                    $data['created_at'] = date('Y-m-d');
                    $data['updated_at'] = date('Y-m-d');
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO owner_property_relation (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
            }
        } catch (Exception $exception){
            dd($exception);
        }
    }

    public function addLatefee() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            if (isset($data['one_time_radio']) && $data['one_time_radio'] == 'on') {
                $data['checkbox'] = '1';
            } elseif (isset($data['daily_time_radio']) && $data['daily_time_radio'] == 'on') {
                $data['checkbox'] = '2';
            }
            unset($data['one_time_radio']);
            unset($data['daily_time_radio']);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if (!isset($_POST['propertyEditid'])):
                    $pid = $_POST['property_id'];
                else:
                    $pid = $_POST['propertyEditid'];
                endif;
                $queryFetch = $this->companyConnection->query("SELECT * FROM  late_fee_management WHERE property_id=$pid");
                $checkData = $queryFetch->fetch();
                if (empty($checkData)) {
                    $data['property_id']=$pid;
                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database
                    $query = "INSERT INTO late_fee_management (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
                } else {
                    $data['property_id']=$pid;
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE late_fee_management SET " . $sqlData['columnsValuesPair'] . " where property_id='$pid'";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addKeytag() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            unset($data['KeyAddTrack']);
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = ['key_tag', 'description', 'total_keys'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (!isset($_POST['propertyEditid'])):
                $data['property_id'] = $_POST['property_id'];
            else:
                $data['property_id'] = $_POST['propertyEditid'];
            endif;
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'key_tracker', 'key_tag', $data['key_tag'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Key Tag already exists!');
                    }
                }
                if (empty($id)) {
                    $data['available_keys'] = $data['total_keys'];
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    unset($data['id']);
                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database
                    $query = "INSERT INTO key_tracker (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                } else {
                    $chekout_sql = 'SELECT SUM(checkout_keys) AS total_checkout_keys FROM key_checkout WHERE key_id=' . $data['id'];
                    // $row = $chekout_sql->fetch(PDO::FETCH_ASSOC);
                    $stmt = $this->companyConnection->prepare($chekout_sql);
                    $stmt->execute();
                    $total_checkout_keys = 0;
                    while ($row1 = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $total_checkout_keys += $row1['total_checkout_keys'];
                    }
                    $keytag = $data['key_tag'];
                    $keydesc = $data['description'];
                    $keytotal = $data['total_keys'];
                    $keyCreated = date('Y-m-d H:i:s');
                    $keyUpdated = date('Y-m-d H:i:s');
                    $availablekeys = $data['total_keys'] - $total_checkout_keys;
                    if ($availablekeys < 0) {
                        return array('code' => 400, 'status' => 'error', 'message' => 'You cannot update the key count less than the allocated keys.');
                    }
                    $query = "UPDATE key_tracker SET key_tag='$keytag',description='$keydesc',total_keys='$keytotal',available_keys='$availablekeys',created_at='$keyCreated',updated_at='$keyUpdated' where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function deleteKey() {
        //delete
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'key_tracker');

            $sql = "DELETE FROM key_tracker WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute();
            return array(['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.']);

        }catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function getKey() {
        try {
            $id = $_POST['id'];
            //check if custom field already exists or not
            $sql = "SELECT * FROM key_tracker WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
            } else {
                return array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }

    public function addManagementFee() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            unset($data['ManagementFeeclick']);
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = ['management_type', 'management_value'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if (!isset($_POST['propertyEditid'])):
                    $pid = $_POST['property_id'];
                else:
                    $pid = $_POST['propertyEditid'];
                endif;

                $queryFetch = $this->companyConnection->query("SELECT * FROM  management_fees WHERE property_id=$pid");
                $checkData = $queryFetch->fetch();
                unset($data['id']);
                if (empty($checkData)) {
                    $data['property_id']=$pid;
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database

                    $query = "INSERT INTO management_fees (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                } else {
                    $data['property_id']=$pid;
                    $data['updated_at'] = date('Y-m-d H:i:s');
//                    dd($data);
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE management_fees SET " . $sqlData['columnsValuesPair'] . " where property_id='$pid'";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);

                    $data1['update_at'] = date('Y-m-d H:i:s');
                    $sqlData1 = createSqlUpdateCase($data1);
                    $query1 = "UPDATE general_property SET " . $sqlData1['columnsValuesPair'] . " where property_id='$pid'";
                    $stmt = $this->companyConnection->prepare($query1);
                    $stmt->execute($sqlData1['data']);

                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            dd($e);
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addManagementInfo() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $property_id = $_POST['property_id'];
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = ['management_type', 'management_value'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $data['management_start_date'] = mySqlDateFormat($data['management_start_date'], null, $this->companyConnection);
                $data['management_end_date'] = mySqlDateFormat($data['management_end_date'], null, $this->companyConnection);
                if (!isset($_POST['propertyEditid'])):
                    $pid = $_POST['property_id'];
                else:
                    $pid = $_POST['propertyEditid'];
                endif;
                $queryFetch = $this->companyConnection->query("SELECT * FROM  management_info WHERE property_id=$pid");
                $checkData = $queryFetch->fetch();
                if(empty($checkData)){
                    $data['property_id']=$pid;
                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database
                    $query = "INSERT INTO management_info (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                }else{
                    $data['property_id']=$pid;
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE management_info SET " . $sqlData['columnsValuesPair'] . " where property_id='$pid'";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addMaintenanceinformation() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $property_id = $_POST['property_id'];
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = ['management_type', 'management_value'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['Insurance_expiration'] = mySqlDateFormat($data['Insurance_expiration'], null, $this->companyConnection);
                $data['warranty_expiration'] = mySqlDateFormat($data['warranty_expiration'], null, $this->companyConnection);
                if (!isset($_POST['propertyEditid'])):
                    $pid = $_POST['property_id'];
                else:
                    $pid = $_POST['propertyEditid'];
                endif;
                $queryFetch = $this->companyConnection->query("SELECT * FROM  maintenance_information WHERE property_id=$pid");
                $checkData = $queryFetch->fetch();
                if(empty($checkData)){

                    $data['property_id']=$pid;
                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database
                    $query = "INSERT INTO maintenance_information (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                }else{
                    $data['property_id']=$pid;
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE maintenance_information SET " . $sqlData['columnsValuesPair'] . " where property_id='$pid'";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addMortgageinfo() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $property_id = $_POST['property_id'];
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = ['mortgage_amount', 'property_assessed_amount', 'mortgage_start_date', 'term', 'interest_rate', 'financial_institution'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable arraypl
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['mortgage_start_date'] = mySqlDateFormat($data['mortgage_start_date'], null, $this->companyConnection);
                if (!isset($_POST['propertyEditid'])):
                    $pid = $_POST['property_id'];
                else:
                    $pid = $_POST['propertyEditid'];
                endif;
                $queryFetch = $this->companyConnection->query("SELECT * FROM  mortgage_information WHERE property_id=$pid");

                $checkData = $queryFetch->fetch();
                if(empty($checkData)){
                    $data['property_id']=$pid;
                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database
                    $query = "INSERT INTO mortgage_information (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                }else{
                    $data['property_id']=$pid;
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE mortgage_information SET " . $sqlData['columnsValuesPair'] . " where property_id='$pid'";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addPropertyloan() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['loan_start_date'] = (isset($data['loan_start_date']) && !empty($data['loan_start_date'])) ? mySqlDateFormat($data['loan_start_date'], null, $this->companyConnection) : '';
                $data['loan_maturity_date'] = (isset($data['loan_maturity_date']) && !empty($data['loan_maturity_date'])) ? mySqlDateFormat($data['loan_maturity_date'], null, $this->companyConnection) : '';
                $data['interest_only'] = (isset($data['interest_only']) && !empty($data['interest_only'])) ?  $data['interest_only'] : '0';

                if (!isset($_POST['propertyEditid'])):
                    $pid = $_POST['property_id'];
                else:
                    $pid = $_POST['propertyEditid'];
                endif;
                $queryFetch = $this->companyConnection->query("SELECT * FROM  property_loan WHERE property_id=$pid");
                $checkData = $queryFetch->fetch();

                if (empty($checkData)) {
                    $data['property_id']=$pid;
                    unset($data['id']);
                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database
                    $query = "INSERT INTO  property_loan (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                } else {
                    $data['property_id']=$pid;
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE property_loan SET " . $sqlData['columnsValuesPair'] . " where property_id='$pid'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addPropertynotes() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;

                if (empty($id)) {
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    unset($data['id']);
                    if (isset($_POST['property_id']) && $_POST['property_id'] != 'undefined'):
                        $data['property_id'] = $_POST['property_id'];
                    else:
                        $data['property_id'] = $_POST['propertyEditid'];
                    endif;
                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database
                    $query = "INSERT INTO  property_notes (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                } else {
                    $notes = $data['notes'];
                    $query = "UPDATE property_notes SET notes='$notes',updated_at ='".date('Y-m-d H:i:s')."' where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function getNotes() {
        try {
            $id = $_POST['id'];
            //check if custom field already exists or not
            $sql = "SELECT * FROM property_notes WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully');
            } else {
                return array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }

    public function deleteNotes() {
        //delete
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'property_notes');

            $sql = "UPDATE property_notes SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            echo json_encode(['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.']);
            return;
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function addInsurancetype() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $property_id = $_POST['property_id'];
            //Required variable array
            $required_array = ['insurance_type'];
            /* Max length variable array */
            $maxlength_array = ['insurance_type' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_property_insurance', 'insurance_type', $data['insurance_type'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Insurance Type already exists');
                    }
                }

                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['property_id'] = $property_id;
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_property_insurance (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
            }
            return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function fetchAllinsurancetypes() {
        $html = '';
        $sql = "SELECT * FROM company_property_insurance";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html .= '<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= '<option value=' . $d['id'] . '>' . $d['insurance_type'] . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function fetchAllpolicytypes() {
        $html = '';
        $sql = "SELECT * FROM company_property_policy";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = '<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= '<option value=' . $d['id'] . '>' . $d['policy_type'] . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function addPolicytype() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $property_id = $_POST['property_id'];
            //Required variable array
            $required_array = ['insurance_type'];
            /* Max length variable array */
            $maxlength_array = ['insurance_type' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['property_id'] = $property_id;
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_property_policy (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addPropertyinsurance() {
        try {
            $data = $_REQUEST;
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation


            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $id = (isset($_POST['property_ins_id']) && !empty($_POST['property_ins_id'])) ? $_POST['property_ins_id'] : null;
                if (isset($_POST['property_id']) && $_POST['property_id'] != 'undefined'):
                    $data['property_id'] = $_POST['property_id'];
                else:
                    $data['property_id'] = $_POST['propertyEditid'];
                endif;

                unset($data['insurance_type']);
                unset($data['policy_type']);
                unset($data['PropertyInsuranceid']);
                unset($data['action']);
                unset($data['class']);
                unset($data['property_ins_id']);

                if (empty($id)) {
                    $data['created_at']=date('Y-m-d H:i:s');
                    $data['updated_at']=date('Y-m-d H:i:s');

                    //Save Data in Company Database
                    $i = 0;
                    foreach ($_POST['insurance_company_name'] as $companyName) {

                        $data['company_insurance_type'] = $_POST['company_insurance_type'][$i];
                        $data['insurance_company_name'] = $_POST['insurance_company_name'][$i];
                        $data['policy'] = $_POST['policy'][$i];
                        $data['policy_name'] = $_POST['policy_name'][$i];
                        $data['agent_name'] = $_POST['agent_name'][$i];
                        $data['insurance_country_code'] = $_POST['insurance_country_code'][$i];
                        $data['insurance_country_code'] = $_POST['insurance_country_code'][$i];
                        $data['property_insurance_email'] = $_POST['property_insurance_email'][$i];
                        $data['coverage_amount'] = $_POST['coverage_amount'][$i];
                        $data['annual_premium'] = $_POST['annual_premium'][$i];
                        $data['start_date'] = mySqlDateFormat($_POST['start_date'][$i], null, $this->companyConnection);
                        $data['phone_number'] = $_POST['phone_number'][$i];
                        $data['end_date'] = mySqlDateFormat($_POST['end_date'][$i], null, $this->companyConnection);
                        $data['renewal_date'] = mySqlDateFormat($_POST['renewal_date'][$i], null, $this->companyConnection);
                        $data['notes'] = $_POST['notes'][$i];

                        unset($data['propertyEditid']);
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO  property_insurance (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                        $i++;
                    }
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'id' => $id, 'message' => 'Record added successfully');
                } else {
                    $data['updated_at']=date('Y-m-d H:i:s');
                    $i = 0;
                    foreach ($_POST['insurance_company_name'] as $companyName) {
                        $data['company_insurance_type'] = $_POST['company_insurance_type'][$i];
                        $data['insurance_company_name'] = $_POST['insurance_company_name'][$i];
                        $data['policy'] = $_POST['policy'][$i];
                        $data['policy_name'] = $_POST['policy_name'][$i];
                        $data['agent_name'] = $_POST['agent_name'][$i];
                        $data['insurance_country_code'] = $_POST['insurance_country_code'][$i];
                        $data['insurance_country_code'] = $_POST['insurance_country_code'][$i];
                        $data['property_insurance_email'] = $_POST['property_insurance_email'][$i];
                        $data['coverage_amount'] = $_POST['coverage_amount'][$i];
                        $data['annual_premium'] = $_POST['annual_premium'][$i];
                        $data['start_date'] = mySqlDateFormat($_POST['start_date'][$i], null, $this->companyConnection);
                        $data['phone_number'] = $_POST['phone_number'][$i];
                        $data['end_date'] = mySqlDateFormat($_POST['end_date'][$i], null, $this->companyConnection);
                        $data['renewal_date'] = mySqlDateFormat($_POST['renewal_date'][$i], null, $this->companyConnection);
                        $data['notes'] = $_POST['notes'][$i];
                        unset($data['propertyEditid']);
                        $sqlData = createSqlColValPair($data);

                        $query = "UPDATE property_insurance SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                        $i++;
                    }


                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function getPropertyinsurance() {
        try {
            $id = $_POST['id'];
            //check if custom field already exists or not
            $sql = "SELECT * FROM  property_insurance WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {


                $start_date = dateFormatUser($data['start_date'], null, $this->companyConnection);
                $end_date = dateFormatUser($data['end_date'], null, $this->companyConnection);
                $renewal_date = dateFormatUser($data['renewal_date'], null, $this->companyConnection);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'start_date'=>$start_date,'end_date'=>$end_date,'renewal_date'=>$renewal_date ,'message' => 'Record retrieved successfully');
            } else {
                return array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }

    public function deletePropertyinsurance() {
        //delete
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'key_tracker');

            $sql = "UPDATE  property_insurance SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            return array ('status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }

    public function addWarrantyinfomation() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            unset($data['KeyAddTrack']);
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!isset($_POST['propertyEditid'])):
                    $data['property_id'] = $_POST['property_id'];
                else:
                    $data['property_id'] = $_POST['propertyEditid'];
                endif;
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'warranty_information', 'key_fixture_name', $data['key_fixture_name'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Fixture already exists!');
                    }
                }
                $data['maintenance_reminder'] = mySqlDateFormat($data['maintenance_reminder'], null, $this->companyConnection);
                $data['warranty_expiration_date'] = mySqlDateFormat($data['warranty_expiration_date'], null, $this->companyConnection);
                $data['insurance_expiration_date'] = mySqlDateFormat($data['insurance_expiration_date'], null, $this->companyConnection);
                if (empty($id)) {
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');
                    unset($data['id']);
                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database
                    $query = "INSERT INTO warranty_information (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                } else {

                    $data['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE warranty_information SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function deleteWarrantyInfo() {
        //delete
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'key_tracker');

            $sql = "UPDATE  warranty_information SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            return array('status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.');

        } catch (Exception $exception) {
            return array ('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());

        }
    }

    public function getWarrantyInfo() {
        try {
            $id = $_POST['id'];
            //check if custom field already exists or not
            $sql = "SELECT * FROM  warranty_information WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                $maintenance_reminder = dateFormatUser($data['maintenance_reminder'], null, $this->companyConnection);
                $warranty_expiration_date = dateFormatUser($data['warranty_expiration_date'], null, $this->companyConnection);
                $insurance_expiration_date = dateFormatUser($data['insurance_expiration_date'], null, $this->companyConnection);

                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record retrieved successfully', 'maintenance_reminder' =>
                    $maintenance_reminder, 'warranty_expiration_date' => $warranty_expiration_date, 'insurance_expiration_date' => $insurance_expiration_date);
            } else {
                return array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }

    public function addFixturetype() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');

            //Required variable array
            $required_array = ['fixture_type', 'fixture_desc'];
            /* Max length variable array */
            $maxlength_array = ['fixture_type' => 30, 'fixture_desc' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'fixture_types', 'fixture_type', $data['fixture_type'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Fixture Type already exists!');
                    }
                }
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO fixture_types (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function fetchAllfixture() {
        $html = '';
        $sql = "SELECT * FROM  fixture_types";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html .='<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= '<option value=' . $d['id'] . '>' . $d['fixture_type'] . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function getVendorsdata() {

        try {
            $search = @$_REQUEST['q'];
            $sql = "SELECT * FROM users  where user_type='3' AND status = '1' and first_name LIKE '%$search%' order by name asc";
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);
            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['name'];
                    $data['rows'][$key]['company_name'] = $user_data['company_name'];
                    $data['rows'][$key]['phone_number'] = $user_data['phone_number'];
                    $data['rows'][$key]['address1'] = $user_data['address1'] . ', ' . $user_data['address2'] . ' ' . $user_data['address3'];
                    $data['rows'][$key]['email'] = $user_data['email'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }

    public function addOwneredvendors() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);

            //Required variable array
            $required_array = ['vendor_name', 'vendor_address'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $vendor=$data['vendor_name'];
                $data['vendor_id']=$_POST['owner_pre_vendorID'];
                if (!isset($_POST['propertyEditid'])):
                    $pid = $_POST['property_id'];
                else:
                    $pid = $_POST['propertyEditid'];
                endif;

                $queryFetch = $this->companyConnection->query("SELECT * FROM  owner_blacklist_vendors WHERE vendor_name='".$vendor."' and status='1' and property_id=$pid");
                $checkData = $queryFetch->fetch();
                if (!empty($checkData)) {
                    return (array('code' => 500, 'status' => 'error', 'message' => 'Vendor already exists!'));
                }
                $data['property_id']=$pid;
                $data['status'] = '1';
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO owner_blacklist_vendors (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function deleteVendor() {
        //delete
        try {
            $id = $_POST['id'];
//            echo "<pre>";print_R($id);die;
            foreach ($id as $idds) {
                $sql = "DELETE FROM owner_blacklist_vendors where id=" . $idds;
                $stmt = $this->companyConnection->prepare($sql);
                $stmt->execute();
            }
            return array('status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function addBlacklistvendors() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);

            //Required variable array
            $required_array = ['vendor_name', 'vendor_address'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $vendor=$data['vendor_name'];
                $data['vendor_id']=$_POST['blacklist_vendorID'];
                if (!isset($_POST['propertyEditid'])):
                    $pid = $_POST['property_id'];
                else:
                    $pid = $_POST['propertyEditid'];
                endif;
                $queryFetch = $this->companyConnection->query("SELECT * FROM  owner_blacklist_vendors WHERE vendor_name='".$vendor."' and status='2' and property_id=$pid");
                $checkData = $queryFetch->fetch();
                if (!empty($checkData)) {
                    return (array('code' => 500, 'status' => 'error', 'message' => 'Vendor already exists!'));
                }
                $data['property_id']=$pid;
                $data['status'] = '2';
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO owner_blacklist_vendors (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function deleteBlacklistVendor() {
        //delete
        try {
            $id = $_POST['id'];
            foreach ($id as $idds) {
                $data = date('Y-m-d H:i:s');
                $sql = "DELETE FROM owner_blacklist_vendors where id=" . $idds;
                $stmt = $this->companyConnection->prepare($sql);
                $stmt->execute();
            }
            return array('status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function addBankdetails() {
        try {
            $data = $_REQUEST['form'];
            $data = serializeToPostArray($data);
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                if (isset($_POST['property_id']) && $_POST['property_id'] != 'undefined') {
                    $pid = $_POST['property_id'];
                } else {
                    $pid = $_POST['propertyEditid'];
                }



                $queryFetch = $this->companyConnection->query("SELECT * FROM  property_bank_details WHERE property_id=$pid");
                $checkData = $queryFetch->fetch();
                if(!empty($checkData)){
                    $sql = "DELETE FROM property_bank_details WHERE property_id=" . $pid;
                    $stmt = $this->companyConnection->prepare($sql);
                    $stmt->execute();
                }
                $i = 0;
                $insert = [];
                $insert['property_id'] = $pid;

                foreach ($data['account_name'] as $key => $account_name) {


                    $insert['chart_of_account'] = $data['chart_of_account'][$key];
                    $insert['account_type'] = $data['account_type'][$key];
                    $insert['bank_name'] = $data['bank_name'][$key];
                    $insert['bank_routing'] = $data['bank_routing'][$key];
                    $insert['bank_account'] = $data['bank_account'][$key];
                    $insert['default_security_deposit'] = $data['default_security_deposit'][$key];
                    $insert['bank_account_min_reserve_amount'] = $data['bank_account_min_reserve_amount'][$key];
                    $insert['description'] = $data['description'][$key];
                    $insert['account_name'] = $data['account_name'][$key];
                    $insert['is_default'] = $data['default_value'][$key];
                    $insert['bank_id'] = $data['account_name'][$key];

                    $querysourceId = $this->companyConnection->query("SELECT source_id FROM  company_accounting_bank_account WHERE id=".$insert['bank_id']);
                    $checkDatasourceId = $querysourceId->fetch();

                    $insert['source_id'] = $checkDatasourceId['source_id'];
//                        dd($insert);
                    $sqlData = createSqlColVal($insert);
                    $query = "INSERT INTO property_bank_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($insert);
                    $i++;
                }
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');

            }
        } catch (PDOException $e) {

            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    /**
     * function to grt  a property detail
     */
    public function gerPropertyDetail() {
        $id = $_POST['id'];
//        dd($id);
        $sql = "SELECT * FROM general_property WHERE id=" . $id;
        $data = $this->companyConnection->query($sql)->fetch();
        $school_district_muncipiality = [];
        $school_district_code = [];
        $school_district_notes = [];
        $result = [];

        if (isset($data["school_district_municipality"]) && !empty($data["school_district_municipality"]))
            $school_district_muncipiality = unserialize($data["school_district_municipality"]);
        if (isset($data["school_district_code"]) && !empty($data["school_district_code"]))
            $school_district_code = unserialize($data["school_district_code"]);
        if (isset($data["school_district_notes"]) && !empty($data["school_district_notes"]))
            $school_district_notes = unserialize($data["school_district_notes"]);
        $result = array_map(function ($name, $type, $price) {
            return array_combine(
                ['school_district', 'code', 'notes'], [$name, $type, $price]
            );
        }, $school_district_muncipiality, $school_district_code, $school_district_notes);
        $get_default_settings =  getSingleRecord($this->companyConnection, ['column' => 'user_id', 'value' => $_SESSION[SESSION_DOMAIN]['cuser_id']], 'default_settings');
        return array('property_detail' => $data, 'school_district_muncipiality_data' => $result,'default_settings'=>$get_default_settings, 'status' => 'success');
    }

    public function fetchAllUnittypes() {
        $html = '';
        $sql = "SELECT * FROM company_unit_type WHERE status = '1' AND deleted_at IS NULL";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $defaultsettings = "SELECT * FROM default_settings WHERE id=" . $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $defaultsettingsData = $this->companyConnection->query($defaultsettings)->fetch();
        $html = '<option value="default">Select</option>';
        foreach ($data as $d) {
            $html.= "<option value='" . $d['id'] . "' " . (($d['is_default'] == '1') ? "selected" : "") . ">" . $d['unit_type'] . "</option>";
        }


        return array('data' => $html, 'status' => 'success', 'default_rent' => $defaultsettingsData['default_rent']);
    }

    public function fetchAllManageUnittypes() {
        $html = '';
        $sql = "SELECT * FROM company_unit_type WHERE status = '1' AND deleted_at IS NULL";
        $data = $this->companyConnection->query($sql)->fetchAll();
//        $html = '<option value="default">Select</option>';
        $html = '';
        foreach ($data as $d) {
            $html.= "<option value='" . $d['id'] . "' >" . $d['unit_type'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function addUnittype() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');

            //Required variable array
            $required_array = ['unit_type', 'description"'];
            /* Max length variable array */
            $maxlength_array = ['unit_type' => 30, 'description' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_unit_type', 'unit_type', $data['unit_type'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Name already exists!');
                    }
                }
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['status'] = '1';
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO   company_unit_type (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addTrackKeydetail() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = ['key_name', 'company_name', 'phone', 'Address1', 'Address2', 'Address3', 'Address4', 'key_number', 'key_quality'
                , 'pick_up_date', 'return_date', 'return_time', 'key_designator'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if (!isset($_POST['propertyEditid'])):
                    $data['property_id'] = $_POST['property_id'];
                else:
                    $data['property_id'] = $_POST['propertyEditid'];
                endif;
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                unset($data['id']);
                $data['pick_up_date'] = mySqlDateFormat($data['pick_up_date'], null, $this->companyConnection);
                $data['pick_up_time'] = mySqlTimeFormat($data['pick_up_time']);
                $data['return_date'] = mySqlDateFormat($data['return_date'], null, $this->companyConnection);
                $data['return_time'] = mySqlTimeFormat($data['return_time']);
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                if (empty($id)) {


                    $sqlData = createSqlColVal($data);
                    //Save Data in Company Database
                    $query = "INSERT INTO track_key (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    $id = $this->companyConnection->lastInsertId();
                    return array('code' => 200, 'status' => 'success', 'lastid' => $id, 'data' => $stmt, 'message' => 'Record added successfully');
                } else {
                    $sqlData = createSqlColValPair($data);
                    $query = "UPDATE track_key SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function getTrackKey() {
        try {
            $id = $_POST['id'];
            //check if custom field already exists or not
            $sql = "SELECT * FROM track_key WHERE id=" . $id . " AND deleted_at IS NULL";
            $data = $this->companyConnection->query($sql)->fetch();
            if (!empty($data) && count($data) > 0) {
                $pick_up_date = dateFormatUser($data['pick_up_date'], null, $this->companyConnection);
                $return_date = dateFormatUser($data['return_date'], null, $this->companyConnection);

                $pick_up_time = timeFormat($data['pick_up_time'], null, $this->companyConnection);
                $return_time = timeFormat($data['return_time'], null, $this->companyConnection);

                return array('code' => 200, 'status' => 'success', 'data' => $data,'pick_up_time'=>$pick_up_time,'return_time'=>$return_time ,'pick_up_date' => $pick_up_date, 'return_date' => $return_date, 'message' => 'Record retrieved successfully');
            } else {
                return array('code' => 400, 'status' => 'error', 'message' => 'No Record Found!');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage()));
            return;
        }
    }

    public function deleteTrackKey() {
        //delete
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'key_tracker');

            $sql = "DELETE FROM track_key WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute();
            return array('status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function addReasonpopup() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');

            //Required variable array
            $required_array = ['reason"'];
            /* Max length variable array */
            $maxlength_array = ['reason' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_management_reason', 'reason', $data['reason'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Reason already exists!');
                    }
                }
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_management_reason (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function fetchAllReason() {
        $html = '';
        $sql = "SELECT * FROM company_management_reason";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = '<option value="default">Select</option>';
        if (!isset($_POST['propertyEditid'])) {
            foreach ($data as $d) {

                $html.= "<option value='" . $d['id'] . "'>" . $d['reason'] . "</option>";
            }
            return array('data' => $html, 'status' => 'success');
        } else {
            $propertyEditInfo = "SELECT * FROM  management_info WHERE property_id=" . $_POST['propertyEditid'] . " AND deleted_at IS NULL";
            $getpropertyEditInfo = $this->companyConnection->query($propertyEditInfo)->fetch();
            foreach ($data as $d) {

                $html.= "<option value='" . $d['id'] . "'" . (($getpropertyEditInfo['reason_management_end'] == $d['id']) ? "selected" : "") . ">" . $d['reason'] . "</option>";
            }
            return array('data' => $html, 'status' => 'success');
        }
    }

    public function getcurrentdatetime() {
        $timeZone = convertTimeZoneDefaultSeting('', date('Y-m-d H:i:s'), $this->companyConnection);
        $s = strtotime($timeZone);
        $renovationdate = dateFormatUser(date('M d, Y (D)', $s), null, $this->companyConnection);

        $renovationtime = date('h:i:a', $s);
        return array('timeZone' => $renovationdate, 'status' => 'success', 'renovationtime' => $renovationtime);
    }

    public function addRenovationdetails() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);

            //Required variable array
            $required_array = ['last_renovation_date', 'last_renovation_time'];
            /* Max length variable array */
            $maxlength_array = ['last_renovation_date' => 30, 'last_renovation_time' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data["last_renovation_time"] = (isset($data['last_renovation_time']) && !empty($data['last_renovation_time'])) ? mySqlTimeFormat($data['last_renovation_time']) : '';
                $data['object_type'] = 'property';
                $data['status'] = 1;
                $data['object_id'] = $_POST['property_id'];
                $data['last_renovation_date'] = mySqlDateFormat($data['last_renovation_date'], null, $this->companyConnection);
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO  renovation_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                //fetch latest renovation data
                $queryFetch = $this->companyConnection->query("SELECT * FROM  renovation_details WHERE id=$id");
                $checkData = $queryFetch->fetch();
                $checkDatatime=dateFormatUser($checkData['last_renovation_date'], null, $this->companyConnection);
                $checkDatatime2=$checkData['last_renovation_time'];
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt,'checkData'=>$checkData,'checkDatatime'=>$checkDatatime,'checkDatatime2'=>$checkDatatime2,'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function keyCheckout() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            $property_id = $_POST['propertyEditid'];
            $id = $_POST['id'];
//            echo "<pre>";print_r($data);
            //Required variable array
            $required_array = [];
            //Max length variable array
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);

            $queryFetch = $this->companyConnection->query("SELECT * FROM  key_tracker WHERE id=$id");
            $key = $queryFetch->fetch();


            if ($key['available_keys'] >= $data['checkout_keys']) {
                $data['available_keys'] = $key['available_keys'] - $data['checkout_keys'];
                $data['key_id'] = $id;
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO   key_checkout (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $queryupd = "UPDATE  key_tracker SET available_keys='" . $data['available_keys'] . "'  where id=$id";
                $stmtupd = $this->companyConnection->prepare($queryupd);
                $stmtupd->execute();


                $queryupd1 = "UPDATE  key_checkout SET available_keys='" . $data['available_keys'] . "'  where key_id=$id";
                $stmtupd1 = $this->companyConnection->prepare($queryupd1);
                $stmtupd1->execute();

                $queryFetchcheckout = $this->companyConnection->query("SELECT * FROM  key_checkout WHERE key_id=" . $key['id'] . "");
                $keycheckout = $queryFetchcheckout->fetch();
                if (!empty($keycheckout)) {
                    $querystatus = "UPDATE  key_tracker SET status='1'  where id=$id";
                    $stmtstatus = $this->companyConnection->prepare($querystatus);
                    $stmtstatus->execute();
                }

                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
            } else {
                return array('code' => 500, 'status' => 'success', 'message' => 'Cannot checkout more than available keys');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function returnCheckout() {
        try {
            $val =(isset($_POST['val']) && !empty( $_POST['val'])) ?  $_POST['val'] : '';
            $propertyEditid = $_POST['propertyEditid'];
            if(!empty($val)):
                foreach ($val as $returnkey) {
                    $queryreturnkey = $this->companyConnection->query("SELECT * FROM  key_checkout WHERE id=$returnkey");
                    $resreturnkey = $queryreturnkey->fetch();
                    $keytrackerid = $resreturnkey['key_id'];
                    $fetchkeytracker = $this->companyConnection->query("SELECT * FROM  key_tracker WHERE id=$keytrackerid");
                    $Reskeytracker = $fetchkeytracker->fetch();

                    $Latestavailkey = $resreturnkey['checkout_keys'] + $Reskeytracker['available_keys'];
                    $updkeycheckout = "UPDATE  key_checkout SET available_keys=$Latestavailkey ,checkout_keys=0 where id=$returnkey";
                    $stmtupd = $this->companyConnection->prepare($updkeycheckout);
                    $stmtupd->execute();

                    $keyid = $resreturnkey['key_id'];
                    $updkeytracker = "UPDATE  key_tracker SET available_keys=$Latestavailkey  where id=$keyid";
                    $stmttrackerupd = $this->companyConnection->prepare($updkeytracker);
                    $stmttrackerupd->execute();

                    if ($Reskeytracker['total_keys'] == $Reskeytracker['available_keys']) {
                        $querystatus = "UPDATE  key_tracker SET status='0' WHERE id=$keytrackerid";
                        $stmtstatus = $this->companyConnection->prepare($querystatus);
                        $stmtstatus->execute();
                    }
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
            else:
                return array('code' => 500, 'status' => 'close');
            endif;

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function getReturnkeyData() {
        try {
            $html = '';
            $id = $_POST['propertyEditid'];
            $key_id = $_POST['id'];


            $keycheckout = "SELECT * FROM  key_checkout WHERE key_id='" .$key_id . "'  and checkout_keys !=0";
            $reskeycheckout = $this->companyConnection->query($keycheckout)->fetchAll();
            foreach ($reskeycheckout as $keydetail) {
                $keyholderdetail = $this->companyConnection->query("SELECT * FROM  users WHERE id='" . $keydetail['key_holder'] . "'");
                $reskey = $keyholderdetail->fetch();
                $keydetailid = $keydetail['id'];
                $name = ucfirst($reskey['name']);
                $keydetailcheckoutkeys = $keydetail['checkout_keys'];

                $html .="<tr class='clsCheckOut'>";
                $html .="<td class='text-center'>";
                $html .="<input type='checkbox' name='returnCheckbox' class='chkKeyReturn' value=$keydetailid >";
                $html .="</td>";
                $html .="<td>$name</td><td class='clstd2'>Staff</td><td> $keydetailcheckoutkeys</td></tr>";
            }

            return array('code' => 200, 'html' => $html);
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addNewComplaint() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, "false");
            $building_id = $_POST["edit_building_id"];
            $complaint_id = $data["edit_complaint_id"];
            if (isset($complaint_id) && empty($complaint_id)) {
                $complaint_data = [];
                $complaint_data["object_id"] = $building_id;
                $complaint_data["module_type"] = 'property';
                $complaint_data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $complaint_data['status'] = 1;
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : NULL;
                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : NULL;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["created_at"] = date('Y-m-d H:i:s');
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');
                // dd($complaint_data);
                $sqlData = createSqlColVal($complaint_data);
                //Save Data in Company Database

                $query = "INSERT INTO  complaints (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($complaint_data);
                $id = $this->companyConnection->lastInsertId();
                if ($id) {
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully');
                } else {
                    return array('code' => 400, 'status' => 'error', 'message' => 'Record Not Added successfully');
                }
            } else {
                $complaint_data = [];
                $complaint_data["complaint_id"] = (isset($data['complaint_id']) && !empty($data['complaint_id'])) ? $data['complaint_id'] : '';
                $complaint_data["complaint_date"] = (isset($data['complaint_date']) && !empty($data['complaint_date'])) ? mySqlDateFormat($data['complaint_date'], null, $this->companyConnection) : NULL;
                $complaint_data["complaint_type_id"] = (isset($data['complaint_type_options']) && !empty($data['complaint_type_options'])) ? $data['complaint_type_options'] : NULL;
                $complaint_data["complaint_note"] = (isset($data['complaint_note']) && !empty($data['complaint_note'])) ? $data['complaint_note'] : '';
                $complaint_data["other_notes"] = (isset($data['other_notes']) && !empty($data['other_notes'])) ? $data['other_notes'] : '';
                $complaint_data["updated_at"] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($complaint_data);
                $query = "UPDATE complaints SET " . $sqlData['columnsValuesPair'] . " where id='$complaint_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $e->getMessage()));
            return;
        }
    }

    public function getComplaintDetail() {
        try {
            $id = $_POST['id'];
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'complaints');
            if (!empty($record["data"]["complaint_date"])) {
                $record["data"]["complaint_date"] = dateFormatUser($record["data"]["complaint_date"], null, $this->companyConnection);
            }
            return ['status' => 'success', 'code' => 200, 'data' => $record, 'message' => 'Record deleted successfully.'];
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function getComplaintsData() {
        //delete
        try {
            $complaint_ids = $_POST['complaint_ids'];
            $complaint_idss = str_repeat('?,', count($complaint_ids) - 1) . '?';

            $stmt = $this->companyConnection->prepare("SELECT com.complaint_note,ct.complaint_type,gp.property_name,gp.address1,gp.address2,gp.address3,gp.address4 FROM complaints as com JOIN complaint_types as ct ON com.complaint_type_id=ct.id JOIN general_property as gp ON com.object_id=gp.id WHERE com.module_type='property' and com.id IN ($complaint_idss)");
            $stmt->execute($complaint_ids);
            $data = $stmt->fetchAll();
            $print_complaints_html = '';
            foreach ($data as $key => $value) {
                $address=$value['address1'].' '.$value['address2'].' '.$value['address3'].' '.$value['address4'];
                $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
                    <img width="200" src="../company/images/logo.png"/>
                  </td>
                </tr>
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
                    <table width="100%" align="center" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> Complaint by Property </td>
                      </tr>
                    </table>
                    <table width="100%" align="center" border="1" cellspacing="0" cellpadding="5">
                      
                      <tr>
                        <td>Name: </td>
                        <td>'.$value['property_name'].'</td>
                      </tr>
                      <tr>
                        <td>Address: </td>
                        <td>'.$address.'</td>
                      </tr>
                      <tr>
                        <td>Complaint Type: </td>
                        <td>'.$value['complaint_type'].'</td>
                      </tr>
                      <tr>
                        <td>Description: </td>
                        <td>'.$value['complaint_note'].'</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td style="background: #585858; color: #fff; padding: 10px; text-align: center;">
                    Apexlink.apexlink@yopmail.com
                    
                  </td>
                </tr>
              </table>';
            }

            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function deleteComplaint() {
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'key_tracker');

            $sql = "UPDATE complaints SET deleted_at=? WHERE id=?";
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute([$data, $id]);
            echo json_encode(['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.']);
            return;
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function addaccountName() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'account_name_details', 'account_name', $data['account_name'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Account Name already exists!');
                    }
                }
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO account_name_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function fetchAccountName() {
        $html = '';
        $sqlPort = "SELECT * FROM  company_accounting_bank_account where portfolio=" . $_POST['pid'];
        $dataPort = $this->companyConnection->query($sqlPort)->fetchAll();

        $html = '<option value="default">Select</option>';
        foreach ($dataPort as $d) {
            $html .= "<option value='" . $d['id'] . "'>" . $d['bank_name'] . "</option>";
        }

        return array('data' => $html, 'status' => 'success');
    }

    public function insertChartofaccount() {

        try {
            $data = $_POST['form'];
//             print_r($data);
            $required_array = ['account_type_id', 'account_code', 'account_name', 'reporting_code'];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = ['range_from' => 1, 'range_to' => 1];
            //Number variable array
            $number_array = ['range_from', 'range_to'];
            //Server side validation

            $err_array = validation($data, $this->conn, $required_array, $maxlength_array, $minlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if (isset($data['form_type']) && !empty($data['form_type'])) {
                    unset($data['form_type']);
                }
                if (isset($data['chart_account_edit_id']) || empty($data['chart_account_edit_id'])) {
                    unset($data['chart_account_edit_id']);
                }

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];


                $accountCodeDuplicate = checkNameAlreadyExists($this->companyConnection, 'company_chart_of_accounts', 'account_code', $data['account_code'], '');
                if ($accountCodeDuplicate['is_exists'] == 1) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Chart account code already exists.');
                }
                $accountNameDuplicate = checkNameAlreadyExists($this->companyConnection, 'company_chart_of_accounts', 'account_name', $data['account_name'], '');
                if ($accountNameDuplicate['is_exists'] == 1) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Chart account name already exists.');
                }

                $query = $this->companyConnection->query("SELECT * FROM company_chart_of_accounts");
                $checkEmptyRecord = $query->fetchAll();


                if ($data['account_code'] < $data['range_from'] || $data['account_code'] > $data['range_to']) {
                    return array('code' => 503, 'status' => 'error', 'message' => 'Account code does not fall in given range ' . $data['range_from'] . ' and ' . $data['range_to'] . '.');
                }
                if (isset($data['range_from']))
                    unset($data['range_from']);
                if (isset($data['range_to']))
                    unset($data['range_to']);

//                if ($data['status'] == 0) {
//                    return array('code' => 503, 'status' => 'error', 'message' => 'Deactivate value can\'t be set as default.');
//                }

//                if(isset($data['is_default']) && $data['is_default'] == 'on'){
//                    $data['is_default'] = '1';
//                }

                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_chart_of_accounts (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'lastid' => $id, 'message' => 'Record added successfully.');
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    public function viewAccountType() {

        try {
            $id = $_POST['id'];
            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $dataselected = $this->companyConnection->query("SELECT account_type_name,id FROM company_account_type")->fetchAll();
//            print_r($dataselected);
            foreach ($dataselected as $key => $value) {
                $keyval[] = array($value['id'] => $value['account_type_name']);
            }
            //print_r($keyval);
            if (isset($login_user_id) && !empty($login_user_id)) {
                return getDataById($this->companyConnection, 'company_chart_of_accounts', $id, $keyval);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getAllAccountType() {
        try {
            $query = $this->companyConnection->query("SELECT * FROM company_account_type WHERE status = '1'");
            $data = $query->fetchAll();
            return array('status' => 'success', 'code' => 200, 'data' => $data);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    public function getAllAccountSubType() {
        try {
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM company_account_sub_type WHERE account_type_id='$id'");
            $data = $query->fetchAll();
            return ['status' => 'success', 'code' => 200, 'data' => $data];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function fetchChartAccounts() {
        $html = '';
        $sql = "SELECT * FROM company_chart_of_accounts";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = '<option value="default">Select</option>';
        foreach ($data as $d) {
            if($d['is_default'] == '1'){
                $html.= "<option value='" . $d['id'] . "' selected>" . $d['account_name'] . '-' . $d['account_code'] . "</option>";
            } else {
                $html.= "<option value='" . $d['id'] . "'>" . $d['account_name'] . '-' . $d['account_code'] . "</option>";
            }
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function addaccountType() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field already exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'account_type_details', 'account_type', $data['account_type'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Account Type already exists!');
                    }
                }
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO account_type_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function fetchAllAccountType() {
        $html = '';
        $sql = "SELECT * FROM account_type_details";
        $data = $this->companyConnection->query($sql)->fetchAll();

        $sql1 = "SELECT * FROM company_account_type";
        $data1 = $this->companyConnection->query($sql1)->fetchAll();

        $is_default = '';
        foreach ($data1 as $d1) {
            if($d1['is_default'] == '1'){
                $is_default = $d1['id'];
            }
        }
        $html = '<option value="default">Select</option>';
        foreach ($data as $d) {
            if(isset($is_default) || !empty($is_default)){
                if ($d['account_type_id'] == $is_default){
                    $html.= "<option value='" . $d['id'] . "' selected>" . $d['account_type'] . "</option>";
                } else {
                    $html.= "<option value='" . $d['id'] . "'>" . $d['account_type'] . "</option>";
                }
            }
        }
        return array('data' => $html, 'status' => 'success');
    }

    /**
     * function to archive property
     * @return array
     */
    public function archiveProperty(){
        try {
            $id = $_POST['id'];
            $data = [];
            $data['status'] = '3';
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE general_property SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Archived Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to archive property
     * @return array
     */
    public function activateProperty(){
        try {
            $id = $_POST['id'];
            $data = [];
            $data['status'] = '1';
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE general_property SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Activated Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to resign property
     * @return array
     */
    public function resignProperty(){
        try {
            $id = $_POST['id'];
            $data = [];
            $data['status'] = '6';
            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE general_property SET ".$sqlData['columnsValuesPair']." WHERE id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Resign Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to resign property
     * @return array
     */
    public function deleteProperty(){
        try {
            $id = $_POST['id'];
            $query = "DELETE FROM general_property WHERE id=".$id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('status' => 'success', 'code' => 200, 'message' => 'Record Deleted Successfully.');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }

    /**
     * function to show envelop of property
     * @return array|void
     */
    public function getCompanyData() {
        try {
            $id= $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => '1'], 'users');
            $building_detail = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'general_property');
            return array('status' => 'success', 'code' => 200, 'data' => $data,'building'=>$building_detail, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    /**
     *
     * @param null $userId
     * @return array
     */
    public function sendMail($userId = null)
    {
        try{
            $server_name = 'https://'.$_SERVER['HTTP_HOST'];
            $userId = (isset($userId) ? $userId : $_REQUEST['id']);
            $user_details = $this->companyConnection->query("SELECT * FROM users where id=".$userId)->fetch();
            $user_name = userName($user_details['id'], $this->companyConnection);
            $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/welcomeUser.php');
            $body = str_replace("#name#",$user_name,$body);
            $body = str_replace("#email#",$user_details['email'],$body);
            $body = str_replace("#password#",$user_details['actual_password'],$body);
            $body = str_replace("#logo#",SITE_URL.'/company/images/logo.png',$body);
            $body = str_replace("#website#",$server_name,$body);
            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $user_details['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal']  = '1';
            curlRequest($request);

            return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully'];
        }catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Function to get return key data history
     */
    public function getReturnkeyDatahistory() {
        try {
            $html = '';
            $id = $_POST['propertyEditid'];
            $key_id = $_POST['id'];

            $keycheckout = "SELECT * FROM  key_checkout WHERE key_id='" .$key_id . "'  and checkout_keys !=0";
            $reskeycheckout = $this->companyConnection->query($keycheckout)->fetchAll();
            foreach ($reskeycheckout as $keydetail) {
                $keyholderdetail = $this->companyConnection->query("SELECT * FROM  users WHERE id='" . $keydetail['key_holder'] . "'");
                $reskey = $keyholderdetail->fetch();
                $keydetailid = $keydetail['id'];
                $name = ucfirst($reskey['name']);
                $keydetailcheckoutkeys = $keydetail['checkout_keys'];

                $html .="<tr class='clsCheckOut'>";
                $html .="<td class='text-center'>";
                $html .="</td>";
                $html .="<td>$name</td><td class='clstd2'>Staff</td><td> $keydetailcheckoutkeys</td></tr>";
            }

            return array('code' => 200, 'html' => $html);
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function getPropertiesdata() {

        try {
            $search = @$_REQUEST['q'];
            if(empty($search)){
                $sql = "SELECT * FROM general_property  where status = '1' order by property_name ASC";
            } else {
                $sql = "SELECT * FROM general_property  where status = '1' and property_name LIKE '%$search%' order by property_name ASC";
            }
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);

            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['property_name'];
                    $data['rows'][$key]['address1'] = $user_data['address1'] . ', ' . $user_data['address2'] . ' ' . $user_data['address3'];
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }
    public function getCloneproperty(){
        $table = 'general_property';
        $columns=[
            'general_property.*','general_property.description as property_description','general_property.address1 as street_address1','general_property.address2 as street_address2','general_property.property_id as property_idrandom',
            'general_property.address4 as street_address4','general_property.phone_number as phone_numbercopy',
            'general_property.property_tax_ids as property_tax_id','general_property.property_year as year_built',
            'general_property.school_district_municipality as school_district_municipalityID','general_property.key_access_codes_info as key_options',
            'general_property.garage_available as garage_options','general_property.key_access_code_desc as key_access_code','general_property.market_rent as default_rent','general_property.portfolio_id as portfolio_name_options',
            'general_property.owner_id as owner_ids','general_property.owner_percentowned as owner_percentownedcopy','property_bank_details.*','property_bank_details.bank_account_min_reserve_amount as bank_account_min_amount','property_bank_details.description as bankDescription',
            'late_fee_management.checkbox as applycheckbox','late_fee_management.apply_one_time_flat_fee as one_time_flat_fee_desc','late_fee_management.apply_one_time_percentage as one_time_monthly_fee_desc',
            'late_fee_management.apply_daily_flat_fee as daily_flat_fee_desc','late_fee_management.apply_daily_percentage as daily_monthly_fee_desc','late_fee_management.grace_period as grace_period',
            'management_fees.management_type as magt_type','management_fees.management_value as management_fee_value','management_info.*','management_info.reason_management_end as selectreasondiv',
            'management_info.description as management_infodesc','maintenance_information.*','maintenance_information.amount as maintenance_amount',
            'maintenance_information.Insurance_expiration as InsuranceExpiration','maintenance_information.warranty_expiration as HomeWarrantyExpiration',
            'mortgage_information.*'
        ];
        $joins = [[
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'property_bank_details',
            'as' => 'property_bank_details'
        ],[
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'late_fee_management',
            'as' => 'late_fee_management'
        ],[
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'management_fees',
            'as' => 'management_fees'
        ],[
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'management_info',
            'as' => 'management_info'
        ],[
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'mortgage_information',
            'as' => 'mortgage_information'
        ],[
            'type' => 'LEFT',
            'table' => 'general_property',
            'column' => 'id',
            'primary' => 'property_id',
            'on_table' => 'maintenance_information',
            'as' => 'maintenance_information'
        ]];
        $where = [[
            'table' => 'general_property',
            'column' => 'id',
            'condition' => '=',
            'value' => $_POST['id']
        ]];
        $getdata = selectQueryBuilder($columns, $table, $joins, $where);
        $resgetdata = $this->companyConnection->query($getdata)->fetch();
        $amenties=unserialize($resgetdata['amenities']);
        $school_district_code=unserialize($resgetdata['school_district_code']);
        $school_district_notes=unserialize($resgetdata['school_district_notes']);
        $key_access_code_desc=unserialize($resgetdata['key_access_code']);
        $property_tax_ids=unserialize($resgetdata['property_tax_ids']);
        $phone_number=unserialize($resgetdata['phone_number']);
        $key_options=unserialize($resgetdata['key_options']);
        $key_options=unserialize($resgetdata['key_options']);
        $school_district_municipalityID=unserialize($resgetdata['school_district_municipalityID']);
        $owner_id_copy=unserialize($resgetdata['owner_ids']);
        $owner_percentowned=unserialize($resgetdata['owner_percentownedcopy']);
        $manager_id=unserialize($resgetdata['manager_id']);
        $attach_groups=unserialize($resgetdata['attach_groups']);
        $vendor_1099_payer=unserialize($resgetdata['vendor_1099_payer']);
        //date pcikers

        $management_start_date=dateFormatUser($resgetdata['management_start_date'], null, $this->companyConnection);

        $management_end_date=dateFormatUser($resgetdata['management_end_date'], null, $this->companyConnection);

        $InsuranceExpiration=dateFormatUser($resgetdata['InsuranceExpiration'], null, $this->companyConnection);

        $HomeWarrantyExpiration=dateFormatUser($resgetdata['HomeWarrantyExpiration'], null, $this->companyConnection);

        $mortgage_start_date=dateFormatUser($resgetdata['mortgage_start_date'], null, $this->companyConnection);
        unset($resgetdata['owner_percentowned']);
        unset($resgetdata['owner_ids']);

        return array('code' => 200, 'data' => $resgetdata,'amenties'=>$amenties,'school_district_code'=>$school_district_code,
            'school_district_notes'=>$school_district_notes,'key_access_code_desc'=>$key_access_code_desc,'property_tax_ids'=>$property_tax_ids,
            'phone_number'=>$phone_number,'key_options'=>$key_options,'school_district_municipalityID'=>$school_district_municipalityID,
            'owner_id_copy'=>$owner_id_copy,'owner_percentowned_copy'=>$owner_percentowned,
            'management_start_date'=>$management_start_date,'management_end_date'=>$management_end_date,'InsuranceExpiration'=>$InsuranceExpiration,
            'HomeWarrantyExpiration'=>$HomeWarrantyExpiration,'mortgage_start_date'=>$mortgage_start_date,'manager_id'=>$manager_id,'attach_groups'=>$attach_groups,'vendor_1099_payer'=>$vendor_1099_payer);
    }
    public function countries(){
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html = "";
        foreach ($countryCode as $country) {

            $html.= "<option value='" . $country['id'] . "' " . (($country['code'] == '+1') ? "selected" : "") . ">" . $country['name'] ."(".$country['code'].")". "</option>";
        }
//       echo "<pre>";print_r($html);die;
        return array('data' => $html, 'status' => 'success');
    }
    public function getLatLong($id){
        try {
            $propertyAddress = "SELECT address1,address2,address_list,address3,address4 FROM  general_property WHERE id=".$id;
            $res = $this->companyConnection->query($propertyAddress)->fetch();

            if(!empty($res['address1']) || !empty($res['address2']) || !empty($res['address3']) || !empty($res['address4'])) {
                $address = $res['address1'] . '' . $res['address2'] . '' . $res['address3'] . '' . $res['address4'];
                $formattedAddr = str_replace(' ', '+', $address);

                $api_maps = 'https://maps.googleapis.com/maps/';

                $api_key = 'AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww';
                $url = $api_maps . 'api/geocode/json?address=' . $formattedAddr . '&key=' . $api_key;
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                $curl_response = curl_exec($curl);
                curl_close($curl);
                $response_a = json_decode($curl_response);
                if (!empty($response_a) && $response_a->status != 'ZERO_RESULTS') {
                    $lat = $response_a->results[0]->geometry->location->lat;
                    $long = $response_a->results[0]->geometry->location->lng;
                } else {
                    $lat = null;
                    $long = null;
                }

                $mapArr = [];
                $mapArr['property_id'] = $id;
                $mapArr['address'] = $address;
                $mapArr['latitude'] = $lat;
                $mapArr['longitude'] = $long;
                $mapArr['created_at'] = date('Y-m-d H:i:s');
                $mapArr['updated_at'] = date('Y-m-d H:i:s');

                $mapCheck = "SELECT property_id FROM  map_address WHERE property_id=".$id;
                $resCheck = $this->companyConnection->query($mapCheck)->fetch();
                if(empty($resCheck)){
                    $renovationData = createSqlColVal($mapArr);
                    $query1 = "INSERT INTO map_address (" . $renovationData['columns'] . ") VALUES (" . $renovationData['columnsValues'] . ")";

                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute($mapArr);
                }else{

                    $renovationData = createSqlUpdateCase($mapArr);
                    $query = "UPDATE map_address SET " . $renovationData['columnsValuesPair'] . " where property_id=".$id;

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($renovationData['data']);

                }        }

            return "done";
        } catch (Exception $e){
            dd($e);
        }

    }
    public function getPropertyPortfolio(){
        try{
            $sql = "SELECT cpp.portfolio_name,cpp.id FROM company_property_portfolio as cpp JOIN general_property as gp ON gp.portfolio_id=cpp.id WHERE gp.id=".$_POST['property'];
            $data = $this->companyConnection->query($sql)->fetch();
            return array('data' => $data, 'status' => 'success');
        } catch (Exception $e){
            dd($e);
        }

    }
    public function fetchEmail(){
        try{
            $sql = "SELECT email FROM users WHERE id=".$_POST['id'];
            $data = $this->companyConnection->query($sql)->fetch();
            dd($data);
            return array('data' => $data, 'status' => 'success');
        } catch (Exception $e){
            dd($e);
        }

    }
    public function getComplaintsDataForEmail($complaint_id, $owner_id) {
        try {
            $user_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $owner_id], 'general_property');

            $address1 = isset($user_data['data']['address1']) ? $user_data['data']['address1'] : '';
            $address2 = isset($user_data['data']['address2']) ? $user_data['data']['address2'] : '';
            $address3 = isset($user_data['data']['address3']) ? $user_data['data']['address3'] : '';
            $address4 = isset($user_data['data']['address4']) ? $user_data['data']['address4'] : '';
            $user_data['data']['owner_address'] = $address1.' '.$address2.' '.$address3.' '.$address4;

            $complaint_data = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $complaint_id], 'complaints');
            $print_complaints_html = '';

            $record = '';
            if(isset($complaint_data['data']['complaint_type_id'])){
                $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $complaint_data['data']['complaint_type_id']], 'complaint_types');
            }
            $complaint_data['data']['complaint_type_name'] = isset($record['data']) ? $record['data']['complaint_type'] : '';
            $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
                    <img width="200" src="#logo#"/>
                  </td>
                </tr>
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
                    <table width="100%" align="center" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> '.$complaint_data['data']['complaint_by_about'].' </td>
                      </tr>
                    </table>     
                    <table width="100%" cellpadding="0" cellspacing="0" style="padding:0px 20px 50px 20px;">
                            <tr>
                                <td width="30%" style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Name:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                   '.$user_data['data']['property_name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Address:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$user_data['data']['owner_address'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Complaint Type:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$complaint_data['data']['complaint_type_name'].'
                                </td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    Description:
                                </td>
                                <td style="border: 1px solid #444; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px; padding: 5px;">
                                    '.$complaint_data['data']['complaint_note'].'
                                </td>
                            </tr>                          
                        </table>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif;
                                        font-size: 14px;" align="center"
                        bgcolor="#585858">
                        ApexLink Property Manager● <a style="color: #fff; text-decoration: none;" href="javascript:;">
                            support@apexlink.com ●772-212-1950 </a>
                    </td>
                </tr>
              </table>';
            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }
    public function SendComplaintMail()
    {
        try{
            $data = $_POST;
            $owner_id = $data['propertyID'];
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subdomain = array_shift($domain);
            $user_detail = $_SESSION['SESSION_'.$subdomain];
            $toUsers = $user_detail['email'].','. $data['owner_email'];
            $to_arr =  explode(",",$toUsers);
            $complaint_id = $data['complaint_ids'];
            $result = '';

            $logo = SUBDOMAIN_URL."/company/images/logo.png";
            foreach ($complaint_id as $k => $v){
                $body = $this->getComplaintsDataForEmail($v, $owner_id);
                $request['action']  = 'SendMailPhp';
                $request['to']      = $to_arr;
                $request['subject'] = 'Send Owner Complaint!';
                $body['html']       = str_replace("#logo#",$logo,$body['html']);
                $request['message'] = $body['html'];
                $request['portal']  = '1';
                $result = curlRequest($request);
            }
            return ['status' => 'success', 'code' => 200, 'data' => $result];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    public function updateXmlStatus() {
        try {
            $id = isset($_POST['id'])? $_POST['id']:'';
            $xmlFunction = $this->XML($id);
            if(empty($id)){
                return array('status' => 'error', 'code' => 500, 'message' => 'Select Atleast one Property!');
            }else{
                $uncheck = isset($_POST['uncheck'])? $_POST['uncheck']:'';
                if(!empty($id)):
                    foreach ($id as $singleId){
                        $query = "UPDATE general_property SET is_xml= '1' where id=" .$singleId ." and deleted_at IS NULL";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                    }
                endif;
                if(!empty($uncheck)):
                    foreach ($uncheck as $uncheckId){
                        $query = "UPDATE general_property SET is_xml= '0' where id=" .$uncheckId ." and deleted_at IS NULL";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                    }
                endif;
            }
            return array('status' => 'success', 'code' => 200, 'message' => 'XML has been created successfully!');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }
    public function sendXMLMail()
    {
        try{
            $logo = SUBDOMAIN_URL."/company/images/logo.png";
            $body = file_get_contents(COMPANY_DIRECTORY_URL.'/views/Emails/welcomeRenatlUser.php');
            $body = str_replace("#logo#",$logo,$body);
            $data = $_POST;
            $request['action']  = 'SendMailPhp';
            $request['to']      = ["jkorphosy@rentpath.com","info@apexlink.com"];
            $request['subject'] = 'Rental Request';
            $request['message'] = $body;
            $request['portal']  = '1';
            $response = curlRequest($request);
            return ['status' => 'success', 'code' => 200, 'data' => $response];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    public function XML($getIds)
    {
        try {
            $filename=$_SESSION[SESSION_DOMAIN]['domain_name'].'.xml';
            $xmlFileLocation = ROOT_URL . '/company/upload/'.$filename;
            $xmldoc = new DomDocument('1.0');
            $xmldoc->preserveWhiteSpace = false;
            $xmldoc->formatOutput = true;

            $xml = simplexml_load_file($xmlFileLocation);
            if(isset($xml->headercontent)){
                unset($xml->headercontent);
            }
            $headercontent=  $xml->addChild('headercontent','');
            $domainname=$_SESSION[SESSION_DOMAIN]['domain_name'];
            $pmDetail= $this->conn->query("SELECT id as customeradreference,name as contactname,phone_number as contactphone1,email as contactemail2,
              company_name as contactcompany FROM `users` WHERE domain_name ='$domainname'")->fetch();

            foreach ($pmDetail as $kpm => $pm){
                $pmDetailTag =  $headercontent->addChild($kpm, $pm);
            }
            if(!empty($getIds)) {
                foreach ($getIds as $value) {

                    $fetchQuery = $this->companyConnection->query("SELECT gp.id,gp.status as adactive,gp.property_for_sale as listingtype,
                    gp.address1 as propertyaddress,gp.zipcode as propertypostalcode,gp.property_name as propertyname,gp.city as propertycity,gp.state as propertystate,gp.base_rent as propertyrentmin,
                    gp.school_district_municipality as propertyschooldistrict,gp.amenities,gp.property_squareFootage as propertysquarefootmin,ud.bedrooms_no as propertybedrooms,ud.bathrooms_no as propertybathrooms       
                    FROM general_property as gp LEFT JOIN unit_details as ud ON gp.id= ud.property_id WHERE gp.id =" . $value);
                    $getdata = $fetchQuery->fetch();
                    $indexNode = $headercontent->addChild('property_' . $getdata['id'], '');

                    foreach ($getdata as $k => $v) {
                        if($k == 'propertyschooldistrict'){
                            $schooldata=unserialize($getdata['propertyschooldistrict']);
                            if(!empty($schooldata)) {
                                $s=1;
                                foreach ($schooldata as $school) {
                                    $indexNode->addChild('propertyschooldistrict'.$s, $school);
                                    $s++;
                                }
                            }
                        }
                        if($k == 'amenities'){
                            $amenities = unserialize($getdata['amenities']);
                            if(!empty($amenities)) {
                                $a=1;
                                foreach ($amenities as $amen) {$amenData= $this->companyConnection->query("SELECT name FROM company_property_amenities WHERE id=".$amen)->fetch();
                                    $indexNode->addChild('amenities'.$a, htmlspecialchars($amenData['name']));
                                    $a++;
                                }
                            }
                        }

                        $indexNode->addChild($k, $v);
                        $prope='property_'. $getdata['id'];
                        unset($xml->headercontent->$prope->propertyschooldistrict);
                        unset($xml->headercontent->$prope->id);
                        unset($xml->headercontent->$prope->amenities);
                    }

//                    $leaseDetail= $this->companyConnection->query("SELECT tp.user_id,tld.start_date,tld.end_date,tld.term,tld.tenure FROM tenant_property as tp join tenant_lease_details as tld WHERE tp.property_id =".$getdata['id'])->fetch();
//                    dd($leaseDetail);

                }
            }
            $xml->asXML($xmlFileLocation);

        } catch (Exception $ex) {
            dd($ex);
        }



    }
    public function addRentalRequest() {
        try {
            $data = $_POST['form'];
            $data = postArray($data);
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                //Save Data in Company Database
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO property_rental (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                $sendXMLMail=$this->sendXMLMail();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Rental Request has been send');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function getPmdetails(){
        try{

            $sql = "SELECT name,first_name,last_name,email,phone_number FROM users WHERE id='1'";
            $data = $this->companyConnection->query($sql)->fetch();

            $subscription_plan=$this->companyConnection->query("SELECT subscription_plan FROM `plans_history` WHERE status ='1'")->fetch();
            $unitPlan = $this->conn->query("SELECT max_units FROM `plans` WHERE id =".$subscription_plan['subscription_plan'])->fetch();

            $xmlProperty=$this->companyConnection->query("SELECT is_xml FROM `general_property` WHERE is_xml ='1'")->fetchAll();
            $totalUnit=$unitPlan['max_units'];
            $xmlProperty=count($xmlProperty);

            return array('code' => 200, 'data' => $data, 'status' => 'success','vaccantUnits'=>$xmlProperty,'totalUnits'=>$totalUnit);
        } catch (Exception $e){
            dd($e);
        }
    }
    public function createXmlAction(){
        $filename=$_SESSION[SESSION_DOMAIN]['domain_name'].'.xml';
        $xmlFileLocation = ROOT_URL . 'company/upload/'.$filename;
        $xml = new DOMDocument();
        $xml_album = $xml->createElement("maincontent");
        $xml_track = $xml->createTextNode("");
        $xml_album->appendChild( $xml_track );
        $xml->appendChild( $xml_album );
        if(file_exists ($xmlFileLocation)){
        }else{
            $xml->save($xmlFileLocation);
        }


        return array('code' => 200,'status' => 'success');
    }

}

$propertyDetail = new propertyDetail();
?>