<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( $_SERVER['DOCUMENT_ROOT']."/helper/globalHelper.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class OwnerAjax extends DBConnection{

    /**
     * OwnerAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * @param null $internal
     * @return array
     */
    public function getIntialData($internal = null){

        try{
            $data = [];
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['property_list'] =$this->companyConnection->query("SELECT id,property_id, property_name FROM general_property WHERE status = 1")->fetchAll();

            foreach ($data['property_list'] as $key=>$value){
                if(!empty($value)){
                    $propertyPercent = $this->companyConnection->query("SELECT SUM(property_percent_owned) FROM owner_property_owned WHERE property_id=".$value['id'])->fetch();
                    $data['property_list'][$key]['property_percent_owned'] = !empty($propertyPercent['SUM(property_percent_owned)'])?$propertyPercent['SUM(property_percent_owned)']:0;
                }
            }
            $data['phone_type'] =$this->companyConnection->query("SELECT id,type FROM phone_type")->fetchAll();
            $data['carrier'] = $this->companyConnection->query("SELECT id,carrier FROM carrier")->fetchAll();
            $data['referral'] = $this->companyConnection->query("SELECT id,referral FROM tenant_referral_source")->fetchAll();
            $data['ethnicity'] = $this->companyConnection->query("SELECT id,title FROM tenant_ethnicity where title != 'Other' order by title asc")->fetchAll();
            array_push($data['ethnicity'],array("id"=>9,"title" =>"Other"));
            $data['hobbies'] = $this->companyConnection->query("SELECT * FROM hobbies")->fetchAll();
            $data['marital'] = $this->companyConnection->query("SELECT id,marital FROM tenant_marital_status")->fetchAll();
            $data['veteran'] = $this->companyConnection->query("SELECT id,veteran FROM tenant_veteran_status")->fetchAll();
            $data['collection_reason'] = $this->companyConnection->query("SELECT id,reason FROM tenant_collection_reason")->fetchAll();
            $data['credential_type'] = $this->companyConnection->query("SELECT id,credential_type FROM tenant_credential_type")->fetchAll();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
            $data['state'] = $this->companyConnection->query("SELECT state FROM `users` WHERE id=$id")->fetch();
            $data['complaints'] = $this->companyConnection->query("SELECT id,complaint_type FROM `complaint_types` ORDER BY complaint_type ASC")->fetchAll();
            $data['account_name'] = $this->companyConnection->query("SELECT id,account_name FROM `owner_account_name`")->fetchAll();
            $data['chart_accounts'] = $this->companyConnection->query("SELECT id,account_name,account_code,is_default FROM company_chart_of_accounts WHERE status = '1'")->fetchAll();
            $data['account_type'] = $this->companyConnection->query("SELECT id,account_type_name,range_from,range_to,status,is_default FROM `company_account_type`")->fetchAll();
//            dd($data['property_list']);
            if($internal)
            {
                return $data;
            }
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function addAccountName()
    {
        try{
            $column = $_POST['colName'];
            $tableName = $_POST['tableName'];
            $value = $_POST['val'];
            $query = "SELECT * FROM `$tableName` WHERE $column = '$value'";
            $checkIfExist = $this->companyConnection->query($query)->fetch();

            if ($checkIfExist !== false){
                return array('code' => 504, 'status' => 'error','message' => 'Record already exist!');
            }

            $data[$_POST['colName']] = $_POST['val'];
            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];

            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO `$tableName` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);

            $stmt->execute($data);
            $data1['col_val'] = $_POST['val'];
            $data1['last_insert_id'] = $this->companyConnection->lastInsertId();
            return array('code' => 200, 'status' => 'success', 'data' => $data1,'message' => 'Record added successfully.');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getPropertyPopUpData(){
        try{
            $data = [];
            $data['portfolio'] =$this->companyConnection->query("SELECT id,portfolio_name,is_default FROM company_property_portfolio")->fetchAll();
            $data['property_type'] =$this->companyConnection->query("SELECT id,property_type,is_default FROM company_property_type WHERE status='1'")->fetchAll();
            $data['property_style'] =$this->companyConnection->query("SELECT id,property_style,is_default FROM company_property_style WHERE status='1'")->fetchAll();
            $data['manager'] =$this->companyConnection->query("SELECT id,first_name,last_name FROM users WHERE status='1' AND user_type='1' AND role='2'")->fetchAll();
            $data['property_group'] = $this->companyConnection->query("SELECT id,group_name,is_default FROM company_property_groups WHERE status='1'")->fetchAll();
            $data['property_sub_type'] = $this->companyConnection->query("SELECT id,property_subtype,is_default FROM company_property_subtype WHERE status='1'")->fetchAll();
            $data['property_unit_type'] = $this->companyConnection->query("SELECT id,unit_type,is_default FROM company_unit_type WHERE status='1'")->fetchAll();
            $data['amenity'] = $this->companyConnection->query("SELECT id,name FROM company_property_amenities WHERE status= '1'")->fetchAll();
            $data['year_built'] = date('Y');
//            $data['group'] = '';
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    //Add Owner
    public function insert(){
        try{
            $postData = $_POST;
            $response['OwnerUserDetails'] = $this->insertOwnerUserDetails($postData);
            return $response;
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function update()
    {
        try{
            $postData = $_POST;
            $response = $this->insertOwnerUserDetails($postData,true);
            return $response;
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function insertOwnerUserDetails($postData,$update=null){
        try{

//            dd($postData);
            $table="users";
            $columns = [$table . '.first_name', $table . '.last_name', $table . '.address1', $table . '.address2', $table . '.address3', $table . '.address4', $table . '.middle_name', $table . '.dob', $table . '.ssn_sin_id'];

            if (isset($postData['dob']) && $postData['dob'] != "") {
                $alreadyExistData['dob'] = mySqlDateFormat($postData['dob'],null,$this->companyConnection);
            }else{
                $alreadyExistData['dob'] ='';
            }

            $edit_id = (isset($postData['edit_id']) && !empty($postData['edit_id'])) ? $postData['edit_id'] : '';

            $where = [
                ['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $postData["first_name"]],
                ['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $postData["last_name"]],
                ['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $postData["middle_name"]],
                ['table' => 'users', 'column' => 'address1', 'condition' => '=', 'value' => $postData["address1"]],
                ['table' => 'users', 'column' => 'address2', 'condition' => '=', 'value' => $postData["address2"]],
                ['table' => 'users', 'column' => 'address3', 'condition' => '=', 'value' => $postData["address3"]],
                ['table' => 'users', 'column' => 'address4', 'condition' => '=', 'value' => $postData["address4"]],
                ['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $postData['phone_number'][0]],
                ['table' => 'users', 'column' => 'dob', 'condition' => '=', 'value' => $alreadyExistData['dob']]
            ];
          //  dd($_POST);


            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subDomain = array_shift($domain);
            $custom_field = isset($postData['custom_field']) ? json_decode(stripslashes($postData['custom_field'])) : [];
            if (!empty($custom_field)) {
                foreach ($custom_field as $key => $value) {
                    $custom_field[$key] = (array) $value;
                }
            }


            $owner_email = $postData['email'];

            $fieldArray = array(
                'first_name','last_name','maiden_name','salutation','nick_name','middle_name',
                'zipcode','city','state','country','address1','address2','address3','address4',
                'phone_number_note','company_name', 'referral_source', 'tax_payer_name',
                'tax_payer_id', 'eligible_1099', 'owners_portal', 'send_owners_package',
                'include_reports', 'draw_payment_method'
            );

            foreach ($fieldArray as $item) {
                $data[$item]    = $postData[$item];
            }

            if($postData['company_name'] == '' || empty($postData['company_name'])){
                $data['tax_payer_name'] = $postData['first_name'].' '.$postData['last_name'];
            }
            //  dd($data);
            $data['if_entity_name_display'] = (isset($postData['if_entity_name_display']) && $postData['if_entity_name_display'] == 'on') ? '1': '0';


            $data['company_name_as_tax_payer'] = (isset($postData['company_name_as_tax_payer']) && $postData['company_name_as_tax_payer'] == 'on') ? '1': '0';

            if(isset($postData['if_entity_name_display']) && $postData['if_entity_name_display'] == 'on'){
                $data['ethnicity']           = null;
                $data['maritial_status']     = null;
                $data['veteran_status']      = null;
                $data['hobbies']             = null;
                $data['ssn_sin_id']          = null;
            } else {
                $data['ethnicity']          = @$postData['ethncity'];
                $data['maritial_status']    = @$postData['maritalStatus'];
                $data['veteran_status']     = @$postData['veteranStatus'];
                if (isset($postData['hobbies'])) {
                    $data['hobbies'] = serialize($postData['hobbies']);
                }
                foreach($postData['ssn'] as $key=>$value)
                {
                    if(is_null($value) || $value == '')
                        unset($postData['ssn'][$key]);
                }
                $data['ssn_sin_id'] = !empty($postData['ssn'])? serialize($postData['ssn']) : null;

//                $data['ssn_sin_id']         = isset($postData['ssn']) ? serialize($postData['ssn']) : '';
            }

            /* if(($data['if_entity_name_display'] == '1') && isset($postData['company_name']) && !empty($postData['company_name'])){
                 $data['name'] = $postData['company_name'];
             } else {*/
//            if(($data['if_entity_name_display'] == '1') && isset($postData['company_name']) && !empty($postData['company_name'])){
//                $data['name'] = $postData['company_name'];
//            } else {
            if (!empty($data['nick_name']) && isset($data['nick_name'])) {
                $data['name'] = $postData['nick_name'] . ' ' . $postData['last_name'];
            } else {
                $data['name'] = $postData['first_name'] . ' ' . $postData['last_name'];
            }
//            }
            $data['email']                  = $owner_email[0];
//            $password                       = randomString();
//            $data['actual_password']        = $password;
//            $data['password']               = md5($password);
            $data['status']                 = '1';
            $data['portal_status']          = $postData['owners_portal'];
            $data['user_type']              = '4';
            $data['eligible_1099']          =  (isset($postData['eligible_1099']) && $postData['eligible_1099'] == 'on') ? '1' : '0';
            $data['domain_name']            = $subDomain;
            $data['created_at']             = date('Y-m-d H:i:s');
            $data['updated_at']             = date('Y-m-d H:i:s');
            $data['record_status']          = '1';
            $data['phone_type']             = (isset($postData['phone_type']))? $postData['phone_type'][0] : '';
            $data['carrier']                = (isset($postData['carrier']))? $postData['carrier'][0] : '';
            $data['phone_number']           = (isset($postData['phone_number']))? $postData['phone_number'][0] : '';
            if ( $data['phone_type'] == 5 ||  $data['phone_type']  == 2){
                $data['other_work_phone_extension'] = (isset($postData['other_work_phone_extension']))? $postData['other_work_phone_extension'][0] : '';
            } else{
                $data['other_work_phone_extension'] = '';
            }

            $data['country_code']           = (isset($postData['country_code']))? $postData['country_code'][0] : '';
            $data['send_1099']              = (isset($postData['send_1099']) && $postData['send_1099'] == 'on') ? '1' : '0';
            $data['hold_owners_payments']   = (isset($postData['hold_owners_payments']) && $postData['hold_owners_payments'] == 'on') ? '1' : '0';
            $data['include_reports']        = (isset($postData['include_reports']) && $postData['include_reports'] == 'on') ? '1' : '0';
            $data['email_financial_info']   = (isset($postData['email_financial_info']) && $postData['email_financial_info'] == 'on') ? '1' : '0';


            if (isset($postData['dob']) && $postData['dob'] != "") {
                $data['dob'] = mySqlDateFormat($postData['dob'],null,$this->companyConnection);
            }else{
                $data['dob'] ='';
                unset($data['dob']);
            }

            if (isset($postData['gender']) && $postData['gender'] != "") {
                $data['gender'] = $postData['gender'];
            }

            if(!empty($custom_field)){
                foreach ($custom_field as $key=>$value){
                    unset($data[$value['name']]);
                    if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] =  mySqlDateFormat($value['default_value'],null,$this->companyConnection);
                    if($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] =  mySqlDateFormat($value['value'],null,$this->companyConnection);
                    continue;
                }
            }
            $data['custom_fields'] = (count($custom_field)>0) ? serialize($custom_field):NULL;

            if($update) {
                $duplicate_record_check=checkMultipleDuplicatesEdit($this->companyConnection, $_POST['email'][0],$_POST['phone_number'][0],$_POST['ssn'],'','','',$postData['edit_id']);

                if(isset($duplicate_record_check)) {
                    if ($duplicate_record_check['code'] == 503) {
                        $OwnerUserDetails=array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']);
                        echo json_encode(array('OwnerUserDetails'=>$OwnerUserDetails));
                        die();
                    }
                }
            }else{
                $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, $_POST['email'][0],$_POST['phone_number'][0],$_POST['ssn'],'','','');

                if(isset($duplicate_record_check)) {
                    if ($duplicate_record_check['code'] == 503) {
                        $OwnerUserDetails=array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']);
                        echo json_encode(array('OwnerUserDetails'=>$OwnerUserDetails));
                        die();
                    }
                }
            }


            if($update)
            {
                $user_details = $this->companyConnection->query("SELECT portal_status FROM users where id=".$postData['edit_id'])->fetch();
                if ($user_details['portal_status'] == '0' && $postData['owners_portal'] == '1'){
                    $password = randomString();
                    $data['actual_password'] = $password;
                    $data['password'] = md5($password);
                }
                $sqlData = createSqlUpdateCase($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." WHERE id=".$postData['edit_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($sqlData['data']);
                $last_id = $postData['edit_id'];
                $ownerData = $data;
                $ownerData['id'] = $last_id;
                $ElasticSearchSave = insertDocument('OWNER','UPDATE',$ownerData,$this->companyConnection);
            } else {
                unset($data['image-data']);
                if ($postData['owners_portal'] == '1'){
                    $password = randomString();
                    $data['actual_password'] = $password;
                    $data['password'] = md5($password);
                }

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO users(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $last_id = $this->companyConnection->lastInsertId();
                $ownerData = $data;
                $ownerData['id'] = $last_id;
                $ElasticSearchSave = insertDocument('OWNER','ADD',$ownerData,$this->companyConnection);
            }

            if($last_id){
                $custom_data = ['is_deletable' => '0','is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if(count($custom_field)> 0){
                    foreach ($custom_field as $key=>$value){
                        updateCustomField($this->companyConnection ,$updateColumn,$value['id']);
                    }
                }
                if($update)
                {
                    $this->addOwnerDetails($_REQUEST,$last_id,$update);
                    $this->addOwnerBankingDetailDetail($last_id,$update);
                    $this->addPhoneNumberDetail($last_id,$update);
                    $this->addNotesDetail($last_id,$update);
                    $this->addPropertyOwnedDetail($last_id,$update);
                    $user_details = $this->companyConnection->query("SELECT portal_status FROM users where id=".$postData['edit_id'])->fetch();
//                    dd($user_details);
                    if ($user_details['portal_status'] == '0' && $postData['owners_portal'] == '1'){
                        $password = randomString();
                        $data['actual_password'] = $password;
                        $data['password'] = md5($password);
                        $this->sendMail($last_id);
                    }
                } else {
                    $this->addOwnerDetails($_REQUEST,$last_id);
                    $this->addOwnerBankingDetailDetail($last_id);
                    $this->addPhoneNumberDetail($last_id);
                    $this->addNotesDetail($last_id);
                    $this->addPropertyOwnedDetail($last_id);
//                    dd($_REQUEST);
                    if(!empty($_POST["hidden_customer_id"])) {
//                      //  $VendorPaymentStep1 = $this->VendorPaymentStep1($last_id, $_REQUEST['ccard_number'], $_REQUEST['ccvv'], $_REQUEST['cexpiry_year'], $_REQUEST['cexpiry_month']);
//                        dd($VendorPaymentStep1);

                        $customer_id =  $_POST["hidden_customer_id"];
                        $upateData =  "UPDATE users SET stripe_customer_id='$customer_id' where id='$last_id'";
                        $stmt = $this->companyConnection->prepare($upateData);
                        $stmt->execute();

                    }
//                    $this->sendMail($last_id);

                    if(!empty($postData["stripe_detail_form_data"])) {
                        if ($postData["hidden_account_type"] == "Company") {
                            $params = array();
                            $new_data = parse_str($postData["stripe_detail_form_data"], $params);
                            $this->createVendorCompanyAccount($params, $last_id, $postData["hidden_account_id"]);
                        } else {
                            $new_data = parse_str($postData["stripe_detail_form_data"], $params);
                            $this->createVendorIndividualAccount($params, $last_id, $postData["hidden_account_id"]);
                        }


                    }
                    if ($postData['owners_portal'] == '1'){
                        $this->sendMail($last_id);
                    }


                    /*Notification*/
                    $notificationTitle= "Owner Account Created";
                    $module_type = "OWNER";
                    $descriptionNotifi ='New Owner Account has been created.';
                    $alert_type = "Real Time";
                    insertNotification($this->companyConnection,$last_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                    /*Notification*/


                }

            }

            return array('status' => 'success', 'code' => 200, 'message' => 'Record added successfully.', 'table' => 'users', 'owner_id' => $last_id);

        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }



    /**
     *
     * @param null $userId
     * @return array
     */
    public function sendMail($userId)
    {
        try{
            $server_name = 'https://'.$_SERVER['HTTP_HOST'];
            $userId = (isset($userId) ? $userId : $_REQUEST['id']);
            $user_details = $this->companyConnection->query("SELECT * FROM users where id=".$userId)->fetch();
//            dd($user_details['email']);
            $body = getEmailTemplateData($this->companyConnection, 'newOwnerWelcome_Key');
            $body = str_replace("{Firstname}",$user_details['first_name'],$body);
            $body = str_replace("{Username}",$user_details['email'],$body);
            $logo_url = SITE_URL.'company/images/logo.png';
            $image = '<img alt="Apexlink" src="'.$logo_url.'" width="150" height="50">';
            $body = str_replace("{CompanyLogo}",$image,$body);
            $resetPasswordToken =  randomTokenString(8);
            $urlHtml = SUBDOMAIN_URL  ."/OwnerPortal/ChangePassword?owner_id=$userId&secretkey=$resetPasswordToken";
            $body = str_replace("{Url}",$urlHtml,$body);    //Need to add Portal Url here
//
//            dd($body);
            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $user_details['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal']  = '1';
//            dd($request);
            curlRequest($request);

            return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email sent successfully.'];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>504,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }
    public function addPropertyOwnedDetail($last_id,$update=null){
        try {

            if($update)
            {
                $query1 = "DELETE FROM owner_property_owned WHERE user_id =".$last_id;
                $stmt1 =$this->companyConnection->prepare($query1);
                $stmt1->execute();
            }

            $propertyIds = $_POST['property_id'];
            $i = 0;
            foreach ($propertyIds as $propertyId) {
                $data['user_id'] = $last_id;
                $data['property_id'] = $propertyId;
                $data['property_percent_owned'] = $_POST['property_percent_owned'][$i];

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO owner_property_owned(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;
            }
            return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'owner_property_owned');
        } catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function addOwnerBankingDetailDetail($last_id,$update=null){
        try {
//            dd($_POST['account_name']);
            $accountNames = $_POST['account_name'];
            if($update)
            {
                $query1 = "DELETE FROM owner_banking_information WHERE user_id =".$last_id;
                $stmt1 =$this->companyConnection->prepare($query1);
                $stmt1->execute();
            }
            $i = 0;
//            dd($accountNames);
            foreach ($accountNames as $accountName) {
                $data['user_id']                 = $last_id;
                $data['account_name']            = $accountName;
                $data['account_for_transaction'] = $_POST['account_for_transaction'][$i];
                $data['bank_account_type']       = $_POST['bank_account_type'][$i];
                $data['bank_account_number']     = $_POST['bank_account_number'][$i];
                $data['routing_transit_number']  = $_POST['routing_transit_number'][$i];
                $data['bank_institution_name']   = $_POST['bank_institution_name'][$i];
                $data['bank_fraction_number']    = $_POST['bank_fraction_number'][$i];

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO owner_banking_information(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;
            }
            return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'owner_property_owned');
        } catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function addPhoneNumberDetail($last_id,$update=null){
        try {

            if($update)
            {
                $query1 = "DELETE FROM tenant_phone WHERE user_id =".$last_id;
                $stmt1 =$this->companyConnection->prepare($query1);
                $stmt1->execute();
            }

            $phoneCarrier = $_POST['carrier'];

            $i = 0;
            foreach ($phoneCarrier as $carrier) {

                $data['user_id'] = $last_id;
                $data['parent_id'] = 0;
                if(isset($_POST['phone_type'][$i]) && $_POST['phone_type'][$i] > 0){
                    $data['phone_type'] = $_POST['phone_type'][$i];
                }
                $data['carrier'] = @$_POST['carrier'][$i];
                $data['phone_number'] = @$_POST['phone_number'][$i];
                $data['country_code'] = @$_POST['country_code'][$i];
                $data['work_phone_extension'] = @$_POST['work_phone_extension'][$i];
                if ( $_POST['phone_type'][$i] == 5 ||  $_POST['phone_type'][$i] == 2){
                    $data['other_work_phone_extension'] = (isset($_POST['other_work_phone_extension'][$i]))? $_POST['other_work_phone_extension'][$i] : '';
                } else{
                    $data['other_work_phone_extension'] = '';
                }
                $data['user_type'] = 0; //0 value for main tenant

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;
            }
            return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_phone');
        } catch (Exception $exception)
        {
            dd($exception);
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function addNotesDetail($last_id,$update=null){
        try {
            if($update)
            {
                $query1 = "DELETE FROM tenant_chargenote WHERE user_id =".$last_id;
                $stmt1 =$this->companyConnection->prepare($query1);
                $stmt1->execute();
            }

//            dd($_POST);
            $notes = $_POST['notes'];
            $i = 0;
            foreach ($notes as $note) {
                $data['user_id'] = $last_id;
                $data['note'] = $note;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                if(!empty($data['note'])) {

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_chargenote(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);

                    $i++;
                }
            }
            return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_chargenote');
        } catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function addOwnerDetails($data,$last_id,$update=null){
        try{

            $data['if_entity_name_display'] = (isset($postData['if_entity_name_display']) && $data['if_entity_name_display'] == 'on') ? '1': '0';

            $owner_details['email'] =(isset($_POST['email']))? serialize($_POST['email']) : '';
            $owner_details['entity_company'] =  $data['company_name'];

            if(!empty($_POST['emergency_contact_name'][0]) || !empty($_POST['emergency_relation'][0]) || !empty($_POST['emergency_phone'][0]) || !empty($_POST['emergency_email'][0]) ) {
                $owner_details['emergency_contact_name'] = (isset($_POST['emergency_contact_name'])) ? serialize($_POST['emergency_contact_name']) : '';
                $owner_details['emergency_relation'] = (isset($_POST['emergency_relation'])) ? serialize($_POST['emergency_relation']) : '';
                $owner_details['emergency_other_relation'] = (isset($_POST['emergency_other_relation'])) ? serialize($_POST['emergency_other_relation']) : '';
                $owner_details['emergency_country_code'] = (isset($_POST['emergency_country'])) ? serialize($_POST['emergency_country']) : '';
                $owner_details['emergency_phone_number'] = (isset($_POST['emergency_phone'])) ? serialize($_POST['emergency_phone']) : '';
                $owner_details['emergency_email'] = (isset($_POST['emergency_email'])) ? serialize($_POST['emergency_email']) : '';
            } else {
                unset($_POST['emergency_contact_name']);
                unset($_POST['emergency_relation']);
                unset($_POST['emergency_other_relation']);
                unset($_POST['emergency_country_code']);
                unset($_POST['emergency_phone_number']);
                unset($_POST['emergency_email']);
            }
            //change date format
            $acquireDateArray = (array) $_POST['acquireDate'];
            $expirationDateArray = (array) $_POST['expirationDate'];
            if(!empty($acquireDateArray)){
                foreach ($acquireDateArray as $key=>$value){
                    $acquireDateArray[$key] = mySqlDateFormat($value,null,$this->companyConnection);
                }
                $acquireDateArray = serialize($acquireDateArray);
            } else {
                $acquireDateArray = '';
            }
            if(!empty($expirationDateArray)){
                foreach ($expirationDateArray as $key=>$value){
                    $expirationDateArray[$key] = mySqlDateFormat($value,null,$this->companyConnection);
                }
                $expirationDateArray = serialize($expirationDateArray);
            } else {
                $expirationDateArray = '';
            }

            $owner_details['property_id'] = (isset($_POST['property_id']))? serialize($_POST['property_id']) : '';
            $owner_details['property_percent_owned'] = (isset($_POST['property_percent_owned']))? serialize($_POST['property_percent_owned']) : '';
            $owner_details['owner_credential_name'] = (isset($_POST['credentialName']))? serialize($_POST['credentialName']) : '';

            $owner_details['owner_credential_type'] = (isset($_POST['credentialType']))? serialize($_POST['credentialType']) : '';
            $owner_details['owner_credential_acquire_date'] =  $acquireDateArray;
            $owner_details['owner_credential_expiration_date'] =  $expirationDateArray;
            $owner_details['owner_credential_notice_period'] =  (isset($_POST['noticePeriod']))? serialize($_POST['noticePeriod']) : '';
            $owner_details['status'] = '1';
            $owner_details['created_at']         = date('Y-m-d H:i:s');
            $owner_details['updated_at']         = date('Y-m-d H:i:s');
            $owner_details['owner_image']        = json_decode($_POST['owner_image']);


            if($update)
            {
                $sqlData = createSqlUpdateCase($owner_details);
                $query = "UPDATE owner_details SET ".$sqlData['columnsValuesPair']." WHERE user_id=".$last_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($sqlData['data']);
            }else{
                $owner_details['user_id'] = $last_id;
                $sqlData = createSqlColVal($owner_details);
                $owner_query = "INSERT INTO owner_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($owner_query);             $stmt1->execute($owner_details);
            }


            if(isset($_FILES['file_library']))
            {
                $this->insertFileLibrary($_FILES['file_library'],$last_id);
            }

            return array('status' => 'success', 'message' => 'Data added successfully.', 'table' => 'users', 'owner_id' => $last_id);
        }catch (Exception $exception)
        {
            print_r($exception->getMessage()); die;
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());

        }
    }


    public function insertFileLibrary($files, $last_id, $validation_check=null)
    {
        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = "uploads/" . 'apexlink_company_' .$last_id . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($files)) {
            $countFiles = count($files['name']);

            if ($countFiles != 0) {

                $i = 0;
                if (isset($files)) {
                    foreach ($files['name'] as $key => $filename) {
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $files['name'][$key];
                            //   $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['user_id'] = $last_id;
                            $data1['filename'] = $uniqueName;
                            $data1['file_location'] = $path . '/' . $uniqueName;

                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;

                            $query = $this->companyConnection->query("SELECT count(*) as image_counts FROM tenant_chargefiles where filename='$uniqueName' and user_id = '$last_id'");
                            $queryData = $query->fetch();
                            $ownerimage_counts = (int)$queryData['image_counts'];

                            if((int)$ownerimage_counts > (int)0)
                            {
                                return array('code' => 500, 'status' => 'warning', 'message' => 'File already exists.');
                            }
                            if(!$validation_check)
                            {
                                $sqlData1 = createSqlColVal($data1);
                                $query1 = "INSERT INTO tenant_chargefiles(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                                $stmt1 = $this->companyConnection->prepare($query1);
                                $stmt1->execute($data1);
                                $tmp_name = $files["tmp_name"][$key];
                                if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                    //Directory does not exist, so lets create it.
                                    mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                                }
                                move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                            }
                        }
                        $i++;
                    }
                }
            }
        }
        return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_chargefiles');
    }


    //Add Delete
    public function delete(){

    }


    public function archive_owner()
    {
        try {
            $data=[];
            $owner_id = $_POST['user_id'];
            $this->checkOwnership($owner_id);
            $query1 = "UPDATE users SET status='2' WHERE id =".$owner_id;
            $query2 = "UPDATE owner_details SET status='2' WHERE user_id =".$owner_id;
            $stmt1 =$this->companyConnection->prepare($query1);
            $stmt2 =$this->companyConnection->prepare($query2);
            $stmt1->execute();
            $stmt2->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record archived successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function checkOwnership($id)
    {
        try{
            // dd($id);
        }catch (Exception $exception)
        {

        }
    }
    public function resign_owner()
    {
        try {
            $data=[];
            $owner_id = $_POST['user_id'];
            $query1 = "UPDATE users SET status='4' WHERE id =".$owner_id;
            $query2 = "UPDATE owner_details SET status='4' WHERE user_id =".$owner_id;
            $stmt1 =$this->companyConnection->prepare($query1);
            $stmt2 =$this->companyConnection->prepare($query2);
            $stmt1->execute();
            $stmt2->execute();

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record resigned successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function deactivate_owner()
    {
        try {
            $data=[];
            $owner_id = $_POST['user_id'];

            $query1 = "UPDATE users SET owners_portal='2' WHERE id =".$owner_id;
            $stmt1 =$this->companyConnection->prepare($query1);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deactivated successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function delete_owner()
    {
        try {
            $data=[];
            $date = date('Y-m-d H:i:s');
            $owner_id = $_POST['user_id'];
            $data['deleted_at'] = $date ;
            $data['status'] = '3';

            $sqlData = createSqlColValPair($data);

            $query1 = "UPDATE users SET ".$sqlData['columnsValuesPair']." WHERE id =".$owner_id;
            $stmt1 =$this->companyConnection->prepare($query1);
            $stmt1->execute();

            $query2 = "UPDATE owner_details SET ".$sqlData['columnsValuesPair']." WHERE user_id =".$owner_id;
            $stmt2 =$this->companyConnection->prepare($query2);
            $stmt2->execute();

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function activate_owner()
    {
        try {
            $data=[];
            $owner_id = $_POST['user_id'];
            $query1 = "UPDATE users SET owners_portal='1' WHERE id =".$owner_id;
            $stmt1 =$this->companyConnection->prepare($query1);
            $stmt1->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'This record is Activated successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function reactivate_owner()
    {
        try {
            $data=[];
            $owner_id = $_POST['user_id'];
            $query1 = "UPDATE users SET status='1' WHERE id =".$owner_id;
            $query2 = "UPDATE owner_details SET status='1' WHERE user_id =".$owner_id;
            $stmt1 =$this->companyConnection->prepare($query1);
            $stmt2 =$this->companyConnection->prepare($query2);
            $stmt1->execute();
            $stmt2->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'This record is Activated successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getviewData(){
        try{

            $owner_id=$_POST['owner_id'];
            $data = [];
            $sql_query2 = "SELECT * FROM owner_details WHERE owner_details.user_id=$owner_id";

            $data['owner_detail'] =$this->companyConnection->query($sql_query2)->fetch();

            $data['owner_detail']['email']= isset($data['owner_detail']['email']) ? unserialize($data['owner_detail']['email']) : '';

            $data['owner_detail']['contact_country_code_data']=(isset($data['owner_detail']['contact_country_code']) ? unserialize($data['owner_detail']['contact_country_code']) : '');
            $data['owner_detail']['contact_phone_number_data']=(isset($data['owner_detail']['contact_phone_number']) ? unserialize($data['owner_detail']['contact_phone_number']) : '');
            $data['owner_detail']['contact_phone_type_data']=(isset($data['owner_detail']['contact_phone_type']) ? unserialize($data['owner_detail']['contact_phone_type']) : '');

            $data['owner_detail']['emergency_contact_name'] = isset($data['owner_detail']['emergency_contact_name']) ? unserialize($data['owner_detail']['emergency_contact_name']): '';
            $data['owner_detail']['emergency_other_relation'] = isset($data['owner_detail']['emergency_other_relation']) ? unserialize($data['owner_detail']['emergency_other_relation']): '';
            $data['owner_detail']['emergency_email'] = isset($data['owner_detail']['emergency_email']) ? unserialize($data['owner_detail']['emergency_email']): '';
            $data['owner_detail']['emergency_phone_number'] = isset($data['owner_detail']['emergency_phone_number']) ? unserialize($data['owner_detail']['emergency_phone_number']) : '';
            if(isset($data['owner_detail']['emergency_relation']) && !empty($data['owner_detail']['emergency_relation'])) {
                $e_relation = unserialize($data['owner_detail']['emergency_relation']);
                $owner_emergency_relation = [];
                $data['owner_detail']['emergency_relation_new'] = ($data['owner_detail']['emergency_relation']) ? unserialize($data['owner_detail']['emergency_relation']):'';
                if (!empty($e_relation)){
                    foreach ($e_relation as $k => $v){
                        $emergency_rel = emergencyRelation($v);
                        if($emergency_rel == 'Other'){
                            $emergency_rel = $data['owner_detail']['emergency_other_relation'][$k];
                        }

                        array_push($owner_emergency_relation,$emergency_rel);
                    }
                    $data['owner_detail']["emergency_relation"] = $owner_emergency_relation;
                } else {
                    $data['owner_detail']['emergency_relation'] = '';
                }



            } else {
                $data['owner_detail']['emergency_relation'] = '';
            }
            $data['owner_detail']['edit_emergency_code'] = unserialize($data['owner_detail']['emergency_country_code']);
            if(isset($data['owner_detail']['emergency_country_code']) && !empty($data['owner_detail']['emergency_country_code'])) {
                $e_country_code = unserialize($data['owner_detail']['emergency_country_code']);
                $country_code_value = [];

                if (!empty($e_country_code)){
                    foreach ($e_country_code as $k => $v){
                        if($v)
                        {
                            $query = 'SELECT code FROM countries WHERE id="'.$v.'"';
                            $country_code = $this->companyConnection->query($query)->fetch();
                            array_push($country_code_value,$country_code['code']);
                        } else {
                            array_push($country_code_value,'');
                        }

                    }

                    $data['owner_detail']["emergency_country_code"] = $country_code_value;
                } else {
                    $data['owner_detail']['emergency_country_code'] = '';
                }
            } else {
                $data['owner_detail']['emergency_country_code'] = '';
            }

            $emergency_data = [];

            if ($data['owner_detail']['emergency_contact_name']) {
                $length = count($data['owner_detail']['emergency_contact_name']);
                for ($i = 0; $i < $length; $i++) {
                    $emergency_data[$i]['emergency_contact_name'] = (isset($data['owner_detail']['emergency_contact_name'][$i])?$data['owner_detail']['emergency_contact_name'][$i]:'');
                    $emergency_data[$i]['emergency_relation'] = (isset($data['owner_detail']['emergency_relation'][$i])?$data['owner_detail']['emergency_relation'][$i]:'');
                    $emergency_data[$i]['emergency_country_code'] = (isset($data['owner_detail']['emergency_country_code'][$i])?$data['owner_detail']['emergency_country_code'][$i]:'');
                    $emergency_data[$i]['emergency_phone_number'] = (isset($data['owner_detail']['emergency_phone_number'][$i])?$data['owner_detail']['emergency_phone_number'][$i]:'');
                    $emergency_data[$i]['emergency_email'] = (isset($data['owner_detail']['emergency_email'][$i])?$data['owner_detail']['emergency_email'][$i]:'');
                }
            }


            $data['owner_detail']['owner_credential_expiration_date'] = isset($data['owner_detail']['owner_credential_expiration_date']) ? unserialize($data['owner_detail']['owner_credential_expiration_date']) : '';
            $data['owner_detail']['owner_credential_name'] = isset($data['owner_detail']['owner_credential_name']) ? unserialize($data['owner_detail']['owner_credential_name']) : '';
            $data['owner_detail']['owner_credential_acquire_date'] = isset($data['owner_detail']['owner_credential_acquire_date']) ? unserialize($data['owner_detail']['owner_credential_acquire_date']) : '';
            if(isset($data['owner_detail']['owner_credential_notice_period']) && !empty($data['owner_detail']['owner_credential_notice_period'])) {
                $cred_notice_period = unserialize($data['owner_detail']['owner_credential_notice_period']);
                $owner_cred_notice_period = [];

                if (is_array($cred_notice_period) && !empty($cred_notice_period)){
                    foreach ($cred_notice_period as $k => $v){
                        $cred_period = noticeperiod($v);

                        array_push($owner_cred_notice_period,$cred_period);
                    }
                    $data['owner_detail']["owner_credential_notice_period"] = $owner_cred_notice_period;
                } else {
                    $data['owner_detail']['owner_credential_notice_period'] = '';
                }
            } else {
                $data['owner_detail']['owner_credential_notice_period'] = '';
            }

            if(isset($data['owner_detail']['owner_credential_type']) && !empty($data['owner_detail']['owner_credential_type'])) {
                $owner_cred_type = unserialize($data['owner_detail']['owner_credential_type']);
                $cred_type= [];
                if (!empty($owner_cred_type)){

                    foreach ($owner_cred_type as $k => $v){

                        if(isset($v) && !empty($v))
                        {
                            $query = 'SELECT credential_type FROM tenant_credential_type WHERE id='.@$v;
                            $credential_type = $this->companyConnection->query($query)->fetch();
                            array_push($cred_type,$credential_type['credential_type'] );
                        }

                    }
                    $data['owner_detail']["owner_credential_type"] = $cred_type;
                } else {
                    $data['owner_detail']['owner_credential_type'] = '';
                }
            } else {
                $data['owner_detail']['owner_credential_type'] = '';
            }

            $credential_data = [];
            if ($data['owner_detail']['owner_credential_name']) {
                $len = count($data['owner_detail']['owner_credential_name']);
                for ($i = 0; $i < $len; $i++) {
                    $credential_data[$i]['owner_credential_name'] = (isset($data['owner_detail']['owner_credential_name'][$i]) ? $data['owner_detail']['owner_credential_name'][$i] : '');
                    $credential_data[$i]['owner_credential_type'] = isset( $data['owner_detail']['owner_credential_type'][$i])? $data['owner_detail']['owner_credential_type'][$i] : '';
                    $credential_data[$i]['owner_credential_expiration_date'] = isset($data['owner_detail']['owner_credential_expiration_date'][$i]) ? dateFormatUser($data['owner_detail']['owner_credential_expiration_date'][$i], null, $this->companyConnection):'';
                    $credential_data[$i]['owner_credential_acquire_date'] = isset($data['owner_detail']['owner_credential_acquire_date'][$i]) ? dateFormatUser($data['owner_detail']['owner_credential_acquire_date'][$i], null, $this->companyConnection):'';
                    $credential_data[$i]['owner_credential_notice_period'] = isset($data['owner_detail']['owner_credential_notice_period'][$i])?$data['owner_detail']['owner_credential_notice_period'][$i]:'';
                }
            }

            $sql_query1 = "SELECT users. *,
                           tenant_ethnicity.title as ethnicity_name,
                           tenant_marital_status.marital as marital_status,
                           tenant_veteran_status.veteran as veteran_name,
                           tenant_veteran_status.veteran as veteran_name
                        FROM users 
                        LEFT JOIN tenant_ethnicity ON users.ethnicity=tenant_ethnicity.id
                        LEFT JOIN tenant_marital_status ON users.maritial_status=tenant_marital_status.id
                        LEFT JOIN tenant_veteran_status ON users.veteran_status=tenant_veteran_status.id
                        WHERE users.id=$owner_id";


            $data['user_detail'] =$this->companyConnection->query($sql_query1)->fetch();

//            $data['user_detail']['name'] = userName($owner_id, $this->companyConnection);
            $address1 = isset($data['user_detail']['address1']) ? $data['user_detail']['address1'] : '';
            $address2 = isset($data['user_detail']['address2']) ? $data['user_detail']['address2'] : '';
            $address3 = isset($data['user_detail']['address3']) ? $data['user_detail']['address3'] : '';
            $address4 = isset($data['user_detail']['address4']) ? $data['user_detail']['address4'] : '';
            $data['user_detail']['owner_address'] = $address1.' '.$address2.' '.$address3.' '.$address4;

            $data['user_detail']['phone_number_note'] = isset($data['user_detail']['phone_number_note']) ? $data['user_detail']['phone_number_note'] : 'N/A';
            $data['credential_data'] = $credential_data;


            if(isset($data['user_detail']['hobbies']) && !empty($data['user_detail']['hobbies'])) {
                $hobbies = isset($data['user_detail']['hobbies']) ? unserialize($data['user_detail']['hobbies']) : '';
                $hobby_name='';

                if(!empty($hobbies)){
                    foreach ($hobbies as $key => $value){
                        $query = 'SELECT hobby FROM hobbies WHERE id='.$value;
                        $groupData = $this->companyConnection->query($query)->fetch();
                        $name = $groupData['hobby'];
                        $hobby_name .= $name.',';
                    }
                    $data['user_detail']["hobbies"] = $hobby_name;
                }else{
                    $data['user_detail']["hobbies"] = "";
                }
            }else{
                $data['user_detail']["hobbies"] = "";
            }

            $data['banking_detail'] =$this->companyConnection->query("SELECT * FROM users WHERE id=$owner_id")->fetch();
            if(!empty($data['user_detail']) && count($data['user_detail']) > 0){
                $custom_data = !empty($data['user_detail']['custom_fields']) ? unserialize($data['user_detail']['custom_fields']) : [];
                if(!empty($custom_data)){
                    foreach ($custom_data as $key=>$value){
                        if($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_data[$key]['default_value'] = dateFormatUser($value['default_value'],null,$this->companyConnection);
                        if($value['data_type'] == 'date' && !empty($value['value'])) $custom_data[$key]['value'] = dateFormatUser($value['value'],null,$this->companyConnection);
                        continue;
                    }
                }
                $data['custom_data'] = $custom_data;
            }

            $data['user_detail']['dob'] = isset($data['user_detail']['dob']) ? dateFormatUser($data['user_detail']['dob'],null,$this->companyConnection) : '';
            $data['user_detail']['current_date'] = dateFormatUser(date('Y-m-d'),null,$this->companyConnection);
            $data['user_detail']['ssn_sin_id'] = isset($data['user_detail']['ssn_sin_id']) ? unserialize($data['user_detail']['ssn_sin_id']) : 'N/A';
            $data['intial_data'] = $this->getIntialData(true);
            //Owner Banking Information
            $data['owner_banking_information']  = [];
            $data['owner_banking_information'] =$this->companyConnection->query("SELECT * FROM owner_banking_information WHERE user_id=$owner_id")->fetchAll();

            //Owner Property Owned Information
            $data['owner_property_owned_info']  = [];
            $data['owner_property_owned_info'] =$this->companyConnection->query("SELECT * FROM owner_property_owned WHERE user_id=$owner_id")->fetchAll();

            $data['tenant_chargenote']  = [];
            $data['tenant_chargenote'] =$this->companyConnection->query("SELECT * FROM tenant_chargenote WHERE user_id=$owner_id")->fetchAll();

            $tenant_phone_query= $this->companyConnection->query("SELECT * FROM tenant_phone WHERE tenant_phone.user_id=$owner_id")->fetchAll();
            $data['owner_detail']['contact_carrier_data']=$tenant_phone_query;
//            $data['owner_phone_Info'] = $this->getPhoneInfo($owner_id);
            return ['code'=>200, 'status'=>'success', 'data'=>$data ,'emergency_data'=>$emergency_data,'credential_data'=>$credential_data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function exportExcel()
    {
        include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");

        $objPHPExcel = new PHPExcel();
        $query1 = "select u.id,u.salutation,u.first_name,u.last_name,u.email,u.if_entity_name_display,u.company_name,u.middle_name,u.zipcode,u.country,
              u.state,u.city,u.address1,u.address2,u.address3,u.address4,u.phone_type,u.phone_number,u.email,ob.account_name ,ob.account_for_transaction,ob.account_for_transaction,ob.bank_account_type,ob.bank_account_number,ob.routing_transit_number,ob.bank_institution_name,ob.bank_fraction_number,u.draw_payment_method,u.tax_payer_name,u.tax_payer_id,u.eligible_1099,u.send_1099,u.send_owners_package,po.property_percent_owned,po.property_percent_owned,u.owners_portal,u.send_owners_package,u.hold_owners_payments,u.email_financial_info,u.include_reports,u.notes,cr.carrier from users u JOIN owner_details od ON u.id=od.user_id LEFT JOIN owner_banking_information ob ON u.id=ob.user_id LEFT JOIN owner_property_owned po ON u.id=po.user_id LEFT JOIN carrier cr ON cr.id=u.carrier where u.user_type='4' and u.deleted_at IS NULL ORDER BY u.first_name";

        $data = $this->companyConnection->query($query1)->fetchAll();

        $data1 = [];
        foreach ($data as $key=>$value)
        {
            $data1[$value['id']]['id']          = @$value['id'];
            $data1[$value['id']]['salutation']  = @$value['salutation'];
            $data1[$value['id']]['first_name']  = @$value['first_name'];
            $data1[$value['id']]['last_name']   = @$value['last_name'];
            $data1[$value['id']]['email']       = @$value['email'];
            $data1[$value['id']]['if_entity_name_display']  = @$value['if_entity_name_display'];
            $data1[$value['id']]['company_name']  = @$value['company_name'];
            $data1[$value['id']]['middle_name']  = @$value['middle_name'];
            $data1[$value['id']]['zipcode']  = @$value['zipcode'];
            $data1[$value['id']]['country']  = @$value['country'];
            $data1[$value['id']]['state']  = @$value['state'];
            $data1[$value['id']]['city']  = @$value['city'];
            $data1[$value['id']]['address1']  = @$value['address1'];
            $data1[$value['id']]['address2']  = @$value['address2'];
            $data1[$value['id']]['address3']  = @$value['address3'];
            $data1[$value['id']]['address4']  = @$value['address4'];
            $data1[$value['id']]['phone_type']  = @$value['phone_type'];
            $data1[$value['id']]['phone_number']  = @$value['phone_number'];
            $data1[$value['id']]['account_name'][]  = @$value['account_name'];
            $data1[$value['id']]['account_for_transaction'][] = @$value['account_for_transaction'];
            $data1[$value['id']]['bank_account_type'][]  = @$value['bank_account_type'];
            $data1[$value['id']]['bank_account_number'][]  = @$value['bank_account_number'];
            $data1[$value['id']]['routing_transit_number'][]  = @$value['routing_transit_number'];
            $data1[$value['id']]['bank_institution_name'][]  = @$value['bank_institution_name'];
            $data1[$value['id']]['bank_fraction_number'][]  = @$value['bank_fraction_number'];
            $data1[$value['id']]['draw_payment_method']  = @$value['draw_payment_method'];
            $data1[$value['id']]['tax_payer_name']  = @$value['tax_payer_name'];
            $data1[$value['id']]['tax_payer_id']  = @$value['tax_payer_id'];
            $data1[$value['id']]['eligible_1099']  = @$value['eligible_1099'];
            $data1[$value['id']]['send_1099']  = @$value['send_1099'];
            $data1[$value['id']]['send_owners_package']  = @$value['send_owners_package'];
            $data1[$value['id']]['property_percent_owned'][]  = @$value['property_percent_owned'];
            $data1[$value['id']]['owners_portal']  = @$value['owners_portal'];
            $data1[$value['id']]['hold_owners_payments']  = @$value['hold_owners_payments'];
            $data1[$value['id']]['email_financial_info']  = @$value['email_financial_info'];
            $data1[$value['id']]['include_reports']  = @$value['include_reports'];
            $data1[$value['id']]['notes']  = @$value['notes'];
            $data1[$value['id']]['carrier'][]  = $value['carrier'];


        }

        $columnHeadingArr =array("Salutation","FirstName","MiddleName","LastName","Company","Usecompanynameasdisplayname","ZipCode","Country","State","City","Address1","Address2","PhoneType","Carrier","PhoneNumber","Email","BankAccforACHTransaction","BankAccountType","BankAccountNumber","BankRouting","BankInstitutionName","BankFractionNo","DrawPaymentMethod","TaxPayerName","TaxPayerID","Eligiblefor1099","Send1099","CompanyNameastaxpayer","PropertyName","PercentOwned","OwnersPortal","SendOwnersPackage","HoldOwnersPayments","Emailstatement&otherfinancialInfo","IncludeReports","Notes");
        //take new main array and set header array in it.
        $sheet =array($columnHeadingArr);
        // dd($data1);

        $i = 0;

        foreach($data1 as $key=>$value)
        {//dd($value['id']);

            $keyid= $i+1;
            $sheet[$keyid][0] = $value['salutation'];
            $sheet[$keyid][1] = $value['first_name'];
            $sheet[$keyid][2] = $value['middle_name'];
            $sheet[$keyid][3] = $value['last_name'];
            $sheet[$keyid][4] = $value['company_name'];
            $sheet[$keyid][5] = ($value['if_entity_name_display'] == '1')? 'Yes': 'No';
            $sheet[$keyid][6] = $value['zipcode'];
            $sheet[$keyid][7] = $value['country'];
            $sheet[$keyid][8] = $value['state'];
            $sheet[$keyid][9] = $value['city'];
            $sheet[$keyid][10] = $value['address1'];
            $sheet[$keyid][11] = $value['address2'];
            $sheet[$keyid][12] = $this->phone_type($value['phone_type']);
            $sheet[$keyid][13] = implode(array_unique($value['carrier']),',');
            $sheet[$keyid][14] = $value['phone_number'];
            $sheet[$keyid][15] = $value['email'];
            $sheet[$keyid][16] = implode(array_unique($value['account_for_transaction']),',');
            $sheet[$keyid][17] = implode(array_unique($value['bank_account_type']),',');
            $sheet[$keyid][18] = implode(array_unique($value['bank_account_number']),',');
            $sheet[$keyid][19] = implode(array_unique($value['routing_transit_number']),',');
            $sheet[$keyid][20] = implode(array_unique($value['bank_institution_name']),',');
            $sheet[$keyid][21] = implode(array_unique($value['bank_fraction_number']),',');
            $sheet[$keyid][22] = $value['draw_payment_method'];
            $sheet[$keyid][23] = $value['tax_payer_name'];
            $sheet[$keyid][24] = $value['tax_payer_id'];
            $sheet[$keyid][25] = $value['eligible_1099'];
            $sheet[$keyid][26] = $value['send_1099'];
            $sheet[$keyid][27] = $value['tax_payer_name'];
            $sheet[$keyid][28] = '';
            $sheet[$keyid][29] = implode(array_unique($value['property_percent_owned']),',');
            $sheet[$keyid][30] = ($value['owners_portal'] == '1')?'Yes':'No';
            $sheet[$keyid][31] = ($value['send_owners_package'] == '1')? 'Yes': 'No';
            $sheet[$keyid][32] = ($value['hold_owners_payments'] == '1')? 'Yes': 'No';
            $sheet[$keyid][33] = $value['email_financial_info'];
            $sheet[$keyid][34] = $value['include_reports'];
            $sheet[$keyid][35] = $value['notes'];
            $i++;
        }

        $worksheet = $objPHPExcel->getActiveSheet();
        foreach($sheet as $row => $columns) {
            foreach($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
        //make first row bold
        $objPHPExcel->getActiveSheet()->getStyle("A1:AA1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Owner_details.xlsx"');
        $objWriter->save('php://output');
        die();
    }

    public function phone_type($phone_type)
    {
        switch ($phone_type)
        {
            case '1':
                $phn_type = 'mobile';
                break;
            case '2':
                $phn_type = 'work';
                break;
            case '3':
                $phn_type = 'Fax';
                break;
            case '4':
                $phn_type = 'Home';
                break;
            case '5':
                $phn_type = 'Other';
                break;
            default:
                $phn_type = '';
        }
        return $phn_type;
    }

    public function updateReasonForLeaving(){
        try {
            $data = $_POST['form'];
            $updateData['reason_for_leaving']      = $data['reason_for_leaving'];
            if($updateData['reason_for_leaving'] == 'Other') {
                $updateData['reason_for_leaving_note'] = $data['reason_for_leaving_note'];
            } else {
                $updateData['reason_for_leaving_note'] = '';
            }

            $sqlData = createSqlColValPair($updateData);
            $query = "UPDATE owner_details SET ".$sqlData['columnsValuesPair']." WHERE user_id =".$data['reason_for_leaving_ownerId'];

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Reason for leaving updated successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * @return array
     */
    public function importExcel()
    {
        try{
            if (isset($_FILES['file'])) {
                if (isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                    $allowedExtensions = array("xls", "xlsx", "csv");
                    $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                    if (in_array($ext, $allowedExtensions)) {
                        $file_size = $_FILES['file']['size'] / 1024;
                        if ($file_size < 3000) {
                            $file = "uploads/" . $_FILES['file']['name'];
                            $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                            if ($isUploaded) {
                                include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                                try {
                                    //Load the excel(.xls/.xlsx/ .csv) file
                                    $objPHPExcel = PHPExcel_IOFactory::load($file);
                                } catch (Exception $e) {
                                    $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                    return ['status' => 'failed', 'code' => 503, 'data' => $error_message];
                                    printErrorLog($e->getMessage());
                                }
                                //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                                $sheet = $objPHPExcel->getSheet(0);
                                //It returns the highest number of rows
                                $total_rows = $sheet->getHighestRow();
                                //It returns the first element of 'A1'
                                $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
                                //It returns the highest number of columns
                                $total_columns = $sheet->getHighestDataColumn();
                                $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                for ($row = 2; $row <= $total_rows; $row++) {
                                    //Read a single row of data and store it as a array.
                                    //This line of code selects range of the cells like A1:D1
                                    $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);
                                    //Creating a dynamic query based on the rows from the excel file

                                    if ($first_row_header == 'Salutation') {
                                        foreach ($single_row[0] as $key => $value) {
//                                            dd($single_row[0]);
                                            if( $key == '1' || $key == '3' || $key == '13' || $key == '14' ||
                                                $key == '15' || $key == '28' || $key == '29' || $key == '30'){

                                            } else if (empty($value)){
                                                continue;
                                            }
                                            switch ($key) {
                                                case "1":
                                                    if($value == '' || $value == null){
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'First Name is required.'];
                                                    }
                                                    break;
                                                case "3":
                                                    if($value == '' || $value == null){
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Last Name is required.'];
                                                    }
                                                    break;
                                                case "5":
                                                    $single_row[0][$key] = ($value == 'Yes' || $value == 'yes') ? '1' : '0';
                                                    break;
                                                case "12":
                                                    $phoneType = getSingleRecord($this->companyConnection, ['column' => 'type', 'value' => $value], 'phone_type');
                                                    if ($phoneType['code'] == 200) {
                                                        $single_row[0][$key] = $phoneType['data']['id'];
                                                    } else {
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Phone type not found'];
                                                    }
                                                    break;
                                                case "13":
                                                    if($value == '' || $value == null){
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Carrier is required.'];
                                                    } else {
                                                        $carrierData = getSingleRecord($this->companyConnection, ['column' => 'carrier', 'value' => $value], 'carrier');
                                                        if ($carrierData['code'] == 200) {
                                                            $single_row[0][$key] = $carrierData['data']['id'];
                                                        } else {
                                                            return ['status' => 'failed', 'code' => 503, 'message' => 'Carrier not found'];
                                                        }
                                                    }
                                                    break;
                                                case "14":
                                                    if($value == '' || $value == null){
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Phone Number is required.'];
                                                    } else {
                                                        if (!is_numeric($value)) {
                                                            return ['status' => 'failed', 'code' => 503, 'message' => 'Phone Number only allow numerical values.'];
                                                        } else {
                                                            $phone = formatPhoneNum($value);
                                                            if ($phone == 'lengthError') {
                                                                return ['status' => 'failed', 'code' => 503, 'message' => 'Phone Number should not be less/greater than 10 digits.'];
                                                            } else {
                                                                $single_row[0][$key] = $phone;
                                                            }
                                                        }
                                                    }
                                                    break;
                                                case "15":
                                                    if($value == '' || $value == null){
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Email is required.'];
                                                    } else {
                                                        $emailData = getSingleRecord($this->companyConnection, ['column' => 'email', 'value' => $value], 'users');
                                                        if ($emailData['code'] == 200) {
                                                            return ['status' => 'warning', 'code' => 503, 'message' => $value . ' already exist in the system.'];
                                                        } else {
                                                            $single_row[0][$key] = $value;
                                                        }
                                                    }
                                                    break;
                                                case "16":
                                                    $chartAccountData = getSingleRecord($this->companyConnection, ['column' => 'account_name', 'value' => $value], 'company_chart_of_accounts');
                                                    if ($chartAccountData['code'] == 200) {
                                                        $single_row[0][$key] = $chartAccountData['data']['id'];
                                                    } else {
                                                        $insert = [];
                                                        $insert['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                                                        $insert['account_name'] = $value;
                                                        $insert['account_type_id'] = '1';
                                                        $insert['account_code'] = '1111';
                                                        $insert['reporting_code'] = '2';
                                                        $insert['posting_status'] = '1';
                                                        $insert['is_default'] = '0';
                                                        $insert['is_editable'] = '1';
                                                        $insert['status'] = '1';
                                                        $insert['created_at'] = date('Y-m-d H:i:s');
                                                        $insert['updated_at'] = date('Y-m-d H:i:s');
                                                        $sqlData = createSqlColVal($insert);
                                                        $query = "INSERT INTO company_chart_of_accounts (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                        $stmt = $this->companyConnection->prepare($query);
                                                        $stmt->execute($insert);
                                                        $single_row[0][$key] = $this->companyConnection->lastInsertId();
                                                    }
                                                    break;
                                                case "17":
                                                    $single_row[0][$key] = $this->bankAccountTypeOwner($single_row[0][17]);
                                                    break;
                                                case "25":
                                                    $single_row[0][$key] = (empty($value) || $value == 'Yes' || $value == 'yes') ? '1' : '0';
                                                    break;
                                                case "26":
                                                    $single_row[0][$key] = ($value == 'Yes' || $value == 'yes') ? '1' : '0';
                                                    break;
                                                case "27":
                                                    $single_row[0][$key] = ($value == 'Yes' || $value == 'yes') ? '1' : '0';
                                                    break;
                                                case "28":
                                                    if($value == '' || $value == null){
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Property Name is required.'];
                                                    } else if($single_row[0]['29'] == '' || $single_row[0]['29'] == null){
                                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Property Percent Owned is required.'];
                                                    } else {
                                                        $data['property_list'] = $this->companyConnection->query("SELECT id,property_id, property_name FROM general_property WHERE status = 1")->fetchAll();
                                                        foreach ($data['property_list'] as $key1 => $value1) {
                                                            if (!empty($value1)) {
                                                                $propertyPercent = $this->companyConnection->query("SELECT SUM(property_percent_owned) FROM owner_property_owned WHERE property_id=" . $value1['id'])->fetch();
                                                                $data['property_list'][$key1]['property_percent_owned'] = !empty($propertyPercent['SUM(property_percent_owned)']) ? $propertyPercent['SUM(property_percent_owned)'] : 0;
                                                            }
                                                        }
                                                        $propertyData = getSingleRecord($this->companyConnection, ['column' => 'property_name', 'value' => $value], 'general_property');
                                                        if ($propertyData['code'] == 200) {
                                                            foreach ($data['property_list'] as $k => $v) {
                                                                if (!empty($v)) {
                                                                    if ($v['id'] == $propertyData['data']['id']) {
                                                                        $remaining_percentage = 100 - $v['property_percent_owned'];
                                                                        if ($single_row[0]['29'] > 100) {
                                                                            return ['status' => 'failed', 'code' => 503, 'message' => "Property percent owned of  '" . $propertyData['data']['property_name'] . "' property should not be greater than 100."];
                                                                        } else if ($remaining_percentage == 0) {
                                                                            return ['status' => 'failed', 'code' => 503, 'message' => $propertyData['data']['property_name'] . ' is 100% owned by other owners.'];
                                                                        } else if ($single_row[0]['29'] > $remaining_percentage) {
                                                                            return ['status' => 'failed', 'code' => 503, 'message' => 'Exceed the limit of remaining percentage of ' . $propertyData['data']['property_name'] . ' property. 
                                                                                                                                   Limit should be less than or equal to ' . $remaining_percentage . '.'];
                                                                        } else {
                                                                            $single_row[0][$key] = $propertyData['data']['id'];
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            $insert = [];
                                                            $insert['property_name'] = $value;
                                                            $insert['status'] = '1';
                                                            $insert['created_at'] = date('Y-m-d H:i:s');
                                                            $insert['update_at'] = date('Y-m-d H:i:s');
                                                            $sqlData = createSqlColVal($insert);
                                                            $query = "INSERT INTO general_property (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                                            $stmt = $this->companyConnection->prepare($query);
                                                            $stmt->execute($insert);
                                                            $single_row[0][$key] = $this->companyConnection->lastInsertId();
                                                        }
                                                    }
                                                    break;
                                                case "30":
                                                    $single_row[0][$key] = ($value == 'Yes' || $value == 'yes') ? '1' : '0';
                                                    break;
                                                case "31":
                                                    $single_row[0][$key] = ($value == 'fax' || $value == 'Fax') ? '1' :(($value == 'email' || $value == 'Email') ? '0': '');
                                                    break;
                                                case "32":
                                                    $single_row[0][$key] = ($value == 'Yes' || $value == 'yes') ? '1' : '0';
                                                    break;
                                                case "33":
                                                    $single_row[0][$key] = ($value == 'Yes' || $value == 'yes') ? '1' : '0';
                                                    break;
                                                case "34":
                                                    $single_row[0][$key] = ($value == 'Yes' || $value == 'yes') ? '1' : '0';
                                                    break;
                                                default:
//                                                    continue;
                                            }
                                        }

                                        $domain = explode('.', $_SERVER['HTTP_HOST']);
                                        $subdomain = array_shift($domain);
                                        $users = [];
                                        $users['salutation'] = $single_row[0][0];
                                        $users['first_name'] = $single_row[0][1];
                                        $users['last_name'] = $single_row[0][3];
                                        $users['middle_name'] = $single_row[0][2];
                                        $users['status'] = '1';
                                        $users['user_type'] = '4';
                                        $users['domain_name'] = $subdomain;
                                        $users['record_status'] = '1';

                                        $users['company_name'] = $single_row[0][4];
                                        $users['if_entity_name_display'] = $single_row[0][5];

                                        if(($users['if_entity_name_display'] == '1') && isset($users['company_name']) && !empty($users['company_name'])){
                                            $users['name'] =  $users['company_name'];
                                        } else {
                                            $users['name'] = $users['first_name'] . ' ' . $users['last_name'];
                                        }
                                        $users['zipcode'] = $single_row[0][6];
                                        $users['city'] = $single_row[0][9];
                                        $users['state'] = $single_row[0][8];
                                        $users['country'] = $single_row[0][7];
                                        $users['address1'] = $single_row[0][10];
                                        $users['address2'] = $single_row[0][11];
                                        $users['phone_type'] = $single_row[0][12];
                                        $users['carrier'] = $single_row[0][13];
                                        $users['phone_number'] = $single_row[0][14];

                                        $users['email'] = $single_row[0][15];

                                        $users['tax_payer_name'] = $single_row[0][23];
                                        $users['tax_payer_id'] = $single_row[0][24];
                                        $users['eligible_1099'] = $single_row[0][25];
                                        $users['send_1099'] = $single_row[0][26];
                                        $users['company_name_as_tax_payer'] = $single_row[0][27];
                                        $users['owners_portal'] = $single_row[0][30];
                                        $users['send_owners_package'] = $single_row[0][31];
                                        $users['hold_owners_payments'] = $single_row[0][32];
                                        $users['email_financial_info'] = $single_row[0][33];
                                        $users['include_reports'] = $single_row[0][34];
                                        $users['notes'] = $single_row[0][35];
                                        $users['created_at'] = date('Y-m-d H:i:s');
                                        $users['updated_at'] = date('Y-m-d H:i:s');
                                        $sqlData = createSqlColVal($users);
                                        $query = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                        $stmt = $this->companyConnection->prepare($query);
                                        $executed_query = $stmt->execute($users);
                                        $owner_id = $this->companyConnection->lastInsertId();
                                        $ownerInfo = [];
                                        $ownerInfo['user_id'] = $owner_id;
                                        $ownerInfo['status'] = '1';
                                        $ownerInfo['entity_company'] = $single_row[0][4];
                                        $ownerInfo['if_entity_name_display'] = $single_row[0][5];
                                        $ownerInfo['owner_image'] = '<img src="'.COMPANY_SITE_URL.'/images/dummy-img.jpg">';
                                        $data_email[] = $single_row[0][15];
                                        $ownerInfo['email'] = isset($single_row[0][15]) ? serialize($data_email) : '';
                                        $ownerInfo['created_at'] = date('Y-m-d H:i:s');
                                        $ownerInfo['updated_at'] = date('Y-m-d H:i:s');
                                        $vendorSqlData = createSqlColVal($ownerInfo);
                                        $query2 = "INSERT INTO owner_details (" . $vendorSqlData['columns'] . ") VALUES (" . $vendorSqlData['columnsValues'] . ")";
                                        $stmt2 = $this->companyConnection->prepare($query2);
                                        $executed_query2 = $stmt2->execute($ownerInfo);
                                        $phoneInfo = [];
                                        $phoneInfo['user_id'] = $owner_id;
                                        $phoneInfo['parent_id'] = 0;
                                        $phoneInfo['user_type'] = '4';
                                        $phoneInfo['phone_type'] = $single_row[0][12];
                                        $phoneInfo['carrier'] = $single_row[0][13];
                                        $phoneInfo['phone_number'] = $single_row[0][14];
                                        $phoneInfo['created_at'] = date('Y-m-d H:i:s');
                                        $phoneInfo['updated_at'] = date('Y-m-d H:i:s');
                                        $phoneSqlData = createSqlColVal($phoneInfo);
                                        $query3 = "INSERT INTO tenant_phone (" . $phoneSqlData['columns'] . ") VALUES (" . $phoneSqlData['columnsValues'] . ")";
                                        $stmt3 = $this->companyConnection->prepare($query3);
                                        $executed_query3 = $stmt3->execute($phoneInfo);

                                        $note = [];
                                        $note['user_id'] = $owner_id;
                                        $note['note'] = $single_row[0][35];
                                        $note['created_at'] = date('Y-m-d H:i:s');
                                        $note['updated_at'] = date('Y-m-d H:i:s');
                                        $noteSqlData = createSqlColVal($note);
                                        $query4 = "INSERT INTO tenant_chargenote (" . $noteSqlData['columns'] . ") VALUES (" . $noteSqlData['columnsValues'] . ")";
                                        $stmt4 = $this->companyConnection->prepare($query4);
                                        $executed_query4 = $stmt4->execute($note);

                                        $propertyOwnedInfo = [];
                                        $propertyOwnedInfo['user_id'] = $owner_id;
                                        $propertyOwnedInfo['property_id'] = $single_row[0][28];
                                        $propertyOwnedInfo['property_percent_owned'] = $single_row[0][29];
                                        $propertyOwnedInfo['created_at'] = date('Y-m-d H:i:s');
                                        $propertyOwnedInfo['updated_at'] = date('Y-m-d H:i:s');
                                        $phoneSqlData = createSqlColVal($propertyOwnedInfo);
                                        $query3 = "INSERT INTO owner_property_owned (" . $phoneSqlData['columns'] . ") VALUES (" . $phoneSqlData['columnsValues'] . ")";
                                        $stmt3 = $this->companyConnection->prepare($query3);
                                        $executed_query5 = $stmt3->execute($propertyOwnedInfo);

                                        $bankInfo = [];
                                        $bankInfo['user_id'] = $owner_id;
                                        $bankInfo['account_name'] = '';
                                        $bankInfo['account_for_transaction'] = $single_row[0][16];
                                        $bankInfo['bank_account_type'] = $single_row[0][17];
                                        $bankInfo['bank_account_number'] = $single_row[0][18];
                                        $bankInfo['routing_transit_number'] = $single_row[0][19];
                                        $bankInfo['bank_institution_name'] = $single_row[0][20];
                                        $bankInfo['bank_fraction_number'] = $single_row[0][21];
                                        $bankInfo['created_at'] = date('Y-m-d H:i:s');
                                        $bankInfo['updated_at'] = date('Y-m-d H:i:s');
                                        $phoneSqlData = createSqlColVal($bankInfo);
                                        $query3 = "INSERT INTO owner_banking_information (" . $phoneSqlData['columns'] . ") VALUES (" . $phoneSqlData['columnsValues'] . ")";
                                        $stmt3 = $this->companyConnection->prepare($query3);
                                        $executed_query6 = $stmt3->execute($bankInfo);
                                    } else {
                                        return ['status' => 'failed', 'code' => 503, 'message' => 'Please select valid file.'];
                                    }
                                }
                                if (isset($executed_query6)) {
                                    unlink($file);
                                    return ['status' => 'success', 'code' => 200, 'message' => 'Data imported successfully.'];
                                } else {
                                    return ['status' => 'failed', 'code' => 503, 'message' => 'There is some error in your file.'];
                                }
                            } else {
                                return ['status' => 'failed', 'code' => 503, 'message' => 'File not uploaded!'];
                                //echo '<span class="msg">File not uploaded!</span>';
                            }
                        } else {
                            return ['status' => 'failed', 'code' => 503, 'message' => 'Maximum file size should not cross 3 MB on size!'];
                            //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                        }
                    } else {
                        return ['status' => 'failed', 'code' => 503, 'message' => 'Only .xls/.xlsx/.csv files are accepted.'];
                        //echo '<span class="msg">This type of file not allowed!</span>';
                    }
                } else {
                    return ['status' => 'failed', 'code' => 503, 'message' => 'Select an excel file first!'];
                    //echo '<span class="msg">Select an excel file first!</span>';
                }
            }
        } catch (Exception $exception) {
            dd($exception);
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    public function bankAccountTypeOwner($val){
        $value = strtolower($val);
        if($value == 'business checking account'){
            return '1';
        } else if($value == 'personal checking account'){
            return '2';
        }else if($value == 'business savings account'){
            return '3';
        }else if($value == 'personal savings account'){
            return '4';
        }else if($value == 'credit'){
            return '5';
        }else if($value == 'undeposited funds'){
            return '6';
        }else if($value == 'other'){
            return '7';
        } else {
            return '';
        }

    }
    public function getPhoneInfo($owner_id)
    {
        $getPhoneInfo = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE parent_id=0 and user_id ='".$owner_id."'")->fetchAll();
        $getPhoneType = $this->companyConnection->query("SELECT * FROM phone_type")->fetchAll();
        $getCarrierInfo = $this->companyConnection->query("SELECT * FROM carrier")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();

        $html = "";

        if(empty($getPhoneInfo))
        {
            $html .= '<div class="primary-owner-phone-row" id="primary-owner-phone-row-divId">
                           <div class="form-outer">
                               <div class="col-sm-3 col-md-3">
                                    <label>Phone Type</label>
                                    <select class="form-control" name="phone_type[]" id="phone_type">
                                        <option value="">Select</option>
                                        <option value="1">mobile</option>
                                        <option value="2">work</option>
                                        <option value="3">Fax</option>
                                        <option value="4">Home</option>
                                        <option value="5">Other</option>
                                    </select>
                               </div>
                               <div class="col-sm-3 col-md-3 work_extension_div" style="display: none;">
                                    <label>Office/Work Extension <em class="red-star">*</em></label>
                                    <input class="form-control phone_format" type="text" disabled id="work_phone_extension" name="work_phone_extension[]" placeholder="Eg: 154-175-4301">
                                    <span class="work_phone_extensionErr error red-star"></span>
                               </div>
                               <div class="col-sm-3 col-md-3 ">
                                    <label>Carrier <em class="red-star">*</em></label>
                                    <select class="form-control" name="carrier[]">
                                        <option value="">Select</option>';
            foreach($getCarrierInfo as $carrierInfo){
                $html .= "<option value=".$carrierInfo['id'].">".$carrierInfo['carrier']."</option>";
            }
            $html .= '</select>
                         </div>
                         <div class="col-sm-3 col-md-3 countycodediv">
                              <label>Country Code</label>
                              <select class="form-control" name="countryCode[]" id="country_code">
                              <option value="">Select</option>';
            foreach($countryCode as $code){
                $html .= "<option value=".$code['id']." data-id='".$code['code']."'>".$code['name']." (".$code['code'].")</option>";
            }
            $html .= '</select>
                     <span class="term_planErr error red-star"></span>
                     </div>
                     <div class="col-sm-3 col-md-3 ">
                          <label>Phone Number <em class="red-star">*</em></label>
                          <input class="form-control capsOn add-input phone_format" placeholder="Phone Number" type="text" name="phoneNumber[]">
                               <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                               <a class="add-icon" href="javascript:;" style="display:none"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                     </div>
                     <div class="clearfix"></div>
                     </div>
                     </div>';
        } else {
            $i = 0;
            foreach($getPhoneInfo as $phoneInfo)
            {
                $html .= '<div class="primary-owner-phone-row" id="primary-owner-phone-row-divId">
                            <div class="form-outer">
                                <div class="col-sm-3 col-md-3">
                                <label>Phone Type</label>
                                <select class="form-control" name="phone_type[]" id="phone_type">
                                <option value="">Select</option>';
                foreach($getPhoneType as $phoneType){
                    if($phoneType['id']==$phoneInfo['phone_type']) {
                        $selected = "selected=selected";
                    } else {
                        $selected = "";
                    }
                    $html .= "<option value=".$phoneType['id']." ".$selected.">".$phoneType['type']."</option>";
                }
                $html.= '</select>
                         </div>
                         <div class="col-sm-3 col-md-3 work_extension_div" style="display: none;">
                            <label>Office/Work Extension <em class="red-star">*</em></label>
                            <input class="form-control phone_format" type="text" disabled id="work_phone_extension" name="work_phone_extension[]" placeholder="Eg: 154-175-4301">
                            <span class="work_phone_extensionErr error red-star"></span>
                        </div>
                         <div class="col-sm-3">
                              <label>Carrier <em class="red-star">*</em></label>
                                  <select class="form-control" name="carrier[]">
                                  <option value="">Select</option>';
                foreach($getCarrierInfo as $carrierInfo) {
                    if($carrierInfo['id']==$phoneInfo['carrier']) {
//                                            print_r($phoneInfo['carrier']);
                        $html .= "           <option value=".$carrierInfo['id']." selected>".$carrierInfo['carrier']."</option>";
                    } else {
                        $html .= "           <option value=".$carrierInfo['id'].">".$carrierInfo['carrier']."</option>";
                    }
                }
                $html.=     '    </select>
                        </div>
                        <div class="col-sm-3 countycodediv">
                            <label>Country Code</label>
                                <select class="form-control" name="countryCode[]" id="country_code">
                                <option value="">Select</option>';
                foreach($countryCode as $code){
                    if($code['id']==$phoneInfo['country_code']){
                        $html .= "<option value=".$code['id']." data-id='".$code['code']."' selected>".$code['name']." (".$code['code'].")</option>";
                    } else{
                        $html .= "<option value=".$code['id']." data-id='".$code['code']."'>".$code['name']." (".$code['code'].")</option>";
                    }
                }
                $html.=         '</select></div>
                        <div class="col-sm-3">
                            <label>Phone Number</label>
                                <input class="form-control add-input phone_format" placeholder="Phone Number" maxlength="12" type="text" name="phoneNumber[]" value="'.$phoneInfo['phone_number'].'">';
                if($i==0){
                    $html .=  '<a class="add-icon add-icon-abs" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                    $html .='<a class="add-icon add-icon-abs" href="javascript:;" ><i class="fa fa-times-circle red-star" aria-hidden="true" style="display:none"></i></a>';
                }
                if($i!=0){
                    $html .='<a class="add-icon add-icon-abs" href="javascript:;" ><i class="fa fa-times-circle red-star" aria-hidden="true" ></i></a>';
                }
                $html.= '</div>
                         </div>
                         </div>';
                $i++;
            }
        }
        return $html;
    }


    public function ownerPortalLogin()
    {
        try {
            $owner_id = $_POST['id'];
            $sql_query2 = "SELECT * FROM users WHERE id=$owner_id";
            $data = $this->companyConnection->query($sql_query2)->fetch();

            if(!empty($data))
            {
                $status = 'success';
                $message = 'User fetched successfully.';
                $url = '';
                $user_id = $data['id'];
                $portal = 'Owner_Portal';

                if(!empty($user_id)){
                    $_SESSION[SESSION_DOMAIN][$portal]['portal_id'] = $user_id;
                    $_SESSION[SESSION_DOMAIN][$portal]['default_name'] = $data['first_name'].' '.$data['last_name'];
                    $_SESSION[SESSION_DOMAIN][$portal]['name'] = userName($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['company_name'] = $data['company_name'];
                    $_SESSION[SESSION_DOMAIN][$portal]['email'] = $data['email'];
                    $_SESSION[SESSION_DOMAIN][$portal]['phone_number'] = $data['phone_number'];
                    $_SESSION[SESSION_DOMAIN][$portal]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $data['id'], $this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['datepicker_format'] = getDatePickerFormat($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['company_logo'] = getCompanyLogo($this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['super_admin_name'] = isset($_POST['super_admin_name']) ? $_POST['super_admin_name'] : '';
                    //Default setting session data
                    $_SESSION[SESSION_DOMAIN][$portal]['default_notice_period'] = defaultNoticePeriod($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_rent'] = defaultRent($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_application_fee'] = defaultApplicationfee($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_zipcode'] = defaultZipCode($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_city'] = defaultCity($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_currency'] = defaultCurrency($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_currency_symbol'] = defaultCurrencySymbol($_SESSION[SESSION_DOMAIN][$portal]['default_currency'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['pagination'] = pagination($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['payment_method'] = defaultPaymentMethod($data['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['property_size'] = defaultPropertySize($data['id'],$this->companyConnection);
                }

                return array('code' => 200, 'status'=>$status,'message'=>$message,'portal_url'=>$url,'user_id'=>$user_id);
            }
            else
            {
                return array('status'=>'error','message'=>'Email/Password is incorrect');
            }
        } catch (PDOException $e) {
            dd($e);
            return array('code' => 400, 'status' => 'failed','message' => $e);
        }

    }

    public function VendorPaymentStep1($id,$card,$cvv,$year,$month)
    {

        try{
            $customer_id = $this->createCustomerNew($id);
            $request['customer_id']=$customer_id;
            $createdData=createCard($card,$cvv,$year,$month,$request);
            return $createdData;

        }catch(\Stripe\Error\RateLimit $e){

            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 'false', 'message' => $results['message'] ));
            exit;
        }
        catch(\Stripe\Error\Card $e){

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $results['status'] = $e->getHttpStatus();
            $results['type']   = $err['type'];
            $results['code']   = $err['code'];
            // param is '' in this case
            $results['param']  = $err['param'];
            $results['message'] = $err['message'];

            echo  json_encode(array('status' => 409, 'message' => $results['message'] ));
            exit;

        } catch (\Stripe\Error\RateLimit $e) {

            // Too many requests made to the API too quickly

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message'] = 'Too many requests made to the API too quickly';
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\InvalidRequest $e) {

            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  = "Invalid parameters were supplied to Stripe's API";
            echo  json_encode(array('status' => 409, 'message' => $err['message']));
            exit;

        } catch (\Stripe\Error\Authentication $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Network communication with Stripe failed";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (\Stripe\Error\Base $e) {

            $body = $e->getJsonBody();
            $err  = $body['error'];

            $results['message']  =  "Error";
            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;

        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $results['message']  = "Something else happened, completely unrelated to Stripe";

            $body = $e->getJsonBody();
            $err  = $body['error'];

            echo  json_encode(array('status' => 409, 'message' => $err['message'] ));
            exit;
        }
    }
    function createCustomerNew($user_id)
    {

        $getData =  $this->companyConnection->query("SELECT stripe_customer_id,email FROM users WHERE id ='" . $user_id . "'")->fetch();
        $email['email'] = $getData['email'];
        if($getData['stripe_customer_id']=='')
        {
            $customer = createCustomer($email);
            $customer_id = $customer['account_data'];
            $upateData =  "UPDATE users SET stripe_customer_id='$customer_id' where id='$user_id'";
            $stmt = $this->companyConnection->prepare($upateData);
            $stmt->execute();


            return $customer_id;
        }
        else
        {

            return $getData['stripe_customer_id'];
        }


    }
    public function VendorPaymentStep2($stripe_account_array,$id){
        try {

            $request['email']=$stripe_account_array['email'];
            $request['business_type']='individual';
            $data['line1'] = "address_full_match"; /*$fdata['faddress1'];*/
            $accountId=createAccount($request);
            if ($accountId["code"] == 200 && $accountId["status"] == "success") {
                $data['user_id'] = $id;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $data['country'] = $stripe_account_array['country'];
                $data['account_type'] = 'individual';
                $data['account_number'] = $stripe_account_array['account_number'];
                $data['routing_number'] = $stripe_account_array['routing_number'];
                $data['city'] = $stripe_account_array['city'];
                $data['line1'] =$stripe_account_array['line1'];
                $data['address_line2'] = 'address_full_match';
                $data['postal_code'] = $stripe_account_array['postal_code'];
                $data['state'] = $stripe_account_array['state'];
                $data['email'] = $stripe_account_array['email'];
                $data['first_name'] = $stripe_account_array['first_name'];
                $data['last_name'] = $stripe_account_array['last_name'];
                $data['phone'] = $stripe_account_array['phone'];
                $data['ssn_last'] = $stripe_account_array['ssn_last_4'];
                $dateofbirth= $stripe_account_array['year'] -  $stripe_account_array['month'] -  $stripe_account_array['day'];
                $data['dob'] = $dateofbirth;

                $stripe_account_array['stripe_account_id']=$accountId['account_id'];
                $getUserAccountData =  $this->companyConnection->query("SELECT * FROM user_account_detail WHERE user_id ='" . $data['user_id'] . "'")->fetch();
                if(empty($getUserAccountData)){
                    $enableAccount = enableAccount($stripe_account_array);
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO user_account_detail(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                    if (!empty($accountId['account_id'])) {
                        $upateData = "UPDATE users SET stripe_account_id= '" .$stripe_account_array['stripe_account_id'] . "' where id= " . $data['user_id'];
                        $stmt = $this->companyConnection->prepare($upateData);
                        $stmt->execute();

                    }
                    if($enableAccount["code"] == 400){
                        $account = \Stripe\Account::retrieve($accountId['account_id']);
                        $account->delete();
                    }
//                    dd($enableAccount);
                    return $enableAccount;
                }else{
                    $enableAccount = enableAccount($stripe_account_array);
                    $sqlData = createSqlUpdateCase($data);
                    $query = "UPDATE user_account_detail SET " . $sqlData['columnsValuesPair'] . " WHERE user_id=" . $data['user_id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
//                    dd($enableAccount);
                    return array('status' => 'AccountDetailupdated', 'enableAccount' => $enableAccount);
                }


            }else{
                dd($accountId);
                return $accountId;
            }



        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    function createVendorCompanyAccount($account_details,$user_id,$stripe_account_id)
    {

        $data["stripe_account_id"] = $stripe_account_id;
        $sqlData = createSqlColValPair($data);
        $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id='" . $user_id . "'";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        // $alldata['stripe_account_id'] = $createPmAccount['account_id'];
        $stripe_account_detail_array = ["user_id" => $user_id, "email" => $account_details["femail"], "first_name" => $account_details["ffirst_name"], "last_name" => $account_details["flast_name"], "city" => $account_details["fccity"], "state" => $account_details["fcstate"], "address_1" => $account_details["fcaddress"], "address_2" => $account_details["fcaddress2"], 'postal_code' => $account_details["fczipcode"], 'day' => $account_details["fday"], 'month' => $account_details["fmonth"], 'year' => $account_details["fyear"], 'ssn_last' => $account_details["fssn"], 'phone' => $account_details["fphone_number"], 'url' => $account_details["furl"], 'country' => 'US', 'routing_number' => $account_details["frouting_number"], 'account_number' => $account_details["faccount_number"], 'mcc' => $account_details["fmcc"], 'company_name' => $account_details["fcname"], 'company_address1' => $account_details["fcaddress"], 'company_address2' => $account_details["fcaddress2"], 'company_city' => $account_details["fccity"], 'company_postal_code' => $account_details["fczipcode"], 'company_state' => $account_details["fcstate"], 'tax_id' => $account_details["fctax_id"], 'business_type' => "company", 'company_phone_number' => $account_details["fcphone_number"], 'company_document_id' => $account_details["company_document_id"]];

        $save_account_detail = saveUserAccountDetail($this->companyConnection, $stripe_account_detail_array);
    }



    function createVendorIndividualAccount($account_details,$user_id,$stripe_account_id)
    {

        $data["stripe_account_id"] = $stripe_account_id;
        $sqlData = createSqlColValPair($data);
        $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id='" . $user_id . "'";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();

        $stripe_account_detail_array = ["user_id"=>$user_id,"email" => $account_details['email'], "first_name" => $account_details['first_name'], "last_name" => $account_details['last_name'], "city" => $account_details['city'], "state" => $account_details['state'], "address_1" => $account_details["line1"], "address_2" => $account_details["address_line2"], 'postal_code' => $account_details['postal_code'], 'day' => $account_details['day'], 'month' => $account_details['month'], 'year' => $account_details['year'], 'ssn_last' => $account_details['ssn_last'], 'phone' => $account_details["phone"], 'url' => $account_details['url'], 'country' => 'US', 'routing_number' => $account_details["routing_number"], 'account_number' => $account_details["account_number"], 'mcc' => !empty($account_details["mcc"])?$account_details["mcc"]:'','business_type'=>'individual','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')];


        $save_account_detail = saveUserAccountDetail($this->companyConnection, $stripe_account_detail_array);
    }





}
$ownerAjax = new OwnerAjax();