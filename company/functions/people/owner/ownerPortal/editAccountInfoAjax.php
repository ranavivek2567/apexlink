<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class editAccountInfoAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function viewForEditAccountInfo(){
        try{

            $owner_id = $_POST['id'];

            $data['user_data'] = $this->companyConnection->query("SELECT * FROM users where id=".$_REQUEST['id'])->fetch();

            $data['owner_billing_data'] = $this->companyConnection->query("SELECT * FROM owner_billing_info WHERE owner_billing_info.user_id=$owner_id")->fetch();
//            dd($data);
            $data['owner_detail'] = $this->companyConnection->query("SELECT * FROM owner_details WHERE owner_details.user_id=$owner_id")->fetch();

            $data['owner_detail']['email']= isset($data['owner_detail']['email']) ? unserialize($data['owner_detail']['email']) : '';

            $data['owner_detail']['contact_country_code_data']=(isset($data['owner_detail']['contact_country_code']) ? unserialize($data['owner_detail']['contact_country_code']) : '');
            $data['owner_detail']['contact_phone_number_data']=(isset($data['owner_detail']['contact_phone_number']) ? unserialize($data['owner_detail']['contact_phone_number']) : '');
            $data['owner_detail']['contact_phone_type_data']=(isset($data['owner_detail']['contact_phone_type']) ? unserialize($data['owner_detail']['contact_phone_type']) : '');

            $data['owner_detail']['emergency_contact_name'] = isset($data['owner_detail']['emergency_contact_name']) ? unserialize($data['owner_detail']['emergency_contact_name']): '';
            $data['owner_detail']['emergency_other_relation'] = isset($data['owner_detail']['emergency_other_relation']) ? unserialize($data['owner_detail']['emergency_other_relation']): '';
            $data['owner_detail']['emergency_email'] = isset($data['owner_detail']['emergency_email']) ? unserialize($data['owner_detail']['emergency_email']): '';
            $data['owner_detail']['emergency_phone_number'] = isset($data['owner_detail']['emergency_phone_number']) ? unserialize($data['owner_detail']['emergency_phone_number']) : '';
            if(isset($data['owner_detail']['emergency_relation']) && !empty($data['owner_detail']['emergency_relation'])) {
                $e_relation = unserialize($data['owner_detail']['emergency_relation']);
                $owner_emergency_relation = [];
                $data['owner_detail']['emergency_relation_new'] = ($data['owner_detail']['emergency_relation']) ? unserialize($data['owner_detail']['emergency_relation']):'';
                if (!empty($e_relation)){
                    foreach ($e_relation as $k => $v){
                        $emergency_rel = emergencyRelation($v);
                        if($emergency_rel == 'Other'){
                            $emergency_rel = $data['owner_detail']['emergency_other_relation'][$k];
                        }

                        array_push($owner_emergency_relation,$emergency_rel);
                    }
                    $data['owner_detail']["emergency_relation"] = $owner_emergency_relation;
                } else {
                    $data['owner_detail']['emergency_relation'] = '';
                }



            } else {
                $data['owner_detail']['emergency_relation'] = '';
            }
            $data['owner_detail']['edit_emergency_code'] = unserialize($data['owner_detail']['emergency_country_code']);
            if(isset($data['owner_detail']['emergency_country_code']) && !empty($data['owner_detail']['emergency_country_code'])) {
                $e_country_code = unserialize($data['owner_detail']['emergency_country_code']);
                $country_code_value = [];

                if (!empty($e_country_code)){
                    foreach ($e_country_code as $k => $v){
                        if($v)
                        {
                            $query = 'SELECT code FROM countries WHERE id="'.$v.'"';
                            $country_code = $this->companyConnection->query($query)->fetch();
                            array_push($country_code_value,$country_code['code']);
                        } else {
                            array_push($country_code_value,'');
                        }

                    }

                    $data['owner_detail']["emergency_country_code"] = $country_code_value;
                } else {
                    $data['owner_detail']['emergency_country_code'] = '';
                }
            } else {
                $data['owner_detail']['emergency_country_code'] = '';
            }

            $emergency_data = [];

            if ($data['owner_detail']['emergency_contact_name']) {
                $length = count($data['owner_detail']['emergency_contact_name']);
                for ($i = 0; $i < $length; $i++) {
                    $emergency_data[$i]['emergency_contact_name'] = (isset($data['owner_detail']['emergency_contact_name'][$i])?$data['owner_detail']['emergency_contact_name'][$i]:'');
                    $emergency_data[$i]['emergency_relation'] = (isset($data['owner_detail']['emergency_relation'][$i])?$data['owner_detail']['emergency_relation'][$i]:'');
                    $emergency_data[$i]['emergency_country_code'] = (isset($data['owner_detail']['emergency_country_code'][$i])?$data['owner_detail']['emergency_country_code'][$i]:'');
                    $emergency_data[$i]['emergency_phone_number'] = (isset($data['owner_detail']['emergency_phone_number'][$i])?$data['owner_detail']['emergency_phone_number'][$i]:'');
                    $emergency_data[$i]['emergency_email'] = (isset($data['owner_detail']['emergency_email'][$i])?$data['owner_detail']['emergency_email'][$i]:'');
                }
            }


            $tenant_phone_query= $this->companyConnection->query("SELECT * FROM tenant_phone WHERE tenant_phone.user_id=$owner_id")->fetchAll();
            $data['owner_detail']['contact_carrier_data']=$tenant_phone_query;

            $data['carrier'] = $this->companyConnection->query("SELECT id,carrier FROM carrier")->fetchAll();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();


            return ['status'=>'success','code'=>200,'data'=>$data, 'emergency_data'=>$emergency_data];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function updateOwnerBillingInfo(){
        try {
            $data = $_POST['form'];
            $owner_id = $_POST['id'];
            $billing_data = postArray($data);

            $billing_data["billing_info_as_contact_info"] = isset($billing_data['billing_info_as_contact_info']) && $billing_data['billing_info_as_contact_info'] == 'on' ? '1' : '0';
            $billing_data["updated_at"] = date('Y-m-d H:i:s');

            $sqlData = createSqlColValPair($billing_data);
            $query = "UPDATE owner_billing_info SET " . $sqlData['columnsValuesPair'] . " where user_id='$owner_id'";
//            dd($query);
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $e->getMessage()));
            return;
        }
    }

    public function changePassword(){
        try {
            $data=[];

//            if ($_POST['id'] == "") {
//                $data['id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
//            }else{
            $data['id'] = $_POST['id'];
//            }
            $data['npassword'] = $_POST['nPassword'];
            $data['cpassword'] = $_POST['cPassword'];
            $data['current'] = $_POST['currentPass'];
            $check = $this->checkCurrentPassword($data['id'],$data['current']);
            if($check['code'] == 400){
                return $check;
            }
            //Required variable array
            $required_array = ['password', 'new_password','confirm_new_password'];
            /* Max length variable array */
            $maxlength_array = ['password'=>25,'new_password'=>25,'confirm_new_password'=>25];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = $this->validationPassword($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $record['password'] = md5($data['npassword']);
                $record['actual_password'] = $data['npassword'];
                $id = $data['id'];
                $sqlData = createSqlColValPair($record);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password has been updated!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function checkCurrentPassword($id, $pass){
        try {
            $data=[];
//            $data['id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['password'] = $pass;

            if ($data['password'] == "") {
                return array('code' => 400, 'status' => 'error', 'data' => '', 'message' => 'Validation Errors!');
            } else {
                $record['password'] = md5($data['password']);
//                $id = $data['id'];

                $query = $this->companyConnection->query("SELECT password FROM users WHERE id='$id'");
                $user = $query->fetch();
                if ($user['password'] === $record['password']) {
                    return array('code' => 200, 'status' => 'success', 'data' => $user['password'], 'message' => 'Password matched!');
                }else{
                    return array('code' => 400, 'status' => 'error', 'message' => 'Current password do not match.');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    function validationPassword($data,$db,$required_array=[],$maxlength_array=[],$number_array=[]){
        $err_array = array();

        if(($data['npassword'] !== $data['cpassword'])){
            $error = 'Enter same password';
            $errName = 'NewPasswordErr';
            $err_array[$errName][] = $error;
        }

        foreach ($data as $key => $value) {
            $errName = $key . 'Err';
            $err_array[$errName] = [];
            if(in_array($key,$required_array)){
                if (empty($value)) {
                    $colName = ucfirst(str_replace('_',' ',$key));
                    $error = $colName." is required";
                    array_push($err_array[$errName],$error);
                }
            }

            if(array_key_exists($key,$maxlength_array)){
                $length = strlen($value);
                if ($key != 'npassword') {
                    continue;
                }
                if (!empty($value) && $length < 8) {
                    $error = 'Please enter minimum 8 characters';
                    array_push($err_array[$errName],$error);
                }
            }
            if(empty($err_array[$errName])){
                unset($err_array[$errName]);
            }
        }
        return $err_array;
    }

    public function update()
    {
        try{
            $postData = $_POST;
            $response = $this->updateOwnerUserDetails($postData,true);
            return $response;
        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function updateOwnerUserDetails($postData,$update=null){
        try{

            $owner_id = $postData['edit_id'];
            $sql_query1 = "SELECT * FROM users WHERE users.id=$owner_id";


            $owner_data =$this->companyConnection->query($sql_query1)->fetch();
            $domain = explode('.', $_SERVER['HTTP_HOST']);
            $subDomain = array_shift($domain);

            $owner_email = $postData['email'];

            $fieldArray = array(
                'first_name','last_name','maiden_name','salutation','nick_name','middle_name',
                'zipcode','city','state','country','address1','address2','address3','address4',
            );

            foreach ($fieldArray as $item) {
                $data[$item]    = $postData[$item];
            }

//            if($postData['company_name'] == '' || empty($postData['company_name'])){
//                $data['tax_payer_name'] = $postData['first_name'].' '.$postData['last_name'];
//            }
//            $data['if_entity_name_display'] = (isset($postData['if_entity_name_display']) && $postData['if_entity_name_display'] == 'on') ? '1': '0';
//
//
//            $data['company_name_as_tax_payer'] = (isset($postData['company_name_as_tax_payer']) && $postData['company_name_as_tax_payer'] == 'on') ? '1': '0';

            if(($owner_data['if_entity_name_display'] == '1') && isset($owner_data['company_name']) && !empty($owner_data['company_name'])){
                $data['name'] = $owner_data['company_name'];
            } else {
                if (!empty($data['nick_name']) && isset($data['nick_name'])) {
                    $data['name'] = $data['nick_name'] . ' ' . $data['last_name'];
                } else {
                    $data['name'] = $data['first_name'] . ' ' . $data['last_name'];
                }
            }
            $data['email']                  = $owner_email[0];
            $data['updated_at']             = date('Y-m-d H:i:s');
            $data['phone_type']             = (isset($postData['phone_type']))? $postData['phone_type'][0] : '';
            $data['carrier']                = (isset($postData['carrier']))? $postData['carrier'][0] : '';
            $data['phone_number']           = (isset($postData['phone_number']))? $postData['phone_number'][0] : '';
            $data['country_code']           = (isset($postData['country_code']))? $postData['country_code'][0] : '';

            $duplicate = checkNameAlreadyExists($this->companyConnection, 'users', 'email', $owner_email[0], $postData['edit_id']);

            if($duplicate['is_exists'] == 1){
                return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Email already exists!');
            }


            $sqlData = createSqlUpdateCase($data);
            $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." WHERE id=".$postData['edit_id'];
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);

            $last_id = $postData['edit_id'];

            if($last_id){
                $this->updateOwnerDetails($postData,$last_id);
                $this->updatePhoneNumberDetail($last_id);
            }

            return array('status' => 'success', 'code' => 200, 'message' => 'Record added successfully.', 'table' => 'users', 'owner_id' => $last_id);

        }catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function updateOwnerDetails($postData,$last_id){
        try{

            $owner_details['email'] =(isset($_POST['email']))? serialize($_POST['email']) : '';

            if(!empty($_POST['emergency_contact_name'][0]) || !empty($_POST['emergency_relation'][0]) || !empty($_POST['emergency_phone'][0]) || !empty($_POST['emergency_email'][0]) ) {
                $owner_details['emergency_contact_name'] = (isset($_POST['emergency_contact_name'])) ? serialize($_POST['emergency_contact_name']) : '';
                $owner_details['emergency_relation'] = (isset($_POST['emergency_relation'])) ? serialize($_POST['emergency_relation']) : '';
                $owner_details['emergency_other_relation'] = (isset($_POST['emergency_other_relation'])) ? serialize($_POST['emergency_other_relation']) : '';
                $owner_details['emergency_country_code'] = (isset($_POST['emergency_country'])) ? serialize($_POST['emergency_country']) : '';
                $owner_details['emergency_phone_number'] = (isset($_POST['emergency_phone'])) ? serialize($_POST['emergency_phone']) : '';
                $owner_details['emergency_email'] = (isset($_POST['emergency_email'])) ? serialize($_POST['emergency_email']) : '';
            } else {
                unset($_POST['emergency_contact_name']);
                unset($_POST['emergency_relation']);
                unset($_POST['emergency_other_relation']);
                unset($_POST['emergency_country_code']);
                unset($_POST['emergency_phone_number']);
                unset($_POST['emergency_email']);
            }

            $owner_details['updated_at']         = date('Y-m-d H:i:s');

            $sqlData = createSqlUpdateCase($owner_details);
            $query = "UPDATE owner_details SET ".$sqlData['columnsValuesPair']." WHERE user_id=".$last_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);

            return array('status' => 'success', 'message' => 'Record updated successfully.', 'table' => 'users', 'owner_id' => $last_id);
        }catch (Exception $exception)
        {
            print_r($exception->getMessage()); die;
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());

        }
    }

    public function updatePhoneNumberDetail($last_id){
        try {

            $query1 = "DELETE FROM tenant_phone WHERE user_id =".$last_id;
            $stmt1 =$this->companyConnection->prepare($query1);
            $stmt1->execute();


            $phoneType = $_POST['phone_type'];
            $i = 0;
            foreach ($phoneType as $phone) {
                $data['user_id'] = $last_id;
                $data['parent_id'] = 0;
                $data['phone_type'] = $phone;
                $data['carrier'] = @$_POST['carrier'][$i];
                $data['phone_number'] = @$_POST['phone_number'][$i];
                $data['country_code'] = @$_POST['country_code'][$i];
                $data['work_phone_extension'] = @$_POST['work_phone_extension'][$i];
                $data['user_type'] = 0; //0 value for main tenant

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;
            }
            return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_phone');
        } catch (Exception $exception)
        {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }
    public function updateOwnerImage(){
        try{
            $id = $_POST['id'];
            $image = $_POST['image'];

            $sql  = "UPDATE owner_details SET owner_image='".$image."' WHERE user_id=$id";
            $stmt = $this->companyConnection->prepare($sql);

            if($stmt->execute())
            {
                return array('code' => 200, 'status' => 'success', 'message' => 'Image updated successfully.');
            } else {
                return array('code' => 403, 'status' => 'error', 'message' => 'Image not Uploaded due to technical error.Please try Later.');
            }


        }catch (Exception $exception)
        {
            echo $exception->getMessage();
            printErrorLog($exception->getMessage()); die;
        }
    }
}
$editAccountInfoAjax = new editAccountInfoAjax();