<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class ownerBillsAjax extends DBConnection{

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * @return array
     */
    public function ownerPropertyList(){
        try{
            $id= $_POST['owner_id'];
            $query = $this->companyConnection->query("SELECT general_property.id, property_name from owner_property_owned JOIN general_property ON owner_property_owned.property_id = general_property.id WHERE owner_property_owned.user_id='$id'");
            $property = $query->fetchAll();
            $html = '<option value="">Select</option>';
            if(isset($property)) {
                foreach ($property as $item) {
                    $html .='<option value="'.$item['id'].'">'.$item['property_name'].'</option>';
                }
            }
            return array('code' => 200, 'status' => 'success','data' => $html);
        } catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }
}
$ownerBillsAjax = new ownerBillsAjax();