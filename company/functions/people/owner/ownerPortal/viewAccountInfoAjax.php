<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class viewAccountInfoAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function goToOwnerPortal(){
        try{

            $owner_id = $_POST['id'];

            $data['user_data'] = $this->companyConnection->query("SELECT * FROM users where id=".$_REQUEST['id'])->fetch();
            $data['user_data']['name'] = userName($data['user_data']['id'],$this->companyConnection);
            $data['owner_billing_data'] = $this->companyConnection->query("SELECT * FROM owner_billing_info WHERE owner_billing_info.user_id=$owner_id")->fetch();

            $data['owner_detail'] = $this->companyConnection->query("SELECT * FROM owner_details WHERE owner_details.user_id=$owner_id")->fetch();

            $data['owner_detail']['email']= isset($data['owner_detail']['email']) ? unserialize($data['owner_detail']['email']) : '';

            $data['owner_detail']['contact_country_code_data']=(isset($data['owner_detail']['contact_country_code']) ? unserialize($data['owner_detail']['contact_country_code']) : '');
            $data['owner_detail']['contact_phone_number_data']=(isset($data['owner_detail']['contact_phone_number']) ? unserialize($data['owner_detail']['contact_phone_number']) : '');
            $data['owner_detail']['contact_phone_type_data']=(isset($data['owner_detail']['contact_phone_type']) ? unserialize($data['owner_detail']['contact_phone_type']) : '');

            $data['owner_detail']['emergency_contact_name'] = isset($data['owner_detail']['emergency_contact_name']) ? unserialize($data['owner_detail']['emergency_contact_name']): '';
            $data['owner_detail']['emergency_other_relation'] = isset($data['owner_detail']['emergency_other_relation']) ? unserialize($data['owner_detail']['emergency_other_relation']): '';
            $data['owner_detail']['emergency_email'] = isset($data['owner_detail']['emergency_email']) ? unserialize($data['owner_detail']['emergency_email']): '';
            $data['owner_detail']['emergency_phone_number'] = isset($data['owner_detail']['emergency_phone_number']) ? unserialize($data['owner_detail']['emergency_phone_number']) : '';
            if(isset($data['owner_detail']['emergency_relation']) && !empty($data['owner_detail']['emergency_relation'])) {
                $e_relation = unserialize($data['owner_detail']['emergency_relation']);
                $owner_emergency_relation = [];
                $data['owner_detail']['emergency_relation_new'] = ($data['owner_detail']['emergency_relation']) ? unserialize($data['owner_detail']['emergency_relation']):'';
                if (!empty($e_relation)){
                    foreach ($e_relation as $k => $v){
                        $emergency_rel = emergencyRelation($v);
                        if($emergency_rel == 'Other'){
                            $emergency_rel = $data['owner_detail']['emergency_other_relation'][$k];
                        }

                        array_push($owner_emergency_relation,$emergency_rel);
                    }
                    $data['owner_detail']["emergency_relation"] = $owner_emergency_relation;
                } else {
                    $data['owner_detail']['emergency_relation'] = '';
                }



            } else {
                $data['owner_detail']['emergency_relation'] = '';
            }
            $data['owner_detail']['edit_emergency_code'] = unserialize($data['owner_detail']['emergency_country_code']);
            if(isset($data['owner_detail']['emergency_country_code']) && !empty($data['owner_detail']['emergency_country_code'])) {
                $e_country_code = unserialize($data['owner_detail']['emergency_country_code']);
                $country_code_value = [];

                if (!empty($e_country_code)){
                    foreach ($e_country_code as $k => $v){
                        if($v)
                        {
                            $query = 'SELECT code FROM countries WHERE id="'.$v.'"';
                            $country_code = $this->companyConnection->query($query)->fetch();
                            array_push($country_code_value,$country_code['code']);
                        } else {
                            array_push($country_code_value,'');
                        }

                    }

                    $data['owner_detail']["emergency_country_code"] = $country_code_value;
                } else {
                    $data['owner_detail']['emergency_country_code'] = '';
                }
            } else {
                $data['owner_detail']['emergency_country_code'] = '';
            }

            $emergency_data = [];

            if ($data['owner_detail']['emergency_contact_name']) {
                $length = count($data['owner_detail']['emergency_contact_name']);
                for ($i = 0; $i < $length; $i++) {
                    $emergency_data[$i]['emergency_contact_name'] = (isset($data['owner_detail']['emergency_contact_name'][$i])?$data['owner_detail']['emergency_contact_name'][$i]:'');
                    $emergency_data[$i]['emergency_relation'] = (isset($data['owner_detail']['emergency_relation'][$i])?$data['owner_detail']['emergency_relation'][$i]:'');
                    $emergency_data[$i]['emergency_country_code'] = (isset($data['owner_detail']['emergency_country_code'][$i])?$data['owner_detail']['emergency_country_code'][$i]:'');
                    $emergency_data[$i]['emergency_phone_number'] = (isset($data['owner_detail']['emergency_phone_number'][$i])?$data['owner_detail']['emergency_phone_number'][$i]:'');
                    $emergency_data[$i]['emergency_email'] = (isset($data['owner_detail']['emergency_email'][$i])?$data['owner_detail']['emergency_email'][$i]:'');
                }
            }
            return ['status'=>'success','code'=>200,'data'=>$data, 'emergency_data'=>$emergency_data];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function updateOwnerBillingInfo(){
        try {
            $data = $_POST['form'];
            $owner_id = $_POST['id'];
            $billing_data = postArray($data);
//            dd($billing_data);

            $billing_data["billing_info_as_contact_info"] = isset($billing_data['billing_info_as_contact_info']) && $billing_data['billing_info_as_contact_info'] == 'on' ? '1' : '0';
            $billing_data["updated_at"] = date('Y-m-d H:i:s');

            $sqlData = createSqlColValPair($billing_data);
            $query = "UPDATE owner_billing_info SET " . $sqlData['columnsValuesPair'] . " where user_id='$owner_id'";
//            dd($query);
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');

        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $e->getMessage()));
            return;
        }
    }

    public function changePassword(){
        try {
            $data=[];

            $data['id'] = $_POST['id'];
            $data['npassword'] = $_POST['nPassword'];
            $data['cpassword'] = $_POST['cPassword'];
            $data['current'] = $_POST['currentPass'];
            $check = $this->checkCurrentPassword($data['id'],$data['current']);
            if($check['code'] == 400){
                return $check;
            }
            //Required variable array
            $required_array = ['password', 'new_password','confirm_new_password'];
            /* Max length variable array */
            $maxlength_array = ['password'=>25,'new_password'=>25,'confirm_new_password'=>25];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = $this->validationPassword($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $record['password'] = md5($data['npassword']);
                $record['actual_password'] = $data['npassword'];
                $id = $data['id'];
                $sqlData = createSqlColValPair($record);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password has been updated!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function checkCurrentPassword($id, $pass){
        try {
            $data=[];
//            $data['id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['password'] = $pass;

            if ($data['password'] == "") {
                return array('code' => 400, 'status' => 'error', 'data' => '', 'message' => 'Validation Errors!');
            } else {
                $record['password'] = md5($data['password']);
//                $id = $data['id'];

                $query = $this->companyConnection->query("SELECT password FROM users WHERE id='$id'");
                $user = $query->fetch();
                if ($user['password'] === $record['password']) {
                    return array('code' => 200, 'status' => 'success', 'data' => $user['password'], 'message' => 'Password matched!');
                }else{
                    return array('code' => 400, 'status' => 'error', 'message' => 'Current password do not match.');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    function validationPassword($data,$db,$required_array=[],$maxlength_array=[],$number_array=[]){
        $err_array = array();

        if(($data['npassword'] !== $data['cpassword'])){
            $error = 'Enter same password';
            $errName = 'NewPasswordErr';
            $err_array[$errName][] = $error;
        }

        foreach ($data as $key => $value) {
            $errName = $key . 'Err';
            $err_array[$errName] = [];
            if(in_array($key,$required_array)){
                if (empty($value)) {
                    $colName = ucfirst(str_replace('_',' ',$key));
                    $error = $colName." is required";
                    array_push($err_array[$errName],$error);
                }
            }

            if(array_key_exists($key,$maxlength_array)){
                $length = strlen($value);
                if ($key != 'npassword') {
                    continue;
                }
                if (!empty($value) && $length < 8) {
                    $error = 'Please enter minimum 8 characters';
                    array_push($err_array[$errName],$error);
                }
            }
            if(empty($err_array[$errName])){
                unset($err_array[$errName]);
            }
        }
        return $err_array;
    }

    public function portalLogout(){
        try {
            $url = $_POST['data'];
            $condition = ['column'=>'id','value'=>$_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']];
//            dd($condition);
//        print_r($condition); die('aaaa');
            $user = getSingleRecord($this->companyConnection,$condition,'users');

            if(!empty($user)){
                $update_last_url= $this->companyConnection
                    ->prepare("UPDATE users SET last_user_url=? WHERE id=?")
                    ->execute([$url, $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']]);
            }

            unset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']);
            return array('status' => 'success', 'data' =>$update_last_url, 'message' => 'Owner Portal logout successfully');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }







}
$viewAccountInfoAjax = new viewAccountInfoAjax();