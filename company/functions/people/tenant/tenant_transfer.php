<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL."/config.php");

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");

class TenantTransferTypeAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    public function getTenant(){
        $tenant_id = $_POST['tenant_id'];
        $query = $this->companyConnection->query("SELECT unit_id FROM tenant_property where user_id=".$tenant_id);
        $unit_id = $query->fetch();
        $query = $this->companyConnection->query("SELECT unit_prefix,unit_no FROM unit_details where id=".$unit_id['unit_id']);
        $unit_det = $query->fetch();
        $query = $this->companyConnection->query("SELECT u.first_name,u.salutation,tld.move_in,u.ssn_sin_id,u.email,u.middle_name,u.last_name,tp.property_id,tld.rent_amount,tph.phone_type,tph.carrier,tld.security_deposite,tld.start_date,tph.phone_number,u.company_name FROM users u join tenant_property tp on u.id=tp.user_id  join tenant_lease_details tld on u.id=tld.user_id join tenant_phone tph on u.id=tph.user_id where u.id=".$tenant_id);
        $user = $query->fetch();
        $property_id= $user['property_id'];
        $query1 = $this->companyConnection->query("SELECT property_name from general_property WHERE id='$property_id'");
        $user1 = $query1->fetch();


        return ['code'=>200, 'status'=>'success', 'data'=>['all'=>$user,'property'=>$user1,'unit'=>$unit_det]];

    }
    public function getPortfolio(){
        $query1 = $this->companyConnection->query("SELECT portfolio_name,id from company_property_portfolio");
        $user1 = $query1->fetchAll();
        return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
    }
    public function getPropName(){
        $prop_name = $_POST['conceptName'];
        $query1 = $this->companyConnection->query("SELECT id from company_property_portfolio where portfolio_name='$prop_name'");
        $user1 = $query1->fetch();
        $portfolio_id=$user1['id'];
        $query = $this->companyConnection->query("SELECT property_name,id from general_property where portfolio_id=".$portfolio_id);
        $property = $query->fetchAll();
        return ['code'=>200, 'status'=>'success', 'data'=>['selected_data'=>$property]];
    }
    public function getBuilding(){
        $prop_id=$_POST['prop_id'];
        $query1 = $this->companyConnection->query("SELECT id,building_name from building_detail where property_id=".$prop_id);
        $user1 = $query1->fetchAll();
        return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
    }
    public function getUnit(){

        $query1 = $this->companyConnection->query("SELECT id,unit_type from company_unit_type");
        $user1 = $query1->fetchAll();
        return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
    }
    public function getAmenities(){

        $query1 = $this->companyConnection->query("SELECT id,name from company_property_amenities");
        $user1 = $query1->fetchAll();
        return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
    }
    public function getTable(){
        $port_id=$_POST['port_id'];
        $prop_id=$_POST['prop_id'];
        $build_id=$_POST['build_id'];
        $unit_id=$_POST['unit_id'];
        $unit_type=$_POST['unit_type'];
        $bedrooms=$_POST['bedrooms'];
        $bathroom=$_POST['bathroom'];
        $floor=$_POST['floor'];
        if($port_id!="0" && $prop_id=="0")
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,b.id AS bid,ud.id AS udid,ut.id AS utid,gp.property_name, b.building_name, ud.unit_prefix, ut.unit_type,ud.unit_no,ut.id,ut.status,ud.security_deposit,ud.base_rent,gp.portfolio_id,company_property_portfolio.id AS cppid FROM company_property_portfolio,general_property gp LEFT JOIN building_detail b ON b.property_id = gp.id LEFT JOIN unit_details ud ON ud.building_id = b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1' AND gp.portfolio_id=".$port_id);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id!="0" && $prop_id!="0" && $build_id=="0")
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,b.id AS bid,ud.id AS udid,ut.id AS utid,gp.property_name, b.building_name, ud.unit_prefix, ut.unit_type,ud.unit_no,ut.id,ut.status,ud.security_deposit,ud.base_rent,gp.portfolio_id,company_property_portfolio.id AS cppid FROM company_property_portfolio,general_property gp LEFT JOIN building_detail b ON b.property_id = gp.id LEFT JOIN unit_details ud ON ud.building_id = b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1' AND gp.portfolio_id=".$port_id." AND gp.id=".$prop_id );
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id!="0" && $prop_id!="0" && $build_id!="0" && $unit_id=="0")
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,b.id AS bid,ud.id AS udid,ut.id AS utid,gp.property_name, b.building_name, ud.unit_prefix, ut.unit_type,ud.unit_no,ut.id,ut.status,ud.security_deposit,ud.base_rent,gp.portfolio_id,company_property_portfolio.id AS cppid FROM company_property_portfolio,general_property gp LEFT JOIN building_detail b ON b.property_id = gp.id LEFT JOIN unit_details ud ON ud.building_id = b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1' AND gp.portfolio_id=".$port_id." AND gp.id=".$prop_id." AND b.id=".$build_id );
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id!="0" && $prop_id!="0" && $build_id!="0" && $unit_id!="0" && $unit_type=="0")
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,b.id AS bid,ud.id AS udid,ut.id AS utid,gp.property_name, b.building_name, ud.unit_prefix, ut.unit_type,ud.unit_no,ut.id,ut.status,ud.security_deposit,ud.base_rent,gp.portfolio_id,company_property_portfolio.id AS cppid FROM company_property_portfolio,general_property gp LEFT JOIN building_detail b ON b.property_id = gp.id LEFT JOIN unit_details ud ON ud.building_id = b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1' AND gp.portfolio_id=".$port_id." AND gp.id=".$prop_id." AND b.id=".$build_id." AND ut.id=".$unit_id);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id!="0" && $prop_id!="0" && $build_id!="0" && $unit_id!="0" && $unit_type!="0" && $bedrooms=='1')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,b.id AS bid,ud.id AS udid,ut.id AS utid,gp.property_name, b.building_name, ud.unit_prefix, ut.unit_type,ud.unit_no,ut.id,ut.status,ud.security_deposit,ud.base_rent,gp.portfolio_id,company_property_portfolio.id AS cppid FROM company_property_portfolio,general_property gp LEFT JOIN building_detail b ON b.property_id = gp.id LEFT JOIN unit_details ud ON ud.building_id = b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1' AND gp.portfolio_id=".$port_id." AND gp.id=".$prop_id." AND b.id=".$build_id." AND ut.id=".$unit_id." AND ud.unit_type_id=".$unit_type);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id!="0" && $prop_id!="0" && $build_id!="0" && $unit_id!="0" && $unit_type!="0" && $bedrooms!='1' && $bathroom=='1')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,b.id AS bid,ud.id AS udid,ut.id AS utid,gp.property_name, b.building_name, ud.unit_prefix, ut.unit_type,ud.unit_no,ut.id,ut.status,ud.security_deposit,ud.base_rent,gp.portfolio_id,company_property_portfolio.id AS cppid FROM company_property_portfolio,general_property gp LEFT JOIN building_detail b ON b.property_id = gp.id LEFT JOIN unit_details ud ON ud.building_id = b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1' AND gp.portfolio_id=".$port_id." AND gp.id=".$prop_id." AND b.id=".$build_id." AND ut.id=".$unit_id." AND ud.unit_type_id=".$unit_type." AND ud.bedrooms_no=".$bedrooms);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id!="0" && $prop_id!="0" && $build_id!="0" && $unit_id!="0" && $unit_type!="0" && $bedrooms!='1' && $bathroom!='1' && $floor=='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,b.id AS bid,ud.id AS udid,ut.id AS utid,gp.property_name, b.building_name, ud.unit_prefix, ut.unit_type,ud.unit_no,ut.id,ut.status,ud.security_deposit,ud.base_rent,gp.portfolio_id,company_property_portfolio.id AS cppid FROM company_property_portfolio,general_property gp LEFT JOIN building_detail b ON b.property_id = gp.id LEFT JOIN unit_details ud ON ud.building_id = b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1' AND gp.portfolio_id=".$port_id." AND gp.id=".$prop_id." AND b.id=".$build_id." AND ut.id=".$unit_id." AND ud.unit_type_id=".$unit_type." AND ud.bedrooms_no=".$bedrooms." AND ud.bathrooms_no='$bathroom'");
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id!="0" && $prop_id!="0" && $build_id!="0" && $unit_id!="0" && $unit_type!="0" && $bedrooms!='1' && $bathroom!='1' && $floor!='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,b.id AS bid,ud.id AS udid,ut.id AS utid,gp.property_name, b.building_name, ud.unit_prefix, ut.unit_type,ud.unit_no,ut.id,ut.status,ud.security_deposit,ud.base_rent,gp.portfolio_id,company_property_portfolio.id AS cppid FROM company_property_portfolio,general_property gp LEFT JOIN building_detail b ON b.property_id = gp.id LEFT JOIN unit_details ud ON ud.building_id = b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1' AND gp.portfolio_id=".$port_id." AND gp.id=".$prop_id." AND b.id=".$build_id." AND ut.id=".$unit_id." AND ud.unit_type_id=".$unit_type." AND ud.bedrooms_no=".$bedrooms." AND ud.bathrooms_no=".$bathroom." AND ud.floor_no=".$floor);

            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_id=="0" && $unit_type=="0" && $bedrooms=='1' && $bathroom=='1' && $floor!='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.floor_no=".$floor);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_id=="0" && $unit_type=="0" && $bedrooms=='1' && $bathroom!='1' && $floor=='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.bathrooms_no='$bathroom'");
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_id=="0" && $unit_type=="0" && $bedrooms!='1' && $bathroom=='1' && $floor=='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.bedrooms_no=".$bedrooms);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_type!="0" && $bedrooms=='1' && $bathroom=='1' && $floor=='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.unit_type_id=".$unit_type);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_type!="0" && $bedrooms!='1' && $bathroom=='1' && $floor=='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.unit_type_id=".$unit_type." AND ud.bedrooms_no=".$bedrooms);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_type!="0" && $bedrooms=='1' && $bathroom!='1' && $floor=='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.unit_type_id=".$unit_type." AND ud.bathrooms_no='$bathroom'");
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_type!="0" && $bedrooms=='1' && $bathroom=='1' && $floor!='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.unit_type_id=".$unit_type." AND ud.floor_no=".$floor);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_type=="0" && $bedrooms!='1' && $bathroom!='1' && $floor=='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.bedrooms_no=".$bedrooms." AND ud.bathrooms_no='$bathroom'");
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_type=="0" && $bedrooms!='1' && $bathroom=='1' && $floor!='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.bedrooms_no=".$bedrooms." AND ud.floor_no=".$floor);
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_type!="0" && $bedrooms!='1' && $bathroom!='1' && $floor=='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.bedrooms_no=".$bedrooms." AND ud.floor_no=".$unit_type." AND ud.bathrooms_no='$bathroom'");
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        elseif ($port_id=="0" && $unit_type=="0" && $bedrooms!='1' && $bathroom!='1' && $floor!='0')
        {
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1'  AND ud.bedrooms_no=".$bedrooms." AND ud.floor_no=".$floor." AND ud.bathrooms_no='$bathroom'");
            $user1 = $query1->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];
        }
        else{
            $query1 = $this->companyConnection->query("select gp.id AS gpid,gp.property_name,b.id AS bid, b.building_name,ud.id AS udid, ud.unit_prefix,ud.security_deposit,ud.base_rent,ud.building_unit_status,ut.id AS utid,ut.unit_type,ud.unit_no,ut.id,ut.status from general_property gp LEFT JOIN building_detail b ON b.property_id=gp.id LEFT JOIN unit_details ud ON ud.building_id=b.id LEFT JOIN company_unit_type ut ON ut.id=ud.unit_type_id WHERE ud.building_unit_status='1' ");
            $user1 = $query1->fetchAll();

        return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user1]];

    }

    }
    public function saveTenantTransfer(){
        try {
          $property_name=$_POST['property'];

            $data['new_property']=$_POST['property_id'];
            $data['new_building']=$_POST['building_id'];
            $data['new_unit']=$_POST['unit_id'];
            $data['status']='0';
            $data['secuirty_deposite']=$_POST['secuirty_deposite'];
            if($_POST['rent']== 'null')
            {
                $data['rental_approved']=null;
            }
            $data['user_id']=$_POST['tenant_id'];
            $tenant_id=$_POST['tenant_id'];
            $data['new_property_type']=$_POST['type'];
           /*$data['building']=$_POST['building'];*/
          /* $data['new_unit']=$_POST['unit'];*/
           /*$data['unit_type_id']=$_POST['unit_type_id'];*/
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $query5 = $this->companyConnection->query("SELECT user_id,status from tenant_transfer WHERE user_id=".$tenant_id);
                $user5 = $query5->fetch();
                $query1 = $this->companyConnection->query("SELECT id,state,city from general_property WHERE property_name='$property_name'");
                $user1 = $query1->fetch();

                $prop_name=$user1['id'];

                $current_state=$user1['state'];
                $current_city=$user1['city'];
                $query2 = $this->companyConnection->query("SELECT unit_no from unit_details WHERE user_id=".$tenant_id);
                $user2 = $query2->fetch();
                $current_unit=$user2['unit_no'];
                $query3 = $this->companyConnection->query("SELECT property_id,building_id,unit_id from tenant_property WHERE user_id=".$tenant_id);
                $user3 = $query3->fetch();
                $previous_building=$user3['building_id'];
                $previous_property=$user3['property_id'];
                $previous_unit=$user3['unit_id'];
                $query4 = $this->companyConnection->query("SELECT state,city from general_property WHERE id=".$previous_property);
                $user4 = $query4->fetch();
                $previous_state=$user4['state'];
                $previous_city=$user4['city'];
                if(!empty($user5['user_id']))
                {
                    if(($user5['status'])==2)
                    {
                        return array('code' => 503, 'status' => 'success','message' => 'Your request is already in progress!');
                    }
                    elseif(($user5['status'])==5)
                    {
                        return array('code' => 503, 'status' => 'success','message' => 'Your request has been declined!');
                    }
                    else {
                        $data1['new_property'] = $prop_name;
                        $data1['new_state'] = $current_state;
                        $data1['new_city'] = $current_city;
                        $data1['new_unit'] = $_POST['unit_id'];;
                        /* $data1['current_unit']= $previous_unit;
                          $data1['current_property']= $previous_property;
                          $data1['current_building']= $previous_building;
                          $data1['current_state']= $previous_state;
                          $data1['current_city']= $previous_city;*/
                        $data1['status'] = '1';
                        $sqlData = createSqlColValPair($data1);
                        $query9 = "UPDATE tenant_transfer SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $tenant_id;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();
                        return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'Records Updated successfully!');
                    }
                    }
                else {
                        $data['new_property'] = $prop_name;
                        $data['new_state'] = $current_state;
                        $data['new_city'] = $current_city;
                        $data['new_unit'] = $_POST['unit_id'];;
                        $data['current_unit'] = $previous_unit;
                        $data['current_property'] = $previous_property;
                        $data['current_building'] = $previous_building;
                        $data['current_state'] = $previous_state;
                        $data['current_city'] = $previous_city;
                        $data['status'] = '1';
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO tenant_transfer (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                        return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records inserted successfully!');
                    }

            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function changeStatus(){
        $tenant_id=$_POST['user_id'];
        $data['status']= '2';
        $sqlData = createSqlColValPair($data);
        $query9 = "UPDATE tenant_transfer SET ".$sqlData['columnsValuesPair']." where user_id=".$tenant_id ;
        $stmt = $this->companyConnection->prepare($query9);
        $stmt->execute();
        return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Records Updated successfully!');


    }
    public function tenant_lease_generate(){
        $tenant_id=$_POST['tenant_id'];

        $query1 = $this->companyConnection->query("SELECT * from tenant_transfer where user_id=".$tenant_id);
        $user1 = $query1->fetch();
        $prop_id=$user1['new_property'];
        $build_id=$user1['new_building'];
        $unit_id=$user1['new_unit'];
        $query2 = $this->companyConnection->query("SELECT unit_no,unit_prefix from unit_details where id=".$unit_id);
        $user2 = $query2->fetch();
        $query3 = $this->companyConnection->query("SELECT property_name from general_property where id=".$prop_id);
        $user3 = $query3->fetch();
        $query4 = $this->companyConnection->query("SELECT building_name from building_detail where id=".$build_id);
        $user4 = $query4->fetch();
        $query5 = $this->companyConnection->query("SELECT first_name,last_name,salutation,middle_name,phone_number,phone_type,carrier,email,ssn_sin_id from users where id=".$tenant_id);
        $user5 = $query5->fetch();
         $ssn=unserialize($user5['ssn_sin_id']);
          $query7 = $this->companyConnection->query("SELECT tenant_contact from tenant_details where user_id=".$tenant_id);
        $user7 = $query7->fetch();
        $user6 = $this->getPhoneInfo($tenant_id);

        return ['code'=>200, 'status'=>'success','data'=>['transfer'=>$user1,'unit'=>$user2,'property'=>$user3,'building'=>$user4,'user'=>$user5,'ids'=>$user6,'companyCheck'=>$user7,'ssn'=>$ssn[0]]];
    }
    public function transferTenantLease(){
        try {
           $data=$_POST['formData'];
            $data= postArray($data);
            $tenant_id=$_POST['tenant_id'];
            $id_tenFone=$_POST['id_tenFone'];



            $dataVal['start_date'] = $data['start_date1'];
            $dataVal['end_date'] = $data['end_date1'];

            //Required variable array
            $required_array = ['start_date','end_date'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($dataVal,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $days_remaining = $data['end_date1'];
                $daysRemaining = mySqlDateFormat($days_remaining, null, $this->companyConnection);
                $data1['days_remaining'] = $daysRemaining;
                $data1['balance'] = '1000';
                $data1['move_in'] = mySqlDateFormat($data['move_in_date'], null, $this->companyConnection);
                $data1['move_out'] = mySqlDateFormat($data['move_out_date1'], null, $this->companyConnection);
                $data1['start_date'] = mySqlDateFormat($data['start_date1'], null, $this->companyConnection);
                $data1['end_date'] = mySqlDateFormat($data['end_date1'], null, $this->companyConnection);
                $data1['notice_date'] = mySqlDateFormat($data['notice_date'], null, $this->companyConnection);
                $data1['term'] = $data['lease_term'];
                $data1['tenure'] = $data['lease_tenure'];
                $data1['notice_period'] = $data['notice_period'];
                $data1['rent_due_day'] = $data['transfer_rent_due_day'];
                $data1['rent_amount'] = $data['transfer_rent_amount'];
                $data1['cam_amount'] = $data['transfer_cam_amount'];
                $data1['security_deposite'] = $data['transfer_security_deposite'];
                $data1['next_rent_incr'] = $data['transfer_next_rent_increase'];
                $data1['flat_perc'] = $data['transfer_rent'];
                $data1['amount_incr'] = $data['transfer_amount'];
                $data1['record_status'] = '1';
                $data1['user_id']=$tenant_id;
                $data1['created_at'] =  date('Y-m-d H:i:s');
                $data1['updated_at'] =  date('Y-m-d H:i:s');

                $data2['record_status'] = '0';
                $sqlData = createSqlColValPair($data2);
                $query = "UPDATE tenant_lease_details SET ".$sqlData['columnsValuesPair']." where user_id=".$tenant_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO tenant_lease_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                   /* $data2['salutation'] = $data['transfer_salutation'];
                    $data2['first_name'] = $data['transfer_firstName'];
                    $data2['middle_name'] = $data['transfer_middleName'];
                    $data2['last_name'] = $data['transfer_lastName'];*/
                        if (is_array($data['phoneType'])){
                                $data2['phone_type'] = $data['phoneType'][0];
                        }else{
                            $data2['phone_type'] = $data['phoneType'];//In Array PHONETYPE
                        }
                        if(is_array($data['carrier']))
                        {
                                $data2['carrier'] = $data['carrier'][0];//In Array CARRIER
                        }
                        else{
                            $data2['carrier'] = $data['carrier'];//In Array CARRIER
                        }
                        if(is_array($data['phoneNumber'])) {
                            $data2['phone_number'] = $data['phoneNumber'][0];//In Array CARRIER
                        }
                        else{
                            $data2['phone_number'] = $data['phoneNumber'];//In Array CARRIER
                        }
                   /* $data2['next_rent_incr'] = $data['transfer_moveINN'];//Date*/
                  /*  $data2['ssn_sin_id'] = $data['transfer_ssn'];*/
                    /*$data2['next_rent_incr'] = $data['entity_comapny'];//isset*/
                    $data2['created_at'] =  date('Y-m-d H:i:s');
                    $data2['updated_at'] =  date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data2);
                    $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id=".$tenant_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();

                    if (is_array($data['phoneType'])){
                        $count=count($data['phoneType']);
                        for($i=0;$i<$count;$i++) {
                            $data4['phone_type'] = $data['phoneType'][$i];
                            $data4['carrier'] = $data['carrier'][$i];
                            $data4['country_code'] = $data['countryCode'][$i];
                            $data4['phone_number'] = $data['phoneNumber'][$i];//In Array CARRIER
                            $data4['created_at'] =  date('Y-m-d H:i:s');
                            $data4['updated_at'] =  date('Y-m-d H:i:s');
                            $data4['user_id']=$tenant_id;
                            if(!empty($data['hidden_id_phone'][$i])){
                                $sqlData = createSqlColValPair($data4);
                                $query = "UPDATE tenant_phone SET ".$sqlData['columnsValuesPair']." where id=".$data['hidden_id_phone'][$i];
                                $stmt = $this->companyConnection->prepare($query);
                                $stmt->execute();
                            }else{
                                $sqlData = createSqlColVal($data4);
                                $query = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")";
                                $stmt = $this->companyConnection->prepare($query);
                                $stmt->execute($data4);

                            }
                        }
                    }else{
                         $data4['phone_type'] = $data['phoneType'];//In Array PHONETYPE
                         $data4['carrier'] = $data['carrier'];
                        $data4['country_code'] = $data['countryCode'];
                         $data4['phone_number'] = $data['phoneNumber'];//In Array CARRIER
                        $data4['created_at'] =  date('Y-m-d H:i:s');
                        $data4['updated_at'] =  date('Y-m-d H:i:s');
                        $data4['user_id']=$tenant_id;

                          $sqlData = createSqlColValPair($data4);
                          $query = "UPDATE tenant_phone SET ".$sqlData['columnsValuesPair']." where id=".$id_tenFone;
                          $stmt = $this->companyConnection->prepare($query);
                          $stmt->execute();
                    }
                if(isset($data['email'])) {
                    if (is_array($data['email'])) {
                        $count = count($data['email']);
                            $num = 2;
                        for ($i = 0; $i < $count; $i++) {
                            $num = $i + $num;
                                $data3['email'.$num] = $data['email'][$i];
                                $sqlData = createSqlColValPair($data3);
                                $query = "UPDATE tenant_details SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $tenant_id;
                                $stmt = $this->companyConnection->prepare($query);
                                $stmt->execute();
                        }
                    } else {
                        $data3['email3'] = $data['email'];
                        $sqlData = createSqlColValPair($data3);
                        $query = "UPDATE tenant_details SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $tenant_id;
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                    }
                }
                $transfer['status']='3';
                $transfer['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($transfer);
                $query = "UPDATE tenant_transfer SET ".$sqlData['columnsValuesPair']." where user_id=".$tenant_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'message' => 'Records updated successfully!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function taxpssDet(){
        try {
            $tenant_id=$_POST['tenant_id'];
            $data=$_POST['formData'];
            $data= postArray($data);

            $data1['tax_type']=$data['tax_pass'];
            if($data['transfer_overall_amount']=="")
            {
                $data1['amount']='0';
            }
            else {
                $data1['amount'] = $data['transfer_overall_amount'];
            }
            $data1['frequency']=$data['transfer_frequency_in'];
            $data1['user_id']=$tenant_id;

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data1,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO tenant_taxpass (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);


                $notes=$_POST['notes'];
                $note['notes']=$notes;
                $note['user_id']=$tenant_id;
                $note['type']='1';
                $note['record_status']='1';
                $sqlData = createSqlColVal($note);
                $query = "INSERT INTO tenant_notes (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($note);

                return array('code' => 200, 'status' => 'success', 'message' => 'Records inserted successfully!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function addAdditionalInfo(){
        try {
            $tenant_id=$_POST['tenant_id'];
            $data=$_POST['formData'];
            $data= postArray($data);

            //Required variable array
            $required_array = ['additional_firstname','additional_lastname'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $add_data['salutation']=$data['additional_salutation'];
                $add_data['first_name']=$data['additional_firstname'];
                $add_data['mi']=$data['additional_middlename'];
                $add_data['last_name']=$data['additional_lastname'];
                $add_data['relationship']=$data['additional_relationship'];
                $add_data['tenant_status']=$data['additional_tenantStatus'];
                $add_data['user_id']=$tenant_id;
                if(isset($data['additional_financallyResponsible'])) {
                    $add_data['financial_responsible'] = $data['additional_financallyResponsible'];
                }
                $sqlData = createSqlColVal($add_data);
                $query = "INSERT INTO tenant_additional_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($add_data);
                $last_id=$this->companyConnection->lastInsertId();

                if (is_array($data['additional_ssn'])){
                    $count=count($data['additional_ssn']);
                    for($i=0;$i<$count;$i++) {
                        $add_data1['ssn']=$data['additional_ssn'][$i];
                        $add_data1['user_id']=$last_id;
                        $add_data1['parent_id']=$tenant_id;
                        $sqlData = createSqlColVal($add_data1);
                        $query = "INSERT INTO tenant_ssn_id (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($add_data1);
                    }
                }
                else{
                    $add_data1['ssn']=$data['additional_ssn'];
                    $add_data1['user_id']=$last_id;
                    $add_data1['parent_id']=$tenant_id;
                    $sqlData = createSqlColVal($add_data1);
                    $query = "INSERT INTO tenant_ssn_id (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($add_data1);
                }

                if (is_array($data['additional_phoneType'])){
                    $count=count($data['additional_phoneType']);
                    for($i=0;$i<$count;$i++) {
                        $add_data2['phone_type'] = $data['additional_phoneType'][$i];
                        $add_data2['carrier'] = $data['additional_carrier'][$i];
                        $add_data2['country_code'] = $data['additional_countryCode'][$i];
                        $add_data2['phone_number'] = $data['additional_phone'][$i];
                        $add_data2['created_at'] =  date('Y-m-d H:i:s');
                        $add_data2['updated_at'] =  date('Y-m-d H:i:s');
                        $add_data2['user_id']=$last_id;
                        $add_data2['parent_id']=$tenant_id;
                        $add_data2['user_type']='1';
                        $sqlData = createSqlColVal($add_data2);
                        $query = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($add_data2);
                    }
                }else{
                    $add_data2['phone_type'] = $data['additional_phoneType'];
                    $add_data2['carrier'] = $data['additional_carrier'];
                    $add_data2['country_code'] = $data['additional_countryCode'];
                    $add_data2['phone_number'] = $data['additional_phone'];
                    $add_data2['created_at'] =  date('Y-m-d H:i:s');
                    $add_data2['updated_at'] =  date('Y-m-d H:i:s');
                    $add_data2['user_id']=$last_id;
                    $add_data2['parent_id']=$tenant_id;
                    $add_data2['user_type']='1';
                    $sqlData = createSqlColVal($add_data2);
                    $query = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($add_data2);
                }
                if (is_array($data['additional_email'])) {
                    $count = count($data['additional_email']);
                    for ($i = 0; $i < $count; $i++) {
                        $next=$i+1;
                        $add_data3['email'.$next] = $data['additional_email'][$i];
                        $sqlData = createSqlColValPair($add_data3);
                        $query = "UPDATE tenant_additional_details SET " . $sqlData['columnsValuesPair'] . " where id=" . $last_id;
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();

                    }
                }
                else{
                    $add_data3['email1'] = $data['additional_email'];
                    $sqlData = createSqlColValPair($add_data3);
                    $query = "UPDATE tenant_additional_details SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $tenant_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();

                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Records inserted successfully!');

            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function getPhoneInfo($tenant_id)
    {
        $getPhoneInfo = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE parent_id=0 and user_id ='".$tenant_id."'")->fetchAll();
        $getPhoneType = $this->companyConnection->query("SELECT * FROM phone_type")->fetchAll();
        $getCarrierInfo = $this->companyConnection->query("SELECT * FROM carrier")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();

        $html = "";

        if(empty($getPhoneInfo))
        {
            $html .= '<div class="row primary-tenant-phone-row2">

                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-2 ">
                                                    
                                                        <label>Phone Type</label>
                                                        <select class="form-control" name="phoneType[]">
                                                            <option value="">Select</option>
                                                            <option value="1">mobile</option>
                                                            <option value="2">work</option>
                                                            <option value="3">Fax</option>
                                                            <option value="4">Home</option>
                                                            <option value="5">Other</option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-2 ">
                                                        <label>Carrier <em class="red-star">*</em></label>
                                                        <select class="form-control" name="carrier[]">';
            foreach($getCarrierInfo as $carrierInfo){
                $html .= "<option value=".$carrierInfo['id'].">".$carrierInfo['carrier']."</option>";
            }
            $html .= '</select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-2 countycodediv">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="countryCode[]">';
            foreach($countryCode as $code){
                $html .= "<option value=".$code['id']." data-id='".$code['code']."'>".$code['name']."</option>";
            }
            $html .=     '</select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-2 ">
                                                        <label>Phone Number <em class="red-star">*</em></label>
                                                        <input class="form-control capsOn add-input phone_format" type="text"
                                                               name="phoneNumber[]" maxlenght="12">
                                                        <a class="add-icon" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <a class="add-icon" href="javascript:;" style="display:none"><i
                                                                    class="fa fa-minus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>';
        }
        else
        {


            $i = 0;
            foreach($getPhoneInfo as $phoneInfo)
            {

                $html .= '<div class="row primary-tenant-phone-row2">
                                  <div class="col-sm-2">
                                      <label>Phone Type</label>
                                     <select class="form-control" name="phoneType[]">';

                foreach($getPhoneType as $phoneType){

                    if($phoneType['id']==$phoneInfo['phone_type'])
                    {
                        $selected = "selected=selected";
                    }
                    else
                    {
                        $selected = "";
                    }



                    $html .= "<option value=".$phoneType['id']." ".$selected.">".$phoneType['type']."</option>";
                }

                $html.= '</select>
                                     </div>
                                      <div class="col-sm-2">
                                      <label>Carrier <em class="red-star">*</em></label>
                                      <select class="form-control" name="carrier[]">
                                      ';
                foreach($getCarrierInfo as $carrierInfo)
                {
                    if($carrierInfo['id']==$phoneInfo['carrier'])
                    {
                        $selected = "selected=selected";
                    }
                    else
                    {
                        $selected = "";
                    }

                    $html .= "<option value=".$carrierInfo['id']." ".$selected.">".$carrierInfo['carrier']."</option>";
                }

                $html.=     '</select></div>



                                  <div class="col-sm-2">
                                      <label>Country Code</label>
                                         <select class="form-control" name="countryCode[]">';
                foreach($countryCode as $code){
                    if($code['id']==$phoneInfo['country_code']){
                        $html .= "<option value=".$code['id']." data-id='".$code['code']."' selected>".$code['name']."</option>";
                    }else{
                        $html .= "<option value=".$code['id']." data-id='".$code['code']."'>".$code['name']."</option>";
                    }
                }

                $html.=     '</select></div>

                                     
                                  
                                  <div class="col-sm-2">
                                      <label>Phone Number</label>
                                      
                                      <input class="form-control add-input phone_format" maxlength="12" type="text" name="phoneNumber[]" value="'.$phoneInfo['phone_number'].'">
                                      <input type="hidden" name="hidden_id_phone[]" class="id_tenFone" value="'.$phoneInfo['id'].'">';

                if($i==0){
                    $html .=  '<a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                    $html .='<a class="add-icon" href="javascript:;" style="display:none"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>';
                }


                if($i!=0){

                    $html .='<a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>';
                }

                $html.= '</div>
                              </div>';

                $i++;
            }
        }
        return $html;
    }
    public function gettenantTransfer(){

        $tenant_id=$_POST['user_id'];
        $query1 = $this->companyConnection->query("SELECT id,move_out,security_deposite from tenant_lease_details WHERE record_status='0' AND user_id=".$tenant_id);
        $user1 = $query1->fetch();
        $user2=dateFormatUser($user1['move_out'],null,$this->companyConnection);
        return ['code'=>200, 'status'=>'success', 'data'=>['user'=>$user2,'sec_dep'=>$user1,'tenant_id'=>$tenant_id]];
    }
    public function save_actual_move_out(){
        try {

            $data=$_POST['formData'];
           // print_r($data);die;
            $data= postArray($data);
            $data1['actual_move_out']= mySqlDateFormat($data['actual_transfer_date'], null, $this->companyConnection);
            $data1['created_at'] =  date('Y-m-d H:i:s');
            $data1['updated_at'] =  date('Y-m-d H:i:s');

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data1,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $sqlData = createSqlColValPair($data1);
                $query = "UPDATE tenant_lease_details SET " . $sqlData['columnsValuesPair'] ."  where record_status='0' AND user_id=" . $data['hidden_ten_tras_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $data2['status']='4';
                $data2['created_at'] =  date('Y-m-d H:i:s');
                $data2['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data2);
                $query = "UPDATE tenant_transfer SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $data['hidden_ten_tras_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $query1 = $this->companyConnection->query("SELECT new_unit,current_unit,new_property,new_building from tenant_transfer WHERE id=".$data['hidden_transfer_id']);
                $transfer = $query1->fetch();

                $dataNew['building_unit_status']='1';
                $sqlData = createSqlColValPair($dataNew);
                $query = "UPDATE unit_details SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $transfer['current_unit'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();


                $dataNew1['building_unit_status']='4';
                $sqlData = createSqlColValPair($dataNew1);
                $query = "UPDATE unit_details SET " . $sqlData['columnsValuesPair'] . " where id=" . $transfer['new_unit'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $dataNew2['unit_id']=$transfer['new_unit'];
                $dataNew2['property_id']=$transfer['new_property'];
                $dataNew2['building_id']=$transfer['new_building'];
                $sqlData = createSqlColValPair($dataNew2);
                $query = "UPDATE tenant_property SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $data['hidden_ten_tras_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'message' => 'Tenant transferred successfully!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function declineTenant(){
        $tenant_id=$_POST['user_id'];
        $data1['status']="5";
        $sqlData = createSqlColValPair($data1);
        $query = "UPDATE tenant_transfer SET " . $sqlData['columnsValuesPair'] ."  where user_id=" . $tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array('code' => 200, 'status' => 'success', 'message' => 'Tenant transfer declined successfully!');

    }
    public function deleteTenant(){
        $tenant_id=$_POST['user_id'];
        $data1['deleted_at']=date('Y-m-d H:i:s');
        $sqlData = createSqlColValPair($data1);
        $query = "UPDATE tenant_transfer SET " . $sqlData['columnsValuesPair'] ."  where user_id=" . $tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array('code' => 200, 'status' => 'success', 'message' => 'The record deleted successfully!');

    }

}
$TenantTransferTypeAjax = new TenantTransferTypeAjax();
