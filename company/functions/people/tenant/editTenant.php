<?php

/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");

class EditTenant extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Insert data to company amenities table
     * @return array
     */

    public function getTenantInformation()
    {
        $tenant_id = $_REQUEST['tenant_id'];
        $propertyData = $this->propertyData($tenant_id);
        $property_id = $propertyData['property_id'];
        $building_id = $propertyData['building_id'];
        $unit_id = $propertyData['unit_id'];


        $propertyInfo = $this->getPropertyInfo($property_id);
        $data['gn_property_name'] = $propertyInfo['property_name'];/*gn is prefix for general tab*/
        $data['gn_address1'] = $propertyInfo['address1'];
        $data['gn_address2'] = $propertyInfo['address2'];
        $data['gn_address3'] = $propertyInfo['address3'];
        $data['gn_address4'] = $propertyInfo['address4'];
        $data['gn_city'] = $propertyInfo['city'] . ", " . $propertyInfo['state'] . " " . $propertyInfo['zipcode'];
        $buildingInfo = $this->getBuildingInfo($building_id);
        $data['gn_building_name'] = $buildingInfo['building_name'];


        $tenantInfo = $this->getTenantInfo($tenant_id);


        $data['gn_tenant_name'] = ucwords($tenantInfo['name']);
        $data['gn_first_name'] = $tenantInfo['first_name'];
        $data['gn_last_name'] = $tenantInfo['last_name'];
        $data['gn_tenant_phone'] = $this->getPhoneNumber($tenant_id);
        $data['gn_email1'] = $tenantInfo['email1'];
        $data['gn_email2'] = $tenantInfo['email2'];
        $data['gn_email3'] = $tenantInfo['email3'];

        $data['gn_tenant_image'] = $tenantInfo['tenant_image'];
        $data['veteran_id'] = $tenantInfo['veteran_status'];
        $data['gn_veteran_name'] = $tenantInfo['veteran_name'];
        $data['gn_marital_status'] = $tenantInfo['marital_status'];
        $data['ethnicity_id'] = $tenantInfo['ethnicity'];
        $data['gn_ethnicity'] = $tenantInfo['ethnicity_name'];
        $data['gn_tenant_contact'] = $tenantInfo['tenant_contact'];
        $data['gn_salutation'] = $tenantInfo['salutation'];

        $data['gn_nick_name'] = $tenantInfo['nick_name'];
        $data['gn_maiden_name'] = $tenantInfo['maiden_name'];
        $data['gn_middle_name'] = $tenantInfo['middle_name'];
        $data['tenant_dob'] = dateFormatUser($tenantInfo['dob'], null, $this->companyConnection);
        $data['movein_key_signed'] = $tenantInfo['movein_key_signed'];
        $data['gn_status'] = $this->getTenantStatusHtml($tenantInfo['status']);
        $data['gn_tenant_status'] = $this->getTenantStatus($tenantInfo['status']);
        $data['gn_updated'] = $this->getTenantStatus($tenantInfo['status']);

        $data['tenantAdditionalInfo'] = $this->getTenantAdditionalInfo($tenant_id);
        $data['gn_tenant_updated'] = $tenantInfo['updated_at'];

        if ($tenantInfo['phone_number_note'] == "") {
            $data['phone_number_note'] = 'N/A';
        } else {
            $data['phone_number_note'] = $tenantInfo['phone_number_note'];
        }


        if ($tenantInfo['gender'] == 1) {
            $data['gender'] = 'Male';
        } else {
            $data['gender'] = 'Female';
        }

        $data['vehicle'] = $tenantInfo['vehicle'];

        $getLicenseInfo = $this->license($tenant_id);
        $data['license'] = $getLicenseInfo['license'];
        $data['license_state'] = $getLicenseInfo['license_state'];


        if ($tenantInfo['vehicle'] == "0") {
            $data['vehicle'] = "No";
        } else {
            $data['vehicle'] = "Yes";
        }


        if ($tenantInfo['smoker'] == "0") {
            $data['smoker'] = "No";
        } else {
            $data['smoker'] = "Yes";
        }


        if ($tenantInfo['pet'] == "0") {
            $data['pet'] = "No";
        } else {
            $data['pet'] = "Yes";
        }


        if ($tenantInfo['medical_allergy'] == "0") {
            $data['medical_allergy'] = "No";
        } else {
            $data['medical_allergy'] = "Yes";
        }


        $data['gn_phone_number_note'] = $tenantInfo['phone_number_note'];
        if (!empty($tenantInfo['hobbies'])) {
            $datahobbies = unserialize($tenantInfo['hobbies']);
            $data['gn_hobbies'] = $this->getHobbiesName($datahobbies);

        } else {
            $data['gn_hobbies'] = 'N/A';
        }
        $data['generalEmail'] = $this->getGeneralEmails($tenantInfo['email1'], $tenantInfo['email2'], $tenantInfo['email3']);


        $unitInfo = $this->getUnitInfo($unit_id);

        $data['gn_unit_name'] = $unitInfo['unit_prefix'] . "-" . $unitInfo['unit_no'];


        $data['phoneInfo'] = $this->getPhoneInfo($tenant_id);

        $data['emergencyInfo'] = $this->getEmergencyInfo($tenant_id);
        $data['viewEmergencyInfo'] = $this->getViewEmergencyInfo($tenant_id);

        $data['viewCredentialsInfo'] = $this->getviewCredentialsInfo($tenant_id);


        $data['credentialInfo'] = $this->getCredentialInfo($tenant_id);
        //  $data['additionalInfo'] = $this->getAdditionalInfo($tenant_id);


        $ssn_id = unserialize($tenantInfo['ssn_sin_id']);
        if (isset($ssn_id[0]) && $ssn_id[0] == "") {
            $data['gn_ssn'] = "N/A";
        } else {
            if(is_array($ssn_id))
            {
                $implodeSnn = implode(',',$ssn_id);

            }
            else
            {
                $implodeSnn = $ssn_id;
            }
            $data['gn_ssn'] = $implodeSnn;
        }

        $data['gn_ssn_data'] = $this->getSsnData($ssn_id);


        //return $ssn_id;


        $leaseInfo = $this->leaseInfo($tenant_id);
        $startDate = $leaseInfo['start_date'];
        $endDate = $leaseInfo['end_date'];
        $moveInDate = $leaseInfo['move_in'];
        $moveOutDate = $leaseInfo['move_out'];
        $termlease = $leaseInfo['term'];
        $tenures = $leaseInfo['tenure'];
        if($leaseInfo['notice_period']=='null')
        {
                $data['gn_noticePeriod'] = '-';
        }
        else
        {
                    $data['gn_noticePeriod'] = $leaseInfo['notice_period'] . ' days';
        }
        //dd($leaseInfo);


        $data['gn_moveInDate'] = dateFormatUser($moveInDate, null, $this->companyConnection);
        $data['gn_moveOutDate'] = dateFormatUser($moveOutDate, null, $this->companyConnection);
        $data['gn_lease_start'] = dateFormatUser($startDate, null, $this->companyConnection);
        $data['gn_lease_end'] = dateFormatUser($endDate, null, $this->companyConnection);
        if ($tenures == "1") {
            $data['gn_term'] = $termlease . ' Months';
        } else {
            $data['gn_term'] = $termlease . ' Years';
        }
        $data['gn_security_deposite'] = $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . "" . $leaseInfo['security_deposite'];
        $data['gn_rent'] = $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . "" . $leaseInfo['rent_amount'];
        $data['gn_nsf'] = $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . '1000';
        $data['gn_balancedue'] = $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . '500';


        $tenentCharges = $this->getTenantCharge($tenant_id);
        if (!empty($tenentCharges)) {
            $charge_type = $tenentCharges['charge_type'];
            $apply_type = $tenentCharges['apply_type'];
            $fee = $tenentCharges['fee'];
            $grace_period = $tenentCharges['grace_period'];

            if ($apply_type == "0") {
                if ($charge_type == "1") {
                    $data['onetimeFlatFee'] = $fee;
                } else {
                    $data['onetimeMonthlyRent'] = $fee;
                }
            } else if ($apply_type == "1") {
                if ($charge_type == "1") {
                    $data['dailyFlatFee'] = $fee;
                } else {
                    $data['dailyMonthlyRent'] = $fee;
                }

            }
        }

        $taxDetails = $this->getTaxDetails($tenant_id);
        if (!empty($taxDetails)) {
            $data['tax_name'] = $taxDetails['tax_name'];
            $data['tax_value'] = $taxDetails['tax_value'];

        }


        return $data;


    }

    public function propertyData($tenant_id)
    {
        return $this->companyConnection->query("SELECT * FROM tenant_property WHERE user_id ='" . $tenant_id . "'")->fetch();

    }

    public function getPropertyInfo($property_id)
    {
        return $this->companyConnection->query("SELECT property_name,address1,address2,address3,address4,city,state,country,zipcode FROM general_property WHERE id ='" . $property_id . "'")->fetch();

    }

    public function getBuildingInfo($building_id)
    {
        return $this->companyConnection->query("SELECT building_name FROM building_detail WHERE id ='" . $building_id . "'")->fetch();

    }

    public function getTenantInfo($tenant_id)
    {
        return $this->companyConnection->query("SELECT u.*,td.tenant_contact,td.tenant_image,td.email1,td.email2,td.email3,td.vehicle,td.pet,td.animal,td.medical_allergy,td.guarantor,td.collection,td.smoker,td.movein_key_signed,td.status,td.tenant_license_state,td.tenant_license_number,(SELECT tvs.veteran from tenant_veteran_status as tvs where tvs.id=u.veteran_status) as veteran_name,(select tms.marital from tenant_marital_status as tms where tms.id=u.maritial_status) as marital_status,(select te.title from tenant_ethnicity as te where te.id=u.ethnicity) as ethnicity_name FROM users as u inner join tenant_details as td on u.id=td.user_id  WHERE u.id ='" . $tenant_id . "'")->fetch();

    }

    public function getPhoneNumber($id)
    {
        $getPhone = $this->companyConnection->query("SELECT phone_number FROM tenant_phone WHERE parent_id=0 and user_id ='" . $id . "'")->fetch();
        if (empty($getPhone)) {
            return '';
        } else {
            return $getPhone['phone_number'];
        }
    }

    public function getTenantStatusHtml($status)
    {
        if ($status == 0) {
            return "Inactive";
        } else if ($status == 1) {
            return "Active";
        } else if ($status == 2) {

            return "<span class='collection' style=color:red;>Evicting</span>";
        } else if ($status == 3) {
            return "<span class='collection' style=color:red;>In-Collection</span>";
        } else if ($status == 4) {

            return "<span class='collection' style=color:red;>Bankruptcy</span>";
        } else if ($status == 5) {
            return "<span class='collection' style=color:red;>Evicted</span>";
        }
    }

    public function getTenantStatus($status)
    {
        $html = '';
        $array = array('0' => "Inactive", '1' => "Active", '2' => "Evicting", '3' => "In-Collection", '4' => "Bankruptcy", '5' => "Evicted");
        $html = "<select name='tenant_status' class='tenant_status' style='display:none;'>";
        foreach ($array as $key => $value) {
            if ($key == $status) {
                $html .= "<option value='" . $key . "' selected>" . $value . "</option>";
            } else {
                $html .= "<option value='" . $key . "'>" . $value . "</option>";
            }
        }
        $html .= "</select>";
        return $html;


    }

    public function getTenantAdditionalInfo($tenant_id)
    {

        $html = "";
        $tenantInfo = $this->getTenantInfo($tenant_id);

        $birth = dateFormatUser($tenantInfo['dob'], null, $this->companyConnection);
        $gender = $tenantInfo['gender'];
        $vehicle = $tenantInfo['vehicle'];
        $smocker = $tenantInfo['smoker'];
        $pet = $tenantInfo['pet'];
        $medical = $tenantInfo['medical_allergy'];
        $keys = $tenantInfo['movein_key_signed'];
        $license_state = $tenantInfo['tenant_license_state'];
        $licenseNumber = $tenantInfo['tenant_license_number'];

        $html .= '<form id="addAdditionalInfoTenant"><div class="detail-outer detail-outer-loop">
                      <div class="row">
                          <div class="col-sm-6">
                              <div class="col-xs-12">
                                
                                  <label class="text-right">Birth Date : </label>
                                  <span>
                                  <input class="form-control additionalBirth calander" type="text" name="birth" value="' . $birth . '" Placeholder="Birth"> </span>
                                  
                              </div>
                              <div class="col-xs-12">
                                  <label class="text-right">Gender :</label>
                                  <span>
                                  <select name="gender" class="form-control additionalGender">';
        if ($gender == "") {
            $html .= '<option value="">Select</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                    <option value="3">Prefer Not To Say</option>
                                    <option value="4">Other</option>';
        } elseif ($gender == "1") {
            $html .= '<option value="">Select</option>
                                    <option value="1" selected>Male</option>
                                    <option value="2">Female</option>
                                    <option value="3">Prefer Not To Say</option>
                                    <option value="4">Other</option>';
        } elseif ($gender == "2") {
            $html .= '<option value="">Select</option>
                                    <option value="1">Male</option>
                                    <option value="2" selected>Female</option>
                                    <option value="3">Prefer Not To Say</option>
                                    <option value="4">Other</option>';
        } elseif ($gender == "3") {
            $html .= '<option value="">Select</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                    <option value="3" selected>Prefer Not To Say</option>
                                    <option value="4">Other</option>';
        } else {
            $html .= '<option value="">Select</option>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                    <option value="3">Prefer Not To Say</option>
                                    <option value="4" selected>Other</option>';
        }

        $html .= '</select></span>
                              </div>
                              <div class="col-xs-12">
                                  <label class="text-right">Driver License State/Province :</label>
                                  <span>
                                    <input class="form-control additionalLicense_state" type="text" name="license_state" value="' . $license_state . '"  maxlength="18">
                                  </span>
                              </div>
                              <div class="col-xs-12">
                                  <label class="text-right">Driver License # :</label>
                                  <span>
                                  <input class="form-control additionalLicense" type="text" name="additionalLicense" value="' . $licenseNumber . '" >
                                  </span>
                              </div>
                              <div class="col-xs-12">
                               
                                  <label class="text-right">Vehicle :</label>';
        if ($vehicle == "1") {
            $html .= '<span><input type="radio" name="vehicle" value="1" checked >Yes
                                  <input type="radio" name="vehicle" value="0" >No</span>';
        } else {
            $html .= '<span><input type="radio" name="vehicle" value="1" >Yes
                                  <input type="radio" name="vehicle" value="0" checked>No</span>';
        }

        $html .= '
                              </div>

                                <div class="col-xs-12">
                                  <label class="text-right">Semester Billing :</label>
                                  <span class="semester_bill">No</span>
                              </div>                             
                          </div>
                          <div class="col-sm-6">
                              <div class="col-xs-12">
                                
                                  <label class="text-right">Number of Keys Signed at Move In :</label>
                                  <span>
                                  <input class="form-control add-input keys_signed" type="text" value="' . $keys . '" name="keys_signed"></span>
                                
                              </div> 
                              <div class="col-xs-12">
                                  <label class="text-right">Smoker :</label>';
        if ($smocker == "1") {
            $html .= '<span><input type="radio" name="smoker" value="1" checked >Yes
                                  <input type="radio" name="smoker" value="0" >No</span>';
        } else {
            $html .= '<span><input type="radio" name="smoker" value="1" >Yes
                                  <input type="radio" name="smoker" value="0" checked> No</span>';
        }
        $html .= '</div>
                              <div class="col-xs-12">
                                
                                  <label class="text-right">Pet :</label>';
        if ($pet == "1") {
            $html .= '<span><input type="radio" name="pet" value="1" checked >Yes
                                  <input type="radio" name="pet" value="0" >No</span>';
        } else {
            $html .= '<span><input type="radio" name="pet" value="1" >Yes
                                  <input type="radio" name="pet" value="0" checked>No</span>';
        }
        $html .= '</div>
                              <div class="col-xs-12">
                                
                                  <label class="text-right">Medical/Allergy :</label>';
        if ($medical == "1") {
            $html .= '<span><input type="radio" name="medical" value="1" checked >Yes
                                  <input type="radio" name="smedicalmoker" value="0" >No</span>';
        } else {
            $html .= '<span><input type="radio" name="medical" value="1" >Yes
                                  <input type="radio" name="medical" value="0" checked>No</span>';
        }
        $html .= '</div>
                                                                               
                          </div>
                      </div>
          </div><div class="btn-outer text-right"><input type="submit" value="Update" class="blue-btn editAdditionalTenantForm"><input type="button" value="Reset" class="resetEditGereral clear-btn" data-tab="additionalInfo"></div>
          </form>';
        return $html;


    }

    public function license($tenant_id)
    {
        $license = $this->companyConnection->query("SELECT tenant_license_number as license,tenant_license_state as license_state FROM tenant_details WHERE user_id =" . $tenant_id)->fetch();

        return  $license;
    }

    public function getHobbiesName($datahobbies)
    {
        if (is_array($datahobbies)) {
            $hobby = [];
            foreach ($datahobbies as $key => $value) {
                if($value!='multiselect-all'){
                    $getName = $this->companyConnection->query("SELECT * FROM hobbies WHERE id=$value")->fetch();
                    $hobby[] = $getName['hobby'];
                }
                 }
            return implode(',', $hobby);
        } else if (is_numeric($datahobbies)) {
            $getName = $this->companyConnection->query("SELECT * FROM hobbies WHERE id=$datahobbies")->fetch();
            return $getName['hobby'];
        } else {
            return 'N/A';
        }
    }

    public function getGeneralEmails($email1, $email2, $email3)
    {
        $html = '';
        $html .= '<div class="col-sm-3 col-md-3">
      <label>Email <em class="red-star">*</em></label>
      <div class="multipleEmail">
      <input class="form-control add-input" type="text" name="email[]" value="' . $email1 . '" maxlength="100">
     <a class="add-icon email-plus-sign" href="javascript:;"><i
                      class="fa fa-plus-circle"
                      aria-hidden="true"></i></a>
      <a class="add-icon email-remove-sign" href="javascript:;" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>                
      </div>';
        if ($email2 != "") {
            $html .= '
      <div class="multipleEmail">
      <input class="form-control add-input" type="text" name="email[]" value="' . $email2 . '" maxlength="100">
  <a class="add-icon email-remove-sign" href="javascript:;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
      </div>';
        }


        if ($email3 != "") {
            $html .= '
      <div class="multipleEmail">
      <input class="form-control add-input" type="text" name="email[]" value="' . $email3 . '" maxlength="100">
  <a class="add-icon email-remove-sign" href="javascript:;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
      </div>';
        }


        $html .= '</div>';

        return $html;


    }

    public function getUnitInfo($unit_id)
    {
        return $this->companyConnection->query("SELECT * FROM unit_details WHERE id ='" . $unit_id . "'")->fetch();
    }

    public function getPhoneInfo($tenant_id)
    {
        $getPhoneInfo = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE parent_id=0 and user_id ='" . $tenant_id . "'")->fetchAll();
        $getPhoneType = $this->companyConnection->query("SELECT * FROM phone_type")->fetchAll();
        $getCarrierInfo = $this->companyConnection->query("SELECT * FROM carrier")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();

        $html = "";

        if (empty($getPhoneInfo)) {

            $html .= '<div class="row primary-tenant-phone-row2">
                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>Phone Type</label>
                                                        <select class="form-control" name="phoneType[]">
                                                            <option value="">Select</option>
                                                            <option value="1">mobile</option>
                                                            <option value="2">work</option>
                                                            <option value="3">Fax</option>
                                                            <option value="4">Home</option>
                                                            <option value="5">Other</option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>Carrier <em class="red-star">*</em></label>
                                                        <select class="form-control" name="carrier[]">';
            foreach ($getCarrierInfo as $carrierInfo) {
                $html .= "<option value=" . $carrierInfo['id'] . ">" . $carrierInfo['carrier'] . "</option>";
            }
            $html .= '</select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 countycodediv">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="countryCode[]">';
            foreach ($countryCode as $code) {
                $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "</option>";
            }
            $html .= '</select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>Phone Number <em class="red-star">*</em></label>
                                                        <input class="form-control capsOn add-input phone_format" type="text"
                                                               name="phoneNumber[]">
                                                        <a class="add-icon" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <a class="add-icon" href="javascript:;" style="display:none"><i
                                                                    class="fa fa-minus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>';
        } else {

            $i = 0;
            foreach ($getPhoneInfo as $phoneInfo) {

                $html .= '<div class="row primary-tenant-phone-row2" >
                                  <div class="col-sm-3">
                                      <label>Phone Type</label>
                                     <select class="form-control" name="phoneType[]">';

                foreach ($getPhoneType as $phoneType) {

                    if ($phoneType['id'] == $phoneInfo['phone_type']) {
                        $selected = "selected=selected";
                    } else {
                        $selected = "";
                    }


                    $html .= "<option value=" . $phoneType['id'] . " " . $selected . ">" . $phoneType['type'] . "</option>";
                }

                $html .= '</select>
                                     </div>
                                     <div class="col-sm-3">
                                      <label>Phone Number</label>
                                      <input class="form-control phone_format" maxlength="12" type="text" name="phoneNumber[]" value="' . $phoneInfo['phone_number'] . '">
                                      </div>
                                        <div class="col-md-1 ext_phone">
                                                            <label>Extension</label>
                                                            <input name="Extension[]" class="form-control" type="text" placeholder="Eg: + 161" value="' . $phoneInfo['work_phone_extension'] . '">
                                                        </div>
                                      <div class="col-sm-3">
                                      <label>Carrier <em class="red-star">*</em></label>
                                      <select class="form-control" name="carrier[]">';
                foreach ($getCarrierInfo as $carrierInfo) {
                    if ($carrierInfo['id'] == $phoneInfo['carrier']) {
                        $selected = "selected=selected";
                    } else {
                        $selected = "";
                    }

                    $html .= "<option value=" . $carrierInfo['id'] . " " . $selected . ">" . $carrierInfo['carrier'] . "</option>";
                }

                $html .= '</select></div>

                                       
                                  <div class="col-sm-3">
                                      <label>Country Code</label>
                                         <select class="form-control  add-input" name="countryCode[]" style="">';
                foreach ($countryCode as $code) {
                    if ($code['id'] == $phoneInfo['country_code']) {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] . "</option>";
                    } else {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "</option>";
                    }
                }

                $html .= '</select>

                                     
                                  
                                  ';

                if ($i == 0) {
                    $html .= '<a class="add-icon check2" href="javascript:;" style=""><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                    $html .= '<a class="add-icon check2" href="javascript:;" style="display:block;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></div>';
                }


                if ($i > 0) {

                    $html .= '<a class="add-icon check1" href="javascript:;" style="display: none"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>';
                    $html .= '<a class="add-icon check1" href="javascript:;" style="display:block;"><i  style="display:block;" class="fa fa-minus-circle" aria-hidden="true"></i></a></div>';

                }

                $html .= '</div>
                              </div>';

                $i++;
            }
        }
        return $html;
    }

    public function getEmergencyInfo($tenant_id)
    {
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $tenant_id . "' and parent_id=0")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html = "";
        if (empty($getEmergencyInfo)) {
            $html .= '<div class="row tenant-emergency-contact">
                                                    <div class="form-outer">

                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Emergency Contact Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="emergency" name="emergency_contact_name[]" maxlength="18">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Relationship</label>
                                                            <select class="form-control" id="relationship"
                                                                    name="emergency_relation[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Daughter</option>
                                                                <option value="2">Father</option>
                                                                <option value="3">Friend</option>
                                                                <option value="4">Mother</option>
                                                                <option value="5">Owner</option>
                                                                <option value="6">Partner</option>
                                                                <option value="7">Son</option>
                                                                <option value="8">Spouse</option>
                                                                <option value="9">Other</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 countycodediv">
                                                            <label>Country Code</label>
                                                            <select class="form-control" name="emergency_country[]">';
            foreach ($countryCode as $code) {

                $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "(" . $code['code'] . ")" . "</option>";

            }
            $html .= '</select>';
            $html .= '<span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">

                                                            <label>Phone</label>
                                                            <input class="form-control capsOn phone_format" type="text"
                                                                   id="phoneNumber"
                                                                   name="emergency_phone[]">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="clearfix"></div>

                                                    </div>

                                               
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Email</label>
                                                            <input class="form-control " type="text" 
                                                                   name="emergency_email[]" maxlength="100">
                                                                      <a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                                     <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    

                                                </div>';


        } else {
            $i = 0;
            foreach ($getEmergencyInfo as $emergencyInfo) {
                $html .= '
                                                <div class="row tenant-emergency-contact">
                                                    <div class="col-sm-2">
                                                        <label>Emergency Contact Name</label>
                                              <input class="form-control" type="text" value="' . $emergencyInfo['emergency_contact_name'] . '" name="emergency_contact_name[]" maxlength="18">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Relationship</label>
                                                        <select class="form-control" name="emergency_relation[]">';
                $relationArr = array("" => "Select", "1" => "Daughter", "2" => "Father", "3" => "Friend", "4" => "Mother", "5" => "Owner", "6" => "Partner", "7" => "Son", "8" => "Spouse", "9" => "Other");
                foreach ($relationArr as $key => $value) {
                    if ($key == $emergencyInfo['emergency_relation']) {
                        $html .= '<option value="' . $key . '" selected>' . $value . '</option>';
                    } else {
                        $html .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                }

                $html .= '</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="emergency_country[]">';
                foreach ($countryCode as $code) {
                    if ($code['id'] == $emergencyInfo['emergency_country_code']) {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] . "(" . $code['code'] . ")" . "</option>";
                    } else {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "(" . $code['code'] . ")" . "</option>";
                    }
                }
                $html .= '</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Phone Number</label>
                                            <input class="form-control phone_format" type="text" maxlength="12" name="emergency_phone[]" value="' . $emergencyInfo['emergency_phone_number'] . '">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Email</label>
                                                        <input class="form-control add-input" type="text" value="' . $emergencyInfo['emergency_email'] . '" name="emergency_email[]" maxlength="100">';

                if ($i != 0) {


                    $html .= '<a class="add-icon remove-emergency-contant"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                } else {
                    $html .= '<a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>';

                    $html .= '<a class="add-icon remove-emergency-contant" style="display:none"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                }


                $html .= '</div>
                                </div>';

                $i++;
            }


        }

        return $html;

    }

    public function getViewEmergencyInfo($tenant_id)
    {
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $tenant_id . "' and parent_id=0")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html = "";


        if (empty($getEmergencyInfo)) {

            $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                    <span></span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Email :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                    <span></span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';

        } else {

            foreach ($getEmergencyInfo as $emergencyInfo) {

                $countryCode = $emergencyInfo['emergency_country_code'];
                $getCountryCode = $this->companyConnection->query("SELECT * FROM countries WHERE id ='" . $countryCode . "'")->fetch();
                $code = $getCountryCode['code'];

                $relation = $emergencyInfo['emergency_relation'];
                if ($relation == 1) {
                    $emergencyRelation = 'Daughter';
                } else if ($relation == 2) {
                    $emergencyRelation = 'Father';
                } else if ($relation == 3) {
                    $emergencyRelation = 'Friend';
                } else if ($relation == 4) {
                    $emergencyRelation = 'Mother';
                } else if ($relation == 5) {
                    $emergencyRelation = 'Owner';
                } else if ($relation == 6) {
                    $emergencyRelation = 'Partner';
                } else if ($relation == 7) {
                    $emergencyRelation = 'Son';
                } else if ($relation == 8) {
                    $emergencyRelation = 'Spouce';
                } else {
                    $emergencyRelation = 'Other';
                }


                $getEmergencyRelation = $this->companyConnection->query("SELECT * FROM countries WHERE id ='" . $countryCode . "'")->fetch();

                $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span>' . $emergencyInfo['emergency_contact_name'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span>' . $code . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                     <span>' . $emergencyInfo['emergency_phone_number'] . '</span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Emaile :</label>
                                                                   <span>' . $emergencyInfo['emergency_email'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                     <span>' . $emergencyRelation . '</span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';
            }


        }

        return $html;

    }

    public function getviewCredentialsInfo($tenant_id)
    {

        $getCredentialInfo = $this->companyConnection->query("SELECT * FROM tenant_credential WHERE user_id ='" . $tenant_id . "'")->fetchAll();
        $getCredentialType = $this->companyConnection->query("SELECT * FROM tenant_credential_type")->fetchAll();

        $html = "";
        if (empty($getCredentialInfo)) {

            $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Credential Name :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Credential Type :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Acquire Date:</label>
                                                                    <span></span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Date:</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right"></label>
                                                                    <span></span>
                                                           </div>
                                                                                                                 
                                         </div>
                                   </div>
                             </div>';

        } else {

            foreach ($getCredentialInfo as $credentialInfo) {

                $acquire_date = dateFormatUser($credentialInfo['acquire_date'], null, $this->companyConnection);

                $expire_date = dateFormatUser($credentialInfo['expire_date'], null, $this->companyConnection);

                $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Credential Name : </label>
                                                                    <span>' . $credentialInfo['credential_name'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Credential Type :</label>
                                                                    <span>' . $this->getCredentialType($credentialInfo['credential_type']) . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Acquire Date:</label>
                                                                    <span>' . $acquire_date . '</span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Date:</label>
                                                                   <span>' . $expire_date . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Notice Period :</label>
                                                                    <span>' . $credentialInfo['notice_period'] . '</span>
                                                           </div>
                                                                                                                 
                                         </div>
                                   </div>
                             </div>';


            }

        }

        return $html;


    }

    public function getCredentialType($id)
    {
        $getData = $this->companyConnection->query("SELECT credential_type FROM tenant_credential_type WHERE id ='" . $id . "'")->fetch();
        if (empty($getData)) {
            return "Other";
        } else {
            return $getData['credential_type'];
        }


    }

    public function getCredentialInfo($tenant_id)
    {
        $getCredentialInfo = $this->companyConnection->query("SELECT * FROM tenant_credential WHERE user_id ='" . $tenant_id . "'")->fetchAll();
        $getCredentialType = $this->companyConnection->query("SELECT * FROM tenant_credential_type")->fetchAll();

        $today = date('Y-m-d H:i:s');
        $todayDate = dateFormatUser($today, null, $this->companyConnection);


        $html = "";
        if (empty($getCredentialInfo)) {
            $html .= '<div class="row tenant-credentials-control">
                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Credential Name</label>
                                                        <input class="form-control capsOn" type="text"
                                                               id="credentialName" name="credentialName[]" maxlength="18">
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 apx-inline-popup credintials">
                                                        <label>Credential Type
                                                            <a class="pop-add-icon tenantCredentialType" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="credentialType[]">
                                                            <option value="">Select</option>
                                                            <option value="1">Bond</option>
                                                            <option value="2">Certification</option>
                                                        </select>
                                                        <div class="add-popup" id="tenantCredentialType1">
                                                            <h4>Add Credential Type</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Credential Type <em class="red-star">*</em></label>
                                                                        <input class="form-control credential_source" type="text" placeholder="Ex: License">
                                                                        <span class="red-star" id="credential_source"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                         <input type="button" class="clear-btn" value="Clear">
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Acquire Date</label>
                                                        <input class="form-control capsOn calander" type="text"
                                                               id="acquireDate" name="acquireDate[]" value=' . $todayDate . ' readonly >
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Expiration Date</label>
                                                        <input class="form-control capsOn calander" type="text"
                                                               id="expirationDate" name="expirationDate[]" value=' . $todayDate . ' readonly>
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>

                                                    <div class="clearfix"></div>

                                                </div>
                                                <div class="notice-period">
                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>Notice Period</label>
                                                        <select class="form-control add-input" id="term_plan"
                                                                name="noticePeriod[]">
                                                            <option value="">Select</option>
                                                            <option value="1">5 day</option>
                                                            <option value="2">1 Month</option>
                                                            <option value="3">2 Month</option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                        <a class="add-icon add-notice-period" href="javascript:;"><i
                                                                    class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                        <a class="add-icon remove-notice-period" style="">
                                                            <i class="fa fa-minus-circle " aria-hidden="true "></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>';
        } else {
            $i = 0;
            foreach ($getCredentialInfo as $credentialInfo) {

                $acquire_date = dateFormatUser($credentialInfo['acquire_date'], null, $this->companyConnection);

                $expire_date = dateFormatUser($credentialInfo['expire_date'], null, $this->companyConnection);

                $html .= '  <div class="row tenant-credentials-control">
                                                    <div class="col-sm-2">
                                                        <label>Credential Name</label>
                                                        <input class="form-control" type="text" name="credentialName[]" value="' . $credentialInfo['credential_name'] . '" maxlength="18">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Credential Type <a class="pop-add-icon credntl_popup"
                                                                                  href="javascript:;"><i
                                                                        class="fa fa-plus-circle"
                                                                        aria-hidden="true"></i></a></label>
                                                        <select class="form-control edit_general_credentials" name="credentialType[]">';

                foreach ($getCredentialType as $credentialType) {
                    if ($credentialType['id'] == $credentialInfo['credential_type']) {
                        $selected = "selected=selected";
                    } else {
                        $selected = "";
                    }

                    $html .= "<option value=" . $credentialType['id'] . " " . $selected . ">" . $credentialType['credential_type'] . "</option>";
                }


                $html .= '</select><div class="add-popup" id="tenantCredentialType1">
                                                                <h4>Add Credential Type</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>Credential Type <em class="red-star">*</em></label>
                                                                            <input class="form-control credential_source" type="text" placeholder="Ex: License">
                                                                            <span class="red-star" id="credential_source"></span>
                                                                        </div>
                                                                        <div class="btn-outer text-right">
                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                             <input type="button" class="clear-btn" value="Clear">
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>';
                $html .= '<div class="col-sm-2">
                                                        <label>Acquire Date</label>
                                                        <input class="form-control calander" type="text" value="' . $acquire_date . '" name="acquireDate[]" readonly>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Expiration Date </label>
                                                        <input class="form-control calander" type="text"  value="' . $expire_date . '" name="expirationDate[]" readonly>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Notice Period </label>
                                                        <input class="form-control add-input" type="text" name="noticePeriod[]" value="' . $credentialInfo['notice_period'] . '">';
                if ($i != 0) {


                    $html .= '<a class="add-icon remove-notice-period"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                } else {
                    $html .= '<a class="add-icon add-notice-period" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>';

                    $html .= '<a class="add-icon remove-notice-period" style="display:none"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                }
                $html .= '</div>
                                                                    </div>';
                $i++;
            }

        }


        return $html;
    }

    public function getSsnData($ssn_id)
    {
        $html = '';
        if (is_array($ssn_id)) {
            $count = count($ssn_id);
        } else {
            $count = 0;
        }

        if (empty($ssn_id)) {
            $html .= '<div class="multipleSsn col-sm-12">
                          <label>SSN/SIN/ID</label>
                          <input class="form-control add-input" type="text" name="ssn[]"  maxlength="10"/>
                          <a class="add-icon ssn-remove-sign" href="javascript:;" style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                          <a class="add-icon ssn-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                        </div>';
        } else {
            if ($count == 0 && $ssn_id == "") {
                return $html;
            }
            for ($i = 0; $i < count($ssn_id); $i++) {
                if ($ssn_id[$i] != "") {
                    $html .= '<div class="multipleSsn col-sm-12">
                          <label>SSN/SIN/ID</label>
                          <input class="form-control add-input" type="text" name="ssn[]"  maxlength="10" value="' . $ssn_id[$i] . '">';
                    if ($i == 0) {
                        $html .= '<a class="add-icon ssn-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                <a class="add-icon ssn-remove-sign" href="javascript:;" style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>';
                    } else {
                        $html .= '<a class="add-icon ssn-remove-sign" href="javascript:;" style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>';
                    }
                    $html .= '</div>';

                }
            }

        }
        return $html;

    }

    public function leaseInfo($tenant_id)
    {
        return $this->companyConnection->query("SELECT move_in,move_out,start_date,end_date,security_deposite,rent_amount,notice_period,term,tenure FROM tenant_lease_details  WHERE user_id ='" . $tenant_id . "'")->fetch();

    }

    public function getTenantCharge($tenant_id)
    {
        $getData = $this->companyConnection->query("SELECT * FROM tenant_charge where user_id ='" . $tenant_id . "'")->fetch();
        return $getData;
    }

    public function getTaxDetails($tenant_id)
    {
        $taxDetails = $this->companyConnection->query("SELECT * FROM tenant_taxdetails where user_id ='" . $tenant_id . "'")->fetch();
        return $taxDetails;
    }

    public function insertChargeFormData()
    {
        $data['user_id'] = $_POST['tenant_id'];
        $data['charge_code'] = $_POST['chargeCode'];
        $data['frequency'] = $_POST['frequency'];
        $data['amount'] = str_replace(',', '', $_POST['amount']);

        $data['amount_due'] = str_replace(',', '', $_POST['amount']);
        $data['start_date'] = mySqlDateFormat($_POST['startDate'], null, $this->companyConnection);
        $data['end_date'] = mySqlDateFormat($_POST['endDate'], null, $this->companyConnection);
        $data['record_status'] = 1;
        $sqlData = createSqlColVal($data);
        $query = "INSERT INTO tenant_charges(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($data);
        $getChargeAmount = $this->addChargeAmount($data['user_id']);
        echo $this->getChargeListing();
        exit;
        array('status' => 'success', 'message' => 'Data Add Successfully.', 'table' => 'tenant_charges');

    }


public function addChargeAmount($user_id)
    {

      $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due FROM tenant_charges WHERE user_id ='" . $user_id . "' and frequency='One Time'")->fetch();

    if($getCharges['total_amount']=='')
      {
        $totalAmount = 0;
        $data['total_amount'] = $totalAmount;
      }
      else
      {
        $totalAmount = $getCharges['total_amount'];
        $data['total_amount'] = $totalAmount;
      }


    if($getCharges['amount_paid']=='')
      {
        $amount_paid = 0;
        $data['total_amount_paid'] = $amount_paid;
      }
      else
      {
        $amount_paid = $getCharges['amount_paid'];
        $data['total_amount_paid'] = $amount_paid;
      }

       if($getCharges['amount_refunded']=='')
      {
        $amount_refunded = 0;
         $data['total_refunded_amount'] = $amount_refunded;
      }
      else
      {
        $amount_refunded = $getCharges['amount_refunded'];
         $data['total_refunded_amount'] = $amount_refunded;
      }
       $data['created_at'] = date('Y-m-d H:i:s');
       $data['updated_at'] = date('Y-m-d H:i:s');

      $checkManageCharge  = $this->companyConnection->query("SELECT *  FROM accounting_manage_charges WHERE user_id ='" . $user_id . "'")->fetch();
      if(empty($checkManageCharge))
      {
         $data['total_due_amount'] = $totalAmount;
         $data['user_id'] = $user_id;
          $sqlData = createSqlColVal($data);
           $query = "INSERT INTO accounting_manage_charges(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
      }
      else
      {
        $data['user_id'] = $user_id;
         $data['total_due_amount'] = $getCharges['amount_due'];
        $sqlData = createSqlColValPair($data);

            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $user_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
      }


    }

    public function chargeDetails()
    {
        $tenant_id = $_REQUEST['tenant_id'];
        $getChargeList = $this->companyConnection->query("SELECT * FROM tenant_charges WHERE user_id ='" . $tenant_id . "'")->fetchAll();
$default_symbolCurrency = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';

        $html = '';


        $html .= "<form id='editChargeDateAmount'>";



        $html .= "<table border= '1px'>";
        $html .= "<tr>
  <th>Charge</th>
  <th>Frequency</th>
  <th>Start Date</th>
  <th>End Date</th>
  <th>Amount(".$default_symbolCurrency.")</th>
            </tr>";

        foreach ($getChargeList as $list) {

            $id = $list['id'];
            $charge = $this->getChargeCode($list['charge_code']);
            $frequency = $list['frequency'];
            $start_date = $list['start_date'];
            $startDate = dateFormatUser($start_date, null, $this->companyConnection);


            $end_date = $list['end_date'];
            $endDate = dateFormatUser($end_date, null, $this->companyConnection);

            $amount = $list['amount'];

            $html .= "<input type='hidden' name='chargeId[]' value='$id'>";
            $html .= "<tr>";
            $html .= "<td>" . $charge . "</td>";
            $html .= "<td>" . $frequency . "</td>";
            $html .= "<td>" . $startDate . "</td>";
            $html .= "<td><div class='endDate_$id'><input type='hidden' name='editDate[]'><a href='JavaScript:Void(0);' class='editEndDate' data-id='" . $id . "'>" . $endDate . "<i class='fas fa-highlighter'></i></a></div></td>";
            $html .= "<td><div class='endAmount_$id'><input type='hidden' name='editAmount[]'><a href='JavaScript:Void(0);' class='editAmount amount_$id' data-id='" . $id . "'>" . $amount . "<i class='fas fa-highlighter'></i></a></div></td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";
        if (!empty($getChargeList)) {
            $html .= "<input type='button' class='blue-btn editChargeInfo' value='Apply'>";

        }




        echo $html;
        exit;

    }

    public function getChargeListing()
    {
        $tenant_id = $_REQUEST['tenant_id'];
        $getChargeList = $this->companyConnection->query("SELECT * FROM tenant_charges WHERE user_id ='" . $tenant_id . "' and invoice_id is NULL")->fetchAll();


        $html = '';
        $default_symbolCurrency = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';
        $html = "<form id='editChargeDateAmount'>";
        $html .= "<table class='table-responsive' border= '1px'>";
        $html .= "<tr>
                  <th width='5%'>Charge</th>
                  <th width='20%'>Frequency</th>
                  <th width='15%'>Start Date</th>
                  <th width='20%'>End Date</th>
                  <th width='20%'>Amount (" . $default_symbolCurrency . ")</th>
            </tr>";

        foreach ($getChargeList as $list) {

            $id = $list['id'];
            $charge = $this->getChargeCode($list['charge_code']);
            $frequency = $list['frequency'];
            $start_date = $list['start_date'];
            $startDate = dateFormatUser($start_date, null, $this->companyConnection);


            $end_date = $list['end_date'];
            $endDate = dateFormatUser($end_date, null, $this->companyConnection);

            $amount = $list['amount'];
            $amount = $default_symbolCurrency.number_format($amount,2);


            $html .= "<input type='hidden' name='chargeId[]' value='$id'>";
            $html .= "<tr>";
            $html .= "<td>" . $charge . "</td>";
            $html .= "<td>" . $frequency . "</td>";
            $html .= "<td>" . $startDate . "</td>";
            $html .= "<td>" . $endDate . "</td>";
            $html .= "<td>" . $amount . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";


        echo $html;
        exit;
    }
    public function getChargeCode($codeId)
    {
        $chargeCode = $this->companyConnection->query("SELECT * FROM company_accounting_charge_code WHERE id ='" . $codeId . "'")->fetch();
        return $chargeCode['charge_code'];

    }


    /*update tenant data*/

    public function deleteRecords()
    {
        $date = date('Y-m-d H:i:s');
        $tableName = $_REQUEST['tablename'];
        $id = $_REQUEST['id'];


        $query = "UPDATE $tableName SET deleted_at='$date' where id=" . $id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array('status' => 'success', 'message' => 'Record Deleted Successfully.', 'table' => $tableName);

    }

    public function update()
    {
        $tenant_id = $_POST['tenant_id'];

        $updateUser = $this->updateUserTable($tenant_id);

        $updateTenantDetails = $this->updateTenantDetails($tenant_id);

        $updatePhoneInfo = $this->updatePhoneInfo($tenant_id);

        $updateEmergencyContant = $this->updateEmergencyContant($tenant_id);

        $editNoticePeriod = $this->editNoticePeriod($tenant_id);
        $notifiData = "SELECT u.name from users as u where u.id =".$tenant_id;
        $notifiquery = $this->companyConnection->query($notifiData)->fetch();
        $tName = $notifiquery['name'];
        /* lease notification */
        $notificationTitle= "Tenant Information Update";
        $module_type = "TENANT";
        $alert_type = "Real Time";
        $descriptionNotifi ='The information for Tenant ID-'.$tenant_id.'  ( Name -'.$tName.' ) has been updated.';
        insertNotification($this->companyConnection,$tenant_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
        /* lease notification */

        return $updateCredentialControl = $this->updateCredentialControl($tenant_id);



    }

    public function updateUserTable($tenant_id)
    {
        $columns = ['users.first_name', 'users.last_name', 'users.middle_name', 'users.dob', 'users.ssn_sin_id'];
        if (isset($_POST['birth']) && $_POST['birth'] != "") {
            $dataa['dob'] = mySqlDateFormat($_POST['birth'], null, $this->companyConnection);
        } else {
            $dataa['dob'] = '';
        }
        $where = [['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $_POST["firstname"]], ['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $_POST["lastname"]], ['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $_POST["middlename"]], ['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $_POST['phoneNumber'][0]], ['table' => 'users', 'column' => 'dob', 'condition' => '=', 'value' => $dataa['dob']]];
        $duplicate_record_check=checkMultipleDuplicatesEdit($this->companyConnection, $_POST['email'][0],$_POST['phoneNumber'][0],$_POST['ssn'],'','','',$tenant_id);
        if(isset($duplicate_record_check)){
            if($duplicate_record_check['code'] == 503){
                echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                die();
            }
        }
        try {

            if (isset($_POST['email'])) {
                if (is_array($_POST['email'])) {
                    $data['email'] = $_POST['email'][0];
                } else {
                    $data['email'] = $_POST['email'];
                }
            }

            if (isset($_POST['salutation'])) {
                $data['salutation'] = $_POST['salutation'];
            }

            $data['name'] = $_POST['firstname'] . ' ' . $_POST['lastname'];

            if (isset($_POST['firstname'])) {
                $data['first_name'] = $_POST['firstname'];
            }

            if (isset($_POST['lastname'])) {
                $data['last_name'] = $_POST['lastname'];
                $data['name'] = $_POST['firstname'] . ' ' . $_POST['lastname'];
            }

            if (isset($_POST['middlename'])) {
                $data['middle_name'] = $_POST['middlename'];
            }

            if (isset($_POST['medianName'])) {
                $data['maiden_name'] = $_POST['medianName'];
            }

            if (isset($_POST['ethncity'])) {
                $data['ethnicity'] = $_POST['ethncity'];
            }

            if (isset($_POST['maritalStatus'])) {
                $data['maritial_status'] = $_POST['maritalStatus'];
            }

            if (isset($_POST['edit_general_veteran'])) {
                $data['veteran_status'] = $_POST['edit_general_veteran'];
            }

            if (isset($_POST['ssn'])) {
               foreach($_POST['ssn'] as $key=>$value)
            {
                if(is_null($value) || $value == '')
                    unset($_POST['ssn'][$key]);
            }
            $data['ssn_sin_id'] = !empty($_POST['ssn'])? serialize($_POST['ssn']):NULL;
            }

            if (isset($_POST['note'])) {
                $data['phone_number_note'] = $_POST['note'];


            }

            if (isset($_POST['nickname'])) {
                $data['nick_name'] = $_POST['nickname'];
            }


            if (isset($_POST['checkBoxhobbies']) && !empty($_POST['checkBoxhobbies'])) {
                $data['hobbies'] = serialize($_POST['checkBoxhobbies']);
            }
            $data['updated_at'] = date('Y-m-d H:i:s');

            $sqlData = createSqlUpdateCase($data);

            $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" . $tenant_id;


            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($sqlData['data']);
            $tenantData = $data;
            $tenantData['email'] = $_POST['email'];
            $tenantData['phone_number'] = $_POST['phoneNumber'];
            $tenantData['status'] = '1';
            $tenantData['id'] = $tenant_id;
            $ElasticSearchSave = insertDocument('TENANT','UPDATE',$tenantData,$this->companyConnection);
            updateUsername($tenant_id, $this->companyConnection);
            return array('code' => 200, 'status' => 'success', 'table' => "users");

        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'table' => "users");
        }


    }

    public function updateTenantDetails($tenant_id)
    {

        try {
            $data['tenant_image'] = json_decode($_POST['tenant_image']);

            $emailCount = count($_POST['email']);
            if ($emailCount == 1) {
                $data['email1'] = $_POST['email'][0];
            } else if ($emailCount == 2) {
                $data['email1'] = $_POST['email'][0];
                $data['email2'] = $_POST['email'][1];
            } else if ($emailCount == 3) {
                $data['email1'] = $_POST['email'][0];
                $data['email2'] = $_POST['email'][1];
                $data['email3'] = $_POST['email'][2];
            }
            $data['status'] = $_POST['tenant_status'];


            $sqlData = createSqlColValPair($data);

            $query = "UPDATE tenant_details SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $tenant_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'table' => "tenant_details");
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'table' => "tenant_details");
            printErrorLog($e->getMessage());
        }

    }

    public function updatePhoneInfo($tenant_id)
    {

        try {

            $phoneType = $_POST['phoneType'];
            $userphoneNumber = $_POST['phoneNumber'];


            $count = $this->companyConnection->prepare("DELETE FROM tenant_phone WHERE  parent_id=0 and user_id=$tenant_id");
            $count->execute();
            $i = 0;
            foreach ($phoneType as $phone) {
                if (isset($_POST['other_work_phone_extension'][$i]) && $_POST['other_work_phone_extension'][$i] != "") {
                    $data['work_phone_extension'] = $_POST['other_work_phone_extension'][$i];
                }
                if (isset($_POST['work_phone_extension'][$i]) && $_POST['work_phone_extension'][$i] != "") {
                    $data['other_work_phone_extension'] = $_POST['work_phone_extension'][$i];
                }

                $data['user_id'] = $tenant_id;
                $data['parent_id'] = 0;
                $data['phone_type'] = $phone;
                $data['carrier'] = $_POST['carrier'][$i];
                $data['phone_number'] = $_POST['phoneNumber'][$i];
                $data['country_code'] = $_POST['countryCode'][$i];
                $data['user_type'] = 0; //0 value for main tenant

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_phone(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);


                $i++;
            }

            $data1['phone_number'] = $userphoneNumber[0];

            $sqlData1 = createSqlColValPair($data1);
            $query1 = "UPDATE users SET " . $sqlData1['columnsValuesPair'] . " where id='$tenant_id'";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute();


            return array('status' => 'success', 'message' => 'Data Retrieved Successfully.', 'table' => 'tenant_phone');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'table' => "tenant_details");
            printErrorLog($e->getMessage());
        }


    }

    public function updateEmergencyContant($tenant_id)
    {


        $count = $this->companyConnection->prepare("DELETE FROM emergency_details WHERE  parent_id=0 and user_id=$tenant_id");
        $count->execute();
        try {


            $i = 0;
            foreach ($_POST['emergency_contact_name'] as $credentialName) {

                $data['user_id'] = $tenant_id;
                $data['parent_id'] = 0;
                $data['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $data['emergency_relation'] = $_POST['emergency_relation'][$i];
                $data['emergency_country_code'] = $_POST['emergency_country'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $data['emergency_email'] = $_POST['emergency_email'][$i];


                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;

            }

            return array('status' => 'success', 'message' => 'Data Retrieved Successfully.', 'table' => 'emergency_details');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'emergency_details');
            printErrorLog($e->getMessage());
        }


    }

    public function editNoticePeriod($tenant_id)
    {
        $data['notice_period'] = $_POST['notice_period'];
        $sqlData = createSqlColValPair($data);

        $query = "UPDATE tenant_lease_details SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();

    }

    public function updateCredentialControl($tenant_id)
    {

        $count = $this->companyConnection->prepare("DELETE FROM tenant_credential WHERE  user_id=$tenant_id");
        $count->execute();

        try {
            if (isset($_POST['credentialName'])) {


                $i = 0;
                foreach ($_POST['credentialName'] as $credentialName) {
                    $acquire_date = $_POST['acquireDate'][$i];
                    $expireDate = $_POST['expirationDate'][$i];
                    $data['user_id'] = $tenant_id;
                    $data['credential_name'] = $_POST['credentialName'][$i];
                    $data['credential_type'] = $_POST['credentialType'][$i];
                    $data['acquire_date'] = mySqlDateFormat($acquire_date, null, $this->companyConnection);
                    $data['expire_date'] = mySqlDateFormat($expireDate, null, $this->companyConnection);
                    $data['notice_period'] = $_POST['noticePeriod'][$i];
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_credential(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt1 = $this->companyConnection->prepare($query);
                    $stmt1->execute($data);

                    $i++;
                }
            }


            return array('status' => 'success', 'message' => 'Data Retrieved Successfully.', 'table' => 'tenant_credentials');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_credentials');
            printErrorLog($e->getMessage());
        }

    }

    public function checkEmailIsExists()
    {
        $userId = $_POST['id'];
        $userEmail = $_POST['email'][0];
        $sql = "SELECT id,email FROM users where email='$userEmail'";
        $info = $this->companyConnection->query($sql)->fetch();
        if (!empty($info)) {
            if ($info['id'] == $userId) {
                return true;
            }
            return false;
        }
        return true;
    }

    public function getVehicleInfo()
    {
        $id = $_REQUEST['id'];
        $getVehicleInfo = $this->companyConnection->query("SELECT * FROM tenant_vehicles where id=$id")->fetch();
        return $getVehicleInfo;

    }

    public function getPetInfo()
    {
        $id = $_REQUEST['id'];
        $getPetInfo = $this->companyConnection->query("SELECT * FROM tenant_pet where id=$id")->fetch();
        return $getPetInfo;

    }

    public function getAnimalInfo()
    {
        $id = $_REQUEST['id'];
        $getAnimalInfo = $this->companyConnection->query("SELECT * FROM tenant_service_animal where id=$id")->fetch();
        return $getAnimalInfo;

    }

    public function getMedicalInfo()
    {
        $id = $_REQUEST['id'];
        $getMedicalInfo = $this->companyConnection->query("SELECT * FROM tenant_medical_allergies where id=$id")->fetch();
        return $getMedicalInfo;

    }

    public function getCollectionInfo()
    {
        $id = $_REQUEST['id'];
        $getCollectionInfo = $this->companyConnection->query("SELECT * FROM tenant_collection where id=$id")->fetch();
        return $getCollectionInfo;

    }

    public function getParkingInfo()
    {
        $id = $_REQUEST['id'];
        $getParkingInfo = $this->companyConnection->query("SELECT * FROM tenant_parking where id=$id")->fetch();
        return $getParkingInfo;

    }

    public function getGuarantorInfo()
    {

        $tenant_id = $_REQUEST['tenant_id'];
        $id = $_REQUEST['id'];
        $getGuarantorInfo['guarantor_info'] = $this->companyConnection->query("SELECT * FROM tenant_guarantor where id=$id")->fetch();
        $email1 = $getGuarantorInfo['guarantor_info']['email1'];
        $email2 = $getGuarantorInfo['guarantor_info']['email2'];
        $email3 = $getGuarantorInfo['guarantor_info']['email3'];
        $getGuarantorInfo['phone_info'] = $this->getGuarantorPhoneInfo($id, $tenant_id);
        $getGuarantorInfo['generalEmails'] = $this->getGuarantorGeneralEmails($email1, $email2, $email3);

        $getGuarantorInfo['phone_info_form2'] = $this->getGuarantorForm2PhoneInfo($id, $tenant_id);
        $getGuarantorInfo['generalEmailsForm2'] = $this->getGuarantorForm2GeneralEmails($email1, $email2, $email3);


        return $getGuarantorInfo;


    }

    public function getGuarantorPhoneInfo($guarantor_id, $tenant_id)
    {
        $getPhoneInfo = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE parent_id=$tenant_id and user_id ='" . $guarantor_id . "'")->fetchAll();
        $getPhoneType = $this->companyConnection->query("SELECT * FROM phone_type")->fetchAll();
        $getCarrierInfo = $this->companyConnection->query("SELECT * FROM carrier")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();

        $html = "";

        foreach ($getPhoneInfo as $phoneInfo) {

            $html .= '<div class="row guarantor-form1-phone-row">
                                  <div class="col-sm-2">
                                      <label>Phone Type</label>
                                     <select class="form-control" name="guarantor_phoneType[]">';

            foreach ($getPhoneType as $phoneType) {

                if ($phoneType['id'] == $phoneInfo['phone_type']) {
                    $selected = "selected=selected";
                } else {
                    $selected = "";
                }


                $html .= "<option value=" . $phoneType['id'] . " " . $selected . ">" . $phoneType['type'] . "</option>";
            }

            $html .= '</select>
                                     </div>
                                      <div class="col-sm-2">
                                      <label>Carrier <em class="red-star">*</em></label>
                                      <select class="form-control" name="guarantor_carrier[]">';
            foreach ($getCarrierInfo as $carrierInfo) {
                if ($carrierInfo['id'] == $phoneInfo['carrier']) {
                    $selected = "selected=selected";
                } else {
                    $selected = "";
                }

                $html .= "<option value=" . $carrierInfo['id'] . " " . $selected . ">" . $carrierInfo['carrier'] . "</option>";
            }

            $html .= '</select></div>



                                  <div class="col-sm-2">
                                      <label>Country Code</label>
                                         <select class="form-control" name="guarantor_countryCode[]">';
            foreach ($countryCode as $code) {
                if ($code['id'] == $phoneInfo['country_code']) {
                    $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] . "</option>";
                } else {
                    $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "</option>";
                }
            }

            $html .= '</select></div>

                                     
                                  
                                  <div class="col-sm-2">
                                      <label>Phone Number</label>
                                      <input class="form-control add-input phone_format" type="text" name="guarantor_phone[]" value="' . $phoneInfo['phone_number'] . '">
                                      <a class="add-icon guarantor-phonerow-form1-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                       <a class="add-icon guarantor-phonerow-form1-remove-sign" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                  </div>
                              </div>';


        }
        return $html;
    }

    public function getGuarantorGeneralEmails($email1, $email2, $email3)
    {
        $html = '';
        $html .= '<div class="col-sm-3 col-md-3">
      <label>Email <em class="red-star">*</em></label>
      <div class="multipleEmail-form1">
      <input class="form-control add-input" type="text" name="guarantor_email[]" value="' . $email1 . '" maxlength="100">
     <a class="add-icon email-form1-plus-sign" href="javascript:;"><i
                      class="fa fa-plus-circle"
                      aria-hidden="true"></i></a>
      <a class="add-icon email-form1-plus-sign" href="javascript:;" style="display:none;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>                
      </div>';


        if ($email2 != "") {
            $html .= '<div class="multipleEmail-form1">
      <input class="form-control add-input" type="text" name="guarantor_email[]" value="' . $email2 . '" maxlength="100">
        <a class="add-icon email-form1-remove-sign" href="javascript:;"><i
                      class="fa fa-minus-circle"
                      aria-hidden="true"></i></a>
  <a class="add-icon email-form1-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
      </div>';
        }


        if ($email3 != "") {
            $html .= '<div class="multipleEmail-form1">
      <input class="form-control add-input" type="text" name="guarantor_email[]" value="' . $email3 . '" maxlength="100">
      <a class="add-icon email-form1-remove-sign" href="javascript:;"><i
                      class="fa fa-minus-circle"
                      aria-hidden="true"></i></a>
  <a class="add-icon email-form1-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
      </div>';
        }


        $html .= '</div>';

        return $html;

    }

    public function getGuarantorForm2PhoneInfo($guarantor_id, $tenant_id)
    {
        $getPhoneInfo = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE parent_id=$tenant_id and user_id ='" . $guarantor_id . "'")->fetchAll();
        $getPhoneType = $this->companyConnection->query("SELECT * FROM phone_type")->fetchAll();
        $getCarrierInfo = $this->companyConnection->query("SELECT * FROM carrier")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();

        $html = "";

        foreach ($getPhoneInfo as $phoneInfo) {

            $html .= '<div class="row guarantor-form2-phone-row">
                                  <div class="col-sm-2">
                                      <label>Phone Type</label>
                                     <select class="form-control" name="guarantor_form2_phoneType[]">';

            foreach ($getPhoneType as $phoneType) {

                if ($phoneType['id'] == $phoneInfo['phone_type']) {
                    $selected = "selected=selected";
                } else {
                    $selected = "";
                }


                $html .= "<option value=" . $phoneType['id'] . " " . $selected . ">" . $phoneType['type'] . "</option>";
            }

            $html .= '</select>
                                     </div>
                                      <div class="col-sm-2">
                                      <label>Carrier <em class="red-star">*</em></label>
                                      <select class="form-control" name="guarantor_form2_carrier[]">';
            foreach ($getCarrierInfo as $carrierInfo) {
                if ($carrierInfo['id'] == $phoneInfo['carrier']) {
                    $selected = "selected=selected";
                } else {
                    $selected = "";
                }

                $html .= "<option value=" . $carrierInfo['id'] . " " . $selected . ">" . $carrierInfo['carrier'] . "</option>";
            }

            $html .= '</select></div>



                                  <div class="col-sm-2">
                                      <label>Country Code</label>
                                         <select class="form-control" name="guarantor_form2_countryCode[]">';
            foreach ($countryCode as $code) {
                if ($code['id'] == $phoneInfo['country_code']) {
                    $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] . "</option>";
                } else {
                    $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "</option>";
                }
            }

            $html .= '</select></div>

                                     
                                  
                                  <div class="col-sm-2">
                                      <label>Phone Number</label>
                                      <input class="form-control add-input phone_format" type="text" name="guarantor_form2_phoneNumber[]" value="' . $phoneInfo['phone_number'] . '">
                                      <a class="add-icon guarantor-phonerow-form2-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                       <a class="add-icon guarantor-phonerow-form2-remove-sign" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                  </div>
                              </div>';


        }
        return $html;
    }

    public function getGuarantorForm2GeneralEmails($email1, $email2, $email3)
    {
        $html = '';
        $html .= '<div class="col-sm-3 col-md-3">
      <label>Email <em class="red-star">*</em></label>
      <div class="multipleEmail-form2">
      <input class="form-control add-input" type="text" name="guarantor_form2_email[]" value="' . $email1 . '" maxlength="100">
     <a class="add-icon email-form2-plus-sign" href="javascript:;"><i
                      class="fa fa-plus-circle"
                      aria-hidden="true"></i></a>
      <a class="add-icon email-form2-remove-sign" href="javascript:;" style="display:none;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>                
      </div>';


        if ($email2 != "") {
            $html .= '<div class="multipleEmail-form2">
      <input class="form-control add-input" type="text" name="guarantor_form2_email[]" value="' . $email2 . '" maxlength="100">
        <a class="add-icon email-form2-remove-sign" href="javascript:;"><i
                      class="fa fa-minus-circle"
                      aria-hidden="true"></i></a>
  <a class="add-icon email-form2-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
      </div>';
        }


        if ($email3 != "") {
            $html .= '<div class="multipleEmail-form2">
      <input class="form-control add-input" type="text" name="guarantor_form2_email[]" value="' . $email3 . '" maxlength="100">
      <a class="add-icon email-form2-remove-sign" href="javascript:;"><i
                      class="fa fa-minus-circle"
                      aria-hidden="true"></i></a>
  <a class="add-icon email-form2-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
      </div>';
        }

        $html .= '</div>';

        return $html;

    }

    public function addEditVehicle()
    {

        $vehicleType = $_POST['vehicle_type'];
        $vehicle_image1 = json_decode($_POST['vehicle_image1']);
        $vehicle_image2 = json_decode($_POST['vehicle_image2']);
        $vehicle_image3 = json_decode($_POST['vehicle_image3']);

        $data['user_id'] = $_POST['tenant_id'];
        $data['type'] = $vehicleType;
        $data['make'] = $_POST['vehicle_make'];
        $data['license'] = $_POST['vevicle_license'];
        $data['color'] = $_POST['vehicle_color'];
        $data['year'] = $_POST['vevicle_year'];
        $data['vin'] = $_POST['vehicle_vin'];
        $data['registration'] = $_POST['vehicle_registration'];
        $data['updated_at'] =  date('Y-m-d H:i:s');

        $data['photo1'] = $vehicle_image1;
        $data['photo2'] = $vehicle_image2;
        $data['photo3'] = $vehicle_image3;

        if ($_POST['vehicle_action'] == 'edit') {
            $duplicate_record_check=checkMultipleDuplicatesEdit($this->companyConnection, '','','','','',$_POST['vehicle_vin'],$_POST['vehicle_id']);
            if(isset($duplicate_record_check)){
                if($duplicate_record_check['code'] == 503){
                    echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                    die();
                }
            }
            $id = $_POST['vehicle_id'];
            try {

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE tenant_vehicles SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'table' => "tenant_vehicle", 'message' => 'Record updated successfully.');

            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'table' => "users");
                printErrorLog($e->getMessage());
            }


        } else {
            $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, '','','','','',$_POST['vehicle_vin']);
            if(isset($duplicate_record_check)){
                if($duplicate_record_check['code'] == 503){
                    echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                    die();
                }
            }
            try {

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_vehicles(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_vechcle');
            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_vechile');
                printErrorLog($e->getMessage());
            }

        }

    }

    public function addEditPet()
    {
        $petName = $_POST['pet_name'];
        $data['user_id'] = $_POST['tenant_id'];
        $data['name'] = $_POST['pet_name'];
        $data['pet_id'] = $_POST['pet_id'];
        $data['type'] = $_POST['pet_type'];
        $dob = $_POST['pet_birth'];
        $data['dob'] = mySqlDateFormat($dob, null, $this->companyConnection);
        $data['age'] = $_POST['pet_age'];
        $data['gender'] = $_POST['pet_gender'];

        if ($_POST['pet_weight'] == "") {
            $data['weight'] = 0;
        } else {
            $data['weight'] = $_POST['pet_weight'];
        }

        if ($_POST['pet_weight_unit'] == "") {
            $data['weight_unit'] = 0;
        } else {
            $data['weight_unit'] = $_POST['pet_weight_unit'];
        }


        $data['note'] = $_POST['pet_note'];


        $data['color'] = $_POST['pet_color'];
        $data['chip_id'] = $_POST['pet_chipid'];
        $data['hospital_name'] = $_POST['pet_vet'];
        $data['phone_number'] = $_POST['pet_phoneNumber'];


        $next_visit = $_POST['pet_nextVisit'];
        $last_visit = $_POST['pet_lastVisit'];

        $image1 = json_decode($_POST['pet_image1']);
        $image2 = json_decode($_POST['pet_image2']);
        $image3 = json_decode($_POST['pet_image3']);


        $data['image1'] = $image1;
        $data['image2'] = $image2;
        $data['image3'] = $image3;


        $data['next_visit'] = mySqlDateFormat($next_visit, null, $this->companyConnection);
        $data['last_visit'] = mySqlDateFormat($last_visit, null, $this->companyConnection);


        $data['medical_condition'] = $_POST['pet_medical'];
        if ($data['medical_condition'] == '1') {
            $data['medical_condition_note'] = $_POST['pet_medical_condition_note'];
        } else {
            $data['medical_condition_note'] = "";
        }

        $data['shots'] = $_POST['pet_shots'];

        if ($data['shots'] == '1') {

            $data['shots_name'] = $_POST['pet_name_shot'];
            $shots_given_date = $_POST['pet_date_given'];
            $shots_expire_date = $_POST['pet_expiration_date'];
            $shots_followup_date = $_POST['pet_follow_up'];
            $data['shots_given_date'] = mySqlDateFormat($shots_given_date, null, $this->companyConnection);
            $data['shots_expire_date'] = mySqlDateFormat($shots_expire_date, null, $this->companyConnection);
            $data['shots_followup_date'] = mySqlDateFormat($shots_followup_date, null, $this->companyConnection);
            $data['shots_note'] = $_POST['pet_note'];
        }


        $data['rabies'] = $_POST['pet_rabies'];

        if ($data['rabies'] == '1') {

            $data['rabies_name'] = $_POST['pet_rabies_name'];
            $rabies_given_date = $_POST['pet_rabies_given_date'];
            $rabies_expire_date = $_POST['pet_rabies_expiration_date'];

            $data['rabies_given_date'] = mySqlDateFormat($rabies_given_date, null, $this->companyConnection);
            $data['rabies_expire_date'] = mySqlDateFormat($rabies_expire_date, null, $this->companyConnection);
            $data['rabies_note'] = $_POST['pet_rabies_note'];

        }


        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_at'] = date('Y-m-d H:i:s');


        if ($_POST['pet_action'] == "edit") {
            try {


                $id = $_POST['pet_unique_id'];
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE tenant_pet SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'table' => "tenant_pets", 'message' => "Record updated successfully");
            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_pet');
                printErrorLog($e->getMessage());
            }
        } else {
            try {
                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO tenant_pet(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'table' => "tenant_pets", 'message' => "Record added successfully");

            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_pet');
                printErrorLog($e->getMessage());
            }
        }

    }

    public function addEditService()
    {


        $data['user_id'] = $_POST['tenant_id'];
        $data['name'] = $_POST['service_name'];
        $data['animal_id'] = $_POST['service_id'];
        $data['type'] = $_POST['service_type'];
        $dob = $_POST['service_birth'];
        $data['dob'] = mySqlDateFormat($dob, null, $this->companyConnection);


        if ($_POST['service_year'] == '') {
            $data['age'] = 0;
        } else {
            $data['age'] = $_POST['service_year'];
        }


        $data['gender'] = $_POST['service_gender'];

        if ($_POST['service_weight'] == '') {
            $data['weight'] = 0;
        } else {
            $data['weight'] = $_POST['service_weight'];
        }


        $data['weight_unit'] = $_POST['service_weight_unit'];


        $data['note'] = $_POST['service_note'];
        $data['color'] = $_POST['service_color'];
        $data['chip_id'] = $_POST['service_chipid'];
        $data['hospital_name'] = $_POST['service_vet'];
        $data['phone_number'] = $_POST['service_countryCode'];

        $next_visit = $_POST['service_nextVisit'];
        $last_visit = $_POST['service_lastVisit'];


        $data['next_visit'] = mySqlDateFormat($next_visit, null, $this->companyConnection);
        $data['last_visit'] = mySqlDateFormat($last_visit, null, $this->companyConnection);


        $data['medical_condition'] = $_POST['service_medical'];
        if ($data['medical_condition'] == '1') {
            $data['medical_condition_note'] = $_POST['service_medical_condition_note'];
        } else {
            $data['medical_condition_note'] = "";
        }

        $data['shots'] = $_POST['service_shots'];

        if ($data['shots'] == '1') {

            $data['shots_name'] = $_POST['service_name_shot'];

            $shots_given_date = $_POST['service_date_given'];
            $shots_expire_date = $_POST['service_expiration_date'];
            $shots_followup_date = $_POST['service_follow_up'];

            $data['shots_given_date'] = mySqlDateFormat($shots_given_date, null, $this->companyConnection);
            $data['shots_expire_date'] = mySqlDateFormat($shots_expire_date, null, $this->companyConnection);
            $data['shots_followup_date'] = mySqlDateFormat($shots_followup_date, null, $this->companyConnection);


            $data['shots_note'] = $_POST['service_note'];
        }


        $data['rabies'] = $_POST['service_rabies'];

        if ($data['rabies'] == '1') {

            $data['rabies_name'] = $_POST['animal_rabies'];
            $rabies_given_date = $_POST['animal_date_given'];
            $rabies_expire_date = $_POST['animal_expiration_date'];

            $data['rabies_given_date'] = mySqlDateFormat($rabies_given_date, null, $this->companyConnection);
            $data['rabies_expire_date'] = mySqlDateFormat($rabies_expire_date, null, $this->companyConnection);


            $data['rabies_note'] = $_POST['animal_rabies'];

        }
        $image1 = json_decode($_POST['service_image1']);
        $image2 = json_decode($_POST['service_image2']);
        $image3 = json_decode($_POST['service_image3']);

        $data['image1'] = $image1;
        $data['image2'] = $image2;
        $data['image3'] = $image3;

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');


        if ($_POST['service_action'] == "edit") {
            try {


                $id = $_POST['service_unique_id'];
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE tenant_service_animal SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'table' => "tenant_service_animal", 'message' => "Record updated successfully");
            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_service_animal');
                printErrorLog($e->getMessage());
            }
        } else {
            try {
                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO tenant_service_animal(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'table' => "tenant_service_animal", 'message' => "Record added successfully");
            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_service_animal');
                printErrorLog($e->getMessage());
            }
        }


    }

    public function addEditMedical()
    {

        $data['user_id'] = $_POST['tenant_id'];
        $medical_issue = $_POST['medical_issue'];
        $data['allergy'] = $_POST['medical_issue'];
        $data['note'] = $_POST['medical_note'];


        $data['date'] = mySqlDateFormat($_POST['medical_date'], null, $this->companyConnection);

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');


        if ($_POST['medical_action'] == "edit") {
            $id = $_POST['medical_id'];
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE tenant_medical_allergies SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'table' => "tenant_medical_allergies", 'message' => 'Record updated successfully.',);

        } else {
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO tenant_medical_allergies(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('status' => 'success', 'message' => 'Data Added Successfully.', 'table' => 'tenant_medical_allergies');
        }


    }

    public function addEditCollection()
    {

        $data['user_id'] = $_POST['tenant_id'];
        $data['collection_id'] = $_POST['collection_collectionId'];
        $data['reason'] = $_POST['collection_reason'];
        $data['description'] = $_POST['collection_description'];
        $data['status'] = $_POST['collection_status'];
        $data['amount_due'] = $_POST['collection_amountDue'];
        $data['notes'] = $_POST['collection_amountDue'];

        if ($_POST['collection_action'] == "edit") {

            $id = $_POST['collection_id'];
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE tenant_collection SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'table' => "tenant_collection", 'message' => 'Record updated successfully');
        } else {

            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO tenant_collection(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);

            return array('status' => 'success', 'message' => 'Data Retrieved Successfully.', 'table' => 'tenant_collection', 'message' => 'Record added successfully');
        }


    }

    public function addEditParking()
    {
        $id = $_POST['tenant_id'];
        $data['user_id'] = $_POST['tenant_id'];
        $data['permit_number'] = $_POST['parking_number'];
        $data['space_number'] = $_POST['parking_space'];
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        if ($_POST['parking_action'] == "edit") {
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE tenant_parking SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('status' => 'success', 'message' => 'Data Updated Successfully.', 'table' => 'tenant_parking');
        } else {
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO tenant_parking(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('status' => 'success', 'message' => 'Data Added Successfully.', 'table' => 'tenant_parking');
        }


    }

    public function addEditGuarantorForm2()
    {


        $random = uniqid();

        try {
            $data['user_id'] = $_POST['tenant_id'];
            $data['company_name'] = $_POST['guarantor_form2_entity'];
            $data['is_guarantor_company'] = 1;
            $data['zip_code'] = $_POST['guarantor_form2_postalcode'];
            $data['country'] = $_POST['guarantor_form2_country'];
            $data['state'] = $_POST['guarantor_form2_province'];
            $data['city'] = $_POST['guarantor_form2_city'];
            $data['address1'] = $_POST['guarantor_form2_address1'];
            $data['address2'] = $_POST['guarantor_form2_address2'];
            $data['address3'] = $_POST['guarantor_form2_address3'];
            $data['address4'] = $_POST['guarantor_form2_address4'];
            $data['relationship'] = $_POST['guarantor_form2_relationship'];
            $data['mc_first_name'] = $_POST['guarantor_form2_mainContact'];
            $data['mc_mi'] = $_POST['guarantor_form2_middlename'];
            $data['mc_last_name'] = $_POST['guarantor_form2_lastname'];
            $data['note'] = $_POST['guarantor_form2_note'];
            $data['guarantee_years'] = $_POST['guarantor_form2_guarantee'];
            if (isset($_FILES['guarantor_form2_files']['name'])) {
                $tmp_name = $_FILES["guarantor_form2_files"]["tmp_name"];
                $data['file_name'] = $random . $_FILES['guarantor_form2_files']['name'];
                move_uploaded_file($tmp_name, ROOT_URL . "/company/uploads/tenant_files/" . $data['file_name']);

            } else {
                $data['file_name'] = "";
            }

            if (isset($_POST['guarantor_form2_email'])) {

                $guarantor_email = $_POST['guarantor_form2_email'];


                $emailCount = count($guarantor_email);


                if ($emailCount == 3) {
                    $email1 = $guarantor_email[0];
                    $email2 = $guarantor_email[1];
                    $email3 = $guarantor_email[2];
                } else if ($emailCount == 2) {
                    $email1 = $guarantor_email[0];
                    $email2 = $guarantor_email[1];
                    $email3 = "";
                } else if ($emailCount == 1) {
                    $email1 = $guarantor_email[0];
                    $email2 = "";
                    $email3 = "";
                } else {
                    $email1 = "";
                    $email2 = "";
                    $email3 = "";
                }
                $data['email1'] = $email1;
                $data['email2'] = $email2;
                $data['email3'] = $email3;
            }

            $tenant_id = $_POST['tenant_id'];
            if ($_POST['main_guarantor_action'] == 'edit') {
                $id = $_POST['guarantor_id'];
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE tenant_guarantor SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $message = 'Record Updated Succesfully';


            } else {
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_guarantor(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                $k = 0;

                $count = $this->companyConnection->prepare("DELETE FROM tenant_phone WHERE  parent_id=$tenant_id and user_id=$id");
                $count->execute();
                $message = 'Record Inserted Succesfully';
            }

            if (isset($_POST['guarantor_form2_phoneType'])) {


                $k = 0;

                foreach ($_POST['guarantor_form2_phoneType'] as $guarantor_form2_phoneType) {
                    $data1['user_id'] = $id;  /*$id is id of guarantor */
                    $data1['parent_id'] = $_POST['tenant_id'];
                    $data1['phone_type'] = $_POST['guarantor_form2_phoneType'][$k];
                    $data1['carrier'] = $_POST['guarantor_form2_carrier'][$k];

                    $data1['phone_number'] = $_POST['guarantor_form2_phoneNumber'][$k];
                    $data1['user_type'] = 2; //2 value for guarantor

                    $sqlData1 = createSqlColVal($data1);

                    $query1 = "INSERT INTO tenant_phone(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute($data1);
                    $k++;
                }
            }
            return array('status' => 'success', 'message' => $message, 'table' => 'tenant_guarantor');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_guarantor');
            printErrorLog($e->getMessage());
        }
    }

    public function addEditGuarantorForm1()
    {


        try {

            if (isset($_POST['guarantor_email'])) {


                $guarantor_email = $_POST['guarantor_email'];
                $emailCount = count($guarantor_email);


                if ($emailCount == 3) {
                    $email1 = $guarantor_email[0];
                    $email2 = $guarantor_email[1];
                    $email3 = $guarantor_email[2];
                } else if ($emailCount == 2) {
                    $email1 = $guarantor_email[0];
                    $email2 = $guarantor_email[1];
                    $email3 = "";
                } else {
                    $email1 = $guarantor_email[0];
                    $email2 = "";
                    $email3 = "";
                }
                $data['email1'] = $email1;
                $data['email2'] = $email2;
                $data['email3'] = $email3;
            }


            $data['user_id'] = $_POST['tenant_id'];
            $data['salutation'] = $_POST['guarantor_salutation'];
            $data['first_name'] = $_POST['guarantor_firstname'];
            $data['middle_name'] = $_POST['guarantor_middlename'];
            $data['last_name'] = $_POST['guarantor_lastname'];
            $data['relationship'] = $_POST['guarantor_relationship'];
            $data['zip_code'] = $_POST['guarantor_zipcode'];
            if (isset($_POST['guarantor_country'])) {
                $data['country'] = $_POST['guarantor_country'];
            }

            $data['state'] = $_POST['guarantor_province'];
            $data['city'] = $_POST['guarantor_city'];
            $data['address1'] = $_POST['guarantor_address1'];
            $data['address2'] = $_POST['guarantor_address2'];
            $data['address3'] = $_POST['guarantor_address3'];
            $data['address4'] = $_POST['guarantor_address3'];
            $data['is_guarantor_company'] = 0;


            $data['guarantee_years'] = $_POST['guarantor_guarantee'];
            $data['note'] = $_POST['guarantor_note'];
            $data['entity_fid_number'] = $_POST['guarantor_ssn'];


            $tenant_id = $_POST['tenant_id'];
            if ($_POST['main_guarantor_action'] == 'edit') {
                $id = $_POST['guarantor_id'];
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE tenant_guarantor SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $message = 'Record Updated Succesfully';


            } else {
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO tenant_guarantor(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                $k = 0;


                $message = 'Record Inserted Succesfully';
            }

            $count = $this->companyConnection->prepare("DELETE FROM tenant_phone WHERE  parent_id=$tenant_id and user_id=$id");
            $count->execute();


            if (isset($_POST['guarantor_countryCode'])) {


                $k = 0;
                foreach ($_POST['guarantor_countryCode'] as $countryCode) {
                    $data1['user_id'] = $id;
                    $data1['parent_id'] = $tenant_id;
                    $data1['phone_type'] = $_POST['guarantor_phoneType'][$k];
                    $data1['carrier'] = $_POST['guarantor_carrier'][$k];
                    $data1['country_code'] = $_POST['guarantor_countryCode'][$k];
                    $data1['phone_number'] = $_POST['guarantor_phone'][$k];
                    $data1['user_type'] = 2; //2 value for guarantor

                    $sqlData1 = createSqlColVal($data1);

                    $query1 = "INSERT INTO tenant_phone(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute($data1);
                    $k++;
                }
            }
            return array('status' => 'success', 'message' => $message, 'table' => 'tenant_guarantor');
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_guarantor');
            printErrorLog($e->getMessage());

        }

    }

    public function getComplaintInfo()
    {
        $id = $_REQUEST['id'];
        $complaintInfo = $this->companyConnection->query("SELECT * FROM complaints WHERE id ='" . $id . "'")->fetch();
        $data['complaint'] = $complaintInfo['complaint_by_about'];
        $data['complaint_id'] = $complaintInfo['complaint_id'];
        $data['notes'] = $complaintInfo['complaint_note'];
        $data['type'] = $complaintInfo['complaint_type_id'];
        $data['date'] = dateFormatUser($complaintInfo['complaint_date'], null, $this->companyConnection);
        $data['updated'] = $complaintInfo['updated_at'];
        return $data;

    }

    public function addEditComplaint()
    {
        $data['user_id'] = $_POST['tenant_id'];
        $data['object_id'] = $_POST['tenant_id'];
        $data['complaint_by_about'] = $_POST['complaint'];
        $data['complaint_id'] = $_POST['complaint_id'];
        $data['complaint_note'] = $_POST['complaint_note'];
        $data['complaint_type_id'] = $_POST['complaint_type'];
        $data['complaint_date'] = mySqlDateFormat($_POST['complaint_date'], null, $this->companyConnection);


        if ($_POST['complaint_action'] == "edit") {
            try {


                $id = $_POST['complaint_unique_id'];
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE complaints SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'table' => "complaints", "message" => "Data Updated Successfully");
            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'complaints');
                printErrorLog($e->getMessage());
            }
        } else {
            try {
                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO complaints(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'table' => "complaints", "message" => 'Data Added Successfully');
            } catch (PDOException $e) {
                return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'complaints');
                printErrorLog($e->getMessage());
            }
        }
    }

    public function getComplaintsData()
    {
        $tenant_id = $_POST['id'];
        $tenantData = $this->companyConnection->query("SELECT * FROM users WHERE id ='" . $tenant_id . "'")->fetch();
        $tenantName = $tenantData['name'];
        $address = $tenantData['address1'];
        //delete
        try {

            $complaint_ids = $_POST['complaint_ids'];
            $complaint_idss = str_repeat('?,', count($complaint_ids) - 1) . '?';
            $stmt = $this->companyConnection->prepare("Select * FROM `complaints` as c inner join complaint_types as ct on ct.id=c.type
            WHERE c.id IN ($complaint_idss)");
            $stmt->execute($complaint_ids);
            $data = $stmt->fetchAll();
            $print_complaints_html = '';
            $company_logo = SITE_URL . "company/images/logo.png";
            foreach ($data as $key => $value) {
                $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
    <tr>
      <td style="background: #00b0f0; height: 30px;">
        
      </td>
    </tr>
    <tr>
      <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
        <img width="200" src="' . $company_logo . '"/>
      </td>
    </tr>
    <tr>
      <td style="background: #00b0f0; height: 30px;">
        
      </td>
    </tr>
    <tr>
      <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
        <table width="100%" align="center" cellspacing="0" cellpadding="0">
          <tr>
            <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> Complaint by Building </td>
          </tr>
        </table>
        <table width="100%" align="center" border="1" cellspacing="0" cellpadding="5">
          <tr>
            <td>Tenant Name: </td>
            <td>' . $tenantName . '</td>
          </tr>
          <tr>
            <td>Address: </td>
            <td>' . $address . '</td>
          </tr>
          <tr>
            <td>Complaint Type: </td>
            <td>' . $value["complaint_type"] . '</td>
          </tr>
          <tr>
            <td>Description: </td>
            <td>' . $value['notes'] . '</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="background: #585858; color: #fff; padding: 10px; text-align: center;">
        Apexlink.apexlink@yopmail.com
        
      </td>
    </tr>
  </table>';
            }


            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function getImageByTable(){
        $id = $_REQUEST['id'];
        $tableName = $_REQUEST['tablename'];
        if ($tableName == "tenant_vehicles") {
            $allImages = 'photo1 as img1,photo2 as img2,photo3 as img3';
        } else if ($tableName == "tenant_pet") {
            $allImages = 'image1 as img1,image2 as img2,image3 as img3';
        } else if ($tableName == "tenant_service_animal") {
            $allImages = 'image1 as img1,image2 as img2,image3 as img3';
        } else if ($tableName == "hoa_violation") {
            $allImages = 'upload_pic_1 as img1,upload_pic_2 as img2,upload_pic_3 as img3';
        } else if ($tableName == "tenant_maintenance") {
            $allImages = 'image1 as img1,image2 as img2,image3 as img3';
        }

        $getData = $this->companyConnection->query("SELECT $allImages FROM $tableName WHERE id ='" . $id . "'")->fetch();
        $userName = $this->companyConnection->query("SELECT `user_id` FROM $tableName WHERE id ='" . $id . "'")->fetch();
        $data = [];
        $data['getData'] = $getData;
        $data['userName'] = userName($userName['user_id'], $this->companyConnection);
        return $data;
    }

    public function updateAdditionalInfoTenant()
    {
        $tenant_id = $_POST['tenant_id'];
        if (isset($_POST['birth']) && $_POST['birth'] != "") {
            $data['dob'] = mySqlDateFormat($_POST['birth'], null, $this->companyConnection);
        }

        $data['gender'] = $_POST['additionalGender'];
        $data['updated_at'] = date('Y-m-d H:i:s');
        $sqlData = createSqlColValPair($data);
        $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" . $tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();


        $data1['tenant_license_state'] = $_POST['additionalLicense_state'];
        $data1['tenant_license_number'] = $_POST['additionalLicense'];
        $data1['movein_key_signed'] = $_POST['keys_signed'];
        $data1['vehicle'] = $_POST['vehicle'];
        $data1['smoker'] = $_POST['smoker'];
        $data1['pet'] = $_POST['pet'];
        $data1['medical_allergy'] = $_POST['medical'];


        $sqlData1 = createSqlColValPair($data1);
        $query1 = "UPDATE tenant_details SET " . $sqlData1['columnsValuesPair'] . " where user_id=" . $tenant_id;
        $stmt1 = $this->companyConnection->prepare($query1);
        $stmt1->execute();


        return array('code' => 200, 'status' => 'success');


    }

    public function sendEmailTemplate()
    {

        try {
            $userId = $_POST['tenant_id'];
            $resetPasswordToken = rand();


            $query = "UPDATE users SET portal_password_key='$resetPasswordToken',portal_status='1' where id=" . $userId;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();


            $server_name = 'https://' . $_SERVER['HTTP_HOST'];
            $userId = (isset($userId) ? $userId : $_REQUEST['id']);
            $user_details = $this->companyConnection->query("SELECT * FROM users where id=" . $userId)->fetch();
            $user_name = $user_details['first_name'] . ' ' . $user_details['last_name'];
            $user_name = userName($user_details['id'], $this->companyConnection);
            if (isset($_POST['temp_key']) && $_POST['temp_key'] == "newVendorWelcome_Key") {
                $body = getEmailTemplateData($this->companyConnection, "newVendorWelcome_Key");
            } else {
                $body = getEmailTemplateData($this->companyConnection, "newTenantWelcome_Key");
            }


            //dd($body);

            $body = str_replace("{Username}", $user_name, $body);
            $body = str_replace("{Firstname}", $user_name, $body);
            $body = str_replace("{email}", $_POST['sendEmail'], $body);
            $body = str_replace("{password}", $user_details['actual_password'], $body);
            $imgtag = "<a href='#'><img src='" . SITE_URL . '/company/images/logo.png' . "' style='height:50px;width:50px;'></a>";
            $body = str_replace("{CompanyLogo}", $imgtag, $body);
            $urlHtml = SUBDOMAIN_URL . "/TenantPortal/changePassword?tenant_id=$userId&secretkey=$resetPasswordToken";


            //dd($urlHtml);
            $body = str_replace("{Url}", $urlHtml, $body);

            $request['action'] = 'SendMailPhp';
            $request['to[]'] = $_POST['sendEmail'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            //dd($request['message']);
            $request['portal'] = '1';
            curlRequest($request);

            return ['status' => 'success', 'code' => 200, 'data' => $request, 'message' => 'Email send successfully'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'data' => $exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

    }






}


$UnitTypeAjax = new EditTenant();