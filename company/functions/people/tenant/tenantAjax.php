<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class TenantAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getIntialData(){

        try{
            $data = [];
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['propertylist'] =$this->companyConnection->query("SELECT id,property_id, property_name FROM general_property WHERE is_short_term_rental != 1 OR is_short_term_rental IS NULL ORDER BY property_name ASC")->fetchAll();
            $data['phone_type'] =$this->companyConnection->query("SELECT id,type FROM phone_type")->fetchAll();
            $data['carrier'] = $this->companyConnection->query("SELECT id,carrier FROM carrier")->fetchAll();
            $data['referral'] = $this->companyConnection->query("SELECT id,referral FROM tenant_referral_source")->fetchAll();
            $data['ethnicity'] = $this->companyConnection->query("SELECT id,title FROM tenant_ethnicity where title != 'Other'order by title asc")->fetchAll();
            array_push($data['ethnicity'],array("id"=>9,"title" =>"Other"));
            $data['hobbies'] = $this->companyConnection->query("SELECT * FROM hobbies")->fetchAll();
            $data['marital'] = $this->companyConnection->query("SELECT id,marital FROM tenant_marital_status")->fetchAll();
            $data['veteran'] = $this->companyConnection->query("SELECT id,veteran FROM tenant_veteran_status")->fetchAll();
            $data['collection_reason'] = $this->companyConnection->query("SELECT id,reason FROM tenant_collection_reason")->fetchAll();
            $data['credential_type'] = $this->companyConnection->query("SELECT id,credential_type FROM tenant_credential_type")->fetchAll();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
            $data['state'] = $this->companyConnection->query("SELECT state FROM `users` WHERE id=$id")->fetch();
            $data['complaints'] = $this->companyConnection->query("SELECT id,complaint_type FROM `complaint_types`")->fetchAll();
            $data['petgender'] = $this->companyConnection->query("SELECT id,gender FROM `tenant_pet_gender`")->fetchAll();
            $data['animalgender'] = $this->companyConnection->query("SELECT id,gender FROM `tenant_service_animal_gender`")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    
    public function getRentInfo(){

        try{
            $data = [];
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $tenantId = $_POST['id'];
            $unitId =$this->companyConnection->query("SELECT unit_id FROM tenant_property WHERE user_id=$tenantId")->fetch();
            $unitId = $unitId['unit_id'];
            $data['unit'] =$this->companyConnection->query("SELECT base_rent,market_rent,security_deposit FROM unit_details WHERE id=$unitId")->fetch();
            $data['currency'] = $_SESSION[SESSION_DOMAIN]['default_currency_symbol'];
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getAlphabetName(){

        try{
            $data = [];
            $teantntId =$this->companyConnection->query("SELECT * FROM users WHERE user_type='2'")->fetchAll();
            $array = [];
            foreach ($teantntId as $key => $value) {
                $array[] = $value['first_name']."---".$value['record_status'];
            }

            return ['code'=>200, 'status'=>'success', 'data'=>$array];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getUnitById(){

        try{
            $unitId = $_POST['id'];
            $unitId =$this->companyConnection->query("SELECT unit_prefix,unit_no FROM unit_details WHERE id=$unitId")->fetch();
            return ['code'=>200, 'status'=>'success', 'data'=>$unitId];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getUserNameById(){

        try{
            $userId = $_POST['id'];
            $return = userName($userId, $this->companyConnection,'users');
            return ['code'=>200, 'status'=>'success', 'data'=>ucwords($return)];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getPropertyPopUpData(){

        try{
            $data = [];

            $data['portfolio'] =$this->companyConnection->query("SELECT id,portfolio_name FROM company_property_portfolio")->fetchAll();
            $data['property_type'] =$this->companyConnection->query("SELECT id,property_type FROM company_property_type WHERE status='1'")->fetchAll();
            $data['manager'] =$this->companyConnection->query("SELECT id,first_name,last_name FROM users WHERE status='1' AND user_type='1' AND role='2'")->fetchAll();
            $data['group'] = $this->companyConnection->query("SELECT id,group_name FROM company_property_groups")->fetchAll();
            $data['style'] = $this->companyConnection->query("SELECT id,property_style FROM company_property_style")->fetchAll();
            $data['subtype'] = $this->companyConnection->query("SELECT id,property_subtype FROM company_property_subtype")->fetchAll();
            $data['amenity'] = $this->companyConnection->query("SELECT id,name FROM company_property_amenities WHERE status= '1'")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getBuildingPopUpData(){

        try{
            $data = [];

            /*$data['portfolio'] =$this->companyConnection->query("SELECT id,portfolio_name FROM company_property_portfolio")->fetchAll();
            $data['property_type'] =$this->companyConnection->query("SELECT id,property_type FROM company_property_type WHERE status='1'")->fetchAll();
            $data['manager'] =$this->companyConnection->query("SELECT id,first_name,last_name FROM users WHERE status='1'")->fetchAll();
            $data['group'] = $this->companyConnection->query("SELECT id,group_name FROM company_property_groups")->fetchAll();*/
            $data['amenity'] = $this->companyConnection->query("SELECT id,name FROM company_property_amenities WHERE status= '1'")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getUnitPopUpData(){

        try{
            $data = [];
            $buildingId = $_POST['buildingId'];
            $data['building'] = $this->companyConnection->query("SELECT smoking_allowed,pet_friendly FROM building_detail WHERE id= $buildingId")->fetch();
			$data['market'] = $_SESSION[SESSION_DOMAIN]['default_rent'];
            $data['unitType'] = $this->companyConnection->query("SELECT id,unit_type FROM company_unit_type WHERE status= '1'")->fetchAll();
            $data['amenity'] = $this->companyConnection->query("SELECT id,name FROM company_property_amenities WHERE status= '1'")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function saveTenantLease(){

        try{
            $data = $_POST['form'];
            $data = postArray($data);

            //$required_array = ['move_in_date', 'start_date', 'lease_term', 'end_date','notice_date','rent_amount'];
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $data1['user_id'] = $data['lease_user_id'];
                $data1['days_remaining'] = mySqlDateFormat($data['end_date1'], $id = null,$this->companyConnection);
                $data1['balance'] = '1000';
                $data1['user_id'] = $data['lease_user_id'];
                $data1['move_in'] = mySqlDateFormat($data['move_in_date'], $id = null,$this->companyConnection);
                $data1['move_out'] = mySqlDateFormat($data['move_out_date1'], $id = null,$this->companyConnection);
                $data1['start_date'] =mySqlDateFormat($data['start_date1'], $id = null,$this->companyConnection);
                $data1['end_date'] =mySqlDateFormat($data['end_date1'], $id = null,$this->companyConnection);
                $data1['term'] = $data['lease_term'];
                $data1['tenure'] = $data['lease_tenure'];
                $data1['notice_period'] = $data['notice_period'];
                $data1['notice_date'] = mySqlDateFormat($data['notice_date'], $id = null,$this->companyConnection);
                $data1['rent_due_day'] = str_replace(",", "", $data['rent_due_day']);
                $data1['rent_amount'] =str_replace(",", "", $data['rent_amount']);
                $data1['cam_amount'] = str_replace(",", "", $data['cam_amount']);
                $data1['security_deposite'] = $data['security_amount'];
                $data1['next_rent_incr'] = $data['next_rent_incr'];
                if(isset($data['incr_by'])!="")
                {
                    $data1['flat_perc'] = $data['incr_by'];
                }
                $data1['amount_incr'] = $data['increase_amount'];
                $data1['record_status'] = '0';
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');

                dd($data1);
                //Save Data in Company Database
                $sqlData = createSqlColVal($data1);

                $query = "INSERT INTO `tenant_lease_details` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);

                $stmt->execute($data1);
                $leaseData = $data;
                $leaseData['status'] = '1';
                $leaseData['id'] = $this->companyConnection->lastInsertId();
                $ElasticSearchSave = insertDocument('LEASE','ADD',$leaseData,$this->companyConnection);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record saved successfully');
            }

        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function saveTenantMoveIn(){

        try{
            $data = $_POST['form'];
            $data = postArray($data);

            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data1['user_id'] = $data['user_id'];
                $data1['scheduled_move_in'] = mySqlDateFormat($data['scheduled_move_in'], $id = null,$this->companyConnection);
                $data1['actual_move_in'] = mySqlDateFormat($data['actual_move_in'], $id = null,$this->companyConnection);
                $data1['paid_amount'] = $data['paid_amount'];
                if(isset($data['check_no'])){
                    $data1['check_no'] = $data['check_no'];
                }
                $data1['prorated_amount'] = $data['prorated_amount'];
                $data1['memo'] =$data['memo'];
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                if (isset($data['id_from_other']) && $data['id_from_other'] != ""){
                    $rental_id=$data['id_from_other'];
                    $this->updateRentalStatus($rental_id);
                }
                //Save Data in Company Database
                $sqlData = createSqlColVal($data1);

                $query = "INSERT INTO `tenant_move_in` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);

                /*if(isset( $_GET['id']) && !empty($_GET['id']))*/

                $stmt->execute($data1);
                $this->updateTenantRecordStatus($data['user_id']);
                $this->saveTenantMoveInRecord($data['user_id']);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Tenant added successfully.');
            }

        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function saveTenantMoveInRecord($userid){
        try{
            $data1['user_id'] = $userid;
            $data1['status'] = '4';//0=>pending,1=>active,2=>decline,4=>approved
            $data1['record_status'] = '1';
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $data1['deleted_at'] = null;
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO `movein_list_record` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            $data1['id'] = $this->companyConnection->lastInsertId();
            return ['code'=>200, 'status'=>'success', 'data'=>$data1,'message'=> 'Tenant moved-in successfully.'];
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Tenant not saved in move-in.'];
        }
    }

    public function savePopUpData(){

        try{
            $column = $_POST['colName'];
            $tableName = $_POST['tableName'];
            $value = $_POST['val'];
            $query = "SELECT * FROM `$tableName` WHERE $column = '$value'";
            $checkIfExist = $this->companyConnection->query($query)->fetch();

            if ($checkIfExist !== false){
                return array('code' => 504, 'status' => 'error','message' => 'Record already exist!');
            }

            $data[$_POST['colName']] = $_POST['val'];
            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO `$tableName` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);

            $stmt->execute($data);
            $data1['col_val'] = $_POST['val'];
            $data1['last_insert_id'] = $this->companyConnection->lastInsertId();
            return array('code' => 200, 'status' => 'success', 'data' => $data1,'message' => 'Record added successfully.');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }
    
    public function savePopUpData3(){

       try{
           $column = $_POST['colName'];
           $tableName = $_POST['tableName'];
           $value = $_POST['val'];
           $query = "SELECT * FROM `$tableName` WHERE $column = '$value'";
           $checkIfExist = $this->companyConnection->query($query)->fetch();

           if ($checkIfExist !== false){
               return array('code' => 504, 'status' => 'error','message' => 'Record already exist!');
           }

           $data[$_POST['colName']] = $_POST['val'];
           $data['user_id'] = '1';
           $data['status'] = '1';
           $sqlData = createSqlColVal($data);

           $query = "INSERT INTO `$tableName` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
           $stmt = $this->companyConnection->prepare($query);

           $stmt->execute($data);
           $data1['col_val'] = $_POST['val'];
           $data1['last_insert_id'] = $this->companyConnection->lastInsertId();
           return array('code' => 200, 'status' => 'success', 'data' => $data1,'message' => 'Record added successfully.');
       }catch (Exception $exception){
           return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
           printErrorLog($exception->getMessage());
       }
   }

    public function saveDoublePopUpData(){

        try{
            $tableName = $_POST['tableName'];
            $column = $_POST['colName'];
            $value = $_POST['val'];
            $firstVal = $value[0];
            $secondVal = $value[1];
            $explodeColumn = explode(",",$column);
            $firstCol = $explodeColumn[0];
            $secondCol = $explodeColumn[1];

            $query = "SELECT * FROM `$tableName` WHERE $firstCol = '$firstVal' OR $secondCol = '$secondVal'";
            $checkIfExist = $this->companyConnection->query($query)->fetch();
            if ($checkIfExist !== false){
                return array('code' => 504, 'status' => 'error','message' => 'Record already exist!');
            }
            $data = [];
            for($initialize = 0;$initialize < count($explodeColumn); $initialize++){
                $data[$explodeColumn[$initialize]] = $value[$initialize];

            }

            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO `$tableName` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);

            $stmt->execute($data);
            $data1['col_val'] = $secondVal;
            $data1['last_insert_id'] = $this->companyConnection->lastInsertId();
            return array('code' => 200, 'status' => 'success', 'data' => $data1,'message' => 'Record added successfully.');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function saveDoublePopUpData2(){

        try{
            $tableName = $_POST['tableName'];
            $column = $_POST['colName'];
            $value = $_POST['val'];
            $explodeColumn = explode(",",$column);
            /*
            $query = "SELECT * FROM `$tableName` WHERE $firstCol = '$firstVal' OR $secondCol = '$secondVal'";
            $checkIfExist = $this->companyConnection->query($query)->fetch();
            if ($checkIfExist !== false){
                return array('code' => 504, 'status' => 'error','message' => 'Record already exist!');
            }*/
            $data = [];
            for($initialize = 0;$initialize < count($explodeColumn); $initialize++){
                $data[$explodeColumn[$initialize]] = $value[$initialize];

            }

            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO `$tableName` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);

            $stmt->execute($data);
            $data1['col_val'] = $value[0].' '.$value[1];
            $data1['last_insert_id'] = $this->companyConnection->lastInsertId();
            return array('code' => 200, 'status' => 'success', 'data' => $data1,'message' => 'Record added successfully.');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function updateTenantRecordStatus($user_id){
        try{
            $tableName = ['tenant_details', 'tenant_lease_details', 'tenant_notes', 'tenant_property', 'tenant_vehicles', 'tenant_charges'];
            foreach ($tableName as $val){
                $recordStatus['record_status'] = "1";
                $sqlData = createSqlColValPair($recordStatus);
                $query = "UPDATE $val SET ".$sqlData['columnsValuesPair']." where user_id='$user_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
            }
            $this->updateTenantRecordStatusUser($user_id);

            return array('code' => 200, 'status' => 'success', 'data' => $tableName, 'message' => 'Status has been updated!');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function updateTenantRecordStatusUser($user_id){
        try{
            $tableName = ['users'];
            foreach ($tableName as $val){
                $recordStatus['record_status'] = "0";
                $sqlData = createSqlColValPair($recordStatus);
                $query = "UPDATE $val SET ".$sqlData['columnsValuesPair']." where id=".$user_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $this->updateTenantUnitStatus($user_id);
            }

            return array('code' => 200, 'status' => 'success', 'data' => $tableName, 'message' => 'Status has been updated!');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function updateTenantUnitStatus($user_id){
        try{
            $query = "SELECT * FROM `tenant_property` WHERE user_id = ".$user_id;
            $unitData = $this->companyConnection->query($query)->fetch();
            if ($unitData !==  false){
                $id = $unitData['unit_id'];
                $recordStatus['building_unit_status'] = "4";
                $sqlData = createSqlColValPair($recordStatus);
                $query = "UPDATE `unit_details` SET ".$sqlData['columnsValuesPair']." where id=".$id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $this->sendNotification($user_id);
                return array('code' => 200, 'status' => 'success', 'message' => 'Unit status has been updated!');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function sendNotification($last_id){
        $notifiData = "SELECT u.name,tp.user_id,tp.property_id,tp.unit_id,tld.id,CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name,tld.start_date,tld.created_at FROM `tenant_property` as tp JOIN unit_details as ud ON ud.id = tp.unit_id JOIN tenant_lease_details as tld ON tld.user_id = tp.user_id JOIN users as u ON u.id = tp.user_id where ud.building_unit_status ='4' AND tp.user_id =".$last_id;
        $notifiquery = $this->companyConnection->query($notifiData)->fetch();
        $name = $notifiquery['name'];
        $leaseId = $notifiquery['id'];
        $tenantId = $notifiquery['user_id'];
        $propertyId=$notifiquery['property_id'];
        $unitname=$notifiquery['unit_name'];
        $startDate= dateFormatUser($notifiquery['start_date'] ,$last_id ,$this->companyConnection);
        $dateCreated = dateFormatUser($notifiquery['created_at'],$last_id,$this->companyConnection);

        /* lease notification */
        $notificationTitle= "Start Date";
        $module_type = "LEASE";
        $alert_type = "Real Time";
        $descriptionNotifi ='Notification of Lease Start Date for Tenant ID- '.$tenantId.'(PropertyID-'.$propertyId.') on '.$dateCreated;
        insertNotification($this->companyConnection,$last_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
        /* lease notification */

        /* lease notification */
            $notificationTitle= "Lease signed";
            $module_type = "LEASE";
            $alert_type = "Real Time";
            $descriptionNotifi ='New Lease (ID - '.$leaseId.'). has been generated for tenant ID-'.$tenantId.' in  PropertyID-'.$propertyId;
            insertNotification($this->companyConnection,$last_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
            /* lease notification */

        /* lease notification */
                $notificationTitle= "New tenant notification";
                $module_type = "LEASE";
                $alert_type = "Real Time";
                $descriptionNotifi ='New Tenant ID -'.$tenantId.' has been moved into(Name-'.$name.', Property - '.$tenantId.', Unit '.$unitname.') as of today.';
                insertNotification($this->companyConnection,$last_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
        /* lease notification */
    }

    public function getDateformat(){
        return dateFormatUser($_POST['date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
    }

    public function getBuilding(){
        $query = "SELECT id, building_name FROM `building_detail` WHERE property_id = ".$_POST['propertyID']." AND deleted_at IS NULL ORDER BY building_name ASC";
        $buildingData = $this->companyConnection->query($query)->fetchAll();
        return array('code' => 200, 'status' => 'success', 'data' => $buildingData,'message' => 'Building loaded successfully!');
    }

    public function getUnits(){
        $query = "SELECT id, unit_prefix, unit_no FROM `unit_details` WHERE property_id = ".$_POST['propertyID']." && building_id = ".$_POST['buildingID']." && building_unit_status = '1'";
        $unitData = $this->companyConnection->query($query)->fetchAll();
        $newArray = array();
        foreach ($unitData as $key => $value) {
            $query1 = "SELECT unit_id FROM `tenant_property` WHERE unit_id = ".$value['id'];
            $unitData1 = $this->companyConnection->query($query1)->fetch();    
            if ($unitData1) {
                continue;
            }
            $newArray[] = $value;
        }
        
        return array('code' => 200, 'status' => 'success', 'data' => $newArray,'message' => 'Units loaded successfully!');
    }

    public function getTenantRentInfo(){
        $query = "SELECT * FROM `tenant_lease_details` WHERE user_id = ".$_POST['id'];
        $rentData = $this->companyConnection->query($query)->fetch();
        return array('code' => 200, 'status' => 'success', 'data' => $rentData,'message' => 'Rent data loaded successfully!');
    }

    public function getTenantRentInfoMoveIn(){
        $query = "SELECT * FROM `tenant_lease_details` WHERE user_id = ".$_POST['id'];
        $rentData = $this->companyConnection->query($query)->fetch();
        $data = [];
        $data['move_in'] = dateFormatUser($rentData['move_in'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['actual_move_in'] = dateFormatUser(date('Y-m-d'), $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['move_out'] = dateFormatUser($rentData['move_out'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['start_date'] = dateFormatUser($rentData['start_date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['end_date'] = dateFormatUser($rentData['end_date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['notice_date'] = dateFormatUser($rentData['notice_date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['rent_amount'] = $rentData['rent_amount'];
        $date1=date_create(date('Y-m-d'));
        $date2=date_create($rentData['move_in']);
        $diff=date_diff($date1,$date2);
        $days = $diff->days;
       /* if ($days >0 ) {
            $dayRent = floor($rentData['rent_amount'] / $days);
            $data['pro_rent'] = $dayRent;
        }*/

        return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Rent data loaded successfully!');
    }

    public function getHoaInfo(){
        $query = "SELECT * FROM `hoa_violation_type`";
        $hoaData = $this->companyConnection->query($query)->fetchAll();
        return array('code' => 200, 'status' => 'success', 'data' => $hoaData,'message' => 'Rent data loaded successfully!');
    }

    public function getHoaData(){
        $rowId = $_POST['rowIds'];
        $query = "SELECT * FROM `hoa_violation` WHERE id=$rowId";
        $hoaData = $this->companyConnection->query($query)->fetch();
        $hoaData['date'] = dateFormatUser($hoaData['date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        return array('code' => 200, 'status' => 'success', 'data' => $hoaData,'message' => 'HOA data loaded successfully!');
    }


    public function saveTenantRentInfo(){

        try{
            $data = $_POST['form'];
            $data = postArray($data);

            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $recordStatus['rent_due_day'] = $data['edit_rent_due_date'];
                $recordStatus['rent_amount'] = $data['edit_rent_amount'];
                $recordStatus['cam_amount'] = $data['edit_cam_amount'];
                $recordStatus['security_deposite'] = $data['edit_security_amount'];
                $recordStatus['next_rent_incr'] = $data['edit_next_rent_inc'];
                $recordStatus['flat_perc'] = $data['edit_rent_incr'];
                $recordStatus['amount_incr'] = $data['edit_incr_amount'];
                $user_id = $data['edit_rent_user_id'];
                $sqlData = createSqlColValPair($recordStatus);
                $query = "UPDATE `tenant_lease_details` SET " . $sqlData['columnsValuesPair'] . " where user_id='$user_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
            }
            return array('code' => 200, 'status' => 'success', 'data' => $recordStatus, 'message' => 'Record has been updated!');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getChargeCodes(){

        try{
            $data = [];
            $data['debits'] = $this->companyConnection->query("SELECT id,debit_accounts FROM company_debit_accounts")->fetchAll();
            $data['credits'] = $this->companyConnection->query("SELECT id,credit_accounts FROM company_credit_accounts")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function downloadLeaseFile(){

        try{
            $id = $_POST['id'];

            $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/tenant_leaseGeneratePDF.php';
            $data = file_get_contents($fileUrls);

            $users =$this->companyConnection->query("SELECT first_name,last_name FROM users WHERE id=$id")->fetch();
            $propSql = "SELECT * FROM tenant_property WHERE user_id=".$id;
            $tproperty = $this->companyConnection->query($propSql)->fetch();
            $chars =$this->companyConnection->query("SELECT grace_period FROM tenant_charge WHERE user_id=$id")->fetch();
            $add = '';
            if ($tproperty != false){
                $propId = $tproperty['id'];
                $property =$this->companyConnection->query("SELECT city,state,country,zipcode,address1,address2,address3,address4 FROM general_property WHERE id=$propId")->fetch();

                if ($property['address1'] != ""){
                    $add .= $property['address1'];
                }
                if ($property['address2'] != ""){
                    $add .= ', '.$property['address2'];
                }
                if ($property['address3'] != ""){
                    $add .= ', '.$property['address3'];
                }
                if ($property['address4'] != ""){
                    $add .= ', '.$property['address4'];
                }
                if ($property['city'] != ""){
                    $add .= ', '.$property['city'];
                }
                if ($property['state'] != ""){
                    $add .= ', '.$property['state'];
                }
                if ($property['country'] != ""){
                    $add .= ', '.$property['country'];
                }
                if ($property['zipcode'] != ""){
                    $add .= ', '.$property['zipcode'];
                }
            }
            $lease =$this->companyConnection->query("SELECT start_date,end_date,notice_period,notice_date,rent_due_day,rent_amount,security_deposite FROM tenant_lease_details WHERE user_id=$id")->fetch();

            $replaceText = str_replace("TENANTNAME",$users['first_name']." ".$users['last_name'],$data);
            $replaceText = str_replace("LEASESTARTDATE",$lease['start_date'],$replaceText);
            $replaceText = str_replace("LEASEENDDATE",$lease['end_date'],$replaceText);
            $replaceText = str_replace("RENTBEFOREDAY",$lease['rent_due_day'],$replaceText);
            $replaceText = str_replace("RENTBEFOREDAY",$lease['rent_amount'],$replaceText);
            $replaceText = str_replace("GRACEPERIOD",$chars['grace_period'],$replaceText);
            $replaceText = str_replace("SECURITYDEPOSITE",$lease['security_deposite'],$replaceText);
            $replaceText = str_replace("NOTICEDAYS",$lease['notice_date'],$replaceText);
            $replaceText = str_replace("PROPERTYADDRESS",$add,$replaceText);
            $replaceText = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$replaceText);
            $replaceText = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$replaceText);
            //$fileUrl = COMPANY_DIRECTORY_URL . '/uploads/tenant_leaseGeneratePDF-'.$id.'.php';
            $fileUrl = COMPANY_DIRECTORY_URL . '/uploads/tenant_lease/generate_lease_files/tenant_leaseGeneratePDF-'.$id.'.php';
           file_put_contents($fileUrl,$replaceText);
            //chmod($fileUrl, 0777);
            $fileUrl2 = 'https://'.$_SERVER['HTTP_HOST'] . '/Tenantlisting/tenant_leaseGeneratePDFs.php?id='.$id;
            return ['code'=>200, 'status'=>'success', 'refurl'=>$fileUrl2];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function flagChangeOption() {
        try {
            $data=[];
            $recordid = $_POST['recordid'];
            $fieldType = $_POST['fieldType'];//getRecord

            if ($fieldType == "getRecord"){
                $data1['record'] = $this->companyConnection->query("SELECT * FROM flags WHERE id=$recordid")->fetch();
                $data1['record']['date'] = dateFormatUser($data1['record']['date'], null, $this->companyConnection);
                $data1['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
                return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'The record fetched up successfully.');
            }

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $message = "";
                if ($fieldType == "deleted_at"){
                    $data['deleted_at'] = date('Y-m-d H:i:s');
                    $message = "deleted";
                }else{
                    $status = $this->companyConnection->query("SELECT completed FROM flags WHERE id=$recordid")->fetch();
                    if ($status['completed'] == "1"){
                        $data['completed'] = '0';
                    }else{
                        $data['completed'] = '1';
                    }
                    $message = "updated";
                }
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE flags SET ".$sqlData['columnsValuesPair']." where id='$recordid'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record '.$message.' successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getCharges(){

        try{
            $data = [];
            $data['charges'] = $this->companyConnection->query("SELECT id,charge_code FROM company_accounting_charge_code")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function saveChargeCodes(){
        try{
            $data['charge_code'] = $_POST['chargeCode'];
            $data['credit_account'] = $_POST['creditValue'];
            $data['debit_account'] = $_POST['debitValue'];
            $data['status'] = $_POST['status'];
            $data['description'] = $_POST['description'];

            //$required_array = ['move_in_date', 'start_date', 'lease_term', 'end_date','notice_date','rent_amount'];
            $required_array = ['charge_code','credit_account','debit_account','status'];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['charge_code'] = $_POST['chargeCode'];
                $data['credit_account'] = $_POST['creditValue'];
                $data['debit_account'] = $_POST['debitValue'];
                $data['status'] = $_POST['status'];
                $data['description'] = $_POST['description'];
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $data['is_editable'] = '0';
                $data['is_default'] = '0';

                $sqlData = createSqlColVal($data);

                $query = "INSERT INTO `company_accounting_charge_code` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $last_insert_id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'last_insert_id'=> $last_insert_id,'data'=> $data, 'status' => 'success', 'data' => $data,'message' => 'Chargecode added successfully.');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getFlagInfo(){
        try{
            $id = $_POST['id'];
            $sql = "SELECT tp.property_id,gp.owner_id,CONCAT( u.first_name, ' ', u.last_name ) AS fullname,
            u.phone_number,u.mobile_number FROM tenant_property tp JOIN general_property gp ON tp.property_id
             = gp.id JOIN users u ON gp.owner_id = u.id WHERE u.id=$id";
            $data['propid'] = $this->companyConnection->query($sql)->fetch();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
            return array('code' => 200, 'data'=> $data, 'status' => 'success', 'message' => 'Record fetched successfully.');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getRenterInfo(){
        try{
            $tenantId = $_POST['tenantId'];
            $sql = "SELECT * FROM tenant_renter_insurance WHERE user_id=$tenantId";
            $renter = $this->companyConnection->query($sql)->fetchAll();
            $data = [];
            if ($renter !== false){
                $postArray = array();
                foreach ($renter as $val){
                    $tempArray = array();
                    $tempArray['id'] = $val['id'];
                    $tempArray['user_id'] = $val['user_id'];
                    $tempArray['policy_holder'] = $val['policy_holder'];
                    $tempArray['provider'] = $val['provider'];
                    $tempArray['policy_no'] = $val['policy_no'];
                    $tempArray['status'] = $val['status'];
                    $tempArray['expire_date'] = dateFormatUser($val['expire_date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
                    $tempArray['renewel_date'] = dateFormatUser($val['renewel_date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
                    $tempArray['effective_date'] = dateFormatUser($val['effective_date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
                    $tempArray['created_at'] = $val['created_at'];
                    $tempArray['updated_at'] = $val['updated_at'];
                    $postArray[] = $tempArray;
                }
                $data['renter'] = $postArray;
            }

            return array('code' => 200, 'data'=> $data, 'status' => 'success', 'message' => 'Record fetched successfully.');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function saveFlagData(){
        try{
            $data = $_POST['form'];
            $data = postArray($data);

            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $tenant_id = $data['flag_tenant_id'];
            $date = date('Y-m-d H:i:s');

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if ($data['record_id'] == ""){
                    $data1['flag_by'] = $data['flagged_by_name'];
                    $data1['date'] = mySqlDateFormat($data['date'], $id = null,$this->companyConnection);
                    $data1['flag_name'] = $data['flag_name'];
            //        $data1['country_code'] = $data['country_code'];
                    $data1['flag_phone_number'] = $data['phone_number'];
                    $data1['flag_reason'] = $data['flag_reason'];
                    $data1['completed'] = $data['status'];
                    $data1['flag_note'] = $data['note'];
                    $data1['object_id'] = $data['flag_tenant_id'];
                    $data1['object_type'] = 'tenant';
                    $data1['created_at'] = date('Y-m-d H:i:s');
                    $data1['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data1);

                    $query = "INSERT INTO `flags` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data1);
                    $last_insert_id = $this->companyConnection->lastInsertId();

                    $qry = "UPDATE `users` SET updated_at='$date' where id='$tenant_id'";
                    $stm = $this->companyConnection->prepare($qry);
                    $stm->execute();
                    return array('code' => 200, 'last_insert_id'=> $last_insert_id,'data'=> $data1, 'status' => 'success', 'data' => $data,'message' => 'Flag added successfully.');
                }else{

                    $data1['flag_by'] = $data['flagged_by_name'];
                    $data1['date'] = mySqlDateFormat($data['date'], $id = null,$this->companyConnection);
                    $data1['flag_name'] = $data['flag_name'];
                    $data1['country_code'] = $data['country_code'];
                    $data1['flag_phone_number'] = $data['phone_number'];
                    $data1['flag_reason'] = $data['flag_reason'];
                    $data1['completed'] = $data['status'];
                    $data1['flag_note'] = $data['note'];
                    $data1['object_id'] = $data['flag_tenant_id'];
                    $data1['updated_at'] = date('Y-m-d H:i:s');
                    

                    $rowId = $data['record_id'];
                    $sqlData = createSqlColValPair($data1);
                    $query = "UPDATE `flags` SET " . $sqlData['columnsValuesPair'] . " where id='$rowId'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();


                    $qry = "UPDATE `users` SET updated_at='$date' where id='$tenant_id'";
                    $stm = $this->companyConnection->prepare($qry);
                    $stm->execute();
                   


                    return array('code' => 200,'data'=> $data1, 'status' => 'success', 'data' => $data,'message' => 'Flag updated successfully.');
                }

            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function saveRenterData(){
        try{
            $data = $_POST['form'];
            $data = postArray($data);

            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $tenantId = $data['renter_tenant_id'];
                $tenantRecord =$this->companyConnection->query("SELECT u.first_name,u.last_name,ri.id FROM users u JOIN tenant_renter_insurance ri ON ri.user_id = u.id WHERE u.id= $tenantId")->fetch();
                $tenantName =$this->companyConnection->query("SELECT CONCAT( first_name, ' ', last_name ) AS fullname FROM users WHERE id= $tenantId")->fetch();
                $data1 = [];
                if (is_array($data['policyinfo'])){
                    for($i = 0; $i < count($data['policyinfo']); $i++ ){
                        $rowId = $data['policyid'][$i];
                        if ($rowId == ""){
                            $data1['user_id'] = $data['renter_tenant_id'];
                            $data1['policy_holder'] = $tenantName['fullname'];
                            $data1['policy_no'] = $data['policyinfo'][$i];
                            $data1['provider'] = $data['provider'][$i];
                            $data1['status'] = $data['status'][$i];
                            $data1['expire_date'] = mySqlDateFormat($data['expiration'][$i], $id = null,$this->companyConnection);
                            $data1['effective_date'] = mySqlDateFormat($data['effective'][$i], $id = null,$this->companyConnection);
                            $data1['renewel_date'] = mySqlDateFormat($data['renewal'][$i], $id = null,$this->companyConnection);
                            $data1['created_at'] = date('Y-m-d H:i:s');
                            $data1['updated_at'] = date('Y-m-d H:i:s');
                            $this->insertRenterDetails($data1);
                        }else{
                            $data1['policy_holder'] = $tenantName['fullname'];
                            $data1['policy_no'] = $data['policyinfo'][$i];
                            $data1['provider'] = $data['provider'][$i];
                            $data1['status'] = $data['status'][$i];
                            $data1['expire_date'] = mySqlDateFormat($data['expiration'][$i], $id = null,$this->companyConnection);
                            $data1['effective_date'] = mySqlDateFormat($data['effective'][$i], $id = null,$this->companyConnection);
                            $data1['renewel_date'] = mySqlDateFormat($data['renewal'][$i], $id = null,$this->companyConnection);
                            $data1['updated_at'] = date('Y-m-d H:i:s');
                            $this->updateRenterDetails($data1,$rowId);
                        }

                    }
                }else{
                    $rowId = $data['policyid'];
                    if ($rowId == ""){
                        $data1['user_id'] = $data['renter_tenant_id'];
                        $data1['policy_holder'] = $tenantName['fullname'];
                        $data1['policy_no'] = $data['policyinfo'];
                        $data1['provider'] = $data['provider'];
                        $data1['status'] = $data['status'];
                        $data1['expire_date'] = mySqlDateFormat($data['expiration'], $id = null,$this->companyConnection);
                        $data1['effective_date'] = mySqlDateFormat($data['effective'], $id = null,$this->companyConnection);
                        $data1['renewel_date'] = mySqlDateFormat($data['renewal'], $id = null,$this->companyConnection);
                        $data1['created_at'] = date('Y-m-d H:i:s');
                        $data1['updated_at'] = date('Y-m-d H:i:s');
                        $this->insertRenterDetails($data1);
                    }else{
                        $data1['policy_holder'] = $tenantName['fullname'];
                        $data1['policy_no'] = $data['policyinfo'];
                        $data1['provider'] = $data['provider'];
                        $data1['status'] = $data['status'];
                        $data1['expire_date'] = mySqlDateFormat($data['expiration'], $id = null,$this->companyConnection);
                        $data1['effective_date'] = mySqlDateFormat($data['effective'], $id = null,$this->companyConnection);
                        $data1['renewel_date'] = mySqlDateFormat($data['renewal'], $id = null,$this->companyConnection);
                        $data1['updated_at'] = date('Y-m-d H:i:s');
                        $this->updateRenterDetails($data1,$rowId);
                    }
                }


                    $notificationTitle= "Tenant Rental Ins. Purchased";
                    $module_type = "TENANT";
                    $alert_type = "Real Time";
                    $descriptionNotifi ='Tenant ID-'.$tenantId.' has purchased rental insurance.';
                    insertNotification($this->companyConnection,$tenantId,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);

                return array('code' => 200, 'data'=> $data1, 'status' => 'success', 'data' => $data1,'message' => 'Renter insurance updated successfully.');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function uploadFiles($files,$name){
        $target_dir = COMPANY_DIRECTORY_URL."/uploads/apexlink_company_".$_SESSION[SESSION_DOMAIN]['cuser_id']."/hoa/";

        $target_file = $target_dir . basename($files[$name]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check file size
        if ($_FILES[$name]["size"] > 500000) {
            $uploadOk = 0;
            $message = "File is not an image.";
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            $uploadOk = 0;
            $message = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $message = "Sorry, your file was not uploaded.";
        } else {
            if (move_uploaded_file($files[$name]["tmp_name"], $target_file)) {
                $message = "The file ". basename( $files[$name]["name"]). " has been uploaded.";
            } else {
                $message = "Sorry, there was an error uploading your file.";
            }
        }
        return array('code' => 200, 'status' => 'error','message' => $message);
    }

    public function saveHoaData(){
        try{
            $data = $_POST;

            if ($data['hoa_tenant'] == ""){
                $data1['user_id'] = $data['tenant_id'];
                $data1['hoa_violation_id'] = $data['hoa_id'];
                $data1['date'] = mySqlDateFormat($data['hoa_date'], $id = null,$this->companyConnection);
                $data1['time'] = mySqlTimeFormat($data['hoa_time']);
                $data1['hoa_type'] = $data['hoa_type'];
                $data1['hoa_description'] = $data['hoa_text'];
                $data1['upload_pic_1'] = json_decode($_POST['hoaImg1'])[0];
                $data1['upload_pic_2'] = json_decode($_POST['hoaImg2'])[0];
                $data1['upload_pic_3'] = json_decode($_POST['hoaImg3'])[0];
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO `hoa_violation` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $last_insert_id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'data'=> $data1, 'status' => 'success', 'last_insert_id' => $last_insert_id,'message' => 'HOA Violation inserted successfully.');
            }else{
                $rowId = $data['hoa_tenant'];
                $data1['hoa_violation_id'] = $data['hoa_id'];
                $data1['date'] = mySqlDateFormat($data['hoa_date'], $id = null,$this->companyConnection);
                $data1['time'] = mySqlTimeFormat($data['hoa_time']);
                $data1['hoa_type'] = $data['hoa_type'];
                $data1['hoa_description'] = $data['hoa_text'];
                $data1['upload_pic_1'] = json_decode($_POST['hoaImg1'])[0];
                $data1['upload_pic_2'] = json_decode($_POST['hoaImg2'])[0];
                $data1['upload_pic_3'] = json_decode($_POST['hoaImg3'])[0];
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data1);
                $query = "UPDATE `hoa_violation` SET " . $sqlData['columnsValuesPair'] . " where id='$rowId'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'data'=> $data1, 'status' => 'success', 'last_insert_id' => '','message' => 'HOA Violation updated successfully.');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function insertRenterDetails($data1){
        $sqlData = createSqlColVal($data1);

        $query = "INSERT INTO `tenant_renter_insurance` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($data1);
        $last_insert_id = $this->companyConnection->lastInsertId();
        //return $last_insert_id;
    }

    public function updateRenterDetails($data1,$rowId){
        $sqlData = createSqlColValPair($data1);
        $query = "UPDATE `tenant_renter_insurance` SET " . $sqlData['columnsValuesPair'] . " where id='$rowId'";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        //return array('code' => 200, 'data'=> $data1, 'status' => 'success', 'data' => $data1,'message' => 'Renter insurance updated successfully.');
    }
    
    public function getHoaPrintData() {
        //delete
        try {
            $complaint_ids = $_POST['complaint_ids'];
            $complaint_idss = str_repeat('?,', count($complaint_ids) - 1) . '?';
            $stmt = $this->companyConnection->prepare("Select hoa_violation.*,users.name,users.address1,users.address2,users.address3,users.address4,hoa_violation_type.type FROM `hoa_violation` LEFT JOIN users ON hoa_violation.user_id = users.id LEFT JOIN hoa_violation_type ON hoa_violation.hoa_type = hoa_violation_type.id WHERE hoa_violation.id IN ($complaint_idss)");
            $stmt->execute($complaint_ids);
            $data = $stmt->fetchAll();
            $print_complaints_html = '';
            foreach ($data as $key => $value) {
                $print_complaints_html .= '  <table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td align="center" style="padding: 10px 0; border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0;">
                    <img width="200" src="../company/images/logo.png"/>
                  </td>
                </tr>
                <tr>
                  <td style="background: #00b0f0; height: 30px;">
                    
                  </td>
                </tr>
                <tr>
                  <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
                    <table width="100%" align="center" cellspacing="0" cellpadding="0">
                      <tr>
                        <td style="font-size: 20px; color: #00b0f0; font-weight: bold; padding: 20px 0; text-align: center;"> Tenant HOA Volation(s) </td>
                      </tr>
                    </table>
                    <table width="100%" align="center" border="1" cellspacing="0" cellpadding="5">
                      <tr>
                        <td>Tenant Name: </td>
                        <td>'.$value['name'].'</td>
                      </tr>
                      <tr>
                        <td>Address: </td>
                        <td>'.$value['address1'].'</br>
                        '.$value['address2'].'</br>
                        '.$value['address3'].'</br>
                        '.$value['address4'].'
                      </tr>
                      <tr>
                        <td>Violation Type(s): </td>
                        <td>'.$value['type'].'</td>
                      </tr>
                      <tr>
                        <td>Description: </td>
                        <td>'.$value['hoa_description'].'</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td style="background: #585858; color: #fff; padding: 10px; text-align: center;">
                    Apexlink.apexlink@yopmail.com
                    
                  </td>
                </tr>
              </table>';
            }
            return array('status' => 'success', 'code' => 200, 'html' => $print_complaints_html, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }
    public function updateRentalStatus($rental_id){
        try {

            $data['status']="3";
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE company_rental_applications SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();


        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function generateSmartMoveSecurityHeader(){
        $partnerId = 188;
        if (isset($_POST['securityKey']) && $_POST['securityKey'] != ""){
            $securityKey = $_POST['securityKey'];
        }else{
            $securityKey = '';
        }

        $serverTime = $_POST['serverTime'];
        $serverTime = explode('.',$serverTime)[0];
        $message = "{$partnerId}{$serverTime}";
        //Generate HMAC SHA1 hash
        $hmac = hash_hmac("sha1", $message, $securityKey , true);
        $hash = base64_encode($hmac);
        echo json_encode(array('hash_key'=>$hash)); die();
    }



    public function getReceiableData()
    {

        //dd($_POST);
        $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';

        $html = '';
        $html .= "<table class='table  table-bordered' border= '1px'>";
        $html .= "<tr>
        <th>Name</th>
        <th>Payment Type</th>
        <th>Check#</th>
        <th>Ref#</th>
        <th>Date#</th>
        <th>Memo</th>
        <th>Amount($default_symbol)</th>
        <th>Overpay/Underpay#</th>
        </tr>";
        $type = $_POST['type'];

        if($type=='user')
        {
            $user_id = $_POST['user_id'];
            $getData =  $this->companyConnection->query("SELECT * FROM accounting_manage_charges where user_id='$user_id'")->fetch();
        }
        else
        {
            $invoice_id = $_POST['invoice_id'];
            $getData =  $this->companyConnection->query("SELECT * FROM accounting_manage_charges where invoice_id='$invoice_id'")->fetch();
        }

        $name = $_POST['username'];

        $total_amount = $getData['total_amount'];
        $total_amount_paid = $getData['total_amount_paid'];
        $total_due_amount = $getData['total_due_amount'];
        $total_refunded_amount = $getData['total_refunded_amount'];
        $overpay_underpay = $getData['overpay_underpay'];
        if($overpay_underpay=='')
        {
            $overpay_underpay = 0;
        }

        $dueAndOverPay = $total_due_amount + $overpay_underpay;

        $totalAmtPaidByUser = $total_amount+$overpay_underpay;
        $status = $getData['status'];
        $date = date('Y-m-d H:i:s');
        $dateFormat = dateFormatUser($date,null,$this->companyConnection);
        $html .= "<tr>
        <td><select class='form-control'>";
        if($type=='user')
        {
            $html .="<option value='$user_id'>$name</option></select></td>";
        }
        else
        {
            $html .="<option value='$invoice_id'>$name</option></select></td>";
        }

        $html .="<td><select class='paymnt_type form-control'>
        <option value='check'>check</option>
        <option value='cash'>cash</option>
        <option value='money-order'>Money Order</option>";
        if($type=='user')
        {
            $html.="<option value='card'>card</option>
        <option value='ACH'>ACH</option></td>";
        }
        $html.="<td><input type='text' class='check form-control'></td>
        <td><input type='text' class='ref form-control'></td>
        <td><input type='text' name='paymentDate' class='paymentDate form-control' value='$dateFormat'></td>
        <td><textarea class='memo'></textarea></td>
        <td><input type='text' name='paidAmt' class='paidAmt numberonly form-control' value='".$total_due_amount."'></td>
        <td><input type='text' class='overUnderPay numberonly form-control' value='".$overpay_underpay."' readonly></td>
        </tr> <input type='hidden' class='hiddenDueAmount form-control' value='$total_due_amount'>
         <input type='hidden' class='hiddenOverPay form-control' value='$overpay_underpay'>
         <input type='hidden' class='hiddenUncheckedAmt form-control' value='0'>
         <input type='hidden' class='hiddenUser_id form-control' value='$user_id'>
         <input type='hidden' class='hiddenChargeType form-control' value='$type'>";

        echo $html;
        exit;

    }




    public function getAllChargesData()
    {
        $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';
        $type = $_POST['type'];
        $html =  "";
        $html .= "<table class='table  table-bordered' border= '1px'>";
        $html .= "<tr>
        <th>Date</th>
        <th>Description</th>
        <th>Charge Code</th>
        <th>Original Amount($default_symbol)</th>
        <th>Amount paid($default_symbol)</th>
        <th>Amount Due($default_symbol)</th>
        <th>Amount Waived off($default_symbol)</th>
        <th>Waive off comment</th>
        <th>Waive off Amount($default_symbol)</th>
        <th>Current Payment($default_symbol)</th>
        <th>Priority</th>
        <th>Pay</th>


        </tr>";
        if($type=='user')
        {
            $user_id = $_POST['user_id'];
            $idByType = $user_id;
            $getData =  $this->companyConnection->query("SELECT tc.id,tc.created_at,tc.amount,tc.amount_due,tc.amount_paid,tc.amount_refunded,tc.start_date,tc.end_date,tc.waive_of_amount,tc.waive_of_comment,tc.status,ca.description,ca.charge_code,ca.priority FROM tenant_charges as tc inner join company_accounting_charge_code as ca on tc.charge_code = ca.id   where tc.user_id='$user_id'")->fetchAll();

        }
        else
        {
            $invoice_id = $_POST['invoice_id'];
            $idByType = $invoice_id;
            $getData =  $this->companyConnection->query("SELECT tc.id,tc.created_at,tc.amount,tc.amount_due,tc.amount_paid,tc.amount_refunded,tc.start_date,tc.end_date,tc.waive_of_amount,tc.waive_of_comment,tc.status,ca.description,ca.charge_code,ca.priority FROM tenant_charges as tc inner join company_accounting_charge_code as ca on tc.charge_code = ca.id   where tc.invoice_id='$invoice_id'")->fetchAll();

        }
        $html .= "<input type='hidden' name='typeofcharge' value='$type'>
                <input type='hidden' name='idbytype' value='$idByType'>";
        foreach($getData as $data)
        {
            $created_at = $data['created_at'];
            $date = dateFormatUser($created_at,null,$this->companyConnection);
            $description = $data['description'];
            $amount_due = $data['amount_due'];
            if($amount_due=='')
            {
                $amount_due = 0;
            }

            $totalamount = $data['amount'];
            $amount_paid = $data['amount_paid'];
            if($amount_paid=='')
            {
                $amount_paid = 0;
            }
            $amount_refunded = $data['amount_refunded'];
            $status = $data['status'];
            $charge_code = $data['charge_code'];
            $waive_of_amount = $data['waive_of_amount'];
            if($waive_of_amount=='')
            {
                $waive_of_amount = 0;
            }
            $waive_of_comment = $data['waive_of_comment'];
            $chargeId = $data['id'];

            if($status=='1')
            {
                $disabled = 'disabled=disabled';
            }
            else
            {
                $disabled = '';
            }
            $priority = $data['priority'];

            $html .= "<tr>
       
        <td>$date</td>
        <td>$description</td>
        <td>$charge_code</td>
        <td>$totalamount</td>
        <td>$amount_paid</td>
        <td class= 'current_due_amt_".$chargeId."'>$amount_due</td>
        <td>$waive_of_amount</td>
        <td><textarea class='waiveOfComment_".$chargeId."' name='waiveOfComment[]' $disabled>$waive_of_comment</textarea></td>
        <td><input type='text' class='waiveOfAmount_".$chargeId." numberonly' value='$waive_of_amount' name='waiveOfAmount[]' $disabled></td>
        <td><input type='text' class='currentPayment_".$chargeId." numberonly' value='$amount_due' name='currentPayment[]' $disabled></td>
        <td>$priority</td>";



            if($status=='1')
            {
                $html .= "<td><input type='checkbox' name='pay[]' disabled='disabled' checked='checked' value='$chargeId' class='pay_checkbox'></td>";

            }
            else
            {
                $html .= "<td><input type='checkbox' name='pay[]' checked='checked' value='$chargeId' class='pay_checkbox'></td>";
            }


            $html .=   "</tr>";

        }


        $html .= "</table>";
        $html .=   "<div class='footer-btn-outer'> <input type='submit' name='applyAccountingCalculations' class='submitBtn blue-btn' value='Save & New'>";
        $html .=   "<input type='submit' name='applyAccountingCalculations' class='submitBtn blue-btn' value='Save'>";
        $html .=   "<input type='button' class='CancelBtn blue-btn' value='Cancel'>";
        $html .=   "<input type='button'  class='reallocate blue-btn' value='Re-allocate'></div>";
        echo $html;
        exit;

    }



    public function updateAllCharges()
    {
//dd($_POST);
        $chargeType = $_POST['typeofcharge'];
        $idByType = $_POST['idbytype'];
        $payment_type = $_POST['paymnt_type'];
        $paid_amount = $_POST['paid_amount'];
        $hiddenChargeType = $_POST['hiddenChargeType'];
        if($payment_type=='card' || $payment_type=='ACH')
        {

            $checkPayment =  $this->checkForPayment($idByType,$payment_type);
            if($checkPayment['status']=='false')
            {
                return array('status'=>'false','message'=>$checkPayment['message']);
            }
            else
            {
                $customer_id = $checkPayment['customer_id'];
            }

            $receivePaymentCheck = $this->checkForReceivePayment();    /*this is to check if PM is Elizible for payment or not*/
            if($receivePaymentCheck['account_status']=='false')
            {
                return array('status'=>'false','message'=>'Currently PM Account is not verified');
            }
            else
            {
                $account_id = $receivePaymentCheck['account_data']['id'];

            }
        }

        if(isset($_POST['pay']))
        {


            $count = count($_POST['pay']);





            $wrongPaymentCheck = $this->wrongPaymentCheck();
            if($wrongPaymentCheck['status']=='false')
            {
                return array('status'=>'false','message'=>$wrongPaymentCheck['message']);
            }




            for($i=0; $i<$count; $i++)
            {
                $chargeTableId = $_POST['pay'][$i]; /*id of charge table*/

                $getData =  $this->companyConnection->query("SELECT * FROM tenant_charges where id='$chargeTableId'")->fetch();



                $currentPayment = $_POST['currentPayment'][$i];

                $waveAmt =  $_POST['waiveOfAmount'][$i];
                $amt = $getData['amount'];
                $waiveComment = $_POST['waiveOfComment'][$i];

                if($waveAmt=='')
                {
                    $waveAmt = 0;
                }
                $alreadyPaid = $getData['amount_paid'];
                if($alreadyPaid=='')
                {
                    $alreadyPaid = 0;
                }
                $currentAmtPaid = $alreadyPaid + $currentPayment;


                $currentDueAmt = $amt - $currentPayment - $waveAmt;
                $data['amount_due'] = $currentDueAmt;
                $data['amount_paid'] = $currentAmtPaid;
                $data['waive_of_amount'] = $waveAmt;

                if($amt>$currentAmtPaid + $waveAmt)
                {
                    $data['status'] = 0;
                }
                else if($amt=$currentAmtPaid + $waveAmt)
                {
                    $data['status'] = 1;
                }



                $sqlData = createSqlColValPair($data);


                $query = "UPDATE tenant_charges SET " . $sqlData['columnsValuesPair'] . " where id=" . $chargeTableId;


                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();


            }


        }

        if(!isset($_POST['pay']) && $paid_amount<=1)
        {
            return array('status'=>'false','message'=>'There must be atleast one charge to pay');

        }

        if($payment_type=='card' || $payment_type=='ACH' && $paid_amount>=1)
        {
            $this->createCharge($customer_id,$account_id,$paid_amount,$payment_type,$idByType,$chargeType);
        }
        else
        {
            $this->addTransactionData($paid_amount,$payment_type,$idByType,$chargeType);
        }

        $this->addChargeAmount($idByType,$chargeType);
        $this->changeInvoiceStatus($chargeType,$idByType);

        for($i=0; $i<count($_POST['pay']); $i++){
            $now = new \DateTime('now');
            $month = $now->format('m');
            $year = $now->format('Y');
        $journal_entry_data['transaction_id'] = (isset($data['']) && !empty($data[''])?$data[''] :'');
        $journal_entry_data['transaction_type'] = (isset($_POST['paymnt_type']) && !empty($_POST['paymnt_type'])?$_POST['paymnt_type'] :'cash');
        $journal_entry_data['module_type'] = (isset($data['module_type']) && !empty($data['module_type'])?$data['module_type'] :'tenant_charges');
        $journal_entry_data['module_id'] = (isset($_POST['pay'][$i]) && !empty($_POST['pay'][$i])?$_POST['pay'][$i] :1);
        $journal_entry_data['from_user'] = (isset($_POST['idbytype']) && !empty($_POST['idbytype'])?$_POST['idbytype'] :1);
        $journal_entry_data['to_user'] = (isset($data['to_user']) && !empty($data['to_user'])?$data['to_user'] :1);
        $journal_entry_data['debit'] = (isset($data['debit']) && !empty($data['debit'])?$data['debit'] :0);
        $journal_entry_data['credit'] = (isset($_POST['currentPayment'][$i]) && !empty($_POST['currentPayment'][$i])?$_POST['currentPayment'][$i] :NULL);
        $journal_entry_data['current_month_year'] = $year.'-'.$month;
        $journal_entry_data['created_at'] = date('Y-m-d H:i:s');
        saveLedgerEnteries($this->companyConnection,$journal_entry_data);  // call  this function for saving enteries
        }
        return array('status'=>'true','message'=>'Payment has been done successfully.');



    }


    public function wrongPaymentCheck()
    {
        $count = count($_POST['pay']);
        $idByType = $_POST['idbytype'];
        $chargeType = $_POST['typeofcharge'];




        for($i=0; $i<$count; $i++)
        {
            $chargeTableId = $_POST['pay'][$i]; /*id of charge table*/

            $getData =  $this->companyConnection->query("SELECT * FROM tenant_charges where id='$chargeTableId'")->fetch();



            $currentPayment = $_POST['currentPayment'][$i];

            $waveAmt =  $_POST['waiveOfAmount'][$i];
            $amt = $getData['amount'];
            $waiveComment = $_POST['waiveOfComment'][$i];

            if($waveAmt=='')
            {
                $waveAmt = 0;
            }
            $alreadyPaid = $getData['amount_paid'];
            if($alreadyPaid=='')
            {
                $alreadyPaid = 0;
            }
            $currentAmtPaid = $alreadyPaid + $currentPayment;



            if($amt>$currentAmtPaid+$waveAmt)
            {
                return array('status'=>'false','message'=>'Currently you are paying less amount');
            }
            else if($amt<$currentAmtPaid+$waveAmt)
            {
                return array('status'=>'false','message'=>'Currently you are paying greater amount');
            }

        }

    }


    public function checkForCard()
    {
        $user_id = $_POST['user_id'];
        $type = $_POST['type'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users where id='$user_id'")->fetch();
        if($getData['stripe_customer_id']=='')
        {
            return array('status'=>'false','message'=>"This user has not entered his $type infomation");
        }
        else
        {
            return array('status'=>'true','message'=>"this user has customer id");
        }


    }



    public function checkForACH()
    {
        $user_id = $_POST['user_id'];
        $type = $_POST['type'];
        $getData =  $this->companyConnection->query("SELECT stripe_customer_id FROM users where id='$user_id'")->fetch();
        if($getData['stripe_customer_id']=='')
        {
            return array('status'=>'false','message'=>"This user has not entered his $type infomation");
        }
        else
        {
            return array('status'=>'true','message'=>"this user has customer id");
        }

    }


    public function addTransactionData($paid_amount,$payment_type,$user_id,$chargeType)
    {
        try{
            $data['user_id'] = $user_id;
            $type = $this->getUserType($user_id);
            if($type=='2')
            {
                $data['user_type'] = 'TENANT';
            }
            else if($type=='4')
            {
                $data['user_type'] = 'OWNER';
            }
            else
            {
                $data['user_type'] = 'TENANT';
            }
            $data['amount'] = $paid_amount;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO transactions(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            return array('code' => 400, 'status' => 'true','message' => 'charge added successfully');

        }
        catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }

    }


    public function getUserType($user_id)
    {
        $getData = $this->companyConnection->query("SELECT user_type FROM users where id='$user_id'")->fetch();
        return  $getData['user_type'];

    }



    public function addChargeAmount($id,$type)
    {
        if($type=='user')
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE user_id ='" . $id . "'")->fetch();
        }
        else
        {
            $getCharges = $this->companyConnection->query("SELECT sum(amount) as total_amount,sum(amount_paid) as amount_paid,sum(amount_refunded) as amount_refunded,sum(amount_due) as amount_due,sum(waive_of_amount) as waive_of_amount FROM tenant_charges WHERE invoice_id ='" . $id . "'")->fetch();
        }


        if($getCharges['total_amount']=='')
        {
            $totalAmount = 0;
            $data['total_amount'] = $totalAmount;
        }
        else
        {
            $totalAmount = $getCharges['total_amount'];
            $data['total_amount'] = $totalAmount;
        }


        if($getCharges['amount_paid']=='')
        {
            $amount_paid = 0;
            $data['total_amount_paid'] = $amount_paid;
        }
        else
        {
            $amount_paid = $getCharges['amount_paid'];
            $data['total_amount_paid'] = $amount_paid;
        }

        if($getCharges['amount_refunded']=='')
        {
            $amount_refunded = 0;
            $data['total_refunded_amount'] = $amount_refunded;
        }
        else
        {
            $amount_refunded = $getCharges['amount_refunded'];
            $data['total_refunded_amount'] = $amount_refunded;
        }

        if($getCharges['waive_of_amount']=='')
        {
            $waive_of_amount = 0;
            $data['total_waive_amount'] = $waive_of_amount;
        }
        else
        {
            $waive_of_amount = $getCharges['waive_of_amount'];
            $data['total_waive_amount'] = $waive_of_amount;
        }

        $data['overpay_underpay'] = $_POST['overPay'];
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');




        $data['total_due_amount'] = $getCharges['amount_due'];
        $sqlData = createSqlColValPair($data);
        if($type=='user')
        {
            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where user_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }
        else
        {

            $query = "UPDATE accounting_manage_charges SET " . $sqlData['columnsValuesPair'] . " where invoice_id=" . $id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }


    }
    public function changeInvoiceStatus($chargeType,$id)
    {


        if($chargeType=='invoice')
        {
            $payedInvoices  = $this->companyConnection->query("SELECT * FROM tenant_charges where invoice_id='$id' and status='1'")->fetchAll();
            $countPayedInvoice = count($payedInvoices);
            $invoices = $this->companyConnection->query("SELECT * FROM tenant_charges where invoice_id='$id'")->fetchAll();
            $countInvoices = count($invoices);

            if($countInvoices==$countPayedInvoice)
            {
                $data['status'] = 1;
                $sqlData = createSqlColValPair($data);

                $query = "UPDATE accounting_invoices SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

            }


        }
        else
        {
            $getInvoices  = $this->companyConnection->query("SELECT id FROM accounting_invoices where invoice_to='$id'")->fetchAll();

            if(!empty($getInvoices))
            {

                foreach($getInvoices as $invoice)
                {
                    $invoiceId = $invoice['id'];
                    $payedInvoices  = $this->companyConnection->query("SELECT * FROM tenant_charges where invoice_id='$invoiceId' and status='1'")->fetchAll();
                    $countPayedInvoice = count($payedInvoices);
                    $invoices = $this->companyConnection->query("SELECT * FROM tenant_charges where invoice_id='$invoiceId'")->fetchAll();
                    $countInvoices = count($invoices);
                    if($countInvoices==$countPayedInvoice)
                    {
                        $data['status'] = 1;
                        $sqlData = createSqlColValPair($data);

                        $query = "UPDATE accounting_invoices SET " . $sqlData['columnsValuesPair'] . " where id=" . $invoiceId;
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();

                    }
                }

            }
        }



    }

    public function getledgerdata()
    {
        $user_id = $_POST['user_id'];
        $start_date=$_POST['st_date'];
        $end_date=$_POST['end_date'];
        //dd("SELECT cacc.charge_code,cacc.description,al.credit,al.debit,tc.end_date from accounting_ledger al LEFT JOIN tenant_charges tc ON al.module_id=tc.id LEFT JOIN company_accounting_charge_code cacc ON tc.charge_code=cacc.id where al.from_user='$user_id' and al.module_type='tenant_charges' and al.current_month_year BETWEEN '$start_date' and '$end_date'");
        $getData =  $this->companyConnection->query("SELECT cacc.charge_code,cacc.description,al.credit,al.debit,tc.end_date from accounting_ledger al LEFT JOIN tenant_charges tc ON al.module_id=tc.id LEFT JOIN company_accounting_charge_code cacc ON tc.charge_code=cacc.id where al.from_user='$user_id' and al.module_type='tenant_charges' and tc.end_date BETWEEN '$start_date' and '$end_date'")->fetchAll();
        $dates=[];
        for($i=0;$i<count($getData);$i++){
           $date = dateFormatUser($getData[$i]['end_date'],null,$this->companyConnection);
          array_push($dates,$date);
        }
            return ['code'=>200,'message'=>'success','data'=>$getData,'dates'=>$dates];
    }


    public function getPdfContent(){
        try{
            $html=$_POST['htmls'];
            $html='<html><table>'.$html.'</table></html>';
            $pdf = $this->createHTMLToPDF($html);
            $data['record'] = $pdf['data'];
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function createHTMLToPDF($report){
        $dompdf = new Dompdf();
        $dompdf->loadHtml($report);
        $dompdf->render();
        $path = "uploads/";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'/Ticket.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'/Ticket.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
    }

    public function sendFileLibraryEmail(){
        try{
            $userId = $_POST['tenantId'];
            $rowId = $_POST['rowId'];
            $user =  $this->companyConnection->query("SELECT id,email from users WHERE id=$userId")->fetch();
            $file =  $this->companyConnection->query("SELECT id,filename, file_location, file_extension from tenant_chargefiles WHERE id=$rowId")->fetch();
            $companyUrl = SITE_URL."company/";
            $filePath = $companyUrl.$file['file_location'];
            $request["attachments"][] = $filePath;
            $body = 'PFA';
            $request['to'][]    = $user['email'];
            $request['action']  = 'SendMailPhp';
            $request['subject'] = 'test';
            $request['portal']  = '1';
            $request['message'] = $body;
            $response = curlRequest($request);
            return array('code' => 200, 'status' => 'success','message' => 'Email sent successfully!');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }
    public function sendFileLibraryattachEmail(){
        try{
            $toUsers = $_POST['to'];

            $userEmails = explode(",",$toUsers);
            $subject = $_POST['subject'];
            $path = $_POST['path'];
            $data= $_POST;
            $body =$data['body'];
            $request["attachments"][] = $path;
            $request['to']    = $userEmails;
            if(isset($_POST['cc']) && !empty($_POST['cc'])){
                $ccEmails = explode(",",$_POST['cc']);
                $request['cc']      = $ccEmails;
            }
            if(isset($_POST['bcc']) && !empty($_POST['bcc'])){
                $bccUsers = explode(",",$_POST['bcc']);
                $request['bcc']     = $bccUsers;
            }
            $request['action']  = 'SendMailPhp';
            $request['subject'] = $subject;
            $request['portal']  = '1';
            $request['message'] = $body;
            $response = curlRequest($request);
            return array('code' => 200, 'status' => 'success','message' => 'Email sent successfully!','response'=>$response);
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function get_mime_type($filename) {
        $idx = explode( '.', $filename );
        $count_explode = count($idx);
        $idx = strtolower($idx[$count_explode-1]);
        $mimet = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        if (isset( $mimet[$idx] )) {
            return $mimet[$idx];
        } else {
            return 'application/octet-stream';
        }
    }

    public function getContactUserDetails(){
        try{
            $userId = $_POST['contactid'];
            $sql = "SELECT u.salutation,u.first_name,u.last_name,u.nick_name,u.middle_name,tp.property_id,tp.building_id,tp.unit_id,";
            $user =  $this->companyConnection->query("SELECT id,email from users WHERE id=$userId")->fetch();
            return array('code' => 200, 'status' => 'success','message' => '', 'data' => $user);
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

}
$tenantAjax = new TenantAjax();