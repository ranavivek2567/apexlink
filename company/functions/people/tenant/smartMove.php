<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
error_reporting(E_ALL);
ini_set("display_errors",1);
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class SmartMove extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getTenantById(){
        try {
            $tenantId=$_POST['id'];
            $sql = "SELECT u.id,u.first_name,u.last_name,u.middle_name,u.dob,u.ssn_sin_id,u.email, 
            gp.address1,gp.address2,gp.city,gp.state,gp.zipcode, u.phone_number FROM users u LEFT JOIN tenant_property tp ON u.id=tp.user_id 
            LEFT JOIN general_property gp ON tp.property_id= gp.id WHERE u.id=$tenantId";
            $record = $this->companyConnection->query($sql)->fetch();
            $newRecord['id'] = $record['id'];
            $newRecord['first_name'] = $record['first_name'];
            $newRecord['last_name'] = $record['last_name'];
            $newRecord['middle_name'] = $record['middle_name'];
            $newRecord['dob'] = $record['dob'];
            $ssn_id = unserialize($record['ssn_sin_id']);
            if(isset($ssn_id[0]) && $ssn_id[0] == ""){
                $newRecord['ssn_sin_id'] = "";
            }else{
                $newRecord['ssn_sin_id'] = $ssn_id[0];
            }
            $newRecord['address1'] = $record['address1'];
            $newRecord['address2'] = $record['address2'];
            $newRecord['city'] = $record['city'];
            $newRecord['state'] = $record['state'];
            $newRecord['zipcode'] = $record['zipcode'];
            $newRecord['phone_number'] = $record['phone_number'];

            echo json_encode(array($newRecord));die();
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getlandlordinfo(){
        try {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $sql = "SELECT u.id,u.first_name,u.last_name,u.middle_name,u.role,u.company_name,u.address1,u.address2,u.city,u.state,
            u.zipcode,u.email,u.number_of_units,tphon.phone_type,tphon.work_phone_extension,tphon.phone_number,gp.no_of_units FROM users u 
            LEFT JOIN tenant_phone tphon ON u.id=tphon.user_id LEFT JOIN tenant_property tp ON u.id=tp.user_id LEFT JOIN general_property gp 
            ON tp.property_id= gp.id WHERE u.id=$id";
            $record = $this->companyConnection->query($sql)->fetch();
            $returnData = ['status' => 'error', 'code'=>503, 'data'=>'','message'=>'No record found!'];
            if (!empty($record)){
                $returnData = ['status' => 'success', 'code'=>200, 'data'=>$record,'message'=>''];
                echo json_encode(array($returnData)); die();
            }
            echo json_encode(array($returnData)); die();
        } catch (PDOException $e) {
            echo $e->getMessage();die();
        }
    }

    public function getPropertyDetailsByTenant(){
        try {
            $id = $_POST['id'];
            $sql = "SELECT ud.unit_no,ud.unit_prefix,tp.property_id,tp.building_id,tp.unit_id,gp.manager_id,gp.property_id,gp.property_name,gp.address1,
            gp.address2,gp.city,gp.state,gp.zipcode FROM tenant_property tp 
            LEFT JOIN general_property gp ON tp.property_id=gp.id LEFT JOIN unit_details ud ON ud.id=tp.unit_id WHERE tp.user_id=$id";
            $record = $this->companyConnection->query($sql)->fetch();
            if ($record['manager_id'] != ""){
                $unserializeid = unserialize($record['manager_id']);
                $mangerId = $unserializeid[0];
                $record['manager_id'] = ucwords(userName($mangerId, $this->companyConnection,'users'));
            }else{
                $record['manager_id'] = $record['company_name'];
            }
            $returnData = ['status' => 'error', 'code'=>503, 'data'=>'','message'=>'No record found!'];
            if (!empty($record)){
                $returnData = ['status' => 'success', 'code'=>200, 'data'=>$record,'message'=>''];
                echo json_encode(array($returnData)); die();
            }
            echo json_encode(array($returnData)); die();
        } catch (PDOException $e) {
            echo $e->getMessage();die();
        }
    }

    public function getApplicationDetailsByTenant(){
        try {
            $id = $_POST['id'];
            $sql = "SELECT ud.id as unitId,ud.unit_no,ud.unit_prefix,tp.property_id,gp.property_id,gp.property_name,gp.address1,
            gp.address2,gp.city,gp.state,gp.zipcode,tld.rent_amount,tld.security_deposite,tld.term,u.email 
            FROM users u 
            LEFT JOIN tenant_property tp ON tp.user_id = u.id 
            LEFT JOIN general_property gp ON tp.property_id=gp.id 
            LEFT JOIN unit_details ud ON ud.id=tp.unit_id
            LEFT JOIN tenant_lease_details tld ON tld.user_id=tp.user_id
            WHERE u.id=$id";
            $record = $this->companyConnection->query($sql)->fetch();
            $getAdditionalTenant = $this->getOtherUsersByTenantId($id, 'tenant_additional_details', 'user_id');
            if (!empty($getAdditionalTenant)){
                $record['occupants'] = $getAdditionalTenant;
            }
            $getGuarantor = $this->getOtherUsersByTenantId($id, 'tenant_guarantor', 'user_id');
            if (!empty($getGuarantor)){
                $record['guarantors'] = $getGuarantor;
            }
            $record['sum'] = 1;
            if (isset($record['occupants']) && $record['occupants'] != ""){
                $occupantArray = explode(',',$record['occupants']);
                $countOccupantArray = count($occupantArray);
                $record['sum'] = $record['sum']+(int)$countOccupantArray;
            }
            if (isset($record['guarantors']) && $record['guarantors'] != ""){
                $guarantorsArray = explode(',',$record['guarantors']);
                $countGuarantorsArray = count($guarantorsArray);
                $record['sum'] = $record['sum']+(int)$countGuarantorsArray;
            }

            $returnData = ['status' => 'error', 'code'=>503, 'data'=>'','message'=>'No record found!'];
            if (!empty($record)){
                $returnData = ['status' => 'success', 'code'=>200, 'data'=>$record,'message'=>''];
            }
            echo json_encode(array($returnData)); die();
        } catch (PDOException $e) {
            echo $e->getMessage();die();
        }
    }

    public function getOtherUsersByTenantId($tenantId, $tablename, $whereColumn){
        $sql = "SELECT email1 FROM `$tablename` WHERE $whereColumn=$tenantId";
        $record = $this->companyConnection->query($sql)->fetchAll();
        $return = array();
        $emails = array();
        if (!empty($record)) {
            foreach ($record as $rec) {
                if ($rec['email1'] == "") {
                    continue;
                }
                $emails[] = $rec['email1'];
            }
            return implode(", ", $emails);
        }
        return $return;
    }

    public function sendEmailToRenter(){
        try{
            $userId = $_POST['id'];
            $userId = (isset($userId) ? $userId : $_REQUEST['id']);
            $company = $this->companyConnection->query("SELECT company_name FROM users where id=1")->fetch();
            $companyName = $company['company_name'];
            $propertyAdd = $this->companyConnection->query("SELECT u.email,tp.property_id,gp.address1,gp.address2,gp.city,gp.state,gp.zipcode 
            FROM users u 
            JOIN tenant_property tp ON tp.user_id = u.id 
            JOIN general_property gp ON tp.property_id = gp.id 
            where u.id=$userId")->fetch();
            $email = [];
            $email[] = $propertyAdd['email'];

            $additionalTenantEmails = $this->getOtherUsersByTenantId($userId, 'tenant_additional_details', 'user_id');
            if (!empty($additionalTenantEmails)){
                $occupantsEmails = explode(', ',$additionalTenantEmails);
            }
            $guarantorsEmails = $this->getOtherUsersByTenantId($userId, 'tenant_guarantor', 'user_id');
            if (!empty($guarantorsEmails)){
                $guarantorsEmails = explode(', ',$guarantorsEmails);
            }

            if (!empty($occupantsEmails) && empty($guarantorsEmails)){
                $toEmails = array_merge($email, $occupantsEmails);
            }else if (empty($occupantsEmails) && !empty($guarantorsEmails)){
                $toEmails = array_merge($email, $guarantorsEmails);
            }else if (!empty($occupantsEmails) && !empty($guarantorsEmails)){
                $toEmails = array_merge($email, $occupantsEmails, $guarantorsEmails);
            }else{
                $toEmails = $email;
            }

            $address1 = "";
            $address2 = "";
            if(!empty($propertyAdd)){
                if ($propertyAdd['address1'] != "" && $propertyAdd['address2'] == ""){
                    $address1 = $propertyAdd['address1'];
                }else if($propertyAdd['address1'] == "" && $propertyAdd['address2'] != ""){
                    $address1 = $propertyAdd['address2'];
                }else{
                    $address1 = $propertyAdd['address1']." ".$propertyAdd['address2'];
                }

                if ($propertyAdd['city'] != "" && $propertyAdd['state'] == "" && $propertyAdd['zipcode'] == ""){
                    $address2 = $propertyAdd['city'];
                }else if($propertyAdd['city'] != "" && $propertyAdd['state'] != "" && $propertyAdd['zipcode'] == ""){
                    $address2 = $propertyAdd['city'].", ".$propertyAdd['state'];
                }else{
                    $address2 = $propertyAdd['city'].", ".$propertyAdd['state'].", ".$propertyAdd['zipcode'];
                }
            }
            $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/smRenterEmail.php';
            $body = file_get_contents($fileUrls);
            $body = str_replace("SMCOMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
            $body = str_replace("SMADDRESS",$address1,$body);
            $body = str_replace("SMCITYSTATEZIP",$address2,$body);
            $tokenId = base64_encode($userId);
            $urlHtml = SUBDOMAIN_URL  ."/SM/V3/Renter?token=$tokenId";
            $body = str_replace("SMTENANTLINK",$urlHtml,$body);

            $request['action']  = 'SendMailPhp';
            $request['to']    = $toEmails;
            $request['subject'] = 'Welcome in SmartMove Renter Screening!';
            $request['message'] = $body;
            $request['portal']  = '1';
            if(!empty($toEmails)){
                curlRequest($request);
                return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email sent successfully'];
            }else{
                return ['status'=>'failed','code'=>503,'data'=>'To email cannot be blank!'];
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }


    }

    public function generateSmartMoveSecurityHeader(){
        $partnerId = 212;
        if (isset($_POST['securityKey']) && $_POST['securityKey'] != ""){
            $securityKey = $_POST['securityKey'];
        }else{
            $securityKey = '';
        }

        $serverTime = $_POST['serverTime'];
        //$serverTime = explode('.',$serverTime)[0];
        $message = "{$partnerId}{$serverTime}";
        //Generate HMAC SHA1 hash
        $hmac = hash_hmac("sha1", $message, $securityKey , true);
        $hash = base64_encode($hmac);
        echo json_encode(array('hash_key'=>$hash)); die();
    }

    public function saveLandlordAccountInfo(){
        try{
            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['tenant_id'] = $_POST['tent_id'];
            $data['user_type'] = $_POST['landlord'];
            $data['currenttimestamp'] = date('Y-m-d H:i:s');
            $data['status'] = '1';
            $data['email_id'] = $_POST['email'];
            $data['is_agrmnt_accepted'] = $_POST['accept_term'];

            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO `sm_landlord` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $return = array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record saved successfully');
        }catch (Exception $exception){
            $return = array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
        echo json_encode($return); die();
    }

    public function getTenantPorpertyInfo(){
        try{
            $tenantId = $_POST['tent_id'];
            $record['landlord'] = array();
            $sql = "SELECT tp.property_id as propertyId,gp.property_id,gp.property_name,gp.legal_name,gp.address1 as property_address1,gp.address2 as property_address2,gp.city as property_city,gp.state as property_state,gp.zipcode as property_zipcode,gp.phone_number as property_phone_number,gp.owner_id,ud.id as unitId,ud.unit_no,ud.unit_prefix,tld.rent_amount,tld.security_deposite 
            FROM tenant_property tp 
            LEFT JOIN general_property gp ON tp.property_id= gp.id 
            LEFT JOIN unit_details ud ON tp.unit_id = ud.id 
            LEFT JOIN tenant_lease_details tld ON tld.user_id = tp.user_id 
            WHERE tp.user_id=$tenantId";
            $record = $this->companyConnection->query($sql)->fetch();

            if ($record['property_phone_number'] != ""){
                $checkSerialized = $this->checkSerializedOrNot($record['property_phone_number']);
                if ($checkSerialized){
                    $propertyPhoneNumber =  unserialize($record['property_phone_number']);
                    if (isset($propertyPhoneNumber[0]) && $propertyPhoneNumber[0] != ""){
                        $propertyPhoneNumberss = str_replace("-","",$propertyPhoneNumber[0]);
                        $record['property_phone_number'] = $propertyPhoneNumberss;
                    }else{
                        $record['property_phone_number'] = "9999999999";
                    }
                }else{
                    $propertyPhoneNumber = str_replace("-","",$record['property_phone_number']);
                    $record['property_phone_number'] = $propertyPhoneNumber;
                }
            }else{
                $record['property_phone_number'] = "9999999999";
            }

            $companyName = $this->companyConnection->query("SELECT admindb_id as companyId,company_name FROM users WHERE id=1")->fetch();

            $record['company_id'] = $companyName['companyId'];
            $record['company_name'] = $companyName['company_name'];
            if ($record['owner_id'] != "") {
                $unserializeOwner = unserialize($record['owner_id']);
                $ownerId = $unserializeOwner[0];
                $ownerDetails = $this->companyConnection->query("SELECT first_name, last_name, address1, address2, city, state, zipcode, phone_number, email FROM users WHERE id=$ownerId")->fetch();
                if (!empty($ownerDetails)) {
                    if ($ownerDetails['phone_number'] != ""){
                        $checkSerialized = $this->checkSerializedOrNot($ownerDetails['phone_number']);
                        if ($checkSerialized){
                            $landlordPhoneNumber =  unserialize($ownerDetails['phone_number']);
                            if (isset($landlordPhoneNumber[0]) && $landlordPhoneNumber[0] != ""){
                                $landlordPhoneNumbers = str_replace("-","",$landlordPhoneNumber[0]);
                                $ownerDetails['phone_number'] = $landlordPhoneNumbers;
                            }else{
                                $ownerDetails['phone_number'] = "9999999999";
                            }
                        }else{
                            $landlordPhoneNumbers = str_replace("-","",$ownerDetails['phone_number']);
                            $ownerDetails['phone_number'] = $landlordPhoneNumbers;
                        }
                    }else{
                        $ownerDetails['phone_number'] = "9999999999";
                    }
                    $record['landlord'] = $ownerDetails;
                }else {
                    $record['landlord']['first_name'] = $companyName['company_name'];
                }
            }else {
                $record['landlord']['first_name'] = $companyName['company_name'];
            }

            $return = array('code' => 200, 'status' => 'success', 'data' => $record,'message' => 'Record fetched successfully');
        }catch (Exception $exception){
            $return = array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
        echo json_encode($return); die();
    }

    public function checkSerializedOrNot($str){
        $data = @unserialize($str);
        if($data !== false || $str === 'b:0;')
            return true;
        else
            return false;
    }

    public function savePropertyData(){
       try{
            $data = $_POST['form'];
            $tenantId = $_POST['tenantId'];
            $propertyReponse = $_POST['propertyReponse'];

            $data = postArray($data);

            //$required_array = ['move_in_date', 'start_date', 'lease_term', 'end_date','notice_date','rent_amount'];
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data1['tenant_id'] = $tenantId;
                $data1['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data1['property_id'] = $data['property_list'];
                $data1['property_unique_id'] = $data['property_id'];
                $data1['rent_amount'] = $data['rent_amount'];
                $data1['deposite_amount'] = $data['deposite_amount'];
                if (isset($data['bankrupters_check']) && $data['bankrupters_check'] != "") {
                    $data1['bankrupters_text'] = $data['bankrupters_text'];
                }else{
                    $data1['bankrupters_text'] = '';
                }
                $data1['record_status'] = '0';
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $data1['status'] = '0';
                $data1['sm_property_id'] = $propertyReponse['PropertyId'];
                $data1['unit_id'] = $data['unit_list'];
                $data1['address1'] = $data['address1'];
                $data1['address2'] = $data['address2'];
                $data1['city'] = $propertyReponse['City'];
                $data1['state'] = $propertyReponse['State'];
                $data1['zipcode'] = $propertyReponse['Zip'];
                $data1['sm_property_id'] = $propertyReponse['PropertyId'];
                $data1['property_identifier'] = $propertyReponse['PropertyIdentifier'];
                $data1['organization_id'] = $propertyReponse['OrganizationId'];
                if ($propertyReponse['Active'] === true) {
                    $data1['sm_active'] = 1;
                }else{
                    $data1['sm_active'] = 0;
                }
                $data1['sm_name'] = $propertyReponse['Name'];
                $data1['sm_street'] = $propertyReponse['Street'];
                $data1['sm_phone'] = $propertyReponse['Phone'];
                $data1['phone_extension'] = $propertyReponse['PhoneExtension'];
                $data1['unit_number'] = $propertyReponse['UnitNumber'];
                $data1['sm_ir'] = $propertyReponse['IR'];
                $data1['include_medical_collections'] = $propertyReponse['IncludeMedicalCollections'];
                $data1['include_foreclosures'] = $propertyReponse['IncludeForeclosures'];
                $data1['open_bankruptcy_window'] = $propertyReponse['OpenBankruptcyWindow'];
                $data1['is_fcra_agreement_accepted'] = $propertyReponse['IsFcraAgreementAccepted'];
                $data1['decline_for_open_bankruptcies'] = $propertyReponse['DeclineForOpenBankruptcies'];
                $data1['landlord_firstName'] = $propertyReponse['Landlord']['FirstName'];
                $data1['landlord_lastName'] = $propertyReponse['Landlord']['LastName'];
                $data1['landlord_streetAddressLineOne'] = $propertyReponse['Landlord']['StreetAddressLineOne'];
                $data1['landlord_streetAddressLineTwo'] = $propertyReponse['Landlord']['StreetAddressLineTwo'];
                $data1['landlord_city'] = $propertyReponse['Landlord']['City'];
                $data1['landlord_state'] = $propertyReponse['Landlord']['State'];
                $data1['landlord_zip'] = $propertyReponse['Landlord']['Zip'];
                $data1['landlord_phoneNumber'] = $propertyReponse['Landlord']['PhoneNumber'];
                $data1['landlord_email'] = $propertyReponse['Landlord']['Email'];
                $data1['landlord_isCompany'] = $propertyReponse['Landlord']['IsCompany'];

                //Save Data in Company Database
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO `sm_property` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                return array('code' => 200, 'status' => 'success', 'data' => $data1,'message' => 'Record saved successfully');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function getSMPropertyIdFromDB(){
        $propertyId = $_POST['property_db_id'];
        $unitId = $_POST['unit_db_id'];
        $sql = "SELECT sm_property_id FROM `sm_property` WHERE property_id=$propertyId AND unit_id=$unitId";
        $record = $this->companyConnection->query($sql)->fetch();
        if (!empty($record)) {
            return array('code' => 200, 'status' => 'success', 'data' => $record,'message' => 'Record fetched successfully');
        }
        return array('code' => 503, 'status' => 'error', 'data' => '','message' => 'No record found!');
    }

    public function getSMPropertyId(){
        $data = $_POST['form'];
        $data = postArray($data);
        $propertyUniueId = $data['app_property_id'];
        $unit_id = $data['unit'];
        $tent_id = $_POST['tent_id'];
        
        $sql = "SELECT * FROM `sm_property` WHERE property_unique_id='".$propertyUniueId."' AND unit_id='".$unit_id."'";
        $record = $this->companyConnection->query($sql)->fetch();

        $sql1 = "SELECT unit_prefix,unit_no FROM `unit_details` WHERE id=$unit_id";
        $record1 = $this->companyConnection->query($sql1)->fetch();

        $sql2 = "SELECT term,tenure FROM `tenant_lease_details` WHERE user_id=$tent_id";
        $record2 = $this->companyConnection->query($sql2)->fetch();
        $termTenure = $record2['term'] * $record2['tenure'];
        $unitNumber = $record1['unit_prefix']."-".$record1['unit_no'];
        if (!empty($record)) {
            $record['unit'] = $unitNumber; // S-2
            $record['termTenure'] = $termTenure; // S-2
            $record['tenant_email'] = $data['tenant_email']; // grahem@yopmail.com
            $record['additional_tenants_emails'] = $data['additional_tenants_emails']; // struat@yopmail.com, davis@yopmail.com
            $record['guarantors_emails'] = $data['guarantors_emails']; // 
            $record['pay_by'] = $data['pay_by']; // 2
            return array('code' => 200, 'status' => 'success', 'data' => $record,'message' => 'Record fetched successfully');
        }
        return array('code' => 503, 'status' => 'error', 'data' => '','message' => 'No record found!');
    }

    public function saveApplication(){
        try{
            $data = $_POST['form'];
            
            $data1['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data1['tenant_id'] = $_POST['tenantId'];
            $data1['product_bundle'] = $data['ProductBundle'];
            $data1['applicants'] = serialize($data['Applicants']);
            $data1['application_id'] = $data['ApplicationId'];
            $data1['landlord_pays'] = $data['LandlordPays'];
            $data1['property_id'] = $data['PropertyId'];
            $data1['rent'] = $data['Rent'];
            $data1['deposit'] = $data['Deposit'];
            $data1['lease_term'] = $data['LeaseTermInMonths'];
            $data1['unit_number'] = $data['UnitNumber'];
            $data1['currenttime'] = date('Y-m-d H:i:s');
            $data1['cancel_status'] = 0;
            $data1['status'] = 0;
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO `sm_application` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            $return = array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Application saved successfully');
        }catch (Exception $exception){
            $return = array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
        echo json_encode($return); die();
    }


}
$smartMove = new SmartMove();