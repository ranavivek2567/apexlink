<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");

class TenantPortal extends DBConnection{

    public function __construct(){
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function getPortalInfo(){
          $tenant_id                 = $_REQUEST['tenant_id'];
          $propertyData = $this->getPropertyData($tenant_id);
          $property_id = $propertyData['property_id']; 
          $building_id = $propertyData['building_id'];
          $unit_id     = $propertyData['unit_id'];

          $propInfo = $this->getPropInfo($property_id);
            if($propInfo['address1'] == ""){ 
            $data['address1'] = "N/A";
        }else{
            $data['address1'] = $propInfo['address1']; 
        }
        if($propInfo['address2'] != ""){
            $data['address2']          = $propInfo['address2'];
        }else{
            $data['address2']          = 'N/A';
        }
        if($propInfo['address3'] != ""){
            $data['address3']          = $propInfo['address3'];
        }else{
            $data['address3']          = 'N/A';
        }
        if($propInfo['address4'] != ""){
            $data['address4']          = $propInfo['address4'];
        }else{
            $data['address4']          = 'N/A';
        }

         if($propInfo['zipcode'] != ""){
            $data['zipcode']          = $propInfo['zipcode'];
        }else{
            $data['zipcode']          = 'N/A';
        }
        
        if($propInfo['city'] != ""){
            $data['city']          = $propInfo['city'];
        }else{
            $data['city']          = 'N/A';
        }

        if($propInfo['state'] != ""){
            $data['state']          = $propInfo['state'];
        }else{
            $data['state']          = 'N/A';
        }


        $getTenantInfo             = $this->getPropertyInfo($tenant_id);
        $data['property_name']     = $getTenantInfo['property_name'];
        $unitName = "";
        if ($getTenantInfo['unit_prefix'] == "" && $getTenantInfo['unit_no'] != "") {
            $unitName  = $getTenantInfo['unit_no'];
        }else if ($getTenantInfo['unit_prefix'] != "" && $getTenantInfo['unit_no'] == "") {
            $unitName  = $getTenantInfo['unit_prefix'];
        }else{
            $unitName  = $getTenantInfo['unit_prefix']."-".$getTenantInfo['unit_no'];;
        }
        $data['building_unit']     = $getTenantInfo['building_name'] . ' ' . $unitName;
        $getLeaseInfo              = $this->getLeaseInfo($tenant_id);
        $data['rent']              = $getLeaseInfo['rent_amount'];
        $data['security_deposite'] = $getLeaseInfo['security_deposite'];
        $data['notice_period']     = $getLeaseInfo['notice_period'];
        $data['move_in'] =           dateFormatUser($getLeaseInfo['move_in'], $tenant_id, $this->companyConnection);
        $data['start_date']        = dateFormatUser($getLeaseInfo['start_date'], $tenant_id, $this->companyConnection);
        $data['end_date']          = dateFormatUser($getLeaseInfo['end_date'], $tenant_id, $this->companyConnection);
        $data['nsf']               = 100;
        $data['balance_due']       = 100;
        $getContactInfo            = $this->getContactInfo($tenant_id);


      
        
        
        $data['fullname']          = $getContactInfo['first_name'].' '.$getContactInfo['last_name'];
        $data['company_name']      = $getContactInfo['first_name'];

       
        $data['email']          = $getContactInfo['email1'];
     

        
       

        if($getContactInfo['phone_number'] != ""){
            $data['phone_number']          = $getContactInfo['phone_number'];
        }else{
            $data['phone_number']          = 'N/A';
        }
        
        $data['tenant_image']      = $getContactInfo['tenant_image'];
        $data['emergencyInfo']     = $this->getEmergencyInfo($tenant_id);
        $data['viewEmergencyInfo'] = $this->getViewEmergencyInfo($tenant_id);
        $today = date('Y-m-d H:i:s');
        $data['current_date'] = dateFormatUser($today, $tenant_id, $this->companyConnection);
        return $data;
    }



      public function getPropInfo($property_id)
    {
     return $this->companyConnection->query("SELECT property_name,address1,address2,address3,address4,state,city,zipcode FROM general_property WHERE id ='".$property_id."'")->fetch(); 
      
    }

    public function getContactInfo($tenant_id){
        return $this->companyConnection->query("SELECT u.*,td.tenant_contact,td.tenant_image,td.email1,td.email2,td.email3,td.vehicle,td.pet,td.animal,td.medical_allergy,td.guarantor,td.collection,td.smoker,td.movein_key_signed,td.status,td.tenant_license_state,td.tenant_license_number FROM users as u inner join tenant_details as td on u.id=td.user_id  WHERE u.id ='" . $tenant_id . "'")->fetch();

    }

    public function getPropertyInfo($tenant_id){
        $getPropertyInfo = $this->companyConnection->query("SELECT tp.property_id,tp.building_id,tp.unit_id,gp.property_name,bd.building_name,ud.unit_prefix,ud.unit_no from tenant_property as tp inner join general_property as gp on gp.id=tp.property_id inner join building_detail as bd on bd.id=tp.building_id inner join unit_details as ud on ud.id=tp.unit_id where tp.user_id = '" . $tenant_id . "'")->fetch();
        return $getPropertyInfo;

    }


    public function getEmergencyInfo($tenant_id)
    {
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $tenant_id . "' and parent_id=0")->fetchAll();
        $countryCode      = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html             = "";
        if (empty($getEmergencyInfo)) {
            $html .= '<form id="editEmergency"><div class="row tenant-emergency-contact">
                                                    <div class="form-outer">

                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Emergency Contact Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="emergency" name="emergency_contact_name[]" maxlength="18">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Relationship</label>
                                                            <select class="form-control" id="relationship"
                                                                    name="emergency_relation[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Daughter</option>
                                                                <option value="2">Father</option>
                                                                <option value="3">Friend</option>
                                                                <option value="4">Mother</option>
                                                                <option value="5">Owner</option>
                                                                <option value="6">Partner</option>
                                                                <option value="7">Son</option>
                                                                <option value="8">Spouse</option>
                                                                <option value="9">Other</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 countycodediv">
                                                            <label>Country Code</label>
                                                            <select class="form-control" name="emergency_country[]">';



                                                        foreach ($countryCode as $code) {
                                                      
                                                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "(".$code['code'].")". "</option>";
                                                       
                                                        }
                                                        $html .= '</select>';


                                                                $html .= '<span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">

                                                            <label>Phone</label>
                                                            <input class="form-control capsOn phone_format" type="text"
                                                                   id="phoneNumber"
                                                                   name="emergency_phone[]" maxlenght="12">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="clearfix"></div>

                                                    </div>

                                               
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Email</label>
                                                            <input class="form-control capsOn" type="text" 
                                                                   name="emergency_email[]" maxlength="100">
                                                                      <a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                                     <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    

                                                </div><input type="submit" class="blue-btn"></form>';


        } else {
            $i = 0;
            foreach ($getEmergencyInfo as $emergencyInfo) {
                $html .= '<form id="editEmergency"><div class="row tenant-emergency-contact">
                                                    <div class="col-sm-2">
                                                        <label>Emergency Contact Name</label>
                                              <input class="form-control" type="text" value="' . $emergencyInfo['emergency_contact_name'] . '" name="emergency_contact_name[]" maxlength="18">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Relationship</label>
                                                        <select class="form-control" name="emergency_relation[]">';
                $relationArr = array(
                    "" => "Select",
                    "1" => "Daughter",
                    "2" => "Father",
                    "3" => "Friend",
                    "4" => "Mother",
                    "5" => "Owner",
                    "6" => "Partner",
                    "7" => "Son",
                    "8" => "Spouse",
                    "9" => "Other"
                );
                foreach ($relationArr as $key => $value) {
                    if ($key == $emergencyInfo['emergency_relation']) {
                        $html .= '<option value="' . $key . '" selected>' . $value . '</option>';
                    } else {
                        $html .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                }

                $html .= '</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="emergency_country[]">';
                foreach ($countryCode as $code) {
                    if ($code['id'] == $emergencyInfo['emergency_country_code']) {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] ."(".$code['code'].")". "</option>";
                    } else {
                           $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "(".$code['code'].")". "</option>";
                    }
                }
                $html .= '</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Phone Number</label>
                                            <input class="form-control phone_format" type="text" maxlength="12" name="emergency_phone[]" value="' . $emergencyInfo['emergency_phone_number'] . '">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Email</label>
                                                        <input class="form-control add-input" type="text" value="' . $emergencyInfo['emergency_email'] . '" name="emergency_email[]" maxlength="100">';

                if ($i != 0) {


                    $html .= '<a class="add-icon remove-emergency-contant"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                } else {
                    $html .= '<a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>';

                    $html .= '<a class="add-icon remove-emergency-contant" style="display:none"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                }


                $html .= '</div>
                                </div></div>';

                $i++;
            }
            $html .= '<input type="submit" class="blue-btn editEmergency">
            </form>';
        }

        return $html;

    }





    public function getViewEmergencyInfo($tenant_id)
    {
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $tenant_id . "' and parent_id=0")->fetchAll();
        $countryCode      = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html             = "";



        if (empty($getEmergencyInfo)) {

            $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                    <span></span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Email :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                    <span></span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';

        } else {

            foreach ($getEmergencyInfo as $emergencyInfo) {

                $countryCode    = $emergencyInfo['emergency_country_code'];
                $getCountryCode = $this->companyConnection->query("SELECT * FROM countries WHERE id ='" . $countryCode . "'")->fetch();
                $code           = $getCountryCode['code'];

                $relation = $emergencyInfo['emergency_relation'];
                if ($relation == 1) {
                    $emergencyRelation = 'Daughter';
                } else if ($relation == 2) {
                    $emergencyRelation = 'Father';
                } else if ($relation == 3) {
                    $emergencyRelation = 'Friend';
                } else if ($relation == 4) {
                    $emergencyRelation = 'Mother';
                } else if ($relation == 5) {
                    $emergencyRelation = 'Owner';
                } else if ($relation == 6) {
                    $emergencyRelation = 'Partner';
                } else if ($relation == 7) {
                    $emergencyRelation = 'Son';
                } else if ($relation == 8) {
                    $emergencyRelation = 'Spouce';
                } else {
                    $emergencyRelation = 'Other';
                }








                $getEmergencyRelation = $this->companyConnection->query("SELECT * FROM countries WHERE id ='" . $countryCode . "'")->fetch();

                $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span>' . $emergencyInfo['emergency_contact_name'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span>' . $code . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                     <span>' . $emergencyInfo['emergency_phone_number'] . '</span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Emaile :</label>
                                                                   <span>' . $emergencyInfo['emergency_email'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                     <span>' . $emergencyRelation . '</span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';
            }


        }

        return $html;

    }



    public function getLeaseInfo($tenant_id)
    {
        $getLeaseInfo = $this->companyConnection->query("SELECT tld.move_in,tld.move_out,tld.start_date,tld.end_date,tld.notice_period,tld.rent_amount,tld.security_deposite from tenant_lease_details as tld where tld.user_id = '" . $tenant_id . "'")->fetch();
        return $getLeaseInfo;


    }

    public function editEmergency()
    {
        $tenant_id = $_POST['tenant_id'];
        $count     = $this->companyConnection->prepare("DELETE FROM emergency_details WHERE  parent_id=0 and user_id=$tenant_id");
        $count->execute();
        try {



            $i = 0;
            foreach ($_POST['emergency_contact_name'] as $credentialName) {

                $data['user_id']                = $tenant_id;
                $data['parent_id']              = 0;
                $data['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $data['emergency_relation']     = $_POST['emergency_relation'][$i];
                $data['emergency_country_code'] = $_POST['emergency_country'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $data['emergency_email']        = $_POST['emergency_email'][$i];


                $sqlData = createSqlColVal($data);
                $query   = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt    = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;

            }

            return array(
                'status' => 'success',
                'message' => 'Record updated Successfully.',
                'function' => 'editEmergency'
            );
        }
        catch (PDOException $e) {
            return array(
                'code' => 400,
                'status' => 'failed',
                'message' => $e->getMessage(),
                'function' => 'editEmergency'
            );
            printErrorLog($e->getMessage());
        }


    }



    public function editTenantImage()
    {
        $tenant_id = $_POST['tenant_id'];
        $image = json_decode($_POST['tenant_image']);
        $query = "UPDATE tenant_details SET tenant_image='$image' where user_id=".$tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array("message"=>"Image Updated Successfully","status"=>"success","function"=>'editTenantImage');

    }


    public function changePassword()
    {
        $tenant_id = $_POST['tenant_id'];
        $getUserPassword = $this->getUserPassword($tenant_id);
        $current_password = md5($_POST['current_password']);
        $new_password = md5($_POST['new_password']);
        $confirm_password = md5($_POST['confirm_password']);
        $actualPasseword = $_POST['confirm_password'];


        if($current_password!=$getUserPassword)
        {
            return array('message'=>'Current password is not matching with User Password','status'=>'error');
        }


        if($new_password!=$confirm_password)
        {
            return array('message'=>'New Password and Confirm Password are not same','status'=>'error');
        }


        $query = "UPDATE users SET password='$new_password',actual_password='$actualPasseword' where id=".$tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array("message"=>"Password is updated successfully","status"=>"success","function"=>'editTenantImage');

    }



    public function getUserPassword($id)
    {
        $getPassword = $this->companyConnection->query("SELECT password FROM users WHERE id ='".$id."'")->fetch();
        return $getPassword['password'];



    }



    public function checkSceretKey()
    {
        $tenant_id = $_POST['id'];
        $secretKey = $_POST['secretKey'];


        $getKey =  $this->companyConnection->query("SELECT reset_password_token FROM `users` where id =  $tenant_id and portal_password_key='$secretKey'")->fetch();



        if(empty($getKey))
        {
            return array('status'=>'error','message'=>'Link has been expire');
        }
        else
        {
            return array('status'=>'success','message'=>'Please reset your password');
        }


    }



    public function resetPassword()
    {
        $tenant_id = $_POST['tenant_id'];
        $new_password = md5($_POST['new_password']);
        $confirm_password = md5($_POST['confirm_password']);
        $actualPasseword = $_POST['confirm_password'];

        if($new_password!=$confirm_password)
        {
            return array('message'=>'New Password and Confirm Password are not same','status'=>'error');
        }


        $query = "UPDATE users SET password='$new_password',actual_password='$actualPasseword',portal_password_key='' where id=".$tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array("message"=>"Password has been setup successfully","status"=>"success","function"=>'editTenantImage');

    }


    public function portalLogin()
    {
        try {
            $email = $_POST['email'];
            $password = md5($_POST['password']);
            $checkLogin = $this->companyConnection->query("SELECT * FROM `users` WHERE email ='".$email."' AND password= '".$password."' AND status='1' AND portal_status='1'" )->fetch();
            if(!empty($checkLogin)) {
                $portal = '';
                $status = 'success';
                $message = 'Successfully logged in to ';
                $url = '';
                $user_id = $checkLogin['id'];
                switch ($checkLogin['user_type']) {
                    case "2":
                        $url = '/Tenant-portal?tenant_id='.$user_id;
                        $message .= 'Tenant Portal.';
                        $portal = 'Tenant_Portal';
                        $_SESSION[SESSION_DOMAIN][$portal]['tenant_portal_id'] = $user_id;
                        break;
                    case "3":
                        if(isset($_SESSION['Admin_Access'])) {
                            unset($_SESSION['Admin_Access']);
                        }
                        $url = '/VendorPortal/Vendor/MyAccount';
                        $message .= 'Vendor Portal.';
                        $portal = 'Vendor_Portal';
                        $_SESSION[SESSION_DOMAIN][$portal]['vendor_portal_id'] = $user_id;
                        break;
                    case "4":
                        $url = '/Owner/MyAccount/AccountInfo';
                        $message .= 'Owner Portal.';
                        $portal = 'Owner_Portal';
                        $_SESSION[SESSION_DOMAIN][$portal]['owner_portal_id'] = $user_id;
                        break;
                    default:
                        $status = 'error';
                        $message = 'Invalid User!';
                        $user_id = '';
                        break;
                }
                if(!empty($user_id)){

                    if (!empty($_POST["remember"]) && $_POST["remember"] == 'on') {
                        $remember_login_data['email'] = $checkLogin["email"];
                        $remember_login_data['password'] = $checkLogin["actual_password"];
                        $remember_login_data['remember_token'] = $checkLogin["remember_token"];

                        $expire = time() + 60 * 60 * 24 * 30; // expires in one month
                        setcookie("remember_login", serialize($remember_login_data), $expire);
                    } else {
                        setcookie("remember_login", '');
                    }

                    $_SESSION[SESSION_DOMAIN][$portal]['portal_id'] = $user_id;
                    $_SESSION[SESSION_DOMAIN][$portal]['default_name'] = $checkLogin['first_name'].' '.$checkLogin['last_name'];
                    $_SESSION[SESSION_DOMAIN][$portal]['name'] = userName($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['company_name'] = $checkLogin['company_name'];
                    $_SESSION[SESSION_DOMAIN][$portal]['email'] = $checkLogin['email'];
                    $_SESSION[SESSION_DOMAIN][$portal]['phone_number'] = $checkLogin['phone_number'];
                    $_SESSION[SESSION_DOMAIN][$portal]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $checkLogin['id'], $this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['datepicker_format'] = getDatePickerFormat($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['company_logo'] = getCompanyLogo($this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['super_admin_name'] = isset($_POST['super_admin_name']) ? $_POST['super_admin_name'] : '';
                    //Default setting session data
                    $_SESSION[SESSION_DOMAIN][$portal]['default_notice_period'] = defaultNoticePeriod($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_rent'] = defaultRent($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_application_fee'] = defaultApplicationfee($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_zipcode'] = defaultZipCode($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_city'] = defaultCity($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_currency'] = defaultCurrency($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['default_currency_symbol'] = defaultCurrencySymbol($_SESSION[SESSION_DOMAIN][$portal]['default_currency'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['pagination'] = pagination($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['payment_method'] = defaultPaymentMethod($checkLogin['id'],$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN][$portal]['property_size'] = defaultPropertySize($checkLogin['id'],$this->companyConnection);
                }

                return array('status'=>$status,'message'=>$message,'portal_url'=>$url,'user_id'=>$user_id);
            }
            else
            {
                return array('status'=>'error','message'=>'Email/Password is incorrect');
            }
        } catch (PDOException $e) {
            dd($e);
            return array('code' => 400, 'status' => 'failed','message' => $e);
        }

    }



    public function logout()
    {
        $tenant_id = $_POST['id'];
        unset($_SESSION["tenant_id"]);
        return array('status'=>'success','message'=>'successfully logout');

    }


    public function sendMail()
    {
        $toUsers = $_POST['to_users'];
        $ccUsers = $_POST['cc_users'];
        $bccUsers = $_POST['bcc_users'];

        $userEmails = explode(",",$toUsers);
        $ccEmails = explode(",",$ccUsers);

        $bccUsers = explode(",",$bccUsers);

        $allUsers = array_merge($userEmails,$ccEmails,$bccUsers);


        $cc = $_POST['cc'];
        $bcc = $_POST['bcc'];
        $subject = $_POST['subject'];
        $path = $_POST['path'];

        $data= $_POST;
        $body =$data['body'];
        $domain = explode('.', $_SERVER['HTTP_HOST']);
        $subdomain = array_shift($domain);
        $user_detail = $_SESSION['SESSION_'.$subdomain];
        try{

            $request['action']  = 'SendMailPhp';
            $request['to']    = $allUsers;
            $request['subject'] = $subject;
            $request['message'] = $body;
            $request['attachment'] = $path;
            $request['portal']  = '1';
            curlRequest($request);

            return ['status'=>'success','code'=>200,'data'=>$request];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

    }



    public function getUsers()
    {
        $type = $_POST['type'];
        if($type=="tenant")
        {
            $user_type = 2;
        }
        else if($type=="owner")
        {
            $user_type = 4;
        }
        else if($type=="vendor")
        {
            $user_type = 3;
        }

        else if($type=="employee")
        {
            $user_type = 8;
        }

        else if($type=="guestCard")
        {
            $user_type = 6;
        }

        /* echo $user_type;
         exit;*/


        $getUsers = $this->companyConnection->query("SELECT id,first_name,email FROM users where user_type='$user_type'")->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['first_name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='getEmails' data-email='".$email."'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;

    }




    public function getCcUsers()
    {
        $type = $_POST['type'];
        if($type=="tenant")
        {
            $user_type = 2;
        }
        else if($type=="owner")
        {
            $user_type = 4;
        }
        else if($type=="vendor")
        {
            $user_type = 3;
        }

        else if($type=="employee")
        {
            $user_type = 8;
        }

        else if($type=="guestCard")
        {
            $user_type = 6;
        }

        /* echo $user_type;
         exit;*/


        $getUsers = $this->companyConnection->query("SELECT id,first_name,email FROM users where user_type='$user_type'")->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px' >";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['first_name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='getCcEmails' data-email='".$email."'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;

    }





    public function getBccUsers()
    {
        $type = $_POST['type'];
        if($type=="tenant")
        {
            $user_type = 2;
        }
        else if($type=="owner")
        {
            $user_type = 4;
        }
        else if($type=="vendor")
        {
            $user_type = 3;
        }

        else if($type=="employee")
        {
            $user_type = 8;
        }

        else if($type=="guestCard")
        {
            $user_type = 6;
        }

        /* echo $user_type;
         exit;*/


        $getUsers = $this->companyConnection->query("SELECT id,first_name,email FROM users where user_type='$user_type'")->fetchAll();

        $html = '';
        $html .= "<table class='table' border= '1px'>";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['first_name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='getBccEmails' data-email='".$email."'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;


    }




    public function getMaintenanceInfo()
    {

        $tenant_id = $_REQUEST['tenant_id'];
        $ticket_id = $_REQUEST['ticket_id'];

        $getData =$this->companyConnection->query("SELECT * FROM tenant_maintenance where id='$ticket_id'")->fetch();

        $data['tkt_type'] =   $getData['ticket_type'];
        $data['duration_type'] = $getData['duration_type'];
        $data['ticket_number'] = $getData['ticket_number'];
        $data['created_at'] =    dateFormatUser($getData['created_on'],$tenant_id,$this->companyConnection);
        $data['category'] =      $getData['category'];
        $data['start_date'] =    dateFormatUser($getData['start_date'],$tenant_id,$this->companyConnection);
        $data['end_date'] =      dateFormatUser($getData['end_date'],$tenant_id,$this->companyConnection);
        $data['priority'] =      $getData['priority'];
        $data['estimate_cost'] = $getData['estimate_cost'];
        $data['required_matirial'] = $getData['required_matirial'];
        $data['vendor_instruction'] = $getData['vendor_instruction'];
        $data['description'] = $getData['description'];
        $data['pro_id'] = $getData['property_id'];
        $data['un_id'] = $getData['unit_id'];
        $data['name'] = $getData['name'];
        $data['phone'] = $getData['phone'];
        $data['assign_to'] = $getData['assign_to'];
        $data['ticket_image1'] = $getData['image1'];
        $data['ticket_image2'] = $getData['image2'];
        $data['ticket_image3'] = $getData['image3'];
        $data['description'] = $getData['note'];
        $data['property_id'] = $getData['property_id'];
        $data['unit_id'] = $getData['unit_id'];
        $data['assign'] = $getData['assign_to'];


        $tenantInfo = $this->companyConnection->query("SELECT first_name,last_name,phone_number FROM users where id='$tenant_id'")->fetch();

        $data['name'] = $tenantInfo['first_name'].' '.$tenantInfo['last_name'];
        $data['phone'] = $tenantInfo['phone_number'];

        return $data;




    }









   public function getPropertyData($tenant_id)
    {
       return $this->companyConnection->query("SELECT * FROM tenant_property WHERE user_id ='".$tenant_id."'")->fetch();

    }


      public function getProp()
    {
        $propertyData =$this->companyConnection->query("SELECT id,property_id, property_name FROM general_property")->fetchAll();

        if(!empty($propertyData))
        {

            return array('data'=>$propertyData,'status'=>'success');
        }
        else
        {
            return array('status'=>'error');
        }
    }



    public function getUnitData()
    {

        $property_id = $_REQUEST['property_id'];
        $unitData =$this->companyConnection->query("SELECT id,unit_no,unit_prefix FROM unit_details where property_id='$property_id' and status='1'")->fetchAll();

        $getTenantData  = $this->companyConnection->query("SELECT id,unit_no,unit_prefix FROM unit_details where property_id='$property_id' and status='1'")->fetchAll();

        if(!empty($unitData))
        {

            return array('data'=>$unitData,'status'=>'success');
        }
        else
        {
            return array('status'=>'error');
        }





    }



    public function getUserDataByUnit()
    {
        $unit_id = $_REQUEST['unit_id'];
        $getTenantId  = $this->companyConnection->query("SELECT id,user_id FROM tenant_property where unit_id='$unit_id'")->fetch();
        $tenant_id = $getTenantId['user_id'];
        $getTenantData =  $this->companyConnection->query("SELECT id,first_name,last_name,phone_number FROM users where id='$tenant_id'")->fetch();
        $name= $getTenantData['first_name'].' '.$getTenantData['last_name'];
        $phone = $getTenantData['phone_number'];
        return array('name'=>$name,'phone'=>$phone);

    }






    public function addTicket()
    {

        $data['user_id'] = $_POST['tenant_id'];
        $data['ticket_type'] = $_POST['ticket_type'];
        $data['duration_type'] = $_POST['duration_type'];
        $data['ticket_number'] = $_POST['ticket_number'];
        $data['created_on'] =    mySqlDateFormat($_POST['created_on'],$_POST['tenant_id'],$this->companyConnection);
        $data['category']   =    $_POST['category'];
        $data['start_date'] =    mySqlDateFormat($_POST['start_date'],$_POST['tenant_id'],$this->companyConnection);
        $data['end_date']   =    mySqlDateFormat($_POST['end_date'],$_POST['tenant_id'],$this->companyConnection);
        $data['priority']   =   $_POST['priority'];
        $data['estimate_cost'] = $_POST['estimate_cost'];
        $data['required_matirial'] = $_POST['required_matirial'];
        $data['vendor_instruction'] = $_POST['vendor_instruction'];
        $data['description'] = $_POST['description'];
        $data['property_id'] = $_POST['property_id'];
        $data['unit_id'] = $_POST['unit_id'];
        $data['name'] = $_POST['name'];
        $data['phone'] = $_POST['phone'];
        $data['assign_to'] = $_POST['assign_to'];
        $data['note'] = $_POST['notes'];


        $data['image1'] = json_decode($_POST['ticket_image1']);
        $data['image2'] = json_decode($_POST['ticket_image2']);
        $data['image3'] = json_decode($_POST['ticket_image3']);
        $data['status'] = 1;


        $sqlData = createSqlColVal($data);
        $query = "INSERT INTO tenant_maintenance(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($data);

        $domain = getDomain();
        $getCompanyId = $this->companyConnection->query("SELECT admindb_id FROM users WHERE id ='1'")->fetch();
        $company_id = $getCompanyId['admindb_id'];
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $company_id;
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($_FILES['file_library'])) {
            $countFiles = count($_FILES['file_library']['name']);

            if ($countFiles != 0) {
                $files = $_FILES['file_library'];
                $i = 0;
                if (isset($_FILES['file_library'])) {
                    foreach ($files['name'] as $key => $filename) {
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['user_id'] = $_POST['tenant_id'];
                            $data1['filename'] = $uniqueName;
                            $data1['type'] = 'T';
                            $data1['file_location'] = $path . '/' . $uniqueName;

                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;
                            $sqlData1 = createSqlColVal($data1);
                            $query1 = "INSERT INTO tenant_chargefiles(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query1);
                            $stmt1->execute($data1);
                            $tmp_name = $files["tmp_name"][$key];
                            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                //Directory does not exist, so lets create it.
                                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                            }
                            move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                        }
                        $i++;
                    }
                }
            }
        }



        return array('message'=>'Ticket ','status'=>'success');


    }




    public function editTicket()
    {
        $ticket_id = $_POST['ticket_id'];
        $tenant_id = $_POST['tenant_id'];

        $data['user_id'] =       $_POST['tenant_id'];
        $data['ticket_type'] =   $_POST['ticket_type'];
        $data['duration_type'] = $_POST['duration_type'];
      
        $data['ticket_number'] = $_POST['ticket_number'];
        $data['created_on'] =    mySqlDateFormat($_POST['created_on'],$tenant_id,$this->companyConnection);
        $data['category']   =    $_POST['category'];
        $data['start_date'] =    mySqlDateFormat($_POST['start_date'],$tenant_id,$this->companyConnection);
        $data['end_date']   =    mySqlDateFormat($_POST['end_date'],$tenant_id,$this->companyConnection);
        $data['priority']   =    $_POST['priority'];
        $data['estimate_cost'] = $_POST['estimate_cost'];
        $data['required_matirial'] = $_POST['required_matirial'];
        $data['vendor_instruction'] = $_POST['vendor_instruction'];
        $data['description'] = $_POST['description'];
        $data['property_id'] = $_POST['property_id'];
        $data['unit_id'] = $_POST['unit_id'];
        $data['name'] = $_POST['name'];
        $data['phone'] = $_POST['phone'];
        $data['assign_to'] = $_POST['assign_to'];
        $data['note'] = $_POST['notes'];


        $data['image1'] = json_decode($_POST['ticket_image1']);

        $data['image2'] = json_decode($_POST['ticket_image2']);
        $data['image3'] = json_decode($_POST['ticket_image3']);



        $sqlData  = createSqlColValPair($data);

        $query = "UPDATE tenant_maintenance SET " . $sqlData['columnsValuesPair'] ." where id=".$ticket_id;

        $stmt = $this->companyConnection->prepare($query);
        $editData = $stmt->execute();


        return array('code' => 200, 'status' => 'success','table' => "tenant_maintenance");

    }





    public function insertChargeFileNote()
    {
        $tenant_id = $_POST['tenant_id'];
        if (isset($_POST['chargeNote'])) {
            $chargeNote = $_POST['chargeNote'];
            $i = 0;
            foreach ($chargeNote as $note) {
                if ($note != "") {
                    $data['user_id'] = $tenant_id;
                    $data['note'] = $note;
                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO tenant_chargenote(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                $i++;
            }
        }
        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $getCompanyId = $this->companyConnection->query("SELECT admindb_id FROM users WHERE id ='1'")->fetch();
        $company_id = $getCompanyId['admindb_id'];






        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $company_id;
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($_FILES['file_library'])) {
            $countFiles = count($_FILES['file_library']['name']);

            if ($countFiles != 0) {
                $files = $_FILES['file_library'];
                $i = 0;
                if (isset($_FILES['file_library'])) {
                    foreach ($files['name'] as $key => $filename) {
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['user_id'] = $tenant_id;
                            $data1['filename'] = $uniqueName;
                            $data1['file_location'] = $path . '/' . $uniqueName;

                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;
                            $sqlData1 = createSqlColVal($data1);
                            $query1 = "INSERT INTO tenant_chargefiles(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query1);
                            $stmt1->execute($data1);
                            $tmp_name = $files["tmp_name"][$key];
                            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                //Directory does not exist, so lets create it.
                                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                            }
                            move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                        }
                        $i++;
                    }
                }
            }
        }
        return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_chargenote');
    }





    public function insertMaintenanceFile()
    {
        $tenant_id = $_POST['tenant_id'];

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $getCompanyId = $this->companyConnection->query("SELECT admindb_id FROM users WHERE id ='1'")->fetch();
        $company_id = $getCompanyId['admindb_id'];






        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $company_id;
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($_FILES['file_library'])) {
            $countFiles = count($_FILES['file_library']['name']);

            if ($countFiles != 0) {
                $files = $_FILES['file_library'];
                $i = 0;
                if (isset($_FILES['file_library'])) {
                    foreach ($files['name'] as $key => $filename) {
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['user_id'] = $tenant_id;
                            $data1['filename'] = $uniqueName;
                            $data1['file_location'] = $path . '/' . $uniqueName;

                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;
                            $data1['type'] = 'T';
                            $sqlData1 = createSqlColVal($data1);
                            $query1 = "INSERT INTO tenant_chargefiles(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query1);
                            $stmt1->execute($data1);
                            $tmp_name = $files["tmp_name"][$key];
                            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                //Directory does not exist, so lets create it.
                                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                            }
                            move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                        }
                        $i++;
                    }
                }
            }
        }
        return array('status' => 'success', 'message' => 'Record added successfully.', 'table' => 'tenant_chargenote');
    }



    public function changeTicketStatus()
    {
        $id = $_POST['ticket_id'];

        $query = "UPDATE tenant_maintenance SET status='0' where id=".$id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array('status'=>'success','message'=>'Ticket canceled successfully.');
    }




    public function getViewMaintenanceInfo()
    {

        $tenant_id = $_REQUEST['tenant_id'];

        $ticket_id = $_REQUEST['ticket_id'];

        $getData =$this->companyConnection->query("SELECT * FROM tenant_maintenance where id='$ticket_id'")->fetch();



        $data['ticket_type'] =   $getData['ticket_type'];
        $data['duration_type'] = $getData['duration_type'];
        $data['ticket_number'] = $getData['ticket_number'];
        $data['created_on'] =    dateFormatUser($getData['created_on'],$tenant_id,$this->companyConnection);
        $data['category'] =      $getData['category'];
        $data['start_date'] =    dateFormatUser($getData['start_date'],$tenant_id,$this->companyConnection);
        $data['end_date'] =      dateFormatUser($getData['end_date'],$tenant_id,$this->companyConnection);
        $data['priority'] =      $getData['priority'];
        $data['estimate_cost'] = $getData['estimate_cost'];
        $data['required_materials'] = $getData['required_matirial'];
        $data['vendor_instruction'] = $getData['vendor_instruction'];
        $data['description'] = $getData['description'];
        $data['pro_id'] = $getData['property_id'];
        $data['un_id'] = $getData['unit_id'];
        $data['name'] = $getData['name'];
        $data['phone'] = $getData['phone'];
        $data['assign_to'] = $getData['assign_to'];
        $data['ticket_image1'] = $getData['image1'];
        $data['ticket_image2'] = $getData['image2'];
        $data['ticket_image3'] = $getData['image3'];
        $data['notes'] = $getData['note'];
        $property_id = $getData['property_id'];
        $unit_id = $getData['unit_id'];

         $unit_id;


        $tenantInfo = $this->companyConnection->query("SELECT first_name,last_name,phone_number FROM users where id='$tenant_id'")->fetch();

        //dd($tenantInfo);

        $data['name'] = $tenantInfo['first_name'].' '.$tenantInfo['last_name'];
        $data['phone'] = $tenantInfo['phone_number'];

        $propertyData = $this->companyConnection->query("SELECT property_name FROM general_property where id='$property_id'")->fetch();



        $data['property_name'] =  $propertyData['property_name'];

     

        $unitData = $this->companyConnection->query("SELECT unit_prefix,unit_no FROM unit_details where id='$unit_id'")->fetch();
      
        $data['building_unit'] =  $unitData['unit_prefix'] . ' ' . $unitData['unit_no'];

        return $data;





    }


    public function changeLeaseStatus()
    {
        $tenant_id =  $_POST['tenant_id'];
        $query = "UPDATE tenant_lease_details SET tenant_status='2',tenant_portal='1' where user_id=".$tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array("message"=>"Status Updated Successfully","status"=>"success");
    }



        public function getTenantPortal(){
        try{
            $tenant_id = $_POST['tenant_id'];
            $checkLogin =    $this->companyConnection->query("SELECT * FROM `users` WHERE user_type='2' AND status='1' AND id=".$tenant_id )->fetch();
            if(!empty($checkLogin)) {
                $user_id = $checkLogin['id'];
                $url = "/Tenant-portal?tenant_id=$tenant_id";
                $portal = 'Tenant_Portal';
                $_SESSION[SESSION_DOMAIN][$portal]['tenant_portal_id'] = $user_id;
                $_SESSION[SESSION_DOMAIN][$portal]['portal_id'] = $user_id;
                $_SESSION[SESSION_DOMAIN][$portal]['default_name'] = $checkLogin['first_name'].' '.$checkLogin['last_name'];
                $_SESSION[SESSION_DOMAIN][$portal]['name'] = userName($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['company_name'] = $checkLogin['company_name'];
                $_SESSION[SESSION_DOMAIN][$portal]['email'] = $checkLogin['email'];
                $_SESSION[SESSION_DOMAIN][$portal]['phone_number'] = $checkLogin['phone_number'];
                $_SESSION[SESSION_DOMAIN][$portal]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $checkLogin['id'], $this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['datepicker_format'] = getDatePickerFormat($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['company_logo'] = getCompanyLogo($this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['super_admin_name'] = isset($_POST['super_admin_name']) ? $_POST['super_admin_name'] : '';
                $_SESSION[SESSION_DOMAIN][$portal]['admin_url'] = $_SERVER['HTTP_REFERER'];
                //Default setting session data
                $_SESSION[SESSION_DOMAIN][$portal]['default_notice_period'] = defaultNoticePeriod($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_rent'] = defaultRent($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_application_fee'] = defaultApplicationfee($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_zipcode'] = defaultZipCode($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_city'] = defaultCity($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_currency'] = defaultCurrency($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_currency_symbol'] = defaultCurrencySymbol($_SESSION[SESSION_DOMAIN][$portal]['default_currency'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['pagination'] = pagination($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['payment_method'] = defaultPaymentMethod($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['property_size'] = defaultPropertySize($checkLogin['id'],$this->companyConnection);
                return array('status'=>'success','code'=>200,'portal_url'=>$url,'user_id'=>$user_id);
            } else {
                return array('status'=>'error','message'=>'Error occured  while redirecting!');
            }
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }




        public function getIntialData(){

        try{
            $data = [];
          
            $data['propertylist'] =$this->companyConnection->query("SELECT id,property_id, property_name FROM general_property")->fetchAll();
            $data['phone_type'] =$this->companyConnection->query("SELECT id,type FROM phone_type")->fetchAll();
            $data['carrier'] = $this->companyConnection->query("SELECT id,carrier FROM carrier")->fetchAll();
            $data['referral'] = $this->companyConnection->query("SELECT id,referral FROM tenant_referral_source")->fetchAll();
            $data['ethnicity'] = $this->companyConnection->query("SELECT id,title FROM tenant_ethnicity")->fetchAll();
            $data['hobbies'] = $this->companyConnection->query("SELECT * FROM hobbies")->fetchAll();
            $data['marital'] = $this->companyConnection->query("SELECT id,marital FROM tenant_marital_status")->fetchAll();
            $data['veteran'] = $this->companyConnection->query("SELECT id,veteran FROM tenant_veteran_status")->fetchAll();
            $data['collection_reason'] = $this->companyConnection->query("SELECT id,reason FROM tenant_collection_reason")->fetchAll();
            $data['credential_type'] = $this->companyConnection->query("SELECT id,credential_type FROM tenant_credential_type")->fetchAll();
            $data['country'] = $this->companyConnection->query("SELECT id,name,code FROM countries")->fetchAll();
            $data['state'] = $this->companyConnection->query("SELECT state FROM `users`")->fetch();
            $data['complaints'] = $this->companyConnection->query("SELECT id,complaint_type FROM `complaint_types`")->fetchAll();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }


    public function getOneYearDate()
    {
        $date = $_POST['date'];
        if($date!='')
        {
          $dateFormat =  mySqlDateFormat($date,$_POST['tenant_id'],$this->companyConnection);
          $newEndingDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($dateFormat)) . " + 1 year"));
          $newDate = dateFormatUser($newEndingDate, $tenant_id, $this->companyConnection);
          
          return $newDate;

        }

    }
     public function getNotice(){
        try{


                if (isset($_POST['tenant_id']) && $_POST['tenant_id'] != ""){
                    $tenant_id=$_POST['tenant_id'];
                  
                   $query = $this->companyConnection->query(" SELECT * FROM `give_notice` WHERE tenant_id ='$tenant_id'");
                    $data = $query->fetch();
                
                    

                    return ['status' => 'success', 'code' => 200, 'data' => $data, 'message' => 'Record fetched successfully.']; 
                }
               
            } catch (Exception $exception) {
                return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
                printErrorLog($e->getMessage());
            }
    }

public function giveNotice()
        {
            try{
                    if (isset($_POST['tenant_id']) && $_POST['tenant_id'] != ""){
                        $tenant_id = $_POST['tenant_id'];  
                        $data = $_POST; 
                        $query = $this->companyConnection->query(" SELECT * FROM `give_notice` WHERE tenant_id ='$tenant_id'");
                        $noticedata = $query->fetch();
                    
                    
                    if (!empty($noticedata)) {
                    
                    $id = $noticedata['id'];
                        
                            $data2 = [];
                            $data2['tenant_id'] = $data['tenant_id'];
                            $data2['notice_given_date'] = mySqlDateFormat($data['notice_given_date'], null, $this->companyConnection);
                            $data2['reason_for_leaving'] = $data['reasonForLeaving'];
                            $data2['scheduled_move_out'] = mySqlDateFormat($data['scheduled_move_out'], null,$this->companyConnection);
                            $data2['first_name'] = $data['first_name'];
                            $data2['last_name'] = $data['last_name'];
                            $data2['country'] = $data['country'];
                            $data2['address'] = $data['address'];
                            $data2['state'] = $data['state'];
                            $data2['city'] = $data['city'];
                            $data2['phone'] = $data['phone'];
                            $data2['zip_code'] = $data['zip_code'];
                            $data2['email'] = $data['email'];
                            
                            $sqlData = createSqlColValPair($data2);
                            $query = "UPDATE give_notice SET " . $sqlData['columnsValuesPair'] . " where id=" . $id;

                            $stmt = $this->companyConnection->prepare($query);
                            $stmt->execute();
                            $this->noticeNotification($tenant_id);
                         //  print_r($rr);die;
                            return array('code' => 200,'status' => 'success', 'message' => 'Data update Successfully.', 'table' => 'give_notice','dataNew'=>$data2);   

                    }

                    // insert
                    else
                        {
                            $data1 = [];
                            $data1['tenant_id'] = $data['tenant_id'];
                            $data1['notice_given_date'] = mySqlDateFormat($data['notice_given_date'], null, $this->companyConnection);
                            $data1['reason_for_leaving'] = $data['reasonForLeaving'];
                            $data1['scheduled_move_out'] = mySqlDateFormat($data['scheduled_move_out'], null,$this->companyConnection);
                            $data1['first_name'] = $data['first_name'];
                            $data1['last_name'] = $data['last_name'];
                            $data1['country'] = $data['country'];
                            $data1['address'] = $data['address'];
                            $data1['state'] = $data['state'];
                            $data1['city'] = $data['city'];
                            $data1['phone'] = $data['phone'];
                            $data1['zip_code'] = $data['zip_code'];
                            $data1['email'] = $data['email'];
                        //dd($data1);
                            $sqlData = createSqlColVal($data1);
                            $query = "INSERT INTO give_notice(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                            $stmt = $this->companyConnection->prepare($query);
                            $stmt->execute($data1);
                            $idd = $this->companyConnection->lastInsertId();
                           $this->noticeNotification($tenant_id);
                            return array('code' => 200,'status' => 'success', 'message' => 'Data insert Successfully.', 'table' => 'give_notice' , $idd => 'idd');   

                        }
                    
                    }
                }
                catch (PDOException $e) {
                        return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'give_notice');
                        printErrorLog($e->getMessage());
                    }
         
        }

        // notification starts
        public function noticeNotification($idd){
        $noticeNotifications = "SELECT u.name,tp.user_id,tp.property_id,tp.unit_id,CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name FROM `tenant_property` as tp JOIN unit_details as ud ON ud.id = tp.unit_id JOIN users as u ON u.id = tp.user_id where tp.user_id =".$idd;
        $querydata = $this->companyConnection->query($noticeNotifications)->fetch();
        $name = $querydata['name'];
        $tenantId = $querydata['user_id'];
        $propertyId=$querydata['property_id'];
        $unitname=$querydata['unit_name'];
        //echo $name;
        //echo $idd;
        // echo "jjdfj";
        // echo $tenantId;
        // echo $propertyId;
        // echo $unitname;
        // die;
        $dateCreated = "2019-03-03";
        $notificationTitle= "Tenant Give Notice";
        $module_type = "TENANT";
        $alert_type = "Real Time";
        $descriptionNotifi ='has been generated for tenant ID Tenant ID- '.$tenantId.'(PropertyID-'.$propertyId.',UnitID-'.$unitname.') on '.$dateCreated;
      //  print_r($descriptionNotifi);
      insertNotification($this->companyConnection,$idd,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);

    }



    // reason for leaving

    public function getReasonForLeavingDetail()
    {
        try{
            $query = $this->companyConnection->query("SELECT * FROM reasonmoveout_tenant");
            $settings = $query->fetchAll();
            //dd($settings);
            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');
        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function propInfo(){
        $id = $_POST['id'];
        $sql = "SELECT gp.id as propid,gp.property_name,gp.manager_id,ud.id as unitid,ud.unit_prefix,ud.unit_no,u.name,u.phone_number,tpp.phone_number FROM users u JOIN tenant_property tp ON u.id=tp.user_id JOIN general_property gp ON tp.property_id = gp.id JOIN unit_details ud ON tp.unit_id = ud.id JOIN tenant_phone tpp ON u.id=tpp.user_id";
        $userData = $this->companyConnection->query($sql)->fetch();
        if (!empty($userData)) {
            if (!empty($userData['manager_id'])) {
                $mangerId = unserialize($userData['manager_id']);
                $userData['manager_id'] = $mangerId[0];
                $userData['manager_name'] = userName($mangerId[0], $this->companyConnection,'users');
            }else{
                $userData['manager_id']='';
                $userData['manager_name'] = '';
            }
            return ['status' => 'success', 'code' => 200, 'data' => $userData, 'message' => 'Record fetched successfully.'];    
        }
        return ['status' => 'error', 'code' => 503, 'data' => '', 'message' => 'Record not found!'];
    }











}



$TenantPortal = new TenantPortal();