<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL."/config.php");

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");

class TenantTypeAjax extends DBConnection {

    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }

    public function getAllAddress(){
        $id = $_POST['id'];
        $data = [];
        $tenantInfo = $this->companyConnection->query("SELECT name FROM `users` WHERE id=$id")->fetch();
        $tenantProperties = $this->companyConnection->query("SELECT property_id,unit_id FROM `tenant_property` WHERE user_id='$id'")->fetch();
        $tenantProperty = $tenantProperties['property_id'];
        $tenantUnit = $tenantProperties['unit_id'];
        $data['unit'] = $this->companyConnection->query("SELECT * FROM unit_details WHERE id='$tenantUnit'")->fetch();
        $data['property'] = $this->companyConnection->query("SELECT address1,address2,address3,address4,city,state,zipcode FROM `general_property` WHERE id='$tenantProperty'")->fetch();
        $data['name'] = $tenantInfo['name'];
        $data['company'] = $this->companyConnection->query("SELECT name,address1,address2,address3,address4,city,state,zipcode FROM `users` WHERE id=1")->fetch();
        return $data;



    }

    /**
     * Import Tenant type Excel
     * @return array
     * @throws PHPExcel_Exception
     */
    public function importExcel(){
        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                if(in_array($ext, $allowedExtensions)) {
                    $file_size = $_FILES['file']['size'] / 1024;
                    if($file_size < 3000) {
                        $file = ROOT_URL."/uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);
                        if($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL. "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                //Load the excel(.xls/.xlsx/ .csv) file
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                                printErrorLog($e->getMessage());
                            }
                            //An excel file may contains many sheets, so you have to specify which one you need to read or work with.
                            $sheet = $objPHPExcel->getSheet(0);
                            //It returns the highest number of rows
                            $total_rows = $sheet->getHighestRow();
                            //It returns the first element of 'A1'
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
                            //It returns the highest number of columns
                            $total_columns = $sheet->getHighestColumn();
                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $extra_columns = ",`user_id`, `status`, `created_at`, `updated_at`";
                            $status = 1;
                            $extra_values = ",'" . $login_user_id . "','" . $status . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
                            //Loop through each row of the worksheet
                            $rowData = array();
                            for ($row = 0; $row <= $total_rows; $row++){
                                //  Read a row of data into an array
                                $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
                            }
                            $rowRecords = array();
                            $postRecords = array();
                            for($i = 2; $i < count($rowData); $i++){
                                    $rowRecords['users']['salutation'] = $rowData[$i][0][4];
                                    $rowRecords['users']['first_name'] = $rowData[$i][0][5];
                                    $rowRecords['users']['middle_name'] = $rowData[$i][0][6];
                                    $rowRecords['users']['last_name'] = $rowData[$i][0][7];
                                    $rowRecords['users']['ssn_sin_id'] = $rowData[$i][0][13];
                                    $rowRecords['users']['email'] = $rowData[$i][0][11];
                                    $rowRecords['general_property']['property_name'] = $rowData[$i][0][0];
                                    $rowRecords['tenant_phone']['phone_type'] = $rowData[$i][0][8];
                                    $rowRecords['tenant_phone']['carrier'] = $rowData[$i][0][9];
                                    $rowRecords['tenant_phone']['phone_number'] = $rowData[$i][0][10];
                                    $rowRecords['tenant_details']['email1'] = $rowData[$i][0][11];
                                    $rowRecords['tenant_lease_details']['move_in'] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[$i][0][12]));
                                    $rowRecords['tenant_lease_details']['move_out'] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[$i][0][14]));
                                    $rowRecords['tenant_lease_details']['start_date'] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[$i][0][15]));
                                    $rowRecords['tenant_lease_details']['term'] = $rowData[$i][0][16];
                                    $rowRecords['tenant_lease_details']['end_date'] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[$i][0][17]));
                                    $rowRecords['tenant_lease_details']['days_remaining'] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[$i][0][17]));
                                    $rowRecords['tenant_lease_details']['notice_period'] = $rowData[$i][0][18];
                                    $rowRecords['tenant_lease_details']['notice_date'] = date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($rowData[$i][0][19]));
                                    $rowRecords['tenant_lease_details']['rent_due_day'] = $rowData[$i][0][20];
                                    $rowRecords['tenant_lease_details']['rent_amount'] = $rowData[$i][0][22];
                                    $rowRecords['tenant_lease_details']['security_deposite'] = $rowData[$i][0][23];
                                    $rowRecords['tenant_lease_details']['next_rent_incr'] = $rowData[$i][0][24];
                                    $rowRecords['tenant_lease_details']['flat_perc'] = $rowData[$i][0][25];
                                    $rowRecords['tenant_lease_details']['amount_incr'] = $rowData[$i][0][26];
                                    $rowRecords['tenant_charge']['grace_period'] = $rowData[$i][0][21];
                                    $rowRecords['unit_details']['unit_no'] = $rowData[$i][0][2];
                                    $rowRecords['unit_details']['unit_prefix'] = $rowData[$i][0][1];
                                    $rowRecords['building_detail']['building_name'] = $rowData[$i][0][3];
                                    $rowRecords['tenant_property']['property_id'] = "";
                                    $postRecords[] = $rowRecords;

                            }
                            //$rowRecords1 = createSqlColVal($rowRecords);
                           /* $this->saveImportRecord($rowRecords);*/
                            $returnArray = [];
                            for($i=2;$i< count($rowData);$i++) {
                                $first_name=($rowRecords['users']['first_name']);

                                $email=($rowRecords['users']['email']);
                                $last_name=($rowRecords['users']['last_name']);
                                $query = $this->companyConnection->query("SELECT first_name,last_name,email FROM users WHERE first_name ='$first_name' and last_name='$last_name' or email='$email'");
                                $user_val = $query->fetch();
                                if($user_val !== false){
                                    continue;
                                }
                                $prop_name=($rowRecords['general_property']['property_name']);
                                $query1 = $this->companyConnection->query("SELECT property_name FROM general_property WHERE property_name ='$prop_name'");
                                $prop_val = $query1->fetch();

                                if($prop_val !== false){
                                    continue;
                                }
                                $last_id= $this->saveImportRecordUser($rowRecords['users'],'users');
                                //$this->saveImportRecordUser($rowRecords['users'],'users');
                                $last_prop_id=  $this->saveImportRecordProp($rowRecords['general_property'],'general_property',$last_id);
                                $returnArray[] = $this->saveImportRecordPhone($rowRecords['tenant_phone'],'tenant_phone',$last_id);
                                $returnArray[] = $this->saveImportRecord($rowRecords['tenant_details'],'tenant_details',$last_id);
                                $returnArray[] = $this->saveImportRecordLease($rowRecords['tenant_lease_details'],'tenant_lease_details',$last_id);
                                $last_unit_id = $this->saveImportRecordUnit($rowRecords['unit_details'],'unit_details',$last_id);
                                $last_building_id = $this->saveImportRecordbuild($rowRecords['building_detail'],'building_detail',$last_id,$last_prop_id);
                                $returnArray[] = $this->saveImportRecordbuildDetail($rowRecords['tenant_property'],'tenant_property',$last_id,$last_prop_id,$last_unit_id,$last_building_id);
                                $returnArray[] = $this->saveImportRecordCharge($rowRecords['tenant_charge'],'tenant_charge',$last_id,$last_prop_id,$last_unit_id);
                              //  $this->saveImportRecord($rowRecords['building_detail'],'building_detail',$last_id);
                            }
                            //$query = "insert into ` users` (`salutation`, `first_name`, `middle_name`, `last_name`, `ssn_sin_id`$extra_columns) VALUES  ".$rowRecords['users'];
                            /* $query = substr($query, 0, -1);*/
                            try {
                                if (!empty($returnArray)){
                                    return ['status'=>'success','code'=>200,'message'=>'Tenant Imported Successfully!'];
                                }else{
                                    return ['status'=>'failed','code'=>503,'message'=>'Duplicate records found!'];
                                }
                            } catch (Exception $e) {
                                return ['status'=>'failed','code'=>503,'message'=>'Error in importing the data1'];
                                printErrorLog($e->getMessage());
                            }
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }
    function saveImportRecordUser($rowRecords, $tableName){

        $rowRecords['user_type'] ='2';
        $rowRecords['status'] ='1';
        $rowRecords['name'] =$rowRecords['first_name']." ".$rowRecords['last_name'];
        $rowRecords['name'] =$rowRecords['first_name']." ".$rowRecords['last_name'];
        $rowRecords['nick_name'] =$rowRecords['last_name'];
        $rowRecords['record_status'] ='0';
        $rowRecords['created_at'] = date('Y-m-d H:i:s');
        $rowRecords['updated_at'] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($rowRecords);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords);
        return $this->companyConnection->lastInsertId();
       /*return array('code' => 200, 'status' => 'success', 'data' => $rowRecords,'message' => 'Records added successfully.');*/
    }
    function saveImportRecordProp($rowRecords, $tableName){

        $sqlData = createSqlColVal($rowRecords);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords);
        return $this->companyConnection->lastInsertId();
        /*return array('code' => 200, 'status' => 'success', 'data' => $rowRecords,'message' => 'Records added successfully.');*/
    }
    function saveImportRecordBuild($rowRecords, $tableName,$last_prop_id){
        $rowRecords1['building_name'] = $rowRecords['building_name'];
        $rowRecords1['property_id'] = $last_prop_id;
        $rowRecords1['linked_units'] = '0';
        $sqlData = createSqlColVal($rowRecords1);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords1);
        return $this->companyConnection->lastInsertId();
        /*return array('code' => 200, 'status' => 'success', 'data' => $rowRecords,'message' => 'Records added successfully.');*/
    }

    function saveImportRecord($rowRecords, $tableName,$last_id){
        $rowRecords['user_id'] = $last_id;
        $rowRecords['record_status'] = '1';
        $rowRecords['status'] = '1';
        $sqlData = createSqlColVal($rowRecords);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords);
        return array('code' => 200, 'status' => 'success', 'data' => $rowRecords,'message' => 'Records added successfully.');
    }
    function saveImportRecordlease($rowRecords, $tableName,$last_id){
        $rowRecords1['move_in'] = $rowRecords['move_in'];
        $rowRecords1['move_out'] = $rowRecords['move_out'];
        $rowRecords1['start_date'] = $rowRecords['start_date'];
        $rowRecords1['end_date'] = $rowRecords['end_date'];
      /*  $rowRecords['term'] = $rowRecords['term'];*/
        $term_y_m = (explode(" ",$rowRecords['term']));
        $rowRecords1['term'] = $term_y_m[0];
        $tenure_data = '1';
        if($term_y_m[1]== 'Months' || $term_y_m[1]=='months' ||$term_y_m[1]== 'm' ||$term_y_m[1]== 'M')
        {
            $tenure_data='1';
        }
        elseif ($term_y_m[1]== 'Years' || $term_y_m[1]=='years' ||$term_y_m[1]== 'y' ||$term_y_m[1]== 'Y')
        {
            $tenure_data='0';
        }

        $rowRecords1['tenure'] = $tenure_data;
        $rowRecords1['days_remaining'] = $rowRecords['days_remaining'];
        $temp1 = str_replace(" Months","",$rowRecords['notice_period']);
        $number="";
        if($temp1=='1')
        {
            $number='30';
        }
        elseif ($temp1=='2')
        {
            $number='60';
        }
        elseif ($temp1=='3')
        {
            $number='90';
        }
        elseif ($temp1=='6')
        {
            $number='180';
        }
        elseif ($temp1=='9')
        {
            $number='270';
        }
        elseif ($temp1=='12')
        {
            $number='360';
        }

        $rowRecords1['notice_period'] = $number;
        $rowRecords1['notice_date'] = $rowRecords['notice_date'];
        $rowRecords1['rent_due_day'] = $rowRecords['rent_due_day'];
        $rowRecords1['rent_amount'] = $rowRecords['rent_amount'];
        $rowRecords1['security_deposite'] = $rowRecords['security_deposite'];
        $rowRecords1['next_rent_incr'] = $rowRecords['next_rent_incr'];
        if($rowRecords['flat_perc']== 'Flat')
        {
            $falt_perc='flat';
        }
        elseif ($rowRecords['flat_perc']== 'Percentage') {
            $falt_perc = 'perc';
        }
        $rowRecords1['flat_perc'] =  $falt_perc;
        $rowRecords1['amount_incr'] = $rowRecords['amount_incr'];
        $rowRecords1['record_status'] = '1';
        $rowRecords1['user_id'] = $last_id;
        $sqlData = createSqlColVal($rowRecords1);
        $query ="INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords1);
        return array('code' => 200, 'status' => 'success', 'data' => $rowRecords,'message' => 'Records added successfully.');
    }
    function saveImportRecordPhone($rowRecords, $tableName,$last_id){
        $carrier=$rowRecords['carrier'];
        $query = $this->companyConnection->query("SELECT id FROM carrier WHERE carrier='$carrier'");
        $data = $query->fetch();
        $rowRecords1['carrier']=$data['id'];
        if($rowRecords['phone_type']='Mobile')
        {
            $p_type='2';
        }
        elseif ($rowRecords['phone_type']='Work')
        {
            $p_type='3';
        }
        elseif ($rowRecords['phone_type']='Fax')
        {
            $p_type='4';
        }
        elseif ($rowRecords['phone_type']='Home')
        {
            $p_type='5';
        }
        elseif ($rowRecords['phone_type']='Other')
        {
            $p_type='6';
        }
        $rowRecords1['phone_type']=$p_type;
        $rowRecords1['phone_number']=$rowRecords['phone_number'];
        $rowRecords1['user_id'] = $last_id;
        $sqlData = createSqlColVal($rowRecords1);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords1);
        return array('code' => 200, 'status' => 'success', 'data' => $rowRecords,'message' => 'Records added successfully.');
    }
    function saveImportRecordUnit($rowRecords, $tableName,$last_id){
        $rowRecords['user_id'] = $last_id;
        $rowRecords['floor_no'] = 1;
        $sqlData = createSqlColVal($rowRecords);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords);
        return $this->companyConnection->lastInsertId();

    }
    function saveImportRecordCharge($rowRecords, $tableName,$last_id){
        $rowRecords['user_id'] = $last_id;
        $sqlData = createSqlColVal($rowRecords);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords);
        return $this->companyConnection->lastInsertId();

    }
    function saveImportRecordbuildDetail($rowRecords, $tableName,$last_id,$last_prop_id,$last_unit_id,$last_building_id){
        $rowRecords['user_id'] = $last_id;
        $rowRecords['building_id'] = $last_building_id;

        $rowRecords['property_id'] = $last_prop_id;
        $rowRecords['unit_id'] = $last_unit_id;
        $sqlData = createSqlColVal($rowRecords);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecords);
        return array('code' => 200, 'status' => 'success', 'data' => $rowRecords,'message' => 'Records added successfully.');
    }




    public function exportExcel(){

        include(SUPERADMIN_DIRECTORY_URL . "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");

        $objPHPExcel = new PHPExcel();
        $users = $_REQUEST['table'];
        $general_property = $_REQUEST['table1'];
        $tenant_phone = $_REQUEST['table2'];
        $tenant_details = $_REQUEST['table3'];
        $tenant_lease_details = $_REQUEST['table4'];
        $unit_details = $_REQUEST['table5'];
        $building_detail = $_REQUEST['table6'];
        $tenant_property = $_REQUEST['table7'];
      $query1 = "select u.id,u.salutation,u.first_name,u.last_name,u.email,u.ssn_sin_id,u.middle_name,tph.phone_type,tph.carrier,tph.phone_number,td.email1,tld.move_in,tld.move_out,tld.start_date,tld.term,tld.end_date,tld.days_remaining,tld.notice_period,tld.notice_date,tld.rent_due_day,tld.rent_amount,tld.security_deposite,tld.next_rent_incr,tld.flat_perc,tld.amount_incr,tld.tenure,ud.unit_no,ud.unit_prefix,tp.property_id ,tc.grace_period,bd.building_name from users u JOIN tenant_details td ON u.id=td.user_id Join tenant_property tp ON u.id=tp.user_id join tenant_phone tph ON u.id=tph.user_id JOIN tenant_lease_details tld on u.id=tld.user_id JOIN unit_details ud on tp.unit_id=ud.id LEFT JOIN tenant_charge tc ON u.id=tc.user_id JOIN building_detail bd on tp.building_id=bd.id where u.user_type='2' AND u.record_status='0' AND td.record_status='1' AND tld.record_status='1' ORDER BY u.first_name";
        $data = $this->companyConnection->query($query1)->fetchAll();
        
        //Set header with temp array

        $columnHeadingArr =array("Property","Building Unit Prefix","Building Unit Number","Building Name","Salutation","First Name","Middle Name","Last Name","Phone Type","Carrier","Phone Number","Email","Move In Date","SIN","Move Out","Lease Start Date","Lease Term","Lease End Date","Notice Period","Notice Date","Rent Due Day","Grace Period","Rent Amount","Security Deposit","Next Rent Increase","Rent Increase","Amount");
        //take new main array and set header array in it.
        $sheet =array($columnHeadingArr);

        for($i = 0; $i <count($data); $i++){

           
            $query = $this->companyConnection->query("SELECT property_name FROM general_property WHERE id=".$data[$i]['property_id']);
            $prop_name = $query->fetch();
            $tmpArray =array();
            $property =$prop_name['property_name'] ;
            array_push($tmpArray,$property);
            $unit_prefix = $data[$i]['unit_prefix'];
            array_push($tmpArray,$unit_prefix);
            $unit_no = $data[$i]['unit_no'];
            array_push($tmpArray,$unit_no);
            $building_name = $data[$i]['building_name'];
            array_push($tmpArray,$building_name);
            $salutation = $data[$i]['salutation'];
            if($salutation=="Select")
            {
                $salutation="";
            }
            array_push($tmpArray,$salutation);
            $first_name = $data[$i]['first_name'];
            array_push($tmpArray,$first_name);
            $middle_name = $data[$i]['middle_name'];
            array_push($tmpArray,$middle_name);
            $last_name = $data[$i]['last_name'];
            array_push($tmpArray,$last_name);
            $phone_type = $data[$i]['phone_type'];
            array_push($tmpArray,$phone_type);
            $query2 = $this->companyConnection->query("SELECT carrier FROM carrier WHERE id=".$data[$i]['carrier']);
            $car_name = $query2->fetch();
            $carrier = $car_name['carrier'];
            array_push($tmpArray,$carrier);
            $phone_number = $data[$i]['phone_number'];
            array_push($tmpArray,$phone_number);
            $email1 = $data[$i]['email1'];
            array_push($tmpArray,$email1);
            $move_in = date("m/d/Y",strtotime($data[$i]['move_in']));
            array_push($tmpArray,$move_in);
            if (is_numeric($data[$i]['ssn_sin_id'])){
                $ssn_sin_id = $data[$i]['ssn_sin_id'];
            }else{
                $unSSN = unserialize($data[$i]['ssn_sin_id']);
                if (!empty($unSSN)){
                    $ssn_sin_id = implode(",",$unSSN);
                }else{
                    $ssn_sin_id = '';
                }
            }

            array_push($tmpArray,$ssn_sin_id);
            $move_out = date("m/d/Y",strtotime ($data[$i]['move_out']));
            array_push($tmpArray,$move_out);
            $start_date =  date("m/d/Y",strtotime($data[$i]['start_date']));
            array_push($tmpArray,$start_date);
            $term1 = $data[$i]['term'];
            if(($data[$i]['tenure'])=='1'){
                if ($term1 == "1"){
                    $mon_yrs=' Month';
                }else{
                    $mon_yrs=' Months';
                }
            }else{
                if ($term1 == "1"){
                    $mon_yrs=' Year';
                }else{
                    $mon_yrs=' Years';
                }
            }
            $term_m_Y=($term1.$mon_yrs);
            $term = $term_m_Y;
            array_push($tmpArray,$term);
            $end_date = date("m/d/Y",strtotime($data[$i]['end_date']));
            array_push( $tmpArray,$end_date);
            $notice_period = $data[$i]['notice_period'];
            array_push($tmpArray,$notice_period);
            $notice_date =  date("m/d/Y",strtotime($data[$i]['notice_date']));
            array_push($tmpArray,$notice_date);
            $rent_due_day = $data[$i]['rent_due_day'];
            array_push($tmpArray,$rent_due_day);
            $grace_period = $data[$i]['grace_period'];
            array_push($tmpArray,$grace_period);
            $rent_amount = $data[$i]['rent_amount'];
            array_push($tmpArray,$rent_amount);
            $security_deposite = $data[$i]['security_deposite'];
            array_push($tmpArray,$security_deposite);
            $next_rent_incr = $data[$i]['next_rent_incr'];
            array_push($tmpArray,$next_rent_incr);
            if($data[$i]['flat_perc']=='flat')
            {
                $fl_pr='Flat';
            }
            else{
                $fl_pr="Percentage";
            }
            $flat_perc = $fl_pr;
            array_push($tmpArray,$flat_perc);
            $amount_incr = $data[$i]['amount_incr'];
            array_push($tmpArray,$amount_incr);


            array_push($sheet,$tmpArray);
        }
        $worksheet = $objPHPExcel->getActiveSheet();
        foreach($sheet as $row => $columns) {
            foreach($columns as $column => $data) {
                $worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
            }
        }
        //make first row bold
        $objPHPExcel->getActiveSheet()->getStyle("A1:AA1")->getFont()->setBold(true);
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        if (ob_get_contents()) ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Tenant_details.xlsx"');
        $objWriter->save('php://output');
        die();
    }
    public function deletetenant() {
        try {
            $data=[];
            $tenant_id = $_POST['user_id'];
            
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                //$query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$tenant_id'";
                $query = "DELETE FROM users WHERE id='$tenant_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function changePortalStatus() {
        try {
            $data=[];
            $tenant_id = $_POST['user_id'];

            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $query = $this->companyConnection->query("SELECT id, portal_status FROM users WHERE id ='$tenant_id'");
                $user_val = $query->fetch();

                if ($user_val['portal_status'] == '1') {
                    $statusVal = '2';
                    $statusTitle = "deactivated";
                }else{
                    $statusVal = '1';
                    $statusTitle = "activated";
                }
                $data['portal_status'] = $statusVal;
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$tenant_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                if($statusVal == 1){
                    /*Notification for tenant portal account activate*/
                    $notificationTitle= "T/Portal Login Request alert";
                    $module_type = "TENANT";
                    $alert_type = "Real Time";
                    $descriptionNotifi ='Your request for access to the tenant portal has been approved. Please log in using the details in this email.';
                    insertNotification($this->companyConnection,$tenant_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                    /*Notification for tenant portal account activate*/
                }
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record '.$statusTitle.' successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
}

$TenantTypeAjax = new TenantTypeAjax();
