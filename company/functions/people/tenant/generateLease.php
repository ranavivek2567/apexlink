<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */

//error_reporting(E_ALL);
//ini_set("display_errors", 1);
ini_set('memory_limit', '1024M'); // or you could use 1G
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class GenerateLease extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    public function getUserName($id = null,$table) {
        if($id === null) {
            $id = $_SESSION[SESSION_DOMAIN]['user_id'];
        }
        $sql = "SELECT * FROM $table WHERE id=".$id;
        $data = $this->companyConnection->query($sql)->fetch();
        $name = '';


        if(!empty($data) && count($data) > 0){
            $firstName = !empty($data['first_name'])?$data['first_name']:null;
            $lastName = !empty($data['last_name'])?$data['last_name']:null;
            if(!empty($data['nick_name'])){
                $name = $data['nick_name'].' '.$lastName;
            } else {
                $name = $firstName.' '.$lastName;
            }
        }
        return $name;
    }

    public function sendESigatureMail(){

        try{
            if (isset($_POST['type']) && $_POST['type'] == 'guarantor'){
                $return = $this->sendEmailToAllGuarantors($_POST);
                if($return['status'] == 'success' && $return['code'] == 404){
                    $return = $this->sendEmailToMangers($_POST);
                    if ($return['status'] == "error"){
                        $return = $this->sendEmailToOwner($_POST);
                    }
                    return $return;
                }
                return $return;
            }

            if (isset($_POST['type']) && $_POST['type'] == 'owner'){

                $return = $this->sendEmailToMangers($_POST);
                if ($return['status'] == "error"){
                    $return = $this->sendEmailToOwner($_POST);
                }
                return $return;
            }

            if (isset($_POST['type']) && $_POST['type'] == 'manager'){
                $return = $this->sendEmailToManger($_POST);
                return $return;
            }

            if (isset($_POST['type']) && $_POST['type'] == 'tenant'){
                $return = $this->sendEmailToTenants($_POST);
                return $return;
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function sendEmailToTenants($post){
        $emailResponse = [];
        $domainName = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
        $userid = $post['user_id'];
        $company_details = $this->companyConnection->query("SELECT * FROM users where id=1")->fetch();
        $user_details = $this->companyConnection->query("SELECT * FROM users where id=".$userid)->fetch();
        $name = $this->getUserName($user_details['id'], 'users');
        $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/leaseGenerateLink.php';
        $data = file_get_contents($fileUrls);

        $url = $domainName.'/EsignatureUser/EsignatureLease?id=0&ti='.$userid;
        $body = str_replace("TENANTNAME",$name,$data);
        $body = str_replace("RESIDENTIALLEASEURL",$url,$body);
        $body = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
        $body = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
        $request['action']  = 'SendMailPhp';
        $request['to'][]    = $user_details['email'];
        $request['subject'] = 'Lease Agreement';
        $request['message'] = $body;
        $companyUrl = SITE_URL."company/";
        $filePath = $companyUrl.'uploads/Electronic_Record_and_Signature_Disclosure_Customer_Disclosure.docx';
        $request['attachments'][] = $filePath;
        $request['portal']  = '1';

        $emailResponse[] = curlRequest($request);
        $this->sendEmailToAdditionaltenants($userid);
        return ['status'=>'success','code'=>200,'data'=>$request ,'message'=> '', 'emailResponse' => $emailResponse];
    }

    public function sendEmailToAdditionaltenants($userId){
        try{
            $domainName = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
            $request = [];
            $user_details = $this->companyConnection->query("SELECT * FROM  tenant_additional_details where user_id=".$userId)->fetchAll();
            $company_details = $this->companyConnection->query("SELECT * FROM  users where id=1")->fetch();
            $emailResponse = [];
            if (!empty($user_details)){
                $emailResponse = [];
                foreach ($user_details as $user_detail) {
                    $name = $user_detail['first_name'] . ' ' . $user_detail['last_name'];
                    $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/leaseGenerateLink.php';
                    $data = file_get_contents($fileUrls);
                    $aId = $user_detail['id'];
                    $url = $domainName . '/EsignatureUser/EsignatureLease?id=' . $userId . '&ai=' . $aId;
                    $body = str_replace("TENANTNAME", $name, $data);
                    $body = str_replace("RESIDENTIALLEASEURL", $url, $body);
                    $body = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
                    $body = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                    $request['action'] = 'SendMailPhp';
                    $request['to'][] = $user_detail['email1'];
                    $request['subject'] = 'Lease Agreement';
                    $request['message'] = $body;
                    $companyUrl = SITE_URL."company/";
                    $filePath = $companyUrl.'uploads/Electronic_Record_and_Signature_Disclosure_Customer_Disclosure.docx';
                    $request['attachments'][] = $filePath;
                    $request['portal'] = '1';
                    $emailResponse[] = curlRequest($request);
                }
            }
            return ['status'=>'success','code'=>200,'data'=>$request, 'emailResponse' => $emailResponse];
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function sendEmailToAllGuarantors($post){
        try{
            $emailResponse = [];
            $domainName = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
            $userid = $post['user_id'];

            $company_details = $this->companyConnection->query("SELECT * FROM `users` where id=1")->fetch();
            $users = $this->companyConnection->query("SELECT * FROM `tenant_guarantor` where user_id=".$userid)->fetchAll();
            if (!empty($users)){
                $totalGurantor = [];
                foreach ($users as $user){

                    $name = $user['first_name'].' '.$user['last_name'];
                    $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/leaseGenerateLink.php';
                    $data = file_get_contents($fileUrls);
                    $url = $domainName.'/EsignatureUser/EsignatureLease?id='.$userid.'&gi='.$user['id'];
                 //   dd($data);
                    $body = str_replace("TENANTNAME",$name,$data);
                    $body = str_replace("RESIDENTIALLEASEURL",$url,$body);
                    $body = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
                    $body = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                    if ($user['email1'] != ""){
                        $request['action']  = 'SendMailPhp';
                        $request['to'][]    = $user['email1'];
                        $request['subject'] = 'Lease Agreement';
                        $request['message'] = $body;
                        $request['portal']  = '1';
                        $companyUrl = SITE_URL."company/";
                        $filePath = $companyUrl.'uploads/Electronic_Record_and_Signature_Disclosure_Customer_Disclosure.docx';
                        $request['attachments'][] = $filePath;
                        $emailResponse[] = curlRequest($request);
                        $totalGurantor[] =  $request;
                    }
                }
                return ['status'=>'success','code'=>200,'data'=>$totalGurantor, 'message' => 'Email sent to guarantors for e-sign.', '$emailResponse' => $emailResponse];
            }else{
                return ['status'=>'success','code'=>404,'data'=>'No Guarantor Found!'];
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function sendEmailToOwner($post){
//        dd('heeloo');
        try{
            $domainName = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
            $userid = $post['user_id'];
            $emailResponse = [];
            $guarantors = $this->companyConnection->query("SELECT * FROM `tenant_guarantor` where user_id=".$userid)->fetchAll();
            $company_details = $this->companyConnection->query("SELECT * FROM `users` where id=1")->fetch();
            $guarantorCount = count($guarantors);

            $sqlQuery = "SELECT * FROM `tenant_rental_lease_signaures` where parent_id=".$userid." AND usertype='G'";
            $signedGuarantors = $this->companyConnection->query($sqlQuery)->fetchAll();
            $signedGuarantorCount = count($signedGuarantors);
            if ($guarantorCount == $signedGuarantorCount){
                $tenantProperty = $this->companyConnection->query("SELECT * FROM `tenant_property` where user_id='$userid'")->fetch();
                if (!empty($tenantProperty)){
                    $propertyId = $tenantProperty['property_id'];
                    $property = $this->companyConnection->query("SELECT * FROM `general_property` where id='$propertyId'")->fetch();
                    if (!empty($property)){
                        $ownersIds = $property['owner_id'];
                        if ($ownersIds != ""){
                            $unserializeOwner = unserialize($ownersIds);
                            $totalOwners = [];
                            foreach ($unserializeOwner as $ownerId){
                                $user = $this->companyConnection->query("SELECT * FROM `users` where id='$ownerId'")->fetch();
                                $name = $user['first_name'].' '.$user['last_name'];
                                $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/leaseGenerateLink.php';
                                $data = file_get_contents($fileUrls);
                                $url = $domainName.'/EsignatureUser/EsignatureLease?id='.$userid.'&oi='.$user['id'];
                                $body = str_replace("TENANTNAME",$name,$data);
                                $body = str_replace("RESIDENTIALLEASEURL",$url,$body);
                                $body = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
                                $body = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                                $request['action']  = 'SendMailPhp';
                                $request['to'][]    = $user['email'];
                                $request['subject'] = 'Lease Agreement';
                                $request['message'] = $body;
                                $request['portal']  = '1';
                                $companyUrl = SITE_URL."company/";
                                $filePath = $companyUrl.'uploads/Electronic_Record_and_Signature_Disclosure_Customer_Disclosure.docx';
                                $request['attachments'][] = $filePath;
                                $emailResponse[] = curlRequest($request);
                                $totalOwners[] =  $request;
                            }
                            return ['status'=>'success','code'=>200,'data'=>$totalOwners, 'message' => 'Email sent to Owners for e-sign.', 'emailResponse' => $emailResponse];
                        }else{
                            return ['status'=>'error','code'=>503,'data'=>'','Owner doesn`t exist'];
                        }
                    }else{
                        return ['status'=>'error','code'=>503,'data'=>'','Property doesn`t exist'];
                    }
                }else{
                    return ['status'=>'error','code'=>503,'data'=>'','Property doesn`t exist'];
                }
            }else{
                return ['status'=>'error','code'=>503,'data'=>'','All guarantors hasn`t signed yet.'];
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function sendEmailToMangers($post){
        try{
            $domainName = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
            $userid = $post['user_id'];
            $emailResponse = [];
            $guarantors = $this->companyConnection->query("SELECT * FROM `tenant_guarantor` where user_id=".$userid)->fetchAll();
            $company_details = $this->companyConnection->query("SELECT * FROM `users` where id=1")->fetch();
            $guarantorCount = count($guarantors);

            $sqlQuery = "SELECT * FROM `tenant_rental_lease_signaures` where parent_id=".$userid." AND usertype='G'";
            $signedGuarantors = $this->companyConnection->query($sqlQuery)->fetchAll();
            $signedGuarantorCount = count($signedGuarantors);
            if ($guarantorCount == $signedGuarantorCount){
                $tenantProperty = $this->companyConnection->query("SELECT * FROM `tenant_property` where user_id='$userid'")->fetch();
                if (!empty($tenantProperty)){
                    $propertyId = $tenantProperty['property_id'];
                    $property = $this->companyConnection->query("SELECT * FROM `general_property` where id='$propertyId'")->fetch();
                    if (!empty($property)){
                        $ownersIds = $property['manager_id'];
                        if ($ownersIds != ""){
                            $unserializeOwner = unserialize($ownersIds);
                            $totalOwners = [];
                            foreach ($unserializeOwner as $ownerId){
                                $user = $this->companyConnection->query("SELECT * FROM `users` where id='$ownerId'")->fetch();
                                $name = $user['first_name'].' '.$user['last_name'];
                                $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/leaseGenerateLink.php';
                                $data = file_get_contents($fileUrls);
                             //   dd($data);
                                $url = $domainName.'/EsignatureUser/EsignatureLease?id='.$userid.'&oi='.$user['id'];
                                $body = str_replace("TENANTNAME",$name,$data);
                                $body = str_replace("RESIDENTIALLEASEURL",$url,$body);
                                $body = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
                                $body = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                                $request['action']  = 'SendMailPhp';
                                $request['to'][]    = $user['email'];
                                $request['subject'] = 'Lease Agreement';
                                $request['message'] = $body;
                                $request['portal']  = '1';
                                $companyUrl = SITE_URL."company/";
                                $filePath = $companyUrl.'uploads/Electronic_Record_and_Signature_Disclosure_Customer_Disclosure.docx';
                                $request['attachments'][] = $filePath;
                                $emailResponse[] = curlRequest($request);
                                $totalOwners[] =  $request;
                            }
                            return ['status'=>'success','code'=>200,'data'=>$totalOwners, 'message' => 'Email sent to Managers for e-sign.', 'emailResponse' => $emailResponse];
                        }else{
                            return ['status'=>'error','code'=>503,'data'=>'','Manager doesn`t exist'];
                        }
                    }else{
                        return ['status'=>'error','code'=>503,'data'=>'','Property doesn`t exist'];
                    }
                }else{
                    return ['status'=>'error','code'=>503,'data'=>'','Property doesn`t exist'];
                }
            }else{
                return ['status'=>'error','code'=>503,'data'=>'','All guarantors hasn`t signed yet.'];
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function sendEmailToManger($post){
        try{
            $domainName = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
            $userid = $post['user_id'];

            $tenantProperty = $this->companyConnection->query("SELECT * FROM `tenant_property` where user_id='$userid'")->fetch();
            $company_details = $this->companyConnection->query("SELECT * FROM `users` where id=1")->fetch();
            if (!empty($tenantProperty)){
                $propertyId = $tenantProperty['property_id'];
                $property = $this->companyConnection->query("SELECT * FROM `general_property` where id='$propertyId'")->fetch();
                if (!empty($property)){
                    $managerIds = $property['manager_id'];
                    if ($managerIds != ""){
                        $unserializeManager = unserialize($managerIds);
                        $totalManagers = [];
                        $emailResponse = [];
                        foreach ($unserializeManager as $managerId){
                            $user = $this->companyConnection->query("SELECT * FROM `users` where id='$managerId'")->fetch();
                            $name = $user['first_name'].' '.$user['last_name'];
                            $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/leaseGenerateLink.php';
                            $data = file_get_contents($fileUrls);
                            $url = $domainName.'/Lease/Movein';
                            $body = str_replace("TENANTNAME",$name,$data);
                            $body = str_replace("RESIDENTIALLEASEURL",$url,$body);
                            $body = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
                            $body = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                            $request['action']  = 'SendMailPhp';
                            $request['to'][]    = $user['email'];
                            $request['subject'] = 'Lease Agreement';
                            $request['message'] = $body;
                            $request['portal']  = '1';
                            $emailResponse[] = curlRequest($request);
                            $totalManagers[] =  $request;
                        }
                        $this->saveTenantMoveInRecord($userid);
                        $this->updateTenantESignHistoryRecord1($userid, '1','0');
                        return ['status'=>'success','code'=>200,'data'=>$totalManagers, 'message' => 'Email sent to Manager for e-sign.', 'emailResponse' => $emailResponse];
                    }
                }else{
                    return ['status'=>'error','code'=>503,'data'=>'','Property doesn`t exist'];
                }
            }else{
                return ['status'=>'error','code'=>503,'data'=>'','Property doesn`t exist'];
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function getTenantRentInfoMoveIn(){
        $query = "SELECT * FROM `tenant_lease_details` WHERE user_id = ".$_POST['id'];
        $rentData = $this->companyConnection->query($query)->fetch();
        $data = [];
        $data['move_in'] = dateFormatUser($rentData['move_in'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['actual_move_in'] = dateFormatUser(date('Y-m-d'), $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['move_out'] = dateFormatUser($rentData['move_out'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['start_date'] = dateFormatUser($rentData['start_date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['end_date'] = dateFormatUser($rentData['end_date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['notice_date'] = dateFormatUser($rentData['notice_date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
        $data['rent_amount'] = $rentData['rent_amount'];
        $date1=date_create(date('Y-m-d'));
        $date2=date_create($rentData['move_in']);
        $diff=date_diff($date1,$date2);
        $days = $diff->days;

        $data['tenantName'] = userName($_POST['id'], $this->companyConnection,'users');

        $query = "SELECT * FROM `tenant_property` WHERE user_id = ".$_POST['id'];
        $propertyData = $this->companyConnection->query($query)->fetch();
        if(!empty($propertyData)){
            $query = "SELECT property_name,address_list,state FROM `general_property` WHERE id = ".$propertyData['property_id'];
            $propertyName = $this->companyConnection->query($query)->fetch();
            if(!empty($propertyName)){
                $data['property_name'] = $propertyName['property_name'];
                $data['address_list'] = $propertyName['address_list'];
                $data['state'] = $propertyName['state'];
            }

            $query = "SELECT unit_prefix, unit_no FROM `unit_details` WHERE id = ".$propertyData['unit_id'];
            $unitName = $this->companyConnection->query($query)->fetch();
            if(!empty($unitName)){
                $data['unit_name'] = $unitName['unit_prefix'].'-'.$unitName['unit_no'];
            }
        }


        return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Rent data loaded successfully!');
    }

    public function getSignatureInfo(){
        try{
            $userId = $_POST['user_id'];
            $parentId = $_POST['parentId'];
            $type = $_POST['type'];

            $data['user_id'] = $userId;
            $data['parentId'] = $parentId;
            $data['type'] = $type;

            $sqlQuery = "SELECT * FROM `tenant_rental_lease_signaures` where user_id=".$userId." AND parent_id=".$parentId." AND usertype='$type'";
            $user = $this->companyConnection->query($sqlQuery)->fetch();
            if(!empty($user)){
                return ['status'=>'success','code'=>200,'data'=>$user, 'checkData'=>$data, 'message' => ''];
            }else{
                return ['status'=>'success','code'=>200,'data'=>'', 'checkData'=>$data, 'message' => ''];
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function saveSignatureStyle(){
        try{
            $parentId = $_POST['parentId'];
            $tenantId = $_POST['tId'];
            $query = 'SELECT id FROM tenant_rental_lease_signaures where user_id='.$tenantId.' AND parent_id='.$parentId;
            $dataQuery = $this->companyConnection->query($query)->fetch();
         /*   if(!empty($dataQuery)){
                return ['code'=>400, 'status'=>'warnin12g', 'message'=> 'Signature Already updated.'];
            }*/
            if (isset($_POST['styleAttr']) && $_POST['styleAttr'] != ""){
                $data1['style'] = $_POST['styleAttr'];
                $data1['updated_at'] = date('Y-m-d H:i:s');
            }else{
                $data1['style'] = '';
            }


            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE `tenant_rental_lease_signaures` SET ".$sqlData['columnsValuesPair']." where user_id=".$tenantId." AND parent_id=".$parentId;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $data1['id'] = $this->companyConnection->lastInsertId();
            return ['code'=>200, 'status'=>'success', 'data'=>$data1, 'message'=> 'Signature saved successfully.'];
        }catch (Exception $e){
            return ['status'=>'failed','code'=>503,'data'=>$e->getMessage(),'message'=>'Signature not saved successfully.'];
        }
    }

    public function saveSignatureImage(){
        try{
            $parent = $_POST['dataParent'];
            $uId = $_POST['id'];
            $newSql = "SELECT * FROM tenant_rental_lease_signaures where user_id='$uId' AND parent_id ='$parent'";
            $userSig =  $this->companyConnection->query($newSql)->fetch();
            if ($userSig != ""){

                if (isset($_POST['inputVal']) && $_POST['inputVal'] == "s"){
                    $data1['signature_image'] = $_POST['img'];
                    $data1['signature_text'] = 0;
                }else{
                    $data1['signature_image'] = '';
                    $data1['signature_text'] = $_POST['fontText'].",".$_POST['fontfamily'];
                }
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data1);
                $query = "UPDATE tenant_rental_lease_signaures SET " . $sqlData['columnsValuesPair'] . " where user_id=".$_POST['id'];

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $id = $this->companyConnection->lastInsertId();

                $data2['user_id'] = $_POST['id'];
                $data2['signature_image'] = $data1['signature_image'];
                $data2['signature_text'] = $data1['signature_text'];
                $data2['status'] = 1;
                $data2['parent_id'] = $_POST['dataParent'];
                $data2['usertype'] = $_POST['dataType'];
                $data2['record_status'] = 0;
                $data2['updated_at'] = date('Y-m-d H:i:s');

                return ['code'=>200, 'status'=>'success', 'data'=>$data2, 'id'=>$id, 'message'=> 'Signature updated successfully.'];
            }else{
                $data1['user_id'] = $_POST['id'];
                $data1['status'] = 1;

                if (isset($_POST['inputVal']) && $_POST['inputVal'] == "s"){
                    $data1['signature_image'] = $_POST['img'];
                    $data1['signature_text'] = 0;
                }else{
                    $data1['signature_image'] = '';
                    $data1['signature_text'] = $_POST['fontText'].",".$_POST['fontfamily'];
                }

                $data1['parent_id'] = $_POST['dataParent'];
                $data1['usertype'] = $_POST['dataType'];
                $data1['record_status'] = 0;
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO `tenant_rental_lease_signaures` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $id = $this->companyConnection->lastInsertId();
                if($_POST['dataType'] == "T"){
                    $this->saveTenantESignHistoryRecord($uId, $parent, $_POST['dataType']);
                }
                if($_POST['dataType'] == "O"){
                    $this->updateTenantESignHistoryRecord1($parent,'0');
                }

                return ['code'=>200, 'status'=>'success', 'data'=>$data1, 'id'=>$id, 'message'=> 'Signature saved successfully.'];
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Signature not saved successfully.'];
            printErrorLog($exception->getMessage());
        }
    }

    public function saveTenantMoveInRecord($userid){
        try{
            $data1['user_id'] = $userid;
            $data1['status'] = '0';//0=>pending,1=>active,2=>decline,3=>approved
            $data1['record_status'] = '0';
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $data1['deleted_at'] = null;
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO `movein_list_record` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            $data1['id'] = $this->companyConnection->lastInsertId();
            return ['code'=>200, 'status'=>'success', 'data'=>$data1,'message'=> 'Tenant move-in successfully.'];
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Tenant not saved in move-in.'];
        }
    }

    public function updateTenantMoveInRecord($userid,$moveinlistre = ""){
        try{
            if ($moveinlistre == ""){
                $data1['status'] = '1';/* active */
                $data1['record_status'] = '1';
            }else{
                $data1['status'] = $moveinlistre;/* active */
                $data1['record_status'] = $moveinlistre;
            }
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE `movein_list_record` SET " . $sqlData['columnsValuesPair'] . " where user_id='$userid'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $data1['id'] = $this->companyConnection->lastInsertId();

            return ['code'=>200, 'status'=>'success', 'data'=>$data1,'message'=> 'Tenant move-in status updated successfully.'];
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Tenant move-in status not updated.'];
        }
    }

    public function updateTenantMoveInRecord2($userid){
        try{

            $data1['status'] = '0';/* active */
            $data1['record_status'] = '0';
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE `movein_list_record` SET " . $sqlData['columnsValuesPair'] . " where user_id='$userid'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $data1['id'] = $this->companyConnection->lastInsertId();

            return ['code'=>200, 'status'=>'success', 'data'=>$data1,'message'=> 'Tenant move-in status updated successfully.'];
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Tenant move-in status not updated.'];
        }
    }

    public function saveTenantESignHistoryRecord($userid, $parent, $dataType){
        try{
            $sqlQuery = "SELECT * FROM `users` where id='$userid'";
            $user = $this->companyConnection->query($sqlQuery)->fetch();
            
            $data1['user_id'] = $userid;
            $data1['username'] = userName($userid, $this->companyConnection,'users');
            $data1['email'] = $user['email'];
            $data1['done_date'] = date('Y-m-d H:i:s');
            $data1['document_name'] = '';
            $data1['status'] = '0';
            if ($dataType == "O") {
                $data1['status'] = '1';/*0=>sent,1=>Recieved,2=>Accept,3=>Decline*/
            }
            $data1['record_status'] = '1';
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data1);
            $query = "INSERT INTO `e_sign_history` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data1);
            $data1['id'] = $this->companyConnection->lastInsertId();
            return ['code'=>200, 'status'=>'success', 'data'=>$data1,'message'=> 'Tenant move-in successfully.'];
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Tenant not saved in move-in.'];
        }
    }

    public function getMoveInListRecord(){
        try{
            $id = $_POST['id'];
            $sqlQuery = "SELECT * FROM `movein_list_record` where user_id=".$id;
            $user = $this->companyConnection->query($sqlQuery)->fetch();
            


            if (!empty($user)){
                if ($user['status'] == '0'){
                    return ['code'=>503, 'status'=>'success', 'data'=>'','message'=> 'E-Signature Document has not been signed/approved by the Property Manager/Owner yet.'];
                } else {
                    $data = $this->getAgreementData($id);
                    $html = $this->createPdfHtml($data['html'],$data['data']);
                    $createPDF = $this->createPdfFile3($id,$html);

                    $pdfData = $this->savePdfToLibrary($id,$createPDF['path'],$createPDF['tempName']);					
                    $this->sendFinalPdf($data['userArray'],$pdfData);
					
					//send pdf email to Gurrantor
                    $guarantorIds = $this->sendPdfToGuarantor($id);
                    $this->sendFinalPdftoGurrantor($guarantorIds,$pdfData);
					
					//send pdf email to Gurrantor					
                    $additionalIds = $this->sendPdfToAdditionalTenant($id);
                    $this->sendFinalPdftoAdditionalTenant($additionalIds,$pdfData);
					
                    $return = $this->saveMoveInRecords($_POST);
                    return ['code'=>200, 'status'=>'success', 'data'=>$return,'message'=> 'Tenant added successfully.'];
                }
            }else{
                return ['code'=>503, 'status'=>'success', 'data'=>'','message'=> 'E-Signature Document has not been signed/approved by the Property Manager/Owner yet.'];
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Tenant not saved in move-in.'];
        }
    }

    public function sendPdfToGuarantor($id){
        $idArray = [];
        $query = 'SELECT id FROM tenant_guarantor where user_id='.$id;
        $data = $this->companyConnection->query($query)->fetchAll();
        foreach ($data as $id){
            $idArray[] = $id['id'];
        }
        return $idArray;
    }

    public function sendPdfToAdditionalTenant($id){
        $query = 'SELECT id FROM tenant_additional_details where user_id='.$id;
        $data = $this->companyConnection->query($query)->fetchAll();
        $idArray = [];
        foreach ($data as $id){
            $idArray[] = $id['id'];
        }
        return $idArray;
    }

    public function sendFinalPdf($users,$pdfUrl){
        try{
            foreach ($users as $ownerId){
                $user = $this->companyConnection->query("SELECT * FROM `users` where id='$ownerId'")->fetch();
                $name = $user['first_name'].' '.$user['last_name'];
                $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/pdfGenerateLink.php';
                $data = file_get_contents($fileUrls);
                $body = str_replace("TENANTNAME",$name,$data);
                $body = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
                $body = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                $request = [];
                $request['action']  = 'SendMailPhp';
                $request['to'][]    = $user['email'];
                $request['subject'] = 'Lease Agreement';
                $request['message'] = $body;
                $request['portal']  = '1';
                $companyUrl = SITE_URL."company/";
                $filePath = $companyUrl.$pdfUrl;
                $request['attachments'][] = $filePath;
                $emailResponse[] = curlRequest($request);
            }
        } catch (Exception $exception) {
            dd($exception);
        }
    }
	
	public function sendFinalPdftoAdditionalTenant($users,$pdfUrl){
        try{
            foreach ($users as $ownerId){
                $user = $this->companyConnection->query("SELECT * FROM `tenant_additional_details` where id='$ownerId'")->fetch();
                $name = $user['first_name'].' '.$user['last_name'];
                $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/pdfGenerateLink.php';
                $data = file_get_contents($fileUrls);
                $body = str_replace("TENANTNAME",$name,$data);
                $body = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
                $body = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                $request = [];
                $request['action']  = 'SendMailPhp';
                $request['to'][]    = $user['email1'];
                $request['subject'] = 'Lease Agreement';
                $request['message'] = $body;
                $request['portal']  = '1';
                $companyUrl = SITE_URL."company/";
                $filePath = $companyUrl.$pdfUrl;
                $request['attachments'][] = $filePath;
                $emailResponse[] = curlRequest($request);
            }
        } catch (Exception $exception) {
            dd($exception);
        }
    }
	
	public function sendFinalPdftoGurrantor($users,$pdfUrl){
        try{
            foreach ($users as $ownerId){
                $user = $this->companyConnection->query("SELECT * FROM `tenant_guarantor` where id='$ownerId'")->fetch();
                $name = $user['first_name'].' '.$user['last_name'];
                $fileUrls = COMPANY_DIRECTORY_URL . '/views/company/tenants/emailTemplates/pdfGenerateLink.php';
                $data = file_get_contents($fileUrls);
                $body = str_replace("TENANTNAME",$name,$data);
                $body = str_replace("COMPANYNAME",$_SESSION[SESSION_DOMAIN]['company_name'],$body);
                $body = str_replace("COMPANYPHONENUMBER",$_SESSION[SESSION_DOMAIN]['phone_number'],$body);
                $request = [];
                $request['action']  = 'SendMailPhp';
                $request['to'][]    = $user['email1'];
                $request['subject'] = 'Lease Agreement';
                $request['message'] = $body;
                $request['portal']  = '1';
                $companyUrl = SITE_URL."company/";
                $filePath = $companyUrl.$pdfUrl;
                $request['attachments'][] = $filePath;
                $emailResponse[] = curlRequest($request);
            }
        } catch (Exception $exception) {
            dd($exception);
        }
    }

    public function getAgreementData($id){
        try {
            $query = 'SELECT tenant_charge.grace_period,tenant_rental_lease_signaures.*,users.name,general_property.address_list,tenant_lease_details.start_date,tenant_lease_details.end_date,tenant_lease_details.notice_period,tenant_lease_details.security_deposite,tenant_lease_details.rent_amount FROM tenant_rental_lease_signaures LEFT JOIN users ON tenant_rental_lease_signaures.user_id=users.id LEFT JOIN tenant_property ON tenant_rental_lease_signaures.user_id=tenant_property.user_id LEFT JOIN general_property ON tenant_property.property_id=general_property.id LEFT JOIN tenant_lease_details ON tenant_rental_lease_signaures.user_id=tenant_lease_details.user_id LEFT JOIN tenant_charge ON tenant_rental_lease_signaures.user_id=tenant_charge.user_id WHERE tenant_rental_lease_signaures.user_id='.$id.' OR tenant_rental_lease_signaures.parent_id='.$id;
            $data = $this->companyConnection->query($query)->fetchAll();
            $session_data = $_SESSION[SESSION_DOMAIN];
            $html = '';
            if(!empty($data)){
                $ownerSectionHtml = '';
                $tenantSectionHtml = '';
                $additionalTenantSectionHtml ='';
                $guarantorSectionHtml = '';
                $userArray = [];
                foreach ($data as $key=>$value){
                    if($value['usertype'] == 'T' ){
                        array_push($userArray,$value['user_id']);
                        $session_data['tenant_name'] = $value['name'];
                        $session_data['property_address'] = $value['address_list'];
                        $session_data['start_date'] = $value['start_date'];
                        $session_data['end_date'] = $value['end_date'];
                        $session_data['rent_amount'] = $value['rent_amount'];
                        $session_data['security_deposite'] = $value['security_deposite'];
                        $session_data['grace_period'] = $value['grace_period'];
                        $session_data['notice_period'] = $value['notice_period'];
                        $session_data['address_list'] = $value['address_list'];
                        if($value['signature_text'] == '0') {
                            $tenantSectionHtml = '<div style="display: block;clear: both;margin: 10px 0; text-align: left">
<p style="display:block;clear:both;height:110px;">
                                                    <img height="90" src="' . $value["signature_image"] . '" alt="signature" style="display:inline-block;margin:auto;">
                                                    </p>
                                                    <p style="display:block;margin: 10px 0;"><small style="display:block;clear:both;">'.$value["name"].' :</small>
                                                    <time style="display:block;font-size: 15px;">'.$value["created_at"].'</time></p>
                                                </div>';
                        } else {
                            $tenantSectionHtml = '<div style="display: block;clear: both;margin: 10px 0; text-align: left">
                                                    <strong style="display:block;margin-bottom: 10px;font-weight: 700;font-size: 24px;">'.$value['signature_text'].'</strong>
                                                    <p style="display:block;margin: 10px 0;"><small style="display:block;">'.$value["name"].' :</small>
                                                    <time style="display:block;font-size: 15px;">'.$value["created_at"].'</time></p>
                                                </div>';
                        }
                    } elseif($value['usertype'] == 'G') {

                        $session_data['guarantor_name'] = $value['name'];
                        if($value['signature_text'] == '0') {
                            $guarantorSectionHtml = '<div style="display: block;clear: both;margin: 10px 0; text-align: left">
<p style="display:block;clear:both;height:110px;">
                                                    <img height="90" src="' . $value["signature_image"] . '" alt="signature" style="display:inline-block;margin:auto;">
                                                    </p>
                                                    <p style="display:block;margin: 10px 0;"><small style="display:block;clear:both;">'.$value["name"].' :</small>
                                                    <time style="display:block;font-size: 15px;">'.$value["created_at"].'</time></p>
                                                </div>';
                        } else {
                            $guarantorSectionHtml = '<div style="display: block;clear: both;margin: 10px 0; text-align: left">
                                                    <strong style="display:block;margin-bottom: 10px;font-weight: 700;font-size: 24px;">'.$value['signature_text'].'</strong>
                                                    <p style="display:block;margin: 10px 0;"><small style="display:block;clear:both;">'.$value["name"].' :</small>
                                                    <time style="display:block;font-size: 15px;">'.$value["created_at"].'</time></p>
                                                </div>';
                        }
                    } elseif($value['usertype'] == 'AT') {
                        $session_data['additionalTenant_name'] = $value['name'];
                        if($value['signature_text'] == '0') {
                            $additionalTenantSectionHtml = '<div style="display: block;clear: both;margin: 10px 0; text-align: left">
<p style="display:block;clear:both;height:110px;">
                                                    <img  height="90" src="' . $value["signature_image"] . '" alt="signature" style="display:inline-block;margin:auto;">
                                                    </p>
                                                    <p style="display:block;margin: 10px 0;"><small style="display:block;clear:both;">'.$value["name"].' :</small>
                                                    <time style="display:block;font-size: 15px;">'.$value["created_at"].'</time></p>
                                                </div>';
                        } else {
                            $additionalTenantSectionHtml = '<div style="display: block;clear: both;margin: 10px 0; text-align: left">
                                                    <strong style="display:block;margin-bottom: 10px;font-weight: 700;font-size: 24px;">'.$value['signature_text'].'</strong>
                                                    <p style="display:block;margin: 10px 0;"><small style="display:block;clear:both;">'.$value["name"].' :</small>
                                                    <time style="display:block;font-size: 15px;">'.$value["created_at"].'</time></p>
                                                </div>';
                        }
                    } elseif($value['usertype'] == 'O') {
                        array_push($userArray,$value['user_id']);
                        $session_data['owner_name'] = $value['name'];
                        if($value['signature_text'] == '0') {
                            $ownerSectionHtml = '<div style="display: block;clear: both;margin: 10px 0; text-align: left">
                                                    <p style="display:block;clear:both;height:110px;">
                                                    <img  height="90" src="' . $value["signature_image"] . '" alt="signature" style="display:inline-block;margin:auto;">
                                                    </p>
                                                    <p style="display:block;margin: 10px 0;"><small style="display:block;clear:both;">'.$value["name"].' :</small>
                                                    <time style="display:block;font-size: 15px;">'.$value["created_at"].'</time></p>
                                                </div>';
                        } else {
                            $ownerSectionHtml = '<div style="display: block;clear: both;margin: 10px 0; text-align: left">
                                                    <strong style="display:block;margin-bottom: 10px;font-weight: 700;font-size: 24px;">'.$value['signature_text'].'</strong>
                                                    <p style="display:block;margin: 10px 0;"><small style="display:block;clear:both;">'.$value["name"].' :</small>
                                                    <time style="display:block;font-size: 15px;">'.$value["created_at"].'</time></p>
                                                </div>';

                        }
                    }
                }
                $html = '<div style="width: 85%; margin-top: 20px; padding: 0 10px;">
                            <div style="width:100%;">
                                    <b style="float:left;" class="owner-manger-block">MANAGER [by Agent under Property Management Agreement]:</b><br>
                                    '.$ownerSectionHtml.'
                                    <b style="float:left;">TENANT(S):</b><br>
                                    '.$tenantSectionHtml.'
                                    '.$additionalTenantSectionHtml.'
                                    <b style="float:left;">GUARANTOR:</b><br>
                                    '.$guarantorSectionHtml.'
                            </div>
                           
                        </div>';

            }
            return ['status'=>'success','code'=>200,'html'=>$html, 'data'=> $session_data,'userArray'=>$userArray ];

        } catch(Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Tenant not saved in move-in.'];
        }
    }

    public function savePdfToLibrary($id,$path,$filename){
        try {
            $domain = getDomain();
            $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
            $dopath = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];

            $randomNumber = uniqid();
            $uniqueName = $randomNumber . $filename;

            $data1['user_id'] = $id;
            $data1['filename'] = $uniqueName;
            $data1['file_location'] = $dopath . '/' . $uniqueName;

            $data1['file_type'] = 2;
            $data1['file_extension'] = 'pdf';
            $sqlData1 = createSqlColVal($data1);
            $query1 = "INSERT INTO tenant_chargefiles(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute($data1);
            copy(COMPANY_DIRECTORY_URL . '/'.$path.$filename,COMPANY_DIRECTORY_URL . '/'.$dopath.'/'.$uniqueName);
            return $dopath.'/'.$uniqueName;
        } catch (Exception $exception){
            dd($exception);
        }
    }


    public function saveMoveInRecords($post){
        $id = $post['id'];
        $data['user_id'] = $post['id'];
        $data['scheduled_move_in'] = $post['mi_schedule_move_in'];
        $data['actual_move_in'] = $post['mi_actual_move_in'];
        $data['paid_amount'] = $post['paid_amount'];
        $data['check_no'] = (isset($post['check_no']) && !empty($post['check_no'])) ? $post['check_no'] : null ;
        $data['prorated_amount'] = $post['prorated_amount'];
        $data['memo'] = $post['memo'];
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($data);
        $query = "INSERT INTO `tenant_move_in` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($data);

        $data2 = [];
        $data2['updated_at'] = date('Y-m-d H:i:s');
        $sqlData2 = createSqlColValPair($data2);
        $query2 = "UPDATE `users` SET ".$sqlData2['columnsValuesPair']." where id=".$id;
        $stmt2 = $this->companyConnection->prepare($query2);
        $stmt2->execute();

        $id = $this->companyConnection->lastInsertId();
        $this->updateTenantRecordStatus($data['user_id']);
        return ['code'=>200, 'status'=>'success', 'data'=>$data, 'id'=>$id, 'message'=> 'Tenant created successfully.'];
    }

    public function updateTenantRecordStatus($user_id){
        try{
            $tableName = ['tenant_details', 'tenant_lease_details', 'tenant_notes', 'tenant_property', 'tenant_vehicles', 'tenant_charges'];
            foreach ($tableName as $val){
                $recordStatus['record_status'] = "1";
                $sqlData = createSqlColValPair($recordStatus);
                $query = "UPDATE $val SET ".$sqlData['columnsValuesPair']." where user_id='$user_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
            }
            $this->updateTenantRecordStatusUser($user_id);

            return array('code' => 200, 'status' => 'success', 'data' => $tableName, 'message' => 'Status has been updated!');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function updateTenantRecordStatusUser($user_id){
        try{
            $tableName = ['users'];
            foreach ($tableName as $val){
                $recordStatus['record_status'] = "0";
                $sqlData = createSqlColValPair($recordStatus);
                $query = "UPDATE $val SET ".$sqlData['columnsValuesPair']." where id=".$user_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $this->updateTenantUnitStatus($user_id);
            }

            return array('code' => 200, 'status' => 'success', 'data' => $tableName, 'message' => 'Status has been updated!');
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function updateTenantUnitStatus($user_id){
        try{
            $query = "SELECT * FROM `tenant_property` WHERE user_id = ".$user_id;
            $unitData = $this->companyConnection->query($query)->fetch();
            if ($unitData !==  false){
                $id = $unitData['unit_id'];
                $recordStatus['building_unit_status'] = "4";
                $sqlData = createSqlColValPair($recordStatus);
                $query = "UPDATE `unit_details` SET ".$sqlData['columnsValuesPair']." where id=".$id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $last_id=$this->companyConnection->lastInsertId();
                $this->sendNotification($user_id);

                return array('code' => 200, 'status' => 'success', 'message' => 'Unit status has been updated!');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }
    public function sendNotification($last_id){
        $notifiData = "SELECT u.name,tp.user_id,tp.property_id,tp.unit_id,tld.id,CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name,tld.start_date,tld.created_at FROM `tenant_property` as tp JOIN unit_details as ud ON ud.id = tp.unit_id JOIN tenant_lease_details as tld ON tld.user_id = tp.user_id JOIN users as u ON u.id = tp.user_id where ud.building_unit_status ='4' AND tp.user_id =".$last_id;
        $notifiquery = $this->companyConnection->query($notifiData)->fetch();
        $name = $notifiquery['name'];
        $leaseId = $notifiquery['id'];
        $tenantId = $notifiquery['user_id'];
        $propertyId=$notifiquery['property_id'];
        $unitname=$notifiquery['unit_name'];
        $startDate= dateFormatUser($notifiquery['start_date'] ,$last_id ,$this->companyConnection);
        $dateCreated = dateFormatUser($notifiquery['created_at'],$last_id,$this->companyConnection);

        /* lease notification */
        $notificationTitle= "Start Date";
        $module_type = "LEASE";
        $alert_type = "Real Time";
        $descriptionNotifi ='Notification of Lease Start Date for Tenant ID- '.$tenantId.'(PropertyID-'.$propertyId.') on '.$dateCreated;
        insertNotification($this->companyConnection,$last_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
        /* lease notification */

        /* lease notification */
        $notificationTitle= "Lease signed";
        $module_type = "LEASE";
        $alert_type = "Real Time";
        $descriptionNotifi ='New Lease (ID - '.$leaseId.'). has been generated for tenant ID-'.$tenantId.' in  PropertyID-'.$propertyId;
        insertNotification($this->companyConnection,$last_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
        /* lease notification */

        /* lease notification */
        $notificationTitle= "New tenant notification";
        $module_type = "LEASE";
        $alert_type = "Real Time";
        $descriptionNotifi ='New Tenant ID -'.$tenantId.' has been moved into(Name-'.$name.', Property - '.$tenantId.', Unit '.$unitname.') as of today.';
        insertNotification($this->companyConnection,$last_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
        /* lease notification */
    }
    public function updateTenantESignHistoryRecord1($userid,$status,$moveinlistre = ""){
        try{

            $data1['status'] = $status;//0=>sent,1=>Recieved,2=>Accept,3=>Decline
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data1);
            $query = "UPDATE `e_sign_history` SET " . $sqlData['columnsValuesPair'] . " where user_id='$userid'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $data1['id'] = $this->companyConnection->lastInsertId();
            $this->updateTenantMoveInRecord($userid,$moveinlistre);
            return ['code'=>200, 'status'=>'success', 'data'=>$data1,'message'=> 'Status has been updated to received.'];
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Tenant not saved in move-in.'];
        }
    }

    public function updateTenantESignHistoryRecord(){
        try{
            if (isset($_POST['user_id']) && $_POST['user_id'] != "") {
                
                if ($_POST['value'] == "2") {
                    $userid = $_POST['user_id'];
                    $status = $_POST['value'];
                    $data1['status'] = $status;//0=>sent,1=>Recieved,2=>Accept,3=>Decline
                    $data1['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data1);
                    $query = "UPDATE `e_sign_history` SET " . $sqlData['columnsValuesPair'] . " where user_id='$userid'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    $data1['id'] = $this->companyConnection->lastInsertId();
                    $this->updateTenantMoveInRecord($userid);
                    return ['code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'Status has been updated to received.'];    
                }else{
                    $this->updateTenantDeclineStatus($_POST);
                    return ['code' => 200, 'status' => 'success', 'data' => '', 'message' => 'Email has been resent to tenant.'];
                }
                
            }
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage(), 'message'=> 'Tenant not saved in move-in.'];
        }
    }

    public function updateTenantDeclineStatus($post){
        $userid = $post['user_id'];
        $query1 = "DELETE FROM `e_sign_history` WHERE  user_id='$userid'";
        $stmt1 = $this->companyConnection->prepare($query1);
        $stmt1->execute();

        $query2 = "DELETE FROM `movein_list_record` WHERE  user_id='$userid'";
        $stmt2 = $this->companyConnection->prepare($query2);
        $stmt2->execute();

        $query3 = "DELETE FROM `tenant_rental_lease_signaures` WHERE  user_id='$userid' OR parent_id='$userid'";
        $stmt3 = $this->companyConnection->prepare($query3);
        $stmt3->execute();

        $this->sendEmailToTenants($post);
    }

    public function residentialLeaseAgreement(){
        try{

            $userid = $_POST['id'];
            $data = [];
            $returnData = [];
            $returnRecords = [];
            $addtionalTenantRecords = [];
            $ownersRecords = [];
            $guarantorsRecords = [];
            $data['user'] =  $this->companyConnection->query("SELECT company_logo  FROM users where id=".$userid)->fetch();
            $data['lease'] =  $this->companyConnection->query("SELECT * FROM tenant_lease_details where user_id=".$userid)->fetch();
            $data['grace'] =  $this->companyConnection->query("SELECT grace_period FROM tenant_charge where user_id=".$userid)->fetch();
            $data['tproperty'] =  $this->companyConnection->query("SELECT * FROM tenant_property where user_id=".$userid)->fetch();
            $data['property'] =  $this->companyConnection->query("SELECT address1, address2, address3, address4, city, state, country, zipcode FROM general_property where id=".$data['tproperty']['property_id'])->fetch();
            $data['days'] =  $this->companyConnection->query("SELECT tenant_time_bond FROM default_settings")->fetch();
            $data['pm'] =  $this->companyConnection->query("SELECT name, company_name, phone_number FROM users where id=1")->fetch();

            $name = $this->getUserName($userid, 'users');
            $startDate = date("m/d/Y", strtotime($data['lease']['start_date']));
            $endDate = date("m/d/Y", strtotime($data['lease']['end_date']));

            $address = "";
            if (isset($data['property']['address1']) && $data['property']['address1'] != ""){
                $address .= $data['property']['address1'];
            }
            if (isset($data['property']['address2']) && $data['property']['address2'] != ""){
                $address .= ', '.$data['property']['address2'];
            }
            if (isset($data['property']['address3']) && $data['property']['address3'] != ""){
                $address .= ', '.$data['property']['address3'];
            }
            if (isset($data['property']['address4']) && $data['property']['address4'] != ""){
                $address .= ', '.$data['property']['address4'];
            }
            if (isset($data['property']['city']) && $data['property']['city'] != ""){
                $address .= ', '.$data['property']['city'];
            }
            if (isset($data['property']['state']) && $data['property']['state'] != ""){
                $address .= ', '.$data['property']['state'];
            }
            if (isset($data['property']['country']) && $data['property']['country'] != ""){
                $address .= ', '.$data['property']['country'];
            }
            if (isset($data['property']['zipcode']) && $data['property']['zipcode'] != ""){
                $address .= ', '.$data['property']['zipcode'];
            }
            if ($data['user']['company_logo'] != "") {
                $logoURL = $_SERVER['HTTP_HOST'].'/'.$data['user']['company_logo'];   
            }else{
                $logoURL = '';
            }

            $signatureData = $this->getSignatureData($userid);

            $returnRecords['TENANTNAME'] = $name;
            $returnRecords['LEASESTARTDATE'] = $startDate;
            $returnRecords['LEASEENDDATE'] = $endDate;
            $returnRecords['ALLADDRESS'] = $address;
            $returnRecords['RENTAMOUNT'] = $data['lease']['rent_amount'];
            $returnRecords['GRADEPERIOD'] = $data['grace']['grace_period'];
            $returnRecords['SECURITYDEPOSITE'] = $data['lease']['security_deposite'];
            $returnRecords['RENTBEFOREDAY'] = $data['lease']['rent_due_day'];
            $returnRecords['NOTICEPERIODDAYS'] = $data['lease']['notice_period'];
            $returnRecords['COMPANYLOGO'] = $logoURL;
            $returnRecords['days'] = $data['days']['tenant_time_bond'];

            $getTenantData = $this->getTenants($userid);
            $additionalTenantData = $this->getAdditionalTenants($userid);
            $ownersData = $this->getOwners($userid);
            $managersData = $this->getManagers($userid);
            $guarantorsData = $this->guarantorsData($userid);

            $returnData['tenantData'] = $name;
            $returnData['tenantId'] = $userid;
            $returnData['additionalTenantData'] = $additionalTenantData;
            $returnData['ownersData'] = $ownersData;
            $returnData['managersData'] = $managersData;
            $returnData['guarantorsData'] = $guarantorsData;
            $returnData['leaseData'] = $returnRecords;
            $returnData['getTenantData'] = $getTenantData;
            $returnData['signatureData'] = $signatureData;
            $returnData['pm'] = $data['pm'];;

            return ['code'=>200, 'status'=>'success', 'data'=>$returnData];
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
        }
    }

    public function getAdditionalTenants($userId){
        $sql = "SELECT * FROM `tenant_additional_details` WHERE user_id='$userId'";
        $record = $this->companyConnection->query($sql)->fetchAll();
        $additionalArray = [];
        if (!empty($record)){
            $addCount = count($record);
            $count = 0;
            foreach ($record as $additional){
                $addid =$additional['id'];
                $addName =$additional['first_name'].' '.$additional['last_name'];
                $sql2 = "SELECT * FROM tenant_rental_lease_signaures WHERE user_id='$addid' AND parent_id='$userId' AND usertype = 'AT'";
                $record2 = $this->companyConnection->query($sql2)->fetch();
                if ($addCount > 1){
                    if ($count == 0){
                        if (!empty($record2)){


                            $additionalArray1['id'] = $addid;
                            $additionalArray1['name'] = $addName;
                            $additionalArray1['status'] = $record2['status'];
                            $additionalArray1['signature_image'] = $record2['signature_image'];
                            $additionalArray1['signature_text'] = $record2['signature_text'];
                            $additionalArray1['usertype'] = $record2['usertype'];
                            $additionalArray1['style'] = $record2['style'];
                            $additionalArray1['updated_at'] = $record2['updated_at'];
                            $additionalArray1['done'] = '1';
                        }else{
                            $additionalArray1['id'] = $addid;
                            $additionalArray1['name'] = $addName;
                            $additionalArray1['status'] = '0';
                            $additionalArray1['signature_image'] = '';
                            $additionalArray1['signature_text'] = '';
                            $additionalArray1['usertype'] = '';
                            $additionalArray1['style'] = '';
                            $additionalArray1['updated_at'] = '';
                            $additionalArray1['done'] = '1';
                        }
                    }else{
                        if (!empty($record2)){
                            $additionalArray1['id'] = $addid;
                            $additionalArray1['name'] = $addName;
                            $additionalArray1['status'] = $record2['status'];
                            $additionalArray1['signature_image'] = $record2['signature_image'];
                            $additionalArray1['signature_text'] = $record2['signature_text'];
                            $additionalArray1['usertype'] = $record2['usertype'];
                            $additionalArray1['style'] = $record2['style'];
                            $additionalArray1['updated_at'] = $record2['updated_at'];
                            $additionalArray1['done'] = '0';
                        }else{
                            $additionalArray1['id'] = $addid;
                            $additionalArray1['name'] = $addName;
                            $additionalArray1['status'] = '0';
                            $additionalArray1['signature_image'] = '';
                            $additionalArray1['signature_text'] = '';
                            $additionalArray1['usertype'] = '';
                            $additionalArray1['style'] = '';
                            $additionalArray1['updated_at'] = '';
                            $additionalArray1['done'] = '0';
                        }
                    }
                }else{
                    if (!empty($record2)){
                        $additionalArray1['id'] = $addid;
                        $additionalArray1['name'] = $addName;
                        $additionalArray1['status'] = $record2['status'];
                        $additionalArray1['signature_image'] = $record2['signature_image'];
                        $additionalArray1['signature_text'] = $record2['signature_text'];
                        $additionalArray1['usertype'] = $record2['usertype'];
                        $additionalArray1['style'] = $record2['style'];
                        $additionalArray1['updated_at'] = $record2['updated_at'];
                    }else{
                        $additionalArray1['id'] = $addid;
                        $additionalArray1['name'] = $addName;
                        $additionalArray1['status'] = '0';
                        $additionalArray1['signature_image'] = '';
                        $additionalArray1['signature_text'] = '';
                        $additionalArray1['usertype'] = '';
                        $additionalArray1['style'] = '';
                        $additionalArray1['updated_at'] = '';
                    }
                }
                $count++;
                $additionalArray[] =$additionalArray1;
            }
            return ['code'=>200, 'status'=>'success', 'data'=>$additionalArray];
        }else{
            return ['code'=>200, 'status'=>'success', 'data'=>''];
        }
    }

    public function getTenants($userId){
        try{
            $sql = "SELECT * FROM tenant_rental_lease_signaures WHERE user_id='$userId' AND parent_id='0' AND usertype = 'T'";
            $record = $this->companyConnection->query($sql)->fetch();
            $additionalArray1 = [];
            if (!empty($record)){
                $additionalArray1['id'] = $userId;
                $additionalArray1['name'] = userName($userId, $this->companyConnection,'users');
                $additionalArray1['status'] = $record['status'];
                $additionalArray1['signature_image'] = $record['signature_image'];
                $additionalArray1['signature_text'] = $record['signature_text'];
                $additionalArray1['usertype'] = $record['usertype'];
                $additionalArray1['style'] = $record['style'];
                $additionalArray1['updated_at'] = $record['updated_at'];
                $additionalArray1['done'] = '1';
            }else{
                $additionalArray1['id'] = $userId;
                $additionalArray1['name'] = userName($userId, $this->companyConnection,'users');
                $additionalArray1['status'] = '0';
                $additionalArray1['signature_image'] = '';
                $additionalArray1['signature_text'] = '';
                $additionalArray1['usertype'] = '';
                $additionalArray1['style'] = '';
                $additionalArray1['updated_at'] = '';
                $additionalArray1['done'] = '1';
            }
            return ['code'=>200, 'status'=>'success', 'data'=>$additionalArray1];
        }catch (Exception $exception){
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    public function getSignatureData($userId = 0,$parentId = '0',$userTypes = 'T'){
            $sql = "SELECT * FROM tenant_rental_lease_signaures WHERE user_id=$userId AND parent_id='$parentId' AND usertype = '$userTypes'";
            $record = $this->companyConnection->query($sql)->fetch();
            if (!empty($record)){
                return ['code'=>200, 'status'=>'success', 'data'=>$record];
            }else{
                return ['code'=>200, 'status'=>'success', 'data'=>''];
            }
    }

    public function getOwners($userId){
        $sql = "SELECT * FROM tenant_property WHERE user_id=$userId";
        $tenantPorperty = $this->companyConnection->query($sql)->fetch();

        if (!empty($tenantPorperty)){
            $propertyId = $tenantPorperty['property_id'];
            $sql = "SELECT * FROM general_property WHERE id=$propertyId";
            $porpertyOwner = $this->companyConnection->query($sql)->fetchAll();

            if (!empty($porpertyOwner)){
                $ownerName = [];
                foreach ($porpertyOwner as $porpertyOwnerVal){
                    $porpertyOwner = $porpertyOwnerVal['owner_id'];
                    if(!empty($porpertyOwner)){
                        $oUser = unserialize($porpertyOwner);
                        if ($oUser != ""){
                            foreach ($oUser as $o_user){
                                $sql = "SELECT id,first_name, last_name FROM users WHERE id=$o_user";
                                $oUser = $this->companyConnection->query($sql)->fetch();

                                $name = $oUser['first_name'].' '.$oUser['last_name'];
                                $newArray['id'] =  $oUser['id'];
                                $newArray['name'] =  $name;
                                $oId = $oUser['id'];
                                $sql1 = "SELECT * FROM tenant_rental_lease_signaures WHERE user_id=$oId AND parent_id='$userId' AND usertype = 'O'";
                                $record1 = $this->companyConnection->query($sql1)->fetch();
                                if (!empty($record1)){
                                    $newArray['status'] =  $record1['status'];
                                    $newArray['signature_image'] =  $record1['signature_image'];
                                    $newArray['signature_text'] =  $record1['signature_text'];
                                    $newArray['style'] =  $record1['style'];
                                    $newArray['updated_at'] =  $record1['updated_at'];
                                }else{
                                    $newArray['status'] =  '0';
                                    $newArray['signature_image'] =  '';
                                    $newArray['signature_text'] =  '';
                                    $newArray['style'] =  '';
                                    $newArray['updated_at'] =  '';
                                }

                                $ownerName[] = $newArray;
                            }

                        }
                    }
                }
                return ['code'=>200, 'status'=>'success', 'data'=>$ownerName];
            }else{
                return ['code'=>200, 'status'=>'success', 'data'=>''];
            }
        }else{
            return ['code'=>200, 'status'=>'success', 'data'=>''];
        }
    }

    public function getManagers($userId){

        $sql = "SELECT * FROM tenant_property WHERE user_id=$userId";
        $tenantPorperty = $this->companyConnection->query($sql)->fetch();

        if (!empty($tenantPorperty)){
            $propertyId = $tenantPorperty['property_id'];
            $sql = "SELECT * FROM general_property WHERE id=$propertyId";
            $porpertyOwner = $this->companyConnection->query($sql)->fetchAll();

            if (!empty($porpertyOwner)){
                $ownerName = [];
                foreach ($porpertyOwner as $porpertyOwnerVal){
                    $porpertyOwner = $porpertyOwnerVal['manager_id'];
                    if(!empty($porpertyOwner)){
                        $oUser = unserialize($porpertyOwner);
                        if ($oUser != ""){
                            foreach ($oUser as $o_user){
                                $sql = "SELECT id,first_name, last_name FROM users WHERE id=$o_user";
                                $oUser = $this->companyConnection->query($sql)->fetch();

                                $name = $oUser['first_name'].' '.$oUser['last_name'];
                                $newArray['id'] =  $oUser['id'];
                                $newArray['name'] =  $name;
                                $oId = $oUser['id'];
                                $sql1 = "SELECT * FROM tenant_rental_lease_signaures WHERE user_id=$oId AND parent_id='$userId' AND usertype = 'O'";
                                $record1 = $this->companyConnection->query($sql1)->fetch();
                                if (!empty($record1)){
                                    $newArray['status'] =  $record1['status'];
                                    $newArray['signature_image'] =  $record1['signature_image'];
                                    $newArray['signature_text'] =  $record1['signature_text'];
                                    $newArray['style'] =  $record1['style'];
                                    $newArray['updated_at'] =  $record1['updated_at'];
                                }else{
                                    $newArray['status'] =  '0';
                                    $newArray['signature_image'] =  '';
                                    $newArray['signature_text'] =  '';
                                    $newArray['style'] =  '';
                                    $newArray['updated_at'] =  '';
                                }

                                $ownerName[] = $newArray;
                            }

                        }
                    }
                }
                return ['code'=>200, 'status'=>'success', 'data'=>$ownerName];
            }else{
                return ['code'=>200, 'status'=>'success', 'data'=>''];
            }
        }else{
            return ['code'=>200, 'status'=>'success', 'data'=>''];
        }
    }

    public function guarantorsData($userId){

        $sql = "SELECT * FROM tenant_guarantor WHERE user_id=$userId";
        $tenantGuarantor = $this->companyConnection->query($sql)->fetchAll();
        if (!empty($tenantGuarantor)){
            $guarantorName = [];
            foreach ($tenantGuarantor as $tenantGuarantorVal){
                $name = $tenantGuarantorVal['first_name'].' '.$tenantGuarantorVal['last_name'];
                $newArray['id'] = $tenantGuarantorVal['id'];
                $newArray['name'] = $name;

                $gId = $tenantGuarantorVal['id'];

                $sql1 = "SELECT * FROM tenant_rental_lease_signaures WHERE user_id=$gId AND parent_id='$userId' AND usertype = 'G'";
                $record = $this->companyConnection->query($sql1)->fetch();
                if (!empty($record)){
                    $newArray['status'] = $record['status'];
                    $newArray['user_id'] = $record['user_id'];
                    $newArray['signature_image'] = $record['signature_image'];
                    $newArray['signature_text'] = $record['signature_text'];
                    $newArray['parent_id'] = $record['parent_id'];
                    $newArray['usertype'] = $record['usertype'];
                    $newArray['style'] = $record['style'];
                    $newArray['updated_at'] = $record['updated_at'];
                    $newArray['record_status'] = $record['record_status'];
                }else{
                    $newArray['status'] = '0';
                    $newArray['user_id'] = $gId;
                    $newArray['signature_image'] = '';
                    $newArray['signature_text'] = '0';
                    $newArray['parent_id'] = $userId;
                    $newArray['usertype'] = $record['usertype'];
                    $newArray['style'] = '';
                    $newArray['updated_at'] = $record['updated_at'];
                    $newArray['record_status'] = $record['record_status'];
                }

                $guarantorName[] = $newArray;
            }
            return ['code'=>200, 'status'=>'success', 'data'=>$guarantorName];
        }else{
            return ['code'=>200, 'status'=>'success', 'data'=>''];
        }
    }

    public function checkIfSignedOrNot(){
        $userType = $_POST['userType'];
        if ($userType == "T" ){
            $user_id = $_POST['id'];
            $parent_id = '0';
        }else{
            $user_id = $_POST['paramId'];
            $parent_id = $_POST['id'];
        }

        $sql = "SELECT * FROM `tenant_rental_lease_signaures` WHERE user_id=".$user_id." AND parent_id = ".$parent_id;
       // dd($sql);
        $data = $this->companyConnection->query($sql)->fetch();
        //dd($data);
        if (!empty($data)){
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }
        return ['code'=>503, 'status'=>'error', 'data'=>''];
    }

    public function createPdfFile(){
        $url = $_POST['url'];
        ob_start();
        $html = file_get_contents($url);
        phpinfo();
        die();
        /*$html2pdf = new Html2Pdf();
        $html2pdf->writeHTML('<h1>HelloWorld</h1>This is my first test');
        $html2pdf->output();*/

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . 'generated_lease_files';
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (!is_dir($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        //Close and output PDF document

        $content_pdf = ob_get_contents();
        ob_end_flush();

        $fileUrl = $uploadPath.'/Lease_AuthorizationAgreement.pdf';

        //file_put_contents($fileUrl, $output);
        //chmod($fileUrl, 0777);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'Lease_AuthorizationAgreement.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
    }

    public function createPdfFile2(){
        $url = $_POST['url'];
        $html = file_get_contents($url);
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . 'generated_lease_files';
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (!is_dir($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        $fileUrl = $uploadPath.'/Lease_AuthorizationAgreement.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        chmod($fileUrl, 0777);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'Lease_AuthorizationAgreement.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
    }
    public function checkSignatureDetails(){
        $tenant_id=$_POST['tenant_id'];
        $id=$_POST['id'];
        $sql = "SELECT style FROM tenant_rental_lease_signaures WHERE user_id=".$tenant_id." AND  parent_id=".$id;

        $data = $this->companyConnection->query($sql)->fetch();
    }

    public function createPdfFile3($id,$html){
        try {

            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $domain = getDomain();
            $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
            $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'].'/lease_AuthorizationAgreement/';
            $tempName = 'Lease_AuthorizationAgreement'.$id.'.pdf';
            $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'Lease_AuthorizationAgreement'.$id.'.pdf';
            if (!is_dir(COMPANY_DIRECTORY_URL . '/' . $path)) {
                //Directory does not exist, so lets create it.
                mkdir(COMPANY_DIRECTORY_URL . '/' . $path, 0777, true);
            }
           // $fileUrl = $uploadPath . '/Lease_AuthorizationAgreement.pdf';
            $output = $dompdf->output();
            file_put_contents($fileUrl, $output);
            $fileUrl2 = "http://" . $_SERVER['HTTP_HOST'] . '/company/' . $path . 'Lease_AuthorizationAgreement'.$id.'.pdf';
            return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2, 'message' => 'Record retrieved successfully','path' => $path,'tempName' => $tempName );
        } catch (Exception $exception) {
            dd($exception);
        }
    }

    /**
     * function to create lease document pdfS
     * @param $data
     * @return string
     */
    public function createPdfHtml($dataHTML,$data){
        // dd(COMPANY_DIRECTORY_URL.$data['company_logo']);
        $html = "<!DOCTYPE html>
                    <html>
                    <head>
					<meta charset=\"utf-8\">
                        <title>E-Signature</title>
                    </head>
                    <body>
                    <div class=\"\" style=\"float: left; width: 100%;\" id=\"dv-Main-pdf\">
                        <div class=\"\" id=\"dvPdfToHtml\" style=\"min-height:500px;overflow-y:scroll;float:left;width:80%; padding:2%;border:1px solid #ddd;margin-left: 8%;\">
                            <div  style=\"border-radius: 6px 6px 0px 0px; -moz-border-radius: 6px 6px 0px 0px; -webkit-border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2D3091; margin: auto;\">
                                <table style=\"width: 100%;font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse; padding: 0; margin: 0;\">
                                    <tbody>
                                        <tr style=\"background-color: #00b0f0; height: 20px;\"><td></td></tr>
                                        <tr style=\"border-collapse: collapse; background-color: #fff;\">
                                            <td class=\"w580\" style=\"font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse: collapse;text-align:center;\">
                                            <img class=\"COMPANYLOGO\" alt='company_logo' src='".COMPANY_DIRECTORY_URL.$data['company_logo']."' width=\"150\" height=\"50\">
                                            </td>
                                        </tr>
                                        <tr style=\"background-color: #00b0f0; height: 20px;\"><td></td></tr>
                                    </tbody>
                                </table>
                            </div>
                            
                                <div>
                                    <div  style=\"font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; font-size: 13px; padding: 0; line-height: 19px; margin: 0; border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;\">
                                        <p style=\"margin: 5px 0; text-align: left;\">&nbsp;</p>
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td style=\"color: #000; font-weight: bold; line-height: 5px; font-size: 12px;\">
                                                    Residential Lease agreement<br>
                                                    <span style=\"font-size: 7px;\">______________________________________________</span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style=\"padding: 0 10px; margin-bottom: 5px; text-align: left; font-weight: bold;\"> 
                                            Please read carefully and accept the electronic record consent and <a class=\"e_sign_doc_url\"  style=\"color: #079b21\"> e-signature disclosure</a>
                                        </p>
                                            <div style=\"padding: 0 10px; margin-bottom: 5px; text-align: left; font-weight: bold;\"> I agree to use electronic signature and records.</div>
                                       
                                        <p>&nbsp;</p>
                                        <p style=\"padding: 0 10px; margin-bottom: 5px; text-align: left;\">
                                            The following residential agreement is between <span class=\"COMPANYNAME\">".$data['company_name']."</span> . new and <span class=\"TENANTNAME\">".$data['tenant_name']."</span>,
                                            as of <span class=\"LEASESTARTDATE\"></span>. Each numbered section defines the term, conditions, and
                                            responsibility of each party.
                                        </p>
                                        <div style=\"width: 85%;text-align:left;\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; line-height: 5px; font-weight: bold; padding-right: 15px;
                                                    font-size: 12px; text-align: left; width: 150px;\">
                                                        LANDLORD
                                                    </td>
                                                    <td style=\"text-align: left;  border-bottom: 1px solid blue;\">
                                                        In this agreement the Landlord(s) and/or related agent(s) is/are referred to as
                                                        “Landlord”. <span class=\"COMPANYNAME\">".$data['company_name']."</span> . new is the Landlord.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left;\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; line-height: 5px; font-weight: bold; padding-right: 15px;
                                                    font-size: 12px; text-align: left; width: 150px;\" >
                                                        TENANT
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\" >
                                                        Every instance of the term “Tenant” in this Lease will refer to <span class=\"TENANTNAME\">".$data['tenant_name']."</span>.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left; margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; font-weight: bold; padding-right: 15px; line-height: 16px; font-size: 12px;
                                                    text-align: left; width: 150px;\">
                                                        RENTAL PROPERTY
                                                    </td>
                                                    <td style=\"text-align: left;  border-bottom: 1px solid blue;\">
                                                        The property located at <span class=\"ALLADDRESS\">".$data['address_list']."</span>,
                                                        which is owned/managed
                                                        by the Landlord will be rented to the Tenant. In this Lease the property will be
                                                        referred to as “lease Premises”.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left; margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; font-weight: bold;  padding-right: 15px; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        TERM OF LEASE AGREEMENT
                                                    </td>
                                                    <td style=\"text-align: left;  border-bottom: 1px solid blue;\">
                                                        This Lease Agreement will start on <span class=\"LEASESTARTDATE\">".$data['start_date']."</span> and ends on <span class=\"LEASEENDDATE\">".$data['end_date']."</span>.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left; margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        USE &amp; OCCUPANCY OF PROPERTY
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        The only authorized person(s) that may live in the Leased Premises is/are: <span class=\"TENANTNAME\">".$data['tenant_name']."</span>.
                                                    </td>
                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left; margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        AMOUNT OF RENT
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        The exact amount of Rent due monthly for the right to live in the Leased Premises
                                                        is <span class=\"RENTAMOUNT\">".$data['rent_amount']."</span>.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left; margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091;  width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        DUE DATE FOR RENT
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        Rent is to be paid before or on the <span class=\"RENTBEFOREDAY\"></span>. A <span class=\"GRADEPERIOD\">".$data['grace_period']."</span> day grace period
                                                        will ensue; nevertheless, rent cannot be paid after this grace period without
                                                        incurring
                                                        a late fee.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left; margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091;  width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        LATE FEE
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        In the event that rent or associated fees are not paid in full by the rental due
                                                        date or before the end of the <span class=\"GRADEPERIOD\">".$data['grace_period']."</span> day grace period, a late fee of __________________
                                                        will be incurred.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left; margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 5px; font-size: 12px;
                                                    text-align: left;\">
                                                        RETURNED PAYMENTS
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        Any returned payments from financial institutions that the Tenant utilizes will
                                                        incur an addition fee of 0.00 that will be added to the rental amount.
                                                    </td>
                                                </tr>
                                              
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left; margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 5px; font-size: 12px;
                                                    text-align: left;\">
                                                        SECURITY DEPOSIT
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        A. Security Deposit of <span class=\"SECURITYDEPOSITE\">".$data['security_deposite']."</span> is to be paid by the Tenant before move-in.<br>
                                                        B. This Security Deposit is reserved for the purpose of paying any cost toward
                                                        damages,
                                                        cleaning, excessive wear to the property, and in the event of unreturned keys or
                                                        abandonment of the Leased Premises before or upon the end of the Lease.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left; margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        UTILITIES &amp; SERVICES
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        A. The responsibility of the registering and paying for the following utility
                                                        services is upon the Tenant: <br>
                                                        These services should be maintained at all time during the leasing period.<br>
                                                        B. The Landlord has included the Water utility as a part of rent; however, if the
                                                        property water usage exceeds Enter amount here the Landlord reserves the right to
                                                        request that any overages in usage be compensated as a fee in addition to the normal
                                                        rental rate. These overages must be paid within 20 days.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        MAINTENANCE &amp; REPAIRS
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        General maintenance and repairs are the responsibility of the Landlord, except in
                                                        cases where the Tenant has been negligent or has accidentally caused damages to
                                                        the property. <br>
                                                    A. It will remain the Tenant’s responsibility to promptly notify the Landlord of
                                                    any necessary repairs to the property.<br>
                                                    B. Any damages that have resulted from the Tenant or the Tenant’s guests will be
                                                    the Tenants full responsibility to pay for the costs of repair that is required.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        CONDITION OF PROPERTY
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        A. The Tenant has inspected the Leased Premises acknowledges and accepts that the
                                                        Leased Premises is in an acceptable living condition and that all parts of the
                                                        Leased Premises are in good working order. <br>
                                                        B. It is agreed by the Landlord and Tenant that no promises are made concerning
                                                        the condition of the Leased Premises. <br>
                                                        C. The Leased Premises will be returned to the Landlord by the Tenant in the same
                                                        or better condition that it was in at the commencement of the Lease.

                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        ENDING OR RENEWING THE LEASE AGREEMENT
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        A. When this Lease ends, if the Tenant or Landlord do not furnish a written
                                                        agreement to end the Lease it will continue on a month to month basis indefinitely. In order
                                                        to terminate or renew the Lease agreement either party, Landlord or Tenant, must
                                                        furnish a written notice at least <span class=\"NOTICEPERIODDAYS\">".$data['notice_period']."</span> days before the end of this Lease agreement.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                                    text-align: left;\">
                                                        GOVERNING LAW
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        The terms, conditions, and any addenda to this Lease are to be governed and held
                                                        in accord to the statutes and Laws of Enter Governing entity here..
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style=\"width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px\">
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <p style=\"text-align: left;\">&nbsp;</p>
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td style=\"color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 5px; font-size: 12px;
                                                    text-align: left;\">
                                                        ENTIRE AGREEMENT
                                                    </td>
                                                    <td style=\"text-align: left; border-bottom: 1px solid blue;\">
                                                        NOTICE: This is a LEGALLY binding document.<br>
                                                        A. You are relinquishing specific important rights. <br>
                                                        B. You may reserve the right to have an attorney review the Lease Agreement before
                                                        signing it. <br>
                                                        By signing this Lease Agreement, the Tenant certifies in good faith to have read,
                                                        understood, and agrees to abide by all of the terms and conditions stated within
                                                        this Lease.
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        ".$dataHTML."
                                       
                                    </div>
                                </div>
                            
                            <div style=\"width:100%;border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px;
                            -webkit-font-smoothing: antialiased; background-color: #585858; color: #fff;clear: both;\">
                                <table style=\"font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;
                                border-collapse: collapse; padding: 0; margin: 0;width:100%;\">
                                    <tbody>
                                    <tr>
                                        <td style=\"padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold; text-align:center;\">
                                            <span class=\"pmname\"><span class=\"COMPANYNAME\">".$data['company_name']."</span> </span> Property Manager● <span class=\"pmphone\">".$data['phone_number']."</span>
                                        </td>
                                    </tr>
                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                        
                    </body>
                </html>";
        return $html;
    }

}
$generateLease = new GenerateLease();