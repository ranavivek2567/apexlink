<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class paymentVendor extends DBConnection {

    /**
     * paymentVendor constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * Edit payment vendor function
     */
    public function editPaymentVendor() {
        try{
            $data = $_POST['form'];
            $data = postArray($data);
            $edit_user_id = $_POST['vendor_id'];
            /*Required variable array*/
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [ ];
            /*Number variable array*/
            $number_array = [];
            /*Server side validation*/
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array, $edit_user_id, 'users');
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                /*update user data*/
                $userData = [];
                $userData['tax_payer_name'] = $data['tax_payer_name'];
                $userData['tax_payer_id'] = $data['tax_payer_id'];

                $sqlData1 = createSqlUpdateCase($userData);
                $query1 = "UPDATE users SET " . $sqlData1['columnsValuesPair'] . " where id='$edit_user_id'";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($sqlData1['data']);

                unset($data['tax_payer_name'],$data['tax_payer_id']);
                /*vendor additional detail*/
                $sqlData = createSqlUpdateCase($data);
                $query = "UPDATE vendor_additional_detail SET " . $sqlData['columnsValuesPair'] . " where vendor_id='$edit_user_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($sqlData['data']);
                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully.');
            }
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e);
        }
    }
}

$addVendor = new paymentVendor();
?>