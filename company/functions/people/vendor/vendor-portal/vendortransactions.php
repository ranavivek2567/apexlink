<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 5/29/19
 * Time: 6:23 PM
 */
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //$url = BASE_URL . "login";
    header('Location: ' . BASE_URL);
};


class VendorPortalAjax extends DBConnection{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    public function gettransactiondata()
    {
        $user_id = $_POST['user_id'];
        $start_date=$_POST['st_date'];
        $end_date=$_POST['end_date'];
    //    dd("SELECT id,transaction_id,total_charge_amount,payment_response,DATE_FORMAT(created_at,'%Y-%m-%d') as created_at,reference_no FROM `transactions` WHERE user_id='$user_id' and DATE_FORMAT(created_at,'%Y-%m-%d') between BETWEEN '$start_date' and '$end_date'");
        $getData =  $this->companyConnection->query("SELECT id,transaction_id,total_charge_amount,payment_response,DATE_FORMAT(created_at,'%Y-%m-%d') as created_at,reference_no FROM `transactions` WHERE user_id='$user_id' and DATE_FORMAT(created_at,'%Y-%m-%d') BETWEEN '$start_date' and '$end_date'") ->fetchAll();
        //and created_at BETWEEN '$start_date' and '$end_date'
      // dd($getData);
        $dates=[];
        for($i=0;$i<count($getData);$i++){
            $date = dateFormatUser($getData[$i]['created_at'],null,$this->companyConnection);
            array_push($dates,$date);
        }
        return ['code'=>200,'message'=>'success','data'=>$getData,'dates'=>$dates];
    }


    public function getPdfContent(){
        try{
            $html=$_POST['htmls'];
            $html='<html><table>'.$html.'</table></html>';
            $pdf = $this->createHTMLToPDF($html);
            $data['record'] = $pdf['data'];
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function createHTMLToPDF($report){
        $dompdf = new Dompdf();
        $dompdf->loadHtml($report);
        $dompdf->render();
        $path = "uploads/";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'/Ticket.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'/Ticket.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
    }



}
$tenantAjax = new VendorPortalAjax();