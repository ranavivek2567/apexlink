<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};
class reportsView extends DBConnection
{
    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }



    public function taxHtml() {
        $currentYear=date('Y');
        $yearArray = range(1900, $currentYear);
        $html = "<option value=''>Select</option>";
        foreach ($yearArray as $year) {
            $html .='<option>' . $year . '</option>';
        }

        $propertyhtml = '';
        $sql = "SELECT id,property_name FROM general_property WHERE status ='1'";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {
            $propertyhtml.= '<option data-id="'.$d['id'].'" value=' . $d['id'] . '>' . $d['property_name'] . '</option>';
        }
        return array('data' => $html, 'status' => 'success','propertyhtml'=>$propertyhtml);
    }
    public function getVendorSummaryReport(){
            $data = $_POST['form'];
            $totalvendorsumm=[];
            $propertyids=implode(',',$data['selectPropertyhtml']);

            $html='';
            $query1="SELECT REPLACE(u.name,' ','--'),REPLACE(gp.property_name,' ','--'),REPLACE(u.state,' ','--'),REPLACE(u.tax_id,' ','--'),vad.vendor_rate AS amt from owner_blacklist_vendors obv JOIN users u ON u.id=obv.vendor_id JOIN general_property gp on gp.id=obv.property_id LEFT JOIN vendor_additional_detail vad ON u.id=vad.vendor_id WHERE gp.id IN ($propertyids)";
            $stmt1 = $this->companyConnection->query($query1)->fetchAll();

            $html .='  <table width="100%" class="title-table" border="0">
	                            <tr>
	                                <td colspan="2"><img style="width: 137px; height: 49px;" src="http://phytotherapy.in:8097/company/images/logo.png" alt=""> </td>
	                            </tr>\';
	                           <tr>
	                            <td>Title</td>
	                           	<td align="right" style="padding-right: 200px;" >\' . $filter_date . \'</td>
	                            </tr>
	                            <tr>
	                            <td colspan="2" class="lease-hading"> table title1</td>
	                            </tr>
	                            <tr>
	                            <td colspan="2" class="lease-hading2"> table title2</td>
	                            </tr>
	                        </table> 
	                                               
	                        <table width="100%" class="table table-report-record table-hover reports_table">
	                        <thead>
	                           <tr >
	                           	<td colspan="2" class="font-bold">Assets</td>
	                           </tr>
	                           <tr >
	                           	<td colspan="2" class="font-bold">Current Assets</td>
	                           </tr>
	                           <tr >
	                           	<td colspan="2" class="font-bold">Cash & Cash Equivalents</td>
	                           </tr>
	                           <tr >
	                           	<td>Cash in Bank-Operating </td>
	                           	<td align="right" class="red-star">-530.00</td>
	                           </tr>
	                           <tr >
	                           	<td>Payroll Account </td>
	                           	<td align="right">-0.00</td>
	                           </tr>
	                           <tr class="total-cash">
	                           	<td>Payroll Account </td>
	                           	<td align="right">-0.00</td>
	                           </tr>
	                        </thead>
	                    </div>';

//            foreach ($stmt1 as $key=>$value){
//                if(isset($value['amt']) && !empty($value['amt'])) {
//
//                    $abc=str_replace($_SESSION[SESSION_DOMAIN]['default_currency_symbol'],'',$value['amt']);
//                    // print_r($_SESSION[SESSION_DOMAIN]['default_currency_symbol']);die;
//                    $abc= str_replace('.00','',$abc);
//                    $abc=str_replace(',','',$abc);
//                    array_push($totalvendorsumm,$abc);
//                }
//            }

//                $html .= ' <tr><td colspan="4" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #05A0E4; font-weight: 700;" class="" href="javascript:;">Total: ' . count($stmt1) . '  </a></td>
//                 <td colspan="2" style="border-right:1px solid #ddd; border-bottom: 1px solid #ddd; border-left: 0px solid #ddd; line-height: 20px; padding-bottom: 5px; padding-top: 5px;"><a style=" color: #05A0E4; font-weight: 700;" class="" href="javascript:;">'.$_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . number_format(array_sum($totalvendorsumm)). '.00'.'  </a></td>
//                 </tr>';

        return array('html' => $html, 'status' => 'success');
    }
}
$reportsView = new reportsView();