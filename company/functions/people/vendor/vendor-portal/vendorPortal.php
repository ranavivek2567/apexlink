<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class VendorPortal extends DBConnection{

    public function __construct(){
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }


    /**
     * function to get property details
     */
    public function getVendorDetail()
    {

        $id = $_POST['vendor_id'];
        $table = 'users';
        $vendor_additional_data_table = 'vendor_additional_detail';
        $columns = ['vendor_additional_detail.*','users.*','company_vendor_type.vendor_type as vendor_type_id_acc'];
        $joins = [[
            'type' => 'LEFT',
            'table' => 'users',
            'column' => 'id',
            'primary' => 'vendor_id',
            'on_table' => 'vendor_additional_detail',
            'as' => 'vendor_additional_detail'
        ],[
            'type' => 'LEFT',
            'table' => 'vendor_additional_detail',
            'column' => 'vendor_type_id',
            'primary' => 'id',
            'on_table' => 'company_vendor_type',
            'as' => 'company_vendor_type'
        ]];
        $where = [[
            'table' => 'users',
            'column' => 'id',
            'condition' => '=',
            'value' => $id
        ]];
        $query = selectQueryBuilder($columns, $table, $joins, $where);
        $data = $this->companyConnection->query($query)->fetch();
        $unserializeData = getUnserializedData($data, 'true');
        $emergency = $this->getEmergencyDetails($id);
        return array('code' => 200, 'status' => 'success', 'data' => $unserializeData, 'emergency' =>$emergency , 'message' => 'Record retrieved Successfully.');
    }

    public function getEmergencyDetails($id){
        //Emergency contact data

        $table2 = 'emergency_details';
        $columns2 = ['emergency_details.*'];
        $joins2 = [];
        $where2 = [[
            'table' => 'emergency_details',
            'column' => 'user_id',
            'condition' => '=',
            'value' => $id
        ]];
        $query2 = selectQueryBuilder($columns2, $table2, $joins2, $where2);
        $data2 = $this->companyConnection->query($query2)->fetchAll();
        $emergency['emergency_relation'] = '';
        $emergency['emergency_contact_name'] = '';
        $emergency['emergency_country_code'] = '';
        $emergency['emergency_phone_number'] = '';
        $emergency['emergency_email'] = '';
        if(!empty($data2)){
            foreach ($data2 as $key=>$value){
                $emergency['emergency_relation'] .= !empty($value['emergency_relation'])?getRelation($value['emergency_relation']).',':'';
                $emergency['emergency_contact_name'] .= !empty($value['emergency_contact_name'])?$value['emergency_contact_name'].',':'';
                $emergency['emergency_country_code'] .= !empty($value['emergency_country_code'])?$value['emergency_country_code'].',':'';
                $emergency['emergency_phone_number'] .= !empty($value['emergency_phone_number'])?$value['emergency_phone_number'].',':'';
                $emergency['emergency_email'] .= !empty($value['emergency_email'])?$value['emergency_email'].',':'';
            }
            $emergency['emergency_contact_name'] = $emergency['emergency_contact_name'] != ''? str_replace_last(',', '', $emergency['emergency_contact_name']):'N/A';
            $emergency['emergency_relation'] = $emergency['emergency_relation'] != ''? str_replace_last(',', '', $emergency['emergency_relation']):'N/A';
            $emergency['emergency_country_code'] = $emergency['emergency_country_code'] != ''? str_replace_last(',', '', $emergency['emergency_country_code']):'N/A';
            $emergency['emergency_phone_number'] = $emergency['emergency_phone_number'] != ''? str_replace_last(',', '', $emergency['emergency_phone_number']):'N/A';
            $emergency['emergency_email'] = $emergency['emergency_email'] != ''? str_replace_last(',', '', $emergency['emergency_email']):'N/A';
        }
        return $emergency;
    }


    public function getPortalInfo(){
        $tenant_id                 = $_REQUEST['tenant_id'];
        $getTenantInfo             = $this->getPropertyInfo($tenant_id);
        $data['property_name']     = $getTenantInfo['property_name'];
        $data['building_unit']     = $getTenantInfo['building_name'] . ' ' . $getTenantInfo['unit_no'];
        $getLeaseInfo              = $this->getLeaseInfo($tenant_id);
        $data['rent']              = $getLeaseInfo['rent_amount'];
        $data['security_deposite'] = $getLeaseInfo['security_deposite'];
        $data['notice_period']     = $getLeaseInfo['notice_period'];
        $data['move_in'] =           dateFormatUser($getLeaseInfo['move_in'], null, $this->companyConnection);
        $data['start_date']        = dateFormatUser($getLeaseInfo['start_date'], null, $this->companyConnection);
        $data['end_date']          = dateFormatUser($getLeaseInfo['end_date'], null, $this->companyConnection);
        $data['nsf']               = 100;
        $data['balance_due']       = 100;
        $getContactInfo            = $this->getContactInfo($tenant_id);
        $data['address1']          = $getContactInfo['address1'];
        $data['address2']          = $getContactInfo['address2'];
        $data['address3']          = $getContactInfo['address3'];
        $data['address4']          = $getContactInfo['address4'];
        $data['fullname']          = $getContactInfo['first_name'].' '.$getContactInfo['last_name'];
        $data['company_name']      = $getContactInfo['first_name'];
        $data['email']             = $getContactInfo['email1'];
        $data['zipcode']           = $getContactInfo['zipcode'];
        $data['city']              = $getContactInfo['city'];
        $data['state']             = $getContactInfo['state'];
        $data['phone_number']      = $getContactInfo['phone_number'];
        $data['tenant_image']      = $getContactInfo['tenant_image'];
        $data['emergencyInfo']     = $this->getEmergencyInfo($tenant_id);
        $data['viewEmergencyInfo'] = $this->getViewEmergencyInfo($tenant_id);
        return $data;
    }

    public function getContactInfo($tenant_id){
        return $this->companyConnection->query("SELECT u.*,td.tenant_contact,td.tenant_image,td.email1,td.email2,td.email3,td.vehicle,td.pet,td.animal,td.medical_allergy,td.guarantor,td.collection,td.smoker,td.movein_key_signed,td.status,td.tenant_license_state,td.tenant_license_number FROM users as u inner join tenant_details as td on u.id=td.user_id  WHERE u.id ='" . $tenant_id . "'")->fetch();

    }

    public function getPropertyInfo($tenant_id){
        $getPropertyInfo = $this->companyConnection->query("SELECT tp.property_id,tp.building_id,tp.unit_id,gp.property_name,bd.building_name,ud.unit_no from tenant_property as tp inner join general_property as gp on gp.id=tp.property_id inner join building_detail as bd on bd.id=tp.building_id inner join unit_details as ud on ud.id=tp.unit_id where tp.user_id = '" . $tenant_id . "'")->fetch();
        return $getPropertyInfo;

    }


    public function getEmergencyInfo()
    {
        $tenant_id = $_POST['id'];
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $tenant_id . "' and parent_id=0")->fetchAll();
        $countryCode      = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html             = "";
        if (empty($getEmergencyInfo)) {
            $html .= '<form id="editEmergency"><div class="row tenant-emergency-contact">
                                                    <div class="form-outer">

                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Emergency Contact Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="emergency" name="emergency_contact_name[]" maxlength="18">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Relationship</label>
                                                            <select class="form-control" id="relationship"
                                                                    name="emergency_relation[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Daughter</option>
                                                                <option value="2">Father</option>
                                                                <option value="3">Friend</option>
                                                                <option value="4">Mother</option>
                                                                <option value="5">Owner</option>
                                                                <option value="6">Partner</option>
                                                                <option value="7">Son</option>
                                                                <option value="8">Spouse</option>
                                                                <option value="9">Other</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 countycodediv">
                                                            <label>Country Code</label>
                                                            <select class="form-control" name="emergency_country[]">
                                                                <option value="">Select</option>
                                                                <option value="1">test</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">

                                                            <label>Phone</label>
                                                            <input class="form-control capsOn phone_format" type="text"
                                                                   id="phoneNumber"
                                                                   name="emergency_phone[]">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="clearfix"></div>

                                                    </div>

                                               
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Email</label>
                                                            <input class="form-control capsOn" type="text" 
                                                                   name="emergency_email[]" maxlength="100">
                                                                      <a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                                     <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    

                                                </div><div class="btn-outer text-right"><input type="button" id="updateEmergencyButton" class="blue-btn" value="Update"><button type="button"  class="clear-btn" id="resetClearEmergencyInfo">Reset</button><input type="button" id="emergencyCancel" value="Cancel" class="grey-btn"></div></form>';


        } else {
            $i = 0;
            foreach ($getEmergencyInfo as $emergencyInfo) {
                $html .= '<form id="editEmergency"><div class="row tenant-emergency-contact">
                                                    <div class="col-sm-2">
                                                        <label>Emergency Contact Name</label>
                                              <input class="form-control" type="text" value="' . $emergencyInfo['emergency_contact_name'] . '" name="emergency_contact_name[]" maxlength="18">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Relationship</label>
                                                        <select class="form-control" name="emergency_relation[]">';
                $relationArr = array(
                    "" => "Select",
                    "1" => "Daughter",
                    "2" => "Father",
                    "3" => "Friend",
                    "4" => "Mother",
                    "5" => "Owner",
                    "6" => "Partner",
                    "7" => "Son",
                    "8" => "Spouse",
                    "9" => "Other"
                );
                foreach ($relationArr as $key => $value) {
                    if ($key == $emergencyInfo['emergency_relation']) {
                        $html .= '<option value="' . $key . '" selected>' . $value . '</option>';
                    } else {
                        $html .= '<option value="' . $key . '">' . $value . '</option>';
                    }
                }

                $html .= '</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="emergency_country[]">';
                foreach ($countryCode as $code) {
                    if ($code['id'] == $emergencyInfo['emergency_country_code']) {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "' selected>" . $code['name'] . "</option>";
                    } else {
                        $html .= "<option value=" . $code['id'] . " data-id='" . $code['code'] . "'>" . $code['name'] . "</option>";
                    }
                }
                $html .= '</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Phone Number</label>
                                            <input class="form-control phone_format" type="text" maxlength="12" name="emergency_phone[]" value="' . $emergencyInfo['emergency_phone_number'] . '">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Email</label>
                                                        <input class="form-control add-input" type="text" value="' . $emergencyInfo['emergency_email'] . '" name="emergency_email[]" maxlength="100">';

                if ($i != 0) {


                    $html .= '<a class="add-icon remove-emergency-contant"><i
                                                                class="fa fa-times-circle red-star" aria-hidden="true "></i></a>';

                } else {
                    $html .= '<a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>';

                    $html .= '<a class="add-icon remove-emergency-contant" style="display:none"><i
                                                                class="fa fa-times-circle red-star" aria-hidden="true "></i></a>';

                }


                $html .= '</div></div></div>';

                $i++;
            }
            $html .= '<div class="btn-outer text-right"><input type="button" id="updateEmergencyButton" class="blue-btn" value="Update"><button type="button"  class="clear-btn" id="resetClearEmergencyInfo">Reset</button><input type="button" id="emergencyCancel" value="Cancel" class="grey-btn">
            </div></form>';
        }

        return array('code' => 200, 'status' => 'success', 'data' => $html, 'message' => 'Emergency Successfully.');

    }





    public function getViewEmergencyInfo($tenant_id)
    {
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $tenant_id . "' and parent_id=0")->fetchAll();
        $countryCode      = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html             = "";



        if (empty($getEmergencyInfo)) {

            $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                    <span></span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Email :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                    <span></span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';

        } else {

            foreach ($getEmergencyInfo as $emergencyInfo) {

                $countryCode    = $emergencyInfo['emergency_country_code'];
                $getCountryCode = $this->companyConnection->query("SELECT * FROM countries WHERE id ='" . $countryCode . "'")->fetch();
                $code           = $getCountryCode['code'];

                $relation = $emergencyInfo['emergency_relation'];
                if ($relation == 1) {
                    $emergencyRelation = 'Daughter';
                } else if ($relation == 2) {
                    $emergencyRelation = 'Father';
                } else if ($relation == 3) {
                    $emergencyRelation = 'Friend';
                } else if ($relation == 4) {
                    $emergencyRelation = 'Mother';
                } else if ($relation == 5) {
                    $emergencyRelation = 'Owner';
                } else if ($relation == 6) {
                    $emergencyRelation = 'Partner';
                } else if ($relation == 7) {
                    $emergencyRelation = 'Son';
                } else if ($relation == 8) {
                    $emergencyRelation = 'Spouce';
                } else {
                    $emergencyRelation = 'Other';
                }








                $getEmergencyRelation = $this->companyConnection->query("SELECT * FROM countries WHERE id ='" . $countryCode . "'")->fetch();

                $html .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span>' . $emergencyInfo['emergency_contact_name'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span>' . $code . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                     <span>' . $emergencyInfo['emergency_phone_number'] . '</span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Emaile :</label>
                                                                   <span>' . $emergencyInfo['emergency_email'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                     <span>' . $emergencyRelation . '</span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';
            }


        }

        return $html;

    }



    public function getLeaseInfo($tenant_id)
    {
        $getLeaseInfo = $this->companyConnection->query("SELECT tld.move_in,tld.move_out,tld.start_date,tld.end_date,tld.notice_period,tld.rent_amount,tld.security_deposite from tenant_lease_details as tld where tld.user_id = '" . $tenant_id . "'")->fetch();
        return $getLeaseInfo;


    }

    public function editEmergency()
    {
        $tenant_id = $_POST['tenant_id'];
        $count     = $this->companyConnection->prepare("DELETE FROM emergency_details WHERE  parent_id=0 and user_id=$tenant_id");
        $count->execute();
        try {



            $i = 0;
            foreach ($_POST['emergency_contact_name'] as $credentialName) {

                $data['user_id']                = $tenant_id;
                $data['parent_id']              = 0;
                $data['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $data['emergency_relation']     = $_POST['emergency_relation'][$i];
                $data['emergency_country_code'] = $_POST['emergency_country'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $data['emergency_email']        = $_POST['emergency_email'][$i];


                $sqlData = createSqlColVal($data);
                $query   = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt    = $this->companyConnection->prepare($query);
                $stmt->execute($data);

                $i++;

            }

            return array(
                'status' => 'success',
                'message' => 'Record updated successfully.',
                'function' => 'editEmergency'
            );
        }
        catch (PDOException $e) {
            return array(
                'code' => 400,
                'status' => 'failed',
                'message' => $e->getMessage(),
                'function' => 'editEmergency'
            );
            printErrorLog($e->getMessage());
        }


    }



    public function editVendorImage()
    {
        $vendor_id = $_POST['vendor_id'];
        $data = [];
        $data['image'] = $_POST['vendor_image'];
        $sqlData = createSqlUpdateCase($data);
        $query = "UPDATE vendor_additional_detail SET " . $sqlData['columnsValuesPair'] . " WHERE vendor_id=" . $vendor_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($sqlData['data']);
        return array("message"=>"Image Updated Successfully","status"=>"success","function"=>$_POST['vendor_image']);

    }


    public function changePassword()
    {
        $tenant_id = $_POST['tenant_id'];
        $getUserPassword = $this->getUserPassword($tenant_id);
        $current_password = md5($_POST['current_password']);
        $new_password = md5($_POST['new_password']);
        $confirm_password = md5($_POST['confirm_password']);
        $actualPasseword = $_POST['confirm_password'];


        if($current_password!=$getUserPassword)
        {
            return array('message'=>'Current password is not matching with User Password','status'=>'error');
        }


        if($new_password!=$confirm_password)
        {
            return array('message'=>'New Password and Confirm Password are not same','status'=>'error');
        }


        $query = "UPDATE users SET password='$new_password',actual_password='$actualPasseword' where id=".$tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array("message"=>"Password is updated successfully","status"=>"success","function"=>'editTenantImage');

    }



    public function getUserPassword($id)
    {
        $getPassword = $this->companyConnection->query("SELECT password FROM users WHERE id ='".$id."'")->fetch();
        return $getPassword['password'];



    }



    public function checkSceretKey()
    {
        $tenant_id = $_POST['id'];
        $secretKey = $_POST['secretKey'];


        $getKey =  $this->companyConnection->query("SELECT reset_password_token FROM `users` where id =  $tenant_id and reset_password_token='$secretKey'")->fetch();



        if(empty($getKey))
        {
            return array('status'=>'error','message'=>'Link has been expire');
        }
        else
        {
            return array('status'=>'success','message'=>'Please reset your password');
        }


    }



    public function resetPassword()
    {
        $tenant_id = $_POST['tenant_id'];
        $new_password = md5($_POST['new_password']);
        $confirm_password = md5($_POST['confirm_password']);
        $actualPasseword = $_POST['confirm_password'];

        if($new_password!=$confirm_password)
        {
            return array('message'=>'New Password and Confirm Password are not same','status'=>'error');
        }


        $query = "UPDATE users SET password='$new_password',actual_password='$actualPasseword' where id=".$tenant_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return array("message"=>"Password is updated successfully","status"=>"success","function"=>'editTenantImage');

    }


    public function portalLogin()
    {
        $email = $_POST['email'];
        $password = md5($_POST['password']);

        $checkLogin =    $this->companyConnection->query("SELECT id FROM `users` where email = '$email' and password='$password' and user_type='2'")->fetch();
        if(!empty($checkLogin))
        {
            // session_start();
            $id = $checkLogin['id'];

            $_SESSION['tenant_id'] = $id;

            return array('status'=>'success','message'=>'Successfully Logged in','tenant_id'=>$id);


        }
        else
        {
            return array('status'=>'error','message'=>'Email/Password is incorrect');
        }

    }



    public function logout()
    {
        $tenant_id = $_POST['id'];
        unset($_SESSION["tenant_id"]);
        return array('status'=>'success','message'=>'successfully logout');

    }


    public function sendMail()
    {
        $toUsers = $_POST['to_users'];
        $userEmails = explode(",",$toUsers);
        $cc = $_POST['cc'];
        $bcc = $_POST['bcc'];
        $subject = $_POST['subject'];
        $path = $_POST['path'];

        $data= $_POST;
        $body =$data['body'];
        $domain = explode('.', $_SERVER['HTTP_HOST']);
        $subdomain = array_shift($domain);
        $user_detail = $_SESSION['SESSION_'.$subdomain];
        try{

            $request['action']  = 'SendMailPhp';
            $request['to']    = $userEmails;
            $request['subject'] = $subject;
            $request['message'] = $body;
            $request['attachment'] = $path;
            $request['portal']  = '1';
            curlRequest($request);

            return ['status'=>'success','code'=>200,'data'=>$request];
        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }







    }



    public function getUsers()
    {
        $type = $_POST['type'];
        if($type=="tenant")
        {
            $user_type = 2;
        }
        else if($type=="owner")
        {
            $user_type = 4;
        }
        else if($type=="vendor")
        {
            $user_type = 3;
        }

        else if($type=="employee")
        {
            $user_type = 8;
        }

        else if($type=="guestCard")
        {
            $user_type = 6;
        }

        /* echo $user_type;
         exit;*/


        $getUsers = $this->companyConnection->query("SELECT id,first_name,email FROM users where user_type='$user_type'")->fetchAll();

        $html = '';
        $html .= "<table border= '1px'>";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['first_name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='getEmails' data-email='".$email."'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;
    }




    public function getCcUsers()
    {
        $type = $_POST['type'];
        if($type=="tenant")
        {
            $user_type = 2;
        }
        else if($type=="owner")
        {
            $user_type = 4;
        }
        else if($type=="vendor")
        {
            $user_type = 3;
        }

        else if($type=="employee")
        {
            $user_type = 8;
        }

        else if($type=="guestCard")
        {
            $user_type = 6;
        }

        /* echo $user_type;
         exit;*/


        $getUsers = $this->companyConnection->query("SELECT id,first_name,email FROM users where user_type='$user_type'")->fetchAll();

        $html = '';
        $html .= "<table border= '1px'>";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['first_name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='getCcEmails' data-email='".$email."'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;

    }





    public function getBccUsers()
    {
        $type = $_POST['type'];
        if($type=="tenant")
        {
            $user_type = 2;
        }
        else if($type=="owner")
        {
            $user_type = 4;
        }
        else if($type=="vendor")
        {
            $user_type = 3;
        }

        else if($type=="employee")
        {
            $user_type = 8;
        }

        else if($type=="guestCard")
        {
            $user_type = 6;
        }

        /* echo $user_type;
         exit;*/


        $getUsers = $this->companyConnection->query("SELECT id,first_name,email FROM users where user_type='$user_type'")->fetchAll();

        $html = '';
        $html .= "<table border= '1px'>";
        $html .= "<tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        </tr>";

        foreach ($getUsers as $users) {

            $id = $users['id'];
            $name = $users['first_name'];
            $email = $users['email'];
            $html .= "<tr>";
            $html .= "<td><input type='checkbox' class='getBccEmails' data-email='".$email."'></td>";
            $html .= "<td>" . $name . "</td>";
            $html .= "<td>" . $email . "</td>";
            $html .= "</tr>";

        }

        $html .= "</table><br>";

        echo $html;
        exit;


    }

    public function getMaintenanceInfo()
    {
        return "code needs to be write";
    }

    public function viewEmergencyData(){
        $id = $_POST['id'];
        $emergency = $this->getEmergencyDetails($id);
        return array('code' => 200, 'status' => 'success', 'emergency' =>$emergency , 'message' => 'Record retrieved Successfully.');
    }

    public function updateEmergencyContant()
    {
        $vendor_id = $_POST['vendor_id'];
        $count=$this->companyConnection->prepare("DELETE FROM emergency_details WHERE  parent_id=0 and user_id=$vendor_id");
        $count->execute();
        try{
            $i=0;
            foreach($_POST['emergency_contact_name'] as $credentialName)
            {
                $data['user_id'] = $vendor_id;
                $data['parent_id'] = 0;
                $data['emergency_contact_name'] = $_POST['emergency_contact_name'][$i];
                $data['emergency_relation'] = $_POST['emergency_relation'][$i];
                $data['emergency_country_code'] = $_POST['emergency_country'][$i];
                $data['emergency_phone_number'] = $_POST['emergency_phone'][$i];
                $data['emergency_email'] = $_POST['emergency_email'][$i];

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO emergency_details(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $i++;
            }
            return array('status' => 'success',  'message' => 'Record updated successfully.','table'=>'emergency_details');
        }catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage(),'table'=>'emergency_details');
            printErrorLog($e->getMessage());
        }

    }


    /**
     * Update admin password!
     * Use this endpoint to update the user password.
     * @author Sarita
     * @return array
     */
    public function changePasswordVendor(){
        try {
            $data=[];
            $id = $_POST['id'];
//            print_r($_POST);
            $current_password = $_POST['current_password'];

            $query = $this->companyConnection->query("SELECT actual_password FROM users WHERE id='$id'");
            $user = $query->fetch();
            if ($user['actual_password'] != $current_password) {
                return array('code' => 503, 'status' => 'error', 'data' => $user['actual_password'], 'message' => 'Invaild current password.');
            }else {
                $data['id'] = $_POST['id'];
                $data['npassword'] = $_POST['nPassword'];
                $data['cpassword'] = $_POST['cPassword'];


                //Required variable array
                $required_array = ['npassword', 'cpassword'];
                /* Max length variable array */
                $maxlength_array = ['npassword' => 15, 'cpassword' => 25];

                //Number variable array
                $number_array = [];
                //Server side validation
                $err_array = [];
                $err_array = $this->validationPassword($data, $this->conn, $required_array, $maxlength_array, $number_array);
                //Checking server side validation
                if (checkValidationArray($err_array)) {
                    return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
                } else {

                    $record['password'] = md5($data['npassword']);
                    $record['actual_password'] = $data['npassword'];
                    $id = $data['id'];
                    $sqlData = createSqlColValPair($record);
                    $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                    return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password changed successfully');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Server side Validations
     * @author Deepak
     * @param $data
     * @param $db
     * @param array $required_array
     * @param array $maxlength_array
     * @param array $number_array
     * @return array
     */
    function validationPassword($data,$db,$required_array=[],$maxlength_array=[],$number_array=[]){
        $err_array = array();

        if(($data['npassword'] !== $data['cpassword'])){
            $error = 'Enter same password';
            $errName = 'NewPasswordErr';
            $err_array[$errName][] = $error;
        }

        foreach ($data as $key => $value) {
            $errName = $key . 'Err';
            $err_array[$errName] = [];
            if(in_array($key,$required_array)){
                if (empty($value)) {
                    $colName = ucfirst(str_replace('_',' ',$key));
                    $error = $colName." is required";
                    array_push($err_array[$errName],$error);
                }
            }

            if(array_key_exists($key,$maxlength_array)){
                $length = strlen($value);
                if ($key != 'npassword') {
                    continue;
                }
                if (!empty($value) && $length < 8) {
                    $error = 'Please enter minimum 8 characters';
                    array_push($err_array[$errName],$error);
                }
            }
            if(empty($err_array[$errName])){
                unset($err_array[$errName]);
            }
        }
        return $err_array;
    }

    public function sendEmailTemplate()
    {
        try{
            $userId = $_POST['tenant_id'];
            $resetPasswordToken =  rand();
            $query = "UPDATE users SET reset_password_token='$resetPasswordToken' where id=".$userId;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            $server_name = 'https://'.$_SERVER['HTTP_HOST'];
            $userId = (isset($userId) ? $userId : $_REQUEST['id']);
            $user_details = $this->companyConnection->query("SELECT * FROM users where id=".$userId)->fetch();
            if(!empty($user_details)){
                if($user_details['portal_status'] == '1' || $user_details['portal_status'] == '2') {
                    return ['status' => 'success', 'code' => 200, 'message' => 'Vendor portal already genrated!'];
                }
            }
            $user_name = $user_details['first_name'].' '.$user_details['last_name'];
            $user_name = userName($user_details['id'], $this->companyConnection);
            if(isset($_POST['temp_key']) && $_POST['temp_key'] == "newVendorWelcome_Key")
            {
                $body = getEmailTemplateData($this->companyConnection, "newVendorWelcome_Key");
            }else{
                $body = getEmailTemplateData($this->companyConnection, "newTenantWelcome_Key");
            }
            $body = str_replace("{Username}",$user_name,$body);
            $body = str_replace("{Firstname}",$user_name,$body);
            $body = str_replace("{email}",$user_details['email'],$body);
            $body = str_replace("{password}",$user_details['actual_password'],$body);
            $imgtag = "<a href='#'><img src='".SITE_URL.'/company/images/logo.png' ."' style='height:50px;width:50px;'></a>";
            $body = str_replace("{CompanyLogo}",$imgtag,$body);
            $urlHtml = SUBDOMAIN_URL  ."/VendorPortal/ChangePassword?vendor_id=$userId&secretkey=$resetPasswordToken";

//            dd($urlHtml);
            $body = str_replace("{Url}",$urlHtml,$body);

            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $user_details['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
//            dd($request['message']);
            $request['portal']  = '1';
            curlRequest($request);

            return ['status'=>'success','code'=>200,'data'=>$request,'message' => 'Email send successfully'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }

    }

    public function getVendorPortal(){
        try{
            $vendor_id = $_POST['vendor_id'];
            $checkLogin =    $this->companyConnection->query("SELECT * FROM `users` WHERE user_type='3' AND status='1' AND id=".$vendor_id )->fetch();
            if(!empty($checkLogin)) {
                $user_id = $checkLogin['id'];
                $url = '/VendorPortal/Vendor/MyAccount';
                $portal = 'Vendor_Portal';
                $_SESSION[SESSION_DOMAIN][$portal]['vendor_portal_id'] = $user_id;
                $_SESSION[SESSION_DOMAIN][$portal]['portal_id'] = $user_id;
                $_SESSION[SESSION_DOMAIN][$portal]['default_name'] = $checkLogin['first_name'].' '.$checkLogin['last_name'];
                $_SESSION[SESSION_DOMAIN][$portal]['name'] = userName($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['company_name'] = $checkLogin['company_name'];
                $_SESSION[SESSION_DOMAIN][$portal]['email'] = $checkLogin['email'];
                $_SESSION[SESSION_DOMAIN][$portal]['phone_number'] = $checkLogin['phone_number'];
                $_SESSION[SESSION_DOMAIN][$portal]['formated_date'] = dateFormatUser(date('Y-m-d H:i:s'), $checkLogin['id'], $this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['datepicker_format'] = getDatePickerFormat($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['company_logo'] = getCompanyLogo($this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['super_admin_name'] = isset($_POST['super_admin_name']) ? $_POST['super_admin_name'] : '';
                $_SESSION[SESSION_DOMAIN][$portal]['admin_url'] = $_SERVER['HTTP_REFERER'];
                //Default setting session data
                $_SESSION[SESSION_DOMAIN][$portal]['default_notice_period'] = defaultNoticePeriod($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_rent'] = defaultRent($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_application_fee'] = defaultApplicationfee($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_zipcode'] = defaultZipCode($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_city'] = defaultCity($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_currency'] = defaultCurrency($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['default_currency_symbol'] = defaultCurrencySymbol($_SESSION[SESSION_DOMAIN][$portal]['default_currency'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['pagination'] = pagination($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['payment_method'] = defaultPaymentMethod($checkLogin['id'],$this->companyConnection);
                $_SESSION[SESSION_DOMAIN][$portal]['property_size'] = defaultPropertySize($checkLogin['id'],$this->companyConnection);
                return array('status'=>'success','code'=>200,'portal_url'=>$url,'user_id'=>$user_id);
            } else {
                return array('status'=>'error','message'=>'Error occured while redirecting!');
            }
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

}



$VendorPortal = new VendorPortal();