<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class changepassword extends DBConnection {

    /**
     * UserAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    public function sendMail($userid = null)
    {

        try{
            $userid = (isset($userid) ? $userid : $_REQUEST['id']);
            $user_details = $this->conn->query("SELECT * FROM users where id=".$userid)->fetch();
            $body = file_get_contents(SUPERADMIN_DIRECTORY_URL.'/views/Emails/welcomesuperadmin.php');
            $body = str_replace("#name#",$user_details['first_name'],$body);
            $body = str_replace("#email#",$user_details['email'],$body);
            $body = str_replace("#password#",$user_details['actual_password'],$body);
            $body = str_replace("#logo#",SITE_URL.'/company/images/logo.png',$body);
            $request['action']  = 'SendMailPhp';
            $request['to[]']    = $user_details['email'];
            $request['subject'] = 'Welcome in Apexlink!';
            $request['message'] = $body;
            $request['portal']  = '1';
            $request['attachments[]'] = SITE_URL.'uploads/email_attachments/WelcomeMailAttachment.docx';
            /*       echo '<pre>';
                   print_r($request);
                   echo '</pre>';*/
            curlRequest($request);

            return ['status'=>'success','code'=>200,'data'=>$request];

        }catch (Exception $exception)
        {
            return ['status'=>'failed','code'=>503,'data'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }

    /**
     * Update user password!
     * Use this endpoint to update the user password.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function changePassword(){
        try {
            $data=[];

            if ($_POST['id'] == "") {
                $data['id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            }else{
                $data['id'] = $_POST['id'];
            }
            $data['npassword'] = $_POST['nPassword'];
            $data['cpassword'] = $_POST['cPassword'];
            $data['current'] = $_POST['currentPass'];
            $check = $this->checkCurrentPassword($data['current']);
            if($check['code'] == 400){
                return $check;
            }
            //Required variable array
            $required_array = ['npassword','cpassword'];
            /* Max length variable array */
            $maxlength_array = ['npassword'=>50,'cpassword'=>50];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = $this->validationPassword($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $record['password'] = md5($data['npassword']);
                $record['actual_password'] = $data['npassword'];
                $id = $data['id'];
                $sqlData = createSqlColValPair($record);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

                $query = $this->companyConnection->query("SELECT admindb_id FROM users WHERE id='$id'");
                $user = $query->fetch();
                if ($user['admindb_id']) {
                    $super_admin_id = $user['admindb_id'];
                    $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id='$super_admin_id'";
                    $stmt = $this->conn->prepare($query);
                    $stmt->execute();
                }

                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password has been updated!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    /**
     * validate user current password!
     * Use this endpoint to validate the user current password.
     * @author Parvesh
     * @param int $length
     * @success Success
     * @error (Exceptions) Validation of Model failed. See status.returnValues
     * @return string
     */
    public function checkCurrentPassword($pass){
        try {
            $data=[];
            $data['id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['password'] = $pass;

            if ($data['password'] == "") {
                return array('code' => 400, 'status' => 'error', 'data' => '', 'message' => 'Validation Errors!');
            } else {
                $record['password'] = md5($data['password']);
                $id = $data['id'];

                $query = $this->companyConnection->query("SELECT password FROM users WHERE id='$id'");
                $user = $query->fetch();
                if ($user['password'] === $record['password']) {
                    return array('code' => 200, 'status' => 'success', 'data' => $user['password'], 'message' => 'Password matched!');
                }else{
                    return array('code' => 400, 'status' => 'error', 'message' => 'Current password do not match.');
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }



    /**
     * Server side Validations
     * @author Deepak
     * @param $data
     * @param $db
     * @param array $required_array
     * @param array $maxlength_array
     * @param array $number_array
     * @return array
     */
    function validationPassword($data,$db,$required_array=[],$maxlength_array=[],$number_array=[]){
        $err_array = array();

        if(($data['npassword'] !== $data['cpassword'])){
            $error = 'Enter same password';
            $errName = 'NewPasswordErr';
            $err_array[$errName][] = $error;
        }

        foreach ($data as $key => $value) {
            $errName = $key . 'Err';
            $err_array[$errName] = [];
            if(in_array($key,$required_array)){
                if (empty($value)) {
                    $colName = ucfirst(str_replace('_',' ',$key));
                    $error = $colName." is required";
                    array_push($err_array[$errName],$error);
                }
            }

            if(array_key_exists($key,$maxlength_array)){
                $length = strlen($value);
                if ($key != 'npassword') {
                    continue;
                }
                if (!empty($value) && $length < 8) {
                    $error = 'Please enter minimum 8 characters';
                    array_push($err_array[$errName],$error);
                }
            }
            if(empty($err_array[$errName])){
                unset($err_array[$errName]);
            }
        }
        return $err_array;
    }

}

$user = new changepassword();
