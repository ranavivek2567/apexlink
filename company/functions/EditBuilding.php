<?php
include(ROOT_URL."/config.php");
include_once( ROOT_URL."/company/functions/FlashMessage.php");
include_once( ROOT_URL."/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class EditBuilding extends DBConnection {

    /**
     * CustomFields constructor.
     */
    public function __construct() {
        parent::__construct();
        $function = FlashMessage::render();
        $this->$function();
    }

    /**
     * function to add custom field.
     * @return array|void
     */
    public function edit(){
        try {
            $renovation_detail_html="";
            $school_district_html ="";
            $building_images_slider="";
            $data = [];
            $images_data =[];
            $renovation_detail_data =[];
            $custom_data=[];
            $amenities =[];
            //$building = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$_SESSION[SESSION_DOMAIN]['cuser_id']], 'building_detail');

            $id= $_GET["id"];
            $sql = "SELECT b.*,b.pet_friendly as building_pet_friendly,b.updated_at
                    ,c.pet_friendly as pet_friendly
                    ,g.property_id as property_id, g.property_name as property_name
                    ,g.school_district_municipality as property_school_district_municipality
                    ,g.school_district_code as property_school_district_code
                    ,g.school_district_notes as property_school_district_notes
                    ,g.smoking_allowed as property_smoking_allowed
                    ,g.pet_friendly as property_pet_friendly
                    ,b.address as building_address
                    ,r.last_renovation_date as building_last_renovation_date
                    ,r.last_renovation_time as building_last_renovation_time
                    ,r.last_renovation_description as building_last_renovation_description
                    FROM building_detail as b
                    LEFT JOIN renovation_details as r ON b.id=r.object_id AND r.object_type='building'  
                    LEFT JOIN general_property as g ON b.property_id=g.id
                    LEFT JOIN company_pet_friendly as c ON b.pet_friendly=c.id
                    WHERE b.id='".$id. "'ORDER BY  r.created_at DESC";
            $stmt = $this->companyConnection->prepare($sql); // Prepare the statement
            $data = [];
            $images_data =[];
            $renovation_detail_data =[];
            $custom_data=[];
            $phone_number=[];
            $fax_number=[];
            if($stmt->execute()) {
                $data = $stmt->fetch();

                $stmt->closeCursor();
                if(!empty($data)){
                    if(!empty($data["custom_field"])){
                        $custom_data = unserialize($data["custom_field"]);
                    }
                    if(isset($data["phone_number"]) && !empty($data["phone_number"])) {
                        $phone_number = unserialize($data["phone_number"]);
                        $data["phone_number"]= $phone_number;
                    }
                    if(isset($data["fax_number"]) && !empty($data["fax_number"])) {
                        $fax_number = unserialize($data["fax_number"]);
                        $data["fax_number"]= $fax_number;
                    }

                    if(!empty($data["building_last_renovation_date"])){
                        $data["building_last_renovation_date"] =   dateFormatUser($data["building_last_renovation_date"],null,$this->companyConnection);
                    }

                    if(!empty($data["building_last_renovation_time"])){
                        $data["building_last_renovation_time"] =   timeFormat($data["building_last_renovation_time"],'',$this->companyConnection);
                    }

                    if(isset($data["amenities"]) && !empty($data["amenities"])) {
                        $amenities = unserialize($data["amenities"]);
                    }
                    $sql1 = "SELECT * FROM building_file_uploads WHERE file_type=1 AND building_id=".$id ;
                    $stmt1 = $this->companyConnection->prepare($sql1); // Prepare the statement
                    if($stmt1->execute())
                    {
                        $images_data =$stmt1->fetchAll();
                    }
                    $school_district_muncipiality=[];
                    $school_district_code=[];
                    $school_district_notes=[];
                    $result=[];
                    if(isset($data["property_school_district_municipality"]) && !empty($data["property_school_district_municipality"]))
                        $school_district_muncipiality = unserialize($data["property_school_district_municipality"]);
                    if(isset($data["property_school_district_code"]) && !empty($data["property_school_district_code"]))
                        $school_district_code = unserialize($data["property_school_district_code"]);
                    if(isset($data["property_school_district_notes"]) && !empty($data["property_school_district_notes"]))
                        $school_district_notes = unserialize($data["property_school_district_notes"]);
                    $result = array_map(function ($name, $type, $price) {
                        return array_combine(
                            ['school_district', 'code', 'notes'],
                            [$name, $type, $price]
                        );
                    }, $school_district_muncipiality, $school_district_code, $school_district_notes);
                    /*query to fetch renovation details */

                    $sql2 = "SELECT * FROM renovation_details WHERE object_type='building' AND object_id=".$id ;
                    $stmt2 = $this->companyConnection->prepare($sql2); // Prepare the statement
                    if($stmt2->execute())
                    {
                        $renovation_detail_data =$stmt2->fetchAll();
                    }
                    foreach($renovation_detail_data as $key=>$value) {
                        if(!empty($value['last_renovation_date'])){

                            $renovation_date = dateFormatUser($value['last_renovation_date'],null,$this->companyConnection);
                        }else{
                            $renovation_date ="";
                        }

                        if(!empty($value['last_renovation_time'])){
                            $renovation_time = timeFormat($value['last_renovation_time'],NULL,$this->companyConnection);
                        }else{
                            $renovation_time ="";
                        }
                        $renovation_detail_html .= "<tr>";
                        $renovation_detail_html .= "<td>".$renovation_date."</td>";
                        $renovation_detail_html .= "<td>".$renovation_time."</td>";
                        $renovation_detail_html .= "<td>".$value['last_renovation_description']."</td>";
                        $renovation_detail_html .= "</tr>";
                    }
                    $current_date = dateFormatUser(date('Y-m-d'),null,$this->companyConnection);
                    $redirect = VIEW_COMPANY."company/building/edit.php";
                    return require $redirect;
                  //  return array('status' => 'success', 'code' => 200, 'data' => $data,'renovation_detail'=>$renovation_detail_html,'school_district_muncipiality_data'=>$school_district_html,'images_data'=>$images_data,'images_html'=>$building_images_slider,'custom_data'=>$custom_data, 'message' => 'Record fetched successfully.');
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }

    public function update(){
        try {

            $data = $_REQUEST['data'];
            $data = postArray(serializeToPostArray($data));
            //Required variable array
            $required_array = ['company_name','first_name','last_name','website','zipcode','state','address1','city','country'];
            //Max length variable array
            $maxlength_array = ['company_name'=>50,'first_name'=>20,'last_name'=>100,'zipcode'=>20,'address1'=>250,'city'=>50,'country'=>20,'state'=>50];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            $files = $_FILES;
            if(empty($files)){
                if(!isset($files['company_logo']) && !isset($data['previous_logo']))array_push($err_array,['company_logoErr'=>['This field is required']]);
                if(!isset($files['signature']) && !isset($data['previous_signature']))array_push($err_array,['signatureErr'=>['This field is required']]);
            }
            if (checkValidationArray($err_array)) {
                echo array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
                return;
            } else {
                $user = getSingleRecord($this->companyConnection ,['column'=>'id','value'=>$_SESSION[SESSION_DOMAIN]['cuser_id']], 'users');
                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn ,['column'=>'domain_name','value'=>$domain], 'users');
                $company_id = CompanyId($this->companyConnection);
                $adminData =[];
                //file uploading
                if(isset($data['previous_logo'])) $data['company_logo'] = $data['previous_logo'];
                if(isset($data['previous_signature'])) $data['signature'] = $data['previous_signature'];
                foreach ($files as $key=>$value) {
                    $file_name = $value['name'];
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name= time().uniqid(rand());
                    $path = "uploads/".'apexlink_company_'.$adminUser['data']['id'].'/'.$_SESSION[SESSION_DOMAIN]['cuser_id'];
//                    print_r($path);die;
                    //Check if the directory already exists.
                    if(!is_dir(ROOT_URL.'/company/'.$path)){
                        //Directory does not exist, so lets create it.
                        mkdir(ROOT_URL.'/company/'.$path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL.'/company/'.$path.'/'.$name.'.'.$ext);
                    $data[$key] = $path.'/'.$name.'.'.$ext;
                }
                //unset data
                unset($data['previous_logo']);
                unset($data['previous_signature']);
                //declaring additional variables
                $data['name'] = $data['first_name'].' '.$data['last_name'];
                $adminData['name'] = $data['first_name'].' '.$data['last_name'];
                $adminData['first_name'] = $data['first_name'];
                $adminData['company_name'] = $data['company_name'];
                $adminData['middle_name'] = $data['middle_name'];
                $adminData['last_name'] = $data['last_name'];
                $adminData['maiden_name'] = $data['maiden_name'];
                $adminData['nick_name'] = $data['nick_name'];
                $adminData['website'] = $data['website'];
                $adminData['city'] = $data['city'];
                $adminData['state'] = $data['state'];
                $adminData['country'] = $data['country'];
                $adminData['address1'] = $data['address1'];
                $adminData['address2'] = $data['address2'];
                $adminData['address3'] = $data['address3'];
                $adminData['fax'] = $data['fax'];

                //updating user table
                $sqlData = createSqlColValPair($data);
                $company_id = $company_id['data'];



                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$company_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                //updating super admin user table
                $sqlData = createSqlColValPair($adminData);
                $query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where email='".$user['data']["email"]."'";
                $stmt = $this->conn->prepare($query);

                if($stmt->execute()) {
                    $defaultData['application_fees']= $data['application_fees'];
                    $defaultData['default_rent']= $data['rent_amount'];
                    $defaultData['zip_code']= $data['zipcode'];
                    $defaultData['country']= $data['country'];
                    $defaultData['state']= $data['state'];
                    $defaultData['city']= $data['city'];
                    $defaultData['currency']= $data['currency'];
                    $defaultData['payment_method']= $data['default_payment_method'];
                    $this->updateDefaultSettings($defaultData);
                    $_SESSION[SESSION_DOMAIN]["message"] = 'Record updated successfully.';
                    $_SESSION[SESSION_DOMAIN]['name'] = userName($company_id,$this->companyConnection);
                    $_SESSION[SESSION_DOMAIN]['company_logo'] = getCompanyLogo($this->companyConnection);
                    echo json_encode(array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully'));
                    return;
                } else {
                    echo json_encode(array('code' => 400, 'status' => 'error', 'message' => 'Internal Server Error!'));
                    return;
                }
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed','message' => $e->getMessage()));
            return;
        }
    }


}

$building = new EditBuilding();
