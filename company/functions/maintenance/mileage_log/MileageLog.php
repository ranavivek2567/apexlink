<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class mileagelog extends DBConnection {

    /**
     * UserAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * function to get post data array
     * @param $post
     * @return array
     */



    public function getmileagedata(){
        try{

            $query = $this->companyConnection->query("SELECT mielage_log. * FROM mielage_log ");
            $data = $query->fetch();

            return ['status'=>'success','code'=>200, 'data'=>$data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }


    public function getuserdata(){
        try{
            $query = $this->companyConnection->query("SELECT name, id, phone_number, email FROM users where user_type = '8' ");
            $data = $query->fetchAll();

            $query1 = $this->companyConnection->query("SELECT company_name FROM users where id = '1' ");
            $data1 = $query1->fetchAll();

            return ['status'=>'success','code'=>200, 'data'=>$data,'data1'=>$data1];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function getuserdata1(){
        try{
            $a=$_POST['a'];

            $query = $this->companyConnection->query("SELECT * FROM users WHERE name LIKE '$a%'");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=>$data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function getvehicledata(){
        try{

            $query = $this->companyConnection->query("SELECT company_manage_vehicle.vehicle_name FROM company_manage_vehicle ");
            $data = $query->fetchAll();


            return ['status'=>'success','code'=>200, 'data'=>$data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function getvehicledetails(){
        try{
            $data="";
            $id=$_POST['id'];
            if($id !=="" && $id !=='undefined') {
                $query = $this->companyConnection->query("SELECT company_manage_vehicle.* FROM company_manage_vehicle where user_id=" . $_POST['id']);
                $data = $query->fetchAll();
            }
        else{
                $data="";
            }
            return ['status'=>'success','code'=>200, 'data'=>$data];

        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function geteditdata(){
        try{
            $data_id=$_POST['id'];
            $query = $this->companyConnection->query("SELECT mielage_log. *, users.id as emp_id FROM mielage_log 
                                                             left join users on mielage_log.user_id=users.id
                                                             WHERE mielage_log.id=".$data_id);
            $data = $query->fetchAll();

            $data[0]['date']=dateFormatUser( $data[0]['date'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
          $data[0]['created_at']=dateFormatUser($data[0]['created_at'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);

            $query1 = $this->companyConnection->query("SELECT company_name FROM users where id = '1' ");
            $data1 = $query1->fetchAll();

            return ['status'=>'success','code'=>200, 'data'=>$data,'data1'=>$data1];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function addlog(){
        try{
            $formData2=$_POST['formData'];
            $formData2=postArray($formData2);
            $query1 = $this->companyConnection->query("SELECT users.name FROM users WHERE users.id=".$formData2['full_name']);
            $data2 = $query1->fetch();
            $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, "","","",'','', $formData2['vin']);
            if(isset($duplicate_record_check)){
                if($duplicate_record_check['code'] == 503){
                    echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                    die();
                }
            }
            $data['user_id'] = $formData2['emp_id'];
            $data['emp_name'] = $data2['name'];
            $data['vehicle_type'] = $formData2['vehicle_type'];
            $data['company_name'] = $formData2['comp_name'];
            $data['branch'] = $formData2['location'];
            $data['date'] = mySqlDateFormat($formData2['date'],$id='null',$this->companyConnection);
            (isset($formData2['date_purchases']) && !empty($formData2['date_purchases'])) ? $data['date_purchased'] = mySqlDateFormat($formData2['date_purchases'],$id='null',$this->companyConnection) : null;
            $data['vehicle_name'] = $formData2['vehicle_name'];
            $data['vehicle_id'] = $formData2['vehicle_id'];
            $data['vehicle_make'] = $formData2['vehicle_make'];
            $data['model'] = $formData2['model'];
            $data['vin_no'] = $formData2['vin'];
            $data['registration'] = $formData2['registration_no'];
            $data['licence_plate'] = $formData2['license_no'];
            $data['color'] = $formData2['color'];
            $data['year_of_vehicle'] = $formData2['year_vehicle'];
            $data['start_location'] = $formData2['start_loc'];
            $data['end_location'] = $formData2['end_loc'];
            $data['initial_reading'] = $formData2['start_reading'];
            $data['final_reading'] = $formData2['stop_reading'];
            $data['mileage'] = $formData2['mileage'];
            if(isset($formData2['amount']) && !empty($formData2['amount'])){
                $data['amount'] = $formData2['amount'];
            }
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO mielage_log (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")" ;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);
            $last_id1=$this->companyConnection->lastInsertId();
            //$query2 = $this->companyConnection->query("SELECT users.name FROM users WHERE users.id=".$formData2['full_name']);

            $data1['user_id'] = $formData2['full_name'];
            $data1['vehicle_type'] = $formData2['vehicle_type'];
            (isset($formData2['date_purchases']) && !empty($formData2['date_purchases'])) ? $data['date_purchased'] = mySqlDateFormat($formData2['date_purchases'],$id='null',$this->companyConnection) : null;
            $data1['vehicle_name'] = $formData2['vehicle_name'];
            $data1['vehicle_type'] = $formData2['vehicle_type'];;
            $data1['vehicle'] = $formData2['vehicle_id'];;
            $data1['make'] = $formData2['vehicle_make'];
            $data1['model'] = $formData2['model'];
            $data1['vin'] = $formData2['vin'];
            $data1['registration'] = $formData2['registration_no'];
            $data1['plate_number'] = $formData2['license_no'];
            $data1['color'] = $formData2['color'];
            $data1['year_of_vehicle'] = $formData2['year_vehicle'];

            $data1['starting_mileage'] = $formData2['mileage'];
            if(isset($formData2['amount']) && !empty($formData2['amount'])){
                $data1['amount'] = $formData2['amount'];
            }
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData1 = createSqlColVal($data1);
            $query3 = "INSERT INTO company_manage_vehicle (" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] .")" ;
            $stmt1 = $this->companyConnection->prepare($query3);
            $stmt1->execute($data1);
            $last_id=$this->companyConnection->lastInsertId();
            return ['status'=>'success','code'=>200, 'message' => 'Record added successfully.','last_id'=>$last_id,'last_id1'=>$last_id1];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }


    public function addlog1(){
        try{
            $formData2=$_POST['formData'];
            $formData2=postArray($formData2);
            $query1 = $this->companyConnection->query("SELECT users.name FROM users WHERE users.id=".$formData2['full_namee']);
            $data2 = $query1->fetch();
            $data['user_id'] = $formData2['emp_idd'];
            $data['emp_name'] = $data2['name'];
            $data['company_name'] = $formData2['comp_namee'];
            $data['branch'] = $formData2['locationn'];
            $data['date'] = mySqlDateFormat($formData2['date'],$id='null',$this->companyConnection);
            $data['vehicle_name'] = $formData2['select_vehicle'];
            $data['driver'] = $formData2['driver'];
            $data['start_location'] = $formData2['start_loca'];
            $data['end_location'] = $formData2['end_loca'];
            $data['initial_reading'] = $formData2['start_readingg'];
            $data['final_reading'] = $formData2['stop_readingg'];
            $data['mileage'] = $formData2['mileagee'];
            $data['notes'] = $formData2['note'];
            $data['type']='C';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($data);
            $query = "INSERT INTO mielage_log (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")" ;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($data);

            return ['status'=>'success','code'=>200, 'message' => 'Record added successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function edit_emplog(){
        try{
            $formData2=$_POST['formData'];
            $formData2=postArray($formData2);
           $data_id=$_POST['id'];
           $man_id=$_POST['man_id'];
            $query1 = $this->companyConnection->query("SELECT users.name FROM users WHERE users.id=".$formData2['full_name']);
            $data2 = $query1->fetch();
            $data['user_id'] = $formData2['emp_id'];
            $data['emp_name'] = $data2['name'];
            $data['vehicle_type'] =$formData2['vehicle_type'];
            $data['company_name'] = $formData2['comp_name'];
            $data['branch'] = $formData2['location'];
            $data['date'] = mySqlDateFormat($formData2['date'],$id='null',$this->companyConnection);
            (isset($formData2['date_purchases']) && !empty($formData2['date_purchases'])) ? $data['date_purchased'] = mySqlDateFormat($formData2['date_purchases'],$id='null',$this->companyConnection) : null;
            $data['vehicle_name'] = $formData2['vehicle_name'];
            $data['vehicle_id'] = $formData2['vehicle_id'];
            $data['vehicle_make'] = $formData2['vehicle_make'];
            $data['model'] = $formData2['model'];
            $data['vin_no'] = $formData2['vin'];
            $data['registration'] = $formData2['registration_no'];
            $data['licence_plate'] = $formData2['license_no'];
            $data['color'] = $formData2['color'];
            $data['year_of_vehicle'] = $formData2['year_vehicle'];
            $data['start_location'] = $formData2['start_loc'];
            $data['end_location'] = $formData2['end_loc'];
            $data['initial_reading'] = $formData2['start_reading'];
            $data['final_reading'] = $formData2['stop_reading'];
            $data['mileage'] = $formData2['mileage'];
            if(isset($formData2['amount']) && !empty($formData2['amount'])){
                $data['amount'] = $formData2['amount'];
            }
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE mielage_log SET ".$sqlData['columnsValuesPair']." where id='$data_id'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            $data1['user_id'] = $formData2['full_name'];
            $data1['vehicle_type'] = $formData2['vehicle_type'];
            (isset($formData2['date_purchases']) && !empty($formData2['date_purchases'])) ? $data['date_purchased'] = mySqlDateFormat($formData2['date_purchases'],$id='null',$this->companyConnection) : null;
            $data1['vehicle_name'] = $formData2['vehicle_name'];
            $data1['vehicle_type'] = $formData2['vehicle_id'];
            $data1['make'] = $formData2['vehicle_make'];
            $data1['model'] = $formData2['model'];
            $data1['vin'] = $formData2['vin'];
            $data1['registration'] = $formData2['registration_no'];
            $data1['plate_number'] = $formData2['license_no'];
            $data1['color'] = $formData2['color'];
            $data1['year_of_vehicle'] = $formData2['year_vehicle'];

            $data1['starting_mileage'] = $formData2['mileage'];
            if(isset($formData2['amount']) && !empty($formData2['amount'])){
                $data1['amount'] = $formData2['amount'];
            }
            $data1['created_at'] = date('Y-m-d H:i:s');
            $data1['updated_at'] = date('Y-m-d H:i:s');
            $sqlData1 = createSqlColValPair($data1);
            $query1 = "UPDATE company_manage_vehicle SET ".$sqlData1['columnsValuesPair']." where id='$man_id'";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute();
            return ['status'=>'success','code'=>200, 'message' => 'Record added successfully.'];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }



    public function edit_emplog1()

    {
        try {
            $data_id=$_POST['id'];
            $formData2 = $_POST['formData'];
            $formData2 = postArray($formData2);
            $query1 = $this->companyConnection->query("SELECT users.name FROM users WHERE users.id=" . $formData2['full_namee']);
            $data2 = $query1->fetch();
            $data['user_id'] = $formData2['emp_idd'];
            $data['emp_name'] = $data2['name'];
            $data['company_name'] = $formData2['comp_namee'];
            $data['branch'] = $formData2['locationn'];
            $data['date'] = mySqlDateFormat($formData2['date'], $id = 'null', $this->companyConnection);
            $data['vehicle_name'] = $formData2['select_vehicle'];
            $data['driver'] = $formData2['driver'];
            $data['start_location'] = $formData2['start_loca'];
            $data['end_location'] = $formData2['end_loca'];
            $data['initial_reading'] = $formData2['start_readingg'];
            $data['final_reading'] = $formData2['stop_readingg'];
            $data['mileage'] = $formData2['mileagee'];
            $data['notes'] = $formData2['note'];
            $data['type'] = 'C';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE mielage_log SET ".$sqlData['columnsValuesPair']." where id='$data_id'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            return ['status' => 'success', 'code' => 200, 'message' => 'Record added successfully.'];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }


    public function deletelog() {
        try {
            $data=[];
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT id FROM users WHERE id=".$id);
            $data3 = $query->fetch();
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE mielage_log SET ".$sqlData['columnsValuesPair']." where id='$id'";
//                $query = "DELETE FROM users WHERE id=".$data3['user_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }





}







$mileagelogAjax = new mileagelog();

