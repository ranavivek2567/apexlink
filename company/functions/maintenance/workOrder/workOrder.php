<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class workOrder extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function getVendorsdata() {

        try {
            $search = @$_REQUEST['q'];
            $sql = "SELECT users.*, vendor_additional_detail.vendor_rate  FROM users  
                left join vendor_additional_detail on users.id = vendor_additional_detail.id
                where users.user_type='3' AND users.status = '1' and name LIKE '%$search%' order by name asc";
            $users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);
            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['id'] = $user_data['id'];
                    $data['rows'][$key]['name'] = $user_data['name'];
                    $data['rows'][$key]['company_name'] = $user_data['company_name'];
                    $data['rows'][$key]['phone_number'] = $user_data['phone_number'];
                    $data['rows'][$key]['address1'] = $user_data['address1'] . ', ' . $user_data['address2'] . ' ' . $user_data['address3'];
                    $data['rows'][$key]['email'] = $user_data['email'];

                    $data['rows'][$key]['A1'] = $user_data['address1'];
                    $data['rows'][$key]['A2'] = $user_data['address2'];
                    $data['rows'][$key]['A3'] = $user_data['address3'];
                    $data['rows'][$key]['zip'] = $user_data['zipcode'];
                    $data['rows'][$key]['country'] = $user_data['country'];
                    $data['rows'][$key]['state'] = $user_data['state'];
                    $data['rows'][$key]['city'] = $user_data['city'];
                    $data['rows'][$key]['vendor_rate'] = $user_data['vendor_rate'];
                    $venid=$user_data['id'];
                    $sqlsingle = "SELECT vendor_additional_detail.*,users.* , company_vendor_type.vendor_type FROM users JOIN vendor_additional_detail as vendor_additional_detail
                    ON users.id=vendor_additional_detail.vendor_id left join company_vendor_type on users.id = company_vendor_type.user_id WHERE (users.id = $venid)";
                    $ressqlsingle = $this->companyConnection->query($sqlsingle)->fetch();

                    $sqlvendortype = "SELECT * FROM  company_vendor_type  where id='".$ressqlsingle['vendor_type_id']."'";
                    $resvendortype = $this->companyConnection->query($sqlvendortype)->fetch();
                    $data['rows'][$key]['vendor_type'] = $resvendortype['vendor_type'];
//dd($data);
                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }
    public function getPreffVendorsdata() {

        try {
            $search = @$_REQUEST['q'];
            $sql = "SELECT * FROM owner_blacklist_vendors  where  status = '1' and vendor_name LIKE '%$search%' order by vendor_name asc";$users_data = $this->companyConnection->query($sql)->fetchAll();
            $users_count = sizeOf($users_data);
            $data = [];
            if (!empty($users_data)) {
                foreach ($users_data as $key => $user_data) {
                    $data['total'] = $users_count;
                    $data['rows'][$key]['vendor_id'] = $user_data['vendor_id'];


                    $venid=$user_data['vendor_id'];
                    $sqlsingle = "SELECT * FROM users where id=$venid";
                    $ressqlsingle = $this->companyConnection->query($sqlsingle)->fetch();
                    $data['rows'][$key]['email'] = $ressqlsingle['email'];
                    $data['rows'][$key]['name'] = $ressqlsingle['name'];
                    $data['rows'][$key]['A1'] = $ressqlsingle['address1'];
                    $data['rows'][$key]['A2'] = $ressqlsingle['address2'];
                    $data['rows'][$key]['A3'] = $ressqlsingle['address3'];
                    $data['rows'][$key]['phone_number'] = $ressqlsingle['phone_number'];

                    $sqlsingle = "SELECT vendor_additional_detail.*,users.* FROM users JOIN vendor_additional_detail as vendor_additional_detail
                    ON users.id=vendor_additional_detail.vendor_id WHERE (users.id = $venid)";
                    $ressqlsingle = $this->companyConnection->query($sqlsingle)->fetch();

                    $sqlvendortype = "SELECT * FROM  company_vendor_type  where id='".$ressqlsingle['vendor_type_id']."'";
                    $resvendortype = $this->companyConnection->query($sqlvendortype)->fetch();
                    $data['rows'][$key]['vendor_type'] = $resvendortype['vendor_type'];

                }
            }
            echo json_encode($data, JSON_UNESCAPED_SLASHES);
            exit;
        } catch (Exception $e) {
            return ['status' => 'failed', 'code' => 503, 'data' => $e->getMessage()];
        }
    }
    public function addVendor() {
        try {
            $data = $_POST;
            $userData = [];
            $userData['salutation'] = $_POST['salutation'];
            $userData['first_name'] = $_POST['first_name'];
            $userData['middle_name'] = $_POST['middle_name'];
            $userData['last_name'] = $_POST['last_name'];
            $userData['status'] = '1';
            $userData['role'] = '';
            $userData['user_type'] = '3';
            $userData['record_status'] = '1';
            $userData['gender'] = (isset($_POST['gender']) && !empty($_POST['gender']) ? $_POST['gender'] : 0);
            $userData['company_name'] = (isset($_POST['company_name']) && !empty($_POST['company_name']) ? $_POST['company_name'] : '');
            $userData['if_entity_name_display'] = (isset($_POST['use_company_name']) && !empty($_POST['use_company_name'])) ? '1' : '0';
            $userData['created_at'] = date('Y-m-d H:i:s');
            $userData['updated_at'] = date('Y-m-d H:i:s');
            $userData['referral_source'] = empty($_POST['additional_referralSource']) ? NULL : $_POST['additional_referralSource'];
            $userData['address1'] = $_POST['address1'];
            $userData['address2'] = $_POST['address2'];
            $userData['address3'] = $_POST['address3'];
            $userData['zipcode'] = $_POST['zipcode'];
            $userData['country'] = $_POST['country'];
            $userData['country_code']=$_POST['additional_country'];
            $userData['state'] = $_POST['state'];
            $userData['city'] = $_POST['city'];
            $userData['email'] =$_POST['additional_email'];
            if((isset($_POST['company_name'])) && !empty($_POST['company_name'])){
                $userData['name'] = $_POST['company_name'];
            } else {
                    $userData['name'] = $_POST['first_name'] . ' ' . $_POST['last_name'];
            }
//Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
//Number variable array
            $number_array = [];
//Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'users', 'first_name', $_POST['first_name'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Vendor Name already exists!');
                    }
                }
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'users', 'email', $_POST['additional_email'], '');
                if ($duplicate['is_exists'] == 1) {
                    return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Email already exists!');
                }
//declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                if (!empty($id)) unset($data['id']);
//Save Data in Company Database
 
                $sqlData12 = createSqlColVal($userData);
                $query = "INSERT INTO  users (" . $sqlData12['columns'] . ") VALUES (" . $sqlData12['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($userData);
                $id = $this->companyConnection->lastInsertId();
                $getdetailVendor="SELECT * FROM vendor_additional_detail WHERE vendor_id=" . $id . "";
                $datadetailVendor = $this->companyConnection->query($getdetailVendor)->fetch();
                $vendorData=[];
                $vendorData['vendor_id'] = $id;
                $vendorData['vendor_type_id'] = $_POST['vendor_type_id'];
                $vendorData['vendor_random_id'] = $_POST['vendor_random_id'];
                $vendorData['vendor_rate'] = $_POST['vendor_rate'];
                $vendorData['email'] = serialize($_POST['additional_email']);
                if(empty($datadetailVendor)) {
                    $sqlData = createSqlColVal($vendorData);
                    $query = "INSERT INTO  vendor_additional_detail (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($vendorData);
                    return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
                }else{
                    $sqlData = createSqlUpdateCase($vendorData);
                    $query = "UPDATE vendor_additional_detail SET " . $sqlData['columnsValuesPair'] . " where vendor_id='$id'";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($sqlData['data']);
                    return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                }
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }
    public function updateAddress(){
        try {
            $id=$_POST['id'];
            $data = $_POST['form'];
            $data = postArray($data);
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id='$id'";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function addSingleData()
    {
        $value = $_POST['fieldValue'];
        $value2 = $_POST['fieldValue2'];
        $table = $_POST['table'];
        $column = $_POST['column'];
        $column2 = $_POST['column2'];
        $column3 = 'status';
        $column4 = 'user_id';
        $column5 = 'created_at';
        $column6 = 'updated_at';
        $column7='is_editable';
        $fieldValue3=date('Y-m-d H:i:s');
        $fieldValue4=date('Y-m-d H:i:s');
        $fieldValue5='1';

        $fieldValue = $_POST['fieldValue'];
        $fieldValue2 = $_POST['fieldValue2'];
        $status='1';
        $user_id= $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $query = "INSERT INTO $table($column,$column2,$column3,$column4,$column5,$column6,$column7) VALUES ('$fieldValue','$fieldValue2','$status','$user_id','$fieldValue3','$fieldValue4','$fieldValue5')";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        $id = $this->companyConnection->lastInsertId();
        if ($column2 == "portfolio_name") {
            return array('id'=>$id,'value'=>$value2);
        }
        return array('id'=>$id,'value'=>$value);
    }
    public function addPopUpData()
    {
        $value = $_POST['fieldValue'];
        $table = $_POST['table'];
        $column = $_POST['column'];
        $column3 = 'status';
        $column4 = 'user_id';
        $column5 = 'created_at';
        $column6 = 'updated_at';
        $column7='is_editable';
        $fieldValue3=date('Y-m-d H:i:s');
        $fieldValue4=date('Y-m-d H:i:s');
        $fieldValue5='1';
        $fieldValue = $_POST['fieldValue'];
        $status='1';
        $user_id= $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $query = "INSERT INTO $table($column,$column3,$column4,$column5,$column6,$column7) VALUES ('$fieldValue','$status','$user_id','$fieldValue3','$fieldValue4','$fieldValue5')";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        $id = $this->companyConnection->lastInsertId();
        return array('id'=>$id,'value'=>$value);
    }
    public function getAllProperties(){
        try {
            $id=$_POST['id'];
            $html = "";
            $query ="SELECT * FROM `general_property` WHERE `portfolio_id`=$id ORDER BY property_name ASC" ;
            $dataVendors = $this->companyConnection->query($query)->fetchAll();
//            dd($query);
            $html .='<option value="0">Select</option>';
            foreach ($dataVendors as $dataSingle) {
                $html .= "<option value=" . $dataSingle['id'] . ">" . $dataSingle['property_name'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html' => $html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }

    }
    public function getAllBuildings(){
        try {
            $id=$_POST['id'];
            $html = "";
            $building ="SELECT * FROM building_detail WHERE property_id = $id ORDER BY building_name ASC";
            $databuilding = $this->companyConnection->query($building)->fetchAll();

            $html .='<option value="0">Select</option>';
            foreach ($databuilding as $databuildingSingle) {
                $html .= "<option value=" . $databuildingSingle['id'] . ">" . $databuildingSingle['building_name'] . "</option>";
            }



            return array('code' => 200, 'status' => 'success', 'html' => $html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function getAllUnits(){
        try {
            $id=$_POST['id'];
            $html = "";
            $building ="SELECT * FROM unit_details WHERE building_id = $id";
            $databuilding = $this->companyConnection->query($building)->fetchAll();

            $html .="<option value=''>Select</option>";
            foreach ($databuilding as $databuildingSingle) {
                $html .= "<option value=" . $databuildingSingle['id'] . ">" . $databuildingSingle['unit_no'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html' => $html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function petinUnits(){
        try {
            $id=$_POST['id'];
            $html = "";
            $petinunits ="SELECT unit_details.*,company_pet_friendly.* FROM unit_details JOIN company_pet_friendly as company_pet_friendly ON unit_details.pet_friendly_id=company_pet_friendly.id 
            WHERE (unit_details.id = $id)";

            $datapets = $this->companyConnection->query($petinunits)->fetchAll();

            $tenant ="SELECT * FROM  tenant_property WHERE unit_id = ".$id;
            $datatenant = $this->companyConnection->query($tenant)->fetch();

            $tenantuser ="SELECT * FROM users  WHERE id = '".$datatenant['user_id']."'";
            $datatenantuser = $this->companyConnection->query($tenantuser)->fetch();

//            $html .="<option value=''>Select</option>";

            foreach ($datapets as $datapetsSingle) {
                $html .= "<option value=" . $datapetsSingle['pet_friendly_id'] . ">" . $datapetsSingle['pet_friendly'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html' => $html,'datatenantuser'=>$datatenantuser);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function fetchAllPriorities(){
        try {
            $html = "";
            $prior="SELECT * FROM company_priority_type WHERE status = '1'";
            $dataprior = $this->companyConnection->query($prior)->fetchAll();

            $html .="<option value=''>Select</option>";
            foreach ($dataprior as $datapriorSingle) {
                $html .= "<option value=" . $datapriorSingle['id'] . ">" . $datapriorSingle['priority'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html' => $html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function fetchAllCategory(){
        try {
            $html = "";
            $cat="SELECT * FROM company_workorder_category WHERE status = '1'";
            $datacat = $this->companyConnection->query($cat)->fetchAll();

            $html .="<option value=''>Select</option>";
            foreach ($datacat as $datacatSingle) {
                $html .= "<option value=" . $datacatSingle['id'] . " selected>" . $datacatSingle['category'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html' => $html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function workOrderStatus(){
        try {
            $html = "";
            $cat="SELECT * FROM company_workorder_status WHERE status = '1' ORDER BY work_order_status ASC";
            $datacat = $this->companyConnection->query($cat)->fetchAll();

            $html .="<option value=''>Select</option>";
            foreach ($datacat as $datacatSingle) {
                $selected = $datacatSingle['id'] =='1' ? 'selected' : '';
                $html .= "<option ".$selected." value=" . $datacatSingle['id'] . ">" . $datacatSingle['work_order_status'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html' => $html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function workOrderSource(){
        try {
            $html = "";
            $source="SELECT * FROM company_workorder_source WHERE status = '1' ORDER BY work_order_source ASC";
            $datasource = $this->companyConnection->query($source)->fetchAll();

            $html .="<option value=''>Select</option>";
            foreach ($datasource as $datasourceSingle) {
                $html .= "<option value=" . $datasourceSingle['id'] . ">" . $datasourceSingle['work_order_source'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html' => $html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function workOrderRequest(){
        try {
            $html = "";
            $req="SELECT * FROM company_workorder_requestedby WHERE status = '1'";
            $datareq = $this->companyConnection->query($req)->fetchAll();

            $html .="<option value=''>Select</option>";
            foreach ($datareq as $datareqSingle) {
                $html .= "<option value=" . $datareqSingle['id'] . ">" . $datareqSingle['work_order_request'] . "</option>";
            }
            return array('code' => 200, 'status' => 'success', 'html' => $html);
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }
    public function newWorkOrder(){
        try {
            $workOrder=[];
            $files = $_FILES;
            $libraryFiles = $_FILES;
            unset($libraryFiles['file_library']);
            $custom_field = isset($_POST['custom_field']) ? json_decode(stripslashes($_POST['custom_field'])) : [];
            if (!empty($custom_field)) {
                foreach ($custom_field as $key => $value) {
                    $custom_field[$key] = (array) $value;
                }
            }
            if (!empty($custom_field)) {
                foreach ($custom_field as $key => $value) {
                    if ($value['data_type'] == 'date' && !empty($value['default_value']))
                        $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                    if ($value['data_type'] == 'date' && !empty($value['value']))
                        $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                    continue;
                }
            }
            $workOrder['custom_fields'] = (count($custom_field) > 0) ? serialize($custom_field) : null;
//Save Data in Company Database
            $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
            $updateColumn = createSqlColValPair($custom_data);
            if (count($custom_field) > 0) {
                foreach ($custom_field as $key => $value) {
                    updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                }
            }


            $workOrder['work_order_type'] = (isset($_POST['work_order_type']) && ($_POST['work_order_type'] == 0 || $_POST['work_order_type'] == 1) ? $_POST['work_order_type'] : null);
            $workOrder['rec_month'] = (isset($_POST['rec_month']) && !empty($_POST['rec_month']) ? $_POST['rec_month'] : '');
            $workOrder['work_order_number'] = (isset($_POST['work_order_number']) && !empty($_POST['work_order_number']) ? $_POST['work_order_number'] : '');
            $workOrder['work_order_cat'] = (isset($_POST['work_order_cat']) && !empty($_POST['work_order_cat']) ? $_POST['work_order_cat'] : null);
            $workOrder['priority_id'] = (isset($_POST['priority_id']) && !empty($_POST['priority_id']) ? $_POST['priority_id'] : null);
            $workOrder['portfolio_id'] = (isset($_POST['portfolio_id']) && !empty($_POST['portfolio_id']) ? $_POST['portfolio_id'] : '');
            $workOrder['property_id'] = (isset($_POST['property_id']) && !empty($_POST['property_id']) ? $_POST['property_id'] : '');
            $workOrder['building_id'] = (isset($_POST['building_id']) && !empty($_POST['building_id']) ? $_POST['building_id'] : '');
            $workOrder['unit_id'] = (isset($_POST['unit_id']) && !empty($_POST['unit_id']) ? $_POST['unit_id'] : '');
            $workOrder['pet_id'] =(isset($_POST['pet_id']) && !empty($_POST['pet_id']) ? $_POST['pet_id'] : 0);
            $workOrder['created_at'] = date('Y-m-d H:i:s');
            $workOrder['updated_at'] = date('Y-m-d H:i:s');
            $workOrder['created_on'] = (!empty($_POST['created_on'])) ? mySqlDateFormat($_POST['created_on'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection) : null;


            //gen infor
            $workOrder['status_id'] =(isset($_POST['status_id']) && !empty($_POST['status_id']) ? $_POST['status_id'] : null);
            $workOrder['source_id'] = (isset($_POST['source_id']) && !empty($_POST['source_id']) ? $_POST['source_id'] : null);
            $workOrder['request_id'] =(isset($_POST['request_id']) && !empty($_POST['request_id']) ? $_POST['request_id'] : null);
            $workOrder['estimated_cost'] = (isset($_POST['estimated_cost']) && !empty($_POST['estimated_cost']) ? $_POST['estimated_cost'] : null);
            $workOrder['scheduled_on'] = (!empty($_POST['scheduled_on'])) ? mySqlDateFormat($_POST['scheduled_on'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection) : null;

            $workOrder['completed_on'] =  (!empty($_POST['completed_on'])) ? mySqlDateFormat($_POST['completed_on'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection) : null;

            $workOrder['authority'] =(isset($_POST['authority']) && !empty($_POST['authority']) ? $_POST['authority'] : null);
            $workOrder['recipient'] = (isset($_POST['recipient']) && !empty($_POST['recipient']) ? $_POST['recipient'] : null);
            $workOrder['work_order_description'] = (isset($_POST['work_order_description']) && !empty($_POST['work_order_description']) ? $_POST['work_order_description'] : null);
            $workOrder['required_materials'] = (isset($_POST['required_materials']) && !empty($_POST['required_materials']) ? $_POST['required_materials'] : null);
            $workOrder['approved_by_owner'] = (isset($_POST['approved_by_owner']) && !empty($_POST['approved_by_owner']) ? $_POST['approved_by_owner'] : null);
            $workOrder['publish_to_tenant'] = (isset($_POST['publish_to_tenant']) && !empty($_POST['publish_to_tenant']) ? $_POST['publish_to_tenant'] : null);
            $workOrder['send_alert'] =  (isset($_POST['send_alert']) && !empty($_POST['send_alert']) ? $_POST['send_alert'] : null);
            $workOrder['publish_to_owner'] = (isset($_POST['publish_to_owner']) && !empty($_POST['publish_to_owner']) ? $_POST['publish_to_owner'] : null);
            $workOrder['publish_to_vendor'] = (isset($_POST['publish_to_vendor']) && !empty($_POST['publish_to_vendor']) ? $_POST['publish_to_vendor'] : null);
            $workOrder['tenant_id'] = (isset($_POST['tenant_id']) && !empty($_POST['tenant_id']) ? $_POST['tenant_id'] : null);
            $workOrder['vendor_id'] = (isset($_POST['vendor_id']) && !empty($_POST['vendor_id']) ? $_POST['vendor_id'] : null);
            $workOrder['user_id'] = (isset($_POST['user_id']) && !empty($_POST['user_id']) ? $_POST['user_id'] : null);

//            dd($workOrder);
            $sqlData = createSqlColVal($workOrder);

            $query = "INSERT INTO  work_order (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

            $stmt = $this->companyConnection->prepare($query);

            $stmt->execute($workOrder);
            $id = $this->companyConnection->lastInsertId();
            $libraryData = $this->addFileLibrary($id, $libraryFiles);
            $addNotes = $this->addNotes($id);
            $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';

            if( $workOrder['status_id'] == 6){
                $notifiData = "SELECT wo.user_id,wo.property_id,wo.created_on ,CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name FROM work_order as wo JOIN unit_details as ud ON ud.id = wo.unit_id where wo.id =".$id;
                $notifiquery = $this->companyConnection->query($notifiData)->fetch();
                $unitName  = $notifiquery['unit_name'];
                $propertyId=$notifiquery['property_id'];
                $notificationTitle= "Work order completed";
                $module_type = "WORKORDER";
                $alert_type = "Real Time";
                $descriptionNotifi ='Work order ID -'.$id.'  has been marked as complete for Property ID-'.$propertyId.', Unit '.$unitName.'.';
                insertNotification($this->companyConnection,$id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
            }

            if (!empty($user_id)){
                $owner_query = "Select id, user_type FROM users WHERE id =".$user_id;
                $owner_data = $this->companyConnection->query($owner_query)->fetch();
                if ($owner_data['user_type'] == 4){
                    /* Owner notification */
                    $notifiData = "SELECT u.name,wo.user_id,wo.property_id,wo.created_on FROM work_order as wo JOIN users as u ON u.id=wo.user_id where wo.user_id =".$user_id;
                    $notifiquery = $this->companyConnection->query($notifiData)->fetch();

                    $name = $notifiquery['name'];
                    $propertyId=$notifiquery['property_id'];
                    $dateCreated = dateFormatUser($notifiquery['created_on'],$user_id,$this->companyConnection);

                    $notificationTitle= "Owner work order alert";
                    $module_type = "WORKORDER";
                    $alert_type = "Real Time";
                    $descriptionNotifi ='Work order for Owner ID-'.$user_id.'(Name - '.$name.', PropertyID-'.$propertyId.') has been created on '.$dateCreated.'.';
                    insertNotification($this->companyConnection,$user_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                    /* Owner notification */
                }

                /*tenant work order*/
                if ($owner_data['user_type'] == 2){
                    /* Owner notification */
                    $notifiData = "SELECT u.name,CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name,wo.user_id,wo.property_id,wo.created_on FROM work_order as wo JOIN users as u ON u.id=wo.user_id JOIN unit_details as ud ON ud.id = wo.unit_id where wo.user_id =".$user_id;
                    $notifiquery = $this->companyConnection->query($notifiData)->fetch();
                    $unitName  = $notifiquery['unit_name'];
                    $name = $notifiquery['name'];
                    $propertyId=$notifiquery['property_id'];
                    $dateCreated = dateFormatUser($notifiquery['created_on'],$user_id,$this->companyConnection);

                    $notificationTitle= "Tenant work order confirmation";
                    $module_type = "WORKORDER";
                    $alert_type = "Real Time";
                    $descriptionNotifi ='Work Order has been generated by Tenant ID -'.$user_id.'(Name - '.$name.', PropertyID-'.$propertyId.', Unit '.$unitName.') has been created on '.$dateCreated.'.';
                    insertNotification($this->companyConnection,$user_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                    /* Owner notification */
                }
                /*tenant work order*/
            }else {
                /*notification for Add workorder*/
                $notifiData = "SELECT wo.user_id,wo.property_id,wo.created_on ,CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name FROM work_order as wo JOIN unit_details as ud ON ud.id = wo.unit_id where wo.id =".$id;
                $notifiquery = $this->companyConnection->query($notifiData)->fetch();
                $unitName  = $notifiquery['unit_name'];
                $propertyId=$notifiquery['property_id'];
                $notificationTitle= "Work order created";
                $module_type = "WORKORDER";
                $alert_type = "Real Time";
                $descriptionNotifi ='Work order ID -'.$id.' has been created for  Property ID-'.$propertyId.', Unit '.$unitName.'.';
                insertNotification($this->companyConnection,$id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
            }


            /*notification for Add workorder*/

            return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }


    public function addFileLibrary($id, $fileData) {
        try {
            $files = $fileData;
            unset($files['tenant_image']);
            $user_id = $id;
            if (!empty($files)) {
                $domain = getDomain();
                $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
                foreach ($files as $key => $value) {
                    $file_name = $value['name'];
                    $file_tmp = $value['tmp_name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);
                    $name = time() . uniqid(rand());
                    $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
//Check if the directory already exists.
                    if (!is_dir(ROOT_URL . '/company/' . $path)) {
//Directory does not exist, so lets create it.
                        mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                    }
                    move_uploaded_file($file_tmp, ROOT_URL . '/company/' . $path . '/' . $name . '.' . $ext);
                    $data = [];
                    $data['work_order_id'] = $id;
                    $data['name'] = $file_name;
//                    $data['file_size'] = isa_convert_bytes_to_specified($value['size'], 'K').'kb';
                    $data['path'] = $path . '/' . $name . '.' . $ext;
                    $data['extension'] = $ext;
//                    $data['marketing_site'] = '0';
//                    $data['file_type'] = '2';
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($data);
                    $query = "INSERT INTO work_file_library (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($data);
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Documents uploaded successfully.');
            }
        } catch (PDOException $e) {
            dd($e);
            return array('code' => 500, 'status' => 'failed', 'message' => $e->getMessage());
        }
    }

    public function addNotes($id) {
        try {
            if (isset($_POST['note'])) {
                $i=0;
                $chargeNote = $_POST['note'];
//dd($chargeNote);
                foreach ($chargeNote as $k=>$value) {

                        $data['work_order_id'] = $id;
                        $data['notes'] = (isset($value) && !empty($value) ? $value : null);
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO work_order_notes(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                   // dd($query);
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                    $i++;
                }

            }
            return array('status' => 'success', 'message' => 'Data added successfully.', );
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_credentials');
            printErrorLog($e->getMessage());
        }
    }






    public function getworkorderdata() {

        try{
            $id = $_POST['id'];

            // $query = $this->companyConnection->query("SELECT user_id, unit_id, expected_move_in FROM lease_guest_card where user_id=".$user_id);

            //  $data1 = $query->fetch();

            $query = $this->companyConnection->query("SELECT work_order. *,
                                        company_workorder_category.category as work_category,
                                        general_property.property_name as property_name,
                                        general_property.no_of_units as units,
                                        general_property.no_of_buildings as buldings,
                                        company_priority_type.priority as priority_type,
                                         company_workorder_status.work_order_status as work_status,
                                          company_workorder_source.work_order_source as work_source,
                                          users.id as ten_id,
                                           users.name as tenant_name,
                                            users.email as email,
                                             users.phone_number as phone,
                                             work_order_notes.notes as note
                                                           FROM work_order 
                          LEFT JOIN company_workorder_category ON work_order.work_order_cat=company_workorder_category.id
                          LEFT JOIN general_property ON work_order.property_id=general_property.id
                          LEFT JOIN company_priority_type ON work_order.priority_id=company_priority_type.id
                          LEFT JOIN company_workorder_status ON work_order.status_id=company_workorder_status.id
                          LEFT JOIN company_workorder_source ON work_order.source_id=company_workorder_source.id
                          LEFT JOIN users ON work_order.tenant_id=users.id  
                          LEFT JOIN work_order_notes ON work_order.id=work_order_notes.work_order_id 
                            where work_order.id=".$id);
            $data = $query->fetch();
//            dd($query);
            $data['created_on']=dateFormatUser( $data['created_on'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
            $data['scheduled_on']=dateFormatUser( $data['scheduled_on'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
            $data['completed_on']=dateFormatUser( $data['completed_on'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);
//dd($data);
            $query1 = $this->companyConnection->query("SELECT work_order.vendor_id ,
                                           users.name as vendor_name,
                                            users.email as email,
                                            users.address1 as vendor_adress1,
                                             users.address2 as vendor_adress2,
                                            company_vendor_type.vendor_type as category,
                                             users.phone_number as phone
                                                           FROM work_order 
                          LEFT JOIN users ON work_order.vendor_id=users.id  
                          LEFT JOIN company_vendor_type ON work_order.vendor_id=company_vendor_type.id  
                            where work_order.id=".$id);
            $data1 = $query1->fetch();

            return ['status'=>'success','code'=>200, 'data'=> $data, 'data1'=>$data1];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }


    public function updateworkorder(){
        try {

            $id = $_POST['id'];
            $workOrder=[];
            $files = $_FILES;
            $libraryFiles = $_FILES;
            unset($libraryFiles['file_library']);
            $custom_field = isset($_POST['custom_field']) ? json_decode(stripslashes($_POST['custom_field'])) : [];
            if (!empty($custom_field)) {
                foreach ($custom_field as $key => $value) {
                    $custom_field[$key] = (array) $value;
                }
            }
            if (!empty($custom_field)) {
                foreach ($custom_field as $key => $value) {
                    if ($value['data_type'] == 'date' && !empty($value['default_value']))
                        $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                    if ($value['data_type'] == 'date' && !empty($value['value']))
                        $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                    continue;
                }
            }
            $workOrder['custom_fields'] = (count($custom_field) > 0) ? serialize($custom_field) : null;
//Save Data in Company Database
            $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
            $updateColumn = createSqlColValPair($custom_data);
            if (count($custom_field) > 0) {
                foreach ($custom_field as $key => $value) {
                    updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                }
            }

            $workOrder['work_order_type'] = (isset($_POST['work_order_type']) && ($_POST['work_order_type'] == 0 || $_POST['work_order_type'] == 1) ? $_POST['work_order_type'] : 0);
            $workOrder['rec_month'] = (isset($_POST['rec_month']) && !empty($_POST['rec_month']) ? $_POST['rec_month'] : '');
            $workOrder['work_order_number'] = (isset($_POST['work_order_number']) && !empty($_POST['work_order_number']) ? $_POST['work_order_number'] : '');
            $workOrder['work_order_cat'] = (isset($_POST['work_order_cat']) && !empty($_POST['work_order_cat']) ? $_POST['work_order_cat'] : 0);
            $workOrder['priority_id'] = (isset($_POST['priority_id']) && !empty($_POST['priority_id']) ? $_POST['priority_id'] : 0);
            $workOrder['portfolio_id'] = (isset($_POST['portfolio_id']) && !empty($_POST['portfolio_id']) ? $_POST['portfolio_id'] : null);
            $workOrder['property_id'] = (isset($_POST['property_id']) && !empty($_POST['property_id']) ? $_POST['property_id'] : 0);
            $workOrder['building_id'] = (isset($_POST['building_id']) && !empty($_POST['building_id']) ? $_POST['building_id'] : 0);
            $workOrder['unit_id'] = (isset($_POST['unit_id']) && !empty($_POST['unit_id']) ? $_POST['unit_id'] : 0);
            $workOrder['pet_id'] =(isset($_POST['pet_id']) && !empty($_POST['pet_id']) ? $_POST['pet_id'] : 0);
            $workOrder['created_at'] = date('Y-m-d H:i:s');
            $workOrder['updated_at'] = date('Y-m-d H:i:s');
            $workOrder['created_on'] = (!empty($_POST['created_on'])) ? mySqlDateFormat($_POST['created_on'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection) : null;

            //gen infor
            $workOrder['status_id'] =(isset($_POST['status_id']) && !empty($_POST['status_id']) ? $_POST['status_id'] : 0);
            $workOrder['source_id'] = (isset($_POST['source_id']) && !empty($_POST['source_id']) ? $_POST['source_id'] : 0);
            $workOrder['request_id'] =(isset($_POST['request_id']) && !empty($_POST['request_id']) ? $_POST['request_id'] : 0);
            $workOrder['estimated_cost'] = (isset($_POST['estimated_cost']) && !empty($_POST['estimated_cost']) ? $_POST['estimated_cost'] : '');
            $workOrder['scheduled_on'] = (!empty($_POST['scheduled_on'])) ? mySqlDateFormat($_POST['scheduled_on'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection) : null;
            $workOrder['completed_on'] =  (!empty($_POST['completed_on'])) ? mySqlDateFormat($_POST['completed_on'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection) : null;
            $workOrder['authority'] =(isset($_POST['authority']) && !empty($_POST['authority']) ? $_POST['authority'] : '');
            $workOrder['recipient'] = (isset($_POST['recipient']) && !empty($_POST['recipient']) ? $_POST['recipient'] : '');
            $workOrder['work_order_description'] = (isset($_POST['work_order_description']) && !empty($_POST['work_order_description']) ? $_POST['work_order_description'] : '');
            $workOrder['required_materials'] = (isset($_POST['required_materials']) && !empty($_POST['required_materials']) ? $_POST['required_materials'] : '');
            $workOrder['approved_by_owner'] = (isset($_POST['approved_by_owner']) && !empty($_POST['approved_by_owner']) ? $_POST['approved_by_owner'] : '');
            $workOrder['publish_to_tenant'] = (isset($_POST['publish_to_tenant']) && !empty($_POST['publish_to_tenant']) ? $_POST['publish_to_tenant'] : '');
            $workOrder['send_alert'] =  (isset($_POST['send_alert']) && !empty($_POST['send_alert']) ? $_POST['send_alert'] : null);
            $workOrder['publish_to_owner'] = (isset($_POST['publish_to_owner']) && !empty($_POST['publish_to_owner']) ? $_POST['publish_to_owner'] : '');
            $workOrder['publish_to_vendor'] = (isset($_POST['publish_to_vendor']) && !empty($_POST['publish_to_vendor']) ? $_POST['publish_to_vendor'] : '');
            $workOrder['tenant_id'] = (isset($_POST['tenant_id']) && !empty($_POST['tenant_id']) ? $_POST['tenant_id'] : 0);
            $workOrder['vendor_id'] = (isset($_POST['vendor_id']) && !empty($_POST['vendor_id']) ? $_POST['vendor_id'] : 0);
             //dd($workOrder);
            $sqlData = createSqlColValPair($workOrder);
            $query = "UPDATE work_order SET " . $sqlData['columnsValuesPair'] . " where id=$id";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $updatenots=$this->updateNotes($id);

            /*notification for update workorder*/
            $notifiData = "SELECT wo.id,wo.user_id,wo.property_id,wo.created_on ,CONCAT(ud.unit_prefix,'-',ud.unit_no) AS unit_name FROM work_order as wo JOIN unit_details as ud ON ud.id = wo.unit_id where wo.id =".$id;
            $notifiquery = $this->companyConnection->query($notifiData)->fetch();
            $woId = $notifiquery['id'];
            $wouserId = $notifiquery['user_id'];
            $unitName  = $notifiquery['unit_name'];
            $propertyId=$notifiquery['property_id'];
            $notificationTitle= "Work order Changed";
            $module_type = "WORKORDER";
            $alert_type = "Real Time";
            $descriptionNotifi = 'Work order ID-'.$woId.' has been changed for  Property ID-'.$propertyId.', Unit '.$unitName.'.';
            insertNotification($this->companyConnection,$woId,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
            /* notification for update work order*/


         /* $libraryData = $this->addFileLibrary($id, $libraryFiles);*/
            return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
        } catch (Exception $exception) {
            return array('status' => 'failed', 'code' => 503, 'message' => $exception->getMessage());
        }
    }

    public function updateNotes($id) {
        try {

            $getnotesdetail="SELECT * FROM work_order_notes WHERE work_order_id=".$id;
            $getnotesdetails = $this->companyConnection->query($getnotesdetail)->fetch();

            if (isset($_POST['note'])) {
                $i=0;
                $chargeNote = $_POST['note'];
//dd($chargeNote);

                if(empty($getnotesdetails)) {
                    foreach ($chargeNote as $k=>$value) {
                        $id=trim($id, "''");
                        $data['created_at'] = date('Y-m-d H:i:s');
                        $data['work_order_id'] = $id;
                        $data['notes'] = (isset($value) && !empty($value) ? $value : null);
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO work_order_notes(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                        $i++;
                    }
                   // return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
                }else{
                    foreach ($chargeNote as $k=>$value) {
                        $data['updated_at'] = date('Y-m-d H:i:s');
                        $id=trim($id, "''");

                        $data['work_order_id'] = $id;
                        $data['notes'] = (isset($value) && !empty($value) ? $value : null);
                        $sqlData = createSqlColValPair($data);
                        $query = "UPDATE work_order_notes SET " . $sqlData['columnsValuesPair'] . " where work_order_id=".$id;

                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                        // return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
                        $i++;
                    }

                }

            }
            return array('status' => 'success', 'message' => 'Data added successfully.', );
        } catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'tenant_credentials');
            printErrorLog($e->getMessage());
        }
    }


    public function deleteworkorder() {
        try {
            $data=[];
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT id FROM users WHERE id=".$id);
            $data3 = $query->fetch();
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE work_order SET ".$sqlData['columnsValuesPair']." where id='$id'";
//                $query = "DELETE FROM users WHERE id=".$data3['user_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getpropdata() {

        try{
            $id = $_POST['id'];
            $redirection_from = $_POST['redirection_from'];


            // $query = $this->companyConnection->query("SELECT user_id, unit_id, expected_move_in FROM lease_guest_card where user_id=".$user_id);

            //  $data1 = $query->fetch();
            switch ($redirection_from) {
                case "owner_module":
                    $query="SELECT owner_property_owned.property_id, general_property.property_name 
                            FROM owner_property_owned LEFT JOIN general_property ON general_property.id=owner_property_owned.property_id
                            where owner_property_owned.user_id=$id";
                    $data =$this->companyConnection->query($query)->fetchAll();
                    return ['status'=>'success','code'=>200, 'data'=> $data];
                    break;
                case "property_module":
                    $query = $this->companyConnection->query("SELECT general_property. *                              
                                                           FROM general_property 
                                                      where general_property.id=".$id);
                    $data = $query->fetch();

                    $query1 = $this->companyConnection->query("SELECT building_detail.id                              
                                                                     FROM building_detail              
                                                     where building_detail.property_id=".$id);
                    $data1 = $query1->fetchAll();
                    $data2='';
                    if(isset($data1) && !empty($data1)){
                        $query2 = $this->companyConnection->query("SELECT unit_details. *
                                                          FROM unit_details
                           where unit_details.building_id=".$data1[0]['id']);
                        $data2 = $query2->fetchAll();
                    }
                    return ['status'=>'success','code'=>200, 'data'=> $data,'data1'=>$data1,'data2'=>$data2];
                    break;
                case "ticket_module":

                    $query3 = $this->companyConnection->query("SELECT property_id, user_id, unit_id                            
                                                           FROM tenant_maintenance 
                                                      where tenant_maintenance.id=".$id);
                    $data3 = $query3->fetch();

                    $query4 = $this->companyConnection->query("SELECT id, portfolio_id, property_name                           
                                                           FROM general_property 
                                                      where general_property.id=".$data3['property_id']);
                    $data4 = $query4->fetch();


                    $query5 = $this->companyConnection->query("SELECT id, user_id, building_id, unit_prefix, unit_no                           
                                                           FROM unit_details 
                                                      where unit_details.id=".$data3['unit_id']);
                    $data5 = $query5->fetch();

                    $query6 = $this->companyConnection->query("SELECT id, building_name                           
                                                           FROM building_detail 
                                                      where building_detail.id=".$data5['building_id']);
                    $data6 = $query6->fetch();
                    $query7 = $this->companyConnection->query("SELECT id, name, email, Phone_number                           
                                                           FROM users 
                                                      where users.id=".$data3['user_id']);
                    $data7 = $query7->fetch();
//                    dd($data7);
                    return ['status'=>'success','code'=>200,'data7'=>$data7,'data4'=>$data4,'data5'=>$data5];
                    break;
                case "vendor_module":
                    $query = $this->companyConnection->query("SELECT users.name, users.id, users.email, users.Phone_number,users.address1,
                                users.address2,users.address3,vendor_additional_detail.vendor_type_id                   
                                FROM users LEFT JOIN vendor_additional_detail ON vendor_additional_detail.vendor_id=users.id
                                where users.id=".$id);
                    $data = $query->fetch();

                    $sqlvendortype = "SELECT * FROM  company_vendor_type  where id='".$data['vendor_type_id']."'";
                    $resvendortype = $this->companyConnection->query($sqlvendortype)->fetch();

                    return ['status'=>'success','code'=>200,'data'=>$data,'resvendortype'=>$resvendortype['vendor_type']];
                    break;
                case "tenant_module":
                    $sql = "SELECT u.name,u.email,u.id, u.phone_number,tp.property_id,tp.building_id,tp.unit_id,gp.portfolio_id FROM 
                    users u JOIN tenant_property tp ON u.id=tp.user_id
                    JOIN general_property gp ON tp.property_id=gp.id
                    WHERE u.id=$id";
                    $query = $this->companyConnection->query($sql);
                    $data = $query->fetch();
                    return ['status'=>'success','code'=>200,'data'=>$data,];
                    break;

                default:

                    continue;
            }


        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function fetchPortfolioname() {
        $html = '';
        $sql = "SELECT * FROM company_property_portfolio";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html = "<option value='default'>Select</option>";
        foreach ($data as $d) {

            $html.= "<option value='" . $d['id'] . "' " . (($d['is_default'] == '1') ? "selected" : "") . ">" . $d['portfolio_name'] . "</option>";
        }
        return array('data' => $html, 'status' => 'success');
    }



}

$workOrder = new workOrder();
?>