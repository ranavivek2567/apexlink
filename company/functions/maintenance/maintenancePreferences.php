
<?php
include(ROOT_URL . "/config.php");
include_once(ROOT_URL . "/company/helper/helper.php");
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    header('Location: ' . BASE_URL);
};

class MaintenanceInventoryTracker extends DBConnection {

    /**
     * UserAjax constructor.
     */
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }
    public function createSqlColValPair($data) {
        $columnsValuesPair = '';
        foreach ($data as $key=>$value){
            if($key == 'deleted_at'){
                $columnsValuesPair .=  $key."=NULL";
            }else{
                $columnsValuesPair .=  $key."='".$value."',";
                $columnsValuesPair = substr_replace($columnsValuesPair ,"",-1);
            }
        }

        $sqlData = ['columnsValuesPair'=>$columnsValuesPair];
        return $sqlData;
    }
    public function insert(){
        try {

            $data=[];
            // $data['id'] = $_SESSION['user_id'];


            $data['inventory_item'] = $_POST['inventory_item'];
            $data['add_suppliers'] = $_POST['add_suppliers'];
            $data['upload_photos'] = $_POST['upload_photos'];



            //Required variable array
            $required_array = ['date','time'];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];

                $sqlData = createSqlColValPair($data);
                $query = "UPDATE company_inventory_preferences SET " . $sqlData['columnsValuesPair'] . " where id=".$user_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Password has been updated!');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function updateCategory($data,$editId){
        $data1['category'] = $data['category'];
        $sqlData = createSqlColValPair($data1);
        $query = "UPDATE company_maintenance_subcategory SET " . $sqlData['columnsValuesPair'] . " where id = " . $editId;
        $stmt1 = $this->companyConnection->prepare($query);
        $stmt1->execute();

        if(is_array($data['sub_category'])){
            for($i = 0; $i < count($data['sub_category']); $i++){
                $data2['sub_category'] = $data['sub_category'][$i];
                $sqlData = createSqlColValPair($data2);
                $query1 = "UPDATE company_maintenance_subcategory SET " . $sqlData['columnsValuesPair'] . " where id = " . $data['sub_cat_id'][$i];
                $stmt2 = $this->companyConnection->prepare($query1);
                $stmt2->execute();
            }
        }else{
            $data3['sub_category'] = $data['sub_category'];
            $editId = $data['sub_cat_id'];
            $sqlData = createSqlColValPair($data3);
            $query2 = "UPDATE company_maintenance_subcategory SET " . $sqlData['columnsValuesPair']." where id = " . $editId ;
            $stmt2 = $this->companyConnection->prepare($query2);
            $stmt2->execute();
        }
        return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record update successfully');
    }

    public function saveCategory($data){

        $data1['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $data1['parent_id'] = null;
        $data1['category'] = $data ['category'];

        $data1['sub_category'] = '';
        $data1['created_at'] =  date('Y-m-d H:i:s');
        $data1['updated_at'] =  date('Y-m-d H:i:s');


        $sqlData = createSqlColVal($data1);

        $query = "INSERT INTO  company_maintenance_subcategory (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($data1);
        $last_id = $this->companyConnection->lastInsertId();


        if(!empty($data['sub_category']) && is_array($data['sub_category'])) {
            foreach ($data['sub_category'] as $val) {
                if ($val != '') {
                    $subdata['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                    $subdata['parent_id'] = $last_id;
                    $subdata['category'] = '';
                    $subdata['sub_category'] = $val;
                    $subdata['created_at'] = date('Y-m-d H:i:s');
                    $subdata['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColVal($subdata);

                    $query = "INSERT INTO company_maintenance_subcategory (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($subdata);
                }
            }
        }else{
                $subdata['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $subdata['parent_id'] = $last_id;
                $subdata['category'] = '';
                $subdata['sub_category'] = $data['sub_category'];
                $subdata['created_at'] = date('Y-m-d H:i:s');
                $subdata['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColVal($subdata);

                $query = "INSERT INTO company_maintenance_subcategory (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($subdata);
            }
        return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'The record saved successfully');
    }
    public function insertcategory(){
        try {
            //print_r($_POST);
            $data=[];
            $data = postArray($_POST['form_data']);
            $required_array = [''];//Required variable array
            $maxlength_array = [];/* Max length variable array */
            $number_array = [];//Number variable array
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);

            if (checkValidationArray($err_array)) {//Checking server side validation
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if(isset($_POST['user_id_hidden10']) && $_POST['user_id_hidden10'] != ""){
                    $editId = $_POST['user_id_hidden10'];
                    return $this->updateCategory($data,$editId);
                }else{
                    unset($data['user_id_hidden10']);
                    unset($data['sub_cat_id']);
                    return $this->saveCategory($data);
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function insertsupplier()
    {
        try {
            if(isset($_POST['user_id_hidden']) && $_POST['user_id_hidden'] != ""){
                $editId = $_POST['user_id_hidden'];
            }else{
                $editId = '';
            }
            $data['supplier'] = $_POST['supplier'];
            $last_id="";
            $data['category_id'] = $_POST['category_id'];
            //$data['parent_id'] = $last_id;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['sub_category_id'] = "";


            $required_array = ['supplier'];
            //Max length variable array
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if($editId != '' ) {
                    $data1['supplier'] = $_POST['supplier'];
                    $data1['category_id'] = $_POST['category_id'];
                    $data1['updated_at'] = date('Y-m-d H:i:s');

                    $sqlData = createSqlColValPair($data1);

                    $query = "UPDATE company_maintenance_supplier SET " . $sqlData['columnsValuesPair'] . " where id = " . $editId;
                    $stmt1 = $this->companyConnection->prepare($query);
                    $stmt1->execute();
                    for($i = 0; $i <count($_POST['sub_category_id']); $i++){
                        $data2['sub_category_id'] = $_POST['sub_category_id'][$i];
                        $sqlData = createSqlColValPair($data2);
                        $query1 = "UPDATE company_maintenance_supplier SET " . $sqlData['columnsValuesPair'] . " where id = " . $editId;
                        $stmt2 = $this->companyConnection->prepare($query1);
                        $stmt2->execute();
                    }
                    return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'category has been updated!');

                }
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_maintenance_supplier (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $last_id = $this->companyConnection->lastInsertId();

            }

            for($i=0;$i<count($_POST['sub_category_id']);$i++) {
                $data1['parent_id'] = $last_id;
                $data1['category_id'] = "";
                $data1['parent_id'] = $last_id;
                $data1['supplier'] = $_POST['supplier'];
                $data1['sub_category_id'] = $_POST['sub_category_id'][$i];

                $sqlData = createSqlColVal($data1);
                $query1 = "INSERT INTO company_maintenance_supplier (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($data1);
            }
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record added successfully.');
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }
    public function insertbrand()
    {
        try {
            if(isset($_POST['user_id_hidden']) && $_POST['user_id_hidden'] != ""){
                $editId = $_POST['user_id_hidden'];
            }else{
                $editId = '';
            }
            $data['brand'] = $_POST['brand'];
            $last_id="";
            $data['category_id'] = $_POST['category_id'];
            //$data['parent_id'] = $last_id;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['sub_category_id'] = "";


            $required_array = ['brand'];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                 if($editId != '' ) {
                     $data1['brand'] = $_POST['brand'];
                     $data1['category_id'] = $_POST['category_id'];
                     $data1['updated_at'] = date('Y-m-d H:i:s');

                     $sqlData = createSqlColValPair($data1);
                     $query = "UPDATE company_maintenance_brand SET " . $sqlData['columnsValuesPair'] . " where id = " . $editId;
                     $stmt1 = $this->companyConnection->prepare($query);
                     $stmt1->execute();
                     $testSub = $_POST['sub_category_id'];

                     for($i = 0; $i < count($_POST['sub_category_id']); $i++){
                         $data2['sub_category_id'] = $_POST['sub_category_id'][$i];
                         $sqlData = createSqlColValPair($data2);
                         $query1 = "UPDATE company_maintenance_brand SET " . $sqlData['columnsValuesPair'] . " where id = " . $editId;
                         $stmt2 = $this->companyConnection->prepare($query1);
                         $stmt2->execute();
                     }
                     return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'category has been updated!');

                 }
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO company_maintenance_brand (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $last_id = $this->companyConnection->lastInsertId();

            }

            for($i=0;$i<count($_POST['sub_category_id']);$i++) {
                $data1['parent_id'] = $last_id;
                $data1['category_id'] = "";
                $data1['parent_id'] = $last_id;
                $data1['brand'] = $_POST['brand'];
                $data1['sub_category_id'] = $_POST['sub_category_id'][$i];

                $sqlData = createSqlColVal($data1);
                $query1 = "INSERT INTO company_maintenance_brand (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($data1);
            }
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Record added successfully.');
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }

    /**
     *  function for fetching propertySetup type data
     */
    public function getInventoryData()
    {
        try {
            $data = [];
            $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['property_type_data'] =$this->companyConnection->query("SELECT * FROM company_inventory_preferences WHERE id =".$user_id )->fetch();
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }
        catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function getsubcategoryType(){
        $data = $_POST["cuser_id"];

        $query = $this->companyConnection->query("SELECT * FROM company_maintenance_subcategory WHERE id=".$data." OR parent_id =".$data. " ORDER BY id ASC") ;
        $user = $query->fetchAll();

        echo json_encode($user);
        die();
    }

    public function getsupplier(){



        $query =$this->companyConnection->query("SELECT * FROM company_maintenance_subcategory ");
        $user = $query->fetchAll();

        echo json_encode($user);
        die();
    }
    public function getbrand(){



        $query =$this->companyConnection->query("SELECT * FROM company_maintenance_subcategory ");
        $user = $query->fetchAll();

        echo json_encode($user);
        die();
    }
    public function getsupplierType(){
        $data = $_POST['id'];
        $sql = "SELECT s.*,sb.id as subcatid,sb.category,sb.sub_category FROM company_maintenance_supplier s JOIN company_maintenance_subcategory sb ON s.category_id  = sb.id OR s.sub_category_id = sb.id where s.id =".$data." OR  s.parent_id =".$data;
        $query = $this->companyConnection->query($sql);
        $user = $query->fetchAll();
        $query1 =$this->companyConnection->query("SELECT id,sub_category,parent_id FROM company_maintenance_subcategory");
        $user1 = $query1->fetchAll();
        return ['code'=>200, 'status'=>'success', 'data'=>['selected'=>$user,'all'=>$user1]];

        // echo json_encode($user);

    }
    public function getsupplierType2(){
        $data = $_POST['id'];
        $sql = "SELECT s.*,sb.id as subcatid,sb.category,sb.sub_category FROM company_maintenance_brand s JOIN company_maintenance_subcategory sb ON s.category_id  = sb.id OR s.sub_category_id = sb.id where s.id =".$data." OR  s.parent_id =".$data;
        $query = $this->companyConnection->query($sql);
        $user = $query->fetchAll();
        $query1 =$this->companyConnection->query("SELECT id,sub_category,parent_id FROM company_maintenance_subcategory");
        $user1 = $query1->fetchAll();
        return ['code'=>200, 'status'=>'success', 'data'=>['selected'=>$user,'all'=>$user1]];

        // echo json_encode($user);

    }

    public function getsupplierSub(){

        $user_id=$_POST["id"];
        $sub_cat_id=$_POST["sub_cat_id"];
        $query =$this->companyConnection->query("SELECT * FROM company_maintenance_subcategory where id=".$user_id." OR parent_id=".$user_id);
        $user = $query->fetchAll();
        if ($sub_cat_id == ""){
            $query1 =$this->companyConnection->query("SELECT sub_category_id FROM company_maintenance_supplier");
        }else{
            $query1 =$this->companyConnection->query("SELECT sub_category_id FROM company_maintenance_supplier where parent_id = ". $sub_cat_id);
        }

        $user1 = $query1->fetchAll();


        return ['code'=>200, 'status'=>'success', 'data'=>['all'=>$user,'selected'=>$user1]];
    }

    public function getsupplierSub2(){

        $user_id=$_POST["id"];
        $sub_cat_id = $_POST["sub_cat_id"];

        $query =$this->companyConnection->query("SELECT * FROM company_maintenance_subcategory where id=".$user_id." OR parent_id=".$user_id);
        $user = $query->fetchAll();
        if ($sub_cat_id == ""){
            $query1 =$this->companyConnection->query("SELECT sub_category_id FROM company_maintenance_brand");
        }else{
            $query1 =$this->companyConnection->query("SELECT sub_category_id FROM company_maintenance_brand where parent_id = ". $sub_cat_id);
        }

        $user1 = $query1->fetchAll();


        return ['code'=>200, 'status'=>'success', 'data'=>['all'=>$user,'selected'=>$user1]];
    }
    public function getsupplierBrn(){

        $user_id=$_POST["id"];
        $sub_cat_id=$_POST["sub_cat_id"];

        $query =$this->companyConnection->query("SELECT * FROM company_maintenance_subcategory where id=".$user_id." OR parent_id=".$user_id);
        $user = $query->fetchAll();
        if ($sub_cat_id == ""){
            $query1 =$this->companyConnection->query("SELECT sub_category_id FROM company_maintenance_brand");
        }else{
            $query1 =$this->companyConnection->query("SELECT sub_category_id FROM company_maintenance_brand where parent_id = ". $sub_cat_id);
        }

        $user1 = $query1->fetchAll();


        return ['code'=>200, 'status'=>'success', 'data'=>['all'=>$user,'selected'=>$user1]];
    }


    public function insertvolume(){
        try {

            $data=[];
            if(isset($_POST['user_id_hidden2']) && $_POST['user_id_hidden2'] != ""){
                $editId = $_POST['user_id_hidden2'];
            }else{
                $editId = '';
            }
            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['volume'] = $_POST['volume'];
            $data['created_at'] =  date('Y-m-d H:i:s');
            $data['updated_at'] =  date('Y-m-d H:i:s');


            //Required variable array
            $required_array = ['volume'];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if($editId != '' ){
                    $data1['volume'] = $_POST['volume'];


                    $sqlData = createSqlColValPair($data1);
                    $query = "UPDATE list_of_volumes SET ".$sqlData['columnsValuesPair']." where id = ".$editId;
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                    return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'This record update successfully');
                }

                $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO list_of_volumes (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'This record saved successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function getVolume(){
        $data = $_POST['cuser_id'];
        $query = $this->companyConnection->query("SELECT * FROM list_of_volumes WHERE id='$data'");
        $user = $query->fetch();

        if(!empty($users)){
            foreach ($users as $key=>$value){
                if(!empty($value['date'])) $users[$key]['date'] = dateFormatUser($value['date'], null);
                continue;
            }
        }
        echo json_encode($user);
        die();
    }
    public function deletevolume() {
        try {
            $data=[];
            $apexnewuser_id = $_POST['user_id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE list_of_volumes SET ".$sqlData['columnsValuesPair']." where id='$apexnewuser_id'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User Deleted Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    /*   Reason Tab Starts*/

    public function insertreason(){
        try {


                $data=[];
                if(isset($_POST['user_id_hidden1']) && $_POST['user_id_hidden1'] != ""){
                    $editId = $_POST['user_id_hidden1'];
                }else{
                    $editId = '';
                }


                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['reason'] = $_POST['reason'];
                $data['created_at'] =  date('Y-m-d H:i:s');
                $data['updated_at'] =  date('Y-m-d H:i:s');


                //Required variable array
                $required_array = ['reason'];
                /* Max length variable array */
                $maxlength_array = [];

                //Number variable array
                $number_array = [];
                //Server side validation
                $err_array = [];
                $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
                //Checking server side validation
                if (checkValidationArray($err_array)) {
                    return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
                } else {
                    $dataabc = $_POST['reason'];
                    $duplicateData = $this->companyConnection->query("SELECT * FROM list_of_reason WHERE reason LIKE '$dataabc'")->fetch();
                    if (!empty($duplicateData)) {
                        return array('code' => 600, 'status' => 'success', 'message' => 'Record already exists!');
                    } else {
                        if ($editId != '') {
                            $data1['reason'] = $_POST['reason'];


                            $sqlData = createSqlColValPair($data1);
                            $query = "UPDATE list_of_reason SET " . $sqlData['columnsValuesPair'] . " where id = " . $editId;
                            $stmt1 = $this->companyConnection->prepare($query);
                            $stmt1->execute();
                            return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'reason has been updated!');
                        }

                        $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                        $sqlData = createSqlColVal($data);
                        $query = "INSERT INTO list_of_reason (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data);
                        return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'reason has been Inserted!');
                    }
                }

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function getReason(){
        $data = $_POST['cuser_id'];
        $query = $this->companyConnection->query("SELECT * FROM list_of_reason WHERE id='$data'");
        $user = $query->fetch();

        if(!empty($users)){
            foreach ($users as $key=>$value){
                if(!empty($value['date'])) $users[$key]['date'] = dateFormatUser($value['date'], null);
                continue;
            }
        }
        echo json_encode($user);
        die();
    }
    /* Reason Tab Ends*/

    public function getPropertyListing()
    {
        try{
            $query = $this->companyConnection->query("SELECT * FROM general_property");
            $settings = $query->fetchAll();
            $dataAl1['propertyDetails'] =$settings;

            return array('code' => 200, 'status' => 'success', 'data' => $dataAl1, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function getBuildinglisting()
    {
        try{
            $property_id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT * FROM building_detail WHERE property_id =".$property_id);
            $settings = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $settings, 'message' => 'Record retrieved successfully');

        }catch (Exception $exception){
            echo $exception->getMessage();
            printErrorLog($exception->getMessage());
        }
    }

    public function insertLocation(){
        try {
            $dAtA = $_POST['form_data'];
            //$dataCheck =$_POST['form_data'];
            $data22 = postArray($dAtA);


            $data=[];
            if(isset($data22['user_id_hiddenLoction']) && $data22['user_id_hiddenLoction'] != ""){
                $editId = $data22['user_id_hiddenLoction'];
            }else{
                $editId = '';
            }
            // $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['property_id'] = $data22['subLoc_property'];
            $data['building_id'] = $data22['sub_building'];
            $data['sub_location'] = $data22['sub_location'];
            $data['status'] = '1';
            $data['created_at'] =  date('Y-m-d H:i:s');
            $data['updated_at'] =  date('Y-m-d H:i:s');


            //Required variable array
            $required_array = ['sub_location'];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                if($editId != '' ){
                    $data1['property_id'] = $data22['subLoc_property'];
                    $data1['building_id'] = $data22['sub_building'];
                    $data1['sub_location'] = $data22['sub_location'];
                    $data1['status'] = '1';


                    $sqlData = createSqlColValPair($data1);
                    $query = "UPDATE maintainence_sub_loc SET ".$sqlData['columnsValuesPair']." where id = ".$editId;
                    $stmt1 =$this->companyConnection->prepare($query);
                    $stmt1->execute();
                    return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'Sub Location update successfully');
                }

               // $user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO maintainence_sub_loc (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'Sub Location added successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function deletesubLoc(){
        try {
            $id = $_POST['id'];
            $data = date('Y-m-d H:i:s');
            $record = getSingleRecord($this->companyConnection, ['column' => 'id', 'value' => $id], 'maintainence_sub_loc');

            $sql = "DELETE FROM maintainence_sub_loc WHERE id=" . $id;
            $stmt = $this->companyConnection->prepare($sql);
            $stmt->execute();
            return array(['status' => 'success', 'code' => 200, 'message' => 'Record deleted successfully.']);

        }catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }


    public function updateSublocStatus() {
        try {
            $data=[];
            $manageuser_id = $_POST['user_id'];
            $status_type = $_POST['status_type'];
            if ($status_type == "Deactivate") {
                $status = 0;
            }else{
                $status = 1;
            }
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = $status;
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE maintainence_sub_loc SET ".$sqlData['columnsValuesPair']." where id='$manageuser_id'";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'User '.$status_type.' Successfully');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getSubLoc(){
        $data = $_POST['cuser_id'];
        $query = $this->companyConnection->query("SELECT * FROM maintainence_sub_loc WHERE id='$data'");
        $user = $query->fetch();
        if(!empty($users)){
            foreach ($users as $key=>$value){
                if(!empty($value['date'])) $users[$key]['date'] = dateFormatUser($value['date'], null);
                continue;
            }
        }
        echo json_encode($user);
        die();
    }


}






$inventoryTracker = new MaintenanceInventoryTracker();

