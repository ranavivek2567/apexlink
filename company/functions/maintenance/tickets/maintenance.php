
<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

include(ROOT_URL."/config.php");

include_once( COMPANY_DIRECTORY_URL."/helper/helper.php");
include_once( COMPANY_DIRECTORY_URL."/helper/MigrationCompanySetup.php");

class Maintenance extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];

        echo json_encode($this->$action());

    }

    public function getMaintenanceData() {
        $maintenanceIds= $_POST['maintenanceIds'];
        $maintenanceId = implode($maintenanceIds,',');
        try {

            $stmt = $this->companyConnection->prepare("Select * FROM `tenant_maintenance` WHERE id IN ($maintenanceId)");
            $stmt->execute();
            $data = $stmt->fetchAll();

            $print_complaints_html = '';
            $company_logo = SITE_URL."company/images/logo.png";


            $print_complaints_html .= '<table width="100%" style="margin-bottom: 30px;" align="center" cellspacing="0" cellpadding="0">';
            foreach($data as  $value) {

                $tenant_id = $value['user_id'];
                $property_id = $value['property_id'];
                $unit_id = $value['unit_id'];

                $tenantName = $this->getTenantName($tenant_id);
                $propertyName = $this->getPropertyName($property_id);
                $unitName = $this->getUnitName($unit_id);

                $print_complaints_html .= '<tr>
      <td style="background: #00b0f0; height: 30px;">
        
      </td>
    </tr>
    <tr>
      <td style="border-left: 1px solid #00b0f0; border-right: 1px solid #00b0f0; padding: 0 50px 50px 50px">
    
          <tr>
            <td>Tenant Name: </td>
            <td>' .$tenantName.'</td>
          </tr>
          <tr>
            <td>Unit: </td>
            <td>'.$unitName.'</td>
          </tr>
          <tr>
            <td>Property: </td>
            <td>'.$propertyName.'</td>
          </tr>
          <tr>
            <td>Ticket Number: </td>
            <td>'.$value['ticket_number'].'</td>
          </tr>
      </td>
    </tr>';
            }

            $print_complaints_html .= '</table>';

            echo $print_complaints_html;

            exit;
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function getTenantName($tenant_id)
    {
        $getTenantName =  $this->companyConnection->query("SELECT first_name,last_name FROM users WHERE id ='" . $tenant_id . "'")->fetch();

        return $getTenantName['first_name'].' '.$getTenantName['last_name'];
    }

    public function getPropertyName($property_id)
    {
        $data =  $this->companyConnection->query("SELECT property_name FROM general_property WHERE id ='" . $property_id . "'")->fetch();

        return $data['property_name'];

    }

    public function getUnitName($unit_id)
    {
        $data =  $this->companyConnection->query("SELECT unit_no,unit_prefix FROM unit_details WHERE id ='" . $unit_id . "'")->fetch();

        return $data['unit_prefix'].'-'.$data['unit_no'];
    }

    public function getInitialData()
    {
        $categories =  $this->companyConnection->query("SELECT * FROM maintenance_category")->fetchAll();
        $categoryHtml = "";
        $itemHtml = "";
        $colorHtml = "";

        foreach($categories as $category)
        {
            $catName = $category['category_name'];
            $id = $category['id'];
            $categoryHtml .= "<option value=".$id.">".$catName."</option>";
        }

        $colors = $this->companyConnection->query("SELECT * FROM maintenance_color")->fetchAll();

        $colorHtml .= "<option value=".'0'." checked>Select</option>";
        foreach($colors as $color)
        {
            $colorName = $color['color_name'];
            $id = $color['id'];
            $colorHtml .= "<option value=".$id.">".$colorName."</option>";
        }
        $items = $this->companyConnection->query("SELECT * FROM maintenance_item")->fetchAll();

        foreach($items as $item)
        {
            $itemName = $item['item_name'];
            $id = $item['id'];
            $itemHtml .= "<option value=".$id.">".$itemName." </option>";
        }
        return array('category'=>$categoryHtml,'colors'=>$colorHtml,'items'=>$itemHtml);
    }

    public function getUserData()
    {
        $user_type = $_POST['user_type'];

        $html = '';

        if($user_type==2)
        {
            $getUsers = $this->companyConnection->query("SELECT id,first_name,last_name,email,phone_number FROM users where user_type='$user_type'")->fetchAll();
            $html .= "<table class='table'>";
            $html .= "<tr><th>Tenant Name</th><th>Unit Number</th><th>PropertyID</th><th>Property Name</th></tr>";
            foreach($getUsers as $user)
            {
                $user_id = $user['id'];
                $data['user_id'] = $user['id'];
                $user_name = $user['first_name'].' '.$user['last_name'];
                $email = $user['email'];
                $phone_number = $user['phone_number'];


                $propertyInfo = $this->getPropertyInfo($user_id);
                $property_id = $propertyInfo['property_id'];
                $unit_number = $propertyInfo['unit_prefix'].'-'.$propertyInfo['unit_no'];
                $property_name = $propertyInfo['property_name'];
                $pro_id = $propertyInfo['pro_id'];
                $address = $propertyInfo['address1'];

                $html .= "<tr><td><a href='JavaScript:Void(0);' class='getUserInfo' data-user_id='$user_id' data-user_type='$user_type'>$user_name</a></td><td>$unit_number</td><td>$pro_id</td><td>$property_name </td></tr>";
            }
            $html .= "</table>";

        }
        elseif($user_type==4)
        {
            $getUsers = $this->companyConnection->query("SELECT u.id,u.first_name,u.last_name,u.email,u.phone_number,gp.property_id as pro_id,gp.property_name,gp.address1,op.property_id  FROM users as u inner join owner_property_owned as op on op.user_id = u.id inner join general_property as gp on gp.id=op.property_id")->fetchAll();

            $html .= "<table class='table'>";
            $html .= "<tr><th>Owner Name</th><th>PropertyID</th><th>Property Name</th></tr>";

            foreach($getUsers as $user)
            {

                $user_id = $user['id'];
                $user_name = $user['first_name'].' '.$user['last_name'];
                $email = $user['email'];
                $phone_number = $user['phone_number'];



                $data['property_id'] = $user['property_id'];

                $property_name = $user['property_name'];
                $pro_id = $user['pro_id'];
                $address = $user['address1'];


                $html .= "<tr><td><a href='JavaScript:Void(0);' class='getUserInfo' data-user_id='$user_id' data-user_type='$user_type'>$user_name</a></td><td>$pro_id</td><td>$property_name</td></tr>";
            }
            $html .= "</table>";


        }
        elseif($user_type==3)
        {

            $getUsers = $this->companyConnection->query("SELECT u.id,u.first_name,u.last_name,u.email,u.phone_number,u.address1,vad.vendor_rate,vad.rating,cvt.vendor_type FROM users as u left join vendor_additional_detail as vad on vad.vendor_id = u.id left join company_vendor_type as cvt on cvt.id=vad.vendor_type_id where u.user_type='$user_type'")->fetchAll();
            $html .= "<table class='table'>";
            $html .= "<tr><th>Vendor Name</th><th>Address</th><th>Email</th><th>Vendor Type</th><th>Rate(AFN</th><th>Rating</th></tr>";

            foreach($getUsers as $user)
            {

                $user_id = $user['id'];
                $user_name = $user['first_name'].' '.$user['last_name'];
                $email = $user['email'];
                $address = $user['address1'];
                $vendor_type = $user['vendor_type'];
                $vendor_rate = $user['vendor_rate'];
                $rating = $user['rating'];
                $html .= "<tr><td><a href='JavaScript:Void(0);' class='getUserInfo' data-user_id='$user_id' data-user_type='$user_type'>$user_name</a></td><td>$address</td><td>$email</td><td>$vendor_type</td><td>$vendor_rate</td><td>$rating</td></tr>";

            }
            $html .= "</table>";

        }
        elseif($user_type==1)
        {

            $getUsers = $this->companyConnection->query("SELECT u.id,u.first_name,u.last_name,u.email,u.phone_number,u.role from users as u where u.user_type='$user_type'")->fetchAll();
            $html .= "<table class='table'>";
            $html .= "<tr><th>Name</th><th>Email</th><th>Phone</th><th>Role</th></tr>";
            foreach($getUsers as $user)
            {
                $role = $user['role'];
                if($role==2)
                {
                    $userRole = "Property-Manger";
                }
                else
                {
                    $userRole = "Admin";
                }
                $user_id = $user['id'];
                $user_name = $user['first_name'].' '.$user['last_name'];
                $email = $user['email'];
                $phone = $user['phone_number'];
                $html .= "<tr><td><a href='JavaScript:Void(0);' class='getUserInfo' data-user_id='$user_id' data-user_type='$user_type'>$user_name</td><td>$email</td><td>$phone</td><td>$userRole</td></tr>";

            }
            $html .= "</table>";


        }

        echo $html;
        exit;


    }

    public function getUserInfo()
    {

        $user_id = $_POST['user_id'];
        $user_type = $_POST['user_type'];
        if($user_type==2)
        {
            $user = $this->companyConnection->query("SELECT id,first_name,last_name,email,phone_number FROM users where user_type='$user_type' and id='$user_id'")->fetch();



            $data['user_id'] = $user['id'];
            $data['user_name'] = $user['first_name'].' '.$user['last_name'];
            $data['email']   = $user['email'];
            $data['phone_number']  = $user['phone_number'];


            $propertyInfo = $this->getPropertyInfo($user_id);
            $data['property_id']       = $propertyInfo['property_id'];
            $data['unit_number']     = $propertyInfo['unit_prefix'].'-'.$propertyInfo['unit_no'];
            $data['property_name']    = $propertyInfo['property_name'];
            $data['pro_id']     = $propertyInfo['pro_id'];
            $data['address']     = $propertyInfo['address1'];



        }
        elseif($user_type==4)
        {
            $user = $this->companyConnection->query("SELECT u.id,u.first_name,u.last_name,u.email,u.phone_number, u.address1,gp.property_id as pro_id,gp.property_name,op.property_id  FROM users as u inner join owner_property_owned as op on op.user_id = u.id inner join general_property as gp on gp.id=op.property_id where u.id='$user_id'")->fetch();


            $data['user_id']         = $user['id'];
            $data['user_name']       = $user['first_name'].' '.$user['last_name'];
            $data['email']         = $user['email'];
            $data['phone_number']       = $user['phone_number'];
            $data['property_id'] = $user['property_id'];

            $data['property_name']   = $user['property_name'];
            $data['pro_id']    =    $user['pro_id'];
            $data['address']     = $user['address1'];




        }
        elseif($user_type==3)
        {

            $user = $this->companyConnection->query("SELECT u.id,u.first_name,u.last_name,u.email,u.phone_number,u.address1,vad.vendor_rate,vad.rating,cvt.vendor_type FROM users as u left join vendor_additional_detail as vad on vad.vendor_id = u.id left join company_vendor_type as cvt on cvt.id=vad.vendor_type_id where u.user_type='$user_type' and u.id='$user_id'")->fetch();



            $data['user_id']   = $user['id'];
            $data['user_name'] = $user['first_name'].' '.$user['last_name'];
            $data['email']    = $user['email'];
            $data['address']  = $user['address1'];
            $data['vendor_type']     = $user['vendor_type'];
            $data['vendor_rate']     = $user['vendor_rate'];
            $data['rating']      = $user['rating'];


        }
        elseif($user_type==1)
        {

            $user = $this->companyConnection->query("SELECT u.id,u.first_name,u.last_name,u.email,u.phone_number,u.role from users as u where u.user_type='$user_type' and u.id='$user_id'")->fetch();


            $role = $user['role'];
            if($role==2)
            {
                $userRole = "Property-Manger";
            }
            else
            {
                $userRole = "Admin";
            }
            $data['user_id'] = $user['id'];
            $data['user_name']= $user['first_name'].' '.$user['last_name'];
            $data['email'] = $user['email'];
            $data['phone_number'] = $user['phone_number'];






        }

        return $data;


    }





    public function getPropertyInfo($tenant_id){
        $getPropertyInfo = $this->companyConnection->query("SELECT tp.property_id,tp.unit_id,gp.property_name,gp.property_id as pro_id,gp.address1,ud.unit_prefix,ud.unit_no from tenant_property as tp inner join general_property as gp on gp.id=tp.property_id inner join building_detail as bd on bd.id=tp.building_id inner join unit_details as ud on ud.id=tp.unit_id where tp.user_id = '" . $tenant_id . "'")->fetch();
        return $getPropertyInfo;

    }


    public function addSingleData()
    {
        $value = $_POST['fieldValue'];
        $table = $_POST['table'];
        $column = $_POST['column'];

        $fieldValue = $_POST['fieldValue'];
        $sql= "SELECT * FROM $table WHERE $column ='$fieldValue'";
        $exist =  $this->companyConnection->query($sql)->fetch();

        if(!empty($exist)){
            return array('status'=>'false','message'=>'Already Exists');
        }
        $query = "INSERT INTO $table(`$column`) VALUES ('$fieldValue')";


        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        $id = $this->companyConnection->lastInsertId();
        return array('id'=>$id,'value'=>$value);



    }




    public function addDoubleData()
    {
        $value = $_POST['fieldValue'];
        $table = $_POST['table'];
        $column = $_POST['column'];
        $value2 = $_POST['value2'];
        $column2 = $_POST['column2'];

        $fieldValue = $_POST['fieldValue'];
        $a="";

        $query = "INSERT INTO $table(`$column`,`$column2`, `category`) VALUES ('$fieldValue','$value2','$a')";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        $id = $this->companyConnection->lastInsertId();
        return array('id'=>$id,'value'=>$value);



    }


    public function addtrippleData()
    {
        $value = $_POST['fieldValue'];
        $table = $_POST['table'];
        $column = $_POST['column'];
        $value2 = $_POST['value2'];
        $column2 = $_POST['column2'];
        $value3 = $_POST['value3'];
        $column3 = $_POST['column3'];

        $fieldValue = $_POST['fieldValue'];


        $query = "INSERT INTO $table(`$column`,`$column2`,`$column3`) VALUES ('$fieldValue','$value2','$value3')";


        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        $id = $this->companyConnection->lastInsertId();
        return array('id'=>$id,'value'=>$value);



    }



    public function addquardData()
    {
        $value = $_POST['fieldValue'];
        $table = $_POST['table'];
        $column = $_POST['column'];
        $value2 = $_POST['value2'];
        $column2 = $_POST['column2'];
        $value3 = $_POST['value3'];
        $column3 = $_POST['column3'];
        $value4 = $_POST['value4'];
        $column4 = $_POST['column4'];

        $fieldValue = $_POST['fieldValue'];


        $query = "INSERT INTO $table(`$column`,`$column2`,`$column3`,`$column4`) VALUES ('$fieldValue','$value2','$value3','$value4')";


        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        $id = $this->companyConnection->lastInsertId();
        return array('id'=>$id,'value'=>$value);



    }









    public function addLostFound()
    {
        $data['item_name']  =  $_POST['lost_item_name'];
        $data['item_number']  =  $_POST['item_number'];
        $data['category_id']  =  $_POST['category'];
        $data['description']  =  $_POST['brand_description'];
        $data['brand_name']  =  $_POST['brand_name'];
        $data['color_id']  =  $_POST['color'];
        $data['age_item_id']  =  $_POST['item'];
        $data['serial_number']  =  $_POST['serial_number'];
        $data['lost_location']  =  $_POST['loation_lost'];
        $data['lost_date']  =  $_POST['lost_date'];
        $expiration_date =     date('Y-m-d', strtotime("+3 months", strtotime($data['lost_date'])));
        $data['expiration_date']  =  $expiration_date;

        $data['other_details']  =  $_POST['other_details'];
        $data['user_id']  =  $_POST['user_id'];
        $data['type'] = $_POST['type'];
        $data['property_id'] = $_POST['property_id'];

        $sqlData = createSqlColVal($data);

        $query = "INSERT INTO maintenance_lost_found(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($data);
        $last_id=$this->companyConnection->lastInsertId();

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($_FILES['file_library'])) {
            $countFiles = count($_FILES['file_library']['name']);

            if ($countFiles != 0) {
                $files = $_FILES['file_library'];
                $i = 0;
             //   print_r($_FILES['file_library']); die;
                if (isset($_FILES['file_library'])) {
                    foreach ($files['name'] as $key => $filename) {
                       // print_r($files['name']); die;
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['record_id'] = $last_id;
                            $data1['filename'] = $uniqueName;
                            $data1['type'] = 'LF';


                            $data1['file_location'] = $path . '/' . $uniqueName;

                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;
                            $sqlData1 = createSqlColVal($data1);
                            $query1 = "INSERT INTO maintenance_files(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query1);
                            $stmt1->execute($data1);
                            $tmp_name = $files["tmp_name"][$key];
                            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                //Directory does not exist, so lets create it.
                                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                            }
                            move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                        }
                        $i++;
                    }
                }
            }
        }


        return array('status' => 'success','type'=>$data['type'], 'message' => 'Data added successfully.');




    }



    public function getItemLists()
    {
        $lost = $this->companyConnection->query("Select mlf.id,mlf.created_at,mlf.lost_date,mc.category_name,gp.property_name FROM `maintenance_lost_found` as mlf join general_property as gp on gp.id=mlf.property_id join maintenance_category as mc on mc.id=mlf.category_id WHERE mlf.type = 'L' and mlf.matched_status='False' ORDER BY mlf.updated_at desc")->fetchAll();
        $array1 = [];
        $array2 = [];

        foreach($lost as $lostItems)
        {
            $createdDate = $lostItems['created_at'];

            $time = timeFormat($createdDate,null,$this->companyConnection);

            $data['lost_date'] = dateFormatUser($lostItems['lost_date'], null, $this->companyConnection) . " ".$time;

            $data['id'] =$lostItems['id'];
            $data['category_name'] =$lostItems['category_name'];
            $data['property_name'] = $lostItems['property_name'];
            $array1[] = $data;
        }


        $found = $this->companyConnection->query("Select mlf.id,mlf.created_at,mlf.lost_date,mc.category_name,gp.property_name,mlf.created_at FROM `maintenance_lost_found` as mlf join general_property as gp on gp.id=mlf.property_id join maintenance_category as mc on mc.id=mlf.category_id WHERE mlf.type = 'F' and mlf.matched_status='False'  ORDER BY mlf.updated_at desc")->fetchAll();

        foreach($found as $foundItems)
        {
            $createdDate = $foundItems['created_at'];

            $time = timeFormat($createdDate,null,$this->companyConnection);

            $data1['created_date'] = dateFormatUser($foundItems['lost_date'], null, $this->companyConnection) . " ".$time;
            $data1['id'] =$foundItems['id'];
            $data1['category_name'] =$foundItems['category_name'];
            $data1['property_name'] = $foundItems['property_name'];
            $array2[] = $data1;
        }


        return array('lost'=>$array1,'find'=>$array2);





    }




    public function getProperty()
    {
        $getData = $this->companyConnection->query("SELECT id,property_name FROM general_property")->fetchAll();
        $html = "<option value=''>Select</option>";


        foreach($getData as $data)
        {

            $html .= "<option value='".$data['id']."' data-id='".$data['id']."' data-name='".$data['property_name']."' class='getPropertyId'>".$data['property_name']."</option>";
            //$html .= "<p><a href='JavaScript:Void(0)' data-id=".$data['id']." data-name=".$data['property_name']." class='getPropertyId'>".$data['property_name']."</a></p>";

        }

        echo $html;
        exit;


    }




    public function getItemCounts()
    {

        $today = date('Y-m-d H:i:s');
        $lostCount = $this->companyConnection->prepare("Select count(id) FROM `maintenance_lost_found` WHERE type = 'L' and matched_status='False'");
        $lostCount->execute();
        $lostItemCount =  $lostCount->fetchColumn();


        $findCount = $this->companyConnection->prepare("Select count(id) FROM `maintenance_lost_found` WHERE type = 'F' and matched_status='False'");
        $findCount->execute();
        $findItemCount =  $findCount->fetchColumn();

        $claimedCount = $this->companyConnection->prepare("Select count(id) FROM `maintenance_lost_found` WHERE type = 'F' and matched_status='True'");
        $claimedCount->execute();
        $claimedItemCount =  $claimedCount->fetchColumn();

        $unclaimedCount = $this->companyConnection->prepare("Select count(id) FROM `maintenance_lost_found` WHERE type = 'L' and matched_status='False' and expiration_date < '$today'");
        $unclaimedCount->execute();
        $unclaimedItemCount =  $unclaimedCount->fetchColumn();

        return array('lost'=>$lostItemCount,'find'=>$findItemCount,'claimed'=>$claimedItemCount,'unclaimed'=>$unclaimedItemCount);
    }

    public function getProperties()
    {
        $data =  $this->companyConnection->query("SELECT property_name,id FROM general_property ORDER BY property_name ASC")->fetchAll();
        return array('property'=>$data);
    }

    public function getitemDetails()
    {
        $item_id = $_POST['item_id'];
        $data =  $this->companyConnection->query("SELECT mlf.*,gp.property_name,u.first_name,u.name,u.user_type,u.address1 as useraddress,u.last_name,gp.address1 as user_address,u.email as user_email,u.phone_number as user_phone FROM maintenance_lost_found as mlf join general_property as gp on gp.id=mlf.property_id join users as u on u.id=mlf.user_id WHERE mlf.id ='" . $item_id . "'")->fetch();
        $data["lost_date"] = $data["lost_date"];
        if(!empty($data['first_name']) && ! empty($data['last_name']))
        $data["full_name"] = $data['first_name'].' '.$data['last_name'];

        if($data["user_type"]==1 || $data["user_type"]==3)
        {
            $data["user_address"] = $data['useraddress'];
        }
        return $data;
    }

    public function editLostFound()
    {
        $item_id = $_POST['item_id'];
        unset($_POST['action']);
        unset($_POST['class']);
        $item_number = $_POST['item_number'];

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $_SESSION[SESSION_DOMAIN]['cuser_id'];

        $uploadPath = ROOT_URL . '/company/' . $path;
        if (isset($_FILES['file_library'])) {
            $countFiles = count($_FILES['file_library']['name']);

            if ($countFiles != 0) {
                $files = $_FILES['file_library'];
                $i = 0;
                if (isset($_FILES['file_library'])) {
                    foreach ($files['name'] as $key => $filename) {
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['record_id'] = $item_id;
                            $data1['filename'] = $uniqueName;
                            $data1['type'] = 'LF';

                            $data1['file_location'] = $path . '/' . $uniqueName;

                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;
                            $sqlData1 = createSqlColVal($data1);
                            $query1 = "INSERT INTO maintenance_files(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query1);
                            $stmt1->execute($data1);
                            $tmp_name = $files["tmp_name"][$key];
                            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                //Directory does not exist, so lets create it.
                                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                            }
                            move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                        }
                        $i++;
                    }
                }
            }
        }

        if(isset($_POST['markedAsMatched']))
        {
            if($_POST['matched_date']=='')
            {
                $data['match_date'] =  '';
            }
            else
            {
                $data['match_date'] =  mySqlDateFormat($_POST['matched_date'], null, $this->companyConnection);
            }

            $data['matched_status']='True';

            if(isset($_POST['matchedAsReturn']))
            {
                $data['matched_returned'] = 'True';

                if(isset($_POST['returnDate']) && $_POST['returnDate']!="")
                {
                    $data['return_date'] =  mySqlDateFormat($_POST['returnDate'], null, $this->companyConnection);
                }
                $data['release_method'] = $_POST['releaseMethod'];
            }
            $matchWithF = $_POST['matchWithF'];
            $getData =  $this->companyConnection->query("SELECT item_number,id FROM maintenance_lost_found WHERE type ='F' and item_number='$matchWithF' and matched_status='False'")->fetch();
            if(empty($getData))
            {
                return array('status'=>'false','message'=>'Item Number is not matching with found items');
            }
            else
            {
                $foundItemNumber = $getData['item_number'];
                $foundItemId = $getData['id'];
                $sqlData = createSqlColValPair($data);

                $query = "UPDATE maintenance_lost_found SET " . $sqlData['columnsValuesPair'] ." where id='$item_id'";

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();


                $query2 = "UPDATE maintenance_lost_found SET " . $sqlData['columnsValuesPair'] ." where item_number='$foundItemNumber'";

                $stmt2 = $this->companyConnection->prepare($query2);
                $stmt2->execute();
            }
        }
        //  print_r($_POST); die;
        //  if(!empty($item_id))
        $data3['category_id'] = !empty($_POST['category'])?$_POST['category']:'';
        $data3['age_item_id'] = !empty($_POST['item'])?$_POST['item']:'';
        $data3['color_id'] = !empty($_POST['color'])?$_POST['color']:'';
        $data3['updated_at'] = date('Y-m-d H:i:s');
        if(isset($_POST['editItem']))
        {
            $data3['item_name'] = $_POST['lost_item_name'];

            $data3['description'] = $_POST['brand_description'];
            $data3['brand_name'] = $_POST['brand_name'];
            $data3['user_id'] = $_POST['user_id'];
            $data3['serial_number'] = $_POST['serial_number'];
            $data3['lost_location'] = $_POST['loation_lost'];
            $data3['lost_date'] = $_POST['lost_date'];
            $data3['other_details'] = $_POST['other_details'];
            $data3['property_id'] = $_POST['property_id'];
        }
        $sqlData = createSqlColValPair($data3);

        $query3 = "UPDATE maintenance_lost_found SET " . $sqlData['columnsValuesPair'] ." where id=".$item_id;
        $stmt4 = $this->companyConnection->prepare($query3);
        $stmt4->execute();
       /// print_r($sqlData); die;
        return array('status'=>'success','message'=>'Successfully Updated');
    }

    public function getNotesData($item_id=Null)
    {
        if(isset($_POST['item_id']))
        {

            $item_id = $_POST['item_id'];
        }
        $stmt = $this->companyConnection->prepare("Select lfn.*,mlf.user_id FROM `lost_found_notes` as lfn inner join maintenance_lost_found as mlf on mlf.id= lfn.item_id WHERE lfn.item_id = '$item_id' and lfn.status='1'");
        $stmt->execute();
        $data = $stmt->fetchAll();

        $html = '';

        $html .= "<div class='grid-outer'>";
        $html .= "<table class='table-responsive table' border= '1px'>";
        $html .= "<thead>";
        $html .= "<tr>
      <th width='10%'>Date</th>
      <th width='17%'>User ID</th>
      <th width='15%'>Notes</th>
      <th width='17%'>Action</th>
      </tr>";
        $html .= "</thead>";

        foreach($data as $list)
        {
            $id = $list['id'];
            $date =  dateFormatUser($list['created_at'], null, $this->companyConnection);
            $user_id = $list['user_id'];
            $userName = userName($user_id, $this->companyConnection,'users');
            $notes = $list['notes'];

            $html.="<tr>";
            $html.="<td>".$date."</td>";
            $html.="<td>".$userName."</td>";
            $html.="<td>".$notes."</td>";
            $html.="<td><select data-id=".$id." class='deleteNote form-control'><option value=''>Select</option><option value='delete'>Delete</option></select></td>";
            $html .="</tr>";
        }
        $html .="</table>";
        echo   $html .= "</div>";
        exit;
    }

    public function addItemNote()
    {
        $data['item_id'] = $_POST['item_id'];
        $data['notes'] =   $_POST['note'];
        $data['status'] =  1;
        $sqlData1 = createSqlColVal($data);
        $query1 = "INSERT INTO lost_found_notes(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
        $stmt1 = $this->companyConnection->prepare($query1);
        $stmt1->execute($data);
        $noteData = $this->getNotesData($data['item_id']  );
    }

    public function deleteItemNote()
    {
        $note_id = $_POST['note_id'];
        $item_id = $_POST['item_id'];
        $data['status'] = '0';

        $sqlData = createSqlColValPair($data);
        $query = "UPDATE lost_found_notes SET " . $sqlData['columnsValuesPair'] ." where id=$note_id";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        $noteData = $this->getNotesData($item_id);
    }

    public function importInventoryTracker(){

        if(isset($_FILES['file'])) {
            if(isset($_FILES['file']['name']) && $_FILES['file']['name'] != "") {
                $allowedExtensions = array("xls","xlsx", "csv");
                $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

                if(in_array($ext, $allowedExtensions)) {

                    $file_size = $_FILES['file']['size'] / 1024;

                    if($file_size < 3000) {
                        $file = ROOT_URL."/uploads/".$_FILES['file']['name'];
                        $isUploaded = copy($_FILES['file']['tmp_name'], $file);

                        if($isUploaded) {
                            include(SUPERADMIN_DIRECTORY_URL. "/vendor/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
                            try {
                                $objPHPExcel = PHPExcel_IOFactory::load($file);
                            } catch (Exception $e) {
                                $error_message = 'Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage();
                                return ['status'=>'failed','code'=>503,'data'=>$error_message];
                            }
                            $sheet = $objPHPExcel->getSheet(0);
                            $total_rows = $sheet->getHighestRow();
                            $first_row_header = $objPHPExcel->getActiveSheet()->getCell('A1')->getValue();
                            $total_columns = $sheet->getHighestDataColumn();

                            $login_user_id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                            $extra_columns = ",`user_id`, `status`, `created_at`, `updated_at`";
                            $status = 1;
                            $extra_values = ",'" . $login_user_id . "','" . $status . "','" . date("Y-m-d h:i:s") . "','" . date("Y-m-d h:i:s") . "'";
                            $rowData = array();

                            for ($row = 0; $row <= $total_rows; $row++){
                                //  Read a row of data into an array
                                $rowData[] = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row,
                                    NULL,
                                    TRUE,
                                    FALSE);
                            }
                            $rowRecords = array();

                            $postRecords = array();

                            for($i = 2; $i < count($rowData); $i++){
                                $rowRecords['Property'] = $rowData[$i][0][0];
                                $rowRecords['Building'] = $rowData[$i][0][1];
                                $rowRecords['SubLocation'] = $rowData[$i][0][2];
                                $rowRecords['Category'] = $rowData[$i][0][3];
                                $rowRecords['SubCategory'] = $rowData[$i][0][4];
                                $rowRecords['Supplier'] = $rowData[$i][0][5];
                                $rowRecords['NoOfItems'] = $rowData[$i][0][6];
                                $rowRecords['ItemCodes'] = $rowData[$i][0][7];
                                $postRecords[] = $rowRecords;
                            }

                            $returnArray = [];
                            $ids = [];

                            for($i=2;$i< count($rowData);$i++) {
                                $property=$rowRecords['Property'];

                                $propSql = "SELECT id FROM `general_property` WHERE property_name ='$property'";
                                $query = $this->companyConnection->query($propSql);
                                $prop_val = $query->fetch();

                                if($prop_val === false){
                                    continue;
                                }else{
                                    $ids['property'] = $prop_val['id'];
                                }


                                $building=$rowRecords['Building'];
                                $query = $this->companyConnection->query("SELECT id FROM `building_detail` WHERE building_name ='$building'");
                                $build_val = $query->fetch();
                                if($build_val === false){
                                    continue;
                                }else{
                                    $ids['building'] = $build_val['id'];
                                }

                                $subLocation=$rowRecords['SubLocation'];
                                $query = $this->companyConnection->query("SELECT id FROM `maintenance_inventory_sublocation` WHERE sublocation ='$subLocation'");
                                $sublocation = $query->fetch();
                                if($sublocation === false){
                                    continue;
                                }else{
                                    $ids['sublocation'] = $sublocation['id'];
                                }

                                $categoryName=$rowRecords['Category'];
                                $query = $this->companyConnection->query("SELECT id FROM `company_maintenance_subcategory` WHERE category='$categoryName'");
                                $category_name = $query->fetch();
                                if($category_name === false){
                                    continue;
                                }else{
                                    $ids['category'] = $category_name['id'];
                                }

                                $subcategory=$rowRecords['SubCategory'];
                                $query = $this->companyConnection->query("SELECT id FROM `company_maintenance_subcategory` WHERE sub_category ='$subcategory'");
                                $subCategory = $query->fetch();

                                if($subCategory === false){
                                    continue;
                                }else{
                                    $ids['subCategory'] = $subCategory['id'];
                                }


                                $supplier=$rowRecords['Supplier'];
                                $query = $this->companyConnection->query("SELECT id FROM `company_maintenance_supplier` WHERE supplier ='$supplier'");
                                $supplier = $query->fetch();
                                if($supplier === false){
                                    continue;
                                }else{
                                    $ids['supplier'] = $supplier['id'];
                                }

                                $ids['NoOfItems'] = $rowRecords['NoOfItems'];
                                $ids['ItemCodes'] = $rowRecords['ItemCodes'];
                                $last_id= $this->maintenanceInventory($rowRecords[],'maintenance_inventory');
                                $returnArray[] = $last_id;
                                 echo "<pre>";
                                 print_r($returnArray); die();
//                                return array('code'=>200, 'status'=>'success','data'=>$returnArray, 'message'=>'Records imported successfully!');
                            }
                            try {
                                if (!empty($returnArray)){
                                    return ['status'=>'success','code'=>200,'message'=>'Tenant Imported Successfully!'];
                                }else{
                                    return ['status'=>'failed','code'=>503,'message'=>'Duplicate records found!'];
                                }
                            } catch (Exception $e) {
                                return ['status'=>'failed','code'=>503,'message'=>'Error in importing the data1'];
                                printErrorLog($e->getMessage());
                            }
                        } else {
                            return ['status'=>'failed','code'=>503,'message'=>'File not uploaded!'];
                            //echo '<span class="msg">File not uploaded!</span>';
                        }
                    } else {
                        return ['status'=>'failed','code'=>503,'message'=>'Maximum file size should not cross 3 MB on size! '];
                        //echo '<span class="msg">Maximum file size should not cross 50 KB on size!</span>';
                    }
                } else {
                    return ['status'=>'failed','code'=>503,'message'=>'Only .xls/.xlsx/.csv files are accepted.'];
                    // echo '<span class="msg">This type of file not allowed!</span>';
                }
            } else {
                return ['status'=>'failed','code'=>503,'message'=>'Select an excel file first!'];
                // echo '<span class="msg">Select an excel file first!</span>';
            }
        }
    }
    public function maintenanceInventory($rowRecords, $tableName){
        $rowRecord['user_id'] =  $_SESSION[SESSION_DOMAIN]['cuser_id'];
        $rowRecord['property_id'] = $rowRecords['property'];
        $rowRecord['building_id'] = $rowRecords['building'];
        $rowRecord['sub_location_id'] = $rowRecords['sublocation'];
        $rowRecord['category_id'] = $rowRecords['category'];
        $rowRecord['sub_category_id'] = $rowRecords['subCategory'];
        $rowRecord['supplier_id'] = $rowRecords['supplier'];
        $rowRecord['item_purchased'] = $rowRecords['NoOfItems'];
        $rowRecord['cost_per_item'] = $rowRecords['ItemCodes'];
        $rowRecord['created_at'] = date('Y-m-d H:i:s');
        $rowRecord['updated_at'] = date('Y-m-d H:i:s');
        $sqlData = createSqlColVal($rowRecord);
        $query = "INSERT INTO $tableName (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";

        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute($rowRecord);
        return $this->companyConnection->lastInsertId();
    }

    public function getInventoryData()
    {
        $categories =  $this->companyConnection->query("SELECT * FROM company_maintenance_subcategory where parent_id is NULL ORDER BY category ASC ")->fetchAll();
        $categoryHtml = "";
        $generalProperty = "";
        $brandHtml = "";
        $supplierHtml = "";

        $volumeHtml = "";
        $sublocationHtml = "";


        foreach($categories as $category)
        {
            $catName = $category['category'];
            $id = $category['id'];
            $categoryHtml .= "<option value=".$id.">".$catName."</option>";
        }
        $volumes = $this->companyConnection->query("SELECT * FROM maintenance_inventory_volume")->fetchAll();
        $volumeHtml .= "<option value=''>Select</option>";
        foreach($volumes as $volume)
        {
            $volumeName = $volume['volume'];
            $id = $volume['id'];
            $volumeHtml .= "<option value=".$id.">".$volumeName." </option>";
        }

        $sublocations = $this->companyConnection->query("SELECT * FROM maintenance_inventory_sublocation")->fetchAll();
        $sublocationHtml .= "<option value=''>Select</option>";
        foreach($sublocations as $sublocation)
        {
            $sublocationName = $sublocation['sublocation'];
            $id = $sublocation['id'];
            $sublocationHtml .= "<option value=".$id.">".$sublocationName." </option>";
        }

        $properties = $this->companyConnection->query("SELECT property_name,id FROM general_property")->fetchAll();
        $generalProperty .= "<option value=''>Select</option>";
        foreach($properties as $property)
        {
            $propertyName = $property['property_name'];
            $id = $property['id'];
            $generalProperty .= "<option value=".$id.">".$propertyName." </option>";
        }

        return array('categoryHtml'=>$categoryHtml,'volumeHtml'=>$volumeHtml,'sublocationHtml'=>$sublocationHtml,'property'=>$generalProperty);

    }

    public function getSubcategories()
    {
        $category_id = $_POST['cat_id'];
        $subCategoryHtml = "";
        $subCategories = $this->companyConnection->query("SELECT * FROM company_maintenance_subcategory where parent_id='$category_id' ORDER BY sub_category ASC")->fetchAll();
        $subCategoryHtml .= "<option value=''>Select</option>";
        foreach($subCategories as $subCategory)
        {
            $subCategoryname = $subCategory['sub_category'];
            $id = $subCategory['id'];
            $subCategoryHtml .= "<option value=".$id.">".$subCategoryname."</option>";
        }
        return  $subCategoryHtml;
    }

    public function getBrands()
    {
        $subcategory_id = $_POST['subCat_id'];
        $brandHtml = "";
        $brands = $this->companyConnection->query("SELECT * FROM company_maintenance_brand where sub_category_id='$subcategory_id' ORDER BY brand ASC")->fetchAll();
        $brandHtml .= "<option value=''>Select </option>";
        foreach($brands as $brand)
        {
            $brandname = $brand['brand'];
            $id = $brand['id'];
            $brandHtml .= "<option value=".$id.">".$brandname." </option>";
        }
        return  $brandHtml;
    }

    public function getSuppliers()
    {
        $brand_id = $_POST['brand_id'];
        $supplierHtml = "";
        $suppliers = $this->companyConnection->query("SELECT * FROM company_maintenance_supplier where brand_id='$brand_id' ORDER BY supplier ASC ")->fetchAll();
        $supplierHtml .= "<option value=''>Select</option>";
        foreach($suppliers as $supplier)
        {
            $suppliername = $supplier['supplier'];
            $id = $supplier['id'];
            $supplierHtml .= "<option value=".$id.">".$suppliername." </option>";
        }
        return  $supplierHtml;
    }

    public function getBuilding()
    {
        $property_id = $_POST['property_id'];
        $buildingHtml = "";
        $buildings = $this->companyConnection->query("SELECT building_name,id FROM building_detail where id='$property_id'")->fetchAll();

        foreach($buildings as $building)
        {
            $buildingname = $building['building_name'];
            $id = $building['id'];
            $buildingHtml .= "<option value=".$id.">".$buildingname." </option>";
        }
        return  $buildingHtml;
    }

    public function addInventoryData()
    {
        try{

               if(isset($_POST['item_code']))
                {
                    foreach($_POST['item_code'] as $itmCode)
                    {
                        
                     if($itmCode=="")
                     {
                      return array('code' => 400, 'status' => 'failed', 'message' => "Item Code can not be empty"); 
                     }
                     
                    }
                }


            $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data['image'] =  json_decode($_POST['inventory_image']);
            $data['category_id'] = $_POST['inventoryCategory'];
            $data['sub_category_id'] = $_POST['subcategory'];
            $data['brand_id'] = $_POST['brand'];
            $data['supplier_id'] = $_POST['supplier'];
            $data['description'] = $_POST['description'];
            $data['item_purchased'] = $_POST['nameOfItemPurchased'];
            $data['cost_per_item'] = $_POST['costPerItem'];
            $data['size'] = $_POST['size'];

            $data['purchase_cost'] = $_POST['purchaseCost'];
            $data['purchase_date'] = mySqlDateFormat($_POST['purchaseDate'], null, $this->companyConnection);
            $data['expected_deliver_date'] = mySqlDateFormat($_POST['expectedDeliveryDate'], null, $this->companyConnection);
            $data['stock_reorder_level'] = $_POST['stockRecorderLevel'];
            $data['property_id'] = $_POST['property'];
            $data['building_id'] = $_POST['building'];
            $data['sub_location_id'] = $_POST['inventorySublocation'];
            $data['volume_id'] = $_POST['volume'];

            $custom = isset($_POST['custom_field']) ? $_POST['custom_field'] : [];

            $custom_field = json_decode($_POST['custom_field']);
            $custom_fields = array();
            foreach ($custom_field as $key => $value) {
                # code...
                $customData = (array)$value;

                $custom_fields[] = $customData;
            }

            if (!empty($custom_fields)) {
                foreach ($custom_fields as $key => $value) {
                    if ($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                    if ($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                    continue;
                }
            }

            $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
            $updateColumn = createSqlColValPair($custom_data);
            if (count($custom_fields) > 0) {
                foreach ($custom_fields as $key => $value) {
                    updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                }
            }
            $data['custom_field'] = (count($custom_fields) > 0) ? serialize($custom_fields) : null;

            if($_POST['type']=="add")
            {

                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO maintenance_inventory(" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();

                if(isset($_POST['item_code']))
                {
                    foreach($_POST['item_code'] as $itmCode)
                    {

                        $data1['inventory_id'] = $id;
                        $data1['code'] = $itmCode;
                        $sqlData1 = createSqlColVal($data1);
                        $qry = "INSERT INTO maintenance_inventory_item_code(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";

                        $stmt1 = $this->companyConnection->prepare($qry);

                        $stmt1->execute($data1);
                    }
                }
                return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully');
            }
            else if($_POST['type']=="edit")
            {
                $inventory_id = $_POST['inventory_id'];
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE maintenance_inventory SET " . $sqlData['columnsValuesPair'] ." where id=".$inventory_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully');
            }
        }catch (PDOException $e) {

            return array('code' => 400, 'status' => 'failed', 'message' => $e->getMessage(), 'table' => 'users');
            printErrorLog($e->getMessage());
        }
    }

    public function getInventory()
    {
        $inventory_id = $_POST['inventory_id'];
        $data =  $this->companyConnection->query("SELECT * FROM maintenance_inventory WHERE id ='" . $inventory_id . "'")->fetch();
        $data['purchase_date'] = dateFormatUser($data["purchase_date"],null,$this->companyConnection);
        $data['expected_deliver_date'] = dateFormatUser($data["expected_deliver_date"],null,$this->companyConnection);
        return $data;
    }

    /**
     * Insert data to company amenities table
     * @return array
     */

    public function getPdfContent(){
        try{

            $nameArray = $_POST['dataArray'];
            $nameArray = implode($nameArray,",");
            //SELECT po.po_number,po.required_by,po.vendor,po.invoice_number,po.status,po.owner_approve,po.tenant_approve,p.property_name,b.building_name,ud.unit_prefix,ud.unit_no,wo.work_order_number FROM `purchaseOrder` po JOIN `general_property` p ON po.property = p.property_name JOIN `building_detail` b ON po.building = b.building_name JOIN `unit_details` ud ON po.unit = ud.id JOIN `work_order` wo ON po.work_order= wo.id WHERE po.id IN (10,8)
            $sql = "SELECT * FROM `tenant_maintenance` WHERE id IN ($nameArray)";
            $records = $this->companyConnection->query($sql)->fetchAll();
            $postArray = [];
            foreach ($records as $record){
                $tempArray = [];
                $propertyId = $record['property_id'];
                $propertyName = $this->companyConnection->query("SELECT property_name FROM `general_property` WHERE id = $propertyId")->fetch();
                $tempArray['property'] = $propertyName['property_name'];

                $userId = $record['user_id'];
                $tenantName = $this->companyConnection->query("SELECT name FROM `users` WHERE id = $userId")->fetch();
                $tempArray['tenant_name'] = $tenantName['name'];

                $unitId = $record['unit_id'];
                $unitName = $this->companyConnection->query("SELECT unit_prefix,unit_no FROM `unit_details` WHERE id = $unitId")->fetch();
                $tempArray['unit'] = $unitName['unit_prefix']."-".$unitName['unit_no'];
                $tempArray['ticket_type'] = $record['ticket_type'];
                $tempArray['ticket_number'] = $record['ticket_number'];
                $tempArray['category'] = $record['category'];
                $tempArray['created_at'] = dateFormatUser($record['created_at'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);;
                if($record['status'] == "0"){
                    $tempArray['status'] = 'Cancel';
                }else if ($record['status'] == "1"){
                    $tempArray['status'] = 'Open';
                }
//                $tenant_approve = $record['tenant_approve'];
//                $owner_approve = $record['owner_approve'];
//                $approvers = $this->companyConnection->query("SELECT id,name FROM `users` WHERE id = $tenant_approve OR id= $owner_approve")->fetchAll();
//                $approveNames = [];
//                foreach ($approvers as $approver){
//                    $approveNames[] = $approver['name'];
//                }
//                $approvers = implode($approveNames,",");
//                $tempArray['approvers'] = $approvers;

                $postArray[] = $tempArray;
            }

            $html = $this->createHtml($postArray);
            $pdf = $this->createHTMLToPDF($html);
            $data['record'] = $pdf['data'];
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }

    }

    public function createHtml($postArray){
        $html = '<html>
<head>
	<title></title>
</head>
<body>
	<div style="background-color: #fff">
		<div style="background-color: #40c3f5">
			<div style="background-color: #40c3f5; margin: 20px;text-align: center;">
			    <div style="background-color: #fff;text-align: center; height: 50px;">
				    <img src="" style="width: 50px; margin: 0px auto;">
				 </div>
			</div>
			<div style="background-color: #40c3f5; margin: 20px;">
				<table border="0" style="background-color: #fff;font-size: 12px; width: 100%;">
					<thead>
                        <tr>
                            <th>Tenant Name</th>
                            <th>Unit</th>
                            <th>Property</th>
                            <th>Ticket Number</th>
                            <th>Ticket Type</th>
                            <th>Category</th>
                            <th>Created Date</th>
                            <th>Status</th>
                           
                        </tr>
					</thead>
					<tbody>';
        foreach ($postArray as $post){
            $html .= '<tr>';
            $html .=    '<td>'.$post['tenant_name'].'</td>';
            $html .=    '<td>'.$post['unit'].'</td>';
            $html .=    '<td>'.$post['property'].'</td>';
            $html .=    '<td>'.$post['ticket_number'].'</td>';
            $html .=    '<td>'.$post['ticket_type'].'</td>';
            $html .=    '<td>'.$post['category'].'</td>';
            $html .=    '<td>'.$post['created_at'].'</td>';
            $html .=    '<td>'.$post['status'].'</td>';
            $html .= '</tr>';
        }
        $html .= '</tbody></table>';
        $html .= '<div style="background-color: #40c3f5; height: 20px;"></div>';
        $html .= '</div></div></div></body></html>';
        return $html;
    }

    public function createHTMLToPDF($replaceText){
        $dompdf = new Dompdf();
        $dompdf->loadHtml($replaceText);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $getCompanyId = $this->companyConnection->query("SELECT admindb_id FROM users WHERE id ='1'")->fetch();
        $company_id = $getCompanyId['admindb_id'];
        $path = "uploads/";

        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'/Ticket.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'/Ticket.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record Retrived successfully');
    }



    public function getExcelContent(){
        try{
            $nameArray = $_POST['dataArray'];
            $nameArray = implode($nameArray,",");
            //SELECT po.po_number,po.required_by,po.vendor,po.invoice_number,po.status,po.owner_approve,po.tenant_approve,p.property_name,b.building_name,ud.unit_prefix,ud.unit_no,wo.work_order_number FROM `purchaseOrder` po JOIN `general_property` p ON po.property = p.property_name JOIN `building_detail` b ON po.building = b.building_name JOIN `unit_details` ud ON po.unit = ud.id JOIN `work_order` wo ON po.work_order= wo.id WHERE po.id IN (10,8)
            $sql = "SELECT * FROM `tenant_maintenance` WHERE id IN ($nameArray)";
            $records = $this->companyConnection->query($sql)->fetchAll();
            $postArray = [];
            foreach ($records as $record) {
                $tempArray = [];
                $propertyId = $record['property_id'];
                $propertyName = $this->companyConnection->query("SELECT property_name FROM `general_property` WHERE id = $propertyId")->fetch();
                $tempArray['property'] = $propertyName['property_name'];

                $userId = $record['user_id'];
                $tenantName = $this->companyConnection->query("SELECT name FROM `users` WHERE id = $userId")->fetch();
                $tempArray['tenant_name'] = $tenantName['name'];

                $unitId = $record['unit_id'];
                $unitName = $this->companyConnection->query("SELECT unit_prefix,unit_no FROM `unit_details` WHERE id = $unitId")->fetch();
                $tempArray['unit'] = $unitName['unit_prefix'] . "-" . $unitName['unit_no'];
                $tempArray['ticket_type'] = $record['ticket_type'];
                $tempArray['ticket_number'] = $record['ticket_number'];
                $tempArray['category'] = $record['category'];
                $tempArray['created_at'] = dateFormatUser($record['created_at'], $_SESSION[SESSION_DOMAIN]['cuser_id'], $this->companyConnection);;
                if ($record['status'] == "0") {
                    $tempArray['status'] = 'Cancel';
                } else if ($record['status'] == "1") {
                    $tempArray['status'] = 'Open';
                }

                $postArray[] = $tempArray;

            }

            $html = '<table id="ticketexcel" style="display: none;"><thead>  <tr>
                            <th>Tenant Name</th>
                            <th>Unit</th>
                            <th>Property</th>
                            <th>Ticket Number</th>
                            <th>Ticket Type</th>
                            <th>Category</th>
                            <th>Created Date</th>
                            <th>Status</th>
                           
                        </tr></thead><tbody>';
            foreach ($postArray as $post){
                $html .= '<tr>';
                $html .=    '<td>'.$post['tenant_name'].'</td>';
                $html .=    '<td>'.$post['unit'].'</td>';
                $html .=    '<td>'.$post['property'].'</td>';
                $html .=    '<td>'.$post['ticket_number'].'</td>';
                $html .=    '<td>'.$post['ticket_type'].'</td>';
                $html .=    '<td>'.$post['category'].'</td>';
                $html .=    '<td>'.$post['created_at'].'</td>';
                $html .=    '<td>'.$post['status'].'</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody></table>';
            return ['code'=>200, 'status'=>'success', 'data'=>$html];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }

    }

    public function getAllExcelContent(){
        try{
            $sql = "SELECT mi.description,mi.item_purchased,mi.size,mi.cost_per_item,mi.description,cmp.supplier,cmc.category,cms.sub_category,cmd.brand,gp.property_name,b.building_name,mi.stock_reorder_level,mis.sublocation FROM `maintenance_inventory` as mi join company_maintenance_subcategory as cmc on cmc.id=mi.category_id join company_maintenance_subcategory as cms on cms.id= mi.sub_category_id join company_maintenance_brand as cmd on cmd.id=mi.brand_id join company_maintenance_supplier cmp on cmp.id=mi.supplier_id join maintenance_inventory_volume as miv on miv.id=mi.volume_id join general_property as gp on gp.id=mi.property_id join building_detail as b on b.id=mi.building_id join maintenance_inventory_sublocation as mis on mis.id=mi.sub_location_id";
            $records = $this->companyConnection->query($sql)->fetchAll();

            $postArray = [];
            foreach ($records as $record){
                $tempArray = [];
                $tempArray['property_name']  =  $record['property_name'];
                $tempArray['building_name']  =  $record['building_name'];
                $tempArray['sublocation']  =  $record['sublocation'];
                $tempArray['category']  =  $record['category'];
                $tempArray['sub_category']  =  $record['sub_category'];
                $tempArray['supplier']  =  $record['supplier'];
                $tempArray['item_purchased']  =  $record['item_purchased'];
                $tempArray['cost_per_item']  =  $record['cost_per_item'];
                $postArray[] = $tempArray;
            }
            $html = "";
            $html .= '<table id="ticketexcel"><thead><tr>
                            <th>Property</th>
                            <th>Building</th>
                            <th>SubLocation</th>
                            <th>Category</th>
                            <th>SubCategory</th>
                            <th>Supplier</th>
                            <th>NoOfItems</th>
                            <th>ItemCodes</th>
                        </tr></thead><tbody>';
            foreach ($postArray as $post){
                $html .= '<tr>';
                $html .=    '<td>'.$post['property_name'].'</td>';
                $html .=    '<td>'.$post['building_name'].'</td>';
                $html .=    '<td>'.$post['sublocation'].'</td>';
                $html .=    '<td>'.$post['category'].'</td>';
                $html .=    '<td>'.$post['sub_category'].'</td>';
                $html .=    '<td>'.$post['supplier'].'</td>';
                $html .=    '<td>'.$post['item_purchased'].'</td>';
                $html .=    '<td>'.$post['cost_per_item'].'</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody></table>';
            return ['code'=>200, 'status'=>'success', 'data'=>$html, 'tablename' => "ticketexcel"];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }
}
$UnitTypeAjax = new Maintenance();