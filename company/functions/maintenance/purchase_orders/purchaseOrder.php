<?php

/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;

include(ROOT_URL . "/config.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");
include_once (ROOT_URL . "/company/functions/elasticSearch/CrudFunction.php");

class PurchaseOrder extends DBConnection{

    public function __construct(){
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     * get initial required data
     * @return array
     */
    public function getIntialData(){
        try{
            $data = [];
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $email = $_SESSION[SESSION_DOMAIN]['email'];
            $data['charts'] =$this->companyConnection->query("SELECT id,account_code, account_name FROM company_chart_of_accounts")->fetchAll();
            $data['property'] = $this->companyConnection->query("SELECT id,property_name FROM general_property WHERE status='1' ORDER BY property_name ASC")->fetchAll();
            $data['tenants'] = $this->companyConnection->query("SELECT id,name FROM users WHERE user_type='2' AND record_status='0'")->fetchAll();
            $data['owners'] = $this->companyConnection->query("SELECT id,name FROM users WHERE  user_type='4'")->fetchAll();
            $companyOwners =$this->conn->query("SELECT id,name,email FROM users WHERE user_type='1' ORDER BY name ASC")->fetchAll();
            $ownerOption = "";
            foreach ($companyOwners as $companyOwner){
                $cid = $companyOwner['id'];
                $cemail = $companyOwner['email'];
                $cname = $companyOwner['name'];
                $oId = $companyOwner['id'];
                if($email == $cemail){
                    $ownerOption .= "<option value='".$oId."' selected>".$cname."</option>";
                }else{
                    $ownerOption .= "<option value='".$oId."'>".$cname."</option>";
                }
            }
            $data['created_by'] = $ownerOption;
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    /**
     * get existing order data
     * @return array
     */
    public function getOrderData(){
        try{
            $data = [];
            $orderId = $_POST['orderId'];
            $data['orders'] =$this->companyConnection->query("SELECT * FROM `purchaseOrder` WHERE id=$orderId")->fetch();
            $data['orders']['currentdate'] = dateFormatUser($data['orders']['currentdate'], $id = null,$this->companyConnection);
            $data['orders']['required_by'] = dateFormatUser($data['orders']['required_by'], $id = null,$this->companyConnection);
            $vendorId = $data['orders']['vendorid'];
            $purchaseId = $data['orders']['id'];
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $email = $_SESSION[SESSION_DOMAIN]['email'];
            $data['charts'] =$this->companyConnection->query("SELECT id,account_code, account_name FROM company_chart_of_accounts")->fetchAll();
            $data['property'] = $this->companyConnection->query("SELECT id,property_name FROM general_property WHERE status='1'")->fetchAll();
            $data['tenants'] = $this->companyConnection->query("SELECT id,name FROM users WHERE user_type='2' AND record_status='0'")->fetchAll();
            $data['owners'] = $this->companyConnection->query("SELECT id,name FROM users WHERE  user_type='4'")->fetchAll();
            $dataOrderDetails = $this->companyConnection->query("SELECT * FROM purchaseOrderDetails WHERE  purchase_id=$purchaseId")->fetchAll();

            $orderDetialsRow = "";
            $totalProce = [];
            foreach ($dataOrderDetails as $orderDetaill){
                $orderDetialsRow .= '<tr>';
                $orderDetialsRow .=     '<td><input type="hidden" name="orderId[]" value="'.$orderDetaill['id'].'"><input class="form-control" type="text" name="qty_number[]" value="'.$orderDetaill['qty_number'].'"></td>';
                $orderDetialsRow .=     '<td><select class="form-control" name="gl_account[]">';
                foreach ($data['charts'] as $chart){
                    $fullName = $chart['account_code']."-".$chart['account_name'];
                    if ($orderDetaill['gl_account'] == $chart['id']){
                        $orderDetialsRow .= "<option value='".$chart['id']."' selected>".$fullName."</option>";
                    }else{
                        $orderDetialsRow .= "<option value='".$chart['id']."'>".$fullName."</option>";
                    }
                }
                $orderDetialsRow .=     '</select></td>';
                $orderDetialsRow .=     '<td><input class="form-control" type="text" name="description[]" value="'.$orderDetaill['description'].'"></td>';
                $orderDetialsRow .=     '<td><input class="form-control" type="text" name="item_amount[]" value="'.$orderDetaill['item_amount'].'.00"></td>';
                $orderDetialsRow .=     '<td><input class="form-control" type="text" name="total_amount[]" value="'.$orderDetaill['item_total'].'.00"></td>';
                $orderDetialsRow .=     '<td><a class="minus-icon remove_clone" href="javascript:void(0);" style="color:#000 !important;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a></td>';
                $orderDetialsRow .= '</tr>';
                $totalProce[] = $orderDetaill['item_total'];
            }
            $data['orderDetails'] = $orderDetialsRow;
            $data['totalPrice'] = array_sum($totalProce);
            $companyOwners =$this->conn->query("SELECT id,name,email FROM users WHERE user_type='1'")->fetchAll();
            $ownerOption = "";
            foreach ($companyOwners as $companyOwner){
                $cname = $companyOwner['name'];
                $oId = $companyOwner['id'];
                $ownerOption .= "<option value='".$oId."'>".$cname."</option>";
            }
            $data['created_by'] = $ownerOption;
            $sql = "SELECT u.id,u.name,u.email,u.address1,u.address2,u.address3,u.address4,u.city,u.state,u.zipcode,u.salutation,av.id as aid, cvt.vendor_type, tp.phone_number FROM 
        users u JOIN vendor_additional_detail av ON u.id = av.vendor_id JOIN tenant_phone tp ON u.id = tp.user_id JOIN company_vendor_type cvt ON av.vendor_type_id = cvt.id 
        WHERE u.id=$vendorId";
            $vendorRecord = $this->companyConnection->query($sql)->fetch();

            $data['vendorRecord'] = $vendorRecord;

            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function deleteFile(){
        $id = $_POST['id'];
        $table = $_POST['table'];
        $col = $_POST['col'];
        $sql = "DELETE FROM `$table` WHERE $col = $id";
        $stmt1 = $this->companyConnection->prepare($sql);
        $stmt1->execute();
        return ['code'=>200, 'status'=>'success', 'data'=>'', 'message' => 'Record deleted successfully!'];
    }

    public function changeStatusAjax(){
        $id = $_POST['id'];
        $table = $_POST['table'];
        $col = $_POST['col'];
        $val = $_POST['val'];

        $data1[$col] = $val;
        $data1['updated_at'] = date('Y-m-d H:i:s');
        $sqlData = createSqlColValPair($data1);
        $query = "UPDATE `$table` SET ".$sqlData['columnsValuesPair']." where id='$id'";
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
        return ['code'=>200, 'status'=>'success', 'data'=>'', 'message' => 'Status updated successfully!'];
    }

    public function getPdfContent(){
        try{
            $nameArray = $_POST['dataArray'];
            $nameArray = implode($nameArray,",");
            //SELECT po.po_number,po.required_by,po.vendor,po.invoice_number,po.status,po.owner_approve,po.tenant_approve,p.property_name,b.building_name,ud.unit_prefix,ud.unit_no,wo.work_order_number FROM `purchaseOrder` po JOIN `general_property` p ON po.property = p.property_name JOIN `building_detail` b ON po.building = b.building_name JOIN `unit_details` ud ON po.unit = ud.id JOIN `work_order` wo ON po.work_order= wo.id WHERE po.id IN (10,8)
            $sql = "SELECT * FROM `purchaseOrder` WHERE id IN ($nameArray) ORDER BY id DESC";
            $records = $this->companyConnection->query($sql)->fetchAll();
            $postArray = [];
            foreach ($records as $record){
                $tempArray = [];
                $tempArray['po_number'] = $record['po_number'];
                $propertyId = $record['property'];
                $propertyName = $this->companyConnection->query("SELECT property_name FROM `general_property` WHERE id = $propertyId")->fetch();
                $tempArray['property'] = $propertyName['property_name'];

                $buildingId = $record['building'];
                $buildingName = $this->companyConnection->query("SELECT building_name FROM `building_detail` WHERE id = $buildingId")->fetch();
                $tempArray['building'] = $buildingName['building_name'];

                $unitId = $record['unit'];
                $unitName = $this->companyConnection->query("SELECT unit_prefix,unit_no FROM `unit_details` WHERE id = $unitId")->fetch();
                $tempArray['unit'] = $unitName['unit_prefix']."-".$unitName['unit_no'];
                $tempArray['required_by'] = $record['required_by'];
                $tempArray['vendor'] = $record['vendor'];
                $tempArray['work_order'] = $record['work_order'];
                $tempArray['invoice_number'] = $record['invoice_number'];
                if($record['status'] == "0"){
                    $tempArray['status'] = 'Rejected';
                }else if ($record['status'] == "1"){
                    $tempArray['status'] = 'Created';
                }else if ($record['status'] == "2"){
                    $tempArray['status'] = 'Approved';
                }else{
                    $tempArray['status'] = 'Completed';
                }

                $tenant_approve = $record['tenant_approve'];
                $owner_approve = $record['owner_approve'];
                $approvers = $this->companyConnection->query("SELECT id,name FROM `users` WHERE id = $tenant_approve OR id= $owner_approve")->fetchAll();
                $approveNames = [];
                foreach ($approvers as $approver){
                    $approveNames[] = $approver['name'];
                }
                $approvers = implode($approveNames,",");
                $tempArray['approvers'] = $approvers;

                $postArray[] = $tempArray;
            }

            $html = $this->createHtml($postArray);
            $pdf = $this->createHTMLToPDF($html);
            $data['record'] = $pdf['data'];
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }

    }

    public function getExcelContent(){
        try{
            $nameArray = $_POST['dataArray'];
            $nameArray = implode($nameArray,",");
            //SELECT po.po_number,po.required_by,po.vendor,po.invoice_number,po.status,po.owner_approve,po.tenant_approve,p.property_name,b.building_name,ud.unit_prefix,ud.unit_no,wo.work_order_number FROM `purchaseOrder` po JOIN `general_property` p ON po.property = p.property_name JOIN `building_detail` b ON po.building = b.building_name JOIN `unit_details` ud ON po.unit = ud.id JOIN `work_order` wo ON po.work_order= wo.id WHERE po.id IN (10,8)
            $sql = "SELECT * FROM `purchaseOrder` WHERE id IN ($nameArray)";
            $records = $this->companyConnection->query($sql)->fetchAll();
            $postArray = [];
            foreach ($records as $record){
                $tempArray = [];
                $tempArray['po_number'] = $record['po_number'];
                $propertyId = $record['property'];
                $propertyName = $this->companyConnection->query("SELECT property_name FROM `general_property` WHERE id = $propertyId")->fetch();
                $tempArray['property'] = $propertyName['property_name'];

                $buildingId = $record['building'];
                $buildingName = $this->companyConnection->query("SELECT building_name FROM `building_detail` WHERE id = $buildingId")->fetch();
                $tempArray['building'] = $buildingName['building_name'];

                $unitId = $record['unit'];
                $unitName = $this->companyConnection->query("SELECT unit_prefix,unit_no FROM `unit_details` WHERE id = $unitId")->fetch();
                $tempArray['unit'] = $unitName['unit_prefix']."-".$unitName['unit_no'];
                $tempArray['required_by'] = $record['required_by'];
                $tempArray['vendor'] = $record['vendor'];
                $tempArray['work_order'] = $record['work_order'];
                $tempArray['invoice_number'] = $record['invoice_number'];
                if($record['status'] == "0"){
                    $tempArray['status'] = 'Rejected';
                }else if ($record['status'] == "1"){
                    $tempArray['status'] = 'Created';
                }else if ($record['status'] == "2"){
                    $tempArray['status'] = 'Approved';
                }else{
                    $tempArray['status'] = 'Completed';
                }

                $tenant_approve = $record['tenant_approve'];
                $owner_approve = $record['owner_approve'];
                $approvers = $this->companyConnection->query("SELECT id,name FROM `users` WHERE id = $tenant_approve OR id= $owner_approve")->fetchAll();
                $approveNames = [];
                foreach ($approvers as $approver){
                    $approveNames[] = $approver['name'];
                }
                $approvers = implode($approveNames,",");
                $tempArray['approvers'] = $approvers;

                $postArray[] = $tempArray;
            }

            $html = '<table id="purchaseorderexcel" style="display: none;"><thead><tr><th>PO Number</th><th>Property Name</th><th>Building Name</th>
                            <th>Unit Name</th><th>Required By</th><th>Vendor</th><th>Work Orders</th>
                            <th>Approvers</th><th>Invoice Number</th><th>PO Status</th></tr></thead><tbody>';
            foreach ($postArray as $post){
                $html .= '<tr>';
                $html .=    '<td>'.$post["po_number"].'</td>';
                $html .=    '<td>'.$post["property"].'</td>';
                $html .=    '<td>'.$post["building"].'</td>';
                $html .=    '<td>'.$post["unit"].'</td>';
                $html .=    '<td>'.$post["required_by"].'</td>';
                $html .=    '<td>'.$post["vendor"].'</td>';
                $html .=    '<td>'.$post["work_order"].'</td>';
                $html .=    '<td>'.$post["approvers"].'</td>';
                $html .=    '<td>'.$post["invoice_number"].'</td>';
                $html .=    '<td>'.$post["status"].'</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody></table>';
            return ['code'=>200, 'status'=>'success', 'data'=>$html];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }

    }

    public function getUnits(){
        $query = "SELECT id, unit_prefix, unit_no FROM `unit_details` WHERE property_id = ".$_POST['propertyID']." && building_id = ".$_POST['buildingID']."";
        $unitData = $this->companyConnection->query($query)->fetchAll();
        return array('code' => 200, 'status' => 'success', 'data' => $unitData,'message' => 'Units loaded successfully!');
    }

    public function createHtml($postArray){
        $html = '<html>
<head>
	<title></title>
</head>
<body>
	<div style="background-color: #fff">
		<div style="background-color: #40c3f5">
			<div style="background-color: #40c3f5; margin: 20px;text-align: center;">
			    <div style="background-color: #fff;text-align: center; height: 50px;">
				    <img src="" style="width: 50px; margin: 0px auto;">
				 </div>
			</div>
			<div style="background-color: #40c3f5; margin: 20px;">
				<table border="0" style="background-color: #fff;font-size: 12px; width: 100%;">
					<thead>
                        <tr>
                            <th>PO Number</th>
                            <th>Property Name</th>
                            <th>Building Name</th>
                            <th>Unit Name</th>
                            <th>Required By</th>
                            <th>Vendor</th>
                            <th>Work Orders</th>
                            <th>Approvers</th>
                            <th>Invoice Number</th>
                            <th>PO Status</th>
                        </tr>
					</thead>
					<tbody>';
        foreach ($postArray as $post){
            $html .= '<tr>';
            $html .=    '<td>'.$post["po_number"].'</td>';
            $html .=    '<td>'.$post["property"].'</td>';
            $html .=    '<td>'.$post["building"].'</td>';
            $html .=    '<td>'.$post["unit"].'</td>';
            $html .=    '<td>'.$post["required_by"].'</td>';
            $html .=    '<td>'.$post["vendor"].'</td>';
            $html .=    '<td>'.$post["work_order"].'</td>';
            $html .=    '<td>'.$post["approvers"].'</td>';
            $html .=    '<td>'.$post["invoice_number"].'</td>';
            $html .=    '<td>'.$post["status"].'</td>';
            $html .= '</tr>';
        }
        $html .= '</tbody></table>';
        $html .= '<div style="background-color: #40c3f5; height: 20px;"></div>';
        $html .= '</div></div></div></body></html>';
        return $html;
    }

    public function createHTMLToPDF($replaceText){
        $dompdf = new Dompdf();
        $dompdf->loadHtml($replaceText);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $getCompanyId = $this->companyConnection->query("SELECT admindb_id FROM users WHERE id ='1'")->fetch();
        $company_id = $getCompanyId['admindb_id'];
        $path = "uploads/";
        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'/PurchaseOrder.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        chmod($fileUrl, 0777);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'/PurchaseOrder.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
    }

    /**
     * save purchase order data
     * @return array
     */
    public function savePurchaseOrder(){
        try{
            $data = $_POST;
            $files = $_FILES;
            $required_array = ['po_number'];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $data1['po_number'] = $data['po_number'];
                $data1['created_by'] = $data['created_by'];
                $data1['currentdate'] = mySqlDateFormat($data['current_date'], null,$this->companyConnection);
                $data1['required_by'] = mySqlDateFormat($data['required_by'], null,$this->companyConnection);

                $data1['property'] = $data['property'];
                $data1['building'] = $data['building'];
                $data1['unit'] = $data['unit'];
                $data1['work_order'] = $data['work_order'];
                if (isset($data['tenant_approve']) && $data['tenant_approve'] != ""){
                    $data1['tenant_approve'] = $data['tenant_approve'];
                    $data1['tenants'] = $data['tenants'];
                }
                if (isset($data['owner_approve']) && $data['owner_approve'] != ""){
                    $data1['owner_approve'] = $data['owner_approve'];
                    $data1['owners'] = $data['owners'];
                }

                $data1['invoice_number'] = $data['invoice_number'];
                $data1['vendor'] = $data['vendor'];
                $data1['vendorid'] = (isset($data['vendorid'])?$data['vendorid']:0);
                $data1['address'] = $data['address'];
                $data1['vendor_instruction'] = $data['vendor_instruction'];
                $data1['status'] = '1';
                $data1['created_at'] = date('Y-m-d H:i:s');
                $data1['updated_at'] = date('Y-m-d H:i:s');

                $custom_field = json_decode($data['custom_field']);
                $custom_fields = array();
                foreach ($custom_field as $key => $value) {
                    $customData = (array)$value;
                    $custom_fields[] = $customData;
                }
                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $key => $value) {
                        if ($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                        if ($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                        continue;
                    }
                }
                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_fields) > 0) {
                    foreach ($custom_fields as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }

                $data1['custom_field'] = (count($custom_fields) > 0) ? serialize($custom_fields) : null;

                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO `purchaseOrder` (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $data1['lastId'] = $this->companyConnection->lastInsertId();

                //Add elastic search document
                $purchaseData = $data1;
                $purchaseData['id'] = $this->companyConnection->lastInsertId();
                $purchaseData['approver_name'] = isset($data1['tenant_approve'])?$data1['tenant_approve']:$data1['owner_approve'];
                $ElasticSearchSave = insertDocument('PURCHASE_ORDER','ADD',$purchaseData,$this->companyConnection);

                $response = $this->savePurchaseOrderDetails($data1['lastId'], $data);
                if ($response['status'] == "success"){
                    $data1['detailsIds'] = $response['data'];
                }
                $this->savePurchaseOrderFiles($data1['lastId'],$_FILES);
                return array('code' => 200, 'status' => 'success', 'data' => $data1,'message' => 'Record saved successfully');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function updatePurchaseOrder(){
        try{
            $data = $_POST;
            $files = $_FILES;
            $required_array = ['po_number'];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $data1['po_number'] = $data['po_number'];
                $data1['created_by'] = $data['created_by'];
                $data1['currentdate'] = mySqlDateFormat($data['current_date'], null,$this->companyConnection);
                $data1['required_by'] = mySqlDateFormat($data['required_by'], null,$this->companyConnection);

                $data1['property'] = $data['property'];
                $data1['building'] = $data['building'];
                $data1['unit'] = $data['unit'];
                $data1['work_order'] = $data['work_order'];
                if (isset($data['tenant_approve']) && $data['tenant_approve'] != ""){
                    $data1['tenant_approve'] = $data['tenant_approve'];
                    $data1['tenants'] = $data['tenants'];
                }
                if (isset($data['owner_approve']) && $data['owner_approve'] != ""){
                    $data1['owner_approve'] = $data['owner_approve'];
                    $data1['owners'] = $data['owners'];
                }

                $data1['invoice_number'] = $data['invoice_number'];
                $data1['vendor'] = $data['vendor'];
                $data1['vendorid'] = $data['vendorid'];
                $data1['address'] = $data['address'];
                $data1['vendor_instruction'] = $data['vendor_instruction'];
                $data1['status'] = '1';
                $data1['updated_at'] = date('Y-m-d H:i:s');

                $custom_field = json_decode($data['custom_field']);
                $custom_fields = array();
                foreach ($custom_field as $key => $value) {
                    $customData = (array)$value;
                    $custom_fields[] = $customData;
                }
                if (!empty($custom_fields)) {
                    foreach ($custom_fields as $key => $value) {
                        if ($value['data_type'] == 'date' && !empty($value['default_value'])) $custom_field[$key]['default_value'] = mySqlDateFormat($value['default_value'], null, $this->companyConnection);
                        if ($value['data_type'] == 'date' && !empty($value['value'])) $custom_field[$key]['value'] = mySqlDateFormat($value['value'], null, $this->companyConnection);
                        continue;
                    }
                }
                $custom_data = ['is_deletable' => '0', 'is_editable' => '0'];
                $updateColumn = createSqlColValPair($custom_data);
                if (count($custom_fields) > 0) {
                    foreach ($custom_fields as $key => $value) {
                        updateCustomField($this->companyConnection, $updateColumn, $value['id']);
                    }
                }

                $data1['custom_field'] = (count($custom_fields) > 0) ? serialize($custom_fields) : null;


                $purchaseId = $data['purchaseId'];
                $sqlData = createSqlColValPair($data1);
                $query = "UPDATE `purchaseOrder` SET ".$sqlData['columnsValuesPair']." where id='$purchaseId'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                //Update elastic search document
                $purchaseData = $data1;
                $purchaseData['id'] = $purchaseId;
                $purchaseData['approver_name'] = isset($data1['tenant_approve'])?$data1['tenant_approve']:$data1['owner_approve'];
                $ElasticSearchSave = insertDocument('PURCHASE_ORDER','UPDATE',$purchaseData,$this->companyConnection);

                $response = $this->updatePurchaseOrderDetails($data);
                if ($response['status'] == "success"){
                    $data1['detailsIds'] = $response['data'];
                }
                $this->savePurchaseOrderFiles($purchaseId,$_FILES);
                return array('code' => 200, 'status' => 'success', 'data' => $data1,'message' => 'Record updated successfully');
            }
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }
    }

    public function savePurchaseOrderFiles($recordId, $files){

        $domain = getDomain();
        $adminUser = getSingleRecord($this->conn, ['column' => 'domain_name', 'value' => $domain], 'users');
        $getCompanyId = $this->companyConnection->query("SELECT admindb_id FROM users WHERE id ='1'")->fetch();
        $company_id = $getCompanyId['admindb_id'];
        $path = "uploads/" . 'apexlink_company_' . $adminUser['data']['id'] . '/' . $company_id;
        $uploadPath = ROOT_URL . '/company/' . $path;

        if (isset($files['file_library'])) {
            $countFiles = count($files['file_library']['name']);

            if ($countFiles != 0) {
                $files = $files['file_library'];
                $i = 0;
                if (isset($_FILES['file_library'])) {
                    foreach ($files['name'] as $key => $filename) {
                        $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
                        if (isset($filename[$i]) && $filename[$i] != "") {
                            $randomNumber = uniqid();
                            $uniqueName = $randomNumber . $files['name'][$key];
                            $data1['type'] = 'P';
                            $data1['record_id'] = $recordId;
                            $data1['filename'] = $uniqueName;
                            $data1['file_location'] = $path . '/' . $uniqueName;
                            $data1['file_type'] = strstr($files['type'][$key], "image/") ? 1 : 2;
                            $data1['file_extension'] = $ext;
                            $sqlData1 = createSqlColVal($data1);
                            $query1 = "INSERT INTO `maintenance_files`(" . $sqlData1['columns'] . ") VALUES (" . $sqlData1['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query1);
                            $stmt1->execute($data1);
                            $tmp_name = $files["tmp_name"][$key];
                            if (!is_dir(ROOT_URL . '/company/' . $path)) {
                                /*Directory does not exist, so lets create it.*/
                                mkdir(ROOT_URL . '/company/' . $path, 0777, true);
                            }
                            move_uploaded_file($tmp_name, "$uploadPath/$uniqueName");
                        }
                        $i++;
                    }
                }
            }
        }
    }

    public function savePurchaseOrderDetails($lastId, $data){
        if (isset($data['qty_number']) && !empty($data['qty_number'])){
            $ids = [];
            for ($i = 0; $i < count($data['qty_number']); $i++){
                $data2 = [];
                $data2['purchase_id'] = $lastId;
                $data2['qty_number'] = $data['qty_number'][$i];
                $data2['gl_account'] = $data['gl_account'][$i];
                $data2['description'] = $data['description'][$i];
                $data2['item_amount'] = $data['item_amount'][$i];
                $data2['item_total'] = $data['total_amount'][$i];
                $data2['total'] =  $data['total_amount'][$i];
                $data2['status'] = '1';
                $data2['created_at'] = date('Y-m-d H:i:s');
                $data2['updated_at'] = date('Y-m-d H:i:s');
                $sqlData2 = createSqlColVal($data2);
                $query2 = "INSERT INTO `purchaseOrderDetails` (".$sqlData2['columns'].") VALUES (".$sqlData2['columnsValues'].")";
                $stmt2 = $this->companyConnection->prepare($query2);
                $stmt2->execute($data2);
                $ids[] = $this->companyConnection->lastInsertId();
            }
            return ['code'=>200, 'status'=>'success', 'data'=>$ids];
        }else{
            return ['code'=>404, 'status'=>'error', 'data'=>''];
        }
    }

    public function updatePurchaseOrderDetails($data){
        if (isset($data['qty_number']) && !empty($data['qty_number'])){
            $ids = [];
            for ($i = 0; $i < count($data['qty_number']); $i++){
                $data2 = [];
                $orderId = $data['orderId'][$i];
                $data2['qty_number'] = $data['qty_number'][$i];
                $data2['gl_account'] = $data['gl_account'][$i];
                $data2['description'] = $data['description'][$i];
                $data2['item_amount'] = $data['item_amount'][$i];
                $data2['item_total'] = $data['total_amount'][$i];
                $data2['total'] =  $data['total_amount'][$i];
                $data2['status'] = '1';
                $data2['updated_at'] = date('Y-m-d H:i:s');

                $sqlData = createSqlColValPair($data2);
                $query = "UPDATE `purchaseOrderDetails` SET ".$sqlData['columnsValuesPair']." where id='$orderId'";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                $ids[] = $this->companyConnection->lastInsertId();
            }
            return ['code'=>200, 'status'=>'success', 'data'=>$ids];
        }else{
            return ['code'=>404, 'status'=>'error', 'data'=>''];
        }
    }

    public function getViewPageInformation(){
        $id = $_POST['id'];
        $data = [];
        $query = "SELECT * from `purchaseOrder` WHERE id=$id";
        $data['purchaseOrderData'] = $this->companyConnection->query($query)->fetch();
        $data['purchaseOrderData']['currentdate'] = dateFormatUser($data['purchaseOrderData']['currentdate'], null,$this->companyConnection);
        $data['purchaseOrderData']['required_by'] = dateFormatUser($data['purchaseOrderData']['required_by'], null,$this->companyConnection);
        if ($data['purchaseOrderData']['status'] == '0')
            $data['purchaseOrderData']['status'] = "Rejected";
        else if($data['purchaseOrderData']['status'] == '1')
            $data['purchaseOrderData']['status'] = "Created";
        else if($data['purchaseOrderData']['status'] == '2')
            $data['purchaseOrderData']['status'] = "Approved";
        else
            $data['purchaseOrderData']['status'] = 'Completed';

        $propertyId = $data['purchaseOrderData']['property'];
        $buildingId = $data['purchaseOrderData']['building'];
        $unitId = $data['purchaseOrderData']['unit'];
        $propertyData = $this->companyConnection->query("SELECT property_name from `general_property` WHERE id=$propertyId")->fetch();
        $data['purchaseOrderData']['property'] = $propertyData['property_name'];

        $buildingData = $this->companyConnection->query("SELECT building_name from `building_detail` WHERE id=$buildingId")->fetch();
        $data['purchaseOrderData']['building'] = $buildingData['building_name'];

        $unitData = $this->companyConnection->query("SELECT unit_prefix,unit_no from `unit_details` WHERE id=$unitId")->fetch();
        if ($unitData['unit_prefix'] != "" && $unitData['unit_no'] != ""){
            $data['purchaseOrderData']['unit'] = $unitData['unit_prefix']."-".$unitData['unit_no'];
        }else if($unitData['unit_prefix'] != "" && $unitData['unit_no'] == ""){
            $data['purchaseOrderData']['unit'] = $unitData['unit_prefix'];
        }else if($unitData['unit_prefix'] == "" && $unitData['unit_no'] != ""){
            $data['purchaseOrderData']['unit'] = $unitData['unit_no'];
        }else{
            $data['purchaseOrderData']['unit'] = "";
        }

        $orderDetails = $this->companyConnection->query("SELECT * from `purchaseOrderDetails` WHERE purchase_id=$id")->fetchAll();
        $totalAmt = [];
        $postArray = [];
        $totalProce = [];
        foreach ($orderDetails as $orderDetail){
            $tempArray = [];
            $tempArray['id'] = $orderDetail['id'];
            $tempArray['qty_number'] = $orderDetail['qty_number'];
            $glAccountId = $orderDetail['gl_account'];
            $glAccount = $this->companyConnection->query("SELECT id,account_code, account_name FROM company_chart_of_accounts WHERE id=$glAccountId")->fetch();
            $tempArray['gl_account'] = $glAccount['account_code']."-".$glAccount['account_name'];
            $tempArray['description'] = $orderDetail['description'];
            $tempArray['item_amount'] = $orderDetail['item_amount'];
            $tempArray['item_total'] = $orderDetail['item_total'];
            $totalProce[] = $orderDetail['item_total'];
            $postArray[] = $tempArray;

        }
        $data['totalPrice'] = array_sum($totalProce);
        $data['orderDetails'] = $postArray;

        $vendorId = $data['purchaseOrderData']['vendorid'];
        $sql = "SELECT u.id,u.name,u.email,av.id as aid, cvt.vendor_type, tp.phone_number FROM 
        users u JOIN vendor_additional_detail av ON u.id = av.vendor_id JOIN tenant_phone tp ON u.id = tp.user_id JOIN company_vendor_type cvt ON av.vendor_type_id = cvt.id 
        WHERE u.id=$vendorId";
        $vendorRecord = $this->companyConnection->query($sql)->fetch();
        $data['vendorRecord'] = $vendorRecord;

        return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Purchase Order loaded successfully!');
    }
}
$purchaseOrder = new PurchaseOrder();