<?php
include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class notificationsAjax extends DBConnection {
    public function __construct() {
        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());

    }
    /**
     * function for fetching listing view data
     * @return array
     */
    public function notificationsView () {
        /*$getRoleSettting  = $this->companyConnection->query("SELECT ua.send_to_users FROM user_alerts as ua where ua.alert_name = 'T/Portal Login Request Alert'")->fetch();
        $getAllRollChecked = unserialize($getRoleSettting['send_to_users']);

        if(is_array($getAllRollChecked) && count( $getAllRollChecked ) > 1 ){
            foreach($getAllRollChecked as $key => $value) {
                $getIdsSettting  = $this->companyConnection->query("SELECT u.id FROM users as u where u.role = '$value'")->fetch();
            }
        }else{
            $getIdsSettting  = $this->companyConnection->query("SELECT u.id FROM users as u where u.role = '1'")->fetch();
        }*/
        $currentUser = $_SESSION[SESSION_DOMAIN]['cuser_id'];

        $html = '';
        $link = '';
        $dataselected = $this->companyConnection->query("SELECT * FROM notification_apex where user_id = '$currentUser'")->fetchAll();
        if(is_array($dataselected) && count( $dataselected ) > 1 ) {
            foreach ($dataselected as $key => $value) {

                $html .= "<tr class='clsUserAlert'>";
                $typeM = $value["module_type"];

                if ($typeM == 'GUESTCARD') {
                    $link = '/GuestCard/ListGuestCard';
                } elseif ($typeM == 'TENANT') {
                    $link = '/Tenantlisting/Tenantlisting';
                } elseif ($typeM == 'OWNER') {
                    $link = '/People/Ownerlisting';
                } elseif ($typeM == 'RENTALAPPICATION') {
                    $link = '/RentalApplication/RentalApplications';
                } elseif ($typeM == 'VENDER') {
                    $link = '/Vendor/Vendor';
                } elseif ($typeM == 'LEASE') {
                    $link = '/Lease/ViewEditLease';
                } elseif ($typeM == 'WORKORDER') {
                    $link = '/WorkOrder/WorkOrders';
                } elseif ($typeM == 'PROPERTYINSURANCE') {
                    $link = '/Property/PropertyModules';
                } elseif ($typeM == 'ACCOUNT') {
                    $link = '/Accounting/Accounting';
                } elseif ($typeM == 'COMMUNICATION') {
                    $link = '/Communication/Conversation';
                } else {
                    $link = '/Alert/Notifications';
                }

                $html .= "<td><a href='" . $link . "'>" . $value["module_title"] . "</a></td>";
                $html .= "<td>" . $value["notifi_description"] . "</td>";
                $html .= "</td>";
                $html .= "</tr>";


            }
        }else if (is_array($dataselected) && count( $dataselected ) == 1 ){

            $html .= "<tr class='clsUserAlert'>";
            $typeM = $dataselected[0]["module_type"];

            if ($typeM == 'GUESTCARD') {
                $link = '/GuestCard/ListGuestCard';
            } elseif ($typeM == 'TENANT') {
                $link = '/Tenantlisting/Tenantlisting';
            } elseif ($typeM == 'OWNER') {
                $link = '/People/Ownerlisting';
            } elseif ($typeM == 'RENTALAPPICATION') {
                $link = '/RentalApplication/RentalApplications';
            } elseif ($typeM == 'VENDER') {
                $link = '/Vendor/Vendor';
            } elseif ($typeM == 'LEASE') {
                $link = '/Lease/ViewEditLease';
            } elseif ($typeM == 'WORKORDER') {
                $link = '/WorkOrder/WorkOrders';
            } elseif ($typeM == 'PROPERTYINSURANCE') {
                $link = '/Property/PropertyModules';
            } elseif ($typeM == 'ACCOUNT') {
                $link = '/Accounting/Accounting';
            } elseif ($typeM == 'COMMUNICATION') {
                $link = '/Communication/Conversation';
            } else {
                $link = '/Alert/Notifications';
            }

            $html .= "<td><a href='" . $link . "'>" . $dataselected[0]["module_title"] . "</a></td>";
            $html .= "<td>" . $dataselected[0]["notifi_description"] . "</td>";
            $html .= "</td>";
            $html .= "</tr>";
        }else {
            $html .= "<tr class='clsUserAlert'>";
            $html .= "<td colspan='3' style='text-align: center;'><span>No record found..</span></td>";
            $html .= "</tr>";
        }

        return array('code' => 200,'status'=>'success' ,'html' => $html);

    }

    /**
     * function for fetching listing view data
     * @return array
     */
    public function notificationslist ()
    {
        //dd("reach here");
        $currentUser = $_SESSION[SESSION_DOMAIN]['cuser_id'];
        dd($currentUser);
        $datalistN = $this->companyConnection->query("SELECT * FROM notification_apex ORDER BY id DESC LIMIT 2")->fetchAll();
        return array('code' => 200,'status'=>'success' ,'html' => $datalistN);
    }

}
$notificationsAjax = new notificationsAjax();
?>