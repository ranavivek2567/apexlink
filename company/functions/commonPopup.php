<?php

include(ROOT_URL . "/config.php");
include_once (ROOT_URL . "/company/helper/helper.php");

class commonPopup extends DBConnection {

    public function __construct() {

        parent::__construct();
        $action = $_REQUEST['action'];
        echo json_encode($this->$action());
    }

    /**
     *  function for add pet
     */
    public function addPetPopup() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');
            //Required variable array
            $required_array = ['pet_friendly'];
            /* Max length variable array */
            $maxlength_array = ['pet_friendly' => 150];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field allready exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_pet_friendly', 'pet_friendly', $data['pet_friendly'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Record already exist');
                    }
                }
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO   company_pet_friendly (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addKeyPopup() {
        try {
            $data = $_POST['form'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation        
            //Save Data in Company Database
            $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
            $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_key_access', 'key_code', $data, $id);
            if ($duplicate['is_exists'] == 1) {
                if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                    return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Record already exist');
                }
            }

            $query = "INSERT INTO   company_key_access (`user_id`,`key_code`) VALUES (" . $_SESSION[SESSION_DOMAIN]['cuser_id'] . ",'" . $data . "')";
//                echo $query;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            $id = $this->companyConnection->lastInsertId();
            return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function addAmenityPopup() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');
//            dd($data);
            //Required variable array
            $required_array = ['code', 'name'];
            /* Max length variable array */
            $maxlength_array = ['code' => 10, 'name' => 30];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field allready exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'company_property_amenities', 'name', $data['name'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Name allready exists!');
                    }
                }
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['type'] = "1,2,3";
                $data['status'] = 1;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO   company_property_amenities (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'status' => 'success', 'data' => $stmt, 'lastid' => $id, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function fetchPetdata() {
        $html = '';
        if (!isset($_POST['propertyEditid'])):
            $sql = "SELECT * FROM company_pet_friendly";
            $data = $this->companyConnection->query($sql)->fetchAll();
            $html='<option value="" selected>Select</option>';
            foreach ($data as $d) {
                $html.= '<option value=' . $d["id"] . '>' . $d['pet_friendly'] . '</option>';
            }
        else:
            $propertyEditInfo = "SELECT * FROM general_property WHERE id=" . $_POST['propertyEditid'] . " AND deleted_at IS NULL";
            $getpropertyEditInfo = $this->companyConnection->query($propertyEditInfo)->fetch();
            $sql = "SELECT * FROM company_pet_friendly";
            $data = $this->companyConnection->query($sql)->fetchAll();
            foreach ($data as $d) {
                $html.= '<option value="' . $d['id'] . '" ' . (($getpropertyEditInfo['pet_friendly'] == $d['id']) ? "selected" : "") . '>' . $d['pet_friendly'] . '</option>';
            }
        endif;


        return array('data' => $html, 'status' => 'success');
    }

    public function fetchkeydata() {
        $html = '';
        $sql = "SELECT * FROM company_key_access";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html.= " <option value=''>Select</option>";
        foreach ($data as $d) {
            $html.= '<option value=' . $d['id'] . '>' . $d['key_code'] . '</option>';
        }


        return array('data' => $html, 'status' => 'success');
    }

    public function fetchAllAmenities() {
        $html = '';

        $sql = "SELECT * FROM   company_property_amenities WHERE status = '1' AND deleted_at IS NULL ORDER By name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html .="<div id='selectAllamennity' class='commonCheckboxClass'><input type='checkbox' value='selectall'  class='all'>Select all</div>";
        foreach ($data as $d) {
            $name = $d['name'];
            $idd = $d['id'];
            $html.= "<div class='commonCheckboxClass'><input class='inputAllamenity' type='checkbox' value='$name'  id='$idd' name='amenities[]'> $name </div>";
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function fetchAllAmenitiesProperty() {
        $html = '';

        $sql = "SELECT * FROM   company_property_amenities WHERE status = '1' AND deleted_at IS NULL ORDER By name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html .="<div id='selectAllamennity' class='commonCheckboxClass'><input type='checkbox' value='selectall'  class='all'>Select all</div>";
        foreach ($data as $d) {
            $name = $d['name'];
            $idd = $d['id'];
            $html.= "<div class='commonCheckboxClass'><input class='inputAllamenity' type='checkbox' value='$idd'  id='$idd' name='amenities[]'> $name </div>";
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function fetchAllAmenitiesOwner() {
        $html = '';
        $sql = "SELECT * FROM   company_property_amenities WHERE status = '1' AND deleted_at IS NULL ORDER By name ASC";
        $data = $this->companyConnection->query($sql)->fetchAll();
        foreach ($data as $d) {
            $name = $d['name'];
            $idd = $d['id'];
            $html.= "<div class='col-sm-4 col-md-4' style='min-height:40px; padding: 7px;' class='commonCheckboxClass'><input class='inputAllamenity' type='checkbox' value='$idd'  id='$idd' name='amenities[]'> $name </div>";
        }
        return array('data' => $html, 'status' => 'success');
    }

    public function fetchAllComplaintType() {
        $html = '';
        $sql = "SELECT * FROM complaint_types WHERE status = 1 ORDER BY complaint_type asc ";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html .= " <option value='' selected disabled hidden>Select</option>";
        foreach ($data as $d) {
            $html .= '<option value=' . $d["id"] . '>' . @$d['complaint_type'] . '</option>';
        }

        return array('data' => $html, 'status' => 'success');
    }

    /**
     *  function for add pet
     */
    public function addComplaintType() {
        try {
            $data = $_POST['form'];
            $data = postArray($data, 'true');
            //Required variable array
            $required_array = ['complaint_type'];
            //Max length variable array
            $maxlength_array = ['complaint_type' => 150];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = validation($data, $this->companyConnection, $required_array, $maxlength_array, $number_array);
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                //declaring additional variables
                $id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
                //check if custom field allready exists or not
                $duplicate = checkNameAlreadyExists($this->companyConnection, 'complaint_types', 'complaint_type', $data['complaint_type'], $id);
                if ($duplicate['is_exists'] == 1) {
                    if (empty($duplicate['data']['id']) || $id != $duplicate['data']['id']) {
                        return array('code' => 500, 'status' => 'error', 'data' => $duplicate['data'], 'message' => 'Record already Exist');
                    }
                }
                //Save Data in Company Database
                $data['user_id'] = $_SESSION[SESSION_DOMAIN]['cuser_id'];
                $data['status'] = 1;
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($data);
                $query = "INSERT INTO   complaint_types (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data);
                $id = $this->companyConnection->lastInsertId();
                return array('code' => 200, 'lastid' => $id, 'status' => 'success', 'data' => $stmt, 'message' => 'Record added successfully');
            }
        } catch (PDOException $e) {
            echo json_encode(array('code' => 400, 'status' => 'failed', 'message' => $e));
            return;
        }
    }

    public function savePopUpData() {
        try {
            $column = $_POST['colName'];
            $tableName = $_POST['tableName'];
            $value = $_POST['val'];
            $query = "SELECT * FROM `$tableName` WHERE $column = '$value'";
            $checkIfExist = $this->companyConnection->query($query)->fetch();

            if ($checkIfExist !== false) {
                return array('code' => 504, 'status' => 'error', 'message' => 'Record already exist!');
            }
            $data[$_POST['colName']] = ucfirst($_POST['val']);
            //dd($data);
            $sqlData = createSqlColVal($data);

            $query = "INSERT INTO `$tableName` (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);

            $stmt->execute($data);
            $data1['col_val'] = ucfirst($_POST['val']);
            $data1['last_insert_id'] = $this->companyConnection->lastInsertId();
            return array('code' => 200, 'status' => 'success', 'data' => $data1, 'message' => 'Record added successfully.');
        } catch (Exception $exception) {
            return array('code' => 504, 'status' => 'error', 'message' => $exception->getMessage());
            printErrorLog($exception->getMessage());
        }
    }

    public function getCompanyDefaultData() {
        try {
            $id = $_SESSION[SESSION_DOMAIN]['cuser_id'];
            $data = $this->companyConnection->query("SELECT * FROM default_settings where id=".$id)->fetch();
            return array('status' => 'success', 'code' => 200, 'record' => $data, 'message' => 'Record fetched successfully.');
        } catch (Exception $exception) {
            echo json_encode(['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()]);
            return;
        }
    }

    public function fetchAllCallType() {
        $html = '';
        $sql = "SELECT * FROM call_type ORDER BY call_type asc ";
        $data = $this->companyConnection->query($sql)->fetchAll();
        $html .= " <option value='' selected disabled hidden>Select</option>";
        foreach ($data as $d) {
            $html .= '<option value=' . $d["id"] . '>' . @$d['call_type'] . '</option>';
        }

        return array('data' => $html, 'status' => 'success');
    }



}

$commonPopup = new commonPopup();
?>