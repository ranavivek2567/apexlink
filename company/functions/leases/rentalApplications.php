<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */
ini_set('memory_limit', '1024M'); // or you could use 1G

include(ROOT_URL . "/config.php");
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/lib/html5lib/Parser.php');
require_once ( COMPANY_DIRECTORY_URL.'/library/dompdf/src/Autoloader.php');
Dompdf\Autoloader::register();
use Dompdf\Dompdf;
include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class RenatlApplicationAjax extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
//        print_r($_REQUEST); die();
        echo json_encode($this->$action());

    }
    public function getdetails(){
        try{
            $id=$_POST['id'];
            $query = $this->companyConnection->query("SELECT market_rent,   base_rent,security_deposit FROM unit_details where id=".$id);
            $data = $query->fetch();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function getpropdetails(){
        try{
            $id=$_POST['id'];
            $query = $this->companyConnection->query("SELECT address1,address2,address3,address4,zipcode,city,state FROM general_property where id=".$id);
            $data = $query->fetch();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
// Tenant Detail Starts
    public function getrentalpropdetails(){
        try{

                if (isset($_POST['id']) && $_POST['id'] != ""){
                    $id=$_POST['id'];
                  // print_r($id);
                   $queryrental = $this->companyConnection->query("SELECT id, property_id, property_name, address1, address2, address3, address4, zipcode, country, state, city FROM `general_property` WHERE id ='".$id."'");
                    $data = $queryrental->fetch();
                   // dd($data);

                    return ['status'=>'success','code'=>200, 'data'=> $data];
                }

            } catch (Exception $exception) {
                return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
                printErrorLog($e->getMessage());
            }
    }
    // Tenant Detail Ends
    public function getresonleaving(){
        try{
            $query = $this->companyConnection->query("SELECT id,reasonName FROM reasonmoveout_tenant ");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function saverentalApplicaions()
    {
        try {

            $data = $_POST['formData'];
            $data= postArray($data);
            $formData=$_POST['formData2'];
            $formData=postArray($formData);
            $formData2=$_POST['formData3'];
            $formData2=postArray($formData2);
            $formData3=$_POST['formData4'];
            $formData3=postArray($formData3);
            $formData4=$_POST['formData5'];
            $formData4=postArray($formData4);
            $formData5=$_POST['formData6'];
            $formData5=postArray($formData5);
            $formData6=$_POST['formData7'];
            $formData6=postArray($formData6);
            $notes=$_POST['notes'];
            $notes=postArray($notes);

            /* dplicate validations start */

            $columns = ['users.first_name', 'users.last_name', 'users.middle_name', 'users.dob', 'users.ssn_sin_id'];
            $data12 = [];
            if (isset($data['birthdate']) && $data['birthdate'] != "") {
                $data12 = mySqlDateFormat($data['birthdate'],null,$this->companyConnection);
            }else{
                $data12 ='';
            }
            if (is_array($data['phone_number'])) {
                $dataPhone = $data['phone_number'][0];
            }else{
                $dataPhone = $data['phone_number'];
            }
            $where = [
                ['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $data["first_name"]],
                ['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $data["last_name"]],
                ['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $data["middle_name"]],
                ['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $dataPhone],
                ['table' => 'users', 'column' => 'dob', 'condition' => '=', 'value' => $data12]
            ];
            //dd($data);
            $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, $data['email'],$data['phone_number'],'','','','');
            if(isset($duplicate_record_check)){
                if($duplicate_record_check['code'] == 503){
                    echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                    die();
                }
            }
            /* dplicate validations end */
          /*  echo"<pre>";print_r($notes);die;*/
            $data1['salutation'] = $data['salutation'];
            $data1['first_name'] = $data['first_name'];
            $data1['last_name'] = $data['last_name'];
            $data1['name'] = $data['first_name']." ". $data['last_name'];
            $data1['middle_name'] = $data['middle_name'];
            $data1['nick_name'] = $data['nick_name'];
            $data1['gender'] = $data['gender'];
            $data1['maiden_name'] = $data['maiden_name'];
            $data1['state'] = $data['stateLicence'];
            if(isset($data['ssn_sin_id']) && is_array($data['ssn_sin_id'])){
                foreach($data['ssn_sin_id'] as $key=>$value)
                {
                    if(is_null($value) || $value == '')
                        unset($data['ssn_sin_id'][$key]);
                }
            }
            $data['ssn_sin_id'] = !empty($data['ssn_sin_id'])? serialize($data['ssn_sin_id']):NULL;
            $data1['dob'] = mySqlDateFormat($data['birthdate'], null, $this->companyConnection);
            $data1['referral_source'] = $data['referralSource'];
          //  $data1['email'] = $data['email'];
            $data1['ethnicity'] = $data['ethnicity'];
            $data1['maritial_status'] = $data['martial_status'];
            $data1['veteran_status'] = $data['veteran_status'];
            /*$data1['phone_type'] = $data['other_Name'];
            $data1['phone_type'] = $data['other_ssn'];
            $data1['phone_type'] = $data['other_relation'];*/
            $data1['user_type'] = '10';
            $data1['created_at'] =  date('Y-m-d H:i:s');
            $data1['updated_at'] =  date('Y-m-d H:i:s');
            //Required variable array
            $required_array = [''];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data1,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $sqlData = createSqlColVal($data1);
                $query1 = "INSERT INTO users (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($data1);
                $last_id=$this->companyConnection->lastInsertId();

                $rental['user_id']=$last_id;
                $rental['status']='1';
                $sqlData = createSqlColVal($rental);
                $querys = "INSERT INTO company_rental_applications (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($querys);
                $stmt1->execute($rental);

                if(is_array($data['phone_type'])) {
                    $data4['phone_type'] = $data['phone_type'][0];
                    $data4['country'] = $data['country'][0];
                    $data4['carrier'] = $data['carrier'][0];
                    $data4['phone_number'] = $data['phone_number'][0];
                    $data4['work_phone_extension'] = $data['Extension'][0];
                    $sqlData = createSqlColValPair($data4);
                    $query5 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                    $stmt = $this->companyConnection->prepare($query5);
                    $stmt->execute();
                    for($i=1;$i<count($data['phone_type']);$i++) {
                        $data5['phone_type'] = $data['phone_type'][$i];
                        $data5['country_code'] = $data['country'][$i];
                        $data5['carrier'] = $data['carrier'][$i];
                        $data5['phone_number'] = $data['phone_number'][$i];
                        $data5['work_phone_extension'] = $data['Extension'][$i];
                        $data5['user_id'] = $last_id;
                        $data5['user_type'] = '0';
                        $data5['parent_id'] = '0';
                        $sqlData = createSqlColVal($data5);
                        $query6 = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt1 = $this->companyConnection->prepare($query6);
                        $stmt1->execute($data5);
                    }
                }
                else{
                    $data6['phone_type'] = $data['phone_type'];
                    $data6['country'] = $data['country'];
                    $data6['carrier'] = $data['carrier'];
                    $data6['phone_number'] = $data['phone_number'];
                    $data6['work_phone_extension'] = $data['Extension'];
                    $sqlData = createSqlColValPair($data6);
                    $query7 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                    $stmt = $this->companyConnection->prepare($query7);
                    $stmt->execute();
                }

                if(is_array($data['email'])){

                    for($i=1;$i<count($data['email']);$i++) {
                        $data2['email'] = $data['email'][0];
                        $sqlData = createSqlColValPair($data2);
                        $query2 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query2);
                        $stmt->execute();
                        $n=$i+1;
                        $data3['email'.$n] = $data['email'][$i];
                        $data3['user_id'] = $last_id;
                        $sqlData = createSqlColValPair($data3);
                        $query3 = "UPDATE company_rental_applications SET " . $sqlData['columnsValuesPair'] . " where user_id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query3);
                        $stmt->execute();
                    }
                }
                else{
                    $data2['email'] = $data['email'];
                    $sqlData = createSqlColValPair($data2);
                    $query2 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                    $stmt = $this->companyConnection->prepare($query2);
                    $stmt->execute();
                }
                if(isset($data['guestHobbies'])) {
                    if (is_array($data['guestHobbies'])) {
                        $data8['hobbies'] = $data['guestHobbies'][0];
                        $sqlData = createSqlColValPair($data8);
                        $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" . $last_id;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();
                        for ($i = 1; $i < count($data['guestHobbies']); $i++) {
                            $data9['hobby'] = $data['guestHobbies'][$i];
                            $data9['user_id'] = $last_id;
                            $sqlData = createSqlColVal($data9);
                            $query10 = "INSERT INTO tenant_hobbies (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query10);
                            $stmt1->execute($data9);
                        }
                    }
                }
                updateUsername($last_id,$this->companyConnection);
                $this->rentUnitDetails($last_id,$formData);
                $this->otherOccupants($last_id,$data);
                $this->RenatlHistory($last_id,$formData2);
                $this->EmploymentHistory($last_id,$formData3);
                $this->AdditionalInfo($last_id,$formData4);
                $this->EmergencyDetails($last_id,$formData5);
                $this->RenatlNotes($last_id,$notes);
                $this->GuarantorDetails($last_id,$formData6);

                /*Notification created*/
                $notificationTitle= "New applicant";
                $module_type = "RENTALAPPICATION";
                $alert_type = "Real Time";
                $descriptionNotifi ='A New Applicant has been created: Application number - '.$last_id.'.';
                insertNotification($this->companyConnection,$last_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                /*Notification created end*/
                return array('code' => 200, 'status' => 'success', 'message' => 'Record added successfully.');
            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }
    public function rentUnitDetails($last_id,$formData){
        $datarecord['exp_move_in'] = mySqlDateFormat($formData['expected_moveIn'], null, $this->companyConnection);
        $datarecord['lease_term'] = $formData['request_lease_term'];
        $datarecord['lease_tenure'] = $formData['period_lease'];
        $datarecord['exp_move_out'] = mySqlDateFormat($formData['expected_moveOut'], null, $this->companyConnection);
        $datarecord['prop_id'] = $formData['prop_short'];
        $datarecord['build_id'] = $formData['build_short'];
        $datarecord['unit_id'] = $formData['unit_short'];
        $datarecord['address1'] = $formData['address1'];
        $datarecord['address2'] = $formData['address2'];
        $datarecord['address3'] = $formData['address3'];
        $datarecord['address4'] = $formData['address4'];
        $datarecord['zip_code'] = $formData['postalCode'];
        $datarecord['city'] = $formData['city'];
        $datarecord['state'] = $formData['state'];
        $datarecord['market_rent'] = $formData['marketRent'];
        $datarecord['base_rent'] = $formData['baseRent'];
        $datarecord['sec_deposite'] = $formData['secdeposite'];
        $datarecord['created_at'] =  date('Y-m-d H:i:s');
        $datarecord['updated_at'] =  date('Y-m-d H:i:s');

        $sqlData = createSqlColValPair($datarecord);
        $query = "UPDATE company_rental_applications SET " . $sqlData['columnsValuesPair'] ." where user_id=".$last_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
    }
    public function otherOccupants($last_id,$data){

        if(is_array($data['other_Name'])){

            for($i=0;$i<count($data['other_Name']);$i++) {
                $occupants['first_name'] = $data['other_Name'][$i];
                $occupants['email1'] = $data['other_email'][$i];
                if(isset($data['other_ssn'])&& !empty(($data['other_ssn']))){
                    $occupants['ssn'] = $data['other_ssn'][$i];
                }
                $occupants['relationship'] = $data['other_relation'][$i];
                $occupants['user_id'] = $last_id;
                $occupants['created_at'] = date('Y-m-d H:i:s');
                $occupants['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($occupants);
                $query1 = "INSERT INTO tenant_additional_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($occupants);
            }
        } else{
            $occupants1['first_name'] =$data['other_Name'];
            if(isset($data['other_ssn']) && !empty($data['other_ssn'])){
                $occupants1['ssn'] =$data['other_ssn'];
            }
            if(isset($data['other_email']) && $data['other_email'] != ""){
                $occupants1['email1'] = $data['other_email'];
            }
            $occupants1['relationship'] =$data['other_relation'];
            $occupants1['user_id'] =$last_id;
            $occupants1['created_at'] =  date('Y-m-d H:i:s');
            $occupants1['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($occupants1);
            $query1 = "INSERT INTO tenant_additional_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute($occupants1);

        }
    }
    public function RenatlHistory($last_id,$formData2){
            $renal['current_address'] =$formData2['current_address'];
            $renal['current_zip_code'] =$formData2['current_zip'];
            $renal['current_city'] =$formData2['current_city'];
            $renal['current_state'] =$formData2['current_state'];
            $renal['current_landlord'] =$formData2['current_landlord'];
            $renal['current_landlord_no'] =$formData2['current_landlord_no'];
            $renal['current_salary'] =$formData2['current_salary'];
            if(isset($formData2['current_residence_from'])){
                $renal['current_resided_from'] =mySqlDateFormat($formData2['current_residence_from'], null, $this->companyConnection);
            }
            if(isset($formData2['current_residence_to'])) {
                $renal['current_resided_to'] =mySqlDateFormat($formData2['current_residence_to'], null, $this->companyConnection);
            }
            $renal['current_reason'] =$formData2['current_reason'];
            $renal['previous_address'] =$formData2['previous_address'];
            $renal['previous_zip_code'] =$formData2['previous_zip'];
            $renal['previous_city'] =$formData2['previous_city'];
            $renal['previous_state'] =$formData2['previous_state'];
            $renal['previous_landlord'] =$formData2['previous_landlord'];
            $renal['previous_landlord_no'] =$formData2['previous_landlord_no'];
            $renal['previous_salary'] =$formData2['previous_salary'];
            if(isset($formData2['previous_residing_from'])){
                $renal['previous_resided_from'] =mySqlDateFormat($formData2['previous_residing_from'], null, $this->companyConnection);
            }
            if(isset($formData2['previous_residing_to'])){
                $renal['previous_resided_to'] =mySqlDateFormat($formData2['previous_resided_to'], null, $this->companyConnection);
            }
            $renal['previous_reason'] =$formData2['previous_reason'];
            $renal['user_id'] =$last_id;
            $renal['created_at'] =  date('Y-m-d H:i:s');
            $renal['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($renal);
            $query1 = "INSERT INTO company_rental_history (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute($renal);

    }
    public function EmploymentHistory($last_id,$formData3){
        $employee['current_employer'] = $formData3['current_employer'];
        $employee['current_employee_address'] = $formData3['current_employee_address'];
        $employee['current_employer_zip'] = $formData3['current_employer_zip'];
        $employee['current_employee_city'] = $formData3['current_employee_city'];
        $employee['current_employee_state'] = $formData3['current_employee_state'];
        $employee['current_employee_position'] = $formData3['current_employee_position'];
        $employee['current_employee_salary'] = $formData3['current_employee_salary'];
        $employee['current_employee_from_date'] = mySqlDateFormat($formData3['current_employee_from_date'], null, $this->companyConnection);
        $employee['current_employee_to_date'] =  mySqlDateFormat($formData3['current_employee_to_date'], null, $this->companyConnection);
        $employee['current_employee_no'] = $formData3['current_employee_no'];
        $employee['previous_employer'] = $formData3['previous_employer'];
        $employee['previous_employee_address'] = $formData3['previous_employee_address'];
        $employee['previous_employer_zip'] = $formData3['previous_employer_zip'];
        $employee['previous_employee_city'] = $formData3['previous_employee_city'];
        $employee['previous_employee_state'] = $formData3['previous_employee_state'];
        $employee['previous_employee_position'] = $formData3['previous_employee_position'];
        $employee['previous_employee_salary'] = $formData3['previous_employee_salary'];
        $employee['previous_employee_from_date'] = mySqlDateFormat($formData3['previous_employee_from_date'], null, $this->companyConnection);
        $employee['previous_employee_to_date'] = mySqlDateFormat($formData3['previous_employee_to_date'], null, $this->companyConnection);
        $employee['previous_employee_no'] = $formData3['previous_employee_no'];
        $employee['created_at'] =  date('Y-m-d H:i:s');
        $employee['updated_at'] =  date('Y-m-d H:i:s');
        $employee['user_id'] =  $last_id;
        $sqlData = createSqlColVal($employee);
        $query1 = "INSERT INTO company_employment_history (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
        $stmt1 = $this->companyConnection->prepare($query1);
        $stmt1->execute($employee);

    }
    public function AdditionalInfo($last_id,$formData4){
        if(is_array($formData4['additional_amount'])){
            for($i=0;$i<count($formData4['additional_amount']);$i++){
                if(isset($formData4['additional_amount'][$i]) && !empty($formData4['additional_amount'][$i]))
                {
                    $addinfo['amount'] = $formData4['additional_amount'][$i];
                }
                $addinfo['source_of_income'] = $formData4['additional_income'][$i];
                $addinfo['created_at'] =  date('Y-m-d H:i:s');
                $addinfo['updated_at'] =  date('Y-m-d H:i:s');
                $addinfo['user_id'] =  $last_id;
                $sqlData = createSqlColVal($addinfo);
                $query1 = "INSERT INTO company_annual_income (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($addinfo);
            }
        }
        else{
            if(isset($formData4['additional_amount']) && !empty($formData4['additional_amount']))
            {
                $addinfo['amount'] = $formData4['additional_amount'];
            }
            $addinfo['source_of_income'] = $formData4['additional_income'];
            $addinfo['created_at'] =  date('Y-m-d H:i:s');
            $addinfo['updated_at'] =  date('Y-m-d H:i:s');
            $addinfo['user_id'] =  $last_id;
            $sqlData = createSqlColVal($addinfo);
            $query1 = "INSERT INTO company_annual_income (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute($addinfo);
        }
    }
    public function EmergencyDetails($last_id,$formData5){
        if(is_array($formData5['emergency_contact_name'])){
            for($i=0;$i<count($formData5['emergency_contact_name']);$i++)
            {
                $emerDet['emergency_contact_name'] =  $formData5['emergency_contact_name'][$i];
                $emerDet['emergency_relation'] =  $formData5['emergency_relation'][$i];
                $emerDet['emergency_country_code'] =  $formData5['emergency_country']{$i};
                $emerDet['emergency_phone_number'] =  $formData5['emergency_phone'][$i];
                $emerDet['emergency_email'] =  $formData5['emergency_email'][$i];
                $emerDet['created_at'] =  date('Y-m-d H:i:s');
                $emerDet['updated_at'] =  date('Y-m-d H:i:s');
                $emerDet['user_id'] =  $last_id;
                $sqlData = createSqlColVal($emerDet);
                $query1 = "INSERT INTO emergency_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($emerDet);

            }
        }else{
            $emerDet['emergency_contact_name'] =  $formData5['emergency_contact_name'];
            $emerDet['emergency_relation'] =  $formData5['emergency_relation'];
            $emerDet['emergency_country_code'] =  $formData5['emergency_country'];
            $emerDet['emergency_phone_number'] =  $formData5['emergency_phone'];
            $emerDet['emergency_email'] =  $formData5['emergency_email'];
            $emerDet['created_at'] =  date('Y-m-d H:i:s');
            $emerDet['updated_at'] =  date('Y-m-d H:i:s');
            $emerDet['user_id'] =  $last_id;
            $sqlData = createSqlColVal($emerDet);
            $query1 = "INSERT INTO emergency_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute($emerDet);

            }
        }
    public function RenatlNotes($last_id,$notes){
        if(is_array($notes['chargeNote']))
        {
            for($i=0;$i<count($notes['chargeNote']);$i++){
                $note['notes'] =  $notes['chargeNote'][$i];
                $note['type'] =  '4';
                $note['record_status'] =  '0';
                $note['user_id'] =  $last_id;
                $note['created_at'] =  date('Y-m-d H:i:s');
                $note['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData = createSqlColVal($note);
                $query1 = "INSERT INTO tenant_notes (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt1 = $this->companyConnection->prepare($query1);
                $stmt1->execute($note);

            }
        }else{
            $note['notes'] =  $notes['chargeNote'];
            $note['type'] =  '4';
            $note['record_status'] =  '0';
            $note['user_id'] =  $last_id;
            $note['created_at'] =  date('Y-m-d H:i:s');
            $note['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColVal($note);
            $query1 = "INSERT INTO tenant_notes (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt1 = $this->companyConnection->prepare($query1);
            $stmt1->execute($note);

            }
        }
    public function GuarantorDetails($last_id,$formData6){
                    if(is_array($formData6['salutaion'])){
                        for($j=0;$j<count($formData6['salutaion']);$j++) {
                            $guarantor['salutation'] = $formData6['salutaion'][$j];
                            $guarantor['first_name'] = $formData6['first_name'][$j];
                            $guarantor['middle_name'] = $formData6['middle_name'][$j];
                            $guarantor['last_name'] = $formData6['last_name'][$j];
                            $guarantor['gender'] = $formData6['gender'][$j];
                            $guarantor['relationship'] = $formData6['relationship'][$j];
                            $guarantor['address1'] = $formData6['address1'][$j];
                            $guarantor['address2'] = $formData6['address2'][$j];
                            $guarantor['address3'] = $formData6['address3'][$j];
                            $guarantor['address4'] = $formData6['address4'][$j];
                            $guarantor['zip_code'] = $formData6['zipcode'][$j];
                            $guarantor['city'] = $formData6['emer_city'][$j];
                            $guarantor['state'] = $formData6['state_province'][$j];

                            if (is_array($formData6['email_'.$j])) {
                                $count = count($formData6['email_'.$j]);
                                if ($count == 2) {
                                    $guarantor['email1'] = $formData6['email_'.$j][0];
                                    $guarantor['email2'] = $formData6['email_'.$j][1];
                                } else {
                                    $guarantor['email1'] = $formData6['email_'.$j][0];
                                    $guarantor['email2'] = $formData6['email_'.$j][1];
                                    $guarantor['email3'] = $formData6['email_'.$j][3];
                                }
                            } else {
                                $guarantor['email1'] = $formData6['email_0'];
                            }
                            $guarantor['user_id'] = $last_id;
                            $guarantor['created_at'] = date('Y-m-d H:i:s');
                            $guarantor['updated_at'] = date('Y-m-d H:i:s');
                            $sqlData = createSqlColVal($guarantor);
                            $query1 = "INSERT INTO tenant_guarantor (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query1);
                            $stmt1->execute($guarantor);
                            $last_id_guar = $this->companyConnection->lastInsertId();
                            if (is_array($formData6['phone_type'][$j])) {
                                for ($i = 0; $i < count($formData6['phone_type'][$j]); $i++) {
                                    $guarantor_emer['phone_type'] = $formData6['phone_type'][$i];
                                    if(isset($formData6['carrier'][$i]) && !empty($formData6['carrier'][$i])){
                                        $guarantor_emer['carrier'] = $formData6['carrier'][$i];
                                    }
                                    $guarantor_emer['country_code'] = $formData6['country'][$i];
                                    $guarantor_emer['phone_number'] = $formData6['phone_number'][$i];
                                    $guarantor_emer['user_id'] = $last_id;
                                    $guarantor_emer['parent_id'] = $last_id_guar;
                                    $guarantor_emer['user_type'] = '1';
                                    $sqlData = createSqlColVal($guarantor_emer);
                                    $query1 = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                    $stmt1 = $this->companyConnection->prepare($query1);
                                    $stmt1->execute($guarantor_emer);

                                }
                            } else {
                                $guarantor_emer['phone_type'] = $formData6['phone_type'];
                                if(isset($formData6['carrier']) && !empty($formData6['carrier']))
                                {
                                    $guarantor_emer['carrier'] = $formData6['carrier'];
                                }
                                $guarantor_emer['country_code'] = $formData6['country'];
                                $guarantor_emer['phone_number'] = $formData6['phone_number'];
                                $guarantor_emer['user_id'] = $last_id;
                                $guarantor_emer['parent_id'] = $last_id_guar;
                                $guarantor_emer['user_type'] = '1';
                                $sqlData = createSqlColVal($guarantor_emer);
                                $query1 = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                                $stmt1 = $this->companyConnection->prepare($query1);
                                $stmt1->execute($guarantor_emer);

                            }
                        }

                }else{
                    //$this->guarantorElsePart($formData6, $last_id);
                    $guarantor['salutation'] = $formData6['salutaion'];
                    $guarantor['first_name'] = $formData6['first_name'];
                    $guarantor['middle_name'] = $formData6['middle_name'];
                    $guarantor['last_name'] = $formData6['last_name'];
                    $guarantor['gender'] = $formData6['gender'];
                    $guarantor['relationship'] = $formData6['relationship'];
                    $guarantor['address1'] = $formData6['address1'];
                    $guarantor['address2'] = $formData6['address2'];
                    $guarantor['address3'] = $formData6['address3'];
                    $guarantor['address4'] = $formData6['address4'];
                    $guarantor['zip_code'] = $formData6['zipcode'];
                    $guarantor['city'] = $formData6['emer_city'];
                    $guarantor['state'] = $formData6['state_province'];
                    if (is_array($formData6['email_0'])) {
                        $count=count($formData6['email_0']);
                        if ($count == 2) {
                            $guarantor['email1'] = $formData6['email_0'][0];
                            $guarantor['email2'] = $formData6['email_0'][1];
                        } else {
                            $guarantor['email1'] = $formData6['email_0'][0];
                            $guarantor['email2'] = $formData6['email_0'][1];
                            $guarantor['email3'] = $formData6['email_0'][2];
                        }
                    }else{
                        $guarantor['email1'] = $formData6['email_0'];
                    }
                    $guarantor['user_id'] = $last_id;
                    $guarantor['created_at'] = date('Y-m-d H:i:s');
                    $guarantor['updated_at'] = date('Y-m-d H:i:s');
                    $sqlData = createSqlColVal($guarantor);
                    $query1 = "INSERT INTO tenant_guarantor (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt1 = $this->companyConnection->prepare($query1);
                    $stmt1->execute($guarantor);
                    $last_id_guar=$this->companyConnection->lastInsertId();
                    if(is_array($formData6['phone_type'])){
                        for($i=0;$i<count($formData6['phone_type']);$i++){
                            $guarantor_emer['phone_type'] =  $formData6['phone_type'][$i];
                            if(isset($formData6['carrier'][$i]) && !empty($formData6['carrier'][$i])){
                                $guarantor_emer['carrier'] =  $formData6['carrier'][$i];
                            }
                            $guarantor_emer['country_code'] =  $formData6['country'][$i];
                            $guarantor_emer['phone_number'] =  $formData6['phone_number'][$i];
                            $guarantor_emer['user_id'] =  $last_id;
                            $guarantor_emer['parent_id'] =  $last_id_guar;
                            $guarantor_emer['user_type'] =  '1';
                            $sqlData = createSqlColVal($guarantor_emer);
                            $query1 = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                            $stmt1 = $this->companyConnection->prepare($query1);
                            $stmt1->execute($guarantor_emer);

                        }
                    }else{
                        $guarantor_emer['phone_type'] =  $formData6['phone_type'];
                        if(isset($formData6['carrier']) && !empty($formData6['carrier']))
                        {
                            $guarantor_emer['carrier'] =  $formData6['carrier'];
                        }
                        $guarantor_emer['country_code'] =  $formData6['country'];
                        $guarantor_emer['phone_number'] =  $formData6['phone_number'];
                        $guarantor_emer['user_id'] =  $last_id;
                        $guarantor_emer['parent_id'] =  $last_id_guar;
                        $guarantor_emer['user_type'] =  '1';
                        $sqlData = createSqlColVal($guarantor_emer);
                        $query1 = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                        $stmt1 = $this->companyConnection->prepare($query1);
                        $stmt1->execute($guarantor_emer);

                    }

                }
                           }
    /*Add secttion ends*/
    /*Edit section starts*/
    public function getInitialData(){
        $renatl_id = $_POST['rental_id'];
        try{
            $query = $this->companyConnection->query("SELECT * FROM company_rental_applications WHERE user_id=".$renatl_id);
            $data = $query->fetch();
            $query = $this->companyConnection->query("SELECT * FROM users WHERE id=".$renatl_id);
            $data2 = $query->fetch();
            $query = $this->companyConnection->query("SELECT * FROM tenant_additional_details WHERE user_id=".$renatl_id);
            $data3 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM company_rental_history WHERE user_id=".$renatl_id);
            $data4 = $query->fetch();
            $query = $this->companyConnection->query("SELECT * FROM company_employment_history WHERE user_id=".$renatl_id);
            $data5 = $query->fetch();
            $query = $this->companyConnection->query("SELECT * FROM company_annual_income WHERE user_id=".$renatl_id);
            $data6 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id=".$renatl_id);
            $data7 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_guarantor WHERE user_id=".$renatl_id);
            $data8 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_notes WHERE user_id=".$renatl_id);
            $data9 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE user_type='1' AND user_id=".$renatl_id);
            $data10 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE user_type='0' AND user_id=".$renatl_id);
            $data11 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_ssn_id WHERE user_id=".$renatl_id);
            $data12 = $query->fetchAll();
            //echo"<pre>";print_r($data2);die;
            return ['status'=>'success','code'=>200, 'data1'=> $data,'data2'=>$data2,'data3'=>$data3,'data4'=>$data4,'data5'=>$data5,'data6'=>$data6,'data7'=>$data7,'data8'=>$data8,'data9'=>$data9,'data10'=>$data10,'data11'=>$data11,'data12'=>$data12];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function updaterentalApplicaions()
    {
        try {

            $data = $_POST['formData'];
            $data= postArray($data);
            $formData=$_POST['formData2'];
            $formData=postArray($formData);
            $formData2=$_POST['formData3'];
            $formData2=postArray($formData2);
            $formData3=$_POST['formData4'];
            $formData3=postArray($formData3);
            $formData4=$_POST['formData5'];
            $formData4=postArray($formData4);
            $formData5=$_POST['formData6'];
            $formData5=postArray($formData5);
            $formData6=$_POST['formData7'];
            $formData6=postArray($formData6);
            $notes=$_POST['notes'];
            $notes=postArray($notes);

            /* dplicate validations start */

            $columns = ['users.first_name', 'users.last_name', 'users.middle_name', 'users.dob', 'users.ssn_sin_id'];
            $data12 = [];
            if (isset($data['birthdate']) && $data['birthdate'] != "") {
                $data12 = mySqlDateFormat($data['birthdate'],null,$this->companyConnection);
            }else{
                $data12 ='';
            }
            if (is_array($data['phone_number'])) {
                $dataPhone = $data['phone_number'][0];
            }else{
                $dataPhone = $data['phone_number'];
            }
            $where = [
                ['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $data["first_name"]],
                ['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $data["last_name"]],
                ['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $data["middle_name"]],
                ['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $dataPhone],
                ['table' => 'users', 'column' => 'dob', 'condition' => '=', 'value' => $data12]
            ];
       //     $duplicate_record = checkDuplicateRecord($this->companyConnection, 'users',$columns,$where,$data['ssn_sin_id'], $_POST['rental_id']);
//            if ($duplicate_record['is_exists'] == 1) {
////                return array('code' => 400, 'status' => 'error', 'message' => 'Record already exists!');
////            }

            /* dplicate validations end */
          // dd($_POST['rental_id']);
            $duplicate_record_check=checkMultipleDuplicatesEdit($this->companyConnection, $data['email'],$data['phone_number'],'','','','',$_POST['rental_id']);
            if(isset($duplicate_record_check)){
                if($duplicate_record_check['code'] == 503){
                    echo json_encode(array('code' => 400, 'status' => 'error', 'message' => $duplicate_record_check['message']));
                    die();
                }
            }
            $rental_id=$_POST['rental_id'];
              /*echo"<pre>";print_r($rental_id);die;*/
            $data1['salutation'] = $data['salutation'];
            $data1['first_name'] = $data['first_name'];
            $data1['last_name'] = $data['last_name'];
            $data1['name'] = $data['first_name']." ". $data['last_name'];
            $data1['middle_name'] = $data['middle_name'];
            $data1['nick_name'] = $data['nick_name'];
            $data1['gender'] = $data['gender'];
            $data1['maiden_name'] = $data['maiden_name'];
            $data1['state'] = $data['stateLicence'];
            if(!empty($data['ssn_sin_id'])) {
                foreach ($data['ssn_sin_id'] as $key => $value) {
                    if (is_null($value) || $value == '')
                        unset($data['ssn_sin_id'][$key]);
                }
            }
            $data['ssn_sin_id'] = !empty($data['ssn_sin_id'])? serialize($data['ssn_sin_id']):NULL;
            $data1['dob'] = mySqlDateFormat($data['birthdate'], null, $this->companyConnection);
            $data1['referral_source'] = $data['referralSource'];
            //  $data1['email'] = $data['email'];
            $data1['ethnicity'] = $data['ethnicity'];
            $data1['maritial_status'] = $data['martial_status'];
            $data1['veteran_status'] = $data['veteran_status'];
            /*$data1['phone_type'] = $data['other_Name'];
            $data1['phone_type'] = $data['other_ssn'];
            $data1['phone_type'] = $data['other_relation'];*/
            $data1['user_type'] = '10';
            $data1['created_at'] =  date('Y-m-d H:i:s');
            $data1['updated_at'] =  date('Y-m-d H:i:s');
            //Required variable array
            $required_array = [''];
            /* Max length variable array */
            $maxlength_array = [];

            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data1,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $sqlData = createSqlColValPair($data1);
                $query1 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$rental_id;
                $stmt = $this->companyConnection->prepare($query1);
                $stmt->execute();
                if(is_array($data['phone_type'])) {
                    $data4['phone_type'] = $data['phone_type'][0];
                    $data4['country'] = $data['country'][0];
                    $data4['carrier'] = $data['carrier'][0];
                    $data4['phone_number'] = $data['phone_number'][0];
                    $data4['work_phone_extension'] = $data['Extension'][0];
                    $sqlData = createSqlColValPair($data4);
                    $query5 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$rental_id;
                    $stmt = $this->companyConnection->prepare($query5);
                    $stmt->execute();
                    for($i=1;$i<count($data['phone_type']);$i++) {
                        $data5['phone_type'] = $data['phone_type'][$i];
                        $data5['country_code'] = $data['country'][$i];
                        $data5['carrier'] = $data['carrier'][$i];
                        $data5['phone_number'] = $data['phone_number'][$i];
                        $data5['work_phone_extension'] = $data['Extension'][$i];
                        $data5['user_type'] = '0';
                        $data5['parent_id'] = '0';
                        $sqlData = createSqlColValPair($data5);
                        $query6 = "UPDATE tenant_phone SET " . $sqlData['columnsValuesPair'] . " where id=".$rental_id;
                        $stmt = $this->companyConnection->prepare($query6);
                        $stmt->execute();
                    }
                }
                else{
                    $data6['phone_type'] = $data['phone_type'];
                    $data6['country'] = $data['country'];
                    $data6['carrier'] = $data['carrier'];
                    $data6['phone_number'] = $data['phone_number'];
                    $data6['work_phone_extension'] = $data['Extension'];
                    $sqlData = createSqlColValPair($data6);
                    $query7 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$rental_id;
                    $stmt = $this->companyConnection->prepare($query7);
                    $stmt->execute();
                }

                if(is_array($data['email'])){

                    for($i=1;$i<count($data['email']);$i++) {
                        $data2['email'] = $data['email'][0];
                        $sqlData = createSqlColValPair($data2);
                        $query2 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$rental_id;
                        $stmt = $this->companyConnection->prepare($query2);
                        $stmt->execute();
                        $n=$i+1;
                        $data3['email'.$n] = $data['email'][$i];
                        $sqlData = createSqlColValPair($data3);
                        $query3 = "UPDATE company_rental_applications SET " . $sqlData['columnsValuesPair'] . " where user_id=".$rental_id;
                        $stmt = $this->companyConnection->prepare($query3);
                        $stmt->execute();
                    }
                }
                else{
                    $data2['email'] = $data['email'];
                    $sqlData = createSqlColValPair($data2);
                    $query2 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$rental_id;
                    $stmt = $this->companyConnection->prepare($query2);
                    $stmt->execute();
                }
                if(isset($data['guestHobbies'])) {
                    if (is_array($data['guestHobbies'])) {
                        $data8['hobbies'] = $data['guestHobbies'][0];
                        $sqlData = createSqlColValPair($data8);
                        $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" . $rental_id;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();
                        for ($i = 1; $i < count($data['guestHobbies']); $i++) {
                            $data9['hobby'] = $data['guestHobbies'][$i];
                            $sqlData = createSqlColValPair($data9);
                            $query9 = "UPDATE tenant_hobbies SET " . $sqlData['columnsValuesPair'] . " where id=" . $rental_id;
                            $stmt = $this->companyConnection->prepare($query9);
                            $stmt->execute();
                        }
                    }
                }
                updateUsername($rental_id,$this->companyConnection);
                $this->updaterentUnitDetails($rental_id,$formData);
                $this->updateotherOccupants($rental_id,$data);
                $this->updateRenatlHistory($rental_id,$formData2);
                $this->updateEmploymentHistory($rental_id,$formData3);
                $this->updateAdditionalInfo($rental_id,$formData4);
                $this->updateEmergencyDetails($rental_id,$formData5);
                $this->updateRenatlNotes($rental_id,$notes);
                $this->updateGuarantorDetails($rental_id,$formData6);
                return array('code' => 200, 'status' => 'success', 'message' => 'Record updated successfully.');
            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }
    public function updaterentUnitDetails($rental_id,$formData){
        $datarecord['exp_move_in'] = mySqlDateFormat($formData['expected_moveIn'], null, $this->companyConnection);
        $datarecord['lease_term'] = $formData['request_lease_term'];
        $datarecord['lease_tenure'] = $formData['period_lease'];
        $datarecord['exp_move_out'] = mySqlDateFormat($formData['expected_moveOut'], null, $this->companyConnection);
        $datarecord['prop_id'] = $formData['prop_short'];
        $datarecord['build_id'] = $formData['build_short'];
        $datarecord['unit_id'] = $formData['unit_short'];
        $datarecord['address1'] = $formData['address1'];
        $datarecord['address2'] = $formData['address2'];
        $datarecord['address3'] = $formData['address3'];
        $datarecord['address4'] = $formData['address4'];
        $datarecord['zip_code'] = $formData['postalCode'];
        $datarecord['city'] = $formData['city'];
        $datarecord['state'] = $formData['state'];
        $datarecord['market_rent'] = $formData['marketRent'];
        $datarecord['base_rent'] = $formData['baseRent'];
        $datarecord['sec_deposite'] = $formData['secdeposite'];
        $datarecord['created_at'] =  date('Y-m-d H:i:s');
        $datarecord['updated_at'] =  date('Y-m-d H:i:s');

        $sqlData = createSqlColValPair($datarecord);
        $query = "UPDATE company_rental_applications SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
    }
    public function updateotherOccupants($rental_id,$data){

        if(is_array($data['other_Name'])){
            for($i=0;$i<count($data['other_Name']);$i++) {
                $occupants['first_name'] = $data['other_Name'][$i];
                $occupants['email1'] = $data['other_email'][$i];
                if(isset($data['other_ssn'])&& !empty(($data['other_ssn']))){
                    $occupants['ssn'] = $data['other_ssn'][$i];
                }
                $occupants['relationship'] = $data['other_relation'][$i];
                $occupants['created_at'] = date('Y-m-d H:i:s');
                $occupants['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($occupants);
                $query = "UPDATE tenant_additional_details SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

            }
        } else{
            $occupants1['first_name'] =$data['other_Name'];
            $occupants1['email1'] = $data['other_email'];
            if(isset($data['other_ssn']) && !empty($data['other_ssn'])){
                $occupants1['ssn'] =$data['other_ssn'];
            }
            $occupants1['relationship'] =$data['other_relation'];
            $occupants1['created_at'] =  date('Y-m-d H:i:s');
            $occupants1['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($occupants1);
            $query = "UPDATE tenant_additional_details SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
        }
    }
    public function updateRenatlHistory($rental_id,$formData2){
        $renal['current_address'] =$formData2['current_address'];
        $renal['current_zip_code'] =$formData2['current_zip'];
        $renal['current_city'] =$formData2['current_city'];
        $renal['current_state'] =$formData2['current_state'];
        $renal['current_landlord'] =$formData2['current_landlord'];
        $renal['current_landlord_no'] =$formData2['current_landlord_no'];
        $renal['current_salary'] =$formData2['current_salary'];
        if(isset($formData2['current_residence_from'])){
            $renal['current_resided_from'] =mySqlDateFormat($formData2['current_residence_from'], null, $this->companyConnection);
        }
        if(isset($formData2['current_residence_to'])) {
            $renal['current_resided_to'] =mySqlDateFormat($formData2['current_residence_to'], null, $this->companyConnection);
        }
        $renal['current_reason'] =$formData2['current_reason'];
        $renal['previous_address'] =$formData2['previous_address'];
        $renal['previous_zip_code'] =$formData2['previous_zip'];
        $renal['previous_city'] =$formData2['previous_city'];
        $renal['previous_state'] =$formData2['previous_state'];
        $renal['previous_landlord'] =$formData2['previous_landlord'];
        $renal['previous_landlord_no'] =$formData2['previous_landlord_no'];
        $renal['previous_salary'] =$formData2['previous_salary'];
        if(isset($formData2['previous_residing_from'])){
            $renal['previous_resided_from'] =mySqlDateFormat($formData2['previous_residing_from'], null, $this->companyConnection);
        }
        if(isset($formData2['previous_residing_to'])){
            $renal['previous_resided_to'] =mySqlDateFormat($formData2['previous_resided_to'], null, $this->companyConnection);
        }
        $renal['previous_reason'] =$formData2['previous_reason'];
        $renal['created_at'] =  date('Y-m-d H:i:s');
        $renal['updated_at'] =  date('Y-m-d H:i:s');
        $sqlData = createSqlColValPair($renal);
        $query = "UPDATE company_rental_history SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
    }

    public function updateEmploymentHistory($rental_id,$formData3){
        $employee['current_employer'] = $formData3['current_employer'];
        $employee['current_employee_address'] = $formData3['current_employee_address'];
        $employee['current_employer_zip'] = $formData3['current_employer_zip'];
        $employee['current_employee_city'] = $formData3['current_employee_city'];
        $employee['current_employee_state'] = $formData3['current_employee_state'];
        $employee['current_employee_position'] = $formData3['current_employee_position'];
        $employee['current_employee_salary'] = $formData3['current_employee_salary'];
        $employee['current_employee_from_date'] = mySqlDateFormat($formData3['current_employee_from_date'], null, $this->companyConnection);
        $employee['current_employee_to_date'] =  mySqlDateFormat($formData3['current_employee_to_date'], null, $this->companyConnection);
        $employee['current_employee_no'] = $formData3['current_employee_no'];
        $employee['previous_employer'] = $formData3['previous_employer'];
        $employee['previous_employee_address'] = $formData3['previous_employee_address'];
        $employee['previous_employer_zip'] = $formData3['previous_employer_zip'];
        $employee['previous_employee_city'] = $formData3['previous_employee_city'];
        $employee['previous_employee_state'] = $formData3['previous_employee_state'];
        $employee['previous_employee_position'] = $formData3['previous_employee_position'];
        $employee['previous_employee_salary'] = $formData3['previous_employee_salary'];
        $employee['previous_employee_from_date'] = mySqlDateFormat($formData3['previous_employee_from_date'], null, $this->companyConnection);
        $employee['previous_employee_to_date'] = mySqlDateFormat($formData3['previous_employee_to_date'], null, $this->companyConnection);
        $employee['previous_employee_no'] = $formData3['previous_employee_no'];
        $employee['created_at'] =  date('Y-m-d H:i:s');
        $employee['updated_at'] =  date('Y-m-d H:i:s');
        $sqlData = createSqlColValPair($employee);
        $query = "UPDATE company_employment_history SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
    }
    public function updateAdditionalInfo($rental_id,$formData4){
        if(is_array($formData4['additional_amount'])){
            for($i=0;$i<count($formData4['additional_amount']);$i++){
                if(isset($formData4['additional_amount'][$i]) && !empty($formData4['additional_amount'][$i]))
                {
                    $addinfo['amount'] = $formData4['additional_amount'][$i];
                }
                $addinfo['source_of_income'] = $formData4['additional_income'][$i];
                $addinfo['created_at'] =  date('Y-m-d H:i:s');
                $addinfo['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($addinfo);
                $query = "UPDATE company_annual_income SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
            }
        }
        else{
            if(isset($formData4['additional_amount']) && !empty($formData4['additional_amount']))
            {
                $addinfo['amount'] = $formData4['additional_amount'];
            }
            $addinfo['source_of_income'] = $formData4['additional_income'];
            $addinfo['created_at'] =  date('Y-m-d H:i:s');
            $addinfo['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($addinfo);
            $query = "UPDATE company_annual_income SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

        }
    }
    public function updateEmergencyDetails($rental_id,$formData5){
        if(is_array($formData5['emergency_contact_name'])){
            for($i=0;$i<count($formData5['emergency_contact_name']);$i++)
            {
                $emerDet['emergency_contact_name'] =  $formData5['emergency_contact_name'][$i];
                $emerDet['emergency_relation'] =  $formData5['emergency_relation'][$i];
                $emerDet['emergency_country_code'] =  $formData5['emergency_country']{$i};
                $emerDet['emergency_phone_number'] =  $formData5['emergency_phone'][$i];
                $emerDet['emergency_email'] =  $formData5['emergency_email'][$i];
                $emerDet['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($emerDet);
                $query = "UPDATE emergency_details SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
            }
        }else{
            $emerDet['emergency_contact_name'] =  $formData5['emergency_contact_name'];
            $emerDet['emergency_relation'] =  $formData5['emergency_relation'];
            $emerDet['emergency_country_code'] =  $formData5['emergency_country'];
            $emerDet['emergency_phone_number'] =  $formData5['emergency_phone'];
            $emerDet['emergency_email'] =  $formData5['emergency_email'];
            $emerDet['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($emerDet);
            $query = "UPDATE emergency_details SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

        }
    }
    public function updateRenatlNotes($rental_id,$notes){
        if(is_array($notes['chargeNote']))
        {
            for($i=0;$i<count($notes['chargeNote']);$i++){
                $note['notes'] =  $notes['chargeNote'][$i];
                $note['type'] =  '4';
                $note['record_status'] =  '0';
               $note['updated_at'] =  date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($note);
                $query = "UPDATE tenant_notes SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
            }
        }else{
            $note['notes'] =  $notes['chargeNote'];
            $note['type'] =  '4';
            $note['record_status'] =  '0';
            $note['updated_at'] =  date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($note);
            $query = "UPDATE tenant_notes SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

        }
    }
    public function updateGuarantorDetails($rental_id,$formData6){
        if(is_array($formData6['salutaion'])){
            for($j=0;$j<count($formData6['salutaion']);$j++) {
                $guarantor['salutation'] = $formData6['salutaion'][$j];
                $guarantor['first_name'] = $formData6['first_name'][$j];
                $guarantor['middle_name'] = $formData6['middle_name'][$j];
                $guarantor['last_name'] = $formData6['last_name'][$j];
                $guarantor['gender'] = $formData6['gender'][$j];
                $guarantor['relationship'] = $formData6['relationship'][$j];
                $guarantor['address1'] = $formData6['address1'][$j];
                $guarantor['address2'] = $formData6['address2'][$j];
                $guarantor['address3'] = $formData6['address3'][$j];
                $guarantor['address4'] = $formData6['address4'][$j];
                $guarantor['zip_code'] = $formData6['zipcode'][$j];
                $guarantor['city'] = $formData6['emer_city'][$j];
                $guarantor['state'] = $formData6['state_province'][$j];

                if (is_array($formData6['email_'.$j])) {
                    $count = count($formData6['email_'.$j]);
                    if ($count == 2) {
                        $guarantor['email1'] = $formData6['email_'.$j][0];
                        $guarantor['email2'] = $formData6['email_'.$j][1];
                    } else {
                        $guarantor['email1'] = $formData6['email_'.$j][0];
                        $guarantor['email2'] = $formData6['email_'.$j][1];
                        $guarantor['email3'] = $formData6['email_'.$j][3];
                    }
                } else {
                    $guarantor['email1'] = $formData6['email_0'];
                }
                $guarantor['updated_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($guarantor);
                $query = "UPDATE tenant_guarantor SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                if (is_array($formData6['phone_type'][$j])) {
                    for ($i = 0; $i < count($formData6['phone_type'][$j]); $i++) {
                        $guarantor_emer['phone_type'] = $formData6['phone_type'][$i];
                        if(isset($formData6['carrier'][$i]) && !empty($formData6['carrier'][$i])){
                            $guarantor_emer['carrier'] = $formData6['carrier'][$i];
                        }
                        $guarantor_emer['country_code'] = $formData6['country'][$i];
                        $guarantor_emer['phone_number'] = $formData6['phone_number'][$i];
                        $guarantor_emer['user_type'] = '1';
                        $sqlData = createSqlColValPair($guarantor_emer);
                        $query = "UPDATE tenant_phone SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();


                    }
                } else {
                    $guarantor_emer['phone_type'] = $formData6['phone_type'];
                    if(isset($formData6['carrier']) && !empty($formData6['carrier']))
                    {
                        $guarantor_emer['carrier'] = $formData6['carrier'];
                    }
                    $guarantor_emer['country_code'] = $formData6['country'];
                    $guarantor_emer['phone_number'] = $formData6['phone_number'];
                    $guarantor_emer['user_type'] = '1';
                    $sqlData = createSqlColValPair($guarantor_emer);
                    $query = "UPDATE tenant_phone SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();

                }
            }

        }else{
            //$this->guarantorElsePart($formData6, $last_id);
            $guarantor['salutation'] = $formData6['salutaion'];
            $guarantor['first_name'] = $formData6['first_name'];
            $guarantor['middle_name'] = $formData6['middle_name'];
            $guarantor['last_name'] = $formData6['last_name'];
            $guarantor['gender'] = $formData6['gender'];
            $guarantor['relationship'] = $formData6['relationship'];
            $guarantor['address1'] = $formData6['address1'];
            $guarantor['address2'] = $formData6['address2'];
            $guarantor['address3'] = $formData6['address3'];
            $guarantor['address4'] = $formData6['address4'];
            $guarantor['zip_code'] = $formData6['zipcode'];
            $guarantor['city'] = $formData6['emer_city'];
            $guarantor['state'] = $formData6['state_province'];
            if (is_array($formData6['email_0'])) {
                $count=count($formData6['email_0']);
                if ($count == 2) {
                    $guarantor['email1'] = $formData6['email_0'][0];
                    $guarantor['email2'] = $formData6['email_0'][1];
                } else {
                    $guarantor['email1'] = $formData6['email_0'][0];
                    $guarantor['email2'] = $formData6['email_0'][1];
                    $guarantor['email3'] = $formData6['email_0'][2];
                }
            }else{
                $guarantor['email1'] = $formData6['email_0'];
            }
            $guarantor['updated_at'] = date('Y-m-d H:i:s');
            $sqlData = createSqlColValPair($guarantor);
            $query = "UPDATE tenant_guarantor SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            if(is_array($formData6['phone_type'])){
                for($i=0;$i<count($formData6['phone_type']);$i++){
                    $guarantor_emer['phone_type'] =  $formData6['phone_type'][$i];
                    if(isset($formData6['carrier'][$i]) && !empty($formData6['carrier'][$i])){
                        $guarantor_emer['carrier'] =  $formData6['carrier'][$i];
                    }
                    $guarantor_emer['country_code'] =  $formData6['country'][$i];
                    $guarantor_emer['phone_number'] =  $formData6['phone_number'][$i];
                    $guarantor_emer['user_type'] =  '1';
                    $sqlData = createSqlColValPair($guarantor_emer);
                    $query = "UPDATE tenant_phone SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                }
            }else{
                $guarantor_emer['phone_type'] =  $formData6['phone_type'];
                if(isset($formData6['carrier']) && !empty($formData6['carrier'])) {
                    $guarantor_emer['carrier'] =  $formData6['carrier'];
                }
                $guarantor_emer['country_code'] =  $formData6['country'];
                $guarantor_emer['phone_number'] =  $formData6['phone_number'];
               // $guarantor_emer['user_id'] =  $last_id;
               // $guarantor_emer['parent_id'] =  $last_id_guar;
                $guarantor_emer['user_type'] =  '1';
                $sqlData = createSqlColValPair($guarantor_emer);
                $query = "UPDATE tenant_phone SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();

            }

        }
    }
    public function deleteRentalApplication(){
        try {
                $rental_id=$_POST['id'];
                $query = "DELETE FROM users WHERE id=".$rental_id;
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $rental_id, 'message' => 'The record deleted successfully.');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function archiveRentalApplication(){
        try {
            $rental_id=$_POST['id'];
            $data['status']="2";
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE company_rental_applications SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $rental_id, 'message' => 'The record archived successfully.');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function activeRentalApplication(){
        try {
            $rental_id=$_POST['id'];
            $data['status']="1";
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE company_rental_applications SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();
            return array('code' => 200, 'status' => 'success', 'data' => $rental_id, 'message' => 'The record activated successfully.');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function getRedirectionData(){
        try {
            $id=$_POST['id'];
            $query = $this->companyConnection->query("SELECT salutation,first_name,last_name,mi,maiden_name,nick_name,phone_type,carrier,country,phone_number,phone_number_note,email,referral_source,ethnicity,maritial_status,hobbies,veteran_status,code,ssn_sin_id FROM users left join countries on users.country_code=countries.id where users.id=".$id);
            $data = $query->fetch();
            $query = $this->companyConnection->query("SELECT prop_id,unit_id,build_id,exp_move_in FROM company_rental_applications where user_id=".$id);
            $data2 = $query->fetch();
            $query = $this->companyConnection->query("SELECT * FROM emergency_details where user_id=".$id);
            $data3 = $query->fetch();

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'data2' => $data2,'data3' => $data3, 'message' => 'The record activated successfully.');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function viewrentalapplication()
    {
        $renatl_id = $_POST['id'];
        try {
            $query = $this->companyConnection->query("SELECT * FROM company_rental_applications WHERE user_id=" . $renatl_id);
            $data = $query->fetch();
            $query = $this->companyConnection->query("SELECT property_name FROM general_property WHERE id=" . $data['prop_id']);
            $property = $query->fetch();
            $query = $this->companyConnection->query("SELECT building_name FROM building_detail WHERE id=" . $data['build_id']);
            $building = $query->fetch();
            $query = $this->companyConnection->query("SELECT unit_prefix,unit_no FROM unit_details WHERE id=" . $data['unit_id']);
            $unit = $query->fetch();
            $query = $this->companyConnection->query("SELECT * FROM users WHERE id=" . $renatl_id);
            $data2 = $query->fetch();
            if(isset($data2['veteran_status']) && !empty($data2['veteran_status']) && $data2['veteran_status']!='0'){
                $query = $this->companyConnection->query("SELECT veteran FROM tenant_veteran_status WHERE id=" . $data2['veteran_status']);
                $veteran = $query->fetch();
            }
            else {
                $veteran="";
            }
            if(isset($data2['referral_source'])&& !empty($data2['referral_source']) && $data2['referral_source']!='0'){
                $query = $this->companyConnection->query("SELECT referral FROM tenant_referral_source WHERE id=" . $data2['referral_source']);
                $referral = $query->fetch();
            }
            else{
                $referral="";
            }
            if(isset($data2['ethnicity']) && !empty($data2['ethnicity']) && $data2['ethnicity']!='0'){
                $query = $this->companyConnection->query("SELECT title FROM tenant_ethnicity WHERE id=" . $data2['ethnicity']);
                $ethinicity = $query->fetch();
            }else{
                $ethinicity="";
            }
            if(isset($data2['hobbies']) && !empty($data2['hobbies']) && $data2['hobbies']!='0'){
                $query = $this->companyConnection->query("SELECT hobby FROM hobbies WHERE id=" . $data2['hobbies']);
                $hobbies = $query->fetch();
            }else{
                $hobbies="";
            }
            $query = $this->companyConnection->query("SELECT * FROM tenant_additional_details WHERE user_id=" . $renatl_id);
            $data3 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM company_rental_history WHERE user_id=" . $renatl_id);
            $data4 = $query->fetch();
            if(isset($data4['current_reason']) && !empty($data4['current_reason']) && $data4['current_reason']!='0'){
                $query = $this->companyConnection->query("SELECT reason FROM tenant_collection_reason WHERE id=" . $data4['current_reason']);
                $curr_reason = $query->fetch();
            }else{
                $curr_reason="";
            }
            if(isset($data4['previous_reason']) && !empty($data4['previous_reason']) && $data4['previous_reason']!='0'){
                $query = $this->companyConnection->query("SELECT reason FROM tenant_collection_reason WHERE id=" . $data4['previous_reason']);
                $previous_reason = $query->fetch();
            }else{
                $previous_reason="";
            }
            // print_r($curr_reason);

            $query = $this->companyConnection->query("SELECT * FROM company_employment_history WHERE user_id=" . $renatl_id);
            $data5 = $query->fetch();
            $query = $this->companyConnection->query("SELECT * FROM company_annual_income WHERE user_id=" . $renatl_id);
            $data6 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id=" . $renatl_id);
            $data7 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_guarantor WHERE user_id=" . $renatl_id);
            $data8 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_notes WHERE user_id=" . $renatl_id);
            $data9 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE user_type='1' AND user_id=" . $renatl_id);
            $data10 = $query->fetchAll();
            //echo"<pre>";print_r($data2);die;
            return ['status' => 'success', 'code' => 200, 'data1' => $data, 'data2' => $data2, 'data3' => $data3, 'data4' => $data4, 'data5' => $data5, 'data6' => $data6, 'data7' => $data7, 'data8' => $data8, 'data9' => $data9, 'data10' => $data10, 'prop' => $property, 'unit' => $unit, 'build' => $building, 'veteran' => $veteran, 'referral' => $referral, 'ethinicity' => $ethinicity, 'hobbies' => $hobbies, 'curr_reason' => $curr_reason, 'previous_reason' => $previous_reason];
        } catch (Exception $exception) {
            return ['status' => 'failed', 'code' => 503, 'message' => $exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function approverental(){
        try {
            $rental_id=$_POST['id'];
            $data['status']="4";
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE company_rental_applications SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            return array('code' => 200, 'status' => 'success', 'message' => 'The Application has been activated.');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function declinerental(){
        try {
            $rental_id=$_POST['id'];
            $data['status']="5";
            $sqlData = createSqlColValPair($data);
            $query = "UPDATE company_rental_applications SET " . $sqlData['columnsValuesPair'] ." where user_id=".$rental_id;
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute();

            return array('code' => 200, 'status' => 'success', 'message' => 'The Application has been declined.');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
 public function getcount(){
        try {
            $query = $this->companyConnection->query("SELECT status FROM company_rental_applications WHERE status='1'");
            $data['active'] = $query->fetchAll();
            //echo "<pre>";print_r($data);die;
            $query = $this->companyConnection->query("SELECT status FROM company_rental_applications WHERE status='3'");
            $data['lease_generated'] = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT status FROM company_rental_applications WHERE status='4'");
            $data['approved'] = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT status FROM company_rental_applications WHERE status='5'");
            $data['declined'] = $query->fetchAll();
            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record retrieved successfully.');
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }


    public function getPdfContent(){
        try{
            $renatl_id=$_POST['rental_id'];
            $query = $this->companyConnection->query("SELECT * FROM company_rental_applications WHERE user_id=" . $renatl_id);
            $data = $query->fetch();
            $query = $this->companyConnection->query("SELECT property_name FROM general_property WHERE id=" . $data['prop_id']);
            $property = $query->fetch();

            $query = $this->companyConnection->query("SELECT building_name FROM building_detail WHERE id=" . $data['build_id']);
            $building = $query->fetch();
            $query = $this->companyConnection->query("SELECT unit_prefix,unit_no FROM unit_details WHERE id=" . $data['unit_id']);
            $unit = $query->fetch();
            $query = $this->companyConnection->query("SELECT * FROM users WHERE id=" . $renatl_id);
            $data2 = $query->fetch();
            if(isset($data2['veteran_status']) && !empty($data2['veteran_status']) && $data2['veteran_status']!='0'){
                $query = $this->companyConnection->query("SELECT veteran FROM tenant_veteran_status WHERE id=" . $data2['veteran_status']);
                $veteran = $query->fetch();
            }
            if(isset($data2['referral_source'])&& !empty($data2['referral_source']) && $data2['referral_source']!='0'){
                $query = $this->companyConnection->query("SELECT referral FROM tenant_referral_source WHERE id=" . $data2['referral_source']);
                $referral = $query->fetch();
            }
            if(isset($data2['ethnicity']) && !empty($data2['ethnicity']) && $data2['ethnicity']!='0'){
                $query = $this->companyConnection->query("SELECT title FROM tenant_ethnicity WHERE id=" . $data2['ethnicity']);
                $ethinicity = $query->fetch();
            }
            if(isset($data2['hobbies']) && !empty($data2['hobbies']) && $data2['hobbies']!='0'){
                $query = $this->companyConnection->query("SELECT hobby FROM hobbies WHERE id=" . $data2['hobbies']);
                $hobbies = $query->fetch();
            }
            $query = $this->companyConnection->query("SELECT * FROM tenant_additional_details WHERE user_id=" . $renatl_id);
            $data3 = $query->fetchAll();

            $query = $this->companyConnection->query("SELECT * FROM company_rental_history WHERE user_id=" . $renatl_id);
            $data4 = $query->fetch();

            if(isset($data4['current_reason']) && !empty($data4['current_reason']) && $data4['current_reason']!='0'){
                $query = $this->companyConnection->query("SELECT reason FROM tenant_collection_reason WHERE id=" . $data4['current_reason']);
                $curr_reason = $query->fetch();
            }
            if(isset($data4['previous_reason']) && !empty($data4['previous_reason']) && $data4['previous_reason']!='0'){
                $query = $this->companyConnection->query("SELECT reason FROM tenant_collection_reason WHERE id=" . $data4['previous_reason']);
                $previous_reason = $query->fetch();
            }
            // print_r($curr_reason);
            $query = $this->companyConnection->query("SELECT * FROM company_employment_history WHERE user_id=" . $renatl_id);
            $data5 = $query->fetch();
            $query = $this->companyConnection->query("SELECT * FROM company_annual_income WHERE user_id=" . $renatl_id);
            $data6 = $query->fetchAll();

            $query = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id=" . $renatl_id);
            $data7 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_guarantor WHERE user_id=" . $renatl_id);
            $data8 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_notes WHERE user_id=" . $renatl_id);
            $data9 = $query->fetchAll();
            $query = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE user_type='1' AND user_id=" . $renatl_id);
            $data10 = $query->fetchAll();



            $postArray = [];
            $postArray['exp_move_in']=$data['exp_move_in'];
            $postArray['exp_move_out']=$data['exp_move_out'];
            $postArray['lease_term']=$data['lease_term'];
            $postArray['month']=$data['lease_tenure'];
            $postArray['zip_code']=$data['zip_code'];
            $postArray['city']=$data['city'];
            $postArray['state']=$data['state'];
            $postArray['state']=$data['state'];
            $postArray['address1']=$data['address1'];
            $postArray['address2']=$data['address2'];
            $postArray['address3']=$data['address3'];
            $postArray['address4']=$data['address4'];
            $postArray['market_rent']=$data['market_rent'];
            $postArray['base_rent']=$data['base_rent'];
            $postArray['sec_deposite']=$data['sec_deposite'];
            $postArray['property_name']=$property['property_name'];
            $postArray['building_name']=$building['building_name'];
            $postArray['unit_prefix']=$unit['unit_prefix'];
            $postArray['unit_no']=$unit['unit_no'];

            $postArray['first_name']=$data2['first_name'];
            $postArray['salutation']=$data2['salutation'];
            $postArray['mi']=$data2['mi'];
            $postArray['last_name']=$data2['last_name'];
            $postArray['maiden_name']=$data2['maiden_name'];
            $postArray['nick_name']=$data2['nick_name'];
            $postArray['gender']=$data2['gender'];
            $postArray['state']=$data2['state'];
            $postArray['ssn_sin_id']=$data2['ssn_sin_id'];
            $postArray['dob']=$data2['dob'];
            $postArray['carrier']=$data2['carrier'];
            $postArray['country']=$data2['country'];
            $postArray['phone_number']=$data2['phone_number'];
            $postArray['phone_type']=$data2['phone_type'];
            $postArray['veteran']=$veteran['veteran'];
            $postArray['referral']=$referral['referral'];
            $postArray['ethinicity']=$ethinicity['title'];
            $postArray['hobby']=$hobbies['hobby'];
            $postArray['email']=$data2['email'];
            $postArray['martial_status']=$data2['maritial_status'];

            $postArray['salutation_occ']=$data3[0]['salutation'];
            $postArray['first_name_occ']=$data3[0]['first_name'];
            $postArray['maiden_name_occ']=$data3[0]['maiden_name'];
            $postArray['nick_name_occ']=$data3[0]['nick_name'];
            $postArray['relationship_occ']=$data3[0]['relationship'];
            $postArray['ssn_occ']=$data3[0]['ssn'];


            $postArray['current_address']=$data4['current_address'];
            $postArray['current_zip_code']=$data4['current_zip_code'];
            $postArray['current_city']=$data4['current_city'];
            $postArray['current_state']=$data4['current_state'];
            $postArray['current_landlord']=$data4['current_landlord'];
            $postArray['current_landlord_no']=$data4['current_landlord_no'];
            $postArray['current_salary']=$data4['current_salary'];
            $postArray['current_resided_from']=$data4['current_resided_from'];
            $postArray['current_resided_to']=$data4['current_resided_to'];
            $postArray['current_reason']=$curr_reason['reason'];

            $postArray['previous_address']=$data4['previous_address'];
            $postArray['previous_zip_code']=$data4['previous_zip_code'];
            $postArray['previous_city']=$data4['previous_city'];
            $postArray['previous_state']=$data4['previous_state'];
            $postArray['previous_landlord']=$data4['previous_landlord'];
            $postArray['previous_landlord_no']=$data4['previous_landlord_no'];
            $postArray['previous_salary']=$data4['previous_salary'];
            $postArray['previous_resided_from']=$data4['previous_resided_from'];
            $postArray['previous_resided_to']=$data4['previous_resided_to'];
            $postArray['previous_reason']=$previous_reason['reason'];

            $postArray['current_employer']=$data5['current_employer'];
            $postArray['current_employee_address']=$data5['current_employee_address'];
            $postArray['current_employer_zip']=$data5['current_employer_zip'];
            $postArray['current_employee_city']=$data5['current_employee_city'];
            $postArray['current_employee_state']=$data5['current_employee_state'];
            $postArray['current_employee_position']=$data5['current_employee_position'];
            $postArray['current_employee_salary']=$data5['current_employee_salary'];
            $postArray['current_employee_from_date']=$data5['current_employee_from_date'];
            $postArray['current_employee_to_date']=$data5['current_employee_to_date'];
            $postArray['current_employee_no']=$data5['current_employee_no'];

            $postArray['previous_employer']=$data5['previous_employer'];
            $postArray['previous_employee_address']=$data5['previous_employee_address'];
            $postArray['previous_employer_zip']=$data5['previous_employer_zip'];
            $postArray['previous_employee_city']=$data5['previous_employee_city'];
            $postArray['previous_employee_state']=$data5['previous_employee_state'];
            $postArray['previous_employee_position']=$data5['previous_employee_position'];
            $postArray['previous_employee_salary']=$data5['previous_employee_salary'];
            $postArray['previous_employee_from_date']=$data5['previous_employee_from_date'];
            $postArray['previous_employee_to_date']=$data5['previous_employee_to_date'];
            $postArray['previous_employee_no']=$data5['previous_employee_no'];

            $postArray['amount']=$data6[0]['amount'];

            $postArray['emergency_contact_name']=$data7[0]['emergency_contact_name'];
            $postArray['emergency_relation']=$data7[0]['emergency_relation'];
            $postArray['emergency_country_code']=$data7[0]['emergency_country_code'];
            $postArray['emergency_phone_number']=$data7[0]['emergency_phone_number'];
            $postArray['emergency_email']=$data7[0]['emergency_email'];

            $postArray['company_name_co']=$data8[0]['company_name'];
            $postArray['salutation_co']=$data8[0]['salutation'];
            $postArray['description_co']=$data8[0]['description'];
            $postArray['first_name_co']=$data8[0]['first_name'];
            $postArray['middle_name_co']=$data8[0]['middle_name'];
            $postArray['last_name_co']=$data8[0]['last_name'];
            $postArray['gender_co']=$data8[0]['gender'];
            $postArray['relationship_co']=$data8[0]['relationship'];
            $postArray['zip_code_co']=$data8[0]['zip_code'];
            $postArray['country_co']=$data8[0]['country'];
            $postArray['state_co']=$data8[0]['state'];
            $postArray['city_co']=$data8[0]['city'];
            $postArray['address1_co']=$data8[0]['address1'];
            $postArray['address2_co']=$data8[0]['address2'];
            $postArray['address3_co']=$data8[0]['address3'];
            $postArray['address4_co']=$data8[0]['address4'];
            $postArray['email1_co']=$data8[0]['email1'];
            $postArray['email2_co']=$data8[0]['email2'];
            $postArray['email3_co']=$data8[0]['email3'];
            $postArray['guarantee_years_co']=$data8[0]['guarantee_years'];
            $postArray['phone_type_co']=$data10[0]['phone_type'];
            $postArray['guarantee_years_co']=$data10[0]['work_phone_extension'];
            $postArray['carrier_co']=$data10[0]['carrier'];
            $postArray['country_code_co']=$data10[0]['country_code'];
            $postArray['phone_number_co']=$data10[0]['phone_number'];

            $postArray['notes']=$data9[0]['notes'];

            $html = $this->createHtml($postArray);
            $pdf = $this->createHTMLToPDF($html);
            $data['record'] = $pdf['data'];
            return ['code'=>200, 'status'=>'success', 'data'=>$data];
        }catch (Exception $exception){
            return array('code' => 504, 'status' => 'error','message' => $exception->getMessage());
        }

    }


    public function createHtml($postArray){
       /* $html = '<html>
<head>
    <title></title>
</head>
<body>
    <div style="background-color: #fff">
        <div style="background-color: #40c3f5">
            <div style="background-color: #40c3f5; margin: 20px;text-align: center;">
                <div style="background-color: #fff;text-align: center; height: 50px;">
                    <img src="" style="width: 50px; margin: 0px auto;">
                 </div>
            </div>
            <div style="background-color: #40c3f5; margin: 20px;">
                <table border="0" style="background-color: #fff;font-size: 12px; width: 100%;">
                    <thead>
                        <tr>
                            <th>Tenant Name</th>
                            <th>Unit</th>
                            <th>Property</th>
                            <th>Ticket Number</th>
                            <th>Ticket Type</th>
                            <th>Category</th>
                            <th>Created Date</th>
                            <th>Status</th>

                        </tr>
                    </thead>
                    <tbody>';*/
       //echo"<pre>";print_r($postArray);die;

        $html = '<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_002{font-family:Arial,serif;font-size:8.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:8.5px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_003{font-family:Arial,serif;font-size:5.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_003{font-family:Arial,serif;font-size:5.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:9.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:9.2px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Arial,serif;font-size:7.8px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Arial,serif;font-size:7.8px;color:rgb(255,255,255);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="8fbfafba-e0e8-11e9-9d71-0cc47a792c0a_id_8fbfafba-e0e8-11e9-9d71-0cc47a792c0a_files/wz_jsgraphics.js"></script>
</head>
<body>

<div style="border-left: 2px solid #9a9a9a; position:absolute;left:50%;margin-left:-297px;top:0px;width:595px;height:842px;border-style:outset;overflow:hidden; border-top: 15px solid #1c91cc;">
<div style="position:absolute;left:0px;top:0px;">
</div>
<div style="position:absolute;left:0px;top:80px; text-align:center; border-top: 15px solid #1c91cc; width:595px" class="cls_002"><span class="cls_002">Rental Application</span></div>
<div style="position:absolute;left:276.99px;top:151.12px" class="cls_003"><span class="cls_003">________________________</span></div>
<div style="position:absolute;left:267.09px;top:176.58px" class="cls_002"><span class="cls_002">RENTAL UNIT DETAILS</span></div>
<div style="position:absolute;left:89.60px;top:219.01px" class="cls_004"><span class="cls_004">Expected Move­</span></div>
<div style="position:absolute;left:310.94px;top:219.01px" class="cls_004"><span class="cls_004">Requested</span></div>
<div style="position:absolute;left:156.07px;top:225.37px" class="cls_004"><span class="cls_004">'.$postArray['exp_move_in'].'</span></div>
<div style="position:absolute;left:377.41px;top:225.37px" class="cls_004"><span class="cls_004">'.$postArray['lease_term'].'</span></div>
<div style="position:absolute;left:89.60px;top:232.44px" class="cls_004"><span class="cls_004">In Date:</span></div>
<div style="position:absolute;left:310.94px;top:232.44px" class="cls_004"><span class="cls_004">Lease Term:</span></div>
<div style="position:absolute;left:310.94px;top:269.21px" class="cls_004"><span class="cls_004">Expected Move­</span></div>
<div style="position:absolute;left:89.60px;top:275.58px" class="cls_004"><span class="cls_004">Month:</span></div>
<div style="position:absolute;left:156.07px;top:275.58px" class="cls_004"><span class="cls_004">'.$postArray['month'].'</span></div>
<div style="position:absolute;left:377.41px;top:275.58px" class="cls_004"><span class="cls_004">'.$postArray['exp_move_out'].'</span></div>
<div style="position:absolute;left:310.94px;top:282.65px" class="cls_004"><span class="cls_004">Out Date:</span></div>
<div style="position:absolute;left:89.60px;top:325.78px" class="cls_004"><span class="cls_004">Property:</span></div>
<div style="position:absolute;left:156.07px;top:325.78px" class="cls_004"><span class="cls_004">'.$postArray['property_name'].'</span></div>
<div style="position:absolute;left:310.94px;top:325.78px" class="cls_004"><span class="cls_004">Building:</span></div>
<div style="position:absolute;left:377.41px;top:325.78px" class="cls_004"><span class="cls_004">'.$postArray['building_name'].'</span></div>
<div style="position:absolute;left:310.94px;top:369.63px" class="cls_004"><span class="cls_004">Zip / Postal</span></div>
<div style="position:absolute;left:89.60px;top:375.99px" class="cls_004"><span class="cls_004">Unit:</span></div>
<div style="position:absolute;left:156.07px;top:375.99px" class="cls_004"><span class="cls_004">'.$postArray['unit_prefix'].'-'.$postArray['unit_no'].'</span></div>
<div style="position:absolute;left:377.41px;top:375.99px" class="cls_004"><span class="cls_004">'.$postArray['zip_code'].'</span></div>
<div style="position:absolute;left:310.94px;top:383.06px" class="cls_004"><span class="cls_004">Code:</span></div>
<div style="position:absolute;left:310.94px;top:419.83px" class="cls_004"><span class="cls_004">State /</span></div>
<div style="position:absolute;left:89.60px;top:426.20px" class="cls_004"><span class="cls_004">City:</span></div>
<div style="position:absolute;left:156.07px;top:426.20px" class="cls_004"><span class="cls_004">'.$postArray['city'].'</span></div>
<div style="position:absolute;left:377.41px;top:426.20px" class="cls_004"><span class="cls_004">'.$postArray['state'].'</span></div>
<div style="position:absolute;left:310.94px;top:433.27px" class="cls_004"><span class="cls_004">Province:</span></div>
<div style="position:absolute;left:89.60px;top:470.04px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:310.94px;top:470.04px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:476.41px" class="cls_004"><span class="cls_004">'.$postArray['address1'].'</span></div>
<div style="position:absolute;left:377.41px;top:476.41px" class="cls_004"><span class="cls_004">'.$postArray['address2'].'</span></div>
<div style="position:absolute;left:89.60px;top:483.48px" class="cls_004"><span class="cls_004">1:</span></div>
<div style="position:absolute;left:310.94px;top:483.48px" class="cls_004"><span class="cls_004">2:</span></div>
<div style="position:absolute;left:89.60px;top:520.25px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:310.94px;top:520.25px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:526.61px" class="cls_004"><span class="cls_004">'.$postArray['address3'].'</span></div>
<div style="position:absolute;left:377.41px;top:526.61px" class="cls_004"><span class="cls_004">'.$postArray['address4'].'</span></div>
<div style="position:absolute;left:89.60px;top:533.68px" class="cls_004"><span class="cls_004">3:</span></div>
<div style="position:absolute;left:310.94px;top:533.68px" class="cls_004"><span class="cls_004">4:</span></div>
<div style="position:absolute;left:89.60px;top:576.82px" class="cls_004"><span class="cls_004">Market Rent :</span></div>
<div style="position:absolute;left:156.07px;top:576.82px" class="cls_004"><span class="cls_004">'.$postArray['market_rent'].'</span></div>
<div style="position:absolute;left:310.94px;top:576.82px" class="cls_004"><span class="cls_004">Base Rent :</span></div>
<div style="position:absolute;left:377.41px;top:576.82px" class="cls_004"><span class="cls_004">'.$postArray['base_rent'].'</span></div>
<div style="position:absolute;left:89.60px;top:620.66px" class="cls_004"><span class="cls_004">Security</span></div>
<div style="position:absolute;left:156.07px;top:627.03px" class="cls_004"><span class="cls_004">'.$postArray['sec_deposite'].'</span></div>
<div style="position:absolute;left:89.60px;top:634.10px" class="cls_004"><span class="cls_004">Deposit (US $):</span></div>
<div style="position:absolute;left:263.56px;top:670.87px" class="cls_002"><span class="cls_002">GENERAL INFORMATION</span></div>
<div style="position:absolute;left:89.60px;top:719.66px" class="cls_004"><span class="cls_004">Salutation:</span></div>
<div style="position:absolute;left:156.07px;top:719.66px" class="cls_004"><span class="cls_004">'.$postArray['salutation'].'</span></div>
<div style="position:absolute;left:310.94px;top:719.66px" class="cls_004"><span class="cls_004">First Name:</span></div>
<div style="position:absolute;left:377.41px;top:719.66px" class="cls_004"><span class="cls_004">'.$postArray['first_name'].'</span></div>
<div style="position:absolute;left:89.60px;top:769.87px" class="cls_004"><span class="cls_004">Middle Name:</span></div>
<div style="position:absolute;left:156.07px;top:769.87px" class="cls_004"><span class="cls_004">'.$postArray['mi'].'</span></div>
<div style="position:absolute;left:310.94px;top:769.87px" class="cls_004"><span class="cls_004">Last Name:</span></div>
<div style="position:absolute;left:377.41px;top:769.87px" class="cls_004"><span class="cls_004">'.$postArray['last_name'].'</span></div>
</div>
<div style="border-left: 2px solid #9a9a9a; position:absolute;left:50%;margin-left:-297px;top:852px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
</div>
<div style="position:absolute;left:89.60px;top:78.08px" class="cls_004"><span class="cls_004">Maiden Name:</span></div>
<div style="position:absolute;left:156.07px;top:78.08px" class="cls_004"><span class="cls_004">'.$postArray['maiden_name'].'</span></div>
<div style="position:absolute;left:310.94px;top:78.08px" class="cls_004"><span class="cls_004">Nickname:</span></div>
<div style="position:absolute;left:377.41px;top:78.08px" class="cls_004"><span class="cls_004">'.$postArray['nick_name'].'</span></div>
<div style="position:absolute;left:310.94px;top:121.92px" class="cls_004"><span class="cls_004">Driver\'s License</span></div>
<div style="position:absolute;left:89.60px;top:128.28px" class="cls_004"><span class="cls_004">Gender:</span></div>
<div style="position:absolute;left:156.07px;top:128.28px" class="cls_004"><span class="cls_004">'.$postArray['gender'].'</span></div>
<div style="position:absolute;left:377.41px;top:128.28px" class="cls_004"><span class="cls_004">'.$postArray['state'].'</span></div>
<div style="position:absolute;left:310.94px;top:135.36px" class="cls_004"><span class="cls_004">/ State:</span></div>
<div style="position:absolute;left:89.60px;top:178.49px" class="cls_004"><span class="cls_004">SSN/SIN/ID:</span></div>
<div style="position:absolute;left:156.07px;top:178.49px" class="cls_004"><span class="cls_004">'.$postArray['ssn_sin_id'].'</span></div>
<div style="position:absolute;left:310.94px;top:178.49px" class="cls_004"><span class="cls_004">DOB:</span></div>
<div style="position:absolute;left:377.41px;top:178.49px" class="cls_004"><span class="cls_004">'.$postArray['dob'].'</span></div>
<div style="position:absolute;left:89.60px;top:222.33px" class="cls_004"><span class="cls_004">Referral Source</span></div>
<div style="position:absolute;left:156.07px;top:228.70px" class="cls_004"><span class="cls_004">'.$postArray['referral'].'</span></div>
<div style="position:absolute;left:310.94px;top:228.70px" class="cls_004"><span class="cls_004">Phone Type:</span></div>
<div style="position:absolute;left:377.41px;top:228.70px" class="cls_004"><span class="cls_004">'.$postArray['phone_type'].'</span></div>
<div style="position:absolute;left:89.60px;top:235.77px" class="cls_004"><span class="cls_004">:</span></div>
<div style="position:absolute;left:89.60px;top:278.91px" class="cls_004"><span class="cls_004">Carrier :</span></div>
<div style="position:absolute;left:156.07px;top:278.91px" class="cls_004"><span class="cls_004">'.$postArray['carrier'].'</span></div>
<div style="position:absolute;left:310.94px;top:278.91px" class="cls_004"><span class="cls_004">Country Code:</span></div>
<div style="position:absolute;left:377.41px;top:278.91px" class="cls_004"><span class="cls_004">'.$postArray['country'].'</span></div>
<div style="position:absolute;left:89.60px;top:329.11px" class="cls_004"><span class="cls_004">Phone Number: '.$postArray['phone_number'].'</span></div>
<div style="position:absolute;left:310.94px;top:329.11px" class="cls_004"><span class="cls_004">Email:</span></div>
<div style="position:absolute;left:377.41px;top:329.11px" class="cls_004"><span class="cls_004">'.$postArray['email'].'</span></div>
<div style="position:absolute;left:89.60px;top:379.32px" class="cls_004"><span class="cls_004">Ethnicity:</span></div>
<div style="position:absolute;left:156.07px;top:379.32px" class="cls_004"><span class="cls_004">'.$postArray['ethinicity'].'</span></div>
<div style="position:absolute;left:310.94px;top:379.32px" class="cls_004"><span class="cls_004">Marital Status:</span></div>
<div style="position:absolute;left:377.41px;top:379.32px" class="cls_004"><span class="cls_004">'.$postArray['martial_status'].'</span></div>
<div style="position:absolute;left:89.60px;top:429.53px" class="cls_004"><span class="cls_004">Hobbies:</span></div>
<div style="position:absolute;left:156.07px;top:429.53px" class="cls_004"><span class="cls_004">'.$postArray['hobby'].'</span></div>
<div style="position:absolute;left:310.94px;top:429.53px" class="cls_004"><span class="cls_004">Veteran Status: '.$postArray['veteran'].'</span></div>
<div style="position:absolute;left:173.75px;top:491.76px" class="cls_002"><span class="cls_002">Please list names of all other occupants that will be staying with you.</span></div>
<div style="position:absolute;left:89.60px;top:538.43px" class="cls_004"><span class="cls_004">Name:</span></div>
<div style="position:absolute;left:156.07px;top:538.43px" class="cls_004"><span class="cls_004">'.$postArray['first_name_occ'].'</span></div>
<div style="position:absolute;left:310.94px;top:538.43px" class="cls_004"><span class="cls_004">SSN/SIN/ID:</span></div>
<div style="position:absolute;left:377.41px;top:538.43px" class="cls_004"><span class="cls_004">'.$postArray['ssn_occ'].'</span></div>
<div style="position:absolute;left:89.60px;top:588.63px" class="cls_004"><span class="cls_004">Relationship:</span></div>
<div style="position:absolute;left:156.07px;top:588.63px" class="cls_004"><span class="cls_004">'.$postArray['relationship_occ'].'</span></div>
<div style="position:absolute;left:89.60px;top:645.21px" class="cls_004"><span class="cls_004">Name:</span></div>
<div style="position:absolute;left:156.07px;top:645.21px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:645.21px" class="cls_004"><span class="cls_004">SSN/SIN/ID:</span></div>
<div style="position:absolute;left:377.41px;top:645.21px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:695.41px" class="cls_004"><span class="cls_004">Relationship:</span></div>
<div style="position:absolute;left:156.07px;top:695.41px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:751.98px" class="cls_004"><span class="cls_004">Name:</span></div>
<div style="position:absolute;left:156.07px;top:751.98px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:751.98px" class="cls_004"><span class="cls_004">SSN/SIN/ID:</span></div>
<div style="position:absolute;left:377.41px;top:751.98px" class="cls_004"><span class="cls_004"></span></div>
</div>
<div style="border-left: 2px solid #9a9a9a; position:absolute;left:50%;margin-left:-297px;top:1704px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
</div>
<div style="position:absolute;left:89.60px;top:60.19px" class="cls_004"><span class="cls_004">Relationship:</span></div>
<div style="position:absolute;left:156.07px;top:60.19px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:116.76px" class="cls_004"><span class="cls_004">Name:</span></div>
<div style="position:absolute;left:156.07px;top:116.76px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:116.76px" class="cls_004"><span class="cls_004">SSN/SIN/ID:</span></div>
<div style="position:absolute;left:377.41px;top:116.76px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:166.97px" class="cls_004"><span class="cls_004">Relationship:</span></div>
<div style="position:absolute;left:156.07px;top:166.97px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:223.54px" class="cls_004"><span class="cls_004">Name:</span></div>
<div style="position:absolute;left:156.07px;top:223.54px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:223.54px" class="cls_004"><span class="cls_004">SSN/SIN/ID:</span></div>
<div style="position:absolute;left:377.41px;top:223.54px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:273.75px" class="cls_004"><span class="cls_004">Relationship:</span></div>
<div style="position:absolute;left:156.07px;top:273.75px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:276.99px;top:345.88px" class="cls_002"><span class="cls_002">RENTAL HISTORY</span></div>
<div style="position:absolute;left:89.60px;top:388.31px" class="cls_004"><span class="cls_004">Current</span></div>
<div style="position:absolute;left:310.94px;top:388.31px" class="cls_004"><span class="cls_004">Rental Zip</span></div>
<div style="position:absolute;left:156.07px;top:394.67px" class="cls_004"><span class="cls_004">'.$postArray['current_address'].'</span></div>
<div style="position:absolute;left:377.41px;top:394.67px" class="cls_004"><span class="cls_004">'.$postArray['current_zip_code'].'</span></div>
<div style="position:absolute;left:89.60px;top:401.74px" class="cls_004"><span class="cls_004">Address:</span></div>
<div style="position:absolute;left:310.94px;top:401.74px" class="cls_004"><span class="cls_004">Code:</span></div>
<div style="position:absolute;left:310.94px;top:438.51px" class="cls_004"><span class="cls_004">Rental State /</span></div>
<div style="position:absolute;left:89.60px;top:444.88px" class="cls_004"><span class="cls_004">Rental City:</span></div>
<div style="position:absolute;left:156.07px;top:444.88px" class="cls_004"><span class="cls_004">'.$postArray['current_state'].'</span></div>
<div style="position:absolute;left:377.41px;top:444.88px" class="cls_004"><span class="cls_004">'.$postArray['current_city'].'</span></div>
<div style="position:absolute;left:310.94px;top:451.95px" class="cls_004"><span class="cls_004">Province:</span></div>
<div style="position:absolute;left:310.94px;top:484.48px" class="cls_004"><span class="cls_004">Landlord /</span></div>
<div style="position:absolute;left:89.60px;top:490.84px" class="cls_004"><span class="cls_004">Landlord /</span></div>
<div style="position:absolute;left:156.07px;top:497.91px" class="cls_004"><span class="cls_004">'.$postArray['current_state'].'</span></div>
<div style="position:absolute;left:310.94px;top:497.91px" class="cls_004"><span class="cls_004">Manager Phone '.$postArray['current_landlord_no'].'</span></div>
<div style="position:absolute;left:89.60px;top:504.28px" class="cls_004"><span class="cls_004">Manager:</span></div>
<div style="position:absolute;left:310.94px;top:511.35px" class="cls_004"><span class="cls_004">No.:</span></div>
<div style="position:absolute;left:89.60px;top:550.24px" class="cls_004"><span class="cls_004">Monthly Rent:</span></div>
<div style="position:absolute;left:156.07px;top:550.24px" class="cls_004"><span class="cls_004">'.$postArray['current_salary'].'</span></div>
<div style="position:absolute;left:310.94px;top:550.24px" class="cls_004"><span class="cls_004">Resided From:</span></div>
<div style="position:absolute;left:377.41px;top:550.24px" class="cls_004"><span class="cls_004">'.$postArray['current_resided_from'].'</span></div>
<div style="position:absolute;left:310.94px;top:594.09px" class="cls_004"><span class="cls_004">Reason For</span></div>
<div style="position:absolute;left:89.60px;top:600.45px" class="cls_004"><span class="cls_004">Resided To:</span></div>
<div style="position:absolute;left:156.07px;top:600.45px" class="cls_004"><span class="cls_004">'.$postArray['current_reason'].'</span></div>
<div style="position:absolute;left:377.41px;top:600.45px" class="cls_004"><span class="cls_004">'.$postArray['current_resided_to'].'</span></div>
<div style="position:absolute;left:310.94px;top:607.52px" class="cls_004"><span class="cls_004">Leaving:</span></div>
</div>
<div style="border-left: 2px solid #9a9a9a;position:absolute;left:50%;margin-left:-297px;top:2556px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
</div>
<div style="position:absolute;left:255.07px;top:69.09px" class="cls_002"><span class="cls_002">RENTAL HISTORY SECTION II</span></div>
<div style="position:absolute;left:89.60px;top:111.52px" class="cls_004"><span class="cls_004">Previous</span></div>
<div style="position:absolute;left:310.94px;top:111.52px" class="cls_004"><span class="cls_004">Rental Zip</span></div>
<div style="position:absolute;left:156.07px;top:117.88px" class="cls_004"><span class="cls_004">'.$postArray['previous_address'].'</span></div>
<div style="position:absolute;left:377.41px;top:117.88px" class="cls_004"><span class="cls_004">'.$postArray['previous_zip_code'].'</span></div>
<div style="position:absolute;left:89.60px;top:124.96px" class="cls_004"><span class="cls_004">Address:</span></div>
<div style="position:absolute;left:310.94px;top:124.96px" class="cls_004"><span class="cls_004">Code:</span></div>
<div style="position:absolute;left:310.94px;top:161.73px" class="cls_004"><span class="cls_004">Rental State /</span></div>
<div style="position:absolute;left:89.60px;top:168.09px" class="cls_004"><span class="cls_004">Rental City:</span></div>
<div style="position:absolute;left:156.07px;top:168.09px" class="cls_004"><span class="cls_004">'.$postArray['previous_city'].'</span></div>
<div style="position:absolute;left:377.41px;top:168.09px" class="cls_004"><span class="cls_004">'.$postArray['previous_state'].'</span></div>
<div style="position:absolute;left:310.94px;top:175.16px" class="cls_004"><span class="cls_004">Province:</span></div>
<div style="position:absolute;left:310.94px;top:207.69px" class="cls_004"><span class="cls_004">Landlord /</span></div>
<div style="position:absolute;left:89.60px;top:214.06px" class="cls_004"><span class="cls_004">Landlord /</span></div>
<div style="position:absolute;left:156.07px;top:221.13px" class="cls_004"><span class="cls_004">'.$postArray['previous_landlord'].'</span></div>
<div style="position:absolute;left:310.94px;top:221.13px" class="cls_004"><span class="cls_004">Manager Phone '.$postArray['previous_landlord_no'].'</span></div>
<div style="position:absolute;left:89.60px;top:227.49px" class="cls_004"><span class="cls_004">Manager:</span></div>
<div style="position:absolute;left:310.94px;top:234.56px" class="cls_004"><span class="cls_004">No:</span></div>
<div style="position:absolute;left:89.60px;top:273.46px" class="cls_004"><span class="cls_004">Monthly Rent:</span></div>
<div style="position:absolute;left:156.07px;top:273.46px" class="cls_004"><span class="cls_004">'.$postArray['previous_salary'].'</span></div>
<div style="position:absolute;left:310.94px;top:273.46px" class="cls_004"><span class="cls_004">Resided From:</span></div>
<div style="position:absolute;left:377.41px;top:273.46px" class="cls_004"><span class="cls_004">'.$postArray['previous_resided_from'].'</span></div>
<div style="position:absolute;left:310.94px;top:317.30px" class="cls_004"><span class="cls_004">Reason For</span></div>
<div style="position:absolute;left:89.60px;top:323.66px" class="cls_004"><span class="cls_004">Resided To:</span></div>
<div style="position:absolute;left:156.07px;top:323.66px" class="cls_004"><span class="cls_004">'.$postArray['previous_resided_to'].'</span></div>
<div style="position:absolute;left:377.41px;top:323.66px" class="cls_004"><span class="cls_004">'.$postArray['previous_reason'].'</span></div>
<div style="position:absolute;left:310.94px;top:330.73px" class="cls_004"><span class="cls_004">Leaving:</span></div>
<div style="position:absolute;left:264.26px;top:413.47px" class="cls_002"><span class="cls_002">EMPLOYMENT HISTORY</span></div>
<div style="position:absolute;left:89.60px;top:456.61px" class="cls_004"><span class="cls_004">Current</span></div>
<div style="position:absolute;left:310.94px;top:456.61px" class="cls_004"><span class="cls_004">Employer</span></div>
<div style="position:absolute;left:156.07px;top:462.97px" class="cls_004"><span class="cls_004">'.$postArray['current_employer'].'</span></div>
<div style="position:absolute;left:377.41px;top:462.97px" class="cls_004"><span class="cls_004">'.$postArray['current_employee_address'].'</span></div>
<div style="position:absolute;left:89.60px;top:470.04px" class="cls_004"><span class="cls_004">Employer:</span></div>
<div style="position:absolute;left:310.94px;top:470.04px" class="cls_004"><span class="cls_004">Address:</span></div>
<div style="position:absolute;left:89.60px;top:506.81px" class="cls_004"><span class="cls_004">Zip/Postal</span></div>
<div style="position:absolute;left:156.07px;top:513.18px" class="cls_004"><span class="cls_004">'.$postArray['current_employer_zip'].'</span></div>
<div style="position:absolute;left:310.94px;top:513.18px" class="cls_004"><span class="cls_004">Employer City:</span></div>
<div style="position:absolute;left:377.41px;top:513.18px" class="cls_004"><span class="cls_004">'.$postArray['current_employee_city'].'</span></div>
<div style="position:absolute;left:89.60px;top:520.25px" class="cls_004"><span class="cls_004">Code:</span></div>
<div style="position:absolute;left:89.60px;top:557.02px" class="cls_004"><span class="cls_004">Employer State</span></div>
<div style="position:absolute;left:156.07px;top:563.38px" class="cls_004"><span class="cls_004">'.$postArray['current_employee_state'].'</span></div>
<div style="position:absolute;left:310.94px;top:563.38px" class="cls_004"><span class="cls_004">Position Held:</span></div>
<div style="position:absolute;left:377.41px;top:563.38px" class="cls_004"><span class="cls_004">'.$postArray['current_employee_position'].'</span></div>
<div style="position:absolute;left:89.60px;top:570.46px" class="cls_004"><span class="cls_004">/ Province:</span></div>
<div style="position:absolute;left:89.60px;top:607.23px" class="cls_004"><span class="cls_004">Monthly Salary</span></div>
<div style="position:absolute;left:310.94px;top:607.23px" class="cls_004"><span class="cls_004">Employed From</span></div>
<div style="position:absolute;left:156.07px;top:613.59px" class="cls_004"><span class="cls_004">'.$postArray['current_employee_salary'].'</span></div>
<div style="position:absolute;left:377.41px;top:613.59px" class="cls_004"><span class="cls_004">'.$postArray['current_employee_from_date'].'</span></div>
<div style="position:absolute;left:89.60px;top:620.66px" class="cls_004"><span class="cls_004">:</span></div>
<div style="position:absolute;left:310.94px;top:620.66px" class="cls_004"><span class="cls_004">Date Date:</span></div>
<div style="position:absolute;left:89.60px;top:658.85px" class="cls_004"><span class="cls_004">Employed To</span></div>
<div style="position:absolute;left:310.94px;top:658.85px" class="cls_004"><span class="cls_004">Employer</span></div>
<div style="position:absolute;left:156.07px;top:665.21px" class="cls_004"><span class="cls_004">'.$postArray['current_employee_to_date'].'</span></div>
<div style="position:absolute;left:377.41px;top:665.21px" class="cls_004"><span class="cls_004">'.$postArray['current_employee_no'].'</span></div>
<div style="position:absolute;left:89.60px;top:672.28px" class="cls_004"><span class="cls_004">Date Date:</span></div>
<div style="position:absolute;left:310.94px;top:672.28px" class="cls_004"><span class="cls_004">Phone Number:</span></div>
</div>
<div style="border-left: 2px solid #9a9a9a;position:absolute;left:50%;margin-left:-297px;top:3408px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
</div>
<div style="position:absolute;left:260.73px;top:69.09px" class="cls_002"><span class="cls_002">EMPLOYMENT HISTORY II</span></div>
<div style="position:absolute;left:89.60px;top:112.23px" class="cls_004"><span class="cls_004">Previous</span></div>
<div style="position:absolute;left:310.94px;top:112.23px" class="cls_004"><span class="cls_004">Employer</span></div>
<div style="position:absolute;left:156.07px;top:118.59px" class="cls_004"><span class="cls_004">'.$postArray['previous_employer'].'</span></div>
<div style="position:absolute;left:377.41px;top:118.59px" class="cls_004"><span class="cls_004">'.$postArray['previous_employee_address'].'</span></div>
<div style="position:absolute;left:89.60px;top:125.66px" class="cls_004"><span class="cls_004">Employer:</span></div>
<div style="position:absolute;left:310.94px;top:125.66px" class="cls_004"><span class="cls_004">Address:</span></div>
<div style="position:absolute;left:89.60px;top:162.44px" class="cls_004"><span class="cls_004">Employer Zip</span></div>
<div style="position:absolute;left:156.07px;top:168.80px" class="cls_004"><span class="cls_004">'.$postArray['previous_employer_zip'].'</span></div>
<div style="position:absolute;left:310.94px;top:168.80px" class="cls_004"><span class="cls_004">Employer City:</span></div>
<div style="position:absolute;left:377.41px;top:168.80px" class="cls_004"><span class="cls_004">'.$postArray['previous_employee_city'].'</span></div>
<div style="position:absolute;left:89.60px;top:175.87px" class="cls_004"><span class="cls_004">Code:</span></div>
<div style="position:absolute;left:89.60px;top:212.64px" class="cls_004"><span class="cls_004">Employer State</span></div>
<div style="position:absolute;left:156.07px;top:219.01px" class="cls_004"><span class="cls_004">'.$postArray['previous_employee_state'].'</span></div>
<div style="position:absolute;left:310.94px;top:219.01px" class="cls_004"><span class="cls_004">Position Held:</span></div>
<div style="position:absolute;left:377.41px;top:219.01px" class="cls_004"><span class="cls_004">'.$postArray['previous_employee_position'].'</span></div>
<div style="position:absolute;left:89.60px;top:226.08px" class="cls_004"><span class="cls_004">/ Province:</span></div>
<div style="position:absolute;left:89.60px;top:262.85px" class="cls_004"><span class="cls_004">Monthly Salary</span></div>
<div style="position:absolute;left:310.94px;top:262.85px" class="cls_004"><span class="cls_004">Employed From</span></div>
<div style="position:absolute;left:156.07px;top:269.21px" class="cls_004"><span class="cls_004">'.$postArray['previous_employee_salary'].'</span></div>
<div style="position:absolute;left:377.41px;top:269.21px" class="cls_004"><span class="cls_004">'.$postArray['previous_employee_from_date'].'</span></div>
<div style="position:absolute;left:89.60px;top:276.29px" class="cls_004"><span class="cls_004">:</span></div>
<div style="position:absolute;left:310.94px;top:276.29px" class="cls_004"><span class="cls_004">Date Date:</span></div>
<div style="position:absolute;left:89.60px;top:313.06px" class="cls_004"><span class="cls_004">Employed To</span></div>
<div style="position:absolute;left:310.94px;top:313.06px" class="cls_004"><span class="cls_004">Employer</span></div>
<div style="position:absolute;left:156.07px;top:319.42px" class="cls_004"><span class="cls_004">'.$postArray['previous_employee_to_date'].'</span></div>
<div style="position:absolute;left:377.41px;top:319.42px" class="cls_004"><span class="cls_004">'.$postArray['previous_employee_no'].'</span></div>
<div style="position:absolute;left:89.60px;top:326.49px" class="cls_004"><span class="cls_004">Date Date:</span></div>
<div style="position:absolute;left:310.94px;top:326.49px" class="cls_004"><span class="cls_004">Phone Number</span></div>
<div style="position:absolute;left:260.02px;top:417.01px" class="cls_002"><span class="cls_002">Additional Income (If Any)</span></div>
<div style="position:absolute;left:89.60px;top:466.51px" class="cls_004"><span class="cls_004">Amount:</span></div>
<div style="position:absolute;left:156.07px;top:466.51px" class="cls_004"><span class="cls_004">'.$postArray['amount'].'</span></div>
<div style="position:absolute;left:256.49px;top:578.24px" class="cls_002"><span class="cls_002">Emergency Contact Details</span></div>
<div style="position:absolute;left:89.60px;top:627.03px" class="cls_004"><span class="cls_004">Salutation:</span></div>
<div style="position:absolute;left:156.07px;top:627.03px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:627.03px" class="cls_004"><span class="cls_004">First Name:</span></div>
<div style="position:absolute;left:377.41px;top:627.03px" class="cls_004"><span class="cls_004">'.$postArray['emergency_contact_name'].'</span></div>
<div style="position:absolute;left:89.60px;top:677.24px" class="cls_004"><span class="cls_004">Middle Name:</span></div>
<div style="position:absolute;left:156.07px;top:677.24px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:677.24px" class="cls_004"><span class="cls_004">Last Name:</span></div>
<div style="position:absolute;left:377.41px;top:677.24px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:727.44px" class="cls_004"><span class="cls_004">Gender:</span></div>
<div style="position:absolute;left:156.07px;top:727.44px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:727.44px" class="cls_004"><span class="cls_004">Relationship:</span></div>
<div style="position:absolute;left:377.41px;top:727.44px" class="cls_004"><span class="cls_004">'.$postArray['emergency_relation'].'</span></div>
</div>
<div style="border-left: 2px solid #9a9a9a;position:absolute;left:50%;margin-left:-297px;top:4260px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
</div>
<div style="position:absolute;left:89.60px;top:50.00px" class="cls_004"><span class="cls_004">Zip / Postal</span></div>
<div style="position:absolute;left:156.07px;top:56.36px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:56.36px" class="cls_004"><span class="cls_004">City:</span></div>
<div style="position:absolute;left:377.41px;top:56.36px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:63.44px" class="cls_004"><span class="cls_004">Code:</span></div>
<div style="position:absolute;left:89.60px;top:100.21px" class="cls_004"><span class="cls_004">State /</span></div>
<div style="position:absolute;left:310.94px;top:100.21px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:106.57px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:377.41px;top:106.57px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:113.64px" class="cls_004"><span class="cls_004">Province:</span></div>
<div style="position:absolute;left:310.94px;top:113.64px" class="cls_004"><span class="cls_004">1:</span></div>
<div style="position:absolute;left:89.60px;top:150.41px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:310.94px;top:150.41px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:156.78px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:377.41px;top:156.78px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:163.85px" class="cls_004"><span class="cls_004">2:</span></div>
<div style="position:absolute;left:310.94px;top:163.85px" class="cls_004"><span class="cls_004">3:</span></div>
<div style="position:absolute;left:89.60px;top:200.62px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:206.99px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:206.99px" class="cls_004"><span class="cls_004">Phone Type:</span></div>
<div style="position:absolute;left:377.41px;top:206.99px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:214.06px" class="cls_004"><span class="cls_004">4:</span></div>
<div style="position:absolute;left:89.60px;top:257.19px" class="cls_004"><span class="cls_004">Carrier:</span></div>
<div style="position:absolute;left:156.07px;top:257.19px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:257.19px" class="cls_004"><span class="cls_004">Country Code:</span></div>
<div style="position:absolute;left:377.41px;top:257.19px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:307.40px" class="cls_004"><span class="cls_004">Phone Number: </span></div>
<div style="position:absolute;left:310.94px;top:307.40px" class="cls_004"><span class="cls_004">Email:</span></div>
<div style="position:absolute;left:377.41px;top:307.40px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:252.95px;top:404.99px" class="cls_002"><span class="cls_002">Emergency Contact Details­II</span></div>
<div style="position:absolute;left:89.60px;top:453.78px" class="cls_004"><span class="cls_004">Salutation:</span></div>
<div style="position:absolute;left:156.07px;top:453.78px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:453.78px" class="cls_004"><span class="cls_004">First Name:</span></div>
<div style="position:absolute;left:377.41px;top:453.78px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:503.99px" class="cls_004"><span class="cls_004">Middle Name:</span></div>
<div style="position:absolute;left:156.07px;top:503.99px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:503.99px" class="cls_004"><span class="cls_004">Last Name:</span></div>
<div style="position:absolute;left:377.41px;top:503.99px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:554.19px" class="cls_004"><span class="cls_004">Gender:</span></div>
<div style="position:absolute;left:156.07px;top:554.19px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:554.19px" class="cls_004"><span class="cls_004">Relationship:</span></div>
<div style="position:absolute;left:377.41px;top:554.19px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:598.04px" class="cls_004"><span class="cls_004">Zip / Postal</span></div>
<div style="position:absolute;left:156.07px;top:604.40px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:604.40px" class="cls_004"><span class="cls_004">City:</span></div>
<div style="position:absolute;left:377.41px;top:604.40px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:611.47px" class="cls_004"><span class="cls_004">Code:</span></div>
<div style="position:absolute;left:89.60px;top:648.24px" class="cls_004"><span class="cls_004">State /</span></div>
<div style="position:absolute;left:310.94px;top:648.24px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:654.61px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:377.41px;top:654.61px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:661.68px" class="cls_004"><span class="cls_004">Province:</span></div>
<div style="position:absolute;left:310.94px;top:661.68px" class="cls_004"><span class="cls_004">1:</span></div>
<div style="position:absolute;left:89.60px;top:698.45px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:310.94px;top:698.45px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:704.81px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:377.41px;top:704.81px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:711.89px" class="cls_004"><span class="cls_004">2:</span></div>
<div style="position:absolute;left:310.94px;top:711.89px" class="cls_004"><span class="cls_004">3:</span></div>
<div style="position:absolute;left:89.60px;top:748.66px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:755.02px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:755.02px" class="cls_004"><span class="cls_004">Phone Type:</span></div>
<div style="position:absolute;left:377.41px;top:755.02px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:762.09px" class="cls_004"><span class="cls_004">4:</span></div>
</div>
<div style="border-left: 2px solid #9a9a9a;position:absolute;left:50%;margin-left:-297px;top:5112px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
</div>
<div style="position:absolute;left:89.60px;top:63.23px" class="cls_004"><span class="cls_004">Carrier:</span></div>
<div style="position:absolute;left:156.07px;top:63.23px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:310.94px;top:63.23px" class="cls_004"><span class="cls_004">Country Code:</span></div>
<div style="position:absolute;left:377.41px;top:63.23px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:89.60px;top:113.44px" class="cls_004"><span class="cls_004">Phone Number: </span></div>
<div style="position:absolute;left:310.94px;top:113.44px" class="cls_004"><span class="cls_004">Email:</span></div>
<div style="position:absolute;left:377.41px;top:113.44px" class="cls_004"><span class="cls_004"></span></div>
</div>
<div style="border-left: 2px solid #9a9a9a;position:absolute;left:50%;margin-left:-297px;top:5964px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
</div>
<div style="position:absolute;left:262.85px;top:83.24px" class="cls_002"><span class="cls_002">Co­Signer | Guarantor ­ I</span></div>
<div style="position:absolute;left:89.60px;top:132.03px" class="cls_004"><span class="cls_004">Salutation:</span></div>
<div style="position:absolute;left:156.07px;top:132.03px" class="cls_004"><span class="cls_004">'.$postArray['salutation_co'].'</span></div>
<div style="position:absolute;left:310.94px;top:132.03px" class="cls_004"><span class="cls_004">First Name:</span></div>
<div style="position:absolute;left:377.41px;top:132.03px" class="cls_004"><span class="cls_004">'.$postArray['first_name_co'].'</span></div>
<div style="position:absolute;left:89.60px;top:182.24px" class="cls_004"><span class="cls_004">Middle Name:</span></div>
<div style="position:absolute;left:156.07px;top:182.24px" class="cls_004"><span class="cls_004">'.$postArray['middle_name_co'].'</span></div>
<div style="position:absolute;left:310.94px;top:182.24px" class="cls_004"><span class="cls_004">Last Name:</span></div>
<div style="position:absolute;left:377.41px;top:182.24px" class="cls_004"><span class="cls_004">'.$postArray['last_name_co'].'</span></div>
<div style="position:absolute;left:89.60px;top:232.44px" class="cls_004"><span class="cls_004">Gender:</span></div>
<div style="position:absolute;left:156.07px;top:232.44px" class="cls_004"><span class="cls_004">'.$postArray['gender_co'].'</span></div>
<div style="position:absolute;left:310.94px;top:232.44px" class="cls_004"><span class="cls_004">Relationship:</span></div>
<div style="position:absolute;left:377.41px;top:232.44px" class="cls_004"><span class="cls_004">'.$postArray['relationship_co'].'</span></div>
<div style="position:absolute;left:89.60px;top:276.29px" class="cls_004"><span class="cls_004">Zip / Postal</span></div>
<div style="position:absolute;left:156.07px;top:282.65px" class="cls_004"><span class="cls_004">'.$postArray['zip_code_co'].'</span></div>
<div style="position:absolute;left:310.94px;top:282.65px" class="cls_004"><span class="cls_004">City:</span></div>
<div style="position:absolute;left:377.41px;top:282.65px" class="cls_004"><span class="cls_004">'.$postArray['state_co'].'</span></div>
<div style="position:absolute;left:89.60px;top:289.72px" class="cls_004"><span class="cls_004">Code:</span></div>
<div style="position:absolute;left:89.60px;top:326.49px" class="cls_004"><span class="cls_004">State /</span></div>
<div style="position:absolute;left:310.94px;top:326.49px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:332.86px" class="cls_004"><span class="cls_004">'.$postArray['city_co'].'</span></div>
<div style="position:absolute;left:377.41px;top:332.86px" class="cls_004"><span class="cls_004">'.$postArray['state_co'].'</span></div>
<div style="position:absolute;left:89.60px;top:339.93px" class="cls_004"><span class="cls_004">Province:</span></div>
<div style="position:absolute;left:310.94px;top:339.93px" class="cls_004"><span class="cls_004">1:</span></div>
<div style="position:absolute;left:89.60px;top:376.70px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:310.94px;top:376.70px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:383.06px" class="cls_004"><span class="cls_004">'.$postArray['address1_co'].'</span></div>
<div style="position:absolute;left:377.41px;top:383.06px" class="cls_004"><span class="cls_004">'.$postArray['address2_co'].'</span></div>
<div style="position:absolute;left:89.60px;top:390.14px" class="cls_004"><span class="cls_004">2:</span></div>
<div style="position:absolute;left:310.94px;top:390.14px" class="cls_004"><span class="cls_004">3:</span></div>
<div style="position:absolute;left:89.60px;top:426.91px" class="cls_004"><span class="cls_004">Street Address</span></div>
<div style="position:absolute;left:156.07px;top:433.27px" class="cls_004"><span class="cls_004">'.$postArray['address3_co'].'</span></div>
<div style="position:absolute;left:310.94px;top:433.27px" class="cls_004"><span class="cls_004">Phone Type:</span></div>
<div style="position:absolute;left:377.41px;top:433.27px" class="cls_004"><span class="cls_004">'.$postArray['address4_co'].'</span></div>
<div style="position:absolute;left:89.60px;top:440.34px" class="cls_004"><span class="cls_004">4:</span></div>
<div style="position:absolute;left:89.60px;top:483.48px" class="cls_004"><span class="cls_004">Carrier:</span></div>
<div style="position:absolute;left:156.07px;top:483.48px" class="cls_004"><span class="cls_004">'.$postArray['carrier_co'].'</span></div>
<div style="position:absolute;left:310.94px;top:483.48px" class="cls_004"><span class="cls_004">Country Code:</span></div>
<div style="position:absolute;left:377.41px;top:483.48px" class="cls_004"><span class="cls_004">'.$postArray['country_code_co'].'</span></div>
<div style="position:absolute;left:89.60px;top:533.69px" class="cls_004"><span class="cls_004">Phone Number: '.$postArray['phone_number_co'].'</span></div>
<div style="position:absolute;left:310.94px;top:533.69px" class="cls_004"><span class="cls_004">Email:</span></div>
<div style="position:absolute;left:377.41px;top:533.69px" class="cls_004"><span class="cls_004">'.$postArray['email1_co'].'</span></div>
</div>
<div style="border-left: 2px solid #9a9a9a;position:absolute;left:50%;margin-left:-297px;top:6816px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
</div>
<div style="position:absolute;left:298.91px;top:83.24px" class="cls_002"><span class="cls_002">Notes</span></div>
<div style="position:absolute;left:89.60px;top:132.03px" class="cls_004"><span class="cls_004">Notes:</span></div>
<div style="position:absolute;left:135.56px;top:132.03px" class="cls_004"><span class="cls_004">'.$postArray['notes'].'</span></div>
<div style="position:absolute;left:135.56px;top:167.39px" class="cls_004"><span class="cls_004"></span></div>
<div style="position:absolute;left:282.65px;top:202.04px" class="cls_002"><span class="cls_002">Custom Fields</span></div>
<div style="position:absolute;left:312.35px;top:230.32px" class="cls_004"><span class="cls_004">Test custom</span></div>
<div style="position:absolute;left:91.72px;top:236.69px" class="cls_004"><span class="cls_004">Test field</span></div>
<div style="position:absolute;left:158.90px;top:236.69px" class="cls_004"><span class="cls_004">_______________________</span></div>
<div style="position:absolute;left:378.82px;top:236.69px" class="cls_004"><span class="cls_004">_______________________</span></div>
<div style="position:absolute;left:312.35px;top:243.76px" class="cls_004"><span class="cls_004">field</span></div>
<div style="position:absolute;left:312.35px;top:268.51px" class="cls_004"><span class="cls_004">New Custom</span></div>
<div style="position:absolute;left:91.72px;top:274.87px" class="cls_004"><span class="cls_004">Test</span></div>
<div style="position:absolute;left:158.90px;top:274.87px" class="cls_004"><span class="cls_004">_______________________</span></div>
<div style="position:absolute;left:378.82px;top:274.87px" class="cls_004"><span class="cls_004">_______________________</span></div>
<div style="position:absolute;left:312.35px;top:281.94px" class="cls_004"><span class="cls_004">Field</span></div>
</div>
<div style="border-left: 2px solid #9a9a9a; position:absolute;left:50%;margin-left:-297px;top:7668px;width:595px;height:842px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
</div>
<div style="position:absolute;left:264.26px;top:69.09px" class="cls_002"><span class="cls_002">TERMS OF CONDITIONS</span></div>
<div style="position:absolute;left:96.67px;top:102.33px" class="cls_004"><span class="cls_004">I, the undersigned applicant, understand and authorize ApexLink and its agents to acquire an</span></div>
<div style="position:absolute;left:96.67px;top:115.76px" class="cls_004"><span class="cls_004">investigative consumer report. This authorization will enable the release of information from employers,</span></div>
<div style="position:absolute;left:96.67px;top:129.20px" class="cls_004"><span class="cls_004">previous/current landlords, and banking entities. This report may include the applicant’s credit history,</span></div>
<div style="position:absolute;left:96.67px;top:142.64px" class="cls_004"><span class="cls_004">past rental court search, criminal record, and sex offender registration listing. This investigation is only to</span></div>
<div style="position:absolute;left:96.67px;top:156.07px" class="cls_004"><span class="cls_004">be used in determining the applicant’s credit worthiness for screening purposes. Any information</span></div>
<div style="position:absolute;left:96.67px;top:169.51px" class="cls_004"><span class="cls_004">collected will remain confidential. ApexLink and its agents will compile information from sources that are</span></div>
<div style="position:absolute;left:96.67px;top:182.94px" class="cls_004"><span class="cls_004">deemed reliable; however, the accuracy of such information cannot be guaranteed. I will not hold</span></div>
<div style="position:absolute;left:96.67px;top:196.38px" class="cls_004"><span class="cls_004">ApexLink or its agents liable for any misuse or damages resulting from the improper use of the collected</span></div>
<div style="position:absolute;left:96.67px;top:209.81px" class="cls_004"><span class="cls_004">information.</span></div>
<div style="position:absolute;left:96.67px;top:255.07px" class="cls_004"><span class="cls_004">Important information about your rights under the Fair Credit reporting Act:</span></div>
<div style="position:absolute;left:96.67px;top:291.84px" class="cls_004"><span class="cls_004">(i) You have the right to request disclosure of the nature & scope of the investigation.</span></div>
<div style="position:absolute;left:96.67px;top:305.99px" class="cls_004"><span class="cls_004">(ii) You must be told if information in your file has been used against you.</span></div>
<div style="position:absolute;left:96.67px;top:320.13px" class="cls_004"><span class="cls_004">(iii) You have a right to know what is in your file, and this disclosure may be free.</span></div>
<div style="position:absolute;left:96.67px;top:334.27px" class="cls_004"><span class="cls_004">(iiii) You have the right to ask for a credit score (although you may have to submit a fee for this service).</span></div>
<div style="position:absolute;left:96.67px;top:348.41px" class="cls_004"><span class="cls_004">(iv) You have the right to dispute incomplete or inaccurate information. Consumer reporting agencies</span></div>
<div style="position:absolute;left:96.67px;top:361.85px" class="cls_004"><span class="cls_004">must correct inaccurate, incomplete, or unverifiable information.</span></div>
<div style="position:absolute;left:103.74px;top:417.71px" class="cls_004"><span class="cls_004">Agreed By:</span></div>
<div style="position:absolute;left:210.52px;top:417.71px" class="cls_004"><span class="cls_004">__________________</span></div>
<div style="position:absolute;left:361.85px;top:417.71px" class="cls_004"><span class="cls_004">Date:</span></div>
<div style="position:absolute;left:390.84px;top:417.71px" class="cls_004"><span class="cls_004">_____________________</span></div>
<div style="position:absolute;left:192.14px;top:474.29px" class="cls_005"><span class="cls_005">ApexLink Property Manager● </span><A HREF="javascript:;">support@apexlink.com  ●772­212­1950</A> </div>
</div>

</body>
</html>
';
//print_r($html);die;
        return $html;

    }

    public function createHTMLToPDF($replaceText){
       // ini_set('memory_limit','512');
        //define( 'WP_MAX_MEMORY_LIMIT', '256M' );
        $dompdf = new Dompdf();
        $dompdf->loadHtml($replaceText);
        //$dompdf->setPaper('A4', 'portrait');

        $dompdf->render();
        //echo"<pre>"; print_r($dompdf);die;
        $domain = getDomain();
        $path = "uploads/";

        $fileUrl = COMPANY_DIRECTORY_URL . '/'.$path.'/Ticket.pdf';
        $output = $dompdf->output();
        file_put_contents($fileUrl, $output);
        $fileUrl2 = "http://".$_SERVER['HTTP_HOST'] . '/company/'.$path.'/Ticket.pdf';
        return array('code' => 200, 'status' => 'success', 'data' => $fileUrl2,  'message' => 'Record retrieved successfully');
    }

    public function getpropdata() {

        try{
            $id = $_POST['id'];

            $query = $this->companyConnection->query("SELECT unit_id,user_id FROM lease_guest_card WHERE id=".$id);
            $data = $query->fetch();

            $query1 = $this->companyConnection->query("SELECT id,property_id,building_id                              
                                                                     FROM unit_details              
                                                     where unit_details.id=".$data['unit_id']);
            $data1 = $query1->fetch();
 $query2 = $this->companyConnection->query("SELECT *                              
                                                             FROM users              
                                                     where users.id=".$data['user_id']);
            $data2 = $query2->fetch();


     /*       $query2 = $this->companyConnection->query("SELECT building_detail. *
                                                           FROM building_detail                          
                            where building_detail.id=".$data1['building_id']);
            $data2 = $query2->fetch();

            $query3 = $this->companyConnection->query("SELECT  *                   
                                                           FROM general_property                          
                            where general_property.id=".$data1['property_id']);
            $data3 = $query3->fetch();*/

            return ['status'=>'success','code'=>200, 'data1'=> $data1,'data2'=>$data2];
        } catch (Exception $exception) {
           // return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($exception->getMessage());
        }
    }



}
$RenatlApplicationAjax = new RenatlApplicationAjax();