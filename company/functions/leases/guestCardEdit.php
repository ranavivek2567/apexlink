<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class GuestCardEditAjax extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
//        print_r($_REQUEST); die();
        echo json_encode($this->$action());

    }

    public function guestCardUserID(){
        try{
            $user_id = $_POST['user_id'];

            $query = $this->companyConnection->query("SELECT user_id, unit_id, expected_move_in FROM lease_guest_card where user_id=".$user_id);
            
            $data1 = $query->fetch();

            $query = $this->companyConnection->query("SELECT users. *,
                                                    hobbies.hobby as hobbiess,
                          tenant_ethnicity.title as ethnicity_name,
                           tenant_marital_status.marital as marital_status,
                           tenant_referral_source.referral as referrals,
                           tenant_veteran_status.veteran as veteran_name  
                                                    FROM users 
                         LEFT JOIN tenant_ethnicity ON users.ethnicity=tenant_ethnicity.id
                        LEFT JOIN tenant_marital_status ON users.maritial_status=tenant_marital_status.id
                        LEFT JOIN tenant_veteran_status ON users.veteran_status=tenant_veteran_status.id
                        LEFT JOIN tenant_referral_source ON users.referral_source=tenant_referral_source.id
                         LEFT JOIN hobbies ON users.hobbies=hobbies.id
                                                    where users.id=".$data1['user_id']);
            $data = $query->fetch();
            if(!empty($data1['unit_id'])) {
                $query3 = $this->companyConnection->query("SELECT unit_details. *, property_name 
                                                            FROM unit_details
                                                            left join general_property on unit_details.property_id=general_property.id
                                                            where unit_details.id=" . $data1['unit_id']);
                $data2 = $query3->fetch();
            }
            else{
                $data2="";
            }

            $html='';
            $getEmergencyInfo = $this->getEmergencyInfo($user_id);
            $getNotes = $this->getNotes($user_id);
            $getPhoneInfo = $this->getPhoneInfo($user_id);
            $ViewEmergencyInfo=$this->ViewEmergencyInfo($data1['user_id']);
            $viewNotes=$this->viewNotes($data1['user_id']);

            return ['status'=>'success','code'=>200, 'data'=> $data, 'data2'=>$data2,'data1'=>$data1, 'html'=>$html, 'emergencyInfo'=>$getEmergencyInfo, 'ViewEmergencyInfo'=>$ViewEmergencyInfo, 'notes'=>$getNotes,'viewNotes'=>$viewNotes,'phonetype'=>$getPhoneInfo];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }



    public function getEmergencyInfo($tenant_id)
    {
        $getEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='".$tenant_id."' and parent_id=0")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();
        $html = "";
        if(empty($getEmergencyInfo))
        {
            $html .=            '<div class="row tenant-emergency-contact">
                                                    <div class="form-outer">

                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Emergency Contact Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="emergency" name="emergency_contact_name[]" maxlength="18">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Relationship</label>
                                                            <select class="form-control" id="relationship"
                                                                    name="emergency_relation[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Daughter</option>
                                                                <option value="2">Father</option>
                                                                <option value="3">Friend</option>
                                                                <option value="4">Mother</option>
                                                                <option value="5">Owner</option>
                                                                <option value="6">Partner</option>
                                                                <option value="7">Son</option>
                                                                <option value="8">Spouse</option>
                                                                <option value="9">Other</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 countycodediv">
                                                            <label>Country Code</label>
                                                            <select class="form-control" name="emergency_country[]">
                                                                <option value="">Select</option>
                                                                <option value="1">test</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">

                                                            <label>Phone</label>
                                                            <input class="form-control capsOn phone_format" type="text"
                                                                   id="phoneNumber"
                                                                   name="emergency_phone[]">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="clearfix"></div>

                                                    </div>

                                               
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Email</label>
                                                            <input class="form-control" type="text" 
                                                                   name="emergency_email[]" maxlength="100">
                                                                      <a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                                     <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    

                                                </div>';


        }
        else
        {
            $i = 0;
            foreach($getEmergencyInfo as $emergencyInfo)
            {
                $html .= '<div class="form-outer form-outer2">
                                                <div class="row tenant-emergency-contact">
                                                    <div class="col-sm-2">
                                                        <label>Emergency Contact Name</label>
                                              <input class="form-control" type="text" value="'.$emergencyInfo['emergency_contact_name'].'" name="emergency_contact_name[]" maxlength="18">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Relationship</label>
                                                        <select class="form-control" name="emergency_relation[]">';
                $relationArr = array("" => "Select","1" => "Daughter","2" => "Father","3" => "Friend","4" => "Mother","5" => "Owner","6" => "Partner","7" => "Son","8" => "Spouse","9" => "Other");
                foreach ($relationArr as $key => $value) {
                    if ($key == $emergencyInfo['emergency_relation']) {
                        $html .= '<option value="'.$key.'" selected>'.$value.'</option>';
                    }else{
                        $html .= '<option value="'.$key.'">'.$value.'</option>';
                    }
                }

                $html .= '</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="emergency_country[]">';
                foreach($countryCode as $code){
                    if($code['id']==$emergencyInfo['emergency_country_code']){
                        $html .= "<option value=".$code['id']." data-id='".$code['code']."' selected>".$code['name']."</option>";
                    }else{
                        $html .= "<option value=".$code['id']." data-id='".$code['code']."'>".$code['name']."</option>";
                    }
                }
                $html .='</select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Phone Number</label>
                                            <input class="form-control phone_format" type="text" maxlength="12" name="emergency_phone[]" value="'.$emergencyInfo['emergency_phone_number'].'">
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>Email</label>
                                                        <input class="form-control add-input" type="text" value="'.$emergencyInfo['emergency_email'].'" name="emergency_email[]" maxlength="100">';

                if($i!=0)
                {


                    $html .= '<a class="add-icon remove-emergency-contant"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                }
                else
                {
                    $html .= '<a class="add-icon add-emergency-contant" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>';

                    $html .= '<a class="add-icon remove-emergency-contant" style="display:none"><i
                                                                class="fa fa-minus-circle " aria-hidden="true "></i></a>';

                }


                $html .= '</div>
                                </div>';

                $i++;
            }


        }

        return $html;

    }


    public function getNotes($tenant_id){
        $getNotes = $this->companyConnection->query("SELECT * FROM tenant_notes WHERE user_id ='".$tenant_id."'")->fetchAll();
//        dd($getNotes);
        $html = "";
        if(empty($getNotes))
        {
            $html .=            '<div class="form-outer add_notes">
                                        <div class="form-hdr">
                                            <h3>Notes </h3>
                                        </div>
                                        <div class="form-data">
                                            <form id="notes_form">

                                                <div class="chargeNoteHtml">
                                                    <div class="row">
                                                   <div class="form-outer guest-notes-clone-divclass" id="guest-notes-clone-div">
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                        <div class="notes_date_right_div">
                                                            <textarea class="form-control add-input chargeNoteClone notes_date_right capsOn" name="chargeNote[]"></textarea></div>
                                                        
                                                      
                                                        <a class="add-icon guest-notes-minus" href="javascript:;" style="display:none;">

                                                            <i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                  
                                                      
                                                        <a class="add-icon guest-notes-plus" href="javascript:;">

                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                      
                                                                
                                                        </div>
                                                     </div>
                                                   </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>';

        } else {
            $i = 1;
            foreach($getNotes as $notes) {
                $html .= '<div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Notes</h3>
                                        </div>
                                        <div class="form-data">
                                            <form id="notes_form">

                                                <div class="chargeNoteHtml">
                                                <div class="form-outer guest-notes-clone-divclass" id="guest-notes-clone-div">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                        <div class="notes_date_right_div">
                                                            <textarea class="form-control add-input chargeNoteClone notes_date_right capsOn" value="'.$notes['notes'].'" name="chargeNote[]">'.$notes['notes'].'</textarea> </div>';

                if($i!=1)
                {
                    $html.= '<a class="add-icon guest-notes-minus" href="javascript:;">

                                                            <i class="fa fa-minus-circle" aria-hidden="true"></i></a>';
                }
                else
                {
                    $html .= '<a class="add-icon guest-notes-plus" href="javascript:;">

                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                }
                $html .= '</div>
                                                    </div>
                                                  </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>';
                $i++;
            }
        }
        return $html;
    }


    public function getPhoneInfo($user_id)
    {

        $getPhoneInfo = $this->companyConnection->query("SELECT * FROM tenant_phone WHERE user_id ='".$user_id."'")->fetchAll();
        $getPhoneType = $this->companyConnection->query("SELECT * FROM phone_type")->fetchAll();
        $getCarrierInfo = $this->companyConnection->query("SELECT * FROM carrier")->fetchAll();
        $countryCode = $this->companyConnection->query("SELECT * FROM countries")->fetchAll();

        $html = "";

        if(empty($getPhoneInfo))
        {
            $html .= '<div class=" primary-tenant-phone-row2">

                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-2 clear">
                                                    
                                                        <label>Phone Type</label>
                                                        <select class="form-control" name="phoneType[]">
                                                            <option value="">Select</option>
                                                            <option value="1">mobile</option>
                                                            <option value="2">work</option>
                                                            <option value="3">Fax</option>
                                                            <option value="4">Home</option>
                                                            <option value="5">Other</option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-2 ">
                                                        <label>Carrier <em class="red-star">*</em></label>
                                                        <select class="form-control" name="carrier[]">';
            foreach($getCarrierInfo as $carrierInfo){
                $html .= "<option value=".$carrierInfo['id'].">".$carrierInfo['carrier']."</option>";
            }
            $html .= '</select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>
                                                     <div class="col-sm-12 col-md-1 ext_phone" style="display: none;">
                                                            <label>Extension</label>
                                                            <input name="Extension[]" class="form-control" type="text" placeholder="Eg: + 161">
                                                        </div>
                                                        <div class="col-sm-3 col-md-2 ">
                                                        <label>Phone Number <em class="red-star">*</em></label>
                                                        <input class="form-control capsOn add-input phone_format" type="text"
                                                               name="phoneNumber[]">
                                                        
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-2 countycodediv">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="countryCode[]">';
            foreach($countryCode as $code){
                $html .= "<option value=".$code['id']." data-id='".$code['code']."'>".$code['name']."</option>";
            }
            $html .=     '</select>
                                                        <a class="add-icon" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <a class="add-icon" href="javascript:;" ><i
                                                                    class="fa fa-minus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>';
        }
        else
        {
            $i = 0;
            foreach($getPhoneInfo as $phoneInfo)
            {

                $html .= '<div class="primary-tenant-phone-row2">
                                  <div class="col-sm-2 clear">
                                      <label>Phone Type</label>
                                     <select class="form-control" name="phoneType[]">';

                foreach($getPhoneType as $phoneType){

                    if($phoneType['id']==$phoneInfo['phone_type'])
                    {
                        $selected = "selected=selected";
                    }
                    else
                    {
                        $selected = "";
                    }



                    $html .= "<option value=".$phoneType['id']." ".$selected.">".$phoneType['type']."</option>";
                }


                $html.=     '</select></div>

                                      
                                  
                                  <div class="col-sm-2">
                                      <label>Phone Number</label>
                                      
                                      <input class="form-control phone_format" maxlength="12" type="text" name="phoneNumber[]" value="'.$phoneInfo['phone_number'].'">
                                      <input type="hidden" name="hidden_id_phone[]" class="id_tenFone" value="'.$phoneInfo['id'].'"></div>
                                      <div class="col-md-1 ext_phone">
                                                            <label>Extension</label>
                                                            <input name="Extension[]" class="form-control" type="text" placeholder="Eg: + 161" value="'.$phoneInfo['work_phone_extension'].'">
                                                        ';

                $html.= '
                                     </div>
                                      <div class="col-sm-2">
                                      <label>Carrier <em class="red-star">*</em></label>
                                      <select class="form-control" name="carrier[]">
                                      ';
                foreach($getCarrierInfo as $carrierInfo)
                {
                    if($carrierInfo['id']==$phoneInfo['carrier'])
                    {
                        $selected = "selected=selected";
                    }
                    else
                    {
                        $selected = "";
                    }

                    $html .= "<option value=".$carrierInfo['id']." ".$selected.">".$carrierInfo['carrier']."</option>";
                }
                $html.=     '</select></div>



                                  <div class="col-sm-2">
                                      <label>Country Code</label>
                                         <select class="form-control  add-input" name="countryCode[]">';
                foreach($countryCode as $code){
                    if($code['id']==$phoneInfo['country_code']){
                        $html .= "<option value=".$code['id']." data-id='".$code['code']."' selected>".$code['name']."</option>";
                    }else{
                        $html .= "<option value=".$code['id']." data-id='".$code['code']."'>".$code['name']."</option>";
                    }
                }

                $html.=     '</select></div>';

                if($i==0){
                    $html .=  '<a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>';
                    $html .='<a class="add-icon" href="javascript:;" style="display:inline;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>';
                }


                if($i!=0){

                    $html .='<a class="add-icon" href="javascript:;" style=""><i class="fa fa-minus-circle" aria-hidden="true"></i></a>';
                }

                $html.= '</div>
                              </div>';

                $i++;
            }
        }
        return $html;
    }

//    public function getSsnData($ssn_id)
//    {
//        $html = '';
//        $count = count($ssn_id);
//        if (isset($ssn_id[0]) && $ssn_id[0] == "") {
//            $html .= '<div class="multipleSsn col-sm-12">
//                          <label>SSN/SIN/ID</label>
//                          <input class="form-control add-input" type="text" name="ssn[]"  maxlength="10"/>
//                          <a class="add-icon ssn-remove-sign" href="javascript:;" style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
//                          <a class="add-icon ssn-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
//                        </div>';
//        }else{
//            for($i = 0; $i < count($ssn_id); $i++){
//                if ($ssn_id[$i] !="") {
//                    $html .= '<div class="multipleSsn col-sm-12">
//                          <label>SSN/SIN/ID</label>
//                          <input class="form-control add-input" type="text" name="ssn[]"  maxlength="10" value="'.$ssn_id[$i].'">';
//                    if ($i == 0) {
//                        $html .= '<a class="add-icon ssn-plus-sign" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
//                                <a class="add-icon ssn-remove-sign" href="javascript:;" style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>';
//                    }else{
//                        $html .= '<a class="add-icon ssn-remove-sign" href="javascript:;" style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>';
//                    }
//                    $html .= '</div>';
//
//                }
//            }
//
//        }
//        return $html;
//
//    }










    public function update()
    {
        try {

            $data = $_POST['formData'];
            $data=postArray($data);
            $unit_id=$_POST['unit_id'];


//            $formData2=$_POST['formData2'];
//            $formData2=postArray($formData2);
//            $notes=$_POST['notes'];
//            $notes=postArray($notes);

            $data1['salutation']=$data['salutation'];
            $data1['first_name']=$data['first_name'];
            $data1['middle_name']=$data['middle_name'];
            $data1['last_name']=$data['last_name'];
            $data1['maiden_name']=$data['maiden_name'];
            $data1['nick_name']=$data['nick_name'];
            $data1['gender']=$data['gender'];
          //  $data1['email']=$data['email'];
            $data1['address1']=$data['address1'];
            $data1['address2']=$data['address2'];
            $data1['address3']=$data['address3'];
            $data1['address4']=$data['address4'];
            $data1['zipcode']=$data['zipcode'];
            $data1['country']=$data['country'];
            $data1['state']=$data['state'];
            $data1['city']=$data['city'];
            $data1['referral_source']=$data['referral_source'];
            $data1['ethnicity']=$data['ethnicity'];
            $data1['maritial_status']=$data['martial_status'];
            $data1['veteran_status']=$data['veteran_status'];
            //$data1['user_type']="6";
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data1,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                //$data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                //Save Data in Company Database
//                dd($data);
                $query = $this->companyConnection->query("SELECT user_id FROM lease_guest_card where id='$unit_id'");
                $data2 = $query->fetch();
                $sqlData = createSqlUpdateCase($data1);

                /* dplicate validations start */

                $columns = ['users.first_name', 'users.last_name', 'users.middle_name', 'users.ssn_sin_id', 'users.address1', 'users.address2'];
                if (is_array($data['phoneNumber'])) {
                    $dataPhone = $data['phoneNumber'][0];
                }else{
                    $dataPhone = $data['phoneNumber'];
                }
                $where = [
                    ['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $data['first_name']],
                    ['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $data["last_name"]],
                    ['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $data["middle_name"]],
                    ['table' => 'users', 'column' => 'address1', 'condition' => '=', 'value' => $data["address1"]],
                    ['table' => 'users', 'column' => 'address2', 'condition' => '=', 'value' => $data["address2"]],
                    ['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $dataPhone]
                ];
                $duplicate_record_check=checkMultipleDuplicatesEdit($this->companyConnection, $data['email'],$data['phoneNumber'],'','','','',$data2['user_id']);
                //dd($duplicate_record_check);
                if(isset($duplicate_record_check)){
                    if($duplicate_record_check['code'] == 503){
                        echo json_encode(array('code' => 400, 'status' => 'errorr', 'message' => $duplicate_record_check['message']));
                        die();
                    }
                }

                /* dplicate validations end */


              //  $query = "INSERT INTO users (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $query ="UPDATE users SET " . $sqlData['columnsValuesPair'] . " where users.id=".$data2['user_id'];

                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($sqlData['data']);

//expected_movein

//                $expected_move_in['expected_move_in']=mySqlDateFormat($data['expected_move_in'],$unit_id,$this->companyConnection);
//               // dd($expected_move_in['expected_move_in']);
//                $sqlData = createSqlColValPair($expected_move_in);
//                $query ="UPDATE lease_guest_card SET " . $sqlData['columnsValuesPair'] . " where id=".$unit_id;
//
//                $stmt = $this->companyConnection->prepare($query);
//                $stmt->execute();

//
//                //SSN ID
                if(is_array($data['ssn_sin_id']))
                {
                    $count=count($data['ssn_sin_id']);
                    for($i=0;$i<$count;$i++)
                    {

                        if($i==0)
                        {

                            $ssn['ssn']=$data['ssn_sin_id'][$i];

                            $data10['ssn_sin_id']=$data['ssn_sin_id'][$i];
                            $sqlData = createSqlColValPair($data10);
                            $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$data2['user_id'];
                            $stmt = $this->companyConnection->prepare($query9);
                            $stmt->execute();

                        }

                        else{

                            $ssn['ssn']=$data['ssn_sin_id'][$i];

                            $data11['ssn']=$data['ssn_sin_id'][$i];
                            $sqlData = createSqlColValPair($data11);
                            $query9 = "UPDATE tenant_ssn_id SET " . $sqlData['columnsValuesPair'] . " where user_id=".$unit_id;
                            $stmt = $this->companyConnection->prepare($query9);
                            $stmt->execute();

                        }
//
                    }
                }
//


                //Email

                if(is_array($data['email']))
                {


                    $count=count($data['email']);
                    for($i=0;$i<$count;$i++)
                    {
                            $data3['email']=$data['email'][0];
                            $sqlData = createSqlColValPair($data3);
                            $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$data2['user_id'];
                            $stmt = $this->companyConnection->prepare($query9);
                            $stmt->execute();
                            $n=$i+1;
                            $data4['email'.$n]=$data['email'][$i];
                            $sqlData = createSqlColValPair($data4);
                            $query8 = "UPDATE lease_guest_card SET " . $sqlData['columnsValuesPair'] . " where user_id=".$data2['user_id'];
                            $stmt = $this->companyConnection->prepare($query8);
                            $stmt->execute();
             }
                }
//
                /*Hobbies*/
                if(isset($data['hobbies'])) {
                    if (is_array($data['hobbies'])) {
                        $datahobby1['hobbies'] = $data['hobbies'];
                        $sqlData = createSqlColValPair($datahobby1);
                        $query8 = "UPDATE tenant_hobbies SET " . $sqlData['columnsValuesPair'] . " where id=" . $unit_id;
                        $stmt = $this->companyConnection->prepare($query8);
                        $stmt->execute();

                    }
                }
//

               // emergencydetails

                    $emergencydetail['emergency_contact_name']=$data['emergency_contact_name'];
                    $emergencydetail['emergency_relation']=$data['emergency_relation'];
                    $emergencydetail['emergency_country_code']=$data['emergency_country'];
                    $emergencydetail['emergency_phone_number']=$data['emergency_phone'];
                    $emergencydetail['emergency_email']=$data['emergency_email'];

                    $sqlData = createSqlColValPair($emergencydetail);
                    $query8 = "UPDATE emergency_details SET " . $sqlData['columnsValuesPair'] . " where id=".$data2['user_id'];
                    $stmt = $this->companyConnection->prepare($query8);
                $stmt->execute();

//
//                }
//                //Phonetype


                if (is_array($data['phoneType'])){
                    $data5['phone_type'] = $data['phoneType'][0];
                    $data5['carrier'] = $data['carrier'][0];
                    $data5['country_code'] = $data['countryCode'][0];
                    $data5['phone_number'] = $data['phoneNumber'][0];
                    $data5['updated_at'] =  date('Y-m-d H:i:s');
//                    dd($data5);
//                    $data12['guestcard_contact'] = $data['phoneNumber'][0];//In Array CARRIER
                    $sqlData = createSqlColValPair($data5);
                    $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$data2['user_id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();

                    $count=count($data['phoneType']);
                    for($i=1;$i<$count;$i++) {
                        $data4['phone_type'] = $data['phoneType'][$i];
                        $data4['carrier'] = $data['carrier'][$i];
                        $data4['country_code'] = $data['countryCode'][$i];
                        $data4['phone_number'] = $data['phoneNumber'][$i];//In Array CARRIER
                        $data4['created_at'] =  date('Y-m-d H:i:s');
                        $data4['updated_at'] =  date('Y-m-d H:i:s');
                       $data4['user_id']=$data2['user_id'];

                        $sqlData = createSqlColVal($data4);
                        $query9 = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")" ;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute($data4);


                    }
                }else{
                    $data4['phone_type'] = $data['phoneType'];//In Array PHONETYPE
                    $data4['carrier'] = $data['carrier'];
                    $data4['country_code'] = $data['countryCode'];
                    $data4['phone_number'] = $data['phoneNumber'];//In Array CARRIER
                  //  $data4['created_at'] =  date('Y-m-d H:i:s');
                    $data4['updated_at'] =  date('Y-m-d H:i:s');
                    $sqlData = createSqlColValPair($data4);
                    $query = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$data2['user_id'];
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute();
                }


                if(is_array($data['chargeNote']))
                {


                    $count=count($data['chargeNote']);
                    for($i=0;$i<$count;$i++)
                    {
                        $data3['notes']=$data['chargeNote'][$i];
                        $sqlData = createSqlColValPair($data3);
                        $query9 = "UPDATE tenant_notes SET " . $sqlData['columnsValuesPair'] . " where user_id=".$data2['user_id'];
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();

                    }
                }
                else{
                    $data3['notes']=$data['chargeNote'];
                    $sqlData = createSqlColValPair($data3);
                    $query9 = "UPDATE tenant_notes SET " . $sqlData['columnsValuesPair'] . " where user_id=".$data2['user_id'];
                    $stmt = $this->companyConnection->prepare($query9);
                    $stmt->execute();


                }



//                $this->unitRowRecord($last_id,$unit_id);
//                $this->addEmergencyDetails($last_id,$formData2);
//                $this->addnotes($last_id,$notes);

                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }















    public function referralSourceData(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM tenant_referral_source");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function guestPhoneType(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM phone_type");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function carrierdata(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM carrier");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function guestCountries(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM countries");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function guestEthnicity(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM tenant_ethnicity");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function guestMaritalStatus(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM tenant_marital_status");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function guestHobbies(){
        try{
            $guest_id = $_POST['guest_id'];
            $query = $this->companyConnection->query("SELECT * FROM hobbies");
            $data = $query->fetchAll();
            $query2 = $this->companyConnection->query("SELECT id FROM tenant_hobbies where user_id='$guest_id'");
            $data2 = $query2->fetchAll();
            $data3 = [];
            foreach ($data2 as $val){
                $data3[] = $val['id'];
            }
            return ['status'=>'success','code'=>200, 'data'=> $data ,'hobby'=>$data3];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function guestVeteranStatus(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM tenant_veteran_status");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Property data DDL
     */
    public function guestProperty(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM general_property");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }



    public function ViewEmergencyInfo($tenant_id) {
        $ViewEmergencyInfo = $this->companyConnection->query("SELECT * FROM emergency_details WHERE user_id ='" . $tenant_id . "'")->fetchAll();

        $html2 = "";


        if (empty($ViewEmergencyInfo['emergency_contact_name'])) {

            $html2 .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                    <span></span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Email :</label>
                                                                    <span></span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                    <span></span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';
        } else {

            foreach ($ViewEmergencyInfo as $key => $value) {


                if (!empty($value['emergency_relation'])) {
                    $e_relation = $value['emergency_relation'];
                    // $vendor_cred_notice_period = [];

                    if (!empty($e_relation)) {
                        $emergency_rel = emergencyRelation($e_relation);

                        $value['emergency_relation'] = $emergency_rel;
                    } else {
                        $value['emergency_relation'] = 'N/A';
                    }
                } else {
                    $value['emergency_relation'] = 'N/A';
                }


                if(isset($value['emergency_country_code']) && !empty($value['emergency_country_code'])) {
                    $e_country_code = $value['emergency_country_code'];
                    if (!empty($e_country_code)){

                                $query = 'SELECT code FROM countries WHERE id="'.$e_country_code.'"';
                        $value['emergency_country_code'] = $this->companyConnection->query($query)->fetch();
                        $html2 .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span>' . $value['emergency_contact_name'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span>' . $value['emergency_country_code']['code'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                     <span>' . $value['emergency_phone_number'] . '</span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Email :</label>
                                                                   <span>' . $value['emergency_email'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                     <span>' . $value['emergency_relation'] . '</span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';

                    } else {
                        $value['emergency_country_code'] = '';
                        $html2 .= '<div class="detail-outer detail-outer-loop">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Name :</label>
                                                                    <span>' . $value['emergency_contact_name'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Country Code :</label>
                                                                    <span>' . $value['emergency_country_code'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Phone:</label>
                                                                     <span>' . $value['emergency_phone_number'] . '</span>
                                                                </div>
                                                                                                               
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Email :</label>
                                                                   <span>' . $value['emergency_email'] . '</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Emergency Contact Relation :</label>
                                                                     <span>' . $value['emergency_relation'] . '</span>
                                                                </div>
                                                                                                                 
                                                            </div>
                                                        </div>
                                                    </div>';
                    }

                } else {
                    $value['emergency_country_code'] = '';
                }



                $value = getUnserializedData($value,'true');



            }
        }

        return $html2;
    }








    public function viewNotes($unit_id){
        $viewNotes = $this->companyConnection->query("SELECT * FROM tenant_notes WHERE user_id ='".$unit_id."'")->fetchAll();
//        dd($getNotes);
        $html = "";
        if(empty($viewNotes))
        {
            $html .=            '  <div class="form-outer ">
                                       <div class="">
                                                
                                             </div>
                                          </div>
                                               ';

        } else {

            foreach($viewNotes as $notes)
            {
                $html .= '      <div  value="'.$notes['notes'].'" >'.$notes['notes'].'</div>
                                      ';

            }
        }
        return $html;
    }





}







$GuestCardEditAjax = new GuestCardEditAjax();