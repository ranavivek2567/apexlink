<?php
/**
 * Created by PhpStorm.
 * User: ShuklaNisha
 * Date: 23-Jan-19
 * Time: 11:44 AM
 */

include(ROOT_URL . "/config.php");

include_once(COMPANY_DIRECTORY_URL . "/helper/helper.php");
include_once(COMPANY_DIRECTORY_URL . "/helper/MigrationCompanySetup.php");

class GuestCardAjax extends DBConnection
{

    public function __construct()
    {
        parent::__construct();
        $action = $_REQUEST['action'];
//        print_r($_REQUEST); die();
        echo json_encode($this->$action());

    }

    public function referralSourceData(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM tenant_referral_source");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function guestPhoneType(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM phone_type");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function carrierdata(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM carrier");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function guestCountries(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM countries");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function guestEthnicity(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM tenant_ethnicity ORDER BY title ASC ");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function guestMaritalStatus(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM tenant_marital_status");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    public function guestHobbies(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM hobbies");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }
    public function guestVeteranStatus(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM tenant_veteran_status");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Insert data to general information guestcard table
     * @return array
     */
    public function insert()
    {
        try {
            $expected_move_in=$_POST['expected_move_in'];
            $data = $_POST['formData'];
            $data=postArray($data);
            $unit_id=$_POST['unit_id'];
            $formData2=$_POST['formData2'];
            $formData2=postArray($formData2);
            $notes=$_POST['notes'];
            $notes=postArray($notes);

            /* dplicate validations start */

            $columns = ['users.first_name', 'users.last_name', 'users.middle_name', 'users.ssn_sin_id', 'users.address1', 'users.address2'];
            if (is_array($data['phoneNumber'])) {
                $dataPhone = $data['phoneNumber'][0];
            }else{
                $dataPhone = $data['phoneNumber'];
            }
            $where = [
                ['table' => 'users', 'column' => 'first_name', 'condition' => '=', 'value' => $data["first_name"]],
                ['table' => 'users', 'column' => 'last_name', 'condition' => '=', 'value' => $data["last_name"]],
                ['table' => 'users', 'column' => 'middle_name', 'condition' => '=', 'value' => $data["middle_name"]],
                ['table' => 'users', 'column' => 'address1', 'condition' => '=', 'value' => $data["address1"]],
                ['table' => 'users', 'column' => 'address2', 'condition' => '=', 'value' => $data["address2"]],
                ['table' => 'users', 'column' => 'phone_number', 'condition' => '=', 'value' => $dataPhone]
            ];
           // dd($data);
            $duplicate_record_check=checkMultipleDuplicates($this->companyConnection, $data['email'],$data['phoneNumber'],'','','','');
            //dd($duplicate_record_check);
            if(isset($duplicate_record_check)){
                if($duplicate_record_check['code'] == 503){
                    echo json_encode(array('code' => 400, 'status' => 'errorr', 'message' => $duplicate_record_check['message']));
                    die();
                }
            }

            /* dplicate validations end */

            $data1['salutation']=$data['salutation'];
            $data1['first_name']=$data['first_name'];
            $data1['middle_name']=$data['middle_name'];
            $data1['last_name']=$data['last_name'];
            $data1['name']=$data['first_name'].' '.$data['last_name'];
            $data1['maiden_name']=$data['maiden_name'];
            $data1['nick_name']=$data['nick_name'];
            if(isset($data['gender'])&& $data['gender']!="")
            {
                $data1['gender']=$data['gender'];
            }
            $data1['address1']=$data['address1'];
            $data1['address2']=$data['address2'];
            $data1['address3']=$data['address3'];
            $data1['address4']=$data['address4'];
            $data1['zipcode']=$data['zipcode'];
            $data1['country']=$data['country'];
            $data1['state']=$data['state'];
            $data1['city']=$data['city'];
            $data1['referral_source']=$data['referral_source'];
            $data1['ethnicity']=$data['ethnicity'];
            $data1['maritial_status']=$data['martial_status'];
            $data1['veteran_status']=$data['veteran_status'];
            $data1['user_type']="6";
            $required_array = ['first_name','last_name','carrier','email','phoneNumber'];
            /* Max length variable array */
            $maxlength_array = [];
            //Min length variable array
            $minlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation

            $err_array = validation($data,$this->conn,$required_array,$maxlength_array,$minlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {

                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');

                //Save Data in Company Database
//                dd($data);

                $sqlData = createSqlColVal($data1);
                $query = "INSERT INTO users (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($data1);
                $last_id=$this->companyConnection->lastInsertId();
                $tenant['user_id']=$last_id;
                $tenant['status']='1';
                $tenant['expected_move_in']=mySqlDateFormat($expected_move_in, null, $this->companyConnection);
                $sqlData = createSqlColVal($tenant);
                $query = "INSERT INTO lease_guest_card (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($tenant);

                //SSN ID
                if(is_array($data['ssn_sin_id']))
                {
                    $count=count($data['ssn_sin_id']);
                    for($i=1;$i<$count;$i++)
                    {
                        $ssn['ssn']=$data['ssn_sin_id'][$i];
                        $ssn['user_id']=$last_id;
                       // print_r($ssn['user_id']);die;
                        $data10{'ssn_sin_id'}=$data['ssn_sin_id'][0];
                        $sqlData = createSqlColValPair($data10);
                        $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();
                        $sqlData = createSqlColVal($ssn);
                        $query = "INSERT INTO tenant_ssn_id (".$sqlData['columns'].") VALUES (".$sqlData['columnsValues'].")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($ssn);

                    }
                }
                else{

                    $data10['ssn_sin_id']=$data['ssn_sin_id'];
                    $sqlData = createSqlColValPair($data10);
                    $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                    $stmt = $this->companyConnection->prepare($query9);
                    $stmt->execute();

                }
                //Email
                if(is_array($data['email']))
                {
                    $count=count($data['email']);
                    for($i=0;$i<$count;$i++)
                    {
                        $next=$i+1;
                        $data2['email'.$next]=$data['email'][$i];
                        $data3{'email'}=$data['email'][0];
                        $sqlData = createSqlColValPair($data3);
                        $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();
                        $sqlData = createSqlColValPair($data2);
                        $query = "UPDATE lease_guest_card SET " . $sqlData['columnsValuesPair'] ." where user_id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute();
                    }
                }
                else{
                    $data3{'email'}=$data['email'];
                    $sqlData = createSqlColValPair($data3);
                    $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                    $stmt = $this->companyConnection->prepare($query9);
                    $stmt->execute();

                }
                //Hobbies
                if(isset($data['guestHobbies'])) {
                    if (is_array($data['guestHobbies'])) {
                        $count = count($data['guestHobbies']);
                        for ($i = 0; $i < $count; $i++) {
                            $datahobby['hobby'] = $data['guestHobbies'][$i];
                            $datahobby['user_id'] = $last_id;
                            $datahobby1{'hobbies'} = $data['guestHobbies'][0];
                            $sqlData = createSqlColValPair($datahobby1);
                            $query8 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" . $last_id;
                            $stmt = $this->companyConnection->prepare($query8);
                            $stmt->execute();
                            $sqlData = createSqlColVal($datahobby);
                            $query = "INSERT INTO tenant_hobbies (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                            $stmt = $this->companyConnection->prepare($query);
                            $stmt->execute($datahobby);

                        }
                    } else {
                        $datahobby1{'hobbies'} = $data['guestHobbies'];
                        $sqlData = createSqlColValPair($datahobby1);
                        $query8 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=" . $last_id;
                        $stmt = $this->companyConnection->prepare($query8);
                        $stmt->execute();
                    }
                }
                //Phonetype
                if (is_array($data['phoneType'])){
                    $count=count($data['phoneType']);
                    for($i=0;$i<$count;$i++) {
                        $data4['phone_type'] = $data['phoneType'][$i];
                        $data4['carrier'] = $data['carrier'][$i];
                        $data4['country_code'] = $data['countryCode'][$i];
                        $data4['phone_number'] = $data['phoneNumber'][$i];//In Array CARRIER
                        $data4['work_phone_extension'] = $data['Extension'][$i];
                        $data4['created_at'] =  date('Y-m-d H:i:s');
                        $data4['updated_at'] =  date('Y-m-d H:i:s');
                        $data4['user_id']=$last_id;
                        $data5['phone_type'] = $data['phoneType'][0];
                        $data5['carrier'] = $data['carrier'][0];
                        $data5['country_code'] = $data['countryCode'][0];
                        $data5['phone_number'] = $data['phoneNumber'][0];
                        $data5['work_phone_extension'] = $data['Extension'][0];
                        $data12['guestcard_contact'] = $data['phoneNumber'][0];//In Array CARRIER
                        $sqlData = createSqlColVal($data4);
                        $query = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data4);

                        $sqlData = createSqlColValPair($data5);
                        $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();
                        $data12['referral_source']=$data['referral_source'];
                        $data12['record_status']='1';

                        $sqlData = createSqlColValPair($data12);
                        $query9 = "UPDATE lease_guest_card SET " . $sqlData['columnsValuesPair'] . " where user_id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();

                    }
                }else{
                    $data4['phone_type'] = $data['phoneType'];//In Array PHONETYPE
                    $data4['carrier'] = $data['carrier'];
                    $data4['country_code'] = $data['countryCode'];
                    $data4['phone_number'] = $data['phoneNumber'];//In Array CARRIER
                    $data4['work_phone_extension'] = $data['Extension'];
                    $data4['created_at'] =  date('Y-m-d H:i:s');
                    $data4['updated_at'] =  date('Y-m-d H:i:s');
                
                     $sqlData = createSqlColValPair($data4);
                        $query9 = "UPDATE users SET " . $sqlData['columnsValuesPair'] . " where id=".$last_id;
                        $stmt = $this->companyConnection->prepare($query9);
                        $stmt->execute();
                        $data14['phone_type'] = $data['phoneType'];//In Array PHONETYPE
                        $data14['carrier'] = $data['carrier'];
                        $data14['country_code'] = $data['countryCode'];
                        $data14['phone_number'] = $data['phoneNumber'];//In Array CARRIER
                        $data14['work_phone_extension'] = $data['Extension'];
                        $data14['created_at'] =  date('Y-m-d H:i:s');
                        $data14['updated_at'] =  date('Y-m-d H:i:s');
                        $data14['user_id']=$last_id;
                          $sqlData = createSqlColVal($data14);
                        $query = "INSERT INTO tenant_phone (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] .")";
                        $stmt = $this->companyConnection->prepare($query);
                        $stmt->execute($data14);
                }
               
                    $this->unitRowRecord($last_id, $unit_id);
                  $this->addEmergencyDetails($last_id,$formData2);
                $this->addnotes($last_id,$notes);
                /*Call Notification function*/



                $descriptionNotifi ='A New prospect has been created: Guest Card number - '.$last_id.'.';
                $module_type = "GUESTCARD";
                $notificationTitle= "New Prospect";
                $alert_type = "Real Time";
                insertNotification($this->companyConnection,$last_id,$notificationTitle,"created",$descriptionNotifi,$module_type,$alert_type);
                return array('code' => 200, 'status' => 'success', 'data' => $data,'message' => 'Record added successfully.');

            }
        }
        catch (PDOException $e) {
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Property data DDL
     */
    public function guestProperty(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM general_property WHERE is_short_term_rental != 1 OR is_short_term_rental IS NULL order by property_name ASC");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * unit data DDL
     */
    public function guestUnitBuilding(){
        try{
            $id = $_POST['prop_id'];
            $query = $this->companyConnection->query("SELECT id FROM building_detail WHERE property_id='$id'");
            $data = $query->fetchAll();
            if(!empty($data)) {
                $building_id=[];
                $count = count($data);
                for ($i = 0; $i < $count; $i++) {
                  array_push($building_id,$data[$i]['id']);
                }
                $building_id=implode("','",$building_id);
                $building_id= "'".$building_id."'";
                $query = $this->companyConnection->query("SELECT unit_prefix,unit_type_id,unit_no,id FROM unit_details WHERE building_id in($building_id)");
                $data1 = $query->fetchAll();
                return ['status' => 'success', 'code' => 200, 'data' => $data1];
            }
            else {
                return ['status' => 'success', 'code' => 200, 'data' => $data];
            }
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * Unit preferences table listing
     */
    public function unitPreferencesListing(){
        $prop_id=$_POST['prop_id'];
        $unit_id=$_POST['unit_id'];
        $bed_id=$_POST['bed_id'];
        $bath_id=$_POST['bath_id'];
        $floor_id=$_POST['floor_id'];
        $move_in_id=$_POST['move_in_id'];
        $prop_id=$_POST['prop_id'];
        $min_id=$_POST['min_id'];
        $max_id=$_POST['max_id'];

        try{
            $where = "";
            $join = "";
            $select = "";

            if(isset($prop_id) && $prop_id!='')
            {
                $where .= " gp.id=".$prop_id." AND ud.building_unit_status=1";
                $select .= "ud.id,gp.property_name,ud.unit_no,ud.market_rent,ud.security_deposit";
            }

           if(isset($unit_id) && $unit_id!='')
            {
                $where .= " AND ud.id=".$unit_id;
                $select .= "";

            }
            if (isset($bed_id) && $bed_id != null){
                $where .= " AND ud.bedrooms_no=".$bed_id;
                $select .= "";
            }
            if (isset($bath_id) && $bath_id != null){
                $where .= " AND ud.bathrooms_no=".$bath_id;
                $select .= "";
            }
            if (isset($floor_id) && $floor_id != null){
                $where .= " AND ud.floor_no=".$floor_id;
                $select .= "";
            }

            $sql ="SELECT ".$select." FROM general_property gp LEFT JOIN unit_details ud ON ud.property_id
                 = gp.id where ".$where;
            $query = $this->companyConnection->query($sql);
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];

        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * unit preferences row data save
     */
    public function unitRowRecord($last_id,$unit_id){
//        print_r($last_id);die;
        $datarecord['unit_id'] = $unit_id;
        $sqlData = createSqlColValPair($datarecord);
        $query = "UPDATE lease_guest_card SET " . $sqlData['columnsValuesPair'] ." where user_id=".$last_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
    }

    /**
     * emergency countries fetching data
     */
    public function emerCountries(){
        try{
            $query = $this->companyConnection->query("SELECT * FROM countries");
            $data = $query->fetchAll();
            return ['status'=>'success','code'=>200, 'data'=> $data];
        } catch (Exception $exception) {
            return ['status'=>'failed','code'=>503,'message'=>$exception->getMessage()];
            printErrorLog($e->getMessage());
        }
    }

    /**
     * emergency contact detail clone
     */
    public function emerRecord($last_id){
//        print_r($last_id);die;
        $datarecord['user_id'] = $last_id;
        $sqlData = createSqlColValPair($datarecord);
        $query = "UPDATE emergency_details SET " . $sqlData['columnsValuesPair'] ." where user_id=".$last_id;
        $stmt = $this->companyConnection->prepare($query);
        $stmt->execute();
    }
    public function addnotes($last_id,$notes){
            //  echo"<pre>";print_r(count($formData2['emergency_contact_name']));die;
            if (is_array($notes['chargeNote'])) {
                $count=count($notes['chargeNote']);
                for ($j = 0; $j < $count; $j++) {
                    $notes1['notes'] = $notes['chargeNote'][$j];
                    $notes1['user_id'] = $last_id;
                    $notes1['type'] = '1';
                    $notes1['record_status'] = '0';
                    $sqlData = createSqlColVal($notes1);
                    $query = "INSERT INTO tenant_notes (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                    $stmt = $this->companyConnection->prepare($query);
                    $stmt->execute($notes1);
                }

            } else {
                $notes1['notes'] = $notes['chargeNote'];
                $notes1['user_id'] = $last_id;
                $notes1['type'] = '1';
                $notes1['record_status'] = '0';
                $sqlData = createSqlColVal($notes1);
                $query = "INSERT INTO tenant_notes (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($notes1);
            }
        }


    public function addEmergencyDetails($last_id,$formData2)
    {
      //  echo"<pre>";print_r(count($formData2['emergency_contact_name']));die;
        if (is_array($formData2['emergency_contact_name'])) {
            $count=count($formData2['emergency_contact_name']);
            for ($j = 0; $j < $count; $j++) {
                $emer['emergency_contact_name'] = $formData2['emergency_contact_name'][$j];
                $emer['emergency_relation'] = $formData2['emergency_relation'][$j];
                $emer['emergency_country_code'] = $formData2['emergency_country'][$j];
                $emer['emergency_phone_number'] = $formData2['emergency_phone'][$j];
                $emer['emergency_email'] = $formData2['emergency_email'][$j];
                $emer['user_id'] = $last_id;
                $sqlData = createSqlColVal($emer);
                $query = "INSERT INTO emergency_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute($emer);
            }

        } else {
            $emer['emergency_contact_name'] = $formData2['emergency_contact_name'];
            $emer['emergency_relation'] = $formData2['emergency_relation'];
            $emer['emergency_country_code'] = $formData2['emergency_country'];
            $emer['emergency_phone_number'] = $formData2['emergency_phone'];
            $emer['emergency_email'] = $formData2['emergency_email'];
            $emer['user_id'] = $last_id;
            $sqlData = createSqlColVal($emer);
            $query = "INSERT INTO emergency_details (" . $sqlData['columns'] . ") VALUES (" . $sqlData['columnsValues'] . ")";
            $stmt = $this->companyConnection->prepare($query);
            $stmt->execute($emer);
        }
    }
    public function deleteguestCard() {
        try {
            $data=[];
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT user_id FROM lease_guest_card WHERE id=".$id);
            $data3 = $query->fetch();
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['deleted_at'] = date('Y-m-d H:i:s');
                $sqlData = createSqlColValPair($data);
                //$query = "UPDATE users SET ".$sqlData['columnsValuesPair']." where id='$tenant_id'";
                $query = "DELETE FROM users WHERE id=".$data3['user_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record deleted successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function archiveguestCard() {
        try {
            $data=[];
            $id = $_POST['id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = 2;
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE lease_guest_card SET ".$sqlData['columnsValuesPair']." where id='$id'";
                //$query = "DELETE FROM users WHERE id=".$data3['user_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record updated successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }
    public function activateGuest() {
        try {
            $data=[];
            $id = $_POST['id'];
            //Required variable array
            $required_array = [];
            /* Max length variable array */
            $maxlength_array = [];
            //Number variable array
            $number_array = [];
            //Server side validation
            $err_array = [];
            $err_array = validation($data,$this->companyConnection,$required_array,$maxlength_array,$number_array);
            //Checking server side validation
            if (checkValidationArray($err_array)) {
                return array('code' => 400, 'status' => 'error', 'data' => $err_array, 'message' => 'Validation Errors!');
            } else {
                $data['status'] = 1;
                $sqlData = createSqlColValPair($data);
                $query = "UPDATE lease_guest_card SET ".$sqlData['columnsValuesPair']." where id='$id'";
                //$query = "DELETE FROM users WHERE id=".$data3['user_id'];
                $stmt = $this->companyConnection->prepare($query);
                $stmt->execute();
                return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record updated successfully.');
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getGuestCardStatus() {
        try {

            $query = $this->companyConnection->query("SELECT  lease_guest_card.id,  users.name, general_property.property_name, lease_guest_card.user_id, lease_guest_card.id, lease_guest_card.expected_move_in, lease_guest_card.status, unit_details.unit_no, unit_details.unit_prefix, users.phone_number, users.email FROM lease_guest_card LEFT JOIN users ON lease_guest_card.user_id=users.id LEFT JOIN unit_details ON lease_guest_card.unit_id=unit_details.id LEFT JOIN general_property ON unit_details.property_id=general_property.id WHERE ( lease_guest_card.deleted_at IS NULL AND users.user_type = '6' and lease_guest_card.status='1')");
            $data['active'] = $query->fetchAll();
            //echo "<pre>";print_r($data);die;
            $query = $this->companyConnection->query("SELECT  lease_guest_card.id,  users.name, general_property.property_name, lease_guest_card.user_id, lease_guest_card.id, lease_guest_card.expected_move_in, lease_guest_card.status, unit_details.unit_no, unit_details.unit_prefix, users.phone_number, users.email FROM lease_guest_card LEFT JOIN users ON lease_guest_card.user_id=users.id LEFT JOIN unit_details ON lease_guest_card.unit_id=unit_details.id LEFT JOIN general_property ON unit_details.property_id=general_property.id WHERE ( lease_guest_card.deleted_at IS NULL AND users.user_type = '6' and lease_guest_card.status='2')");
            $data['Archived'] = $query->fetchAll();

            return array('code' => 200, 'status' => 'success', 'data' => $data, 'message' => 'The record retrieved successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

    public function getallRecordguestCard() {
        try {
            $id = $_POST['id'];
            $query = $this->companyConnection->query("SELECT user_id FROM lease_guest_card WHERE id='$id'");
            $userId_guest = $query->fetch();

            $user_id = $userId_guest['user_id'];

            $query1 = $this->companyConnection->query("SELECT u.name,u.zipcode,u.address1,u.address2,u.address3,u.address4,u.city,u.state,u.country from users as u where id = '$user_id'");
            $dataGuest = $query1->fetch();

            $query2 = $this->companyConnection->query("SELECT u.name,u.zipcode,u.address1,u.address2,u.address3,u.address4,u.city,u.state,u.country from users as u where id = '1'");
            $data2 = $query2->fetch();
            $dataCompany = $data2;


            return array('code' => 200, 'status' => 'success', 'data' => $dataGuest,'company'=>$dataCompany, 'message' => 'The record retrieved successfully.');

        } catch (PDOException $e) {
            echo $e->getMessage();
            printErrorLog($e->getMessage());
        }
    }

}
$GuestCardAjax = new GuestCardAjax();