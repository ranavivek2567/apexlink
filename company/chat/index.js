var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var mysql = require('mysql');


var conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "mind@123",
    database: "apexlink_pm_3"
});

//import html2canvas from 'html2canvas';
// html2canvas(document.body).then(function(canvas) {
//     //document.body.appendChild(canvas);
//     console.log(canvas);
// });

// This array will contains the entries for virtual chat
// from_user_id: data.from_user_id,
// to_user_id: data.to_user_id,
// isAvailable: false
// isChatScreen: false
global.virtual_chat_array = [];

// This array will contains the entries for socket
global.socket_array = [];

io.on('connection', function(socket){
    try {
        if (socket.handshake && socket.handshake.query && socket.handshake.query.user_id) {
            let user_id = socket.handshake.query.user_id;
            console.log('cuser_id',user_id);

            // conn.connect(function(err) {
            //     if (!err) {
            //         setInterval(function () {
            //             let date = new Date();
            //             let startDate = new Date();
            //             var endDate = new Date(date.setMinutes(date.getMinutes() + parseInt(3)));
            //             let startTime = "'" + startDate.getHours() + ':' + startDate.getMinutes() + ':' + startDate.getSeconds() + "'";
            //             let endTime = "'" + endDate.getHours() + ':' + endDate.getMinutes() + ':' + endDate.getSeconds() + "'";
            //             // let sql = "SELECT * FROM calendar_schedule WHERE end_date=CURDATE() AND end_time BETWEEN " + startTime + " AND " + endTime;
            //             let sql = "SELECT * FROM calendar_schedule WHERE status_shown='0' order by CONCAT(end_date, end_time) ASC";
            //             conn.query(sql, function (err, result) {
            //                 console.log(result);
            //                 if (err) throw err;
            //                 if (result != '') {
            //                     io.emit("notification", result);
            //                 }
            //             });
            //
            //         }, 30000);
            //     }
            //
            // });


            for (let i = 0; i < socket_array.length; i++) {
                if (socket_array[i].user_id == user_id || socket_array[i].user_id == undefined) {
                    socket_array.splice(i, 1);
                }
            }

            let arr = {
                socket_id: socket.id,
                user_id: user_id,
                is_online: true
            };

            socket_array.push(arr);
            io.emit('online_users',socket_array);

            /**
             * message event listener and emitter
             */
            socket.on('private_chat', async (data) => {
                try {
                    // Add timestamp in message if it doesnot have any
                    if (!data.time_stamp) {
                        let d1 = new Date();
                        d1.toUTCString();
                        let time_stamp = Math.floor(d1.getTime());
                        data.time_stamp = time_stamp.toString();
                        data.time_stamp_double = time_stamp.toString();
                    }
                    // Send to To
                    let toUserInfo = socket_array.find(x => x.user_id === data.receiver);
                    let socket_id = (toUserInfo && toUserInfo.socket_id) ? toUserInfo.socket_id : '';
                    if (socket_id) {
                        io.to(socket_id).emit("conversation private post", data);
                        console.log('message send to');
                    }
                    // Send to From
                    let socketInfo = socket_array.find(x => x.user_id === data.sender);
                    if (socketInfo) {
                        console.log('message - send to from socket info found');
                        let socketFromID = socketInfo.socket_id;
                        io.to(socketFromID).emit("message_received_server", data);
                        console.log('message_received_server emitted ', data);
                    } else {
                        console.log('message - send to from socket info not found, now in temp chat');
                    }
                } catch (ex) {
                    console.log('error @ message socket ', ex);
                }
            });


            /**
             * typing event listener and emitter
             */
            socket.on('typing', async (data) => {
                try {
                    let toUserInfo = socket_array.find(x => x.user_id === data.receiver);
                    if (toUserInfo) {
                        io.to(toUserInfo.socket_id).emit("typing", data);
                    } else {
                        console.log('typing socket info not found');
                    }
                } catch (ex) {
                    console.log('error @ typing socket ', ex);
                }
            });

            /**
             * stoptyping event listener and emitter
             */
            socket.on('stoptyping', async (data) => {
                try {
                    console.log('stoptyping ', data);
                    let socketInfo = socket_array.find(x => x.user_id === data.receiver);
                    if (socketInfo) {
                        io.to(socketInfo.socket_id).emit("stoptyping", data);
                    } else {
                        console.log('stoptyping socket info not found');
                    }
                } catch (ex) {
                    console.log('error @ stoptyping socket ', ex);
                }
            });

            /**
             * stoptyping event listener and emitter
             */
            socket.on('check_user_online', async (data) => {
                try {
                    let socketInfo = socket_array.find(x => x.user_id === data.receiver);
                    if (socketInfo) {
                        io.to(socketInfo.socket_id).emit("stoptyping", data);
                    } else {
                        console.log('stoptyping socket info not found');
                    }
                } catch (ex) {
                    console.log('error @ stoptyping socket ', ex);
                }
            });

            /**
             * Disconnect Event listener
             */
            socket.on('disconnect', async () => {
                try {
                    let socketId = socket.id;
                    let userObj = '';
                    for (let i = 0; i < socket_array.length; i++) {
                        if (socket_array[i].socket_id == socketId) {
                            userObj = socket_array.splice(i, 1)[0];
                        }
                    }
                    io.emit('online_users',socket_array);
                } catch (ex) {
                    console.log('error @ socket disconnect ', ex);
                }
            });

        }
    } catch (ex) {
        console.log('error @ Push on socket # The Saviour ', ex);
    }
});





http.listen(3000, function(){
    console.log('listening on *:3000');
});