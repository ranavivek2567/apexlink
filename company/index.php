<?php

include_once( ROOT_URL . "/company/functions/FlashMessage.php");
//include_once (COMPANY_DIRECTORY_URL.'/constants.php');
include_once ('constants.php');
$request = $_SERVER['REQUEST_URI'];

$request = preg_replace('{/$}', '', $request);

$request1 = explode('/', $_SERVER["REQUEST_URI"]);
$key = end($request1);
if (is_numeric($key)) {
    array_pop($request1);
    $request = implode('/', $request1);
}

$var1 = strrpos($request, '?');
if (!empty($var1)) {
    $url = explode('?', $request);
    array_pop($url);
    $request = end($url);
}

function Route($uri, $prefix = null, $controller, $function) {
    FlashMessage::add($function);
    if (!empty($prefix))
        $controller = $prefix . '/' . $controller;
    return require __DIR__ . '/functions/' . $controller . '.php';
}

switch ($request) {

    case '/' :
        require __DIR__ . '/views/auth/login.php';
        break;
    case '' :
        require __DIR__ . '/views/auth/login.php';
        break;
    case '/login' :
        require __DIR__ . '/views/auth/login.php';
        break;

    /* Forgot Password Module Starts */
    case '/User/ForgotPassword' :
        require __DIR__ . '/views/auth/forgot_password.php';
        break;
    case '/User/forgotPassword' :
        require __DIR__ . '/functions/forgotPassword.php';
        break;
    case '/User/ResetPassword' :
        require __DIR__ . '/views/auth/reset_password.php';
        break;
    case '/User/resetPassword' :
        require __DIR__ . '/functions/forgotPassword.php';
        break;
    /* Forgot Password Module Ends */


    /* 2 Factor Authorization Module Starts */
    case '/2fa' :
        require __DIR__ . '/views/auth/two_factor_authentication.php';
        break;
    /* 2 Factor Authorization Module Ends */


    case '/Dashboard/Dashboard' :
        require __DIR__ . '/views/dashboard/dashboard.php';
        break;

    case '/MasterData/AddPropertyType' :
        require __DIR__ . '/views/admin/propertySetup/propertyType/admin-property-type.php';
        break;



    /* Company Admin Setting module start */
    case '/Setting/Settings' :
        require __DIR__ . '/views/admin/entityCompanySetup/defaultSetting/admin-entity-default-setting.php';
        break;
    case '/BillingAndSubscription/SubscriptionInfo' :
        require __DIR__ . '/views/admin/subscription/billing-subscription.php';
        break;

    case '/settings-ajax' :
        require __DIR__ . '/functions/default_settings.php';
        break;
    case '/company-ajax' :
        require __DIR__ . '/functions/changepassword.php';
        break;
    case '/timesettings-ajax' :
        require __DIR__ . '/functions/timesetting.php';
        break;

    /* Company Admin Setting module end */

    /* Admin Unit Type Module Starts */
    case '/MasterData/AddUnitType' :
        require __DIR__ . '/views/admin/propertySetup/propertyUnitType/admin-property-unit-type.php';
        break;
    case '/UnitType-Ajax' :
        require __DIR__ . '/functions/propertySetup/unitType.php';
        break;
    /* Admin Unit Type Module Ends */

    /* Admin Property Sub Type Module Starts */
    case '/MasterData/AddPropertySubType' :
        require __DIR__ . '/views/admin/propertySetup/propertySubType/admin-property-sub-type.php';
        break;
    case '/PropertySubType-Ajax' :
        require __DIR__ . '/functions/propertySetup/propertySubType.php';
        break;
    /* Admin Property Sub Type Module Ends */

    /* Admin Property Amenities Module Starts */
    case '/MasterData/AddAmenity' :
        require __DIR__ . '/views/admin/propertySetup/amenities/admin-property-amenities.php';
        break;
    case '/Amenities-Ajax' :
        require __DIR__ . '/functions/propertySetup/amenities.php';
        break;
    /* Admin Property Amenities Module Ends */

//    /* Apexlink feedback*/
//    case '/submit-feedback' :
//        echo   __DIR__ . '/views/company/file.php';die;
//        require __DIR__ . '/views/company/file.php';
//        break;
//    /* Apexlink feedback Ends*/

    /* Admin Property Style Module Starts */
    case '/MasterData/AddPropertyStyle' :
        require __DIR__ . '/views/admin/propertySetup/propertyStyle/admin-property-style.php';
        break;
    case '/MasterData/PropertyStyle-Ajax' :
        require __DIR__ . '/functions/propertySetup/propertyStyle.php';
        break;
    /* Admin Property Style Module Ends */

    /* Admin Account Setup Module Starts */
    case '/AccountType/AccountType' :
        require __DIR__ . '/views/admin/accountSetup/accountType/accountType.php';
        break;
    case '/AccountType-Ajax' :
        require __DIR__ . '/functions/accounting/accountType.php';
        break;
    /* Admin Account Type Setup Module Ends*/

    /* Admin Chart Of Account Setup Module Starts*/
    case '/ChartOfAccount/AddChartOfAccount' :
        require __DIR__ . '/views/admin/accountSetup/chartOfAccounts/chartOfAccounts.php';
        break;
    case '/AddChartOfAccount-Ajax' :
        require __DIR__ . '/functions/accounting/chartOfAccount.php';
        break;
    /* Admin  Chart Of Account Setup Module Ends */

    /* Admin Account Preference Setup Module Starts */
    case '/AccountPreference/AccountPreference' :
        require __DIR__ . '/views/admin/accountSetup/accountingPreferences/accountingPreferences.php';
        break;
    case '/AccountPreference-Ajax' :
        require __DIR__ . '/functions/accounting/accountingPreferences.php';
        break;
    /* Admin  Account Preference Setup Module Ends */

    /* Admin Charge Code Setup Module Starts */
    case '/MasterData/AddChargeCode' :
        require __DIR__ . '/views/admin/accountSetup/chargeCode/chargeCode.php';
        break;
    case '/ChargeCode-Ajax' :
        require __DIR__ . '/functions/accounting/chargeCode.php';
        break;
    /* Admin  Charge Code Setup Module Ends */

    /* Admin Charge Code Preferences Setup Module Starts */
    case '/MasterData/ChargeCodePreferences' :
        require __DIR__ . '/views/admin/accountSetup/chargeCodePreferences/chargeCodePreferences.php';
        break;
    case '/ChargeCodePreferences-Ajax' :
        require __DIR__ . '/functions/accounting/ChargeCodePreferences.php';
        break;
    /* Admin Charge Code Preferences Setup Module Ends */

    /* Admin Allocation Order Setup Module Starts */
    case '/MasterData/AllocationOrder' :
        require __DIR__ . '/views/admin/accountSetup/allocationOrder/allocationOrder.php';
        break;
    case '/AllocationOrder-Ajax' :
        require __DIR__ . '/functions/accounting/allocationOrder.php';
        break;
    /* Admin Allocation Order Setup Module Ends */

    /* Admin Check SetUp Setup Module Starts */
    case '/MasterData/AddCheckSetUp' :
        require __DIR__ . '/views/admin/accountSetup/checkSetup/checkSetup.php';
        break;
    /* Admin Check SetUp Setup Module Ends */

    /* Admin Bank Account Setup Module Starts */
    case '/MasterData/BankAccount' :
        require __DIR__ . '/views/admin/accountSetup/bankAccount/bankAccount.php';
        break;
    case '/BankAccount-Ajax' :
        require __DIR__ . '/functions/accounting/bankAccount.php';
        break;
    /* Admin Bank Account Setup Module Ends */

    /* Admin Tax Setup Module Starts */
    case '/MasterData/TaxSetup' :
        require __DIR__ . '/views/admin/accountSetup/taxSetup/taxSetup.php';
        break;
    case '/MasterData/Tax-Ajax' :
        require __DIR__ . '/functions/accounting/taxSetup.php';
        break;
    /* Admin Tax Setup Module Ends */

    /* Admin Vendor Type Module Starts */
    case '/MasterData/VendorType' :
        require __DIR__ . '/views/admin/vendorsSetup/vendorType/vendorType.php';
        break;
    case '/MasterData/VendorType-Ajax' :
        require __DIR__ . '/functions/vendorTypes.php';
        break;
    /* Admin Vendor Type Module Ends */


    /* Admin Event Type Module Starts */
    case '/MasterData/AddEventType' :
        require __DIR__ . '/views/admin/EventSetup/EventTypes/view.php';
        break;
    case '/MasterData/EventType-Ajax' :
        require __DIR__ . '/functions/eventType.php';
        break;
    /* Admin Event Type Module Ends */

    case '/User/ManageUser' :
        require __DIR__ . '/views/admin/userSetup/manageUsers/manageUsers.php';
        break;

    case '/ManageUser-Ajax' :
        require __DIR__ . '/functions/adminUsers/manageUsers.php';
        break;
    case '/EditEmailTemplate-Ajax' :
        require __DIR__ . '/functions/adminUsers/editEmailTemplate.php';
        break;
    case '/user/ManageUserRoles' :
        require __DIR__ . '/views/admin/userSetup/manageUserRoles/manageUserRoles.php';
        break;
    case '/UserRoles-Ajax' :
        require __DIR__ . '/functions/adminUsers/manageUserRoles.php';
        break;

    case '/user/UserRoleAuthorization' :
        require __DIR__ . '/views/admin/userSetup/roleAuthorization/roleAuthorization.php';
        break;
    case '/RoleAuthorization-Ajax' :
        require __DIR__ . '/functions/adminUsers/roleAuthorization.php';
        break;
    case '/User/ManageGroups' :
        require __DIR__ . '/views/admin/userSetup/manageGroups/manageGroups.php';
        break;
    case '/user/LoginHistory' :
        require __DIR__ . '/views/admin/userSetup/loginHistory/view.php';
        break;
    case '/User/ManageVehicle' :
        require __DIR__ . '/views/admin/userSetup/manageVehicles/view.php';
        break;
    case '/manageVehicle-Ajax' :
        require __DIR__ . '/functions/adminUsers/manageVehicles.php';
        break;

    /* Admin user Type Module Ends */

    /* Admin Maintenance Type Module Starts */
    case '/Maintenance/MaintenanceCategory' :
        require __DIR__ . '/views/admin/maintenanceSetup/category/view.php';
        break;
    case '/Maintenance/MaintenancePriority' :
        require __DIR__ . '/views/admin/maintenanceSetup/priority/view.php';
        break;
    case '/MasterData/maintenance-Ajax' :
        require __DIR__ . '/functions/maintenance/maintenancePriority.php';
        break;
    case '/MasterData/category-Ajax' :
        require __DIR__ . '/functions/maintenance/maintenanceCategory.php';
        break;
    case '/MasterData/MaintenanceSeverity' :
        require __DIR__ . '/views/admin/maintenanceSetup/severity/view.php';
        break;
    case '/MasterData/severity-Ajax' :
        require __DIR__ . '/functions/maintenance/maintenanceSeverity.php';
        break;

    case '/Maintenance/WorkOrderType' :
        require __DIR__ . '/views/admin/maintenanceSetup/workOrderType/view.php';
        break;
    case '/MasterData/workOrder-Ajax' :
        require __DIR__ . '/functions/maintenance/maintenanceWorkOrder.php';
        break;
    case '/Maintenance/InventoryTracker' :
        require __DIR__ . '/views/admin/maintenanceSetup/inventoryTracker/view.php';
        break;
    case '/MasterData/InventoryTracker-Ajax' :
        require __DIR__ . '/functions/maintenance/maintenancePreferences.php';
        break;
    case '/Alert/ManageAlert' :
        require __DIR__ . '/views/admin/alert/view.php';
        break;
    case '/UserAlert-Ajax':
        require __DIR__ . '/functions/userAlert.php';
        break;
    case '/mileage-log-ajax' :
        require __DIR__ . '/functions/maintenance/mileage_log/MileageLog.php';
        break;

    /* Admin Maintenance Type Module Ends */
    /* Admin Template Type Module Starts */
    case '/Template/EmailTemplate' :
        require __DIR__ . '/views/admin/templates/view.php';
        break;
    case '/Template/EmailTemplateEdit' :
        require __DIR__ . '/views/admin/templates/editTemplate.php';
        break;

    /* Admin Template Type Module ends */

    /* Admin Trash Bin Module Starts */
    case '/TrashBin/PropertyType' :
        require __DIR__ . '/views/admin/trashBin/propertyType/view.php';
        break;
    case '/TrashBin/UnitType' :
        require __DIR__ . '/views/admin/trashBin/unitType/view.php';
        break;
    case '/TrashBin/PropertySubType' :
        require __DIR__ . '/views/admin/trashBin/propertySubType/view.php';
        break;
    case '/TrashBin/PropertyStyle' :






        require __DIR__ . '/views/admin/trashBin/propertyStyle/view.php';
        break;
    case '/TrashBin/PropertyGroups' :
        require __DIR__ . '/views/admin/trashBin/propertyGroups/view.php';
        break;
    case '/TrashBin/Amenities' :
        require __DIR__ . '/views/admin/trashBin/amenities/view.php';
        break;
    case '/TrashBin/VendorType' :
        require __DIR__ . '/views/admin/trashBin/vendorType/view.php';
        break;
    case '/TrashBin/Category' :
        require __DIR__ . '/views/admin/trashBin/category/view.php';
        break;
    case '/TrashBin/Severity' :
        require __DIR__ . '/views/admin/trashBin/severity/view.php';
        break;

    case '/TrashBin/EventTypes' :
        require __DIR__ . '/views/admin/trashBin/eventType/view.php';
        break;
    case '/TrashBin/WorkOrderType' :
        require __DIR__ . '/views/admin/trashBin/WorkOrderType/view.php';
        break;
    /* Admin Trash Bin Module Ends */

    case '/TrashBin/AccountType' :
        require __DIR__ . '/views/admin/trashBin/accountType/view.php';
        break;
    case '/TrashBin/Priority' :
        require __DIR__ . '/views/admin/trashBin/priority/view.php';
        break;
    case '/AccountPreference/AccountPreference' :
        require __DIR__ . '/views/admin/trashBin/accountPrefences/view.php';
        break;
    case '/TrashBin/ChargeCode' :
        require __DIR__ . '/views/admin/trashBin/chargeCode/view.php';
        break;
    case '/TrashBin/ManageUsers' :
        require __DIR__ . '/views/admin/trashBin/manageUser/view.php';
        break;
    case '/TrashBin/ManageUserRoles' :
        require __DIR__ . '/views/admin/trashBin/manageUserRole/view.php';
        break;
    case '/TrashBin/Contact' :
        require __DIR__ . '/views/admin/trashBin/contacts/view.php';
        break;
    case '/TrashBin/Building' :
        require __DIR__ . '/views/admin/trashBin/building/view.php';
        break;
    case '/TrashBin/Unit' :
        require __DIR__ . '/views/admin/trashBin/unit/view.php';
        break;
    case '/TrashBin/TaxSetup' :
        require __DIR__ . '/views/admin/trashBin/taxSetup/view.php';
        break;
    case '/TrashBin/ChartofAccount' :
        require __DIR__ . '/views/admin/trashBin/chartOfAccounts/view.php';
        break;
    case '/TrashBin/ChargeCodePreferences' :
        require __DIR__ . '/views/admin/trashBin/chargeCodePreferences/view.php';
        break;
    /* Admin Trash Bin Module Ends */

    case '/MasterData/AddPropertyGroup' :
        require __DIR__ . '/views/admin/propertySetup/propertyGroup/admin-property-groups.php';
        break;
    case '/Property/PropertyModules' :
        require __DIR__ . '/views/company/properties/view.php';
        break;
    case '/Property/PropertyView' :
        //  print_r('sgsdg');die;
        require __DIR__ . '/views/company/properties/propertyDetail.php';
        break;

    case '/Property/PropertyModule' :
        require __DIR__ . '/views/admin/propertySetup/propertyPortfolio/admin-property-portfolio.php';
        break;

    case '/Property/AddProperty' :
        require __DIR__ . '/views/company/properties/new.php';
        break;
    case '/Property/EditProperty' :
//        require __DIR__ . '/views/company/properties/edit-property.php';
        Route('/Property/EditProperty', 'property', 'propertyEdit', 'view');
        break;

    case ('/Property/XMLFile') :
        require __DIR__ . '/functions/property/xmlFile.php';
        break;

    case ('/Property/GeneralLedger') :
        require __DIR__ . '/views/company/properties/general_ledger_property.php';
        break;


    case '/GuestCard/ListGuestCard' :
        require __DIR__ . '/views/company/leases/guest_cards/view.php';
        break;

    case '/RentalApplication/RentalApplications' :
        require __DIR__ . '/views/company/leases/rental_applications/rental.php';
        break;
    case ('/RentalApplication/RentalApplication') :
        require __DIR__ . '/views/company/leases/rental_applications/addRentalApplications.php';
        break;

    case '/RentalApplication/EditRentalApplications' :
        require __DIR__ . '/views/company/leases/rental_applications/Edit_rental.php';
        break;

    case ('/RentalApplication/Ajax') :
        require __DIR__ . '/functions/leases/rentalApplications.php';
        break;

    case '/Lease/ViewEditLease' :
        require __DIR__ . '/views/company/leases/leases/lease.php';
        break;

    case '/Lease/Movein' :
        require __DIR__ . '/views/company/leases/move-in/move.php';
        break;
    case '/Lease/Move-in' :
        require __DIR__ . '/views/company/leases/move-in/move-in.php';
        break;
    case '/GuestCard/NewGuestCard' :
        require __DIR__ . '/views/company/leases/guest_cards/new_guest_card.php';
        break;
    case '/GuestCard/NewGuestCard/Edit' :
        require __DIR__ . '/views/company/leases/guest_cards/new_guest_card_edit.php';
        break;
    case '/guest-card-ajax' :
        require __DIR__ . '/functions/leases/guestCard.php';
        break;
    case '/guest-card-edit-ajax' :
        require __DIR__ . '/functions/leases/guestCardEdit.php';
        break;
    /* unit module */
    case '/Unit/View' :
        require __DIR__ . '/views/company/properties/unit/view.php';
        break;

    case '/Unit/UnitModule' :
        require __DIR__ . '/views/company/properties/unit/list.php';
        break;
    case '/Unit/UnitView' :
//        dd('aaaa');
        require __DIR__ . '/views/company/properties/unit/view.php';
        break;
    case '/Unit/AddUnit' :
        require __DIR__ . '/views/company/properties/unit/addUnit.php';
        break;

    case '/Unit/EditUnit' :
        require __DIR__ . '/views/company/properties/unit/editUnit.php';
        break;
    /* unit module */

    //jqgrid
    case '/List/jqgrid' :
        require __DIR__ . '/functions/jqgrid.php';
        break;
    case '/Companies/List/jqgrid' :
        require __DIR__ . '/functions/jqgrid.php';
        break;
    case '/propertytype-ajax' :
        require __DIR__ . '/functions/propertySetup/propertytype.php';
        break;
    case '/propertygroups-ajax' :
        require __DIR__ . '/functions/propertySetup/propertygroups.php';
        break;

    case '/company-user-ajax' :
        require __DIR__ . '/functions/companyUser.php';
        break;

    case '/company/changepassword' :
        require __DIR__ . '/functions/changepassword.php';
        break;

    case '/common-ajax' :
        require __DIR__ . '/functions/common.php';
        break;

    case '/export-excel' :
        require __DIR__ . '/functions/propertySetup/propertytype.php';
        break;

    case '/export-groups-excel' :
        require __DIR__ . '/functions/propertySetup/propertygroups.php';
        break;

    case '/export-style-excel' :
        require __DIR__ . '/functions/propertySetup/propertyStyle.php';
        break;

    case '/ApexlinkNewAjax' :
        require __DIR__ . '/functions/apexlink_new.php';
        break;
    case '/SupportManagement-Ajax' :
        require __DIR__ . '/functions/supportManagement.php';
        break;


    case '/MasterData/AddPortfolio' :
        require __DIR__ . '/views/admin/propertySetup/propertyPortfolio/admin-property-portfolio.php';
        break;

    /* Portfolio Module start */
    case ('/portfolio/add') :
        Route('/portfolio/add', 'propertySetup', 'Portfolio', 'add');
        break;
    case ('/portfolio/get') :
        Route('/portfolio/get', 'propertySetup', 'Portfolio', 'get');
        break;
    case ('/portfolio/delete') :
        Route('/portfolio/delete', 'propertySetup', 'Portfolio', 'delete');
        break;
    case ('/portfolio/updateStatus') :
        Route('/portfolio/updateStatus', 'propertySetup', 'Portfolio', 'updateStatus');
        break;
    /* Portfolio Module end */

    /* custom field Module Start */

    /* Tenant module route */
    case ('/Tenantlisting/Tenantlisting') :
        require __DIR__ . '/views/company/tenants/view.php';
        break;
    case ('/Tenantlisting/ActiveTenant') :
        require __DIR__ . '/views/company/tenants/activeTenant.php';
        break;
    case ('/Tenant/Receivebatchpayment') :
        require __DIR__ . '/views/company/tenants/receiveBatch.php';
        break;
    case ('/Tenant/FormarTenant') :
        require __DIR__ . '/views/company/tenants/receiveBatch.php';
        break;
    case ('/Tenant/InstrumentRegister') :
        require __DIR__ . '/views/company/tenants/instrumentRegister.php';
        break;
    case ('/Tenant/TenantStatements') :
        require __DIR__ . '/views/company/tenants/tenantStatements.php';
        break;
    case ('/Tenant/BatchRegister') :
        require __DIR__ . '/views/company/tenants/batchRegister.php';
        break;
    case ('/ShortTermRental/RentersListing') :
        require __DIR__ . '/views/company/tenants/shortterm.php';
        break;
    case ('/Tenantlisting/add') :
        require __DIR__ . '/views/company/tenants/add-tenant.php';
        break;
    case ('/Lease/Leases') :
        require __DIR__ . '/views/company/leases/leases/add-tenant.php';
        break;
    case ('/tenantAjax') :
        require __DIR__ . '/functions/people/tenant/tenant.php';
        break;
    case ('/smartmove') :
        require __DIR__ . '/functions/people/tenant/smartMove.php';
        break;
    case ('/RenterSmartMove') :
        require __DIR__ . '/functions/smartmove/smartMove.php';
        break;
    case ('/smartmoveview') :
        require __DIR__ . '/views/company/tenants/emailTemplates/smRenterEmail.php';
        break;
    case ('/SM/V3/Renter') :
        //require __DIR__ . '/views/smartmove/renter/createRenter.php';
        require __DIR__ . '/views/smartmove/renter/renterpayment.php';
        break;
    case ('/RenterInfo') :
        require __DIR__ . '/views/smartmove/renter/renterInfo.php';
        break;
    case ('/RenterPayment') :
        require __DIR__ . '/views/smartmove/renter/renterpayment.php';
        break;
    case ('/SM/V3/Renters') :
        require __DIR__ . '/views/smartmove/renter/createRenter.php';
        break;
    case ('/Spotlight') :
        require __DIR__ . '/functions/spotlight/spotlight.php';
        break;
    case ('/tenantTypeAjax') :
        require __DIR__ . '/functions/people/tenant/tenantList.php';
        break;
    case ('/tenantOccupantsTypeAjax') :
        require __DIR__ . '/functions/people/tenant/tenant_occupants.php';
        break;
    case ('/Tenantlisting/addTenant') :
        require __DIR__ . '/views/company/tenants/add-tenant.php';
    case ('/editTenant') :
        require __DIR__ . '/functions/people/tenant/editTenant.php';
        break;
    case ('/Tenant-portal') :
        require __DIR__ . '/views/company/tenants/tenant-portal.php';
        break;
        break;
    case ('/Tenant-insurance') :
        require __DIR__ . '/views/company/tenants/tenant-insurance.php';
        break;
    case ('/TenantPortal/Tenant/TenantAddTicket') :
        require __DIR__ . '/views/company/tenants/add-ticket.php';
        break;
    case ('/TenantPortal/Tenant/TenantTranactions') :
        require __DIR__ . '/views/company/tenants/transactions.php';
        break;
    case ('/TenantPortal/Communication/SentEmails') :
        require __DIR__ . '/views/company/tenants/communications.php';
        break;

    case ('/TenantPortal/Tenant/TenantEditTicket') :
        require __DIR__ . '/views/company/tenants/edit-ticket.php';
        break;
    case ('/TenantPortal/portal-insurance') :
        require __DIR__ . '/views/company/tenants/portal-insurance.php';
        break;
    case ('/TenantPortal/Tenant/TenantViewTicket') :
        require __DIR__ . '/views/company/tenants/view-ticket.php';
        break;
    case ('/TenantPortal/changePassword') :
        require __DIR__ . '/views/company/tenants/change-password.php';
        break;
    case ('/TenantPortal/login') :
        require __DIR__ . '/views/company/tenants/portal-login.php';
        break;
    case ('/TenantPortal/file-library') :
        require __DIR__ . '/views/company/tenants/file-library.php';
        break;
    case ('/TenantPortal/maintenance') :
        require __DIR__ . '/views/company/tenants/maintenance.php';
        break;
    case ('/tenantPortal') :
        require __DIR__ . '/functions/people/tenant/tenantportal.php';
        break;
    case ('/maintenance') :
        require __DIR__ . '/functions/maintenance/tickets/maintenance.php';
        break;
    case ('/Tenant/ViewEditTenant') :
        require __DIR__ . '/views/company/tenants/edit-tenant.php';
        break;
    case ('/Tenantlisting/getInitialData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getTenantRentInfoMoveIn') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/EsignatureUser/getTenantRentInfoMoveIn') :
        require __DIR__ . '/functions/people/tenant/generateLease.php';
        break;
    case ('/EsignatureUser/updateESignHistoryState') :
        require __DIR__ . '/functions/people/tenant/generateLease.php';
        break;
    case ('/EsignatureUser/getMoveInListRecord') :
        require __DIR__ . '/functions/people/tenant/generateLease.php';
        break;
    case ('/Tenantlisting/getAlphabetName') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getUnitById') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getUserNameById') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/EsignatureUser/EsignatureLease') :
        require __DIR__ . '/views/company/tenants/emailTemplates/residentialLeaseAgreement.php';
        break;
    case ('/EsignatureUser/saveSignatureImage') :
        require __DIR__ . '/functions/people/tenant/generateLease.php';
        break;
    case ('/EsignatureUser/thankyou') :
        require __DIR__ . '/views/company/tenants/thankyou.php';
        break;
    case ('/EsignatureUser/Accessdenied') :
        require __DIR__ . '/views/company/tenants/accessdenied.php';
        break;
    case ('/EsignatureUser/getSignatureInfo') :
        require __DIR__ . '/functions/people/tenant/generateLease.php';
        break;
    case ('/EsignatureUser/residentialLeaseAgreement') :
        require __DIR__ . '/functions/people/tenant/generateLease.php';
        break;
    case ('/Tenantlisting/getRentInfo') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenant/Transfer') :
        require __DIR__ . '/views/company/tenants/tenantTransfer.php';
        break;
    case ('/Tenantlisting/savePopUpData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/savePopUpData3') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/saveDoublePopUpData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/saveDoublePopUpData2') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getChargeCodes') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/saveChargeCodes') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getCharges') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getTenantRentInfo') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/saveTenantRentInfo') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getDateFormat') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getBuildingByPropertyId') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/flagChangeOption') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getUnitByBuildingId') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getTenantLeasePage') :
        require __DIR__ . '/views/company/tenants/lease.php';
        break;
    case ('/Tenantlisting/saveTenantLeasePage') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/saveTenantMoveInPage') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getPropertyPopUpData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getBuildingPopUpData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getUnitPopUpData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getTenantChargePage') :
        require __DIR__ . '/views/company/tenants/charge.php';
        break;
    case ('/Tenantlisting/movein') :
        require __DIR__ . '/views/company/tenants/movein.php';
        break;
    case ('/Tenantlisting/getTenantGeneratePage') :
        require __DIR__ . '/views/company/tenants/generate.php';
        break;
    case ('/Tenantlisting/tenant_leaseGeneratePDFs.php') :
        require __DIR__ . '/uploads/tenant_lease/generate_lease_files/tenant_leaseGeneratePDF-'.$_GET['id'] .'.php';
        break;
    case ('/Tenantlisting/TenantLease_Generate_Pdf') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/EsignatureUser/sendESigatureMail') :
        //require __DIR__ . '/views/company/tenants/emailTemplates/leaseGenerateLink.php';
        require __DIR__ . '/functions/people/tenant/generateLease.php';
        break;
    case ('/Tenantlisting/saveFlagData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/saveRenterData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/saveHoaData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getFlagInfo') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getRenterInfo') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getHoaInfo') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/Tenantlisting/getHoaData') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break;
    case ('/moveOutTenant') :
        require __DIR__ . '/functions/people/tenant/moveOutTenant.php';
        break;
    case ('/People/Ownerlisting') :
        require __DIR__ . '/views/company/owners/listOwner.php';
        break;
    case ('/People/AddOwners') :
        require __DIR__ . '/views/company/owners/addOwner.php';
        break;
    case ('/People/ViewOwner') :
        require __DIR__ . '/views/company/owners/viewOwner.php';
        break;
    case ('/OwnerAjax') :
        require __DIR__ . '/functions/people/owner/ownerAjax.php';
        break;
    case ('/ViewOwnerAjax') :
        require __DIR__ . '/functions/people/owner/viewOwnerAjax.php';
        break;
    case ('/EditOwnerAjax') :
        require __DIR__ . '/functions/people/owner/editOwnerAjax.php';
        break;
    case ('/People/EditOwners') :
        require __DIR__ . '/views/company/owners/editOwner.php';
        break;
    case ('/Owner/MyAccount/AccountInfo') :
        require __DIR__ . '/views/company/owners/ownerPortal/accountInfo.php';
        break;
    case ('/Owner/MyAccount/UpdateAccountInfo') :
        require __DIR__ . '/views/company/owners/ownerPortal/editAccountInfo.php';
        break;
    case ('/Owner/MyAccount/AllAnnouncements') :
        require __DIR__ . '/views/company/owners/ownerPortal/allAnnouncementsOwner.php';
        break;
    case ('/OwnerPortal-Ajax') :
        require __DIR__ . '/functions/people/owner/ownerPortal/viewAccountInfoAjax.php';
        break;
    case ('/OwnerPortal/ChangePassword') :
        require __DIR__ . '/views/company/owners/ownerPortal/changePassword.php';
        break;
    case ('/OwnerPortalEdit-Ajax') :
        require __DIR__ . '/functions/people/owner/ownerPortal/editAccountInfoAjax.php';
        break;
    case ('/Owner/FileLibrary/Library') :
        require __DIR__ . '/views/company/owners/ownerPortal/fileLibrary.php';
        break;
    case ('/Owner/MyAccount/BillInfo') :
        require __DIR__ . '/views/company/owners/ownerPortal/ownerBills.php';
        break;
    case ('/OwnerBills-Ajax') :
        require __DIR__ . '/functions/people/owner/ownerPortal/ownerBillsAjax.php';
        break;
    case ('/Owner/OwnerTransactionStatement/OwnerStatement') :
        require __DIR__ . '/views/company/owners/ownerPortal/ownerStatement.php';
        break;
    case ('/Owner/Ticket/Tickets') :
        require __DIR__ . '/views/company/owners/ownerPortal/maintenance/tickets.php';
        break;
    case ('/Owner/WorkOrder/WorkOrders') :
        require __DIR__ . '/views/company/owners/ownerPortal/maintenance/workOrders.php';
        break;
    case ('/Owner/LostAndFoundOwner/LostAndFound') :
        require __DIR__ . '/views/company/owners/ownerPortal/maintenance/lost&FoundManager.php';
        break;
    case ('/Owner/MyAccount/OwnerPurchaseOrder') :
        require __DIR__ . '/views/company/owners/ownerPortal/maintenance/purchaseOrder.php';
        break;
    case ('/Owner/Login') :
        require __DIR__ . '/views/company/owners/ownerPortal/ownerPortalLogin.php';
        break;
    case ('/OwnerPortalLogin-Ajax') :
        require __DIR__ . '/functions/people/owner/ownerPortal/portalLoginAjax.php';
        break;
    case ('/Owner/PropertiesOwned/Properties') :
        require __DIR__ . '/views/company/owners/ownerPortal/propertiesOwned.php';
        break;
    case '/Owner/Login/ForgotPassword' :
        require __DIR__ . '/views/company/owners/ownerPortal/forgotPassword.php';
        break;
    case '/Owner/Login/ResetPassword' :
        require __DIR__ . '/views/company/owners/ownerPortal/resetPassword.php';
        break;
    case ('/Owner/OwnerPayments/OwnerContribution') :
        require __DIR__ . '/views/company/owners/ownerPortal/ownerContribution.php';
        break;
    case ('/Owner/OwnerPayments/OwnerDraw') :
        require __DIR__ . '/views/company/owners/ownerPortal/ownerDraw.php';
        break;

    case ('/Accounting/GenerateOwnerDraw') :
        require __DIR__ . '/views/company/accounting/ownerDraw/add.php';
        break;
    case ('/Accounting/OwnerDraw') :
        require __DIR__ . '/views/company/accounting/ownerDraw/list.php';
        break;
    case ('/OwnerDraw-Ajax') :
        require __DIR__ . '/functions/accounting/ownerDraw.php';
        break;

    case ('/Accounting/GenerateOwnerContribution') :
        require __DIR__ . '/views/company/accounting/ownerContribution/add.php';
        break;
        case ('/Accounting/GenerateOwnerContributionPortal') :
        require __DIR__ . '/views/company/owners/ownerPortal/addOwnerContribution.php';
        break;
    case ('/Accounting/OwnerContribution') :
        require __DIR__ . '/views/company/accounting/ownerContribution/list.php';
        break;
    case ('/OwnerContribution-Ajax') :
        require __DIR__ . '/functions/accounting/ownerContribution.php';
        break;

//    case ('/Owner/Communication/SentEmails') :
//        require __DIR__ . '/views/company/owners/ownerPortal/communication/sentEmails.php';
//        break;
//    case ('/Owner/Communication/TextMessage') :
//        require __DIR__ . '/views/company/owners/ownerPortal/communication/textMessage.php';
//        break;
    case ('/Owner/MyAccount/AllCompanyAdminAnnouncements') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/announcement.php';
        break;
    case ('/Vendor/Vendor') :
        require __DIR__ . '/views/company/vendors/list.php';
        break;
    case ('/Vendor/AddVendor') :
        require __DIR__ . '/views/company/vendors/new_vendor.php';
        break;
    case ('/Vendor/ViewVendor') :
        require __DIR__ . '/views/company/vendors/viewVendor.php';
        break;
    case ('/Vendor/file-library') :
        require __DIR__ . '/functions/people/vendor/fileLibrary.php';
        break;
    case ('/Vendor/NewBill') :
        require __DIR__ . '/views/company/vendors/newBill.php';
        break;
    case ('/Vendor/EditBill') :
        require __DIR__ . '/views/company/vendors/editBill.php';
        break;
    //work order routes start
    case ('/WorkOrder/AddWorkOrder') :
        require __DIR__ . '/views/company/maintenance/work_orders/add_work_order.php';
        break;
    case ('/WorkOrder/EditWorkOrder') :
        require __DIR__ . '/views/company/maintenance/work_orders/edit_workorder.php';
        break;
    case ('/work-order-ajax') :
        require __DIR__ . '/functions/maintenance/workOrder/workOrder.php';
        break;
    case ('/tickets/ViewTickets') :
        require __DIR__ . '/views/company/maintenance/tickets/view-ticket.php';
        break;
    //work order routes end

//    case ('/People/ContactListt') :
//        require __DIR__ . '/views/company/contacts/listProcessManagementFee.php';
//        break;
//    case '/People/AddContact' :
//        require __DIR__ . '/views/company/contacts/addContact.php';
//        break;
    case '/Ajax-People/AddContact':
        require __DIR__ . '/functions/contactPopup.php';
        break;
    case ('/People/ContactModule') :
        require __DIR__ . '/views/company/contacts/view_contact.php';
        break;
    case '/Ajax-People/EditViewContact':
        require __DIR__ . '/functions/ContactEdit.php';
        break;
    case ('/People/ContactModule') :
        require __DIR__ . '/views/company/contacts/view_contact.php';
        break;
//    case ('/People/ContactModule') :
//        require __DIR__ . '/views/company/contacts/editContact.php';
//        break;
    case ('/People/GetEmployeeList') :
        require __DIR__ . '/views/company/employee/list.php';
        break;
    case ('/People/AddEmployee') :
        require __DIR__ . '/views/company/employee/add.php';
        break;
    case ('/Employee/getInitialData') :
        require __DIR__ . '/functions/people/employee/employeeAjax.php';
        break;
    case ('/EmployeeAjax') :
        require __DIR__ . '/functions/people/employee/employeeAjax.php';
        break;
    case ('/employeeListAjax') :
        require __DIR__ . '/functions/people/employee/employeeList.php';
        break;
    case ('/Employee/View') :
        Route('/Employee/View', 'people/employee', 'employeeEdit', 'view');
        break;
    case ('/Employee/Edit') :
        Route('/Employee/Edit', 'people/employee', 'employeeEdit', 'edit');
        break;
    case ('/Employee/saveNewVehicle') :
        Route('/Employee/saveNewVehicle', 'people/employee', 'employeeEdit', 'addNewVehicle');
        break;
    case ('/Employee/getVehicleDetail') :
        Route('/Employee/getVehicleDetail', 'people/employee', 'employeeEdit', 'getVehicleDetail');
        break;
    case ('/Employee/deleteVehicleDetail') :
        Route('/Employee/deleteVehicleDetail', 'people/employee', 'employeeEdit', 'deleteVehicleDetail');
        break;
    case ('/Employee/addLibraryFiles') :
        Route('/Employee/addLibraryFiles', 'people/employee', 'employeeEdit', 'addLibraryFiles');
        break;
    case ('/Employee/deleteLibraryFiles') :
        Route('/Employee/deleteLibraryFiles', 'people/employee', 'employeeEdit', 'deleteLibraryFiles');
        break;
    case ('/Employee/edit') :
        Route('/Employee/edit', 'people/employee', 'employeeEdit', 'update');
        break;

    /*
     * contact routes start
     */
    case ('/People/ContactListt') :
        require __DIR__ . '/views/company/contacts/list.php';
        break;
    case ('/People/AddContact') :
        require __DIR__ . '/views/company/contacts/add.php';
        break;
    case ('/ContactAjax') :
        require __DIR__ . '/functions/people/contacts/contactAjax.php';
        break;
    case ('/contactListAjax') :
        require __DIR__ . '/functions/people/contacts/contactList.php';
        break;
    case ('/Contact/View') :
        Route('/Contact/View', 'people/contacts', 'contactEdit', 'view');
        break;
    case ('/Contact/Edit') :
        Route('/Contact/Edit', 'people/contacts', 'contactEdit', 'edit');
        break;
    case ('/Contact/edit') :
        Route('/Contact/edit', 'people/contacts', 'contactEdit', 'update');
        break;

    /*
     * contact routes ends
     */


    case ('/Reporting/Reporting') :
        require __DIR__ . '/views/company/reports/view.php';
        break;
    case ('/Reporting/Tenant_InsuranceReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/TenantPetDetailReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/RentRollReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/RentRollConsolidatedReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/Maintenance_OpenWorkOrderReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/PurchaseOrderReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/UnitListReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/PropertyOwnerListingReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/TenantListReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/TenantListConsolidatedReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/UnitFeatureReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/VacantUnitsReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/VendorListConsolidate') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/PropertyStatementReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/PropertyListingReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/PropertySummaryReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/InventorySummary') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/PropertyGroup') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/Portfolio') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/VacantPropertyListingReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/MarketingReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/InspectionReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/TenantMailingLabelReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/EmployeeMailingLabelReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/OwnerMailingLabel') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/RentalApplicationReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/InventoryReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/InventoryDetails') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/InventoryReordering') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/Reporting') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/LostItemsReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/FoundItemsReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/MatchedItemsReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/PrintEnvelopeReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/VendorList') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/Vendor1099DetailReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/EmployeeCompanyVehicleReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/EmployeeListReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/EmployeeEEOReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/Owner_WithHoldingReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/Owner_OpenWorkOrderReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/Owner1099SummaryReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/Vendor_WorkOrderReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/VendorInsurance') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/Vendor1099SummaryReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;

    case ('/Reporting/Owner1099DetailReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/Owner_ListingReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/InventorySupplier_purchase') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/Reporting/VendorMailingLabelReport') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;


    case ('/Reporting-Ajax') :
        require __DIR__ . '/functions/reports/reportsPopupData.php';
        break;
    case ('/lease/expiration') :
        require __DIR__ . '/views/company/reports/tenant/leaseExpiration.php';
        break;
    case ('/reports/ajax') :
        require __DIR__ . '/functions/reports/reportsView.php';
        break;
    case ('/ShortTermRental/RentersListing') :
        require __DIR__ . '/views/company/tenants/shortterm.php';
        break;
    case ('/RentalApplication/RentalApplication') :
        require __DIR__ . '/views/company/leases/rental_applications/addRentalApplications.php';
        break;
    case ('/RentalApplication/Ajax') :
        require __DIR__ . '/functions/leases/rentalApplications.php';
        break;
    case ('/Reporting-Ajax-download') :
        require __DIR__ . '/functions/reports/downloadreport.php';
        break;

    //marketing routes start
    case ('/Marketing/MarketingListing') :
        require __DIR__ . '/views/company/marketing/listing/listing.php';
        break;
    case ('/Marketing/marketingCampaigns') :
        require __DIR__ . '/views/company/marketing/campaign/marketingCampaigns.php';
        break;
    case ('/Marketing/marketingFlyer') :
        require __DIR__ . '/views/company/marketing/flyer/viewFlyer.php';
        break;
    case ('/Marketing/marketingViewDetail') :
        require __DIR__ . '/views/company/marketing/marketingViewDetail.php';
        break;
    case ('/Marketing/marketingWebsiteSettings') :
        require __DIR__ . '/views/company/marketing/settings/marketingWebsiteSettings.php';
        break;
    case ('/Marketing/campaignAjax') :
        require __DIR__ . '/functions/marketing/campaign/campaign.php';
        break;
    case ('/Marketing/settingsAjax') :
        require __DIR__ . '/functions/marketing/settings/settings.php';
        break;
    case ('/Marketing/listingAjax') :
        require __DIR__ . '/functions/marketing/listing/listing.php';
        break;
    case ('/Marketing/view') :
        require __DIR__ . '/views/company/marketing/listing/view.php';
        break;
    case ('/Marketing/viewEditAjax') :
        require __DIR__ . '/functions/marketing/listing/viewEdit.php';
        break;
    case ('/Marketing/editAjax') :
        require __DIR__ . '/functions/marketing/listing/edit.php';
        break;
    case ('/Marketing/edit') :
        require __DIR__ . '/views/company/marketing/listing/edit.php';
        break;
    case ('/Marketing/flyer') :
        require __DIR__ . '/functions/marketing/flyer/flyer.php';
        break;
    case ('/Marketing/map') :
        require __DIR__ . '/functions/marketing/listing/map.php';
        break;
    case ('/Marketing/MarketingPropertiesMap') :
        require __DIR__ . '/views/company/marketing/listing/map.php';
        break;

    case ('/Marketing_Flyer_Templates/FlyerTemplate1') :
        require __DIR__ . '/views/company/marketing/flyer/flyer-temp1.php';
        break;
    case ('/Marketing_Flyer_Templates/FlyerTemplate2') :
        require __DIR__ . '/views/company/marketing/flyer/flyer-temp2.php';
        break;
    case ('/Marketing_Flyer_Templates/FlyerTemplate6') :
        require __DIR__ . '/views/company/marketing/flyer/flyer-temp6.php';
        break;

    case ('/Marketing_Flyer_Templates/FlyerTemplate3') :
        require __DIR__ . '/views/company/marketing/flyer/flyer-temp3.php';
        break;
    case ('/Marketing_Flyer_Templates/FlyerTemplate4') :
        require __DIR__ . '/views/company/marketing/flyer/flyer-temp4.php';
        break;
    case ('/Marketing_Flyer_Templates/FlyerTemplate5') :
        require __DIR__ . '/views/company/marketing/flyer/flyer-temp5.php';
        break;


    //marketing routes end
    case ('/ticket/tickets') :
        require __DIR__ . '/views/company/maintenance/view.php';
        break;
    case ('/WorkOrder/WorkOrders') :
        require __DIR__ . '/views/company/maintenance/work_orders/view.php';
        break;
    case ('/PurchaseOrder/PurchaseOrderListing') :
        require __DIR__ . '/views/company/maintenance/purchase_orders/view.php';
        break;
    case ('/PurchaseOrder/PurchaseOrder') :
        require __DIR__ . '/views/company/maintenance/purchase_orders/add.php';
        break;
    case ('/PurchaseOrder/EditPurchaseOrder') :
        require __DIR__ . '/views/company/maintenance/purchase_orders/edit.php';
        break;
    case ('/PurchaseOrder/PurchaseOrderView') :
        require __DIR__ . '/views/company/maintenance/purchase_orders/viewinfo.php';
        break;
    case ('/PurchaseOrder/ajaxData') :
        require __DIR__ . '/functions/maintenance/purchase_orders/purchaseOrder.php';
        break;
    case ('/LostAndFound/LostAndFound') :
        require __DIR__ . '/views/company/maintenance/lost_found/view.php';
        break;
    case ('/LostAndFound/AddLostItem') :
        require __DIR__ . '/views/company/maintenance/lost_found/add-lost-item.php';
        break;
    case ('/LostAndFound/editLostFoundItems') :
        require __DIR__ . '/views/company/maintenance/lost_found/edit-item.php';
        break;

    case ('/LostAndFound/AddFoundItem') :
        require __DIR__ . '/views/company/maintenance/lost_found/add-find-item.php';
        break;
    case ('/MileageLog/MileageLog') :
        require __DIR__ . '/views/company/maintenance/mileage_log/view.php';
        break;
    case ('/Inspection/Inspection') :
        require __DIR__ . '/views/company/maintenance/inspection/view.php';
        break;
    case ('/Maintenance/Inventory') :
        require __DIR__ . '/views/company/maintenance/inventory_tracker/view.php';
        break;
    case ('/Maintenance/InventorySetup') :
        require __DIR__ . '/views/company/maintenance/inventory_tracker/viewSetup.php';
        break;
    case ('/Accounting/Accounting') :
        require __DIR__ . '/views/company/accounting/receivables/view.php';
        break;
    case ('/Accounting/PendingGLPosting') :
        require __DIR__ . '/views/company/accounting/pending_transaction/tenant_transaction_nsf.php';
        break;
    case ('/Accounting/AjaxPendingGLPosting') :
        require __DIR__ . '/functions/accounting/pending_transaction/tenant_transaction_nsf.php';
        break;
    case ('/Accounting/paybills') :
        require __DIR__ . '/views/company/accounting/pay_bills/view.php';
        break;
    case ('/Accounting/ConsolidatedInvoice') :
        require __DIR__ . '/views/company/accounting/invoices/list.php';
        break;
    case ('/Accounting/RecurringBill') :
        require __DIR__ . '/views/company/accounting/recurring/view.php';
        break;
    case ('/Accounting/BankRegister') :
        require __DIR__ . '/views/company/accounting/banking/view.php';
        break;
    case ('/Accounting/checkLists') :
        require __DIR__ . '/views/company/accounting/banking/check.php';
        break;
    case ('/Accounting/JournalEntries') :
        require __DIR__ . '/views/company/accounting/journal/list.php';
        break;
    case ('/Accounting/Budgeting') :
        require __DIR__ . '/views/company/accounting/budgeting/view.php';
        break;
    case ('/Accounting/Budgeting/AddBudget') :
        require __DIR__ . '/views/company/accounting/budgeting/newBudget.php';
        break;
    case ('/Accounting/Budgeting/EditBudget') :
        require __DIR__ . '/views/company/accounting/budgeting/editBudget.php';
        break;
    case ('/Accounting/AccountReconcile') :
        require __DIR__ . '/views/company/accounting/bank_reconcile/addList.php';
        break;
    case ('/Accounting/AccountReconcile/View') :
        require __DIR__ . '/views/company/accounting/bank_reconcile/view.php';
        break;
    case ('/AccountReconcile-Ajax') :
        require __DIR__ . '/functions/accounting/bankReconcile.php';
        break;
    case ('/EFTPayments/EFTTenant') :
        require __DIR__ . '/views/company/accounting/eft/view.php';
        break;
    case ('/Accounting/AccountClosing') :
        require __DIR__ . '/views/company/accounting/account_closing/view.php';
        break;
    case ('/Accounting/UtilityBilling') :
        require __DIR__ . '/views/company/accounting/utility_billing/view.php';
        break;
    case ('/UtilityBilling-Ajax') :
        require __DIR__ . '/functions/accounting/utilityBilling.php';
        break;
    case ('/MultiPay/MultiPay') :
        require __DIR__ . '/views/company/accounting/multi_payments/view.php';
        break;
    case ('/MultiPay/PaymentPlan') :
        require __DIR__ . '/views/company/accounting/payment_plan/view.php';
        break;

    case ('/Tenant/TransferTenant') :

        require __DIR__ . '/views/company/tenants/tenant_transfer.php';
        break;
    case ('/TenantTransfer-Ajax') :
        require __DIR__ . '/functions/people/tenant/tenant_transfer.php';
        break;
    case ('/ShortTermRental-Ajax') :
        require __DIR__ . '/functions/people/tenant/shortTermRental.php';
        break;
    /*Moveout Tenant Module*/
    case ('/Tenant/MoveOutTenant') :
        require __DIR__ . '/views/company/tenants/moveOutTenant.php';
        break;

    case ('/Tenant/MoveOut') :
        require __DIR__ . '/views/company/tenants/moveOutTenantList.php';
        break;
    /*Moveout Tenant Module*/

    /* Communication Email module Function */
    case ('/Communication/ComposeMailAjax') :
        require __DIR__ . '/functions/communication/email/composeMailAjax.php';
        break;
    /* Communication module route */
    case ('/Communication/InboxMails') :
        require __DIR__ . '/views/company/communication/email/inboxEmail.php';
        break;
    case ('/Communication/DraftedMails') :
        require __DIR__ . '/views/company/communication/email/draftEmail.php';
        break;
    case ('/Communication/SentEmails') :
        require __DIR__ . '/views/company/communication/email/sentEmail.php';
        break;
    case ('/Communication/ComposeEmail') :
        require __DIR__ . '/views/company/communication/email/composeEmail.php';
        break;


    /* Communication Email Tenant route */
    case ('/Tenant/Communication/InboxMails') :
        require __DIR__ . '/views/company/communication/email/inboxEmail.php';
        break;
    case ('/Tenant/Communication/DraftedMails') :
        require __DIR__ . '/views/company/communication/email/draftEmail.php';
        break;
    case ('/Tenant/Communication/SentEmails') :
        require __DIR__ . '/views/company/communication/email/sentEmail.php';
        break;
    case ('/Tenant/Communication/ComposeEmail') :
        require __DIR__ . '/views/company/communication/email/composeEmail.php';
        break;

    /* Communication Email Vendor route */
    case ('/Vendor/Communication/InboxMails') :
        require __DIR__ . '/views/company/communication/email/inboxEmail.php';
        break;
    case ('/Vendor/Communication/DraftedMails') :
        require __DIR__ . '/views/company/communication/email/draftEmail.php';
        break;
    case ('/Vendor/Communication/SentEmails') :
        require __DIR__ . '/views/company/communication/email/sentEmail.php';
        break;
    case ('/Vendor/Communication/ComposeEmail') :
        require __DIR__ . '/views/company/communication/email/composeEmail.php';
        break;

    /*Routes of text message*/
    case ('/Communication/TextMessage') :
        require __DIR__ . '/views/company/communication/textMessage/sentText.php';
        break;
    case ('/Communication/InboxText') :
        require __DIR__ . '/views/company/communication/textMessage/inboxText.php';
        break;
    case ('/Communication/TextMessageDrafts') :
        require __DIR__ . '/views/company/communication/textMessage/draftText.php';
        break;

    case ('/Communication/AddTextMessage') :
        require __DIR__ . '/views/company/communication/textMessage/composeText.php';
        break;

    /*Routes of Group Message/Email*/
    case ('/Communication/DraftedGroupMessages') :
        require __DIR__ . '/views/company/communication/groupMessage/draftGMessage.php';
        break;
    case ('/Communication/AddGroupMessage') :
        require __DIR__ . '/views/company/communication/groupMessage/composeMessage.php';
    break;
    case ('/Communication/GroupMessageAjax') :
        require __DIR__ . '/functions/communication/groupMessage/groupMessageAjax.php';
        break;

    /*Routes of Task Reminder*/
    case ('/Communication/AddNewTaskAndReminders') :
        require __DIR__ . '/views/company/communication/task_reminders/add.php';
        break;
    case ('/taskReminder-Ajax') :
        require __DIR__ . '/functions/communication/taskReminderAjax.php';
        break;
    /*Routes of Group Task Reminder*/
    /*Routes of Add Announcement*/
    case ('/Announcement/AddAnnouncement') :
        require __DIR__ . '/views/company/communication/announcement/addAnnouncement.php';
        break;
    /*Routes of Add Announcement*/
    /*Routes of Add Package*/
    case ('/Package/AddPackages') :
        require __DIR__ . '/views/company/communication/package_tracker/addPackage.php';
        break;
    /*Routes of Add Package*/
    /*Routes of Add Time Sheet*/
    case ('/Communication/AddTimeSheet') :
        require __DIR__ . '/views/company/communication/time_sheet/addTimeSheet.php';
        break;
    /*Routes of Add Time Sheet*/
    /*Routes of Add Time Sheet*/
    case ('/Communication/NewInTouch') :
        require __DIR__ . '/views/company/communication/in-touch/newInTouch.php';
        break;
    case ('/in-touch-ajax') :
        require __DIR__ . '/functions/communication/inTouch/InTouch.php';
        break;
    case ('/in-touch-ajax-list') :
        require __DIR__ . '/functions/communication/inTouch/InTouchList.php';
        break;
    /*Routes of Add Time Sheet*/

    case ('/MasterData/Documents') :
        require __DIR__ . '/views/company/communication/letters_notices/documents.php';
        break;
    case ('/LettersNoticesDoc-Ajax') :
        require __DIR__ . '/functions/communication/lettersNoticesAjax.php';
        break;
    case ('/PackageTracker-Ajax') :
        require __DIR__ . '/functions/communication/packageTracker/packageTrackerAjax.php';
        break;
    case ('/Communication/Conversation') :
        require __DIR__ . '/views/company/communication/conversation/view.php';
        break;
    case ('/Conversation-Ajax') :

        require __DIR__ . '/functions/communication/conversationAjax.php';
        break;
    case ('/Communication/GroupMessage') :
        require __DIR__ . '/views/company/communication/groupMessage/sentGroupMessage.php';
        break;
    case ('/Communication/NewTaskAndReminders') :
        require __DIR__ . '/views/company/communication/task_reminders/view.php';
        break;
    case ('/Communication/InTouch') :
        require __DIR__ . '/views/company/communication/in-touch/list.php';
        break;
    case ('/Communication/editInTouch') :
        require __DIR__ . '/views/company/communication/in-touch/editInTouch.php';
        break;
    case ('/Communication/PhoneCall') :
        require __DIR__ . '/views/company/communication/phone_call_log/view.php';
        break;
    case ('/Communication/TimeSheet') :
        require __DIR__ . '/views/company/communication/time_sheet/view.php';
        break;
    case ('/Communication/Chat') :
        require __DIR__ . '/views/company/communication/staff_chat_room/view.php';
        break;
    case ('/Communication/DailyVisitor') :
        require __DIR__ . '/views/company/communication/daily_visitor_log/view.php';
        break;
    case ('/Package/Packages') :
        require __DIR__ . '/views/company/communication/package_tracker/view.php';
        break;
    case ('/Announcement/Announcements') :
        require __DIR__ . '/views/company/communication/announcement/listAnnouncement.php';
        break;
    case ('/Announcement/AddAnnouncement') :
        require __DIR__ . '/views/company/communication/announcement/addAnnouncement.php';
        break;
    case ('/Announcement-Ajax') :
        require __DIR__ . '/functions/communication/announcementAjax.php';
        break;
    case ('/VendorPortalAnnouncement-Ajax') :
        require __DIR__ . '/functions/people/vendor/vendor-portal/announcementAjax.php';
        break;
    case ('/MasterData/AllAnnouncements') :
        require __DIR__ . '/views/company/communication/announcement/listAllAnnouncements.php';
        break;
    case ('/VendorPortal/Vendor/AllCompanyAdminAnnouncements') :
        require __DIR__ . '/views/company/vendor-portal/announcement/vendorListAllAnnouncements.php';
        break;
    case ('/OwnerPortalAnnouncement-Ajax') :
        require __DIR__ . '/functions/people/owner/ownerPortal/announcementAjax.php';
        break;
    case ('/Owner/MyAccount/AllCompanyAdminAnnouncements') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/announcement/ownerListAllAnnouncements.php';
        break;
    case ('/Communication/WaitingList') :
        require __DIR__ . '/views/company/communication/waiting_list/view.php';
        break;
    case ('/Tenant/ViewEditWatingList') :
        require __DIR__ . '/views/company/communication/waiting_list/waiting_list_view.php';
        break;
    case ('/waiting-list-ajax') :
        require __DIR__ . '/functions/communication/waiting_list/Waiting_List.php';
        break;
    case ('/MasterData/EsignatureUser') :
        require __DIR__ . '/views/company/communication/esign_history/view.php';
        break;
    /* custom field routes start */
    case ('/CustomField/add') :
        Route('/CustomField/add', null, 'Customfield', 'add');
        break;

    case ('/CustomField/get') :
        Route('/CustomField/get', null, 'Customfield', 'get');
        break;

    case ('/CustomField/edit') :
        Route('/CustomField/edit', null, 'Customfield', 'edit');
        break;

    case ('/CustomField/update') :
        Route('/CustomField/update', null, 'Customfield', 'update');
        break;

    case ('/CustomField/delete') :
        Route('/CustomField/delete', null, 'Customfield', 'delete');
        break;
    /* custom field routes end */



    /* entity company setup */
    case ('/User/AccountSetup') :
        Route('/User/AccountSetup', null, 'EntitySetup', 'view');
        break;
    case ('/User/AccountSetup/update') :
        Route('/User/AccountSetup/update', null, 'EntitySetup', 'update');
        break;

    case ('/MasterData/FlagBank') :
        require __DIR__ . '/views/flagbank/view.php';
        break;

    case ('/User/UserPlans') :
        require __DIR__ . '/views/admin/entityCompanySetup/existingPlan/list.php';
        break;
    case ('/Company/updatePastAnnouncement') :
        require __DIR__ . '/functions/companycron.php';
        break;

    /* entity property setup */

    case ('/add-property') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-portfolio-popup') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-manager-popup') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-attachgroup-popup') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-propertytype-popup') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-propertystyle-popup') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-propertysubtype-popup') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/get-property-detail') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-garageavail-popup') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-zipcode-popup') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-portfolioname') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-propertytype') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-propertysubtype') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-garagedata') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-managers') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-attachgroup') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-owners') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-owner-tab') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-latefee') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-keytag') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/keys-list') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/delete-key') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/get-key') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-managementfee') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-managementinfo') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-maintenanceinformation') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-Mortgageinfo') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-propertyloan') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-propertynotes') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/property/custom_field') :
        require __DIR__ . '/functions/property/customField.php';
        break;
    case ('/property/file_library') :
        require __DIR__ . '/functions/property/propertyFilelibrary.php';
        break;
    case ('/property/photos_videos') :
        require __DIR__ . '/functions/property/propertyPhotosvideos.php';
        break;
    case ('/get-notes') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/delete-notes') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-insurancetype') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-insurancetype') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-policytype') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-policytype') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-propertyinsurance') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/get-propertyinsurance') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/delete-propertyinsurance') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-warrantyinfomation') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/delete-warrantyInfo') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/get-warrantyInfo') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-fixturetype') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/fetch-all-fixture') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/get-vendorsdata') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/add-owneredvendors') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case ('/property-ajax') :
        require __DIR__ . '/functions/propertyDetail.php';
        break;
    case '/propertyView-ajax' :
        require __DIR__ . '/functions/property/propertyView.php';
        break;
    case ('/watingList-ajax') :
        require __DIR__ . '/functions/property/watingList.php';
        break;
    case ('/manageCharge-ajax') :
        require __DIR__ . '/functions/property/manageCharges.php';
        break;
    /* Vendor Module start */

    case ('/vendor-add-ajax') :
        require __DIR__ . '/functions/people/vendor/addVendor.php';
        break;
    case ('/Vendor/Vendor') :
        require __DIR__ . '/views/company/vendors/list.php';
        break;
    case ('/Vendor/AddVendor') :
        require __DIR__ . '/views/company/vendors/new_vendor.php';
        break;
    case ('/Vendor/EditVendor') :
        require __DIR__ . '/views/company/vendors/edit_vendor.php';
        break;
    case ('/vendor-view-ajax') :
        require __DIR__ . '/functions/people/vendor/ViewVendor.php';
        break;
    //vendor portal links

    case ('/VendorPortal/VentorMyaccount') :
        require __DIR__ . '/views/company/vendor-portal/ventor-myaccount.php';
        break;

    case ('/VendorPortal/NewBill') :
        require __DIR__ . '/views/company/vendor-portal/newBill.php';
        break;
    case ('/VendorPortal/NewBillAjax') :
        require __DIR__ . '/functions/people/vendor/vendor-portal/billsAjax.php';
        break;
    case ('/VendorPortal/viewBill') :
        require __DIR__ . '/views/company/vendor-portal/viewBill.php';
        break;
    case ('/VendorPortal/WorkOrderVendor') :
        require __DIR__ . '/views/company/vendor-portal/edit_workorder.php';
        break;

    case ('/VendorPortal/Vendor/PurchaseOrderListing') :
        require __DIR__ . '/views/company/vendor-portal/viewPurchaseOrder.php';
        break;
    case ('/VendorPortal/Vendor/PurchaseOrderListing') :
        require __DIR__ . '/views/company/vendor-portal/viewPurchaseOrder.php';
        break;

    case ('/VendorPortal/LostAndFoundVendor/LostAndFound') :
        require __DIR__ . '/views/company/vendor-portal/viewLostNFound.php';
        break;

    case ('/VendorPortal/LostAndFoundVendor/AddEditLostItem') :
        require __DIR__ . '/views/company/vendor-portal/lost_found/add-lost-item.php';
        break;
    case ('/VendorPortal/Reporting/Vendor1099SummaryReport') :
        require __DIR__ . '/views/company/vendor-portal/ReportView.php';
        break;

    case ('/vendorReportsAjax') :
        require __DIR__ . '/functions/people/vendor/vendor-portal/reportsView.php';
        break;

    /* Vendor Module end */
    /* Building Module start */
    case ('/Building/AddBuilding') :
        require __DIR__ . '/views/company/building/add.php';
        break;
    case ('/Building/EditBuilding') :
        Route('/Building/EditBuilding', null, 'EditBuilding', 'edit');
        break;
    case ('/Building/AddBuildingDetail') :
        require __DIR__ . '/functions/buildingDetail.php';
        break;
    case ('/get-company-data') :
        require __DIR__ . '/functions/buildingDetail.php';
        break;


    case ('/Building/BuildingModule') :
        require __DIR__ . '/views/company/building/list.php';
        break;
    case ('/Building/View') :
        require __DIR__ . '/views/company/building/view.php';
        break;
    case ('/building-ajax') :
        require __DIR__ . '/functions/buildingDetail.php';
        break;

    case ('/add-building-keytag') :
        require __DIR__ . '/functions/buildingDetail.php';
        break;
    case ('/get-building-key') :
        require __DIR__ . '/functions/buildingDetail.php';
        break;

    /* building Module ends here */

    /* Common Popups start here */
    case ('/add-pet-popup') :
        require __DIR__ . '/functions/commonPopup.php';
        break;
    case ('/add-key-popup') :
        require __DIR__ . '/functions/commonPopup.php';
        break;

    case ('/common-popup-ajax') :
        require __DIR__ . '/functions/commonPopup.php';
        break;

    case ('/fetch-allpetdata') :
        require __DIR__ . '/functions/commonPopup.php';
        break;
    case ('/fetch-keydata') :
        require __DIR__ . '/functions/commonPopup.php';
        break;
    case ('/add-amenity-popup') :
        require __DIR__ . '/functions/commonPopup.php';
        break;
    case ('/fetch-all-amenities') :
        require __DIR__ . '/functions/commonPopup.php';
        break;

    case ('/fetch-all-complaint-type') :
        require __DIR__ . '/functions/commonPopup.php';
        break;
    case ('/add-complaint-type-popup') :
        require __DIR__ . '/functions/commonPopup.php';
        break;
    case ('/get-company-default-data') :
        require __DIR__ . '/functions/commonPopup.php';
        break;



    /* Common Popups ends here */
    case ('/flag-ajax') :
        require __DIR__ . '/functions/commonModule/Flag.php';
        break;

    /* Unit add functions */
    case '/propertyUnitAjax' :
        require __DIR__ . '/functions/property/unit/propertyUnitAjax.php';
        break;
    /* Unit add functions */


    /* Unit add functions */
    case ('/Property/PropertyInspection') :
        require __DIR__ . '/views/company/properties/propertyInspection.php';
        break;

    case ('/Building/BuildingInspection') :
        require __DIR__ . '/views/company/building/newInspection.php';
        break;

    case ('/Unit/UnitInspection') :
        require __DIR__ . '/views/company/properties/unit/unitInspection.php';
        break;

    case ('/Inspection/InspectionView') :
        require __DIR__ . '/views/company/maintenance/inspection/inspectionView.php';
        break;
    /* Unit add functions */
    case ('/CheckSetupAjax') :
        require __DIR__ . '/functions/accounting/checkSetup.php';
        break;
    //vendor portal links start
    case ('/VendorPortal/Vendor/MyAccount') :
        require __DIR__ . '/views/company/vendor-portal/vendor-myaccount.php';
        break;
    case ('/VendorPortal/Vendor/WorkOrderVendor') :
        require __DIR__ . '/views/company/vendor-portal/vendor-work-order.php';
        break;
    case ('/VendorPortal/Vendor/Transactions') :
        require __DIR__ . '/views/company/vendor-portal/vendor-transaction.php';
        break;
    case ('/VendorPortal/Vendor/FileRepository') :
        require __DIR__ . '/views/company/vendor-portal/vendor-file.php';
        break;
    case ('/VendorPortal/Vendor/VendorInsurance') :
        require __DIR__ . '/views/company/vendor-portal/vendor-insurance.php';
        break;
    case ('/VendorPortal/Vendor/ManageBill') :
        require __DIR__ . '/views/company/vendor-portal/manage-bills.php';
        break;
    case ('/VendorPortal/Vendor/Reporting/Reporting') :
        require __DIR__ . '/views/company/vendor-portal/vendor-reports.php';
        break;
    case ('/VendorPortal/login') :
        require __DIR__ . '/views/company/vendor-portal/vendor-login.php';
        break;
    case ('/VendorPortal/ForgotPassword') :
        require __DIR__ . '/views/company/vendor-portal/forgotPassword.php';
        break;
    case ('/Vendor-portal-ajax') :
        require __DIR__ . '/functions/people/vendor/vendor-portal/vendorPortal.php';
        break;
    case ('/VendorPortal/ChangePassword') :
        require __DIR__ . '/views/company/vendor-portal/changePassword.php';
        break;
    case ('/VendorPortalLogin-Ajax') :
        require __DIR__ . '/functions/people/vendor/vendor-portal/portalLoginAjax.php';
        break;
    case ('/VendorPortal/Login/ResetPassword') :
        require __DIR__ . '/views/company/vendor-portal/resetPassword.php';
        break;
    //vendor portal links end
    case ('/vendorEdit/payment') :
        require __DIR__ . '/functions/people/vendor/paymentVendor.php';
        break;
    default:
        require __DIR__ . '/views/auth/login.php';
        break;
    case ('/stripe') :
        require __DIR__ . '/views/company/stripe/stripe.php';
        break;
    case ('/StripeAjax') :
        require __DIR__ . '/functions/';
        break;
    //chat module
    case ('/chat-ajax') :
        require __DIR__ . '/functions/chat/chatAjax.php';
        break;

    /*call log  starts here */
    case ('/fetch-all-call-type') :
        require __DIR__ . '/functions/commonPopup.php';
        break;

    case ('/phoneCallLog-Ajax') :
        require __DIR__ . '/functions/communication/phoneCallLogAjax.php';
        break;

    /* Communication Text module Function */
    case ('/Communication/composeTextMessageAjax') :
        require __DIR__ . '/functions/communication/textMessage/composeTextMessageAjax.php';
        break;
    //time sheet
    case ('/timeSheet-Ajax') :
        require __DIR__ . '/functions/communication/timeSheet/TimeSheetAjax.php';
        break;
    case ('/Communication/EditTimeSheet') :
        require __DIR__ . '/views/company/communication/time_sheet/editTimeSheet.php';
        break;
    //daily visitor log
    case ('/dailyVisitorLog-Ajax') :
        require __DIR__ . '/functions/communication/dailyVisitorLog/DailyVisitorLogAjax.php';
        break;

    case ('/getTimeZone') :
        require __DIR__ . '/views/layouts/getTimezone.php';
        break;

    case ('/stripe/checkout') :
        require __DIR__ . '/views/company/stripe/checkoutForm.php';
        break;

    case ('/stripeCheckout') :
        require __DIR__ . '/functions/stripe/stripe.php';
        break;

    case ('/getVictigRecord') :
        require __DIR__ . '/views/company/victig/index.php';
        break;

    /*For Report module*/
    case ('/Reporting-Ajax') :
        require __DIR__ . '/functions/reports/reportsPopupData.php';
        break;
    case '/payment-ajax' :
        require __DIR__ . '/functions/payment.php';
        break;

    case '/payment-stripe-ajax' :
        require __DIR__ . '/functions/payment/payment.php';
        break;
    /*For Report module*/

    case ('/plaid/curl') :
        require __DIR__ . '/functions/plaid/PlaidApis.php';
        break;

    case ('reocceringPmPayment') :
        require __DIR__ . '/functions/plaid/PlaidApis.php';
        break;

    /*Property inventory route */
    case ('/Property-inventory-ajax') :
        require __DIR__ . '/functions/property/propertyInventory.php';
        break;

    /* Subscription php */
    case ('/company-subscription-ajax') :
        require __DIR__ . '/functions/payment/subscription.php';
        break;

    case ('/combo-grid-global-ajax') :
        require __DIR__ . '/functions/commonModule/GlobalComboGrid.php';
        break;

    /*For Accounting Module */
    case ('/Accounting/newConsolidatedInvoice') :
        require __DIR__ . '/views/company/accounting/invoices/add.php';
        // Route('/Accounting/newConsolidatedInvoice', 'accounting', 'invoice', 'add');
        break;

    /*For Accounting Module */

    case ('/Accounting/invoice-ajax') :
        require __DIR__ . '/functions/accounting/invoice.php';
        break;
    case ('/Accounting/ViewInvoice') :
        require __DIR__ . '/views/company/accounting/invoices/view.php';
        break;
    case ('/Accounting/EditInvoice') :
        require __DIR__ . '/views/company/accounting/invoices/edit.php';
        break;
    case ('/Accounting/addRecurringBill') :
        require __DIR__ . '/views/company/accounting/recurring/recurringBill.php';
        break;
    case ('/Accounting/EditRecurringBill') :
        require __DIR__ . '/views/company/accounting/recurring/editrecurringBill.php';
        break;


    case ('/accountingReceivable') :
        require __DIR__ . '/functions/accounting/accountingReceivable.php';
        break;
    case ('/accountingReceivable') :
        require __DIR__ . '/functions/people/accounting/accountingReceivable.php';
        break;

    case ('/Accounting/receivebatchpayment') :
        require __DIR__ . '/views/company/accounting/receivables/receivebatchpayment.php';
        break;

    case ('/global-user-module-ajax') :
        require __DIR__ . '/functions/commonModule/GlobalUserModule.php';
        break;

    case ('/global-get-module-ajax') :
        require __DIR__ . '/functions/commonModule/GlobalGetModule.php';
        break;

    case ('/newBill-ajax') :
        require __DIR__ . '/functions/newBills/billsAjax.php';
        break;

    case ('/newJournalEntry') :
        require __DIR__ . '/views/company/accounting/journal/add.php';
        break;
    case ('/Accounting/ViewJournalEntry') :
        require __DIR__ . '/views/company/accounting/journal/view.php';
        break;

    case ('/Accounting/journal-entry-ajax') :
        require __DIR__ . '/functions/accounting/journalEntry/journalEntry.php';
        break;




    case ('/EsignatureUser/esignView') :
        require __DIR__ . '/views/company/communication/letters_notices/eSignMail.php';
        break;



    case ('/paymentplan-ajax') :
        require __DIR__ . '/functions/accounting/payment_plan/payment_plan.php';
        break;
    case ('/Tenantlisting/payment') :
        require __DIR__ . '/functions/people/tenant/tenantAjax.php';
        break; case ('/vendorportal/transactions') :
    require __DIR__ . '/functions/people/vendor/vendor-portal/vendortransactions.php';
    break;

    /*recurring invoices */
    case ('/Accounting/RecurringInvoices') :
        require __DIR__ . '/views/company/accounting/RecurringInvoices/list.php';
        break;
    case ('/Accounting/newRecurringInvoice') :
        require __DIR__ . '/views/company/accounting/RecurringInvoices/add.php';
        // Route('/Accounting/newConsolidatedInvoice', 'accounting', 'invoice', 'add');
        break;
    /*For Accounting Module */
    case ('/Accounting/recurring-invoice-ajax') :
        require __DIR__ . '/functions/accounting/recurringInvoice.php';
        break;
    case ('/Accounting/ViewRecurringInvoice') :
        require __DIR__ . '/views/company/accounting/RecurringInvoices/view.php';
        break;
    case ('/Accounting/RecurringEditInvoice') :
        require __DIR__ . '/views/company/accounting/RecurringInvoices/edit.php';
        break;

    case ('/Accounting/RecurringChecks') :
        require __DIR__ . '/views/company/accounting/recurring/recurringCheck.php';
        break;
    case ('/Accounting/addRecurringChecks') :
        require __DIR__ . '/views/company/accounting/recurring/addRecurringChecks.php';
        break;
    case ('/Accounting/EditRecurringCheck') :
        require __DIR__ . '/views/company/accounting/recurring/editrecurringCheck.php';
        break;
    case ('/Accounting/recurring-check-ajax') :
        require __DIR__ . '/functions/accounting/recurringChecks.php';
        break;
    case ('/EsignUser/saveEsignImage') :
        require __DIR__ . '/functions/communication/lettersNoticesAjax.php';
        break;

    case ('/budgeting-ajax') :
        require __DIR__ . '/functions/accounting/Budgeting.php';
        break;
    case ('/payBills-ajax') :
        require __DIR__ . '/functions/accounting/paybills/Paybills.php';
        break;

    case ('/Calendar/Calendar') :
        require __DIR__ . '/views/company/calendar_main/calendar.php';
        break;
    case ('/Calendar/RequestAccepted') :
        require __DIR__ . '/views/company/calendar_main/requestAccepted.php';
        break;

    case ('/elastic/ajax') :
        require __DIR__ . '/functions/elasticSearch/elastic_search.php';
        break;
    case ('/500') :
        require __DIR__ . '/views/500400.php';
        break;
    /* Process Management Fee Starts */
    case ('/Accounting/ProcessManagementFee') :
        require __DIR__ . '/views/company/accounting/processManagementFee/setManagementFee.php';
        break;
    case ('/Accounting/RunManagementFee') :
        require __DIR__ . '/views/company/accounting/processManagementFee/runManagementFee.php';
        break;
    case ('/Accounting/PaidManagementFee') :
        require __DIR__ . '/views/company/accounting/processManagementFee/paidManagementFee.php';
        break;
    case ('/ProcessManagementFee-Ajax') :
        require __DIR__ . '/functions/accounting/processManagementFee.php';
        break;

    case ('/test/module') :
        require __DIR__ . '/functions/testing/xmlDetail.php';
        break;

    /* Process Management Fee Ends */
    case ('/calendar/ajax') :
        require __DIR__ . '/functions/calendarData.php';
        break;

    /* Owner Email and Text Message */
    case ('/Owner/Communication/InboxMails') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/email/inboxOwnerEmail.php';
        break;
    case ('/Owner/Communication/DraftedMails') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/email/draftOwnerEmail.php';
        break;
    case ('/Owner/Communication/SentEmails') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/email/sentOwnerEmail.php';
        break;
    case ('/Owner/Communication/ComposeEmail') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/email/composeOwnerEmail.php';
        break;
    case ('/OwnerPortal/Communication/ComposeOwnerMailAjax') :
        require __DIR__ . '/functions/people/owner/ownerPortal/email/composeOwnerMailAjax.php';
        break;
    case ('/OwnerPortal/Communication/ComposeOwnerTextMessageAjax') :
        require __DIR__ . '/functions/people/owner/ownerPortal/textMessage/composeOwnerTextMessageAjax.php';
        break;
    case ('/Owner/Communication/TextMessage') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/textMessage/sentOwnerText.php';
        break;
    case ('/Owner/Communication/InboxText') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/textMessage/inboxOwnerText.php';
        break;
    case ('/Owner/Communication/TextMessageDrafts') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/textMessage/draftOwnerText.php';
        break;
    case ('/Owner/Communication/AddTextMessage') :
        require __DIR__ . '/views/company/owners/ownerPortal/communication/textMessage/composeOwnerTextMessage.php';
        break;
    /* Owner Email and Text Message */

    /*Vendor Email and Text Message*/
    case ('/VendorPortal/Vendor/Communication/SentEmails') :
        require __DIR__ . '/views/company/vendor-portal/sentVendorEmail.php';
        break;
    case ('/VendorPortal/Communication/ComposeEmail') :
        require __DIR__ . '/views/company/vendor-portal/email/composeVendorEmail.php';
        break;
     case ('/VendorPortal/Communication/DraftedMails') :
        require __DIR__ . '/views/company/vendor-portal/email/draftVendorEmail.php';
        break;
     case ('/VendorPortal/Communication/InboxText') :
        require __DIR__ . '/views/company/vendor-portal/textMessage/inboxVendorText.php';
        break;
    case ('/VendorPortal/Communication/TextMessageDrafts') :
        require __DIR__ . '/views/company/vendor-portal/textMessage/draftVendorText.php';
        break;
     case ('/VendorPortal/Communication/InboxMails') :
        require __DIR__ . '/views/company/vendor-portal/email/inboxVendorEmail.php';
        break;
     case ('/VendorPortal/Communication/TextMessage') :
        require __DIR__ . '/views/company/vendor-portal/textMessage/sentVendorText.php';
        break;
     case ('/VendorPortal/Communication/AddTextMessage') :
        require __DIR__ . '/views/company/vendor-portal/textMessage/composeVendorText.php';
        break;
    case ('/VendorPortal/Communication/ComposeVendorMailAjax') :
        require __DIR__ . '/functions/people/vendor/vendor-portal/communication/composeVendorMailAjax.php';
        break;
    case ('/VendorPortal/Communication/ComposeVendorTextMessageAjax') :
        require __DIR__ . '/functions/people/vendor/vendor-portal/textMessage/composeVendorTextMessageAjax.php';
        break;
    /*End Vendor Email and Text Message*/

        // report balance detail
 case ('/CommonReportsModal-Ajax') :
        require __DIR__ . '/functions/reports/commonReportsFunction.php';
        break;

case ('/Reporting/BalanceSheetDetailedReport') :
        require __DIR__ . '/views/company/reports_model/balanceReportView.php';
        break;


    case ('/notification-ajax') :
        require __DIR__ . '/functions/notification/notificationsAjax.php';
        break;

    case ('/Alert/Notifications') :
        require __DIR__ . '/views/notification/newNotifications.php';
        break;
    /*Notification tenant portal view 18-3-2020*/
    case ('/Tenant-portal/Tenant/Notifications') :
        require __DIR__ . '/views/company/tenants/portalNotification.php';
        break;
    /*Notification tenant portal view 18-3-2020*/

    case '/Lease/LeasesAffordability' :
        require __DIR__ . '/views/company/leases/calculator/calculatorView.php';
        break;
    case '/Vendor/FormarVendor' :
        require __DIR__ . '/views/company/vendors/formarVendor.php';
        break;
    case '/Vendor/FormarOwner' :
        require __DIR__ . '/views/company/owners/formarOwner.php';
        break;
    case '/Lease/calculatorAjax' :
        require __DIR__ . '/functions/leases/calculatorAjax.php';
        break;
    case '/Lease/ManualLeaseRequest' :
        require __DIR__ . '/views/company/leases/leases/leaseRequest.php';
        break;
    case '/Lease/RenewLeaseRequest' :
        require __DIR__ . '/views/company/leases/leases/RenewLeaseRequest.php';
        break;

    case('/OwnerPortal/NewBill') :
        require __DIR__ . '/views/company/owners/ownerPortal/maintenance/newBill.php';
        break;

    case('/Owner/MyAccount/OwnerPurchaseOrderView'):
        require __DIR__ . '/views/company/owners/ownerPortal/maintenance/viewinfo.php';
        break;

    /*Elastic search starts*/
    case ('/elastic/search') :
        require __DIR__ . '/functions/elasticSearch/elasticSearch.php';
        break;

    /*Elastic search ends*/

    case '/User/ManageGroupsAjax' :
        require __DIR__ . '/functions/adminUsers/manageGroupsData.php';
        break;
}