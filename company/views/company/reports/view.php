<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/functions/reports/reports.php");
// include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/reports_model/modal.php");
//19-2-2020
// include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/reports_view/balance_sheet/modal.php");
// include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/reports_view/tenant_reports/modal/tenant_reports_modal.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/reports/commonReportsModal.php");




$variClassName1= reports::popupsReportSecond('Tenant Report');
$variClassName2= reports::popupsReportSecond('Maintenance Reports');
$variClassName3= reports::popupsReportSecond('Owner Reports');
$variClassName4= reports::popupsReportSecond('Vendor Reports');
$variClassName5= reports::popupsReportSecond('Contact Reports');
$variClassName6= reports::popupsReportSecond('Property Reports');
//$variClassName7= reports::popupsReportSecond('Property Reports');
$variClassName8= reports::popupsReportSecond('Inventory Reports');
$variClassName9= reports::popupsReportSecond('Guest Card Reports');
$variClassName10= reports::popupsReportSecond('Company Vehicle Reports');
$variClassName11= reports::popupsReportSecond('Employee Reports');
/*echo "<pre>";
print_r($variClassName2); die();*/

/*echo ($variClassName1);die;*/
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/functions/reports/reports.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#reports-one" aria-controls="home" role="tab" data-toggle="tab">Reports-All</a></li>
                            <li role="presentation"><a href="#reports-two" aria-controls="profile" role="tab" data-toggle="tab">General Financial</a></li>
                            <li role="presentation"><a href="#reports-three" aria-controls="profile" role="tab" data-toggle="tab">Tenant</a></li>
                            <li role="presentation"><a href="#reports-four" aria-controls="profile" role="tab" data-toggle="tab">Property</a></li>
                            <li role="presentation"><a href="#reports-five" aria-controls="profile" role="tab" data-toggle="tab">Owner</a></li>
                            <li role="presentation"><a href="#reports-six" aria-controls="profile" role="tab" data-toggle="tab">Vendor</a></li>
                            <li role="presentation"><a href="#reports-seven" aria-controls="profile" role="tab" data-toggle="tab">Maintenance</a></li>
                            <li role="presentation"><a href="#reports-eight" aria-controls="profile" role="tab" data-toggle="tab">Transaction</a></li>
                            <li role="presentation"><a href="#reports-nine" aria-controls="profile" role="tab" data-toggle="tab">Inventory</a></li>
                            <li role="presentation"><a href="#reports-ten" aria-controls="profile" role="tab" data-toggle="tab">Contact</a></li>
                            <li role="presentation"><a href="#reports-eleven" aria-controls="profile" role="tab" data-toggle="tab">Guest Card</a></li>
                            <li role="presentation"><a href="#reports-twelve" aria-controls="profile" role="tab" data-toggle="tab">Vehicle</a></li>
                            <li role="presentation"><a href="#reports-thirteen" aria-controls="profile" role="tab" data-toggle="tab">Employee</a></li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="reports-one">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <div class="reports_hdr" >Reports</div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-5">
                                                <div class="reports-column">
                                                    <h3>General Financial Reports</h3>
                                                    <ul>
                                                        <li><a href="javascript:;">Balance Sheet</a></li>
                                                        <li><a href="javascript:;">Balance Sheet Detail</a></li>
                                                        <li><a href="javascript:;">Balance Sheet Consolidated</a></li>
                                                        <li><a href="javascript:;">Balance Sheet Comparison(2 years)</a></li>
                                                        <li><a href="javascript:;">Income Statement</a></li>
                                                        <li><a href="javascript:;">Income Statement Details</a></li>
                                                        <li><a href="javascript:;">Income Statement - 12-Months Comparison</a></li>
                                                        <li><a href="javascript:;">Income Statement Comparison (2 years)</a></li>
                                                        <li><a href="javascript:;">Income Statement Consolidated</a></li>
                                                        <li><a href="javascript:;">Cash Flow</a></li>
                                                        <li><a href="javascript:;">Cash Flow-12 Months</a></li>
                                                        <li><a href="javascript:;">Cash Flow Comparison</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss Detail</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss - 12 Months</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss Comparison (2 years)</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss Consolidated</a></li>
                                                        <li><a href="javascript:;">Equity Statement</a></li>
                                                        <li><a href="javascript:;">Account Totals</a></li>
                                                        <li><a href="javascript:;">Expense Distribution</a></li>
                                                        <li><a href="javascript:;">Expense Register</a></li>
                                                        <li><a href="javascript:;">General Ledger</a></li>
                                                        <li><a href="javascript:;">Security Deposit</a></li>
                                                        <li><a href="javascript:;">Open Invoices</a></li>
                                                        <li><a href="javascript:;">Collection</a></li>
                                                        <li><a href="javascript:;">A/R Aging Summary</a></li>
                                                        <li><a href="javascript:;">A/R Aging Detail</a></li>
                                                        <li><a href="javascript:;">A/P Aging Summary</a></li>
                                                        <li><a href="javascript:;">A/P Aging Detail</a></li>
                                                        <li><a href="javascript:;">Expenses by Vendor</a></li>
                                                        <li><a href="javascript:;">Property Statement</a></li>
                                                        <li><a href="javascript:;">Bank Account Activity</a></li>
                                                        <li><a href="javascript:;">Voided Checks</a></li>
                                                        <li><a href="javascript:;">Trial Balance</a></li>
                                                        <li><a href="javascript:;">Trial Balance Consolidated</a></li>
                                                        <li><a href="javascript:;">Adjusted Trial Balance</a></li>
                                                        <li><a href="javascript:;">Adjusting Journal Entries</a></li>
                                                        <li><a href="javascript:;">Budget vs. Actual</a></li>
                                                        <li><a href="javascript:;">Budget-to-Actual Comparison</a></li>

                                                        <li><a href="javascript:;">Check Register</a></li>
                                                        <li><a href="javascript:;">Deposit Register</a></li>

                                                        <li><a href="javascript:;">Chart of Accounts</a></li>
                                                        <li><a href="javascript:;">Journal Entry</a></li>
                                                        <li><a href="javascript:;">Transaction Detail by Account</a></li>
                                                        <li><a href="javascript:;">Recent Automatic Transactions</a></li>
                                                        <li><a href="javascript:;">Recurring Journal Entries</a></li>
                                                        <li><a href="javascript:;">Charge Detail</a></li>
                                                        <li><a href="javascript:;">Income Register</a></li>
                                                        <li><a href="javascript:;">Audit Log</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="reports-column">
                                                    <h3>Tenant Reports</h3>
                                                    <ul  class="listPopupdata">
                                                        <?php echo $variClassName1;?>

                                                    </ul>
                                                </div>

                                                <div class="reports-column">
                                                    <h3>Maintenance Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName2;?>

                                                    </ul>
                                                </div>

                                                <div class="reports-column">
                                                    <h3>Owner Reports</h3>
                                                    <ul  class="listPopupdata">
                                                        <?php echo $variClassName3;?>

                                                    </ul>
                                                </div>

                                                <div class="reports-column">
                                                    <h3>Vendor Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName4;?>

                                                    </ul>
                                                </div>

                                                <div class="reports-column">
                                                    <h3>Contact Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName5;?>

                                                    </ul>
                                                </div>

                                            </div>
                                            <div class="col-sm-3">
                                                <div class="reports-column">
                                                    <h3>Property Reports</h3>
                                                    <ul   class="listPopupdata">
                                                    <?php echo $variClassName6;?>

                                                    </ul>
                                                </div>

                                                <div class="reports-column">
                                                    <h3>Transaction Reports</h3>
                                                    <ul>

                                                    </ul>
                                                </div>

                                                <div class="reports-column">
                                                    <h3>Inventory Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName8;?>

                                                    </ul>
                                                </div>

                                                <div class="reports-column">
                                                    <h3>Guest Card Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName9;?>

                                                    </ul>
                                                </div>

                                                <div class="reports-column">
                                                    <h3>Company Vehicle Reports</h3>

                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName10;?>

                                                    </ul>
                                                </div>

                                                <div class="reports-column">
                                                    <h3>Employee Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName11;?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- First Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-two">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>General Financial Reports</h3>
                                                    <ul>
                                                        <li><a href="javascript:;">Balance Sheet</a></li>
                                                        <li><a href="javascript:;">Balance Sheet Detail</a></li>
                                                        <li><a href="javascript:;">Balance Sheet Consolidated</a></li>
                                                        <li><a href="javascript:;">Balance Sheet Comparison(2 years)</a></li>
                                                        <li><a href="javascript:;">Income Statement</a></li>
                                                        <li><a href="javascript:;">Income Statement Details</a></li>
                                                        <li><a href="javascript:;">Income Statement - 12-Months Comparison</a></li>
                                                        <li><a href="javascript:;">Income Statement Comparison (2 years)</a></li>
                                                        <li><a href="javascript:;">Income Statement Consolidated</a></li>
                                                        <li><a href="javascript:;">Cash Flow</a></li>
                                                        <li><a href="javascript:;">Cash Flow-12 Months</a></li>
                                                        <li><a href="javascript:;">Cash Flow Comparison</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss Detail</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss - 12 Months</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss Comparison (2 years)</a></li>
                                                        <li><a href="javascript:;">Profit &amp; Loss Consolidated</a></li>
                                                        <li><a href="javascript:;">Equity Statement</a></li>
                                                        <li><a href="javascript:;">Account Totals</a></li>
                                                        <li><a href="javascript:;">Expense Distribution</a></li>
                                                        <li><a href="javascript:;">Expense Register</a></li>
                                                        <li><a href="javascript:;">General Ledger</a></li>
                                                        <li><a href="javascript:;">Security Deposit</a></li>
                                                        <li><a href="javascript:;">Open Invoices</a></li>
                                                        <li><a href="javascript:;">Collection</a></li>
                                                        <li><a href="javascript:;">A/R Aging Summary</a></li>
                                                        <li><a href="javascript:;">A/R Aging Detail</a></li>
                                                        <li><a href="javascript:;">A/P Aging Summary</a></li>
                                                        <li><a href="javascript:;">A/P Aging Detail</a></li>
                                                        <li><a href="javascript:;">Expenses by Vendor</a></li>
                                                        <li><a href="javascript:;">Property Statement</a></li>
                                                        <li><a href="javascript:;">Bank Account Activity</a></li>
                                                        <li><a href="javascript:;">Voided Checks</a></li>
                                                        <li><a href="javascript:;">Trial Balance</a></li>
                                                        <li><a href="javascript:;">Trial Balance Consolidated</a></li>
                                                        <li><a href="javascript:;">Adjusted Trial Balance</a></li>
                                                        <li><a href="javascript:;">Adjusting Journal Entries</a></li>
                                                        <li><a href="javascript:;">Budget vs. Actual</a></li>
                                                        <li><a href="javascript:;">Budget-to-Actual Comparison</a></li>

                                                        <li><a href="javascript:;">Check Register</a></li>
                                                        <li><a href="javascript:;">Deposit Register</a></li>

                                                        <li><a href="javascript:;">Chart of Accounts</a></li>
                                                        <li><a href="javascript:;">Journal Entry</a></li>
                                                        <li><a href="javascript:;">Transaction Detail by Account</a></li>
                                                        <li><a href="javascript:;">Recent Automatic Transactions</a></li>
                                                        <li><a href="javascript:;">Recurring Journal Entries</a></li>
                                                        <li><a href="javascript:;">Charge Detail</a></li>
                                                        <li><a href="javascript:;">Income Register</a></li>
                                                        <li><a href="javascript:;">Audit Log</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Second Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-three">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Tenant Reports</h3>
                                                    <ul  class="listPopupdata">
                                                        <?php echo $variClassName1;?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Third Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-four">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Property Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName6;?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fourth Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-five">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Owner Reports</h3>
                                                    <ul  class="listPopupdata">
                                                        <?php echo $variClassName3;?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fifth Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-six">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Vendor Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName4;?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- Sixth Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-seven">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
<!--                                                <div class="reports-column">-->
<!--                                                    <h3>Maintenance Reports</h3>-->
<!--                                                    <ul>-->
<!--                                                        <li><a href="javascript:;">Open Work Order</a></li>-->
<!--                                                        <li><a href="javascript:;">Completed Work Order</a></li>-->
<!--                                                        <li><a href="javascript:;">Consolidated Purchase Orders</a></li>-->
<!--                                                    </ul>-->
<!--                                                </div>-->


                                                <div class="reports-column">
                                                    <h3>Maintenance Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName2;?>
                                                        <!-- <li><a href="javascript:;">Open Work Order</a></li>
                                                         <li><a href="javascript:;">Completed Work Order</a></li>
                                                         <li><a href="javascript:;">Consolidated Purchase Orders</a></li>-->
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Seventh Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-eight">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Transaction Reports</h3>
                                                    <ul>
                                                        <li><a href="javascript:;">Check Detail</a></li>
                                                        <li><a href="javascript:;">Income Register</a></li>
                                                        <li><a href="javascript:;">Expense Register</a></li>
                                                        <li><a href="javascript:;">Recurring Charges</a></li>
                                                        <li><a href="javascript:;">General Ledger</a></li>
                                                        <li><a href="javascript:;">Charge Detail</a></li>
                                                        <li><a href="javascript:;">e-Payment Activities</a></li>
                                                        <li><a href="javascript:;">e-Payment Schedule</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Seventh Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-nine">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Inventory Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName8;?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Seventh Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-ten">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Contact Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName5;?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Seventh Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-eleven">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Guest Card Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName9;?>

                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="modal fade" id="guest_report" role="dialog">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">×</button>
                                                <h4 class="modal-title">Print Envelope For Contact Filters</h4>
                                            </div>
                                            <div class="modal-body pull-left">
                                                <div class="">
                                                    <div class="form-outer">
                                                        <form method="post" id="comp_employeelogg">
                                                            <div class="">

                                                                <div class="row">
                                                                    <div class="col-sm-12" style="width: 100%">
                                                                        <label>
                                                                            Start Date <em class="red-star">*</em></label>
                                                                        <input class="form-control capital calander" type="text" maxlength="20" name="date"  />
                                                                    </div>
                                                                    <div class="col-sm-12 clear" style="width: 100%">
                                                                        <label>
                                                                            End Date </label>
                                                                        <input class="form-control capital calander" type="text" maxlength="20" name="driver" />
                                                                    </div>
                                                                    <div class="col-sm-12 clear" style="width: 100%">
                                                                        <label>
                                                                            Status </label>
                                                                        <select class="form-control " type="text" maxlength="20" name="select_vehicle"  id="select_vehicle">
                                                                            <option value="1" selected>All</option>
                                                                            <option value="2">Active</option>
                                                                            <option value="3" >Inactive</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-12 clear" style="width: 100%">
                                                                        <label>
                                                                            Contact </label>
                                                                        <select class="form-control " type="text" maxlength="20" name="select_vehicle"  id="select_vehicle"></select>
                                                                    </div>
                                                                    <div class="col-sm-12 clear" style="width: 100%">
                                                                        <label>
                                                                            Envelope Size</label>
                                                                        <select class="form-control " type="text" maxlength="20" name="select_vehicle"  id="select_vehicle">
                                                                            <option value="1" selected>41/8 * 91⁄2</option>
                                                                            <option value="2">5/4</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </form>
                                                        <div class="btn-outer">
                                                            <input type="button" value="Apply Filters" class="blue-btn" id="save_vehiclelog">
                                                            <input type="button" class="grey-btn cancel_Popup" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>




                            <!-- Seventh Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-twelve">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Company Vehicle Reports</h3>

                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName10;?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Seventh Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="reports-thirteen">
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" class="reports_hdr" data-parent="#accordion">Reports</a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="reports-outer">
                                            <div class="col-sm-12">
                                                <div class="reports-column">
                                                    <h3>Employee Reports</h3>
                                                    <ul   class="listPopupdata">
                                                        <?php echo $variClassName11;?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Seventh Tab Ends-->
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<!--Model for Tenant Transfer start-->
<div class="container">
    <div class="modal fade" id="vendorList" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <form id="popupReportData">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <input type="hidden" name="DBquery" value="" id="dquery"/>
                    <div class="modal-body">
                        <div class="form-outer col-sm-12">
                            <div class="row">
                                <div class="col-xs-12 col-md-12">
                                    <input type="hidden" name="action" value="reportHtml">
                                      <input type="hidden" name="filter" value="filtervalue" id="filterss">
                                    <div id="modalDataClass" class="row">
                                    </div>
                                    <div class="btn-outer text-right">
                                        <input type="button" class="blue-btn submit_btn" value="Apply Filters"/>
                                        <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel"/>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Model for Tenant Transfer End-->
<div class="container">
    <div class="modal fade" id="ancVendor1099SummaryModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">In Touch History

                        <a id="Notesedit" style="color:blue;float: right;margin-right: 10px;cursor:pointer;">Edit</a>
                    </h4>
                </div>
                <div class="modal-body">
                    <form name="add_chart_account_form" id="add_chart_account_form_id">
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <h4 style="text-align:center;margin-top: 25px;">Notes</h4>
                                        </div>
                                        <div class="col-sm-8">

                                            <div class="historyhtml"></div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
</script>

<script>
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });   var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".calander").val(date);

    $('.datefield').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });   var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".datefield").val(date);




    $(document).on('focus',".acquireDateclass,.expirationDateclass", function(){
        $(this).datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
    });



    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.datetimepicker.full.min.js"></script>
<!-- <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/balance/modal.js"></script> -->

<!-- Code Shivam -->
<!-- General Financial -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/commonReports.js"></script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/balanceSheetComparison.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/incomeStatementDetail.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/incomeStatementComparison.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/profitLoss.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/profitLossDetail.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/profitLossComparison12months.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/profitLossComparison2years.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/profitLossConsolidated.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/arAgingSummary.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/arAgingDetail.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/apAgingSummary.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/apAgingDetail.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/auditLog.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/incomeRegister.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/chargeDetail.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/expensesByVendors.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/propertyStatement.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/bankAccountActivity.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/voidCheck.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/trialBalance.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/trialBalanceConsolidated.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/adjustTrialBalance.js"></script>

<!-- General Financial -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/balanceSheet.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/balanceSheetConsolidated.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/incomeStatement.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/incomeStatementConsolidated.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/incomeStatement12MonthsCamparison.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/cashFlow.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/cashFlow12MonthsComparison.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/cashFlowComparsion.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/equlityStatement.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/expenseDistribution.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/generalLedger.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/security.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/expenseRegister.js "></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/openInvoice.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/recurringGeneral.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/recentAutomaticTransactions.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/transactionDetailByAccount.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/journalEntry.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/depositRegister.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/annualBudgetComparison.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/budgetVsActual.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/adjustingJournalEntries.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/generalFinancialReports/collection.js"></script>
<!-- General Financial Ends -->

<!-- Property Reports Starts-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/propertyReports/vacantUnit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/propertyReports/propertyStatement.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/propertyReports/unpaidBillsByProperty.js"></script>
<!-- Property Reports Ends -->

<!-- <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/balance/rental.js"></script>
 --><!-- Tenant Reports Scripts -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/tenantLedger.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/tenantStatement.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/tenantPayment.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/tenantConsolidatedPayment.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/tenantOnlinePayment.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/tenantUnpaidCharges.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/delinquentTenant.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/delinquentTenantEmail.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/consolidatedDelinquentTenant.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/shortTermRenterList.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/tenantReports/tenantEeoReport.js"></script>


<!-- Maintenance Reports -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/maintenanceReports/openWorkOrder.js"></script>

<!-- Owner Reports -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/ownerReports/ownerStatement.js"></script>

<!-- Vendor Reports -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/vendorReports/expenseByVendor.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/vendorReports/vendorBills.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/vendorReports/transactionByVendor.js"></script>

<!-- MultiSelect -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.datetimepicker.full.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/reports/reports.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js" type="text/javascript"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
