<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */






?>

  <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>



<input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php"); ?>

    <?php
    if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id'])) {
        if((!isset($_SESSION['tenant_id'])))
        {
            $localUrl = localUrl;
            header("location:$localUrl/TenantPortal/login");
        }
    }

    ?>

                    
<form id="addTicket">
    <section class="main-content">
          <div class="container-fluid">
            <div class="row">                   
              <div class="col-sm-12">
                      <div class="content-section">
                        <div class="form-outer">
                          <div class="form-hdr">
                            <h3>Add Ticket <a class="back" href="javascript:;" onclick="goBack();"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                          </div>
                          <div class="form-data">
                            <div class="row">
                              <p style="color: red; font-weight: bold; text-align: right; font-size: 13px;">In the event of an Emergency, please call the Property Manager at: 123-456-7890. If this is a Life-Threatening Emergency, please call 911 or your local Emergency number.</p>
                            </div>
                            <div class="row">
                              <div class="col-sm-3">
                                <div class="check-outer">
                                  <input type="radio" name="ticket_type" class="ticket_type" value="One Time" checked/><label>One Time</label>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="check-outer">
                                  <input type="radio" name="ticket_type" class="ticket_type" value="Reccuring"/><label>Reccuring</label>
                                </div>
                              </div>

                             <div class="duration col-sm-3" style="display:none;">
                              <select name="duration_type" class="form-control"><option value="Weekly">Weekly</option><option value="Bi-Weekly">Bi-Weekly</option><option value="Monthly">Monthly</option><option value="Quarterly">Quarterly</option><option value="Semi-Annually">Semi-Annually</option><option value="Annually">Annually</option></select>
                            </div>

                            </div>
                            <div class="row">
                              <div class="col-sm-3">
                                <label>Ticket Number</label>
                                <input class="form-control" type="text" name="ticket_number" value='<?php echo rand(100000,9999999); ?>'/>
                              </div>
                             
                              <div class="col-sm-3">
                                <label>Created On</label>
                                <input class="form-control calander" type="text" name="created_on" readonly/>
                              </div>

                              <div class="col-sm-3">
                                <label>Category <em class="red-star">*</em></label>
                               <select class="form-control" name="category">
                                <option value="" selected="Selected">Select</option>
                                <option value="Delete">Delete</option>
                                <option value="General Inquiry">General Inquiry</option>
                                <option value="Incident">Incident</option>
                                <option value="Inspection">Inspection</option>
                                <option value="Meter Reading">Meter Reading</option>
                                <option value="Monthly Maintenance">Monthly Maintenance</option>
                                <option value="Other">Other</option>
                               <!-- <option value="Preventative Maintenance">Preventative Maintenance</option>
                                <option value="Remove Replace">Remove Replace</option>
                                <option value="Service Request">Service Request</option>
                                <option value="Test">Test</option>
                                <option value="Urgent">Urgent</option>
                                <option value="Violation">Violation</option>
                                <option value="Yes">Yes</option>-->
                               </select>
                              </div>

                              <div class="col-sm-3">
                                <label>Start Date</label>
                                <input class="form-control calander" type="text" name="start_date" readonly/>
                              </div>

                              <div class="col-sm-3">
                                <label>End Date</label>
                                <input class="form-control calander" type="text" name="end_date" readonly/>
                              </div>

                              <div class="col-sm-3">
                                <label>Priority</label>
                                <select class="form-control" name="priority">
                                  <option value="Low">Low</option>
                                    <option value="Normal">Normal</option>
                                      <option value="High">High</option>

                                </select>
                              </div>

                              <div class="col-sm-3">
                                <label>Estimated Cost(<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                <input class="form-control" type="text" name="estimate_cost"/>
                              </div>

                              <div class="col-sm-3 clear">
                                <label>Required Materials</label>
                                <textarea class="form-control" name="required_matirial"></textarea>
                              </div>

                              <div class="col-sm-3">
                                <label>Vendor Instruction</label>
                                <textarea class="form-control" name="vendor_instruction"></textarea>
                              </div>

                              <div class="col-sm-3">
                                <label>Description</label>
                                <textarea class="form-control description" name="description"></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Form Outer Ends -->

                        <div class="form-outer">
                          <div class="form-hdr">
                            <h3>Property Information <a class="back" href="javascript:;" onclick="goBack();"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                          </div>
                          <div class="form-data propInfo">
                            <div class="row">
                              <div class="col-sm-3">
                                <label>Property <em class="red-star">*</em></label>
                                <select class="form-control" name="property_id" id="property">
                                  <option value="">Select</option>

                                </select>
                              </div>

                              <div class="col-sm-3">
                                <label>Unit <em class="red-star">*</em></label>
                                <select class="form-control" name="unit_id" id="unit"></select>
                              </div>

                              <div class="col-sm-3">
                                <label>Tenant Name</label>
                                <input type="text" class="form-control name" name="name" readonly>
                              </div>

                              <div class="col-sm-3">
                                <label>Phone</label>
                                <input type="text" class="form-control phone" name="phone" readonly>
                              </div>

                              <div class="col-sm-3">
                                <label>Assign To <em class="red-star">*</em></label>
                                <select class="form-control manager" name="assign_to"></select>
                              </div>

                            </div>
                          </div>
                        </div>
                        <!-- Form Outer Ends -->

                        <div class="form-outer">
                          <div class="form-hdr">
                            <h3>Upload Image(s)<a class="back" href="javascript:;" onclick="goBack();"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                          </div>
                          <div class="form-data">
                        
                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Photo/Image</label>
                                                             
                                                                <div class="upload-logo">
                                                                    <div class="img-outer ticket_image1"><img src="<?php echo COMPANY_SITE_URL ?>/images/hoa_image.png"></div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="ticket_image1[]" accept="image/*">
                                                                    <div class="cropItData" style="display: none;">
                                                                    <div class="cropit-preview"></div>
                                                                    <div class="cropit-rotate">
                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                    </a>
                                                                    </div>
                                                                        <div class="ticket_image1"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input" min="0"
                                                                               max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="ticket_image1">
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer ticket_image2"><img src="<?php echo COMPANY_SITE_URL ?>/images/hoa_image.png"></div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="ticket_image2[]" accept="image/*">
                                                                    <div class="cropItData" style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                    <div class="cropit-preview"></div>
                                                                    <div class="cropit-rotate">
                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                    </a>
                                                                    </div>
                                                                        </div>
                                                                        <div class="ticket_image2"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input" min="0"
                                                                               max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="ticket_image2">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer ticket_image3"><img src="<?php echo COMPANY_SITE_URL ?>/images/hoa_image.png"></div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="ticket_image3[]" accept="image/*">
                                                                    <div class="cropItData" style="display: none;">
                                                                    <div class="cropit-preview"></div>
                                                                    <div class="cropit-rotate">
                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                    </a>
                                                                    </div>
                                                                        <div class="ticket_image3"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input" min="0"
                                                                               max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="ticket_image3">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                          </div>
                        </div>
                        <!-- Form Outer Ends -->

                        <div class="form-outer">
                          <div class="form-hdr">
                            <h3>Notes</h3>
                          </div>
                          <div class="form-data">
                            <div class="row">
                              <div class="col-sm-4">
                                  <textarea class="notes" name="notes"></textarea>
                              
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Form Outer Ends -->

                        <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeventeen">
                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> File Library</a>
                                            </h4>
                                        </div>
                                        <div id="collapseSeventeen" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-4">
                                                            <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                            <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                           
                                                            <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">
                                                          
                                                        </div>
                                                        <div class="row" id="file_library_uploads">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid-outer">
                                                    <div class="apx-table">
                                                        <div class="table-responsive">
                                                            <table id="propertFileLibrary-table" class="table table-bordered">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        </div>
                                    </div>
                        <!-- Form Outer Ends -->
                        <div class="form-outer text-right">
                          <div class="btn-outer">
                            <button class="blue-btn">Save</button>
                              <button type="button" class="clear-btn email_clear">Clear</button>
                            <input type="button" class="grey-btn cancelTicket"  value="Cancel">
                          </div>
                        </div>
                        

                      </div>
                </div>
            </div>
        </section>
      </form>





    <script>
        $(document).on("click",".clear-btn.email_clear",function () {
/*window.location.reload();*/
            resetFormClear('#addTicket',['created_on','start_date','end_date','name','phone','ticket_number'],'form',false);
        });
    </script>


    <!-- Jquery Starts -->
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['Tenant_Portal']['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">';</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
   <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
      <script>var upload_url = "<?php echo SITE_URL; ?>";</script>

</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script>

  $(".to").tagsinput('items')
  addCalanderDate();


 </script>