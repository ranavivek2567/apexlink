<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/favicon.ico" alt="apex-icon" type="image/ico" sizes="16x16">

    <title>Apexlink</title>
    <!-- Bootstrap -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/ui.jqgrid.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.timepicker.min.css">

    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/passwordscheck.css" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/calculator.css"/>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.theme.default.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.multiselect.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.signature.css"/>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.fontselect.css"/>


    <!-- include libraries(jQuery, bootstrap) -->
    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/summernote.js" defer></script>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-ui.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.timepicker.min.js"></script>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.jqgrid.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/common.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/toastr.min.js"></script>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/bootbox.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.mask.min.js"></script>
    <script defer src="<?php echo SUPERADMIN_SITE_URL; ?>/js/calculator.js"></script>
    <script defer src="<?php echo COMPANY_SITE_URL; ?>/js/owl.carousel.js"></script>
    <script defer src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/jquery.signature.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>

    <script>
        $(function() {
            /*$(".eSignModal").modal('show');
            var sig = $('#defaultSignature').signature();*/
        });
    </script>
</head>
<body>
    <div class="container">
        <div class="modal fade" id="eSignModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Signature Type</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-outer" style="float: none;">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="radio" name="typing" value="t" checked>Typing
                                    <input type="radio" name="typing" value="s">Sketching
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-7 sketchingarea"  style="display: none;">
                                    <div id="defaultSignature" style="overflow-x: scroll;"></div>
                                    <textarea id="signatureJSON" style="display: none;"></textarea>
                                    <div id="redrawSignature" style="display: none;"></div>
                                    <a style="margin: 6px 15px auto auto; background: #7bbb15;" href="javascript:void(0);" class='blue-btn pull-right clearSignature'>Clear</a>
                                </div>
                                <div class="col-sm-12 typingarea">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <input data-font="" type="text" name="typingtext" placeholder="Enter your name" class="form-control typingtext">
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="enterplaceholder">Please enter your name here</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12 fontlisting">
                                        <?php
                                        $arrayFont = ['inherit','Arial','Times+New+Roman', 'Verdana','Action Man', 'Bauer', 'Bubble','Piedra', 'Questrial', 'Ribeye'];
                                        foreach ($arrayFont as $fonts){
                                            echo "<div style='min-height:0px;font-weight:bold;font-family: ".$fonts."' class='col-sm-12'><input type='radio' name='selectfonts' value='".$fonts."'>Guest User</div>";
                                        }
                                        ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-10 submitsign" style="min-height: 0px;">
                                    <a class="blue-btn pull-right signsubmit" data-user="" data-type="" data-parent="">Save</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="font-weight: bold; color: #fff; background: #666; text-align: center; word-spacing: 4px; padding: 10px; font-size: 10px;">
                        Please select at least one signature type above and click on submit when you are done signing.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" value="<?php echo $_GET['id'] ?>" class="leaseid">
    <div class="header" style="max-height: 70px; margin-bottom: 20px;width: 100%;background: #d6f0fe;float: left;padding: 1px 0;min-width: 1024px;max-width: 100%;">
        <div class="header-inner">
            <div class="logo">
                <a class="navbar-brand" href="">
                    <img src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/company/images/logo.png" alt="Company Logo" width="143" height="49">
                </a>
            </div>
            <div class="hdr-rt">
            </div>
        </div>
    </div>
    <div style="float: right;margin-right: 138px;margin-bottom: 10px;">
        <!--<input type="button" id="btnDownload" value="Download" style="margin-right:10px;" class="blue-btn">-->
    <!--    <input type="button" id="btnSave" value="Submit" class="blue-btn">-->
    </div>
    <div class="" style="float: left; width: 100%;" id="dv-Main-pdf">
        <div class="" id="dvPdfToHtml" style="min-height:500px;overflow-y:scroll;float:left;width:80%; padding:2%;border:1px solid #ddd;margin-left: 8%;">
            <meta charset="utf-8">
            <title></title>
            <div width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#d6f0fd" style="border-radius: 6px 6px 0px 0px; -moz-border-radius: 6px 6px 0px 0px; -webkit-border-radius: 6px 6px 0px 0px; -webkit-font-smoothing: antialiased; background-color: #d6f0fd; color: #2D3091; margin: auto;" align="center">
                <table style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; border-collapse: collapse; padding: 0; margin: 0;">
                    <tbody>
                        <tr style="background-color: #00b0f0; height: 20px;"><td></td></tr>
                        <tr style="border-collapse: collapse; background-color: #fff;">
                            <td class="w580" style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;border-collapse: collapse;" width="580">
                                <center><img class="COMPANYLOGO" src="http://<?php echo $_SERVER['HTTP_HOST'] ?>/company/images/logo.png" width="150" height="50"></center>
                            </td>
                        </tr>
                        <tr style="background-color: #00b0f0; height: 20px;"><td></td></tr>
                    </tbody>
                </table>
            </div>

            <div width="640" border="0" cellpadding="0" cellspacing="0" align="center">
                <div>
                    <div width="528" valign="top" style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; font-size: 13px; padding: 0; line-height: 19px; margin: 0; border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 0;">
                        <p style="margin: 5px 0; text-align: left;">&nbsp;</p>
                        <table>
                            <tbody>
                            <tr>
                                <td style="color: #000; font-weight: bold; line-height: 5px; font-size: 12px;">
                                    Residential Lease agreement<br>
                                    <center><span style="font-size: 7px;">______________________________________________</span></center>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <p style="padding: 0 10px; margin-bottom: 5px; text-align: left; font-weight: bold;">
                            <?php
                            $companyUrl = SITE_URL."company/";
                            $filePath = $companyUrl.'uploads/Electronic_Record_and_Signature_Disclosure_Customer_Disclosure.docx';
                            ?>
                            Please read carefully and accept the electronic record consent and <a class="e_sign_doc_url" href="javascript::void(0);" style="color: #079b21"> e-signature disclosure</a>
                            <div style="padding: 0 10px; margin-bottom: 5px; text-align: left; font-weight: bold;"><input type="checkbox" name="termcondition" class="termcondition"> I agree to use electronic signature and records.</div>
                            <span class="hide termconditionerror" style="float: left;padding: 0px 27px;color: red;">Please check the above checkbox</span>
                        </p>
                        <p>&nbsp;</p>
                        <p style="padding: 0 10px; margin-bottom: 5px; text-align: left;">
                            The following residential agreement is between <?php echo $_SESSION[SESSION_DOMAIN]['company_name'] ?>. new and <span class="TENANTNAME"></span>,
                            as of <span class="LEASESTARTDATE"></span>. Each numbered section defines the term, conditions, and
                            responsibility of each party.
                        </p>
                        <div style="width: 85%;text-align:left;">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; line-height: 5px; font-weight: bold; padding-right: 15px;
                                    font-size: 12px; text-align: left; width: 150px;" valign="baseline">
                                        LANDLORD
                                    </td>
                                    <td style="text-align: left;  border-bottom: 1px solid blue;">
                                        In this agreement the Landlord(s) and/or related agent(s) is/are referred to as
                                        “Landlord”. <?php echo $_SESSION[SESSION_DOMAIN]['company_name'] ?>. new is the Landlord.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; line-height: 5px; font-weight: bold; padding-right: 15px;
                                    font-size: 12px; text-align: left; width: 150px;" valign="baseline">
                                        TENANT
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;" >
                                        Every instance of the term “Tenant” in this Lease will refer to <span class="TENANTNAME"></span>.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; font-weight: bold; padding-right: 15px; line-height: 16px; font-size: 12px;
                                    text-align: left; width: 150px;">
                                        RENTAL PROPERTY
                                    </td>
                                    <td style="text-align: left;  border-bottom: 1px solid blue;">
                                        The property located at <span class="ALLADDRESS"></span>,
                                        which is owned/managed
                                        by the Landlord will be rented to the Tenant. In this Lease the property will be
                                        referred to as “lease Premises”.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; font-weight: bold;  padding-right: 15px; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        TERM OF LEASE AGREEMENT
                                    </td>
                                    <td style="text-align: left;  border-bottom: 1px solid blue;">
                                        This Lease Agreement will start on <span class="LEASESTARTDATE"></span> and ends on <span class="LEASEENDDATE"></span>.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        USE &amp; OCCUPANCY OF PROPERTY
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        The only authorized person(s) that may live in the Leased Premises is/are: <span class="TENANTNAME"></span>.
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        AMOUNT OF RENT
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        The exact amount of Rent due monthly for the right to live in the Leased Premises
                                        is <span class="RENTAMOUNT"></span>.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091;  width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        DUE DATE FOR RENT
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        Rent is to be paid before or on the <span class="RENTBEFOREDAY"></span>. A <span class="GRADEPERIOD"></span> day grace period
                                        will ensue; nevertheless, rent cannot be paid after this grace period without
                                        incurring
                                        a late fee.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091;  width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        LATE FEE
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        In the event that rent or associated fees are not paid in full by the rental due
                                        date or before the end of the <span class="GRADEPERIOD"></span> day grace period, a late fee of __________________
                                        will be incurred.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        RETURNED PAYMENTS
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        Any returned payments from financial institutions that the Tenant utilizes will
                                        incur an addition fee of 0.00 that will be added to the rental amount.
                                    </td>
                                </tr>
                                <tr>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        SECURITY DEPOSIT
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        A. Security Deposit of <span class="SECURITYDEPOSITE"></span> is to be paid by the Tenant before move-in.<br>
                                        B. This Security Deposit is reserved for the purpose of paying any cost toward
                                        damages,
                                        cleaning, excessive wear to the property, and in the event of unreturned keys or
                                        abandonment of the Leased Premises before or upon the end of the Lease.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left; margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        UTILITIES &amp; SERVICES
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        A. The responsibility of the registering and paying for the following utility
                                        services is upon the Tenant: <br>
                                        These services should be maintained at all time during the leasing period.<br>
                                        B. The Landlord has included the Water utility as a part of rent; however, if the
                                        property water usage exceeds Enter amount here the Landlord reserves the right to
                                        request that any overages in usage be compensated as a fee in addition to the normal
                                        rental rate. These overages must be paid within 20 days.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        MAINTENANCE &amp; REPAIRS
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        General maintenance and repairs are the responsibility of the Landlord, except in
                                        cases where the Tenant has been negligent or has accidentally caused damages to
                                        the property. <br>
                                    A. It will remain the Tenant’s responsibility to promptly notify the Landlord of
                                    any necessary repairs to the property.<br>
                                    B. Any damages that have resulted from the Tenant or the Tenant’s guests will be
                                    the Tenants full responsibility to pay for the costs of repair that is required.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        CONDITION OF PROPERTY
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        A. The Tenant has inspected the Leased Premises acknowledges and accepts that the
                                        Leased Premises is in an acceptable living condition and that all parts of the
                                        Leased Premises are in good working order. <br>
                                        B. It is agreed by the Landlord and Tenant that no promises are made concerning
                                        the condition of the Leased Premises. <br>
                                        C. The Leased Premises will be returned to the Landlord by the Tenant in the same
                                        or better condition that it was in at the commencement of the Lease.

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        ENDING OR RENEWING THE LEASE AGREEMENT
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        A. When this Lease ends, if the Tenant or Landlord do not furnish a written
                                        agreement to end the Lease it will continue on a month to month basis indefinitely. In order
                                        to terminate or renew the Lease agreement either party, Landlord or Tenant, must
                                        furnish a written notice at least <span class="NOTICEPERIODDAYS"></span> days before the end of this Lease agreement.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 16px; font-size: 12px;
                                    text-align: left;">
                                        GOVERNING LAW
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        The terms, conditions, and any addenda to this Lease are to be governed and held
                                        in accord to the statutes and Laws of Enter Governing entity here..
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 30px; padding: 0 10px">
                            <p style="text-align: left;">&nbsp;</p>
                            <p style="text-align: left;">&nbsp;</p>
                            <table>
                                <tbody>
                                <tr>
                                    <td style="color: #2D3091; width: 150px; padding-right: 15px; font-weight: bold; line-height: 5px; font-size: 12px;
                                    text-align: left;">
                                        ENTIRE AGREEMENT
                                    </td>
                                    <td style="text-align: left; border-bottom: 1px solid blue;">
                                        NOTICE: This is a LEGALLY binding document.<br>
                                        A. You are relinquishing specific important rights. <br>
                                        B. You may reserve the right to have an attorney review the Lease Agreement before
                                        signing it. <br>
                                        By signing this Lease Agreement, the Tenant certifies in good faith to have read,
                                        understood, and agrees to abide by all of the terms and conditions stated within
                                        this Lease.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="width: 85%; margin-top: 20px; padding: 0 10px">
                            <div style="width:100%; float:left;">
                                <b style="float:left;" class="owner-manger-block">MANAGER [by Agent under Property Management Agreement]:</b><br>
                                <div class="ownerSection"></div>
                            </div>
                            <div style="width:100%; float:left;">
                                <b style="float:left;">TENANT(S):</b><br>
                                <div class="tenantSection"></div>
                                <div class="addTenantSection"></div>
                            </div>
                            <div style="width:100%; float:left;">
                                <b style="float:left;">GUARANTOR:</b><br>
                                <div class="guarantorSection"></div>
                            </div>
                        </div>
                        <div style="width: 85%;text-align:left;  margin-top: 20px; padding: 0; height: 500px;">
                        </div>
                    </div>
                </div>
            </div>
            <div width="640" border="0" cellpadding="0" cellspacing="0" align="right">
                <div width="528" valign="top" style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif; font-size: 13px; padding: 0; line-height: 19px; margin: 0; border-left: 1px solid #C1DBE8; border-right: 1px solid #C1DBE8; padding: 10px 20px; display: none;">
                    <input type="button" id="btnSave" value="Submit" class="blue-btn">
                    <input type="button" id="btnDownload" value="Download" style="margin-right:10px;" class="blue-btn">
                </div>
            </div>

            <div width="640" border="0" cellpadding="0" cellspacing="0" bgcolor="#585858" style="border-radius: 0px 0px 6px 6px; -moz-border-radius: 0px 0px 6px 6px; -webkit-border-radius: 0px 0px 6px 6px;
            -webkit-font-smoothing: antialiased; background-color: #585858; color: #fff;clear: both;" align="center">
                <table style="font-family: 'Helvetica Neue', Arial, Helvetica, Geneva, sans-serif;
                border-collapse: collapse; padding: 0; margin: 0;">
                    <tbody>
                    <tr>
                        <td style="padding: 10px; font-size: 12px; color: #ffffff; font-weight: bold;" align="center">
                            <span class="pmname"><?php echo $_SESSION[SESSION_DOMAIN]['company_name'] ?></span> Property Manager● <a href="javascript:;" style="color: #fff; text-decoration: none;"><span class="pmphone"><?php echo $_SESSION[SESSION_DOMAIN]['phone_number'] ?></span>
                            </a></td>
                    </tr>
                    <tr><td style="line-height: 5px" id="Td2" valign="middle" align="center">&nbsp;</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="container">
            <div class="modal fade" id="esignature_disclosure" role="dialog">
                <div class="modal-dialog modal-lg text-center">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Electronic Record and Signature Disclosure Customer Disclosure</h4>
                        </div>
                        <div class="modal-body" style="text-align: left">
                            <div class="form-outer" style="float: none">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Electronic Record and Signature Disclosure Customer Disclosure: From time to time, <?php echo $_SESSION[SESSION_DOMAIN]['company_name'] ?> (we, us or Company) may be required by law to provide to you certain written notices or disclosures. Described below are the terms and conditions for providing to you such notices and disclosures electronically through our electronic signing system. Read the information below carefully and thoroughly, and if you can access this information electronically to your satisfaction and agree to these terms and conditions, confirm your agreement by clicking the ‘I agree’ button of this document. Getting paper copies: At any time, you may request from us a paper copy of any record provided or made available electronically to you by us. You will have the ability to download and print documents we send to you through the Document signing electronic system during and immediately after signing session. You may request delivery of such paper copies from us by following the procedure described below. Withdrawing your consent: If you decide to receive notices and disclosures from us electronically, you may at any time change your mind and tell us that thereafter you want to receive required notices and disclosures only in paper format. How you must inform us of your decision to receive future notices and disclosure in paper format and withdraw your consent to receive notices and disclosures electronically is described below. Consequences of changing your mind: If you elect to receive required notices and disclosures only in paper format, it will slow the speed at which we can complete certain steps in transactions with you and delivering services to you because we will need first to send the required notices or disclosures to you in paper format, and then wait until we receive back from you your acknowledgment of your receipt of such paper notices or disclosures.  All notices and disclosures will be sent to you electronically: Unless you tell us otherwise in accordance with the procedures described herein, we will provide electronically to you through our e-document system all required notices, disclosures, authorizations, acknowledgements, and other documents that are required to be provided or made available to you during the course of our relationship with you. To reduce the chance of you inadvertently not receiving any notice or disclosure, we prefer to provide all of the required notices and disclosures to you by the same method and to the same address that you have given us. Thus, you can receive all the disclosures and notices electronically or in paper format through the paper mail delivery system. If you do not agree with this process, let us know as described below. Also see the paragraph immediately above that describes the consequences of your electing not to receive delivery of the notices and disclosures electronically from us. To let us know of a change in your e-mail address where we should send notices and disclosures electronically to you, please contact the office and be prepared to provide your previous e-mail address and your new e-mail address.</label>
                                        <label>&nbsp;</label>
                                        <label><span style="color: #000; text-decoration: underline">Acknowledging your access and consent to receive materials electronically:</span> To confirm to us that you can access this information electronically, which will be similar to other electronic notices and disclosures that we will provide to you, verify that you were able to read this electronic disclosure and that you also were able to print on paper or electronically save this page for your future reference and access or that you were able to e-mail this disclosure and consent to an address where you will be able to print on paper or save it for your future reference and access. Further, if you consent to receiving notices and disclosures exclusively in electronic format on the terms and conditions described above, let us know by clicking the ‘I agree’ button. By checking the ‘I agree’ box, I confirm that: • I can access and read this Electronic CONSENT TO ELECTRONIC RECEIPT OF ELECTRONIC CUSTOMER DISCLOSURES document; and • I can print on paper the disclosure or save or send the disclosure to a place where I can print it, for future reference and access; and • Until or unless I notify the company as described above, I consent to receive through exclusively electronic means all notices, disclosures, authorizations, acknowledgements, and other documents that are required to be provided or made available to me by the company during the course of my relationship with you.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/jquery.fontselect.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/jquery.ui.touch-punch.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/generateLease.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>