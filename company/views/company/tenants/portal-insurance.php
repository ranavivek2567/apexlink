<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

?>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php");
?>

<div id="wrapper">


    <style>
        .bootstrap-tagsinput { width: 100%; }
        #ccrecepents .modal-content, #ccrecepents .modal-body  { width: 100%; }
        #sendMailModal .modal-body {
            max-height: 400px;
            overflow: auto;
        }
        #sendMailModal .modal-body .note-editor { width: 100% !important; }
    </style>

    <div class="popup-bg"></div>
    <div class="container">
        <form id="sendEmail">
            <div class="modal fade" id="sendMailModal" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <h4 class="modal-title">Add New Manager</h4>
                        </div>
                        <div class="modal-body">
                            <!-- <div class="form-outer" style="float: none;">
                                <label>To <em class="red-star">*</em></label>
                                <input class="form-control to" name="to" type="text" data-role="tagsinput" /><span class=""><a href="#" class="addToRecepent">Add Recepent</a></span>

                                   <label>CC <em class="red-star">*</em></label>
                                <input class="form-control cc" name="cc" type="text" />

                                   <label>BCC </label>
                                <input class="form-control bcc" name="bcc" type="text" />

                                   <label>Subject <em class="red-star">*</em></label>
                                <input class="form-control subject" name="subject" type="text" />

                                 <label>Body <em class="red-star">*</em></label>
                                <textarea class="form-control body" name="body"></textarea>

                                <div class="attachmentFile"></div>


                                <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                                  <button class="blue-btn" value="Send">Send</button>
                                    <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                </div>
                            </div> -->

                            <div class="row">
                                <div class="col-sm-1">
                                    <button class="blue-btn compose-email-btn">Send</button>
                                </div>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-2"><label>To <em class="red-star">*</em></label></div>
                                        <div class="col-sm-10"><span><input class="form-control to" name="to" type="text"/></span></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <a class="add-recipient-link addToRecepent" href="#"> Add Recipients</a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-1">
                                </div>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-2"><label>Cc</div>
                                        <div class="col-sm-10"><span><input class="form-control cc" name="cc" type="text"/></span></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <a class="add-recipient-link addCcRecepent" href="#"> Add Recipients</a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-1">
                                </div>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-2"><label>Bcc </label></div>
                                        <div class="col-sm-10"><span><input class="form-control bcc" name="bcc" type="text"/></span></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <a class="add-recipient-link addBccRecepent" href="#"> Add Recipients</a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-1">
                                </div>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-2"><label>Subject <em class="red-star">*</em></label></div>
                                        <div class="col-sm-10"><span><input class="form-control subject" name="subject" type="text"/></span></div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-1">
                                </div>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-2"><label>Body <em class="red-star">*</em></label></div>
                                        <div class="col-sm-10">
                                            <span><textarea class="form-control summernote" name="body"></textarea></span>
                                            <div class="btn-outer">
                                                <button class="blue-btn">Send</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                </div>
                            </div>


                            <div class="attachmentFile"></div>


                        </div>
                    </div>
                </div>

            </div>
        </form>





        <div class="container">
            <div class="modal fade" id="torecepents" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <h4 class="modal-title">Add Recipients </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-outer" style="float: none;">
                                <label>Select <em class="red-star">*</em></label>
                                <select class="form-control selectUsers">
                                    <option value=""></option>
                                    <option value="tenant">Tenant</option>
                                    <option value="owner">Owner</option>
                                    <option value="vendor">Vendor</option>
                                    <option value="other_contacts">Other Contacts</option>
                                    <option value="guestCard">Guest Card</option>
                                    <option value="rental_application">Rental Application</option>
                                    <option value="employee">Employee</option>
                                </select>

                                <div class="userDetails"></div>


                                <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                                    <button class="blue-btn" value="Send">Send</button>
                                    <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="container">
            <div class="modal fade" id="ccrecepents" role="dialog">
                <div class="modal-dialog modal-md">
                    <div class="modal-content pull-left" style="width: 100%;">
                        <div class="modal-header">
                            <h4 class="modal-title">Add Recipients </h4>
                        </div>
                        <div class="modal-body pull-left">
                            <div class="form-outer" style="float: none;">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Select <em class="red-star">*</em></label>
                                        <select class="form-control selectCcUsers">
                                            <option value=""></option>
                                            <option value="tenant">Tenant</option>
                                            <option value="owner">Owner</option>
                                            <option value="vendor">Vendor</option>
                                            <option value="other_contacts">Other Contacts</option>
                                            <option value="guestCard">Guest Card</option>
                                            <option value="rental_application">Rental Application</option>
                                            <option value="employee">Employee</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="userCcDetails grid-outer"></div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <button class="blue-btn" value="Send">Send</button>
                                        <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="modal fade" id="bccrecepents" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content" style="width: 100%;">
                        <div class="modal-header">
                            <h4 class="modal-title">Add Recipients </h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-outer" style="float: none;">
                                <label>Select <em class="red-star">*</em></label>
                                <select class="form-control selectBccUsers">
                                    <option value=""></option>
                                    <option value="tenant">Tenant</option>
                                    <option value="owner">Owner</option>
                                    <option value="vendor">Vendor</option>
                                    <option value="other_contacts">Other Contacts</option>
                                    <option value="guestCard">Guest Card</option>
                                    <option value="rental_application">Rental Application</option>
                                    <option value="employee">Employee</option>
                                </select>

                                <div class="userBccDetails"></div>


                                <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                                    <button class="blue-btn" value="Send">Send</button>
                                    <a class="grey-btn" data-dismiss="modal">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>





        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>


    </div>
    <input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">
    <?php
    if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id'])) {
        if((!isset($_SESSION['tenant_id'])))
        {
            $localUrl = localUrl;
            header("Location: $localUrl/TenantPortal/login");
        }

    }

    ?>
    <section class="main-content">
        <div role="tabpanel" class="tab-pane renter-container" id="tenant-detail-ten">
            <div class="form-outer">
                <div class="form-hdr">
                    <h3>Renter Insurance</h3>
                </div>
                <div class="form-data">
                    <div class="accordion-grid">
                        <div class="accordion-outer renterform">
                            <div class="bs-example">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div id="collapseOne"
                                             class="panel-collapse collapse  in">
                                            <div class="pad-none">
                                                <div class="grid-outer">
                                                    <div class="form-data">
                                                        <form method="POST"
                                                              name="renterformfield"
                                                              id="renterformfield">
                                                            <input type="hidden"
                                                                   name="renter_tenant_id"
                                                                   value="<?php echo $_GET['tenant_id']; ?>">
                                                            <div class="row renterformdata">
                                                                <div class="col-sm-11">
                                                                    <div class="col-sm-12">
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Policy
                                                                                Info:</label>
                                                                            <input class="form-control"
                                                                                   name="policyinfo[]"
                                                                                   type="text">
                                                                            <input name="policyid[]"
                                                                                   type="hidden">
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Insurance
                                                                                Provider
                                                                                :</label>
                                                                            <input class="form-control"
                                                                                   type="text"
                                                                                   name="provider[]">
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Status</label>
                                                                            <select name="status[]"
                                                                                    class="form-control">
                                                                                <option value="1">
                                                                                    Active
                                                                                </option>
                                                                                <option value="0">
                                                                                    Inactive
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Policy
                                                                                Expiration
                                                                                Date
                                                                                :</label>
                                                                            <input class="form-control calander"
                                                                                   type="text"
                                                                                   name="expiration[]" readonly>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Next
                                                                                Renewal
                                                                                Date
                                                                                :</label>
                                                                            <input class="form-control calander"
                                                                                   type="text"
                                                                                   name="renewal[]" readonly>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Effective
                                                                                Date
                                                                                :</label>
                                                                            <input class="form-control"
                                                                                   type="text"
                                                                                   name="effective[]">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <a class="clonerenteradd pop-add-icon"
                                                                       href="javascript:;">
                                                                        <i class="fa fa-plus-circle"
                                                                           aria-hidden="true"></i>
                                                                    </a>
                                                                    <a class="clonerenterremove pop-add-icon"
                                                                       href="javascript:;"
                                                                       style="display: none;">
                                                                        <i class="fa fa-minus-circle"
                                                                           aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-12">
                                                                    <div class="btn-outer text-right">
                                                                        <a class="blue-btn saverenter"
                                                                           href="javascript:void(0)">Save</a>
                                                                        <button type="button" class="clear-btn email_clear">Clear</button>
                                                                        <a class="grey-btn cancelrenter"
                                                                           href="javascript:void(0)">Cancel</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-outer renterlisting">
                            <div class="bs-example">
                                <div class="panel-group" id="accordion">


                                    <div id="collapseOne"
                                         class="panel-collapse collapse  in">
                                        <div class="panel-body pad-none">
                                            <div class="grid-outer">
                                                <div class="apx-table">
                                                    <table class="" id="renterinsurancetable"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot renter-edit-icon">
                                        <a href="javascript:;">
                                            <i class="fa fa-pencil-square-o"
                                               aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

    <div id="imageModel" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>








<script>
    $(document).on("click",".clear-btn.email_clear",function () {
        /* window.location.reload();*/
        resetFormClear('#renterformfield',['expiration[]','renewal[]                                                                                                                                                                                         '],'form',false);
    });
</script>
<!-- Jquery Starts -->
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>var staticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">';</script>
<script>var petStaticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">';</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
<script src="<?php echo COMPANY_SITE_URL; ?>/js/intlTelInput.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/portalrenterinsurance.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            addclass: {
                debug: false,
                classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
            },
            width: '80%',
            height: '300px',
            //margin-left: '15px',
            toolbar: [
                // [groupName, [list of button]]
                ['img', ['picture']],
                ['style', ['style', 'addclass', 'clear']],
                ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
                ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
                ['extra', ['video', 'table', 'height']],
                ['misc', ['undo', 'redo', 'codeview', 'help']]
            ]
        });
    });
</script>
<script>var upload_url = "<?php echo SITE_URL; ?>"; </script>

</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script>


    addCalanderDate();


</script>