<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/starability-all.css">
<div id="wrapper">
    <!-- Top navigation start -->
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php"); ?>
    <!-- Top navigation end -->

    <style>
        .closeRating{
            font-size: 21px !important;
            color: #fff !important;
            font-weight: 500 !important;
            border: none !important;
            margin-right: -8px;
        }
    </style>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/shorttermcss.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-pseudo-ripple.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-nao-calendar.css">
    <link rel="stylesheet" href="https://unpkg.com/balloon-css/balloon.min.css">
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">


                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <div class="main-tabs">


                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="" id="people-vendor">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse">
                                                <span></span> Former Tenant</a> <a class="back" style="float: right;" href="/Tenantlisting/Tenantlisting">&lt;&lt; Back</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                        <div class="panel-body">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="apx-table">
                                                                                <div class="table-responsive overflow-unset">
                                                                                    <table id="tenant_listing" class="table table-bordered">
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!--Tabs Ends -->
                </div>
            </div>
        </div>
    </section>
</div>














<!--Star Modal End-->

<!-- Wrapper Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script>


    $("#people_top").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->
    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        jqGrid('All',true);
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";


    function jqGrid(status, deleted_at) {
        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }

        var table = 'users';
        var rentCurrSymbol = "Rent ("+currencySymbol+")";
        var balanceCurrSymbol = "Balance ("+currencySymbol+")";
        var columns = ['Tenant Name','Phone', 'Email','Property Name','Unit Number',rentCurrSymbol,balanceCurrSymbol,'Days Remaining'];
        //if(status===0) {
        var select_column = [];
        //}
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];

        var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'0',condition:'=',table:'users'},{column:'status',value:'6',condition:'=',table:'tenant_details'}];

        //var pagination=[];
        var columns_options = [

            {name:'Tenant Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:table,formatter:tenantName},
            {name: 'Phone', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor',change_type:'line_multiple',index2:'phone_number',index3:'other_work_phone_extension',join:{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'id',formatter:addToolTip,cellattr:cellAttri},

            {name:'Email',index:'email', width:80, align:"left",searchoptions: {sopt: conditions},table:table},

             {name:'Property Name',index:'property_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Unit Number',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:rentCurrSymbol,index:'rent_amount', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:currencyFormatter},
            {name:balanceCurrSymbol,index:'balance', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter3},
            {name:'Days Remaining',index:'days_remaining', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter2},

        ];
        var ignore_array = [];
        jQuery("#tenant_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Former Tenants",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    function tenantName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#flagTab" href="javascript:void(0);" data_url="/Vendor/ViewVendor?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }
    }
    function addToolTip(cellValue, options, rowObject) {
        if(rowObject !== undefined){

            if(rowObject.Notes == ''){
                return cellValue;
            } else {

                return '<div class="tooltipgridclass">'+cellValue+'<span class="tooltiptextbotclass">'+rowObject.Notes+'</span></div>';
            }
        }
    }
    function cellAttri(cellValue, options, rowObject) {
        if(rowObject !== undefined){

            if(rowObject.Notes != ''){
                return 'title = " "';
            }
        }
    }
    function currencyFormatter (cellValue, options, rowObject){
        if(cellValue!==undefined && cellValue!==""){
            return currencySymbol+''+cellValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } else {
            return "";
        }
    }
    function statusFormatter3 (cellValue, options, rowObject){
        return currencySymbol+''+'1,000.00';
    }
    function statusFormatter2 (cellValue, options, rowObject){
        today=new Date();
        dt1 = new Date(today);
        dt2 = new Date(cellValue);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }





</script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
