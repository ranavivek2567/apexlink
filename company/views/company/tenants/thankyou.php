<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Thank You!</title>
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400|Montserrat:700' rel='stylesheet' type='text/css'>
    <style>
        @import url(//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.min.css);
        @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css);
    </style>
    <link rel="stylesheet" href="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/default_thank_you.css">
    <script src="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/jquery-1.9.1.min.js"></script>
    <script src="https://2-22-4-dot-lead-pages.appspot.com/static/lp918/min/html5shiv.js"></script>
    <style>
        footer {background:#585858;float: left;width: 100%;text-align: center;color:#fff;padding: 12px;font-size: 14px;position: fixed; bottom: 0;}
        footer .container-fluid {width: 100%;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;}
        footer a {position: fixed;right: 5px;bottom: 50px;display: none;outline: 0;}
        .site-header{padding-top: 100px;}
        body{padding: 0px;}
        .imagelogo{padding: 10px;}
        .logo{ text-align: left}
        .site-header__title{font-size: 2rem;}
    </style>
</head>
<body>
<div class="header" style="max-height: 70px; margin-bottom: 20px;width: 100%;background: #d6f0fe;float: left;padding: 1px 0;min-width: 1024px;max-width: 100%;">
    <div class="header-inner">
        <div class="logo">
            <a class="navbar-brand" href="">
                <?php
                $logo_url = SITE_URL . 'company/images/logo.png';
                ?>
                <img class="imagelogo" alt="Apexlink" src="<?php echo $logo_url; ?>" width="150">
            </a>
        </div>
        <div class="hdr-rt">
        </div>
    </div>
</div>
<header class="site-header" id="header">
    <i class="fa fa-check main-content__checkmark" id="checkmark" style="margin-bottom: 36px !important;"></i>
    <h5 class="site-header__title" data-lead-id="site-header-title" style="font-size: 6.25rem !important; margin-bottom: 36px !important; ">Thank you!</h5>
   <!-- <h5 class="site-header__title" data-lead-id="site-header-title" style="font-size: 3.50rem !important;">YOU ARE ALL DONE!</h5>-->
</header>

<div class="main-content">
    <!--<i class="fa fa-check main-content__checkmark" id="checkmark"></i>-->
    <!--<p class="main-content__body" data-lead-id="main-content-body">Thanks a bunch for filling that out. It means a lot to us, just like you do! We really appreciate you giving us a moment of your time today. Thanks for being you.</p>-->
    <p class="main-content__body" data-lead-id="main-content-body" style="font-size: 2.10rem !important; margin: margin: 57px 0 0 !important;">Your signed document has been transmitted successfully, you will be receiving an email pdf copy of the document you signed as soon as everyone involved has signed.</p>
</div>

<!--<footer class="site-footer" id="footer">
    <p class="site-footer__fineprint" id="fineprint">Copyright © <?php /*echo date('Y');*/?> ApexLink, Inc. All rights reserved.</p>
</footer>-->
<footer>
    <div class="container-fluid">
        <a href="javascript:void(0);" class="movetotopicon"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i>
        </a>
        © <?php echo date('Y');?> ApexLink, Inc.  All rights reserved.
    </div>
</footer>
</body>
</html>