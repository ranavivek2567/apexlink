<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */






?>





<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>



<input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">
<input type="hidden" class="ticket_id" value="<?php echo $_GET['ticket_id']; ?>">


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php"); ?>

    <?php
    if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id'])) {
        if((!isset($_SESSION['tenant_id'])))
        {
            header('Location: '.SUBDOMAIN_URL.'/TenantPortal/login');
        }

    }

    ?>

    <section class="main-content">
        <form id="editTicket">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content-section">
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Add Ticket </h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="check-outer">
                                                <input type="radio" name="ticket_type" class="ticket_type" value="One Time" checked/><label>One Time</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="check-outer">
                                                <input type="radio" name="ticket_type" class="ticket_type" value="Reccuring"/><label>Reccuring</label>
                                            </div>
                                        </div>

                                        <div class="duration col-sm-3" style="display:none;">
                                            <select name="duration_type" class="form-control">
                                                <option value="Weekly">Weekly</option>
                                                <option value="Bi-Weekly">Bi-Weekly</option>
                                                <option value="Monthly">Monthly</option>
                                                <option value="Quarterly">Quarterly</option>
                                                <option value="Semi-Annually">Semi-Annually</option>
                                                <option value="Annually">Annually</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Ticket Number</label>
                                            <input class="form-control ticket_number" type="text" name="ticket_number"/>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Created On</label>
                                            <input class="form-control calander created_at" type="text" name="created_on" readonly/>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Category <em class="red-star">*</em></label>
                                            <select class="form-control category" name="category">
                                                <option value="">Select</option>
                                                <option value="Delete">Delete</option>
                                                <option value="General Inquiry">General Inquiry</option>
                                                <option value="Incident">Incident</option>
                                                <option value="Inspection">Inspection</option>
                                                <option value="Meter Reading">Meter Reading</option>
                                                <option value="Monthly Maintenance">Monthly Maintenance</option>
                                                <option value="Other">Other</option>
                                                <option value="Preventative Maintenance">Preventative Maintenance</option>
                                                <option value="Remove Replace">Remove Replace</option>
                                                <option value="Service Request">Service Request</option>
                                                <option value="Test">Test</option>
                                                <option value="Urgent">Urgent</option>
                                                <option value="Violation">Violation</option>
                                                <option selected="Selected" value="Yes">Yes</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Start Date</label>
                                            <input class="form-control calander start_date" type="text" name="start_date" readonly/>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>End Date</label>
                                            <input class="form-control calander end_date" type="text" name="end_date" readonly/>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Priority</label>
                                            <select class="form-control" name="priority">
                                                <option value="Low">Low</option>
                                                <option value="Normal">Normal</option>
                                                <option value="High">High</option>

                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Estimated Cost(<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                            <input class="form-control estimate_cost" type="text" name="estimate_cost"/>
                                        </div>

                                        <div class="col-sm-3 clear">
                                            <label>Required Materials</label>
                                            <textarea class="form-control required_matirial" name="required_matirial"></textarea>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Vendor Instruction</label>
                                            <textarea class="form-control vendor_instruction" name="vendor_instruction"></textarea>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Description</label>
                                            <textarea class="form-control description" name="description"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Property Information</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>Property <em class="red-star">*</em></label>
                                            <select class="form-control property" name="property_id" id="property">
                                                <option value="">Select</option>

                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Unit <em class="red-star">*</em></label>
                                            <select class="form-control unit" name="unit_id" id="unit"></select>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Tenant Name</label>
                                            <input type="text" class="form-control name" name="name" readonly>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Phone</label>
                                            <input type="text" class="form-control phone" name="phone" readonly>
                                        </div>

                                        <div class="col-sm-3">
                                            <label>Assign To <em class="red-star">*</em></label>
                                            <select class="form-control manager" name="assign_to"></select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Upload Image(s)</h3>
                                </div>
                                <div class="form-data">

                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                        <label>Pet Photo/Image</label>
                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                        <div class="upload-logo">
                                            <div class="img-outer ticket_image1"><img src="<?php echo COMPANY_SITE_URL ?>/images/hoa_image.png"></div>
                                            <a class="choose-img" href="javascript:;">Choose
                                                Image</a>
                                        </div>
                                        <div class="image-editor">
                                            <input type="file"
                                                   class="cropit-image-input form-control"
                                                   name="ticket_image1[]" accept="image/*">
                                            <div class="cropItData" style="display: none;">
                                                        <div class="cropit-preview"></div>
                                                                    <div class="cropit-rotate">
                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                    </a>
                                                                    </div>
                                                <div class="ticket_image1"></div>
                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                <input type="range"
                                                       class="cropit-image-zoom-input" min="0"
                                                       max="1" step="0.01">
                                                <input type="hidden" name="image-data"
                                                       class="hidden-image-data">
                                                <input type="button" class="export" value="Done"
                                                       data-val="ticket_image1">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                        <label>Pet Photo/Image</label>
                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                        <div class="upload-logo">
                                            <div class="img-outer ticket_image2"><img src="<?php echo COMPANY_SITE_URL ?>/images/hoa_image.png"></div>
                                            <a class="choose-img" href="javascript:;">Choose
                                                Image</a>
                                        </div>
                                        <div class="image-editor">
                                            <input type="file"
                                                   class="cropit-image-input form-control"
                                                   name="ticket_image2[]" accept="image/*">
                                            <div class="cropItData" style="display: none;">
                                                                                                                <div class="cropit-preview"></div>
                                                                    <div class="cropit-rotate">
                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                    </a>
                                                                    </div>
                                                <div class="ticket_image2"></div>
                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                <input type="range"
                                                       class="cropit-image-zoom-input" min="0"
                                                       max="1" step="0.01">
                                                <input type="hidden" name="image-data"
                                                       class="hidden-image-data">
                                                <input type="button" class="export" value="Done"
                                                       data-val="ticket_image2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                        <label>Pet Photo/Image</label>
                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                        <div class="upload-logo">
                                            <div class="img-outer ticket_image3"><img src="<?php echo COMPANY_SITE_URL ?>/images/hoa_image.png"></div>
                                            <a class="choose-img" href="javascript:;">Choose
                                                Image</a>
                                        </div>
                                        <div class="image-editor">
                                            <input type="file"
                                                   class="cropit-image-input form-control"
                                                   name="ticket_image3[]" accept="image/*">
                                            <div class="cropItData" style="display: none;">
                                                                    <div class="cropit-preview"></div>
                                                                    <div class="cropit-rotate">
                                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                    </a>
                                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                    </a>
                                                                    </div>
                                                <div class="ticket_image3"></div>
                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                <input type="range"
                                                       class="cropit-image-zoom-input" min="0"
                                                       max="1" step="0.01">
                                                <input type="hidden" name="image-data"
                                                       class="hidden-image-data">
                                                <input type="button" class="export" value="Done"
                                                       data-val="ticket_image3">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Notes</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <textarea class="notes description capital" name="notes"></textarea>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->


                            <!-- Form Outer Ends -->
                            <div class="form-outer">
                                <div class="btn-outer" style="display: none;">
                                    <button class="blue-btn udpatetickets">Update</button>
                                    <input type="button" class="grey-btn cancelTicket"  value="Cancel">
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
        </form>
    </section>


    <section>
        <div role="tabpanel" class="tab-pane" id="tenant-detail-eight">
            <div class="col-sm-12 maintenancefiles_main">
                <div class="form-outer">
                    <div class="form-hdr">
                        <h3>
                            File Library
                        </h3>
                    </div>
                    <div class="form-data">
                        <div class="apx-table">
                            <form id="addMaintenanceFiles">

                                <div id="collapseSeventeen" class="panel-collapse in">
                                    <div class="row">
                                        <div class="form-outer">
                                            <div class="col-sm-4 min-height-0 file_main">
                                                <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">

                                                <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">
                                                <input type="button" class="saveMaintenanceFile blue-btn" value="Save">
                                                <input type="button" class="cancelbtn grey-btn" value="Cancel">
                                            </div>
                                            <div class="row" id="file_library_uploads">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid-outer">
                                        <div class="apx-table">
                                            <div class="table-responsive">
                                                <table id="maintenanceFiles-table" class="table table-bordered">
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- Form Outer Ends -->
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>



    <div class="form-outer text-right">
        <div class="btn-outer">
            <button class="blue-btn udpateticketss">Update</button>
            <button type="button" class="clear-btn email_clear">Reset</button>
            <input type="button" class="grey-btn cancelTicketss" value="Cancel">
        </div>
    </div>



    <script>
        $(document).on("click",".clear-btn.email_clear",function () {
            window.location.reload();
            /*resetFormClear('#addTicket',['created_on','start_date','end_date','name','phone','ticket_number'],'form',false);*/
        });
    </script>

    <!-- Jquery Starts -->
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['Tenant_Portal']['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">';</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/edit-maintenance.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
    <script>var upload_url = "<?php echo SITE_URL; ?>";</script>

</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script>

    $(".to").tagsinput('items')
    addCalanderDate();


</script>