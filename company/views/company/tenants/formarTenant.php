<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
			
                <div class="bread-search-outer">
                    <div class="row">
					
                    
                        <div class="col-sm-4 pull-right">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
               <div class="content-data">

                  <div class="form-outer">
                                      <div class="form-hdr">
                                        <h3>Former Tenant <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                      </div>
                                      <div class="form-data">
										<div class="row">
											 <div class="col-sm-4"><input class="form-control" placeholder="Please Enter Name, Invoice Number, Property, or Balance Amount" type="text"/></div>
										</div>
                                        <div class="accordion-grid">
                                          <div class="accordion-outer">
                                            <div class="bs-example">
                                              <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                  <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> Former Tenant</a> 
                                                    </h4>
                                                  </div>
                                                  <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                      <div class="grid-outer">
                                                        <div class="table-responsive">
                                                          <table class="table table-hover table-dark">
                                                            <!--<thead>
                                                              <tr>
                                                                <th scope="col">Name</th>
                                                                <th scope="col">Property</th>
                                                                <th scope="col">Unit Number</th>
                                                                <th scope="col">Type</th>
                                                                <th scope="col">Balance Amount (US $)</th>
                                                                <th scope="col">Invoice Number</th>
                                                              </tr>
                                                            </thead>-->
                                                            <tbody>
                                                              <tr>
                                                                <td>No Record Found</td>
                                                               
                                                              </tr>                                                             
                                                            </tbody>
                                                          </table>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                </div>
				</div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->


<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
   
    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
	
	$("#accounting_top").addClass("active");
</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->