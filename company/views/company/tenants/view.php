<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php");
?>
<link rel="stylesheet" type="text/css" href="<?php echo COMPANY_SITE_URL; ?>/css/tooltipster.bundle.css" />
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/smartmove.css"/>
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/shorttermcss.css">
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/jquery-pseudo-ripple.css">
<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/jquery-nao-calendar.css">
<style>
    #caddress1
    {
        text-transform:capitalize;
    }

    #caddress2
    {
        text-transform:capitalize;
    }

    #cCompany
    {
        text-transform:capitalize;
    }
    label.tenant-view-label, .amount_paid-new, .currecy_padding-new {
        font-size: 16px;
    }
    .payment-form-label .payment-method-text {
        margin: 0;
        text-align: center;
        font-size: 18px;
        text-decoration: underline;
        font-weight: 600;
    }
    font-size: 16px;
    }
</style>
<div id="wrapper">

    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>

    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Tenant Module &gt;&gt; <span>Tenant Listing</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="javascript:;" aria-controls="home" role="tab" data-toggle="tab">Tenants</a></li>
                            <li role="presentation"><a href="/People/Ownerlisting">Owners</a></li>
                            <li role="presentation"><a href="/Vendor/Vendor" >Vendors</a></li>
                            <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                            <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                        </ul>

                        <!--Right Navigation Link Starts-->

                        <?php
                        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/tenants/right-side-navigation.php");
                        ?>

                        <!--Right Navigation Link Ends-->

                        <div class="property-status">
                            <div class="atoz-outer2">
                                    <span class="apex-alphabets" id="apex-alphafilter" style="display:none;">
                                    <span class="AtoZ"></span></span>
                                <span class="AZ" id="AZ">A-Z</span>
                                <span id="allAlphabet" style="cursor:pointer;">All</span>
                            </div>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="people-tenant">
                                <!-- Sub Tabs Starts-->
                                <div class="sub-tabss sub-navs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="/Tenantlisting/Tenantlisting">Regular</a></li>
                                        <li role="presentation"><a href="/ShortTermRental/RentersListing">Short Term Renters</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="regular-rent">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2 tenant_type_status">
                                                        <label>Status</label>
                                                        <select class="fm-txt form-control jqGridStatusClass"  id="jqgridOptions" data-module="PEOPLELISTING">
                                                            <option value="All">All</option>
                                                            <option value="0">Inactive</option>
                                                            <option value="1">Active</option>
                                                            <option value="2">Evicting</option>
                                                            <option value="3">In-Collection</option>
                                                            <option value="4">Bankruptcy</option>
                                                            <option value="5">Evicted</option>
                                                            <option value="6">Former Tenants</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="btn-outer text-right">
                                                            <a href="<?php echo COMPANY_SITE_URL; ?>/excel/TenantSample.xlsx" download class="blue-btn">Download Sample</a>
                                                            <button class="blue-btn" id="import_tenant">Import Tenant</button>
                                                            <a class="blue-btn" id="export_tenant">Export Tenant</a>
                                                            <a href="add">  <button onclick="window.location.href='Companies/Add'" class="blue-btn">New Tenant</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div  style="display: none;" id="import_tenant_type_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">

                                                                <a>Import Tenant Type</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="panel-body">
                                                                <form name="importTenantTypeForm" id="importTenantTypeFormId" >
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                                                <span class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="btn-outer">
                                                                                <button type="submit" class="blue-btn">Submit</button>
                                                                                <button type="button" id="import_tenant_cancel_btn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">

                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive overflow-unset">
                                                                                <div class="grid-outer">
                                                                                    <div class="apx-table">
                                                                                        <table class="table table-hover table-dark" id="tenant_listing">
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->
                                     <div role="tabpanel" class="tab-pane" id="shortterm-rent">

                                        </div>
                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>
                            <div role="tabpanel" class="tab-pane" id="people-owner">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>Status</label>
                                            <select class="fm-txt form-control"> <option>Active</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn">Download Sample</button>
                                                <button class="blue-btn">Import Owner</button>
                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Owner</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Owners</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Owner Name</th>
                                                                            <th scope="col">Company</th>
                                                                            <th scope="col">Phone</th>
                                                                            <th scope="col">Email</th>
                                                                            <th scope="col">Date Created</th>
                                                                            <th scope="col">Owner's Portal</th>
                                                                            <th scope="col">Status</th>
                                                                            <th scope="col">Actions</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                            <td></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>allen09@gmail.com</td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td>Yes</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                            <td></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>allen09@gmail.com</td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td>Yes</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                            <td></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>allen09@gmail.com</td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td>Yes</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                            <td></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>allen09@gmail.com</td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td>Yes</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="people-vendor">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>Vendor Type</label>
                                            <select class="fm-txt form-control"> <option>Select</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>

                                        </div>
                                        <div class="col-sm-2">
                                            <label>Status</label>
                                            <select class="fm-txt form-control"> <option>Active</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>

                                        </div>
                                        <div class="col-sm-8">
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn">Download Sample</button>
                                                <button class="blue-btn">Import Vendor</button>
                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Vendor</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Vendors</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Vendor Name</th>
                                                                            <th scope="col">Phone</th>
                                                                            <th scope="col">Open Work Orders</th>
                                                                            <th scope="col">YTD Payment</th>
                                                                            <th scope="col">Type</th>
                                                                            <th scope="col">Rate</th>
                                                                            <th scope="col">Rating</th>
                                                                            <th scope="col">Email</th>
                                                                            <th scope="col">Status</th>
                                                                            <th scope="col">Actions</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>1</td>
                                                                            <td>0.00</td>
                                                                            <td>Contractor-General</td>
                                                                            <td>$19.00 /hr</td>
                                                                            <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                            <td>skkokjim@gmail.com</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>1</td>
                                                                            <td>0.00</td>
                                                                            <td>Contractor-General</td>
                                                                            <td>$19.00 /hr</td>
                                                                            <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                            <td>skkokjim@gmail.com</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>1</td>
                                                                            <td>0.00</td>
                                                                            <td>Contractor-General</td>
                                                                            <td>$19.00 /hr</td>
                                                                            <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                            <td>skkokjim@gmail.com</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>1</td>
                                                                            <td>0.00</td>
                                                                            <td>Contractor-General</td>
                                                                            <td>$19.00 /hr</td>
                                                                            <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                            <td>skkokjim@gmail.com</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="people-contact">
                                <!-- Sub Tabs Starts-->
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#contact-tenant" aria-controls="home" role="tab" data-toggle="tab">Tenants<span class="tab-count">84</span></a></li>
                                        <li role="presentation"><a href="#contact-owner" aria-controls="profile" role="tab" data-toggle="tab">Owners<span class="tab-count">36</span></a></li>
                                        <li role="presentation"><a href="#contact-vendor" aria-controls="home" role="tab" data-toggle="tab">Vendors<span class="tab-count">71</span></a></li>
                                        <li role="presentation"><a href="#contact-users" aria-controls="profile" role="tab" data-toggle="tab">Users<span class="tab-count">199</span></a></li>
                                        <li role="presentation"><a href="#contact-others" aria-controls="profile" role="tab" data-toggle="tab">Others<span class="tab-count">84</span></a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="contact-tenant">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Status</label>
                                                        <select class="fm-txt form-control"> <option>Active</option>
                                                            <option></option>
                                                            <option></option>
                                                            <option></option>
                                                        </select>

                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="btn-outer text-right">
                                                            <button class="blue-btn">Download Sample</button>
                                                            <button class="blue-btn">Import Contact</button>
                                                            <button onclick="window.location.href='new-property.html'" class="blue-btn">New Contact</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                            <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Contacts</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-dark">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th scope="col">Contact Name</th>
                                                                                        <th scope="col">Phone</th>
                                                                                        <th scope="col">Email</th>
                                                                                        <th scope="col">Date Created</th>
                                                                                        <th scope="col">Status</th>
                                                                                        <th scope="col">Actions</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Abby N Wesley</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Arsh Sandhu</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Ben Snow</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                        <td>555-444-6666</td>
                                                                                        <td>abby768@gmail.com</td>
                                                                                        <td>12/7/2018 12:30:53 PM</td>
                                                                                        <td>Active</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                        </div>
                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>
                            <div role="tabpanel" class="tab-pane" id="people-employee">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>Status</label>
                                            <select class="fm-txt form-control"> <option>Active</option>
                                                <option></option>
                                                <option></option>
                                                <option></option>
                                            </select>

                                        </div>
                                        <div class="col-sm-10">
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn">Download Sample</button>
                                                <button class="blue-btn">Import Employee</button>
                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Employee</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Employees</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Employee Name</th>
                                                                            <th scope="col">Phone</th>
                                                                            <th scope="col">Email</th>
                                                                            <th scope="col">Date Created</th>
                                                                            <th scope="col">Status</th>
                                                                            <th scope="col">Actions</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>adam@gmail.com</td>
                                                                            <td>11/23/2018 8:48:45 AM</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>adam@gmail.com</td>
                                                                            <td>11/23/2018 8:48:45 AM</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>adam@gmail.com</td>
                                                                            <td>11/23/2018 8:48:45 AM</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                            <td>555-444-6666</td>
                                                                            <td>adam@gmail.com</td>
                                                                            <td>11/23/2018 8:48:45 AM</td>
                                                                            <td>Active</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Wrapper Ends -->
<!--Model For Move Search Start-->
<div class="container">
    <div class="modal fade" id="moveout_listSearch" role="dialog">
        <div class="modal-dialog modal-lg unit-new-renovation-form">
            <div class="modal-content pull-left" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Charges</h4>
                </div>

                <div class="modal-body">
                    <div class="form-outer col-sm-12">
                        <form id="new_chargeCode_form">
                            <!-- <input type ="hidden" id="chargeCode_userId" name="chargeCode_userId" value="<?php /*echo $_REQUEST["id"]*/?>"  />-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <label>Tenant</label>
                                    <input class="form-control" type="text" id="myInput"  placeholder="Click here or start typing the name of the tenant you want to move-out" title="Type in a name">

                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="grid-outer">
                                            <div class="table-responsive" id="tenantData_search">

                                                <!--<table class="table table-hover table-dark" id="myTable">
                                                    <thead>
                                                        <tr class="header">
                                                            <th>Tenant Name</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>propertyName</th>
                                                            <th>Unit</th>
                                                            <th>Rent()</th>
                                                            <th>Balance()</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Model For Move Search Start-->

<!--Model for Tenant Transfer start-->
<div class="container">
    <div class="modal fade" id="transfer_listSearch" role="dialog">
        <div class="modal-dialog modal-lg unit-new-renovation-form">
            <div class="modal-content pull-left" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Tenant Transfer</h4>
                </div>

                <div class="modal-body">
                    <div class="form-outer col-sm-12">
                        <form id="new_chargeCodeTransfer_form">
                            <!-- <input type ="hidden" id="chargeCode_userId" name="chargeCode_userId" value="<?php /*echo $_REQUEST["id"]*/?>"  />-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-12">
                                    <label>Tenant</label>
                                    <input class="form-control" type="text" id="myInputTransfer"  placeholder="Click here or start typing the tenant's name" title="Type in a name">

                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="grid-outer">
                                            <div class="table-responsive" id="transferData_search"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>
<div class="modal fade" id="tenant-payment" role="dialog">
    <input type="hidden" class="stripe_amount">
    <div class="modal-dialog modal-dialog-type modal-md" style="width: 60%;">
        <div class="modal-content payment-modal-new pull-left" style="width: 100%;">
            <div class="modal-header">
                <h3 class="payment-modal-head">Tenant Payment<span class='userName'></span></h3>
                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>
                <!-- <h4 class="modal-title">Online Payment Details</h4> -->
            </div>
            <div class="modal-body">
                <div class="apx-adformbox-content">
                    <div class="apx-adformbox-content-top">
                        <div class="col-sm-8 wd-auto">
                            <h3>Account Balance Due <em class="red-star"><?php echo $default_symbol; ?><span class="paidAmount currecy_padding"></span></em></h3>
                            <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Sum of Rent,Charges and miscellaneous</span></div>
                            <input type="hidden" name="company_user_id" id="company_user_id">
                        </div>
                        <!--<div class="col-sm-4 text-right full-amount-btn pull-right">
                            <button type="button" class="blue-btn anotherAmountbutton" id="anotherAmountbutton"   >Pay Another Amount</button>
                            <button type="button" class="blue-btn fullAmountButton" id="fullAmountButton"  style="display:none">Full Amount</button>
                        </div>
                        <div class="form-group col-sm-12 " >
                            <input type="text" class="fm-txt form-control amount_num number_only"  id="another_amoumt" style="display: none;" >
                        </div>-->
                    </div>

                    <div class="col-sm-12 payment-method-link">
                        <div class=" stripeMsg">
                        </div>
                        <div class="payment-interval-btn">
                            <a href="#" class="setPaymentInterval">Set Payment Interval</a>
                        </div>
                    </div>
                    <form id="tenant_payment_interval_form" name="tenant_payment_interval_form">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Payment Method</label>
                                        <select class="fm-txt form-control"  id="paymentMethod" name="payment_method">
                                            <option value="one-time-payment">One-Time Payment</option>
                                            <option value="reoccurring">Reoccurring</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select class="mdb-select form-control" multiple  id="payment_category" name="payment_category[]">
                                            <option value="0" class="allselect">Select All</option>
                                            <option value="1">Rent</option>
                                            <option value="2">Utilities</option>
                                            <option value="3">Miscellaneous</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label>Comment?</label>
                                    <div class="notes_date_right_div">
                                        <textarea class="payment_comment notes_date_right" id="payment_comment" name="payment_comment" placeholder=""> </textarea>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 payment-custom  full-amount-btn ">
                                    <button type="button" class="blue-btn anotherAmountbutton" id="anotherAmountbutton">Pay Another Amount</button>
                                    <button type="button" class="blue-btn fullAmountButton" id="fullAmountButton"  style="display:none">Full Amount</button>
                                    <input type="text" class="fm-txt form-control amount_num number_only"  id="another_amoumt" style="display: none;" >
                                    </div>
                               
                             <div class="col-md-6">
                                <div class="form-group amountField" style="display: none;">
                                    <label>Amount</label>
                                    <input type='text' class="amount form-control" id="amount">
                                </div>
                             </div>
                            <div class="col-md-6">
                                <div class="form-group countField" style="display: none;">
                                    <label>Count</label>
                                    <input type='text' class="count form-control" id="count">
                                </div>
                             </div>
                                <div class="col-md-6">
                                    <div class="form-group paymentIntervel" style="display: none;">
                                        <label>Payment Interval</label>
                                        <select class="fm-txt form-control mb-15"  id="paymentIntervel" name="payment_interval">
                                                <option value="weekly">Weekly</option>
                                            <option value="bi-weekly">Bi-weekly</option>
                                            <option value="monthly">Monthly</option>
                                            <option value="annual">Annual</option>
                                        </select>

                                    </div>
                                    <div class="form-group paymentIntervelcalander" style="display: none;">
                                        <input type="text" class="fm-txt form-control jqGridStatusClass"  id="paymentIntervelcalander" name="payment_calender" readonly>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="agree-payment-checkbox"><input type='checkbox' id="payment_terms" name='payment_terms' class="payment_terms">I agree to the following <a href="#">payment terms</a> </label>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-sm-12 online-payment-button">

                                    </div>
                                </div>
                            </div>

                            <div class="payment-button">
                                <input type="button"  id="paymentDeduction" class="blue-btn" value="Submit Payment">
                                <button type="button"  id="paymentCancel" class="grey-btn">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="stripe-checkout" role="dialog">
    <div class="modal-dialog modal-dialog-type modal-md" style="width: 60%;">
        <div class="modal-content payment-modal-new" style="width: 100%;">
            <div class="modal-header">
                <h3 class="payment-modal-head">Account Balance Due <em class="red-star"><?php echo $default_symbol; ?><span class="paidAmount currecy_padding"></span></em></h3>
                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>

                <!-- <h4 class="modal-title">Online Payment Details</h4> -->
            </div>
            <div class="modal-body">
                <form action="/charge" method="post" id="payment-form1">
                    <input type="hidden" name="tenant_form_serialize_date" id="tenant_form_serialize_date">
                    <div class="apx-adformbox-content">

                        <div class="form-row">
                            <label for="card-element1" class="card-payment-text">
                                Credit / Debit card
                            </label>
                            <div id="card-element1">
                                <!-- A Stripe Element will be inserted here. -->
                            </div>

                            <!-- Used to display form errors. -->
                            <div id="card-errors" role="alert"></div>
                        </div>




                        <div class="payment-button">
                            <button type="submit"  id="submit" class="blue-btn">Pay <?php echo $default_symbol; ?><span class="paidAmount currecy_padding"></span></button>

                        </div>

                    </div>
                </form>


            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="set-method" role="dialog">
    <div class="modal-dialog modal-md" style="width: 60%;">
        <div class="modal-content online-payment-new" style="width: 100%;">
            <div class="modal-header">

                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>

                <h4 class="modal-title">Online Payment Setup<span class='userName'></span></h4>
            </div>
            <div class="modal-body">

                <div class="apx-adformbox-content">
                    <!--                        <form method="post" id="add-card" name="add-card">-->
                    <input type="hidden" name="company_user_id" id="company_user_id">
                    <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>

                    <h3>Set Payment Method <em class="red-star">*</em></h3>

                    <div class="row">
                        <input type="hidden" class='firstPaymentStatus' value='0'>
                        <div class="form-outer">
                            <div class="row">
                                <div class="col-sm-4 col-md-4 mb-15">
                                    <label></label>
                                    <select class="form-control setType" name="payment_method" id="payment_method" >
                                        <option value="1">Credit Card/Debit Card</option>
                                        <option value="2">ACH</option>

                                    </select>
                                    <span class="ffirst_nameErr error red-star"></span>
                                </div>
                            </div>
                            <div class="row cards">

                                <div class="cardDetails table-responsive col-sm-12"></div>
                                <form id="addCards">
                                    <div class="detail-outer">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="black-label">Card Number <em class="red-star">*</em></label>

                                                <input class="form-control hide_copy card_number numberonly" name="card_number"  type="text" maxlength="16"/>
                                                <em class="nicknameErr error red-star"></em>

                                            </div>
                                            <div class="col-sm-6">
                                                <label class="black-label">Expiration Date <em class="red-star">*</em></label>

                                                <input class="form-control exp_date" name="exp_date"  type="text" readonly/>
                                                <em class="nicknameErr error red-star"></em>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="black-label">Cardholder Name <em class="red-star">*</em></label>

                                                <input class="form-control holder_name" name="holder_name" type="text"/>
                                                <em class="nicknameErr error red-star"></em>

                                            </div>
                                            <div class="col-sm-6">
                                                <label class="black-label">CVC <em class="red-star">*</em></label>

                                                <input class="form-control cvc" name="cvc"  type="text"/>
                                                <em class="nicknameErr error red-star"></em>

                                            </div>
                                        </div>


                                        <div class="col-sm-5 clear">
                                 
                                        </div>



                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 online-payment-button">
                                            <input type="submit" class="blue-btn" value="Confirm">
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="row accounts" style="display:none;">
                                <div class="accountDetails table-responsive col-sm-12"></div>
                            </div>
                        </div>
                        <!--                    <div class="row" style="text-align: center;">-->
                        <!--                        <button type="submit"  id="savefinancialCard" class="blue-btn">Continue</button>-->
                        <!--                    </div>-->

                        <!--                        </form>-->
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="set-interval" role="dialog">
    <div class="modal-dialog modal-dialog-type modal-md" style="width: 60%;">
        <div class="modal-content payment-modal-new" style="width: 100%;">

              <div class="modal-header">
                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>
                <h4 class="modal-title">Active Payment Intervals</h4>
            </div>
            <div class="modal-body">
               <!-- <h3>Set Payment Interval</h3>-->

                <div class="getPaymentIntervals"></div>


            </div>
        </div>
    </div>
</div>



<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SITE_URL ?>/images/loading.gif'/>
    </div>
</div>
<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SITE_URL ?>/images/loading.gif'/>
    </div>
</div>


<!--Model for Tenant Transfer End-->

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script>
    $("#moveout_Search").click(function(){
        $('#moveout_listSearch').modal('show');
    });
</script>
<script>
    $("#transfer_search").click(function(){
        $('#transfer_listSearch').modal('show');
    });
</script>
<!--<script>
function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();

    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        //alert(td);
        if (td) {
        txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                //console.log(tr[i].getElementsByTagName("td")[0]);debugger;
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "";
            }
        }
    }
}
</script>-->
<script src="https://js.stripe.com/v3/"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script>var upload_url = "<?php echo SITE_URL; ?>";</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantListing.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/shortTermRental.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/smartMove.js"></script>
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
</script>

<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
<script type="text/javascript" src="<?php echo COMPANY_SITE_URL; ?>/js/tooltipster.bundle.js"></script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/payments/plaidIntialisation.js"></script>
<!--<script type="text/javascript"
        src="https://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>-->
<script src="<?php echo COMPANY_SITE_URL; ?>/js/fullcal.js" integrity="sha384-vk5WoKIaW/vJyUAd9n/wmopsmNhiy+L2Z+SBxGYnUkunIxVxAv/UtMOhba/xskxh" crossorigin="anonymous"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-pseudo-ripple.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery-nao-calendar.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/payments/subPayment.js"></script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<script>
    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });

    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".calander").val(date);
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";

</script>
<script>
    $(document).ready(function() {
        // $('.toolTipPhone').tooltipster();
        $('.tooltip').tooltipster({
            animation: 'fade',
            delay: 200,
            theme: 'tooltipster-punk',
            trigger: 'click'
        });
    });
</script>
<!-- Footer Ends -->