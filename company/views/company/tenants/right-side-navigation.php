<div class="right-links-outer hide-links">
    <div class="right-links">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </div>
    <div id="RightMenu" class="box2">
        <h2>PEOPLE</h2>
        <div class="list-group panel">
            <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
            <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" id="moveout_Search">Move Out</a>
            <a id ="shorttermrentals" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
            <a href="/People/AddOwners" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
            <a href="/Vendor/AddVendor" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
            <a href="/People/AddEmployee" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
            <a href="/People/AddContact" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
            <a href="/Tenant/Receivebatchpayment" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
            <a href="/Tenant/TenantStatements" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
            <a href="/Tenant/BatchRegister" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
            <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
            <a href="/Tenant/InstrumentRegister" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
            <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
            <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" id="transfer_search">Tenant Transfer</a>
            <a href="/Tenant/Transfer" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
            <a href="/Lease/Movein" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
            <a href="/Tenant/MoveOut" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
            <a href="/Tenant/FormarTenant" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
            <a href="/Vendor/FormarOwner" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
            <a href="/Vendor/FormarVendor" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
            <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
        </div>
    </div>
</div>