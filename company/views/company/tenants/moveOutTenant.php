<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

/*if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}*/
?>

<?php include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php"); ?>


<div class="popup-bg"></div>


<div id="wrapper">
    <input type='hidden' name='moveOutTenant_id' class="moveOutTenant_id" value="<?php echo $_GET['id']; ?>">

    <?php

    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 bread-search-outer">
                        <!--<div class="breadcrumb-outer">
                            Tenant>> <span>Tenant Transfer</span>
                        </div>-->
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text" />
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="content-section">

                            <div class="form-outer form-outer2">
                                <div class="form-hdr">
                                    <h3>Tenant Information</h3>
                                </div>
                                <div class="form-data pad-none">
                                    <div class="grey-detail-box">
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Tenant Name:</label>
                                            <span id="tenantMoveOutName"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Property Name:</label>
                                            <span id="tenantMoveOutPropertyName"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Unit#:</label>
                                            <span id="tenantMoveOutUnit">BillA6</span>
                                        </div>
                                    </div>
                                    <div class="grey-detail-box">
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Balance Due:</label>
                                            <span id="moveOutBalanceDue"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Rent(USh):</label>
                                            <span id="tenantMoveRent"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Security Deposit(USh):</label>
                                            <span id="moveSecurityDeposit"></span>
                                        </div>
                                    </div>

                                    <div class="grey-detail-box">

                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Lease Start Date:</label>
                                            <span id="tenantMoveLeaseDate"></span>
                                        </div>

                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Phone:</label>
                                            <span id="tenantMoveOutPhone"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Entity/Company Name:</label>
                                            <span id="tenantMoveOutEntity"></span>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <!--Tabs Starts -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->

                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>Move Out  <a class="back" href="/Tenantlisting/Tenantlisting"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="main-tabs">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist" id="myTab">
                                                <li role="presentation" class="active"><a href="#moveout-one" id="moveout_tenant1" aria-controls="home" role="tab" data-toggle="tab">Notice Period</a></li>
                                                <li role="presentation"><a href="#moveout-two"  id="moveout_tenant2" aria-controls="profile" role="tab" data-toggle="tab">Record Move Out</a></li>
                                                <li role="presentation" class="moveout_tenant3" ><a href="#moveout-three" id="moveout_tenant3" aria-controls="profile" role="tab" data-toggle="tab">Charges</a></li>
                                                <li role="presentation"><a href="#moveout-four" id="moveout_tenant4" aria-controls="profile" role="tab" data-toggle="tab">Clear Balances</a></li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">

                                                <div role="tabpanel" class="tab-pane active" id="moveout-one">
                                                    <form id="noticePeriodMoveOut">
                                                        <div class="form-outer">
                                                            <div class="form-hdr">
                                                                <h3>Notice Period</h3>
                                                            </div>
                                                            <div class="form-data">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                        <input type="hidden" class="notice-date" id="notice-date">
                                                                        <label>Notice Date <em class="red-star">*</em></label>
                                                                        <input class="form-control calander" type="text" id="noticeDateMoveOut" class="noticeDateMoveOut" name="noticeDate" readonly>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                        <label>Scheduled Move Out Date <em class="red-star">*</em></label>
                                                                        <input class="form-control" type="text" id="scheduledMoveOutDate" name="scheduledMoveOutDate" readonly>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-2 apx-inline-popup">
                                                                        <label>Reason for Leaving <em class="red-star">*</em>
                                                                            <a class="pop-add-icon propMoveOuticon" href="javascript:;">
                                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                            </a>
                                                                        </label>
                                                                        <select class=" form-control" name="reasonForLeaving">
                                                                            <option value="">Select</option>
                                                                        </select>
                                                                        <div class="add-popup" id="selectreasonForLeaving" style="width: 127%;">
                                                                            <h4>Add Reason</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Reason <em class="red-star">*</em></label>
                                                                                        <input name="reasonName" class="form-control customValidateGroup reason_ForLeaving capital" type="text" data_required="true" data_max="150" placeholder="Reason">
                                                                                        <span class="customError required" aria-required="true" id="reason_ForLeaving"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn add_single1" data-table="reasonmoveout_tenant" data-cell="reasonName" data-class="reason_ForLeaving" data-name="reasonForLeaving">Save</button>
                                                                                        <input type="button" class="clear-btn ClearReaasonForm" value="Clear">
                                                                                        <input type="button" class="grey-btn reasonCancelButton" value="Cancel">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Form Outer Ends-->

                                                        <div class="btn-outer text-right">
                                                            <input type="submit" class="blue-btn" value="Update">
                                                            <input type="button" name="button" class="clear-btn ResetForm" value="Reset">
                                                            <a href="javascript:;" id="updateMoveOutCancel" class="grey-btn">Cancel </a>
                                                        </div>
                                                    </form>

                                                </div>
                                                <!-- First Tab Ends-->

                                                <div role="tabpanel" class="tab-pane" id="moveout-two">
                                                    <form id="updateDataRecordMoveOut">
                                                        <div class="form-outer">
                                                            <div class="form-hdr">
                                                                <h3>Record Move Out Date</h3>
                                                            </div>
                                                            <div class="form-data">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                        <label>Actual Moveout Date<em class="red-star">*</em></label>
                                                                        <input name="actualMoveOutDate" id="actualMoveOutDate" class="form-control" type="text" readonly/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                        <label>Unit Available Date<em class="red-star">*</em></label>
                                                                        <input name="unitAvailableDate" class="form-control unitAvailableDate" type="text"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Numbers of Keys Signed at Move In</label>
                                                                        <input name="noOfKeysSigned_movein"  id="noOfKeysSignedAtMoveIn"  class="form-control" type="text" readonly/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>No. Of Keys Signed at Move Out<em class="red-star">*</em></label>
                                                                        <input name="noOfKeysSigned_moveout" id="noOfKeysSigned_moveout" class="form-control" type="text"/>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <!-- Form Outer Ends-->

                                                        <div class="btn-outer text-right">
                                                            <input type="submit" class="blue-btn" value="Save">
<!--                                                            <input type="button" name="button" class="clear-btn ResetkeysForm" value="Reset">-->
                                                            <!--<button class="grey-btn">Cancel</button>-->
                                                        </div>
                                                    </form>
                                                </div>
                                                <!-- Second Tab Ends-->

                                                <div role="tabpanel" class="tab-pane" id="moveout-three">

                                                    <div class="form-outer" id="tenantMoveOut-Forwarding" style="display: none;">
                                                        <form id="addForwordingAddress_Moveout">
                                                            <div class="form-hdr">
                                                                <h3>Forwarding Address</h3>
                                                            </div>
                                                            <div class="form-data">

                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label>First Name <em class="red-star">*</em></label>
                                                                        <input name="FrwdAddress_FName" class="form-control capital" type="text" id="FrwdAddress_FName"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Last Name <em class="red-star">*</em></label>
                                                                        <input name="FrwdAddress_LName" class="form-control capital" type="text" id="FrwdAddress_LName"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Address <em class="red-star">*</em></label>
                                                                        <input name="FrwdAddress_Address" class="form-control" type="text" id="FrwdAddress_Address"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Zip / Postal Code <em class="red-star">*</em></label>
                                                                        <input name="FrwdAddress_Zip" class="form-control" type="text" id="FrwdAddress_Zip"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Country <em class="red-star">*</em></label>
                                                                        <input name="FrwdAddress_Country" class="form-control" type="text" id="FrwdAddress_Country"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>State / Province <em class="red-star">*</em></label>
                                                                        <input name="FrwdAddress_State" class="form-control" type="text" id="FrwdAddress_State"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>City <em class="red-star">*</em></label>
                                                                        <input name="FrwdAddress_City" class="form-control" type="text" id="FrwdAddress_City"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Phone <em class="red-star">*</em></label>
                                                                        <input name="FrwdAddress_Phone" class="form-control add-input phone_format valid" type="text" id="FrwdAddress_Phone" aria-required="true" maxlength="12"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Email <em class="red-star">*</em></label>
                                                                        <input name="FrwdAddress_Email" class="form-control error" type="text" id="FrwdAddress_Email" aria-required="true" aria-invalid="true"/>
                                                                    </div>
                                                                </div>
                                                                <div class="btn-outer text-right">
                                                                    <!--<button class="blue-btn" type="submit">Save</button>-->
                                                                    <!--<button class="grey-btn" type="submit">Cancel</button>-->
                                                                    <a href="javascript:;" ><input type="submit" class="blue-btn" value="Save"></a>
                                                                    <input type="button" name="button" class="clear-btn ResetForm" value="Reset">
                                                                    <a href="javascript:;"><input type="button" class="grey-btn cancel_MoveOut" value="Cancel"></a>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="form-outer" id="MoveOut-ForwardingUpdate" style="display: none;">
                                                        <form id="updateForwordingAddress_Moveout">
                                                            <div class="form-hdr">
                                                                <h3>Forwarding Address</h3>
                                                            </div>
                                                            <div class="form-data">

                                                                <div class="row">
                                                                    <div class="col-lg-3">
                                                                        <label>First Name <em class="red-star">*</em></label>
                                                                        <input name="frwdAddress_FName" class="form-control" type="text" id="FrwdAddress_FName1"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Last Name <em class="red-star">*</em></label>
                                                                        <input name="frwdAddress_LName" class="form-control" type="text" id="FrwdAddress_LName1"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Address <em class="red-star">*</em></label>
                                                                        <input name="frwdAddress_Address" class="form-control" type="text" id="FrwdAddress_Address1"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Zip / Postal Code <em class="red-star">*</em></label>
                                                                        <input name="frwdAddress_Zip" class="form-control" type="text" id="FrwdAddress_Zip1"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Country <em class="red-star">*</em></label>
                                                                        <input name="frwdAddress_Country" class="form-control" type="text" id="FrwdAddress_Country1"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>State / Province <em class="red-star">*</em></label>
                                                                        <input name="frwdAddress_State" class="form-control" type="text" id="FrwdAddress_State1"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>City <em class="red-star">*</em></label>
                                                                        <input name="frwdAddress_City" class="form-control" type="text" id="FrwdAddress_City1"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Phone <em class="red-star">*</em></label>
                                                                        <input name="frwdAddress_Phone" class="form-control  add-input phone_format valid" type="text" id="FrwdAddress_Phone1" aria-required="true" maxlength="12"/>
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <label>Email <em class="red-star">*</em></label>
                                                                        <input name="email" class="form-control add-input error" type="text" id="FrwdAddress_Email1" aria-required="true" aria-invalid="true"/>
                                                                    </div>
                                                                </div>
                                                                <div class="btn-outer text-right">
                                                                    <!--<button class="blue-btn" type="submit">Save</button>-->
                                                                    <!--<button class="grey-btn" type="submit">Cancel</button>-->
                                                                    <a href="javascript:;" ><input type="submit" class="blue-btn" value="Update"></a>
                                                                    <a href="javascript:;"><input type="button" class="grey-btn cancel_MoveOutUpdate" value="Cancel"></a>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>


                                                    <div class="form-outer" id="frwdAdd_records">
                                                        <div class="form-hdr">
                                                            <h3>Forwarding Address</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="btn-outer">
                                                                <button class="grey-btn pull-right" type="submit" id="add-forwading-tenant">Add Forwading</button>
                                                            </div>
                                                            <div class="grid-outer mt-20">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark" id="addForwading-table">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">First Name</th>
                                                                            <th scope="col">Last Name</th>
                                                                            <th scope="col">Address</th>
                                                                            <th scope="col">Phone</th>
                                                                            <th scope="col">Email</th>
                                                                            <th scope="col">Action</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Form Outer Ends-->

                                                    <div class="form-outer">
                                                        <div class="form-hdr">
                                                            <h3>Apply Charges</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark" id="chargeCode-table">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Charge Code</th>
                                                                            <th scope="col">Description</th>
                                                                            <th scope="col">Frequency</th>
                                                                            <th scope="col">Amount</th>
                                                                            <th scope="col">Date</th>
                                                                            <th scope="col">Apply</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Form Outer Ends-->

                                                    <div class="form-outer">
                                                        <div class="form-hdr">
                                                            <h3>Refund Charges</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Amount to be refunded</th>
                                                                            <th scope="col">Frequency Name</th>
                                                                            <th scope="col">Amount</th>
                                                                            <th scope="col">Days Remaining</th>
                                                                            <th scope="col">Charge Date</th>
                                                                            <th scope="col">Apply</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>

                                                                            <td colspan='9'>
                                                                                <div align='center' style='padding:6px'>No Record found</div>
                                                                            </td>

                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Form Outer Ends-->

                                                    <div class="btn-outer text-right">
                                                        <button class="blue-btn saveContinueBtn">Save and Continue</button>
                                                        <!--<input type="submit" value="Save and Continue" class="blue-btn saveContinueBtn">-->
                                                        <button class="grey-btn">Cancel</button>
                                                    </div>


                                                </div>
                                                <!-- Third Tab Ends-->

                                                <div role="tabpanel" class="tab-pane" id="moveout-four">
                                                    <form id="noticeBalanceTab">
                                                        <input type="hidden" id="moveout-tenantId" value="<?php echo $_GET['id'];?>">
                                                        <div class="form-outer">
                                                        <div class="form-hdr">
                                                            <h3>Unpaid Charges</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="property-status">
                                                                <input type="button" class="blue-btn addNew-charges" value="Add New Charge" />
                                                                <a href="javascript:;"><input type="button" class="grey-btn reset-btn" value="Reset Allocation"></a>
                                                            </div>
                                                            <div class="grid-outer">
                                                                <div class="table-responsive balance-sheet-charge">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Date</th>
                                                                            <th scope="col">Description</th>
                                                                            <th scope="col">Charge Code</th>
                                                                            <th scope="col">Original Amount(USh)</th>
                                                                            <th scope="col">Amount Paid(USh)</th>
                                                                            <th scope="col">Amount Due(USh)</th>
                                                                            <th scope="col">Amount Waived Off(USh)</th>
                                                                            <th scope="col">Waive Off Amount(USh)</th>
                                                                            <th scope="col">Comments</th>
                                                                            <th scope="col">Write off Amount(USh)</th>
                                                                            <th scope="col">Bad Debt(USh)</th>
                                                                            <th scope="col">Current Payment(USh)</th>
                                                                            <th scope="col">Allocated</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
																		<tr>
																			<td>Nov 14, 2018 (Wed.)</td>
																			<td>NSF Charge</td>
																			<td>NSF</td>
																			<td>US $100.00</td>
																			<td>US $0.00</td>
																			<td>US $21.00</td>
																			<td>US $0.00</td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><textarea maxlength="100" class="form-control" value="" style="resize:none;"></textarea></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><select class="form-control">
																					<option value="Select">Select</option>
																					<option value="Apply">Apply</option>
																				</select>
																			</td>
																		</tr>
																		
																		<tr>
																			<td>Nov 30, 2018 (Fri.)</td>
																			<td>A/C</td>
																			<td>A/C</td>
																			<td>US $600.00</td>
																			<td>US $0.00</td>
																			<td>US $123.00</td>
																			<td>US $0.00</td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><textarea maxlength="100" class="form-control" value="" style="resize:none;"></textarea></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><select class="form-control">
																					<option value="Select">Select</option>
																					<option value="Apply">Apply</option>
																				</select>
																			</td>
																		</tr>
																		<tr>
																			<td>Dec 31, 2018 (Mon.)</td>
																			<td>A/C</td>
																			<td>A/C</td>
																			<td>US $600.00</td>
																			<td>US $0.00</td>
																			<td>US $123.00</td>
																			<td>US $0.00</td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><textarea maxlength="100" class="form-control" value="" style="resize:none;"></textarea></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><select class="form-control">
																					<option value="Select">Select</option>
																					<option value="Apply">Apply</option>
																				</select>
																			</td>
																		</tr>
																		<tr>
																			<td>Jan 31, 2019 (Thu.)</td>
																			<td>A/C</td>
																			<td>A/C</td>
																			<td>US $600.00</td>
																			<td>US $0.00</td>
																			<td>US $600.00</td>
																			<td>US $0.00</td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><textarea maxlength="100" class="form-control" value="" style="resize:none;"></textarea></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><select class="form-control">
																					<option value="Select">Select</option>
																					<option value="Apply">Apply</option>
																				</select>
																			</td>
																		</tr>
																		<tr>
																			<td>Feb 18, 2019 (Mon.)</td>
																			<td>Internet/WI-FI</td>
																			<td>Internet/WI-FI</td>
																			<td>US $12.00</td>
																			<td>US $0.00</td>
																			<td>US $12.00</td>
																			<td>US $0.00</td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><textarea maxlength="100" class="form-control" value="" style="resize:none;"></textarea></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><input type="text" id="" class="form-control" value="0.00"></td>
																			<td><select  class="form-control">
																					<option value="Select">Select</option>
																					<option value="Apply">Apply</option>
																				</select>
																			</td>
																		</tr>
																		
																		
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                        <!-- Form Outer Ends-->

                                                        <div class="form-outer">
                                                        <div class="form-hdr">
                                                            <h3>Credit / Deposits</h3>
                                                        </div>
                                                        <div class="form-data">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive ">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Charge Code</th>
                                                                            <th scope="col">Description</th>
                                                                            <th scope="col">Type</th>
                                                                            <th scope="col">Org.Amount(USh)</th>
                                                                            <th scope="col">Remaining Balance(USh)</th>
                                                                            <th scope="col">Applied Amount(USh)</th>
                                                                            <th scope="col">Refund Amount(USh)</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
																				<tr><td colspan="7" class="text-center">No records to view</td></tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="btn-outer">
<!--                                                                <button class="blue-btn">Save</button>-->
                                                                <input type="submit" class="blue-btn submit" value="Save">
                                                            </div>

                                                        </div>
                                                    </div>
                                                        <!-- Form Outer Ends-->

                                                        <div class="form-outer">
                                                            <div class="form-hdr">
                                                                <h3>Summary</h3>
                                                            </div>
                                                            <div class="form-data">
                                                                <div class="grid-outer">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-hover table-dark">
                                                                            <thead>
                                                                            <tr>
                                                                                <th scope="col"></th>
                                                                                <th scope="col">Charges</th>
                                                                                <th scope="col">Deposits(USh)</th>
                                                                                <th scope="col">Credits(USh)</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td style="font-weight:bold;">Total Charges, Credits & Deposits</td>
                                                                                <td>US $0.00</td>
                                                                                <td>US $0.00</td>
                                                                                <td>US $0.00</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="font-weight:bold;">Credit Applied</td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td>US $0.00</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="font-weight:bold;">Deposits Applied</td>
                                                                                <td></td>
                                                                                <td>US $0.00</td>
                                                                                <td></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="font-weight:bold;">Adjustment / Write-Offs</td>
                                                                                <td>US $0.00</td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="font-weight:bold;">Amount to be Refunded</td>
                                                                                <td>US $0.00</td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="font-weight:bold;">Amount Due</td>
                                                                                <td>US $0.00</td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Form Outer Ends-->


                                                        <!--<div class="form-outer">
                                                            <div class="form-hdr">
                                                                <h3>Credit Refund</h3>
                                                            </div>
                                                            <div class="form-data">
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                        <label>Pay Refund From Account</label>
                                                                        <select class=" form-control"><option>Select</option></select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                        <label>Check No</label>
                                                                        <input class="form-control" type="text">
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                        <label>Amount (USh)</label>
                                                                        <input disabled="" class="form-control" type="text">
                                                                    </div>
                                                                </div>
                                                                <div class="btn-outer">
                                                                    <button class="blue-btn">Generate Refund Check</button>
                                                                </div>

                                                            </div>
                                                        </div>-->
                                                        <!-- Form Outer Ends-->

                                                    </form>
                                                </div>
                                                <!-- Forth Tab Ends-->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Accordian Ends -->
                    </div>
                </div>
            </div>
            <!--tab Ends -->
        </section>
    </main>
</div>

<!--Model End Forth tab Balance-->
<div class="container">
    <div class="modal fade" id="balanceModal" role="dialog">
        <div class="modal-dialog modal-md unit-new-renovation-form">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Charges</h4>
                </div>

                <div class="modal-body">
                    <div class="form-outer col-sm-12">
                        <form id="new_chargeCode_form">
                            <!-- <input type ="hidden" id="chargeCode_userId" name="chargeCode_userId" value="<?php /*echo $_REQUEST["id"]*/?>"  />-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-6">
                                    <label>Select Charge Code <em class="red-star">*</em>
                                    </label>
                                    <select class="form-control tax_chargeCode" name="tax_chargeCode">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                                <div class="col-sm-3 col-md-6">
                                    <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>
                                    <label>Amount (<?php echo $default_symbol ?>)<em class="red-star">*</em></label>
                                    <input class="form-control"
                                           type="text"
                                           id="amount" name="amount"
                                           maxlength="5">
                                    <span id="amount" class="amount error red-star"></span>
                                </div>

                                <div class="textarea-form">
                                    <div class="col-sm-7"><label>Description </label>
                                        <textarea class="form-control"  id="new_chargeCode_description" name="new_chargeCode_description"></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="btn-outer text-right">
                                <input type="submit" class="blue-btn" id='saveChargeCodeField' value="Save"/>
                                <button type="button" class="clear-btn ClearChargeForm" >Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Model End Forth tab Balance-->
<div class="container">
<div class="modal fade" id="apply-credit-modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content pull-left" style="width: 100%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Apply Credit/Deposit Amounts to</h4>
            </div>
            <div class="modal-body">
                <form id="moveOutCredit">
                    <input type="hidden" id="moveoutId" value="<?php echo $_GET['id'] ;?>">
                    <input type="hidden" name='waiveOfAmount' id="waiveOfAmount" value="">
                    <input type="hidden" name='waiveOfComment' id="waiveOfComment" value="">
                    <input type="hidden" name='writeOfAmount' id="writeOfAmount" value="">
                    <input type="hidden" name='badDebtOfAmount' id="badDebtOfAmount" value="">
                    <input type="hidden" name='appliedAmountHidden' id="appliedAmountHidden" value="">
                    <input type="hidden" name='finalCreditGet' id="finalCreditGet" value="0">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-6 mrg-btm-20">
                                <label>Selected Charge Remaining Balance <em class="red-star">*</em>
                                </label>
                                <input type="text" name='txtRemainingChargeAmount'  class="form-control" id="txtRemainingChargeAmount" readonly="readonly" value="">
                            </div>

                            <div class="col-md-12">

                                <div class="form-outer">
                                    <div class="form-hdr">
                                        Credits / Deposits
                                    </div>
                                    <div class="form-data" style="">
                                            <div class="table-responsive grid-outer" id="tbl-credit-data" style="">
                                            </div>

                                    </div>
                                </div>
                            </div>



                        <div class="col-sm-12 btn-outer mg-btm-20 text-right">
                            <input type="button" id="btnRemainingcredit" class="blue-btn" value="Save">
                            <input type="button" class="grey-btn"  data-dismiss="modal" value="Cancel">
                        </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Starts -->
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<!--<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>-->



<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/moveOutTenant.js"></script>






<script>
    $('#people_top').addClass('active');
    $('.company-top').addClass('active');
    $('.cropItData').hide();


  /*  $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });*/
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(".calander").val(date);


    $('.calander1').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('.calander1').datepicker({dateFormat: jsDateFomat});
    var date = $.datepicker.formatDate(jsDateFomat, new Date());

    $(".calander1").val(date);

    $(document).on('click','.ResetForm',function () {
        bootbox.confirm("Do you want to reset this form?", function (result) {
            if (result == true) {
                $('#addForwordingAddress_Moveout')[0].reset();
            }
        });
    });

    $(document).on('click','.ClearReaasonForm',function () {
        $('.reason_ForLeaving').val('');
    });

    $(document).on('click','.ClearChargeForm',function () {
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
              $('#new_chargeCode_form')[0].reset();
            }
        });
    });

</script>

<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<?php include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php"); ?>

