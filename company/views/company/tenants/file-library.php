<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if ((!isset($_SESSION[SESSION_DOMAIN]['Tenant_Portal']['tenant_portal_id'])) && (!isset($_SESSION['Admin_Access']['tenant_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/TenantPortal/login');
}




?>
<style>
  .bootstrap-tagsinput { width: 100%; }
  #ccrecepents .modal-content, #ccrecepents .modal-body  { width: 100%; }
  #sendMailModal .modal-body {
    max-height: 400px;
    overflow: auto;
  }
   #sendMailModal .modal-body .note-editor { width: 100% !important; }
</style>

<div class="popup-bg"></div>
<div class="container">
  <form id="sendEmail">
    <div class="modal fade" id="sendMailModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Manager</h4>
                </div>
                <div class="modal-body">
                    <!-- <div class="form-outer" style="float: none;">
                        <label>To <em class="red-star">*</em></label>
                        <input class="form-control to" name="to" type="text" data-role="tagsinput" /><span class=""><a href="#" class="addToRecepent">Add Recepent</a></span>

                           <label>CC <em class="red-star">*</em></label>
                        <input class="form-control cc" name="cc" type="text" />

                           <label>BCC </label>
                        <input class="form-control bcc" name="bcc" type="text" />

                           <label>Subject <em class="red-star">*</em></label>
                        <input class="form-control subject" name="subject" type="text" />

                         <label>Body <em class="red-star">*</em></label>
                        <textarea class="form-control body" name="body"></textarea>

                        <div class="attachmentFile"></div>
                      
                     
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                          <button class="blue-btn" value="Send">Send</button>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div> -->

                       <div class="row">
                           <div class="col-sm-1">
                              <button class="blue-btn compose-email-btn">Send</button>
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>To <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control to" name="to" type="text"/></span></div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                                <a class="add-recipient-link addToRecepent" href="#"> Add Recipients</a>
                            </div>
                       </div>

                       <div class="row">
                           <div class="col-sm-1">
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Cc</div>
                                <div class="col-sm-10"><span><input class="form-control cc" name="cc" type="text"/></span></div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                                <a class="add-recipient-link addCcRecepent" href="#"> Add Recipients</a>
                            </div>
                       </div>

                       <div class="row">
                           <div class="col-sm-1">
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Bcc </label></div>
                                <div class="col-sm-10"><span><input class="form-control bcc" name="bcc" type="text"/></span></div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                                <a class="add-recipient-link addBccRecepent" href="#"> Add Recipients</a>
                            </div>
                       </div>

                       <div class="row">
                           <div class="col-sm-1">
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Subject <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control subject" name="subject" type="text"/></span></div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                            </div>
                       </div>

                       <div class="row">
                           <div class="col-sm-1">
                           </div>
                           <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Body <em class="red-star">*</em></label></div>
                                <div class="col-sm-10">
                                    <span><textarea class="form-control summernote" name="body"></textarea></span>
                                    <div class="btn-outer">
                                        <button class="blue-btn">Send</button>
                                
                                    </div>
                                </div>
                            </div>
                           </div>
                           <div class="col-sm-3">
                            </div>
                       </div> 


                       <div class="attachmentFile"></div>


                </div>
            </div>
        </div>

    </div>
  </form>





  <div class="container">
    <div class="modal fade" id="torecepents" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                   <div class="form-outer" style="float: none;">
                     <label>Select <em class="red-star">*</em></label>
                       <select class="form-control selectUsers">
                        <option value=""></option>
                        <option value="tenant">Tenant</option>
                        <option value="owner">Owner</option>
                        <option value="vendor">Vendor</option>
                        <option value="other_contacts">Other Contacts</option>
                        <option value="guestCard">Guest Card</option>
                        <option value="rental_application">Rental Application</option>
                        <option value="employee">Employee</option>   
                       </select>

                        <div class="userDetails"></div>
                      
                     
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                          <button class="blue-btn" value="Send">Send</button>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>



<div class="container">
    <div class="modal fade" id="ccrecepents" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content pull-left" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body pull-left">
                   <div class="form-outer" style="float: none;">
                    <div class="row">
                    <div class="col-sm-6">
                     <label>Select <em class="red-star">*</em></label>
                       <select class="form-control selectCcUsers">
                        <option value=""></option>
                        <option value="tenant">Tenant</option>
                        <option value="owner">Owner</option>
                        <option value="vendor">Vendor</option>
                        <option value="other_contacts">Other Contacts</option>
                        <option value="guestCard">Guest Card</option>
                        <option value="rental_application">Rental Application</option>
                        <option value="employee">Employee</option>   
                       </select>
                     </div>
                   </div>

                        <div class="userCcDetails grid-outer"></div>
                      
                     <div class="row">
                        <div class="col-sm-12">
                          <button class="blue-btn" value="Send">Send</button>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>


  <div class="container">
    <div class="modal fade" id="bccrecepents" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                   <div class="form-outer" style="float: none;">
                     <label>Select <em class="red-star">*</em></label>
                       <select class="form-control selectBccUsers">
                        <option value=""></option>
                        <option value="tenant">Tenant</option>
                        <option value="owner">Owner</option>
                        <option value="vendor">Vendor</option>
                        <option value="other_contacts">Other Contacts</option>
                        <option value="guestCard">Guest Card</option>
                        <option value="rental_application">Rental Application</option>
                        <option value="employee">Employee</option>   
                       </select>

                        <div class="userBccDetails"></div>
                      
                     
                        <div class="col-sm-12" style="float: none;min-height: 35px;padding: 10px 1px;">
                          <button class="blue-btn" value="Send">Send</button>
                            <a class="grey-btn" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>





  <style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>


</div>
<input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_top_navigation.php");
?>

<div id="wrapper">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php"); ?>

 <?php 
    if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id'])) {
     if((!isset($_SESSION['tenant_id'])))
    {
      $localUrl = localUrl;
     header("Location: $localUrl/TenantPortal/login");
    }

   }

?>
    <section class="main-content">
            <div role="tabpanel" class="tab-pane" id="tenant-detail-eight">
                                            <div class="col-sm-12 files_main">
                                                <div class="form-outer">
                                                    <div class="form-hdr">
                                                        <h3>
                                                            File Library
                                                        </h3>
                                                    </div>
                                                    <div class="form-data">
                                                        <div class="apx-table">
                                                            <form id="addChargeNote">

                                                                <div id="collapseSeventeen" class="panel-collapse in">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-4 min-height-0 file_main">
                                                                                <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                                                <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">

                                                                                <input type="button" class="orange-btn" id="remove_library_file" value="Remove All Files...">
                                                                                <input type="button" class="saveChargeFile blue-btn" value="Save">
                                                                                <input type="button" class="cancelbtn grey-btn" value="Cancel">
                                                                            </div>
                                                                            <div class="row" id="file_library_uploads">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="TenantFiles-table" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <!-- Form Outer Ends -->
                                                            </form>
                                                            <div class="table-responsive">

                                                                <table id="TenantFiles-table" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
     
    </section>

  <div id="imageModel" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
         
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>



     




    <!-- Jquery Starts -->
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['Tenant_Portal']['datepicker_format']; ?>";</script>
    <script>var staticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">';</script>
    <script>var petStaticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">';</script>
    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
   <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/intlTelInput.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantportal.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
    <script>
      $(document).ready(function(){
        $('.summernote').summernote({
           addclass: {
               debug: false,
               classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
           },
           width: '80%',
           height: '300px',
           //margin-left: '15px',
           toolbar: [
               // [groupName, [list of button]]
               ['img', ['picture']],
               ['style', ['style', 'addclass', 'clear']],
               ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
               ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
               ['extra', ['video', 'table', 'height']],
               ['misc', ['undo', 'redo', 'codeview', 'help']]
           ]
       });
      });
    </script>
     <script>var upload_url = "<?php echo SITE_URL; ?>"; </script>

</div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script>

  $(".to").tagsinput('items');
   $(".cc").tagsinput('items')
    $(".bcc").tagsinput('items')


 </script>