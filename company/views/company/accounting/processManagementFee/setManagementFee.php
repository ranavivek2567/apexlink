<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Process Management Fee</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <!--- Right Quick Links ---->
                        <!--                        --><?php //include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                        <!--- Right Quick Links ---->

                        <div class="form-outer">
                            <div class="form-hdr">
                                <h3>Process Management Fees</h3>
                            </div>

                            <div class="form-data">
                                <div class="property-status mg-tp-40">
                                    <div class="row">
                                        <div class="btn-outer text-right mg-lt">
                                            <a type="button" class="blue-btn blue-btn-alt" href="/Accounting/PaidManagementFee">Paid Management Fees</a>
                                            <a type="button" class="blue-btn blue-btn-alt" href="/Accounting/RunManagementFee">Run Management Fees</a>
                                            <a type="button" class="blue-btn" href="/Accounting/ProcessManagementFee">Set Management Fees</a>
                                        </div>
                                    </div>
                                </div>

                                <div id="update_process_management_div" style="display: none">
                                    <form id="update_process_management_form_id">
                                        <div class="col-sm-12 outer-border">
                                            <div class="mg-tp-20">
                                                <div class="col-sm-3">
                                                    <label>Property <em class="red-star">*</em></label>
                                                    <select class="form-control" name="property_id" id="property_id" multiple>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Type <em class="red-star">*</em></label>
                                                    <select class="form-control" name="management_type" id="management_type" >
                                                        <option value="Flat">Flat</option>
                                                        <option value="Percent">Percent</option>
                                                        <option value="Percentage and a min. set fee">Percentage and a min. set fee</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3" id="flat_value_div">
                                                    <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                    <label id="value_type">Value(<?php echo $default_symbol; ?>) <em class="red-star">*</em></label>
                                                    <input class="form-control number_only amount_class" type="text" name="management_value_flat" id="management_value_flat">
                                                </div>
                                                <div class="col-sm-3" id="percent_value_div" style="display: none">
                                                    <label id="value_type">Value(%) <em class="red-star">*</em></label>
                                                    <input class="form-control number_only " type="text" name="management_value_percent" id="management_value_percent">
                                                </div>
                                                <div class="col-sm-3" id="minimum_fee" style="display: none">
                                                    <label>Minimum <em class="red-star">*</em></label>
                                                    <input class="form-control number_only amount_class" type="text" name="minimum_management_fee" id="minimum_management_fee">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row col-sm-12 mg-tp-20">
                                            <button type="button" id="update_btn_id" class="blue-btn" >Update</button>
                                            <button type="button" class="clear-btn">Clear</button>
                                            <button type="button" class="grey-btn">Cancel</button>
                                        </div>
                                    </form>
                                </div>

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                    <div class="grid-outer">
                                                        <div class="table-responsive">
                                                            <div class="apx-table listinggridDiv">
                                                                <div class="table-responsive">
                                                                    <table id="list_process_management_table" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="btn-outer text-right mg-tp-20">
                                    <button type="button" class="blue-btn" id="update_process_management_fee">Update</button>
                                </div>

                            </div>
                        </div>
                        <!-- Form Outer Ends -->

                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<!-- Wrapper Ends -->



<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->

<script type="text/javascript">
    $('#accounting_top').addClass('active');
    var upload_url = "<?php echo SITE_URL; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/processManagementFee/setManagementFee.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.easyui.css" rel="stylesheet">
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>

<script>

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });

</script>

<!-- Jquery Ends -->
</body>

</html>