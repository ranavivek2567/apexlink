<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <style>
        .ui-datepicker table,  .ui-datepicker-current{
            display: none;
        }
    </style>
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Process Management Fee</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <!--- Right Quick Links ---->
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                        <!--- Right Quick Links ---->
                        <!--                        <form id="addOwner" enctype='multipart/form-data'>-->
                        <div class="form-outer">
                            <div class="form-hdr">
                                <h3>Process Management Fees</h3>
                            </div>
                            <div class="form-data">
                                <div class="property-status mg-tp-40">
                                    <div class="row">
                                        <div class="btn-outer text-right mg-lt ">
                                            <a type="button" class="blue-btn blue-btn-alt" href="/Accounting/PaidManagementFee">Paid Management Fees</a>
                                            <a type="button" class="blue-btn" href="/Accounting/RunManagementFee">Run Management Fees</a>
                                            <a type="button" class="blue-btn blue-btn-alt" href="/Accounting/ProcessManagementFee">Set Management Fees</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 outer-border">
                                    <div class="col-sm-7 mg-tp-20"></div>
                                    <div class="col-sm-3 text-right mg-tp-20">
                                        <input class="form-control" type="text"  name="process_management_fee_date" id="process_management_fee_date">
                                    </div>
                                    <div class="col-sm-2 text-right mg-tp-20">
                                        <button type="button" class="blue-btn" id="run_management_fee_btn">Run Management Fees</button>
                                    </div>
                                    <div id="showTable" style="display: none;">
                                        <div class="col-sm-12">
                                            <div class="grid-outer" style="overflow: auto!important; max-height: 400px!important; ">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-dark" id="list_run_management_table" >
                                                        <thead>
                                                        <tr>
                                                            <th scope="col"><input id="select_all_properties_checkbox" type="checkbox"></th>
                                                            <th scope="col">Property Name</th>
                                                            <th scope="col">Commission Type</th>
                                                            <th scope="col">Value</th>
                                                            <th scope="col">Minimum</th>
                                                            <th scope="col">Income</th>
                                                            <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                            <th scope="col">Process Management Fee (<?php echo $default_symbol ?>)</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id = "properties_listing">
                                                        <tr>
                                                            <td>
                                                                <span id = "edit_utility_type"></span>
                                                            </td>
                                                            <td>
                                                                <span id = "edit_frequency"></span>
                                                            </td>
                                                            <td>
                                                                <input style="display: none;"  class="form-control start_date calander" readonly id="start_date" name="start_date" placeholder="" type="text">
                                                            </td>
                                                            <td>
                                                                <input style="display: none;"  class="form-control end_date calander" readonly id="end_date" name="end_date" placeholder="" type="text">
                                                            </td>
                                                            <td>
                                                                <input style="display: none;"  data_required="true" class="form-control add-input utility_amount" id="edit_utility_amount" name="edit_utility_amount" autocomplete="off" placeholder="" type="text">
                                                            </td>
                                                            <td>
                                                                <span id = "edit_frequency"></span>
                                                            </td>
                                                            <td>
                                                                <span id = "edit_frequency"></span>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 mg-tp-20">
                                            <input name="process_amount" id="process_amount" type="text" class="form-control amount_class number_only" placeholder="Enter Process Amount">
                                        </div>
                                        <div class="col-sm-9 mg-tp-20 text-right total_management_fee_span">
                                            <span>Total Process Management Fees : </span>
                                            <span class=""><?php echo $default_symbol?></span><span id="total_management_fee_amount" class="total_management_fee_amount">0.00</span>
                                        </div>
                                        <div style="text-align: right; margin-bottom: 10px;" >
                                            <button type="button" class="blue-btn" id="refresh_btn">Refresh</button>
                                            <button type="button" class="blue-btn" id="save-run-management-btn">Save</button>
                                        </div>
                                    </div>
                                </div>
                                <div id="showTotal" style="display: none">
                                    <div class="col-sm-4  mg-tp-20 total_management_fee_span">
                                        <span>Total Process Management Fees : </span>
                                        <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                        <span class=""><?php echo $default_symbol?></span><span class="total_management_fee_amount">0.00</span>
                                    </div>
                                    <div class="col-sm-8 text-right mg-tp-20">
                                        <button type="button" class="blue-btn" id="payProcessManagementFeeBtnId">Pay to Process Management Fees</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Form Outer Ends -->
                        <!--                        </form>-->
                    </div>
                </div>
            </div>

            <!--Pay Process Management Fee Modal-->
            <div class="container">
                <div class="modal fade" id="payProcessManagementFeeModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay Process Management Fee</h4>
                            </div>
                            <div class="modal-body pad-none">
                                <form method="post" id="addPropertyForm">
                                    <input type="hidden" class="propertyIddata" value="">
                                    <div class="panel-body" style="border-color: transparent;">
                                        <div class="form-outer">

                                            <div class="col-sm-12" style="text-align: center">
                                                <h3 class="process_head mg-tp-10" >
                                                    <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                    Total Management Fee = <span><?php echo $default_symbol ?></span><span id="spnTotalAmtPay">AFN 1635</span>
                                                    <input type="hidden" id="hdnPayPMCTotalAmt" spellcheck="true" autocomplete="off" value="1635">
                                                </h3>
                                                <div class="form-data-range" >
                                                    <label>Pay</label>
                                                    <span class="mg-tp-10">
                                                        <select class="form-control" name="vendor_id" id="vendor_id"></select>
                                                        <span class="error" id="vendor_id_error"></span>
                                                    </span>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-data-range">
                                                    <span>
<!--                                                        <textarea style="resize: none;" class="form-control" rows="4" cols="10"disabled="disabled" placeholder="Address"></textarea>-->
                                                        <div class="address-textarea" placeholder="Address">
                                                            <span class="vendor-name">Address</span>
                                                            <span class="address1"></span>
                                                            <span class="address2"></span>
                                                            <span class="address3"></span>
                                                            <span class="address4"></span>
                                                            <span class="city-state"></span>
                                                        </div>
                                                            <a id="ancChangePMCAddress" style="display: none">Edit Address</a>
                                                    </span>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-data-range">
                                                    <label >Payment Type</label>
                                                    <span class="payment-type-span">
                                                        <div class="radio-container">
                                                            <input type="radio" class="radio" name="payment_type" id="rdbCheck" checked="checked" value="Check" style="cursor: pointer;" >
                                                            <div class="radio-text" style="color: #585858;">Check</div>
                                                        </div>
                                                        <div class="radio-container">
                                                            <input type="radio" class="radio" id="rdbACH" name="payment_type" style="cursor: pointer;"  value="ACH">
                                                            <div class="radio-text" style="color: #585858;">ACH</div>
                                                        </div>
                                                        <div class="radio-container">
                                                            <input type="radio" class="radio" id="rdbACH" name="payment_type" style="cursor: pointer;"  value="Credit Card/Debit Card">
                                                            <div class="radio-text" style="color: #585858;">Credit Card/ Debit Card</div>
                                                        </div>
                                                    </span>
                                                </div>
                                                <div class="grid-outer" style="overflow: auto!important; max-height: 400px!important; ">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-dark" id="list_bank_table" >
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Bank</th>
                                                                <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                                <th scope="col">PMC (<?php echo $default_symbol ?>)</th>
                                                                <th scope="col">Check #</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class ="bank_listing" id="bank_with_property_listing">
                                                            <tr>
                                                                <td>
                                                                    <span id = "edit_utility_type"></span>
                                                                </td>
                                                                <td>
                                                                    <span id = "edit_frequency"></span>
                                                                </td>
                                                                <td>
                                                                    <span id = "edit_frequency"></span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-outer text-right mg-btm-20">
                                            <button type="button" class="blue-btn" value="Save" id="pay_run_management_fee_btn">Pay</button>
                                            <button type="button" id="" class="grey-btn">Print</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Pay Process Management Fee Modal-->

            <div class="container">
                <div class="modal fade" id="editVendorAddressModal" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content" style="width: 100%;">
                            <div class="modal-header">
                                <button type="button" class="close" id="crossEditPMCAddressForm">&times;</button>
                                <h4 class="modal-title">PMC Address</h4>
                            </div>
                            <div class="modal-body pad-none">
                                <div class="panel-body" style="border-color: transparent;">
                                    <form  id="updatePMCAddressFormId">
                                        <div class="form-outer">
                                            <input type="hidden" name="user_id" id="user_id">
                                            <div class="col-sm-12 mg-tp-10">
                                                <div class="col-sm-4">
                                                    <label>Zip / Postal Code</label>
                                                    <input name="zipcode" class="form-control zipcode" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Country</label>
                                                    <input name="country" class="form-control country" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>State / Province</label>
                                                    <input name="state" class="form-control state" type="text">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-4">
                                                    <label>City</label>
                                                    <input name="city" class="form-control city" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Address 1</label>
                                                    <input name="address1" class="form-control address1" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Address 2</label>
                                                    <input name="address2" class="form-control address2" type="text">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-4">
                                                    <label>Address 3</label>
                                                    <input name="address3" class="form-control address3" type="text">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Address 4</label>
                                                    <input name="address4" class="form-control address4" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-right mg-btm-20">
                                            <button type="submit" class="blue-btn" value="Save" id="updatePMCAddressBtn">Update</button>
                                            <button type="button" id="resetEditPMCAddressForm" class="clear-btn">Reset</button>
                                            <button type="button" id="cancelEditPMCAddressForm" class="grey-btn" >Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
<!-- Wrapper Ends -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->
<script type="text/javascript">
    $('#accounting_top').addClass('active');
    var upload_url = "<?php echo SITE_URL; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/processManagementFee/runManagementFee.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.easyui.css" rel="stylesheet">
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
</script>
<!-- Jquery Ends -->
</body>
</html>