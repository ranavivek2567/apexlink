<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting &gt;&gt; <span>Recurring Checks </span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data">
                        <!--- Right Quick Links ---->
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                        <!--- Right Quick Links ---->
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                                <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                                <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                                <li role="presentation" class="active"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                                <li role="presentation" ><a href="/Accounting/BankRegister">Banking</a></li>
                                <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                                <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                                <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>
                                <li role="presentation"><a href="/EFTPayments/EFTTenant">EFT</a></li>
                                <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                                <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                                <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                                <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                            </ul>

                            <!-- Tab panes -->

                            <div class="tab-content">
                                <div class="panel-heading">

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="receivables">

                                            <div class="form-hdr" style="margin-bottom: 20px;">
                                                <h3 class="d-inline-block">Recurring Transactions</h3>
                                                <!--<button class="blue-btn">Receive Payment</button>-->
                                                <div class="pull-right">
                                                    <a href="/Accounting/RecurringBill"><input type="button" class="blue-btn btn-pad-normal blue-btn-alt" value="Recurring Bills"></a>
                                                    <a href="/Accounting/RecurringInvoices"><input type="button" class="blue-btn btn-pad-normal blue-btn-alt" value="Recurring Invoices"></a>
                                                    <a href="/Accounting/RecurringChecks"><input type="button" class="blue-btn btn-pad-normal " value="Recurring Checks"></a>
                                                </div>
                                            </div>
                                            <div class="">
                                            <div class="form-hdr" >
                                                <h3 class="d-inline-block">Recurring Checks</h3>
                                            </div>
                                            <div class="form-data">
                                                <form id="createCheckForm" name="createCheckForm">
                                                    <div class="row mb-15">

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Select Property <em class="red-star">*</em>

                                                            </label>
                                                            <select class="form-control" id="property" name="property_id">
                                                                <option value="">Select</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Select Building <em class="red-star">*</em>

                                                            </label>
                                                            <select class="form-control" id="building" name="building_id">
                                                                <option value="">Select</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Select Unit <em class="red-star">*</em>

                                                            </label>
                                                            <select class="form-control" id="unit" name="unit_id">
                                                                <option value="">Select</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Select Bank
                                                            </label>
                                                            <select class="form-control" id="selectBank" name="bank_id">
                                                                <option value="">Select</option>
                                                                <option value="1">Test</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>





                                                    <div class="row mb-15">

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Ending Balance <em class="red-star">*</em></label>
                                                            <!-- </div>-->
                                                            <!-- <div class="col-sm-3">-->
                                                            <input type="text" name="endingbalance" class="form-control endingbalance">
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label> Frequency <em class="red-star">*</em></label>
                                                            <span><select name="frequency" class="form-control" id="frequency"  type="text">
                                                                                            <option value="1">Weekly</option>
                                                                                            <option value="2">Bi-Weekly</option>
                                                                                            <option value="3" blue-btn-alt>Monthly</option>
                                                                                            <option value="4">Annually</option>
                                                                                            <option value="5">Quartely</option>
                                                                                            <option value="6">Semi-Annually</option>
                                                                                            <option value="7">Daily</option>
                                                                                            <option value="8">Bi-Monthly</option>
                                                                                        </select></span> </div><div class="col-sm-3 col-md-3 ">
                                                            <label>Duration <em class="red-star">*</em></label>
                                                            <!-- </div>-->
                                                            <!-- <div class="col-sm-3">-->
                                                            <input type="text" name="duration" class="form-control duration number_only" placeholder="Add Number of Occurrences">
                                                        </div>

                                                    </div>
                                                    <div class="bg-light-blue col-md-10" style="background-color: #F3FFF1;">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <input type="radio" name="userType" value="3" class="selectUser" checked>Vendor
                                                                    <input type="radio" name="userType" value="4" class="selectUser">Owner
                                                                    <input type="radio" name="userType" value="2" class="selectUser">Tenant
                                                                    <input type="radio" name="userType" value="0" class="selectUser">Other

                                                                </div>
                                                                <div class="payonline-form-field">
                                                                    <div class="col-sm-3">
                                                                        <label class="text-center" style="margin-top: 8px;"> <em class="red-star">*</em>
                                                                            Pay To The Order Of
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="orderOff">
                                                                            <select name='payOrderOff' class="form-control payOrderOff" >
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">&nbsp;</div>
                                                                    <div class="col-sm-3">
                                                                        <div style="margin-bottom: 15px;">
                                                                            <label>Date <em class="red-star">*</em>
                                                                            </label>
                                                                            <!-- </div>
                                                                             <div class="col-sm-3">-->
                                                                            <input type="text" name="Date" id="date" class="form-control calander">
                                                                        </div>
                                                                        <div>
                                                                            <label><?php echo $default_symbol; ?><em class="red-star">*</em>
                                                                            </label>
                                                                            <!-- </div>
                                                                             <div class="col-sm-3">-->
                                                                            <input type="text" name="afn" class="form-control afn number_only">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="clearfix"></div>

                                                        <p class="amount-word-text"> Amount in Words </p>
                                                        </label>
                                                        <div class="row">
                                                            <div class="col-sm-6"><div class="col-sm-2">Address</div>
                                                                <div class="col-sm-6">
                                                                    <textarea class="address form-control" name="address" ></textarea>
                                                                    <a class="pull-right" id="EditVendorAddress"  data_id="" style="display: none;">Edit Address</a>
                                                                </div></div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-sm-6"><div class="col-sm-2">Memo</div>
                                                                <div class="col-sm-6">
                                                                    <input type="text" name="memo" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12">
<!--                                                        <div class="text-left d-flex align-center"  style="margin-top: 15px;">-->
                                                           <!-- <div class="w-100">

                                                            </div>-->
                                                            <div class=" w-100 text-right">
                                                                <input type='submit' value="Save" class="blue-btn ">
                                                                <input type='button'  id="clear" value="Clear" class="clear-btn" >
                                                                <input type='button' id="cancel_frm" value="Cancel" class="clear-btn" >
                                                            </div>
<!--                                                        </div>-->

                                                    </div>
                                                </form>
                                            </div>
                                            </div>

                                        <!-- Regular Rent Ends -->

                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>



                            <!-- Sub Tabs Starts-->

                            <!-- Sub tabs ends-->
                        </div>

                    </div>
                </div>

                <!--Tabs Ends -->

            </div>
    </div>
    </div>
    </section>
    </div>


    <div id="updateVendorAddress" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Vendor Address</h4>
                </div>
                <div class="modal-body">
                    <form id="update_vendor_address_form">
                        <input type="hidden" name="id" id="vendor_address_id" >
                        <div class="row">
                            <div class="form-outer">
                                <div class="col-sm-4">
                                    <label>Address 1</label>
                                    <input name="address1" placeholder="Eg: Street Address 1" maxlength="200" id="vendorAddress1" class="capital form-control address_field" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Address 2</label>
                                    <input name="address2" placeholder="Eg: Street Address 2" maxlength="200" id="vendorAddress2" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Address 3</label>
                                    <input name="address3" placeholder="Eg: Street Address 3" maxlength="200" id="vendorAddress13" class="form-control capital address_field" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>Zip / Postal Code</label>
                                    <input name="zipcode" maxlength="9" placeholder="Eg: 10001" id="vendor_zip_code" class="form-control capital" type="text" />
                                </div>
                                <!-- <div class="col-sm-4">
                                     <label>Country</label>
                                     <input name="country" maxlength="50" placeholder="Eg: US" id="vendor_country" class="form-control capital" type="text" />
                                 </div>-->
                                <div class="col-sm-4">
                                    <label>State / Province </label>
                                    <input name="state" id="vendor_state" maxlength="100" placeholder="Eg: AL"  class="form-control capital states" type="text" />
                                </div>
                                <div class="col-sm-4">
                                    <label>City</label>
                                    <input name="city" maxlength="100" placeholder="Eg: Huntsville" id="vendor_city"  class="form-control capital citys" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="blue-btn update_vendor_address_form" >Update</button>
                            <button type="button" class="grey-btn cancel_address_update" >Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/accounting/RecurringChecks/recurringChecks.js">
    </script>
    <script> var date = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script> var default_symbol = "<?php echo $default_symbol; ?>";</script>
    <script> var default_symbol = "<?php echo $default_symbol; ?>";</script>
    <!-- Wrapper Ends -->

    <!-- Footer Ends -->

    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });


            $(document).on('keypress','.numberInput',function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        });


        $(document).on("click",".print_check",function(){
            var base_url = window.location.origin;
            window.location.href = base_url+"/Accounting/checkLists";
        });



    </script>

    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>