<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$';

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid"s>
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Recurring Checks </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>ACCOUNTING</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >Receive Money</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Multi Payments</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">My Bills</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Pay Bills</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Invoices</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Write Checks</a>
                                <!-- Seven Ends-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Print Checks</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Check Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Payment Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bill Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Deposit</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Late Fees</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Draw</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Contributions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Process Management Fee</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Pending Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Credit Card Details</a>
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" ><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"  class="active"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>
                            <li role="presentation"><a href="/EFTPayments/EFTTenant">EFT</a></li>
                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="panel-heading">
                                <div class="form-hdr" style="margin-bottom: 20px;">
                                    <h3 class="d-inline-block">Recurring Transactions</h3>
                                    <!--<button class="blue-btn">Receive Payment</button>-->
                                    <div class="pull-right recurring-button">
                                        <a href="/Accounting/RecurringBill"><input type="button" class="blue-btn btn-pad-normal blue-btn-alt" value="Recurring Bills"></a>
                                        <a href="/Accounting/RecurringInvoices"><input type="button" class="blue-btn btn-pad-normal blue-btn-alt" value="Recurring Invoices"></a>
                                        <a href="/Accounting/RecurringChecks"><input type="button" class="blue-btn btn-pad-normal " value="Recurring Checks"></a>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                        <!--<div class="form-outer">-->
                                            <div class="form-hdr"  >
                                                <h3>Recurring Checks <a href="/Accounting/addRecurringChecks"><input type="button" class="blue-btn text-right pull-right btn-pad-normal blue-btn-alt" value="New Recurring Checks"></a></h3>
                                            </div>
                                           <div id="checkTable">
                                            <div class="form-data">
                                                <table id="RecurringChecksDataTable" class="table table-bordered nowrap"></table>
                                            </div>
                                        </div>
                                  <!--  </div>-->
                                    <div class="form-outer custom-ht" id="viewlisting" style="display: none;">
                                        <div class="row" >
                                            <div class="col-lg-12">
                                            <div class="form-hdr" >
                                                <h3 class="d-inline-block">Bill Information</h3>
                                            </div>


                                            <div class="form-data">
                                            <div class="col-sm-4">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Property Name :</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="propertnme"></span>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Bank :</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="bank"></span>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Bank Balance :</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="bankbal"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Date :</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="date"></span>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Frequency :</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="frequency"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Duration :</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="duration"></span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Check Amount :</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="chkamt"></span>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Amount towards :</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="amttoward"></span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12"> <div class="row">
                                                        <div class="col-xs-6">
                                                            <label>Name :</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="name"></span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label >Address:</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="address"></span>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <label >Mermo:</label>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <span class="memo"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="col-md-12 text-right">
                                                <a href="javascript:;" class="viewEdit" data-editid="">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                           <!-- <div class="col-md-12 text-right">
                                                <a href="/Accounting/EditRecurringCheck"><i class="fa fa-edit"></i> Edit</a>
                                            </div>-->
                                    </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->
                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>

                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>


<!-- Wrapper Ends -->
<!-- Footer Ends -->
<script>
    var default_currency_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var date = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/RecurringChecks/recurringChecks.js"></script>

<script>

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });


    var default_symbol = "<?php echo $default_symbol; ?>";

</script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
