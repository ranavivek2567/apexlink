<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Bank Reconciliation</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>ACCOUNTING</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >Receive Money</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Multi Payments</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">My Bills</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Pay Bills</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Invoices</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Write Checks</a>
                                <!-- Seven Ends-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Print Checks</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Check Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Payment Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bill Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Deposit</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Late Fees</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Draw</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Contributions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Process Management Fee</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Pending Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Credit Card Details</a>
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs tab_list" role="tablist">
                            <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>
                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>

                        <!-- Tab panes -->

                        <div id="add_new_reconciliation_div" style="display: none">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="vendorTab active" data_tab="" id="generalTab" class="active"><a href="#reconciliation" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Reconciliation</a></li>
                                <li role="presentation" class="vendorTab" data_tab="" id="payablesTab"><a href="#research_adjustment_tab" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Research Adjustment</a></li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="reconciliation">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Reconciliation<a class="back add_reconciliation_back_btn" ><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="blue_text">Statement Balance</div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Account <em class="red-star">*</em>
                                                            <a class="pop-add-icon" id="add_more_bank_account_icon2" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select type="text" class="form-control bank_account" name="bank_account"></select>
                                                    </div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Statement Ending Date</label>
                                                        <input type="text" readonly class="form-control statement_ending_date" name="statement_ending_date" >
                                                    </div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Beginning Balance (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input type="text" class="form-control beg_balance money" name="beg_balance">
                                                    </div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Ending Balance (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input type="text" class="form-control end_balance money" name="end_balance">
                                                    </div>
                                                </div>
                                                <div class="btn-outer mg-lt-30">
                                                    <input type="button"  id='reset_statement_balance_btn' class="blue-btn" value="Reset" />
                                                </div>
                                                <div class="col-sm-12 mt-20">
                                                    <div class="blue_text">System Balance</div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Beginning Balance (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input readonly type="text" class="form-control system_beg_balance money" name="system_beg_balance">
                                                    </div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Money In (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input readonly type="text" class="form-control money_in money" name="money_in">
                                                    </div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Money Out (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input readonly type="text" class="form-control money_out" name="money_out">
                                                    </div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Fee/Adjustments (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input readonly type="text" class="form-control fee_adjustment" name="fee_adjustment">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Interest/Adjustments (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input readonly type="text" class="form-control interest_adjustment" name="interest_adjustment">
                                                    </div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Research Adjustments (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input readonly type="text" class="form-control system_research_adjmnt" name="system_research_adjmnt">
                                                    </div>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Calculated System Balance (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input readonly type="text" class="form-control calculated_system_balance" name="calculated_system_balance">
                                                    </div>
                                                </div>
                                                <div class="" id="diffamount">
                                                    <div class="mg-lt-30 difference-text" id="diffamount">
                                                         <?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?><span class="difference">0:00 </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Form Outer Ends -->
                                    <div class="money_in_out_div">
                                        <div class="short">
                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3>Money Out (Withdrawals)<a class="pull-right adjustment_button" >Adjustment</a></h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="grid-outer" >
                                                            <div class="table-responsive">
                                                                <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                                <table class="table table-hover table-dark" id="">
                                                                    <thead>
                                                                    <tr>
                                                                        <th scope="col"></th>
                                                                        <th scope="col">Date</th>
                                                                        <th scope="col">Period</th>
                                                                        <th scope="col">Reference Number</th>
                                                                        <th scope="col">Type</th>
                                                                        <th scope="col">Description</th>
                                                                        <th scope="col">Payee</th>
                                                                        <th scope="col">Amount (<?php echo $default_symbol ?>)</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="money_out_td_div">
                                                                    <tr class="norecord"><td align="center" colspan="8">No records Found</td></tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->
                                        <div class="short">
                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3>Money In (Deposits)<a class="pull-right adjustment_button" >Adjustment</a></h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="grid-outer" >
                                                            <div class="table-responsive">
                                                                <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                                <table class="table table-hover table-dark" id="">
                                                                    <thead>
                                                                    <tr>
                                                                        <th scope="col"></th>
                                                                        <th scope="col">Date</th>
                                                                        <th scope="col">Period</th>
                                                                        <th scope="col">Reference Number</th>
                                                                        <th scope="col">Type</th>
                                                                        <th scope="col">Description</th>
                                                                        <th scope="col">Payer</th>
                                                                        <th scope="col">Amount (<?php echo $default_symbol ?>)</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="money_in_td_div">
                                                                    <tr class="norecord"><td align="center" colspan="8">No records Found</td></tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Research Adjustment</h3>
                                        </div>
                                        <div class="form-data">

                                            <div class="col-sm-10"></div>
                                            <div class="col-sm-2">
                                                <input class="blue-btn pull-right" id="add_new_research_btn" type="button"  value="Add New Research">
                                            </div>

                                            <div id="add_new_research_div" class="row" style="display: none">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-3">
                                                        <label>Date</label>
                                                        <input type="text" readonly class="form-control research_date" name="research_date" >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Reference Numbers</label>
                                                        <input type="text" class="form-control research_reference_number capital" name="research_reference_number" >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Debit (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input type="text" class="form-control debit number_only money" name="debit">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Credit (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                                        <input type="text" class="form-control credit number_only money" name="credit">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-3">
                                                        <label>Description</label>
                                                        <textarea rows="2" class="form-control capital research_description" name="research_description" ></textarea>
                                                    </div>
                                                </div>
                                                <div class="btn-outer mg-lt-30 mb-15">
                                                    <input type="button"  id='reset_statement_balance_btn' class="blue-btn" value="Save" />
                                                    <input type="button"  id='add_new_research_cancel_btn' class="grey-btn" value="Cancel" />
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="grid-outer" >
                                                    <div class="table-responsive">
                                                        <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                        <table class="table table-hover table-dark" id="">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Date</th>
                                                                <th scope="col">Description</th>
                                                                <th scope="col">Reference Numbers</th>
                                                                <th scope="col">Debit (<?php echo $default_symbol ?>)</th>
                                                                <th scope="col">Credit (<?php echo $default_symbol ?>)</th>
                                                                <th scope="col">Action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="">
                                                            <tr class="norecord"><td align="center" colspan="6">No records Found</td></tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                    <div class="form-outer2">
                                        <div class="form-hdr">
                                            <h3>Files</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="col-sm-12 mb-15">
                                                <button type="button" id="add_libraray_file" class="green-btn add_libraray_file">Add Files...</button>
                                                <input class="addFileLibrary" id="addFileLibrary" type="file" name="file_library[]" accept=".gif,.jpg,.png,.jpeg,.doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                <button type="button" class="orange-btn" id="remove_library_file_add">Remove All Files</button>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="row" id="add_file_library_uploads">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-outer " >
                                        <button type="button" class="blue-btn" value="Save" id="finish_btn">Finish</button>
                                        <button type="button" class="blue-btn" value="Save" id="finish_later_btn">Finish later</button>
                                        <button type="button" id="reset_add_reconciliation_btn" class="grey-btn">Reset</button>
                                        <button type="button" id="cancel_add_reconciliation_btn" class="grey-btn">Cancel</button>
                                    </div>
                                    <!-- Form Outer Ends -->
                                </div>
                                <!--tab four starts-->
                                <div role="tabpanel" class="tab-pane" id="research_adjustment_tab">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Research Adjustments</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="grid-outer" >
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-dark" id="">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Date</th>
                                                            <th scope="col">Adjustment Type</th>
                                                            <th scope="col">Description</th>
                                                            <th scope="col">Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="">
                                                        <tr class="norecord"><td align="center" colspan="6">No records Found</td></tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form Outer starts -->
                            </div>
                        </div>
                    </div>

                    <div class="tab-content" id="list_bank_reconciliation_div">
                        <div class="form-outer">
                            <div class="form-hdr">
                                <h3>Bank Reconciliation</h3>
                            </div>
                            <div class="form-data">

                                <div class="row" id="list_utility_billing_div">
                                    <div class="property-status">
                                        <div class="col-sm-3">
                                            <label>Account</label>
                                            <select class="form-control">
                                                <option>Select</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-7"></div>
                                        <div class="col-sm-2" style="margin-top: 23px">
                                            <input class="blue-btn pull-right" id="reconcile_now_btn" type="button"  value="Reconcile Now">
                                        </div>
                                    </div>

                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="table-responsive overflow-unset">
                                                <div class="grid-outer">
                                                    <div class="apx-table">
                                                        <table class="table table-hover table-dark" id="utility_bills_listing">
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<div class="container">
    <div class="modal fade" id="money_adjustment_modal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" >&times;</button>
                    <h4 class="modal-title">Adjustments</h4>
                </div>
                <div class="modal-body pad-none">
                    <div class="panel-body" style="border-color: transparent;">
                        <div class="row">
                            <form id="adjustment_form_id">
                                <div class="form-outer">
                                    <div class="col-sm-12 row">
                                        <div class="col-sm-6">
                                            <label>Account</label>
                                            <select type="text" class="form-control adjustment_type" name="adjustment_type">
                                                <option value="1">Fee / Adjustment</option>
                                                <option value="2">Interest / Adjustment</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Description</label>
                                            <input type="text" autocomplete="off" maxlength="200" class="form-control adjustment_description" name="adjustment_description">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 row">
                                        <div class="col-sm-6">
                                            <label>Reference Number</label>
                                            <input type="text"  autocomplete="off" class="form-control reference_number" name="reference_number" >
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)  <em class="red-star">*</em></label>
                                            <input type="text" autocomplete="off" class="form-control adjustment_amount money" name="adjustment_amount">
                                        </div>
                                    </div>
                                    <div class="btn-outer mg-lt">
                                        <button type="submit" class="blue-btn" value="Save" id="adjustment_save_btn">Save</button>
                                        <button type="button" data-dismiss="modal" id="adjustment_cancel_btn" class="grey-btn">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="start_reconcile_modal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" id="start_reconciliation_cross_btn">&times;</button>
                    <h4 class="modal-title">Start Reconciling</h4>
                </div>
                <div class="modal-body pad-none">
                    <div class="panel-body" style="border-color: transparent;">
                        <div class="row">
                            <form id="start_reconcile_form_id">
                                <div class="form-outer start_reconcile_modal">
                                    <div class="col-sm-12 row">
                                        <div class="col-sm-6">
                                            <label>Account <em class="red-star">*</em>
                                                <a class="pop-add-icon" id="add_more_bank_account_icon" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select type="text" class="form-control bank_account" name="bank_account"></select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 row">
                                        <div class="blue_text">1. Enter the following from your statement.</div>
                                        <div class="col-sm-6">
                                            <label>Statement Ending Date</label>
                                            <input type="text" readonly autocomplete="off" class="form-control statement_ending_date" name="statement_ending_date" >
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Beginning Balance (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                            <input type="text" autocomplete="off" class="form-control beg_balance money number_only" id="f1_beg_balance" name="beg_balance">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 row">
                                        <div class="col-sm-6">
                                            <label>Ending Balance (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                            <input type="text" autocomplete="off" class="form-control end_balance money number_only" id="f1_end_balance" name="end_balance">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 row">
                                        <div class="blue_text">2. Enter service charges and interest earned, if any</div>
                                        <div class="col-sm-6">
                                            <label>Service Charge (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                            <input type="text" autocomplete="off" class="form-control service_charge money number_only" id="f1_service_charge" name="service_charge">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Reference Number</label>
                                            <input type="text" autocomplete="off" class="form-control service_ref_number" name="service_ref_number">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 row">
                                        <div class="col-sm-6">
                                            <label>Description</label>
                                            <input type="text" autocomplete="off" maxlength="200" class="form-control capital service_description" name="service_description">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 row">
                                        <div class="col-sm-6">
                                            <label>Interest Earned (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</label>
                                            <input type="text" autocomplete="off" class="form-control interest_earned money number_only" id="f1_interest_earned" name="interest_earned">
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Reference Number</label>
                                            <input type="text" autocomplete="off" class="form-control interest_ref_number" name="interest_ref_number">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 row">
                                        <div class="col-sm-6">
                                            <label>Description</label>
                                            <input type="text" autocomplete="off" maxlength="200" class="form-control capital interest_description" name="interest_description">
                                        </div>
                                    </div>
                                    <div class="btn-outer text-right" >
                                        <button type="submit" class="blue-btn" value="Save" id="start_reconcile_save_btn">Save</button>
                                        <button type="button"  class="clear-btn clearReconcileForm">Clear</button>
                                        <button type="button" id="start_reconcile_cancel_btn" class="grey-btn">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="add_more_bank_account_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Bank Account</h4>
                </div>
                <div class="modal-body pad-none">
                    <div class="panel-body" style="border-color: transparent;">
                        <div class="row">
                            <form class="form-outer" id="add_bank_account_form">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Portfolio <em class="red-star">*</em></label>
                                        <select id="portfolio" name="portfolio" class="fm-txt form-control">
                                            <option value="">Select</option>
                                        </select>
                                        <span id="portfolioErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Bank Name  <em class="red-star">*</em></label>
                                        <input name="bank_name" id="bank_name" maxlength="100" placeholder="Eg: Wells Fargo"   class="form-control hide_copy" type="text"/>
                                        <span id="bank_nameErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Bank A/c No.  <em class="red-star">*</em></label>
                                        <input name="bank_account_number" id="bank_account_number" maxlength="10" placeholder="Eg: 123A12345678"   class="form-control hide_copy" type="text"/>
                                        <span id="bank_account_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>FDI No.  <em class="red-star">*</em></label>
                                        <input name="fdi_number" id="fdi_number" maxlength="10" placeholder="Eg: 123A12345678"   class="form-control" type="text"/>
                                        <span id="fdi_numberErr" class="error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Branch Code <em class="red-star">*</em></label>
                                        <input name="branch_code" id="branch_code" maxlength="10" placeholder="Eg: AB12 12345 "   class="form-control" type="text"/>
                                        <span id="branch_codeErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Initial Amount(<?php echo $_SESSION[SESSION_DOMAIN]["default_currency_symbol"]?>)  <em class="red-star">*</em></label>
                                        <input name="initial_amount" id="initial_amount" placeholder="Eg: 1000"   class="form-control" type="text"/>
                                        <span id="initial_amountErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Last Used Check Number  <em class="red-star">*</em></label>
                                        <input name="last_used_check_number" id="last_used_check_number" maxlength="100" placeholder="Eg: 1234"   class="form-control" type="text"/>
                                        <span id="last_used_check_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Status <em class="red-star">*</em></label>
                                        <select id="status" name="status" class="fm-txt form-control">
                                            <option value="">Select</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                        <span id="statusErr" class="error"></span>
                                    </div>
                                </div>
                                <div>
                                    <label></label>
                                    <div class="check-outer mg-lt-30 mb-15">
                                        <input name="is_default" id="is_default" type="checkbox"/>
                                        <label>Set as Default</label>
                                    </div>
                                </div>

                                <div class="btn-outer text-right">
                                    <button type="submit" class="blue-btn" value="Save" >Save</button>
                                    <button type="button"  class="clear-btn clearAddAccountForm">Clear</button>
                                    <button type="button" id="add_bank_account_cancel_btn" class="grey-btn">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Ends -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/bankReconciling.js"></script>
<script>
    var upload_url  = "<?php echo SITE_URL; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });


        $(document).on('click','.clearReconcileForm',function () {
            bootbox.confirm("Are you sure you want to clear this form?", function (result) {
                if (result == true) {
                    $('#start_reconcile_form_id').[0].reset();
                }
            });
        });

    });




</script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
</body>
</html>
