<?php
/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 6/03/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$edit_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';
$base_url=$_SERVER['SERVER_NAME'];
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->
    <section class="main-content view-owner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting >> <span>Bank Reconciliation View</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <!--- Right Quick Links ---->
                        <div class="right-links-outer hide-links">
                            <div class="right-links">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div id="RightMenu" class="box2">
                                <h2>ACCOUNTING</h2>
                                <div class="list-group panel">
                                    <!-- Two Ends-->
                                    <a href="#" class="list-group-item list-group-item-success strong collapsed" >Receive Money</a>
                                    <!-- Two Ends-->

                                    <!-- Three Starts-->
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Multi Payments</a>
                                    <!-- Three Ends-->

                                    <!-- Four Starts-->
                                    <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">My Bills</a>
                                    <!-- Four Ends-->

                                    <!-- Five Starts-->
                                    <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Pay Bills</a>
                                    <!-- Five Ends-->

                                    <!-- Six Starts-->
                                    <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Invoices</a>
                                    <!-- Six Ends-->

                                    <!-- Seven Starts-->
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Write Checks</a>
                                    <!-- Seven Ends-->
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Print Checks</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Check Register</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Payment Register</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bill Register</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Register</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Deposit</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Late Fees</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Draw</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Contributions</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Transactions</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Process Management Fee</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Pending Transactions</a>
                                    <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Credit Card Details</a>
                                </div>
                            </div>
                        </div>
                        <!--- Right Quick Links ---->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab_list" role="tablist">
                                <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                                <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                                <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                                <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                                <li role="presentation"><a href="/Accounting/BankRegister">Banking</a></li>
                                <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                                <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                                <li role="presentation" class="active"><a href="/Accounting/AccountReconcile">Bank Reconciliation</a></li>
                                <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                                <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                                <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                                <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                            </ul>
                            <!-- Nav tabs -->
                            <div class="tab-content">

                                <!-- Form Outer Ends -->

                                <div class="form-outer2">
                                    <div class="form-hdr">
                                        <h3>Reconciliation</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <h4 class="text-black">
                                                        <strong style="margin-left:220px;">Statement Balance</strong>
                                                    </h4>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Bank Account :</label>
                                                        <span class="owner_address"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Statement Date :</label>
                                                        <span class="owner_phone"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Beginning Balance (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) :</label>
                                                        <span class="owner_note_for_Phone red-star">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Ending Balance (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) :</label>
                                                        <span class="owner_Email"></span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <h4 class="text-black">
                                                        <strong style="margin-left:220px;">System Balance</strong>
                                                    </h4>
                                                    <div class=col-xs-12>
                                                        <label class=text-right>Beginning Balance (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) : </label>
                                                        <span class=emeregency_name></span>
                                                    </div>
                                                    <div class=col-xs-12>
                                                        <label class=text-right>Money In (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) :</label>
                                                        <span class=country_code> </span>
                                                    </div>
                                                    <div class=col-xs-12>
                                                        <label class=text-right>Money Out (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) : </label>
                                                        <span class=emeregency_contact></span>
                                                    </div>
                                                    <div class=col-xs-12>
                                                        <label class=text-right>Fee / Adjustment (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) : </label>
                                                        <span class=emeregency_email></span>
                                                    </div>
                                                    <div class=col-xs-12>
                                                        <label class=text-right>Interest / Adjustment (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) : </label>
                                                        <span class=emeregency_relation></span>
                                                    </div>
                                                    <div class=col-xs-12>
                                                        <label class=text-right>Research / Adjustment (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) : </label>
                                                        <span class=emeregency_email></span>
                                                    </div>
                                                    <div class=col-xs-12>
                                                        <label class=text-right>Calculated System Balance(<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>) : </label>
                                                        <span class=emeregency_relation></span>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="" id="diffamount">
                                            <div class="mg-lt-30 difference-text" id="diffamount">
                                                <?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?><span class="difference">0:00 </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="money_in_out_div">
                                <div class="short">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Money Out (Withdrawals)<a class="pull-right adjustment_button" >Adjustment</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="grid-outer" >
                                                    <div class="table-responsive">
                                                        <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                        <table class="table table-hover table-dark" id="">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col"></th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col">Period</th>
                                                                <th scope="col">Reference Number</th>
                                                                <th scope="col">Type</th>
                                                                <th scope="col">Description</th>
                                                                <th scope="col">Payee</th>
                                                                <th scope="col">Amount (<?php echo $default_symbol ?>)</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="money_out_td_div">
                                                            <tr class="norecord"><td align="center" colspan="8">No records Found</td></tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form Outer Ends -->
                                <div class="short">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Money In (Deposits)<a class="pull-right adjustment_button" >Adjustment</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="grid-outer" >
                                                    <div class="table-responsive">
                                                        <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                        <table class="table table-hover table-dark" id="">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col"></th>
                                                                <th scope="col">Date</th>
                                                                <th scope="col">Period</th>
                                                                <th scope="col">Reference Number</th>
                                                                <th scope="col">Type</th>
                                                                <th scope="col">Description</th>
                                                                <th scope="col">Payer</th>
                                                                <th scope="col">Amount (<?php echo $default_symbol ?>)</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="money_in_td_div">
                                                            <tr class="norecord"><td align="center" colspan="8">No records Found</td></tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Research Adjustment</h3>
                                </div>
                                <div class="form-data">

                                    <div class="col-sm-12">
                                        <div class="grid-outer" >
                                            <div class="table-responsive">
                                                <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$'; ?>
                                                <table class="table table-hover table-dark" id="">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Date</th>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Reference Numbers</th>
                                                        <th scope="col">Debit (<?php echo $default_symbol ?>)</th>
                                                        <th scope="col">Credit (<?php echo $default_symbol ?>)</th>
                                                        <th scope="col">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="">
                                                    <tr class="norecord"><td align="center" colspan="6">No records Found</td></tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Form Outer Ends -->
                            <div class="form-outer" id="filelibrary">
                                <div class="form-hdr">
                                    <h3>Files</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="file-library" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->
                        </div>
                    </div>
                    <!--tab Ends -->
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



    function goBack() {
        window.history.back();
    }
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
</script>

<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/buildingView.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/viewOwner.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/viewOwner.js"></script>
<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/company/people/owner/complaintsOwner.js"></script>-->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

