<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php"); ?>
<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <!-- New Budget Section Start -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Budgeting</span> >> <span>New Budget</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>New Budget <a class="back" href="/Accounting/Budgeting"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <form id="newBudgetForm">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Budget Name <em class="red-star">*</em></label>
                                                        <input class="form-control" name="budget_name" placeholder="" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Starting Month</label>
                                                        <select id="startingMonth" name="starting_month" class="form-control">
                                                            <option value="">Select</option>
                                                            <option value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">August</option>
                                                            <option value="9">September</option>
                                                            <option selected value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Starting Year <em class="red-star">*</em></label>
                                                        <select name="starting_year" id="yearDesc" class="form-control">
                                                            <option value="">Select</option>
                                                        <?php for($i=0;$i<6;$i++) {?>
                                                            <option value="<?php echo (date("Y",strtotime("-1 year")) + $i) ?>"><?php echo (date("Y",strtotime("-1 year")) + $i) ?></option>
                                                        <?php }?>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Portfolio <em class="red-star">*</em></label>
                                                        <select name="portfolio_id" id="portfolio_id" class="form-control"></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Property <em class="red-star">*</em></label>
                                                        <select name="property_id" id="property_id" class="form-control"></select>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>New Budget</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row" id="NetIncomeTotal" style="display: none; margin-bottom: 20px;">
                                                <div class="col-sm-12">
                                                    <div class="grid-outer">
                                                        <div class="table-responsive v-middle newbudget-table">
                                                            <table id="netIncomeTable" class="table table-hover table-dark">
                                                                <thead>
                                                                <tr>
                                                                    <th width="10%">Description</th>
                                                                    <th width="6%" class="yearDescription">Oct</th>
                                                                    <th width="6%" class="yearDescription">Nov</th>
                                                                    <th width="6%" class="yearDescription">Dec</th>
                                                                    <th width="6%" class="yearDescription">Jan</th>
                                                                    <th width="6%" class="yearDescription">Feb</th>
                                                                    <th width="6%" class="yearDescription">Mar</th>
                                                                    <th width="6%" class="yearDescription">Apr</th>
                                                                    <th width="6%" class="yearDescription">May</th>
                                                                    <th width="6%" class="yearDescription">June</th>
                                                                    <th width="6%" class="yearDescription">Jul</th>
                                                                    <th width="6%" class="yearDescription">Aug</th>
                                                                    <th width="6%" class="yearDescription">Sep</th>
                                                                    <th width="10%" class="yearDesc"></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr id="incomeTrRow">
                                                                    <td>Income (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncome" readonly type="text"/></td>
                                                                    <td><input class="form-control trIncomeTotal" readonly type="text"/></td>
                                                                </tr>
                                                                <tr id="expenseTrRow">
                                                                    <td>Expense (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpense" readonly type="text"/></td>
                                                                    <td><input class="form-control trExpenseTotal" readonly type="text"/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Net Income (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td><input class="form-control grandTotal" readonly type="text"/></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="grid-outer">
                                                        <div class="table-responsive v-middle newbudget-table">
                                                            <table id="budgetTable" class="table table-hover table-dark">
                                                                <thead>
                                                                <tr>
                                                                    <th width="10%">Description</th>
                                                                    <th width="6%" class="yearDescription">Oct</th>
                                                                    <th width="6%" class="yearDescription">Nov</th>
                                                                    <th width="6%" class="yearDescription">Dec</th>
                                                                    <th width="6%" class="yearDescription">Jan</th>
                                                                    <th width="6%" class="yearDescription">Feb</th>
                                                                    <th width="6%" class="yearDescription">Mar</th>
                                                                    <th width="6%" class="yearDescription">Apr</th>
                                                                    <th width="6%" class="yearDescription">May</th>
                                                                    <th width="6%" class="yearDescription">June</th>
                                                                    <th width="6%" class="yearDescription">Jul</th>
                                                                    <th width="6%" class="yearDescription">Aug</th>
                                                                    <th width="6%" class="yearDescription">Sep</th>
                                                                    <th width="10%" class="yearDesc"></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="appendCOA">
                                                                <tr>
                                                                    <td colspan="14" class="bg-grey">Income <em class="red-star">*</em></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4000-Rental Income (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>4000-Rental Income (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td><input class="form-control" type="text"/></td>
                                                                    <td></td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-outer text-right">
                                            <button type="submit" id="budgetSubmit" class="blue-btn">Submit</button>
                                            <button type="button"  class="clear-btn clearFormReset" id="clearNewBudget">Clear</button>
                                            <button type="button" id="budgetCancel" class="grey-btn">Cancel</button>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                </div>


                            </div>
                            <!--tab Ends -->

                        </div>

                    </div>
                </div>
            </div>
    </section>
    <!-- New Budget Section End -->
</div>

<script type="text/javascript">
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var property_unique_id = '';
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
    var manager_id = "";
    var attachGroup_id = "";
    var credential_notice_period =  "<?php echo $_SESSION[SESSION_DOMAIN]['default_notice_period']; ?>";

    $(function () {
        $('.nav-tabs').responsiveTabs();
    });
    $('#accounting_top').addClass('active');
    if(localStorage.getItem('windowScroll') !== null){
        setTimeout(function(){
            $('html, body').animate({
                'scrollTop':  localStorage.getItem('windowScroll')
            });
            localStorage.removeItem('windowScroll');
        },300);
    }

    $(document).on('click','#clearNewBudget',function(){
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                localStorage.setItem('windowScroll',document.body.scrollHeight);
                window.location.reload();
            }
        });
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function () {
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function () {
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->
    $(document).ready(function () {
        $(".slide-toggle").click(function () {
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        $(".slide-toggle2").click(function () {
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accountingModule/budgeting/newBudget.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accountingModule/budgeting/addBudget.js" type="text/javascript"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
