<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php"); ?>
<div id="wrapper">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php"); ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">Accounting &gt;&gt; <span>Pending Transaction </span></div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search"><input placeholder="Easy Search" type="text"/></div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i><i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>ACCOUNTING</h2>
                            <div class="list-group panel">
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >Receive Money</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Multi Payments</a>
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">My Bills</a>
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Pay Bills</a>
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Invoices</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Write Checks</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Print Checks</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Check Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Payment Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bill Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Register</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Bank Deposit</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Late Fees</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Draw</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Owner Contributions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Process Management Fee</a>
                                <a id="" href="/Accounting/PendingGLPosting" class="list-group-item list-group-item-success strong collapsed" >Pending Transactions</a>
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Credit Card Details</a>
                            </div>
                        </div>
                    </div>
                    <div class="main-tabs">
                        <ul class="nav nav-tabs pendingtabs" role="tablist">
                            <li role="presentation" class="active"><a href="/Accounting/PendingGLPosting">Tenant Transaction & NSF</a></li>
                            <li role="presentation" data_tab="tenant_payment"><a href="javascript:void(0)">Tenant Payment</a></li>
                            <li role="presentation" data_tab="tenant_security_deposit"><a href="javascript:void(0)">Security Deposit</a></li>
                            <li role="presentation"><a href="javascript:void(0)">Owner Draw</a></li>
                            <li role="presentation"><a href="javascript:void(0)">Owner Contribution</a></li>
                            <li role="presentation"><a href="javascript:void(0)">Unit Vacancy</a></li>
                            <li role="presentation"><a href="javascript:void(0)">Vendor Invoice</a></li>
                            <li role="presentation"><a href="javascript:void(0)">Vendor Payment</a></li>
                        </ul>
                        <div class="tab-content view_accounting-content">
                            <div class="panel-heading">
                                <h5 class="panel-title">Tenant Transaction & NSF</h5>
                                <div class="panel-title"></div>
                                <ul class="nav nav-tabs" role="tablist"></ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="">
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <table id="tenant_transaction_nsf" class="table table-bordered"></table>
                                                                        </div>
                                                                        <div class="table-responsive hide">
                                                                            <table id="tenant_payment" class="table table-bordered"></table>
                                                                        </div>
                                                                        <div class="table-responsive hide">
                                                                            <table id="tenant_security_deposit" class="table table-bordered"></table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="collapseTwo" class="tab-content">
                                                                <div class="panel-body pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="row">
                                                                            <div class="col-sm-2">
                                                                                <div class="col-sm-4"><input id="postnsf" style="cursor: pointer;" type="button" value="Post" class="blue-btn postnsf"></div>
                                                                                <div class="col-sm-6">
                                                                                    <input id="cancelnsf" style="cursor: pointer;" type="button" value="Cancel" class="grey-btn cancel_edit_nsf">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>var default_currency_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/accounting/pending_transaction/tenant_transaction_nsf.js"></script>
<!-- Jquery Starts -->

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php"); ?>