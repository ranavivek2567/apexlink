<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Receivable </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation" class="active"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="panel-heading">
                                <div class="tab-content">
                                    <div class="main-tabs active">
                                        <ul class="nav nav-tabs acc_bank_subtabs" role="tablist">
                                            <li role="presentation" class="active" data_tab="accordion"><a href="javascript:void(0)">Write Checks</a></li>
                                            <li role="presentation" class="acc_bank_register" data_tab="accordion2"><a href="javascript:void(0)">Bank Register</a></li>
                                        </ul>
                                    </div>
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group subdata_tabs" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Write Check</a>
                                                                </h4>
                                                            </div>
                                                            <div class="form-outer">

                                                                <div class="form-data">
                                                                    <form id="createCheckForm">
                                                                        <div class="row">
                                                                            <div class="col-sm-3 col-md-3 ">
                                                                                <label>Select Property <em class="red-star">*</em>
                                                                                </label>
                                                                                <select class="form-control" id="property" name="property">
                                                                                    <option value="">Select</option>
                                                                                </select>
                                                                                <span class="term_planErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-3 col-md-3 ">
                                                                                <label>Select Building
                                                                                </label>
                                                                                <select class="form-control" id="building" name="building">
                                                                                    <option value="0">Select</option>
                                                                                </select>
                                                                                <span class="term_planErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-3 col-md-3">
                                                                                <label>Select Unit 
                                                                                </label>
                                                                                <select class="form-control" id="unit" name="unit">
                                                                                    <option value="0">Select</option>
                                                                                </select>
                                                                                <span class="term_planErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-3 col-md-3 ">
                                                                                <label>Bank Account <em class="red-star">*</em>
                                                                                </label>
                                                                                <select class="form-control" id="selectBank" name="selectBank">
                                                                                    <option value="">Select</option>
                                                                                   
                                                                                </select>
                                                                                <span class="term_planErr error red-star"></span>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                        <div class="row"  >
                                                                            <div class="col-md-8" style="background: #F3FFF1;padding-top: 20px;">
                                                                                <div class="col-sm-6 col-md-6 ">
                                                                                    <label>Ending Balance <em class="red-star">*</em>
                                                                                    </label>
                                                                                    <input type="radio" name="userType" value="3" class="selectUser" checked>Vendor
                                                                                    <input type="radio" name="userType" value="4" class="selectUser">Owner
                                                                                    <input type="radio" name="userType" value="2" class="selectUser">Tenant
                                                                                    <input type="radio" name="userType" value="0" class="selectUser">Other
                                                                                    <div class="row payonline-form-field">
                                                                                        <div class="col-sm-6">
                                                                                            <label> <em class="red-star">*</em>
                                                                                                Pay To The Order Of
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="col-sm-6">
                                                                                            <div class="orderOff">
                                                                                                <select name='payOrderOff' class="form-control payOrderOff">
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6 col-md-6 ">
                                                                                    <div class="row">
                                                                                        <div class="col-sm-3">
                                                                                            <label>Check Number <em class="red-star">*</em>
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="checkNumber" class="form-control checkNumber">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-sm-3">
                                                                                            <label>Date <em class="red-star">*</em>
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="Date"  class="form-control calander">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-sm-3">
                                                                                            <label>Money($) <em class="red-star">*</em>
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="money" class="form-control money amount_num">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                                <div class="col-sm-12">
                                                                                    <p class="amount-word-text"> Amount in Words </p>
                                                                                    </label>
                                                                                    <div class="row">
                                                                                        <div class="col-sm-6">
                                                                                            <div class="col-sm-4">Address</div>
                                                                                            <div class="col-sm-8"><textarea class="address form-control" name="address" readonly></textarea></div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row">
                                                                                        <div class="col-sm-6">
                                                                                            <div class="col-sm-4">Memo</div>
                                                                                            <div class="col-sm-8"><input type="text" name="memo" class="form-control"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="payonline-form-button">
                                                                            <input type='button' value="Print" class="blue-btn printBtn">
                                                                            <input type='submit' value="Save & New" class="blue-btn">
                                                                            <input type='button' value="Save & Close" class="blue-btn saveClose" data-type='close'>
                                                                            <input type='button' value="Clear" class="clear-btn clearBankForm">
                                                                            <input type='button' value="Cancel" class="grey-btn cancelbtn">

                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <table class='table  table-bordered checkData' border= '1px'>
                                                                                <tr>
                                                                                    <th>Check Number</th>
                                                                                    <th>Account</th>
                                                                                    <th>Amount</th>
                                                                                    <th>Memo</th>
                                                                                    <th>Pay Type</th>
                                                                                    <th>Pay Name</th>
                                                                                    <th>Address</th>
                                                                                    <th>Property</th>
                                                                                </tr>

                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-group hide subdata_tabs" id="accordion2">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Bank Register</a>
                                                                </h4>
                                                            </div>
                                                            <div class="form-outer">
                                                                <div class="form-hdr">
                                                                </div>
                                                                <div class="form-data">
                                                                    <form id="createCheckForm">
                                                                        <div class="row">
                                                                            <div class="col-sm-3 col-md-3 ">
                                                                                <label>Select Bank</label>
                                                                                <select class="form-control" id="selectbank" name="selectbank">
                                                                                    <option value="">Select</option>
                                                                                </select>
                                                                                <span class="term_planErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-3 col-md-3 ">
                                                                                <label>Bank Account Number</label>
                                                                                <span class="bank_acc_num"></span>
                                                                                <span class="term_planErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-3 col-md-3">
                                                                                <?php $default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol']) ? $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] : '$'; ?>
                                                                                <label>Balance (<?php echo $default_symbol; ?>)</label>
                                                                                <span class="bankbalance"></span>
                                                                                <span class="term_planErr error red-star"></span>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-3 col-md-3 ">
                                                                                <label>From</label>
                                                                                <input type="text" name="fromdate" class="form-control" class="fromdate">
                                                                            </div>
                                                                            <div class="col-sm-3 col-md-3">
                                                                                <label>To</label>
                                                                                <input type="text" name="todate" class="form-control" class="todate">
                                                                            </div>
                                                                            <div class="col-sm-1 col-md-1">
                                                                                <a href="javascript:void(0)" class="blue-btn showbanks">Show</a>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <table class='table  table-bordered' id="bankregistertable" border= '1px'></table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->
                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>
                        <!-- Sub Tabs Starts-->
                        <!-- Sub tabs ends-->
                    </div>
                </div>
            </div>
            <!--Tabs Ends -->
        </div>
</div>
</div>
</section>
</div>

<script> var date = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>  var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";</script>
<!-- Wrapper Ends -->
<!-- Footer Ends -->
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>
    $(document).ready(function(){

        getBankDetails();
        $(document).on("click",".acc_bank_subtabs li",function(){
            $(".acc_bank_subtabs li").removeClass("active");
            $(this).addClass("active");
            var data_tab = $(this).attr("data_tab");
            $(".subdata_tabs").addClass("hide");
            $("#"+data_tab).removeClass("hide");
        });

        $(document).on("change","#selectbank",function(){
            var value = $(this).val();
            if (value != "0") {
                getBankDetail(value);
            }
        });
        $('.cancelbtn').click(function () {
        window.location.href=window.location.href;
        });
        var currentdate = $.datepicker.formatDate(jsDateFomat, new Date());
        $("#accordion2 input[name='fromdate'], #accordion2 input[name='todate']").val(currentdate);

        $("#accordion2 input[name='fromdate'], #accordion2 input[name='todate']").datepicker({dateFormat: jsDateFomat});

        function getBankDetail(value){
            $.ajax({
                url:'/Accounting/AjaxPendingGLPosting',
                type: 'POST',
                data: {
                    "class": 'AjaxPendingGLPosting',
                    "action": 'getBankDetail',
                    "id": value
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status=='success'){
                        var optionsValue = response.data;
                        $('.bank_acc_num').text( optionsValue.bank_account_number );
                        $('.bankbalance').text( optionsValue.balance );
                    }else if (response.code == 504) {
                        toastr.success(response.message);
                    }
                }
            });

        }

        function getBankDetails(){
            $.ajax({
                url:'/Accounting/AjaxPendingGLPosting',
                type: 'POST',
                data: {
                    "class": 'AjaxPendingGLPosting',
                    "action": 'getBankDetails'
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status=='success'){
                        var optionsValue = response.data;
                        var options = "<option value='0'>Select</option>";
                        for(var i = 0; i < optionsValue.length; i++){
                            options += "<option value='"+optionsValue[i].id+"'>"+optionsValue[i].bank_name+"</option>";
                        }
                        $('#selectbank').html( options );
                    }else if (response.code == 504) {
                        toastr.success(response.message);
                    }
                }
            });
        }

        //function jqGridBankTransaction(status) {
        //    var table = 'transactions';
        //    var depositeCol = "Deposit (<?php //echo $default_symbol; ?>//)";
        //    var paymentCol = "Payment (<?php //echo $default_symbol; ?>//)";
        //    var balancesCol = "Balances (<?php //echo $default_symbol; ?>//)";
        //    var columns = ['Date','Reference Number','Description','user_type','Bank Name','Reconcile',depositeCol,paymentCol,balancesCol,'total_charge_amount'];
        //    var select_column = ['Post'];
        //    var joins = [
        //        {table:table,column:'bank_id',primary:'id',on_table:'company_accounting_bank_account'},
        //        // {table:'company_accounting_bank_account',column:'portfolio',primary:'portfolio_id',on_table:'general_property'},
        //        // {table:'general_property',column:'id',primary:'property_id',on_table:'property_bank_details'}
        //    ];
        //    var conditions = ["eq","bw","ew","cn","in"];
        //    //var extra_where = [{column:'status',value:2,condition:'=',table:'tenant_charges'}];
        //    var extra_where = [];
        //    var extra_columns = [];
        //    var columns_options = [
        //        {name:'Date',index:'created_at', width:150,align:"left",searchoptions: {sopt: conditions},table:table},
        //        {name:'Reference Number',index:'reference_no', width:150, align:"left",searchoptions: {sopt: conditions},table:table},
        //        {name:'Description',index:'type', width:150, align:"left",searchoptions: {sopt: conditions},table:table},
        //        {name:'user_type',index:'user_type', hidden:true,width:150, align:"left",searchoptions: {sopt: conditions},table:table},
        //        {name:'Bank Name',index:'bank_name', width:150, align:"left",searchoptions: {sopt: conditions},table:'company_accounting_bank_account'},
        //        {name:'Reconcile',index:'id',align:"center", width:170,searchoptions: {sopt: conditions},table:table,formatter:reconformatter},
        //        {name:depositeCol,index:'total_charge_amount',align:"center", width:170,searchoptions: {sopt: conditions},table:table,formatter:paymentcheck},
        //        {name:paymentCol,index:'total_charge_amount',align:"center",width:170,searchoptions: {sopt: conditions},table:table},
        //        {name:balancesCol,index:'total_charge_amount', width:170,align:"center",searchoptions: {sopt: conditions},table:table},
        //        {name:'total_charge_amount',index:'total_charge_amount', width:170,align:"center",searchoptions: {sopt: conditions},table:table}
        //    ];
        //    var ignore_array = [];
        //    jQuery("#bankregistertable").jqGrid({
        //        url: '/List/jqgrid',
        //        datatype: "json",
        //        height: '100%',
        //        autowidth: true,
        //        colNames: columns,
        //        colModel: columns_options,
        //        pager: true,
        //        mtype: "POST",
        //        postData: {
        //            q: 1,
        //            class: 'jqGrid',
        //            action: "listing_ajax",
        //            table: table,
        //            select: select_column,
        //            columns_options: columns_options,
        //            status: status,
        //            ignore:ignore_array,
        //            joins:joins,
        //            extra_where:extra_where,
        //            extra_columns:extra_columns,
        //            deleted_at:'no'
        //        },
        //        viewrecords: true,
        //        sortname: 'transactions.updated_at',
        //        sortorder: "desc",
        //        sorttype:'date',
        //        sortIconsBeforeText: true,
        //        headertitles: true,
        //        rowNum: '5',
        //        rowList: [5, 10, 20, 30, 50, 100, 200],
        //        caption: "Bank Register",
        //        pginput: true,
        //        pgbuttons: true,
        //        navOptions: {
        //            edit: false,
        //            add: false,
        //            del: false,
        //            search: true,
        //            filterable: true,
        //            refreshtext: "Refresh",
        //            reloadGridOptions: {fromServer: true}
        //        }
        //    }).jqGrid("navGrid",
        //        {
        //            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        //        },
        //        {}, // edit options
        //        {}, // add options
        //        {}, //del options
        //        {top:0,left:400,drag:true,resize:false} // search options
        //    );
        //}

    });


</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/BankRegister.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

