<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Accounting &gt;&gt; <span>Receivable </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/accounting/layout/right-nav.php");?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Accounting/Accounting">Receivables</a></li>
                            <li role="presentation"><a href="/Accounting/paybills">Pay Bills</a></li>
                            <li role="presentation"><a href="/Accounting/ConsolidatedInvoice" >Invoices</a></li>
                            <li role="presentation"><a href="/Accounting/RecurringBill">Recurring Transactions</a></li>
                            <li role="presentation" class="active"><a href="/Accounting/BankRegister">Banking</a></li>
                            <li role="presentation"><a href="/Accounting/JournalEntries">Journal Entries</a></li>
                            <li role="presentation"><a href="/Accounting/Budgeting">Budgeting</a></li>
                            <li role="presentation"><a href="/Accounting/AccountReconcile">Bank Reconcilation</a></li>

                            <li role="presentation"><a href="/Accounting/AccountClosing">Account Closing</a></li>
                            <li role="presentation"><a href="/Accounting/UtilityBilling">Utility Billing</a></li>
                            <li role="presentation"><a href="/MultiPay/MultiPay">Multi Payments</a></li>
                            <li role="presentation"><a href="/MultiPay/PaymentPlan">Payment Plan</a></li>
                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">

                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Write Check</a>
                                                                </h4>
                                                            </div>

                                                                                  <div class="form-outer">
                                <div class="form-hdr">
                             
                                </div>
                                <div class="form-data">
                                    <form id="createCheckForm">
                                    <div class="row">

                                        <div class="col-sm-3 col-md-2 ">
                                            <label>Start Date <em class="red-star">*</em>
                                         
                                            </label>
                                           <input type="text" name="start_date" class="form-control calander start_date" readonly>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-2 ">
                                            <label>End Date <em class="red-star">*</em>
                                            
                                            </label>
                                           <input type="text" name="end_date" class="form-control calander end_date" readonly>
                                        </div>

                                        <div class="col-sm-2 col-md-2">
                                            <label>Bank Account <em class="red-star">*</em>
                                           
                                            </label>
                                            <select class="form-control" id="bank_account" name="bank_account">
                                                <option value="">Select</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-2 col-md-2">
                                            <label>Check No
                                             </label>
                                              <input type="text" name="checkNumber" class="form-control checkNumber capital">
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                              <div class="col-sm-2 col-md-2">
                                            <label>Check Status
                                             </label>
                                            <select class="form-control" id="checkStatus" name="checkStatus">
                                               <option value="Entered">Entered</option>
                                               <option value="Cleared">Cleared</option>
                                               <option value="Void">Void</option>
                                               <option value="Printed">Printed</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-2 col-md-2">
                                            <input type="button" class="Search blue-btn check_search" value="Search">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>



                                                     <div class="apx-table">

                                                            <div class="table-responsive">

                                                                <table id="getCheckList"
                                                                       class="table table-bordered">

                                                                </table>
                                                            </div>
                                                        </div>
                    <input type="button" class="print_check blue-btn" value="Print" data-check_id="0" data-user_type="0" style="margin:10px;">





                     
                     
                        

                                   
                                </form>

                                </div>

                            </div>





 


                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>




<div class="modal fade" id="CheckModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content pull-left">
            <div class="modal-header" style="border-bottom: unset;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                </a>
                <input type="button" class="blue-btn pull-left" onclick="PrintElem('#check_content')" onclick="window.print();"  value="Print"/>


            </div>
            <div class="modal-body pull-left" style="height: 487px; overflow: auto;" id="check_content">
                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                    <form name="ChequeForm" id="ChequeForm">
                        <div class="panel-body">
                            <div class="row" style="margin: 0px;">
                                <div class="standard_check common_check col-sm-12" id="divStandardCheck" style="width: 940px; height: 380px; float: left; margin: 30px 14px; background: url('/company/images/cheque/standardCheck-bg.jpg') 0px top no-repeat; display: block;">
                                    <!--Cheque Part-->
                                    <div style="width: 890px; margin: auto; clear: both; height: 300px; margin-bottom: 10px; overflow: hidden; font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;margin-top: 30px;">
                                        <div style="width: 100%; margin-top: 0px; height: 100px; float: left;">
                                            <div style="margin: 50px 20px 0px 0px; width: 110px; text-align: left; float: right;
                                                position: relative;">
                                                <div style="padding-top: 5px; font-size: 16px; float: right; height: 20px; padding-bottom: 5px;
                                                    width: 100%; font-weight: 700;">
                                                    <div id="stDate" class="check_fields"  style="max-width: 100%; text-align: right; float: left; visibility: visible; display:block">
                                                        <table style="letter-spacing: 0px; margin: 0; padding: 0;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                            <tr>
                                                                <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                    0
                                                                </td>
                                                                <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                    4
                                                                </td>
                                                                <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                    /
                                                                </td>
                                                                <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                    2
                                                                </td>
                                                                <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                    7
                                                                </td>
                                                                <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                    /
                                                                </td>
                                                                <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                    2
                                                                </td>
                                                                <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                    0
                                                                </td>
                                                                <td style="padding: 0 1px; text-align: center; font-stretch: condensed;">
                                                                    1
                                                                </td>
                                                                <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                    5
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="width: 100%; margin-top: 10px; float: left; height: 25px;">
                                            <div style="width: 100%; margin-top: 0px; float: left; font-size: 12px;">
                                                <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                    height: 20px; margin-top: 0px; text-indent: 10px; width: 620px; margin-right: 10px;
                                                    font-size: 16px; margin-left: 80px;">
                                                    <span id="stPayeeName" class="check_fields" style=" display:">KENTUCKY CHILD SUPPORT</span>
                                                </div>
                                                <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                    height: 20px; margin: 0px 0 0 30px; width: 120px; font-size: 16px;">
                                                    <span id="stAmountNumeric" class="check_fields" style="display:">**84.00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="width: 100%; margin-top: 10px; float: left; height: 20px;">
                                            <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                height: 20px; width: 650px; text-indent: 10px; font-size: 14px; margin-left: 10px;">
                                                <span id="stAmountText" class="check_fields" style=" display:">Eighty-Four and 00/100***********************</span>
                                            </div>
                                            <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                height: 20px; width: 70px; font-size: 12px; margin-top: 5px">
                                                &nbsp;</div>
                                        </div>
                                        <div style="width: 100%; margin-top: 10px; float: left; height: 60px;">
                                            &nbsp;
                                            <div style="margin: 20px 0px 0px 70px; width: 300px; font-size: 15px; font-weight: 700;
                                                float: left; display: inline-block;">
                                                <div style="float: left;">
                                                                                            <span id="stPayeeNameAddress" class="check_fields" style=" display:">KENTUCKY CHILD SUPPORT,
                                                                                            <br>
                                                                                            P.O BOX 14059
                                                                                            <br>
                                                                                            LEXINGTON, KY 40512 </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="width: 100%; margin-top: 0px; float: left; height: 50px; position: relative;">
                                            <div style="margin: 30px 0px 0px 60px; width: 300px; font-size: 16px; float: left;
                                                letter-spacing: 1px; font-weight: bold; font-stretch: condensed; line-height: 16px;
                                                overflow: hidden;">
                                                <span id="stMemoText" class="check_fields" style=" display:">1D: 056-72-4954= Barksdale, Marcedes</span>
                                            </div>
                                            <div style="margin: 0px 0px 0px 0px; width: 280px; text-align: center; font-size: 12px;
                                                float: right; position: absolute; top: -14px; right: 20px;">
                                                <div style="width: 100%; float: left;">
                                                    <div id="stSignature" class="check_fields"  style="width: 100%; margin: 0px 0px 3px; padding-bottom: 2px; height: 45px; line-height: 30px; float: left;  display:">
                                                        <img src="/company/images/cheque/sig.png" alt="" valign="top">
                                                    </div>
                                                    <div style="padding-right: 8px; float: left; width: 100%; font-size: 9px;">
                                                        &nbsp;</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//Cheque Part-->
                                </div>
                                <div class="wallet_check common_check" id="divWalletCheck" style="width: 940px; height: 300px; float: left; margin: 30px 0px; background: url('/company/images/cheque/walletCheck-bg.jpg') left top / 99% no-repeat; display: none">
                                    <!--Cheque Part-->
                                    <div style="width: 890px; margin: 20px auto 0; clear: both; height: 275px; overflow: hidden;
                                        font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;">
                                        <!--Cheque Left-->
                                        <div style="width: 200px; float: left; height: 275px;">
                                            <div style="width: 100%; float: left; margin: 55px 0 0 0; height: 30px;">
                                                <div style="font-size: 12px; font-weight: 700; float: left; width: 100px; margin-left: 30px;">
                                                    <div style="float: left; width: 100%; text-align: left;">
                                                        <span class="waDate check_fields" style=" display:">04/27/2015</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; clear: both; float: left; margin: 7px 0 0 0; height: 45px;">
                                                <div style="font-size: 12px; font-weight: 700; float: left; margin-left: 30px;">
                                                    <span class="waPayeeName check_fields" style=" display:">JOHN LAUDERDALE</span>
                                                </div>
                                            </div>
                                            <div style="width: 100%; float: left; margin: 8px 0 0 0; height: 30px;">
                                                <div style="font-size: 12px; font-weight: 700; float: left; margin-left: 55px;">
                                                    <span class="waAmountNumeric check_fields" style=" display:">930.00</span>
                                                </div>
                                            </div>
                                            <div style="width: 100%; float: left; margin: 6px 0 0 0; height: 30px;">
                                                <div style="font-size: 12px; font-weight: 700; float: left; margin-left: 30px;">
                                                    <span class="waMemotext check_fields" style=" display:">BUILDING B UNIT 3</span>
                                                </div>
                                            </div>
                                            <div style="width: 100%; float: left; margin: 6px 0 0 0; height: 32px;">
                                                <div style="font-size: 12px; font-weight: 700; float: left; margin-left: 30px;">
                                                    <span class="waAccountPayable check_fields" style=" display:">Accounts Payable</span>
                                                </div>
                                            </div>
                                            <div style="width: 100%; float: left; margin: 5px 0 0 0; height: 30px;">
                                                <div style="font-size: 12px; font-weight: 700; float: left; margin-left: 30px;">
                                                    <span class="waAccounts check_fields" style=" display:">ALERUS-CHECKING</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!--//Cheque Left-->
                                        <!--Cheque Right-->
                                        <div style="width: 620px; float: right;">
                                            <div style="width: 96%; margin-top: 48px; margin-right: 20px; height: 18px; float: left;">
                                                <div style="font-size: 14px; float: right; height: 18px; font-weight: 700; text-align: right;
                                                    margin-right: 10px;">
                                                    <span class="waDate check_fields" style="visibility: visible;">04/27/2015</span>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-top: 13px; float: left; height: 20px;">
                                                <div style="width: 100%; margin-top: 0px; float: left; font-size: 12px;">
                                                    <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                        height: 20px; margin-top: 0px; text-indent: 10px; width: 365px; margin-right: 10px;
                                                        font-size: 14px; margin-left: 60px;">
                                                        <span class="waPayeeName check_fields" style=" display:">JOHN LAUDERDALE</span></div>
                                                    <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                        height: 18px; margin: 0px 0 0 53px; width: 80px; font-size: 16px;">
                                                        <span class="waAmountNumeric check_fields" style=" display:">**930.00</span></div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-top: 5px; float: left; height: 18px;">
                                                <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                    height: 18px; width: 650px; text-indent: 10px; font-size: 14px; margin-left: 10px;">
                                                    <span class="waAmountText check_fields" style=" display:">Nine Hundred Thirty and 00/100***********************</span>
                                                </div>
                                                <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                    height: 18px; width: 70px; font-size: 12px; margin-top: 0px">
                                                    &nbsp;</div>
                                            </div>
                                            <div style="width: 100%; margin-top: 5px; float: left; height: 68px;">
                                                &nbsp;
                                                <div style="margin: 15px 0px 0px 40px; width: 300px; font-size: 13px; font-weight: 700;
                                                    float: left; display: inline-block;">
                                                    <div style="float: left;">
                                                                                            <span class="waPayeeNameAddress check_fields" style=" display:">JOHN LAUDERDALE
                                                                                            <br>
                                                                                            452 BARROWAY LANE
                                                                                            <br>
                                                                                            VERSAILLES, KY 40383 </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-top: 0px; float: left; height: 50px; position: relative;">
                                                <div style="margin: 10px 0px 0px 40px; width: 300px; font-size: 13px; float: left;
                                                    letter-spacing: 1px; font-weight: bold; font-stretch: condensed; line-height: 16px;
                                                    overflow: hidden;">
                                                    <span class="waMemotext check_fields" style=" display:">BUILDING B UNIT 3</span>
                                                </div>
                                                <div style="margin: 0px 0px 0px 0px; width: 180px; text-align: center; font-size: 12px;
                                                    float: right; position: absolute; top: -20px; right: 20px;">
                                                    <div style="width: 100%; float: left;">
                                                        <div class="waSignature check_fields" style="width: 100%; margin: 0px 0px 3px; padding-bottom: 2px; height: 45px; line-height: 30px; float: left; text-align: center;  display:">
                                                            <img src="/company/images/cheque/sig.png" alt="" valign="top" style="width: 70%;
                                                                /">
                                                        </div>
                                                        <div style="padding-right: 8px; float: left; width: 100%; font-size: 9px;">
                                                            &nbsp;</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--//Cheque Right-->
                                    </div>
                                    <!--//Cheque Part-->
                                </div>
                                <div class="voucher_check common_check " id="divVoucherCheck" style="width: 940px; float: left; height: 380px; margin: 30px 50px; background: url('/company/images/cheque/voucherCheck-bg.jpg') 0px top no-repeat; display: none;">
                                    <!--Cheque Part-->
                                    <div style="width: 820px; margin: auto; clear: both; height: 290px; overflow: hidden;
                                        font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;
                                        margin-top: 50px;">
                                        <div style="width: 100%; margin-top: 100px; float: left; height: 20px;">
                                            <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                height: 20px; width: 660px; float: left; margin-left: 40px; position: relative;
                                                top: 10px; font-size: 14px;">
                                                <div id="voAmountText" class="check_fields" style="display:">
                                                    Sixty- Three Thousand Six Hundred Eighty-one and 31 / 100*******
                                                </div>
                                            </div>
                                            <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                height: 20px; width: 70px; font-size: 12px; margin-top: 5px; display: none;">
                                                DOLLARS</div>
                                        </div>
                                        <div style="width: 300px; margin-top: 30px; float: right; height: 20px; clear: both;">
                                            <div style="height: 20px; float: right;">
                                                <div style="margin: 0px 60px 0px 0px; width: 100px; text-align: center; float: left;
                                                    position: relative;">
                                                    <div style="padding-top: 0px; font-size: 15px; float: left; height: 20px; padding-bottom: 0px;
                                                        width: 100%; font-weight: 700;">
                                                        <div id="vodate" class="check_fields" style="float: right; max-width: 100%; text-align: center; display:">
                                                            <table style="letter-spacing: 0px; margin: 0; padding: 0;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tbody>
                                                                <tr>
                                                                    <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                        0
                                                                    </td>
                                                                    <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                        9
                                                                    </td>
                                                                    <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                        /
                                                                    </td>
                                                                    <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                        1
                                                                    </td>
                                                                    <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                        2
                                                                    </td>
                                                                    <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                        /
                                                                    </td>
                                                                    <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                        2
                                                                    </td>
                                                                    <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                        0
                                                                    </td>
                                                                    <td style="padding: 0 1px; text-align: center; font-stretch: condensed;">
                                                                        1
                                                                    </td>
                                                                    <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                        4
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="margin: 0px 0px 0px 0px; width: 115px; text-align: center; float: left;
                                                    position: relative;">
                                                    <div id="voAmountNumeric" class="check_fields" style="letter-spacing: 0.5px; font-weight: bold; float: left;
                                                        font-stretch: condensed; height: 20px; margin-top: 0px; width: 120px; margin-right: 0px;
                                                        font-size: 15px;display:">
                                                        **63,681.31</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="width: 100%; margin-top: 0px; float: left; height: 30px;">
                                            <div style="width: 100%; margin-top: 0px; float: left; font-size: 15px;">
                                                <div id="voPayeeName" class="check_fields" style="letter-spacing: 0.5px; font-weight: bold; float: left;
                                                   font-stretch: condensed; height: 20px; margin-top: 0px; width: 510px; margin-right: 10px;
                                                    margin-left: 40px; font-size: 15px; position: relative; top: -10px; display:">
                                                    PNC BANK</div>
                                            </div>
                                        </div>

                                        <div style="width: 100%; margin-top: 0px; margin-bottom: 20px; float: left; height: 120px;
                                            position: relative;">
                                            <div style="margin: 0px 0px 0px 0px; width: 300px; font-size: 12px; float: left;
                                                letter-spacing: 1px; font-weight: bold; font-stretch: condensed; line-height: 16px;
                                                overflow: hidden; display: none">
                                                MEMO <span style="padding-left: 5px; font-weight: normal; visibility: hidden;">Memo
                                                    Text</span>
                                            </div>
                                            <div style="margin: 0px 0px 20px 0px; width: 350px; text-align: center; font-size: 12px;
                                                float: right; position: relative; top: 20px; right: 0px; height: 100px;">
                                                <div style="width: 100%; float: left;">
                                                    <div id="voSignature" class="check_fields" style="width: 100%; margin: 0px 0 3px 0; padding-bottom: 2px;
                                                        height: 45px; line-height: 30px; float: left;display:">
                                                        <img src="/company/images/cheque/sig.png" alt="" valign="top">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//Cheque Part-->
                                </div>
                                <div class="middle_style common_check" id="divCheckMiddle" style="width: 100%; float: left; display: none;">
                                    <!--top Data-->
                                    <div style="width: 940px; float: left; height: 385px; font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;
                                        margin: 0px 50px 0;">
                                        <!--Top Grid-->
                                        <div style="width: 920px; margin: auto; margin-top: 10px; height: 365px; overflow: hidden;
                                                                    clear: both; background: url('/company/images/cheque/checkBottom-bg.jpg') 0px top no-repeat; background-size: 100%;">
                                            <div style="margin: 10px 0 0; padding: 0; width: 100%; float: left;">
                                                <table style="border-image: none; height: 350px; font-size: 14px; table-layout: fixed;
                                                    font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <th style="padding: 2px 5px; text-align: left;" height="20">
                                                            &nbsp;
                                                        </th>
                                                        <th style="padding: 2px 5px; text-align: left;">
                                                            &nbsp;
                                                        </th>
                                                        <th style="padding: 2px 5px;" align="right" width="120">
                                                            <div style="font-size: 15px; display: none;">
                                                                39608</div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="145" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="90%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 30px;">
                                                                            PNC BANK</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 0;">
                                                                            09/24/2014</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        6180, Insurance: 6420 Work Comp
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right" width="10%">
                                                                        61,478.72
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        MEDICAL
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        476.60
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        OFFICE EXPENCE-MN
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        413.55
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6110 - Automobile Expence
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        357.67
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6340 - Telephone
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        699.90
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                       6390 - UTILITIES: CABLE
                                                                    </td>
                                                                    <td align="left">
                                                                       &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        254.87
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="90" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="80%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        <div style="padding: 10px 0 3px 30px;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 0;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td align="left" width="10%">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="20" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="80%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        ALURES-CHECKING
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right" width="10%">
                                                                        63,681.31
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--//Top Grid-->
                                    </div>
                                    <!--//top Data-->
                                    <!--Check-->
                                    <div style="width: 940px; float: left; height: 380px; font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;
                                                                margin: 0px 50px 0px; background: url('/company/images/cheque/voucherCheck-bg.jpg') 0px top no-repeat;">
                                        <!--Cheque Part-->
                                        <div style="width: 820px; margin: auto; clear: both; height: 290px; overflow: hidden;
                                            font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;
                                            margin-top: 50px;">
                                            <div style="width: 100%; margin-top: 100px; float: left; height: 20px;">
                                                <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                    height: 20px; width: 660px; float: left; margin-left: 40px; position: relative;
                                                    top: 10px; font-size: 14px;">
                                                    <div id="cmvoAmountText" class="check_fields" style="display:none">
                                                        Sixty- Three Thousand Six Hundred Eighty-one and 31 / 100*******
                                                    </div>
                                                </div>
                                                <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                    height: 20px; width: 70px; font-size: 12px; margin-top: 5px; display: none;">
                                                    DOLLARS</div>
                                            </div>
                                            <div style="width: 300px; margin-top: 30px; float: right; height: 20px; clear: both;">
                                                <div style="height: 20px; float: right;">
                                                    <div style="margin: 0px 60px 0px 0px; width: 100px; text-align: center; float: left;
                                                        position: relative;">
                                                        <div style="padding-top: 0px; font-size: 15px; float: left; height: 20px; padding-bottom: 0px;
                                                            width: 100%; font-weight: 700;">
                                                            <div id="cmvodate" class="check_fields" style="float: right; max-width: 100%; text-align: center; float: left;display:none">
                                                                <table style="letter-spacing: 0px; margin: 0; padding: 0;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            0
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            9
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            /
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            1
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            2
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            /
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            2
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            0
                                                                        </td>
                                                                        <td style="padding: 0 1px; text-align: center; font-stretch: condensed;">
                                                                            1
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            4
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="margin: 0px 0px 0px 0px; width: 115px; text-align: center; float: left;
                                                        position: relative;">
                                                        <div id="cmvoAmountNumeric" class="check_fields" style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                            height: 20px; margin-top: 0px; width: 120px; margin-right: 0px; font-size: 15px;display:none">
                                                            **63,681.31</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-top: 0px; float: left; height: 30px;">
                                                <div style="width: 100%; margin-top: 0px; float: left; font-size: 15px;">
                                                    <div id="cmvoPayeeName" class="check_fields" style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                                        height: 20px; margin-top: 0px; width: 510px; margin-right: 10px; margin-left: 40px;
                                                        font-size: 15px; position: relative; top: -10px;display:none">
                                                        PNC BANK</div>
                                                </div>
                                            </div>

                                            <div style="width: 100%; margin-top: 0px; margin-bottom: 20px; float: left; height: 120px;
                                                position: relative;">
                                                <div style="margin: 0px 0px 0px 0px; width: 300px; font-size: 12px; float: left;
                                                    letter-spacing: 1px; font-weight: bold; font-stretch: condensed; line-height: 16px;
                                                    overflow: hidden; display: none">
                                                    MEMO <span style="padding-left: 5px; font-weight: normal; visibility: hidden;">Memo
                                                        Text</span>
                                                </div>
                                                <div style="margin: 0px 0px 20px 0px; width: 350px; text-align: center; font-size: 12px;
                                                    float: right; position: relative; top: 20px; right: 0px; height: 100px;">
                                                    <div style="width: 100%; float: left;">
                                                        <div id="cmvoSignature" class="check_fields" style="width: 100%; margin: 0px 0 3px 0; padding-bottom: 2px; height: 45px;
                                                            line-height: 30px; float: left;display:none">
                                                            <img src="/company/images/cheque/sig.png" alt="" valign="top">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--//Cheque Part-->
                                    </div>
                                    <!--//Check-->
                                    <!--Bottom Data-->
                                    <div style="width: 940px; float: left; height: 440px; font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;
                                        margin: 0px 50px 0;">
                                        <!--Top Grid-->
                                        <div style="width: 920px; margin: auto; margin-top: 10px; height: 420px; overflow: hidden;
                                            clear: both; background: url('/company/images/cheque/checkBottom-bg.jpg') 0px -370px no-repeat;
                                            background-size: 100%;">
                                            <div style="margin: 10px 0 0; padding: 0; width: 100%; float: left;">
                                                <table style="border-image: none; height: 350px; font-size: 14px; table-layout: fixed;
                                                    font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <th style="padding: 2px 5px; text-align: left;" height="20">
                                                            &nbsp;
                                                        </th>
                                                        <th style="padding: 2px 5px; text-align: left;">
                                                            &nbsp;
                                                        </th>
                                                        <th style="padding: 2px 5px;" align="right" width="120">
                                                            <div style="font-size: 15px; display: none;">
                                                                39608</div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="145" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="90%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 30px;">
                                                                            PNC BANK</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 0;">
                                                                            09/24/2014</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        6180, Insurance: 6420 Work Comp
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right" width="10%">
                                                                        61,478.72
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        MEDICAL
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        476.60
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        OFFICE EXPENCE-MN
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        413.55
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6110 - Automobile Expence
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        357.67
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6340 - Telephone
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        699.90
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6390 - UTILITIES: CABLE
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        254.87
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="90" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="80%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        <div style="padding: 10px 0 3px 30px;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 0;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td align="left" width="10%">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="20" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="80%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        ALURES-CHECKING
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right" width="10%">
                                                                        63,681.31
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--//Top Grid-->
                                    </div>
                                    <!--//Bottom Data-->
                                </div>
                                <div class="bottom_style common_check" id="divcheckBottom" style="width: 100%; float: left; display: none;">
                                    <!--top Part Data-->
                                    <div style="width: 940px; float: left; height: 800px; font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;
                                margin: 30px 50px 0; background: url('/company/images/cheque/checkBottom-bg.jpg') 0px top no-repeat;">
                                        <!--Top Grid-->
                                        <div style="width: 900px; margin: auto; margin-top: 20px; height: 740px; overflow: hidden;
                                    clear: both;">
                                            <div style="margin: 10px 0 0; padding: 0; width: 100%; float: left;">
                                                <table style="border-image: none; height: 350px; font-size: 14px; table-layout: fixed;
                                            font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <th style="padding: 2px 5px; text-align: left;" height="20">
                                                            &nbsp;
                                                        </th>
                                                        <th style="padding: 2px 5px; text-align: left;">
                                                            &nbsp;
                                                        </th>
                                                        <th style="padding: 2px 5px;" align="right" width="120">
                                                            <div style="font-size: 15px; display: none;">
                                                                39608</div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="145" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="90%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 30px;">
                                                                            PNC BANK</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 0;">
                                                                            09/24/2014</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        6180, Insurance: 6420 Work Comp
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right" width="10%">
                                                                        61,478.72
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        MEDICAL
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        476.60
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        OFFICE EXPENCE-MN
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        413.55
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6110 - Automobile Expence
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        357.67
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6340 - Telephone
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        699.90
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6390 - UTILITIES: CABLE
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        254.87
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="90" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="80%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        <div style="padding: 10px 0 3px 30px;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 0;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td align="left" width="10%">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="20" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="80%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        ALURES-CHECKING
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right" width="10%">
                                                                        63,681.31
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div style="margin: 10px 0 0; padding: 0; width: 100%; float: left;">
                                                <table style="border-image: none; height: 350px; font-size: 14px; table-layout: fixed;
                                            font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <th style="padding: 2px 5px; text-align: left;" height="20">
                                                            &nbsp;
                                                        </th>
                                                        <th style="padding: 2px 5px; text-align: left;">
                                                            &nbsp;
                                                        </th>
                                                        <th style="padding: 2px 5px;" align="right" width="120">
                                                            <div style="font-size: 15px; display: none;">
                                                                39608</div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="145" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="90%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 30px;">
                                                                            PNC BANK</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 0;">
                                                                            09/24/2014</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        6180, Insurance: 6420 Work Comp
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right" width="10%">
                                                                        61,478.72
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        MEDICAL
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        476.60
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        OFFICE EXPENCE-MN
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        413.55
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6110 - Automobile Expence
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        357.67
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6340 - Telephone
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        699.90
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        6390 - UTILITIES: CABLE
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        254.87
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="90" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="80%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        <div style="padding: 10px 0 3px 30px;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td align="left">
                                                                        <div style="padding: 10px 0 3px 0;">
                                                                            &nbsp;</div>
                                                                    </td>
                                                                    <td align="left" width="10%">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right">
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="20" align="center" valign="top">
                                                            <table style="font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;" border="0" cellpadding="0" cellspacing="0" width="80%">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" width="60%">
                                                                        ALURES-CHECKING
                                                                    </td>
                                                                    <td align="left">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td align="right" width="10%">
                                                                        63,681.31
                                                                    </td>
                                                                </tr>
                                                               </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--//Top Grid-->
                                    </div>
                                    <!--//top Part Data-->
                                    <!--Check-->
                                    <div style="width: 940px; float: left; height: 380px; font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;
                                                            margin: 0px 50px 30px; background: url('/company/images/cheque/voucherCheck-bg.jpg') 0px top no-repeat;">
                                        <!--Cheque Part-->
                                        <div style="width: 820px; margin: auto; clear: both; height: 290px; overflow: hidden;
                                    font-family: Arial, Gill Sans,Gill Sans MT,Myriad Pro,DejaVu Sans Condensed, Helvetica,sans-serif;
                                    margin-top: 50px;">
                                            <div style="width: 100%; margin-top: 100px; float: left; height: 20px;">
                                                <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                            height: 20px; width: 660px; float: left; margin-left: 40px; position: relative;
                                            top: 10px; font-size: 14px;">
                                                    <div id="cbvoAmountText" class="check_fields" style="display:none; visibility: visible;">
                                                        Sixty- Three Thousand Six Hundred Eighty-one and 31 / 100*******
                                                    </div>
                                                </div>
                                                <div style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed;
                                            height: 20px; width: 70px; font-size: 12px; margin-top: 5px; display: none;">
                                                    DOLLARS</div>
                                            </div>
                                            <div style="width: 300px; margin-top: 30px; float: right; height: 20px; clear: both;">
                                                <div style="height: 20px; float: right;">
                                                    <div style="margin: 0px 60px 0px 0px; width: 100px; text-align: center; float: left;
                                                position: relative;">
                                                        <div style="padding-top: 0px; font-size: 15px; float: left; height: 20px; padding-bottom: 0px;
                                                    width: 100%; font-weight: 700;">
                                                            <div id="cbvodate" class="check_fields" style="max-width: 100%; text-align: center; float: left; display:none">
                                                                <table style="letter-spacing: 0px; margin: 0; padding: 0;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            0
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            9
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            /
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                           1
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            2
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            /
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            2
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            0
                                                                        </td>
                                                                        <td style="padding: 0 1px; text-align: center; font-stretch: condensed;">
                                                                            1
                                                                        </td>
                                                                        <td style="padding: 0px 1px; text-align: center; font-stretch: condensed;">
                                                                            4
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="margin: 0px 0px 0px 0px; width: 115px; text-align: center; float: left;
                                                position: relative;">
                                                        <div id="cbvoAmountNumeric" class="check_fields" style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed; height: 20px; margin-top: 0px; width: 120px; margin-right: 0px; font-size: 15px; display:none">
                                                            **63,681.31</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-top: 0px; float: left; height: 30px;">
                                                <div style="width: 100%; margin-top: 0px; float: left; font-size: 15px;">
                                                    <div id="cbvoPayeeName" class="check_fields" style="letter-spacing: 0.5px; font-weight: bold; float: left; font-stretch: condensed; height: 20px; margin-top: 0px; width: 510px; margin-right: 10px; margin-left: 40px; font-size: 15px; position: relative; top: -10px; display:none">
                                                        PNC BANK</div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-top: 10px; float: left; height: 60px; display: none;">
                                                &nbsp;
                                                <div style="margin: 10px 0px 0px 60px; width: 300px; font-size: 12px; font-weight: 700;
                                            float: left; display: inline-block;">
                                                    <div style="float: left;">
                                                        PNC BANK
                                                        <br>
                                                        P.O. BOX 856177
                                                        <br>
                                                        LOUISVILLE, KY 40285-6177
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; margin-top: 0px; margin-bottom: 20px; float: left; height: 120px;
                                        position: relative;">
                                                <div style="margin: 0px 0px 0px 0px; width: 300px; font-size: 12px; float: left;
                                            letter-spacing: 1px; font-weight: bold; font-stretch: condensed; line-height: 16px;
                                            overflow: hidden; display: none">
                                                    MEMO <span style="padding-left: 5px; font-weight: normal; visibility: hidden;">Memo
                                                Text</span>
                                                </div>
                                                <div style="margin: 0px 0px 20px 0px; width: 350px; text-align: center; font-size: 12px;
                                            float: right; position: relative; top: 20px; right: 0px; height: 100px;">
                                                    <div style="width: 100%; float: left;">
                                                        <div id="cbvoSignature" class="check_fields"  style="width: 100%; margin: 0px 0px 3px; padding-bottom: 2px; height: 45px; line-height: 30px; float: left; display:none">
                                                            <img src="/company/images/cheque/sig.png" alt="" valign="top">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--//Cheque Part-->
                                    </div>
                                    <!--//Check-->
                                </div>
                            </div>
                        </div>
                </div>

            </div>

        </div>
    </div>
</div>

 

 

 

 

 

<script>



   /*
   Function to print envelope
*/
   function PrintElem(elem)
   {
       Popup($(elem).html());
   }

   function Popup(data)
   {
       var base_url = window.location.origin;
       var mywindow = window.open('', 'my div');
       $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
           'font-weight:bold;} .right-detail{\n' +
           '        position:relative;\n' +
           '        left:+400px;\n' +
           '    }</style>');
       $(mywindow.document.body).html( '<body>' + data + '</body>');
       mywindow.document.close();
       mywindow.focus(); // necessary for IE >= 10
       if(mywindow.print())
       {
        alert('hi');
       }

       if(mywindow.close()){

       }
       $("#CheckModal").modal('hide');
       return true;
   }

 

</script>

<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/accounting/checkRegister.js">
    


</script>
<script> var date = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>

<!-- Wrapper Ends -->

<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>