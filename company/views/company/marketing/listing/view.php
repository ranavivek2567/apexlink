<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">


    <!-- MAIN Navigation Starts -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/marketing/layouts/leftBar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">

                    <div class="content-rt">
                        <!--- Right Quick Links ---->


                        <!--- Right Quick Links ---->

                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Marketing >> <span>Listing</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="content-data">
                            <!-- Main tabs -->
                            <!--Tabs Starts -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="/Marketing/MarketingListing">Listing</a></li>
                                    <li role="presentation"><a href="/Marketing/MarketingPropertiesMap">Map</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#market-listing-sub1" aria-controls="home" role="tab" data-toggle="tab">View Details</a></li>
                                        <li role="presentation"><a href="#market-listing-sub2" aria-controls="profile" role="tab" data-toggle="tab">Units</a></li>
                                        <li role="presentation"><a href="#market-listing-sub3" aria-controls="profile" role="tab" data-toggle="tab">Analytics</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="market-listing-sub1">

                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Listing Information <a class="back pull-right" href="/Marketing/MarketingListing"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                    </h3>
                                                </div>
                                                <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Listing Type :</label>
                                                                <span id="property_for_sale"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Posting Title :</label>
                                                                <span class="posting_type">Avail The Vacant Units</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Property Style :</label>
                                                                <span class="property_style">Double-wide Mobile Home</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Address :</label>
                                                                <span class="address1">Test Address 1</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">City :</label>
                                                                <span class="city">Alexandria</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">State Province :</label>
                                                                <span class="state">Oh</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Country :</label>
                                                                <span class="country">United States</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Parking :</label>
                                                                <span class="parkingClass"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Year Built :</label>
                                                                <span class="property_year">2013</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">No. of Units :</label>
                                                                <span class="no_of_units">50</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">Vacant Units :</label>
                                                                <span class="vacant">15</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">Property Type :</label>
                                                                <span class="property_type">Commercial</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Pet Friendly :</label>
                                                                <span class="pet_friendly">No</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">No. of Buildings :</label>
                                                                <span class="no_of_buildings">2</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Description :</label>
                                                                <span class="description"> </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edit-foot">
                                                    <a href="javascript:;">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>

                                            </div>
                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Amenities</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-12 amentiesDiv">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edit-foot">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Section Two Ends-->

                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Listing Agent</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Tenants</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Name</th>
                                                                                                <th scope="col">Contact No.</th>
                                                                                                <th scope="col">Email</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody class="tenantTrData">

                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edit-foot">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Section Three Ends-->

                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Media </h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="grid-outer listinggridDiv">
                                                                <div class="apx-table apxtable-bdr-none">
                                                                    <div class="table-responsive">
                                                                        <table id="propertPhotovideos-table" class="table table-bordered">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edit-foot">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- Section Three Ends-->

                                        </div>

                                        <!-- Tab One Ends -->

                                        <div role="tabpanel" class="tab-pane" id="market-listing-sub2">
                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Units</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Publish</th>
                                                                                                <th scope="col">Unit</th>
                                                                                                <th scope="col">Monthly Rent</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Amenities</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody class="unitLisitng">

                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>

                                                                                    <div id="pagination">

                                                                                    </div>
                                                                                </div>


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="edit-foot">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tab Two Ends -->


                                        <div role="tabpanel" class="tab-pane" id="market-listing-sub3">
                                            <div class="form-outer form-outer2">
                                                <div class="form-hdr">
                                                    <h3>Map</h3>
                                                </div>
                                                <div class="form-data">
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                            <label>Days</label>
                                                            <select class="form-control"><option>30</option></select>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tab Three Ends -->

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- Content Data Ends ---->
                </div>
            </div>
        </div>
</div>
</section>
</div>
<!-- Wrapper Ends -->

<!-- Footer Ends -->


<!-- Jquery Starts -->

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.twbsPagination.min.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/marketing/listing/view.js" type="text/javascript"></script>

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>