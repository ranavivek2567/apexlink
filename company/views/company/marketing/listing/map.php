<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
    <style>
        ul, li {
            list-style-type: none !important;
        }


    </style>
    <div id="wrapper">

        <!-- MAIN Navigation Starts -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- MAIN Navigation Ends -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row flex">
                    <?php
                    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/marketing/layouts/leftBar.php");
                    ?>

                    <div class="col-sm-8 col-md-10 main-content-rt">
                        <div class="content-rt">
                            <!--- Right Quick Links ---->


                            <!--- Right Quick Links ---->

                            <div class="bread-search-outer">
                                <div class="col-sm-8">
                                    <div class="breadcrumb-outer">
                                        Marketing >> <span>Map</span>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="content-data">
                                <!-- Main tabs -->
                                <!--Tabs Starts -->
                                <div class="main-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation"><a href="/Marketing/MarketingListing">Listing</a></li>
                                        <li role="presentation" class="active"><a href="/Marketing/MarketingPropertiesMap">Map</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">

                                        <!-- First Tab Ends -->

                                            <div id="map" style="height: 400px;width: 100%;"></div>

                                        <!-- Second Tab Ends -->

                                    </div>
                                </div>


                            </div>
                            <!-- Content Data Ends ---->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Wrapper Ends -->


    <!-- Footer Ends -->
    <script>
    </script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/marketing/listing/map.js"></script>
    <script language="javascript" src="//maps.google.com/maps/api/js?&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww&callback=initMap"></script>

    <!-- Jquery Starts -->
    <script>

        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });



    </script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>