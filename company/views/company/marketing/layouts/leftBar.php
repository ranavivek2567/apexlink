<style>

    #LeftMenu a {
        padding: 13px 15px;
    }
</style>

<div class="col-sm-4 col-md-2 main-content-lt">
    <div class="left-links slide-toggle"><i class="fa fa-caret-right" aria-hidden="true"></i></div>
    <div id="LeftMenu" class="box">
        <div class="list-group panel">

            <!-- One  Starts-->
            <a href="/Marketing/MarketingListing" class="list-group-item list-group-item-success strong collapsed">Listing</a>

            <!-- One Ends-->

            <!-- Two Starts-->
            <a href="/Marketing/marketingFlyer" class="list-group-item list-group-item-success strong">Flyer</a>

            <!-- Two Ends-->

            <!-- Three Starts-->
            <a href="/Marketing/marketingCampaigns" class="list-group-item list-group-item-success strong collapsed">Campaign</a>

            <!-- Three Ends-->

            <!-- Four Starts-->
            <a href="/Marketing/marketingWebsiteSettings" class="list-group-item list-group-item-success strong collapsed">Settings</a>
            <!-- Four Ends-->

        </div>


    </div>
</div>