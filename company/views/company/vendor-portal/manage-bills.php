<?php

if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>

<div id="wrapper">
    <?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                    <div class="content-section">

                            <div  style="float: right;    padding: 13px;">
                                <label> </label>
                                <a href="/VendorPortal/NewBill">
                                    <input type="button" id="updateEmergencyButton" class="blue-btn" value="New Bill"></a>

                            </div>
                        <div class="col-md-12">

                            <div class="accordion-grid">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="grid-outer">
                                                            <div class="apx-table">
                                                                <div class="table-responsive">
                                                                    <table id="paybills_table" class="table table-hover table-dark">
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    
                    </div></div></div>

    </section>
</div>
<!-- Wrapper Ends -->
<script>
var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
var vendor_portal_id  = "<?php echo $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']; ?>";
var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
var upload_url  = "<?php echo SITE_URL; ?>";
var login_user_name =  "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
var amount_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/manageBills.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>

<script>
    $("#manage-bill").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->
</script>