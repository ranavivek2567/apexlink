<?php
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>
    <!-- MAIN Navigation Ends -->
    <section class="main-content view-owner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">

                    </div>
                </div>

                <input type="hidden" id="building_property_id" val=""/>
                <input type="hidden" value="<?php echo $_GET['id'];?>" name="owner_id" class="owner_id">
                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->

                        <div class="main-tabs">
                            <!-- Nav tabs -->

                            <!-- Nav tabs -->

                            <!-- Form Outer Ends -->

                            <!-- Form Outer starts -->
                            <div class="form-outer2">
                                <div class="form-hdr">
                                    <h3>Bill Information</h3>
                                </div>
                                <div class="form-data">
                                    <div class="detail-outer emergency_detail_data">
                                        <div class=row style='margin-bottom: 10px;'>
                                            <div class=col-sm-6>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Vendor Name : </label>
                                                    <span class="name"></span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Bill Date :</label>
                                                    <span class="bill_date"> </span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Amount : </label>
                                                    <span class="amount"></span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Terms : </label>
                                                    <span class="term"></span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>WorkOrderNumber : </label>
                                                    <span class="work_order"></span>
                                                </div>
                                            </div>
                                            <div class=col-sm-6>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Bill Recurrence : </label>
                                                    <span class="due_date"></span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Due Date :</label>
                                                    <span class="due_date"> </span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Memo : </label>
                                                    <span class="memo"></span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Reference : </label>
                                                    <span class="refrence_number"></span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Invoice Number : </label>
                                                    <span class="invoice_number"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer starts -->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Split Bill</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="apx-table listinggridDiv">
                                                                <div class="table-responsive">
                                                                    <table id="splitbills-table" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Form Outer Ends -->


                            <!-- Form Outer Ends -->

                            <!-- Form Outer Ends -->

                            <!-- Form Outer Starts -->



                            <!-- Form Outer starts -->
                            <div class="form-outer" id="OwnerNotes">
                                <div class="form-hdr">
                                    <h3>Notes</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="vendor_table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer Ends -->
                            <div class="form-outer" id="filelibrary">
                                <div class="form-hdr">
                                    <h3>File Library</h3>
                                </div>
                                <input type="hidden" class="vendor_edit_id"/>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="fileLibraryTable" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!--tab Ends -->
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->


    <!-- SLider -->
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            autoplay:false,
            autoplayTimeout:1500,
            autoplayHoverPause:true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: true
                },
                1020: {
                    items: 2,
                    nav: true,
                    loop: true,
                    margin: 20
                },
                1199: {
                    items: 3,
                    nav: true,
                    loop: true,
                    margin: 20
                }
            }
        })
    })
    var upload_url = "<?php echo SITE_URL; ?>";
    function goBack() {
        window.history.back();
    }
    $('#people_top').addClass('active');
    var vendor_portal_id  = "<?php echo $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']; ?>";
    var vendor_unique_id=vendor_portal_id;
    $(".vendor_edit_id").val(vendor_portal_id);
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var amount_symbol = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'] : $_SESSION['Admin_Access']['default_currency_symbol'] ?>';
</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/manageBills.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/vendor/vendorFileLibratry.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/vendor/vendor_notes.js" type="text/javascript"></script>


<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php"); ?>
