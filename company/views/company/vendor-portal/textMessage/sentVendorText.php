<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
//if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
//    $url = SUBDOMAIN_URL;
//    header('Location: ' . $url);
//}
if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>
<?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>
<section class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="bread-search-outer">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="breadcrumb-outer">
                            Communication &gt;&gt; <span>Text Message </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-data">
<!--                --><?php //include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                <!--Tabs Starts -->
                <div class="main-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation"><a href="/VendorPortal/Vendor/Communication/SentEmails">Email</a></li>
                        <li role="presentation" class="active"><a href="/VendorPortal/Communication/TextMessage">Text Message</a></li>
                    </ul>

                    <!-- Tab panes -->

                    <div class="tab-content">
                        <div class="panel-heading">

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="receivables">
                                    <div class="property-status">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="btn-outer text-right">
                                                    <a href="/VendorPortal/Communication/InboxText" class="blue-btn margin-right">Inbox</a>
                                                    <a href="/VendorPortal/Communication/TextMessageDrafts" class="blue-btn margin-right">Drafts</a>
                                                    <a data-urltype="" href="/VendorPortal/Communication/AddTextMessage" class="blue-btn margin-right compose-text">Compose Message</a>
                                                    <!--<button class="blue-btn">Sent Mails</button>
                                                    <button class="blue-btn">Draft Mails</button>
                                                    <button class="blue-btn">Compose Mail</button>-->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-outer" >
                                        <div class="form-hdr">
                                            <h3>Sent Messages</h3>
                                        </div>
                                        <div class="form-data apx-table">
                                            <div class="table-responsive">
                                                <table id="sentvendormails-table" class="table table-bordered"></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Regular Rent Ends -->

                            </div>
                        </div>
                        <!-- Sub tabs ends-->
                    </div>



                    <!-- Sub Tabs Starts-->

                    <!-- Sub tabs ends-->
                </div>

            </div>
        </div>

        <!--Tabs Ends -->

    </div>
    </div>
    </div>
</section>
</div>
<div class="modal fade" id="sentMailModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <a class="close closeAnnouncementmodal closeSystemAnnouncement" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i></a>

                <a  href="javascript:void(0)"><h4>Sent Message </h4></a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <strong>From: </strong>
                        <span class="modal_from"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>To: </strong>
                        <span class="modal_to"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>Subject: </strong>
                        <span class="modal_subject"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>Message: </strong>
                        <span class="modal_message"></span>
                    </div>
                    <div class="col-sm-12"></div>
                    <input type="hidden" id="compose_mail_id">
                    <div class="col-sm-12 text-right">
                        <a class="blue-btn forward-email-btn">Forward</a>
                        <a class="grey-btn" id="cancel_communication" href="javascript:void(0)">
                            Cancel
                        </a>
                        <a class="blue-btn" id="message_recall" href="javascript:void(0)">
                            Recall
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication").addClass("active");
    });
</script>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/common.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/textMessage/sent-vendor-text.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

</body>

</html>
