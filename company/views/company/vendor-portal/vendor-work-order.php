<?php

if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>

    <div id="wrapper">
        <?php  include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php"); ?>
        
        
        <section class="main-content">
          <div class="container-fluid">
            <div class="row">                   
              <div class="col-sm-12">
                <div class="content-section">
                    <div class="main-tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="/VendorPortal/Vendor/WorkOrderVendor" aria-controls="home" role="tab" data-toggle="tab">Open</a></li>
                        <li role="presentation"><a href="#">Closed</a></li>
                        <li role="presentation"><a href="/VendorPortal/LostAndFoundVendor/LostAndFound">Lost and Found Manager</a></li>
                        <li role="presentation"><a href="/VendorPortal/Vendor/PurchaseOrderListing">Purchase Order</a></li>
                    </ul>
                </div>

                    <div class="">
                        <div class="property-status">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="check-outer">
                                        <input name="alt" type="radio" class="common_radio" id="one_time" value="0" checked>
                                        <label>One Time</label>
                                    </div>
                                    <div class="check-outer" >
                                        <input name="alt" type="radio" class="common_radio" id="recurring" value="1">
                                        <label>Recurring</label>
                                    </div>
                                    <div class="col-sm-4" id="filter_div" style="display: none">
                                        <select id="ddlWorkOrderListingFrequency" class="common_radio form-control select-dd">
                                            <option value="all">Select</option>
                                            <option value="Daily">Daily</option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Bi-Weekly">Bi-Weekly</option>
                                            <option value="Monthly">Monthly</option>
                                            <option value="Quarterly">Quarterly</option>
                                            <option value="Semi-Annually">Semi-Annually</option>
                                            <option value="Annually">Annually</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">

                                </div>
                            </div>
                        </div>
                        <div class="accordion-grid">
                            <div class="accordion-outer">
                                <div class="bs-example">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                <div class="panel-body pad-none">
                                                    <div class="apx-table">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table id="workorder_table">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                </div>



                </div>
              </div>
            </div>
          </div>
        </section>

    </div>
        <!-- Wrapper Ends -->


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>

<script>
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var vendor_portal_id = "<?php print_r($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']); ?>";

    $("#work-order").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/openWorkOrder.js"></script>