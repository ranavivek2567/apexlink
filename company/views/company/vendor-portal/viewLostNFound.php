<?php

if ((!isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) && (!isset($_SESSION['Admin_Access']['vendor_portal_id']))) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . '/VendorPortal/login');
}
$satulation_array = ['1' => 'Dr.', '2' => 'Mr.', '3' => 'Mrs.', '4' => 'Mr. & Mrs.', '5' => 'Ms.', 6 => 'Sir', 7 => 'Madam'];
$gender_array = ['1' => 'Male', '2' => 'Female', '3' => 'Prefer Not To Say', '4' => 'Other'];
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
         include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Maintenance &gt;&gt; <span>Lost and Found</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>

                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/VendorPortal/Vendor/WorkOrderVendor">Open</a></li>
                            <li role="presentation"><a href="#">Closed</a></li>
                            <li role="presentation" class="active"><a href="/VendorPortal/LostAndFoundVendor/LostAndFound">Lost and Found Manager</a></li>
                            <li role="presentation"><a href="/VendorPortal/Vendor/PurchaseOrderListing">Purchase Order</a></li>
                        </ul>
                    
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tickets-lost-found">
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs dashboardtabs" role="tablist">
                                        <li role="presentation" class="active dashbrd"><a href="#lostfound-one" aria-controls="home" role="tab" data-toggle="tab">Dashboard</a></li>
                                        <li role="presentation" class="items"><a href="#lostfound-two" aria-controls="profile" role="tab" data-toggle="tab" id="lost1">Items</a></li>

                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="lostfound-one">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Property</label>
                                                        <select class="fm-txt form-control dashboardProperty" multiple>
                                                          
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3 row">
                                                        <label>&nbsp;</label>
                                                        <button onclick="window.location.href='<?php echo SUBDOMAIN_URL ?>/VendorPortal/LostAndFoundVendor/AddEditLostItem'" class="blue-btn">Add Lost</button>
                                                   </div>

                                                </div>
                                            </div>
                                            <input type="hidden" name="itemtype" id="itemtype" value="<?php echo !empty($type)?$type:''; ?>"/>

                                            <div class="lost-found-items">
                                                <div class="lost-found-columns">
                                                    <span class="count lostItemCount">0</span>
                                                    <label>Lost Items</label>
                                                    <span class="item-btn">
                                              <a href="#lostfound-two" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"    class="white-btn" id="showItems1">Show all Items</a>
                                              </span>
                                                </div>

                                                <div class="lost-found-columns">
                                                    <span class="count claimedItemCount">25</span>
                                                    <label>Claimed Items</label>
                                                    <span class="item-btn">
                                          <button class="white-btn" id="showItems3">Show all Items</button>
                                          </span>
                                                </div>
                                                <div class="lost-found-columns">
                                                    <span class="count unclaimedItemCount">71</span>
                                                    <label>Unclaimed Items</label>
                                                    <span class="item-btn">
                                          <button class="white-btn" id="showItems4">Show all Items</button>
                                          </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                               <!-- <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i>-->
                                                                                </span> Lost Items
<!--                                                                                </a>-->
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Date</th>
                                                                                                <th scope="col">Item Type</th>
                                                                                                <th scope="col">Property</th>
                                                                                                <th scope="col">More</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody class="getLostItemList">           
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="lostfound-two">
                                            <div class="sub-tabs">
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs difftabs" role="tablist">
                                                    <li role="presentation" class="active lost"><a href="#lostfound-sub-one" aria-controls="home" role="tab" data-toggle="tab" id="lost1">Lost Items</a></li>
                                                    <li role="presentation" class="found"><a href="#lostfound-sub-two" aria-controls="profile" role="tab" data-toggle="tab" id="lost2">Found Items</a></li>
                                                    <li role="presentation"><a href="#lostfound-sub-three" aria-controls="home" role="tab" data-toggle="tab" id="lost3">Claimed Items</a></li>
                                                    <li role="presentation"><a href="#lostfound-sub-four" aria-controls="profile" role="tab" data-toggle="tab" id="lost4">Unclaimed Items</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="lostfound-sub-one">
                                                        <div class="property-status">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label>Property</label>
                                                                    <select id='lost_property_common' class="common_lost fm-txt form-control lostItemProperty"  multiple>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select id="lost_status_common" class="common_lost fm-txt form-control lost_type">
                                                                        <option value="all">All</option>
                                                                        <option value="1">Active</option>
                                                                        <option value="0">Inactive</option>

                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <label>&nbsp;</label>
                                                                    <input id='lost_common_text' type="text" placeholder="Search by item #, serial#, Desc, Brand Name and Other Details" class="form-control lostKeyword"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn lostSearch">Search</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="accordion-grid">
                                                            <div class="accordion-outer">
                                                                <div class="bs-example">
                                                                    <div class="panel-group" id="accordion">
                                                                        <div class="panel panel-default">

                                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                        <div class="table-responsive">
                                                                                        <div class="apx-table">
                                                                                        <div class="table-responsive">
                                                                                        <table id="lostItem-table" class="table table-bordered">
                                                                                        </table>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="lostfound-sub-two">
                                                        <div class="property-status">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label>Property</label>
                                                                    <select id='found_property_common' class="common_lost fm-txt form-control lostItemProperty"  multiple>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select id="found_status_common" class="common_lost fm-txt form-control lost_type">
                                                                        <option value="all">All</option>
                                                                        <option value="1">Active</option>
                                                                        <option value="0">Inactive</option>

                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <label>&nbsp;</label>
                                                                    <input id='found_common_text' type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control lostKeyword"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn foundSearch">Search</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="accordion-grid">
                                                            <div class="accordion-outer">
                                                                <div class="bs-example">
                                                                    <div class="panel-group" id="accordion">
                                                                        <div class="panel panel-default">

                                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                         <div class="apx-table">
                                                                                        <div class="table-responsive">
                                                                                        <table id="findItem-table" class="table table-bordered">
                                                                                        </table>
                                                                                        </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="lostfound-sub-three">
                                                        <div class="property-status">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label>Property</label>
                                                                    <select id='claimed_property_common' class="common_lost fm-txt form-control lostItemProperty"  multiple>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select id="claimed_status_common" class="common_lost fm-txt form-control lost_type">
                                                                        <option value="all">All</option>
                                                                        <option value="1">Active</option>
                                                                        <option value="0">Inactive</option>

                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <label>&nbsp;</label>
                                                                    <input id='claimed_common_text' type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control lostKeyword"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn claimedSearch">Search</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="accordion-grid">
                                                            <div class="accordion-outer">
                                                                <div class="bs-example">
                                                                    <div class="panel-group" id="accordion">
                                                                        <div class="panel panel-default">

                                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                         <div class="table-responsive">
                                                                                        <div class="apx-table">
                                                                                        <div class="table-responsive">
                                                                                      <table id="claimedItem-table" class="table table-bordered">
                                                                                        </table>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="lostfound-sub-four">
                                                      <div class="property-status">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label>Property</label>
                                                                    <select id='unclaimed_property_common' class="common_lost fm-txt form-control lostItemProperty"  multiple>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select id="unclaimed_status_common" class="common_lost fm-txt form-control lost_type">
                                                                        <option value="all">All</option>
                                                                        <option value="1">Active</option>
                                                                        <option value="0">Inactive</option>

                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <label>&nbsp;</label>
                                                                    <input id='unclaimed_common_text' type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control lostKeyword"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn unclaimedSearch">Search</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="accordion-grid">
                                                            <div class="accordion-outer">
                                                                <div class="bs-example">
                                                                    <div class="panel-group" id="accordion">
                                                                        <div class="panel panel-default">

                                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                         <div class="table-responsive">
                                                                                        <div class="apx-table">
                                                                                        <div class="table-responsive">
                                                                                      <table id="unclaimedItem-table" class="table table-bordered">
                                                                                        </table>
                                                                                        </div>
                                                                                        </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>

</div>
<script>
    var default_Image = "<?php echo COMPANY_SUBDOMAIN_URL ?>/images/profile-img.png";
    var vendor_unique_id = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'] : $_SESSION['Admin_Access']['vendor_portal_id'] ?>';
    var pagination = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['pagination'] : $_SESSION['Admin_Access']['pagination'] ?>';
    var datepicker = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'] : $_SESSION['Admin_Access']['datepicker_format'] ?>';
    var jsDateFomat = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['datepicker_format'] : $_SESSION['Admin_Access']['datepicker_format'] ?>';
    var staticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">';
    var petStaticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">';
    var vendor_id = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['vendor_portal_id'] : $_SESSION['Admin_Access']['vendor_portal_id'] ?>';
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_currency_symbol'] : $_SESSION['Admin_Access']['default_currency_symbol'] ?>';
    var default_name = '<?php echo (isset($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name']) && !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name'])) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['default_name'] : $_SESSION['Admin_Access']['default_name'] ?>';
    var vendorPortalNote = '';
</script>
 <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
 <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/vendor-portal/lostandfound/lostandfound.js"></script>


<!-- Wrapper Ends -->



<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>