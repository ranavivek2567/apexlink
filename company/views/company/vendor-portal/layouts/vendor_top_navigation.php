<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2 visible-xs">

            </div>
            <div class="col-sm-3 col-xs-8">
                <div class="logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL; ?><?php echo!empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['company_logo']) ? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['company_logo'] : $_SESSION['Admin_Access']['company_logo'] ?>" height="60" width="160"></div>
            </div>
            <div class="col-sm-9 col-xs-2">
                <div class="hdr-rt">
                    <!-- TOP Navigation Starts -->

                    <nav class="navbar navbar-default">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="hidden-xs">Welcome: <span id="user_name"><?php echo !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['name'])? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['name'] :  $_SESSION['Admin_Access']['name'] ?></span>, <span id="formated_date"><?php echo !empty($_SESSION[SESSION_DOMAIN]['Vendor_Portal']['formated_date'])? $_SESSION[SESSION_DOMAIN]['Vendor_Portal']['formated_date'] :  $_SESSION['Admin_Access']['formated_date'] ?></span>
                                    <a href="javascript:;" data-toggle="modal" data-target="#myModalCalc" data-backdrop="false" data-keyboard="false"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                                    <span class="company_calendar">
                                             <a href="/Calendar/Calendar"><img src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/calendar_image.png"></a>
                                            <h4 class="clscalendarimg" id="hCalendarDate">
                                                <?php
                                                $t=date('d-m-Y');
                                                echo date("d",strtotime($t));
                                                ?>
                                            </h4>
                                        </span>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" target="_blank" href="https://apexlink.com/kb.php?n=TRUE"><i class="fa fa-question" aria-hidden="true"></i>Help</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="JavaScript:Void(0);"><i class="fa fa-wrench" aria-hidden="true"></i>Support</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link disabled" id="vendor_portal_logout" href="JavaScript:Void(0);"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                </li>
                            </ul>

                        </div><!-- /.navbar-collapse -->
                    </nav>
                    <!-- TOP Navigation Ends -->

                </div>
            </div>
        </div>
    </div>

</header>

<!-- MAIN Navigation Starts -->
<section class="main-nav">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header visible-xs">
            <a id="show" class="icon-bar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <a class="nav-close visible-xs" id="close" href="#"><i class="fa fa-times" aria-hidden="true"></i> </a>
                <li id="my-account"><a href="/VendorPortal/Vendor/MyAccount">My Account</a></li>
                <li id="work-order"><a  href="/VendorPortal/Vendor/WorkOrderVendor">Work Order</a></li>
                <li id="transaction"><a href="/VendorPortal/Vendor/Transactions">Transactions</a></li>
                <li id="file-lib"><a href="/VendorPortal/Vendor/FileRepository">File Library</a></li>
                <li id="vendorinsurance"><a href="/VendorPortal/Vendor/VendorInsurance">Vendor Insurance</a></li>
                <li id="manage-bill"><a href="/VendorPortal/Vendor/ManageBill">Manage Bill</a></li>
                <li id="communication"><a href="/VendorPortal/Vendor/Communication/SentEmails">Communication</a></li>
                <li id="work-reports"><a href="/VendorPortal/Vendor/Reporting/Reporting">Reporting</a></li>
                <a id="listAllAnnouncements" class="listAllAnnouncements pull-right" href="/VendorPortal/Vendor/AllCompanyAdminAnnouncements"><i class="fa fa-bullhorn" aria-hidden="true"></i></a>
            </ul>

        </div><!-- /.navbar-collapse -->
    </nav>
</section>