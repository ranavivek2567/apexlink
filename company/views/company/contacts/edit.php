<?php

/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 07/18/2019
 * Time: 4:19 PM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$satulation_array =['1'=>'Dr.','2'=>'Mr.','3'=>'Mrs.','4'=>'Mr. & Mrs.','5'=>'Ms.',6=>'Sir',7=>'Madam','10'=>'Brother',8=>'Sister','11'=>'Father',9=>'Mother'];
$gender_array =['1'=>'Male','2'=>'Female','3'=>'Prefer Not To Say','4'=>'Other'];
$country_codessss=[];
if(!empty($country_codes)) {
    $country_codessss = unserialize($country_codes);

}

$hobbiesssss=[];
if(!empty($data["hobbies"])) {
    $hobbiesssss = unserialize($data["hobbies"]);
}


$phone_country_codess=[];
if(!empty($phone_country_codes)) {
    $phone_country_codess = unserialize($phone_country_codes);
    if(!empty($phone_country_codess)) {
        $first_country_code = $phone_country_codess[0];
    }else{
        $first_country_code = '';
    }

}

$carrierss=[];
if(!empty($carrier)) {
    $carrierss = unserialize($carrier);
}


?>
<script>
    var credential_notice_period =  "<?php echo $_SESSION[SESSION_DOMAIN]['default_notice_period']; ?>";
    var first_country_code = "<?php echo $first_country_code; ?>";



</script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>  <style>
    .multipleEmail input{margin:5px auto; }
    select[name=hobbies] + .btn-group button{
        margin-top: 0px;
    }
    .closeimagepopupicon{
        float: right;
        font-weight: bold;
        border: 2px solid #666;
        border-radius: 12px;
        padding: 0px 4px;
        cursor: pointer;
    }
    .fa-minus-circle {
        display: none;
    }

    .fa-times-circle {
        display: none;

    }

    .apx-inline-popup {
        position: relative;
    }

    .apx-inline-popup-box {
        position: absolute;
        top: 0;
        left: 0;
        background: #fff;
        z-index: 2;
        border: 1px solid #ddd;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
        padding: 15px;
        width: 100%;
    }

    .apx-inline-popup-box > h4 {
        font-size: 12px
    }

    .image-editor {
        text-align: center;
    }

    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
        display: inline-block;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .cropit-preview-background {
        opacity: .2;
        cursor: auto;
    }

    .image-size-label {
        margin-top: 10px;
    }

    .export {
        /* Use relative position to prevent from being covered by image background */
        position: relative;
        z-index: 10;
        display: block;
    }

    .image-editor input[type="file"] {
        opacity: 0;
        position: absolute;
        top: 45px;
        left: 91px;
    }

    .upload-logo .img-outer img {
        max-width: 100%;
    }

    .choose-img {
        margin: 0 !important;
    }

    /*button {*/
    /*margin-top: 10px;*/
    /*}*/

    .vehicle_image1, .vehicle_image2, .vehicle_image3 {
        width: 100px;
        height: 100px;
    }

    .vehicle_image1 img, .vehicle_image2 img, .vehicle_image3 img {
        width: 100%;
    }
    .capital{text-transform: capitalize;}
</style>
<body>
<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                People Module >> <span>Contact Edit</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <form id="addEmployee" enctype='multipart/form-data'>
                    <input type="hidden" id="employee_edit_id" value="">

                    <div class="col-sm-12">
                        <div class="content-section">
                            <!--Tabs Starts -->
                            <div class="main-tabs">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                                    <li role="presentation"><a href="/People/Ownerlisting">Owners</a></li>
                                    <li role="presentation"><a href="/Vendor/Vendor" >Vendors</a></li>
                                    <li class="active" role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                                    <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                                </ul>
                                <!-- Nav tabs -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Contact Information <a class="back" href="javascript:;" onclick="goBack()"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                            </div>
                                            <div class="form-data" id="info">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Salutation</label>

                                                        <select class="form-control" id="salutation" name="salutation">
                                                            <option value="">Select</option>
                                                            <?php foreach ($satulation_array as $key => $value) {?>

                                                                <option value="<?= $key?>" <?= ($key == $data["salutation"])? 'selected = selected':'' ?> ><?php echo  $value;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>First Name <em class="red-star">*</em></label>
                                                        <input class="form-control" value="<?= $data['first_name']?>"  id="firstname" placeholder="First Name" name="firstname" maxlength="50" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Middle Name</label>
                                                        <input class="form-control" id="middlename" value="<?= $data['middle_name']?>" placeholder="Middle Name" name="middlename" maxlength="50" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Last Name <em class="red-star">*</em></label>
                                                        <input class="form-control" id="lastname" value="<?= $data['last_name']?>" placeholder="Last Name" name="lastname" maxlength="50" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3 hidemaiden">
                                                        <label>Maiden Name</label>
                                                        <input class="form-control" id="maidenname" value="<?= $data['maiden_name']?>" placeholder="Maiden Name" name="maidenname" maxlength="50"  type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Nick Name</label>
                                                        <input class="form-control" id="nickname"
                                                               name="nickname" placeholder="Nick Name" value="<?= $data['nick_name']?>" maxlength="50" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Gender</label>
                                                        <select id="gender" name="gender"   class="form-control"><option value="">Select</option>
                                                            <?php
                                                            foreach( $gender_array as $key => $value){ ?>
                                                                <option value="<?= $key?>"  <?= ($key == $data["gender"])? 'selected = selected':'' ?>><?php echo  $value;?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>SSN/SIN/ID</label>
                                                        <?php  if( !empty($data["ssn_sin_id"])){
                                                            foreach($data["ssn_sin_id"] as $key => $value) { ?>
                                                                <div class='multipleSsn'>

                                                                    <input class="form-control add-input" type="text"
                                                                           id="ssn" name="ssn[]" maxlength="50" style="margin: 5px 0;" value="<?=$value ?>">
                                                                    <a style="display:<?php if($key == 0) { echo 'none';}?>" class="add-icon ssn-remove-sign" href="javascript:;" ><i  class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <a style="display:<?php if($key > 0){ echo 'none';}?>" class="add-icon ssn-plus-sign" href="javascript:;"><i
                                                                                class="fa fa-plus-circle"
                                                                                aria-hidden="true"></i></a>

                                                                </div>
                                                            <?php }
                                                        }else{
                                                            ?>
                                                            <div class='multipleSsn'>

                                                                <input class="form-control add-input" maxlength="50" type="text"
                                                                       id="ssn" name="ssn[]" style="margin: 5px 0;">
                                                                <a class="add-icon ssn-remove-sign" href="javascript:;"
                                                                   style="display:none"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                                                <span class="ffirst_nameErr error red-star"></span>

                                                                <a class="add-icon ssn-plus-sign" href="javascript:;"><i
                                                                            class="fa fa-plus-circle"
                                                                            aria-hidden="true"></i></a>
                                                            </div>
                                                        <?php }?>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Birth Date</label>
                                                        <input class="form-control" placeholder="Birth Date" id="date_of_birth"
                                                               name="date_of_birth" readonly type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Zip/Postal Code</label>
                                                        <input value="<?= $data['zipcode']?>" id="zip_code" name="zip_code" maxlength="50" value="" placeholder="Eg: 35801" class="form-control" />
                                                        <span class="zip_codeErr error"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Country</label>
                                                        <input id="country" value="<?= $data['country']?>" placeholder="Eg: US" maxlength="50" name="country" class="form-control" />
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>State/Province</label>
                                                        <input id="states" value="<?= $data['state']?>" maxlength="100"  placeholder="Eg: AL" name="state" class="form-control"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>City</label>
                                                        <input id="citys" value="<?= $data['city']?>" name="city" maxlength="50"  placeholder="Eg: Huntsville" class="form-control" />
                                                    </div>

                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 1</label>
                                                        <input class="form-control" value="<?= $data['address1']?>" maxlength="250" id="address1" placeholder="Eg: Street address 1" name="address1" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 2</label>
                                                        <input class="form-control" value="<?= $data['address2']?>" id="address2" maxlength="250" placeholder="Eg: Street address 2" name="address2" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 3</label>
                                                        <input class="form-control" id="address3" value="<?= $data['address3']?>" maxlength="250" placeholder="Eg: Street address 3" name="address3" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Address 4</label>
                                                        <input class="form-control" value="<?= $data['address4']?>" placeholder="Eg: Street address 4" maxlength="250" id="address4" name="address4" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Referral Source <a class="pop-add-icon additionalReferralResource" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <select class="form-control"  name="additional_referralSource" id="additional_referralSource" ><option>Mobile</option></select>
                                                        <div class="add-popup" id="additionalReferralResource1">
                                                            <h4>Add New Referral Source</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Referral Source <em class="red-star">*</em></label>
                                                                        <input class="form-control reff_source1 customReferralSourceValidation" data_required="true" type="text" placeholder="New Referral Source">
                                                                        <span class="customError required"></span>
                                                                        <span class="red-star" id="reff_source1"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-validation-class="customReferralSourceValidation" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</button>
                                                                        <button type="button"  class="clear-btn referal_source_clear">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <?php
                                                if(!empty($tenant_phone)){
                                                    foreach($tenant_phone as $key => $value){ ?>
                                                        <div class="row clone-row primary-tenant-phone-row">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Phone Type</label>
                                                                <select class="form-control additional_phoneType" name="additional_phoneType[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1" <?php echo  ($value["phone_type"] == 1 ?"selected":"")?>>mobile</option>
                                                                    <option value="2" <?php echo  ($value["phone_type"] == 2 ?"selected":"")?>>work</option>
                                                                    <option value="3" <?php echo  ($value["phone_type"] == 3 ?"selected":"")?>>Fax</option>
                                                                    <option value="4" <?php echo  ($value["phone_type"] == 4 ?"selected":"")?>>Home</option>
                                                                    <option value="5" <?php echo  ($value["phone_type"] == 5 ?"selected":"")?>>Other</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Phone Number <em class="red-star">*</em></label>
                                                                <input class="form-control phone_format customPhonenumbervalidations" data_required="true" placeholder="Number" name="additional_phone[]" type="text" value="<?= $value['phone_number'];?>"/>
                                                                <span class="error red-star customError"></span>
                                                            </div>
                                                            <?php if($value["phone_type"] == 2 ||  $value["phone_type"] == 5  ){

                                                                ?>
                                                                <div class="work_extension_div" >
                                                                    <div class="col-sm-1 col-md-1">
                                                                        <label>Extension</label>
                                                                        <input class="form-control number_only_for_extension" maxlength="15" type="text" id="other_work_phone_extension" name="other_work_phone_extension[]" value="<?= $value['other_work_phone_extension'];?>" placeholder="Eg: + 161">
                                                                        <span class="other_work_phone_extensionErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            <?php } else{ ?>
                                                                <div class="work_extension_div"  style="display: none;">
                                                                    <div class="col-sm-1 col-md-1">
                                                                        <label>Extension</label>
                                                                        <input class="form-control number_only_for_extension" maxlength="15" type="text" id="other_work_phone_extension" name="other_work_phone_extension[]"  placeholder="Eg: + 161">
                                                                        <span class="other_work_phone_extensionErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>

                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Carrier <em class="red-star">*</em></label>
                                                                <select class="form-control additional_carrier customValidationCarrier select_carrier_<?=$key ?>"
                                                                        name="additional_carrier[]" data_required="true"></select>
                                                                <span class="term_planErr error red-star customError"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Country Code</label>
                                                                <select name="additional_countryCode[]" id="additional_country" class="add-input form-control additional_country select_country_code_<?=$key ?>">
                                                                    <option value="0"></option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                                <a class="add-icon" href="javascript:;"  ><i style=" display:<?php echo ($key == 0 && count($tenant_phone) < 3) ? 'block' : 'none'; ?>"
                                                                            class="fa fa-plus-circle"
                                                                            aria-hidden="true"></i></a>
                                                                <a class="add-icon" href="javascript:;" ><i style=" display:<?php echo ($key > 0) ? 'block' : 'none'; ?>"
                                                                            class="fa fa-times-circle red-star"
                                                                            aria-hidden="true"></i></a>
                                                            </div>
                                                        </div>
                                                    <?php } } else {?>
                                                    <div class="row clone-row primary-tenant-phone-row">
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Phone Type</label>
                                                            <select class="form-control additional_phoneType" name="additional_phoneType[]">
                                                                <option value="">Select</option>
                                                                <option value="1" selected>Mobile</option>
                                                                <option value="2">Work</option>
                                                                <option value="3">Fax</option>
                                                                <option value="4">Home</option>
                                                                <option value="5">Other</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Phone Number <em class="red-star">*</em></label>
                                                            <input class="form-control phone_format" placeholder="Number" name="additional_phone[]" type="text"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Carrier <em class="red-star">*</em></label>
                                                            <select class="form-control additional_carrier customValidationCarrier"
                                                                    name="additional_carrier[]" data_required="true"></select>
                                                            <span class="term_planErr error red-star customError"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Country Code</label>
                                                            <select name="additional_countryCode[]" id="additional_country" class="add-input form-control additional_country">
                                                                <option value="0"></option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                            <a class="add-icon" href="javascript:;"><i
                                                                        class="fa fa-plus-circle"
                                                                        aria-hidden="true"></i></a>
                                                            <a class="add-icon" href="javascript:;" style=""><i
                                                                        class="fa fa-times-circle red-star"
                                                                        aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>

                                                <?php }?>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Add Note for this Phone Number </label>
                                                        <input class="form-control add-input capital firstletter" maxlength="250" value="<?= $data['phone_number_note']?>"  placeholder="Add Note for this Phone Number" id="add_note_phone_number" name="add_note_phone_number" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Email <em class="red-star">*</em></label>
                                                        <?php
                                                        $email_count=count($email_array);

                                                        if(!empty($email_array)) {
                                                            if($email_count == 1){ ?>
                                                                <div class='additional_multipleEmail'>
                                                                    <input class="form-control add-input customadditionalEmailValidation"
                                                                           placeholder="Email" maxlength="50"
                                                                           data_required="true" data_email
                                                                           type="text"
                                                                           name="additional_email[]"
                                                                           value="<?= $data['email'] ?>">

                                                                    <a style=" display:block"
                                                                       class="add-icon additional_email-plus-sign"
                                                                       href="javascript:;"><i
                                                                                class="fa fa-plus-circle"
                                                                                aria-hidden="true"></i></a>

                                                                    <a style=" display:none"
                                                                       class="add-icon additional_email-remove-sign"
                                                                       href="javascript:;">
                                                                        <i class="fa fa-times-circle red-star"
                                                                           aria-hidden="true"
                                                                           style="display: inline;"></i>
                                                                    </a>
                                                                    <span class="customError required"></span>
                                                                </div>

                                                            <?php }else {
                                                                foreach ($email_array as $key => $value) { ?>
                                                                    <div class='additional_multipleEmail'>
                                                                        <input class="form-control add-input customadditionalEmailValidation"
                                                                               placeholder="Email" maxlength="50"
                                                                               data_required="true" data_email
                                                                               type="text"
                                                                               name="additional_email[]"
                                                                               value="<?= $value ?>">

                                                                        <a style=" display:<?php echo ($key == 0 && $email_count < 3) ? 'block' : 'none'; ?>"
                                                                           class="add-icon additional_email-plus-sign"
                                                                           href="javascript:;"><i
                                                                                    class="fa fa-plus-circle"
                                                                                    aria-hidden="true"></i></a>

                                                                        <a style=" display:<?php echo ($key > 0) ? 'block' : 'none'; ?>"
                                                                           class="add-icon additional_email-remove-sign"
                                                                           href="javascript:;">
                                                                            <i class="fa fa-times-circle red-star"
                                                                               aria-hidden="true"
                                                                               style="display: inline;"></i>
                                                                        </a>
                                                                        <span class="customError required"></span>
                                                                    </div>
                                                                <?php }
                                                            }
                                                        }else{
                                                            ?>
                                                            <div class='additional_multipleEmail'>
                                                                <input class="form-control add-input customadditionalEmailValidation"
                                                                       placeholder="Email" maxlength="50"
                                                                       data_required="true" data_email
                                                                       type="text"
                                                                       name="additional_email[]"
                                                                       value="<?= $data['email'] ?>">

                                                                <a style=" display:block"
                                                                   class="add-icon additional_email-plus-sign"
                                                                   href="javascript:;"><i
                                                                            class="fa fa-plus-circle"
                                                                            aria-hidden="true"></i></a>

                                                                <a style=" display:none"
                                                                   class="add-icon additional_email-remove-sign"
                                                                   href="javascript:;">
                                                                    <i class="fa fa-times-circle red-star"
                                                                       aria-hidden="true"
                                                                       style="display: inline;"></i>
                                                                </a>
                                                                <span class="customError required"></span>
                                                            </div>
                                                        <?php }?>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Select Property</label>
                                                        <select class="form-control" id="property" name="property">
                                                            <option>Undecided</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Select Building</label>
                                                        <select class="form-control" id="building" name="building">
                                                            <option>Undecided</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Select Unit</label>
                                                        <select class="form-control" id="unit" name="unit">
                                                            <option value="">Undecided</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Ethnicity
                                                            <a class="pop-add-icon selectPropertyEthnicity " href="javascript:;">
                                                                <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="ethncity">
                                                            <option value="0">Select</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyEthnicity1">
                                                            <h4>Add New Ethnicity</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Ethnicity <em class="red-star">*</em></label>
                                                                        <input class="form-control ethnicity_src customValidateGroup customEthnicityValidation" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                                        <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-validation-class="customEthnicityValidation" data-cell="title" data-class="ethnicity_src" data-name="ethncity">Save</button>
                                                                        <button type="button"  class="clear-btn clear_ethnicity">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Martial Status
                                                            <a class="pop-add-icon selectPropertyMaritalStatus" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="maritalStatus">
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                            <h4>Add New Marital Status</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Marital Status <em class="red-star">*</em></label>
                                                                        <input class="form-control maritalstatus_src customMaritalValidation" type="text" placeholder="Add New Marital Status" data_required="true">
                                                                        <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-validation-class="customMaritalValidation" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                                        <button type="button"  class="clear-btn clear_marital_status">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Hobbies
                                                            <a class="pop-add-icon selectPropertyHobbies" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="hobbies[]" id="edit_hobbies" multiple>
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyHobbies1">
                                                            <h4>Add New Hobbies</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Hobbies <em class="red-star">*</em></label>
                                                                        <input class="form-control hobbies_src customHobbiesValidation" type="text"  placeholder="Add New Hobbies" data_required="true">
                                                                        <span class="customError required red-star" aria-required="true" id="hobbies_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-validation-class="customHobbiesValidation" data-class="hobbies_src" data-name="hobbies">Save</button>
                                                                        <button type="button"  class="clear-btn clear_hobbies">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Veteran Status
                                                            <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="veteranStatus">
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                            <h4>Add New VeteranStatus</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                        <input class="form-control veteran_src customVeteranValidation" type="text" placeholder="Add New Veteran Status" data_required="true">
                                                                        <span class="customError required red-star" aria-required="true" id="veteran_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-validation-class="customVeteranValidation" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                                        <button type="button"  class="clear-btn clear_veteran_status">Clear</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-outer" id="emergency-contact">
                                            <div class="form-hdr">
                                                <h3>Emergency Contact Details</h3>
                                            </div>

                                            <div class="form-data">
                                                <?php
                                                if(!empty($emergency_detail)) {
                                                    foreach ($emergency_detail as $key => $user_emergency_detail) { ?>
                                                        <div class="row additional-tenant-emergency-contact">
                                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                                                <label>Emergency Contact Name</label>
                                                                <input class="form-control
" maxlength="50" type="text"
                                                                       id="emergency" name="emergency_contact_name[]"
                                                                       value="<?= $user_emergency_detail['emergency_contact_name']; ?>">
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                                                <label>Relation</label>
                                                                <select class="form-control" id="relationship"
                                                                        name="emergency_relation[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1" <?= ($user_emergency_detail['emergency_relation'] == 1) ? 'selected' : '' ?>>
                                                                        Brother
                                                                    </option>
                                                                    <option value="2" <?= ($user_emergency_detail['emergency_relation'] == 2) ? 'selected' : '' ?>>
                                                                        Daughter
                                                                    </option>
                                                                    <option value="3" <?= ($user_emergency_detail['emergency_relation'] == 3) ? 'selected' : '' ?>>
                                                                        Employer
                                                                    </option>
                                                                    <option value="4" <?= ($user_emergency_detail['emergency_relation'] == 4) ? 'selected' : '' ?>>
                                                                        Father
                                                                    </option>
                                                                    <option value="5" <?= ($user_emergency_detail['emergency_relation'] == 5) ? 'selected' : '' ?>>
                                                                        Friend
                                                                    </option>
                                                                    <option value="6" <?= ($user_emergency_detail['emergency_relation'] == 6) ? 'selected' : '' ?>>
                                                                        Mentor
                                                                    </option>
                                                                    <option value="7" <?= ($user_emergency_detail['emergency_relation'] == 7) ? 'selected' : '' ?>>
                                                                        Mother
                                                                    </option>
                                                                    <option value="8" <?= ($user_emergency_detail['emergency_relation'] == 8) ? 'selected' : '' ?>>
                                                                        Neighbor
                                                                    </option>
                                                                    <option value="9" <?= ($user_emergency_detail['emergency_relation'] == 9) ? 'selected' : '' ?>>
                                                                        Nephew
                                                                    </option>

                                                                    <option value="10" <?= ($user_emergency_detail['emergency_relation'] == 10) ? 'selected' : '' ?>>
                                                                        Niece
                                                                    </option>
                                                                    <option value="11" <?= ($user_emergency_detail['emergency_relation'] == 11) ? 'selected' : '' ?>>
                                                                        Owner
                                                                    </option>
                                                                    <option value="12" <?= ($user_emergency_detail['emergency_relation'] == 12) ? 'selected' : '' ?>>
                                                                        Partner
                                                                    </option>
                                                                    <option value="13" <?= ($user_emergency_detail['emergency_relation'] == 13) ? 'selected' : '' ?>>
                                                                        Sister
                                                                    </option>
                                                                    <option value="14" <?= ($user_emergency_detail['emergency_relation'] == 14) ? 'selected' : '' ?>>
                                                                        Son
                                                                    </option>
                                                                    <option value="15" <?= ($user_emergency_detail['emergency_relation'] == 15) ? 'selected' : '' ?>>
                                                                        Spouse
                                                                    </option>
                                                                    <option value="16" <?= ($user_emergency_detail['emergency_relation'] == 16) ? 'selected' : '' ?>>
                                                                        Teacher
                                                                    </option>
                                                                    <option value="17" <?= ($user_emergency_detail['emergency_relation'] == 17) ? 'selected' : '' ?>>
                                                                        Other
                                                                    </option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                                                <label>Country Code</label>
                                                                <select class="form-control emergency_additional_countryCode emergency_country emergency_country_code_<?= $key ?>"
                                                                        name="emergency_country[]">
                                                                    <option value="">Select</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                                                <label>Phone Number</label>
                                                                <input class="form-control phone_format"
                                                                       type="text"
                                                                       id="phoneNumber"
                                                                       name="emergency_phone[]"
                                                                       value="<?= $user_emergency_detail['emergency_phone_number']; ?>">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-2 col-md-2">
                                                                <label>Email</label>
                                                                <input class="form-control add-input" type="text"
                                                                       name="emergency_email[]"
                                                                       value="<?= $user_emergency_detail['emergency_email']; ?>">
                                                                <span class="flast_nameErr error red-star"></span>
                                                                <a class="add-icon"
                                                                   style=" display:<?php echo ($key == 0) ? 'block' : 'none'; ?>"
                                                                   href="javascript:;"><i
                                                                            class="fa fa-plus-circle additional-add-emergency-contant"
                                                                            aria-hidden="true"></i></a>
                                                                <a class="add-icon additional-remove-emergency-contant"
                                                                   style=" display:<?php echo ($key > 0) ? 'block' : 'none'; ?>"><i
                                                                            class="fa fa-times-circle red-star"
                                                                            aria-hidden="true"
                                                                            style="display: inline;"></i></a>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                }else{

                                                    ?>


                                                    <div class="row additional-tenant-emergency-contact">
                                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                                            <label>Emergency Contact Name</label>
                                                            <input class="form-control" maxlength="50" type="text"
                                                                   id="emergency" name="emergency_contact_name[]"
                                                            >
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                                            <label>Relation</label>
                                                            <select class="form-control" id="relationship"
                                                                    name="emergency_relation[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Brother</option>
                                                                <option value="2">Daughter</option>
                                                                <option value="3">Employer</option>
                                                                <option value="4">Father</option>
                                                                <option value="5">Friend</option>
                                                                <option value="6">Mentor</option>
                                                                <option value="7">Mother</option>
                                                                <option value="8">Neighbor</option>
                                                                <option value="9">Nephew</option>
                                                                <option value="10">Niece</option>
                                                                <option value="11">Owner</option>
                                                                <option value="12">Partner</option>
                                                                <option value="13">Sister</option>
                                                                <option value="14">Son</option>
                                                                <option value="15">Spouse</option>
                                                                <option value="16">Teacher</option>
                                                                <option value="17">Other</option>

                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                                            <label>Country Code</label>
                                                            <select class="form-control emergency_additional_countryCode emergency_country"
                                                                    name="emergency_country[]">
                                                                <option value="">Select</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                                            <label>Phone Number</label>
                                                            <input class="form-control phone_format"
                                                                   type="text"
                                                                   id="phoneNumber"
                                                                   name="emergency_phone[]"
                                                            >
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-2 col-md-2">
                                                            <label>Email</label>
                                                            <input class="form-control add-input" type="text"
                                                                   name="emergency_email[]"
                                                            >
                                                            <span class="flast_nameErr error red-star"></span>
                                                            <a class="add-icon"

                                                               href="javascript:;"><i
                                                                        class="fa fa-plus-circle additional-add-emergency-contant"
                                                                        aria-hidden="true"></i></a>
                                                            <a class="add-icon additional-remove-emergency-contant"
                                                               style=" display: none"><i
                                                                        class="fa fa-times-circle red-star"
                                                                        aria-hidden="true"
                                                                        style="display: inline;"></i></a>
                                                        </div>
                                                    </div>
                                                <?php }?>

                                            </div>

                                        </div>
                                        <!-- Form Outer Ends -->

                                        <div class="form-outer" id="credential_control">
                                            <div class="form-hdr">
                                                <h3>Contact Credential Control</h3>
                                            </div>
                                            <div class="form-data">
                                                <?php
                                                if(!empty($credential_control)){
                                                    foreach($credential_control as $key => $user_credential_control) { ?>

                                                        <div class="row tenant-credentials-control">
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Credential Name</label>
                                                                <input class="form-control" maxlength="30"  value="<?= $user_credential_control['credential_name']; ?>" id="credentialName" name="credentialName[]"type="text"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Credential Type
                                                                    <a class="pop-add-icon tenantCredentialType" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                </label>
                                                                <select class="form-control credential_type<?=$key?>" name="credentialType[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1">Bond</option>
                                                                    <option value="2">Certification</option>
                                                                </select>
                                                                <div class="add-popup" id="tenantCredentialType1">
                                                                    <h4>Add Credential Type</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer custom-popup-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Credential Type <em class="red-star">*</em></label>
                                                                                <input class="form-control credential_source customCredentialValidation" data_required="true" type="text" placeholder="Ex: License">
                                                                                <span class="customError required"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-validation-class="customCredentialValidation" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                                <button type="button"  class="clear-btn clear_credential_type">Clear</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php if($key ==0){?>
                                                                <div class="col-xs-12 col-sm-4 col-md-2">
                                                                    <label>Acquire Date</label>
                                                                    <input class="form-control calander acquireDateclass commonacquireclass acquire_date_<?=$key?>" readonly type="text" value="<?= dateFormatUser($user_credential_control["acquire_date"],NULL,$this->companyConnection);?>"  id="acquireDateid" name="acquireDate[]">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            <?php } else {?>
                                                                <div class="col-xs-12 col-sm-4 col-md-2">
                                                                    <label>Acquire Date</label>
                                                                    <input class="form-control calander acquireDateclass commonacquireclass acquire_date_<?=$key?>" readonly type="text" value="<?= dateFormatUser($user_credential_control["acquire_date"],NULL,$this->companyConnection);?>"  id="acquireDateid_<?=$key+1?>" name="acquireDate[]">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            <?php }?>
                                                            <?php if($key ==0){?>
                                                                <div class="col-xs-12 col-sm-4 col-md-2">
                                                                    <label>Expiration Date</label>
                                                                    <input class="form-control calander expirationDateclass commonacquireclass expire_date_<?=$key?>" readonly type="text" value="<?= dateFormatUser($user_credential_control["expire_date"],NULL,$this->companyConnection);?>"  id="expirationDateid" name="expirationDate[]">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            <?php } else {?>
                                                                <div class="col-xs-12 col-sm-4 col-md-2">
                                                                    <label>Expiration Date</label>
                                                                    <input class="form-control calander expirationDateclass commonacquireclass expire_date_<?=$key?>" readonly type="text" value="<?= dateFormatUser($user_credential_control["expire_date"],NULL,$this->companyConnection);?>"  id="expirationDateid_<?=$key+1?>" name="expirationDate[]">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            <?php }?>
                                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                                <label>Notice Period</label>
                                                                <select class="form-control add-input" id="notice_period"
                                                                        name="noticePeriod[]">
                                                                    <option value="">Select</option>
                                                                    <option value="4" <?= ($user_credential_control['notice_period'] == 4) ? 'selected' : '' ?>>5 day</option>
                                                                    <option value="1" <?= ($user_credential_control['notice_period'] == 1) ? 'selected' : '' ?>>1 Month</option>
                                                                    <option value="2" <?= ($user_credential_control['notice_period'] == 2) ? 'selected' : '' ?>>2 Month</option>
                                                                    <option value="3" <?= ($user_credential_control['notice_period'] == 3) ? 'selected' : '' ?>>3 Month</option>
                                                                </select>
                                                                <a  style=" display:<?php echo ($key == 0) ? 'block' : 'none'; ?>" class="add-icon add-notice-period" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a class="add-icon remove-notice-period" href="javascript:;"  style=" display:<?php echo ($key > 0) ? 'block' : 'none'; ?>">
                                                                    <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    <?php }  }else{
                                                    ?>
                                                    <div class="row tenant-credentials-control">
                                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                                            <label>Credential Name</label>
                                                            <input class="form-control" maxlength="30" id="credentialName" name="credentialName[]"type="text"/>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                                            <label>Credential Type
                                                                <a class="pop-add-icon tenantCredentialType" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a>
                                                            </label>
                                                            <select class="form-control " name="credentialType[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Bond</option>
                                                                <option value="2">Certification</option>
                                                            </select>
                                                            <div class="add-popup" id="tenantCredentialType1" style="width: 127%;">
                                                                <h4>Add Credential Type</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>Credential Type <em class="red-star">*</em></label>
                                                                            <input class="form-control credential_source customCredentialValidation" data_required="true" type="text" placeholder="Ex: License">
                                                                            <span class="customError required"></span>
                                                                        </div>
                                                                        <div class="btn-outer text-right">
                                                                            <button type="button" class="blue-btn add_single1" data-validation-class="customCredentialValidation" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                            <button type="button"  class="clear-btn clear_credential_type">Clear</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                                            <label>Aquire Date</label>
                                                            <input class="form-control calander acquireDateclass commonacquireclass" readonly type="text" id="acquireDateid" name="acquireDate[]">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                                            <label>Expiration Date</label>
                                                            <input class="form-control calander expirationDateclass commonacquireclass" readonly type="text"
                                                                   id="expirationDateid" name="expirationDate[]">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                                            <label>Notice Period</label>
                                                            <select class="form-control add-input" id="notice_period"
                                                                    name="noticePeriod[]">
                                                                <option value="">Select</option>
                                                                <option value="4">5 day</option>
                                                                <option value="1">1 Month</option>
                                                                <option value="2">2 Month</option>
                                                                <option value="3">3 Month</option>
                                                            </select>
                                                            <a class="add-icon add-notice-period" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon remove-notice-period" href="javascript:;" style="display: none;">
                                                                <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php }?>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->

                                        <div class="form-outer" id="notes">
                                            <div class="form-hdr">
                                                <h3>Notes</h3>
                                            </div>
                                            <div class="form-data ">
                                                <?php
                                                $notes_count = count($charge_notes);
                                                if(!empty($charge_notes)){
                                                    foreach($charge_notes as $key1=> $value){
                                                        ?>
                                                        <div class="row employee_notes" >

                                                            <div class="col-xs-12 col-sm-4 col-md-4 notes_date_right_div">
                                                                <textarea class="form-control add-input  capital notesDateTime notes_date_right"  maxlength="500" name="employee_notes[]" id="employee_notes"><?= $value['note']; ?></textarea>
                                                                <a  class="add-icon add-employee-notes" href="javascript:;"><i style=" display:<?php echo ($key1 == 0 && $notes_count < 3 ) ? 'block' : 'none'; ?>" class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a style=" display:<?php echo ($key1 > 0) ? 'block' : 'none'; ?>"  class="add-icon remove-employee-notes" href="javascript:;">
                                                                    <i  class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    <?php }  }else { ?>
                                                    <div class="row employee_notes" >
                                                        <div class="col-xs-12 col-sm-4 col-md-4">
                                                            <div class="notes_date_right_div">
                                                            <textarea class="form-control add-input notes_date_right" maxlength="500" name="employee_notes[]" id="employee_notes"></textarea>
                                                            </div>
                                                            <a class="add-icon add-employee-notes" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon remove-employee-notes" href="javascript:;" style="display: none;">
                                                                <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->

                                        <!-- flag start-->
                                        <div class="form-outer" id="flag_bank">
                                            <div class="form-hdr">
                                                <h3>Flag Bank</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="property-status">
                                                    <button type="button" id="new_flag" class="blue-btn pull-right" >New Flag</button>
                                                </div>
                                                <div class="row border-0" id="flagFormDiv" style="display: none;">
                                                    <!--                                        <form id="flagForm">-->
                                                    <div class="grey-box-add">
                                                        <div class="form-outer">
                                                            <input type="hidden" name="id" id="flag_id">
                                                            <div class="col-xs-12 col-sm-3">
                                                                <label>Flagged By </label>
                                                                <input class="form-control" name="flag_by" id="flag_flag_by" placeholder="Flagged By" maxlength="100" type="text" value=""/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                <label>Date</label>
                                                                <input class="form-control" name="date" id="flag_flag_date" readonly placeholder="Eg: <?php echo $current_date; ?>" type="text"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                <label>Flag Name </label>
                                                                <input class="form-control capital" name="flag_name" id="flag_flag_name" maxlength="100" placeholder="Please Enter the Name of this Flag"  type="text"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                <label>Country Code</label>
                                                                <select class='form-control flag_countryCode' name='country_code' id="flag_country_code" >
                                                                    <option value="0"></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                <label>Phone Number</label>
                                                                <input class="form-control phone_number p-number-format phone_format" name="flag_phone_number" id="flag_phone_number" value="<?= $data['phone_number']?>" maxlength="12" placeholder="123-456-7890" type="text"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                <label>Flag Reason</label>
                                                                <input CLASS="form-control" name="flag_reason" id="flag_flag_reason" maxlength="100" placeholder="Flag Reason" type="text"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                <label>Flagged For</label>
                                                                <input class="form-control" name="flagged_for" disabled id="flagged_for" maxlength="100"  type="text"/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-3">
                                                                <label>Completed</label>
                                                                <select class='form-control' name='completed' id="completed">
                                                                    <option value="0">No</option>
                                                                    <option value="1">Yes</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6">
                                                                <label>Notes</label>
                                                                <div class="col-xs-12 col-sm-6 notes_date_right_div">
                                                                <textarea class="form-control notes_date_right" name="flag_note" id="flag_note" placeholder="Flag Notes"></textarea>
                                                                </div>
                                                                </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="btn-outer text-right">
                                                                <button type="button" class="blue-btn" type="submit" id="flagSaveBtnId">Save</button>
                                                                <button type="button"  class="clear-btn clear_contact_flag">Clear</button>
                                                                <button type="button"  class="clear-btn reset_contact_flag" style="display:none">Reset</button>
                                                                <button type="button" class="grey-btn" type="button" id="flagCancel">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                        </form>-->
                                                </div>
                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                        <div class="pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <div class="apx-table listinggridDiv">
                                                                                        <div class="table-responsive">
                                                                                            <table id="propertyFlag-table" class="table table-bordered"></table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- flag ends-->

                                        <div class="form-outer" id="filelibrary">
                                            <div class="form-hdr">
                                                <h3>File Library</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                        <button type="button" class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                        <button type="button" class="blue-btn" id="save_file_library">Save</button>

                                                    </div>
                                                    <div class="row" id="file_library_uploads">
                                                    </div>
                                                    <div class="grid-outer listinggridDiv">
                                                        <div class="col-sm-12">
                                                            <div class="table-responsive">
                                                                <table id="buildingFileLibrary-table" class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-outer" id="custom">
                                            <div class="form-hdr">
                                                <h3>Custom Fields</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="custom_field_html">
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 custom_field_msg">
                                                        No Custom Fields
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right">
                                                        <label></label>
                                                        <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends -->
                                    </div>
                                    <div class="btn-outer text-right">
                                        <button type="submit" class="blue-btn employee_submit">Update</button>
                                        <button type="button"  class="clear-btn clear_contact">Reset</button>
                                        <button type="button" class="grey-btn contact_cancel">Cancel</button>
                                    </div>

                                </div>
                                <!--tab Ends -->

                            </div>

                        </div>
                </form>
            </div>
        </div>
    </section>
</div>
<form name="file_library_images" id="file_library_images" enctype='multipart/form-data'>
    <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
</form>

<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="contact">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">

                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button type="button"  class="clear-btn clear_custom_filed_popup">Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



    <div class="modal fade" id="sendMailModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Email</h4>
                    </a>
                </div>
                <form id="sendEmail">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-1">
                            <button class="blue-btn compose-email-btn">Send</button>
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>To <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control to" name="to" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addToRecepent" href="#"> Add Recipients</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Cc</div>
                                <div class="col-sm-10"><span><input class="form-control cc" name="cc" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addCcRecepent" href="#"> Add Recipients</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Bcc </label></div>
                                <div class="col-sm-10"><span><input class="form-control bcc" name="bcc" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <a class="add-recipient-link addBccRecepent" href="#"> Add Recipients</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Subject <em class="red-star">*</em></label></div>
                                <div class="col-sm-10"><span><input class="form-control subject" name="subject" type="text"/></span></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-2"><label>Body <em class="red-star">*</em></label></div>
                                <div class="col-sm-10">
                                    <span><textarea class="form-control summernote" name="body"></textarea></span>
                                    <div class="btn-outer">
                                        <button class="blue-btn">Send</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>


                    <div class="attachmentFile"></div>


                </div>
                </form>
            </div>
        </div>

    </div>


<div class="container">
    <div class="modal fade" id="torecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectUsers">
                                <option value="">Select</option>
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-6">

                        </div>

                        <div class="col-sm-12">
                            <div class="userDetails"></div>
                        </div>

                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectToUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="ccrecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectCcUsers">
                                <option value="">Select</option>
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <div class="userCcDetails"></div>
                        </div>

                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectCcUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="modal fade" id="bccrecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectBccUsers">
                                <option value="">Select</option>
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="7">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <div class="userBccDetails"></div>
                        </div>

                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectBccUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->
<!-- Wrapper Ends -->
<!-- Footer Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->
<script>

    var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var dob = "<?php echo $data['dob']; ?>//";
    var referral_source =  '<?php echo (!empty($data["referral_source"])) ? $data["referral_source"]:"" ?>';
    var ethnicity = "<?= $data["ethnicity"]; ?>";
    var maritial_status = "<?= $data["maritial_status"]; ?>";
    // var hobbies ='<?//php echo (!empty($data["hobbies"])) ? JSON.parse($data["hobbies"]):"" ?>';
    var hobbies = <?php echo json_encode($hobbiesssss) ?>;
    var veteran_status = '<?= $data["veteran_status"]?>';
    var country_codes = <?php echo json_encode($country_codessss) ?>;
    var carriers = <?php echo json_encode($carrierss) ?>;
    var phone_country_codess = <?php echo json_encode($phone_country_codess) ?>;
    var acquire_dates = <?php echo json_encode($acquire_dates) ?>;
    var expire_dates = <?php echo json_encode($expire_dates) ?>;
    var employee_image = '<?= $data["employee_image"]?>';
    var salutation = '<?= $data["salutation"]?>';
    var default_name = '<?php echo $_SESSION[SESSION_DOMAIN]['default_name'];?>';
    var default_number= '<?php echo $_SESSION[SESSION_DOMAIN]['phone_number'];?>';
    var building_id =  "<?php echo $data['building_id']; ?>";
    var property_id =  "<?php echo $data['property_id']; ?>";
    var credential_type =  <?php echo json_encode($credential_type) ?>;
    var unit_id =  "<?php echo $data['unit_id']; ?>";

    $('.employee_image').html(employee_image);
    setTimeout(function () {
        getsalutationData(salutation);
        $.each(country_codes, function (key, value) {
            $(".emergency_country_code_"+key+" option[value='"+value+"']").attr("selected","selected");
        });
        $.each(carriers, function (key, value) {
            $(".select_carrier_"+key+" option[value='"+value+"']").attr("selected","selected");
        });

        $.each(phone_country_codess, function (key, value) {
            $(".select_country_code_"+key+" option[value='"+value+"']").attr("selected","selected");
        });

        $.each(credential_type, function (key, value) {
            $(".credential_type"+key+" option[value='"+value+"']").attr("selected","selected");
        });
        if(property_id != '') {
            $('#property').val(property_id);
        }
        if(property_id != ''){
            getBuildingsByPropertyID(property_id);
            setTimeout(function () {
                $('#building').val(building_id);
            }, 1000);

            if(building_id != ''){

                setTimeout(function () {
                    getUnitsByBuildingsID(property_id,building_id);
                    $('#unit').val(unit_id);
                }, 1200);
            }

        }else{

        }

    }, 2000);
</script>
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />
<!--<script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/views/company/maintenance/lost_found/edit-item.php"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/contacts/editContact.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingFileLibrary.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingPhotoVideos.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>

<!-- flag js -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/contacts/flag/flagValidation.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/contacts/flag/flag.js"></script>

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });

        setTimeout(function () {

            $("#flag_country_code").val(first_country_code);
        },2000);
    });
    <!--- Accordians -->
</script>
<!-- Jquery Starts -->

<script>
    var default_form_data = '';
    var defaultIgnoreArray = [];
    var defaultFormData = '';

    $(document).on('click','.clear_contact',function(){
        bootbox.confirm("Do you want to reset this form?", function (result) {
            if (result == true) {
                window.location.reload();
            }
        });
    });

    $(document).on('click','.referal_source_clear',function(){
        resetFormClear('#additionalReferralResource1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_ethnicity',function(){
        resetFormClear('#selectPropertyEthnicity1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_ethnicity',function(){
        resetFormClear('#selectPropertyEthnicity1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_marital_status',function(){
        resetFormClear('#selectPropertyMaritalStatus1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_hobbies',function(){
        resetFormClear('#selectPropertyHobbies1',[],'div',false,default_form_data,defaultIgnoreArray);
    });
    $(document).on('click','.clear_veteran_status',function(){
        resetFormClear('#selectPropertyVeteranStatus1',[],'div',false,default_form_data,defaultIgnoreArray);
    });
    $(document).on('click','.clear_credential_type',function(){
        resetFormClear('#tenantCredentialType1',[],'div',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_custom_filed_popup',function(){
        resetFormClear('#custom_field',['data_type','is_required'],'form',false,default_form_data,defaultIgnoreArray);
    });

    $(document).on('click','.clear_contact_flag',function(){
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                $("#flag_note").val('');
                resetFormClear('#flagFormDiv', ['flag_by', 'date', 'country_code','flag_phone_number','completed', 'flagged_for'], 'div', false, default_form_data, defaultIgnoreArray);
            }
        });
    });

    $(document).on('click','.reset_contact_flag',function () {
        resetEditForm("#flagFormDiv",[],true,defaultFormData,[]);
        //$()
    });
    var updated_at_date = "<?php echo $data['updated_at']; ?>";

    setTimeout(function () {
    edit_date_time(updated_at_date);
    },2000);

    $("#sendEmail").validate({
        rules: {
            to: {
                required:true
            },
            subject: {
                required:true
            },
            body: {
                required:true
            },
        },
        submitHandler: function (e) {
            var tenant_id = $(".tenant_id").val();
            var form = $('#sendEmail')[0];
            var formData = new FormData(form);
            var path = $(".attachments").attr('href');
            /*  alert(path);*/
            var to = $(".to").val();
            formData.append('to_users',to);
            formData.append('action','sendFileLibraryattachEmail');
            formData.append('class','TenantAjax');
            formData.append('path', path);
            $.ajax({
                url: '/Tenantlisting/getInitialData',
                type: 'POST',
                data: formData,
                success: function (data) {
                    info =  JSON.parse(data);
                    if(info.status=="success"){
                        toastr.success("Email has been sent successfully");
                        $("#sendMailModal").modal('hide');

                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
</script>

<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            focus:true,
            addclass: {
                debug: true,
                classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
            },
            width: '100%',
            height: '200',
            focus:true,
            //margin-left: '15px',
            toolbar: [
                // [groupName, [list of button]]
                ['img', ['picture']],
                ['style', ['style', 'addclass', 'clear']],
                ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
                ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
                ['extra', ['video', 'table', 'height']],
                ['misc', ['undo', 'redo', 'codeview', 'help']]
            ]
        });
        $('.summernote').summernote('focus');
    });
    setTimeout(function () {
        $('.capital').css("text-transform","none");

    },1000);
    // setTimeout(function () {
    //     $('input').removeClass('capital');
    //     $('input').on('keypress', function(event) {
    //         var $this = $(this),
    //             val = $this.val(),
    //             regex = /\b[a-z]/g;
    //
    //         val = val.charAt(0).toUpperCase() + val.substr(1);
    //
    //         // I want this value to be in the input field.
    //         console.log(val);
    //         $('input').val(val);
    //     });
    // },100);
</script>

</body>

</html>