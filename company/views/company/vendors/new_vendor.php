<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$satulation_array =['1'=>'Dr.','2'=>'Mr.','3'=>'Mrs.','4'=>'Mr. & Mrs.','5'=>'Ms.',6=>'Sir',7=>'Madam','10'=>'Brother',8=>'Sister','11'=>'Father',9=>'Mother'];
$gender_array =['1'=>'Male','2'=>'Female','3'=>'Prefer Not To Say','4'=>'Other'];
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>  <style>
    .form-control{
        padding: 6px;
    }
    .birth_date_color{
        background: none !important;
    }
    .multipleEmail input{margin:5px auto; }
    select[name=hobbies] + .btn-group button{
        margin-top: 0px;
    }
    .closeimagepopupicon{
        float: right;
        font-weight: bold;
        border: 2px solid #666;
        border-radius: 12px;
        padding: 0px 4px;
        cursor: pointer;
    }
    .fa-minus-circle {
        display: none;
    }

    .apx-inline-popup {
        position: relative;
    }

    .apx-inline-popup-box {
        position: absolute;
        top: 0;
        left: 0;
        background: #fff;
        z-index: 2;
        border: 1px solid #ddd;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
        padding: 15px;
        width: 100%;
    }

    .apx-inline-popup-box > h4 {
        font-size: 12px
    }

    .image-editor {
        text-align: center;
    }

    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
        display: inline-block;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .cropit-preview-background {
        opacity: .2;
        cursor: auto;
    }

    .image-size-label {
        margin-top: 10px;
    }

    .export {
        /* Use relative position to prevent from being covered by image background */
        position: relative;
        z-index: 10;
        display: block;
    }

    .image-editor input[type="file"] {
        opacity: 0;
        position: absolute;
        top: 45px;
        left: 91px;
    }

    .upload-logo .img-outer img {
        max-width: 100%;
    }

    .choose-img {
        margin: 0 !important;
    }



    .vehicle_image1, .vehicle_image2, .vehicle_image3 {
        width: 100px;
        height: 100px;
    }

    .vehicle_image1 img, .vehicle_image2 img, .vehicle_image3 img {
        width: 100%;
    }
</style>
<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 bread-search-outer">
                    <div class="breadcrumb-outer">
                        People >> <span>New Vendor</span>
                    </div>
                    <div class="easy-search">
                        <input placeholder="Easy Search" type="text" />
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                                <!--                                <li role="presentation"><a href="javascript:;" aria-controls="home" role="tab" data-toggle="tab">Tenants</a></li>-->
                                <li role="presentation"><a href="/People/Ownerlisting">Owners</a></li>
                                <li role="presentation" class="active"><a href="/Vendor/Vendor" >Vendors</a></li>
                                <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                                <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                    <div class="accordion-form">
                                        <form id="FormSaveNewVendor" method="post">
                                        <div class="accordion-outer">
                                            <div class="bs-example">

                                                    <!--                                                    step 1 input-->
                                                    <input type="hidden"  name="ccard_number" value="" id="hidden_ccard_number">
                                                    <input type="hidden"  name="ccvv" value="" id="hidden_ccvv">
                                                    <input type="hidden"  name="cexpiry_year" value="" id="hidden_cexpiry_year">
                                                    <input type="hidden"  name="cexpiry_month" value="" id="hidden_cexpiry_month">

                                                    <!--                                                    step 2 input-->

                                                    <input type="hidden"  name="hidden_acc_detail[]" value="" id="hidden_acc_detail">
                                                    <input type="hidden"  name="hidden_account_type" value="" id="hidden_account_type">
                                                    <input type="hidden"  name="hidden_account_id" value="" id="hidden_account_id">
                                                    <input type="hidden" name = "hidden_customer_id" id="hidden_customer_id">


                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse">
                                                                        <span></span> Vendor Information</a> <a class="back" href="/Vendor/Vendor/"><< Back</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <div class="latefee-check-outer">
                                                                                <div class="col-sm-4">
                                                                                    <div class="check-outer">
                                                                                        <input type="checkbox" name="use_company_name" class="entity-company-checkbox use_company_name"/>
                                                                                        <label class="blue-label">Use Entity/Company Name on Check/Reports</label>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="check-outer">
                                                                                        <input type="checkbox" name="is_pmc" class="is_pmc"/>
                                                                                        <label class="blue-label">Is PMC</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-12">
                                                                                <label>Upload Picture</label>
                                                                                <div class="upload-logo">
                                                                                    <div class="tenant_image img-outer"><img src="<?php echo COMPANY_SITE_URL ?>/images/dummy-img.jpg"></div>
                                                                                    <a href="javascript:;">
                                                                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image</a>
                                                                                    <span>(Maximum File Size Limit: 1MB)</span>
                                                                                </div>
                                                                                <div class="image-editor">

                                                                                    <input type="file" class="cropit-image-input" name="tenant_image">
                                                                                    <div class='cropItData' style="display: none;">


                                                                                        <span class="closeimagepopupicon">X</span>
                                                                                        <div class="cropit-preview"></div>
                                                                                        <div class="cropit-rotate">
                                                                                            <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                                                            </a>
                                                                                            <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="image-size-label">Resize image</div>
                                                                                        <input type="range" class="cropit-image-zoom-input">

                                                                                        <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                                                        <input type="button" class="export" value="Done"
                                                                                               data-val="tenant_image">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Salutation </label>
                                                                                    <select class="form-control" id="salutation" name="salutation">
                                                                                        <option value="">Select</option>
                                                                                        <?php foreach ($satulation_array as $key => $value) { ?>

                                                                                            <option value="<?= $key ?>"><?php echo $value; ?></option>
                                                                                        <?php } ?>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>First Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control capital" type="text" maxlength="20" name="first_name" id="first_name" placeholder="First Name"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Middle Name</label>
                                                                                    <input class="form-control capital" type="text" maxlength="50" name="middle_name" placeholder="Middle Name"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Last Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control capital" type="text" maxlength="20" name="last_name" placeholder="Last Name" id="last_name"/>
                                                                                </div>
                                                                                <div class="col-sm-3 hidemaiden" style="display: none;">
                                                                                    <label>Maiden Name</label>
                                                                                    <input class="form-control capital" type="text" maxlength="20" name="maiden_name" placeholder="Maiden Name"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Nick Name</label>
                                                                                    <input class="form-control capital" type="text" maxlength="20" name="nick_name" placeholder="Nick Name"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Gender</label>
                                                                                    <select id="gender" name="gender"   class="form-control"><option value="">Select</option>
                                                                                        <?php foreach ($gender_array as $key => $value) { ?>
                                                                                            <option value="<?= $key ?>"><?php echo $value; ?></option>
                                                                                        <?php } ?>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Vendor Rate <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> </label>
                                                                                    <input class="form-control amount number_only" type="text" id="vendor_rate" name="vendor_rate" placeholder="Vendor Rate"/>
                                                                                    <span class='add-icon-span'>/hr </span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">

                                                                                <div class="col-sm-3">
                                                                                    <label>Vendor ID <em class="red-star">*</em></label>
                                                                                    <input class="form-control vendor_random_id" type="text" value=""  maxlength="6" name="vendor_random_id"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Entity/Company Name</label>
                                                                                    <input class="form-control capital company_name" type="text" placeholder="Eg: Apexlink" maxlength="50" name="company_name"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Name on Check</label>
                                                                                    <input class="form-control capital" name="name_on_check" type="text" id="name_on_check" placeholder="Name on Check"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Vendor Type <a class="pop-add-icon vendortypeplus" onclick="clearPopUps('#Newvendortype')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <select class="form-control" id='vendor_type_options' name='vendor_type_id'></select>
                                                                                    <div class="add-popup" id='Newvendortype'>
                                                                                        <h4>Add Vendor Type</h4>
                                                                                        <div class="add-popup-body">

                                                                                            <div class="form-outer">
                                                                                                <div class="col-sm-12">
                                                                                                    <label>Vendor Type<em class="red-star">*</em></label>
                                                                                                    <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20" name='@vendor_type' id='vendor_type' placeholder="Eg: PMB"/>
                                                                                                    <span class="customError required"></span>
                                                                                                </div>
                                                                                                <div class="col-sm-12">
                                                                                                    <label> Description <em class="red-star">*</em></label>
                                                                                                    <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="50" name='@description' id='vendor_description' placeholder="Eg: Plumber"/>
                                                                                                    <span class="customError required"></span>
                                                                                                </div>
                                                                                                <div class="btn-outer text-right">
                                                                                                    <input type="button"  id='NewvendortypeSave' class="blue-btn" value="Save" />
                                                                                                    <button type="button"  class="clear-btn clearFormReset" id="clearNewvendortype">Clear</button>
                                                                                                    <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                                                                </div>
                                                                                            </div>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">

                                                                                <div class="col-sm-3">
                                                                                    <label>Birth Date</label>
                                                                                    <input class="form-control birth_date_color" type="text" placeholder="Birth Date" id="birth_date" name="dob" readonly/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="entity-company-check-div">
                                                                                <div class="col-sm-12">
                                                                                    <div class="col-sm-3">
                                                                                        <label>Ethnicity <a class="pop-add-icon ethnicityplus" href="javascript:;" onclick="clearPopUps('#Newethnicity')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>

                                                                                        <select class="form-control" id='ethnicity_options' name='ethnicity'></select>
                                                                                        <div class="add-popup" id='Newethnicity'>
                                                                                            <h4>Add New Ethnicity</h4>
                                                                                            <div class="add-popup-body">
                                                                                                <div class="form-outer">
                                                                                                    <div class="col-sm-12">
                                                                                                        <label>New Ethnicity<em class="red-star">*</em></label>
                                                                                                        <input class="form-control capital customValidateEthnicity" data_required="true" data_max="150" type="text" name="@title" placeholder="Add New Ethnicity"/>
                                                                                                        <span class="customError required"></span>
                                                                                                    </div>
                                                                                                    <div class="btn-outer text-right">
                                                                                                        <input type="button" class="blue-btn" id="NewethnicitySave" value="Save" />
                                                                                                        <button type="button"  class="clear-btn clearFormReset" id="clearEthnicity_options">Clear</button>
                                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                        <label>Martial Status
                                                                                            <a class="pop-add-icon selectPropertyMaritalStatus" href="javascript:;" onclick="clearPopUps('#selectPropertyMaritalStatus1')">
                                                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                            </a>
                                                                                        </label>
                                                                                        <select class="form-control" name="maritalStatus">
                                                                                            <option value="0">Select</option>
                                                                                        </select>
                                                                                        <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                                                            <h4>Add New Marital Status</h4>
                                                                                            <div class="add-popup-body">
                                                                                                <div class="form-outer">
                                                                                                    <div class="col-sm-12">
                                                                                                        <label>New Marital Status <em class="red-star">*</em></label>
                                                                                                        <input class="form-control maritalstatus_src customMaritalValidation capital" type="text" placeholder="Add New Marital Status" data_required="true">
                                                                                                        <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                                                    </div>
                                                                                                    <div class="btn-outer">
                                                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-validation-class="customMaritalValidation" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                                                                        <input type="button" class="clearFormReset clear-btn" id="clearMaritalStatus" value="Clear">
                                                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                        <label>Hobbies
                                                                                            <a class="pop-add-icon selectPropertyHobbies" href="javascript:;" onclick="clearPopUps('#selectPropertyHobbies1')">
                                                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                            </a>
                                                                                        </label>
                                                                                        <select class="form-control" name="hobbies[]"  multiple>
                                                                                            <option value="0">Select</option>
                                                                                        </select>
                                                                                        <div class="add-popup" id="selectPropertyHobbies1">
                                                                                            <h4>Add New Hobbies</h4>
                                                                                            <div class="add-popup-body">
                                                                                                <div class="form-outer">
                                                                                                    <div class="col-sm-12">
                                                                                                        <label>New Hobbies <em class="red-star">*</em></label>
                                                                                                        <input class="form-control hobbies_src customHobbiesValidation capital" type="text"  placeholder="Add New Hobbies" data_required="true">
                                                                                                        <span class="customError required red-star" aria-required="true" id="hobbies_src"></span>
                                                                                                    </div>
                                                                                                    <div class="btn-outer text-right">
                                                                                                        <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-validation-class="customHobbiesValidation" data-class="hobbies_src" data-name="hobbies">Save</button>
                                                                                                        <button type="button"  class="clear-btn clearFormReset" id="clearselectPropertyHobbies1">Clear</button>
                                                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                        <label>Veteran Status
                                                                                            <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;" onclick="clearPopUps('#selectPropertyVeteranStatus1')">
                                                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                            </a>
                                                                                        </label>
                                                                                        <select class="form-control" name="veteranStatus">
                                                                                            <option value="0">Select</option>
                                                                                        </select>
                                                                                        <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                                                            <h4>Add New VeteranStatus</h4>
                                                                                            <div class="add-popup-body">
                                                                                                <div class="form-outer">
                                                                                                    <div class="col-sm-12">
                                                                                                        <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                                                        <input class="form-control veteran_src customVeteranValidation capital" type="text" placeholder="Add New VeteranStatus" data_required="true">
                                                                                                        <span class="customError required red-star" aria-required="true" id="veteran_src"></span>
                                                                                                    </div>
                                                                                                    <div class="btn-outer text-right">
                                                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-validation-class="customVeteranValidation" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                                                                        <button type="button"  class="clear-btn clearFormReset" id="clearselectPropertyVeteranStatus1">Clear</button>
                                                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <div class="col-sm-2 ssn-sin-mainDiv" id="ssn-sin-mainid">
                                                                                        <label>SSN/SIN/ID</label>
                                                                                        <input class="form-control add-input ssn_sin_id capital" type="text" name="ssn_sin_id[]" placeholder="SSN/SIN/ID"/>
                                                                                        <a class="add-icon ssn-sin-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                                        <a  class="add-icon ssn-sin-minus" style="display: none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse">
                                                                        <span></span> Contact Details</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Website </label>
                                                                                    <input placeholder="Eg: http://www.apexlink.com" maxlength="50" id="contact_website" class="form-control" type="text" name="website"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Referral Source <a class="pop-add-icon additionalReferralResource" href="javascript:;" onclick="clearPopUps('#additionalReferralResource1')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <select name="additional_referralSource" id="additional_referralSource" class="form-control"><option>Select</option></select>
                                                                                    <div class="add-popup" id="additionalReferralResource1">
                                                                                        <h4>Add New Referral Source</h4>
                                                                                        <div class="add-popup-body">
                                                                                            <div class="form-outer">
                                                                                                <div class="col-sm-12">
                                                                                                    <label>New Referral Source <em class="red-star">*</em></label>
                                                                                                    <input class="form-control reff_source1 customReferralSourceValidation capital" data_required="true" type="text" placeholder="New Referral Source">
                                                                                                    <span class="customError required"></span>
                                                                                                    <span class="red-star" id="reff_source1"></span>
                                                                                                </div>
                                                                                                <div class="btn-outer text-right">
                                                                                                    <button type="button" class="blue-btn add_single1" data-validation-class="customReferralSourceValidation" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</button>
                                                                                                    <button type="button"  class="clear-btn clearFormReset" id="clearadditionalReferralResource1">Clear</button>
                                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Address 1</label>
                                                                                    <input name="address1" placeholder="Eg: Street Address 1" maxlength="200" id="vAddress1" class="capital form-control address_field" type="text" />
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Address 2</label>
                                                                                    <input name="address2" placeholder="Eg: Street Address 2" maxlength="200" id="vAddress2" class="form-control capital address_field" type="text" />
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Address 3</label>
                                                                                    <input name="address3" placeholder="Eg: Street Address 3" maxlength="200" id="vAddress3" class="form-control capital address_field" type="text" />
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Address 4</label>
                                                                                    <input name="address4" placeholder="Eg: Street Address 4" maxlength="200" id="vAddress4" class="form-control capital address_field" type="text" />
                                                                                </div>
                                                                            </div>
                                                                            <div  id="phonemainIdDiv">
                                                                                <div class="col-sm-12 primary-tenant-phone-row2">
                                                                                    <div class="col-sm-3">
                                                                                        <label>Phone Type</label>
                                                                                        <select name="additional_phoneType[]" class="form-control" id="phone_type_id"></select>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <label>Phone Number <em class="red-star">*</em> </label>
                                                                                        <input id="p_number" name="additional_phone[]" maxlength="12" placeholder="123-456-7891" class="form-control phone_format  customPhonenumbervalidations" type="text" data_required="true"/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="work_extension_div" style="display: none;">
                                                                                        <div class="col-sm-1 col-md-1">
                                                                                            <label>Extension</label>
                                                                                            <input class="form-control number_only_for_extension" maxlength="15" type="text" id="other_work_phone_extension" name="other_work_phone_extension[]" placeholder="Eg: + 161">
                                                                                            <span class="other_work_phone_extensionErr error red-star"></span>
                                                                                        </div>
                                                                                        <!--                                                                                        <div class="col-sm-3 col-md-3">-->
                                                                                        <!--                                                                                            <label>Office/Work Extension <em class="red-star">*</em></label>-->
                                                                                        <!--                                                                                            <input class="form-control phone_format" type="text" id="work_phone_extension" name="work_phone_extension[]" placeholder="Eg: 154-175-4301">-->
                                                                                        <!--                                                                                            <span class="work_phone_extensionErr error red-star"></span>-->
                                                                                        <!--                                                                                        </div>-->
                                                                                    </div>

                                                                                    <div class="col-sm-3">
                                                                                        <label>Carrier <em class="red-star">*</em></label>
                                                                                        <select name="additional_carrier[]" class="form-control additional_carrier customCarriervalidations"  data_required="true"><option>Select</option></select>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-3">
                                                                                        <label>Country Code </label>
                                                                                        <select name="additional_countryCode[]" id="additional_country" class="form-control add-input"><option>Select</option></select>
                                                                                        <a class="add-icon phoneclassDivplus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                                        <a class="add-icon phoneclassDivminus" style="display: none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>


                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Zip / Postal Code </label>
                                                                                    <input name="zipcode" maxlength="9" placeholder="Eg: 10001" id="zip_code" class="form-control capital" type="text" />
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Country </label>
                                                                                    <input name="country" maxlength="50" placeholder="Eg: US" id="country" class="form-control capital" type="text" />
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>State / Province </label>
                                                                                    <input name="state" id="state" maxlength="100" placeholder="Eg: AL"  class="form-control capital states" type="text" />
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>City </label>
                                                                                    <input name="city" maxlength="100" placeholder="Eg: Huntsville"  class="form-control capital citys" type="text" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Add Note for this Phone Number </label>
                                                                                    <div class="notes_date_right_div">
                                                                                        <textarea placeholder="Add Note for this Phone Number" name="phone_number_note" maxlength="500" rows="2" class="capital form-control notes_date_right"></textarea></div>
                                                                                </div>
                                                                                <div class="col-sm-3 emailaddressmainDiv" id="emailaddressmainDivId">
                                                                                    <label>Email Address <em class="red-star">*</em></label>
                                                                                    <input name="additional_email[]" placeholder="Eg: user@domain.com" maxlength="150" class="form-control add-input customadditionalEmailValidation" type="text" data_required="true"/>

                                                                                    <a class="add-icon email-address-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                                    <a class="add-icon email-address-minus" style="display:none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse">
                                                                        <span></span> Emergency Contact Details</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseThree" class="panel-collapse collapse  in">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">

                                                                            <div class="additional-tenant-emergency-contact">
                                                                                <div class="col-sm-12">
                                                                                    <div class="col-sm-2">
                                                                                        <label>Emergency Contact Name</label>
                                                                                        <input class="form-control capsOn capital" type="text"
                                                                                               id="emergency" name="emergency_contact_name[]" placeholder="Emergency Contact Name">
                                                                                        <span class="flast_nameErr error red-star"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <label>Relation</label>
                                                                                        <select class="form-control" id="relationship"
                                                                                                name="emergency_relation[]">
                                                                                            <option value="">Select</option>
                                                                                            <option value="1">Brother</option>
                                                                                            <option value="2">Daughter</option>
                                                                                            <option value="3">Employer</option>
                                                                                            <option value="4">Father</option>
                                                                                            <option value="5">Friend</option>
                                                                                            <option value="6">Mentor</option>
                                                                                            <option value="7">Mother</option>
                                                                                            <option value="8">Neighbor</option>
                                                                                            <option value="9">Nephew</option>
                                                                                            <option value="10">Niece</option>
                                                                                            <option value="11">Owner</option>
                                                                                            <option value="12">Partner</option>
                                                                                            <option value="13">Sister</option>
                                                                                            <option value="14">Son</option>
                                                                                            <option value="15">Spouse</option>
                                                                                            <option value="16">Teacher</option>
                                                                                            <option value="17">Other</option>

                                                                                        </select>
                                                                                        <span class="term_planErr error red-star"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-2" id="otherRelationDiv" style="display: none;">
                                                                                        <label>Other Relation</label>
                                                                                        <input placeholder="Other Relation" class="form-control capital" type="text" id="emergency_other_relation" name="other_relation[]" maxlength="50">
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <label>Country Code</label>
                                                                                        <select class="form-control emergency_additional_countryCode emergency_country" name="emergency_country[]">
                                                                                            <option value="">Select</option>
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <label>Phone Number</label>
                                                                                        <input class="form-control capsOn phone_format" type="text"
                                                                                               id="phoneNumber"
                                                                                               name="emergency_phone[]" placeholder="123-456-7891">
                                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <label>Email</label>
                                                                                        <input class="form-control add-input" type="text"
                                                                                               name="emergency_email[]" placeholder="Eg: user@domain.com">
                                                                                        <span class="flast_nameErr error red-star"></span>
                                                                                        <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle additional-add-emergency-contant" aria-hidden="true"></i></a>
                                                                                        <a class="add-icon additional-remove-emergency-contant" style="display:none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" >
                                                                    <span></span> Bank Account Information</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseFour" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-2">
                                                                                <label>Account Name <a class="pop-add-icon newaccountnameplus" href="javascript:;" onclick="clearPopUps('.Newaccountname')"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                <select class="form-control bankAccountName" name="account_name"><option>Select</option></select>
                                                                                <div class="add-popup Newaccountname" style="width:120%;">
                                                                                    <h4>Add New Account Name</h4>
                                                                                    <div class="add-popup-body">
                                                                                        <div class="form-outer">
                                                                                            <div class="col-sm-12">
                                                                                                <label>Account Name<em class="red-star">*</em></label>
                                                                                                <input class="form-control capital customValidateAccountName" data_required="true" data_max="150" type="text" name="@account_name" placeholder="Add New Account Name"/>
                                                                                                <span class="customError required"></span>
                                                                                            </div>
                                                                                            <div class="btn-outer text-right">
                                                                                                <input type="button" class="blue-btn NewaccountnameSave"  value="Save">
                                                                                                <button type="button"  class="clear-btn clearFormReset" id="clearNewaccountname">Clear</button>
                                                                                                <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <label>Bank Account Number </label>
                                                                                <input class="form-control number_only hide_copy" maxlength="20"  type="text" name="bank_account_number" placeholder="Bank Account Number"/>

                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <label>Routing Number</label>
                                                                                <input class="form-control number_only hide_copy" maxlength="20"  type="text" name="routing_number" placeholder="Routing Number"/>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <label>Eligible for 1099</label>
                                                                                <select class="form-control" name="eligible_for_1099">
                                                                                    <option value="">Select</option>
                                                                                    <option value="1" selected="selected">Yes</option>
                                                                                    <option value="2">No</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-4">
                                                                                <label>Comments</label>
                                                                                <textarea class="form-control capital" name="comment" placeholder="Comments"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse">
                                                                    <span></span> Tax Information</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseSix" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-2">
                                                                            <label>Tax Payer Name</label>
                                                                            <input class="form-control capital" type="text" name="tax_payer_name" id="tax_payer_name" placeholder="Tax Payer Name" maxlength="50"/>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Tax Payer ID</label>
                                                                            <input class="form-control capital" type="text" name="tax_payer_id" placeholder="Tax Payer ID" maxlength="50"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse">
                                                                    <span></span> Payment Settings</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseSeven" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-3">
                                                                            <label>Consolidate Checks</label>
                                                                            <select class="form-control" name="consolidate_checks"><option value="">Select Consolidate Cheque</option><option value="1" selected="">Yes</option><option value="2">No</option></select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <label>Default GL Expense Account <a class="pop-add-icon" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#chartofaccountmodal"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control hide_copy" name="default_GL" id="default_security_bank_account"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <label>Order Limit </label>
                                                                            <input class="form-control amount number_only" type="text" placeholder="Eg:40.00"  name="order_limit" id="order_limit"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title" style="text-decoration: underline;">
                                                                <a class="onlinePaymentInfo" data-toggle="modal" id="onlinePaymentInfo" href="#financial-info" data-target="#financial-info">
                                                                    <span></span> Online Payments</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse">
                                                                    <span></span> Vendor Credential Control</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseEight" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer vendor-credential-control-class" id="vendor-credential-control-div">
                                                                        <div class="col-sm-2">
                                                                            <label>Credential Name</label>
                                                                            <input class="form-control capital" maxlength="20"  type="text" name="credentialName[]" placeholder="Credential Name"/>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Credential Type <a class="pop-add-icon CredentialTypeplus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control credential-options" name="credentialType[]"></select>
                                                                            <div class="add-popup" id='NewCredentialType' style="width:120%;">
                                                                                <h4>Add Credential Type</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Credential Type<em class="red-star">*</em></label>
                                                                                            <input class="form-control capital customValidatecredentialname" data_required="true" data_max="150" type="text" name="@credential_type" placeholder="Eg: License"/>
                                                                                            <span class="customError required"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer text-right">
                                                                                            <input type="button" class="blue-btn" id="NewCredentialTypeSave" value="Save">
                                                                                            <button type="button"  class="clear-btn clearFormReset" id="clearNewCredentialType">Clear</button>
                                                                                            <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Acquire Date </label>
                                                                            <input class="form-control acquireDateclass calander" type="text"  name="acquireDate[]" id="acquireDateid"/>
                                                                        </div>
                                                                        <div class="col-sm-2 commonendclass">
                                                                            <label>Expiration Date </label>
                                                                            <input class="form-control expirationDateclass calander" type="text" name="expirationDate[]" id="expirationDateid"/>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <label>Notice Period </label>
                                                                            <select class="form-control add-input" id="notice_period"
                                                                                    name="noticePeriod[]">
                                                                                <option value="">Select</option>
                                                                                <option value="4" <?= ($_SESSION[SESSION_DOMAIN]['default_notice_period'] == 4) ? 'selected' : '' ?>>5 day</option>
                                                                                <option value="1" <?= ($_SESSION[SESSION_DOMAIN]['default_notice_period'] == 1) ? 'selected' : '' ?>>1 Month</option>
                                                                                <option value="2" <?= ($_SESSION[SESSION_DOMAIN]['default_notice_period'] == 2) ? 'selected' : '' ?>>2 Month</option>
                                                                                <option value="3" <?= ($_SESSION[SESSION_DOMAIN]['default_notice_period'] == 3) ? 'selected' : '' ?>>3 Month</option>
                                                                            </select>

                                                                            <a class="add-icon credential-control-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                            <a class="add-icon credential-control-minus" style="display: none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse">
                                                                    <span></span> Notes</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseNine" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer vendor-notes-clone-divclass notesDateTime" id="vendor-notes-clone-div">
                                                                        <div class="col-sm-4">
                                                                            <div class="notes_date_right_div">
                                                                                <textarea class="notes capital notes_date_right" name="note[]" ></textarea></div>
                                                                            <a class="add-icon-textarea vendor-notes-plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                            <a class="add-icon-textarea vendor-notes-minus" style="display: none" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse">
                                                                    <span></span> File Library</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseTen" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer2">
                                                                        <div class="col-sm-12">
                                                                            <button type="button" id="add_libraray_file" class="green-btn">Click Here to Upload</button>
                                                                            <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                                            <button type="button" class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="row" id="file_library_uploads">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse">
                                                            <span></span> Custom Fields</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseEleven" class="panel-collapse collapse  in">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="form-outer2">
                                                                <div class="col-sm-12">
                                                                    <div class="custom_field_html">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right min-height-0">
                                                                    <div class="btn-outer min-height-0">
                                                                        <button type="button"  id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">New Custom Field</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btn-outer text-right" style="padding-right: 15px !important;">
                                                <input type="submit" id="btnSaveNewVendor" class="blue-btn" value="Save">
                                                <button type="button"  class="grey-btn clear-btn clearFormReset" id="clearVendorForm">Clear</button>
                                                <input type="button" class="grey-btn cancel-all"  value="Cancel">

                                            </div>
                                        </div>
                                        </form>
                                        <!--                                        <button id="bofaButton">Open Link - Bank of America</button>-->
                                    </div>
                                    <!-- Accordian Ends -->
                                </div>
                            </div>
                        </div>


                    </div>
                    <!--tab Ends -->

                </div>

            </div>
        </div>
</div>
<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SITE_URL ?>/images/loading.gif'/>
    </div>
</div>
<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="vendor">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control capital" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">

                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button rel="custom_field" type="submit" class="grey-btn clearVendorcredentialForm" >Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->
<!-- Wrapper Ends -->
<div class="container">
    <div class="modal fade" id="chartofaccountmodal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Chart Of Account</h4>
                </div>
                <div class="modal-body">
                    <form name="add_chart_account_form" id="add_chart_account_form_id">
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Account Type <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" name="account_type_id" id="account_type_id"></select>
                                            <span id="account_type_idErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Code <em class="red-star">*</em></label>
                                            <input id="account_code" name="account_code" maxlength="4" placeholder="Eg: 1110" class="form-control capital" type="text"/>
                                            <span id="account_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Name <em class="red-star">*</em></label>
                                            <input id="com_account_name" name="account_name" maxlength="50" placeholder="Eg: Property Trust" class="form-control capital" type="text"/>
                                            <span id="account_nameErr" class="error"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Reporting Code <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" id="reporting_code" name="reporting_code">
                                                <option value="">Select</option>
                                                <option value="1">C</option>
                                                <option value="2">L</option>
                                                <option value="3">M</option>
                                                <option value="4">B</option>
                                            </select>
                                            <span id="reporting_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Sub Account of</label>
                                            <select class="fm-txt form-control" id="sub_account" name="sub_account">
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Status</label>
                                            <select class="fm-txt form-control" id="status" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>
                                                <div class="check-outer">
                                                    <input name="is_default" id="is_default" type="checkbox"/>
                                                    <label>Set as Default</label>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <select class="fm-txt form-control" id="posting_status" name="posting_status">
                                                <option value="1">Posting</option>
                                                <option value="0">Non Posting</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="btn-outer text-right">
                                        <input type="hidden" value="" name="chart_account_edit_id" class="form-control" id="chart_account_edit_id"/>
                                        <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                        <button type="button"  class="clear-btn clearFormReset" id="clearChartsOfAccountsModal">Clear</button>
                                        <button type="button"  class="grey-btn" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Online Payment Method-->
<div class="container">
    <div class="modal fade" id="financial-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="steps">
                        <ul>
                            <li>
                                <a id="paymentDiv" class="paymentDiv active" href="javascript:;">1</a>
                                <span>Payment</span>
                            </li>
                            <li>
                                <a id="basicDiv" class="basicDiv" href="javascript:;">2</a>
                                <span>Basic</span>
                            </li>
                        </ul>
                    </div>
                    <div class="apx-adformbox-content">
                        <form method="post" id="addStep1Form">
                            <input type="hidden" id="stripe_vendor_id" value="">
                            <input type="hidden" id="company_user_id" value="">
                            <h3>Payment Method <em class="red-star">*</em></h3>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label></label>
                                        <select class="form-control" name="payment_method" id="payment_method">
                                            <option value="1">Credit Card/Debit Card</option>
                                            <!--                                            <option value="2">ACH</option>-->

                                        </select>
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Type<em class="red-star">*</em></label>
                                        <img src="/company/images/card-payment.png" style="width: 190px;">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Holder First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="ffirst_name" name="cfirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Holder Last Name</label>
                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="flast_name" name="clast_name">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Number <em class="red-star">*</em></label>
                                        <input class="form-control hide_copy" type="text" id="ccard_number" name="ccard_number" placeholder="1234 1234 1234 1234" maxlength="16">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label> Expiry Year <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_year" name="cexpiry_year">
                                        </select>
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 fphone_num">
                                        <label> Expiry Month <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_month" name="cexpiry_month">
                                        </select>
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>CVC<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="ccvv" name="ccvv" maxlength="5">
                                        <span class="fbirth_dateErr error red-star"></span>
                                    </div>
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Company <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control capital" id="cCompany" name="cCompany" value="">-->
<!--                                        <span class="fzipcodeErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4 clear">-->
<!--                                        <label>Card Billing Phone Number <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control phone_format" id="cphoneNumber" name="cphoneNumber" value="" >-->
<!--                                        <span class="fcityErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Billing Address Line 1 <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control capital" id="caddress1" name="caddress1" value="">-->
<!--                                        <span class="fstateErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Billing Address Line 2 </label>-->
<!--                                        <input type="text" class="form-control capital" id="caddress2" name="caddress2" value="">-->
<!--                                        <span class="faddressErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>City<em class="red-star">*</em></label>-->
<!--                                        <input class="form-control capsOn capital" type="text" id="ccity" name="ccity">-->
<!--                                        <span class="fbusinessErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>State/Province <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control cstate capital"  name="cstate" value="">-->
<!--                                        <span class="fssnErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Zip/Postal Code<em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control czip_code capital"  name="czip_code" value="--><?php //$_SESSION[SESSION_DOMAIN]['default_zipcode'] ?><!--" >-->
<!--                                        <span class="faccount_holderErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Country <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control ccountry capital"  name="ccountry" value="">-->
<!--                                        <span class="faccount_holdertypeErr error red-star"></span>-->
<!--                                    </div>-->

                                </div>
                            </div>
                            <div class="row" style="text-align: center;">
                                <button  class="blue-btn">Continue</button>
                            </div>

                        </form>
                    </div>
                    <div class="verification-message"><p></p></div>
                    <div class="basic-payment-detail" style="display: none;">
                        <div class="row">
                            <form id="addStep2Form">
                                <div class="form-outer">
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Country <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control ccountry capital"  type="text" id="acc_country" name="country">-->
<!--                                    </div>-->
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control capital ccountry capital" name="country" id="acc_country" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fcountryErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="acc_mcc" name="mcc" readonly="">
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="url" name="url">
                                    </div>

                                    <div class="col-sm-4 col-md-3">
                                        <label>Account Type <em class="red-star">*</em></label>
                                        <input class="form-control account_type"  value="Indiviual" type="text" id="acc_account_type" name="account_type" readonly>
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Account Number <em class="red-star">*</em></label>
                                        <input class="form-control account_number" type="text" id="acc_account_number" name="account_number" maxlength="16">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Routing Number <em class="red-star">*</em></label>
                                        <input class="form-control routing_number" type="text" id="acc_routing_number" name="routing_number" maxlength="9">
                                    </div>

                                    <div class="col-sm-4 col-md-3">
                                        <label>Line1 <em class="red-star">*</em></label>
                                        <input class="form-control line1 capital" type="text" id="acc_line1" name="line1">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Address Line2 </label>
                                        <input class="form-control address_line2 capital" type="text" id="acc_address_line2" name="address_line2">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>POSTAL CODE <em class="red-star">*</em></label>
                                        <input class="form-control postal_code czip_code" type="text" id="acc_postal_code" name="postal_code">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input class="form-control cstate capital"  type="text" id="acc_state" name="state">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input class="form-control ccity capital"  type="text" id="acc_city" name="city">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control email" type="text" id="acc_email" name="email">
                                    </div>

                                    <div class="col-sm-12 date-outer-form">
                                        <label> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>

                                        <div class="col-sm-12 col-md-3 fphone_num  pad-none">
                                            <select class="form-control"  id="fpday" name="day" ><option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <select class="form-control"  id="fpmonth" name="month" ><option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 date-year">
                                            <select class="form-control"  id="fpyear" name="year" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                    </div>


                                    <div class="col-sm-4 col-md-3">
                                        <label>First_name <em class="red-star">*</em></label>
                                        <input class="form-control first_name capital" type="text" id="acc_first_name" name="first_name">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Last_name <em class="red-star">*</em></label>
                                        <input class="form-control last_name capital" type="text" id="acc_last_name" name="last_name">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input class="form-control phone phone_format" type="text" id="acc_phone" name="phone">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>SSN Last 4 <em class="red-star">*</em></label>
                                        <input class="form-control ssn_last" type="text" id="acc_ssn_last" name="ssn_last" maxlength="4">

                                    </div>



                                    <div class="col-sm-12 text-center">
                                        <button  id="savefinancial" class="blue-btn">Submit</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="company-basic-payment-detail" style="display: none;">
                        <div class="row">

                            <form method="post" id="financialCompanyInfo">
                                <input type="hidden" id="company_document_id" name="company_document_id" value="">
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control capital" name="fcountry"  id="fcountry" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fcountryErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fcmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcurl" name="furl">
                                        <span class="furlErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcbusiness" name="fbusiness"  value="Company" readonly>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="fcaccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fprouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress" name="fcaddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress2" name="fcaddress2" >
                                        <span class="fcaddress2Err error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fczipcode" name="fczipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fccity" name="fccity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcstate" name="fcstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Name <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcname" name="fcname" value="">
                                        <span class="fcnameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fcphone_number" name="fcphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="fcphone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Tax_id <em class="red-star">*</em></label>
                                        <input   class="form-control" fixedLength="10" id="fctax_id" name="fctax_id" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Company Document <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Upload Company Document here.</span></div></label>
                                        <input type='file' name="company_document" id="company_document" />
                                        <span class="company_document_idErr error red-star"></span>
                                    </div>




                                </div>
                                <div class="col-sm-12">
                                    <div class="form-hdr">
                                        <h3>Person</h3>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-outer mt-15">
                                        <div class="col-sm-12 col-md-3">
                                            <label>First Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn capital" type="text" id="fpfirst_name" name="ffirst_name">
                                            <span class="ffirst_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Last Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn capital" type="text" id="fplast_name" name="flast_name">
                                            <span class="flast_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Email <em class="red-star">*</em></label>
                                            <input class="form-control" type="email" id="fpemail" name="femail" >
                                            <span class="femailErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Phone <em class="red-star">*</em></label>
                                            <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fpphone_number" name="fphone_number" value="">
                                            <span class="fphone_numberErr error red-star"></span>
                                            <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label> Day <em class="red-star">*</em></label>
                                            <select class="form-control fpday"  id="fppday" name="fday" >
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Month <em class="red-star">*</em></label>
                                            <select class="form-control fpmonth"  id="fppmonth" name="fmonth" >
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Year <em class="red-star">*</em></label>
                                            <select class="form-control fpyear"  id="fppyear" name="fyear" >
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fpssn" name="fssn" value="" maxlength="4">
                                            <span class="fssnErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 1 <em class="red-star">*</em></label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="fpaddress" name="faddress" >
                                            <span class="faddressErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 2</label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="fpaddress2" name="faddress2" >
                                            <span class="faddress2Err error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Postal Code <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fpzipcode" name="fzipcode" value=""  maxlength="9">
                                            <span class="fzipcodeErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>City <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fpcity" name="fcity" value="" >
                                            <span class="fcityErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>State <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fpstate" name="fstate" value="" readonly>
                                            <span class="fstateErr error red-star"></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <button type="submit"  id="savecompanyfinancial" class="blue-btn">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="modal fade" id="financial-infotype" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>
                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="financial-infotype-radio">
                        <label class="radio-inline">
                            <input type="radio" name="companytype" checked value="individual"> Individual
                        </label>
                        <input type="hidden" value="<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id'] ?>" id="hidden_company_id" >
                        <label class="radio-inline">
                            <input type="radio" name="companytype" value="company" >Company
                        </label>
                    </div>

                    <div class="financial-infotype-btn text-right">
                        <button type="button"  id="savefinancialtype" class="blue-btn">Submit</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!--Online Payment Method-->
</div>
<script type="text/javascript">
    $("#people_top").addClass('active');
    $("#people_top").addClass('active');
    var detailCompanyArr=[];
    var stripe_detail_form_data = '';
    var stripe_card_detail_form_data = '';
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var property_unique_id = '';
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
    var manager_id = "";
    var attachGroup_id = "";
    var credential_notice_period =  "<?php echo $_SESSION[SESSION_DOMAIN]['default_notice_period']; ?>";
    var defaultFormData = '';
    var defaultIgnoreArray = [];
    $(document).ready(function () {
        setTimeout(function(){
            defaultFormData = $('#FormSaveNewVendor').serializeArray();
            defaultIgnoreArray = [];
        },300);
    });
    $(function () {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function () {
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function () {
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function () {
        $(".slide-toggle").click(function () {
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function () {
        $(".slide-toggle2").click(function () {
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    getCompanyDefaultData();
    function getCompanyDefaultData() {
        $.ajax({
            type: 'post',
            url: '/get-company-default-data',
            data: {
                class: 'commonPopup',
                action: 'getCompanyDefaultData'},
            success: function (response) {
                var res = JSON.parse(response);
                $('.citys').val(res.record.city);
                $('.states').val(res.record.state);
                $('#country').val(res.record.country);
                $('#zip_code').val(res.record.zip_code);
            },
        });

    }

    $(document).on('click','#clearVendorForm',function(){
                resetFormClear('#FormSaveNewVendor',['vendor_random_id','vendor_type_options','additional_phoneType[]','additional_countryCode[]','zipcode','country','state','city','default_GL','acquireDate[]','expirationDate[]','noticePeriod[]','consolidate_checks'],'form',false,defaultFormData,defaultIgnoreArray);
                $('#file_library_uploads').html('');
    });



    $(document).on('click','#clearMaritalStatus',function(){
        resetFormClear('#selectPropertyMaritalStatus1',[],'div',false);
    });
</script>

<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/autonumeric.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/autonumericDecimal.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/commonPopup.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<!-- custom field js -->
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js" type="text/javascript"></script>
<!-- file library js -->
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/file_library.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/addVendor.js" type="text/javascript"></script>
<!--<script src="--><?php //echo COMPANY_SITE_URL; ?><!--/js/payments/plaidIntialisation.js"></script>-->
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/onlinePayment.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/vendor/clearData.js"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
