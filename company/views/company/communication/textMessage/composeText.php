<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
$user_type = explode("/",$_SERVER['REQUEST_URI']);
if(isset($user_type[1]) && $user_type[1] == 'Owner')
{
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Owner_Portal' ;
    $button_url = '/Owner';

}elseif(isset($user_type[1]) && $user_type[1] == 'Communication'){

    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Communication_Portal' ;
    $button_url = '';

}elseif(isset($user_type[1]) && $user_type[1] == 'Vendor'){
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Vendor_Portal' ;
    $button_url = '/Vendor';
}
elseif(isset($user_type[1]) && $user_type[1] == 'Tenant'){
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Tenant_Portal' ;
    $button_url = '/Tenant';

}else{
}

?>
<section class="main-content">
    <div class="container-fluid">
        <div class="row">
            <div class="bread-search-outer">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="breadcrumb-outer">
                            Communication &gt;&gt; <span>Email </span>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text" />
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-sm-12">

            </div>
            <div class="col-sm-12">
                <div class="content-section">
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <div class="main-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"  class="active"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                        </ul>
                        <div role="tabpanel" class="tab-pane active" id="communication-one">
                            <!-- Sub Tabs Starts-->
                            <div class="property-status" style="margin-top: 50px;">
                                <div class="row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                            <a href="<?php echo $button_url; ?>/Communication/InboxText" class="blue-btn">Inbox</a>
                                            <a class="blue-btn" href="<?php echo $button_url; ?>/Communication/TextMessageDrafts">Drafts</a>
                                            <a data-urltype="<?php echo $button_url; ?>" href="javascript:void(0)" class="blue-btn margin-right compose-text">Compose Message</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-form">
                                <div class="accordion-outer">
                                    <form id="sendEmail">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                Text Message</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body compose-email2">
                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                    <a class="blue-btn compose-email-btn" style="float: left;">
                                                                        Send
                                                                    </a>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <input class="form-control mail_type" name="mail_type" type="hidden" value="draft" />
                                                                        <div class="col-sm-2">
                                                                            <label>To<em class="red-star">*</em></label>
                                                                        </div>
                                                                        <div class="col-sm-10 to_field">
                                                                                <span>
                                                                                    <input class="form-control to" name="to" autofocus type="text"/>
                                                                                </span>

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <a class="add-recipient-link addToRecepent" href="#"> Add Recipients</a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                    <input class="form-control to_name" name="to_name" type="text" style="display:none;"/>

                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>
                                                                                Subject <em class="red-star">*</em>
                                                                            </label>
                                                                            <span style="display: none;">
                                                                                <input class="form-control to_usertype" name="to_usertype" type="text" style="display:none;"/>
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                                <span>
                                                                                    <input class="form-control subject" name="subject" type="text"/>
                                                                                </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>Message
                                                                                <em class="red-star">*</em></label>
                                                                        </div>
                                                                        <input type="hidden" value="" id="compose_mail_id" name="edit_id">
                                                                        <div class="col-sm-10">
                                                                                <span>
                                                                                    <textarea id="mesgbody" style="height:150px;" class="form-control capital" name="mesgbody" maxlength="300"></textarea>
                                                                                    <strong style="color:#585858;float:left;width:100%; margin-top:6px;"><span id="chracter_count">300</span> Characters left</strong>
                                                                                </span>
                                                                            <br>  <br>  <br>  <br>
                                                                            <div class="btn-outer text-right">
                                                                                <a class="blue-btn compose-email-save-btn">Save</a>
                                                                                <button type="button" class="clear-btn email_clear">Clear</button>
                                                                                <a class="grey-btn" id="cancel_email" href="javascript:void(0)">
                                                                                    Cancel
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Accordian Ends -->
                                    </form>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
</section>

<div class="container">
    <div class="modal fade" id="torecepents" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 600px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Recipients </h4>
                </div>
                <div class="modal-body">
                    <div class="form-outer" style="float: none;height:auto;min-height: 300px;">
                        <div class="col-sm-6">
                            <label>Select <em class="red-star">*</em></label>
                            <select class="form-control selectUsers">
                                <option value="">Select</option>
                                <option value="2">Tenant</option>
                                <option value="4">Owner</option>
                                <option value="3">Vendor</option>
                                <option value="5">Other Contacts</option>
                                <option value="6">Guest Card</option>
                                <option value="10">Rental Application</option>
                                <option value="8">Employee</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label></label>
                            <br/>
                            <div class="blue-search">
                                <input type="text" value="" class="form-control" id="toSearch">
                                <span class="icon">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </span>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="userDetails"><table class="table" border="1px"><tbody><tr>
                                        <th>id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                    </tr><tr><td colspan="3" align="center"  bgcolor="#f7f7f7">
                                            No Record Found
                                        </td></tr></tbody></table></div>
                        </div>
                        <div class="popup_values" style="display: none;">
                            <input class="form-control popup_to" type="text"/>
                        </div>
                        <div class="col-sm-12">
                            <button class="blue-btn" id="SendselectToUsers">Done</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<script>
    $(document).on("click",".clear-btn.email_clear",function () {
        $("#sendEmail .label-info").text('');
        $("#sendEmail #mesgbody").val('');
        $("#sendEmail .subject").val('');
        //  resetFormClear('#sendEmail',[''],'form',true);
    });
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="<?php echo COMPANY_SITE_URL; ?>/css/bootstrap-tagsinput.css" />
<script src="<?php echo COMPANY_SITE_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<link rel="stylesheet" href="https://rawgit.com/timschlechter/bootstrap-tagsinput/master/src/bootstrap-tagsinput.css" />
<script src="https://rawgit.com/bassjobsen/Bootstrap-3-Typeahead/master/bootstrap3-typeahead.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/email/common.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/textMessage/compose-message.js"></script>
</body>
<style>
    .userDetails {
        float: left;
        width: 100%;
        margin-bottom: 14px;
    }
</style>
</html>