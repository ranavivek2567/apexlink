<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
    <style>
        label.error {
            width: 100% !important;
        }
        .form-row {
            float: left;
            width: 100%;
            padding: 15px;
        }
    </style>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">

        <link rel="stylesheet" type="text/css" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.datetimepicker.css"/>

    <!-- MAIN Navigation Starts -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->


    <section class="main-content">
        <div class="container-fluid">

            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication >> <span>In Touch</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12">

                </div>
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="main-tabs">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                                <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                                <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                                <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                                <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                                <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                                <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                                <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                                <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                                <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                                <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                                <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                                <li role="presentation" class="active"><a href="/Announcement/Announcements">Announcement</a></li>
                                <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                                <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>
                            </ul>
                            <div role="tabpanel" class="tab-pane active" id="communication-one">

                                <div class="mg-tp-20 form-outer">
                                    <div class="form-hdr">
                                        <h3>In-Touch</h3>
                                    </div>
                                    <div class="form-data">
                                        <form action="method" id="formIn-touch-data-save" >
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-box">
                                                    <div class="form-box-hdr">
                                                        In-Touch Information
                                                    </div>
                                                    <div class="form-box-data">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label class="text-right">Type <em class="red-star">*</em> <i class="fa fa-plus-circle typePopupAdd" aria-hidden="true"></i></label>
                                                                <span>
                                                            <select class="form-control" name="type" id="ddlType">
                                                              <option>Select</option>
                                                            </select>
                                                                <div class="add-popup typePopup" id="add_intouch_form">
                                                                        <h4>Add New Type</h4>
                                                                        <div class="add-popup-body">

                                                                        <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                        <label>Add New Type<em class="red-star">*</em></label>
                                                                            <textarea class="form-control customValidateType capital" data_required="true" data_max="20" name='@in_touch_type' id='vendor_type' placeholder="Add New Type"></textarea>
                                                                        <span class="customError required"></span>
                                                                        </div>

                                                                        <div class="btn-outer">
                                                                        <input type="submit"   class="blue-btn addTypePopup" value="Save" />
                                                                        <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                                        </div>
                                                                        </div>

                                                                        </div>
                                                                </div>
                                                          </span>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <label class="text-right">Category <em class="red-star">*</em></label>
                                                                <span>
                                                                    <select class="form-control" name="category" id="ddlCategory">
                                                                        <option value=" ">Select</option>
                                                                        <option value="Person">Person</option>
                                                                        <option value="Property">Property</option>
                                                                        <option value="Building">Building</option>
                                                                        <option value="Unit">Unit</option>
                                                                        <option value="Lease">Lease</option>
                                                                        <option value="WorkOrder">Work Order</option>
                                                                        <option value="Portfolio">Portfolio</option>
                                                                        <option value="Group">Group</option>
                                                                    </select>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <label class="text-right">Assigned User</label>
                                                                <span>
                                                                <select class="form-control" name="assigned_user" id="ddlassigned_user">
                                                                  <option>Select</option>
                                                                </select>
                                                              </span>
                                                            </div>
                                                            <label class="text-right"></label>
                                                            <span class="subscribed-automatically">
                                                  Assignment Subscribed Automatically
                                                </span>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="form-box">
                                                    <div class="form-box-hdr">
                                                        Optional Due Date/Time
                                                    </div>
                                                    <div class="form-box-data">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label class="text-right">Date</label>
                                                                <span>
                                                    <input class="form-control clear-input" name="optional_due_date" type="text" id="optional_due_date"/>
                                                    <button class="clear-btn" type="button" id="cleardate-picker">Clear</button>
                                                  </span>
                                                            </div>
                                                            <div>
                                                                <label class="text-right"></label>
                                                                <span>
                                                    <div class="check-outer">
                                                      <input type="checkbox" class="completedMark-check" name="status"/>
                                                      <label>Mark Completed</label>
                                                    </div>
                                                  </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="form-box">
                                                    <div class="form-box-hdr">
                                                        Subscribe User
                                                    </div>
                                                    <input type="hidden" id="edit-in-touch-id" value="<?= $_GET['id']; ?>">
                                                    <div class="form-box-data">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <select name="subscribed_user" multiple="multiple"  id="subscription_user">
                                                                </select>

                                                                <button class="clear-btn" type="button" id="AddSubUser"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add</button>
                                                            </div>
                                                            <div class="grid-outer" id="subscribeduserTable" style="display: none;">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">

                                                                        <tbody class="subscribeduserTableTR">
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>


                                                </div>
                                                <div class="btn-outer" style="text-align: right;">
                                                    <button type="submit" id="save_announcement" class="blue-btn add_form_submit_btn">Update</button>
                                                    <button type="button" class="clear-btn email_clear">Reset</button>
                                                    <button type="button"  class="grey-btn add_intouch_cancel">Cancel</button>
                                                </div>

                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-box">
                                                    <div class="form-box-hdr">
                                                        In-Touch Details
                                                    </div>
                                                    <div class="form-box-data">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label class="text-right">For <em class="red-star">*</em></label>
                                                                <span>
                                                    <input class="form-control capital" type="text" name="assigned_for" id="assigned_for_id"/>
                                                  </span>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <label class="text-right">Subject <em class="red-star">*</em></label>
                                                                <span>
                                                    <input class="form-control capital" type="text" name="subject" id="subject_id"/>
                                                  </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <!-- Form-box-ends -->

                                                <div class="form-box">
                                                    <div class="form-box-hdr">
                                                        In-Touch Notes
                                                    </div>
                                                    <div class="form-box-data">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label class="text-right">New Notes</label>
                                                                <span>
                                                                <textarea class="form-control capital" name="notes"></textarea>
                                                              </span>

                                                                <div class="AppendNotesDiv"></div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <!-- Form-box-ends -->

                                                <div class="form-box">
                                                    <div class="form-box-hdr">
                                                        Automated Due Date Reminders for all Users (to all Subscribes)
                                                    </div>
                                                    <div class="form-box-data automated">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label class="text-right remindercheck">Send Due Date Reminders</label>
                                                                <span>
                                                    <input type="checkbox" name="send_reminder" class="remindercheckClass"/>
                                                  </span>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <label class="text-right">Remind Subscriber before due</label>
                                                                <span>
                                                        <select name="remind_days" id="remind_days">
                                                            <?php for($i=0; $i <=31;$i++){?>
                                                                <option value="<?= $i; ?>"><?= $i; ?></option>
                                                           <?php } ?>

                                                        </select> Days
                                                  </span>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <label class="text-right">Remind Subscriber before due</label>
                                                                <span>
                                                    <select name="remind_due_hours" id="remind_due_hours">
                                                           <?php for($h=0; $h <=24;$h++){?>
                                                               <option value="<?= $h; ?>"><?= $h; ?></option>
                                                           <?php } ?>
                                                    </select> Hours
                                                  </span>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <label class="text-right">Remind Subscriber before due</label>
                                                                <span>
                                                    <select name="remind_overdue_hours" id="remind_overdue_hours">
                                                         <?php for($Sh=0; $Sh <=24;$Sh++){?>
                                                             <option value="<?= $Sh; ?>"><?= $Sh; ?></option>
                                                         <?php } ?>
                                                    </select> Hours
                                                  </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Form-box-ends -->

                                            </div>

                                        </div>
                                        </form>

                                    </div>
                                </div>

                            </form>
                            </div>
                            <!-- Sub Tabs Starts-->


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
    <!-- Wrapper Ends -->

    <script>
        var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
        var upload_url  = "<?php echo SITE_URL; ?>";
        var LoginUserName = "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
    </script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/announcement/fileLibrary.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/announcement/addAnnouncement.js"></script>


    <script>
        function goBack() {
            window.history.back();
        }

        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
            $("#communication_top").addClass("active");
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });



    </script>
    <script>
        $(document).on("click",".clear-btn.email_clear",function () {
           window.location.reload();
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/inTouch/addInTouch.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.datetimepicker.full.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js" type="text/javascript"></script>


    <script>
        jQuery('#optional_due_date').datetimepicker({
            calendarWeeks: true,
            locale: moment.locale('se'),
        });
    </script>



    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>