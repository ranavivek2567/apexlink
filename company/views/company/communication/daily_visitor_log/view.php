<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<style>
    .panel-htop .combo-panel {
        margin-top: 9px !important;
        margin-left: -2px !important;
    }



</style>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Daily Visitor </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" ><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation" class="active"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Daily Visitor</h3>
                                </div>

                                <div class="form-data ">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-3 select-visitor-field">
                                            <label>Select/Add Visitor <a class="pop-add-icon addVisitor" data-toggle="modal" data-target="#addCaller" href="javascript:;"><i class="fa fa-plus-circle addVisitor" aria-hidden="true"></i></a></label>
                                            <input class="form-control selectComboGrid" type="text" placeholder="Click here to Select a Visitor"/>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                            <label></label>
                                            <div class="check-outer">
                                                <input type="radio" name="filter_search" checked class="filter_class" value='0'> <label>By Weekly/Bi-Weekly/Monthly</label>
                                            </div>
                                            <div class="check-outer clear">
                                                <input type="radio" name="filter_search" class="filter_class" value='1'> <label>By Date Range</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2" id="ByWeek" style="display: block;">
                                            <label></label>
                                            <select id="ByWeekFilter" class="form-control">
                                                <option value="all">Select</option>
                                                <option value="Weekly">Weekly</option>
                                                <option value="Bi-Weekly">Bi-Weekly</option>
                                                <option value="Monthly">Monthly</option>
                                            </select>
                                        </div>
                                        <div class="date_range" id="ByMonth" style="display:none;">
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <label></label>
                                                <input type="text" class="form-control form-control-1 input-sm from123" placeholder="Select Month" >
<!--                                                <input class="form-control" id="month_date" readonly="" type="text" name="month_date">-->
                                            </div>
                                        </div>
                                        <div class="date_range" id="ByDate" style="display: none;">
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <label></label>
                                                <input class="form-control start_date" id="start_date" readonly="" type="text" name="start_date">
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <label></label>
                                                <input class="form-control end_date" id="end_date" readonly="" type="text" name="end_date">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label></label>
                                            <button class="blue-btn callLogSearch">Search</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="grid-outer">
                                                                        <div class="apx-table">
                                                                            <div class="table-responsive">
                                                                                <table id="timeSheetTable" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn genratePdf">PDF</button>
                                                                        <button type="button" id="export_property_button" class="blue-btn email_button">Excel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>
                        <!-- Sub Tabs Starts-->
                    </div>

                </div>
            </div>
            <!--Tabs Ends -->
        </div>
    </section>
</div>

<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SUBDOMAIN_URL ?>/images/loading.gif'/>
    </div>
</div>
<!-- Wrapper Ends -->

<!--Modal Popup-->

<div id="addCaller" class="modal fade" role="dialog">
    <div class="modal-dialog spotlight-pop">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Visitors</h4>
            </div>
            <div class="modal-body">
                <form id="AddVisitorsForm">
                    <input type="hidden" id="editDailyVisitorId" name="edit_id" value="">
                    <div class="row">
                        <div class="form-outer">
                            <div class="col-sm-4">
                                <label>Visitor <em class="red-star">*</em></label>
                                <input class="form-control capital" type="text" name="visitor" id="visitor" placeholder="Visitor">
                            </div>
                            <div class="col-sm-4">
                                <label>Company</label>
                                <input class="form-control capital" type="text" name="company" id="company" placeholder="Company">
                            </div>
                            <div class="col-sm-4">
                                <label>Phone</label>
                                <input id="visit_phone_number" class="form-control phone_number_format" maxlength="12" type="text" name="phone" placeholder="Eg: 123-456-7894">
                            </div>
                            <div class="col-sm-4">
                                <label>Date</label>
                                <input class="form-control date calander" id="date" readonly type="text" name="date" placeholder="Eg: <?php echo date('Y-m-d'); ?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Time In</label>
                                <input class="form-control time" id="time_in" type="text" name="time_in">
                            </div>
                            <div class="col-sm-4">
                                <label>Time Out</label>
                                <input class="form-control time" id="time_out" type="text" name="time_out">
                            </div>
                            <div class="col-sm-4">
                                <label>Reason for Visit <a class="pop-add-icon addReasonsDiv" href="javascript:;">
                                        <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                    </a></label>
                                <select class="form-control call_type" id="reasonsData" name="reason_for_visit"><option>Select</option></select>
                                <div class="add-popup" id="addReasonsDiv">
                                    <h4>Add New Reason</h4>
                                    <div class="add-popup-body">
                                        <div class="form-outer">
                                            <div class="col-sm-12">
                                                <label>Add New Reason <em class="red-star">*</em></label>
                                                <input id="new_reasons" name="@reason" class="form-control customValidateReasons  capital" maxlength="50" type="text" placeholder="Add New Reason" data_required="true" >
                                                <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                            </div>
                                            <div class="btn-outer">
                                                <button type="button" class="blue-btn add_single1" id="addReasonsPopUpSave">Save</button>
                                                <input type="button" class="grey-btn cancelAddReasons" value="Cancel">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label>Length of Visit(Hours)</label>
                                <input class="form-control" type="text" id="total_hours" maxlength="300" placeholder="Length of Visit" readonly value="2.0" name="length_of_visit">
                            </div>
                            <div class="col-sm-4">
                                <label>Action Taken</label>
                                <input class="form-control" type="text" maxlength="300" placeholder="Action Taken" value="No Action Taken" name="action_taken">
                            </div>
                            <div class="col-sm-4">
                                <label>Follow-up Needed <em class="red-star">*</em></label>
                                <div class="check-outer">
                                    <input type="radio" id='followNo' name="follow_up" checked="checked" value="0"/> No
                                </div>
                                <div class="check-outer">
                                    <input type="radio" id='followYes' name="follow_up" value="1"/> Yes
                                </div>
                            </div>
                            <div class="col-sm-8 clear">
                                <label>Notes</label>
                                <div class="notes_date_right_div"> <textarea class="form-control capital notes_date_right" id="daily_note" name="note" placeholder="Notes for Daily Visitor" maxlemgth="500" ></textarea></div>

                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" id="saveDailyVisitor" class="blue-btn" >Save</button>
                        <button type="button" class="clear-btn email_clear">Clear</button>
                        <button type="button" class="grey-btn cancel_phone_call_log" >Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="PhoneCallLogModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content pull-left">
            <div class="modal-header" style="border-bottom: unset;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                </a>
                <input type="button" class="blue-btn pull-left" onclick="PrintElem('#phone_call_log_content')" onclick="window.print();"  value="Print"/>
                <input type="hidden" id="announcementSys_id" value="">

            </div>
            <div class="modal-body pull-left" style="height: 487px; overflow: auto;" id="phone_call_log_content">


            </div>

        </div>
    </div>
</div>
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>
    $(document).on("click",".clear-btn.email_clear",function () {

        resetFormClear('#AddVisitorsForm',['time_in','time_out','length_of_visit'],'form',false);
    });
</script>
<!--Modal Popup Ends-->
<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $('.calander').datepicker({
        yearRange: '1919:2050',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $('.calander').val(date);
</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/monthpicker.min.js" type="text/javascript"></script>
<link href="<?php echo COMPANY_SITE_URL; ?>/css/MonthPicker.min.css" rel="stylesheet">
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
<link href="<?php echo COMPANY_SITE_URL; ?>/css/easyui.css" rel="stylesheet">
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/communication/dailyVisitorLog.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/dailyVisitorLog/dailyVisitorLogAjax.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/communication/dailyVisitorLog/dailyVisitorLogList.js" type="text/javascript"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
