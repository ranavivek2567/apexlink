<?php
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content" id="conversation-page">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Conversation</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->

                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs  mb-15" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation" class="active"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/NewInTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>
                        </ul>
                        <!-- Nav tabs -->

                        <ul class="nav nav-tabs conversation-tabList" role="tablist">
                            <li role="presentation" class="active"><a href="#conversation-one" id="tenant_tab" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Tenant</a></li>
                            <li role="presentation"><a href="#conversation-two" id="owner_tab" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Owner</a></li>
                            <li role="presentation"><a href="#conversation-three" id="vendor_tab" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">Vendor</a></li>
                        </ul>
                        <!-- Tab panes -->

                        <div class="tab-content">

                            <!-- Tenant Conversation Starts-->
                            <div role="tabpanel" class="tab-pane active" id="conversation-one">

                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>Conversation <a class="conversation-link back" id="add_tenant_conversation_btn"> New Conversation</a></h3>
                                    </div>
                                    <div class="form-data tenant_conversation_div">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Property Manager</label>
                                                <select class="form-control select_mangers_option" id="">
                                                    <option>Select</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Tenant</label>
                                                <select class="form-control user_id" id="user_id">
                                                    <option>Select</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="conversation-outer">
                                                    <ul class="conversation_list">

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="add_tenant_conversation_div" style="display: none;">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3> New Conversation</h3>
                                        </div>
                                        <div class="form-data">
                                            <form id="add_tenant_conversation_form_id">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Tenant <em class="red-star">*</em></label>
                                                        <select class="form-control selected_user_id" name="selected_user_id" ></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Problem Category <em class="red-star">*</em>
                                                            <a class="pop-add-icon selectProblemCategory" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control problem_category" name="problem_category"></select>
                                                        <div class="add-popup" id="selectProblemCategoryPopup">
                                                            <h4>Add Problem Category</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Add New Problem Category <em class="red-star">*</em></label>
                                                                        <input class="form-control problem_category_src customValidateGroup capital" maxlength="50" type="text" placeholder="Add New Problem Category" data_required="true" >
                                                                        <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1"  data-table="conversation_problem_category" data-cell="problem_category_name" data-class="problem_category_src" data-name="problem_category">Save</button>
                                                                        <input type="button" class="clear-btn ClearProblemCategory" value="Clear">
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-6 clear">
                                                        <label>Description <em class="red-star">*</em></label>
                                                        <textarea rows="4" class="form-control capital" name="description"></textarea>
                                                    </div>
                                                </div>
                                                <div class="btn-outer top-marginCls text-right">
                                                    <button class="blue-btn add_form_submit_btn">Send</button>
                                                    <button type="button" class="clear-btn email_clear">Clear</button>
                                                    <button id="cancel_add_tenant_conversation"  type="button"  class="grey-btn">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Tenant Conversation Ends -->

                            <!-- Owner Conversation Starts-->
                            <div role="tabpanel" class="tab-pane" id="conversation-two">

                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>Conversation <a class="conversation-link back" id="add_owner_conversation_btn"> New Conversation</a></h3>
                                    </div>
                                    <div class="form-data owner_conversation_div">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Property Manager</label>
                                                <select class="form-control select_mangers_option"><option>Select</option></select>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Owner</label>
                                                <select class="form-control user_id"><option>Select</option></select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="conversation-outer">
                                                    <ul class="conversation_list">

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="add_owner_conversation_div" style="display: none;">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3> New Conversation</h3>
                                        </div>
                                        <div class="form-data">
                                            <form id="add_owner_conversation_form_id">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Owner <em class="red-star">*</em></label>
                                                        <select class="form-control selected_user_id" name="selected_user_id" ></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Problem Category <em class="red-star">*</em>
                                                            <a class="pop-add-icon selectProblemCategory" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control problem_category" name="problem_category"></select>
                                                        <div class="add-popup" id="selectProblemCategoryPopup">
                                                            <h4>Add Problem Category</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Add New Problem Category <em class="red-star">*</em></label>
                                                                        <input class="form-control problem_category_src customValidateGroup capital" maxlength="50" type="text" placeholder="Add New Problem Category" data_required="true" >
                                                                        <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" class="blue-btn add_single1"  data-table="conversation_problem_category" data-cell="problem_category_name" data-class="problem_category_src" data-name="problem_category">Save</button>

                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-6 clear">
                                                        <label>Description <em class="red-star">*</em></label>
                                                        <textarea rows="4" class="form-control capital" name="description"></textarea>
                                                    </div>
                                                </div>
                                                <div class="btn-outer top-marginCls text-right">
                                                    <button class="blue-btn add_form_submit_btn">Send</button>
                                                    <button type="button" class="clear-btn email_clear">Clear</button>
                                                    <button id="cancel_add_owner_conversation"  type="button" class="grey-btn">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Owner Conversation Starts-->

                            <!-- Vendor Conversation Starts-->
                            <div role="tabpanel" class="tab-pane" id="conversation-three">
                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>Conversation <a class="conversation-link back" id="add_vendor_conversation_btn"> New Conversation</a></h3>
                                    </div>
                                    <div class="form-data vendor_conversation_div">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Property Manager</label>
                                                <select class="form-control select_mangers_option"><option>Select</option></select>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                <label>Vendor</label>
                                                <select class="form-control user_id"><option>Select</option></select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="conversation-outer">
                                                    <ul class="conversation_list">

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="add_vendor_conversation_div" style="display: none;">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3> New Conversation</h3>
                                        </div>
                                        <div class="form-data">
                                            <form id="add_vendor_conversation_form_id">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Vendor <em class="red-star">*</em></label>
                                                        <select class="form-control selected_user_id" name="selected_user_id" ></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <label>Problem Category <em class="red-star">*</em>
                                                            <a class="pop-add-icon selectProblemCategory" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control problem_category" name="problem_category"></select>
                                                        <div class="add-popup" id="selectProblemCategoryPopup">
                                                            <h4>Add Problem Category</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Add New Problem Category <em class="red-star">*</em></label>
                                                                        <input class="form-control problem_category_src customValidateGroup capital" maxlength="50" type="text" placeholder="Add New Problem Category" data_required="true" >
                                                                        <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn add_single1"  data-table="conversation_problem_category" data-cell="problem_category_name" data-class="problem_category_src" data-name="problem_category">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-6 clear">
                                                        <label>Description <em class="red-star">*</em></label>
                                                        <textarea rows="4" class="form-control capital" name="description"></textarea>
                                                    </div>
                                                </div>
                                                <div class="btn-outer top-marginCls text-right">
                                                    <button class="blue-btn add_form_submit_btn">Send</button>
                                                    <button type="button" class="clear-btn vendor_email_clear">Clear</button>
                                                    <button id="cancel_add_vendor_conversation" type="button" class="grey-btn">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Vendor Conversation Starts-->

                        </div>


                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
<script>
    $(document).on("click",".clear-btn.email_clear",function () {
        $(".note-editable").text('');

        resetFormClear('#add_owner_conversation_form_id',[''],'form',false);
    });
    $(document).on("click",".clear-btn.email_clear",function () {
        $(".note-editable").text('');

        resetFormClear('#add_tenant_conversation_form_id',[''],'form',false);
    });
    $(document).on("click",".clear-btn.vendor_email_clear",function () {
        $(".note-editable").text('');

        resetFormClear('#add_vendor_conversation_form_id',[''],'form',false);
    });
</script>
<script>var default_Image = "<?php echo COMPANY_SUBDOMAIN_URL ?>/images/profile-img.png";</script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/conversation/view.js"></script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
        $("#communication_top").addClass("active");
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });

        $(document).on('click','.ClearProblemCategory',function () {
            $('.problem_category_src').val('');
        });
    });



</script>

<!-- Jquery Starts -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
