<?php

/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Phone Call Log </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation" class="active"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="form-outer">
                                <div class="form-hdr">
                                  <h3>Phone Call Log</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Select/Add Caller <a class="pop-add-icon" data-toggle="modal" data-target="#addCaller" href="javascript:;"><i class="fa fa-plus-circle emptyCallLogModal" aria-hidden="true"></i></a></label>
                                            <input class="form-control all_call_users" type="text" />
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label></label>
                                            <div class="check-outer">
                                                <input type="radio" name="call_category" class="call_category" checked="checked" value="1"/> <label>By Call Category</label>
                                            </div>
                                            <div class="check-outer clear">
                                                <input type="radio" name="call_category"  class="call_category" value="2"/> <label>By Date Range</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2" id="call_category_filter" style="display:block">
                                            <label>&nbsp;</label>
                                            <select class="form-control" id="incoming_outgoing_call_type" >
                                                <option value="">Select</option>
                                                <option value="1">Incoming</option>
                                                <option value="2">Outgoing</option>
                                            </select>
                                        </div>
                                        <div id="date_range" class="date_range" style="display:none">
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>&nbsp;</label>
                                            <input class="form-control start_date" id="start_date" readonly type="text" name="start_date">

                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>&nbsp;</label>
                                            <input class="form-control end_date calander" id="end_date" readonly type="text" name="end_date">
                                        </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>&nbsp;</label>
                                            <button class="blue-btn callLogSearch">Search</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
<!--                                                                <div class="panel-heading">-->
<!--                                                                    <h4 class="panel-title">-->
<!--                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> Phone Call Log</a>-->
<!--                                                                    </h4>-->
<!--                                                                </div>-->
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class=" pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <table id="phone-call-logs" class="table table-bordered"></table>
                                                                            </div>
                                                                        </div>
                                                                        <div class="btn-outer text-right">
                                                                            <button type="submit" class="blue-btn preview_button">Preview</button>

                                                                            <button type="button" data-toggle="modal" data-target="#EmailModal" class="blue-btn email_button">Email</button>

                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->

<!--Modal Popup-->

<div id="addCaller" class="modal fade" role="dialog">
    <div class="modal-dialog spotlight-pop">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Phone Call Log</h4>
            </div>
            <div class="modal-body">
                <form id="add_phone_call_log_form" name="add_phone_call_log_form">
                   <input type="hidden" name="edit_phone_call_log_id" id="edit_phone_call_log_id" value="">
                <div class="row">
                    <div class="form-outer">
                        <div class="col-sm-4">
                            <label>Caller <em class="red-star">*</em></label>
                            <input class="form-control capsOn" type="text" name="caller_name" id="caller_name" maxlength="25">
                        </div>
                        <div class="col-sm-4">
                            <label>&nbsp;</label>
                            <div class="check-outer">
                                <input type="radio" name="incoming_outgoing" checked="checked" value="1"/> Incoming
                            </div>
                            <div class="check-outer">
                                <input type="radio" name="incoming_outgoing" value="2" /> Outgoing
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>Call For</label>
                            <input class="form-control capsOn" type="text" name="call_for" id="call_for" maxlength="25">
                        </div>
                        <div class="col-sm-4">
                            <label>Phone</label>
                            <input class="form-control phone_format" type="text" name="phone_number" id="call_phone_number" value="" >
                        </div>
                        <div class="col-sm-4">
                            <label>Date</label>
                            <input class="form-control date" id="date" readonly type="text" name="date">
                        </div>
                        <div class="col-sm-4">
                            <label>Time</label>
                            <input class="form-control time" id="time" readonly type="text" name="time">
                        </div>
                        <div class="col-sm-4">
                            <label>Call Type<a class="pop-add-icon selectCallType href="javascript:;">
                                    <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                </a></label>
                            <select class="form-control call_type" id="call_type" name="call_type_id"><option>Select</option></select>
                            <div class="add-popup" id="selectCallType">
                                <h4>Add New Phone Call Type</h4>
                                <div class="add-popup-body">
                                    <div class="form-outer">
                                        <div class="col-sm-12">
                                            <label>Add New Phone Call Type <em class="red-star">*</em></label>
                                            <input class="form-control ethnicity_src customValidateGroup customPhoneCallTypeValidation capsOn" type="text" placeholder="Add New Phone Call Type" data_required="true" >
                                            <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                        </div>
                                        <div class="btn-outer">
                                            <button type="button" class="blue-btn add_single1"  data-table="call_type" data-validation-class="customPhoneCallTypeValidation" data-cell="call_type" data-class="ethnicity_src" data-name="call_type_id">Save</button>
                                            <button type="button" class="clear-btn add_phone_clear">Clear</button>
                                            <input type="button" class="grey-btn" value="Cancel">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>Purpose of Call</label>
                            <input class="form-control capsOn" type="text" maxlength="300" name="call_purpose" id="call_purpose">
                        </div>
                        <div class="col-sm-4">
                            <label>Action Taken</label>
                            <input class="form-control capsOn" type="text" maxlength="300" name="action_taken" id="action_taken">
                        </div>
                        <div class="col-sm-4">
                            <label>Length of Call</label>
                            <input class="form-control capsOn" type="text" name="call_length" id="call_length">
                        </div>
                        <div class="col-sm-4 ">
                            <label>Follow-up Needed </label>
                            <div class="check-outer">
                                <input type="radio" name="follow_up" checked="checked" value="1" id="folow_up"/> No
                            </div>
                            <div class="check-outer">
                                <input type="radio" name="follow_up" value="2"/> Yes
                            </div>
                        </div>
                        <div class="col-sm-8 clear ">
                            <label>Notes</label>
                            <div class="notes_date_right_div">
                             <textarea class="form-control capsOn notesDateTime notes_date_right" name="notes" maxlemgth="500" id="notes" ></textarea>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="text-right">
                    <button type="submit" class="blue-btn phone_call_log_save_update" >Save</button>
                    <button type="button" class="clear-btn email_clear">Clear</button>
                    <button type="button" class="grey-btn cancel_phone_call_log" >Cancel</button>

                </div>
                </form>


            </div>
        </div>

    </div>

</div>
</div>
<!--Modal Popup Ends-->


<div class="modal fade" id="PhoneCallLogModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content pull-left">
            <div class="modal-header" style="border-bottom: unset;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                </a>
                <input type="button" class="blue-btn pull-left" onclick="PrintElem('#phone_call_log_content')" onclick="window.print();"  value="Print"/>
                <input type="hidden" id="announcementSys_id" value="">

            </div>
            <div class="modal-body pull-left" style="height: 487px; overflow: auto;" id="phone_call_log_content">


            </div>

        </div>
    </div>
</div>




<div class="modal" id="EmailModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content pull-left">
            <div class="modal-header" style="border-bottom: unset;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                </a>
                <h3 class="text-center">EMAIL</h3>

            </div>
            <div class="modal-body pull-left" id="phone_email_content">


            </div>

        </div>
    </div>
</div>
<!-- Wrapper Ends -->




<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication_top").addClass("active");
    });



</script>
<script>
    $(document).on("click",".clear-btn.email_clear",function () {
        resetFormClear('#add_phone_call_log_form',['date','time'],'form',false);
    });
    $(document).on("click",".clear-btn.add_phone_clear",function () {
        $(".form-control.ethnicity_src.customValidateGroup.customPhoneCallTypeValidation.capsOn.capital.valid").val('');
    });
</script>
<script>
    var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url  = "<?php echo SITE_URL; ?>";
   var login_user_name =  "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/phoneCallLog/calllog.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
<!--<link href="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/css/easyui.css" rel="stylesheet">-->

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
