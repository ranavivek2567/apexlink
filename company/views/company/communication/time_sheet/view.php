

<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Time Sheet </span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation" class="active"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="panel-heading">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="property-status">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="btn-outer text-right">
                                                    <a href="/Communication/AddTimeSheet"><input type="button" value="Add Employee Timesheet" class="blue-btn"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row property-status">
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label>Employee Name</label>
                                            <select id="TimeSheetUsers" class="form-control">
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-3 weekly-biweekly">
                                            <label>Pay Period Type</label>
                                            <div class="check-outer">
                                                <input type="radio" name="filter_search" checked class="filter_class" value='0'> <label>By Weekly/Bi-Weekly/Monthly</label>
                                            </div>
                                            <div class="check-outer clear">
                                                <input type="radio" name="filter_search" class="filter_class" value='1'> <label>By Date Range</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2" id="ByWeek" style="display: block;">
                                            <label></label>
                                            <select id="ByWeekFilter" class="form-control">
                                                <option value="all">Select</option>
                                                <option value="Weekly">Weekly</option>
                                                <option value="Bi-Weekly">Bi-Weekly</option>
                                                <option value="Monthly">Monthly</option>
                                            </select>
                                        </div>
                                        <div class="date_range" id="ByMonth" style="display: none;">
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <label></label>
                                                <input type="text" class="form-control form-control-1 input-sm from123" placeholder="Select Month" >


                                            </div>
                                        </div>
                                        <div class="date_range" id="ByDate" style="display: none;">
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <label></label>
                                                <input class="form-control start_date" id="start_date" readonly="" type="text" name="start_date">
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-2">
                                                <label></label>
                                                <input class="form-control end_date calander" id="end_date" readonly="" type="text" name="end_date">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                            <label></label>
                                            <button class="blue-btn callLogSearch">Search</button>
                                        </div>
                                    </div>
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Time Sheet</h3>
                                        </div>
                                        <div role="tabpanel" class="tab-pane active " id="receivables">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="apx-table">
                                                                                <div class="table-responsive overflow-unset">
                                                                                    <table id="timeSheetTable" class="table table-bordered">
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-outer text-right">
                                        <button type="button" id="print_preview" class="blue-btn">Print</button>
                                    </div>
                                    <!-- Regular Rent Ends -->
                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>
                        <!-- Sub tabs ends-->
                    </div>
                </div>
            </div>
            <!--Tabs Ends -->
        </div>
    </section>
</div>
<!-- Print Modal Start-->
<div class="modal fade" id="PhoneCallLogModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content pull-left">
            <div class="modal-header" style="border-bottom: unset;">
                <button type="button" class="close" data-dismiss="modal">×</button>
                </a>
                <input type="button" class="blue-btn pull-left" onclick="PrintElem('#phone_call_log_content')" onclick="window.print();"  value="Print"/>
                <input type="hidden" id="announcementSys_id" value="">

            </div>
            <div class="modal-body pull-left" style="height: 487px; overflow: auto;" id="phone_call_log_content">


            </div>

        </div>
    </div>
</div>
<!-- Print Modal End-->
<!-- Wrapper Ends -->
<!-- Footer Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/monthpicker.min.js" type="text/javascript"></script>
<link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/MonthPicker.min.css" rel="stylesheet">
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/timeSheet/timeSheetList.js" type="text/javascript"></script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication_top").addClass("active");
    });
</script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");

?>
