<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>New Package </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation" class="active"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="btn-outer text-right">
                                                        <a href="/Package/AddPackages"><input type="button" class="blue-btn" value="Add Package"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Package Tracker</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class=" pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="apx-table">
                                                                                    <div class="table-responsive">
                                                                                        <table id="packageTracker-table" class="table table-bordered">

                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
    <!--Model Save & Submit -->
    <div class="container">
        <div class="modal fade" id="myModalPackagePickUp" role="dialog">
            <input type="hidden" value="" class="emailPackageData">
            <input type="hidden" value="" class="phonePackageData">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">
                            Package Pickup
                        </h4>
                    </div>
                    <div class="modal-body ">
                        <div class="">
                            <form id="pickUpPackage_model">
                                <input type="hidden" id="idpickup" value="">
                                <input type="hidden" name="id" id="" value="">
                                <div class="custom_field_form">
                                    <div class="row">
                                        <div class="form-outer">
                                            <div class="col-sm-12 col-md-5 clear pickUpByPackage">
                                                <label>
                                                    Picked Up By
                                                </label>
                                                    <input type="text" name="pickUpByPackage" id="pickUpByPackage" class="form-control pickUpByPackage" value="Select Owner Name">
                                            </div>
                                            <div class="col-sm-12 col-md-5 clear pickUpByPackage">
                                                <label>
                                                    Pick Up Date
                                                </label>
                                                    <input class="form-control pickUpDatePackage" type="text" id="pickUpDatePackage" name="pickUpDatePackage"/>
                                            </div>
                                            <div class="col-sm-12 col-md-5 clear pickUpByPackage">
                                                <label>
                                                    Pick Up Time
                                                </label>
                                                    <input style="background: none;" readonly class="form-control pickUpTimePackage" name="pickUpTimePackage" maxlength="50" id="pickUpTimePackage" autocomplete="off"  type="text" />
                                                    <span class="start_timeErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-12 col-md-5 clear pickUpByPackage">
                                                <label>
                                                    Notes
                                                </label>
                                                    <textarea type="text" name="pickUpNotesPackage" id="pickUpNotesPackage" class="form-control pickUpNotesPackage"></textarea>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-outer">
                                            <input type="button" class="blue-btn" id='doneEmailText' value="Submit">
                                            <input type="button" class="grey-btn CancelBtnPackage" value="Cancel">
                                        </div>
                                    </div>
                                    </div>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End custom field model -->
    <!--Model Save & Submit -->
</div>
<!-- Wrapper Ends -->


<!-- Footer Ends -->
    <script>
        var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
        var upload_url = "<?php echo SITE_URL; ?>";
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    </script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication_top").addClass("active");
    });



</script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/packageTracker/packageTracker.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>