<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->
        <section class="main-content company-announcement-page">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="add_breadcrumb breadcrumb-outer">
                                    Communication &gt;&gt; <span>Add Announcement</span>
                                </div>
                                <div class="edit_breadcrumb breadcrumb-outer" style="display: none;">
                                    Communication &gt;&gt; <span>Edit Announcement</span>
                                </div>
                                <div class="copy_breadcrumb breadcrumb-outer" style="display: none;">
                                    Communication &gt;&gt; <span>Copy Announcement</span>
                                </div>
                                <div class="publish_breadcrumb breadcrumb-outer" style="display: none;">
                                    Communication &gt;&gt; <span>Publish Announcement</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data">
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                                <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                                <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                                <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                                <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                                <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                                <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                                <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                                <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                                <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                                <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                                <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                                <li role="presentation" class="active"><a href="/Announcement/Announcements">Announcement</a></li>
                                <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                                <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>
                            </ul>

                            <div id="add_announcement_div">
                                <form name="add_announcement_form" id="add_announcement_form">
                                    <div class="mg-tp-20 form-outer">
                                        <div class="form-hdr">
                                            <h3>Add New Announcement<a class="back" href="javascript:;" onclick="goBack()"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="add_announcement_div_outer_class" id="add_announcement_div_outer_id">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>Announcement For <em class="red-star">*</em></label>
                                                        <select class="form-control announcement_for" name="announcement_for" id="announcement_for">
                                                            <option value="">Select Type</option>
                                                            <option value="all">All</option>
                                                            <option value="2">Tenant</option>
                                                            <option value="4">Owner</option>
                                                            <option value="3">Vendor</option>
                                                        </select>
                                                        <span class="announcement_forErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Property</label>
                                                        <select class="form-control property" name="property" id="property">
                                                            <option value="">Select</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Building</label>
                                                        <select class="form-control building" name="building" id="building">
                                                            <option value="">Select</option>
                                                        </select>
                                                        <span class="last_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Unit</label>
                                                        <select class="form-control unit" name="unit" id="unit">
                                                            <option value="">Select</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Name</label>
                                                        <select class="form-control add-input user_name" name="user_name[]" id="user_name" multiple></select>
                                                        <span class="user_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <a class="add-icon remove_more_div_icon" href="javascript:;"
                                                           style="display:none"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                        </a>
                                                        <a class="add-icon add_more_div_icon" href="javascript:;">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement Title <em class="red-star">*</em></label>
                                                    <input class="form-control capital title" placeholder="Announcement Title" name="title" id="title" type="text" />
                                                    <span class="titleErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement Start Date <em class="red-star">*</em></label>
                                                    <input class="form-control start_date" readonly placeholder="Name" name="start_date" maxlength="50" id="start_date" type="text" />
                                                    <span class="start_dateErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement Start Time <em class="red-star">*</em></label>
                                                    <input style="background: none;" readonly class="form-control start_time" name="start_time" maxlength="50" id="start_time" autocomplete="off"  type="text" />
                                                    <span class="start_timeErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement End Date <em class="red-star">*</em></label>
                                                    <input class="form-control end_date calander"  readonly placeholder="Name" name="end_date" maxlength="50" id="end_date" type="text" />
                                                    <span class="end_dateErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement End Time <em class="red-star">*</em></label>
                                                    <input style="background: none;" readonly class="form-control end_time" name="end_time" maxlength="50" id="end_time" autocomplete="off" type="text" />
                                                    <span class="end_timeErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Description <em class="red-star">*</em></label>
                                                    <div class="notes_date_right_div">
                                                        <textarea class="form-control description notes_date_right capital" placeholder="Description" name="description" maxlength="50" id="description" ></textarea>
                                                    </div>
                                                    <span class="descriptionErr error red-star"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-outer" id="filelibrary">
                                        <div class="form-hdr">
                                            <h3>File Library</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="form-outer2">
                                                <div class="col-sm-12 mb-15 text-right">
                                                    <button type="button" id="add_libraray_file" class="green-btn add_libraray_file">Add Files...</button>
                                                    <input class="addFileLibrary" id="addFileLibrary" type="file" name="file_library[]" accept=".gif,.jpg,.png,.jpeg,.doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                    <button type="button" class="orange-btn" id="remove_library_file_add">Remove All Files</button>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="row" id="add_file_library_uploads">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="btn-outer text-right">
                                        <button type="submit" id="save_send_announcement" class="blue-btn add_form_submit_btn">Save & Send</button>
                                        <button type="submit" id="save_announcement" class="blue-btn add_form_submit_btn">Save</button>
                                        <button type="button" class="clear-btn email_clear">Clear</button>
                                        <button type="button" id="add_announcement_cancel" class="grey-btn add_announcement_cancel">Cancel</button>
                                    </div>
                                </form>
                            </div>

                            <div id="edit_announcement_div" style="display: none">
                                <form name="edit_announcement_form" id="edit_announcement_form">
                                    <div class="mg-tp-20 form-outer">
                                        <div class="form-hdr">
                                            <h3>Edit Announcement<a class="back" href="javascript:;" onclick="goBack()"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement For <em class="red-star">*</em></label>
                                                    <select class="form-control announcement_for" name="announcement_for" id="announcement_for">
                                                        <option value="">Select Type</option>
                                                        <option value="all">All</option>
                                                        <option value="2">Tenant</option>
                                                        <option value="4">Owner</option>
                                                        <option value="3">Vendor</option>
                                                    </select>
                                                    <span class="announcement_forErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Property</label>
                                                    <select class="form-control property" name="property" id="property">
                                                        <option value="">Select</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Building</label>
                                                    <select class="form-control building" name="building" id="building">
                                                        <option value="">Select</option>
                                                    </select>
                                                    <span class="last_nameErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Unit</label>
                                                    <select class="form-control unit" name="unit" id="unit">
                                                        <option value="">Select</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Name</label>
                                                    <select class="form-control user_name" name="user_name[]" id="user_name_e" multiple></select>
                                                    <span class="user_nameErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement Title <em class="red-star">*</em></label>
                                                    <input class="form-control capital title" placeholder="Announcement Title" name="title" id="title" type="text" />
                                                    <span class="titleErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement Start Date <em class="red-star">*</em></label>
                                                    <input class="form-control start_date" readonly placeholder="Name" name="start_date" maxlength="50" id="start_date_edit" type="text" />
                                                    <span class="start_dateErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement Start Time <em class="red-star">*</em></label>
                                                    <input style="background: none;" readonly class="form-control start_time" name="start_time" maxlength="50" id="start_time" autocomplete="off"  type="text" />
                                                    <span class="start_timeErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement End Date <em class="red-star">*</em></label>
                                                    <input class="form-control end_date calander"  readonly placeholder="Name" name="end_date" maxlength="50" id="end_date_edit" type="text" />
                                                    <span class="end_dateErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement End Time <em class="red-star">*</em></label>
                                                    <input style="background: none;" readonly class="form-control end_time" name="end_time" maxlength="50" id="end_time" autocomplete="off" type="text" />
                                                    <span class="end_timeErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Description <em class="red-star">*</em></label>
                                                    <div class="notes_date_right_div">
                                                        <textarea class="form-control description notes_date_right capital" placeholder="Description" name="description" maxlength="50" id="description" ></textarea>
                                                    </div>
                                                    <span class="descriptionErr error red-star"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-outer" id="filelibrary">
                                        <div class="form-hdr">
                                            <h3>File Library</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="form-outer2">
                                                <div class="col-sm-12 mb-15 text-right">
                                                    <button type="button" id="edit_libraray_file" class="green-btn add_libraray_file">Add Files...</button>
                                                    <input class="fileLibrary" id="editFileLibrary" type="file" name="file_library[]" accept=".gif,.jpg,.png,.jpeg,.doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                    <button type="button" class="orange-btn" id="remove_library_file_edit">Remove All Files</button>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="row file_library_uploads" id="edit_file_library_uploads">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-outer listinggridDiv">
                                                <div class="apx-table apxtable-bdr-none">
                                                    <div class="table-responsive">
                                                        <table id="announcementFileLibrary-table" class="table table-bordered">
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="btn-outer text-right">
                                        <button type="submit" id="update_announcement" class="blue-btn update_announcement">Update</button>
                                        <button type="button" class="clear-btn email_clear">Reset</button>
                                        <button type="button" id="add_announcement_cancel" class="grey-btn add_announcement_cancel">Cancel</button>
                                    </div>
                                </form>
                            </div>

                            <div id="copy_announcement_div" style="display: none">
                                <form name="copy_announcement_form" id="copy_announcement_form">
                                    <div class="mg-tp-20 form-outer">
                                        <div class="form-hdr">
                                            <h3>Copy Announcement<a class="back" href="javascript:;" onclick="goBack()"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement For <em class="red-star">*</em></label>
                                                    <select class="form-control announcement_for" name="announcement_for" id="announcement_for">
                                                        <option value="">Select Type</option>
                                                        <option value="all">All</option>
                                                        <option value="2">Tenant</option>
                                                        <option value="4">Owner</option>
                                                        <option value="3">Vendor</option>
                                                    </select>
                                                    <span class="announcement_forErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Property</label>
                                                    <select class="form-control property" name="property" id="property">
                                                        <option value="">Select</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Building</label>
                                                    <select class="form-control building" name="building" id="building">
                                                        <option value="">Select</option>
                                                    </select>
                                                    <span class="last_nameErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Unit</label>
                                                    <select class="form-control unit" name="unit" id="unit">
                                                        <option value="">Select</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Name</label>
                                                    <select class="form-control user_name" name="user_name[]" id="user_name_copy" multiple></select>
                                                    <span class="user_nameErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement Title <em class="red-star">*</em></label>
                                                    <input class="form-control capital title" placeholder="Announcement Title" name="title" id="title" type="text" />
                                                    <span class="titleErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement Start Date <em class="red-star">*</em></label>
                                                    <input class="form-control start_date" readonly placeholder="Name" name="start_date" maxlength="50" id="start_date_copy" type="text" />
                                                    <span class="start_dateErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement Start Time <em class="red-star">*</em></label>
                                                    <input style="background: none;" readonly class="form-control start_time" name="start_time" maxlength="50" id="start_time" autocomplete="off"  type="text" />
                                                    <span class="start_timeErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement End Date <em class="red-star">*</em></label>
                                                    <input class="form-control end_date calander"  readonly placeholder="Name" name="end_date" maxlength="50" id="end_date_copy" type="text" />
                                                    <span class="end_dateErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement End Time <em class="red-star">*</em></label>
                                                    <input style="background: none;" readonly class="form-control end_time" name="end_time" maxlength="50" id="end_time" autocomplete="off" type="text" />
                                                    <span class="end_timeErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Description <em class="red-star">*</em></label>
                                                    <div class="notes_date_right_div">
                                                        <textarea class="form-control description notes_date_right capital" placeholder="Description" name="description" maxlength="50" id="description" ></textarea>
                                                    </div>
                                                    <span class="descriptionErr error red-star"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-outer" id="filelibrary">
                                        <div class="form-hdr">
                                            <h3>File Library</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="form-outer2">
                                                <div class="col-sm-12 mb-15 text-right">
                                                    <button type="button" id="copy_libraray_file" class="green-btn add_libraray_file">Add Files...</button>
                                                    <input class="fileLibrary" id="copyFileLibrary" type="file" name="file_library[]" accept=".gif,.jpg,.png,.jpeg,.doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                    <button type="button" class="orange-btn" id="remove_library_file_copy">Remove All Files</button>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="row file_library_uploads" id="copy_file_library_uploads">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-outer listinggridDiv">
                                                <div class="apx-table apxtable-bdr-none">
                                                    <div class="table-responsive">
                                                        <table id="announcementFileLibraryCopy-table" class="table table-bordered">
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="btn-outer text-right">
                                        <button type="submit" id="copy_announcement" class="blue-btn copy_announcement">Save</button>
                                        <button type="button" class="clear-btn copy_clear-btn">Clear</button>
                                        <button type="button" id="add_announcement_cancel" class="grey-btn add_announcement_cancel">Cancel</button>
                                    </div>
                                </form>
                            </div>

                            <div id="publish_announcement_div" style="display: none">
                                <form name="publish_announcement_form" id="publish_announcement_form">
                                    <div class="mg-tp-20 form-outer">
                                        <div class="form-hdr">
                                            <h3>Publish Announcement<a class="back" href="javascript:;" onclick="goBack()"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement For <em class="red-star">*</em></label>
                                                    <select class="form-control announcement_for" name="announcement_for" id="announcement_for">
                                                        <option value="">Select Type</option>
                                                        <option value="all">All</option>
                                                        <option value="2">Tenant</option>
                                                        <option value="4">Owner</option>
                                                        <option value="3">Vendor</option>
                                                    </select>
                                                    <span class="announcement_forErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Property</label>
                                                    <select class="form-control property" name="property" id="property">
                                                        <option value="">Select</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Building</label>
                                                    <select class="form-control building" name="building" id="building">
                                                        <option value="">Select</option>
                                                    </select>
                                                    <span class="last_nameErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Unit</label>
                                                    <select class="form-control unit" name="unit" id="unit">
                                                        <option value="">Select</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Name</label>
                                                    <select class="form-control user_name" name="user_name[]" id="user_name_publish" multiple></select>
                                                    <span class="user_nameErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement Title <em class="red-star">*</em></label>
                                                    <input class="form-control capital title" placeholder="Announcement Title" name="title" id="title" type="text" />
                                                    <span class="titleErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement Start Date <em class="red-star">*</em></label>
                                                    <input class="form-control start_date" readonly placeholder="Name" name="start_date" maxlength="50" id="start_date_publish" type="text" />
                                                    <span class="start_dateErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement Start Time <em class="red-star">*</em></label>
                                                    <input style="background: none;" readonly class="form-control start_time" name="start_time" maxlength="50" id="start_time" autocomplete="off"  type="text" />
                                                    <span class="start_timeErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Announcement End Date <em class="red-star">*</em></label>
                                                    <input class="form-control end_date calander"  readonly placeholder="Name" name="end_date" maxlength="50" id="end_date_publish" type="text" />
                                                    <span class="end_dateErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Announcement End Time <em class="red-star">*</em></label>
                                                    <input style="background: none;" readonly class="form-control end_time" name="end_time" maxlength="50" id="end_time" autocomplete="off" type="text" />
                                                    <span class="end_timeErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label>Description <em class="red-star">*</em></label>
                                                    <div class="notes_date_right_div">
                                                        <textarea class="form-control description notes_date_right capital" placeholder="Description" name="description" maxlength="50" id="description" ></textarea>
                                                    </div>
                                                    <span class="descriptionErr error red-star"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-outer" id="filelibrary">
                                        <div class="form-hdr">
                                            <h3>File Library</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="form-outer2">
                                                <div class="col-sm-12 mb-15 text-right">
                                                    <button type="button" id="publish_libraray_file" class="green-btn add_libraray_file">Add Files...</button>
                                                    <input class="fileLibrary" id="publishFileLibrary" type="file" name="file_library[]" accept=".gif,.jpg,.png,.jpeg,.doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                    <button type="button" class="orange-btn" id="remove_library_file_publish">Remove All Files</button>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="row file_library_uploads" id="publish_file_library_uploads">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid-outer listinggridDiv">
                                                <div class="apx-table apxtable-bdr-none">
                                                    <div class="table-responsive">
                                                        <table id="announcementFileLibraryPublish-table" class="table table-bordered">
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="btn-outer text-right">
                                        <button type="submit" id="save_send_announcement_publish" class="blue-btn publish_form_submit_btn">Save & Send</button>
                                        <button type="submit" id="save_announcement_publish" class="blue-btn publish_form_submit_btn">Save</button>
                                        <button type="button" class="clear-btn publish_clear">Clear</button>
                                        <button type="button" id="add_announcement_cancel" class="grey-btn add_announcement_cancel">Cancel</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="overlay">
        <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
            <img width="200"  height="200" src='<?php echo COMPANY_SUBDOMAIN_URL ?>/images/loading.gif'/>
        </div>
    </div>
    <!-- Wrapper Ends -->

    <script>
        var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
        var upload_url  = "<?php echo SITE_URL; ?>";
    </script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/announcement/fileLibrary.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/announcement/addAnnouncement.js"></script>
    <script>
        $(document).on("click",".clear-btn.email_clear",function () {
            resetFormClear('#add_announcement_form',['start_date','start_time','end_date','end_time'],'form',false);
        });
    </script>
    <script>
        $(document).on("click",".copy_clear-btn",function () {
            window.location.reload();
        });$(document).on("click",".publish_clear",function () {
            window.location.reload();
        });
    </script>
    <script>
        function goBack() {
            window.history.back();
        }

        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
            $("#communication_top").addClass("active");
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });



    </script>
    <script>
        $(document).on("click",".email_clear",function () {
            window.location.reload();
        });
    </script>
    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>