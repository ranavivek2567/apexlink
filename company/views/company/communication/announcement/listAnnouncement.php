<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Communication &gt;&gt; <span>List Announcement</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data">
                        <!--- Right Quick Links ---->
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                        <!--- Right Quick Links ---->
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" ><a href="/Communication/InboxMails">Email</a></li>
                                <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                                <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                                <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                                <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                                <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                                <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                                <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                                <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                                <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                                <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                                <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                                <li role="presentation" class="active"><a href="/Announcement/Announcements">Announcement</a></li>
                                <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                                <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>
                            </ul>

                            <div class="mg-tp-20 property-status">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label>Status</label>
                                        <select class="fm-txt form-control jqGridStatusClass" id="jqGridStatus"  data-module="ANNOUNCEMENTLISTING">
                                            <option selected value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                            <option value="2">Draft</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="btn-outer text-right">
                                            <a href="/Announcement/AddAnnouncement"><input type="button" value="Add New Announcement" class="blue-btn"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Announcement</h3>
                                </div>
                                <div class="form-data">
                                    <div class="grid-outer">
                                        <div class="table-responsive">
                                            <div class="apx-table ">
                                                <div class="table-responsive">
                                                    <table id="announcements-table" class="table table-bordered"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Wrapper Ends -->


    <!-- Footer Ends -->
    <script>
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    </script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/announcement/listAnnouncement.js"></script>
    <script>
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
            $("#communication_top").addClass("active");
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });



    </script>
    <!-- Jquery Starts -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>