<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>



    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Communication >> <span>Task and Reminders</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text" />
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">

                    </div>
                    <div class="col-sm-12">
                        <div class="content-section">
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                                    <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                                    <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                                    <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                                    <li role="presentation"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                                    <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                                    <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                                    <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                                    <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                                    <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                                    <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                                    <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                                    <li role="presentation" class="active"><a href="/Announcement/Announcements">Announcement</a></li>
                                    <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                                    <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="communication-one">
                                        <!-- Sub Tabs Starts-->
                                        <form name="add_task_reminder_form" id="add_task_reminder_form">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Task and Reminders</h3>

                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <input type="hidden" name="unit_ids"  id="unit_ids">

                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Title <em class="red-star">*</em></label>
                                                        <input class="form-control capsOn title" type="text" name="title" maxlength="50" id="title" value="New Task/Reminder"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                                        <label>Property <em class="red-star">*</em></label>
                                                        <select class="form-control property selectpicker"  data-live-search="true" name="property" id="property">
                                                            <option value="">Select</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Building <em class="red-star">*</em></label>
                                                        <select class="form-control building" name="building" id="building">
                                                            <option value="">Select</option>
                                                        </select>
                                                        <span class="last_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Unit <em class="red-star">*</em></label>
                                                        <div class="select-div">
                                                            <label><input id="unit" name="unit" class="unit" value="Select" type="text"/> </label>
                                                            <span><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-4 clear textarea-form">
                                                        <label>Details <em class="red-star">*</em></label>
                                                        <textarea class="form-control capital" name="details" maxlength="300" id="details"></textarea>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2 clear">
                                                        <label>Status</label>
                                                        <select class="form-control" id="status" name="status" class="status">
                                                            <option value="1">Not Assigned</option>
                                                            <option value="2">Not Started</option>
                                                            <option value="3">In Progress</option>
                                                            <option value="4">Completed</option>
                                                            <option value="5">Canceled and Resigned</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Assigned To</label>
                                                        <select class="form-control assigned_to" name="assigned_to" id="assigned_to"><option>Select</option></select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <label>Due Date</label>
                                                        <input class="form-control calander" id="due_date" name="due_date" type="text"/>
                                                    </div>

                                                </div>

                                                <div class="row form-outer2">
                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                        <div class="check-outer" >
                                                            <input type="checkbox" id="reccuring_task_checkbox" name="reccuring_task_checkbox"/>
                                                            <label>Reccuring Task</label>
                                                        </div>
                                                    </div>
                                                    <div id="recurring_div" style="display:none">
                                                        <div class="col-xs-12 col-sm-4 col-md-2" >
                                                            <label>Frequency</label>
                                                            <select class="form-control frequency" name="frequency" id="frequency">
                                                                <option value="1">Daily</option>
                                                                <option value="2">Weekly</option>
                                                                <option value="3">Monthly</option>
                                                                <option value="4">Annually</option>
                                                                <option value="5">Semi-Annually</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-2">
                                                            <label>Next Date</label>
                                                            <input class="form-control"  id="next_date" name="next_date" readonly type="text"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <button type="submit" id="task_reminder_save" class="blue-btn task_reminder_save">Save</button>
                                                    <button type="button" class="clear-btn email_clear">Clear</button>
                                                    <button type="button" class="grey-btn task_reminder_cancel">Cancel</button>
                                                </div>

                                            </div>
                                        </div>
                                        </form>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <!-- Wrapper Ends -->

    <!-- Unit selection starts here -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog spotlight-pop">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Unit</h4>
                </div>
                <div class="modal-body">
                    <form id="add_contact_popup" enctype="multipart/form-data">
                        <div class="panel-body pad-none">
                            <div class="row">
                                <div class="property-status">
                                    <div class="col-sm-12">
                                        <div class="check-outer unit_select_all_checkbox_html">
                                            <input type="checkbox" class="select_all_checkbox"/>
                                            <label>Select All</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12 unit_checkbox_html" >
                                        <!--                                        <div class="check-outer">-->
                                        <!--                                            <input type="checkbox"/>-->
                                        <!--                                            <label>1</label>-->
                                        <!--                                        </div>-->
                                        <!--                                        <div class="check-outer">-->
                                        <!--                                            <input type="checkbox"/>-->
                                        <!--                                            <label>1</label>-->
                                        <!--                                        </div>-->
                                    </div>
                                    <div class="btn-outer unit_select_all_checkbox_html">
                                        <button type="button" class="blue-btn modal_box_unit_save">Save</button>
                                        <button type="button" class="grey-btn cancel_unit">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form></div>
            </div>

        </div>

    </div>

    <!-- Unit  selection ends here -->




    <!-- Footer Ends -->
    <script>
        $(document).on("click",".clear-btn.email_clear",function () {
            resetFormClear('#add_task_reminder_form',['title','due_date'],'form',false);
        });
        $(function () {
            $('.selectpicker').selectpicker();
        });
    </script>
    <script>
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
            $("#communication_top").addClass("active");
        });



    </script>
    <script>
        var pagination  = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
        var upload_url  = "<?php echo SITE_URL; ?>";
    </script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/announcement/fileLibrary.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/taskReminders/taskReminders.js"></script>


    <!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>