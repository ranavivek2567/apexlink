<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
$user_type = explode("/",$_SERVER['REQUEST_URI']);
if(isset($user_type[1]) && $user_type[1] == 'Owner')
{
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Owner_Portal' ;
    $button_url = '/Owner';

}elseif(isset($user_type[1]) && $user_type[1] == 'Communication'){
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Communication_Portal' ;
    $button_url = '';

}elseif(isset($user_type[1]) && $user_type[1] == 'Vendor'){
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Vendor_Portal' ;
    $button_url = '/Vendor';

}
elseif(isset($user_type[1]) && $user_type[1] == 'Tenant'){
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
    echo '<div id="wrapper">';
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    $_SESSION[SESSION_DOMAIN]['current_user_type'] = 'Tenant_Portal' ;
    $button_url = '/Tenant';

}else{
}

?>

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Email </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                        <div class="content-section">
                                                            <div class="main-tabs">
                                                                <div role="tabpanel" class="tab-pane active" id="communication-one">
                                                                    <!-- Sub Tabs Starts-->
                                                                    <div class="property-status">
                                                                        <div class="row">
                                                                            <div class="col-sm-2">
                                                                            </div>
                                                                            <div class="col-sm-10">
                                                                                <div class="btn-outer text-right">
                                                                                    <a href="<?php echo $button_url; ?>/Communication/SentEmails" class="blue-btn margin-right">Sent Mails</a>
                                                                                    <a href="<?php echo $button_url; ?>/Communication/DraftedMails" class="blue-btn margin-right">Draft Mails</a>
                                                                                    <a data-urltype="<?php echo $button_url; ?>" href="javascript:void(0)" class="blue-btn margin-right compose-mail">Compose Mail</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="accordion-form">
                                                                        <div class="accordion-outer">
                                                                            <div class="bs-example">
                                                                                <div class="panel-group" id="accordion">
                                                                                    <div class="panel panel-default">
                                                                                        <div class="panel-heading">
                                                                                            <h4 class="panel-title">
                                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Inbox</a>
                                                                                            </h4>
                                                                                        </div>
                                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                            <div class="panel-body">
                                                                                                <div class="inbox-outer">
                                                                                                    <ul id="communication_inbox">

                                                                                                    </ul>

                                                                                                </div>
                                                                                                <div class="detailed_inbox">

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                </div>
                                                                            </div>
                                                                            <!-- Accordian Ends -->
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/file_library.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/common.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/inbox-mails.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
