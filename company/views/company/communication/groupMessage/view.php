<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Group Message/Email </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation" class="active"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">
                                <div class="col-sm-12">
                                    <div class="btn-outer text-right">
                                        <a href="/Communication/DraftedGroupMessages"><input type="button" class="blue-btn" value="Drafts"></a>
                                        <a href="/Communication/AddGroupMessage"><input type="button" class="blue-btn" value="Compose New Mass Message"></a>
                                    </div>
                                </div>
                                <h4 class="panel-title">

                                </h4>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="btn-outer text-right">
                                                        <a href="/Communication/GroupMessage"><input type="button" class="blue-btn" value="Sent Group Emails"></a>
                                                        <a href="/Communication/GroupMessage"><input type="button" class="blue-btn" value="Sent Group Text Messages"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> List of Tenants Receivable</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                <div class="panel-body pad-none">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <table class="table table-hover table-dark">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th scope="col">Tenant Name</th>
                                                                                    <th scope="col">Phone</th>
                                                                                    <th scope="col">Email</th>
                                                                                    <th scope="col">Property Name</th>
                                                                                    <th scope="col">Unit Number</th>
                                                                                    <th scope="col">Rent($)</th>
                                                                                    <th scope="col">Balance ($)</th>
                                                                                    <th scope="col">Days Remaining</th>
                                                                                    <th scope="col">Status</th>
                                                                                    <th scope="col">Actions</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                    <td>555-444-6666</td>
                                                                                    <td>ashleym524@gmail.com</td>
                                                                                    <td>Bill Farms</td>
                                                                                    <td>Bill B20</td>
                                                                                    <td>600.00</td>
                                                                                    <td>(575.00)</td>
                                                                                    <td>317</td>
                                                                                    <td>Active</td>
                                                                                    <td><select class="form-control"><option>Select</option></select></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                    <td>555-444-6666</td>
                                                                                    <td>ashleym524@gmail.com</td>
                                                                                    <td>Bill Farms</td>
                                                                                    <td>Bill B20</td>
                                                                                    <td>600.00</td>
                                                                                    <td>(575.00)</td>
                                                                                    <td>317</td>
                                                                                    <td>Active</td>
                                                                                    <td><select class="form-control"><option>Select</option></select></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                    <td>555-444-6666</td>
                                                                                    <td>ashleym524@gmail.com</td>
                                                                                    <td>Bill Farms</td>
                                                                                    <td>Bill B20</td>
                                                                                    <td>600.00</td>
                                                                                    <td>(575.00)</td>
                                                                                    <td>317</td>
                                                                                    <td>Active</td>
                                                                                    <td><select class="form-control"><option>Select</option></select></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                    <td>555-444-6666</td>
                                                                                    <td>ashleym524@gmail.com</td>
                                                                                    <td>Bill Farms</td>
                                                                                    <td>Bill B20</td>
                                                                                    <td>600.00</td>
                                                                                    <td>(575.00)</td>
                                                                                    <td>317</td>
                                                                                    <td>Active</td>
                                                                                    <td><select class="form-control"><option>Select</option></select></td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<!-- Wrapper Ends -->


<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication_top").addClass("active");
    });


</script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
