<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Communication &gt;&gt; <span>Group Message/Email </span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/communication/sidebar_dropdown.php"); ?>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Communication/InboxMails">Email</a></li>
                            <li role="presentation"><a href="/Communication/TextMessage">Text Message</a></li>
                            <li role="presentation"><a href="/MasterData/Documents" >Letters and Notices</a></li>
                            <li role="presentation"><a href="/Communication/Conversation">Conversation</a></li>
                            <li role="presentation" class="active"><a href="/Communication/GroupMessage">Group Message/Email</a></li>
                            <li role="presentation"><a href="/Communication/NewTaskAndReminders">Tasks and Reminders</a></li>
                            <li role="presentation"><a href="/Communication/InTouch">In-Touch</a></li>
                            <li role="presentation"><a href="/Communication/PhoneCall">Phone Call Log</a></li>
                            <li role="presentation"><a href="/Communication/TimeSheet">Time Sheet</a></li>
                            <li role="presentation"><a href="/Communication/Chat">Staff Chat Room</a></li>
                            <li role="presentation"><a href="/Communication/DailyVisitor">Daily Visitor Log</a></li>
                            <li role="presentation"><a href="/Package/Packages">Package Tracker</a></li>
                            <li role="presentation"><a href="/Announcement/Announcements">Announcement</a></li>
                            <li role="presentation"><a href="/Communication/WaitingList">Waiting List</a></li>
                            <li role="presentation"><a href="/MasterData/EsignatureUser">e-Sign History</a></li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content">
                            <div class="panel-heading">
                                <div class="col-sm-12">
                                    <div class="btn-outer text-right" style="float: left; width: 100%;margin-bottom: 20px;">
                                        <a href="/Communication/DraftedGroupMessages"><input type="button" class="blue-btn" value="Drafts"></a>
                                        <a class="blue-btn"  id="composer_group" href="javascript:void(0)">Compose New Mass Message</a>
                                    </div>
                                </div>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="receivables">
                                        <div class="property-status">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="btn-outer text-right">
                                                        <a class="sent_mail blue-btn" href="javascript:void(0)">Sent Group Emails</a>
                                                        <a class="sent_message blue-btn" href="javascript:void(0)">Sent Group Text Messages</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-outer" >
                                            <div class="form-hdr">
                                                <h3>
                                                    Sent Group Messages/Emails
                                                </h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="col-sm-12">
                                                    <div class="grid-outer">
                                                        <div class="table-responsive">
                                                            <div class="apx-table ">
                                                                <div class=" table-responsive email_tab">
                                                                    <table id="communication-sent-table-email" class="table table-bordered"></table>

                                                                </div>
                                                                <div class=" table-responsive text_tab">
                                                                    <table id="communication-sent-table-text" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Regular Rent Ends -->

                                </div>
                            </div>
                            <!-- Sub tabs ends-->
                        </div>



                        <!-- Sub Tabs Starts-->

                        <!-- Sub tabs ends-->
                    </div>

                </div>
            </div>

            <!--Tabs Ends -->

        </div>
</div>
</div>
</section>
</div>
<div class="modal fade" id="sentMailModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <a class="close closeAnnouncementmodal closeSystemAnnouncement" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i></a>

                <a  href="javascript:void(0)"><h4>Sent Mails </h4></a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <strong>From</strong>
                        <span class="modal_from"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>To</strong>
                        <span class="modal_to"></span>
                    </div>
<!--                    <div class="col-sm-12">-->
<!--                        <strong>Cc</strong>-->
<!--                        <span class="modal_cc"></span>-->
<!--                    </div>-->
<!--                    <div class="col-sm-12">-->
<!--                        <strong>Bcc</strong>-->
<!--                        <span class="modal_bcc"></span>-->
<!--                    </div>-->
                    <div class="col-sm-12">
                        <strong>Subject</strong>
                        <span class="modal_subject"></span>
                    </div>
<!--                    <div class="col-sm-12">-->
<!--                        <strong>Date</strong>-->
<!--                        <span class="modal_date"></span>-->
<!--                    </div>-->
<!--                    <div class="col-sm-12"><span class="modal_message"></span></div>-->
                    <input type="hidden" id="compose_mail_id">
                    <div class="col-sm-12 text-right">
                        <a class="blue-btn forward-email-btn">Forward</a>
                        <a class="grey-btn" id="cancel_communication" href="javascript:void(0)">
                            Cancel
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
</script>

<!-- Wrapper Ends -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/email/common.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/communication/groupMessages/sent-group-message.js"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>

<style>
    span.modal_to {
        word-break: break-all;
    }
</style>
</body>

</html>
