<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/constants.php");

/**

 * Created by PhpStorm.

 * User: ranavivek2567

 * Date: 1/18/2019

 * Time: 11:19 AM

 */



if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {

    $url = DOMAIN_URL;

    header('Location: ' . $url);

}



$view_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';

?>

<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");

?>

<style>
    .apx-table {
        height: max-content;
    }
</style>


<div id="wrapper">

    <main class="apxpg-main">

        <!-- Top navigation start -->

        <?php

        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");

        ?>

        <section class="main-content">

            <div class="container-fluid">

                <div class="bread-search-outer apxpg-top-search">

                    <div class="row">

                        <div class="col-sm-8">

                            <div class="breadcrumb-outer">

                                Property Module >> <span>Property View</span>

                            </div>

                        </div>

                        <div class="col-sm-4">

                            <div class="easy-search">

                                <input placeholder="Easy Search" type="text">

                            </div>

                        </div>

                    </div>

                </div>
                <div class="col-sm-12 list_property_name"><a href="<?php echo SUBDOMAIN_URL; ?> >/Property/PropertyModules" id="property_link_name"></a></div>

            </div>

            <div><b><u>   </u></b><a id="property_link" ><span id="building_property" class="font-weight-bold"></span></a><span id="building_span_name"></span></div>

            <div class="col-sm-12">

                <div class="main-tabs">

                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation"  class="active pointer"><a class="mainPropertytab" href="javascript:;" data="property" role="tab">Property</a></li>

                        <li role="presentation" class="pointer"><a class="mainPropertytab" href="javascript:;" data="building" role="tab" >Building</a></li>

                        <li role="presentation" class="pointer"><a class="mainPropertytab" href="javascript:;" data="unit" role="tab" >Unit</a></li>

                    </ul>
                    <!--Right Navigation Link Starts-->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>Properties</h2>

                            <div class="list-group panel">

                                <!-- Two Ends-->

                                <a href="/Property/AddProperty" class="list-group-item list-group-item-success strong collapsed" >New Property</a>

                                <!-- Two Ends-->


                                <!-- Three Starts-->

                                <a  href="/MasterData/AddPropertyGroup" class="list-group-item list-group-item-success strong collapsed" >Property Group</a>

                                <!-- Three Ends-->



                                <!-- Four Starts-->

                                <a href="/MasterData/AddPortfolio" class="list-group-item list-group-item-success strong collapsed">New Portfolio</a>

                                <!-- Four Ends-->



                                <!-- Five Starts-->

                                <a href="javascript:void(0)"  class="list-group-item list-group-item-success strong collapsed">Process Management Fees</a>

                                <!-- Five Ends-->



                                <!-- Six Starts-->
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Book Now</a>
                                <!-- Six Ends-->

                                <!-- Six Starts-->

                                <a href="/Building/BuildingModule?id=<?= $_GET['id']?>" class="list-group-item list-group-item-success strong collapsed">Manage Buildings</a>

                                <!-- Six Ends-->

                                <!-- Six Starts-->

                                <a href="/Building/AddBuilding?id=<?= $_GET['id']?>" class="list-group-item list-group-item-success strong collapsed" >New Building</a>

                                <!-- Six Ends-->
                            </div>
                        </div>
                    </div>
                    <!--Right Navigation Link Ends-->


                    <div class="content-data apxpg-allcontent">

                        <div class="content-section">

                            <!--Property-->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Property</strong>

                                    <a onclick="goBack()" class="back right" href="/Property/PropertyModules"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>

                                </div>

                                <div class="apx-adformbox-content">

                                    <div class="row">

                                        <div class="col-sm-3">

                                            <span><strong class="left blue-label slider-hdr" id="detail_property_name"></strong></span><br>

                                            <span class="left ft-13">Property ID -<span id="detail_property_id"></span></span><br>

                                            <span class="left ft-13" id="detail_address1"></span><br>

                                            <span class="left ft-13" id="detail_address2"></span><br>

                                            <span class="left ft-13" id="detail_address3"></span><br>

                                            <span class="left ft-13" id="detail_address4"></span><br>

                                        </div>

                                        <div class="col-sm-6">

                                            <div class="owl-carousel owl-theme">

                                            </div>

                                        </div>

                                        <div class="col-sm-3">



                                        </div>

                                    </div>

                                </div>

                            </div>



                            <!--Property Information-->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Property Information</strong>


                                </div>

                                <div class="apx-adformbox-content">

                                    <div class="row">

                                        <div class="view-outer">

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Portfolio ID :</label>

                                                <span id="portfolio_id"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Portfolio Name :</label>

                                                <span id="portfolio_name"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Legal Name :</label>

                                                <span id="legal_name"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Year Built :</label>

                                                <span id="property_year"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Phone No :</label>

                                                <span id="prop_phone_number"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Property Tax ID :</label>

                                                <span id="property_tax_ids"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Fax No :</label>

                                                <span id="fax"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Property Manager :</label>

                                                <span id="manager_list"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Short-Term Rental :</label>

                                                <span id="is_short_term_rental"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Property Style :</label>

                                                <span id="property_style"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Property Price :</label>

                                                <span class="pull-left" id="property_symbol"><?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?></span><span class="pull-left" id="property_price"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Property Group :</label>

                                                <span id="attach_groups"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Property Type :</label>

                                                <span id="property_type"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Number Of Units :</label>

                                                <span id="no_of_units"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Property Sub-Type :</label>

                                                <span id="property_subtype"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Smoking Allowed :</label>

                                                <span id="smoking_allowed"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">No. Of Buildings :</label>

                                                <span id="no_of_buildings"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Pet Friendly :</label>

                                                <span id="pet_friendly"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Sq mtr :</label>

                                                <span id="property_squareFootage"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Key/AccessCode Info:(keyCode:Key) :</label>

                                                <span id="key_access_codes_info"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Amenities :</label>

                                                <span id="amenities"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Property parcel# :</label>

                                                <span id="property_parcel_number"></span>

                                            </div>



                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Description :</label>

                                                <span id="description"></span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Garage Available :</label>

                                                <span id="garage_available"></span>

                                            </div>
                                            
                                            
                                            <div class="col-xs-12 col-sm-6 garage_yes" style="display:none">

                                                <label class="blue-label">No. of Vehicles this Property Accommodates:</label>

                                                <span id="no_of_vehicles"></span>

                                            </div>                                            
                                            <div class="col-xs-12 col-sm-6 garage_other" style="display:none">

                                                <label class="blue-label">Notes:</label>

                                                <span id="garage_note"></span>

                                            </div>
                                            
                                            
                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Online Listing :</label>

                                                <span id="online_listing"></span>

                                            </div>
                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Online Application :</label>

                                                <span id="online_application"></span>

                                            </div>
                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Disable Syndication :</label>

                                                <span id="disable_sync"></span>

                                            </div>

                                        </div>

                                    </div>

                                    <hr>

                                    <!-- Renovation Detail-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="renovationDetail-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <hr>

                                    <!-- School District Detail-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="schoolDistrict-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="edit-foot pb-15">

                                        <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseOne" redirection_data="info">

                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                            Edit

                                        </a>

                                    </div>

                                </div>

                            </div>



                            <!--Owner /landlord -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Link Owners/Landlords</strong>

                                </div>

                                <div class="apx-adformbox-content">



                                    <!-- Link Owners/Landlords table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="linkOwner-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>



                                    <hr>

                                    <div class="row">

                                        <div class="view-outer">

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Payment Type :</label>

                                                <span id="payment_type">N/A</span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Vendor 1099 Payer :</label>

                                                <span id="vendor_1099_payer">N/A</span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Fiscal Year End :</label>

                                                <span id="fiscal_year_end">N/A</span>

                                            </div>

                                        </div>

                                    </div>

                                    

                                     <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseTwo" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Keys -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Keys</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Keys table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="keys-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    

                                      <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseFive" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Property Bank Account -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Bank Accounts</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Property Bank Account table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertyBankAccount-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                   

                                        <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseThree" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Late Fee Management -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Late Fee Management</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <h5><strong>One Time Fee</strong></h5>

                                    <hr>

                                    <div class="row">

                                        <div class="view-outer">

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Flat Fee <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> :</label>

                                                <span id="apply_one_time_flat_fee">N/A</span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">% of Monthly Rent :</label>

                                                <span id="apply_one_time_percentage">N/A</span>

                                            </div>

                                        </div>

                                    </div>

                                </div>



                                <div class="apx-adformbox-content">

                                    <h5><strong>Daily Fee</strong></h5>

                                    <hr>

                                    <div class="row">

                                        <div class="view-outer">

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Flat Fee <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> :</label>

                                                <span id="apply_daily_flat_fee">N/A</span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">% of Monthly Rent :</label>

                                                <span id="apply_daily_percentage">N/A</span>

                                            </div>

                                        </div>

                                    </div>

                                   

                                        <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseFour" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Management Fees-->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Management Fees</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <div class="row">

                                        <div class="view-outer">

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Type :</label>

                                                <span id="management_type">N/A</span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6">

                                                <label class="blue-label">Value(%):</label>

                                                <span id="management_value">N/A</span>

                                            </div>

                                            <div class="col-xs-12 col-sm-6 minimum_management_fee" style="display: none;">

                                                <label class="blue-label">Value <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> :</label>

                                                <span id="minimum_management_fee">N/A</span>

                                            </div>

                                        </div>

                                    </div>

                                   

                                        <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseSix" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Management Info -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Management Info</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <div class="row">

                                        <div class="view-outer">

                                            <div class="col-sm-12">

                                                <div class="col-sm-6">

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Management Company Name :</label>

                                                        <span id="management_management_company_name">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Management Start Date :</label>

                                                        <span id="management_management_start_date">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Management End Date :</label>

                                                        <span id="management_management_end_date">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Reason the Management Ended :</label>

                                                        <span id="management_reason">N/A</span>

                                                    </div>

                                                </div>

                                                <div class="col-sm-6">

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Description :</label>

                                                        <span id="management_description">N/A</span>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                       <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseSeven" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Maintenance Information -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Maintenance Information</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <div class="row">

                                        <div class="view-outer">

                                            <div class="col-sm-12">

                                                <div class="col-sm-6">

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Maintenance Spending Limit :</label>

                                                        <span id="maintenance_maintenance_speed_time">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Amount :</label>

                                                        <span id="maintenance_amount">0.00</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Insurance Expiration Date :</label>

                                                        <span id="maintenance_Insurance_expiration">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Home Warranty Expiration :</label>

                                                        <span id="maintenance_warranty_expiration">N/A</span>

                                                    </div>

                                                </div>

                                                <div class="col-sm-6">

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Special Instructions :</label>

                                                        <span id="maintenance_special_instructions">N/A</span>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                        <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseEight" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Mortgage Information -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Mortgage Information</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <div class="row">

                                        <div class="view-outer">

                                            <div class="col-sm-12">

                                                <div class="col-sm-6">

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Mortgage Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?>:</label>

                                                        <span id="mortgage_mortgage_amount">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Property Assessed Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?>:</label>

                                                        <span id="mortgage_property_assessed_amount">0.00</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Mortgage Start Date :</label>

                                                        <span id="mortgage_mortgage_start_date">N/A</span>

                                                    </div>

                                                </div>

                                                <div class="col-sm-6">

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Term :</label>

                                                        <span id="mortgage_term">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Interest Rate :</label>

                                                        <span id="mortgage_interest_rate">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Financial Institution :</label>

                                                        <span id="mortgage_financial_institution">N/A</span>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                        <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseNine" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Property Loan -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Property Loan</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <div class="row">

                                        <div class="view-outer">

                                            <div class="col-sm-12">

                                                <div class="col-sm-6">

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Loan :</label>

                                                        <span id="loan_loan_status">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Vendor Name(Bank or Lender):</label>

                                                        <span id="loan_vendor_name">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Interest Only :</label>

                                                        <span id="loan_interest_only">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Loan Amount :</label>

                                                        <span id="loan_loan_amount">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Loan Number :</label>

                                                        <span id="loan_loan_number">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Variable Rate :</label>

                                                        <span id="loan_loan_rate">N/A</span>

                                                    </div>

                                                </div>

                                                <div class="col-sm-6">

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Loan Start Date :</label>

                                                        <span id="loan_loan_start_date">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Loan Maturity Date :</label>

                                                        <span id="loan_loan_maturity_date">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Monthly Due Date :</label>

                                                        <span id="loan_monthly_due_date">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Payment Amount :</label>

                                                        <span id="loan_payment_amount">N/A</span>

                                                    </div>

                                                    <div class="col-sm-12">

                                                        <label class="blue-label">Loan Terms :</label>

                                                        <span id="loan_loan_terms">N/A</span>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                     <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseTen" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Property Insurance -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Property Insurance</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Property Insurance table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertyInsurance-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                       <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseEleven" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Warranty Information -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Warranty Information</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Warranty Information table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="warrantyInformation-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                       <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseTwelve" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Manage Charges -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Manage Charges</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Charges Property-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertyCharges-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <hr>

                                    <!-- Charges Units-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertyUnits-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                        <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseThirteen" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Owner Preferred Vendors -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Owner Preferred Vendors</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Owner Preferred Vendors Table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertyOwnerVendor-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                       <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseFourteen" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Blacklisted Vendors -->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Blacklisted Vendors</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Owner Preferred Vendors Table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertyBlacklistVendors-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                        <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseFifteen" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Notes-->

                            <div class="apx-adformbox" id="notesHistory">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Notes</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Notes table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="notes-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                      <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseEighteen" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Photos/Virtual Tour Video-->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Photos/Virtual Tour Video</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Photos/Virtual Tour Video table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertPhotovideos-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                       <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseSixteen" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--File Library-->

                            <div id="propertyFileLibrary" class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">File Library</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- File Library table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertFileLibrary-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                         <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseSeventeen" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Property Flag Bank-->

                            <div id="propertyFlag" class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Flag Bank</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Property Wating List table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertyFlagBank-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                        <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseNinteen" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Property Wating List-->

                            <div id="propertyWatingList" class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Waiting List</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Property Wating List table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertWatingList-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                      <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseTwenty" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Property Complaints-->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Property Complaints</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Property Complaints table-->

                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="apx-table">

                                                <div class="table-responsive">

                                                    <table id="propertComplaints-table" class="table table-bordered">

                                                    </table>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                   <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseTwentyTwo" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>



                            <!--Custom Fields-->

                            <div class="apx-adformbox">

                                <div class="apx-adformbox-title">

                                    <strong class="left">Custom Fields</strong>

                                </div>

                                <div class="apx-adformbox-content">

                                    <!-- Custom Fields-->
                                    <input type="hidden" id="customFieldModule" name="module" value="property">
                                    <div class="row">

                                        <div class="col-sm-12">

                                            <div class="custom_field_html_view_mode">

                                            </div>

                                        </div>

                                    </div>

                                      <div class="edit-foot pb-15">

                                    <a href="javascript:;" class="edit_redirection viewEditClass" data_href="#collapseTwentyOne" redirection_data="info">

                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>

                                      Edit

                                    </a>

                                  </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <!--tab Ends -->

        </section>

    </main>

</div>

<!-- Wrapper Ends -->


<script>

    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";

    var upload_url = "<?php echo SITE_URL; ?>";

    var property_unique_id = getParameterByName('id');

    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";

    var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";



    /**

     * function to get id from url

     * @param name

     * @returns {string}

     */

    function getParameterByName(name) {

        var regexS = "[\\?&]" + name + "=([^&#]*)",

            regex = new RegExp(regexS),

            results = regex.exec(window.location.search);

        if (results == null) {

            return "";

        } else {

            return decodeURIComponent(results[1].replace(/\+/g, " "));

        }

    }



    <!-- SLider -->

    $(document).ready(function() {

        $('.owl-carousel').owlCarousel({

            loop: true,

            margin: 10,

            responsiveClass: true,

            autoplay:false,

            autoplayTimeout:1500,

            autoplayHoverPause:true,

            responsive: {

                0: {

                    items: 1,

                    nav: true

                },

                600: {

                    items: 1,

                    nav: true

                },

                1020: {

                    items: 2,

                    nav: true,

                    loop: true,

                    margin: 20

                },

                1199: {

                    items: 3,

                    nav: true,

                    loop: true,

                    margin: 20

                }

            }

        })

    });

</script>



<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/propertyView.js" type="text/javascript"></script>

<script>

    $(function () {

        $('.nav-tabs').responsiveTabs();

    });



    $("#show").click(function () {

        $("#bs-example-navbar-collapse-2").show();

    });

    $("#close").click(function () {

        $("#bs-example-navbar-collapse-2").hide();

    });



    $('.company-top').addClass('active');



    $(document).ready(function () {

        $(".slide-toggle").click(function () {

            $(".box").animate({

                width: "toggle"

            });

        });

    });



    $(document).ready(function () {

        $(".slide-toggle2").click(function () {

            $(".box2").animate({

                width: "toggle"

            });

        });



        if(localStorage.getItem("scrollDown")) {
            var message = localStorage.getItem("scrollDown");
            $('html, body').animate({
                'scrollTop' : $(message).position().top
            });
            localStorage.removeItem('scrollDown');
        }



    });



    $(document).ready(function () {

        // Add minus icon for collapse element which is open by default

        $(".collapse.in").each(function () {

            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");

        });



        // Toggle plus minus icon on show hide of collapse element

        $(".collapse").on('show.bs.collapse', function () {

            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");

        }).on('hide.bs.collapse', function () {

            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");

        });

    });



</script>

<?php

include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");

?>
