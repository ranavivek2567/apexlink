<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>

<style>
    .ui-datepicker .ui-datepicker-title select {
        color: #000;
    }
    .multiselect-native-select li label{
        height: auto !important;
    }
    .panel-htop .combo-panel {
        margin-top: 0px !important;
        margin-left: 0 !important;
    }

</style>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 bread-search-outer">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Property Module >> <span>Add Property</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">

                    </div>
                    <div class="col-sm-12">
                        <div class="content-section ">
                            <div class="col-sm-12">
                                <div class="content-section">
                                    <div class="add-hdr-links">
                                        <ul>
                                            <li class="active">
                                                <a class="topicons" datahref="/Property/AddProperty" href="javascript:;">
                                                    <label class="icon1"></label>
                                                    Add Property
                                                </a>
                                            </li>
                                            <li class="disabled">
                                                <a class="topicons" datahref="/Building/AddBuilding" href="javascript:;">
                                                    <label class="icon2"></label>
                                                    Add Building
                                                </a>
                                            </li>
                                            <li class="disabled">
                                                <a class="topicons" datahref="/Unit/AddUnit" id="unit_icon" href="javascript:;">
                                                    <label class="icon3"></label>
                                                    Add Units
                                                </a>
                                            </li>
                                            <li id="clone_div_icon" class="disabled">
                                                <a  id="clone_icon" href="javascript:;">
                                                    <label class="icon3"></label>
                                                    Copy/Clone an existing property
                                                </a>
                                            </li>
                                            <div class="col-md-3 all_propertiesDiv" style="display: none">
                                            <input class="form-control all_properties" type="text" />
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-form">
                                <div class="accordion-outer">

                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <!-- General Information -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> General Information</a> <a class="back" href="/Property/PropertyModules"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body">
                                                        <input type="hidden" id="property_clone_status" value="">
                                                        <input type="hidden" id="property_clone_id" value="no">
                                                        <input type="hidden" id="property_unique_id" value="">
                                                        <form action="" method="POST" id="addPropertyForm">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Property ID <em class="red-star">*</em></label>
                                                                        <input class="form-control property_idrandom" type="text" id="property_id" maxlength="20" name="property_id" placeholder="Auto Generated"/>
                                                                        <span class="property_idErr error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Property Name <em class="red-star">*</em></label>
                                                                        <input class="form-control capital" type="text" id="property_name" maxlength="150" name="property_name" placeholder="Eg: The Fairmont Waterfront"/>
                                                                        <span class="property_nameErr error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Legal Name</label>
                                                                        <input class="form-control capital" type="text" id="legal_name" maxlength="150" name="legal_name" placeholder="Eg: The Fairmont Waterfront"/>
                                                                        <span class="legal_nameErr error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Portfolio Name <em class="red-star">*</em> <a id="Newportfolio" class="pop-add-icon" onclick="clearPopUps('#NewportfolioPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class="form-control" id='portfolio_name_options' name='portfolio_id'>

                                                                        </select>
                                                                        <div class="add-popup NewportfolioPopupClass" id='NewportfolioPopup'>
                                                                            <h4>Add New Portfolio</h4>
                                                                            <div class="add-popup-body">

                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Portfolio ID<em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidatePortfoio" type="text" data_required="true" data_max="20" name='@portfolio_id' id='portfolio_id' readonly/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <label> Portfolio Name <em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="50" name='@portfolio_name' id='portfolio_name' placeholder="Add New Portfolio Name"/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <input type="button" id='NewportfolioPopupSave' class="blue-btn" value="Save" />
                                                                                        <button rel="NewportfolioPopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Property For Sale</label>
                                                                        <select class="form-control" name='property_for_sale' id="property_for_sale">
                                                                            <option selected="selected" value="No">No</option>
                                                                            <option value="Yes">Yes</option>
                                                                            <option value="Other">Other</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Property Parcel #</label>
                                                                        <input class="form-control capital" type="text" id="property_parcel_number" maxlength="200" name="property_parcel_number"/>
                                                                        <span class="property_parcel_numberErr error"></span>
                                                                    </div>
                                                                    <div class="clonepropertyTaxhidden">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3  multiplepropertydiv" id="multiplepropertydivId">
                                                                        <label>Property Tax ID </label>
                                                                        <input class="form-control add-input capital" type="text" id="property_tax_id" maxlength="20" name="property_tax_ids" placeholder="Property Tax"/>
                                                                        <span class="property_tax_idsErr error"></span>
                                                                        <a id= 'multiplepropertytax' class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <a style='display: none;' id= 'multiplepropertytaxcross' class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                    </div>
                                                                    </div>
                                                                    <div class="clonepropertyTax"></div>

                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Short-Term Rental</label>
                                                                        <select class="form-control" name='is_short_term_rental' id="is_short_term_rental">
                                                                            <option selected="selected" value="0">No</option>
                                                                            <option value="1">Yes</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Zip/Postal Code</label>
                                                                        <input class="form-control zipcode" type="text" maxlength="9" id="zipcode" name="zipcode"/>
                                                                        <span class="zipcodeErr error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Country</label>
                                                                        <input class="form-control zipcountry" type="text" maxlength="50" id="country" name="country"/>
                                                                        <span class="countryErr error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>State / Province</label>
                                                                        <input class="form-control zipstate" type="text" maxlength="50" id="state" name="state"/>
                                                                        <span class="stateErr error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>City</label>
                                                                        <input class="form-control zipcity" type="text" maxlength="50" id="city" name="city"/>
                                                                        <span class="cityErr error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Street Address 1</label>
                                                                        <input class="form-control capital" type="text" maxlength="150" id="street_address1" name="address1" placeholder="Eg: Street address 1"/>
                                                                        <span class="address1Err error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Street Address 2</label>
                                                                        <input class="form-control capital" type="text" maxlength="150" id="street_address2" name="address2" placeholder="Eg: Street address 2"/>
                                                                        <span class="address2Err error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Street Address 3</label>
                                                                        <input class="form-control capital" type="text" maxlength="150" id="street_address3" name="address3" placeholder="Eg: Street address 3"/>
                                                                        <span class="address3Err error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Street Address 4</label>
                                                                        <input class="form-control capital" type="text" maxlength="150" id="street_address4" name="address4" placeholder="Eg: Street address 4"/>
                                                                        <span class="address4Err error"></span>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Manager Name
                                                                            <a id="Newmanager" class="pop-add-icon" href="javascript:;" onclick="clearPopUps('#NewmanagerPopup')"> <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                            <em class="red-star">*</em>
                                                                        </label>
                                                                        <span id="dynamic_manager">
                                                                            <select name="manager_id" multiple="multiple" class="3col active" id="select_mangers_options">
                                                                            </select></span>
                                                                        <span class="manager_idErr error"></span>
                                                                        <div class="add-popup NewmanagerPopupClass" id='NewmanagerPopup'>
                                                                            <h4>Add New Manager</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>First Name<em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidateManager capital" type="text" data_required="true" data_max="30" name='@first_name'/>
                                                                                        <span class="customError required"></span>
                                                                                        <span class="first_nameErr error"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <label> Last Name <em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidateManager capital" type="text" data_required="true" data_max="30" name='@last_name'/>
                                                                                        <span class="customError required"></span>
                                                                                        <span class="last_nameErr error"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <label> Email <em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidateManager capitalremove" type="text" data_required="true" data_max="50" data_email="true" name='@email'/>
                                                                                        <span class="customError required"></span>
                                                                                        <span class="emailErr error"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" id='NewmanagerPopupSave' class="blue-btn">Save</button>
                                                                                        <button rel="NewmanagerPopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Country Code</label>
                                                                        <select class='form-control' name='country_code' id="country_code">

                                                                        </select>
                                                                        <span class="country_codeErr error"></span>
                                                                    </div>
                                                                    <div class="clonemultiplephonediv">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3  multiplephonediv" id="multiplephoneIDD">
                                                                        <label>Phone Number</label>
                                                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control add-input phone_number_general" name="phone_number" value="" autocomplete="off" >

                                                                        <a id='multiplephoneno' class="add-icon phone_number_error_css" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <a id='multiplephonenocross' class="add-icon phone_number_error_css" style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                    </div>
                                                                    </div>

                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Manager's Email</label>
                                                                        <input class="form-control capital" type="text" maxlength="150" placeholder="Property Manager’s Email" id="manager_email" name="manager_email" placeholder="Email" readonly/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Fax Number</label>
                                                                        <input class="form-control phone_number" type="text" maxlength="12" placeholder="Fax Number" fixedlength="12" id="fax" name="fax" value="">
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label id="price_label_id">Property Price <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                                        <input class="form-control amount number_only" type="text" id="property_price" name="property_price" placeholder="Eg: 500.00"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Attach Group <a id='Newattach' class="pop-add-icon" onclick="clearPopUps('#propertyGroupAdd')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label><span id="dynamic_groups">
                                                                            <select name="attach_groups" multiple="multiple"  id="select_group_options">
                                                                            </select></span><div class="add-popup" id='NewattachPopup'>
                                                                            <h4>Add New Property Group</h4>
                                                                            <div class="add-popup-body propertyGroupAddClass" id="propertyGroupAdd">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Group Name<em class="red-star">*</em></label>
                                                                                        <input class="form-control capital customValidateGroup capital" type="text" data_required="true" data_max="150" name='@group_name'/>
                                                                                        <span class="customError required" aria-required="true"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <label> Description <em class="red-star">*</em></label>
                                                                                        <input class="form-control capital customValidateGroup" type="text" data_required="true" data_max="150" name='@description'/>
                                                                                        <span class="customError required" aria-required="true"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn" id='NewattachPopupSave'>Save</button>
                                                                                        <button rel="propertyGroupAddClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Property Type <a id='Newtype' class="pop-add-icon" onclick="clearPopUps('#propertyTypepop')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class='form-control' id='property_type_options' name='property_type'></select>
                                                                        <div class="add-popup NewtypePopupClass" id='NewtypePopup'>
                                                                            <h4>Add New Property Type</h4>
                                                                            <div class="add-popup-body" id="propertyTypepop">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Property Type<em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidatePropertyType capital" data_required="true" data_max="150" type="text" name='@property_type'/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <label> Description <em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidatePropertyType capital" data_required="true" data_max="200" type="text" name='@description'/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn" id='NewtypePopupSave'>Save</button>
                                                                                        <button rel="NewtypePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Property Style <a id="Newprofile" class="pop-add-icon" onclick="clearPopUps('#propertyStylepop')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a> </label>
                                                                        <select class='form-control' id='property_style_options' name='property_style'></select>
                                                                        <div class="add-popup" id='NewpstylePopup'>
                                                                            <h4>Add New Property Style</h4>
                                                                            <div class="add-popup-body propertyStylepopClass" id="propertyStylepop">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Property Style <em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidatePropertyStyle capital" data_required="true" data_max="150" type="text" name='@property_style'/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <label>Description <em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidatePropertyStyle capital" data_required="true" data_max="200" type="text" name="@description"/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" id='NewpstylePopupSave' class="blue-btn">Save</button>
                                                                                        <button rel="propertyStylepopClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Property Sub-Type <a id='Newsubtype' class="pop-add-icon" onclick="clearPopUps('#NewsubtypePopup')" href=""><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class='form-control' id='property_subtype_options' name='property_subtype'></select>
                                                                        <div class="add-popup NewsubtypePopupClass" id='NewsubtypePopup'>
                                                                            <h4>Add New Property Sub-Type</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Property Sub-Type<em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidatePropertySubType capital" data_required="true" data_max="150" type="text" name="@property_subtype"/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <label> Description <em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidatePropertySubType capital" data_required="true" data_max="200" type="text" name="@description"/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn" id="NewsubtypePopupSave">Save</button>
                                                                                        <button rel="NewsubtypePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Year Built</label>
                                                                        <?php
                                                                        $currentYear=date('Y');
                                                                        $yearArray = range(1900, $currentYear);
                                                                        ?>
                                                                        <select class="form-control" id="year_built" name="property_year">
                                                                            <option value="" selected="">(Select Year Build)</option>
                                                                            <?php
                                                                            foreach ($yearArray as $year) {
                                                                                echo '<option value="' . $year . '">' . $year . '</option>';
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label><?= $_SESSION[SESSION_DOMAIN]['property_size'] ?></label>
                                                                        <input placeholder="Eg: 100" class="form-control number_only" maxlength="10" type="text" id="property_squareFootage" name="property_squareFootage"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Last Renovation</label>
                                                                        <input class="form-control last_renovation" type="text" name="last_renovation_date" value="" id="renovationdate" readonly/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Last Renovation Time</label>
                                                                        <input class="form-control" type="text"  name="last_renovation_time" id="last_renovation_timeaddid"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Last Renovation Description</label>
                                                                        <input class="form-control capital" type="text" id="last_renovation_description" name="last_renovation_description"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Number of Buildings</label>
                                                                        <input value='1' class="form-control number_only" maxlength="6" type="text" id="no_of_building" name="no_of_buildings"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Number of Units</label>
                                                                        <input value='1' class="form-control number_only" maxlength="6" type="text" id="no_of_units" name="no_of_units"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Smoking Allowed</label>
                                                                        <select placeholder="Eg: Yes" class="form-control" id="NonSmoking" name="smoking_allowed">
                                                                            <option value="" selected>Select</option>
                                                                            <option value="0">No</option>
                                                                            <option value="1">Yes</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Pet Friendly <a id='Newpet' class="pop-add-icon" onclick="clearPopUps('#NewpetPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class='form-control' id='pet_options' name='pet_friendly'>
                                                                        </select>
                                                                        <div class="add-popup NewpetPopupClass" id='NewpetPopup'>
                                                                            <h4>Add New Pet Friendly</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Pet Friendly<em class="red-star">*</em></label>
                                                                                        <input class="form-control capital customValidatePetFriendly" data_required="true" data_max="150" type="text" name='@pet_friendly'/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn" id="NewpetPopupSave">Save</button>
                                                                                        <button rel="NewpetPopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="school_district_municipality_div">
                                                                            <div class="col-sm-12 clone-1">
                                                                                <div class="col-sm-3">
                                                                                    <label>School District or Municipality <i style="" class="add-more-div fa fa-plus-circle" id="AddNewSchoolDistrictDiv" aria-hidden="true"></i><i style="display:none;" class="add-more-div glyphicon glyphicon-remove-sign red-star RemoveNewSchoolDistrictDiv" id="" aria-hidden="true"></i></label>
                                                                                    <select class="form-control school_district_municipality_select" id="school_district_municipalityID" name="school_district_municipality">
                                                                                        <option value="">Select</option>
                                                                                        <option value="School District">School District</option>
                                                                                        <option value="Municipality">Municipality</option>
                                                                                        <option value="School District/Municipality">School District/Municipality</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-6 hidden_school_fields"  style="display: none;">
                                                                                    <div class="col-sm-3">
                                                                                        <label>Code</label>
                                                                                        <input id='school_district_code' type="text" name="school_district_code" class="form-control school_district_code">
                                                                                    </div>
                                                                                    <div class="col-sm-3 ">
                                                                                        <label>Notes</label>
                                                                                        <input id="school_district_notes" type="text" name="school_district_notes" class="form-control school_district_notes capital">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3 key_optionsDivClass" id="key_optionsDiv">
                                                                        <label>Key/Access Code Info. <a  class="pop-add-icon Newkey" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class='form-control add-input key_access_codes_list  key_access_codes_info' id='key_options' name='key_access_codes_info'></select>
                                                                        <div class="key_access_codeclass"  style="display: none;">

                                                                            <textarea name="key_access_code_desc" id='key_access_code' class="capital form-control key_access_codetext key_access_codetextdynm"></textarea>

                                                                        </div>
                                                                        <a id="NewkeyPlus" onclick="clearPopUps('.NewkeyPopup')" class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <a style='display:none' id="NewkeyMinus" class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                        <div class="add-popup NewkeyPopup" id="NewkeyPopupid">
                                                                            <h4>Add New Key Access Code</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Key Access Code<em class="red-star">*</em></label>
                                                                                        <input class="form-control keyAccessCode customValidateKeyAccessCode key-access-input  capital" data_required="true" data_max="150" type="text" name='@key_code'/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>

                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn NewkeyPopupSave" data_id="" >Save</button>
                                                                                        <button rel="NewkeyPopup" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn NewkeyPopupCancel cancelPopup" value='Cancel'/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Garage Available <a id='Newgarage' class="pop-add-icon" onclick="clearPopUps('#NewgaragePopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class='form-control' id='garage_options' name='garage_available'></select>

                                                                        <div class="add-popup NewgaragePopupClass" id='NewgaragePopup'>
                                                                            <h4>Add New Garage Available</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Garage Available<em class="red-star">*</em></label>
                                                                                        <input class="form-control capital customValidateGarageAvailable" data_required="true" data_max="150" type="text" name="@garage_avail"/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>

                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn" id="NewgaragePopupSave">Save</button>
                                                                                        <button rel="NewgaragePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4 garageDesc"  style="display:none;">
                                                                        <label class="garageLabel">Notes</label>
                                                                        <textarea class="form-control capital garage_text" placeholder="Eg:- 12345 (Optional)" name="garage_note"></textarea>
                                                                    </div>
                                                                    <div class="col-md-4 garageVehicle"  style="display:none;">
                                                                        <label class="garageLabel">No. of Vehicles this Property Accommodates</label>
                                                                        <textarea class="form-control garage_veh capital" placeholder="Eg:- 12345 (Optional)" name="no_of_vehicles" id="no_of_vehicles"></textarea>
                                                                    </div>
                                                                    <div class="div-full" style="margin-top: 0px; margin-bottom: 0px;">
                                                                        <div class="form-data-range2" style="width:auto !important;">
                                                                            <div class="check-outer amenitieschck-radio">
                                                                                <input class="" type="checkbox" id="chkOnlineListing" name='online_listing' spellcheck="true"> <label>Online Listing</label>
                                                                            </div>
                                                                            <div class="check-outer amenitieschck-radio">
                                                                                <input class="" type="checkbox" id="chkOnlineApplication" name='online_application' spellcheck="true"> <label>Online Application</label>
                                                                            </div>
                                                                            <div class="check-outer amenitieschck-radio">
                                                                                <input class="" type="checkbox" id="chkDisableSyndication" name='disable_sync' spellcheck="true"> <label>Disable Syndication</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3 textarea-form form-group">
                                                                        <label>Amenities <a id='Newamenities' class="pop-add-icon" onclick="clearPopUps('#NewamenitiesPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <div id="amenties_box" class="form-control"></div>
                                                                        <div class="add-popup NewamenitiesPopupClass" id='NewamenitiesPopup'>
                                                                            <h4>Add New Amenity</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>New Amenity Code<em class="red-star">*</em></label>
                                                                                        <input class="form-control  customValidateAmenities" data_required="true" data_max="15" type="text" name="@code"/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <label> Amenity Name <em class="red-star">*</em></label>
                                                                                        <input class="form-control capital customValidateAmenities" data_required="true" data_max="50" type="text" name="@name"/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn" id="NewamenitiesPopupSave">Save</button>
                                                                                        <button rel="NewamenitiesPopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>


                                                                    <div class="col-xs-12 col-sm-4 col-md-3 textarea-form">
                                                                        <label>Description</label>
                                                                        <div class="notes_date_right_div">
                                                                            <textarea name="description" name='description' id='description' class="form-control capital property_description notes_date_right" cols="50" rows="4"></textarea></div>
                                                                    </div>
                                                                    <div class="property-status form-outer2">
                                                                        <div class="col-sm-12">
                                                                            <div class="check-outer clear" >
                                                                                <input name="is_property_with_no_building" id ="default_building_unit" style="margin-right: 5px;" type="checkbox" class="NoBuilding">
                                                                                <label>Property With No Building/Unit</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-10 details" style="display: none;">
                                                                        <label>Details</label>
                                                                        <div class="check-outer" type="text">
                                                                            <div class="col-sm-3">
                                                                                <label>Select Unit Type  <em class="red-star">*</em> <i id="AddNewUnitTypeModalPlus2" class="fa fa-plus-circle"></i></label>
                                                                                <span id="dynamic_unit_type">
                                                                                    <select class="form-control" id="unit_type" name="unit_type">
                                                                                        <option value="">Select</option>
                                                                                    </select>
                                                                                    <div class="add-popup" id='NewunitPopup'>
                                                                                        <h4>Add New Unit Type</h4>
                                                                                        <div class="add-popup-body">
                                                                                            <div class="form-outer">
                                                                                                <div class="col-sm-12">
                                                                                                    <label>Unit Type<em class="red-star">*</em></label>
                                                                                                    <input class="form-control capital customValidateUnitType" data_required="true" data_max="150" type="text" name="@unit_type"/>
                                                                                                    <span class="customError required"></span>
                                                                                                </div>
                                                                                                <div class="col-sm-12">
                                                                                                    <label>Description</label>
                                                                                                    <input class="form-control capital customValidateUnitType" type="text" maxlength="500" name="@description"/>
                                                                                                    <span class="customError required"></span>
                                                                                                </div>
                                                                                                <div class="btn-outer text-right">
                                                                                                    <button type="button" class="blue-btn" id="NewunitPopupSave">Save</button>
                                                                                                  <button type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                                    <input type="button"  class="grey-btn" value='Cancel' />
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </span>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <label>Base Rent <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?>  <em class="red-star">*</em> </label>
                                                                                <input placeholder="Eg: 2000" class="form-control number_only amount" type="text" name="base_rent" id="base_rent">
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <label>Market Rent <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?>  <em class="red-star">*</em> </label>
                                                                                <input placeholder="Eg: 2000" value="" class="form-control number_only amount" type="text" id="default_rent" name="market_rent">
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <label>Security Deposit <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?>  <em class="red-star">*</em> </label>
                                                                                <input placeholder="Eg: 2000" class="form-control number_only amount" type="text" name="security_deposit" id="security_deposit">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="btn-outer text-right">
                                                                            <input type="submit" class="blue-btn" value="Save"/>
                                                                            <button id="SavenNextId" class="blue-btn">Save & Next</button>
                                                                            <button rel="addPropertyForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                            <button type="button" class="grey-btn" id='cancel_property'>Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </div>

                                                        </form>
                                                        <input type="hidden" name="saveandnext" id="saveandnext" value="">

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Link/Owner Landlords -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseTwo" class="tabcheckvalidation" id="2collapseTwo">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Link Owners/Landlords</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action="" method="post" id="addOwnerForm">
                                                            <div class="form-outer">
                                                                <div class="row">
                                                                    <div class="div-full">
                                                                        <a id="ancCreateNewOwner" class="notes-button-bold" style="cursor: pointer; font-size: 14px; font-weight: 700;" href="/People/AddOwners">Create New Owner/Landlord</a>
                                                                    </div>
                                                                </div>
                                                                <div class="cloneOwners_div">
                                                                <div id='add_owners_div' class="grey-box-add owners_div">
                                                                    <div class="grey-box-add-inner">
                                                                        <div class="grey-box-add-lt">
                                                                            <div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Owner/Landlord</label>
                                                                                    <select id='ownerSelectid' name='owner_percentowned' class="form-control ownerSelectclass" >

                                                                                    </select>

                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Percent Owned (%) </label>
                                                                                    <input type="text" name="owner_percentowned" id="owner_percentowned" max="100" placeholder="Eg:50" value="100" class="form-control number_only percentage_owned">

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="grey-box-add-rt">
                                                                            <a id='multiplegreybox' class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                            <a style='display:none' id='multiplegreycross' style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                                <div class="row form-outer2">
                                                                    <div class="col-sm-12">
                                                                        <div class="pull-left">
                                                                            <label>Payment Type</label>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="check-outer">
                                                                                <input id="payment_type" type="radio" name="payment_type" value="Net Income" checked>
                                                                                <label>Net Income</label>
                                                                            </div>
                                                                            <div class="check-outer">
                                                                                <input id="payment_type" type="radio" name="payment_type" value="Flat">
                                                                                <label>Flat</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">&nbsp;</div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Vendor 1099 Payer</label>
                                                                        <input id="vendor_1099_payer" type="text"  name="vendor_1099_payer" readonly="readonly" class="form-control vendor_idclass" id='vendor_id' value='' placeholder="Select">
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Fiscal Year End</label>
                                                                        <select class="form-control" name="fiscal_year_end" id="fiscal_year_end">
                                                                            <option value="January">January</option>
                                                                            <option value="February">February</option>
                                                                            <option value="March">March</option>
                                                                            <option value="April">April</option>
                                                                            <option value="May">May</option>
                                                                            <option value="June">June</option>
                                                                            <option value="July">July</option>
                                                                            <option value="August">August</option>
                                                                            <option value="September">September</option>
                                                                            <option value="October">October</option>
                                                                            <option value="November">November</option>
                                                                            <option value="December" selected="selected">December</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                       <div class="btn-outer text-right">
                                                                           <button class="blue-btn">Save</button>
                                                                           <button class="blue-btn" id="SavenNextownerId">Save & Next</button>
                                                                           <button type="button" rel="addOwnerForm" class="clear-btn clearFormReset">Clear</button>
                                                                           <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                       </div>
                                                                   </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                        <input type="hidden" name="saveandnext" id="saveandnextowner" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Bank Accounts -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseThree" class="tabcheckvalidation" id="3collapseThree">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Bank Accounts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action="" method="post" id="addBankdetailForm">
                                                            <div class="row">

                                                                <div class="form-outer">
                                                                    <div class='BankDetailCloneDiv' id='BankDetailCloneId'>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Account Name <a id="add_more_bank_account_icon" class="pop-add-icon" onclick="clearPopUps('.Newaccountname')"  href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control bankAccountName" name='account_name[]' id='account_name'>
                                                                                <option value=''>Select</option>
                                                                            </select>
                                                                            <div class="add-popup Newaccountname">
                                                                                <h4>Add New Account Name</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Account Name<em class="red-star">*</em></label>
                                                                                            <input placeholder="Add new Account Name" class="form-control capital customValidateAccountName" type="text" data_required="true" data_max="20" name='@account_name' id='account_name'/>
                                                                                            <span class="customError required"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer text-right">
                                                                                            <input type="button" class="blue-btn accountnameplusSave" value="Save" />
                                                                                            <button type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                            <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Chart of Account <a  data-backdrop="static" data-toggle="modal"  data-target="#chartofaccountmodal" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control bankChartsOfAccounts" name='chart_of_account[]' id='chart_of_account'>
                                                                                <option value=''>Select</option>

                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3 accountTypeDiv">
                                                                            <label>Account Type <a class="pop-add-icon accounttypeplus" onclick="clearPopUps('.NewaccountType')" id="accounttypeplus"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control prop_account_type" name='account_type[]' id='account_type'>
                                                                                <option value=''>Select</option>

                                                                            </select>
                                                                            <div class="add-popup NewaccountType">
                                                                                <h4>Add New Account Type</h4>
                                                                                <div class="add-popup-body">

                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Account Type<em class="red-star">*</em></label>
                                                                                            <input class="form-control capital customValidateAccountType" type="text" data_required="true" data_max="20" name='@account_type' id='account_type'/>
                                                                                            <span class="customError required"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer text-right">
                                                                                            <input type="button"  class="blue-btn accounttypeplusSave" value="Save" />
                                                                                            <button rel="NewaccountType" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                            <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Bank Name</label>
                                                                            <input class="form-control capital" placeholder="Bank Name" type="text" maxlength="50" name='bank_name[]' id='bank_name'/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Bank Routing #</label>
                                                                            <input class="form-control number_only hide_copy" placeholder="Bank Routing" type="text" maxlength="16" name='bank_routing[]' id='bank_routing'/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Bank Account #</label>
                                                                            <input class="form-control hide_copy" type="text" placeholder="Bank Account" maxlength="16" name='bank_account[]' id='bank_account'/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Default Security Deposit Bank Account</label>
                                                                            <select class="form-control add-input hide_copy" name='default_security_deposit[]' id='default_security_bank_account'>
                                                                                <option value='default'>Select</option>
                                                                                <option value='Building Supplies : 5270'>Building Supplies : 5270</option>
                                                                                <option value='CAM Receivable : 1150'>CAM Receivable : 1150</option>
                                                                                <option value='Cleaning : 5230'>Cleaning : 5230</option>

                                                                            </select>
                                                                            <a  class="add-icon bankdetailClonePlus add-wholesection" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>

                                                                        </div>
                                                                        <!--                                                                    <div class="col-xs-12 col-sm-4 col-md-3">-->
                                                                        <!--                                                                        <label>Bank Account Min. Reserve Amount  --><?php //echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')';   ?><!--</label>-->
                                                                        <!--                                                                        <input class="form-control number_only amount" placeholder="Bank Account Min. Reserve Amount" type="text" name='bank_account_min_reserve_amount[]' id='bank_account_min_amount'/>-->
                                                                        <!---->
                                                                        <!--                                                                    </div>-->
                                                                        <div class="col-md-12">
                                                                            <div class="col-sm-4 col-md-3 reserve_amount">
                                                                                <label>Bank Account Min. Reserve Amount  <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                                                <input class="form-control number_only amount hide_copy" placeholder="Bank Account Min. Reserve Amount" type="text" name='bank_account_min_reserve_amount[]' id='bank_account_min_amount'/>
                                                                            </div>
                                                                            <div class="col-sm-4 col-md-3 textarea-form">
                                                                                <label>Description</label>
                                                                                <div class="notes_date_right_div">
                                                                                    <textarea class="form-control capital notes_date_right" maxlength="500" placeholder="Description" name='description[]' id="bankDescription"></textarea></div>
                                                                            </div>
                                                                            <div class="col-sm-3">
                                                                                <label></label>
                                                                                <div class="check-outer">
                                                                                    <input name="is_default[]" class="is_default" type="radio"/>
                                                                                    <input type="hidden" name="default_value[]" class="input_default" value="0">
                                                                                    <label>Set as Default</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="btn-outer text-right">
                                                                            <input type='hidden' id='BankdetailClickval' value=''>
                                                                            <button id='BankdetailClickS' class="blue-btn">Save</button>
                                                                            <button id='BankdetailClickSN' class="blue-btn">Save & Next</button>
                                                                            <button rel="addBankdetailForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Late Fee Management -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseFour" class="tabcheckvalidation" id="4collapseFour">
                                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Late Fee Management</a>
                                                </h4>
                                            </div>
                                            <div id="collapseFour" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <form action="" method="post" id="AddLateFee">
                                                        <div class="row">
                                                            <div class="latefee-check-outer">
                                                                <div class="col-sm-12 latefee-check-hdr">
                                                                    <div class="check-outer">
                                                                        <input type="checkbox" id="apply_one_time_id" value="1"/>
                                                                        <label class="blue-label">Apply One Time</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="check-input-outer">
                                                                        <input type="radio" class="late_fee_radio" id="one_time_flat_fee" name="one_time_radio" disabled=""/>
                                                                        <label>
                                                                            <input class="form-control amount number_only" type="text" id='one_time_flat_fee_desc' name="apply_one_time_flat_fee" disabled="" placeholder="Example 10.00"/>
                                                                        </label>
                                                                        <span>Flat Fee <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> (Example 10.00)</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="check-input-outer">
                                                                        <input type="radio" class="late_fee_radio" id="one_time_fee_radio" name="one_time_radio" disabled=""/>
                                                                        <label>
                                                                            <input class="form-control number_only" type="text" id='one_time_monthly_fee_desc' name="apply_one_time_percentage" max="100" disabled="" placeholder="Example 20"/>
                                                                        </label>
                                                                        <span>% of Monthly Rent (Example 20)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="latefee-check-outer">
                                                                <div class="col-sm-12 latefee-check-hdr">
                                                                    <div class="check-outer">
                                                                        <input type="checkbox" id="apply_daily_id" value="2"/>
                                                                        <label class="blue-label"> Apply Daily</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="check-input-outer">
                                                                        <input type="radio" class="late_fee_radio" name="daily_time_radio" id="daily_flat_fee" disabled=""/>
                                                                        <label>
                                                                            <input class="form-control amount number_only" type="text" id='daily_flat_fee_desc' name="apply_daily_flat_fee" disabled="" placeholder="Example 10.00"/>
                                                                        </label>
                                                                        <span>Flat Fee <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> (Example 10.00)</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="check-input-outer">
                                                                        <input type="radio" class="late_fee_radio" name="daily_time_radio" id="daily_monthly_fee" disabled=""/>
                                                                        <label>
                                                                            <input class="form-control number_only" type="text" id='daily_monthly_fee_desc' max="100" name="apply_daily_percentage" disabled="" placeholder="Example 20"/>
                                                                        </label>
                                                                        <span>% of Monthly Rent (Example 20)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <div class="check-outer">
                                                                    <label class="blue-label lh30">Grace Period<em class="red-star">*</em></label>
                                                                    <label>
                                                                        <input class="form-control number_only" type="text" maxlength="10" name='grace_period'  disabled="" id="grace_period" placeholder="Example 10"/>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">
                                                                    <input type="hidden"  id="LatefeebuttonClicked" value="">
                                                                    <button class="blue-btn" id="AddLatefeeButton">Save</button>
                                                                    <button class="blue-btn" id="AddLatefeeButton1">Save & Next</button>
                                                                    <button rel="AddLateFee" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                    <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Key Tracker -->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseFive" class="tabcheckvalidation" id="5collapseFive">
                                                        <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Key Tracker</a>
                                                </h4>
                                            </div>
                                            <div id="collapseFive" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <form action="" method="post" id="addkeytags">
                                                        <input type="hidden" name="id" id="key_id" value="">
                                                        <div class="row">
                                                            <div class="latefee-check-outer">
                                                                <div class="col-sm-12 latefee-check-hdr">
                                                                    <div class="check-outer">
                                                                        <input type="radio" name="KeyAddTrack" checked="" class='addkeytracker' value='addkey'/>
                                                                        <label class="blue-label">Add key</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 latefee-check-hdr">
                                                                    <div class="check-outer">
                                                                        <input type="radio" name="KeyAddTrack" class='addkeytracker' value='trackkey'/>
                                                                        <label class="blue-label">Track Key</label>
                                                                    </div>
                                                                </div>
                                                                <div class="div-full" id="addkeyhref">
                                                                    <a href="javascript:;"  class="active">Add Key</a>
                                                                </div>
                                                                <div class="div-full" id="trackkeyhref" style="display:none;">
                                                                    <a href="javascript:;"  class="active">Track Key</a>
                                                                </div>
                                                            </div>
                                                            <div id="addKeyDiv" style="display: none;">
                                                                <div class="form-outer">

                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Key Tag <em class="red-star">*</em></label>
                                                                            <input class="form-control capital" type="text" maxlength="50" name="key_tag" id="key_tag" placeholder="Key Tag"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Description <em class="red-star">*</em></label>
                                                                            <input class="form-control description1" placeholder="Description" maxlength="50" type="text" name="description" id="description"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Total Keys <em class="red-star">*</em></label>
                                                                            <input class="form-control number_only" placeholder="Total Keys" maxlength="50" type="text" name="total_keys" id="total_keys"/>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="btn-outer text-right">
                                                                                <input type="hidden"  id="KeybuttonClicked" value="">
                                                                                <button type="submit" class="blue-btn" id="AddKeyButton">Save</button>
                                                                                <button class="blue-btn" id="AddKeyButton1">Save & Next</button>
                                                                                <button rel="addkeytags" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" class="grey-btn cancel-all" >Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </form>
                                                        <div class="grid-outer listinggridDiv addkeylistingstyle">
                                                            <div class="apx-table">
                                                                <div class="table-responsive">
                                                                    <table id="key-table" class="table table-bordered">
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <form action="" method="post" id="TrackkeydetailForm">
                                                            <input type="hidden" name="id" id="trackkey_id" value="">
                                                            <div id='addTrackDiv' class="form-outer divNewtracker" style="display: none;">
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Name</label>
                                                                    <input placeholder="Key Tag" type="text" class="form-control capital" name="key_name" id="trackkey_name" placeholder="Key Tag"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Email</label>
                                                                    <input type="text" placeholder="Email" class="form-control edittrackemail capitalremove" maxlength="50" name="trackemail" id="trackemail"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Company Name <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Company Name" class="form-control edittrackcompany_name capital" maxlength="50" name="company_name" id="trackcompany_name"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Phone # <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Phone" class="form-control phone_number edittrackphone" maxlength="12" name="phone" id="trackphone"/>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address1 <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Address1" class="form-control capital" maxlength="50" name="Address1" id="trackAddress1"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address2 <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Address2" class="form-control capital" maxlength="50" name="Address2" id="trackAddress2"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address3 <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Address3" class="form-control capital" maxlength="50" name="Address3" id="trackAddress3"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Address4 <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Address4" class="form-control capital" maxlength="50" name="Address4" id="trackAddress4"/>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Key# <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Key" class="form-control" maxlength="50" name="key_number" id="trackkey_number"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Key Quantity <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Key Quantity" class="form-control number_only" maxlength="20" name="key_quality" id="trackkey_quality"/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Pick Up Date</label>
                                                                    <input type="text" placeholder="Pick Up Date" class="form-control" readonly name="pick_up_date" id="pick_up_date" readonly=""/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Pick Up Time</label>
                                                                    <input type="text" placeholder="Pick Up Time" class="form-control" readonly name="pick_up_time" id="pick_up_time" readonly=""/>
                                                                </div>

                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Return Date <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Return Date" class="form-control" readonly name="return_date" id="return_date" readonly=""/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Return Time <em class="red-star">*</em></label>
                                                                    <input type="text" placeholder="Return Time" class="form-control" readonly name="return_time" id="return_time" readonly=""/>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                    <label>Key Designator</label>
                                                                    <input type="text" placeholder="Key Designator" class="form-control capital" maxlength="50" name="key_designator" id="key_designator"/>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="btn-outer text-right">
                                                                        <input type="hidden"  id="TrackbuttonClicked" value="">
                                                                        <button type="submit" class="blue-btn" id="TrackKeyButton">Save</button>
                                                                        <button class="blue-btn" id="TrackKeyButton1">Save & Next</button>
                                                                        <button rel="TrackkeydetailForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                        <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="grid-outer listinggridDiv trackkeylistingstyle" style="display:none">
                                                                <div class="apx-table apxtable-bdr-none">
                                                                    <div class="table-responsive">
                                                                        <table id="trackkey-table" class="table table-bordered">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>

                                                    </div>

                                                </div>
                                            </div>
                                            <!-- Management Fee -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseSix" class="tabcheckvalidation" id="6collapseSix">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Management Fees</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSix" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action="" method="post" id="addManagementFeeForm">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Type</label>
                                                                        <select class="form-control"  name="management_type" id="magt_type">
                                                                            <option value="Select">Select</option>
                                                                            <option value="Flat">Flat</option>
                                                                            <option value="Percent">Percent</option>
                                                                            <option value="Percentage and a min. set fee">Percentage and a min. set fee</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label id="value_type">Value <em class="red-star">*</em></label>
                                                                        <input class="form-control" type="text" name="management_value" id="management_fee_value"/>
                                                                    </div>
                                                                    <div style="display:none;" id="minimum_fee" class=" col-sm-3">
                                                                        <label>Minimum <em class="red-star">*</em></label>
                                                                        <input type="text" id="minimum_management_fee"  name="minimum_management_fee" class="form-control p-number-format">
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="btn-outer text-right">

                                                                            <input type="hidden" value="" id="ManagementFeeclick"/>
                                                                            <button class="blue-btn" id="AddManagementfeeButton">Apply</button>
                                                                            <button class="blue-btn" id="AddManagementfeeButton1">Save & Next</button>
                                                                            <button rel="addManagementFeeForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Management Info -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseSeven" class="tabcheckvalidation" id="7collapseSeven">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Management Info</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSeven" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action="" method="post" id="addManagementinfoForm">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Management Company Name <em class="red-star">*</em></label>
                                                                        <input class="form-control capital" type="text" maxlength="50" name="management_company_name" id="management_company_name"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Management Start Date <em class="red-star">*</em></label>
                                                                        <input class="form-control calander" type="text" name="management_start_date" id="management_start_date" readonly=""/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Management End Date <em class="red-star">*</em></label>
                                                                        <input class="form-control calander" type="text" name="management_end_date" id="management_end_date" readonly="" placeholder="Eg: 10/10/2014">
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Reason the Management Ended <a class="pop-add-icon NewReasonPlus" onclick="clearPopUps('#NewReasonPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                        <select class="form-control" name="reason_management_end" id="selectreasondiv">

                                                                        </select>
                                                                        <div class="add-popup NewkeyPopup" id="NewReasonPopup">
                                                                            <h4>Add New Reason</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Reason<em class="red-star">*</em></label>
                                                                                        <input class="form-control capital customValidateNewReasonPopup" data_required="true" data_max="150" type="text" name='@reason'/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>

                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn NewReasonSave" data_id="" >Save</button>
                                                                                        <button type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                        <input type="button"  class="grey-btn NewkeyPopupCancel cancelPopup" value='Cancel'/>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Description <em class="red-star">*</em></label>
                                                                        <div class="notes_date_right_div">
                                                                            <textarea class="form-control capital notes_date_right" maxlength="50" type="text" name="description" id="management_infodesc"></textarea></div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="btn-outer text-right">
                                                                            <input type="hidden" id="AddManagementinfoclick" value="">
                                                                            <button class="blue-btn" id="AddManagementinfoButton">Save</button>
                                                                            <button class="blue-btn" id="AddManagementinfoButton1">Save & Next</button>
                                                                            <button rel="addManagementinfoForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Maintenance Info -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseEight" class="tabcheckvalidation" id="8collapseEight">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Maintenance Information</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseEight" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action="" method="post" id="addMaintenanceinformationForm">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Maintenance Spending Limit</label>
                                                                        <select class="form-control maintenance_speed_time"  name="maintenance_speed_time" id="maintenance_speed_time">

                                                                            <option value="No Spending Limit">No Spending Limit</option>
                                                                            <option value="Monthly">Monthly</option>
                                                                            <option value="Quarterly">Quarterly</option>
                                                                            <option value="Semi-Annually">Semi-Annually</option>
                                                                            <option value="Annual">Annual</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Enter Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?></label>
                                                                        <input class="form-control amount number_only" type="text" id="maintenance_amount" name="amount" disabled=""/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Insurance Expiration <em class="red-star">*</em></label>
                                                                        <input class="form-control calander" type="text" id="InsuranceExpiration" name="Insurance_expiration" readonly="" placeholder="Eg: 06/13/2019"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Home Warranty Expiration <em class="red-star">*</em></label>
                                                                        <input class="form-control calander" type="text" id="HomeWarrantyExpiration" name="warranty_expiration" readonly="" placeholder="Eg: 06/13/2019"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3 textarea-form">
                                                                        <label>Special Instructions <em class="red-star">*</em></label>
                                                                        <div class="notes_date_right_div">
                                                                            <textarea id="special_instructions" class="form-control capital notes_date_right" maxlength="200" name="special_instructions" placeholder="Instructions"></textarea></div>
                                                                    </div>
                                                                    <div class="form-outer2">
                                                                        <div class="col-sm-12">
                                                                            <div class="btn-outer text-right">
                                                                                <input  type="hidden" id="AddMaintenanceinfoclick" value=""/>
                                                                                <button class="blue-btn" id="AddMaintenanceinfoButton">Save</button>
                                                                                <button class="blue-btn" id="AddMaintenanceinfoButton1">Save & Next</button>
                                                                                <button rel="addMaintenanceinformationForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Mortgage Info -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseNine" class="tabcheckvalidation" id="9collapseNine">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Mortgage Information</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseNine" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action="" method="post" id="addMortgageinformationForm">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Mortgage Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> <em class="red-star">*</em></label>
                                                                        <input class="form-control amount number_only" type="text" id='mortgage_amount' name="mortgage_amount" maxlength="10" placeholder="Eg: 10.00"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Property Assessed Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> <em class="red-star">*</em></label>
                                                                        <input class="form-control amount number_only" type="text" id='property_assessed_amount' name="property_assessed_amount" maxlength="10" placeholder="Eg: 10.00"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Mortgage Start Date <em class="red-star">*</em></label>
                                                                        <input class="form-control calander" type="text" name="mortgage_start_date" id="mortgage_start_date" placeholder="Eg: 05/31/2019" readonly=""/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Term <em class="red-star">*</em></label>
                                                                        <input id="term" class="form-control number_only" maxlength="2" type="text" name="term" placeholder="Eg: 10"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Interest Rate % <em class="red-star">*</em></label>
                                                                        <input class="form-control" type="text"  id="interest_rate" name="interest_rate" placeholder="Eg: 6"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Financial Institution <em class="red-star">*</em></label>
                                                                        <input id="financial_institution" class="form-control capital" type="text" name="financial_institution" maxlength="20" placeholder="Financial Institution"/>
                                                                    </div>
                                                                    <div class="form-outer2">
                                                                        <div class="col-sm-12">
                                                                            <div class="btn-outer text-right">
                                                                                <input type="hidden" value="" id="AddMortgageinfoclick">
                                                                                <button class="blue-btn" id="AddMortgageinfoButton">Save</button>
                                                                                <button class="blue-btn" id="AddMortgageinfoButton1">Save & Next</button>
                                                                                <button rel="addMortgageinformationForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Property Loan -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseTen" class="tabcheckvalidation" id="10collapseTen">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Property Loan</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTen" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action="" method="post" id="addPropertyloanForm">
                                                            <div class="row">
                                                                <div class="latefee-check-outer">
                                                                    <div class="col-sm-12 latefee-check-hdr">
                                                                        <div class="check-outer">
                                                                            <label class="blue-label">Loan</label>
                                                                        </div>
                                                                        <div class="check-outer">
                                                                            <input type="radio" name="loan_status" value="1" class="loan_status_class"/>
                                                                            <label>Yes</label>
                                                                        </div>
                                                                        <div class="check-outer">
                                                                            <input type="radio" name="loan_status" value="0" class="loan_status_class"/>
                                                                            <label>No</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row" id="loanDiv" style="display:none">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label> Vendor Name(Bank or Lender) <em class="red-star">*</em></label>
                                                                        <input class="form-control capital" maxlength="30" type="text" name="vendor_name"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Loan Amount <em class="red-star">*</em></label>
                                                                        <input class="form-control" id='loan_amount' type="text" name="loan_amount"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Loan Number <em class="red-star">*</em></label>
                                                                        <input class="form-control" maxlength="20" type="text" name="loan_number" id="loan_number"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Loan Start Date <em class="red-star">*</em></label>
                                                                        <input class="form-control calander" type="text" name="loan_start_date" id="loan_start_date" readonly=""/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Loan Maturity Date <em class="red-star">*</em></label>
                                                                        <input class="form-control calander" type="text" name="loan_maturity_date" id="loan_maturity_date" readonly="" placeholder="Eg: 10/10/2014"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Monthly Due Date <em class="red-star">*</em></label>
                                                                        <select style="cursor: pointer;" id="monthly_due_date" class="form-control" name="monthly_due_date">
                                                                            <option value="Select">Select</option>
                                                                            <?php
                                                                            for ($i = 1; $i <= 31; $i++) {
                                                                                echo "<option value='" . $i . "'>$i</option>";
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Payment Amount <em class="red-star">*</em></label>
                                                                        <input class="form-control" id='payment_amount' type="text" name="payment_amount"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Interest Only </label>
                                                                        <div class="check-outer">
                                                                            <input type="radio" name="interest_only" value="yes" />
                                                                            <label>Yes</label>
                                                                        </div>
                                                                        <div class="check-outer">
                                                                            <input type="radio" name="interest_only" value="no" checked=""/>
                                                                            <label>No</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Loan Terms <em class="red-star">*</em></label>
                                                                        <input class="form-control" type="text" name="loan_terms" placeholder="Interest Rate"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>&nbsp;&nbsp;<em class="red-star">*</em></label></label>
                                                                        <select placeholder="Select the Rate"  name="loan_type" class="form-control">
                                                                            <option value=" ">Select</option>
                                                                            <option value="Fixed Rate">Fixed Rate</option>
                                                                            <option value="Variable Rate">Variable Rate</option>
                                                                        </select>

                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>&nbsp;&nbsp;</label>
                                                                        <input class="form-control amount number_only" type="text" name="loan_rate" placeholder="Enter Rate"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-outer">
                                                                <div class="btn-outer text-right">
                                                                    <input type="hidden" id="AddPropertyloanclick" value="">
                                                                    <button class="blue-btn" id="AddPropertyloanButton">Save</button>
                                                                    <button class="blue-btn" id="AddPropertyloanButton1">Save & Next</button>
                                                                    <button rel="addPropertyloanForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                    <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Property Insurance -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseEleven" class="tabcheckvalidation" id="11collapseEleven">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Property Insurance</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseEleven" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form  method="post" id='addPropertyInsurance'>
                                                            <div class="row">
                                                                <div class="div-full" id="insuranceOuterDivhref">
                                                                    <a href="javascript:;"  class="active notes-button-bold">Add New Insurance</a>
                                                                </div>
                                                                <div id="insuranceOuterDivOpen" style="display:none;">
                                                                    <div class="col-sm-12">
                                                                        <div class="grey-box-add" class='insuranceOuterDiv'>
                                                                            <div class="insuranceOuterDivId" id='insuranceOuterDivId'>
                                                                                <div class="grey-box-add-inner">
                                                                                    <div class="grey-box-add-lt" >
                                                                                        <div class="form-outer">
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Insurance Type <a class="pop-add-icon NewInsurancePlus" onclick="clearPopUps('.NewInsurancePopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a> <em class="red-star">*</em></label>
                                                                                                <select name='company_insurance_type[]' id="SelectInsuranceTypes" class="form-control"></select>
                                                                                                <div class="add-popup NewInsurancePopup">
                                                                                                    <h4>Add New Insurance Type</h4>
                                                                                                    <div class="add-popup-body">
                                                                                                        <div class="form-outer">
                                                                                                            <div class="col-sm-12">
                                                                                                                <label>Insurance Type<em class="red-star">*</em></label>
                                                                                                                <input class="form-control customValidateNewInsurance capital" data_required="true" data_max="150" type="text" name='insurance_type' id='insurance_type' placeholder="Add New Insurance Type"/>
                                                                                                                <span class="customError required"></span>
                                                                                                                <span class="portfolio_idErr error"></span>
                                                                                                            </div>
                                                                                                            <div class="btn-outer text-right">
                                                                                                                <input type="button"  class="blue-btn NewInsurancePopupSave" value="Save" data_id="" />
                                                                                                                <button rel="NewInsurancePopup" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                                                <input type="button"  class="grey-btn NewInsuranceCancel cancelPopup" value='Cancel' />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Insurance Company Name <em class="red-star">*</em></label>
                                                                                                <input class="form-control capital" type="text" maxlength="50" placeholder="InsuranceCompanyName" name='insurance_company_name[]' id="insurance_company_name"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Policy # <em class="red-star">*</em></label>
                                                                                                <input class="form-control capital" type="text" maxlength="20" placeholder="Policy#" name="policy[]" id="policy"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Policy Type  <a  class="pop-add-icon NewPolicyPlus" onclick="clearPopUps('.NewPolicyPopup')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                                <select name='policy_name[]' id='SelectPolicyTypes' class="form-control SelectPolicyTypesClass"></select>
                                                                                                <div class="add-popup NewPolicyPopup" id='NewPolicyPopup'>
                                                                                                    <h4>Add New Policy Type</h4>
                                                                                                    <div class="add-popup-body">
                                                                                                        <div class="form-outer">
                                                                                                            <div class="col-sm-12">
                                                                                                                <label>Policy Type<em class="red-star">*</em></label>
                                                                                                                <input class="form-control customValidateNewPolicy capital" data_required="true" data_max="150" type="text" name='policy_type'  placeholder="Add New Policy Type"/>
                                                                                                                <span class="customError required"></span>
                                                                                                                <span class="portfolio_idErr error"></span>
                                                                                                            </div>
                                                                                                            <div class="btn-outer text-right">
                                                                                                                <input type="button" class="blue-btn NewPolicyPopupSave" value="Save" data_id="" />
                                                                                                                <button rel="NewPolicyPopup" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                                                <input type="button" class="grey-btn NewPolicyPopupCancel cancelPopup" value='Cancel' />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Agent Name <em class="red-star">*</em></label>
                                                                                                <input class="form-control capital" type="text" maxlength="50" placeholder="Agent Name" name='agent_name[]' id="agent_name"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Country Code <em class="red-star">*</em> </label>
                                                                                                <select class='form-control' name='insurance_country_code[]' id="insurance_country_code">

                                                                                                </select>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Phone Number <em class="red-star">*</em></label>
                                                                                                <input class="form-control phone_number" type="text" placeholder="Phone Number" name='phone_number[]' maxlength="12" id="ins_phone_number"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Email <em class="red-star">*</em></label>
                                                                                                <input class="form-control capitalremove" type="text" maxlength="50" placeholder="Email" name='property_insurance_email[]' id="property_insurance_email"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Coverage Amount <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> <em class="red-star">*</em></label>
                                                                                                <input class="form-control amount number_only" type="text" maxlength='10' placeholder="Coverage Amount" name='coverage_amount[]' id="coverage_amount"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Annual Premium <?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> <em class="red-star">*</em></label>
                                                                                                <input class="form-control amount number_only" type="text"  maxlength="10" placeholder="Annual Premium" name='annual_premium[]' id="annual_premium"/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Start Date <em class="red-star">*</em></label>
                                                                                                <input class="form-control ins_start_date" type="text" placeholder="Start date" name='start_date[]'  readonly="" />
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>End Date <em class="red-star">*</em></label>
                                                                                                <input class="form-control ins_end_date" type="text" placeholder="End date" name='end_date[]'  readonly=""/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Renewal Date <em class="red-star">*</em></label>
                                                                                                <input class="form-control ins_renewal_date" type="text" placeholder="Renewal Date" name='renewal_date[]'  readonly=""/>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                <label>Notes <em class="red-star">*</em></label>
                                                                                                <div class="notes_date_right_div">
                                                                                                    <textarea class="form-control capital notes_date_right" type="text" maxlength="200" placeholder="" name='notes[]' id='ins_notes'></textarea></div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                    <div class="grey-box-add-rt">
                                                                                        <a id='propertyInsurancePlus' class="pop-add-icon propertyInsurancePlus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                                        <a id='propertyInsuranceCross' class="propertyInsuranceCross" style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <div class="btn-outer text-right">
                                                                                <input type='hidden' id='PropertyInsuranceid' value="">
                                                                                <input type='hidden' id='AddPropertyInsuranceInput' value="">
                                                                                <button class="blue-btn" id='AddPropertyInsuranceSave1'>Save</button>
                                                                                <button class="blue-btn" id='AddPropertyInsuranceSave2'>Save & Next</button>
                                                                                <button rel="addPropertyInsurance" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="grid-outer listinggridDiv">
                                                                    <div class="apx-table apxtable-bdr-none">
                                                                        <div class="table-responsive">
                                                                            <table id="insurance-table" class="table table-bordered">
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Warranty Information -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseTwelve" class="tabcheckvalidation" id="12collapseTwelve">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Warranty Information </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwelve" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form method="post" id="addwarrantyinforform">
                                                            <div class="latefee-check-outer">
                                                                <div class="div-full" id="warrantyinforhref">
                                                                    <a href="javascript:;"  class="active notes-button-bold">Add Fixtures</a>
                                                                </div>
                                                            </div>
                                                            <div id="warrantyinfoDivopen" style="display:none">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Key Fixture Name <em class="red-star">*</em></label>
                                                                            <input class="form-control capital" type="text" maxlength="50" name="key_fixture_name" placeholder="Key Fixture Name"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Fixture Type <a id='FixturePopupPlus' onclick="clearPopUps('#AddFixturePopup')" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select id='fixture_type' name="fixture_type" class="form-control"></select>
                                                                            <div class="add-popup AddFixturePopupClass" id='AddFixturePopup'>
                                                                                <h4>Add New Fixture Type</h4>
                                                                                <div class="add-popup-body">
                                                                                    <div class="form-outer">
                                                                                        <div class="col-sm-12">
                                                                                            <label>Fixture Type<em class="red-star">*</em></label>
                                                                                            <input class="form-control capital customValidateAddFixture" data_required="true" data_max="50" type="text" name='@fixture_type' id='fixture_type' placeholder="Add New Fixture Type"/>
                                                                                            <span class="customError required"></span>
                                                                                        </div>
                                                                                        <div class="col-sm-12">
                                                                                            <label>Fixture Description<em class="red-star">*</em></label>
                                                                                            <input class="form-control capital customValidateAddFixture" data_required="true" data_max="150" type="text" name='@fixture_desc' id='fixture_desc' placeholder="Fixture Description"/>
                                                                                            <span class="customError required"></span>
                                                                                        </div>
                                                                                        <div class="btn-outer text-right">
                                                                                            <button type="submit" id='FixturePopupSave' class="blue-btn">Save</button>
                                                                                            <button rel="AddFixturePopupClass" type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                            <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Model</label>
                                                                            <?php
                                                                            // $yearArray = range(1900, 2019);
                                                                            $yearArray = range(1900, 2019);
                                                                            ?>
                                                                            <select class="form-control" name="model" id="model">
                                                                                <option value="">Select Year</option>
                                                                                <?php
                                                                                foreach ($yearArray as $year) {
                                                                                    $selected = ($year == 2019) ? 'selected' : '';
                                                                                    echo '<option ' . $selected . ' value="' . $year . '">' . $year . '</option>';
                                                                                }
                                                                                ?>
                                                                            </select>

                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Assessed Age <em class="red-star">*</em></label>
                                                                            <input id='assessed_age' class="form-control number_only" maxlength="3" type="text" name='assessed_age' placeholder="Assessed Age"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Maintenance Reminder <em class="red-star">*</em></label>
                                                                            <input type='text' id='maintenance_reminder' name='maintenance_reminder' class="form-control" readonly="" placeholder="Maintenance Reminder">
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Warranty Expiration Date <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" id='warranty_expiration_date' name='warranty_expiration_date' readonly="" placeholder="Maintenance Reminder"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Insurance Expiration Date <em class="red-star">*</em> </label>
                                                                            <input type='text' class="form-control calander" name='insurance_expiration_date' id='insurance_expiration_date' readonly="" placeholder="Insurance Expiration">
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Manufacturer's Name <em class="red-star">*</em></label>
                                                                            <input id='manufacturer_name' class="form-control capital" maxlength="50" type="text" name='manufacturer_name'/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Manufacturer's Phone # <em class="red-star">*</em></label>
                                                                            <input id='manufacturer_phonenumber' class="form-control phone_number" type="text" name='manufacturer_phonenumber' maxlength="12"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3 textarea-form">
                                                                            <label>Add Notes <em class="red-star">*</em></label>
                                                                            <div class="notes_date_right_div">
                                                                                <textarea id='add_notes' class="form-control capital notes_date_right" maxlength="500" name='add_notes' placeholder="Notes"></textarea></div>
                                                                        </div>
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <div class="btn-outer text-right">
                                                                                    <input type='hidden' id='Warrantyinfoid' name='id' value="">
                                                                                    <input type='hidden' id='warrantyinforClick' value=''>
                                                                                    <button class="blue-btn" id='warrantyinforClickB'>Save</button>
                                                                                    <button class="blue-btn" id='warrantyinforClickB1'>Save & Next</button>
                                                                                    <button rel="addwarrantyinforform" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                                    <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="grid-outer listinggridDiv">
                                                                <div class="apx-table apxtable-bdr-none">
                                                                    <div class="table-responsive">
                                                                        <table id="fixtures-table" class="table table-bordered">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Manage Charge -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseThirteen" class="tabcheckvalidation" id="13collapseThirteen">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Manage Charges </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThirteen" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="accordion-grid">
                                                                    <div class="accordion-outer">
                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">
                                                                                <div class="panel panel-default apxtable-bdr-none">
                                                                                    <!-- Unit Charge table-->
                                                                                    <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="apx-table">
                                                                                                <div class="table-responsive">
                                                                                                    <table id="propertyCharge-table" class="table table-bordered">
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-outer">
                                                                <div class="col-sm-12">
                                                                    <div class="btn-outer">
                                                                        <button type="button" id="propertyChargeCodeSave" class="blue-btn">Apply</button>
                                                                        <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="accordion-grid">
                                                                    <div class="accordion-outer">
                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">
                                                                                <div class="panel panel-default apxtable-bdr-none">
                                                                                    <!-- Unit Charge table-->
                                                                                    <div class="row">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="apx-table">
                                                                                                <div class="table-responsive">
                                                                                                    <table id="unitCharge-table" class="table table-bordered">
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="form-outer">
                                                                    <div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" id="saveUnitCharges" class="blue-btn">Apply</button>
                                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="accordion-grid">
                                                                    <div class="accordion-outer">
                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        <h4 class="panel-title">
                                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsePropertyPrefrence" aria-expanded="true" >
                                                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Charges : Unit</a>
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div id="collapsePropertyPrefrence" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                        <form id="manageCharge">
                                                                                            <div class="panel-body pad-none">
                                                                                                <div class="property-status">
                                                                                                    <div class="check-outer">
                                                                                                        <input type="radio" checked class="unitSelect" name="charge_code_type" value="0">
                                                                                                        <label>Property</label>
                                                                                                    </div>
                                                                                                    <div class="check-outer">
                                                                                                        <input type="radio" class="unitSelect" name="charge_code_type" value="1">
                                                                                                        <label>Unit</label>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                        <label>Charge Code <em class="red-star">*</em><a class="pop-add-icon" id="addChargeCodeButton" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#chargeCodeModel"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                                        <select class="form-control" name="charge_code" id="chargeCodeSelect"></select>
                                                                                                    </div>
                                                                                                    <div class="col-xs-12 col-sm-4 col-md-3" id="unitTypePropertyDiv" style="display: none;">
                                                                                                        <label>Unit Type <a class="pop-add-icon" id="AddNewUnitTypeModalPlus3" onclick="clearPopUps('#NewunitPopup2')" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                                        <select class="form-control" name="unit_type_id" multiple="multiple"  id="unit_type2" style="overflow-x: auto;">
                                                                                                            <option value="">Select</option>
                                                                                                        </select>
                                                                                                        <div class="add-popup" id='NewunitPopup2'>
                                                                                                            <h4>Add New Unit Type</h4>
                                                                                                            <div class="add-popup-body">
                                                                                                                <div class="form-outer">
                                                                                                                    <div class="col-sm-12">
                                                                                                                        <label>Unit Type<em class="red-star">*</em></label>
                                                                                                                        <input class="form-control capital customValidateUnitType2" data_required="true" data_max="150" type="text" name="@unit_type"/>
                                                                                                                        <span class="customError required"></span>
                                                                                                                    </div>
                                                                                                                    <div class="col-sm-12">
                                                                                                                        <label>Description</label>
                                                                                                                        <input class="form-control capital customValidateUnitType2" type="text" maxlength="500" name="@description"/>
                                                                                                                        <span class="customError required"></span>
                                                                                                                    </div>
                                                                                                                    <div class="btn-outer text-right">
                                                                                                                        <button type="button" class="blue-btn" id="NewunitPopupSave2">Save</button>
                                                                                                                        <button type="button" class="clear-btn clearFormResetPopup">Clear</button>
                                                                                                                        <input type="button"  class="grey-btn cancelPopup" value='Cancel' />
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                        <label>Frequency <em class="red-star">*</em></label>
                                                                                                        <select id="Frequency" class="form-control" name="frequency" style="cursor: pointer;">
                                                                                                            <option value="">Select</option>
                                                                                                            <option value="1">One Time</option>
                                                                                                            <option value="2">Monthly</option>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                    <div class="col-xs-12 col-sm-4 col-md-3" id="typeForProperty">
                                                                                                        <label>Type <em class="red-star">*</em></label>
                                                                                                        <select id="typeForProperty" name="type" class="form-control" style="cursor: pointer;">
                                                                                                            <option value="">Select</option>
                                                                                                            <option value="1">CAM-Per Unit</option>
                                                                                                            <option value="2">CAM-Per Sq.Ft.</option>
                                                                                                            <option value="3">Non-CAM</option>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                    <div class="col-sm-12">

                                                                                                        <div class="btn-outer text-right">
                                                                                                            <button type="submit" class="blue-btn">Save & Next</button>
                                                                                                            <button rel="manageCharge" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                                                        </div>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Owner Preferred vendor -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseFourteen" class="tabcheckvalidation" id="14collapseFourteen">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Owner Preferred Vendors</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFourteen" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action='' method="post" id='owneredPreferredForm'
                                                              <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3 easy-ui">
                                                                        <label>Select Vendor</label>
                                                                        <input class="form-control owner_pre_vendor" type="text" name='vendor_name'/>
                                                                        <input class="form-control owner_pre_vendorID" type="hidden" value=""/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Address</label>
                                                                        <input class="form-control" type="text" name='vendor_address' id='vendor_address' disabled/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>&nbsp;</label>
                                                                        <button class="blue-btn">ADD</button>
                                                                    </div>
                                                                    <div class="grid-outer listinggridDiv">
                                                                        <div class="apx-table apxtable-bdr-none">
                                                                            <div class="table-responsive">
                                                                                <table id="vendors-table" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="btn-outer text-right">
                                                                            <input type='hidden' id='owneredPreferredClickval' value="">
                                                                            <button type='button' class="blue-btn" id='owneredPreferredClickR'>Remove</button>
                                                                            <button class="blue-btn" id='owneredPreferredClickS'>Save</button>
                                                                            <button class="blue-btn" id='owneredPreferredClickSN'>Save & Next</button>
                                                                            <button rel="owneredPreferredForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                            <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Black List Vendor -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseFifteen" class="tabcheckvalidation" id="15collapseFifteen">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Black Listed Vendors</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseFifteen" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action="" method="post" id="blacklistvendorForm">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Select Vendor</label>
                                                                        <input class="form-control" type="text" name="vendor_name" id="blacklistvendor_name"/>
                                                                        <input class="form-control blacklist_vendorID" type="hidden" value=""/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Address</label>
                                                                        <input class="form-control" type="text" name="vendor_address" id="blacklistvendor_address" disabled=""/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Date</label>
                                                                        <input class="form-control" type="text" name="blacklist_Date" id="blacklist_Date"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <label>Reasoning For BlackListing <em class="red-star">*</em></label>
                                                                        <input class="form-control capital" type="text" name="blacklist_reason" id="blacklist_reason"/>
                                                                    </div>
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <button class="blue-btn">ADD</button>
                                                                    </div>
                                                                    <div class="grid-outer listinggridDiv">
                                                                        <div class="apx-table apxtable-bdr-none">
                                                                            <div class="table-responsive">
                                                                                <table id="blacklisted-table" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="btn-outer text-right">
                                                                            <input type="hidden" id="BlacklistVendorClickval" value="">
                                                                            <button type='button'class="blue-btn" id="BlacklistVendorClickR">Remove</button>
                                                                            <button class="blue-btn" id="BlacklistVendorClickS">Save</button>
                                                                            <button class="blue-btn" id="BlacklistVendorClickSN">Save & Next</button>
                                                                            <!--                                                                        <button class="grey-btn" id="BlacklistVendorClickC">Cancel</button>-->
                                                                            <button rel="blacklistvendorForm" type="button" class="clear-btn clearFormReset">Clear</button>
                                                                            <button type="button" class="grey-btn cancel-all" id="BlacklistVendorClickC">Cancel</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Photos/Virtual Tour Videos -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseSixteen" class="tabcheckvalidation" id="16collapseSixteen">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Photos/Virtual Tour Videos</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSixteen" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="form-outer2">
                                                                <div class="col-sm-12">
                                                                    <div class="btn-outer text-right">
                                                                        <button type="button" id="uploadPhotoVideo" class="green-btn">Click Here to Upload</button>
                                                                        <input id="photosVideos" type="file" name="photo_videos[]" accept="image/*, .mp4, .mkv, .avi" multiple style="display: none;">
                                                                        <button type="button" id="removePhotoVideo" class="orange-btn">Remove All Photos</button>
                                                                        <button type="button" id="savePhotoVideo" class="blue-btn">Save </button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="row" id="photo_video_uploads">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="grid-outer listinggridDiv">
                                                                        <div class="apx-table apxtable-bdr-none">
                                                                            <div class="table-responsive">
                                                                                <table id="propertPhotovideos-table" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="btn-outer text-right">

                                                                    <button class="blue-btn" id="savennextphoto">Next </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- File Library -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseSeventeen" class="tabcheckvalidation" id="17collapseSeventeen">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> File Library</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseSeventeen" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="form-outer2">
                                                                <div class="col-sm-12 text-right">
                                                                    <button type="button" id="add_libraray_file" class="green-btn">Click Here to Upload</button>
                                                                    <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                                    <button class="orange-btn" id="remove_library_file">Remove All Files</button>
                                                                    <button type="button" id="saveLibraryFiles" class="blue-btn">Save</button>
                                                                    <button type="button" class="grey-btn cancel-all">Cancel</button>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="row" id="file_library_uploads">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="grid-outer listinggridDiv">
                                                                <div class="apx-table apxtable-bdr-none">
                                                                    <div class="table-responsive">
                                                                        <table id="propertFileLibrary-table" class="table table-bordered">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 text-right">
                                                                <div class="btn-outer">
                                                                    <button class="blue-btn" id="savennextfilelib" value="">Next </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Notes -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseEighteen" class="tabcheckvalidation" id="18collapseEighteen">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Notes </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseEighteen" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <form action="" method="post" id="addPropertynotesForm">
                                                            <input type="hidden" name="id" id="notes_id" value="">
                                                            <div class="row">
                                                                <div class="form-outer2">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <div class="">
                                                                            <div class="div-full" id="addnoteshref">
                                                                                <a href="javascript:;"  class="active notes-button-bold">Add Notes</a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="div-full " id="addnotesopenDiv" style="display:none">
                                                                            <div class="notes_date_right_div">
                                                                            <textarea id="notesId" maxlength="500" class="form-control capital notes_date_right" name="notes"></textarea>
                                                                            </div>
                                                                            <input type="hidden" id="AddPropertynotesclick" value="">

                                                                            <div class="btn-outer text-right">
                                                                                <button class="blue-btn" id="AddPropertynotesButton">Save </button>
                                                                                <button rel="addPropertynotesForm" type="button" class="grey-btn clearFormReset">Clear</button>
                                                                                <button type="button" class="grey-btn cancel-all">Cancel </button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="grid-outer listinggridDiv">
                                                                        <div class="apx-table apxtable-bdr-none">
                                                                            <div class="table-responsive">
                                                                                <table id="notes-table" class="table table-bordered">
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 text-right">
                                                                <div class="btn-outer">
                                                                    <button class="blue-btn" id="AddPropertynotesButton1">Next </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Custom Fields -->
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" data_href="#collapseNinteen" class="tabcheckvalidation" id="19collapseNinteen">
                                                            <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Custom Fields</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseNinteen" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12">
                                                                    <div class="custom_field_html">
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right">
                                                                    <div class="btn-outer">
                                                                        <button type="button"  id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">New Custom Field</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="btn-outer">
                                                                        <button type="button" id="custom_field_submit" class="blue-btn">Submit</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>

    <!-- start custom field model -->
    <div class="container">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Custom Field</h4>
                    </div>
                    <div class="modal-body" style="height: 380px;">
                        <div class="form-outer col-sm-12">
                            <form id="custom_field">
                                <input type="hidden" id="customFieldModule" name="module" value="property">
                                <input type="hidden" name="id" id="custom_field_id" value="">
                                <div class="row custom_field_form">
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Field Name <em class="red-star">*</em></label>
                                        </div>
                                        <div class="col-sm-9 field_name">
                                            <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                            <span class="required error"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Data Type</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control data_type" id="data_type" name="data_type">
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="currency">Currency</option>
                                                <option value="percentage">Percentage</option>
                                                <option value="url">URL</option>
                                                <option value="date">Date</option>
                                                <option value="memo">Memo</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Default value</label>
                                        </div>
                                        <div class="col-sm-9 default_value">
                                            <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Required Field</label>
                                        </div>
                                        <div class="col-sm-9 is_required">
                                            <select class="form-control" name="is_required" id="is_required">
                                                <option value="1">Yes</option>
                                                <option value="0" selected="selected">No</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer">

                                    <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                    <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End custom field model -->
    <!-- Create charge model -->
    <div class="container">
        <div class="modal fade" id="chargeCodeModel" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Add Chat of Account</h4>
                    </div>
                    <div class="modal-body" style="height: 250px;">
                        <form name="add_charge_code" id="add_charge_code_form">
                            <div class="form-outer">
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>Charge Code <em class="red-star">*</em></label>
                                        <input name="charge_code" id="charge_charge_code" maxlength="10" placeholder="Eg: 1B" class="form-control only_letter" type="text">
                                        <span id="charge_codeErr"></span>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <label>Description</label>
                                        <textarea name="description" style="resize: none;" id="charge_description" rows="1" maxlength="250" placeholder="Eg: 1 Bedroom" class="form-control"></textarea>
                                        <span id="charge_descriptionErr"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-4">
                                        <label>Credit Account <em class="red-star">*</em></label>
                                        <select id="charge_credit_account" name="credit_account" class="fm-txt form-control"><option value="">Select</option><option value="1">1000-Cash in Bank-Operating</option><option value="2">1001-Fdsfds</option><option value="3">1010-Cash in Bank-Saving</option><option value="4">1020-Cash in Bank-Money Market</option><option value="5">1030-Security Deposit Bank Account</option><option value="6">1035-Payroll Bank Account</option><option value="7">1040-Petty Cash</option><option value="8">1100-Tenant Receivables</option><option value="9">1125-Tenant Receivable-Utilities</option><option value="10">1128-Tenant Receivable-Taxes</option><option value="11">1150-CAM Receivable</option><option value="12">1175-Accounts Receivable</option><option value="13">1200-Prepaid 4s</option><option value="14">1290-Other Current Assets</option><option value="15">1300-Furniture &amp; Fixtures</option><option value="16">1320-Equipment</option><option value="17">1325-Vehicles</option><option value="18">1350-Leasehold Improvements</option><option value="19">1400-Building</option><option value="20">1450-Land</option><option value="21">1520-Accum. Depr. Equipment</option><option value="22">1525-Accum. Depr. Vehicles</option><option value="23">1550-Accum. Depr. Building</option><option value="24">1575-Accum. Amort. Leasehold</option><option value="25">1600-Security Deposit</option><option value="26">1700-Intercompany Accounts Receivable</option><option value="27">2000-Accounts Payable</option><option value="28">2010-Security Deposit 2</option><option value="29">2020-Prepaid Rent 2</option><option value="30">2030-Notes Payable</option><option value="31">2100-Fed. 5 Tax Withheld</option><option value="32">2110-FICA Tax Withheld</option><option value="33">2115-Medicare Tax Withheld</option><option value="34">2120-State 5 Tax Withheld</option><option value="35">2125-SUTA Tax Withheld</option><option value="36">2130-Sales Tax Payable</option><option value="37">2140-5 Taxes Payable</option><option value="38">2295-Intercompany Accounts Payable</option><option value="39">2300-Contracts Payable</option><option value="40">2400-Mortgage Payable</option><option value="41">2500-Long Term Notes Payable</option><option value="42">2990-Other Long Term Liabilities</option><option value="43">3000-Retained Earnings</option><option value="44">3010-Owner Draw</option><option value="45">3020-Owner Contribution of Capital</option><option value="46">3999-Current Year Earnings/Loss</option><option value="47">4020-Parking Income</option><option value="48">4030-Application Fee Income</option><option value="49">4040-Cleaning Income</option><option value="50">4060-Laundry Income</option><option value="51">4080-Late Charge Income</option><option value="52">4110-Prepaid Rent Income</option><option value="53">4120-NSF Fee Income</option><option value="54">4125-7787892</option><option value="55">4140-Maint &amp; Repairs Income</option><option value="56">4160-Interest Income</option><option value="57">4170-Electricity Utility Income</option><option value="58">4171-HOA Dues</option><option value="59">4180-Water Utility Income</option><option value="60">4185-Sewer Utility Income</option><option value="61">4187-A/C Utility Income</option><option value="62">4188-Waste Disposal Income</option><option value="63">4190-Gas Utility Income</option><option value="64">4200-CAM Income</option><option value="65">4250-GST Tax Income</option><option value="66">4251-HST Tax Income</option><option value="67">4253-PST Tax Income</option><option value="68">4300-Intercompany Income</option><option value="69">4450-Less: Concessions</option><option value="70">4490-Other Income</option><option value="71">5000-Administrative Salaries</option><option value="72">5010-Management Fees</option><option value="73">5020-Manager Salaries</option><option value="74">5030-Clerical Salaries</option><option value="75">5040-Maintenance Salaries</option><option value="76">5050-Payroll Taxes</option><option value="77">5060-Employee Benefits</option><option value="78">5100-Advertising</option><option value="79">5110-Office Supplies</option><option value="80">5120-Dues &amp; Subscriptions</option><option value="81">5130-Postage &amp; Delivery</option><option value="82">5140-Telephone</option><option value="83">5190-Other Administrative 4s</option><option value="84">5200-Maintenance</option><option value="85">5210-Painting &amp; Decorating</option><option value="86">5220-Landscaping</option><option value="87">5230-Cleaning</option><option value="88">5250-Plumbing</option><option value="89">5260-Security</option><option value="90">5270-Building Supplies</option><option value="91">5290-Other Operating 4s</option><option value="92">5300-Electricity</option><option value="93">5310-Water &amp; Sanitation</option><option value="94">5320-Trash Collection</option><option value="95">5330-Natural Gas</option><option value="96">5337-Heating Oil Utility Income</option><option value="97">5390-Other Utilities/Cable</option><option value="98">5400-Travel &amp; Entertainment</option><option value="99">5410-Bank Charges</option><option value="100">5420-Interest</option><option value="101">5430-Other Interest</option><option value="102">5440-Legal &amp; Accounting</option><option value="103">5490-Data Processing</option><option value="104">5500-Real Estate Taxes</option><option value="105">5510-Fees &amp; Permits</option><option value="106">5520-Insurance</option><option value="107">5530-Depreciation 4</option><option value="108">6200-Repairs &amp; Maintenance</option><option value="109">6250-Parking Lot Sweeping</option><option value="110">6300-Supplies</option><option value="111">7000-Bad Debt Expense</option><option value="112">7990-Other Operating 4s</option><option value="113">8000-Non-Operating Income</option><option value="114">9000-Non-Operating 4s-Taxes</option></select>
                                        <span id="charge_credit_accountErr"></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Debit Account <em class="red-star">*</em></label>
                                        <select id="charge_debit_account" name="debit_account" class="fm-txt form-control"><option value="">Select</option><option value="1">1000-Cash in Bank-Operating</option><option value="2">1001-Fdsfds</option><option value="3">1010-Cash in Bank-Saving</option><option value="4">1020-Cash in Bank-Money Market</option><option value="5">1030-Security Deposit Bank Account</option><option value="6">1035-Payroll Bank Account</option><option value="7">1040-Petty Cash</option><option value="8">1100-Tenant Receivables</option><option value="9">1125-Tenant Receivable-Utilities</option><option value="10">1128-Tenant Receivable-Taxes</option><option value="11">1150-CAM Receivable</option><option value="12">1175-Accounts Receivable</option><option value="13">1200-Prepaid 4s</option><option value="14">1290-Other Current Assets</option><option value="15">1300-Furniture &amp; Fixtures</option><option value="16">1320-Equipment</option><option value="17">1325-Vehicles</option><option value="18">1350-Leasehold Improvements</option><option value="19">1400-Building</option><option value="20">1450-Land</option><option value="21">1520-Accum. Depr. Equipment</option><option value="22">1525-Accum. Depr. Vehicles</option><option value="23">1550-Accum. Depr. Building&lt;</option><option value="24">1575-Accum. Amort. Leasehold</option><option value="25">1600-Security Deposit</option><option value="26">1700-Intercompany Accounts Receivable</option><option value="27">2000-Accounts Payable</option><option value="28">2010-Security Deposit 2</option><option value="29">2020-Prepaid Rent 2</option><option value="30">2030-Notes Payable</option><option value="31">2030-Notes Payable</option><option value="32">2100-Fed. 5 Tax Withheld</option><option value="33">2110-FICA Tax Withheld</option><option value="34">2115-Medicare Tax Withheld</option><option value="35">2120-State 5 Tax Withheld</option><option value="36">2125-SUTA Tax Withheld</option><option value="37">2130-Sales Tax Payable</option><option value="38">2140-5 Taxes Payable</option><option value="39">2295-Intercompany Accounts Payable</option><option value="40">2300-Contracts Payable</option><option value="41">2400-Mortgage Payable</option><option value="42">2500-Long Term Notes Payable</option><option value="43">2990-Other Long Term Liabilities</option><option value="44">3000-Retained Earnings</option><option value="45">3010-Owner Draw</option><option value="46">3020-Owner Contribution of Capital</option><option value="47">3999-Current Year Earnings/Loss</option><option value="48">4020-Parking Income</option><option value="49">4030-Application Fee Income</option><option value="50">4040-Cleaning Income</option><option value="51">4060-Laundry Income</option><option value="52">4080-Late Charge Income</option><option value="53">4110-Prepaid Rent Income</option><option value="54">4120-NSF Fee Income</option><option value="55">4125-7787892</option><option value="56">4140-Maint &amp; Repairs Income</option><option value="57">4160-Interest Income</option><option value="58">4170-Electricity Utility Income</option><option value="59">4171-HOA Dues</option><option value="60">4180-Water Utility Income</option><option value="61">4185-Sewer Utility Income</option><option value="62">4187-A/C Utility Income</option><option value="63">4188-Waste Disposal Income</option><option value="64">4190-Gas Utility Income</option><option value="65">4200-CAM Income</option><option value="66">4250-GST Tax Income</option><option value="67">4251-HST Tax Income</option><option value="68">4253-PST Tax Income</option><option value="69">4300-Intercompany Income</option><option value="70">4450-Less: Concessions</option><option value="71">4490-Other Income</option><option value="72">5000-Administrative Salaries</option><option value="73">5010-Management Fees</option><option value="74">5020-Manager Salaries</option><option value="75">5030-Clerical Salaries</option><option value="76">5040-Maintenance Salaries</option><option value="77">5050-Payroll Taxes</option><option value="78">5060-Employee Benefits</option><option value="79">5100-Advertising</option><option value="80">5110-Office Supplies</option><option value="81">5120-Dues &amp; Subscriptions</option><option value="82">5130-Postage &amp; Delivery</option><option value="83">5140-Telephone</option><option value="84">5190-Other Administrative 4s</option><option value="85">5200-Maintenance</option><option value="86">5210-Painting &amp; Decorating</option><option value="87">5220-Landscaping</option><option value="88">5230-Cleaning</option><option value="89">5250-Plumbing</option><option value="90">5260-Security</option><option value="91">5270-Building Supplies</option><option value="92">5290-Other Operating 4s</option><option value="93">5300-Electricity</option><option value="94">5310-Water &amp; Sanitation</option><option value="95">5320-Trash Collection</option><option value="96">5330-Natural Gas</option><option value="97">5337-Heating Oil Utility Income</option><option value="98">5390-Other Utilities/Cable</option><option value="99">5400-Travel &amp; Entertainment</option><option value="100">5410-Bank Charges</option><option value="101">5420-Interest</option><option value="102">5430-Other Interest</option><option value="103">5440-Legal &amp; Accounting</option><option value="104">5490-Data Processing</option><option value="105">5500-Real Estate Taxes</option><option value="106">5510-Fees &amp; Permits</option><option value="107">5520-Insurance</option><option value="108">5530-Depreciation 4</option><option value="109">6200-Repairs &amp; Maintenance</option><option value="110">6250-Parking Lot Sweeping</option><option value="111">6300-Supplies</option><option value="112">7000-Bad Debt Expense</option><option value="113">7990-Other Operating 4s</option><option value="114">8000-Non-Operating Income</option><option value="115">9000-Non-Operating 4s-Taxes</option></select>
                                        <span id="charge_debit_accountErr"></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Status <em class="red-star">*</em></label>
                                        <select name="status" id="charge_status" class="fm-txt form-control">
                                            <option value="">Select</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                    <div class="btn-outer text-right">
                                        <button type="submit" class="blue-btn" id='saveChargeCode'>Save</button>
                                        <button rel="add_charge_code_form" type="button" class="clear-btn clearFormResetBank">Clear</button>
                                        <button type="button" class="grey-btn" id="cancelChargeCode">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container">
    <div class="modal fade" id="add_more_bank_account_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Bank Account</h4>
                </div>
                <div class="modal-body pad-none">
                    <div class="panel-body" style="border-color: transparent;padding: 15px;overflow: hidden !important;">
                        <div class="row">
                            <form class="form-outer" id="add_bank_account_form">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Portfolio <em class="red-star">*</em></label>

                                        <input  name="portfolio" id="portfolio_bank_name" class="form-control" type="text" disabled/>
                                        <input type="hidden" value="" id="portfolio_bank_name_hddn">
                                        <span id="portfolioErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Bank Name  <em class="red-star">*</em></label>
                                        <input name="bank_name" id="bank_name_rec" maxlength="100" placeholder="Eg: Wells Fargo"   class="form-control hide_copy" type="text"/>
                                        <span id="bank_nameErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Bank A/c No.  <em class="red-star">*</em></label>
                                        <input name="bank_account_number" id="bank_account_number" maxlength="20owner_percentowned" placeholder="Eg: 123A12345678"   class="form-control hide_copy" type="text"/>
                                        <span id="bank_account_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>FDI No.  <em class="red-star">*</em></label>
                                        <input name="fdi_number" id="fdi_number" maxlength="10" placeholder="Eg: 123A12345678"   class="form-control" type="text"/>
                                        <span id="fdi_numberErr" class="error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Branch Code <em class="red-star">*</em></label>
                                        <input name="branch_code" id="branch_code" maxlength="10" placeholder="Eg: AB12 12345 "   class="form-control" type="text"/>
                                        <span id="branch_codeErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Initial Amount(<?php echo $_SESSION[SESSION_DOMAIN]["default_currency_symbol"]?>)  <em class="red-star">*</em></label>
                                        <input name="initial_amount" id="initial_amount" placeholder="Eg: 1000"   class="form-control" type="text"/>
                                        <span id="initial_amountErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Last Used Check Number  <em class="red-star">*</em></label>
                                        <input name="last_used_check_number" id="last_used_check_number" maxlength="100" placeholder="Eg: 1234"   class="form-control" type="text"/>
                                        <span id="last_used_check_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Status <em class="red-star">*</em></label>
                                        <select id="status" name="status" class="fm-txt form-control">
                                            <option value="">Select</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                        <span id="statusErr" class="error"></span>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <label>Bank Routing  <em class="red-star">*</em></label>
                                        <input name="routing_number" id="routing_number_rec" maxlength="10" placeholder="Eg: 110000000"   class="form-control" type="text"/>
                                        <span id="routing_numberErr" class="error"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label></label>
                                        <div class="check-outer mg-lt-30 mb-15">
                                            <input name="is_default" id="is_default" type="checkbox"/>
                                            <label>Set as Default</label>
                                        </div>
                                    </div>
                                </div>


                                <div class="btn-outer text-right">
                                    <button type="submit" class="blue-btn" value="Save" >Save</button>
                                    <button rel="add_bank_account_form"  type="button" class="clear-btn clearFormResetBank">Clear</button>
                                    <button type="button" id="add_bank_account_cancel_btn" class="grey-btn">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- Create charge model end -->

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumericDecimal.js"></script>
    <script type="text/javascript">

        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
        var upload_url = "<?php echo SITE_URL; ?>";
        var property_unique_id = '';
        var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
        var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
        var manager_id = "";
        var attachGroup_id = "";
        var portfolio_default_id = '';

        $('#interest_rate').autoNumeric('init', {  vMax: '99.99' });
        // $('#owner_percentowned').autoNumeric('init', {  vMax: '99.99' });
    </script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/cloneCopy.js"></script>

    <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww&"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/PropertyValidate.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/owner.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/FeeManageValidate.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/KeyValidate.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/ManagementValidate.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/ManagementInfoValidate.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/TabValidates.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js" type="text/javascript"></script>
    <!-- file library js -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/photoVideos.js" type="text/javascript"></script>
    <!-- file library js -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/file_library.js" type="text/javascript"></script>
    <!-- custom field js -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/custom_fields.js" type="text/javascript"></script>
    <!-- custom field js -->
    <!-- Manage Charge Code -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/manageCharges.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/property/manageCharges.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js" type="text/javascript"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.easyui.css" rel="stylesheet">

    <script type="text/javascript">

        $(function () {
            $('.nav-tabs').responsiveTabs();
        });

        $(document).ready(function () {
            $(".slide-toggle").click(function () {
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function () {
            $(".slide-toggle2").click(function () {
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });


        $(document).on('click', '.tabcheckvalidation', function () {
            tabValidation();
        });


        function tabValidation() {
            var property_id_data = $('#property_unique_id').val();

            if (property_id_data == '') {
                $('.tabcheckvalidation').attr('href', '');
                bootbox.alert('Please Add General Information First!');
            } else {
                $(".tabcheckvalidation").each(function () {
                    var dataHref = $(this).attr('data_href');
                    $(this).attr('href', dataHref);
                });
            }
        }


        $(document).on('focusout', '.amount', function () {
            if ($(this).val() != '' && $(this).val().indexOf(".") == -1) {
                var bef = $(this).val().replace(/,/g, '');
                var value = numberWithCommas(bef) + '.00';
                $(this).val(value);
            } else {
                var bef = $(this).val().replace(/,/g, '');
                var before = bef.substr(0, bef.indexOf('.'));
                var after = bef.split('.')[1];
                $(this).val(numberWithCommas(before) + '.' + after.slice(0, 2));
            }
        });

        /**
         * If the letter is not digit in phone number then don't type anything.
         */
        $(document).on('keydown', '.phone_number', function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
                return false;
            } else {
                $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
            }
        });
        
        /**
         * If the letter is not digit in phone number then don't type anything.
         */
        $(document).on('keydown', '.phone_number_general', function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
                return false;
            } else {
                $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
            }
        });

        /**
         * If the letter is not digit in fax then don't type anything.
         */
        $(document).on('keydown', '.number_only', function (e) {
            if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
                if (e.which == 190 || e.which == 110) {
                    return true;
                }
                return false;
            }
        });

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>

    <!-- Jquery Starts -->
<div class="container">
    <div class="modal fade" id="chartofaccountmodal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Chat of Account</h4>
                </div>
                <div class="modal-body">
                    <form name="add_chart_account_form" id="add_chart_account_form_id">
                        <div class="panel-body" style="overflow: hidden;border: none;">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Account Type <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" name="account_type_id" id="account_type_id"></select>
                                            <span id="account_type_idErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Code <em class="red-star">*</em></label>
                                            <input id="account_code" name="account_code" maxlength="4" placeholder="Eg: 1110" class="form-control" type="text"/>
                                            <span id="account_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Name <em class="red-star">*</em></label>
                                            <input id="com_account_name" name="account_name" maxlength="50" placeholder="Eg: Property Trust" class="form-control" type="text"/>
                                            <span id="account_nameErr" class="error"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Reporting Code <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" id="reporting_code" name="reporting_code">
                                                <option value="">Select</option>
                                                <option value="1">C</option>
                                                <option value="2">L</option>
                                                <option value="3">M</option>
                                                <option value="4">B</option>
                                            </select>
                                            <span id="reporting_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Sub Account of</label>
                                            <select class="fm-txt form-control" id="sub_account" name="sub_account">
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Status</label>
                                            <select class="fm-txt form-control" id="status" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <select class="fm-txt form-control" id="posting_status" name="posting_status">
                                                <option value="1">Posting</option>
                                                <option value="0">Non Posting</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="btn-outer">
                                        <input type="hidden" value="" name="chart_account_edit_id" class="form-control" id="chart_account_edit_id"/>
                                        <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                        <button rel="add_chart_account_form_id" type="button" class="clear-btn clearFormResetBank">Clear</button>
                                        <button type="button"  class="grey-btn" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>


