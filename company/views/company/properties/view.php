<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/shorttermcss.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-pseudo-ripple.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-nao-calendar.css">

    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row ">

                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Property Module &gt;&gt; <span>Property Listing</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent property_listing_rightnav">
                        <div class="main-tabs apx-tabs">
                            <div class="property-status right-links-spacing">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label>Status</label>
                                        <select id="jqGridStatus" data-module="PROPERTY" class="fm-txt form-control jqGridStatusClass">
                                            <option value="all" selected="selected">All</option>
                                            <option value="2">All Advertised</option>
                                            <option value="3">All Archived</option>
                                            <option value="4">All For Sale</option>
                                            <option value="5">All Leased</option>
                                            <option value="6">All Resigned</option>
                                            <option value="7">All Vacant</option>
                                            <option value="8">All Short-Term Rentals</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-10 ">


                                        <!--Right Navigation Link Starts-->
                                        <div class="right-links-outer hide-links">
                                            <div class="right-links">
                                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                                            </div>
                                            <div id="RightMenu" class="box2">
                                                <h2>Properties</h2>

                                                <div class="list-group panel">

                                                    <!-- Two Ends-->
                                                    <a href="javascript:void(0)" class="add_new_property list-group-item list-group-item-success strong collapsed" >New Property</a>
                                                    <!-- Two Ends-->

                                                    <!-- Three Starts-->
                                                    <a  href="/MasterData/AddPropertyGroup" class="list-group-item list-group-item-success strong collapsed" >Property Group</a>
                                                    <!-- Three Ends-->

                                                    <!-- Four Starts-->
                                                    <a href="/MasterData/AddPortfolio" class="list-group-item list-group-item-success strong collapsed">New Portfolio</a>
                                                    <!-- Four Ends-->

                                                    <!-- Five Starts-->
                                                    <a href="javascript:void(0)"  class="list-group-item list-group-item-success strong collapsed">Process Management Fees</a>
                                                    <!-- Five Ends-->

                                                    <!-- Six Starts-->
                                                    <a id ="shorttermrentals" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>                                                <!-- Six Ends-->

                                                </div>
                                            </div>
                                        </div>
                                        <!--Right Navigation Link Ends-->

                                    </div>
                                    <div class="col-md-10 col-sm-12 text-right">
                                        <div class="atoz-outer2">
                                        <span class="apex-alphabets" id="apex-alphafilter" style="display:none;background:white;">
                                        <span class="AtoZ"></span></span><span class="AZ" id="AZ">A-Z</span>
                                            <span id="allAlphabet" style="cursor:pointer;">All</span>
                                        </div>
                                        <div class="btn-outer text-right property-view-btn">

                                            <a id="rentalUpload" class="blue-btn">Upload to Rentpath</a>
                                            <button class="blue-btn" id="export_sample_property_button">Download Sample</button>
                                            <button class="blue-btn" id="import_property_button">Import Property</button>
                                            <button class="blue-btn" id="export_property_button">Export Property</button>
                                            <a href="javascript:void(0)" class="add_new_property blue-btn">New Property</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default panel-d" style="display: none;" id="ImportProperty">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#panel-d" href="#collapseOne" aria-expanded="true" class=""><span class="pull-right glyphicon glyphicon-menu-up"></span> Import Property </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body">
                                        <div class="row">
                                            <form name="importPropertyStyleForm" id="importPropertyForm"  enctype="multipart/form-data" >
                                                <div class="form-outer">
                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                        <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                        <span class="error"></span>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12">
                                                    <div class="btn-outer">
                                                        <input type="submit" value="Submit" class="blue-btn" id="save_import_file"/>
                                                        <input  type="button" value="Cancel" class="grey-btn" id="import_property_cancel"/>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="accordion-grid">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="grid-outer">
                                                            <div class="apx-table">
                                                                <div class="table-responsive overflow-unset">
                                                                    <table id="property-table" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
        </section>
    </main>
</div>

<div class="container">
    <div class="modal fade" id="uploadXmlModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 640px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Rental
                    </h4>
                </div>
                <div class="modal-body">
                    <form name=uploadxmlForm id="uploadxmlForm">
                        <div class="row">
                            <div class="form-outer">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <label>XML link  </label>
                                        <input type="text" class="form-control" value="test url" name="xml_link" id="xml_link" disabled>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="btn-outer text-right">
                                        <button type="button" id="uploadXmlForm" class="blue-btn">Submit</button>
                                        <button type="button"  class="grey-btn cancelXml">Cancel</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- Wrapper Ends -->

<script>
    var module = {parent:'Properties',child:'',subChild:''};
    var permissions = <?php echo $_SESSION[SESSION_DOMAIN]['permission']; ?>;
    console.log('permissions',permissions);
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
</script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";


</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/propertyList.js"></script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<?php include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php"); ?>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww&"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/fullcal.js" integrity="sha384-vk5WoKIaW/vJyUAd9n/wmopsmNhiy+L2Z+SBxGYnUkunIxVxAv/UtMOhba/xskxh" crossorigin="anonymous"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-pseudo-ripple.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-nao-calendar.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/shortTermRental.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/subPayment.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/flag/flag.js"></script>