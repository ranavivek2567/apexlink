<?php
/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 6/03/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$edit_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';
$base_url=$_SERVER['SERVER_NAME'];
?>


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Unit Module >> <span>Unit View</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-sm-12">
                    <div class="unit_view_property_name">
                        <a id="property_link" ><span id="unit_property_name" class="font-weight-bold"></span></a>
                        /  <a id="building_link"><span id="building_span_name"></span></a>
                        /  <span id="unit_span_name"></span>
                    </div>
                </div>
                <input type="hidden" id="building_property_id" val=""/>
                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <!--- Right Quick Links ---->
                        <div class="right-links-outer hide-links">
                            <div class="right-links">
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <i class="fa fa-angle-right" aria-hidden="true"></i>
                            </div>
                            <div id="RightMenu" class="box2">
                                <h2>Unit Module</h2>
                                <div class="list-group panel">
                                    <!-- One Ends-->
                                    <a href="/Property/AddProperty" class="list-group-item list-group-item-success strong collapsed" >New Property</a>
                                    <!-- One Ends-->

                                    <!-- Two Starts-->
                                    <a id="new_unit_href" href="/Unit/AddUnit?id=<?php echo $_REQUEST['id']; ?>" class="list-group-item list-group-item-success strong collapsed" >New Unit</a>
                                    <!-- Two Ends-->

                                    <!-- Three Starts-->
                                    <a href="#" id="property_inspection_link" class="list-group-item list-group-item-success strong collapsed">Property Inspection</a>
                                    <!-- Three Ends-->
                                </div>
                            </div>
                        </div>
                        <!--- Right Quick Links ---->

                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" ><a href="#property" id="property_tab_link">Property</a></li>
                                <li role="presentation" ><a href="#building" id="building_tab_link">Building</a></li>
                                <li role="presentation" class="active"><a href="#unit" id="unit_tab_link" >Unit</a></li>
                            </ul>
                            <!-- Nav tabs -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="guest-cards">

                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Unit View <a onclick="goBack()" class="back" style="cursor: pointer;"><i class="fa fa-angle-double-left" aria-hidden="true" ></i> Back</a></h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="col-sm-3">
                                                <label >Property ID - <span class="random_property_id">THUD4S</span></label>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="owl-carousel owl-theme">


                                                </div>
                                            </div>

                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="fileLibrary" >
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Unit Information</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="detail-outer">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Type :</label>
                                                            <span class="unit_type_name"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Sq mtr :</label>
                                                            <span class="square_ft"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">No of Bedrooms :</label>
                                                            <span class="bedrooms_no">1</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">No of Bathrooms :</label>
                                                            <span class="bathrooms_no">1</span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Base Rent (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>) :</label>
                                                            <span class="base_rent"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Market Rent (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>) :</label>
                                                            <span class="market_rent"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Security Deposit (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] ?>) :</label>
                                                            <span class="security_deposit"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Amenities :</label>
                                                            <span class="amenities"></span>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <label class="text-right">Smoking Allowed :</label>
                                                            <span class="smoking_allowed"></span>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <label class="text-right">Pet Friendly :</label>
                                                            <span class="pet_friendly"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Key/AccessCode Info: (KeyCode:Key)</label>
                                                            <span class="key_access_codes"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Phone No :</label>
                                                            <span class="phone_number"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Fax No :</label>
                                                            <span class="fax_number"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">&nbsp;</div>
                                                    <div class="property-status">
                                                        <div class="col-sm-12">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">Last Renovation Date</th>
                                                                            <th scope="col">Last Renovation Time</th>
                                                                            <th scope="col">Last Renovation Description</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody class="renovation_detail">
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>8:57 AM</td>
                                                                            <td></td>
                                                                        </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr>
                                                                        <th scope="col">School District</th>
                                                                        <th scope="col">Code</th>
                                                                        <th scope="col">Notes</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="school_district_html">
                                                                    <tr>
                                                                        <td align="center" colspan="3">No Records Found</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="info">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                    <!-- Form Outer starts -->
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Description</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="description_html">N/A</div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="info">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                    <!-- Form Outer starts -->
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Notes</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="notes_html">N/A</div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="info">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                    <div class="form-outer form-outer2" href="#" id="keys">
                                        <div class="form-hdr">
                                            <h3>Keys</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class="grid-outer">
                                                                    <div class="apx-table listinggridDiv">
                                                                        <div class="table-responsive">
                                                                            <table id="UnitKeys-table" class="table table-bordered"></table>
                                                                            <table id="unit-track-keys" class="table table-bordered"></table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="keys">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Form Outer Ends -->
                                    <div class="form-outer" id="filelibrary">
                                        <div class="form-hdr">
                                            <h3>File Library</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class="grid-outer">
                                                                    <div class="table-responsive">
                                                                        <div class="apx-table listinggridDiv">
                                                                            <div class="table-responsive">
                                                                                <table id="file-library" class="table table-bordered"></table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="fileLibrary">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                    <!-- Form Outer Starts -->
                                    <div class="form-outer" id="flags">
                                        <div class="form-hdr">
                                            <h3>Flag Bank</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class="grid-outer">
                                                                    <div class="table-responsive">
                                                                        <div class="apx-table listinggridDiv">
                                                                            <div class="table-responsive">
                                                                                <table id="unitFlags-table" class="table table-bordered"></table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="flags" >
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->


                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Unit Complaints</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class="grid-outer">
                                                                    <div class="table-responsive">
                                                                        <div class="apx-table listinggridDiv">
                                                                            <div class="table-responsive">
                                                                                <table id="unitComplaints-table" class="table table-bordered"></table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="complaints">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->

                                    <!-- Form Outer starts -->
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Custom Fields</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="col-sm-7 custom_field_html_view_mode">
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_data="custom">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends -->
                                </div>
                            </div>
                            <!--tab Ends -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->


    <!-- SLider -->
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            autoplay:false,
            autoplayTimeout:1500,
            autoplayHoverPause:true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: true
                },
                1020: {
                    items: 2,
                    nav: true,
                    loop: true,
                    margin: 20
                },
                1199: {
                    items: 3,
                    nav: true,
                    loop: true,
                    margin: 20
                }
            }
        })
    })
    var upload_url = "<?php echo SITE_URL; ?>";
    function goBack() {
        window.history.back();
    }
    $('#properties_top').addClass('active');
</script>

<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/buildingView.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/viewUnit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<!--    <script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/buildingList.js"></script>-->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
