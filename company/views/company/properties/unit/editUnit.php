<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

$default_symbol = isset($_SESSION[SESSION_DOMAIN]['default_currency_symbol'])? $_SESSION[SESSION_DOMAIN]['default_currency_symbol']:'$';
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once(ROOT_URL . "/company/views/company/properties/unit/addUnitModal.php");
?>

    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- Top navigation end -->


        <section class="main-content">
            <div class="container-fluid" id="editUnitDetailPageId">
                <div class="row">

                    <div class="col-sm-12 bread-search-outer">
                        <div class="breadcrumb-outer">
                            Unit Module>> <span>Edit Unit</span>
                        </div>
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text" />
                        </div>
                    </div>
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>Unit Module</h2>
                            <div class="list-group panel">
                                <a href="javascript:void(0)" class="PropertyUnitUrl list-group-item list-group-item-success strong">New Property</a>
                                <a href="javascript:void(0)" class="NewUnitUrl list-group-item list-group-item-success strong">New Unit</a>
                                <a href="javascript:void(0)" class="PropertyInspectionUrl list-group-item list-group-item-success strong">Property Inspection</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 hidden_ids">

                        <div class="content-section">
                            <!--Tabs Starts -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->

                                <div class="col-sm-12">
                                    <div class="add-hdr-links">
                                        <ul>
                                            <li>
                                                <a href="javascript:;">
                                                    <label class="icon1"></label>
                                                    Add Property
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <label class="icon2"></label>
                                                    Add Building
                                                </a>
                                            </li>
                                            <li class="active">
                                                <a href="javascript:;">
                                                    <label class="icon3"></label>
                                                    Add Units
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                        <div class="accordion-form">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <form method="post" action="" id="addUnitForm" enctype="multipart/form-data">
                                                        <input type="hidden" name="property_unit_id" id="property_unit_id" value="">
                                                        <div class="panel-group" id="accordion">

                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Add Unit</a> <a class="back" href="javascript:;" onclick="goBack()"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                                    </h4>
                                                                </div>
                                                                <div>
                                                                    <div class="panel-body">
                                                                        <div class="row">

                                                                            <div class="div-full">
                                                                                <div class="name-id-greybox">
                                                                                    <div class="name-id-greybox-inner">
                                                                                        <label>
                                                                                            Property Name :
                                                                                        </label>
                                                                                        <span class="propertyName"> </span>
                                                                                    </div>
                                                                                    <div class="name-id-greybox-inner">
                                                                                        <label>
                                                                                            ID :
                                                                                        </label>
                                                                                        <span class="propertyID"> </span>
                                                                                        <input type="hidden" id="editPropID">
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                            <input type="hidden" name="editUnitId" value="<?php echo $_REQUEST['id'] ?>" id="editUnitId">

                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="check-availability">
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2 building_popup">
                                                                                        <label>Select Building <em class="red-star">*</em>
                                                                                            <a class="pop-add-icon" href="javascript:;">
                                                                                                <i class="add-more-div fa fa-plus-circle" id="addNewBuildingModalPlus" aria-hidden="true" data-backdrop="static" data-keyboard="false" ></i>
                                                                                            </a>
                                                                                        </label>
                                                                                        <select id="unit_building" class="unit_building form-control" name="building_id[]">
                                                                                            <option value="">Select</option>
                                                                                        </select>
                                                                                        <span class="building_id_0Err error red-star building_id_Err"></span>
                                                                                    </div>

                                                                                    <div class="floor-group col-xs-12 col-sm-4 col-md-2">
                                                                                        <label class="floor_individual">Floor No. <em class="red-star">*</em></label>
                                                                                        <label class="floor_group" style="display: none;">Floors <em class="red-star">*</em></label>
                                                                                        <input placeholder="Eg: 1" unit_prefix class="form-control numberInput" value="1" type="text" name="floor_no[]" />
                                                                                        <span class="floor_no_0Err error red-star floor_no_Err"></span>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                                                                        <label>Unit Prefix</label>
                                                                                        <input placeholder="Eg: A" class="form-control" name="unit_prefix[]" type="text" />
                                                                                        <span class="unit_prefix_0Err error red-star"></span>
                                                                                    </div>
                                                                                    <div class="unit-indval col-xs-12 col-sm-4 col-md-2">
                                                                                        <label>Unit Number <em class="red-star">*</em></label>
                                                                                        <input name="unit_no[]" placeholder="Eg: 100" class="form-control numberInput" value="1" type="text" />
                                                                                    </div>

                                                                                    <div class="group-checkAva col-xs-12 col-sm-4 col-md-6" style="display: none;">
                                                                                        <div class="row">
                                                                                            <div class="col-sm-4 group_unit">
                                                                                                <label>
                                                                                                    From <em class="red-star">*</em>
                                                                                                </label>
                                                                                                <span>
                                                                            <input type="text" class="form-control txtFrom numberInput" name="unit_from[]" maxlength="7" value="" placeholder="Eg: 100">
                                                                               <span class="unit_from_0Err error red-star unit_from_Err"></span>
                                                                        </span>
                                                                                            </div>
                                                                                            <div class="col-sm-4 group_unit">
                                                                                                <label>
                                                                                                    To <em class="red-star">*</em></label>
                                                                                                </label>
                                                                                                <span>
                                                                            <input type="text" class="form-control txtTo numberInput" name="unit_to[]" maxlength="7" value="" placeholder="Eg: 100">
                                                                           <span class="unit_to_0Err error red-star unit_to_Err"></span>
                                                                        </span>
                                                                                            </div>
                                                                                            <div class="col-xs-12 col-md-4 col-sm-4 group_unit btn-check-ava">
                                                                                                <label>&nbsp;</label>
                                                                                                <div class="text-green pull-left check_availability">
                                                                                                    <i class="fa fa-check" aria-hidden="true"></i> Check Availability
                                                                                                </div>

                                                                                                <div class="icon-text-text pull-left add_building_div">
                                                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add
                                                                                                </div>

                                                                                                <div class="text-red pull-left remove_building_div">
                                                                                                    <i class="fa fa-minus" aria-hidden="true"></i> Delete
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-xs-12 col-sm-4 col-md-3 unit_blueouter_before">
                                                                                    <label>Select Unit Type <em class="red-star">*</em> <i id="AddNewUnitTypeModalPlus2" class="fa fa-plus-circle"></i></label>
                                                                                    <span id="dynamic_unit_type1">
                                                                        <select class="form-control" id="unit_type" name="unitTypeID">
                                                                            <option value="">Select</option>
                                                                        </select>
                                                                     <span class="unitTypeIDErr error red-star"></span>
                                                                        <div class="add-popup" id='NewunitPopup'>
                                                                            <h4>Add New Unit Type</h4>
                                                                            <div class="add-popup-body">
                                                                                <div class="form-outer">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Unit Type<em class="red-star">*</em></label>
                                                                                        <input class="form-control customValidateUnitType" data_required="true" data_max="150" type="text" name="@unit_type"/>
                                                                                        <span class="customError required error"></span>
                                                                                    </div>
                                                                                    <div class="col-sm-12">
                                                                                        <label>Description</label>
                                                                                        <input class="form-control customValidateUnitType" type="text" data_max="200" name="@description"/>
                                                                                        <span class="customError required"></span>
                                                                                    </div>
                                                                                    <div class="btn-outer text-right">
                                                                                        <button type="button" class="blue-btn" id="NewunitPopupSave">Save</button>
                                                                                            <button type="button"  class="clear-btn clear_single_field">Clear</button>
                                                                                        <input type="button"  class="grey-btn" value='Cancel' />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Sq mtr</label>
                                                                                    <input placeholder="Eg: 200.00" name="square_ft" class="form-control numberInput" type="text" />
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>No. of Bedrooms <em class="red-star">*</em> </label>
                                                                                    <select class="form-control" name="bedrooms_no" id="bedrooms_no">
                                                                                        <option value="">Select</option>
                                                                                        <?php for($i=1; $i<=100; $i++){
                                                                                            echo '<option value="'.$i.'" text="'.$i.'">'.$i.'</option>';
                                                                                        } ?>
                                                                                    </select>
                                                                                    <span class="bedrooms_noErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>No.Of Bathrooms <em class="red-star">*</em></label>
                                                                                    <select class="form-control" name="bathrooms_no" id="bathrooms_no">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">1</option>
                                                                                        <option value="1½">1½</option>
                                                                                        <option value="2">2</option>
                                                                                        <option value="2½">2½</option>
                                                                                        <option value="3">3</option>
                                                                                        <option value="3½">3½</option>
                                                                                        <option value="4">4</option>
                                                                                        <option value="4½">4½</option>
                                                                                        <option value="5">5</option>
                                                                                        <option value="5½">5½</option>
                                                                                        <option value="6">6</option>
                                                                                        <option value="6½">6½</option>
                                                                                        <option value="7">7</option>
                                                                                        <option value="7½">7½</option>
                                                                                        <option value="8">8</option>
                                                                                        <option value="8½">8½</option>
                                                                                        <option value="9">9</option>
                                                                                        <option ="18" value="9½">9½</option>
                                                                                        <option value="10">10</option>
                                                                                        <option value="10½">10½</option>
                                                                                        <option value="11">11</option>
                                                                                        <option value="11½">11½</option>
                                                                                        <option value="12">12</option>
                                                                                        <option value="12½">12½</option>
                                                                                        <option value="13">13</option>
                                                                                        <option value="13½">13½</option>
                                                                                        <option value="14">14</option>
                                                                                        <option value="14½">14½</option>
                                                                                        <option value="15">15</option>
                                                                                        <option value="15½">15½</option>
                                                                                        <option value="16">16</option>
                                                                                        <option value="16½">16½</option>
                                                                                        <option value="17">17</option>
                                                                                        <option value="17½">17½</option>
                                                                                        <option value="18">18</option>
                                                                                        <option value="18½">18½</option>
                                                                                        <option value="19">19</option>
                                                                                        <option value="19½">19½</option>
                                                                                        <option value="20">20</option>
                                                                                        <option value="20½">20½</option>
                                                                                        <option value="21">21</option>
                                                                                        <option value="21½">21½</option>
                                                                                        <option value="22">22</option>
                                                                                        <option value="22½">22½</option>
                                                                                        <option value="23">23</option>
                                                                                        <option value="23½">23½</option>
                                                                                        <option value="24">24</option>
                                                                                        <option value="24½">24½</option>
                                                                                        <option value="25">25</option>
                                                                                        <option value="25½">25½</option>
                                                                                        <option value="26">26</option>
                                                                                        <option value="26½">26½</option>
                                                                                        <option value="27">27</option>
                                                                                        <option value="27½">27½</option>
                                                                                        <option value="28">28</option>
                                                                                        <option value="28½">28½</option>
                                                                                        <option value="29">29</option>
                                                                                        <option value="29½">29½</option>
                                                                                        <option value="30">30</option>
                                                                                        <option value="30½">30½</option>
                                                                                        <option value="31">31</option>
                                                                                        <option value="31½">31½</option>
                                                                                        <option value="32">32</option>
                                                                                        <option value="32½">32½</option>
                                                                                        <option value="33">33</option>
                                                                                        <option value="33½">33½</option>
                                                                                        <option value="34">34</option>
                                                                                        <option value="34½">34½</option>
                                                                                        <option value="35">35</option>
                                                                                        <option value="35½">35½</option>
                                                                                        <option value="36">36</option>
                                                                                        <option value="36½">36½</option>
                                                                                        <option value="37">37</option>
                                                                                        <option value="37½">37½</option>
                                                                                        <option value="38">38</option>
                                                                                        <option value="38½">38½</option>
                                                                                        <option value="39">39</option>
                                                                                        <option value="39½">39½</option>
                                                                                        <option value="40">40</option>
                                                                                        <option value="40½">40½</option>
                                                                                        <option value="41">41</option>
                                                                                        <option value="41½">41½</option>
                                                                                        <option value="42">42</option>
                                                                                        <option value="42½">42½</option>
                                                                                        <option value="43">43</option>
                                                                                        <option value="43½">43½</option>
                                                                                        <option value="44">44</option>
                                                                                        <option value="44½">44½</option>
                                                                                        <option value="45">45</option>
                                                                                        <option value="45½">45½</option>
                                                                                        <option value="46">46</option>
                                                                                        <option value="46½">46½</option>
                                                                                        <option value="47">47</option>
                                                                                        <option value="47½">47½</option>
                                                                                        <option value="48">48</option>
                                                                                        <option value="48½">48½</option>
                                                                                        <option value="49">49</option>
                                                                                        <option value="49½">49½</option>
                                                                                        <option value="50">50</option>
                                                                                        <option value="50½">50½</option>
                                                                                        <option value="51">51</option>
                                                                                        <option value="51½">51½</option>
                                                                                        <option value="52">52</option>
                                                                                        <option value="52½">52½</option>
                                                                                        <option value="53">53</option>
                                                                                        <option value="53½">53½</option>
                                                                                        <option value="54">54</option>
                                                                                        <option value="54½">54½</option>
                                                                                        <option value="55">55</option>
                                                                                        <option value="55½">55½</option>
                                                                                        <option value="56">56</option>
                                                                                        <option value="56½">56½</option>
                                                                                        <option value="57">57</option>
                                                                                        <option value="57½">57½</option>
                                                                                        <option value="58">58</option>
                                                                                        <option value="58½">58½</option>
                                                                                        <option value="59">59</option>
                                                                                        <option value="59½">59½</option>
                                                                                        <option value="60">60</option>
                                                                                        <option value="60½">60½</option>
                                                                                        <option value="61">61</option>
                                                                                        <option value="61½">61½</option>
                                                                                        <option value="62">62</option>
                                                                                        <option value="62½">62½</option>
                                                                                        <option value="63">63</option>
                                                                                        <option value="63½">63½</option>
                                                                                        <option value="64">64</option>
                                                                                        <option value="64½">64½</option>
                                                                                        <option value="65">65</option>
                                                                                        <option value="65½">65½</option>
                                                                                        <option value="66">66</option>
                                                                                        <option value="66½">66½</option>
                                                                                        <option value="67">67</option>
                                                                                        <option value="67½">67½</option>
                                                                                        <option value="68">68</option>
                                                                                        <option value="68½">68½</option>
                                                                                        <option value="69">69</option>
                                                                                        <option value="69½">69½</option>
                                                                                        <option value="70">70</option>
                                                                                        <option value="70½">70½</option>
                                                                                        <option value="71">71</option>
                                                                                        <option value="71½">71½</option>
                                                                                        <option value="72">72</option>
                                                                                        <option value="72½">72½</option>
                                                                                        <option value="73">73</option>
                                                                                        <option value="73½">73½</option>
                                                                                        <option value="74">74</option>
                                                                                        <option value="74½">74½</option>
                                                                                        <option value="75">75</option>
                                                                                        <option value="75½">75½</option>
                                                                                        <option value="76">76</option>
                                                                                        <option value="76½">76½</option>
                                                                                        <option value="77">77</option>
                                                                                        <option value="77½">77½</option>
                                                                                        <option value="78">78</option>
                                                                                        <option value="78½">78½</option>
                                                                                        <option value="79">79</option>
                                                                                        <option value="79½">79½</option>
                                                                                        <option value="80">80</option>
                                                                                        <option value="80½">80½</option>
                                                                                        <option value="81">81</option>
                                                                                        <option value="81½">81½</option>
                                                                                        <option value="82">82</option>
                                                                                        <option value="82½">82½</option>
                                                                                        <option value="83">83</option>
                                                                                        <option value="83½">83½</option>
                                                                                        <option value="84">84</option>
                                                                                        <option value="84½">84½</option>
                                                                                        <option value="85">85</option>
                                                                                        <option value="85½">85½</option>
                                                                                        <option value="86">86</option>
                                                                                        <option value="86½">86½</option>
                                                                                        <option value="87">87</option>
                                                                                        <option value="87½">87½</option>
                                                                                        <option value="88">88</option>
                                                                                        <option value="88½">88½</option>
                                                                                        <option value="89">89</option>
                                                                                        <option value="89½">89½</option>
                                                                                        <option value="90">90</option>
                                                                                        <option value="90½">90½</option>
                                                                                        <option value="91">91</option>
                                                                                        <option value="91½">91½</option>
                                                                                        <option value="92">92</option>
                                                                                        <option value="92½">92½</option>
                                                                                        <option value="93">93</option>
                                                                                        <option value="93½">93½</option>
                                                                                        <option value="94">94</option>
                                                                                        <option value="94½">94½</option>
                                                                                        <option value="95">95</option>
                                                                                        <option value="95½">95½</option>
                                                                                        <option value="96">96</option>
                                                                                        <option value="96½">96½</option>
                                                                                        <option value="97">97</option>
                                                                                        <option value="97½">97½</option>
                                                                                        <option value="98">98</option>
                                                                                        <option value="98½">98½</option>
                                                                                        <option value="99">99</option>
                                                                                        <option value="99½">99½</option>
                                                                                        <option value="100">100</option>
                                                                                        <option value="100½">100½</option>
                                                                                        <option value="Other">Other</option>
                                                                                    </select>
                                                                                    <span class="bathrooms_noErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Base Rent (<?php echo $default_symbol ?>) <em class="red-star">*</em></label>
                                                                                    <input placeholder="Eg: 200.00" class="form-control numberFormatAjax" type="text" name="baseRent" id="baseRent"/>
                                                                                    <span class="baseRentErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Market Rent (<?php echo $default_symbol ?>) <em class="red-star">*</em></label>
                                                                                    <input class="form-control numberFormatAjax" type="text" id="market_rent" name="market_rent"/>
                                                                                    <span class="market_rentErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Security Deposit (<?php echo $default_symbol ?>)</label>
                                                                                    <input placeholder="Eg: 200.00" class="form-control numberFormatAjax" type="text" name="securityDeposit" id="securityDeposit"/>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Smoking Allowded</label>
                                                                                    <select placeholder="Eg: Yes" class="form-control" id="NonSmokingUnit" name="smoking_allowed">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">Yes</option>
                                                                                        <option value="0">No</option>
                                                                                    </select>
                                                                                </div>
                                                                                <!-- <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                     <label>Pet Friendly
                                                                                         <a class="pop-add-icon" href="javascript:;">
                                                                                             <i class="add-more-div fa fa-plus-circle" data-toggle="modal" data-target="#AddNewPetFriendlyModal" aria-hidden="true" data-backdrop="static" data-keyboard="false" ></i>
                                                                                         </a>
                                                                                     </label>
                                                                                     <select class="form-control"></select>
                                                                                 </div>-->
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Pet Friendly <a id='Newpet' class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <select class='form-control' id='pet_options' name='pet_friendly_id'></select>
                                                                                    <div class="add-popup" id='NewpetPopup'>
                                                                                        <h4>Add New Pet Friendly</h4>
                                                                                        <div class="add-popup-body">
                                                                                            <div class="form-outer">
                                                                                                <div class="col-sm-12">
                                                                                                    <label>Pet Friendly<em class="red-star">*</em></label>

                                                                                                    <input class="form-control customValidatePetFriendly" data_required="true" type="text" name='@pet_friendly'/>
                                                                                                    <span class="customError required" aria-required="true"></span>
                                                                                                </div>
                                                                                                <div class="btn-outer text-right">
                                                                                                    <button class="blue-btn" id="NewpetPopupSave">Save</button>
                                                                                                    <button type="button" class="clear-btn clear_single_field" >Clear</button>
                                                                                                    <input type="button"  class="grey-btn" value='Cancel' />
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-xs-12 col-sm-4 col-md-3 primary-tenant-phone-row">
                                                                                    <div>
                                                                                        <label>Phone Number <em class="red-star">*</em>
                                                                                        </label>
                                                                                        <!--<input type="text" name="unit_phn_no" placeholder="Phone Number" class="form-control add-input" maxlength="12"/>-->
                                                                                        <input name="phone_number[]" class="phone_number form-control  valid add-input" type="text" data-mask="000-00-0000" data-mask-reverse="true" autocomplete="off" maxlength="12" aria-invalid="false">
                                                                                        <span class="phone_number_0Err error red-star"></span>
                                                                                    </div>
                                                                                    <div class="phone-add-remove-row">
                                                                                        <span class="glyphicon glyphicon-remove-sign clone-add"></span>
                                                                                        <span class="glyphicon glyphicon-plus-sign clone-add"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Note </label>
                                                                                    <div class="notes_date_right_div">
                                                                                        <input placeholder="Eg: Note" type="text" class="form-control notes_date_right capital" name="building_unit_note"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3 primary-tenant-fax-row">
                                                                                    <div >
                                                                                        <label>Fax Number <em class="red-star">*</em>
                                                                                        </label>
                                                                                        <input class="form-control fax error add-input fax_number" type="text" maxlength="12" placeholder="Fax Number" fixedlength="12" id="fax" name="fax_number[]" value="" aria-invalid="true">
                                                                                        <span class="fax_number_0Err error red-star w-100 d-inline-block"></span>
                                                                                    </div>
                                                                                    <div class="fax-add-remove-row add-remove-unit-m " style="padding: 0px;float: left;">
                                                                                        <span class="glyphicon glyphicon-remove-sign clone-add"></span>
                                                                                        <span class="glyphicon glyphicon-plus-sign clone-add"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="schoolHTML"></div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <label>Status</label>
                                                                                    <span>
                                                                    <select name="building_unit_status" id="ddlStatus" class="form-control" style="cursor: pointer;">
                                                                        <option value="1">Vacant Available</option>
                                                                        <option value="2">Unrentable</option>
                                                                        <option value="4">Occupied</option>
                                                                        <option value="5">Notice Available</option>
                                                                        <option value="6">Vacant Rented</option>
                                                                        <option value="7">Notice Rented</option>
                                                                        <option value="8">Under Make Ready</option></select>
                                                                </span>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3 textarea-form">
                                                                                    <label>Add Notes</label>
                                                                                    <div class="notes_date_right_div">
                                                                                        <textarea id="txtNotes" placeholder="Eg: Add Notes" name="building_unit_notes" maxlength="5000" class="notes_date_right textarea form-control capital" rows="3" cols="40" style="resize: none;" spellcheck="true"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                                <div class="col-sm-2">
                                                                                    <label>Last Renovation</label>
                                                                                    <input class="form-control disabled_field" readonly placeholder="Eg: 01/01/2017" id="last_renovation_date" name="last_renovation_date" type="text" />
                                                                                </div>
                                                                                <div class="col-sm-2">
                                                                                    <label>Last Renovation Time</label>
                                                                                    <input class="form-control disabled_field" placeholder="Eg: 04:36 PM" readonly type="text" id="last_renovation_time" name="last_renovation_time" />
                                                                                </div>
                                                                                <div class="col-sm-4 textarea-form">
                                                                                    <label>Last Renovation Description </label>
                                                                                    <div class="notes_date_right_div">
                                                                                        <textarea class="form-control disabled_field notes_date_right last_renovation_de" readonly id="last_renovation_description" name="last_renovation_description"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                    <a href="#" id="newRenovation" class="unit-m-renovation-link">New Renovation</a>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                                <div class="col-xs-12 col-sm-4 col-md-3 key_optionsDivClass" id="key_optionsDiv">
                                                                                    <label>Key/Access Code Info. <a  class="pop-add-icon Newkey" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <select class='form-control add-input key_access_codes_info key_access_codes_list' id='key_options' name='key_access_codes_info[]'>
                                                                                    </select>
                                                                                    <div class="key_access_codeclass"  style="display: none;">

                                                                                        <textarea name="key_access_codes_desc[]" id='key_access_code' class="form-control add-input"></textarea>
                                                                                        <a id="NewkeyPlus" class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                                        <a style='display:none' id="NewkeyMinus" class="add-icon" href="javascript:;"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                    </div>


                                                                                    <div class="add-popup NewkeyPopup" id="NewkeyPopupid">
                                                                                        <h4>Add New Key Access Code</h4>
                                                                                        <div class="add-popup-body">
                                                                                            <div class="form-outer">
                                                                                                <div class="col-sm-12">
                                                                                                    <label>Key Access Code<em class="red-star">*</em></label>
                                                                                                    <input class="form-control keyAccessCode customValidateKeyAccessCode key-access-input" data_required="true" data_max="150" type="text" name='@key_code'/>
                                                                                                    <span class="customError required error"></span>
                                                                                                </div>

                                                                                                <div class="btn-outer text-right">
                                                                                                    <button type="button" class="blue-btn NewkeyPopupSave" data_id="" >Save</button>
                                                                                                    <button type="button" class="clear-btn clear_single_field" >Clear</button>
                                                                                                    <input type="button"  class="grey-btn NewkeyPopupCancel" value='Cancel'/>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                                <!--<div class="col-xs-12 col-sm-4 col-md-3 textarea-form">
                                                                                    <label>Amenities
                                                                                        <a class="pop-add-icon" href="javascript:;">
                                                                                            <i class="add-more-div fa fa-plus-circle" data-toggle="modal" data-target="#AddNewAmenityModal" aria-hidden="true" data-backdrop="static" data-keyboard="false" ></i>
                                                                                        </a>
                                                                                    </label>
                                                                                    <textarea class="form-control" type="text"></textarea>
                                                                                </div>-->

                                                                                <div class="col-xs-12 col-sm-4 col-md-6 mt-15 textarea-form">
                                                                                    <label>Amenities <a id='NewamenitiesOne' class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <div id="amenties_box" class="form-control amenties_boxNew"></div>
                                                                                    <div class="add-popup" id='NewamenitiesPopupOne'  style="width: 300px;">
                                                                                        <h4>Add New Amenity</h4>
                                                                                        <div class="add-popup-body">
                                                                                            <div class="form-outer">
                                                                                                <div class="col-sm-12">
                                                                                                    <label>New Amenity Code<em class="red-star">*</em></label>
                                                                                                    <input class="form-control customValidateAmenities" data_required="true" type="text" name="@code"/>
                                                                                                    <span id="codeErr" class="customError error required" aria-required="true"></span>
                                                                                                </div>
                                                                                                <div class="col-sm-12">
                                                                                                    <label> Amenity Name <em class="red-star">*</em></label>
                                                                                                    <input class="form-control customValidateAmenities" data_required="true"  type="text" name="@name"/>
                                                                                                    <span id="nameErr" class="customError error required" aria-required="true"></span>
                                                                                                </div>
                                                                                                <div class="btn-outer text-right">
                                                                                                    <button class="blue-btn" id="NewamenitiesPopupSaveOne">Save</button>
                                                                                                    <button type="button" class="clear-btn clear_single_field">Clear</button>
                                                                                                    <input type="button"  class="grey-btn" value='Cancel' />
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-xs-12 col-sm-4 col-md-6 mt-15 textarea-form">
                                                                                    <label>Description </label>
                                                                                    <div class="notes_date_right_div">
                                                                                        <textarea id="txtDescription" name="building_description" class="textarea4 notes_date_right form-control " maxlength="500" cols="40" rows="5" style="resize: none;" spellcheck="true"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix">
                                                                            </div>

                                                                            <div class="col-sm-12 col-sm-6">
                                                                                <div class="photo-upload">
                                                                                    <label>Photos</label>
                                                                                    <button type="button" id="uploadPhotoVideo" class="green-btn">Click Here to Upload</button>
                                                                                    <input id="photosVideos" type="file" name="photo_videos[]" accept="image/*, .mp4, .mkv, .avi" multiple style="display: none;">
                                                                                </div>
                                                                                <div class="row" id="photo_video_uploads">
                                                                                </div>
                                                                                <div class="row" id="photo_video_uploads">
                                                                                </div>
                                                                                <div class="grid-outer listinggridDiv">
                                                                                    <div class="apx-table unit-m-photo-table">
                                                                                        <div class="table-responsive">
                                                                                            <table id="unitPhotovideos-table" class="table table-bordered">
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-sm-12 col-sm-6">
                                                                                <div class="photo-upload">
                                                                                    <label>File Library</label>
                                                                                    <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                                                    <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                                                </div>
                                                                                <div class="row" id="file_library_uploads">
                                                                                </div>
                                                                                <div class="grid-outer listinggridDiv">
                                                                                    <div class="apx-table unit-m-photo-table">
                                                                                        <div class="table-responsive">
                                                                                            <table id="unitFileLibrary-table" class="table table-bordered">
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--Key Tracker starts-->
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion"> Key Tracker</a>
                                                                    </h4>
                                                                </div>
                                                                <div>
                                                                    <div class="panel-body">

                                                                        <div class="latefee-check-outer">
                                                                            <div class="col-sm-12 latefee-check-hdr">
                                                                                <div class="check-outer">
                                                                                    <input type="radio" class="radiobuttonCls" value="keyUnit" name="key" checked="checked"/>
                                                                                    <label class="blue-label">Add key</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12 latefee-check-hdr">
                                                                                <div class="check-outer">
                                                                                    <input type="radio"  class="radiobuttonCls2" value="trackUnit" name="key"/>
                                                                                    <label class="blue-label">Track Key</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="div-full keyFrstdiv">
                                                                            <a id="ancCreateNewKey" class="grid-link" style="cursor: pointer" target="_self">Add Key</a>
                                                                        </div>

                                                                        <div class="form-outer divNewKey" id="addNewKeyDiv" style="display: none;">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Key ID</label>
                                                                                <input type="text" id="key_tag" name="key_tag" placeholder="Key ID" class="form-control" />
                                                                                <span class="key_tagErr error red-star key_tag_holder"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Description</label>
                                                                                <input id="key_desc" type="text" name="key_desc" class="form-control" />
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Total Keys</label>
                                                                                <input id="total_keys" type="number" name="total_keys" class="form-control" />
                                                                                <input type="hidden" id="edit_id_hidden" class="form-control" disabled name="key_id" />
                                                                                <span class="total_keysErr error red-star total_keys_holder"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                                                <div class="btn-outer text-right">
                                                                                    <input type="button" class="updateTagKey blue-btn" name="update" value="Update">
                                                                                    <button type="button" class="clear-btn"  id="clearAddKeyForm">Clear </button>
                                                                                    <a class="grey-btn updateTagKeyCancel" style="cursor: pointer">Cancel </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="div-full trackFrstdiv" style="display: none;">
                                                                            <a id="ancCreateNewtracker" class="grid-link" style="cursor: pointer" target="_self">Track Key</a>
                                                                        </div>
                                                                        <div class="form-outer divNewtracker" id="addNewTrackKeyDiv" style="display: none;">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Name</label>
                                                                                <input placeholder="Key Tag" name="tag_key_name" type="text" class="form-control" />
                                                                                <span class="tag_keyErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Email</label>
                                                                                <input type="text" name="tag_key_email" class="form-control" />
                                                                                <span class="tag_key_emailErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Company Name</label>
                                                                                <input type="text" class="form-control" name="tag_key_company" />
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Phone #</label>
                                                                                <input name="tag_key_phone" class="phone_number form-control  valid add-input" type="text" data-mask="000-00-0000" data-mask-reverse="true" autocomplete="off" maxlength="12" aria-invalid="false">
                                                                            </div>

                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Address1</label>
                                                                                <input type="text" class="form-control" name="tag_key_address1" />
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Address2</label>
                                                                                <input type="text" class="form-control" name="tag_key_address2" />
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Address3</label>
                                                                                <input type="text" class="form-control" name="tag_key_address3" />
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Address4</label>
                                                                                <input type="text" class="form-control" name="tag_key_address4" />
                                                                            </div>

                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Key#</label>
                                                                                <input type="text" class="form-control" name="tag_key"/>
                                                                                <span class="tag_keyErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Key Quantity</label>
                                                                                <input type="text" class="form-control" name="tag_key_quantity" />
                                                                                <span class="tag_key_quantityErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Pick Up Date</label>
                                                                                <input class="form-control" placeholder="Eg: 01/01/2017" readonly id="tag_key_pick_date" name="tag_key_pick_date" type="text" />
                                                                                <span class="tag_key_pick_dateErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Pick Up Time</label>
                                                                                <input type="text" readonly class="form-control" name="tag_key_pick_time" />
                                                                                <span class="tag_key_pick_timeErr error red-star"></span>
                                                                            </div>

                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Return Date</label>
                                                                                <input class="form-control" placeholder="Eg: 01/01/2017" readonly id="tag_key_return_date" name="tag_key_return_date" type="text" />
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Return Time</label>
                                                                                <input type="text" readonly class="form-control" name="tag_key_return_time" />
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Key Designator</label>
                                                                                <input type="text" class="form-control" name="tag_key_designator" />
                                                                                <input type="hidden" id="edit_trackid_hidden" class="form-control" disabled name="key_track_id" />
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                                                <div class="btn-outer text-right">
                                                                                    <input type="button" class="updateTagKey blue-btn" name="update" value="Update">
                                                                                    <button type="button" class="grey-btn" id="clearAddTrackKey">Clear </button>
                                                                                    <a class="grey-btn updateTagKeyCancel" style="cursor: pointer">Cancel </a>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                        <div class="form-outer checkout_div" style="display: none; background: #f3f3f3; padding-top: 30px;">
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Key Holder</label>
                                                                                <input type="text" id="checkout_key_holder" name="checkout_key_holder" placeholder="Enter Tenant, Vendor, Owner or Staff member here" class="form-control" />
                                                                                <span class="checkout_key_holderErr error red-star checkout_key_holder"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Keys</label>
                                                                                <input id="checkout_keys" type="number" name="checkout_keys" class="form-control" placeholder="Number of Keys to check out" />
                                                                                <span class="checkout_keysErr error red-star checkout_keys"></span>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                <label>Description</label>
                                                                                <input id="checkout_description" type="text" name="checkout_description" placeholder="Enter Description here"  class="form-control" />
                                                                                <input type="hidden" id="checkout_id_hidden" class="form-control" name="key_id" />
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                                                <div class="btn-outer text-right">
                                                                                    <input type="button" class="chekoutSave blue-btn" id="chekoutSave" name="save" value="Save">
                                                                                    <button type="button" class="grey-btn">Clear </button>
                                                                                    <a class="grey-btn" id="checkout_cancel" style="cursor: pointer;">Cancel </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--  keys grid starts here  -->
                                                                        <div class="panel panel-default" id="add_key_grid" style="clear:both;">
                                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" >
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                        <div class="apx-table listinggridDiv">
                                                                                            <div class="table-responsive">
                                                                                                <table id="unit-keys" class="table table-bordered"></table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--  keys grid ends here  -->
                                                                        <div class="panel panel-default"  id="track_key_grid" style="clear: both;">
                                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                        <div class="apx-table listinggridDiv">
                                                                                            <div class="table-responsive">
                                                                                                <table id="unit-track-keys" class="table table-bordered"></table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <!-- flag start-->
                                                                        <div class="panel panel-default" id="flags">
                                                                            <div>
                                                                                <div class="panel-collapse">
                                                                                    <div class="panel-body" style="padding-bottom: 0!important;">
                                                                                        <div class="row">
                                                                                            <div class="form-outer" style="margin-bottom: 0;">
                                                                                                <div class="col-sm-12" style="    min-height: inherit;">
                                                                                                    <div class="property-status">
                                                                                                        <button type="button" id="new_flag" class="blue-btn pull-right" >New Flag</button>
                                                                                                    </div>
                                                                                                    <div class="row" id="flagFormDiv" style="display: none;">
                                                                                                        <div class="grey-box-add">
                                                                                                            <div class="form-outer">
                                                                                                                <input type="hidden" name="id" id="flag_id">
                                                                                                                <div class="col-xs-12 col-sm-3">
                                                                                                                    <label>Flagged By </label>
                                                                                                                    <input class="form-control" name="flag_by" id="flag_flag_by" placeholder="Flagged By" maxlength="100" type="text" value=""/>
                                                                                                                </div>
                                                                                                                <div class="col-xs-12 col-sm-3">
                                                                                                                    <label>Date</label>
                                                                                                                    <input class="form-control" name="date" id="flag_flag_date" readonly placeholder="Eg: <?php echo $current_date = date('m/d/Y'); ?>" type="text"/>
                                                                                                                </div>
                                                                                                                <div class="col-xs-12 col-sm-3">
                                                                                                                    <label>Flag Name </label>
                                                                                                                    <input class="form-control" name="flag_name" id="flag_flag_name" maxlength="100" placeholder="Please Enter the Name of this Flag" type="text"/>
                                                                                                                </div>
                                                                                                                <div class="col-xs-12 col-sm-3">
                                                                                                                    <label>Country Code</label>
                                                                                                                    <select class='form-control' name='country_code' id="flag_country_code">
                                                                                                                        <?php
                                                                                                                        $select;
                                                                                                                        foreach ($countryArray as $code => $country) {
                                                                                                                            $countryName = ucwords(strtolower($country["name"])); // Making it look good
                                                                                                                            echo "<option value='" . $code . "' " . (($code == 'US') ? "selected" : "") . ">" . $code . " - " . $countryName . " (+" . $country["code"] . ")</option>";
                                                                                                                        }
                                                                                                                        ?>
                                                                                                                    </select>
                                                                                                                </div>
                                                                                                                <div class="col-xs-12 col-sm-3">
                                                                                                                    <label>Phone Number</label>
                                                                                                                    <input class="form-control phone_number p-number-format" name="flag_phone_number" id="flag_phone_number" maxlength="12" placeholder="123-456-7890" type="text"/>
                                                                                                                </div>
                                                                                                                <div class="col-xs-12 col-sm-3">
                                                                                                                    <label>Flag Reason</label>
                                                                                                                    <input CLASS="form-control" name="flag_reason" id="flag_flag_reason" maxlength="100" placeholder="Flag Reason" type="text"/>
                                                                                                                </div>
                                                                                                                <div class="col-xs-12 col-sm-3">
                                                                                                                    <label>Flagged For</label>
                                                                                                                    <input class="form-control capital" readonly id="flagged_for" maxlength="100"  type="text"/>
                                                                                                                </div>
                                                                                                                <div class="col-xs-12 col-sm-3">
                                                                                                                    <label>Completed</label>
                                                                                                                    <select class='form-control' name='completed' id="completed">
                                                                                                                        <option value="0">No</option>
                                                                                                                        <option value="1">Yes</option>
                                                                                                                    </select>
                                                                                                                </div>
                                                                                                                <div class="col-xs-12 col-sm-6">
                                                                                                                    <label>Note</label>
                                                                                                                    <div class="notes_date_right_div">
                                                                                                                        <textarea class="form-control notes_date_right" name="flag_note" id="flag_note" placeholder="Flag Notes"></textarea>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-xs-12">
                                                                                                                <div class="btn-outer text-right">
                                                                                                                    <button type="button" class="blue-btn" type="submit" id="flagSaveBtnId">Save</button>
                                                                                                                    <button type="button" class="clear-btn" id="clearAddFlagForm">Clear</button>
                                                                                                                    <button type="button" class="grey-btn" type="button" id="flagCancel">Cancel</button>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="apx-table listinggridDiv">
                                                                            <div class="table-responsive">
                                                                                <table id="propertyFlag-table" class="table table-bordered"></table>
                                                                            </div>
                                                                        </div>
                                                                        <!-- flag ends-->
                                                                    </div>

                                                                </div>


                                                            </div>
                                                            <!--Key Tracker Ends-->

                                                            <!-- Complaints Starts -->
                                                            <div class="panel panel-default" id="complaints">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion">Unit Complaint</a>
                                                                    </h4>
                                                                </div>
                                                                <div>
                                                                    <div class="panel-body">
                                                                        <div class="property-status">
                                                                            <button type="button" id="new_complaint_button" class="blue-btn pull-right">New Complaint</button>
                                                                        </div>
                                                                        <div style="display:none" id="new_complaint">
                                                                            <div class="row">
                                                                                <input type="hidden" name="edit_complaint_id" id="edit_complaint_id" />
                                                                                <div class="col-sm-3">
                                                                                    <label>Complaint ID</label>
                                                                                    <input class="form-control" name="complaint_id" id="complaint_id"  placeholder="Eg: AB01234C " type="text" value="GKATCK">
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Complaint Date </label>
                                                                                    <input class="form-control" id="complaint_date" name="complaint_date" placeholder="24/6/1990" readonly>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Complaint Type <a id="complainttype" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                                    <select class='form-control' id='complaint_type_options' name='complaint_type_options'>
                                                                                        <option disabled selected value> -- select an option -- </option>
                                                                                    </select>
                                                                                    <div class="add-popup" id='ComplaintTypePopup'>
                                                                                        <h4>Add New Complaint Type</h4>
                                                                                        <div class="add-popup-body">
                                                                                            <div class="form-outer">
                                                                                                <div class="col-sm-12">
                                                                                                    <label>Complaint Type<em class="red-star">*</em></label>
                                                                                                    <input class="form-control capitalize_popup_input customValidateComplaint" data_required="true" placeholder="Complaint Type" type="text"  name='@complaint_type'/>
                                                                                                    <span class="customError required"></span>
                                                                                                </div>
                                                                                                <div class="btn-outer text-right">
                                                                                                    <button type="button"  class="blue-btn" id="NewpetComplaintSave">Save</button>
                                                                                                    <button type="button"  class="clear-btn clear_single_field" id="clearNewComplaint">Clear</button>
                                                                                                    <button type="button"  class="grey-btn" id="NewComplaintcancel" value='Cancel' >Cancel</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Complaint Notes</label>
                                                                                    <div class="notes_date_right_div">
                                                                                        <textarea class="form-control notes_date_right" placeholder="Notes"  id="complaint_note" name="complaint_note"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-3" id="other_notes_div" style="display:none">
                                                                                    <label>Other Notes</label>
                                                                                    <div class="notes_date_right_div">
                                                                                        <textarea class="form-control notes_date_right" placeholder="Other Notes"  id="other_notes" name="other_notes"></textarea>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="btn-outer text-right">
                                                                                <button type="button" class=" blue-btn" id="complaint_save">Save</button>
                                                                                <button type="button" class=" clear-btn" id="clear_complaint_save">Clear</button>
                                                                                <button type="button" class=" grey-btn" id="complaint_cancel">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="apx-table listinggridDiv" style="float: left;">
                                                                            <div class="table-responsive">
                                                                                <table id="unit-complaints" class="table table-bordered"></table>
                                                                            </div>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class=" blue-btn pull-right" id="print_email_button">Print/Email</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Complaints Ends -->

                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion">Custom Fields</a>
                                                                    </h4>
                                                                </div>
                                                                <div class="panel-collapse">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="collaps-subhdr">
                                                                                            <div class="custom_field_html">
                                                                                                No Custom Fields
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <div class="col-sm-12">
                                                                                        <div class="btn-outer text-right">
                                                                                            <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-12">
                                                <div class="btn-outer text-right">
                                                    <!--<button class="blue-btn">Save </button>-->
                                                    <input type="submit" class="blue-btn" name="submit" id="submit" value="Update">
                                                    <button type="button" class="clear-btn" id="clearUpdateUnitForm">Clear</button>
                                                    <a href="javascript:void(0)" id="editUnitCancel" class="grey-btn">Cancel </a>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>


                                </div>
                                <!--tab Ends -->

                            </div>

                        </div>
                        <!--tab Ends -->

                    </div>

                </div>
            </div>
        </section>
    </div>
    <!-- Wrapper Ends -->

    <script>
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
        var default_login_user_name = "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
        var default_login_user_phone_number = "<?php echo $_SESSION[SESSION_DOMAIN]['phone_number']; ?>";
        var upload_url = "<?php echo SITE_URL; ?>";
        $(document).ready(function () {
            setTimeout(function () {
                $('.capital').css("text-transform","none");

            },1000);
        });
    </script>
    <!-- Footer Ends -->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/addUnit.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/unitFileLibrary.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/unitPhotoVideos.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/editUnit.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/flag/flagValidation.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/flag/flag.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
    <script>
        $('#properties_top').addClass('active');
        $(function() {
            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive -->
        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive -->


        $(document).ready(function(){
            $('a.back').click(function() {
                parent.history.back();
                return false;
            });
            $(".slide-toggle").click(function(){
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function(){
            $(".slide-toggle2").click(function(){
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });



    </script>

    <script type="text/javascript">


        <!--- Accordians-->

        $(document).on('click','#uploadPhotoVideo',function(){
            $('#photosVideos').val('');
            $('#photosVideos').trigger('click');
        });

        $(document).on('click','#add_libraray_file',function(){
            $('#file_library').val('');
            $('#file_library').trigger('click');
        });

    </script>
    <!-- Jquery Starts -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>