<!--AddNewBuildingModal start-->
<div class="modal fade" id="AddNewBuildingModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form name="add_building_form" id="add_building_form" novalidate="novalidate">
                <div class="modal-header">
                    <h4 class="modal-title">Add Building</h4>
                </div>
                <div class="modal-body form-outer">
                    <div class="row">
                        <div class="add_property_data">
                            <div class="col-sm-3">
                                <label>
                                    Property <em class="red-star">*</em>
                                </label>
                                <span>
                                            <select id="property_id" name="property_id" style="margin-bottom: 0px;pointer-events: none;" class="form-control" readonly="">
                                                <option value="">Select Property</option>
                                            </select>
                                    <!--                                           <input type="hidden" value="" id="property_input" name="property_id">-->
                                        </span>
                            </div>
                            <div class="col-sm-3">
                                <label>
                                    Building ID <em class="red-star">*</em>
                                </label>
                                <span>
                                            <input type="text" maxlength="10" placeholder="Eg: AB01234C " value="" id="building_id" name="building_id" class="form-control valid" spellcheck="true">
                                             <span class="required_star" id="building_id_err"></span>
                                       </span>
                            </div>
                            <div class="col-sm-3">
                                <label>
                                    Legal Name
                                </label>
                                <span>
                                            <input type="text" id="legal_name1" maxlength="150" placeholder="Eg: The Fairmont Waterfront" class="form-control" spellcheck="true">
                                        </span>
                            </div>
                            <div class="col-sm-3">
                                <label>
                                    Building Name <em class="red-star">*</em>
                                </label>
                                <span>
                                            <input type="text" id="building_name" maxlength="150" name="building_name" placeholder="Eg: Aber B1" class="form-control" spellcheck="true">
                                            <span class="required_star" id="building_name_err"></span>
                                       </span>
                            </div>


                            <div class="col-sm-3">
                                <label>
                                    No. Of Units <em class="red-star">*</em>
                                </label>
                                <span>
                                            <input type="text" id="no_of_units" placeholder="Eg: 8" maxlength="4" name="no_of_units" class="form-control" value="1" spellcheck="true">
                                            <span class="required_star" id="no_of_units_err"></span>
                                        </span>
                            </div>
                            <div class="col-sm-3">
                                <label>
                                    Building Address <em class="red-star">*</em>
                                </label>
                                <span>
                                            <input type="text" id="address" maxlength="250" name="address" class="form-control" placeholder="Eg: 8 Avenue" spellcheck="true">
                                            <span class="required_star" id="address_err"></span>
                                        </span>
                            </div>
                            <div class="col-sm-3">
                                <label>
                                    Smoking Allowed
                                </label>
                                <span>
                                            <select placeholder="Eg: Yes" class="form-control" id="smoking_allowed" name="smoking_allowed">
                                                <option value="">Select</option>
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </span>
                            </div>
                            <div class="col-sm-3">
                                <label>
                                    Pet Friendly
                                </label>
                                <span>
                                            <select placeholder="Eg: Yes" class="form-control" id="pet_allowed_id" name="pet_friendly">
                                                <option value="">Select</option>
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </span>
                            </div>


                            <div class="col-sm-6 textarea-form">
                                <label>Amenities <a id='NewamenitiesTwo' class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                <div id="amenties_boxBuilding" class="form-control" style="height: 100px;min-height: 100px;overflow: auto;line-height: 36px;"></div>
                                <div class="add-popup" id='NewamenitiesPopupTwo' style="width: 250px;">
                                    <h4>Add New Amenity</h4>
                                    <div class="add-popup-body">
                                        <div class="form-outer">
                                            <div class="col-sm-12">
                                                <label>New Amenity Code<em class="red-star">*</em></label>
                                                <input class="form-control" type="text" name="@code"/>
                                                <span id="codeErr" class="codeErr error red-star"></span>
                                            </div>
                                            <div class="col-sm-12">
                                                <label> Amenity Name <em class="red-star">*</em></label>
                                                <input class="form-control" type="text" name="@name"/>
                                                <span id="nameErr" class="nameErr error red-star"></span>
                                            </div>
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn" id="NewamenitiesPopupSaveTwo">Save</button>
                                                <button type="button" id="clearNewAmenitiesTwo"  class="clear-btn clear_single_field">Clear</button>
                                                <button type="button"  class="grey-btn" value='Cancel' >Cancel</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6 textarea-form">
                                <label>Building Description </label>
                                <div class="notes_date_right_div">
                                    <textarea class="form-control notes_date_right" placeholder="Eg: This is a family friendly building with onsite laundry. Suites are newly renovated with balconies." name="description" id="description" rows="2"></textarea>
                                </div>
                            </div>


                            <div class="form-outer2">
                                <div class="col-sm-12 latefee-check-hdr ">
                                    <div class="check-outer">
                                        <input type="checkbox" id="no_seprate_unit_checkbox"/>
                                        <label class="blue-label">No Separate Unit</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 details"  id="no_seprate_unit" style="display: none;">
                                    <label>Details</label>
                                    <div class="check-outer" type="text">
                                        <div class="col-sm-3">
                                            <label>Select Unit Type <em class="red-star">*</em></label>
                                            <span id="dynamic_unit_type">
                                                        <select class="form-control" id="unit_type" name="unit_type">
                                                            <option value="">Select</option>
                                                        </select>
                                                    </span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Base Rent (<?php echo $default_symbol ?>) <em class="red-star">*</em></label>
                                            <input placeholder="Eg: 2000" class="form-control money number_only" type="text" id="base_rent" name="base_rent"  >
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Market Rent (<?php echo $default_symbol ?>) <em class="red-star">*</em></label>
                                            <input placeholder="Eg: 2000" class="form-control money number_only" type="text" name="market_rent" id="Pmarket_rent" value="">
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Security Deposit (<?php echo $default_symbol ?>) <em class="red-star">*</em></label>
                                            <input placeholder="Eg: 2000" class="form-control money number_only" type="text" id="security_deposit" name="security_deposit" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-outer text-right">
                        <input type="submit" class="blue-btn" value="Save">
                        <button type="button" class="grey-btn" id="cancelAddBuildingForm">Cancel</button>
                        <button type="button" class="clear-btn" id="clearAddBuildingForm">Clear</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--AddNewBuildingModal end-->

<!--AddNewUnitModal Start-->
<div class="container">
    <div class="modal fade" id="AddNewUnitModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Unit Type</h4>
                </div>
                <div class="modal-body  add-newunitcontent">
                    <div class="add_new_building_data">
                        <form id="AddNewUnitData" method="post" action="">
                            <div class="form-outer" >
                                <label class="control-label">
                                    Unit Type <em class="red-star">*</em>
                                </label>
                                <input type="text" maxlength="15" placeholder="Add New Unit Type" class="form-control" name="unit_type" id="unit_type" spellcheck="true">
                                <span class="required_star" id="unit_type_err"></span>
                            </div>
                            <div >
                                <label class="control-label">
                                    Description
                                </label>
                                <input type="text" maxlength="50" placeholder="Description" name="description" id="description" class="form-control" spellcheck="true">
                                <span class="required_star" id="description_err"></span>
                            </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="blue-btn" value="Save">
                    <a id="AddNewAmenityCancel2"><input type="button" class="grey-btn" value="Cancel" data-dismiss="modal"></a>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
<!--AddNewBuildingModal end-->

<!-- Add New Key Access Code Modal Start-->
<div class="container">
    <div class="modal fade" id="AddNewKeyAccessCodeModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Key Access Code</h4>
                </div>
                <form id='AddKeyAccessCodeForm' name="default-setting" method="POST">
                    <div class="modal-body">
                        <div class="form-outer">
                            <label>Key Access Code <em class="red-star">*</em></label>
                            <input placeholder="Add Key Access Code" class="form-control" name="code" id="access_code" type="text" value="" />
                            <span class="required_star" id="code_newerr"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit"  id='AddNewKeyAccessCodeButton' class="blue-btn" value="Save">
                        <a id="AddNewAmenityCancel4"><input type="button" class="grey-btn" value="Cancel" data-dismiss="modal"></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add New Key Access Code Modal End-->

<!--AddNewAmenityModal Start-->
<div class="container">
    <div class="modal fade" id="AddNewAmenityModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Amenity</h4>
                </div>
                <div class="modal-body">
                    <form id='AddAmenityCodeForm' name="default-setting" method="POST">
                        <div class="form-outer">
                            <label class="control-label">
                                New Amenity Code
                            </label>
                            <input type="text" maxlength="15" placeholder="Eg:- 12345 (Optional)" class="form-control input-sm clsCapitaliseChr" name="code" id="AmenityCode2" spellcheck="true">
                            <span class="required_star" id="code_err2"></span>
                        </div>
                        <div class="">
                            <label class="control-label">
                                Amenity Name <em class="red-star">*</em>
                            </label>
                            <input type="text" maxlength="50" placeholder="Add New Amenity Name" id="AmenityName2" class="form-control input-sm validate[required] clsCapitaliseChr" name="name" spellcheck="true">
                            <span class="required_star" id="name_err2"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="submit"  id='AddNewAmenityData' class="blue-btn" value="Save">
                    <a id="AddNewAmenityCancel3"><input type="button" class="grey-btn" value="Cancel" data-dismiss="modal"></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--AddNewAmenityModal end-->

<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="unit">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">
                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button type="button" class="clear-btn" id="clearCustomFieldModal">Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->



<!-- start Renovation field model -->

<div class="container">
    <div class="modal fade" id="renovationModal" role="dialog">
        <div class="modal-dialog modal-md unit-new-renovation-form">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add New Renovation</h4>
                </div>

                <div class="modal-body">
                    <div class="form-outer col-sm-12">
                        <form id="new_renovation_form">
                            <input type ="hidden" id="edit_unit_id" name="edit_unit_id" value="<?php echo $_REQUEST["id"]?>"  />
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-5">
                                        <label>New Renovation Date <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-7 field_name">
                                        <input class="form-control" placeholder="Eg: 01/01/2017" readonly type="text" maxlength="100" id="new_renovation_date" name="new_renovation_date" placeholder="">
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-5">
                                        <label>New Renovation Time </label>
                                    </div>
                                    <div class="col-sm-7 field_name">
                                        <input class="form-control " readonly placeholder="Eg: 04:36 PM" type="text" id="new_renovation_timeModal" name="new_renovation_time" />
                                    </div>
                                </div>

                                <div class="textarea-form">
                                    <div class="col-sm-5"><label>Last Renovation Description <em class="red-star">*</em></label></div>
                                    <div class="col-sm-7">
                                        <div class="notes_date_right_div">
                                            <textarea class="form-control notes_date_right"  id="new_renovation_description" name="new_renovation_description"></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="btn-outer text-right">
                                <button type="submit" class="blue-btn" id='saveRenovationField'>Save</button>
                                <button type="button" class="clear-btn" id='clearRenovationField'>Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="print_complaint" role="dialog">
        <div class="modal-dialog modal-md unit-new-renovation-form">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="blue-btn" id='email_complaint' onclick="sendComplaintsEmail('#modal-body-complaints')">Email</button>
                    <button type="button" class="blue-btn" id='print_complaints'  onclick="PrintElem('#modal-body-complaints')" onclick="window.print();">Print</button>
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body" id="modal-body-complaints">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->
<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="print_complaint" role="dialog">
        <div class="modal-dialog modal-md unit-new-renovation-form">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="blue-btn" id='email_complaint' onclick="sendComplaintsEmail('#modal-body-complaints')">Email</button>
                    <button type="button" class="blue-btn" id='print_complaints'  onclick="PrintElem('#modal-body-complaints')" onclick="window.print();">Print</button>
                    <button type="button" class="close" data-dismiss="modal">×</button>

                </div>
                <div class="modal-body" id="modal-body-complaints">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->


<div class="modal fade" id="PrintEnvelopeUnit" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <a class="close  " data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>
                <input type="button" class="blue-btn sasass pull-left" id="print_unit_enevelope" onclick="PrintElem('#print_envelope_content')" onclick="window.print();"  value="Print">
                <input type="hidden" id="announcementSys_id" value="">

            </div>
            <div class="modal-body" id="print_envelope_content">
                <div>
                    <div class="left-detail">
                        <ul>
                            <li class="padding-top bold mt-n1" id="company_name" >

                            </li>
                            <li class="padding-top bold mt-n1" id="address1">

                            </li>
                            <li class="padding-top bold mt-n1" id="address2">

                            </li>
                            <li class="padding-top bold mt-n1" id="address3">

                            </li >
                            <li class="padding-top bold mt-n1" id="address4">

                            </li>
                            <li class="padding-top bold mt-n1">
                                <span id="city" class="city"></span>, <span id="state" class="state" ></span> <span id="postal_code" class="postal_code"> </span>
                            </li>
                        </ul>
                    </div>
                    <div class="right-detail ">
                        <ul>
                            <li class="padding-top bold mt-n1" id="building_name" >

                            </li>
                            <li class="padding-top bold mt-n1" id="building_address">

                            </li>

                            <li class="padding-top bold mt-n1">
                                <span id="city" class="city"></span>, <span class="state" id="state" ></span> <span class="postal_code" id="postal_code"> </span>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>