<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
//include_once(ROOT_URL . "/company/views/company/properties/unit/addUnitModal.php");
?>

<div id="printSection"></div>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <main class="apxpg-main">
        <section class="main-content">

            <div class="container-fluid">
                <div class="row ">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Unit Module &gt;&gt; <span>Unit Listing</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent"">

                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>Unit Module</h2>

                            <div class="list-group panel">
                                <!-- One Ends-->
                                <a href="/Property/AddProperty" class="list-group-item list-group-item-success strong collapsed" >New Property</a>
                                <!-- One Ends-->

                                <!-- Two Starts-->
                                <a id="" href="/Unit/AddUnit?id=<?php echo $_REQUEST['id']; ?>" class="list-group-item list-group-item-success strong collapsed" >New Unit</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a href="#" id="property_inspection_link" class="list-group-item list-group-item-success strong collapsed">Property Inspection</a>
                                <!-- Three Ends-->
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->

                    <div class="main-tabs apx-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" ><a href="/Property/PropertyModules" id="property_tab_link">Property</a></li>
                            <li role="presentation" ><a href="#building" id="building_tab_link">Building</a></li>
                            <li role="presentation" class="active"><a href="#"   >Unit</a></li>
                        </ul>
                        <div class="property-status">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label></label>
                                    <select id="jqGridStatus" class="fm-txt form-control">
                                        <option value="All" selected="selected">All</option>
                                        <option value="1">Active</option>
                                        <option value="0">InActive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-grid">
                            <div class="accordion-outer">
                                <div class="bs-example">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                <div class="panel-body pad-none">
                                                    <div class="grid-outer">
                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table id="property-unit-table" class="table table-bordered"></table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
</script>
<script>
    $('#properties_top').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();

    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->

    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });
    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
</script>
<script>
    $(document).ready(function () {
        function getParameterByName( name ){
            var regexS = "[\\?&]"+name+"=([^&#]*)",
                regex = new RegExp( regexS ),
                results = regex.exec( window.location.search );
            if( results == null ){
                return "";
            } else{
                return decodeURIComponent(results[1].replace(/\+/g, " "));
            }
        }
        var prop_id =  getParameterByName('id');
        localStorage.setItem('prop_id',prop_id);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/flag/flag.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/unitList.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
