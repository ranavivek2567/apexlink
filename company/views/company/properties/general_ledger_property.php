<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/shorttermcss.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-pseudo-ripple.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-nao-calendar.css">
    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row ">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Property Module &gt;&gt; <span>General Ledger</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Start -->
                <div class="col-sm-12">
                    <div class="content-section">       
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-outer">
                                    <div class="form-hdr">
                                    <h3>General Ledger </h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-3">
                                                <label>Start Date <em class="red-star">*</em></label>
                                                <input type="text" name="start_date" id="cash_start_date" class="is_required form-control datefield calander start_date" readonly="readonly">
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label>End Date <em class="red-star">*</em></label>
                                                <input type="text" name="end_date" id="cash_end_date" class="form-control calander end_date" readonly="readonly">
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <div class="btn-outer">
                                                    <button class="blue-btn" id="rstbtn">Reset</button>
                                                </div>
                                            </div>
                                            <!-- 5-3-2020 Start -->
                                            <div class="col-sm-12">
                                                            <div class="content-section">       
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-outer">
                                                                            <div class="form-hdr">
                                                                                <h3>List of General Ledger </h3>
                                                                            </div>
                                                                            <div class="form-data no-padding-custom">
                                                                                <div class="row">
                                                                                    <div class="grid-outer">
                                                                                        <div class="table-responsive">
                                                                                            <div class="apx-table">
                                                                                                <div class="table-responsive">
                                                                                                    <table id="lostItem-table" class="table table-bordered">
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End -->

                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </section>
    </main>
</div>

<script>
// <---Start


// <-----Old Work---
var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    // var startdate = $.datepicker.formatDate(jsDateFomat, new Date(y, m, 1));
    // $('.calander').datepicker({
    //     yearRange: '1919:2019',
    //     changeMonth: true,
    //     changeYear: true,
    //     dateFormat: jsDateFomat,
    //     onSelect: function(dateText) {
    //     searchFilters();
    //     }
    // });   
    // var enddate = $.datepicker.formatDate(jsDateFomat, new Date(y, m + 1, 0));
    // $("#cash_start_date").val(startdate);
    // $("#cash_end_date").val(enddate);

    $(document).on('click' , '#rstbtn', function(){
        $('#cash_start_date').val('');
        $('#cash_end_date').val('');
        searchFilters();
    });
    // -----end--->
    $('#cash_start_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function (selectedDate) {
            $("#cash_end_date").datepicker("option", "minDate", selectedDate);
            searchFilters();
        }
    }).datepicker("setDate",  new Date(y, m, 1));

    $('#cash_end_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function (selectedDate) {
            $("#cash_start_date").datepicker("option", "maxDate", selectedDate);
            searchFilters();
        }
    }).datepicker("setDate",  new Date(y, m + 1, 0));


//     // $('.datefield').datepicker({
    //     yearRange: '1919:2019',
    //     changeMonth: true,
    //     changeYear: true,
    //     dateFormat: jsDateFomat
    // });   var date = $.datepicker.formatDate(jsDateFomat, new Date());
    // $(".datefield").val(date);

    // $(document).on('focus',".acquireDateclass,.expirationDateclass", function(){
    //     $(this).datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
    // });
    
    // var d2 = new Date();
    //     var month3=d2.getMonth() + 1;
    //     var month2 = d2.getMonth() ;
    //     var year2=d2.getFullYear();
    //     var firstDay = new Date(year2, month2, 1);
    //     var lastDay = new Date(year2, month3, 0);

    //     var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
    //     var week=(lastDay.getDay());
    //         week=(days[week]).substr(0,3);
    //         lastDay= (lastDay.getMonth()+1)+'/'+lastDay.getDate()+'/'+lastDay.getFullYear()+' '+'('+week+'.'+')';

    //     var week1=(firstDay.getDay());
    //             week1=(days[week1]).substr(0,3);
    //             firstDay= (firstDay.getMonth()+1)+'/'+firstDay.getDate()+'/'+firstDay.getFullYear()+' '+'('+week1+'.'+')';
       
    //     var week2=(d2.getDay());
    //     week2=(days[week2]).substr(0,3);
      
    //     d2 = (d2.getMonth()+1)+'/'+d2.getDate()+'/'+d2.getFullYear()+' '+'('+week2+'.'+')';
        
    //     $(".end_date").val(lastDay);
    //     $(".start_date").val(firstDay);
    //     $(".current_date").val(d2);
        
    //     // console.log(lastDay);
    //     // console.log(firstDay);
    //     // console.log(d2);

    //     //FromDate  Format
    //     var fromdate = new Date();
    //     fromdate.setFullYear(fromdate.getFullYear() - 1);
    //     var week3 = (fromdate.getDay());
    //     week3 = (days[week3].substr(0,3));
    //     fromdate = (fromdate.getMonth()+1)+'/'+fromdate.getDate()+'/'+fromdate.getFullYear()+' '+'('+week3+'.'+')';
    //     // console.log(fromdate);
    //     $(".from_date").val(fromdate);
// ---End

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";


</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/property/general_ledger_property.js"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>