<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']) && ($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url.'/Owner/Login');
}
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">

                        <!-- Tabs Start -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="contact-info-tab" role="presentation"><a href="#contact-info-tab" aria-controls="home" role="tab" data-toggle="tab">Contact Information</a></li>
                                <li class="payment-setting-tab" role="presentation"><a href="#payment-setting-tab" aria-controls="profile" role="tab" data-toggle="tab">Payment Settings</a></li>
                                <li class="billing-info-tab" role="presentation"><a href="#billing-info-tab" aria-controls="profile" role="tab" data-toggle="tab">Billing Information</a></li>
                                <li class="conversation-tab" role="presentation"><a href="#conversation-tab" aria-controls="profile" role="tab" data-toggle="tab">Conversation</a></li>
                                <li class="change-password-tab" role="presentation"><a href="#change-password-tab" aria-controls="profile" role="tab" data-toggle="tab">Change Password</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane" id="contact-info-tab">
                                    <form id="updateContactInfo">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Contact Information</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>Salutation</label>
                                                        <select class="form-control" name="salutation" id="salutation">
                                                            <option value="">Select</option>
                                                            <option value="Dr.">Dr.</option>
                                                            <option value="Mr.">Mr.</option>
                                                            <option value="Mrs.">Mrs.</option>
                                                            <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                            <option value="Ms.">Ms.</option>
                                                            <option value="Sir">Sir</option>
                                                            <option value="Madam">Madam</option>
                                                            <option value="Brother">Brother</option>
                                                            <option value="Sister">Sister</option>
                                                            <option value="Father">Father</option>
                                                            <option value="Mother">Mother</option>
                                                        </select>
                                                        <span class="salutationErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>First Name <em class="red-star">*</em></label>
                                                        <input class="form-control capital" name="first_name" placeholder="First Name"  maxlength="50"  id="first_name" type="text" />
                                                        <span class="first_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Middle Name</label>
                                                        <input class="form-control capital" name="middle_name" id="middle_name" maxlength="50" placeholder="Middle Name" type="text" />
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Last Name <em class="red-star">*</em></label>
                                                        <input class="form-control capital" placeholder="Last Name" name="last_name" maxlength="50" id="last_name" type="text" />
                                                        <span class="last_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3" style="display: none;">
                                                        <label>Maiden Name</label>
                                                        <input class="form-control capital" placeholder="Maiden Name" name="maiden_name" maxlength="50" id="maiden_name" type="text" />
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Nick Name</label>
                                                        <input class="form-control capital" placeholder="Nick Name" name="nick_name" maxlength="50" id="nick_name" type="text" />
                                                    </div>
                                                    <div class="form-outer">
                                                        <div class="col-sm-3">
                                                            <label>Zip/Postal Code</label>
                                                            <input class="form-control" type="text" id="zipcode" name="zipcode" placeholder="Eg: 35801">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label>Country </label>
                                                            <input class="form-control" type="text" id="country" name="country" placeholder="Eg: US">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label>State / Province</label>
                                                            <input class="form-control" type="text" id="state" name="state" placeholder="Eg: AL">
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label>City</label>
                                                            <input class="form-control" type="text" id="city" name="city" placeholder="Eg: Huntsville">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Address1</label>
                                                        <input class="form-control capital address_field" maxlength="100" type="text" id="address1" name="address1" placeholder="Eg: Street address 1">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Address2 </label>
                                                        <input class="form-control capital address_field" maxlength="100" type="text" id="address2" name="address2" placeholder="Eg: Street address 2">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Address3</label>
                                                        <input class="form-control capital address_field" maxlength="100" type="text" id="address3" name="address3" placeholder="Eg: Street address 3">
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Address4</label>
                                                        <input class="form-control capital address_field" type="text" maxlength="100" id="address4" name="address4" placeholder="Eg: Street address 4">
                                                    </div>
                                                    <div class="primary-owner-phone-row" id="primary-owner-phone-row-divId">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Phone Type</label>
                                                                <select class="form-control" name="phone_type[]" id="phone_type">
                                                                    <option value="">Select</option>
                                                                    <option value="1">mobile</option>
                                                                    <option value="2">work</option>
                                                                    <option value="3">Fax</option>
                                                                    <option value="4">Home</option>
                                                                    <option value="5">Other</option>
                                                                </select>
                                                            </div>

                                                            <div class="col-sm-3 col-md-3 work_extension_div" style="display: none;">
                                                                <label>Office/Work Extension <em class="red-star">*</em></label>
                                                                <input class="form-control phone_format" type="text" disabled id="work_phone_extension" name="work_phone_extension[]" placeholder="Eg: 154-175-4301">
                                                                <span class="work_phone_extensionErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Carrier <em class="red-star">*</em></label>
                                                                <select class="form-control" name="carrier[]"></select>
                                                                <span class="carrierErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-3 col-md-3 countycodediv">
                                                                <label>Country Code</label>
                                                                <select class="form-control" name="country_code[]" id="country_code">
                                                                    <option value="">Select</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3 ">
                                                                <label>Phone Number <em class="red-star">*</em></label>
                                                                <input class="form-control add-input phone_format" type="text" name="phone_number[]" placeholder="Phone Number">
                                                                <a class="add-icon add-icon-abs" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a class="add-icon add-icon-abs" href="javascript:;" ><i class="fa fa-times-circle red-star" aria-hidden="true" style="display:none"></i></a>
                                                                <span class="phone_numberErr error red-star"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <div class='multipleEmail' id="multiple_email_div_id" >
                                                            <label>Email <em class="red-star">*</em></label>
                                                            <input class="form-control add-input" type="text" placeholder="Email" name="email[]">
                                                            <a class="add-icon email-remove-sign add-icon-abs" href="javascript:;">
                                                                <i class="fa fa-times-circle red-star" aria-hidden="true"  style="display: none"></i>
                                                            </a>
                                                            <a class="add-icon email-plus-sign add-icon-abs" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends-->
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Emergency Contact Details</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class=row style='margin-bottom: 10px;'>
                                                    <div class="owner-emergency-contact" id="owner-emergency-contact_div_id">
                                                        <div class="col-sm-2">
                                                            <label>Emergency Contact Name</label>
                                                            <input placeholder="Emergency Contact Name" class="form-control capital" type="text" id="emergency" name="emergency_contact_name[]" maxlength="50">
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>Relation</label>
                                                            <select class="form-control" id="relationship"
                                                                    name="emergency_relation[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Brother</option>
                                                                <option value="2">Daughter</option>
                                                                <option value="3">Employer</option>
                                                                <option value="4">Father</option>
                                                                <option value="5">Friend</option>
                                                                <option value="6">Mentor</option>
                                                                <option value="7">Mother</option>
                                                                <option value="8">Neighbor</option>
                                                                <option value="9">Nephew</option>
                                                                <option value="10">Niece</option>
                                                                <option value="11">Owner</option>
                                                                <option value="12">Partner</option>
                                                                <option value="13">Sister</option>
                                                                <option value="14">Son</option>
                                                                <option value="15">Spouse</option>
                                                                <option value="16">Teacher</option>
                                                                <option value="17">Other</option>

                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2" id="otherRelationDiv" style="display: none;">
                                                            <label>Other Relation</label>
                                                            <input placeholder="Other Relation" class="form-control capital" type="text" id="emergency_other_relation" name="emergency_other_relation[]" maxlength="50">
                                                        </div>
                                                        <div class="col-sm-2 countycodediv">
                                                            <label>Country Code</label>
                                                            <select name="emergency_country[]" class="form-control emergencycountry">
                                                                <option value="0">Select</option>
                                                            </select>
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <label>Phone</label>
                                                            <input class="form-control" placeholder="Phone Number" type="text" id="phoneNumber" name="emergency_phone[]" maxlength="12">
                                                        </div>

                                                        <div class="col-sm-2">
                                                            <label>Email</label>
                                                            <input class="form-control add-input" placeholder="Email" type="text" id="email1" name="emergency_email[]" maxlength="50">
                                                            <a class="add-icon add-emergency-contant" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!-- Form Outer Ends-->
                                        <div class="btn-outer text-right">
                                            <button type="submit" class="blue-btn">Update</button>
                                            <button type="button" class="clear-btn" id="clearEditContactForm">Clear</button>
                                            <button type="button" id="cancel_contact_info" class="grey-btn">Cancel</button>
                                        </div>
                                    </form>
                                </div>

                                <div class="tab-pane" id="payment-setting-tab">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Payment Setting</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row divBankAccount">
                                                <div class="col-sm-11">
                                                    <div class="col-sm-3 apx-inline-popup">
                                                        <label>Account Name <a class="pop-add-icon add_owner_account_name" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <select name="account_name[]" class="form-control account_name">
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="account_name" style="display: none;">
                                                            <h4>Add New Account Name</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>Account Name <em class="red-star">*</em></label>
                                                                        <input class="form-control owner_account_name capital" type="text" placeholder="Add New Account Name">
                                                                        <span class="red-star" id="owner_account_name"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" id="ownerAccountNameBtn" class="blue-btn add_owner_account_name" data-table="owner_account_name" data-cell="account_name" data-class="owner_account_name" data-name="account_name[]">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Bank Acc. for ACH Transaction <a  data-backdrop="static" data-toggle="modal" id="chartAccountId"  data-target="#chartofaccountmodal" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                        <select class="form-control bankChartsOfAccounts" name='account_for_transaction[]' id='account_for_transaction'>
                                                            <option value=''>Select</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Bank Account Type </label>
                                                        <select class="form-control" name="bank_account_type[]">
                                                            <option value="">Select</option>
                                                            <option value="1" selected>Business Checking Account</option>
                                                            <option value="2">Personal Checking Account</option>
                                                            <option value="3">Business Savings Account</option>
                                                            <option value="4">Personal Savings Account</option>
                                                            <option value="5">Credit</option>
                                                            <option value="6">Undeposited Funds</option>
                                                            <option value="7">Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Bank Account Number</label>
                                                        <input placeholder="Bank Account Number" name="bank_account_number[]" class=" hide_copy form-control capital number_only" maxlength="16" type="text" />
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Bank Routing / Transit Number</label>
                                                        <input placeholder="Bank Routing / Transit Number" name="routing_transit_number[]" class="form-control capital" maxlength="16" type="text" />
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Bank Institution Name</label>
                                                        <input name="bank_institution_name[]" placeholder="Bank Institution Name" class="form-control capital" maxlength="100" type="text" />
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Bank Fraction Number</label>
                                                        <input name="bank_fraction_number[]" placeholder="Bank Fraction Number" class="form-control capital" maxlength="20" type="text" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <a class="add-icon add-chart-bank" href="javascript:;">
                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                    <a class="add-icon remove-chart-bank" style="display:none;"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 draw_payment_owner">
                                                <label>Draw Payment Method</label>
                                                <div class="check-outer">
                                                    <input type="radio" value="check" name="draw_payment_method" />
                                                    <label>Check</label>
                                                </div>
                                                <div class="check-outer">
                                                    <input type="radio" value="eft" name="draw_payment_method" checked/>
                                                    <label>EFT</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="billing-info-tab">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Billing Information</h3>
                                        </div>
                                        <form id="owner_billing_info_form_id">
                                            <div class="form-data">
                                                <div class="col-sm-12">
                                                    <input type="checkbox" name="billing_info_as_contact_info"  id="billing_info_as_contact_info_id" >
                                                    <b style="font-size: 13px">Same as Contact Info</b>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Billing Address 1</label>
                                                    <input class="form-control capital" maxlength="100" type="text" id="billing_address1" name="billing_address1" placeholder="Eg: Billing address 1">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Billing Address 2</label>
                                                    <input class="form-control capital" maxlength="100" type="text" id="billing_address2" name="billing_address2" placeholder="Eg: Billing address 2">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Billing Address 3</label>
                                                    <input class="form-control capital" maxlength="100" type="text" id="billing_address3" name="billing_address3" placeholder="Eg: Billing address 3">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Billing Address4</label>
                                                    <input class="form-control capital" type="text" maxlength="100" id="billing_address4" name="billing_address4" placeholder="Eg: Billing address 4">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Zip/Postal Code</label>
                                                    <input class="form-control" type="text" id="billing_zipcode" name="billing_zipcode" placeholder="Eg: 35801">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>City</label>
                                                    <input class="form-control" type="text" id="billing_city" name="billing_city" placeholder="Eg: Huntsville">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>State / Province</label>
                                                    <input class="form-control" type="text" id="billing_state" name="billing_state" placeholder="Eg: AL">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Billing Email </label>
                                                    <input class="form-control" type="text" id="billing_email" name="billing_email" placeholder="Eg: US">
                                                </div>
                                            </div>
                                            <div class="btn-outer text-right">
                                                <button type="submit" class="blue-btn" id="updateBillingInfoBtn">Update</button>
                                                <button type="button" id="clearBillingInfoForm" class="clear-btn">Clear</button>
                                                <button type="button" id="cancel_update_billing_info_btn" class="grey-btn">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- Fourth Tab Ends-->
                                <div class="tab-pane" id="conversation-tab">
                                    <!--<div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Conversation</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn" id="import_property_type_button">New Conversation</button>
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>-->
                                    <div class="tab-pane" id="">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Conversation</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="owner_conversation_div">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="conversation-outer">
                                                                <ul class="conversation_list">
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-sm-1 conversation-lt">
                                                                                <div class="img-outer">
                                                                                    <img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/profile-img.png">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-10 pad-none conversation-rt">
                                                                                <span class="dark_name">Kambo K(Admin) :</span>
                                                                                <span class="light_name">Sharma Aryan2 (Tenant)</span>
                                                                                <span class="dark_name">Personal Issues</span>
                                                                                <span class="black_text" >Fsdfsdsdsdsds</span>
                                                                                <div class="conversation-time clear">
                                                                                    <span class="black_text" >Sep 25, 2019 (Wed.)-</span>
                                                                                    <a class="light_name" >Comment</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1 pull-right">
                                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="btn-outer text-right">
                                                    <button class="blue-btn" id="add_owner_conversation_btn">New Conversation</button>
                                                    <hr/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="add_owner_conversation_div" style="display: none;">
                                            <div class="form-outer">
                                                <div class="form-hdr">
                                                    <h3> New Conversation</h3>
                                                </div>
                                                <div class="form-data">
                                                    <form id="add_owner_conversation_form_id">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Property Manager  <em class="red-star">*</em></label>
                                                                <select class="form-control select_mangers_option" name="selected_user_id"><option>Select</option></select>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Problem Category <em class="red-star">*</em>
                                                                    <a class="pop-add-icon selectProblemCategory" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                </label>
                                                                <select class="form-control problem_category" name="problem_category"></select>
                                                                <div class="add-popup" id="selectProblemCategoryPopup">
                                                                    <h4>Add Problem Category</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Add New Problem Category <em class="red-star">*</em></label>
                                                                                <input class="form-control problem_category_src customValidateGroup capital" maxlength="50" type="text" placeholder="Add New Problem Category" data_required="true" >
                                                                                <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                            </div>
                                                                            <div class="btn-outer text-right">
                                                                                <button type="button" class="blue-btn add_single1"  data-table="conversation_problem_category" data-cell="problem_category_name" data-class="problem_category_src" data-name="problem_category">Save</button>
                                                                                <button type="button" class="clear-btn" id="clearAddProblemCategoryForm" >Clear</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-6 clear">
                                                                <label>Description <em class="red-star">*</em></label>
                                                                <textarea rows="4" class="form-control capital" name="description"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="btn-outer top-marginCls text-right">
                                                            <button class="blue-btn add_form_submit_btn">Send</button>
                                                            <button type="button" class="clear-btn" id="clearAddConversationForm" >Clear</button>
                                                            <button id="cancel_add_owner_conversation"  type="button" class="grey-btn">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <!-- Fifth Tab Ends-->
                                <div class="tab-pane" id="change-password-tab">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Change Password</h3>
                                        </div>
                                        <form id="changePasswordFormId">
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Current Password <em class="red-star">*</em></label>
                                                        <input class="form-control" placeholder="Type in your current password" type="password" name="password" id="password">

                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>New Password <em class="red-star">*</em></label>
                                                        <input class="form-control" placeholder="Type in your new password" type="password" name="new_password" id="new_password">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Confirm New Password <em class="red-star">*</em></label>
                                                        <input class="form-control" placeholder="Please confirm your current password" type="password" id="confirm_new_password" name="confirm_new_password">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btn-outer text-right">
                                                <button type="submit" class="blue-btn">Save</button>
                                                <button type="button" class="clear-btn" id="clearChangePasswordForm">Clear</button>
                                                <button type="button" id="cancel_change_password" class="grey-btn">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- Sixth Tab Ends-->
                            </div>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
        </div>
    </section>

    <div id="imageModel" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-4" id="getImage1"></div>
                    <div class="col-sm-4" id="getImage2"></div>
                    <div class="col-sm-4" id="getImage3"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>



<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>var owner_id = <?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>;</script>
<script>var default_Image = "<?php echo COMPANY_SUBDOMAIN_URL ?>/images/profile-img.png";</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/intlTelInput.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerPortal/editAccountInfo.js"></script>

<script>
    $('#my_account_top').addClass('active');
</script>

</body>
</html>
