<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">
    <?php

    if (!isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']) && ($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'] == '')) {

        $url = SUBDOMAIN_URL;
        header('Location: ' . $url.'/Owner/Login');
    }
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">

                        <!-- Tabs Start -->
                        <div class="main-tabs">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <form id="generateProperty">
                                    <div role="tabpanel" class="tab-pane active" id="contact-info-tab">
                                        <div class="form-outer">

                                            <div class="form-hdr">
                                                <h3>Owner Statement</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="col-sm-3">
                                                    <label>Select Property</label>
                                                    <select class="form-control" id="owner_property">
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Start Date</label>
                                                    <input class="form-control startDateClass" readonly type="text" id="startDate" name="startDate[]" placeholder="Eg: 10/10/2014">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>End Date</label>
                                                    <input class="form-control endDateClass" readonly  type="text" id="endDate" name="endDate[]" placeholder="Eg: 10/10/2014">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label style="height: 15px;"></label>
                                                    <input type="submit" class="blue-btn" value="Generate Statement">
                                                </div>
                                                <div class="col-sm-12">
                                                    <input type="checkbox" name="consolidated_field">Consolidated Properties on one Statement
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Form Outer Ends-->
                                    </div>
                                    <!-- Sixth Tab Ends-->
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
        </div>
    </section>


</div>



<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>
    $(document).ready(function () {
        var owner_id = <?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>;

        var formData = new FormData();
        formData.append('action','ownerPropertyList');
        formData.append('class','ownerPropertyList');
        formData.append('owner_id',owner_id);
        $.ajax({
            url:'/viewAccountInfoAjax',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success"){
                    // debugger
                    $('#owner_property').html(response.data);
                } else if (response.status == 'error'){

                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

        $('#startDate').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat})
            .datepicker("setDate", new Date());

        $('#endDate').datepicker({yearRange: '-0:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, minDate:  new Date(),})
            .datepicker("setDate", new Date());
    });
</script>

<script>
    $('#statement_top').addClass('active');
    $(document).ready(function () {
       $('#generateProperty').on('submit',function (e) {
           e.preventDefault();

       })
    });
</script>

</body>
</html>
