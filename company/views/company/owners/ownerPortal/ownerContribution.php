<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
?>
<div class="popup-bg"></div>
<div id="wrapper">
    <?php

    if (!isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']) && ($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'] == '')) {

        $url = SUBDOMAIN_URL;
        header('Location: ' . $url.'/Owner/Login');
    }
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">

                        <!-- Tabs Start -->
                        <div class="main-tabs">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <form id="addFileOwner" enctype='multipart/form-data'>
                                    <div role="tabpanel" class="tab-pane active" id="contact-info-tab">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>List of Owner Contribution<a type="button" href="/Accounting/GenerateOwnerContributionPortal" style="padding: 8px 15px; margin-right: 20px;" class="blue-btn pull-right" id="next_btn_id">New Owner Contribution</a></h3>
                                            </div>

                                            <div class="form-data">
                                                <div class="row col-sm-10">
                                                    <div class="col-sm-3">
                                                        <label>Start Date</label>
                                                        <input class="form-control common_search_class" readonly type="text" id="start_date" name="start_date" >
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>End Date</label>
                                                        <input class="form-control common_search_class" readonly type="text" id="end_date" name="end_date" >
                                                    </div>
                                                </div>
                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="owner_contribution_table" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends-->
                                    </div>
                                    <!-- Sixth Tab Ends-->
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
        </div>
    </section>


</div>



<?php

include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<!-- Jquery Starts -->
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script>var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerPortal/ownerContributionPortal.js"></script>
<script type="text/javascript">
    $('#accounting_top').addClass('active');
    var owner_id="<?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script>

    propertyown(1);
    $('#owner_contribution_top').addClass('active');
    function propertyown(status) {
        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";

        var owner_id = <?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>;

        // var table = 'owner_property_owned';
        var table = 'general_property';
        var columns = ['Owner Name','Property Name','Account No.','COA','Amount'];
        var joins = [{table:'general_property',column:'id',primary:'property_id',on_table:'owner_property_owned',type:'JOIN'},{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['owner_property_owned.updated_at', 'general_property.property_name','owner_property_owned.property_percent_owned','general_property.deleted_at', 'general_property.address1', 'general_property.address2', 'general_property.address3', 'general_property.address4', 'general_property.manager_id', 'general_property.owner_id','owner_property_owned.updated_at'];
        var extra_where = [{column: 'user_id', value: owner_id, condition: '=',table:'owner_property_owned'}];
        var columns_options = [
            {name: 'Owner Name', title:false, index: 'address_list', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Property Name', index: 'property_name', width: 100, searchoptions: {sopt: conditions}, table: table, change_type: 'combine_column_line', extra_columns: ['address1', 'address2', 'address3', 'address4'], update_column: 'address_list',original_index: 'address1', classes: 'pointer'},
            {name: 'Account No.', index: 'building_exist', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'COA', index: 'unit_exist', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'Amount', index: 'property_type', width: 80, align: "center", searchoptions: {sopt: conditions}, table: 'company_property_type', classes: 'pointer'},
        ];
        var ignore_array = [];
        jQuery("#property-own").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",

            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                //data:unserialize(),
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'owner_property_owned.updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Owner Contribution",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: false, refresh:false, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }

    }

    function propertyUnitBuilding(cellValue, options, rowObject) {
        if(cellValue == ''){
            return '<span data_redirect="no" style="text-decoration: underline;">0</span>';
        } else {
            return '<span data_redirect="yes" style="text-decoration: underline;">' + cellValue + '</span>';
        }
    }

    function propertyStatus(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Active";
        else if (cellValue == '2')
            return "Active";
        else if (cellValue == '3')
            return "Archive";
        else if (cellValue == '4')
            return "Active";
        else if (cellValue == '5')
            return "Active";
        else if (cellValue == '6')
            return "Resign";
        else if (cellValue == '7')
            return "Active";
        else if (cellValue == '8')
            return "Active";
        else
            return '';
    }
    $('#owner_contribution_top').addClass('active');

</script>

</body>
</html>
