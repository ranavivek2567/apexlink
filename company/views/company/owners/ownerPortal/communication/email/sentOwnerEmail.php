<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
?>
<div id="wrapper">
    <?php

    if (!isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']) && ($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'] == '')) {

        $url = SUBDOMAIN_URL;
        header('Location: ' . $url.'/Owner/Login');
    }
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <!--                              <li role="presentation" class="active"><a href="#tenant-communi-one" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">Email</a></li>-->
                                <!--                              <li role="presentation" class=""><a href="#tenant-communi-two" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">Text Message</a></li>-->
                                <!--                              <li role="presentation" class=""><a href="#tenant-communi-four" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">Announcement</a></li>-->
                                <li role="presentation" class="active"><a href="/Owner/Communication/SentEmails">Email</a></li>
                                <li role="presentation"><a href="/Owner/Communication/TextMessage">Text Message</a></li>
                                <li role="presentation"><a href="/Owner/MyAccount/AllCompanyAdminAnnouncements">Announcement</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="property-status text-right">
                                    <a class="blue-btn" href="/Owner/Communication/InboxMails">Inbox</a>
                                    <a class="blue-btn" href="/Owner/Communication/DraftedMails">Drafts</a>
                                    <a class="blue-btn" href="/Owner/Communication/ComposeEmail">Compose Email</a>
                                </div>
                                <div role="tabpanel" class="tab-pane active" id="tenant-communi-one">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Sent Mails</h3>
                                        </div>
                                        <div class="form-data apx-table">
                                            <div class="table-responsive">
                                                <table id="sentownermails-table" class="table table-bordered"></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Tab One Ends -->

                                <!--                            <div role="tabpanel" class="tab-pane" id="tenant-communi-two">-->
                                <!--                              <div class="form-outer form-outer2">-->
                                <!--                                <div class="form-hdr">-->
                                <!--                                  <h3>Sent Messages</h3>-->
                                <!--                                </div>-->
                                <!--                                  <div class="form-data apx-table">-->
                                <!--                                      <div class="table-responsive">-->
                                <!--                                          <table id="sentvendormsg-table" class="table table-bordered"></table>-->
                                <!--                                      </div>-->
                                <!--                                  </div>-->
                                <!--                              </div>-->
                                <!--                            </div>-->
                                <!-- Tab Two Ends -->

                                <div role="tabpanel" class="tab-pane" id="tenant-communi-three">

                                </div>
                                <!-- Tab Two Ends -->

                                <div role="tabpanel" class="tab-pane" id="tenant-communi-four">

                                </div>
                                <!-- Tab Two Ends -->

                            </div>
                            <!--tab Ends -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="sentownerMailModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:800px;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: unset;">
                <a class="close closeAnnouncementmodal closeSystemAnnouncement" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i></a>

                <a  href="javascript:void(0)"><h4>Sent Mails </h4></a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <strong>From</strong>
                        <span class="modal_from"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>To</strong>
                        <span class="modal_to"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>Cc</strong>
                        <span class="modal_cc"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>Bcc</strong>
                        <span class="modal_bcc"></span>
                    </div>
                    <div class="col-sm-12">
                        <strong>Subject</strong>
                        <span class="modal_subject"></span>
                    </div>
<!--                    <div class="col-sm-12">-->
<!--                        <strong>Date</strong>-->
<!--                        <span class="modal_date"></span>-->
<!--                    </div>-->
                    <div class="col-sm-12"><span class="modal_message"></span></div>
                    <input type="hidden" id="compose_mail_id">
                    <div class="col-sm-12 text-right">
                        <a class="blue-btn forward-email-btn">Forward</a>
                        <a class="grey-btn" id="cancel_communication" href="javascript:void(0)">
                            Cancel
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Wrapper Ends -->


<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<!--        <script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/company/communication/email/common.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/owner-portal/email/sent-owner-email.js"></script>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/vendor-portal/layouts/vendor_footer.php");
?>

<script>
    $("#communication").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
        $("#communication").addClass("active");
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->
</script>

