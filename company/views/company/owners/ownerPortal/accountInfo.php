<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>
<?php

if (!isset($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']) && ($_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url.'/Owner/Login');
}
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_portal_header.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/owner_top_navigation.php");
?>
<div class="popup-bg"></div>
<style>
    .form-outer div[class^="col-"] {
        min-height: 75px;
    }
    label.error {
        color: red !important;
        font-size: 12px;
        font-weight: 500;
        width: 100% !important;
    }
</style>


<div id="wrapper">
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content-section">
                        <div class="form-outer form-outer2">
                            <div class="form-hdr">
                                <h3>Account Information </h3>
                            </div>
                            <div class="form-data">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Upload Picture</label>
                                        <div class="upload-logo">
                                            <div class="owner_image img-outer"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>
                                            <a href="javascript:;" id="cropImageTrigger">
                                                <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image
                                            </a>
                                        </div>
                                        <div class="image-editor">
                                            <input type="file" class="cropit-image-input" name="owner_image">
                                            <div class='cropItData' style="display: none;">
                                                <span class="closeimagepopupicon">X</span>
                                                <div class="cropit-preview"></div>
                                                <div class="image-size-label">Resize image</div>
                                                <input type="range" class="cropit-image-zoom-input">
                                                <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                <input type="button" class="export" value="Done" data-val="owner_image">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grey-detail-box">
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Name :</label>
                                            <span class="name"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Phone :</label>
                                            <span class="phone_number"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Email:</label>
                                            <span class="email"></span>
                                        </div>
                                    </div>
                                    <div class="grey-detail-box">
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Address 1 :</label>
                                            <span class="address1"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Address 2 :</label>
                                            <span class="address2"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Address 3 :</label>
                                            <span class="address3"></span>
                                        </div>
                                    </div>
                                    <div class="grey-detail-box">
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">Address 4 :</label>
                                            <span class="address4"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3">
                                            <label class="blue-label">State :</label>
                                            <span class="state"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Tabs Start -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#contact-info-tab" aria-controls="home" role="tab" data-toggle="tab">Contact Information</a></li>
                                <li role="presentation"><a href="#payment-setting-tab" aria-controls="profile" role="tab" data-toggle="tab">Payment Settings</a></li>
                                <li role="presentation"><a href="#billing-info-tab" aria-controls="profile" role="tab" data-toggle="tab">Billing Information</a></li>
                                <li role="presentation"><a href="#conversation-tab" aria-controls="profile" role="tab" data-toggle="tab">Conversation</a></li>
                                <li role="presentation"><a href="#change-password-tab" aria-controls="profile" role="tab" data-toggle="tab">Change Password</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="contact-info-tab">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Contact Information</h3>
                                        </div>
                                        <div class="form-data form-outer2">
                                            <div class="detail-outer">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Full Name :</label>
                                                            <span class="name"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Address 1 :</label>
                                                            <span class="address1"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Address 3 :</label>
                                                            <span class="address3"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Zip/Postal Code :</label>
                                                            <span class="zipcode"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">City :</label>
                                                            <span class="city"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Email :</label>
                                                            <span class="email"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Entity/Company :</label>
                                                            <span class="company_name"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Address 2 :</label>
                                                            <span class="address2"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Address4 :</label>
                                                            <span class="address4"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">State Province :</label>
                                                            <span class="state"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Phone :</label>
                                                            <span class="phone_number"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_tab="contact-info-tab">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends-->
                                    <div class="form-outer2">
                                        <div class="form-hdr">
                                            <h3>Emergency Contact Details</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="detail-outer emergency_detail_data">
                                                <div class=row style='margin-bottom: 10px;'>
                                                    <div class=col-sm-6>
                                                        <div class=col-xs-12>
                                                            <label class=text-right>Emergency Contact Name : </label>
                                                            <span class=emeregency_name></span>
                                                        </div>
                                                        <div class=col-xs-12>
                                                            <label class=text-right>Country Code :</label>
                                                            <span class=country_code> </span>
                                                        </div>
                                                        <div class=col-xs-12>
                                                            <label class=text-right>Emergency Contact Phone : </label>
                                                            <span class=emeregency_contact></span>
                                                        </div>
                                                    </div>
                                                    <div class=col-sm-6>
                                                        <div class=col-xs-12>
                                                            <label class=text-right>Emergency Contact Email : </label>
                                                            <span class=emeregency_email></span>
                                                        </div>
                                                        <div class=col-xs-12>
                                                            <label class=text-right>Emergency Contact Relation : </label>
                                                            <span class=emeregency_relation></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_tab="contact-info-tab">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Form Outer Ends-->
                                </div>

                                <div class="tab-pane" id="payment-setting-tab">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Online Payment Details</h3>
                                        </div>

                                        <div class="form-outer">

                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>Payment Method <em class="red-star">*</em></label>
                                                        <select class="form-control setType" id="payment_method">
                                                            <option value='1'>Credit Card/Debit Card</option>
                                                            <option value='2'>ACH</option>
                                                        </select>
                                                    </div>
<!--                                                    <div class="col-sm-3">-->
<!--                                                        <label>&nbsp;</label>-->
<!--                                                        <a class="verification-due" href="javascript:;">Verification Due</a>-->
<!--                                                    </div>-->
                                                </div>

                                                <div class="row cards">
                                                    <div class="cardDetails col-sm-12"></div>
                                                    <form id="addCards">
                                                        <div class="detail-outer ">
                                                            <div class="col-sm-5 clear">
                                                                <label class="black-label">Card Type<em class="red-star">*</em></label>
                                                                <img src="/company/images/card-payment.png" style="width: 190px;">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-5 clear">
                                                                <label class="black-label">Card Number <em class="red-star">*</em></label>
                                                                <span>
                                                                        <input type="hidden" id="company_user_id" value="<?php $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id'];?>">
                                                                <input class="form-control hide_copy" name="card_number"  type="text" maxlength="16" placeholder="1234 1234 1234 1234"/>
                                                                <em class="nicknameErr error red-star"></em>
                                                            </span>
                                                            </div>


                                                            <div class="col-sm-5 clear">
                                                                <label class="black-label">Expiration Date <em class="red-star">*</em></label>
                                                                <span>
                                                            <input class="form-control exp_date" name="exp_date"  type="text" placeholder="MM / YY" readonly/>
                                                            <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                            </div>

                                                            <div class="col-sm-5 clear">
                                                                <label class="black-label">CVC <em class="red-star">*</em></label>
                                                                <span>
                                                        <input class="form-control" name="cvc"  type="text" placeholder="CVC" maxlength="5"/>
                                                        <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                            </div>

                                                            <div class="col-sm-5 clear">
                                                                <label class="black-label">Cardholder Name <em class="red-star">*</em></label>
                                                                <span>
                                                            <input class="form-control" name="holder_name" type="text" placeholder="Cardholder Name"/>
                                                            <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                            </div>
                                                            <div class="col-sm-5 clear">
<!--                                                                <label class="black-label">Autopay </label>-->
<!--                                                                <span>-->
<!--                                                            <input class="auto_pay" name="auto_pay" type="checkbox" value="ON">-->
<!---->
<!--                                                        </span>-->
                                                            </div>



                                                        </div>
                                                        <div class="col-sm-12">
                                                            <input type="submit" class="blue-btn" value="Confirm">
                                                            <input type="button" class="blue-btn cancel" value="Cancel">
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row accounts" style="display:none;">
                                            <div class="accountDetails"></div>


                                        </div>




                                    </div>
                                </div>
                                <div class="tab-pane" id="billing-info-tab">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Billing Information</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="detail-outer">
                                                <div class="col-sm-12">
                                                    <input disabled type="checkbox" name="billing_info_as_contact_info" id="billing_info_as_contact_info"/>
                                                    <b style="font-size: 13px">Same as Contact Info</b>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Address 1 :</label>
                                                            <span class="billing_address1"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Address 3 :</label>
                                                            <span class="billing_address3"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Zip/Postal Code :</label>
                                                            <span class="billing_zipcode"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">City :</label>
                                                            <span class="billing_city"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Address 2 :</label>
                                                            <span class="billing_address2"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Address 4 :</label>
                                                            <span class="billing_address4"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">State Province :</label>
                                                            <span class="billing_state"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Billing Email :</label>
                                                            <span class="billing_email"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="edit-foot">
                                                <a href="javascript:;" class="edit_redirection" redirection_tab="billing-info-tab">
                                                    <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                    Edit
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Fourth Tab Ends-->
                                <div class="tab-pane" id="conversation-tab">
                                    <div class="form-outer form-outer2">
                                        <div class="form-hdr">
                                            <h3>Conversation</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="owner_conversation_div">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="conversation-outer">
                                                            <ul class="conversation_list">
                                                                <li>
                                                                    <div class="row">
                                                                        <div class="col-sm-1 conversation-lt">
                                                                            <div class="img-outer">
                                                                                <img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/profile-img.png">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-10 pad-none conversation-rt">
                                                                            <span class="dark_name">Kambo K(Admin) :</span>
                                                                            <span class="light_name">Sharma Aryan2 (Tenant)</span>
                                                                            <span class="dark_name">Personal Issues</span>
                                                                            <span class="black_text" >Fsdfsdsdsdsds</span>
                                                                            <div class="conversation-time clear">
                                                                                <span class="black_text" >Sep 25, 2019 (Wed.)-</span>
                                                                                <a class="light_name" >Comment</a>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-1 pull-right">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="btn-outer text-right">
                                                <button class="blue-btn" id="add_owner_conversation_btn">New Conversation</button>
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add_owner_conversation_div" style="display: none;">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3> New Conversation</h3>
                                            </div>
                                            <div class="form-data">
                                                <form id="add_owner_conversation_form_id">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Property Manager  <em class="red-star">*</em></label>
                                                            <select class="form-control select_mangers_option" name="selected_user_id"><option>Select</option></select>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                            <label>Problem Category <em class="red-star">*</em>
                                                                <a class="pop-add-icon selectProblemCategory" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a>
                                                            </label>
                                                            <select class="form-control problem_category" name="problem_category"></select>
                                                            <div class="add-popup" id="selectProblemCategoryPopup">
                                                                <h4>Add Problem Category</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>Add New Problem Category <em class="red-star">*</em></label>
                                                                            <input class="form-control problem_category_src customValidateGroup capital" maxlength="50" type="text" placeholder="Add New Problem Category" data_required="true" >
                                                                            <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                        </div>
                                                                        <div class="btn-outer text-right">
                                                                            <button type="button" class="blue-btn add_single1"  data-table="conversation_problem_category" data-cell="problem_category_name" data-class="problem_category_src" data-name="problem_category">Save</button>
                                                                            <button type="button" class="clear-btn" id="clearAddProblemCategoryForm" >Clear</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-4 col-md-6 clear">
                                                            <label>Description <em class="red-star">*</em></label>
                                                            <textarea rows="4" class="form-control capital" name="description"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="btn-outer top-marginCls text-right">
                                                        <button class="blue-btn add_form_submit_btn">Send</button>
                                                        <button type="button" class="clear-btn" id="clearAddConversationForm" >Clear</button>
                                                        <button id="cancel_add_owner_conversation"  type="button" class="grey-btn">Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Fifth Tab Ends-->
                                <div class="tab-pane" id="change-password-tab">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Change Password</h3>
                                        </div>
                                        <form id="changePasswordFormId">
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Current Password <em class="red-star">*</em></label>
                                                        <input class="form-control" placeholder="Type in your current password" type="password" name="password" id="password">

                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>New Password <em class="red-star">*</em></label>
                                                        <input class="form-control" placeholder="Type in your new password" type="password" name="new_password" id="new_password">
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Confirm New Password <em class="red-star">*</em></label>
                                                        <input class="form-control" placeholder="Please confirm your current password" type="password" id="confirm_new_password" name="confirm_new_password">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="btn-outer text-right">
                                                <button type="submit" class="blue-btn">Save</button>
                                                <button type="button" class="clear-btn" id="clearChangePasswordForm">Clear</button>
                                                <button type="button" id="cancel_change_password" class="grey-btn">Cancel</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- Sixth Tab Ends-->
                            </div>
                        </div>
                    </div>
                    <!--Tabs End -->
                </div>
            </div>
        </div>
    </section>

    <div id="imageModel" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <div class="col-sm-4" id="getImage1"></div>
                    <div class="col-sm-4" id="getImage2"></div>
                    <div class="col-sm-4" id="getImage3"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>



<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>

<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script>var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>var owner_id = "<?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['portal_id']; ?>";</script>
<script>var petStaticImage = '<img src="<?php //echo COMPANY_SUBDOMAIN_URL ?>///images/Petdummy.jpeg">';</script>
<script>var default_Image = "<?php echo COMPANY_SUBDOMAIN_URL ?>/images/profile-img.png";</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/intlTelInput.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerPortal/accountInfo.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerPortal/ownerPayment.js"></script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/plaidIntialisation.js"></script>
<script>
    $('#my_account_top').addClass('active');
</script>

</body>
</html>
