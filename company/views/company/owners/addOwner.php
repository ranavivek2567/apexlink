<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once(COMPANY_DIRECTORY_URL . "/views/company/owners/modals.php");
?>
<style>
    .row.custom_field_class input {
        width: 258px;
    }
    #selectProperty .add-popup { width: 110%; }
    #selectProperty #newAmenitiesPopup { width: 56%; }
    #selectProperty #newUnitTypePopup { width: 120%; }
</style>
<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="breadcrumb-outer">
                                    People >> <span>Add Owner</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <form id="addOwner" enctype='multipart/form-data'>

                            <input type="hidden"  name="ccard_number" value="" id="hidden_ccard_number">
                            <input type="hidden"  name="ccvv" value="" id="hidden_ccvv">
                            <input type="hidden"  name="cexpiry_year" value="" id="hidden_cexpiry_year">
                            <input type="hidden"  name="cexpiry_month" value="" id="hidden_cexpiry_month">

                            <input type="hidden"  name="hidden_acc_detail[]" value="" id="hidden_acc_detail">
                            <input type="hidden"  name="hidden_account_type" value="" id="hidden_account_type">
                            <input type="hidden"  name="hidden_account_id" value="" id="hidden_account_id">
                            <input type="hidden" name = "hidden_customer_id" id="hidden_customer_id">

                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                                    <li role="presentation" class="active"><a href="/People/Ownerlisting">Owners</a></li>
                                    <li role="presentation"><a href="/Vendor/Vendor">Vendors</a></li>
                                    <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                                    <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                                </ul>
                            </div>

                            <!--General Information tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        General Information <a class="back" href="javascript:;" onclick="goBack()"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-12">
                                        <div class="check-outer mb-15">
                                            <input type="checkbox" name="if_entity_name_display" id="if_entity_name_display">
                                            <label class="">Use Entity/Company Name as Display Name</label>
                                        </div>
                                        <label>Upload Picture</label>
                                        <div class="upload-logo">
                                            <div class="owner_image img-outer"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>
                                            <a href="javascript:;" id="cropImageTrigger">
                                                <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image
                                            </a>

                                        </div>
                                        <div class="image-editor">
                                            <input type="file" class="cropit-image-input" name="owner_image">
                                            <div class='cropItData' style="display: none;">
                                                <span class="closeimagepopupicon">X</span>
                                                <div class="cropit-preview"></div>
                                                <div class="image-size-label">Resize image</div>
                                                <input type="range" class="cropit-image-zoom-input">
                                                <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                <input type="button" class="export" value="Done" data-val="owner_image">
                                                <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                </a>
                                                <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i>
                                                </a>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3">
                                        <label>Salutation</label>
                                        <select class="form-control" name="salutation" id="salutation">
                                            <option value="">Select</option>
                                            <option value="Dr.">Dr.</option>
                                            <option value="Mr.">Mr.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Sir">Sir</option>
                                            <option value="Madam">Madam</option>
                                            <option value="Brother">Brother</option>
                                            <option value="Sister">Sister</option>
                                            <option value="Father">Father</option>
                                            <option value="Mother">Mother</option>
                                        </select>
                                        <span class="salutationErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capital" name="first_name" placeholder="First Name"  maxlength="50"  id="first_name" type="text" />
                                        <span class="first_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Middle Name</label>
                                        <input class="form-control capital" name="middle_name" id="middle_name" maxlength="50" placeholder="Middle Name" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capital" placeholder="Last Name" name="last_name" maxlength="50" id="last_name" type="text" />
                                        <span class="last_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-3" style="display: none;">
                                        <label>Maiden Name</label>
                                        <input class="form-control capital" placeholder="Maiden Name" name="maiden_name" maxlength="50" id="maiden_name" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Nick Name</label>
                                        <input class="form-control capital" placeholder="Nick Name" name="nick_name" maxlength="50" id="nick_name" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Gender</label>
                                        <select class="form-control" id="gender"  name="gender">
                                            <option value="">Select</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                            <option value="3">Prefer Not To Say</option>
                                            <option value="4">Others</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Entity/Company </label>
                                        <input class="form-control capital" placeholder="Entity/Company" name="company_name" maxlength="50" id="entity_company" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>D.O.B</label>
                                        <input class="form-control" type="text" readonly id="birth" name="dob" placeholder="D.O.B">
                                    </div>
                                    <div class="" id="ethnicity_to_ssn_div">
                                        <div class="col-sm-3 ">
                                            <label>Ethnicity
                                                <a class="pop-add-icon selectPropertyEthnicity" href="javascript:;">
                                                    <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control" name="ethncity">
                                                <option value="0">Select</option>
                                                <option value="1">Male</option>
                                                <option value="2">Female</option>
                                            </select>
                                            <div class="add-popup" id="selectPropertyEthnicity1">
                                                <h4>Add New Ethnicity</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>New Ethnicity <em class="red-star">*</em></label>
                                                            <input class="form-control ethnicity_src customValidateGroup capital" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                            <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethncity">Save</button>
                                                            <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                            <input type="button" class="grey-btn" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Marital Status
                                                <a class="pop-add-icon selectPropertyMaritalStatus" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control" name="maritalStatus">
                                                <option value="0">Select</option>
                                            </select>
                                            <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                <h4>Add New Marital Status</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>New Marital Status <em class="red-star">*</em></label>
                                                            <input class="form-control marital_status maritalstatus_src capital" name="marital_status" type="text" placeholder="Add New Marital Status">
                                                            <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                            <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                            <input type="button" class="grey-btn" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Hobbies
                                                <a class="pop-add-icon selectPropertyHobbies" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control" name="hobbies[]" multiple>
                                                <option value="0">Select</option>
                                            </select>
                                            <div class="add-popup" id="selectPropertyHobbies1">
                                                <h4>Add New Hobbies</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>New Hobbies <em class="red-star">*</em></label>
                                                            <input class="form-control hobbies_src capital" type="text" placeholder="Add New Hobbies" data_required="true">
                                                            <span class="red-star required customError" id="hobbies_src"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="hobbies">Save</button>
                                                            <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                            <input type="button" class="grey-btn" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Veteran Status
                                                <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control veteran_status" name="veteranStatus">
                                                <option value="0">Select</option>
                                            </select>
                                            <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                <h4>Add New VeteranStatus</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>New VeteranStatus <em class="red-star">*</em></label>
                                                            <input class="form-control veteran_src capital" type="text" placeholder="Add New VeteranStatus">
                                                            <span class="red-star required customError" id="veteran_src"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                            <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                            <input type="button" class="grey-btn" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class='multipleSsn' id="multiple_ssn_div_id">
                                                <label>SSN/SIN/ID</label>
                                                <input placeholder="SSN/SIN/ID" maxlength="50" class="form-control add-input capital" type="text" id="ssn" name="ssn[]" style="margin-bottom: 14px;">
                                                <a class="add-icon ssn-remove-sign" href="javascript:;"
                                                   style="display:none"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                </a>
                                                <span class="ssnErr error red-star"></span>
                                                <a class="add-icon ssn-plus-sign" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--General Information tab ends here-->


                            <!--Contact Details tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        Contact Details
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-3">
                                        <label>Zip/Postal Code</label>
                                        <input class="form-control" type="text" id="zipcode" name="zipcode" placeholder="Eg: 35801">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Country </label>
                                        <input class="form-control capital" type="text" id="country" name="country" placeholder="Eg: US">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>State / Province</label>
                                        <input class="form-control capital" type="text" id="state" name="state" placeholder="Eg: AL">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>City</label>
                                        <input class="form-control capital" type="text" id="city" name="city" placeholder="Eg: Huntsville">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address1</label>
                                        <input class="form-control capital address_field" unique="address" maxlength="100" type="text" id="address1" name="address1" placeholder="Eg: Street address 1">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address2 </label>
                                        <input class="form-control capital address_field" unique="address" maxlength="100" type="text" id="address2" name="address2" placeholder="Eg: Street address 2">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address3</label>
                                        <input class="form-control capital address_field" unique="address" maxlength="100" type="text" id="address3" name="address3" placeholder="Eg: Street address 3">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address4</label>
                                        <input class="form-control capital address_field" unique="address" type="text" maxlength="100" id="address4" name="address4" placeholder="Eg: Street address 4">
                                    </div>
                                    <div class="primary-owner-phone-row" id="primary-owner-phone-row-divId">
                                        <div class="form-outer">
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Phone Type</label>
                                                <select class="form-control" name="phone_type[]" id="phone_type">
                                                    <option value="">Select</option>
                                                    <option value="1">mobile</option>
                                                    <option value="2">work</option>
                                                    <option value="3">Fax</option>
                                                    <option value="4">Home</option>
                                                    <option value="5">Other</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Phone Number <em class="red-star">*</em></label>
                                                <input class="form-control phone_format" id="phone_number" type="text" name="phone_number[]" placeholder="Phone Number">
                                                <span class="phone_numberErr error red-star"></span>
                                            </div>

                                            <div class="work_extension_div" style="display: none;">
                                                <div class="col-sm-1 col-md-1">
                                                    <label>Extension</label>
                                                    <input class="form-control number_only_for_extension" maxlength="15" type="text" id="other_work_phone_extension" name="other_work_phone_extension[]" placeholder="Eg: + 161">
                                                    <span class="other_work_phone_extensionErr error red-star"></span>
                                                </div>
<!--                                                <div class="col-sm-3 col-md-3 ">-->
<!--                                                    <label>Office/Work Extension <em class="red-star">*</em></label>-->
<!--                                                    <input class="form-control phone_format" type="text" disabled id="work_phone_extension" name="work_phone_extension[]" placeholder="Eg: 154-175-4301">-->
<!--                                                    <span class="work_phone_extensionErr error red-star"></span>-->
<!--                                                </div>-->
                                            </div>

                                            <div class="col-sm-3 col-md-3">
                                                <label>Carrier <em class="red-star">*</em></label>
                                                <select class="form-control" name="carrier[]"></select>
                                                <span class="carrierErr error red-star"></span>
                                            </div>

                                            <div class="col-sm-3 col-md-3 countycodediv">
                                                <label>Country Code</label>
                                                <select class="form-control add-input" name="country_code[]" id="country_code">
                                                    <option value="">Select</option>
                                                </select>
                                                <a class="add-icon add-icon-abs" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                <a class="add-icon add-icon-abs" href="javascript:;" ><i class="fa fa-times-circle red-star" aria-hidden="true" style="display:none"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Add Note for this number </label>
                                        <div class="notes_date_right_div">
                                        <textarea class="form-control capital notes_date_right" type="textArea" placeholder="Add Note for this Phone Number" name="phone_number_note"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3">
                                        <div class='multipleEmail' id="multiple_email_div_id" >
                                            <label>Email <em class="red-star">*</em></label>
                                            <input class="form-control add-input" type="text" placeholder="Email" name="email[]">
                                            <a class="add-icon email-remove-sign add-icon-abs" href="javascript:;">
                                                <i class="fa fa-times-circle red-star" aria-hidden="true"  style="display: none"></i>
                                            </a>
                                            <a class="add-icon email-plus-sign add-icon-abs" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 apx-inline-popup">
                                        <label>Referral Source
                                            <a class="pop-add-icon additionalReferralResource" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a>
                                        </label>
                                        <select class="form-control" name="referral_source">
                                            <option value="">Select</option>
                                        </select>
                                        <div class="add-popup" id="additionalReferralResource1">
                                            <h4>Add New Referral Source</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>New Referral Source</label>
                                                        <input class="form-control reff_source1 capital" type="text" placeholder="New Referral Source">
                                                        <span class="customError required red-star" aria-required="true" id="reff_source1"></span>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="referral_source">Save</button>
                                                        <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                        <input type="button" class="grey-btn" value="Cancel">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Contact Details tab ends here-->

                            <!--Emergency Contact Details tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        <strong class="left">Emergency Contact Details</strong>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="owner-emergency-contact" id="owner-emergency-contact_div_id">
                                        <div class="col-sm-2">
                                            <label>Emergency Contact Name</label>
                                            <input placeholder="Emergency Contact Name" class="form-control capital" type="text" id="emergency" name="emergency_contact_name[]" maxlength="50">
                                        </div>
                                        <div class="col-sm-2">
                                            <label>Relation</label>
                                            <select class="form-control" id="relationship" name="emergency_relation[]">
                                                <option value="">Select</option>
                                                <option value="1">Brother</option>
                                                <option value="2">Daughter</option>
                                                <option value="3">Employer</option>
                                                <option value="4">Father</option>
                                                <option value="5">Friend</option>
                                                <option value="6">Mentor</option>
                                                <option value="7">Mother</option>
                                                <option value="8">Neighbor</option>
                                                <option value="9">Nephew</option>
                                                <option value="10">Niece</option>
                                                <option value="11">Owner</option>
                                                <option value="12">Partner</option>
                                                <option value="13">Sister</option>
                                                <option value="14">Son</option>
                                                <option value="15">Spouse</option>
                                                <option value="16">Teacher</option>
                                                <option value="17">Other</option>

                                            </select>
                                        </div>
                                        <div class="col-sm-2" id="otherRelationDiv" style="display: none;">
                                            <label>Other Relation</label>
                                            <input placeholder="Other Relation" class="form-control capital" type="text" id="emergency_other_relation" name="emergency_other_relation[]" maxlength="50">
                                        </div>
                                        <div class="col-sm-2 countycodediv">
                                            <label>Country Code</label>
                                            <select name="emergency_country[]" class="form-control emergencycountry">
                                                <option value="0">Select</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-2">
                                            <label>Phone</label>
                                            <input class="form-control" placeholder="Phone Number" type="text" id="phoneNumber" name="emergency_phone[]" maxlength="12">
                                        </div>

                                        <div class="col-sm-2">
                                            <label>Email</label>
                                            <input class="form-control add-input" placeholder="Email" type="text" id="email1" name="emergency_email[]" maxlength="50">
                                            <a class="add-icon add-emergency-contant" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <!--Emergency Contact Details tab ends here-->

                            <!--Banking Information tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        Banking Information
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="row divBankAccount">
                                        <div class="col-sm-11">
                                            <div class="col-sm-3 apx-inline-popup">
                                                <label>Account Name <a class="pop-add-icon add_owner_account_name" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                <select name="account_name[]" class="form-control account_name">
                                                    <option value="0">Select</option>
                                                </select>
                                                <div class="add-popup" id="account_name" style="display: none;">
                                                    <h4>Add New Account Name</h4>
                                                    <div class="add-popup-body">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Account Name <em class="red-star">*</em></label>
                                                                <input class="form-control owner_account_name capital" type="text" placeholder="Add New Account Name">
                                                                <span class="customError required red-star" id="owner_account_name"></span>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" id="ownerAccountNameBtn" class="blue-btn add_owner_account_name" data-table="owner_account_name" data-cell="account_name" data-class="owner_account_name" data-name="account_name[]">Save</button>
                                                                <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                                <input type="button" class="grey-btn" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Acc. for ACH Transaction <a  data-backdrop="static" data-toggle="modal" id="chartAccountId"  data-target="#chartofaccountmodal" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                <select class="form-control bankChartsOfAccounts" name='account_for_transaction[]' id='account_for_transaction'>
                                                    <option value=''>Select</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Account Type </label>
                                                <select class="form-control" name="bank_account_type[]">
                                                    <option value="">Select</option>
                                                    <option value="1" selected>Business Checking Account</option>
                                                    <option value="2">Personal Checking Account</option>
                                                    <option value="3">Business Savings Account</option>
                                                    <option value="4">Personal Savings Account</option>
                                                    <option value="5">Credit</option>
                                                    <option value="6">Undeposited Funds</option>
                                                    <option value="7">Other</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Account Number</label>
                                                <input placeholder="Bank Account Number" name="bank_account_number[]" class="hide_copy form-control capital number_only" maxlength="16" type="text" />
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Routing / Transit Number</label>
                                                <input placeholder="Bank Routing / Transit Number" name="routing_transit_number[]" class="hide_copy form-control capital" maxlength="16" type="text" />
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Institution Name</label>
                                                <input name="bank_institution_name[]" placeholder="Bank Institution Name" class="hide_copy form-control capital" maxlength="100" type="text" />
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Fraction Number</label>
                                                <input name="bank_fraction_number[]" placeholder="Bank Fraction Number" class="hide_copy form-control capital" maxlength="20" type="text" />
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <a class="add-icon add-chart-bank" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            <a class="add-icon remove-chart-bank" style="display:none;"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 draw_payment_owner">
                                        <label>Draw Payment Method</label>
                                        <div class="check-outer">
                                            <input type="radio" value="check" name="draw_payment_method" />
                                            <label>Check</label>
                                        </div>
                                        <div class="check-outer">
                                            <input type="radio" value="eft" name="draw_payment_method" checked/>
                                            <label>EFT</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Banking Information tab ends here-->

                            <!--Tax Filling Information tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        Tax Filling Information
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-3">
                                        <label>Tax Payer Name</label>
                                        <input placeholder="Tax Payer Name" class="form-control capital" maxlength="50" id="tax_payer_name" name="tax_payer_name" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Tax Payer ID</label>
                                        <input placeholder="Tax Payer ID" class="form-control capital" maxlength="100" type="text" id="tax_payer_id" name="tax_payer_id"/>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Eligible for 1099</label>
                                        <div class="col-sm-5 p-0">
                                            <input type="radio" id="eligible_1099_yes" name="eligible_1099" value="1" checked />Yes
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="radio" id="eligible_1099_no" name="eligible_1099" value="0"/>No
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Send 1099</label>
                                        <div class="check-outer">
                                            <input type="checkbox" name="send_1099" id="send_1099"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 company_name_as_tax_payer_div" style="display: none;">
                                        <div class="check-outer">
                                            <input type="checkbox" name="company_name_as_tax_payer" checked id="company_name_as_tax_payer"/>
                                            Use Entity/Company Name as Tax Payer Name
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Tax Filling Information tab ends here-->

                            <!--Properties owned tab starts here-->
                            <div class="form-outer" id="propertyOwnedDiv">
                                <div class="form-hdr">
                                    <h3>
                                        Properties Owned
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <a class="select_property_popup" id="ancCreateNewProperty" data-toggle="modal" data-backdrop="static" data-target="#selectProperty">
                                            Create New Property
                                        </a>
                                    </div>
                                    <div class="row owner_property_owned_outer" id="div_add_owner">
                                        <div class="owner_property_owned_lft">
                                            <div class="row">
                                                <div class="col-sm-7">
                                                    <label>Property  Name  <em class="red-star">*</em></label>
                                                    <select class="form-control property_owned_name" name="property_id[]" id="property_owned_id">
                                                        <option value="">Select</option>
                                                    </select>
                                                    <span class="property_idErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <label>Property Owned</label>
                                                    <input class="form-control number_only property_percent_owned" type="text" name="property_percent_owned[]" id="property_percent_owned" value="100"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owner_property_owned_rt">
                                            <a class="pop-add-icon add_property_div_plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            <a class="pop-add-icon remove_property_div_minus" style="display: none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Properties owned tab ends here-->

                            <!--Owner's Package tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Owner's Package</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="form-outer">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <label>Owner's Portal</label>
                                                    <div class="col-sm-3 p-0">
                                                        <input type="radio" name="owners_portal" value="1" checked/>Yes
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="radio" name="owners_portal" value="0"/>No
                                                    </div>
                                                    <span class="portalErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Send Owner's Package</label>
                                                    <div class="col-sm-2 p-0">
                                                        <input type="radio" name="send_owners_package" value="1" />Fax
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="radio" name="send_owners_package" value="0" checked/>Email
                                                    </div>
                                                    <span class="portalErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <div class="check-outer">
                                                        <input type="checkbox" name="hold_owners_payments" id="hold_owners_payments"/>
                                                        <label>
                                                            Hold Owner's Payments
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="check-outer">
                                                        <input type="checkbox" name="email_financial_info" id="email_financial_info" checked/>
                                                        <label>
                                                            Email Statement & Other Financial Info
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="check-outer">
                                                        <label>
                                                            Include Reports
                                                        </label>
                                                        <select class="form-control" name="include_reports" id="include_reports">
                                                            <option value="Select">Select</option>
                                                            <option value="1" selected>Yes</option>
                                                            <option value="0">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Owner's Package tab ends here-->
                            <div class="form-outer form-bdr">
                                <div class="form-hdr">
                                    <h4 class="panel-title" style="text-decoration: underline;">
                                        <a class="onlinePaymentInfo" data-toggle="modal" id="onlinePaymentInfo" href="#financial-info" data-target="#financial-info">
                                            <span></span> Online Payments</a>
                                    </h4>
                                </div>
                            </div>
                            <!--Owner Credential Control tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Owner Credential Control</h3>
                                </div>
                                <div class="form-data">
                                    <div class="owner-credentials-control" id="owner-credentials-control_divId">
                                        <div class="form-outer">
                                            <div class="credential-clone">
                                                <div class="col-sm-2">
                                                    <label>Credential Name</label>
                                                    <input placeholder="Credential Name" class="form-control capital" type="text" maxlength="30" id="credentialName" name="credentialName[]">
                                                </div>
                                                <div class="col-sm-2 credintials">
                                                    <label>Credential Type
                                                        <a class="pop-add-icon ownerCredentialType" href="javascript:;">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                        </a>
                                                    </label>
                                                    <select class="form-control" name="credentialType[]">
                                                        <option value="">Select</option>
                                                        <option value="1">Bond</option>
                                                        <option value="2">Certification</option>
                                                    </select>
                                                    <div class="add-popup" id="ownerCredentialType1">
                                                        <h4>Add Credential Type</h4>
                                                        <div class="add-popup-body">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12">
                                                                    <label>Credential Type <em class="red-star">*</em></label>
                                                                    <input class="form-control credential_source capital" type="text" placeholder="Ex: License">
                                                                    <span class="red-star customError required" id="credential_source"></span>
                                                                </div>
                                                                <div class="btn-outer text-right">
                                                                    <button type="button" class="blue-btn add_single1" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                    <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Acquire Date</label>
                                                    <input class="form-control acquireDateClass calander" readonly type="text" id="acquireDate" name="acquireDate[]" placeholder="Eg: 10/10/2014">
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Expiration Date</label>
                                                    <input class="form-control expirationDateClass" readonly  type="text" id="expirationDate" name="expirationDate[]" placeholder="Eg: 10/10/2014">
                                                </div>
                                                <div class='notice-period'>
                                                    <div class="col-sm-2">
                                                        <label>Notice Period</label>
                                                        <select class="form-control add-input" id="term_plan" name="noticePeriod[]">
                                                            <option value="">Select</option>
                                                            <option value="1">5 day</option>
                                                            <option value="2">1 Month</option>
                                                            <option value="3">2 Month</option>
                                                            <option value="4">3 Month</option>
                                                        </select>
                                                        <a class="add-icon add-notice-period" href="javascript:;">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                        </a>
                                                        <a class="add-icon remove-notice-period" href="javascript:;" style="display: none;">
                                                            <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Owner Credential Control tab ends here-->

                            <!--Notes tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        Notes
                                    </h3>
                                </div>
                                <div class="form-data notesDateTime">
                                    <div class="row ownerTxtArea" id="ownerTxtAreaDivId">
                                        <div class="col-sm-4 notes_date_right_div ">
                                            <textarea placeholder="Notes" class="notes form-control capital notes_date_right" maxlength="500" rows="6" name="notes[]"></textarea>
                                        </div>
                                        <div class="col-sm-1">
                                            <a class="add-icon add-icon-textarea" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a>
                                            <a class="add-icon remove-icon-textarea" href="javascript:;" style="display: none;">
                                                <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Notes tab ends here-->

                            <!--File Library tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        File Library
                                    </h3>
                                </div>
                                <div class="form-data">

                                    <div class="form-outer2">
                                        <div class="col-sm-12 mb-15 text-right">
                                            <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                            <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                            <button type="button" class="orange-btn" id="remove_library_file">Remove All Files</button>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row" id="file_library_uploads">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-outer listinggridDiv">
                                        <div class="apx-table apxtable-bdr-none">
                                            <div class="table-responsive">
                                                <table id="propertFileLibrary-table" class="table table-bordered">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--File Library tab ends here-->

                            <!--Custom Fields tab starts here-->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3 id="headerDivCustom">
                                        Custom Fields
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <input type="hidden" name="id" id="record_id">
                                    <div class="col-sm-12" style="margin-left: 15px;">
                                        <div class="collaps-subhdr">
                                        </div>
                                        <div class="row">
                                            <div class="custom_field_html">
                                            </div>
                                            <div class="col-sm-3 custom_field_msg">
                                                No Custom Fields
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="btn-outer text-right">
                                                    <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Custom Fields tab ends here-->

                            <div class="btn-outer text-right">
                                <button type="submit" class="blue-btn" id="ownerSaveForm">Save</button>
                                <button type="reset" id="reset_add_owner_btn" class="grey-btn">Clear</button>
                                <button type="button" id="cancel_add_owner_btn" class="grey-btn mg-rt-30">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<!-- Wrapper Ends -->
<!--Online Payment Method-->






<!--<div class="container">-->
<!--    <div class="modal fade" id="financial-info" role="dialog">-->
<!--        <div class="modal-dialog modal-md" style="width: 60%;">-->
<!--            <div class="modal-content" style="width: 100%;">-->
<!--                <div class="modal-header">-->
<!--                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->-->
<!--                    <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>-->
<!--                    </a>-->
<!---->
<!--                    <h4 class="modal-title">Online Payment Details</h4>-->
<!--                </div>-->
<!--                <div class="modal-body">-->
<!--                    <div class="steps">-->
<!--                        <ul>-->
<!--                            <li>-->
<!--                                <a id="paymentDiv" class="paymentDiv active" href="javascript:;">1</a>-->
<!--                                <span>Payment</span>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a id="basicDiv" class="basicDiv" href="javascript:;">2</a>-->
<!--                                <span>Basic</span>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                    <div class="apx-adformbox-content">-->
<!--                        <form method="post" id="addStep1Form">-->
<!--                            <input type="hidden" id="stripe_vendor_id" value="">-->
<!--                            <input type="hidden" id="company_user_id" value="">-->
<!--                            <h3>Payment Method <em class="red-star">*</em></h3>-->
<!--                            <div class="row">-->
<!--                                <div class="form-outer">-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label></label>-->
<!--                                        <select class="form-control" name="payment_method" id="payment_method">-->
<!--                                            <option value="1">Credit Card/Debit Card</option>-->
<!--<!--                                            <option value="2">ACH</option>-->-->
<!---->
<!--                                        </select>-->
<!--                                        <span class="ffirst_nameErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4 clear">-->
<!--                                        <label>Card Type<em class="red-star">*</em></label>-->
<!--                                        <img src="/company/images/card-payment.png" style="width: 190px;">-->
<!--                                        <span class="ffirst_nameErr error red-star"></span>-->
<!--                                    </div>-->
<!---->
<!--                                    <div class="col-sm-4 col-md-4 clear">-->
<!--                                        <label>Card Holder First Name <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&-->
<!--	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="ffirst_name" name="cfirst_name">-->
<!--                                        <span class="ffirst_nameErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Holder Last Name</label>-->
<!--                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&-->
<!--	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="flast_name" name="clast_name">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Number <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control hide_copy" type="text" id="ccard_number" name="ccard_number" placeholder="1234 1234 1234 1234" maxlength="16">-->
<!--                                        <span class="flast_nameErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label> Expiry Year <em class="red-star">*</em></label>-->
<!--                                        <select class="form-control"  id="cexpiry_year" name="cexpiry_year">-->
<!--                                        </select>-->
<!--                                        <span class="femailErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4 fphone_num">-->
<!--                                        <label> Expiry Month <em class="red-star">*</em></label>-->
<!--                                        <select class="form-control"  id="cexpiry_month" name="cexpiry_month">-->
<!--                                        </select>-->
<!--                                        <span class="fphone_numberErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>CVC<em class="red-star">*</em></label>-->
<!--                                        <input class="form-control capsOn" type="text" id="ccvv" name="ccvv" maxlength="5">-->
<!--                                        <span class="fbirth_dateErr error red-star"></span>-->
<!--                                    </div>-->
<!--<!--                                    <div class="col-sm-4 col-md-4">-->-->
<!--<!--                                        <label>Company <em class="red-star">*</em></label>-->-->
<!--<!--                                        <input type="text" class="form-control capital" id="cCompany" name="cCompany" value="">-->-->
<!--<!--                                        <span class="fzipcodeErr error red-star"></span>-->-->
<!--<!--                                    </div>-->-->
<!--                                    <div class="col-sm-4 col-md-4 clear">-->
<!--                                        <label>Card Billing Phone Number <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control phone_format" id="cphoneNumber" name="cphoneNumber" value="" >-->
<!--                                        <span class="fcityErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Billing Address Line 1 <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control capital" id="caddress1" name="caddress1" value="">-->
<!--                                        <span class="fstateErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Billing Address Line 2 </label>-->
<!--                                        <input type="text" class="form-control capital" id="caddress2" name="caddress2" value="">-->
<!--                                        <span class="faddressErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>City<em class="red-star">*</em></label>-->
<!--                                        <input class="form-control capsOn capital" type="text" id="ccity" name="ccity">-->
<!--                                        <span class="fbusinessErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>State/Province <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control cstate capital"  name="cstate" value="">-->
<!--                                        <span class="fssnErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Zip/Postal Code<em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control czip_code capital"  name="czip_code" value="--><?php //$_SESSION[SESSION_DOMAIN]['default_zipcode'] ?><!--" >-->
<!--                                        <span class="faccount_holderErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Country <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control ccountry capital"  name="ccountry" value="">-->
<!--                                        <span class="faccount_holdertypeErr error red-star"></span>-->
<!--                                    </div>-->
<!---->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="row" style="text-align: center;">-->
<!--                                <button  class="blue-btn">Continue</button>-->
<!--                            </div>-->
<!---->
<!--                        </form>-->
<!--                    </div>-->
<!--                    <div class="verification-message"><p></p></div>-->
<!--                    <div class="basic-payment-detail" style="display: none;">-->
<!--                        <div class="row">-->
<!--                            <form id="addStep2Form">-->
<!--                                <div class="form-outer">-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Country <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control ccountry capital"  type="text" id="acc_country" name="country">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Account Type <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control account_type"  value="Indiviual" type="text" id="acc_account_type" name="account_type" readonly>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Account Number <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control account_number" type="text" id="acc_account_number" name="account_number" maxlength="16">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Routing Number <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control routing_number" type="text" id="acc_routing_number" name="routing_number" maxlength="9">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>City <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control ccity capital"  type="text" id="acc_city" name="city">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Line1 <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control line1 capital" type="text" id="acc_line1" name="line1">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Address Line2 </label>-->
<!--                                        <input class="form-control address_line2 capital" type="text" id="acc_address_line2" name="address_line2">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>POSTAL CODE <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control postal_code czip_code" type="text" id="acc_postal_code" name="postal_code">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>State <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control cstate capital"  type="text" id="acc_state" name="state">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Email <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control email" type="text" id="acc_email" name="email">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>First_name <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control first_name capital" type="text" id="acc_first_name" name="first_name">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Last_name <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control last_name capital" type="text" id="acc_last_name" name="last_name">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Phone <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control phone" type="text" id="acc_phone" name="phone">-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>SSN Last 4 <em class="red-star">*</em></label>-->
<!--                                        <input class="form-control ssn_last" type="text" id="acc_ssn_last" name="ssn_last" maxlength="4">-->
<!---->
<!--                                    </div>-->
<!--                                    <div class="col-sm-12 date-outer-form">-->
<!--                                        <label> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>-->
<!---->
<!--                                        <div class="col-sm-12 col-md-3 fphone_num  pad-none">-->
<!--                                            <select class="form-control"  id="fpday" name="day"><option value="">Select</option>-->
<!--                                            </select>-->
<!--                                            <span class="fmonthErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-12 col-md-3 fphone_num">-->
<!--                                            <select class="form-control"  id="fpmonth" name="month"><option value="">Select</option>-->
<!--                                            </select>-->
<!--                                            <span class="fmonthErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-12 col-md-3 date-year">-->
<!--                                            <select class="form-control"  id="fpyear" name="year"><option value="">Select</option>-->
<!--                                            </select>-->
<!--                                            <span class="fyearErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-12 text-center">-->
<!--                                        <button  id="savefinancial" class="blue-btn">Submit</button>-->
<!--                                    </div>-->
<!---->
<!--                                </div>-->
<!--                            </form>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--Online Payment Method-->
<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SITE_URL ?>/images/loading.gif'/>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="vendor">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control capital" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">

                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button rel="custom_field" type="submit" class="grey-btn clearVendorcredentialForm" >Clear</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->
<!-- Wrapper Ends -->
<div class="container">
    <div class="modal fade" id="chartofaccountmodal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add Chart Of Account</h4>
                </div>
                <div class="modal-body">
                    <form name="add_chart_account_form" id="add_chart_account_form_id">
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Account Type <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" name="account_type_id" id="account_type_id"></select>
                                            <span id="account_type_idErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Code <em class="red-star">*</em></label>
                                            <input id="account_code" name="account_code" maxlength="4" placeholder="Eg: 1110" class="form-control capital" type="text"/>
                                            <span id="account_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Account Name <em class="red-star">*</em></label>
                                            <input id="com_account_name" name="account_name" maxlength="50" placeholder="Eg: Property Trust" class="form-control capital" type="text"/>
                                            <span id="account_nameErr" class="error"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>Reporting Code <em class="red-star">*</em></label>
                                            <select class="fm-txt form-control" id="reporting_code" name="reporting_code">
                                                <option value="">Select</option>
                                                <option value="1">C</option>
                                                <option value="2">L</option>
                                                <option value="3">M</option>
                                                <option value="4">B</option>
                                            </select>
                                            <span id="reporting_codeErr" class="error"></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Sub Account of</label>
                                            <select class="fm-txt form-control" id="sub_account" name="sub_account">
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Status</label>
                                            <select class="fm-txt form-control" id="status" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4">
                                            <label>
                                                <div class="check-outer">
                                                    <input name="is_default" id="is_default" type="checkbox"/>
                                                    <label>Set as Default</label>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <select class="fm-txt form-control" id="posting_status" name="posting_status">
                                                <option value="1">Posting</option>
                                                <option value="0">Non Posting</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="btn-outer text-right">
                                        <input type="hidden" value="" name="chart_account_edit_id" class="form-control" id="chart_account_edit_id"/>
                                        <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                        <button type="button"  class="clear-btn clearFormReset" id="clearChartsOfAccountsModal">Clear</button>
                                        <button type="button"  class="grey-btn" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Online Payment Method-->
<div class="container">
    <div class="modal fade" id="financial-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="steps">
                        <ul>
                            <li>
                                <a id="paymentDiv" class="paymentDiv active" href="javascript:;">1</a>
                                <span>Payment</span>
                            </li>
                            <li>
                                <a id="basicDiv" class="basicDiv" href="javascript:;">2</a>
                                <span>Basic</span>
                            </li>
                        </ul>
                    </div>
                    <div class="apx-adformbox-content">
                        <form method="post" id="addStep1Form">
                            <input type="hidden" id="stripe_vendor_id" value="">
                            <input type="hidden" id="company_user_id" value="">
                            <h3>Payment Method <em class="red-star">*</em></h3>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label></label>
                                        <select class="form-control" name="payment_method" id="payment_method">
                                            <option value="1">Credit Card/Debit Card</option>
                                            <!--                                            <option value="2">ACH</option>-->

                                        </select>
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Type<em class="red-star">*</em></label>
                                        <img src="/company/images/card-payment.png" style="width: 190px;">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-4 col-md-4 clear">
                                        <label>Card Holder First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="ffirst_name" name="cfirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Holder Last Name</label>
                                        <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="flast_name" name="clast_name">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Card Number <em class="red-star">*</em></label>
                                        <input class="form-control hide_copy" type="text" id="ccard_number" name="ccard_number" placeholder="1234 1234 1234 1234" maxlength="16">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label> Expiry Year <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_year" name="cexpiry_year">
                                        </select>
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 fphone_num">
                                        <label> Expiry Month <em class="red-star">*</em></label>
                                        <select class="form-control"  id="cexpiry_month" name="cexpiry_month">
                                        </select>
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>CVC<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="ccvv" name="ccvv" maxlength="5">
                                        <span class="fbirth_dateErr error red-star"></span>
                                    </div>
                                    <!--                                    <div class="col-sm-4 col-md-4">-->
                                    <!--                                        <label>Company <em class="red-star">*</em></label>-->
                                    <!--                                        <input type="text" class="form-control capital" id="cCompany" name="cCompany" value="">-->
                                    <!--                                        <span class="fzipcodeErr error red-star"></span>-->
                                    <!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4 clear">-->
<!--                                        <label>Card Billing Phone Number <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control phone_format" id="cphoneNumber" name="cphoneNumber" value="" >-->
<!--                                        <span class="fcityErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Billing Address Line 1 <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control capital" id="caddress1" name="caddress1" value="">-->
<!--                                        <span class="fstateErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Card Billing Address Line 2 </label>-->
<!--                                        <input type="text" class="form-control capital" id="caddress2" name="caddress2" value="">-->
<!--                                        <span class="faddressErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>City<em class="red-star">*</em></label>-->
<!--                                        <input class="form-control capsOn capital" type="text" id="ccity" name="ccity">-->
<!--                                        <span class="fbusinessErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>State/Province <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control cstate capital"  name="cstate" value="">-->
<!--                                        <span class="fssnErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Zip/Postal Code<em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control czip_code capital"  name="czip_code" value="--><?php //$_SESSION[SESSION_DOMAIN]['default_zipcode'] ?><!--" >-->
<!--                                        <span class="faccount_holderErr error red-star"></span>-->
<!--                                    </div>-->
<!--                                    <div class="col-sm-4 col-md-4">-->
<!--                                        <label>Country <em class="red-star">*</em></label>-->
<!--                                        <input type="text" class="form-control ccountry capital"  name="ccountry" value="">-->
<!--                                        <span class="faccount_holdertypeErr error red-star"></span>-->
<!--                                    </div>-->

                                </div>
                            </div>
                            <div class="row" style="text-align: center;">
                                <button  class="blue-btn">Continue</button>
                            </div>

                        </form>
                    </div>
                    <div class="verification-message"><p></p></div>
                    <div class="basic-payment-detail" style="display: none;">
                        <div class="row">
                            <form id="addStep2Form">
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control capital ccountry capital" name="country" id="acc_country" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fcountryErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="acc_mcc" name="mcc" readonly="">
                                        </select>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="url" name="url">
                                    </div>

                                    <div class="col-sm-4 col-md-3">
                                        <label>Account Type <em class="red-star">*</em></label>
                                        <input class="form-control account_type"  value="Indiviual" type="text" id="acc_account_type" name="account_type" readonly>
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Account Number <em class="red-star">*</em></label>
                                        <input class="form-control account_number" type="text" id="acc_account_number" name="account_number" maxlength="16">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Routing Number <em class="red-star">*</em></label>
                                        <input class="form-control routing_number" type="text" id="acc_routing_number" name="routing_number" maxlength="9">
                                    </div>

                                    <div class="col-sm-4 col-md-3">
                                        <label>Line1 <em class="red-star">*</em></label>
                                        <input class="form-control line1 capital" type="text" id="acc_line1" name="line1">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Address Line2 </label>
                                        <input class="form-control address_line2 capital" type="text" id="acc_address_line2" name="address_line2">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>POSTAL CODE <em class="red-star">*</em></label>
                                        <input class="form-control postal_code czip_code" type="text" id="acc_postal_code" name="postal_code">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input class="form-control ccity capital"  type="text" id="acc_city" name="city">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input class="form-control cstate capital"  type="text" id="acc_state" name="state">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control email" type="text" id="acc_email" name="email">
                                    </div>
                                    <div class="col-sm-12 date-outer-form">
                                        <label> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>

                                        <div class="col-sm-12 col-md-3 fphone_num  pad-none">
                                            <select class="form-control"  id="fpday" name="day" ><option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <select class="form-control"  id="fpmonth" name="month" ><option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 date-year">
                                            <select class="form-control"  id="fpyear" name="year" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>First_name <em class="red-star">*</em></label>
                                        <input class="form-control first_name capital" type="text" id="acc_first_name" name="first_name">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Last_name <em class="red-star">*</em></label>
                                        <input class="form-control last_name capital" type="text" id="acc_last_name" name="last_name">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input class="form-control phone phone_format" type="text" id="acc_phone" name="phone">
                                    </div>
                                    <div class="col-sm-4 col-md-3">
                                        <label>SSN Last 4 <em class="red-star">*</em></label>
                                        <input class="form-control ssn_last" type="text" id="acc_ssn_last" name="ssn_last" maxlength="4">

                                    </div>



                                    <div class="col-sm-12 text-center">
                                        <button  id="savefinancial" class="blue-btn">Submit</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="company-basic-payment-detail" style="display: none;">
                        <div class="row">

                            <form method="post" id="financialCompanyInfo">
                                <input type="hidden" id="company_document_id" name="company_document_id" value="">
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control capital" name="fcountry"  id="fcountry" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fcountryErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fcmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcurl" name="furl">
                                        <span class="furlErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcbusiness" name="fbusiness"  value="Company" readonly>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="fcaccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fprouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fccity" name="fccity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress" name="fcaddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress2" name="fcaddress2" >
                                        <span class="fcaddress2Err error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fczipcode" name="fczipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcstate" name="fcstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Name <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcname" name="fcname" value="">
                                        <span class="fcnameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fcphone_number" name="fcphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="fcphone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Tax_id <em class="red-star">*</em></label>
                                        <input   class="form-control" fixedLength="10" id="fctax_id" name="fctax_id" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Company Document <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Upload Company Document here.</span></div></label>
                                        <input type='file' name="company_document" id="company_document" />
                                        <span class="company_document_idErr error red-star"></span>
                                    </div>




                                </div>
                                <div class="col-sm-12">
                                    <div class="form-hdr">
                                        <h3>Person</h3>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-outer mt-15">
                                        <div class="col-sm-12 col-md-3">
                                            <label>First Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn capital" type="text" id="fpfirst_name" name="ffirst_name">
                                            <span class="ffirst_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Last Name <em class="red-star">*</em></label>
                                            <input class="form-control capsOn capital" type="text" id="fplast_name" name="flast_name">
                                            <span class="flast_nameErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Email <em class="red-star">*</em></label>
                                            <input class="form-control" type="email" id="fpemail" name="femail" >
                                            <span class="femailErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Phone <em class="red-star">*</em></label>
                                            <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fpphone_number" name="fphone_number" value="">
                                            <span class="fphone_numberErr error red-star"></span>
                                            <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label> Day <em class="red-star">*</em></label>
                                            <select class="form-control fpday"  id="fppday" name="fday" >
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Month <em class="red-star">*</em></label>
                                            <select class="form-control fpmonth"  id="fppmonth" name="fmonth" >
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <label>Year <em class="red-star">*</em></label>
                                            <select class="form-control fpyear"  id="fppyear" name="fyear" >
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fpssn" name="fssn" value="" maxlength="4">
                                            <span class="fssnErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 1 <em class="red-star">*</em></label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="fpaddress" name="faddress" >
                                            <span class="faddressErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Address 2</label>
                                            <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                            <input class="form-control capital" id="fpaddress2" name="faddress2" >
                                            <span class="faddress2Err error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>Postal Code <em class="red-star">*</em></label>
                                            <input type="text" class="form-control" id="fpzipcode" name="fzipcode" value=""  maxlength="9">
                                            <span class="fzipcodeErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3">
                                            <label>State <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fpstate" name="fstate" value="" readonly>
                                            <span class="fstateErr error red-star"></span>
                                        </div>
                                    </div>
                                    <div class="form-outer">
                                        <div class="col-sm-12 col-md-3">
                                            <label>City <em class="red-star">*</em></label>
                                            <input type="text" class="form-control capital" id="fpcity" name="fcity" value="" >
                                            <span class="fcityErr error red-star"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <button type="submit"  id="savecompanyfinancial" class="blue-btn">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="modal fade" id="financial-infotype" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>
                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="financial-infotype-radio">
                        <label class="radio-inline">
                            <input type="radio" name="companytype" checked value="individual"> Individual
                        </label>
                        <input type="hidden" value="<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id'] ?>" id="hidden_company_id" >
                        <label class="radio-inline">
                            <input type="radio" name="companytype" value="company" >Company
                        </label>
                    </div>

                    <div class="financial-infotype-btn text-right">
                        <button type="button"  id="savefinancialtype" class="blue-btn">Submit</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!--Online Payment Method-->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->

<script type="text/javascript">
    var detailCompanyArr=[];
    var stripe_detail_form_data = '';
    var stripe_card_detail_form_data = '';
    $('#people_top').addClass('active');
    var upload_url = "<?php echo SITE_URL; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var default_zipcode = "<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode']; ?>";
    var subdomain_url  = "<?php echo COMPANY_SUBDOMAIN_URL; ?>";
</script>
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/addOwner.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerpopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/file_library.js" type="text/javascript"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/payments/plaidIntialisation.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/onlinePayment.js"></script>

<script>
    $(document).ready(function () {
        setTimeout(function(){
            defaultFormData = $('#FormSaveNewVendor').serializeArray();
        },300);
    });
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });

    <!--- Main Nav Responsive -->
    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

</script>

<script>
    function goBack() {
        window.history.back();
    }

    $('.company-top').addClass('active');
    $('.cropItData').hide();
    jQuery('.phone_format').mask('000-000-0000', {reverse: true});
    jQuery('input[name="pet_phoneNumber[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="service_phoneNumber[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="additional_phone[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="guarantor_phone_1[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});

    $('#acquireDate').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat})
        .datepicker("setDate", new Date());

    $('#expirationDate').datepicker({yearRange: '-0:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, minDate:  new Date(),})
        .datepicker("setDate", new Date());



    $('.calander').datepicker({
        yearRange: '-100:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="dob"]').datepicker({
        yearRange: '-100:+0',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        maxDate:  new Date()
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(".calander").val(date);


    $('.calander1').datepicker({
        yearRange: '-100:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('.calander1').datepicker({dateFormat: jsDateFomat});
    var date = $.datepicker.formatDate(jsDateFomat, new Date());

    $(".calander1").val(date);
</script>
<style>
    select[name=hobbies] + .btn-group button{
        margin-top: 0px;
    }
    .closeimagepopupicon{
        float: right;
        font-weight: bold;
        border: 2px solid #666;
        border-radius: 12px;
        padding: 0px 4px;
        cursor: pointer;
    }

    .apx-inline-popup {
        position: relative;
    }
    .apx-inline-popup-box > h4 {
        font-size: 12px
    }

    .image-editor {
        text-align: center;
    }

    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
        display: inline-block;
    }

    .image-size-label {
        margin-top: 10px;
    }

    .export {
        /* Use relative position to prevent from being covered by image background */
        position: relative;
        z-index: 10;
        display: block;
    }

    .image-editor input[type="file"] {
        opacity: 0;
        position: absolute;
        top: 45px;
        left: 91px;
    }

    .upload-logo .img-outer img {
        max-width: 100%;
    }


</style>
<!-- Jquery Ends -->
</body>

</html>