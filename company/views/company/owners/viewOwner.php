<?php
/**
 * Created by PhpStorm.
 * User: chughraghav
 * Date: 6/03/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
$edit_id = (isset($_REQUEST['id']))?$_REQUEST['id']:'';
$base_url=$_SERVER['SERVER_NAME'];
?>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->
    <section class="main-content view-owner-page">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                People >> <span>Owner View</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text">
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="building_property_id" val=""/>
                <input type="hidden" value="<?php echo $_GET['id'];?>" name="owner_id" class="owner_id">
                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->

                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                                <li role="presentation" class="active"><a href="/People/Ownerlisting">Owners</a></li>
                                <li role="presentation"><a href="/Vendor/Vendor">Vendors</a></li>
                                <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                                <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                            </ul>
                            <!-- Nav tabs -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>General Information <a onclick="goBack()" class="back" style="cursor: pointer;"><i class="fa fa-angle-double-left" aria-hidden="true" ></i> Back</a></h3>
                                        </div>
                                            <div class="form-data owner-general-info-tab">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <div class="owner_image img-outer">
                                                        <img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg">
                                                    </div>
                                                    <div align="center"><h5><span class="name"></span></h5></div>
                                                </div>
                                                <div class="col-sm-10">
                                                    <h3><span class="name"></span></h3>
                                                    <div class="edit-foot">
                                                        <a href="javascript:;" class="edit_redirection" redirection_data="generalInfoDiv" >
                                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form Outer Ends -->

                                <div class="form-outer2">
                                    <div class="form-hdr">
                                        <h3>Contact Information</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Address :</label>
                                                        <span class="owner_address"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Phone :</label>
                                                        <span class="owner_phone"></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Note for this Phone Number :</label>
                                                        <span class="owner_note_for_Phone red-star">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Email :</label>
                                                        <span class="owner_Email"></span>
                                                    </div>
                                                    <div id="ethnicity_to_dob_div">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">SSN/SIN/ID :</label>
                                                            <span class="owner_SSN"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Ethnicity :</label>
                                                            <span class="owner_ethnicity"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Hobbies :</label>
                                                            <span class="owner_hobbies"></span>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <label class="text-right">MaritalStatus :</label>
                                                            <span class="owner_marital_status"></span>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <label class="text-right">Veteran Status :</label>
                                                            <span class="owner_vetran_status"></span>
                                                        </div>

                                                        <div class="col-xs-12">
                                                            <label class="text-right">DOB :</label>
                                                            <span class="owner_dob"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer starts -->
                            <div class="form-outer2">
                                <div class="form-hdr">
                                    <h3>Emergency Contact Details</h3>
                                </div>
                                <div class="form-data">
                                    <div class="detail-outer emergency_detail_data">
                                        <div class=row style='margin-bottom: 10px;'>
                                            <div class=col-sm-6>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Emergency Contact Name : </label>
                                                    <span class=emeregency_name></span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Country Code :</label>
                                                    <span class=country_code> </span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Emergency Contact Phone : </label>
                                                    <span class=emeregency_contact></span>
                                                </div>
                                            </div>
                                            <div class=col-sm-6>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Emergency Contact Email : </label>
                                                    <span class=emeregency_email></span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Emergency Contact Relation : </label>
                                                    <span class=emeregency_relation></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="emergencyInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer starts -->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Banking Info</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="apx-table listinggridDiv">
                                                                <div class="table-responsive">
                                                                    <table id="bankinginfo-table" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="bankingInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer2" href="#" id="keys">
                                <div class="form-hdr">
                                    <h3>Tax Filling Information</h3>
                                </div>

                                <div class="form-data">
                                    <div class="detail-outer">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="col-xs-12">
                                                    <label class="text-right">Tax Payer Name :</label>
                                                    <span class="tax_payer_name"></span>
                                                </div>

                                                <div class="col-xs-12">
                                                    <label class="text-right">Tax Payer ID :</label>
                                                    <span class="tax_payer_id"></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label class="text-right">Eligible for 1099 :</label>
                                                    <span class="eligible_for_1099"></span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="apx-table listinggridDiv">
                                                                <div class="table-responsive">
                                                                    <table id="UnitKeys-table" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="taxInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->
                            <div class="form-outer" id="">
                                <div class="form-hdr">
                                    <h3>Properties Owned</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="property-own" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="propertyOwnedDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer Starts -->
                            <div class="form-outer2">
                                <div class="form-hdr">
                                    <h3>Owner's Package</h3>
                                </div>
                                <div class="form-data">
                                    <div class="detail-outer">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="col-xs-12">
                                                    <label class="text-right">Owners Portal :</label>
                                                    <span class="owners_portal"></span>
                                                </div>

                                                <div class="col-xs-12">
                                                    <label class="text-right">Email Statement & other financial Info :</label>
                                                    <span class="email_statement"></span>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="ownersPackageInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer Starts -->
                            <div class="form-outer2">
                                <div class="form-hdr">
                                    <h3>Owner Credential Control</h3>
                                </div>
                                <div class="form-data">
                                    <div class="detail-outer owner_credentials">
                                        <div class=row>
                                            <div class=col-sm-6>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Credential Name :</label>
                                                    <span class=credential_name></span>
                                                </div>

                                                <div class=col-xs-12>
                                                    <label class=text-right>Credential Type :</label>
                                                    <span class=credential_type></span>
                                                </div>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Acquire Date :</label>
                                                    <span class=acquire_date></span>
                                                </div>
                                            </div>

                                            <div class=col-sm-6>
                                                <div class=col-xs-12>
                                                    <label class=text-right>Expiraion Date :</label>
                                                    <span class=expiration_date></span>
                                                </div>

                                                <div class=col-xs-12>
                                                    <label class=text-right>Notice Period :</label>
                                                    <span class=notice_period></span>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="credentialControlInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer Starts -->
                            <div class="form-outer" style="margin-bottom: 20px" id="flags">
                                <div class="form-hdr">
                                    <h3>Flag Bank</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="ownerFlags-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="flagsBankInfoDiv" >
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->


                            <div class="form-outer" id="complaintInfoDivId">
                                <div class="form-hdr">
                                    <h3>Owner Complaint</h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-2 p-0">
                                        <select class="form-control" id="complaint_by_about_status"  name="complaint_by_about">
                                            <option value="all">All</option>
                                            <option value="Complaint about Owner">Complaint about Owner</option>
                                            <option value="Complaint by Owner">Complaint by Owner</option>
                                        </select>
                                    </div>
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="ownerComplaintsView-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="complaintInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer starts -->
                            <div class="form-outer" id="OwnerNotes">
                                <div class="form-hdr">
                                    <h3>Notes</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="notes-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="noteInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <!-- Form Outer Ends -->
                            <div class="form-outer" id="filelibrary">
                                <div class="form-hdr">
                                    <h3>File Library</h3>
                                </div>
                                <div class="form-data">
                                    <div class="accordion-grid">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <div class="apx-table listinggridDiv">
                                                                    <div class="table-responsive">
                                                                        <table id="file-library" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="fileLibraryIfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->
                            <!-- Form Outer starts -->
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Custom Fields</h3>
                                </div>
                                <div class="form-data">
                                    <input type="hidden" id="customFieldModule" name="module" value="owner">
                                    <div class="col-sm-7 custom_field_html_view_mode">
                                        No Custom Fields
                                    </div>


                                    <div class="edit-foot">
                                        <a href="javascript:;" class="edit_redirection" redirection_data="customFieldsInfoDiv">
                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->
                        </div>
                    </div>
                    <!--tab Ends -->
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    <!--- Accordians -->
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function(){
            $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
        });

        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
        }).on('hide.bs.collapse', function(){
            $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
        });
    });
    <!--- Accordians -->


    <!-- SLider -->
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            autoplay:false,
            autoplayTimeout:1500,
            autoplayHoverPause:true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: true
                },
                1020: {
                    items: 2,
                    nav: true,
                    loop: true,
                    margin: 20
                },
                1199: {
                    items: 3,
                    nav: true,
                    loop: true,
                    margin: 20
                }
            }
        })
    })
    var upload_url = "<?php echo SITE_URL; ?>";
    function goBack() {
        window.history.back();
    }
    $('#people_top').addClass('active');
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
</script>

<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/building/buildingView.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/unit/viewOwner.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/viewOwner.js"></script>

<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/company/people/owner/complaintsOwner.js"></script>-->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/owners/viewOwner.php");
?>
