<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once(COMPANY_DIRECTORY_URL . "/views/company/owners/modals.php");
?>
<style>
    .row.custom_field_class input {
        width: 258px;
    }
    #selectProperty .add-popup {width: 111%;}
    #selectProperty #newAmenitiesPopup { width: 56%; }
    #selectProperty #newUnitTypePopup { width: 120%; }
</style>
<div class="popup-bg"></div>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <main class="apxpg-main" id="edit_owner_page_id">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="breadcrumb-outer">
                                    People >> <span>Edit Owner</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <form id="addOwner" enctype='multipart/form-data'>

                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                                    <li role="presentation" class="active"><a href="/People/Ownerlisting">Owners</a></li>
                                    <li role="presentation"><a href="/Vendor/Vendor">Vendors</a></li>
                                    <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                                    <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                                </ul>
                            </div>

                            <!--General Information tab starts here-->
                            <div class="form-outer" id="generalInfoDiv">
                                <div class="form-hdr">
                                    <h3>
                                        General Information<a class="back" href="/People/Ownerlisting"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-12">
                                        <div class="check-outer mb-15">
                                            <input type="checkbox" name="if_entity_name_display" id="if_entity_name_display">
                                            <label class="">Use Entity/Company Name as Display Name</label>
                                        </div>
                                        <label>Upload Picture</label>
                                        <div class="upload-logo">
                                            <div class="owner_image img-outer"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>
                                            <a href="javascript:;" id="cropImageTrigger">
                                                <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image
                                            </a>
                                        </div>
                                        <div class="image-editor">
                                            <input type="file" class="cropit-image-input" name="owner_image">
                                            <div class='cropItData' style="display: none;">
                                                <span class="closeimagepopupicon">X</span>
                                                <div class="cropit-preview"></div>
                                                <div class="image-size-label">Resize image</div>
                                                <input type="range" class="cropit-image-zoom-input">
                                                <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                <input type="button" class="export" value="Done" data-val="owner_image">
                                                <div class="cropItData-row">
                                                    <a href="javascript:;" id="rotate-ccw" class="rotate-ccw"><i class="fa fa-undo" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="javascript:;" id="rotate-cw" class="rotate-cw"><i class="fa fa-repeat" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3">
                                        <label>Salutation</label>
                                        <select class="form-control" name="salutation" id="salutation">
                                            <option value="">Select</option>
                                            <option value="Dr.">Dr.</option>
                                            <option value="Mr.">Mr.</option>
                                            <option value="Mrs.">Mrs.</option>
                                            <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Sir">Sir</option>
                                            <option value="Madam">Madam</option>
                                            <option value="Brother">Brother</option>
                                            <option value="Sister">Sister</option>
                                            <option value="Father">Father</option>
                                            <option value="Mother">Mother</option>
                                        </select>
                                        <span class="salutationErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capital" name="first_name" placeholder="First Name"  maxlength="50"  id="first_name" type="text" />
                                        <span class="first_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Middle Name</label>
                                        <input class="form-control capital" name="middle_name" id="middle_name" maxlength="50" placeholder="Middle Name" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capital" placeholder="Last Name" name="last_name" maxlength="50" id="last_name" type="text" />
                                        <span class="last_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-3" style="display: none;">
                                        <label>Maiden Name</label>
                                        <input class="form-control capital" placeholder="Maiden Name" name="maiden_name" maxlength="50" id="maiden_name" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Nick Name</label>
                                        <input class="form-control capital" placeholder="Nick Name" name="nick_name" maxlength="50" id="nick_name" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Gender</label>
                                        <select class="form-control" id="gender"  name="gender">
                                            <option value="">Select</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                            <option value="3">Prefer Not To Say</option>
                                            <option value="4">Others</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Entity/Company </label>
                                        <input class="form-control capital" placeholder="Entity/Company" name="company_name" maxlength="50" id="entity_company" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>D.O.B</label>
                                        <input class="form-control" type="text" readonly id="birth" name="dob" placeholder="D.O.B">
                                    </div>
                                    <div class="" id="ethnicity_to_ssn_div">
                                        <div class="col-sm-3 ">
                                            <label>Ethnicity
                                                <a class="pop-add-icon selectPropertyEthnicity" href="javascript:;">
                                                    <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control" name="ethncity">
                                                <option value="0">Select</option>
                                                <option value="1">Male</option>
                                                <option value="2">Female</option>
                                            </select>
                                            <div class="add-popup" id="selectPropertyEthnicity1">
                                                <h4>Add New Ethnicity</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>New Ethnicity <em class="red-star">*</em></label>
                                                            <input class="form-control ethnicity_src customValidateGroup capital" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                            <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethncity">Save</button>
                                                            <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                            <input type="button" class="grey-btn" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Marital Status
                                                <a class="pop-add-icon selectPropertyMaritalStatus" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control" name="maritalStatus">
                                                <option value="0">Select</option>
                                            </select>
                                            <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                <h4>Add New Marital Status</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>New Marital Status <em class="red-star">*</em></label>
                                                            <input class="form-control marital_status maritalstatus_src capital" name="marital_status" type="text" placeholder="Add New Marital Status">
                                                            <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                            <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                            <input type="button" class="grey-btn" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Hobbies
                                                <a class="pop-add-icon selectPropertyHobbies" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control" name="hobbies[]" multiple>
                                                <option value="0">Select</option>
                                            </select>
                                            <div class="add-popup" id="selectPropertyHobbies1">
                                                <h4>Add New Hobbies</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>New Hobbies <em class="red-star">*</em></label>
                                                            <input class="form-control hobbies_src capital" type="text" placeholder="Add New Hobbies" data_required="true">
                                                            <span class="red-star customError required " id="hobbies_src"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="hobbies[]">Save</button>
                                                            <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                            <input type="button" class="grey-btn" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Veteran Status
                                                <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control veteran_status" name="veteranStatus">
                                                <option value="0">Select</option>
                                            </select>
                                            <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                <h4>Add New VeteranStatus</h4>
                                                <div class="add-popup-body">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>New VeteranStatus <em class="red-star">*</em></label>
                                                            <input class="form-control veteran_src capital" type="text" placeholder="Add New VeteranStatus">
                                                            <span class="red-star customError required" id="veteran_src"></span>
                                                        </div>
                                                        <div class="btn-outer text-right">
                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                            <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                            <input type="button" class="grey-btn" value="Cancel">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class='multipleSsn' id="multiple_ssn_div_id">
                                                <label>SSN/SIN/ID</label>
                                                <input placeholder="SSN/SIN/ID" class="form-control add-input capital" type="text" id="ssn" name="ssn[]" style="margin-bottom: 14px;">
                                                <a class="add-icon ssn-remove-sign" href="javascript:;"
                                                   style="display:none"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                </a>
                                                <span class="ssnErr error red-star"></span>
                                                <a class="add-icon ssn-plus-sign" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--General Information tab ends here-->


                            <!--Contact Details tab starts here-->
                            <div class="form-outer" id="contactInfoDiv">
                                <div class="form-hdr">
                                    <h3>
                                        Contact Details
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-3">
                                        <label>Zip/Postal Code</label>
                                        <input class="form-control" value="10001" type="text" id="zipcode" name="zipcode" placeholder="Eg: 35801">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Country </label>
                                        <input class="form-control capital" type="text" id="country" name="country" placeholder="Eg: US">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>State / Province</label>
                                        <input class="form-control capital" type="text" id="state" name="state" placeholder="Eg: AL">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>City</label>
                                        <input class="form-control capital" type="text" id="city" name="city" placeholder="Eg: Huntsville">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address1</label>
                                        <input class="form-control capital address_field" maxlength="100" type="text" id="address1" name="address1" placeholder="Eg: Street address 1">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address2 </label>
                                        <input class="form-control capital address_field" maxlength="100" type="text" id="address2" name="address2" placeholder="Eg: Street address 2">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address3</label>
                                        <input class="form-control capital address_field" maxlength="100" type="text" id="address3" name="address3" placeholder="Eg: Street address 3">
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Address4</label>
                                        <input class="form-control capital address_field" type="text" maxlength="100" id="address4" name="address4" placeholder="Eg: Street address 4">
                                    </div>
                                    <div class="primary-owner-phone-row" id="primary-owner-phone-row-divId">
                                        <div class="form-outer">
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Phone Type</label>
                                                <select class="form-control" name="phone_type[]" id="phone_type">
                                                    <option value="">Select</option>
                                                    <option value="1">mobile</option>
                                                    <option value="2">work</option>
                                                    <option value="3">Fax</option>
                                                    <option value="4">Home</option>
                                                    <option value="5">Other</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Phone Number <em class="red-star">*</em></label>
                                                <input class="form-control phone_format" type="text" name="phone_number[]" placeholder="Phone Number">
                                                <span class="phone_numberErr error red-star"></span>
                                            </div>
                                            <div class="work_extension_div" style="display: none;">
                                                <div class="col-sm-1 col-md-1">
                                                    <label>Extension</label>
                                                    <input class="form-control number_only_for_extension" maxlength="15" type="text" id="other_work_phone_extension" name="other_work_phone_extension[]" placeholder="Eg: + 161">
                                                    <span class="other_work_phone_extensionErr error red-star"></span>
                                                </div>
                                                <!--  <div class="col-sm-3 col-md-3">
                                                      <label>Office/Work Extension <em class="red-star">*</em></label>
                                                      <input class="form-control phone_format" type="text" disabled id="work_phone_extension" name="work_phone_extension[]" placeholder="Eg: 154-175-4301">
                                                      <span class="work_phone_extensionErr error red-star"></span>
                                                  </div>-->
                                            </div>

                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Carrier <em class="red-star">*</em></label>
                                                <select class="form-control" name="carrier[]"></select>
                                                <span class="carrierErr error red-star"></span>
                                            </div>

                                            <!-- <div class="col-sm-3 col-md-3 countycodediv">
                                                 <label>Country Code</label>
                                                 <select class="form-control" name="country_code[]">
                                                     <option value="">Select</option>
                                                 </select>
                                                 <span class="term_planErr error red-star"></span>
                                             </div>
 -->
                                            <div class="col-sm-3 col-md-3 countycodediv">
                                                <label>Country Code</label>
                                                <select class="form-control add-input" name="country_code[]">
                                                    <option value="">Select</option>
                                                </select>
                                                <span class="term_planErr error red-star"></span>
                                                <a class="add-icon" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                                <a class="add-icon" href="javascript:;" >
                                                    <i class="fa fa-times-circle red-star" aria-hidden="true" style="display:none"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Add Note for this number </label>
                                        <div class="notes_date_right_div">
                                        <textarea class="form-control capital notes_date_right" type="textArea" placeholder="Add Note for this Phone Number" name="phone_number_note"></textarea>
                                        </div>
                                        </div>
                                    <div class="col-sm-3 col-md-3">
                                        <div class='multipleEmail' id="multiple_email_div_id" style="margin-bottom: 10px;float: left; width: 100%;">
                                            <label>Email <em class="red-star">*</em></label>
                                            <input class="form-control add-input" type="text" placeholder="Email" name="email[]">
                                            <a class="add-icon email-remove-sign" href="javascript:;">
                                                <i class="fa fa-times-circle red-star" aria-hidden="true"  style="display: none"></i>
                                            </a>
                                            <a class="add-icon email-plus-sign" href="javascript:;" style="margin-top: 0px;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 apx-inline-popup">
                                        <label>Referral Source
                                            <a class="pop-add-icon additionalReferralResource" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a>
                                        </label>
                                        <select class="form-control" name="referral_source">
                                            <option value="">Select</option>
                                            <!--<option value="1">Bond</option>
                                            <option value="2">Certification</option>-->
                                        </select>
                                        <div class="add-popup" id="additionalReferralResource1">
                                            <h4>Add New Referral Source</h4>
                                            <div class="add-popup-body">
                                                <div class="form-outer">
                                                    <div class="col-sm-12">
                                                        <label>New Referral Source</label>
                                                        <input class="form-control reff_source1 capital" type="text" placeholder="New Referral Source">
                                                        <span class="customError required red-star" id="reff_source1"></span>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="referral_source">Save</button>
                                                        <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                        <input type="button" class="grey-btn" value="Cancel">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Contact Details tab ends here-->

                            <!--Emergency Contact Details tab starts here-->
                            <div class="form-outer" id="emergencyInfoDiv">
                                <div class="form-hdr">
                                    <h3>
                                        <strong class="left">Emergency Contact Details</strong>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="owner-emergency-contact" id="owner-emergency-contact_div_id">
                                        <div class="col-sm-2">
                                            <label>Emergency Contact Name</label>
                                            <input placeholder="Emergency Contact Name" class="form-control capital" type="text" id="emergency" name="emergency_contact_name[]" maxlength="50">
                                        </div>
                                        <div class="col-sm-2">
                                            <label>Relation</label>
                                            <select class="form-control" id="relationship"
                                                    name="emergency_relation[]">
                                                <option value="">Select</option>
                                                <option value="1">Brother</option>
                                                <option value="2">Daughter</option>
                                                <option value="3">Employer</option>
                                                <option value="4">Father</option>
                                                <option value="5">Friend</option>
                                                <option value="6">Mentor</option>
                                                <option value="7">Mother</option>
                                                <option value="8">Neighbor</option>
                                                <option value="9">Nephew</option>
                                                <option value="10">Niece</option>
                                                <option value="11">Owner</option>
                                                <option value="12">Partner</option>
                                                <option value="13">Sister</option>
                                                <option value="14">Son</option>
                                                <option value="15">Spouse</option>
                                                <option value="16">Teacher</option>
                                                <option value="17">Other</option>

                                            </select>
                                        </div>
                                        <div class="col-sm-2" id="otherRelationDiv" style="display: none;">
                                            <label>Other Relation</label>
                                            <input placeholder="Other Relation" class="form-control capital" type="text" id="emergency_other_relation" name="emergency_other_relation[]" maxlength="50">
                                        </div>
                                        <div class="col-sm-2 countycodediv">
                                            <label>Country Code</label>
                                            <select name="emergency_country[]" class="form-control emergencycountry">
                                                <option value="0">Select</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-2">
                                            <label>Phone</label>
                                            <input class="form-control" placeholder="Phone Number" type="text" id="phoneNumber" name="emergency_phone[]" maxlength="12">
                                        </div>

                                        <div class="col-sm-2">
                                            <label>Email</label>
                                            <input class="form-control add-input" placeholder="Email" type="text" id="email1" name="emergency_email[]" maxlength="50">
                                            <a class="add-icon add-emergency-contant" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <!--Emergency Contact Details tab ends here-->
                            <!--Emergency Contact Details tab ends here-->

                            <!--Banking Information tab starts here-->
                            <div class="form-outer" id="bankingInfoDiv">
                                <div class="form-hdr">
                                    <h3>
                                        Banking Information
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="row divBankAccount">
                                        <div class="col-sm-11">
                                            <div class="col-sm-3 apx-inline-popup">
                                                <label>Account Name <a class="pop-add-icon add_owner_account_name" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                <select name="account_name[]" class="form-control account_name">
                                                    <option value="0">Select</option>
                                                </select>
                                                <!--<div class="add-popup" id="account_name" style="display: none;">
                                                    <h4>Add New Account Name</h4>
                                                    <div class="add-popup-body">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Account Name <em class="red-star">*</em></label>
                                                                <input class="form-control owner_account_name capital" type="text" placeholder="Add New Account Name">
                                                                <span class="red-star customError required" id="owner_account_name"></span>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" id="ownerAccountNameBtn" class="blue-btn add_owner_account_name" data-table="owner_account_name" data-cell="account_name" data-class="owner_account_name" data-name="account_name[]">Save</button>
                                                                <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                                <input type="button" class="grey-btn" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
-->
                                                <div class="add-popup" id="account_name" style="display: none;">
                                                    <h4>Add New Account Name</h4>
                                                    <div class="add-popup-body">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Account Name <em class="red-star">*</em></label>
                                                                <input class="form-control owner_account_name capital" type="text" placeholder="Add New Account Name">
                                                                <span class="customError required red-star" id="owner_account_name"></span>
                                                            </div>
                                                            <div class="btn-outer text-right">
                                                                <button type="button" id="ownerAccountNameBtn" class="blue-btn add_owner_account_name" data-table="owner_account_name" data-cell="account_name" data-class="owner_account_name" data-name="account_name[]">Save</button>
                                                                <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                                <input type="button" class="grey-btn" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Acc. for ACH Transaction <a  data-backdrop="static" data-toggle="modal" id="chartAccountId"  data-target="#chartofaccountmodal" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                <select class="form-control bankChartsOfAccounts" name='account_for_transaction[]' id='account_for_transaction'>
                                                    <option value=''>Select</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Account Type </label>
                                                <select class="form-control" name="bank_account_type[]">
                                                    <option value="">Select</option>
                                                    <option value="1">Business Checking Account</option>
                                                    <option value="2">Personal Checking Account</option>
                                                    <option value="3">Business Savings Account</option>
                                                    <option value="4">Personal Savings Account</option>
                                                    <option value="5">Credit</option>
                                                    <option value="6">Undeposited Funds</option>
                                                    <option value="7">Other</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Account Number</label>
                                                <input placeholder="Bank Account Number" name="bank_account_number[]" class="hide_copy form-control capital number_only" maxlength="16" type="text" />
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Routing / Transit Number</label>
                                                <input placeholder="Bank Routing / Transit Number" name="routing_transit_number[]" class="hide_copy form-control capital" maxlength="16" type="text" />
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Institution Name</label>
                                                <input name="bank_institution_name[]" placeholder="Bank Institution Name" class="hide_copy form-control capital" maxlength="100" type="text" />
                                            </div>
                                            <div class="col-sm-3">
                                                <label>Bank Fraction Number</label>
                                                <input name="bank_fraction_number[]" placeholder="Bank Fraction Number" class="hide_copy form-control capital" maxlength="20" type="text" />
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <a class="add-icon add-chart-bank" href="javascript:;">
                                                <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            <a class="add-icon remove-chart-bank" style="display:none;"><i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="draw_payment_owner">
                                            <label>Draw Payment Method</label>
                                            <div class="check-outer">
                                                <input type="radio" value="check" name="draw_payment_method" />
                                                <label>Check</label>
                                            </div>
                                            <div class="check-outer">
                                                <input type="radio" value="eft" name="draw_payment_method" checked/>
                                                <label>EFT</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Banking Information tab ends here-->

                            <!--Tax Filling Information tab starts here-->
                            <div class="form-outer" id="taxInfoDiv">
                                <div class="form-hdr">
                                    <h3>
                                        Tax Filling Information
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-3">
                                        <label>Tax Payer Name</label>
                                        <input class="form-control capital" maxlength="50" id="tax_payer_name" name="tax_payer_name" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label>Tax Payer ID</label>
                                        <input class="form-control capital" maxlength="100" type="text" id="tax_payer_id" name="tax_payer_id"/>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Eligible for 1099</label>
                                        <div class="col-sm-5 p-0">
                                            <input type="radio" id="eligible_1099_yes" name="eligible_1099" value="1" checked />Yes
                                        </div>
                                        <div class="col-sm-5">
                                            <input type="radio" id="eligible_1099_no" name="eligible_1099" value="0"/>No
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Send 1099</label>
                                        <div class="check-outer">
                                            <input type="checkbox" name="send_1099" id="send_1099"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 company_name_as_tax_payer_div" style="display: block;">
                                        <div class="check-outer">
                                            <input type="checkbox" name="company_name_as_tax_payer" checked id="company_name_as_tax_payer"/>
                                            Use Entity/Company Name as Tax Payer Name
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Tax Filling Information tab ends here-->

                            <!--Properties owned tab starts here-->
                            <div class="form-outer" id="propertyOwnedDiv">
                                <div class="form-hdr">
                                    <h3>
                                        Properties Owned
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <a class="select_property_popup" id="ancCreateNewProperty" data-toggle="modal" data-backdrop="static" data-target="#selectProperty">
                                            Create New Property
                                        </a>
                                    </div>
                                    <div class="row owner_property_owned_outer" id="div_add_owner">
                                        <div class="owner_property_owned_lft">
                                            <div class="row">
                                                <div class="col-sm-7">
                                                    <label>Property  Name  <em class="red-star">*</em></label>
                                                    <select class="form-control property_owned_name" name="property_id[]" id="property_owned_id">
                                                        <option value="">Select</option>
                                                    </select>
                                                    <span class="property_idErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <label>Property Owned</label>
                                                    <input class="form-control property_percent_owned" type="text" name="property_percent_owned[]" id="property_percent_owned" value="100"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owner_property_owned_rt">
                                            <a class="pop-add-icon add_property_div_plus" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            <a class="pop-add-icon remove_property_div_minus" style="display: none;" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Properties owned tab ends here-->

                            <!--Owner's Package tab starts here-->
                            <div class="form-outer" id="ownersPackageInfoDiv">
                                <div class="form-hdr">
                                    <h3>Owner's Package</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="form-outer">
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <label>Owner's Portal</label>
                                                    <div class="col-sm-3 p-0">
                                                        <input type="radio" name="owners_portal" id="owners_portal_Yes" value="1" checked/>Yes
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="radio" name="owners_portal" id="owners_portal_No" value="0"/>No
                                                    </div>
                                                    <span class="portalErr error red-star"></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Send Owner's Package</label>
                                                    <div class="col-sm-2 p-0">
                                                        <input type="radio" name="send_owners_package" id="send_owners_package_Fax" value="1" />Fax
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="radio" name="send_owners_package" id="send_owners_package_Email" value="0" checked/>Email
                                                    </div>
                                                    <span class="portalErr error red-star"></span>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-2">
                                                    <div class="check-outer">
                                                        <input type="checkbox" name="hold_owners_payments" id="hold_owners_payments"/>
                                                        <label>
                                                            Hold Owner's Payments
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="check-outer">
                                                        <input type="checkbox" name="email_financial_info" id="email_financial_info" checked/>
                                                        <label>
                                                            Email Statement & Other Financial Info
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="check-outer">
                                                        <label>
                                                            Include Reports
                                                        </label>
                                                        <select class="form-control" name="include_reports" id="include_reports">
                                                            <option value="Select">Select</option>
                                                            <option value="1" selected>Yes</option>
                                                            <option value="0">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Owner's Package tab ends here-->

                            <!--Owner Credential Control tab starts here-->
                            <div class="form-outer" id="credentialControlInfoDiv">
                                <div class="form-hdr">
                                    <h3>Owner Credential Control</h3>
                                </div>
                                <div class="form-data">
                                    <div class="owner-credentials-control" id="owner-credentials-control_divId">
                                        <div class="form-outer">
                                            <div class="credential-clone">
                                                <div class="col-sm-2">
                                                    <label>Credential Name</label>
                                                    <input class="form-control capital" type="text" maxlength="30" id="credentialName" name="credentialName[]">
                                                </div>
                                                <div class="col-sm-2 credintials">
                                                    <label>Credential Type
                                                        <a class="pop-add-icon ownerCredentialType" href="javascript:;">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                        </a>
                                                    </label>
                                                    <select class="form-control" name="credentialType[]">
                                                        <option value="">Select</option>
                                                        <option value="1">Bond</option>
                                                        <option value="2">Certification</option>
                                                    </select>
                                                    <div class="add-popup" id="ownerCredentialType1" style="width: 127%;">
                                                        <h4>Add Credential Type</h4>
                                                        <div class="add-popup-body">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12">
                                                                    <label>Credential Type <em class="red-star">*</em></label>
                                                                    <input class="form-control credential_source capital" type="text" placeholder="Ex: License">
                                                                    <span class="red-star" id="credential_source"></span>
                                                                </div>
                                                                <div class="btn-outer">
                                                                    <button type="button" class="blue-btn add_single1" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                    <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Acquire Date</label>
                                                    <input class="form-control acquireDateClass calander" readonly type="text" id="acquireDate" name="acquireDate[]" placeholder="Eg: 10/10/2014">
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Expiration Date</label>
                                                    <input class="form-control expirationDateClass calander" readonly  type="text" id="expirationDate" name="expirationDate[]" placeholder="Eg: 10/10/2014">
                                                </div>
                                                <div class='notice-period'>
                                                    <div class="col-sm-2">
                                                        <label>Notice Period</label>
                                                        <select class="form-control add-input" id="term_plan" name="noticePeriod[]">
                                                            <option value="">Select</option>
                                                            <option value="1">5 day</option>
                                                            <option value="2">1 Month</option>
                                                            <option value="3">2 Month</option>
                                                            <option value="4">3 Month</option>
                                                        </select>
                                                        <a class="add-icon add-notice-period" href="javascript:;">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                        </a>
                                                        <a class="add-icon remove-notice-period" href="javascript:;" style="display: none;">
                                                            <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Owner Credential Control tab ends here-->

                            <!-- Flag Bank Tab Starts -->
                            <div class="form-outer" id="flagsBankInfoDiv">
                                <div class="form-hdr">
                                    <h3>Flag Bank</h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-12 new_flag_btn_div">
                                        <div class="btn-outer text-right">
                                            <button id="new_flag" type="button" class="blue-btn">New Flag</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12" id="flag_form_div" style="display: none;">
                                            <div class="grey-box-add">
                                                <div class="form-outer">
                                                    <input type="hidden" name="id" id="flag_id">
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Flagged By</label>
                                                        <input class="form-control capital" name="flag_by" id="flag_flag_by" placeholder="Flagged By" maxlength="100" type="text" value="<?php echo $_SESSION[SESSION_DOMAIN]['name'] ?>"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Date</label>
                                                        <input class="form-control" name="date" id="flag_flag_date" readonly value="" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Flag Name</label>
                                                        <input class="form-control capital" name="flag_name" id="flag_flag_name" maxlength="100" placeholder="Please Enter the Name of this Flag" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Country Code</label>
                                                        <select class='form-control' name='flag_country_code' id="flag_country_code">
                                                            <?php
                                                            foreach ($countryArray as $code => $country) {
                                                                $countryName = ucwords(strtolower($country["name"])); // Making it look good
                                                                echo "<option value='" . $code . "' " . (($code == 'US') ? "selected" : "") . ">" . $code . " - " . $countryName . " (+" . $country["code"] . ")</option>";
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Phone Number</label>
                                                        <input class="form-control phone_number_format" name="flag_phone_number" id="flag_phone_number" maxlength="12" value="<?php echo $_SESSION[SESSION_DOMAIN]['phone_number'] ?>" placeholder="123-456-7890" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Flag Reason</label>
                                                        <input class="form-control capital" name="flag_reason" id="flag_flag_reason" maxlength="100" placeholder="Flag Reason" type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Flagged For</label>
                                                        <input class="form-control capital" readonly id="flagged_for" maxlength="100"  type="text"/>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <label>Completed</label>
                                                        <select class='form-control' name='completed' id="completed">
                                                            <option value="0">No</option>
                                                            <option value="1">Yes</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 notesDateTime">
                                                        <label>Note</label>
                                                        <div class="notes_date_right_div">
                                                            <textarea class="form-control notes_date_right capital" name="flag_note" id="flag_note" placeholder="Flag Notes"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="btn-outer text-right">
                                                        <button class="blue-btn" type="button" id="flagSaveBtnId">Save</button>
                                                        <input type="button" class="clear-btn" id="clearAddFlagForm" value="Clear">
                                                        <button class="grey-btn" type="button" id="flagCancel">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="grid-outer">
                                            <div class="table-responsive">
                                                <div class="apx-table ">
                                                    <div class="table-responsive">
                                                        <table id="ownerFlags-table" class="table table-bordered"></table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Flag Bank Tab Ends -->

                            <!--Owner Complaints tab ends here-->
                            <div class="form-outer" id="complaintInfoDiv">
                                <div class="form-hdr">
                                    <h3>Owner Complaint</h3>
                                </div>

                                <div class="form-data">
                                    <div class="new_complaint_btn_div">
                                        <div class="col-sm-2">
                                            <select class="form-control" id="complaint_by_about_status"  name="complaint_by_about">
                                                <option value="all">All</option>
                                                <option value="Complaint about Owner">Complaint about Owner</option>
                                                <option value="Complaint by Owner">Complaint by Owner</option>
                                            </select>
                                        </div>

                                        <div class="col-sm-10 text-right">
                                            <button id="new_complaint_btn" type="button" class="blue-btn">New Complaint</button>
                                        </div>
                                    </div>
                                    <div style="display:none" id="new_complaint_div">
                                        <input type="hidden" name="edit_complaint_id" id="edit_complaint_id" />
                                        <div class="complaint_by_about_owner">
                                            <div class="check-outer">
                                                <input type="radio" value="Complaint by Owner" id="complaint_by_owner" name="complaint_by_about" />
                                                <label>Complaint By Owner</label>
                                            </div>
                                            <div class="check-outer">
                                                <input type="radio" value="Complaint about Owner" id="complaint_about_owner" name="complaint_by_about"/>
                                                <label>Complaint About Owner</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3">
                                                    <label>Complaint ID</label>
                                                    <input class="form-control" name="complaint_id" id="complaint_id" maxlength="10"  placeholder="Eg: AB034C " type="text">
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Complaint Date </label>
                                                    <input class="form-control" id="complaint_date" name="complaint_date"  readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Complaint Type<a id="complaintPopupIcon" class="pop-add-icon" href="javascript:;"> <i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                    <select class='form-control' id='complaint_type_options' name='complaint_type_options'></select>
                                                    <div class="add-popup" id='ComplaintTypePopup'>
                                                        <h4>Add New Complaint Type</h4>
                                                        <div class="add-popup-body">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12">
                                                                    <label>Complaint Type<em class="red-star">*</em></label>
                                                                    <input class="form-control capital customValidateComplaint" id="complaint_type_name" data_required="true" placeholder="Complaint Type" type="text"  name='@complaint_type'/>
                                                                    <span class="customError required"></span>
                                                                </div>
                                                                <div class="btn-outer text-right">
                                                                    <button type="button"  class="blue-btn" id="NewpetComplaintSave">Save</button>
                                                                    <input type="button" class="clear-btn clear_single_field" value="Clear">
                                                                    <button type="button"  class="grey-btn" id="NewComplaintcancel" value='Cancel' >Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 notesDateTime" id="other_notes_div" style="display:none">
                                                    <label>Other Notes</label>
                                                    <textarea class="form-control capital"  id="other_complaint_notes" name="other_notes"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-3 notesDateTime">
                                                    <label>Complaint Notes</label>
                                                    <div class="notes_date_right_div">
                                                        <textarea class="form-control notes_date_right capital"  id="complaint_note" name="complaint_note"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mg-lt mt-15 text-right">
                                            <button type="button" class=" blue-btn" id="complaint_save_btn">Save</button>
                                            <input type="button" class="clear-btn" id="clearAddComplaintForm" value="Clear">
                                            <button type="button" class=" grey-btn" id="complaint_cancel">Cancel</button>
                                        </div>
                                    </div>

                                    <div class="mt-15 col-sm-12">
                                        <div class="grid-outer">
                                            <div class="table-responsive">
                                                <div class="apx-table listinggridDiv">
                                                    <div class="table-responsive">
                                                        <table id="ownerComplaints-table" class="table table-bordered"></table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 print_email_owner_btn">
                                        <div class="btn-outer" align="right">
                                            <a type="button" class="blue-btn" id="print_email_button" >Print/Email</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Owner Complaints Ends -->


                            <!--Notes tab starts here-->
                            <div class="form-outer" id="noteInfoDiv">
                                <div class="form-hdr">
                                    <h3>
                                        Notes
                                    </h3>
                                </div>
                                <div class="form-data notesDateTime">
                                    <div class="row ownerTxtArea" id="ownerTxtAreaDivId">
                                        <div class="col-sm-4">
                                            <div class="notes_date_right_div">
                                                <textarea placeholder="Notes" class="notes form-control capital notes_date_right" maxlength="500" rows="6" name="notes[]"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <a class="add-icon add-icon-textarea" href="javascript:;">
                                                <i class="fa fa-plus-circle"  aria-hidden="true"></i>
                                            </a>
                                            <a class="add-icon remove-icon-textarea" href="javascript:;" style="display: none;">
                                                <i class="fa fa-times-circle red-star" aria-hidden="true" style="display: inline;"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Notes tab ends here-->

                            <!--File Library tab starts here-->
                            <div class="form-outer" id="fileLibraryIfoDiv">
                                <div class="form-hdr">
                                    <h3>
                                        File Library
                                    </h3>
                                </div>
                                <div class="form-data">

                                    <div class="form-outer2">
                                        <div class="col-sm-12 text-right">
                                            <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                            <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                            <button class="orange-btn" id="remove_library_file">Remove All Files</button>
                                            <button type="button" class="blue-btn" style="" id="SaveAllimagesFileOwner">Save                                 </button>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="row" id="file_library_uploads">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-outer listinggridDiv">
                                        <div class="apx-table apxtable-bdr-none">
                                            <div class="table-responsive">
                                                <table id="file-library" class="table table-bordered"></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--File Library tab ends here-->

                            <!--Custom Fields tab starts here-->
                            <div class="form-outer" id="customFieldsInfoDiv">
                                <div class="form-hdr">
                                    <h3 id="headerDivCustom">
                                        Custom Fields
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <input type="hidden" name="id" id="record_id">
                                    <div class="col-sm-12" style="margin-left: 15px;">
                                        <div class="collaps-subhdr">
                                        </div>
                                        <div class="row">
                                            <div class="custom_field_html">
                                            </div>
                                            <div class="col-sm-6 custom_field_msg">
                                                No Custom Fields
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="btn-outer text-right">
                                                    <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Custom Fields tab ends here-->

                            <div class="btn-outer text-right">
                                <?php
                                //echo '<prE>'; print_r(); echo '</prE>';
                                ?>
                                <input type="hidden" value="<?php echo $_SESSION[SESSION_DOMAIN]['email']; ?>" class="current_admin_email">
                                <button class="blue-btn" id="ownerSaveForm">Update</button>
                                <button type="reset" id="reset_edit_owner_btn" class="grey-btn">Reset</button>
                                <button type="button" id="cancel_add_owner_btn" class="grey-btn mg-rt-30">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<!-- Wrapper Ends -->


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->

<script type="text/javascript">
    $('#people_top').addClass('active');
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var default_login_user_name = "<?php echo $_SESSION[SESSION_DOMAIN]['name']; ?>";
    var default_login_user_phone_number = "<?php echo $_SESSION[SESSION_DOMAIN]['phone_number']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_zipcode = "<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode']; ?>";
</script>
<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<!--<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />-->
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/bootstrap-tagsinput.css" />
<!--<script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap-tagsinput.min.js"></script>
<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/company/people/owner/addOwner.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/addOwner.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/editOwner.js"></script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerpopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/file_library.js" type="text/javascript"></script>

<!-- flag js -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/flag/flagValidation.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/flagOwner.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/complaintsOwner.js"></script>

<script>

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });

    <!--- Main Nav Responsive -->
    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

</script>

<script>
    $('.company-top').addClass('active');
    $('.cropItData').hide();
    jQuery('.phone_format').mask('000-000-0000', {reverse: true});
    jQuery('input[name="pet_phoneNumber[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="service_phoneNumber[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="additional_phone[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="guarantor_phone_1[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});

    $('#acquireDate').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat})
        .datepicker("setDate", new Date());

    $('#expirationDate').datepicker({yearRange: '-0:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, minDate:  new Date(),})
        .datepicker("setDate", new Date());


    $('.calander').datepicker({
        yearRange: '-100:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="dob"]').datepicker({
        yearRange: '-100:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(".calander").val(date);


    $('.calander1').datepicker({
        yearRange: '-100:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('.calander1').datepicker({dateFormat: jsDateFomat});
    var date = $.datepicker.formatDate(jsDateFomat, new Date());

    $(".calander1").val(date);
</script>
<style>
    select[name=hobbies] + .btn-group button{
        margin-top: 0px;
    }
    .closeimagepopupicon{
        float: right;
        font-weight: bold;
        border: 2px solid #666;
        border-radius: 12px;
        padding: 0px 4px;
        cursor: pointer;
    }

    .apx-inline-popup {
        position: relative;
    }
    .apx-inline-popup-box > h4 {
        font-size: 12px
    }

    .image-editor {
        text-align: center;
    }

    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin-top: 7px;
        width: 250px;
        height: 250px;
        display: inline-block;
    }

    .image-size-label {
        margin-top: 10px;
    }

    .export {
        /* Use relative position to prevent from being covered by image background */
        position: relative;
        z-index: 10;
        display: block;
    }

    .image-editor input[type="file"] {
        opacity: 0;
        position: absolute;
        top: 45px;
        left: 91px;
    }

    .upload-logo .img-outer img {
        max-width: 100%;
    }


</style>

<script>
    $(document).ready(function(){
        $('.summernote').summernote({
            addclass: {
                debug: false,
                classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
            },
            width: '100%',
            height: '300px',
            //margin-left: '15px',
            toolbar: [
                // [groupName, [list of button]]
                ['img', ['picture']],
                ['style', ['style', 'addclass', 'clear']],
                ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
                ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
                ['extra', ['video', 'table', 'height']],
                ['misc', ['undo', 'redo', 'codeview', 'help']]
            ]
        });
    });
</script>
<!-- Jquery Ends -->
</body>
</html>