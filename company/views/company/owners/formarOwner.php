<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/starability-all.css">
<div id="wrapper">
    <!-- Top navigation start -->
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php"); ?>
    <!-- Top navigation end -->

    <style>
        .closeRating{
            font-size: 21px !important;
            color: #fff !important;
            font-weight: 500 !important;
            border: none !important;
            margin-right: -8px;
        }
    </style>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/shorttermcss.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-pseudo-ripple.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-nao-calendar.css">
    <link rel="stylesheet" href="https://unpkg.com/balloon-css/balloon.min.css">
    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">


                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">

                    <div class="main-tabs">


                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="" id="people-vendor">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse">
                                                <span></span> Former Owner</a> <a class="back" style="float: right;" href="/People/Ownerlisting">&lt;&lt; Back</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                        <div class="panel-body">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="apx-table">
                                                                                <div class="table-responsive overflow-unset">
                                                                                    <table id="owner_listing" class="table table-bordered">
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!--Tabs Ends -->
                </div>
            </div>
        </div>
    </section>
</div>














<!--Star Modal End-->

<!-- Wrapper Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script>


    $("#people_top").addClass("active");
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->
    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        jqGrid('all');
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";


    function jqGrid(status, deleted_at) {

        var table = 'users';
        var columns = ['Owner Name','Phone', 'Email','Address','Properties Owned','Active Date','created_at','InActive Date','updated_at','Length of Time','Reason For Leaving'];
        var select_column = [''];
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'owner_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'status',value:'4',condition:'=',table:'owner_details'}];
        var columns_options = [
            {name:'Owner Name',index:'name',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter: ownerName,classes: 'pointer',attr:[{name:'flag',value:'owner'}]},
            {name: 'Phone', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor',change_type:'line_multiple',index2:'phone_number',index3:'other_work_phone_extension',join:{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'id',formatter:addToolTip,cellattr:cellAttri}, /**cellattr:cellAttrdata**/
            {name:'Email',index:'email', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor'},
            {name:'Address',index:'address1', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor'},
            {name:'Properties Owned',index:'id', width:80, align:"center",searchoptions: {sopt: conditions},change_type:'property_owner',table:table, classes: 'cursor'},
            {name:'Active Date',index:'created_at', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor'},

            {name: 'created_at', index: 'created_at',hidden:true, width: 80, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',change_type:'date'},

            {name:'InActive Date',index:'updated_at', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor'},
            {name: 'updated_at', index: 'updated_at',hidden:true,width: 100, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',change_type:'date'},

            {name:'Length of Time',index:'created_at', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor',formatter:dayFormatter},
            {name:'Reason For Leaving',index:'reason_for_leaving', width:80, align:"center",searchoptions: {sopt: conditions},table:'owner_details', classes: 'cursor'},

        ];
        var ignore_array = [];

        jQuery("#owner_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'name',
            sortorder: 'desc',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Former Owners",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }
    function ownerName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }
    }
    function addToolTip(cellValue, options, rowObject) {
        if(rowObject !== undefined){
            if(rowObject.Phone_Note == ''){
                return cellValue;
            } else {
                return '<div class="tooltipgridclass">'+cellValue+'<span class="tooltiptextbotclass">'+rowObject.Phone_Note+'</span></div>';
            }
        }
    }
    function cellAttri(cellValue, options, rowObject) {
        if(rowObject !== undefined){

            if(rowObject.Phone_Note != ''){
                return 'title = " "';
            }
        }
    }
    function dayFormatter(cellValue, options, rowObject)
    {
        if(rowObject!==undefined)
        {
            var today = rowObject.created_at;
            var lastDay = rowObject.updated_at;
            const date1 = new Date(today);
            const date2 = new Date(lastDay);
            const diffTime = Math.abs(date2.getTime() - date1.getTime());
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            return diffDays+' days';
        }
    }





</script>

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
