<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once(COMPANY_DIRECTORY_URL . "/views/company/owners/modals.php");
?>
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/shorttermcss.css">
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-pseudo-ripple.css">
<link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery-nao-calendar.css">
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content owner-listing-page">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                People &gt;&gt; <span>Owner Listing</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--Right Navigation Link Starts-->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>PEOPLE</h2>
                            <div class="list-group panel">
                                <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed" id="moveout_Search">Move Out</a>
                                <a id ="shorttermrentals" href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                <a href="/People/AddOwners" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                <a href="/Vendor/AddVendor" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                <a href="/People/AddEmployee" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                <a href="/People/AddContact" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                <a href="/Tenant/Receivebatchpayment" class="list-group-item list-group-item-success strong collapsed">Receive Payment</a>
                                <a href="/Tenant/TenantStatements" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                <a href="/Tenant/BatchRegister" class="list-group-item list-group-item-success strong collapsed">Bank Deposit</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                <a href="/Tenant/InstrumentRegister" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Transfer</a>
                                <a href="/Tenant/Transfer" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                <a href="/Lease/Movein" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                <a href="/Tenant/MoveOut" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                <a href="/Tenant/FormarTenant" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                            </div>
                        </div>
                    </div>
                    <!--Right Navigation Link Ends-->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/Tenantlisting/Tenantlisting">Tenants</a></li>
                            <li role="presentation" class="active"><a href="/People/Ownerlisting">Owners</a></li>
                            <li role="presentation"><a href="/Vendor/Vendor">Vendors</a></li>
                            <li role="presentation"><a href="/People/ContactListt">Contacts</a></li>
                            <li role="presentation"><a href="/People/GetEmployeeList">Employee</a></li>
                        </ul>
                        <div class="atoz-outer">
                               <span class="apex-alphabets" id="apex-alphafilter" style="display:none;">
                               <span class="AtoZ"></span></span>
                            <span class="AZ" id="AZ">A-Z</span>
                            <span id="allAlphabet" style="cursor:pointer;">All</span>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="regular-rent">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2 tenant_type_status">
                                            <label>Status</label>
                                            <select class="fm-txt form-control jqGridStatusClass"  id="jqgridOptions"  data-module="OWNERLISTING">
                                                <option value="All">All</option>
                                                <option value="1" selected>Active</option>
                                                <option value="2">Archive</option>
                                                <option value="4">Former Owner</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-10">
                                        <div class="btn-outer text-right mg-lt">
                                            <a href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/excel/OwnerSample.xlsx" download class="blue-btn">Download Sample</a>
                                            <button class="blue-btn" id="import_owner">Import Owner</button>
                                            <a class="blue-btn" id="export_owner">Export Owner</a>
                                            <a class="blue-btn" href="/People/AddOwners">New Owner</a>
                                        </div>
                                        </div>
                                    </div>
                                </div>

                                <!--Import owner tab starts here-->
                                <div  style="display: none;" id="import_owner_div">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3 id="headerDivCustom">
                                                Import Owner
                                            </h3>
                                        </div>
                                        <div class="form-data">
                                            <input type="hidden" name="id" id="record_id">
                                            <form name="importOwnerTypeFormId" id="importOwnerTypeFormId" >
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                            <span class="error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="btn-outer text-right">
                                                        <button type="submit" class="blue-btn">Submit</button>
                                                        <button type="button" class="clear-btn" id="clearImportForm" >Clear</button>
                                                        <button type="button" id="import_owner_cancel_btn" class="grey-btn">Cancel</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!--Import owner tab ends here-->

                            </div>
                            <div class="accordion-grid">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">

                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive overflow-unset">
                                                                <div class="grid-outer">
                                                                    <div class="apx-table">
                                                                        <table class="table table-hover table-dark" id="owner_listing">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Regular Rent Ends -->
                        <div role="tabpanel" class="tab-pane" id="shortterm-rent">

                        </div>
                    </div>
                </div>
                <!--Tabs Ends -->

            </div>
        </div>
</div>

<div class="overlay">
    <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >
        <img width="200"  height="200" src='<?php echo COMPANY_SUBDOMAIN_URL ?>/images/loading.gif'/>
    </div>
</div>
</section>
</div>
<!-- Wrapper Ends -->


<!--    <div class="container">-->
<!--        <div class="overlay">-->
<!--            <div id='loadingmessage' style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'; >-->
<!--                <img width="200"  height="200" src='--><?php //echo COMPANY_SUBDOMAIN_URL ?><!--/images/loading.gif'/>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="modal fade" id="financial-info" role="dialog">-->
<!--            <div class="modal-dialog modal-md" style="width: 60%;">-->
<!--                <div class="modal-content" style="width: 100%;">-->
<!--                    <div class="modal-header">-->
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
<!--                        <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>-->
<!--                        </a>-->
<!---->
<!--                        <h4 class="modal-title">Online Payment Details</h4>-->
<!--                    </div>-->
<!--                    <div class="modal-body">-->
<!--                        <div class="steps">-->
<!--                            <ul>-->
<!--                                <li class="payment_li">-->
<!--                                    <a id="paymentDiv" class="paymentDiv active" href="javascript:;">1</a>-->
<!--                                    <span>Payment</span>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a id="basicDiv" class="basicDiv" href="javascript:;">2</a>-->
<!--                                    <span>Basic</span>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                        <div class="apx-adformbox-content">-->
<!--                            <form method="post" id="addVendorCardDetailsForm">-->
<!--                                <input type="hidden" id="stripe_vendor_id" value="">-->
<!--                                <input type="hidden" id="company_user_id" class="company_user_id" value="">-->
<!--                                <h3>Payment Method <em class="red-star">*</em></h3>-->
<!--                                <div class="row">-->
<!--                                    <div class="form-outer">-->
<!---->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label></label>-->
<!--                                            <select class="form-control" name="payment_method" id="payment_method">-->
<!--                                                <option value="1">Credit Card/Debit Card</option>-->
<!--                                                <option value="2">ACH</option>-->
<!---->
<!--                                            </select>-->
<!---->
<!--                                            <span class="ffirst_nameErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4 clear">-->
<!--                                            <label>Card Type<em class="red-star">*</em></label>-->
<!--                                            <img src="/company/images/card-payment.png" style="width: 190px;">-->
<!--                                            <span class="ffirst_nameErr error red-star"></span>-->
<!--                                        </div>-->
<!---->
<!--                                        <div class="col-sm-4 col-md-4 clear">-->
<!--                                            <label>Card Holder First Name <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&-->
<!--	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="ffirst_name" name="cfirst_name">-->
<!--                                            <span class="ffirst_nameErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Card Holder Last Name</label>-->
<!--                                            <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&-->
<!--	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="flast_name" name="clast_name">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Card Number <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control hide_copy" type="text" id="ccard_number" name="ccard_number" maxlength="16">-->
<!--                                            <span class="flast_nameErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label> Expiry Year <em class="red-star">*</em></label>-->
<!--                                            <select class="form-control"  id="cexpiry_year" name="cexpiry_year">-->
<!--                                            </select>-->
<!--                                            <span class="femailErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4 fphone_num">-->
<!--                                            <label> Expiry Month <em class="red-star">*</em></label>-->
<!--                                            <select class="form-control"  id="cexpiry_month" name="cexpiry_month">-->
<!--                                            </select>-->
<!--                                            <span class="fphone_numberErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>CVC<em class="red-star">*</em></label>-->
<!--                                            <input class="form-control capsOn" type="text" id="ccvv" name="ccvv" maxlength="5">-->
<!--                                            <span class="fbirth_dateErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Company <em class="red-star">*</em></label>-->
<!--                                            <input type="text" class="form-control capital" id="cCompany" name="cCompany" value="">-->
<!--                                            <span class="fzipcodeErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4 clear">-->
<!--                                            <label>Card Billing Phone Number <em class="red-star">*</em></label>-->
<!--                                            <input type="text" class="form-control phone_format" id="cphoneNumber" name="cphoneNumber" value="" >-->
<!--                                            <span class="fcityErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Card Billing Address Line 1 <em class="red-star">*</em></label>-->
<!--                                            <input type="text" class="form-control capital" id="caddress1" name="caddress1" value="">-->
<!--                                            <span class="fstateErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Card Billing Address Line 2</label>-->
<!--                                            <input type="text" class="form-control capital" id="caddress2" name="caddress2" value="">-->
<!--                                            <span class="faddressErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>City<em class="red-star">*</em></label>-->
<!--                                            <input class="form-control capsOn capital" type="text" id="ccity" name="ccity">-->
<!--                                            <span class="fbusinessErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>State/Province <em class="red-star">*</em></label>-->
<!--                                            <input type="text" class="form-control capital" id="cstate" name="cstate" value="">-->
<!--                                            <span class="fssnErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Zip/Postal Code<em class="red-star">*</em></label>-->
<!--                                            <input type="text" class="form-control" id="czip_code" name="czip_code" value="--><?php //$_SESSION[SESSION_DOMAIN]['default_zipcode'] ?><!--" >-->
<!--                                            <span class="faccount_holderErr error red-star"></span>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Country <em class="red-star">*</em></label>-->
<!--                                            <input type="text" class="form-control capital" id="ccountry" name="ccountry" value="">-->
<!--                                            <span class="faccount_holdertypeErr error red-star"></span>-->
<!--                                        </div>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="row" style="text-align: center;">-->
<!--                                    <button  class="blue-btn">Continue</button>-->
<!--                                </div>-->
<!---->
<!--                            </form>-->
<!--                        </div>-->
<!--                        <div class="basic-payment-detail" style="display: none;">-->
<!--                            <form id="ownerBasicPaymentDetails">-->
<!--                                <input type="hidden" class="company_user_id" name="user_id" value="">-->
<!--                                <div class="row">-->
<!--                                    <div class="form-outer">-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Country <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control capital" type="text" id="fcountry" name="country">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Account Type <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control" readonly value="Indiviual" type="text"  name="account_type">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Account Number <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control hide_copy" type="text" name="account_number">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Routing Number <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control" type="text" name="routing_number">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>City <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control capital" id="fcity" type="text" name="city">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Address Line1 <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control capital" type="text"  name="line1">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Address Line2 </label>-->
<!--                                            <input class="form-control capital" type="text" name="address_line2">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Postal Code <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control" type="text" id="fpostal_code" name="postal_code">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>State <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control capital"  id="fstate" type="text" name="state">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Email <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control" type="text" name="email">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>First Name <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control capital" type="text" name="first_name">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Last Name <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control capital" type="text" name="last_name">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>Phone <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control" type="text" name="phone">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-4 col-md-4">-->
<!--                                            <label>SSN Last 4 <em class="red-star">*</em></label>-->
<!--                                            <input class="form-control" type="text" name="ssn_last" maxlength="4">-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-12 date-outer-form">-->
<!--                                            <label> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>-->
<!---->
<!--                                            <div class="col-sm-12 col-md-3 fphone_num  pad-none">-->
<!--                                                <select class="form-control"  id="fpday" name="day">-->
<!--                                                    <option value="">Select</option>-->
<!--                                                </select>-->
<!--                                                <span class="fmonthErr error red-star"></span>-->
<!--                                            </div>-->
<!--                                            <div class="col-sm-12 col-md-3 fphone_num">-->
<!--                                                <select class="form-control"  id="fpmonth" name="month">-->
<!--                                                    <option value="">Select</option>-->
<!--                                                </select>-->
<!--                                                <span class="fmonthErr error red-star"></span>-->
<!--                                            </div>-->
<!--                                            <div class="col-sm-12 col-md-3 date-year">-->
<!--                                                <select class="form-control"  id="fpyear" name="year">-->
<!--                                                    <option value="">Select</option>-->
<!--                                                </select>-->
<!--                                                <span class="fyearErr error red-star"></span>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="col-sm-12 text-center">-->
<!--                                            <button type="submit"  id="savefinancial" class="blue-btn">Submit</button>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </form>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

<div class="modal fade" id="billing-subscription" role="dialog">
    <input type="hidden" id="business_type" name="business_type" value="">
    <input type="hidden" id="account_verification"  value="">
    <input type="hidden" id="stripe_vendor_id" value="">
    <input type="hidden" id="company_user_id" value="">
    <div class="modal-dialog modal-md" style="width: 60%;">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <a style="border: none;font-size: 21px;" class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                </a>

                <h4 class="modal-title">Online Payment Details<span class='userName'></span></h4>
            </div>
            <div class="modal-body">
                <div class="steps">
                    <ul>
                        <li class="payment_li">
                            <a id="paymentDiv" class="paymentDiv active" href="javascript:;">1</a>
                            <span >Payment</span>
                        </li>
                        <li class="basicDiv_li">
                            <a id="basicDiv" class="basicDiv" href="javascript:;">2</a>
                            <span class="basic_div_span">Basic</span>
                        </li>
                    </ul>
                </div>
                <div class="apx-adformbox-content">
                    <form method="post" id="financialCardInfo" name="financialCardInfo">
                        <h3>Payment Method <em class="red-star">*</em></h3>
                        <div class="row cards">
                            <div class="cardDetails table-responsive col-sm-12"></div>
                            <div class="form-outer">
                                <div class="col-sm-4 col-md-4">
                                    <label></label>
                                    <select class="form-control" name="payment_method" id="payment_method">
                                        <option value="1">Credit Card/Debit Card</option>
                                        <option value="2">ACH</option>
                                    </select>
                                    <span class="ffirst_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-4 col-md-4 clear">
                                    <label>Card Type<em class="red-star">*</em></label>
                                    <img src="/company/images/card-payment.png" style="width: 190px;">
                                    <span class="ffirst_nameErr error red-star"></span>
                                </div>

                                <div class="col-sm-4 col-md-4 clear">
                                    <label>Card Holder First Name <em class="red-star">*</em></label>
                                    <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="cfirst_name" name="cfirst_name">
                                    <span class="ffirst_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Card Holder Last Name</label>
                                    <input class="form-control capsOn capital" onkeypress="return (event.charCode > 64 &&
	event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type="text" id="clast_name" name="clast_name">
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>Card Number <em class="red-star">*</em></label>
                                    <input class="form-control capsOn hide_copy" type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="ccard_number" name="ccard_number" type="number" maxlength="16" placeholder="1234 1234 1234 1234">
                                    <span class="flast_nameErr error red-star"></span>
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label> Expiry Year <em class="red-star">*</em></label>
                                    <select class="form-control"  id="cexpiry_year" name="cexpiry_year" >
                                    </select>
                                    <span class="femailErr error red-star"></span>
                                </div>
                                <div class="col-sm-4 col-md-4 fphone_num">
                                    <label> Expiry Month <em class="red-star">*</em></label>
                                    <select class="form-control"  id="cexpiry_month" name="cexpiry_month" >
                                    </select>
                                    <span class="fphone_numberErr error red-star"></span>
                                </div>
                                <div class="col-sm-4 col-md-4">
                                    <label>CVC<em class="red-star">*</em></label>
                                    <input class="form-control capsOn" type="text" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="ccvv" name="ccvv" type="number" maxlength="4">
                                    <span class="fbirth_dateErr error red-star"></span>
                                </div>
                                <!--                                    <div class="col-sm-4 col-md-4">-->
                                <!--                                        <label>Company <em class="red-star">*</em></label>-->
                                <!--                                        <input type="text" class="form-control capital" id="cCompany" name="cCompany" value="">-->
                                <!--                                        <span class="fzipcodeErr error red-star"></span>-->
                                <!--                                    </div>-->
                                <!--                                <div class="col-sm-4 col-md-4 clear">-->
                                <!--                                    <label>Card Billing Phone Number <em class="red-star">*</em></label>-->
                                <!--                                    <input type="text" class="form-control phone_format" id="cphoneNumber" name="cphoneNumber" value="" >-->
                                <!--                                    <span class="fcityErr error red-star"></span>-->
                                <!--                                </div>-->
                                <!--                                <div class="col-sm-4 col-md-4">-->
                                <!--                                    <label>Card Billing Address Line 1 <em class="red-star">*</em></label>-->
                                <!--                                    <input type="text" class="form-control capital" id="caddress1" name="caddress1">-->
                                <!--                                    <span class="fstateErr error red-star"></span>-->
                                <!--                                </div>-->
                                <!--                                <div class="col-sm-4 col-md-4">-->
                                <!--                                    <label>Card Billing Address Line 2 </label>-->
                                <!--                                    <input type="text" class="form-control capital" id="caddress2" name="caddress2" value="">-->
                                <!--                                    <span class="faddressErr error red-star"></span>-->
                                <!--                                </div>-->
                                <!--                                <div class="col-sm-4 col-md-4">-->
                                <!--                                    <label>City<em class="red-star">*</em></label>-->
                                <!--                                    <input class="form-control capsOn capital" type="text" id="ccity" name="ccity" readonly>-->
                                <!--                                    <span class="fbusinessErr error red-star"></span>-->
                                <!--                                </div>-->
                                <!--                                <div class="col-sm-4 col-md-4">-->
                                <!--                                    <label>State/Province <em class="red-star">*</em></label>-->
                                <!--                                    <input type="text" class="form-control capital" id="cstate" name="cstate" value="" maxlength="4" readonly>-->
                                <!--                                    <span class="fssnErr error red-star"></span>-->
                                <!--                                </div>-->
                                <!--                                <div class="col-sm-4 col-md-4">-->
                                <!--                                    <label>Zip/Postal Code<em class="red-star">*</em></label>-->
                                <!--                                    <input type="text" class="form-control" id="czip_code" name="czip_code" value="--><?php //$_SESSION[SESSION_DOMAIN]['default_zipcode'] ?><!--" >-->
                                <!--                                    <span class="faccount_holderErr error red-star"></span>-->
                                <!--                                </div>-->
                                <!--                                <div class="col-sm-4 col-md-4">-->
                                <!--                                    <label>Country <em class="red-star">*</em></label>-->
                                <!--                                    <input type="text" class="form-control capital" id="ccountry" name="ccountry" value="" >-->
                                <!--                                    <span class="faccount_holdertypeErr error red-star"></span>-->
                                <!--                                </div>-->
                            </div>
                        </div>
                        <div class="row accounts" style="display:none;">
                            <div class="accountDetails table-responsive col-sm-12"></div>
                        </div>
                        <div class="row" style="text-align: center;">
                            <button type="submit"  id="savefinancialCard" class="blue-btn">Continue</button>
                        </div>

                    </form>
                </div>
                <div class="basic-payment-detail" style="display: none;">
                    <div class="row">

                        <form method="post" id="financialCompanyInfo">
                            <input type="hidden" id="company_document_id" name="company_document_id" value="">
                            <div class="form-outer">
                                <div class="col-sm-12 col-md-3">
                                    <label>Country<em class="red-star">*</em></label>
                                    <select class="form-control capital" name="fcountry" readonly="">
                                        <option value="usa" selected>USA</option>
                                    </select>
                                    <span class="fcountryErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>MCC <em class="red-star">*</em></label>
                                    <select class="form-control" id="fcmcc" name="fmcc" readonly="">
                                    </select>
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <label>URL <em class="red-star">*</em></label>
                                    <input class="form-control capsOn" type="text" id="fcurl" name="furl">
                                    <span class="furlErr error red-star"></span>
                                </div>

                                <div class="col-sm-12 col-md-3">
                                    <label>Business Type<em class="red-star">*</em></label>
                                    <input class="form-control capsOn" type="text" id="fcbusiness" name="fbusiness" readonly>
                                    <span class="fbusinessErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Account Number<em class="red-star">*</em></label>
                                    <input type="text" class="form-control hide_copy" id="fcaccount_number" name="faccount_number" value="">
                                    <span class="faccount_numberErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Routing Number<em class="red-star">*</em></label>
                                    <input type="text" class="form-control" id="fprouting_number" name="frouting_number" value="">
                                    <span class="frouting_numberErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>City <em class="red-star">*</em></label>
                                    <input type="text" class="form-control capital" id="fccity" name="fccity" value="" >
                                    <span class="fcityErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Address 1 <em class="red-star">*</em></label>
                                    <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                    <input class="form-control capital" id="fcaddress" name="fcaddress" >
                                    <span class="faddressErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Address 2</label>
                                    <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                    <input class="form-control capital" id="fcaddress2" name="fcaddress2" >
                                    <span class="fcaddress2Err error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Postal Code <em class="red-star">*</em></label>
                                    <input type="text" class="form-control capital" id="fczipcode" name="fczipcode" value=""  maxlength="9">
                                    <span class="fzipcodeErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>State <em class="red-star">*</em></label>
                                    <input type="text" class="form-control capital" id="fcstate" name="fcstate" value="" readonly>
                                    <span class="fstateErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3">
                                    <label>Name <em class="red-star">*</em></label>
                                    <input type="text" class="form-control capital" id="fcname" name="fcname" value="">
                                    <span class="fcnameErr error red-star"></span>
                                </div>
                                <div class="col-sm-12 col-md-3 fphone_num">
                                    <label>Phone <em class="red-star">*</em></label>
                                    <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fcphone_number" name="fcphone_number" value="">
                                    <span class="fphone_numberErr error red-star"></span>
                                    <input type="hidden" value="" id="fcphone_number_prefix" name="phone_number_prefix">
                                </div>
                                <div class="col-sm-12 col-md-3 fphone_num">
                                    <label>Tax_id <em class="red-star">*</em></label>
                                    <input   class="form-control" fixedLength="10" id="fctax_id" name="fctax_id" value="">
                                    <span class="fphone_numberErr error red-star"></span>
                                </div>

                                <div class="col-sm-12 col-md-3 fphone_num">
                                    <label>Company Document <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Upload Company Document here.</span></div></label>
                                    <input type='file' name="company_document" id="company_document" />
                                    <span class="company_document_idErr error red-star"></span>
                                </div>




                            </div>
                            <div class="col-sm-12">
                                <div class="form-hdr">
                                    <h3>Person</h3>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-outer mt-15">
                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" type="text" id="fpfirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn capital" type="text" id="fplast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control" type="email" id="fpemail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fpphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label> Day <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fpday" name="fday" >
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Month <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fpmonth" name="fmonth" >
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Year <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fpyear" name="fyear" >
                                        </select>
                                        <span class="fyearErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fpaddress" name="faddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fpaddress2" name="faddress2" >
                                        <span class="faddress2Err error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fpstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                </div>
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fpcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-12">
                                    <button type="submit"  id="savecompanyfinancial" class="blue-btn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="basic-user-payment-detail" style="display: none;">
                    <div >
                        <form method="post" id="financialInfo">

                            <div class="form-outer">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control" id="fcountry" name="fcountry" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="furl" name="furl">
                                        <span class="furl_nameErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fbusiness" name="fbusiness" readonly>
                                        <!--                                        <select class="form-control" id="fbusiness" name="fbusiness" readonly="">-->
                                        <!--                                            <option value="individual" selected>Individual</option>-->
                                        <!--                                        </select>-->
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="faccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="frouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="faddress" name="faddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="faddress2" name="faddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label> Day <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fday" name="fday" >
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Month <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fmonth" name="fmonth" >
                                        </select>
                                        <span class="fmonthErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Year <em class="red-star">*</em></label>
                                        <select class="form-control"  id="fyear" name="fyear" >
                                        </select>
                                        <span class="fyearErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control" type="email" id="femail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="ffirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="flast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>

                                </div>
                            </div>
                            <button type="submit"  id="savefinancial" class="blue-btn">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Wrapper Ends -->

<div class="container">
    <div class="modal fade" id="financial-infotype" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>
                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="financial-infotype-radio">
                        <label class="radio-inline">
                            <input type="radio" name="companytype" checked value="individual"> Individual
                        </label>
                        <input type="hidden" value="<?php echo $_SESSION[SESSION_DOMAIN]['cuser_id'] ?>" id="hidden_company_id" >
                        <label class="radio-inline">
                            <input type="radio" name="companytype" value="company" >Company
                        </label>
                    </div>

                    <div class="financial-infotype-btn text-right">
                        <button type="button"  id="savefinancialtype" class="blue-btn">Submit</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="financial-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content" id="financial-indiviual">
                        <form method="post" id="financialInfoIndiviual">
                            <div class="form-outer">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control" id="fcountry" name="fcountry" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fmcc">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3 furl">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="furl" name="furl">
                                        <span class="furl_nameErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fbusiness" name="fbusiness" value="individual" readonly>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="faccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="frouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="faddress" name="faddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="faddress2" name="faddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fppzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fppcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fppstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control femail" type="email" id="femail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="date-outer-form">
                                        <label class="col-sm-12"> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label> Day <em class="red-star">*</em></label>-->
                                            <select class="form-control fpday"  id="fday" name="fday" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fdayErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label>Month <em class="red-star">*</em></label>-->
                                            <select class="form-control fpmonth"  id="fmonth" name="fmonth" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label>Year <em class="red-star">*</em></label>-->
                                            <select class="form-control fpyear"  id="fyear" name="fyear" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn fname capital" type="text" id="ffirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn lname capital" type="text" id="flast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control fphone phone_number_format" fixedLength="10" id="fphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" class="fphonepre" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>

                                </div>
                            </div>
                            <button type="submit"  id="savefinancialindi" class="blue-btn">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="modal fade" id="financial-company-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content" id="financial-company">
                        <form method="post" id="financialCompanyInfoCom">
                            <input type="hidden" id="company_document_con_id" name="company_document_id" value="">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-12 col-md-3">
                                        <label>Country<em class="red-star">*</em></label>
                                        <select class="form-control capital" name="country" readonly="">
                                            <option value="usa" selected>USA</option>
                                        </select>
                                        <span class="fcountryErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>MCC <em class="red-star">*</em></label>
                                        <select class="form-control" id="fcmcc" name="fmcc" readonly="">
                                        </select>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>URL <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcurl" name="furl">
                                        <span class="furlErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="fcbusiness" name="fbusiness" value="company" readonly>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control hide_copy" id="fcaccount_number" name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fprouting_number" name="frouting_number" value="">
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress" name="fcaddress" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2</label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fcaddress2" name="fcaddress2" >
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcczipcode" name="fczipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcccity" name="fccity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fccstate" name="fcstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Name <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fcname" name="fcname" value="">
                                        <span class="fcnameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control phone_number_format" fixedLength="10" id="fcphone_number" name="fcphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="fcphone_number_prefix" name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Tax_id <em class="red-star">*</em></label>
                                        <input   class="form-control" fixedLength="10" id="fctax_id" name="fctax_id" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Company Document <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">Upload Company Document here.</span></div></label>
                                        <input type='file' name="company_document" id="company_document_con" />
                                        <span class="company_document_idErr error red-star"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-hdr">
                                    <h3>Person</h3>
                                </div>
                                <div class="form-outer mt-15">
                                    <div class="col-sm-12 col-md-3">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn fname capital" type="text" id="fpfirst_name" name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn lname capital" type="text" id="fplast_name" name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control femail" type="email" id="fpemail" name="femail" >
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control fphone phone_number_format" fixedLength="10" id="fpphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix" name="phone_number_prefix" class="fphonepre">
                                    </div>
                                    <div class="date-outer-form">
                                        <label class="col-sm-12"> Date of birth    <em class="red-star">*</em> <div class="tooltip1"><i class="fa fa-question-circle" aria-hidden="true"></i><span class="tooltiptext1">DATE OF BIRTH</span></div></label>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label> Day <em class="red-star">*</em></label>-->
                                            <select class="form-control fpday"  id="fpday" name="fday" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fdayErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label>Month <em class="red-star">*</em></label>-->
                                            <select class="form-control fpmonth"  id="fpmonth" name="fmonth" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fmonthErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-12 col-md-3 fphone_num">
                                            <!--                                            <label>Year <em class="red-star">*</em></label>-->
                                            <select class="form-control fpyear"  id="fpyear" name="fyear" >
                                                <option value="">Select</option>
                                            </select>
                                            <span class="fyearErr error red-star"></span>
                                        </div>
                                    </div>


                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 1 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control" id="fpaddress" name="faddress" >
                                        <span class="faddressErr error red-star capital"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Address 2 <em class="red-star">*</em></label>
                                        <!--                                        <textarea class="form-control" id="faddress" name="faddress" ></textarea>-->
                                        <input class="form-control capital" id="fpaddress2" name="faddress2" >
                                        <span class="faddress2Err error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpppzipcode" name="fzipcode" value=""  maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fpppcity" name="fcity" value="" >
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control capital" id="fpppstate" name="fstate" value="" readonly>
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label>SSN(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fpssn" name="fssn" value="" maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>

                                </div>


                            </div>
                    </div>
                    <button type="submit"  id="savecompanyfinancialcom" class="blue-btn">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Star Modal End-->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>"

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }
    /*function to print element by id */
    function Popup(data)
    {
        var base_url = window.location.origin;
        var mywindow = window.open('', 'my div');
        $(mywindow.document.body).html( '<body>' + data + '</body>');
        mywindow.document.close();
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        if(mywindow.close()){

        }
        $("#print_complaint").modal('hide');
        $("#vendorTable").trigger('reloadGrid');
        return true;
    }
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
    $('#people_top').addClass('active');

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });




    function PrintElem(elem)
    {
        Popup($(elem).html());
    }
    /*function to print element by id */
    function Popup(data)
    {
        var base_url = window.location.origin;
        var mywindow = window.open('', 'my div');
        $(mywindow.document.body).html( '<body>' + data + '</body>');
        mywindow.document.close();
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        if(mywindow.close()){

        }
        $("#print_complaint").modal('hide');
        $("#vendorTable").trigger('reloadGrid');
        return true;
    }

    var min = 1,
        max =31,
        select = document.getElementById('fpday');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }


    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var month = 0;
    // var i = 1;
    for (; month < monthNames.length; month++) {

        var new_month = month+1;
        $('#fpmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
    }

    var min = new Date().getFullYear(),
        max = min - 49,
        select = document.getElementById('fpyear');
    for (var i = max; i<=min; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/ownerListing.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/plaidIntialisation.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/additionalMethods.js" type="text/javascript"></script>
<!--    <script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/validation/onlinePaymentStep2.js" type="text/javascript"></script>-->
<!--    <script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/payments/onlinePaymentStep2.js" type="text/javascript"></script>-->

<!--Tenant Modals & Files -->
<?php include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php"); ?>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/shortTermRental.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/fullcal.js" integrity="sha384-vk5WoKIaW/vJyUAd9n/wmopsmNhiy+L2Z+SBxGYnUkunIxVxAv/UtMOhba/xskxh" crossorigin="anonymous"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-pseudo-ripple.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-nao-calendar.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/payments/subPayment.js"></script>-->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/owner/flagOwner.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/userSubPayment.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/moveOutCommon.js"></script>
<!--Tenant Modals & Files -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
</body>

</html>
