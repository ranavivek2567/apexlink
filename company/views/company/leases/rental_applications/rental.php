<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php");
?>
    <link rel="stylesheet" href="<?php echo COMPANY_SITE_URL;?>/css/smartmove.css"/>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid" id="rental_hide">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Leases &gt;&gt; <span>List of Rental Applications</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/company/leases/layout/right-nav.php");?>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/GuestCard/ListGuestCard" >Guest Cards</a></li>
                            <li role="presentation" class="active"><a href="/RentalApplication/RentalApplications">Rental Applications</a></li>
                            <li role="presentation"><a href="/Lease/ViewEditLease" >Leases</a></li>
                            <li role="presentation"><a href="/Lease/Movein" >Move-In</a></li>
                        </ul>
                        <div class="property-status">
                            <div class="atoz-outer2 ">
                                    <span class="apex-alphabets" id="apex-alphafilter" style="display:none;">
                                    <span class="AtoZ"></span></span>
                                <span class="AZ" id="AZ">A-Z</span>
                                <span id="allAlphabet" style="cursor:pointer;">All</span>
                            </div>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="" id="rental-application">
                                <!-- Sub Tabs Starts-->
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a class="active_class" aria-controls="home" role="tab" data-toggle="tab" data_id="1">Active<span class="tab-count status1"></span></a></li>
                                        <li role="presentation" ><a class="approved_class" aria-controls="profile" role="tab" data-toggle="tab" data_id="2">Approved<span class="tab-count status3"></span></a></li>
                                        <li role="presentation"><a class="declined_class" aria-controls="home" role="tab" data-toggle="tab" data_id="3">Declined<span class="tab-count status4"></span></a></li>
                                        <li role="presentation"><a class="lease_generated_class" aria-controls="profile" role="tab" data-toggle="tab" data_id="4">Lease Generated<span class="tab-count status5"></span></a></li>

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="rental-active">
                                            <div class="property-status">
                                                <div class="row">

                                                    <div class="col-sm-6">
                                                        <div class="btn-outer">
                                                            <a href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/excel/RentalApplications.pdf" download class="blue-btn">Print Blank</a>

                                                        </div>


                                                    </div>

                                                        <!--<div class="col-sm-2 rental_type_status">
                                                            <label>Status</label>
                                                            <select class="fm-txt form-control"  id="jqgridOptions">
                                                                <option value="1">Active</option>
                                                                <option value="2">Archived</option>
                                                            </select>
                                                        </div>-->
                                                    <div class="col-sm-6">

                                                        <div class="btn-outer text-right">
                                                            <a href="/RentalApplication/RentalApplication" class="blue-btn">New Rental Application</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                            <div class="table-responsive">
                                                                               <div class="apx-table">
                                                                                   <table class="rental_application_table table" id="rental_application_table" style="cursor: pointer;"></table>
                                                                               </div>
                                                                            </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="rental-approved">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="rental-declined">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="rental-lease-generated">

                                        </div>
                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
                <div class="container-fluid" id="rental_show" style="display: none;">
                    <div class="row">
                        <div class="bread-search-outer">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="breadcrumb-outer">
                                        Leases &gt;&gt; <span>List of Rental Applications</span>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text"/>
                                        <input type="hidden" id="hidden_id_view">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="building_property_id" val=""/>
                        <!--                <input type="hidden" value="--><?php //echo $_GET['id'];?><!--" name="owner_id" class="owner_id">-->
                        <div class="col-sm-12">
                            <div class="content-section">
                                <!--Tabs Starts -->

                                <div class="main-tabs">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation"><a href="/GuestCard/ListGuestCard" >Guest Cards</a></li>
                                        <li role="presentation" class="active"><a href="/RentalApplication/RentalApplications/">Rental Applications</a></li>
                                        <li role="presentation"><a href="/Lease/ViewEditLease/" >Leases</a></li>
                                        <li role="presentation"><a href="/Lease/Movein/0/" >Move-In</a></li>
                                    </ul>
                                    <!-- Nav tabs -->

                                    <!-- Nav tabs -->
                                    <div class="tab-content">
                                        <!--                                <div role="tabpanel" class="tab-pane active" id="guest-cards">-->
                                        <!--                                    <div class="form-outer">-->
                                        <!--                                        <div class="form-hdr">-->
                                        <!--                                            <h3>General Information <a onclick="goBack()" class="back" style="cursor: pointer;"><i class="fa fa-angle-double-left" aria-hidden="true" ></i> Back</a></h3>-->
                                        <!--                                        </div>-->
                                        <!---->
                                        <!--                                    </div>-->
                                        <!--                                </div>-->
                                        <!-- Form Outer Ends -->
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Rental Unit Details</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Expected MI Date :</label>
                                                                <span id="moveindate"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Expected MO Date :</label>
                                                                <span class="moveout">N/A</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">Property  :</label>
                                                                <span class="Property">N/A</span>
                                                            </div>
                                                            <div id="ethnicity_to_dob_div">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Unit:</label>
                                                                    <span class="unit">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Building Name :</label>
                                                                    <span class="Building_name">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">ZIP/Postal Code :</label>
                                                                    <span class="ZIP">N/A</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Request Lease Term :</label>
                                                                <span class="lease_term">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">City :</label>
                                                                <span class="City_unit">N/A</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">State/Province :</label>
                                                                <span class="State_unit">N/A</span>
                                                            </div>
                                                            <div id="ethnicity_to_dob_div">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Address :</label>
                                                                    <span class="Address">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Market Rent<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> :</label>
                                                                    <span class="Market_rent">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Base Rent<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> :</label>
                                                                    <span class="base_rent">N/A</span>
                                                                </div>

                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Security Deposit<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?>  :</label>
                                                                    <span class="sec_dep">N/A</span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>General Information</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Full name :</label>
                                                                <span class="name">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Gender :</label>
                                                                <span class="Gender">N/A</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">Email  :</label>
                                                                <span class="Email">N/A</span>
                                                            </div>
                                                            <div id="ethnicity_to_dob_div">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Martial Status:</label>
                                                                    <span class="Martial">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Veteran Status :</label>
                                                                    <span class="Veteran">N/A</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Driver's License/State :</label>
                                                                <span class="State">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Date of Birth :</label>
                                                                <span class="Birth">N/A</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">Phone :</label>
                                                                <span class="phone_number">N/A</span>
                                                            </div>
                                                            <div id="ethnicity_to_dob_div">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Extension :</label>
                                                                    <span class="work_phone_extension">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Referral Source :</label>
                                                                    <span class="Referral">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">SSN/SIN/ID :</label>
                                                                    <span class="ssn">N/A</span>
                                                                </div>

                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Referral Source  :</label>
                                                                    <span class="referrals">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Ethnicity :</label>
                                                                    <span class="Ethnicity">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Hobbies :</label>
                                                                    <span class="Hobbies">N/A</span>
                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="apx-table">
                                                        <div class="table-responsive">
                                                            <table class="occcupants_table1 table table-bordered" id="occcupants_table1"></table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Current Rental History</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Current Address :</label>
                                                                <span class="curr_addr">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Zip/Postal Code :</label>
                                                                <span class="zip_postal_current">N/A</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">City :</label>
                                                                <span class="city_current">N/A</span>
                                                            </div>
                                                            <div id="ethnicity_to_dob_div">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">State :</label>
                                                                    <span class="curr_state">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Landlord/Manager :</label>
                                                                    <span class="curr_manager">N/A</span>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Landlord/Manager Phone No :</label>
                                                                <span class="land_phone">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Monthly Rent<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?>  :</label>
                                                                <span class="monthly_rent">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Resided From :</label>
                                                                <span class="resided_from">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Resided To :</label>
                                                                <span class="resided_to">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Reason For Leaving :</label>
                                                                <span class="reason _current">N/A</span>
                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Previous Rental History</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Previous Address :</label>
                                                                <span class="previous_addr">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Zip/Postal Code :</label>
                                                                <span class="zip_postal_previous">N/A</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">city :</label>
                                                                <span class="city_previous">N/A</span>
                                                            </div>
                                                            <div id="ethnicity_to_dob_div">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">State :</label>
                                                                    <span class="previous_state">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">Landlord/Manager :</label>
                                                                    <span class="previous_manager">N/A</span>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Landlord/Manager Phone No :</label>
                                                                <span class="previous_phone">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Monthly Rent<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> :</label>
                                                                <span class="previousmonthly_rent">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Resided From :</label>
                                                                <span class="previousresided_from">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Resided To :</label>
                                                                <span class="previousresided_to">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Reason For Leaving :</label>
                                                                <span class="previous_reason">N/A</span>
                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Current Employer History</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Current Employer :</label>
                                                                <span class="curr_emplyyeee">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Address :</label>
                                                                <span class="address_employyer">N/A</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">ZIP/Postal Code :</label>
                                                                <span class="zip_emplyyeee">N/A</span>
                                                            </div>
                                                            <div id="ethnicity_to_dob_div">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">City :</label>
                                                                    <span class="city_emplyyer">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">State/Province :</label>
                                                                    <span class="state_province">N/A</span>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Position Held :</label>
                                                                <span class="position_held">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Monthly Salary<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> :</label>
                                                                <span class="monthly_salary">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Employed From Date :</label>
                                                                <span class="employed_from">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Employed To Date :</label>
                                                                <span class="emplyed_to">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Employer Phone Number :</label>
                                                                <span class="employe_phone">N/A</span>
                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Previous Employer History</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Previous Employer :</label>
                                                                <span class="previous_emplyyeee">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Address :</label>
                                                                <span class="previousaddress_employyer">N/A</span>
                                                            </div>

                                                            <div class="col-xs-12">
                                                                <label class="text-right">ZIP/Postal Code :</label>
                                                                <span class="previouszip_emplyyeee">N/A</span>
                                                            </div>
                                                            <div id="ethnicity_to_dob_div">
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">City :</label>
                                                                    <span class="previouscity_emplyyer">N/A</span>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <label class="text-right">State/Province :</label>
                                                                    <span class="previous_state_province">N/A</span>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Position Held :</label>
                                                                <span class="previous_position_held">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Monthly Salary<?php echo '(' . $_SESSION[SESSION_DOMAIN]['default_currency_symbol'] . ')'; ?> :</label>
                                                                <span class="previous_monthly_salary">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Employed From Date :</label>
                                                                <span class="previous_employed_from">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Employed To Date :</label>
                                                                <span class="previous_emplyed_to">N/A</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Employer Phone Number :</label>
                                                                <span class="previous_employe_phone">N/A</span>
                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>

                                                <div class="edit-foot">
                                                    <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                        <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>


                                        </div>

                                    </div>





                                </div>
                                <!-- Form Outer Ends -->

                                <!-- Form Outer starts -->
                                <div class="form-outer form-outer2">

                                    <div class="form-hdr">
                                        <h3>Additional Income</h3>
                                    </div>
                                    <div class="form-data">
                                        <div id="emergency_detail">
                                            <div class="apx-table">
                                                <div class="table-responsive">
                                                    <table class="additional_income"></table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>

                                    </div>
                                    <!--                                <div class="form-data">-->
                                    <!--                                    <div class="detail-outer emergency_detail_data">-->
                                    <!--                                        <div class=row style='margin-bottom: 10px;'>-->
                                    <!--                                            <div class=col-sm-6>-->
                                    <!--                                                <div class=col-xs-12>-->
                                    <!--                                                    <label class=text-right>Emergency Contact Name : </label>-->
                                    <!--                                                    <span class=emeregency_name></span>-->
                                    <!--                                                </div>-->
                                    <!--                                                <div class=col-xs-12>-->
                                    <!--                                                    <label class=text-right>Country Code :</label>-->
                                    <!--                                                    <span class=country_code> </span>-->
                                    <!--                                                </div>-->
                                    <!--                                                <div class=col-xs-12>-->
                                    <!--                                                    <label class=text-right>Emergency Contact Phone : </label>-->
                                    <!--                                                    <span class=emeregency_contact></span>-->
                                    <!--                                                </div>-->
                                    <!--                                            </div>-->
                                    <!--                                            <div class=col-sm-6>-->
                                    <!--                                                <div class=col-xs-12>-->
                                    <!--                                                    <label class=text-right>Emergency Contact Email : </label>-->
                                    <!--                                                    <span class=emeregency_email></span>-->
                                    <!--                                                </div>-->
                                    <!--                                                <div class=col-xs-12>-->
                                    <!--                                                    <label class=text-right>Emergency Contact Relation : </label>-->
                                    <!--                                                    <span class=emeregency_relation></span>-->
                                    <!--                                                </div>-->
                                    <!--                                            </div>-->
                                    <!--                                        </div>-->
                                    <!--                                    </div>-->
                                    <!--                                    <div class="edit-foot">-->
                                    <!--                                        <a href="javascript:;" class="edit_redirection" redirection_data="emergencyInfoDiv">-->
                                    <!--                                            <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>-->
                                    <!--                                            Edit-->
                                    <!--                                        </a>-->
                                    <!--                                    </div>-->
                                    <!--                                </div>-->
                                </div>

                                <!-- Form Outer Ends -->

                                <div class="form-outer form-outer2">
                                    <div class="form-hdr">
                                        <h3>Emergency Contact Details</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Full Name :</label>
                                                        <span class="emer_name">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Zip/Postal Code :</label>
                                                        <span class="zip_postal_emer">N/A</span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <label class="text-right">city :</label>
                                                        <span class="city_emer">N/A</span>
                                                    </div>
                                                    <div id="ethnicity_to_dob_div">
                                                        <div class="col-xs-12">
                                                            <label class="text-right">Relation :</label>
                                                            <span class="emner_relation">N/A</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Phone :</label>
                                                        <span class="emr_phone">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Extension :</label>
                                                        <span class="emr_exten">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Email :</label>
                                                        <span class="email_emer">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">State/Province :</label>
                                                        <span class="emer_state">N/A</span>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-outer form-outer2">
                                    <div class="form-hdr">
                                        <h3>Co-Signer/Guarantor</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Full Name :</label>
                                                        <span class="co_name">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Address :</label>
                                                        <span class="co_address">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Zip/Postal Code :</label>
                                                        <span class="zip_postal_co">N/A</span>
                                                    </div>

                                                    <div class="col-xs-12">
                                                        <label class="text-right">city :</label>
                                                        <span class="city_co">N/A</span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Phone :</label>
                                                        <span class="co_phone">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Email :</label>
                                                        <span class="email_co">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">State/Province :</label>
                                                        <span class="co_state">N/A</span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="text-right">Relation :</label>
                                                        <span class="co_relation">N/A</span>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-outer form-outer2">
                                    <div class="form-hdr">
                                        <h3>Checklist Section</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <label class=""><input type="checkbox" class="text-right" name="credit_check">  Credit Check</label>
                                                    </div>
                                                    <div class="btn-outer apex-btn-block left">
                                                        <a class="blue-btn gecheck" value="Generate Lease">Add more</a>

                                                    </div>

                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <label class=""><input type="checkbox" class="text-right" name="credit_check">  Rental Check</label>
                                                    </div>

                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <label class=""><input type="checkbox" class="text-right" name="credit_check">  Background Check</label>

                                                    </div>

                                                </div>
                                            </div>


                                        </div>

                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-outer form-outer2" id="notes_div">
                                    <div class="form-hdr">
                                        <h3>Notes</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="detail-outer">
                                          <!--  <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <label class=""><input type="checkbox" class="text-right" name="credit_check">  Credit Check</label>
                                                    </div>
                                                    <div class="btn-outer apex-btn-block left">
                                                        <a class="blue-btn gecheck" value="Generate Lease">Add more</a>

                                                    </div>

                                                </div>

                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <label class=""><input type="checkbox" class="text-right" name="credit_check">  Rental Check</label>
                                                    </div>

                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="col-xs-12">
                                                        <label class=""><input type="checkbox" class="text-right" name="credit_check">  Background Check</label>

                                                    </div>

                                                </div>
                                            </div>-->


                                        </div>

                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="contactInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>


                                </div>

                                <div class="form-outer form-outer2">

                                    <div class="table-responsive">
                                        <table class="table" id="unit_table"></table>
                                    </div>

                                </div>





                                <!-- Form Outer Starts -->
                                <div class="form-outer" style="margin-bottom: 20px" id="flags">
                                    <div class="form-hdr">
                                        <h3>Flag Bank</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <div class="apx-table listinggridDiv">
                                                                        <div class="table-responsive">
                                                                            <table id="flaglistingtable" class="table table-bordered"></table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="flagsBankInfoDiv" >
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form Outer Ends -->
                                <!-- Form Outer Ends -->
                                <div class="form-outer" id="filelibrary">
                                    <div class="form-hdr">
                                        <h3>File Library</h3>
                                    </div>
                                    <div class="form-data">
                                        <div class="accordion-grid">
                                            <div class="accordion-outer">
                                                <div class="bs-example">
                                                    <div class="panel-group" id="accordion">
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="TenantFiles-table" class="table table-bordered">
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="fileLibraryIfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Form Outer Ends -->
                                <!-- Form Outer starts -->
                                <div class="form-outer">
                                    <div class="form-hdr">
                                        <h3>Custom Fields</h3>
                                    </div>
                                    <div class="form-data">
                                        <input type="hidden" id="customFieldModule" name="module" value="owner">
                                        <div class="col-sm-7 custom_field_html_view_mode">
                                            No Custom Fields
                                        </div>


                                        <div class="edit-foot">
                                            <a href="javascript:;" class="edit_redirection" redirection_data="customFieldsInfoDiv">
                                                <i class="fa fa-pencil-square-o"  aria-hidden="true"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-outer">
                                <div class="row btn-outer apex-btn-block left">
                                    <div class="col-sm-6 col-md-6">
                                        <a class="blue-btn rental_application">Run Credit And Background Check</a>

                                        <a class="blue-btn generate_lease" >Run Background Screening</a>
                                    </div>
                                    <div class="col-sm-6 col-md-6 approve_application" style="cursor: pointer;">
                                        <a class="grey-btn approve_rental">Approve</a>
                                        <a class="grey-btn" >Checklist</a>
                                        <a class="grey-btn print_lease" >Download</a>
                                        <a class="grey-btn print_lease" >Print</a>
                                    </div>
                                    <div class="col-sm-6 col-md-6 approve_application_none" style="display: none; cursor: pointer;">
                                        <a class="grey-btn decline_rental">Decline</a>
                                        <a class="grey-btn generate_lease" >Generate Lease</a>
                                        <a class="grey-btn" >Checklist</a>
                                        <a class="grey-btn download_lease" >Download</a>
                                        <a class="grey-btn print_lease" >Print</a>
                                    </div>
                                </div>
                                </div>
                                <!-- Form Outer Ends -->

                            </div>


                        </div>
                        <!--tab Ends -->

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->
    <div class="container">
        <div class="modal fade" id="backgroundReport" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content  pull-left" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-left">Background Reports</h4>
                    </div>
                    <div class="modal-body">
                        <div class="tenant-bg-data">
                            <h3 style="text-align: center;">
                                Open Your Free Account Today!
                            </h3>
                            <p>
                                ApexLink is pleased to partner with Victig Screening Solutions to provide you with
                                the highest quality, timely National Credit, Criminal and Eviction Reports.
                            </p>
                            <div class="grid-outer " style="max-width: 1421px;">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <th colspan="3" align="center" class="bg-Bluehdr">
                                            Package Pricing<br>
                                            Properties Managing
                                        </th>
                                    </tr>
                                    <tr>
                                        <td width="29%" align="left">
                                            National Criminal
                                        </td>
                                        <td width="29%" align="left">
                                            National Criminal and Credit
                                        </td>
                                        <td width="42%" align="left">
                                            National Criminal, Credit and State Eviction
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            $15
                                        </td>
                                        <td align="left">
                                            $20
                                        </td>
                                        <td align="left">
                                            $25
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <p>
                                    *$100 fee to authorize credit report
                                </p>
                            </div>
                            <div class="grid-outer " style="max-width: 1421px;">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <th colspan="5" align="center" class="bg-Bluehdr">
                                            High Volume Users and Larger Properties
                                        </th>
                                    </tr>
                                    <tr>
                                        <td colspan="5" align="left">
                                            <p align="center">
                                                <strong>Contact our Victig Sales Team for custom pricing</strong>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="26%" align="center">
                                            <a href="mailto:sales@victig.com"><strong>sales@victig.com</strong></a>
                                        </td>
                                        <td width="15%" align="center">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td width="21%" align="center">
                                            <strong>866.886.5644 </strong>
                                        </td>
                                        <td width="13%" align="center">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                        </td>
                                        <td width="25%" align="center">
                                            <a href="http://www.victig.com" target="_blank"><strong>www.victig.com</strong></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="div-full">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td width="52%" height="20" style="font-size: 14px;" align="left">
                                            Already have an Account? <a href="https://apexlink.instascreen.net" target="_blank">
                                                Log in here
                                            </a>
                                        </td>
                                        <td width="48%" rowspan="2" align="right">
                                            <a href="https://www.victig.com/wp-content/uploads/2016/05/sample-tenant-report.jpg" target="_blank">
                                                <!--                                            <input value="View Sample Report" class="blue-btn fr" type="button" spellcheck="true" autocomplete="off">-->
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="font-size: 14px;">
                                            New Users: <a href="https://www.victig.com/victig-on-boarding/" target="_blank">
                                                Click
                                                here to register
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="div-full3">
                                <p>
                                    Please be advised that all background services are provided by Victig Screening
                                    Solutions. Direct all contact and questions pertaining to background check services
                                    to <a href="mailto:sales@victig.com">sales@victig.com</a>, 866.886.5644, <a href="http://www.victig.com" target="_blank">www.victig.com</a>
                                </p>
                            </div>
                            <div class="notice">
                                NOTICE: The use of this system is restricted. Only authorized users may access this
                                system. All Access to this system is logged and regularly monitored for computer
                                security purposes. Any unauthorized access to this system is prohibited and is subject
                                to criminal and civil penalties under Federal Laws including, but not limited to,
                                the Computer Fraud and Abuse Act and the National Information Infrastructure Protection
                                Act.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="modal fade" id="backGroundCheckPop" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content  pull-left" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Disclaimer</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-sm-12">
                            <p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div>
                        <div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div>
                        <div class="col-sm-12 btn-outer mg-btm-20">
                            <input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<!-- Footer Ends -->
<script>var upload_url = "<?php echo SITE_URL; ?>";</script>
    <script>var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";</script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/leases/RentalApplication/rentalListing.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/leases/guestCard/guestcardflag.js"></script>
<script>
    $(function() {
        $("#leases_top").addClass("active");
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";

    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
    <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/smartMove.js"></script>



<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>