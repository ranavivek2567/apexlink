<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

/*if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}*/
?>

    <input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">


<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php");
?>
    <div class="popup-bg"></div>
    <div id="wrapper">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/tenant_portal_header.php"); ?>
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content-section">
                            <div class="form-outer form-outer2">
                                <div class="form-hdr">
                                    <h3>Account Information <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">


                                        <div class="col-sm-12 col-md-2 grey-plus-photo-upload clear">
                                            <label>Vehicle Photo/Image</label>
                                            <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                            <div class="upload-logo">
                                                <div class="img-outer tenant_image"
                                                     id="tenant_image"><img
                                                            src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg">
                                                </div>
                                                <a class="choose-img" href="javascript:;">Choose
                                                    Image</a>
                                            </div>
                                            <div class="image-editor">
                                                <input type="file" class="cropit-image-input form-control" name="tenant_image" accept="image/*">
                                                <div class="cropItData" style="display: none;">
                                                    <div class="cropit-preview cropit-image-loading"
                                                         style="position: relative; width: 250px; height: 250px;">
                                                        <div class="cropit-preview-image-container"
                                                             style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                            <img class="cropit-preview-image"
                                                                 alt=""
                                                                 style="transform-origin: left top; will-change: transform;">
                                                        </div>
                                                        <div class="cropit-preview-image-container"
                                                             style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                            <img class="cropit-preview-image"
                                                                 alt=""
                                                                 style="transform-origin: left top; will-change: transform;">
                                                        </div>
                                                    </div>
                                                    <div class="tenant_image"></div>
                                                    <!--  <div class="image-size-label">Resize image</div> -->
                                                    <input type="range"
                                                           class="cropit-image-zoom-input"
                                                           min="0" max="1" step="0.01">
                                                    <input type="hidden" name="image-data"
                                                           class="hidden-image-data">
                                                    <input type="button" class="export"
                                                           value="Done"
                                                           data-val="tenant_image">
                                                </div>
                                            </div>
                                        </div>




                                        <div class="grey-detail-box">
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Property:</label>
                                                <span class="property_name"></span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Building & Unit No.:</label>
                                                <span class="building_unit">Bill Farms Bill A-6</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Rent(USh):</label>
                                                <span class="rent">USh900.00</span>
                                            </div>
                                        </div>
                                        <div class="grey-detail-box">
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Security Deposite(USh):</label>
                                                <span class="security_deposite">USh</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Balance Due(USh):</label>
                                                <span class="balance_due"></span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">NFS(USh):</label>
                                                <span class="nsf"></span>
                                            </div>
                                        </div>
                                        <div class="grey-detail-box">
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Lease Start Date:</label>
                                                <span class="start_date"></span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Lease End Date:</label>
                                                <span class="end_date"></span>
                                            </div>
                                            <div class="col-xs-12 col-sm-3">
                                                <label class="blue-label">Move in Date:</label>
                                                <span class="move_in"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Tabs Start -->
                            <div class="main-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tenant-acc-tab1" aria-controls="home" role="tab" data-toggle="tab">Contact Information</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab2" aria-controls="profile" role="tab" data-toggle="tab">Additional Information</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab3" aria-controls="profile" role="tab" data-toggle="tab">Payment Settings</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab4" aria-controls="profile" role="tab" data-toggle="tab">Billing Information</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab5" aria-controls="profile" role="tab" data-toggle="tab">Conversation</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab6" aria-controls="profile" role="tab" data-toggle="tab">Change Password</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab7" aria-controls="profile" role="tab" data-toggle="tab">Give Notice</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab8" aria-controls="profile" role="tab" data-toggle="tab">Occupants</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab9" aria-controls="profile" role="tab" data-toggle="tab">Tenant Leases</a></li>
                                    <li role="presentation"><a href="#tenant-acc-tab10" aria-controls="profile" role="tab" data-toggle="tab">Payment</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="tenant-acc-tab1">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Contact Information</h3>
                                            </div>
                                            <div class="form-data form-outer2">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Full Name :</label>
                                                                <span class="fullname"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Address 1 :</label>
                                                                <span class="address1"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Address 3 :</label>
                                                                <span class="address3"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Zip/Postal Code :</label>
                                                                <span class="zipcode"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">City :</label>
                                                                <span class="city"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Email :</label>
                                                                <span class="email"></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Entity/Company :</label>
                                                                <span class="company_name"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Address 2 :</label>
                                                                <span class="address2"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Address4 :</label>
                                                                <span class="address4"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">State Province :</label>
                                                                <span class="state"></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Phone :</label>
                                                                <span class="phone_number"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form Outer Ends-->
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Emergency Contact Details</h3>
                                            </div>
                                            <div class="emergencyInfo" style="display:none;"></div>
                                            <div class="viewEmergencyInfo"></div>
                                            <a href="javascript:;" class="emergencySection"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit</a>



                                        </div>
                                        <!-- Form Outer Ends-->
                                    </div>
                                    <!-- First Tab Ends-->
                                    <div role="tabpanel" class="tab-pane" id="tenant-acc-tab2">
                                        <div class="form-outer vehicle_main">
                                            <div class="form-hdr">
                                                <h3 class="pull-left">Vehicles</h3>
                                                <input type="button" name="addVehicle" value="Add Vehicle"
                                                       class="addbtn blue-btn pull-right" id="add_vehicle_button" style="display: none">

                                            </div>
                                            <div class="form-data">
                                                <div>
                                                    <form id='addEditVehicle' style="display: none;">
                                                        <input type="hidden" name="vehicle_action"
                                                               class="vehicle_action">
                                                        <input type="hidden" name="vehicle_id" class="vehicle_id">
                                                        <div class="row">


                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Vehicle Type</label>
                                                                        <input class="form-control" type="text"
                                                                               name='vehicle_type'
                                                                               id="vehicle_type" maxlength="50">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Make</label>
                                                                        <input class="form-control" type="text"
                                                                               name='vehicle_make'
                                                                               id="vehicle_make" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>License Plate Number</label>
                                                                        <input class="form-control" type="text"
                                                                               name='vevicle_license'
                                                                               id="vevicle_license" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Color</label>
                                                                        <input class="form-control" type="text"
                                                                               name='vehicle_color'
                                                                               id="vehicle_color" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <label>Year Of Vehicle</label>
                                                                    <select class="form-control" id="vevicle_year"
                                                                            name="vevicle_year">
                                                                        <option value="">Select</option>

                                                                        <?php
                                                                        for ($vehiclyear = date("Y"); $vehiclyear >= 1900; $vehiclyear--) {
                                                                            echo '<option value="' . $vehiclyear . '">' . $vehiclyear . '</option>';
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>VIN </label>
                                                                        <input class="form-control" type="text"
                                                                               name='vehicle_vin' id="vehicle_vin" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Registration</label>
                                                                        <input class="form-control" type="text"
                                                                               name='vehicle_registration'
                                                                               id="vehicle_registration" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>


                                                                <div class="col-sm-12 col-md-2 grey-plus-photo-upload clear">
                                                                    <label>Vehicle Photo/Image</label>
                                                                    <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                    <div class="upload-logo">
                                                                        <div class="img-outer vehicle_image1"
                                                                             id="vehicle_image1"><img
                                                                                    src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">
                                                                        </div>
                                                                        <a class="choose-img" href="javascript:;">Choose
                                                                            Image</a>
                                                                    </div>
                                                                    <div class="image-editor">
                                                                        <input type="file"
                                                                               class="cropit-image-input form-control"
                                                                               name="vehicle_image1[]"
                                                                               accept="image/*">
                                                                        <div class="cropItData"
                                                                             style="display: none;">
                                                                            <div class="cropit-preview cropit-image-loading"
                                                                                 style="position: relative; width: 250px; height: 250px;">
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                            </div>
                                                                            <div class="vehicle_image1"></div>
                                                                            <!--  <div class="image-size-label">Resize image</div> -->
                                                                            <input type="range"
                                                                                   class="cropit-image-zoom-input"
                                                                                   min="0" max="1" step="0.01">
                                                                            <input type="hidden" name="image-data"
                                                                                   class="hidden-image-data">
                                                                            <input type="button" class="export"
                                                                                   value="Done"
                                                                                   data-val="vehicle_image1">
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                    <label>Vehicle Photo/Image</label>
                                                                    <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                    <div class="upload-logo">
                                                                        <div class="img-outer vehicle_image2"
                                                                             id="vehicle_image2"><img
                                                                                    src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">
                                                                        </div>
                                                                        <a class="choose-img" href="javascript:;">Choose
                                                                            Image</a>
                                                                    </div>
                                                                    <div class="image-editor">
                                                                        <input type="file"
                                                                               class="cropit-image-input form-control"
                                                                               name="vehicle_image2[]"
                                                                               accept="image/*">
                                                                        <div class="cropItData"
                                                                             style="display: none;">
                                                                            <div class="cropit-preview cropit-image-loading"
                                                                                 style="position: relative; width: 250px; height: 250px;">
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                            </div>
                                                                            <div class="vehicle_image2"></div>
                                                                            <!--  <div class="image-size-label">Resize image</div> -->
                                                                            <input type="range"
                                                                                   class="cropit-image-zoom-input"
                                                                                   min="0" max="1" step="0.01">
                                                                            <input type="hidden" name="image-data"
                                                                                   class="hidden-image-data">
                                                                            <input type="button" class="export"
                                                                                   value="Done"
                                                                                   data-val="vehicle_image2">
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                    <label>Vehicle Photo/Image</label>
                                                                    <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                    <div class="upload-logo">
                                                                        <div class="img-outer vehicle_image3"
                                                                             id="vehicle_image3"><img
                                                                                    src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">
                                                                        </div>
                                                                        <a class="choose-img" href="javascript:;">Choose
                                                                            Image</a>
                                                                    </div>
                                                                    <div class="image-editor">
                                                                        <input type="file"
                                                                               class="cropit-image-input form-control"
                                                                               name="vehicle_image3[]"
                                                                               accept="image/*">
                                                                        <div class="cropItData"
                                                                             style="display: none;">
                                                                            <div class="cropit-preview cropit-image-loading"
                                                                                 style="position: relative; width: 250px; height: 250px;">
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                            </div>
                                                                            <div class="vehicle_image3"></div>
                                                                            <!--  <div class="image-size-label">Resize image</div> -->
                                                                            <input type="range"
                                                                                   class="cropit-image-zoom-input"
                                                                                   min="0" max="1" step="0.01">
                                                                            <input type="hidden" name="image-data"
                                                                                   class="hidden-image-data">
                                                                            <input type="button" class="export"
                                                                                   value="Done"
                                                                                   data-val="vehicle_image3">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <div class="col-sm-12">
                                                                    <input type="submit" class="blue-btn"
                                                                           name="submit" value="Save">
                                                                    <input type="button" name="Cancel"
                                                                           value="Cancel"
                                                                           class="grey-btn cancelbtn">
                                                                </div>
                                                            </div>


                                                        </div>


                                                    </form>

                                                    <!-- append vehicle listing -->
                                                    <div class="apx-table">

                                                        <div class="table-responsive">

                                                            <table id="TenantVehicle-table"
                                                                   class="table table-bordered">

                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="edit-foot edit_vehicle_button">
                                                        <a href="javascript:;">
                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            Edit
                                                        </a>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <!-- Form-outer Ends-->
                                        <div class="form-outer  pet_main">
                                            <div class="form-hdr">
                                                <h3 class="pull-left">Pets</h3>
                                                <input type="button" value="Add Pets" id="add_pet_button" style="display: none;"
                                                       class="addbtn blue-btn pull-right">

                                            </div>
                                            <div class="form-data">
                                                <!--                                         <input type="button"   class="addbtn blue-btn" value="Add Pet">-->

                                                <form id='addEditPet' method="post" style="display:none;">
                                                    <input type="hidden" class="pet_action blue-btn"
                                                           name="pet_action">
                                                    <input type="hidden" class="pet_unique_id blue-btn"
                                                           name="pet_unique_id">

                                                    <div class="property_pet">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <label>Pet Name</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_name" id="pet_name" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <label>Pet ID</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_id" maxlength="30">
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <label>Pet Type/Breed</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_type" id="pet_type" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Birth Date</label>
                                                                        <input class="form-control calander pet_birth"
                                                                               type="text" name="pet_birth">
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2 ">
                                                                    <label>Pet Age</label>
                                                                    <select class="form-control" name="pet_age"
                                                                            id="pet_age">
                                                                        <?php
                                                                        for ($petAge = 1; $petAge <= 100; $petAge++) {
                                                                            echo '<option value="' . $petAge . '">' . $petAge . '</option>';
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <label>Gender</label>
                                                                    <select class="form-control" id="pet_gender"
                                                                            name="pet_gender" id="pet_gender"
                                                                    <option value="">Select</option>
                                                                    <option value="1">Male</option>
                                                                    <option value="2">Female</option>
                                                                    <option value="3">Other</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <label>Pet Weight</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_weight" id="pet_weight" maxlength="30">
                                                                        <span class="ffirst_nameErr error red-star" ></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1 col-md-2">
                                                                    <label>Pet Weight Unit</label>
                                                                    <select class="form-control"
                                                                            id="pet_weight_unit"
                                                                            name="pet_weight_unit">
                                                                        <option value="">Select</option>
                                                                        <option value="1">lb</option>
                                                                        <option value="2">lbs</option>
                                                                        <option value="3">kg</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <label>Note</label>
                                                                        <textarea class="form-control"
                                                                                  name="pet_note"
                                                                                  id="pet_note"></textarea>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Pet Color</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_color" id="pet_color" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Chip ID</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_chipid" id="pet_chipid" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <!-- <span class="ffirst_nameErr error red-star"></span> -->
                                                            </div>
                                                        </div>


                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <label>vet\Hosp. Name</label>
                                                                        <input class="form-control" type="text"
                                                                               name="pet_vet" id="pet_vet" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2 countycodediv">
                                                                    <label>Country Code</label>
                                                                    <select class="form-control"
                                                                            name="pet_countryCode"
                                                                            id="pet_countryCode" maxlength="30"></select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <label>Phone Number</label>
                                                                        <input type="text" class="form-control"
                                                                               name="pet_phoneNumber"
                                                                               id="pet_phoneNumber" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Last Visit</label>
                                                                        <input class="form-control calander pet_lastVisit"
                                                                               type="text" name="pet_lastVisit" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class="contactTenant">
                                                                        <!--calander -->
                                                                        <label>Next Visit</label>
                                                                        <input class="form-control calander pet_nextVisit"
                                                                               type="text" name="pet_nextVisit" maxlength="30">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>


                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Medical Condition</label>
                                                                    <div class="check-outer">
                                                                        <input name="pet_medical" type="radio"
                                                                               value="1"
                                                                               class="pet_medical_condition" maxlength="30">
                                                                        <label>Yes</label>
                                                                    </div>
                                                                    <div class="check-outer">
                                                                        <input name="pet_medical" type="radio"
                                                                               value="0"
                                                                               class="pet_medical_condition"
                                                                               checked="" maxlength="30">
                                                                        <label>No</label>
                                                                    </div>
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="pet_medical_html" style="display:none">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12 col-md-3">
                                                                            <div class="contactTenant">
                                                                                <!--calander -->
                                                                                <label>Please Add Pets Medical
                                                                                    Condiction</label>
                                                                                <input class="form-control"
                                                                                       type="text"
                                                                                       name="pet_medical_condition_note"
                                                                                       id="pet_medical_condition_note" maxlength="30">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Shots</label>
                                                                    <div class="check-outer">
                                                                        <input name="pet_shots" type="radio"
                                                                               value="1" class="pet_name_shot" maxlength="30">
                                                                        <label>Yes</label>
                                                                    </div>
                                                                    <div class="check-outer">
                                                                        <input name="pet_shots" type="radio"
                                                                               value="0" class="pet_name_shot"
                                                                               checked="" maxlength="30">
                                                                        <label>No</label>
                                                                    </div>
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="pet_shot_date" style="display:none;">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Name of the shot</label>
                                                                            <input class="form-control" type="text"
                                                                                   name="pet_name_shot"
                                                                                   id="pet_name_shot" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Date Given</label>
                                                                            <input class="form-control calander pet_date_given"
                                                                                   type="text" name="pet_date_given"
                                                                                   id="dp1561612752915">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Expiration Date</label>
                                                                            <input class="form-control calander pet_expiration_date"
                                                                                   type="text"
                                                                                   name="pet_expiration_date">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Follow Up</label>
                                                                            <input class="form-control calander pet_follow_up"
                                                                                   type="text" name="pet_follow_up"
                                                                                   id="dp1561612752917">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Note</label>
                                                                            <input class="form-control" type="text"
                                                                                   name="pet_note"
                                                                                   id="shot_pet_note">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>

                                                            </div>


                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Rabies</label>
                                                                <div class="check-outer">
                                                                    <input name="pet_rabies" type="radio" value="1"
                                                                           class="pet_rabies" maxlength="30">
                                                                    <label>Yes</label>
                                                                </div>
                                                                <div class="check-outer">
                                                                    <input name="pet_rabies" class="pet_rabies"
                                                                           type="radio" value="0" checked="" >
                                                                    <label>No</label>
                                                                </div>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="clearfix"></div>


                                                            <div class="pet_rabies_html" style="display:none">

                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Rabies#</label>
                                                                            <input class="form-control" type="text"
                                                                                   name="pet_rabies_name" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Date Given</label>
                                                                            <input class="form-control calander"
                                                                                   type="text"
                                                                                   name="pet_rabies_given_date"
                                                                                   id="dp1561612752918">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Expiration Date</label>
                                                                            <input class="form-control calander"
                                                                                   type="text"
                                                                                   name="pet_rabies_expiration_date"
                                                                                   id="dp1561612752919">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class="contactTenant">
                                                                            <!--calander -->
                                                                            <label>Note</label>
                                                                            <input class="form-control" type="text"
                                                                                   name="pet_rabies_note[]">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>

                                                            </div>

                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                    <label>Pet Photo/Image</label>
                                                                    <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                    <div class="upload-logo">
                                                                        <div class="img-outer pet_image1"
                                                                             id="pet_image1"><img
                                                                                    src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">
                                                                        </div>
                                                                        <a class="choose-img" href="javascript:;">Choose
                                                                            Image</a>
                                                                    </div>
                                                                    <div class="image-editor">
                                                                        <input type="file"
                                                                               class="cropit-image-input form-control"
                                                                               name="pet_image1[]" accept="image/*">
                                                                        <div class="cropItData"
                                                                             style="display: none;">
                                                                            <div class="cropit-preview cropit-image-loading"
                                                                                 style="position: relative; width: 250px; height: 250px;">
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                            </div>
                                                                            <div class="pet_image1"></div>
                                                                            <!--  <div class="image-size-label">Resize image</div> -->
                                                                            <input type="range"
                                                                                   class="cropit-image-zoom-input"
                                                                                   min="0" max="1" step="0.01">
                                                                            <input type="hidden" name="image-data"
                                                                                   class="hidden-image-data">
                                                                            <input type="button" class="export"
                                                                                   value="Done"
                                                                                   data-val="pet_image1">
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                    <label>Pet Photo/Image</label>
                                                                    <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                    <div class="upload-logo">
                                                                        <div class="img-outer pet_image2"
                                                                             id="pet_image2"><img
                                                                                    src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">
                                                                        </div>
                                                                        <a class="choose-img" href="javascript:;">Choose
                                                                            Image</a>
                                                                    </div>
                                                                    <div class="image-editor">
                                                                        <input type="file"
                                                                               class="cropit-image-input form-control"
                                                                               name="pet_image2[]" accept="image/*">
                                                                        <div class="cropItData"
                                                                             style="display: none;">
                                                                            <div class="cropit-preview cropit-image-loading"
                                                                                 style="position: relative; width: 250px; height: 250px;">
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                            </div>
                                                                            <div class="pet_image2"></div>
                                                                            <!--  <div class="image-size-label">Resize image</div> -->
                                                                            <input type="range"
                                                                                   class="cropit-image-zoom-input"
                                                                                   min="0" max="1" step="0.01">
                                                                            <input type="hidden" name="image-data"
                                                                                   class="hidden-image-data">
                                                                            <input type="button" class="export"
                                                                                   value="Done"
                                                                                   data-val="pet_image2">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                    <label>Pet Photo/Image</label>
                                                                    <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                    <div class="upload-logo">
                                                                        <div class="img-outer pet_image3"
                                                                             id="pet_image3"><img
                                                                                    src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">
                                                                        </div>
                                                                        <a class="choose-img" href="javascript:;">Choose
                                                                            Image</a>
                                                                    </div>
                                                                    <div class="image-editor">
                                                                        <input type="file"
                                                                               class="cropit-image-input form-control"
                                                                               name="pet_image3[]" accept="image/*">
                                                                        <div class="cropItData"
                                                                             style="display: none;">
                                                                            <div class="cropit-preview cropit-image-loading"
                                                                                 style="position: relative; width: 250px; height: 250px;">
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                                <div class="cropit-preview-image-container"
                                                                                     style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                    <img class="cropit-preview-image"
                                                                                         alt=""
                                                                                         style="transform-origin: left top; will-change: transform;">
                                                                                </div>
                                                                            </div>
                                                                            <div class="pet_image3"></div>
                                                                            <!--  <div class="image-size-label">Resize image</div> -->
                                                                            <input type="range"
                                                                                   class="cropit-image-zoom-input"
                                                                                   min="0" max="1" step="0.01">
                                                                            <input type="hidden" name="image-data"
                                                                                   class="hidden-image-data">
                                                                            <input type="button" class="export"
                                                                                   value="Done"
                                                                                   data-val="pet_image3">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <input type="submit" name="submit"
                                                                           class="blue-btn" value="Save">
                                                                    <input type="button" class="cancelbtn grey-btn"
                                                                           value="Cancel">
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- append pet listing -->


                                                <div class="apx-table">
                                                    <div class="table-responsive">
                                                        <table id="TenantPet-table" class="table table-bordered">
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="edit-foot edit_pet_button">
                                                    <a href="javascript:;">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                        Edit
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form-outer Ends-->
                                        <div class="form-outer  service_main">
                                            <div class="form-hdr">
                                                <h3 class="pull-left">Service/Companion Animals</h3>
                                                <input type="button" value="Add Animal"
                                                       class="addbtn blue-btn  pull-right" id="add_animal_animal" style="display: none">


                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <form id="addEditService" method="post"
                                                              style="display:none;">
                                                            <input type="hidden" class="service_action"
                                                                   name="service_action">
                                                            <input type="hidden" class="service_unique_id"
                                                                   name="service_unique_id">

                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Name</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='service_name'
                                                                                   id="service_name" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>ID</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='service_id'
                                                                                   id="service_id" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Type/Breed</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='service_type'
                                                                                   id="service_type" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Birth Date</label>
                                                                            <input class="form-control calander"
                                                                                   type="text"
                                                                                   name='service_birth'
                                                                                   id="service_birth">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <label>Age</label>
                                                                        <select class="form-control"
                                                                                id="service_year"
                                                                                name="service_year">
                                                                            <option value="">Select</option>

                                                                            <?php
                                                                            for ($serviceAge = 1; $serviceAge <= 100; $serviceAge++) {
                                                                                echo '<option value="' . $serviceAge . '">' . $serviceAge . '</option>';
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2 ">
                                                                        <label>Gender</label>
                                                                        <select class="form-control"
                                                                                name="service_gender"
                                                                                id="service_gender">
                                                                            <option value="0">Select</option>
                                                                            <option value="1">Male</option>
                                                                            <option value="2">Female</option>
                                                                            <option value="3">Other</option>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Animal Weight</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='service_weight'
                                                                                   id="service_weight">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2 ">
                                                                        <label>Weight Unit</label>
                                                                        <select class="form-control"
                                                                                name="service_weight_unit"
                                                                                id="service_weight_unit">
                                                                            <option value="">Select</option>
                                                                            <option value="1">lb</option>
                                                                            <option value="2">lbs</option>
                                                                            <option value="3">kg</option>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Note</label>
                                                                            <textarea class="form-control"
                                                                                      name='service_note'
                                                                                      id="service_note"></textarea>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Color</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='service_color'
                                                                                   id="service_color" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Chip ID</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='service_chipid'
                                                                                   id="service_chipid" maxlength="30">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>vet\Hosp. Name</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='service_vet'
                                                                                   id="service_vet" maxlength="30">
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2 countycodediv">
                                                                        <label>Country Code</label>
                                                                        <select class="form-control"
                                                                                name="service_countryCode"
                                                                                id="service_countryCode">
                                                                            <option value="1">Select</option>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Phone Number</label>
                                                                            <input type='text' class="form-control"
                                                                                   name='service_phoneNumber'
                                                                                   id="service_phoneNumber" >
                                                                            <span class="term_planErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Last Visit</label>
                                                                            <input class="form-control calander service_lastVisit"
                                                                                   type="text"
                                                                                   name='service_lastVisit'>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Next Visit</label>
                                                                            <input class="form-control calander service_nextVisit"
                                                                                   type="text"
                                                                                   name='service_nextVisit'>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Medical Condition</label>
                                                                        <div class="check-outer">
                                                                            <input name='service_medical'
                                                                                   type="radio"
                                                                                   value='1'>
                                                                            <strong class='font-weight-bold'>Yes</strong>
                                                                        </div>
                                                                        <div class="check-outer">
                                                                            <input name='service_medical'
                                                                                   type="radio"
                                                                                   value='0' checked>
                                                                            <strong class='font-weight-bold'>No</strong>
                                                                        </div>
                                                                        <span class="flast_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--<div class="row spaceNumber">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Parking Space Number<em class="red-star">*</em></label>
                                                                        <input class="form-control capsOn" type="text" id="parking_space" name="parking_space[]">
                                                                        <span class="flast_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            </div>-->
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Shots</label>
                                                                        <input name='service_shots' type="radio"
                                                                               value='1'
                                                                               class="service_shots"><strong
                                                                                class='font-weight-bold'>Yes</strong>
                                                                        <input name='service_shots' type="radio"
                                                                               value='0'
                                                                               class="service_shots" checked><strong
                                                                                class='font-weight-bold'>No</strong>
                                                                        <span class="flast_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="row">
                                                                <div class="animal_shot_date" style="display:none;">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12 col-md-12">
                                                                            <div class="col-sm-2 col-md-2">
                                                                                <div class='contactTenant'>
                                                                                    <!--calander -->
                                                                                    <label>Name of the shot</label>
                                                                                    <input class="form-control"
                                                                                           type="text"
                                                                                           name='service_name_shot'
                                                                                           id="service_name_shot" maxlength="30">
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-2 col-md-2">
                                                                                <div class='contactTenant'>
                                                                                    <!--calander -->
                                                                                    <label>Date Given</label>
                                                                                    <input class="form-control calander service_date_given"
                                                                                           type="text"
                                                                                           name='service_date_given'
                                                                                           value="<?php echo $today = date('Y-m-d'); ?>">
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-2 col-md-2">
                                                                                <div class='contactTenant'>
                                                                                    <!--calander -->
                                                                                    <label>Expiration Date</label>
                                                                                    <input class="form-control calander service_expiration_date"
                                                                                           type="text"
                                                                                           name='service_expiration_date'
                                                                                           value="<?php echo $today = date("Y-m-d"); ?>">
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-2 col-md-2">
                                                                                <div class='contactTenant'>
                                                                                    <!--calander -->
                                                                                    <label>Follow Up</label>
                                                                                    <input class="form-control calander service_follow_up"
                                                                                           type="text"
                                                                                           name='service_follow_up'>
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                            <div class="col-sm-12 col-md-2">
                                                                                <div class='contactTenant'>
                                                                                    <!--calander -->
                                                                                    <label>Note</label>
                                                                                    <input class="form-control service_note"
                                                                                           type="text"
                                                                                           name='service_note'>
                                                                                </div>
                                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Rabies</label>
                                                                        <input name='service_rabies' type="radio"
                                                                               value='1'
                                                                               class="animal_rabies"><strong
                                                                                class='font-weight-bold'>Yes</strong>
                                                                        <input name='service_rabies' type="radio"
                                                                               value='0'
                                                                               class="animal_rabies" checked><strong
                                                                                class='font-weight-bold'>No</strong>
                                                                        <span class="flast_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row animal_rabies_html"
                                                                 style="display:none">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-12">
                                                                        <div class="col-sm-2 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <label>Rabies#</label>
                                                                                <input class="form-control"
                                                                                       type="text"
                                                                                       name='animal_rabies'
                                                                                       id="animal_rabies">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-2 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Date Given</label>
                                                                                <input class="form-control calander animal_date_given"
                                                                                       type="text"
                                                                                       name='animal_date_given'
                                                                                       value="<?php echo $today = date("Y-m-d"); ?>">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-2 col-md-2">
                                                                            <div class='contactTenant'>

                                                                                <label>Expiration Date</label>
                                                                                <input class="form-control calander animal_expiration_date"
                                                                                       type="text"
                                                                                       name='animal_expiration_date'
                                                                                       value="<?php echo $today = date("Y-m-d"); ?>">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-sm-2 col-md-2">
                                                                            <div class='contactTenant'>
                                                                                <!--calander -->
                                                                                <label>Note</label>
                                                                                <input class="form-control"
                                                                                       type="text"
                                                                                       name='animal__note'
                                                                                       id="animal__note">
                                                                            </div>
                                                                            <span class="ffirst_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                        <label>Animal Photo/Image</label>
                                                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer service_image1"
                                                                                 id="service_image1"><img
                                                                                        src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">
                                                                            </div>
                                                                            <a class="choose-img"
                                                                               href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="service_image1"
                                                                                   accept="image/*">
                                                                            <div class="cropItData"
                                                                                 style="display: none;">
                                                                                <div class="cropit-preview cropit-image-loading"
                                                                                     style="position: relative; width: 250px; height: 250px;">
                                                                                    <div class="cropit-preview-image-container"
                                                                                         style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                        <img class="cropit-preview-image"
                                                                                             alt=""
                                                                                             style="transform-origin: left top; will-change: transform;">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="service_image1"></div>
                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input"
                                                                                       min="0"
                                                                                       max="1" step="0.01">
                                                                                <input type="hidden"
                                                                                       name="image-data"
                                                                                       class="hidden-image-data">
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="service_image1">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                        <label>Animal Photo/Image</label>
                                                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer service_image2"
                                                                                 id="service_image2"><img
                                                                                        src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">
                                                                            </div>
                                                                            <a class="choose-img"
                                                                               href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="service_image2"
                                                                                   accept="image/*">
                                                                            <div class="cropItData"
                                                                                 style="display: none;">
                                                                                <div class="cropit-preview cropit-image-loading"
                                                                                     style="position: relative; width: 250px; height: 250px;">
                                                                                    <div class="cropit-preview-image-container"
                                                                                         style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                        <img class="cropit-preview-image"
                                                                                             alt=""
                                                                                             style="transform-origin: left top; will-change: transform;">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="service_image2"></div>
                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input"
                                                                                       min="0"
                                                                                       max="1" step="0.01">
                                                                                <input type="hidden"
                                                                                       name="image-data"
                                                                                       class="hidden-image-data">
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="service_image2">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                        <label>Animal Photo/Image</label>
                                                                        <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer service_image3"
                                                                                 id="service_image3"><img
                                                                                        src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">
                                                                            </div>
                                                                            <a class="choose-img"
                                                                               href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="service_image3"
                                                                                   accept="image/*">
                                                                            <div class="cropItData"
                                                                                 style="display: none;">
                                                                                <div class="cropit-preview cropit-image-loading"
                                                                                     style="position: relative; width: 250px; height: 250px;">
                                                                                    <div class="cropit-preview-image-container"
                                                                                         style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                        <img class="cropit-preview-image"
                                                                                             alt=""
                                                                                             style="transform-origin: left top; will-change: transform;">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="service_image2"></div>
                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input"
                                                                                       min="0"
                                                                                       max="1" step="0.01">
                                                                                <input type="hidden"
                                                                                       name="image-data"
                                                                                       class="hidden-image-data">
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="service_image3">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <input type="submit" name="submit"
                                                                               class="blue-btn" value="Save">
                                                                        <input type="button" name="Cancel"
                                                                               value="Cancel"
                                                                               class="grey-btn cancelbtn">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--  <a class="pop-add-icon copyServiceCompanion" href="javascript:;"><i
                                                                 class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                     <a class="pop-add-icon removeServiceCompanion" href="javascript:;"
                                                        style="display:none"><i class="fa fa-plus-circle"
                                                                                aria-hidden="true"></i></a> -->


                                                        </form>
                                                        <!-- append service listing -->


                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table id="TenantService-table"
                                                                       class="table table-bordered">
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="edit-foot" id="edit_animal_button">
                                                            <a href="javascript:;">
                                                                <i class="fa fa-pencil-square-o"
                                                                   aria-hidden="true"></i>
                                                                Edit
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Form-outer Ends-->
                                    </div>
                                    <!-- Second Tab Ends-->
                                    <div class="tab-pane" id="tenant-acc-tab4">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Billing Information</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Billing Address 1 :</label>
                                                                <span>56 High Street</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Billing Address 3 :</label>
                                                                <span>Lower Level</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Zip/Postal Code :</label>
                                                                <span>12345</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">City :</label>
                                                                <span>Los Angeles</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Billing Address 2 :</label>
                                                                <span>#23</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Billing Address 4 :</label>
                                                                <span></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">State Province :</label>
                                                                <span>CA</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Billing Email :</label>
                                                                <span>francis5632@gmail.com</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Fourth Tab Ends-->
                                    <div class="tab-pane" id="tenant-acc-tab5">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Conversation</h3>
                                            </div>
                                            <div class="form-data">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Fifth Tab Ends-->
                                    <div class="tab-pane" id="tenant-acc-tab6">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>Change Password</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Current Password <em class="red-star">*</em></label>
                                                        <select class="form-control">
                                                            <option>Select</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>New Password <em class="red-star">*</em></label>
                                                        <select class="form-control">
                                                            <option>Select</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-2">
                                                        <label>Confirm New Password <em class="red-star">*</em></label>
                                                        <select class="form-control">
                                                            <option>Select</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="btn-outer">
                                                    <button class="blue-btn">Save</button>
                                                    <button class="grey-btn">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Sixth Tab Ends-->
                                    <div class="tab-pane" id="tenant-acc-tab7">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Give Notice</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="detail-outer">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Notice Given Date :</label>
                                                                <span>August 30, 2019 (Fri)</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Reason for Leaving :</label>
                                                                <span>Lower Level</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Schedule Moveout Date :</label>
                                                                <span>November 30, 2019 (Fri)</span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Notice Name :</label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Address :</label>
                                                                <span></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">City :</label>
                                                                <span></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">Postal Code :</label>
                                                                <span></span>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <label class="text-right">State :</label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Seventh Tab Ends-->
                                    <div class="tab-pane" id="tenant-acc-tab8">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Occupants</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="grid-outer">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-dark">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Name</th>
                                                                <th scope="col">SSN/SIN/ID</th>
                                                                <th scope="col">Relationship</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Eight Tab Ends-->
                                    <div class="tab-pane" id="tenant-acc-tab9">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Tenant Leases</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="grid-outer">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-dark">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Tenant Name</th>
                                                                <th scope="col">Property</th>
                                                                <th scope="col">Unit</th>
                                                                <th scope="col">Rent(USh)</th>
                                                                <th scope="col">Lease ID</th>
                                                                <th scope="col">Days Remaining</th>
                                                                <th scope="col">Status</th>
                                                                <th scope="col">Action</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Ninth Tab Ends-->
                                    <div class="tab-pane" id="tenant-acc-tab10">
                                        <div class="form-outer form-outer2">
                                            <div class="form-hdr">
                                                <h3>Tenant Leases</h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="property-status">
                                                    <button type="button" class="blue-btn pull-right">Make Payment</button>
                                                </div>
                                                <div class="grid-outer">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-dark">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Date</th>
                                                                <th scope="col">Description</th>
                                                                <th scope="col">Charge Code</th>
                                                                <th scope="col">Original Amount(USh)</th>
                                                                <th scope="col">Amount Paid(USh)</th>
                                                                <th scope="col">Amount Due(USh)</th>
                                                                <th scope="col">Amount Waived Off(USh)</th>
                                                                <th scope="col">Waive Off Comment(USh)</th>
                                                                <th scope="col">Current Payment(USh) </th>
                                                                <th scope="col">Priority</th>
                                                                <th scope="col">Pay</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                            <tbody>
                                                            <tr>
                                                                <td>November 22, 2018 (Thu.)</td>
                                                                <td>Lease Breakage Fee</td>
                                                                <td>LBRKFee</td>
                                                                <td>564.00</td>
                                                                <td>0.00</td>
                                                                <td>478.00</td>
                                                                <td>0.00</td>
                                                                <td><textarea class="form-control"></textarea></td>
                                                                <td><input class="form-control" type="text"/></td>
                                                                <td><input class="form-control" type="text"/></td>
                                                                <td>4</td>
                                                                <td><input type="checkbox"/></td>
                                                            </tr>
                                                            </tbody>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Tenth Tab Ends-->
                                </div>
                            </div>
                        </div>
                        <!--Tabs End -->
                    </div>
                </div>
            </div>
        </section>

        <div id="imageModel" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-sm-4" id="getImage1"></div>
                        <div class="col-sm-4" id="getImage2"></div>
                        <div class="col-sm-4" id="getImage3"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>





        <!-- Jquery Starts -->
        <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
        <script>var staticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Vehicledummy.png">';</script>
        <script>var petStaticImage = '<img src="<?php echo COMPANY_SITE_URL ?>/images/Petdummy.jpeg">';</script>
        <script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>

        <script src="<?php echo COMPANY_SITE_URL; ?>/js/intlTelInput.js"></script>
        <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
        <script src="<?php echo COMPANY_SITE_URL; ?>/js/validation/custom_fields.js"></script>
        <script src="<?php echo COMPANY_SITE_URL; ?>/js/custom_fields.js"></script>
        <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.cropit.js"></script>
        <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>
        <script src="<?php echo COMPANY_SITE_URL; ?>/js/company/people/tenant/tenantportal.js"></script>

    </div>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/tenant_portal_footer.php");
?>