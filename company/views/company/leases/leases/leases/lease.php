<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Leases &gt;&gt; <span>List of Leases</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links1 ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>LEASES</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >New Lease</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="new_building_href" href="#" class="list-group-item list-group-item-success strong collapsed" >New Guest Card</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="/RentalApplication/RentalApplication" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Rental Application</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">Unit Vacancy Details</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="/Lease/LeasesAffordability" class="list-group-item list-group-item-success strong collapsed">Lease Affordability Calculator</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Manual Lease Renewal Request</a>
                                <!-- Seven Ends-->
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links 2---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" ><a href="/GuestCard/ListGuestCard" >Guest Cards</a></li>
                            <li role="presentation" ><a href="/RentalApplication/RentalApplications">Rental Applications</a></li>
                            <li role="presentation" class="active"><a href="/Lease/ViewEditLease" >Leases</a></li>
                            <li role="presentation"><a href="/Lease/Movein/0/" >Move-In</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <section class="main-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="content-data">

                                        <!--Tabs Starts -->
                                        <div class="main-tabs">
                                            <!-- Nav tabs -->
                                            <div class="right-links-outer">
                                                <div class="right-links slide-toggle2"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                                                <div id="RightMenu" class="box2" style="display: none">
                                                    <h2>PEOPLE</h2>
                                                    <div class="list-group panel">
                                                        <a href="/Tenantlisting/add" class="list-group-item list-group-item-success strong collapsed">New Tenant</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Move Out</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Book Now</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Owner</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Vendor</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Employee</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Contact</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Recieve Payment</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Send Tenant Statements</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Bank Deposite</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">New Bill</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Instrument Register</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Vendor Instrument Register</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Tenant Transfer</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Tenant Transfer</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move In</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">All Move Out</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Tenants</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Owners</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">Former Vendors</a>
                                                        <a href="javascript:void(0)" class="list-group-item list-group-item-success strong collapsed">NSF Listing</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="people-tenant">
                                                    <!-- Sub Tabs Starts-->
                                                    <div class="sub-tabss sub-navs">
                                                        <!-- Nav tabs -->
                                                        <!-- Tab panes -->

                                                            <div role="tabpanel" class="tab-pane active" id="regular-rent">
                                                                <div class="property-status">
                                                                    <div class="row">
                                                                        <div class="col-sm-2 tenant_type_status">
                                                                            <label>Status</label>
                                                                            <select class="fm-txt form-control"  id="jqgridOptions">
                                                                                <option value="All">All</option>
                                                                                <option value="1">Active</option>
                                                                                <option value="6">Move In</option>
                                                                                <option value="7">Renewed</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                            <div class="btn-outer text-right">
                                                                                <a href="/Lease/Leases" class="blue-btn">New Lease</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div  style="display: none;" id="import_tenant_type_div">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">

                                                                                    <a>Import Tenant Type</a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                <div class="panel-body">
                                                                                    <form name="importTenantTypeForm" id="importTenantTypeFormId" >
                                                                                        <div class="row">
                                                                                            <div class="form-outer">
                                                                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                                                                    <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                                                                    <span class="error"></span>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="col-xs-12">
                                                                                                <div class="btn-outer">
                                                                                                    <button type="submit" class="blue-btn">Submit</button>
                                                                                                    <button type="button" id="import_tenant_cancel_btn" class="grey-btn">Cancel</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="accordion-grid">
                                                                    <div class="accordion-outer">
                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">
                                                                                <div class="panel panel-default">

                                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                        <div class="panel-body pad-none">
                                                                                            <div class="grid-outer">
                                                                                                <div class="table-responsive">
                                                                                                    <div class="grid-outer">
                                                                                                        <div class="apx-table">
                                                                                                            <table class="table table-hover table-dark" id="lease_listing">
                                                                                                            </table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Regular Rent Ends -->
                                                            <div role="tabpanel" class="tab-pane" id="shortterm-rent">

                                                            </div>
                                                    </div>
                                                    <!-- Sub tabs ends-->
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="people-owner">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Status</label>
                                                                <select class="fm-txt form-control"> <option>Active</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-10">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn">Download Sample</button>
                                                                    <button class="blue-btn">Import Owner</button>
                                                                    <button onclick="window.location.href='new-property.html'" class="blue-btn">New Owner</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Owners</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Owner Name</th>
                                                                                                <th scope="col">Company</th>
                                                                                                <th scope="col">Phone</th>
                                                                                                <th scope="col">Email</th>
                                                                                                <th scope="col">Date Created</th>
                                                                                                <th scope="col">Owner's Portal</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                                                <td></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>allen09@gmail.com</td>
                                                                                                <td>11/23/2018 (Fri.)</td>
                                                                                                <td>Yes</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                                                <td></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>allen09@gmail.com</td>
                                                                                                <td>11/23/2018 (Fri.)</td>
                                                                                                <td>Yes</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                                                <td></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>allen09@gmail.com</td>
                                                                                                <td>11/23/2018 (Fri.)</td>
                                                                                                <td>Yes</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Alien N West</a></td>
                                                                                                <td></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>allen09@gmail.com</td>
                                                                                                <td>11/23/2018 (Fri.)</td>
                                                                                                <td>Yes</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="people-vendor">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Vendor Type</label>
                                                                <select class="fm-txt form-control"> <option>Select</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>

                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Status</label>
                                                                <select class="fm-txt form-control"> <option>Active</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>

                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn">Download Sample</button>
                                                                    <button class="blue-btn">Import Vendor</button>
                                                                    <button onclick="window.location.href='new-property.html'" class="blue-btn">New Vendor</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Vendors</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Vendor Name</th>
                                                                                                <th scope="col">Phone</th>
                                                                                                <th scope="col">Open Work Orders</th>
                                                                                                <th scope="col">YTD Payment</th>
                                                                                                <th scope="col">Type</th>
                                                                                                <th scope="col">Rate</th>
                                                                                                <th scope="col">Rating</th>
                                                                                                <th scope="col">Email</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>1</td>
                                                                                                <td>0.00</td>
                                                                                                <td>Contractor-General</td>
                                                                                                <td>$19.00 /hr</td>
                                                                                                <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                                                <td>skkokjim@gmail.com</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>1</td>
                                                                                                <td>0.00</td>
                                                                                                <td>Contractor-General</td>
                                                                                                <td>$19.00 /hr</td>
                                                                                                <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                                                <td>skkokjim@gmail.com</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>1</td>
                                                                                                <td>0.00</td>
                                                                                                <td>Contractor-General</td>
                                                                                                <td>$19.00 /hr</td>
                                                                                                <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                                                <td>skkokjim@gmail.com</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">ABBOTT LLC</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>1</td>
                                                                                                <td>0.00</td>
                                                                                                <td>Contractor-General</td>
                                                                                                <td>$19.00 /hr</td>
                                                                                                <td class="rating-star"><i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star-o" aria-hidden="true"></i></td>
                                                                                                <td>skkokjim@gmail.com</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="people-contact">
                                                    <!-- Sub Tabs Starts-->
                                                    <div class="sub-tabs">
                                                        <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li role="presentation" class="active"><a href="#contact-tenant" aria-controls="home" role="tab" data-toggle="tab">Tenants<span class="tab-count">84</span></a></li>
                                                            <li role="presentation"><a href="#contact-owner" aria-controls="profile" role="tab" data-toggle="tab">Owners<span class="tab-count">36</span></a></li>
                                                            <li role="presentation"><a href="#contact-vendor" aria-controls="home" role="tab" data-toggle="tab">Vendors<span class="tab-count">71</span></a></li>
                                                            <li role="presentation"><a href="#contact-users" aria-controls="profile" role="tab" data-toggle="tab">Users<span class="tab-count">199</span></a></li>
                                                            <li role="presentation"><a href="#contact-others" aria-controls="profile" role="tab" data-toggle="tab">Others<span class="tab-count">84</span></a></li>
                                                        </ul>
                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active" id="contact-tenant">
                                                                <div class="property-status">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <label>Status</label>
                                                                            <select class="fm-txt form-control"> <option>Active</option>
                                                                                <option></option>
                                                                                <option></option>
                                                                                <option></option>
                                                                            </select>

                                                                        </div>
                                                                        <div class="col-sm-10">
                                                                            <div class="btn-outer text-right">
                                                                                <button class="blue-btn">Download Sample</button>
                                                                                <button class="blue-btn">Import Contact</button>
                                                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Contact</button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="accordion-grid">
                                                                    <div class="accordion-outer">
                                                                        <div class="bs-example">
                                                                            <div class="panel-group" id="accordion">
                                                                                <div class="panel panel-default">
                                                                                    <div class="panel-heading">
                                                                                        <h4 class="panel-title">
                                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                                <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Contacts</a>
                                                                                        </h4>
                                                                                    </div>
                                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                        <div class="panel-body pad-none">
                                                                                            <div class="grid-outer">
                                                                                                <div class="table-responsive">
                                                                                                    <table class="table table-hover table-dark">
                                                                                                        <thead>
                                                                                                        <tr>
                                                                                                            <th scope="col">Contact Name</th>
                                                                                                            <th scope="col">Phone</th>
                                                                                                            <th scope="col">Email</th>
                                                                                                            <th scope="col">Date Created</th>
                                                                                                            <th scope="col">Status</th>
                                                                                                            <th scope="col">Actions</th>
                                                                                                        </tr>
                                                                                                        </thead>
                                                                                                        <tbody>
                                                                                                        <tr>
                                                                                                            <td><a class="grid-link" href="javascript:;">Abby N Wesley</a></td>
                                                                                                            <td>555-444-6666</td>
                                                                                                            <td>abby768@gmail.com</td>
                                                                                                            <td>12/7/2018 12:30:53 PM</td>
                                                                                                            <td>Active</td>
                                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td><a class="grid-link" href="javascript:;">Arsh Sandhu</a></td>
                                                                                                            <td>555-444-6666</td>
                                                                                                            <td>abby768@gmail.com</td>
                                                                                                            <td>12/7/2018 12:30:53 PM</td>
                                                                                                            <td>Active</td>
                                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td><a class="grid-link" href="javascript:;">Ben Snow</a></td>
                                                                                                            <td>555-444-6666</td>
                                                                                                            <td>abby768@gmail.com</td>
                                                                                                            <td>12/7/2018 12:30:53 PM</td>
                                                                                                            <td>Active</td>
                                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td><a class="grid-link" href="javascript:;">Asron Properties</a></td>
                                                                                                            <td>555-444-6666</td>
                                                                                                            <td>abby768@gmail.com</td>
                                                                                                            <td>12/7/2018 12:30:53 PM</td>
                                                                                                            <td>Active</td>
                                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                                        </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Regular Rent Ends -->
                                                            <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                                            </div>
                                                            <div role="tabpanel" class="tab-pane" id="contact-tenant">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Sub tabs ends-->
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="people-employee">
                                                    <div class="property-status">
                                                        <div class="row">
                                                            <div class="col-sm-2">
                                                                <label>Status</label>
                                                                <select class="fm-txt form-control"> <option>Active</option>
                                                                    <option></option>
                                                                    <option></option>
                                                                    <option></option>
                                                                </select>

                                                            </div>
                                                            <div class="col-sm-10">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn">Download Sample</button>
                                                                    <button class="blue-btn">Import Employee</button>
                                                                    <button onclick="window.location.href='new-property.html'" class="blue-btn">New Employee</button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> List of Employees</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Employee Name</th>
                                                                                                <th scope="col">Phone</th>
                                                                                                <th scope="col">Email</th>
                                                                                                <th scope="col">Date Created</th>
                                                                                                <th scope="col">Status</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>adam@gmail.com</td>
                                                                                                <td>11/23/2018 8:48:45 AM</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>adam@gmail.com</td>
                                                                                                <td>11/23/2018 8:48:45 AM</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>adam@gmail.com</td>
                                                                                                <td>11/23/2018 8:48:45 AM</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><a class="grid-link" href="javascript:;">Adam Scott</a></td>
                                                                                                <td>555-444-6666</td>
                                                                                                <td>adam@gmail.com</td>
                                                                                                <td>11/23/2018 8:48:45 AM</td>
                                                                                                <td>Active</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Tabs Ends -->

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->



<!-- Footer Ends -->
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    $(document).ready(function(){


        $("#show").click(function(){
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function(){
            $("#bs-example-navbar-collapse-2").hide();
        });

        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });

        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/leases/leases/leaseListing.js"></script>



<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
