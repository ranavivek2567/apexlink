<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

/*if (!isset($_SESSION[SESSION_DOMAIN]['user_id']) && ($_SESSION[SESSION_DOMAIN]['user_id'] == '')) {
    $url = DOMAIN_URL;
    header('Location: ' . $url);
}*/
?>

<?php include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php"); ?>
<?php include_once(COMPANY_DIRECTORY_URL . "/views/company/tenants/modals.php"); ?>

<div class="popup-bg"></div>


<div id="wrapper">

    <input type='hidden' name='tenant_session_id' class="tenant_session_id" value="1">

    <?php

    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>

    <main class="apxpg-main">
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="bread-search-outer apxpg-top-search">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="breadcrumb-outer">
                                    Leases &gt;&gt; <span>New Leases</span>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 pull-right">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-data apxpg-allcontent">
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="javascript:void(0);"
                                                                          data-tab="property">Select Property</a></li>
                                <li role="presentation" class="tab_alert"><a href="javascript:void(0);" data-tab="lease">Lease Details</a>
                                </li>
                                <li role="presentation" class="tab_alert"><a href="javascript:void(0);" data-tab="charges">Charges</a>
                                </li>
                                <li role="presentation" class="tab_alert"><a href="javascript:void(0);" data-tab="generate">Generate
                                        Lease</a></li>
                            </ul>
                        </div>
                        <div class="overlay">
                            <div id='loadingmessage'
                                 style='display:none; position:absolute; position: fixed; margin: 0 auto;top: 50%; left: 45%;z-index: 1111111111'
                                 ;>
                                <img width="200" height="200"
                                     src='<?php echo SUPERADMIN_SITE_URL ?>/images/loading.gif'/>
                            </div>
                        </div>
                        <form id="addTenant" enctype='multipart/form-data'>


                            <input type="hidden" name="random_string" value="<?php echo rand(); ?>">
                            <input type="hidden" name="tenant_id" class="addTenantId">

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        <strong class="left">Select Property</strong>
                                        <a class="back" href="/Tenantlisting/Tenantlisting"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">

                                        <div class="col-sm-3 col-md-3 ">
                                            <label>Select Property <em class="red-star">*</em>
                                                <a class="pop-add-icon select_property_popup" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#selectProperty">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control" id="property" name="property">
                                                <option value="">Select</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3 ">
                                            <label>Select Building <em class="red-star">*</em>
                                                <a class="pop-add-icon select_building_popup" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#selectBuilding">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control" id="building" name="building">
                                                <option value="">Select</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3">
                                            <label>Select Unit <em class="red-star">*</em>
                                                <a class="pop-add-icon select_unit_popup" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#selectUnit">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </a>
                                            </label>
                                            <select class="form-control" id="unit" name="unit">
                                                <option value="">Select</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3 ">
                                            <label>Select Contact
                                                <a class="pop-add-icon" href="javascript:;">
                                                    <i class="fa fa-plus-circle" aria-hidden="true" title="Coming soon"></i>
                                                </a></label>
                                            <select class="form-control" id="contact" name="contact">
                                                <option value="">Select</option>
                                                <option value="1">Test</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>
                                        <strong class="left">Primary Tenant Info</strong>
                                    </h3>
                                </div>
                                <div class="form-data">
                                    <div class="row form-outer2">


                                        <div class="col-sm-12">
                                            <label><input type="checkbox" class="entity checkifcompany"> Check this box if this tenant is entity/company
                                                </label>
                                            <div class="row">
                                                <div class="form-outer contactTenant" style="display:none">
                                                    <div class="col-sm-3 col-md-3">
                                                        <div class=''>
                                                            <strong class='font-weight-bold'>Contact for this tenant <em class="red-star">*</em></strong>
                                                            <input class="form-control" type="text" name='contactTenant'>
                                                        </div>
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="upload-logo">
                                                <div class="tenant_image img-outer"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>

                                                <a href="javascript:;">
                                                    <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image</a>
                                                <span>(Maximum File Size Limit: 1MB)</span>
                                            </div>
                                            <div class="image-editor">
                                                <input type="file" class="cropit-image-input" name="tenant_image">
                                                <div class='cropItData' style="display: none;">
                                                    <span class="closeimagepopupicon">X</span>
                                                    <div class="cropit-preview"></div>
                                                    <div class="image-size-label">Resize image</div>
                                                    <input type="range" class="cropit-image-zoom-input">
                                                    <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                    <input type="button" class="export" value="Done"
                                                           data-val="tenant_image">
                                                </div>
                                            </div>
                                        </div>


                                        <!--     <div class="col-sm-12">
                                                            <label>Entity/Company Logo <em class="red-star">*</em></label>
                                                            <div class="upload-logo">
                                                                <div class="tenant_image img-outer"></div>

                                                                <a href="javascript:;"><i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Logo</a>
                                                                <span>(Maximum File Size Limit: 1MB)</span>
                                                            </div>
                                                            <div class="image-editor">
                                                                <input type="file" class="cropit-image-input" name="tenant_image">
                                                                <div class='cropItData'>
                                                                    <div class="cropit-preview"></div>
                                                                    <div class="image-size-label">Resize image</div>
                                                                    <input type="range" class="cropit-image-zoom-input">
                                                                    <input type="hidden" name="image-data" class="hidden-image-data" />
                                                                    <input type="button" class="export" value="Done" data-val="tenant_image">
                                                                </div>
                                                            </div>
                                                        </div> -->
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-12">
                                            <div class="tenant_hide_row">
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Salutation</label>
                                                            <select class="form-control" id="salutation"
                                                                    name="salutation">
                                                                <option value="Select">Select</option>
                                                                <option value="Dr.">Dr.</option>
                                                                <option value="Mr.">Mr.</option>
                                                                <option value="Mrs.">Mrs.</option>
                                                                <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                                <option value="Ms.">Ms.</option>
                                                                <option value="Sir">Sir</option>
                                                                <option value="Madam">Madam</option>
                                                                <option value="Brother">Brother</option>
                                                                <option value="Sister">Sister</option>
                                                                <option value="Father">Father</option>
                                                                <option value="Mother">Mother</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>First Name <em class="red-star">*</em></label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="firstname" name="firstname" maxlength="50">
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Middle Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="middlename" name="middlename" maxlength="50">
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Last Name <em class="red-star">*</em></label>
                                                            <input class="form-control capsOn" type="text" id="lastname"
                                                                   name="lastname" maxlength="50">
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3 ">

                                                            <label>Maiden Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="maidenname" name="maidenname" maxlength="50">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">

                                                            <label>Nick Name</label>
                                                            <input class="form-control capsOn" type="text" id="nickname"
                                                                   name="nickname" maxlength="50">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>

                                                    </div>

                                                </div>
                                            </div>

                                            <div class="row primary-tenant-phone-row">
                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>Phone Type</label>
                                                        <select class="form-control" name="phoneType[]">
                                                            <option value="">Select</option>
                                                            <option value="1">mobile</option>
                                                            <option value="2">work</option>
                                                            <option value="3">Fax</option>
                                                            <option value="4">Home</option>
                                                            <option value="5">Other</option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>Carrier <em class="red-star">*</em></label>
                                                        <select class="form-control" name="carrier[]"></select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 countycodediv">
                                                        <label>Country Code</label>
                                                        <select class="form-control" name="countryCode[]">
                                                            <option value="">Select</option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>Phone Number <em class="red-star">*</em></label>
                                                        <input class="form-control capsOn add-input phone_format" type="text"
                                                               name="phoneNumber[]">
                                                        <a class="add-icon" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <a class="add-icon" href="javascript:;" style=""><i
                                                                    class="fa fa-minus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Add Note for this Phone Number</label>
                                                        <input class="form-control  capital" type="text" id="note" name="note" placeholder="Add Note for this Phone Number">
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Email <em class="red-star">*</em></label>

                                                        <div class='multipleEmail'>
                                                            <input class="form-control add-input capsOn" type="text" name="email[]">
                                                            <a class="add-icon email-remove-sign" href="javascript:;">
                                                                <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                            </a>
                                                             <a class="add-icon email-plus-sign" href="javascript:;">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                        </a>
                                                        </div>


                                                       
                                                    </div>




                                                    <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                        <label>Referral Source
                                                            <a class="pop-add-icon propreerralicon" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="referralSource">
                                                            <option value="1">Bond</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyReferralResource1">
                                                            <h4>Add New Referral Source</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Referral Source <em class="red-star">*</em></label>
                                                                        <input name="referral" class="form-control customValidateGroup reff_source" type="text" data_required="true" data_max="150" placeholder="New Referral Source">
                                                                        <span class="customError required" aria-required="true" id="reff_source"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source" data-name="referralSource">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Move-In date</label>
                                                        <input class="form-control capsOn calander" type="text"
                                                               id="moveInDate" name="moveInDate">
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                    </div>

                                                </div>
                                            </div>
                                            <style>
                                         
                                            </style>
                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                        <label>Ethnicity
                                                            <a class="pop-add-icon selectPropertyEthnicity" href="javascript:;">
                                                                <i class="fa fa-plus-circle"aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="ethncity">
                                                            <option value="0">Select</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyEthnicity1">
                                                            <h4>Add New Ethnicity</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Ethnicity <em class="red-star">*</em></label>
                                                                        <input class="form-control ethnicity_src customValidateGroup" type="text" placeholder="Add New Ethnicity" data_required="true" >
                                                                        <span class="customError required red-star" aria-required="true" id="ethnicity_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn add_single1"  data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src" data-name="ethncity">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                        <label>Marital Status
                                                            <a class="pop-add-icon selectPropertyMaritalStatus" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="maritalStatus">
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyMaritalStatus1">
                                                            <h4>Add New Marital Status</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Marital Status <em class="red-star">*</em></label>
                                                                        <input class="form-control maritalstatus_src" type="text" placeholder="Add New Marital Status">
                                                                        <span class="customError required red-star" aria-required="true" id="maritalstatus_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src" data-name="maritalStatus">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                        <label>Hobbies
                                                            <a class="pop-add-icon selectPropertyHobbies" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="hobbies[]" multiple>
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyHobbies1">
                                                            <h4>Add New Hobbies</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New Hobbies <em class="red-star">*</em></label>
                                                                        <input class="form-control hobbies_src" type="text" placeholder="Add New Hobbies" data_required="true">
                                                                        <span class="red-star" id="hobbies_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src" data-name="hobbies">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                        <label>Veteran status
                                                            <a class="pop-add-icon selectPropertyVeteranStatus" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                            </a>
                                                        </label>
                                                        <select class="form-control" name="veteranStatus">
                                                            <option value="0">Select</option>
                                                        </select>
                                                        <div class="add-popup" id="selectPropertyVeteranStatus1">
                                                            <h4>Add New VeteranStatus</h4>
                                                            <div class="add-popup-body">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12">
                                                                        <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                        <input class="form-control veteran_src" type="text" placeholder="Add New VeteranStatus">
                                                                        <span class="red-star" id="veteran_src"></span>
                                                                    </div>
                                                                    <div class="btn-outer">
                                                                        <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src" data-name="veteranStatus">Save</button>
                                                                        <input type="button" class="grey-btn" value="Cancel">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>SSN/SIN/ID</label>
                                                        <div class='multipleSsn'>
                                                            <input class="form-control add-input capsOn" type="text"
                                                                   id="ssn" name="ssn[]" style="margin: 5px 0;">
                                                            <a class="add-icon ssn-remove-sign" href="javascript:;"
                                                               style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>



                                                            <span class="ffirst_nameErr error red-star"></span>



                                                             <a class="add-icon ssn-plus-sign" href="javascript:;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        </div>
                                                  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tenant_hide_row">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>
                                                    Tenant Additional Information
                                                </h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Birth Date</label>
                                                        <input class="form-control capsOn calander" type="text"
                                                               id="birth" name="birth" placeholder="Birth Date">
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>Gender</label>
                                                        <select class="form-control" id="gender" class="gender" name="gender">
                                                            <option value="">Select</option>
                                                            <option value="1">Male</option>
                                                            <option value="2">Female</option>
                                                            <option value="3">Prefer Not To Say</option>
                                                            <option value="4">Others</option>
                                                        </select>
                                                        <span class="term_planErr error red-star"></span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Driver License state/province</label>
                                                        <input class="form-control capsOn" type="text"
                                                               id="driverProvince" name="driverProvince">
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>

                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Driver's License</label>
                                                        <input class="form-control capsOn" type="text"
                                                               id="driverLicense" name="driverLicense">
                                                        <span class="flast_nameErr error red-star"></span>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Vehicle</label>
                                                            <div class="check-outer">
                                                                <input class="select_property_vehicle" type="radio"
                                                                       name='vehicle' value='1'>
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input class="select_property_vehicle" type="radio"
                                                                       name='vehicle' value='0' checked>
                                                                <label>No</label>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>

                                                <div class='property_vehicle' style="display:none;">
                                                    <div class="row">
                                                        <div class="grey-plus-section">

                                                            <div class="grey-plus-left">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Vehicle Type</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='vehicle_type[]'>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Make</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='vehicle_make[]'>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>License Plate Number</label>
                                                                            <input class="form-control" type="text"
                                                                                   maxlength="12" name='vevicle_license[]'>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Color</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='vehicle_color[]'>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <label>Year Of Vehicle</label>
                                                                        <select class="form-control" id="vevicle_year"
                                                                                name="vevicle_year[]">
                                                                            <option value="">Select</option>

                                                                            <?php
                                                                            for ($vehiclyear = date("Y"); $vehiclyear >= 1900; $vehiclyear--) {
                                                                                echo '<option value="' . $vehiclyear . '">' . $vehiclyear . '</option>';
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>VIN</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='vehicle_vin[]'>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <label>Registration #</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='vehicle_registration[]'>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-3 grey-plus-photo-upload">
                                                                        <label>Vehicle Photo/Image</label>
                                                                        <!--                                                                                    <div class="vehicle_image1"></div>-->
                                                                        <!--  <input class="form-control" type="file" id="vehicle_image1" name="vehicle_image1[]"> -->
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer vehicle_image1"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png"></div>
                                                                            <a class="choose-img" href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file" id="vehicle_image1"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="vehicle_image1[]">
                                                                            <div class='cropItData'>
                                                                                <div class="cropit-preview"></div>

                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input">
                                                                                <input type="hidden" name="image-data"
                                                                                       class="hidden-image-data"/>
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="vehicle_image1">
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-sm-12 col-md-3 grey-plus-photo-upload">
                                                                        <label>Vehicle Photo/Image</label>
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer vehicle_image2"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png"></div>
                                                                            <a class="choose-img" href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file" id="vehicle_image2"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="vehicle_image2[]">
                                                                            <div class='cropItData'>
                                                                                <div class="cropit-preview"></div>

                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input">
                                                                                <input type="hidden" name="image-data"
                                                                                       class="hidden-image-data"/>
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="vehicle_image2">
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-sm-12 col-md-3 grey-plus-photo-upload">
                                                                        <label>Vehicle Photo/Image</label>
                                                                        <div class="upload-logo">
                                                                            <div class="img-outer vehicle_image3"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png"></div>
                                                                            <a class="choose-img" href="javascript:;">Choose
                                                                                Image</a>
                                                                        </div>
                                                                        <div class="image-editor">
                                                                            <input type="file" id="vehicle_image2"
                                                                                   class="cropit-image-input form-control"
                                                                                   name="vehicle_image3[]">
                                                                            <div class='cropItData'>
                                                                                <div class="cropit-preview"></div>

                                                                                <!--  <div class="image-size-label">Resize image</div> -->
                                                                                <input type="range"
                                                                                       class="cropit-image-zoom-input">
                                                                                <input type="hidden" name="image-data"
                                                                                       class="hidden-image-data"/>
                                                                                <input type="button" class="export"
                                                                                       value="Done"
                                                                                       data-val="vehicle_image3">
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                            </div>
                                                            <div class="grey-plus-right">
                                                                <a class="pop-add-icon copyVehicle" href="javascript:;"><i
                                                                            class="fa fa-plus-circle"
                                                                            aria-hidden="true"></i></a>

                                                                <a class="pop-add-icon removeVehicle"
                                                                   href="javascript:;" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>

                                                            </div>
                                                        </div>


                                                    </div>

                                                    <div class="row">

                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12">
                                                            <label>Pet</label>
                                                            <div class="check-outer">
                                                                <input class="select_property_pet" name='pet'
                                                                       type="radio" value='1'>
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input class="select_property_pet" name='pet'
                                                                       type="radio" value='0' checked>
                                                                <label>No</label>
                                                                <span class="flast_nameErr error red-star"></span>

                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>

                                                <div class='property_pet' style="display:none;">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Pet Name</label>
                                                                    <input class="form-control" type="text"
                                                                           name='pet_name[]' maxlength="18">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Pet ID</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name='pet_id[]' maxlength="8">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Pet Type/Breed</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name='pet_type[]' maxlength="18">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Birth Date</label>
                                                                    <input class="form-control calander" type="text"
                                                                           name='pet_birth[]' readonly>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 ">
                                                                <label>Pet Age</label>
                                                                <select class="form-control" name="pet_age[]">
                                                                    <option value="">Select</option>
                                                                    <?php
                                                                    for ($petAge = 1; $petAge <= 100; $petAge++) {
                                                                        echo '<option value="' . $petAge . '">' . $petAge . '</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <label>Gender
                                                                    <a class="pop-add-icon selectPropertyPetSex" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                </label>
                                                                <select class="form-control" id="pet_gender" name="pet_gender[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1">Male</option>
                                                                    <option value="2">Female</option>
                                                                    <option value="3">Other</option>
                                                                </select>
                                                                <div class="add-popup" id="selectPropertyPetSex1">
                                                                    <h4>Add New Sex</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Add New Gender <em class="red-star">*</em></label>
                                                                                <input class="form-control gender_source" type="text" placeholder="Add New Gender">
                                                                                <span class="red-star" id="gender_source"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-table="tenant_pet_gender" data-cell="gender" data-class="gender_source" data-name="pet_gender[]">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Pet Weight</label>
                                                                    <input class="form-control" type="text"
                                                                           name='pet_weight[]' value='' maxlength="5">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-1 col-md-2">
                                                                <label>Pet Weight Unit</label>
                                                                <select class="form-control" id="pet_weight_unit"
                                                                        name="pet_weight_unit[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1">lb</option>
                                                                    <option value="2">lbs</option>
                                                                    <option value="3">kg</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Note</label>
                                                                    <textarea class="form-control"
                                                                              name='pet_note[]' maxlength="200"></textarea>
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Pet Color</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name='pet_color[]'  maxlength="10">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Chip ID</label>
                                                                    <input class="form-control" type="text"
                                                                           name='pet_chipid[]' maxlength="12">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                            <!-- <span class="ffirst_nameErr error red-star"></span> -->
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Vet\Hosp. Name</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name='pet_vet[]' maxlength="20">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 countycodediv">
                                                                <label>Country Code</label>
                                                                <select class="form-control" name="pet_countryCode[]">
                                                                    <option value="">Select</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Phone Number</label>
                                                                    <input type='text' class="form-control"
                                                                           name='pet_phoneNumber[]'>
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Last Visit</label>
                                                                    <input class="form-control calander" type="text"
                                                                           name='pet_lastVisit[]' >
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Next Visit</label>
                                                                    <input class="form-control calander" type="text"
                                                                           name='pet_nextVisit[]'>
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Medical Condition</label>
                                                                <div class="check-outer">
                                                                    <input name='pet_medical[]' type="radio" value='1'
                                                                           class="pet_medical_condition">
                                                                    <label>Yes</label>
                                                                </div>
                                                                <div class="check-outer">
                                                                    <input name='pet_medical[]' type="radio" value='0'
                                                                           class="pet_medical_condition" checked>
                                                                    <label>No</label>
                                                                </div>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="pet_medical_html" style="display:none">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-3">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Please Enter Pet's Medical Condition</label>
                                                                        
                                                                                   <textarea class="form-control" name="pet_medical_condition_note[]"></textarea>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Shots</label>
                                                                <div class="check-outer">
                                                                    <input name='pet_shots[]' type="radio" value='1'
                                                                           class='pet_name_shot'>
                                                                    <label>Yes</label>
                                                                </div>
                                                                <div class="check-outer">
                                                                    <input name='pet_shots[]' type="radio" value='0'
                                                                           class="pet_name_shot" checked>
                                                                    <label>No</label>
                                                                </div>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="pet_shot_date" style="display:none;">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Name of the shot</label>
                                                                        <input class="form-control capital" type="text"
                                                                               name='pet_name_shot[]'>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Date Given</label>
                                                                        <input class="form-control calander" type="text"
                                                                               name='pet_date_given[]' readonly>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Expiration Date</label>
                                                                        <input class="form-control calander" type="text"
                                                                               name='pet_expiration_date[]'
                                                                               value="<?php echo $today = date("Y-m-d"); ?>">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Follow Up</label>
                                                                        <input class="form-control calander" type="text"
                                                                               name='pet_follow_up[]'>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Note</label>
                                                                        <input class="form-control" type="text"
                                                                               name='pet_note[]'>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>

                                                        </div>


                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Rabies</label>
                                                            <div class="check-outer">
                                                                <input name='pet_rabies[]' type="radio" value='1'
                                                                       class="pet_rabies">
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input name='pet_rabies[]' class="pet_rabies"
                                                                       type="radio" value='0' checked>
                                                                <label>No</label>
                                                            </div>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>

                                                        <div class="clearfix"></div>


                                                        <div class="pet_rabies_html" style="display:none">

                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Rabies#</label>
                                                                        <input class="form-control capital" type="text"
                                                                               name='pet_rabies_name[]'>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Date Given</label>
                                                                        <input class="form-control calander" type="text"
                                                                               name='pet_rabies_given_date[]'
                                                                               value="<?php echo $today = date("Y-m-d"); ?>">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Expiration Date</label>
                                                                        <input class="form-control calander" type="text"
                                                                               name='pet_rabies_expiration_date[]'
                                                                               value="<?php echo $today = date("Y-m-d"); ?>">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-12 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Note</label>
                                                                        <input class="form-control" type="text"
                                                                               name='pet_rabies_note[]'>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>

                                                        </div>

                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Pet Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer pet_image1"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg"></div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="pet_image1[]" accept="image/*">
                                                                    <div class="cropItData" style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image" alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="pet_image1"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input" min="0"
                                                                               max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="pet_image1">
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Pet Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer pet_image2"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg"></div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="pet_image2[]" accept="image/*">
                                                                    <div class="cropItData" style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image" alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="pet_image2"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input" min="0"
                                                                               max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="pet_image2">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Pet Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer pet_image3"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg"></div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="pet_image3[]" accept="image/*">
                                                                    <div class="cropItData" style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image" alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="pet_image3"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input" min="0"
                                                                               max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="pet_image3">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <a class="pop-add-icon copyPet" href="javascript:;" style="float: right; margin-right: 30px;"><i
                                                                    class="fa fa-plus-circle"
                                                                    aria-hidden="true"></i></a>
                                                        <a class="pop-add-icon removePet" href="javascript:void(0);" style="float: right; margin-right: 30px; display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Service/Companion Animals</label>
                                                            <div class="check-outer">
                                                                <input class="select_property_service" name='companion'
                                                                       type="radio" value='1'>
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input class="select_property_service" name='companion'
                                                                       type="radio" value="0" checked>
                                                                <label>No</label>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class='property_service' style="display:none;">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Name</label>
                                                                    <input class="form-control" type="text"
                                                                           name='service_name[]' maxlength="18">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>ID</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name='service_id[]' maxlength="8">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Type/Breed</label>
                                                                    <input class="form-control" type="text"
                                                                           name='service_type[]' maxlength="18">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Birth Date</label>
                                                                    <input class="form-control calander" type="text"
                                                                           name='service_birth[]'>
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <label>Age</label>
                                                                <select class="form-control" id="service_year"
                                                                        name="service_year[]">
                                                                    <option value="">Select</option>
                                                                    <?php
                                                                    for ($serviceAge = 1; $serviceAge <= 100; $serviceAge++) {
                                                                        echo '<option value="' . $serviceAge . '">' . $serviceAge . '</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2 ">
                                                                <label>Gender
                                                                   <a class="pop-add-icon selectPropertyAnimalSex" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                </label>
                                                                <select class="form-control" name="service_gender[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1">Male</option>
                                                                    <option value="2">Female</option>
                                                                    <option value="3">Other</option>
                                                                </select>
                                                                <div class="add-popup" id="selectPropertyAnimalSex1">
                                                                    <h4>Add New Sex</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Add New Gender <em class="red-star">*</em></label>
                                                                                <input class="form-control gender_source1" type="text" placeholder="Add New Gender">
                                                                                <span class="red-star" id="gender_source1"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-table="tenant_service_animal_gender" data-cell="gender" data-class="gender_source1" data-name="service_gender[]">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <span class="ffirst_nameErr error red-star"></span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Weight</label>
                                                                    <input class="form-control" type="text"
                                                                           name='service_weight[]' maxlength="4">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2 ">
                                                                <label>Weight Unit</label>
                                                                <select class="form-control"
                                                                        name="service_weight_unit[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1">lb</option>
                                                                    <option value="2">lbs</option>
                                                                    <option value="3">kg</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Note</label>
                                                                    <textarea class="form-control"
                                                                              name='service_note[]' maxlength="200"></textarea>
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Color</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name='service_color[]' maxlength="10">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Chip ID</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name='service_chipid[]' maxlength="10">
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>vet\Hosp. Name</label>
                                                                    <input class="form-control capital" type="text"
                                                                           name='service_vet[]' maxlength="18">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 countycodediv">
                                                                <label>Country Code</label>
                                                                <select class="form-control" name="service_countryCode[]">
                                                                    <option value="">Select</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <label>Phone Number</label>
                                                                    <input type='text' class="form-control"
                                                                           name='service_phoneNumber[]'>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Last Visit</label>
                                                                    <input class="form-control calander" type="text"
                                                                           name='service_lastVisit[]'>
                                                                </div>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <div class='contactTenant'>
                                                                    <!--calander -->
                                                                    <label>Next Visit</label>
                                                                    <input class="form-control calander" type="text"
                                                                           name='service_nextVisit[]'>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Medical Condition</label>
                                                                <div class="check-outer">
                                                                    <input name='service_medical[]' type="radio"
                                                                           value='1' class="service_medical_condition">
                                                                    <strong class='font-weight-bold'>Yes</strong>
                                                                </div>
                                                                <div class="check-outer">
                                                                    <input name='service_medical[]' type="radio"
                                                                           value='0' class="service_medical_condition" checked>
                                                                    <strong class='font-weight-bold'>No</strong>
                                                                </div>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                            <div class="service_medical_html" style="display:none">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-12 col-md-3">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Please Enter Animal's Medical Condition</label>
                                                                        
                                                                                   <textarea class="form-control" name="service_medical_condition_note[]"></textarea>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--<div class="row spaceNumber">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Parking Space Number<em class="red-star">*</em></label>
                                                                        <input class="form-control capsOn" type="text" id="parking_space" name="parking_space[]">
                                                                        <span class="flast_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                            </div>-->
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Shots</label>
                                                                <input name='service_shots[]' type="radio" value='1'
                                                                       class="service_shots"><strong
                                                                        class='font-weight-bold'>Yes</strong>
                                                                <input name='service_shots[]' type="radio" value='0'
                                                                       class="service_shots" checked><strong
                                                                        class='font-weight-bold'>No</strong>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="animal_shot_date" style="display:none;">
                                                            <div class="form-outer">
                                                                <div class="col-sm-12 col-md-12">
                                                                    <div class="col-sm-2 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Name of the shot</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='service_name_shot[]'>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-2 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Date Given</label>
                                                                            <input class="form-control calander"
                                                                                   type="text"
                                                                                   name='service_date_given[]'
                                                                                   value="<?php echo $today = date('Y-m-d'); ?>">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-2 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Expiration Date</label>
                                                                            <input class="form-control calander"
                                                                                   type="text"
                                                                                   name='service_expiration_date[]'
                                                                                   value="<?php echo $today = date("Y-m-d"); ?>">
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-2 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Follow Up</label>
                                                                            <input class="form-control calander"
                                                                                   type="text"
                                                                                   name='service_follow_up[]'>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2">
                                                                        <div class='contactTenant'>
                                                                            <!--calander -->
                                                                            <label>Note</label>
                                                                            <input class="form-control" type="text"
                                                                                   name='service_note[]'>
                                                                        </div>
                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Rabies</label>
                                                                <input name='service_rabies[]' type="radio" value='1'
                                                                       class="animal_rabies"><strong
                                                                        class='font-weight-bold'>Yes</strong>
                                                                <input name='service_rabies[]' type="radio" value='0'
                                                                       class="animal_rabies" checked><strong
                                                                        class='font-weight-bold'>No</strong>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row animal_rabies_html" style="display:none">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-12">
                                                                <div class="col-sm-2 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <label>Rabies#</label>
                                                                        <input class="form-control" type="text"
                                                                               name='animal_rabies[]'>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-2 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Date Given</label>
                                                                        <input class="form-control calander" type="text"
                                                                               name='animal_date_given[]'
                                                                               value="<?php echo $today = date("Y-m-d"); ?>">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-2 col-md-2">
                                                                    <div class='contactTenant'>

                                                                        <label>Expiration Date</label>
                                                                        <input class="form-control calander" type="text"
                                                                               name='animal_expiration_date[]'
                                                                               value="<?php echo $today = date("Y-m-d"); ?>">
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-2 col-md-2">
                                                                    <div class='contactTenant'>
                                                                        <!--calander -->
                                                                        <label>Note</label>
                                                                        <input class="form-control" type="text"
                                                                               name='animal__note[]'>
                                                                    </div>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Animal Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer service_image1"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg"></div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="service_image1[]" accept="image/*">
                                                                    <div class="cropItData" style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image" alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="service_image1"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input" min="0"
                                                                               max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="service_image1">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Animal Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer service_image2"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg"></div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="service_image2[]" accept="image/*">
                                                                    <div class="cropItData" style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image" alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="service_image2"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input" min="0"
                                                                               max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="service_image2">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 col-md-2 grey-plus-photo-upload">
                                                                <label>Animal Photo/Image</label>
                                                                <!-- <input class="form-control" type="file"  name="pet_image1[]"> -->
                                                                <div class="upload-logo">
                                                                    <div class="img-outer service_image3"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg"></div>
                                                                    <a class="choose-img" href="javascript:;">Choose
                                                                        Image</a>
                                                                </div>
                                                                <div class="image-editor">
                                                                    <input type="file"
                                                                           class="cropit-image-input form-control"
                                                                           name="service_image3[]" accept="image/*">
                                                                    <div class="cropItData" style="display: none;">
                                                                        <div class="cropit-preview cropit-image-loading"
                                                                             style="position: relative; width: 250px; height: 250px;">
                                                                            <div class="cropit-preview-image-container"
                                                                                 style="position: absolute; overflow: hidden; left: 0px; top: 0px; width: 100%; height: 100%;">
                                                                                <img class="cropit-preview-image" alt=""
                                                                                     style="transform-origin: left top; will-change: transform;">
                                                                            </div>
                                                                        </div>
                                                                        <div class="service_image3"></div>
                                                                        <!--  <div class="image-size-label">Resize image</div> -->
                                                                        <input type="range"
                                                                               class="cropit-image-zoom-input" min="0"
                                                                               max="1" step="0.01">
                                                                        <input type="hidden" name="image-data"
                                                                               class="hidden-image-data">
                                                                        <input type="button" class="export" value="Done"
                                                                               data-val="service_image3">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="pop-add-icon copyServiceCompanion" href="javascript:;" style="margin-right:30px; float:right"><i
                                                                class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                    <a class="pop-add-icon removeServiceCompanion" href="javascript:;" style="margin-right:30px; float:right;display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Assigned Parking Space</label>
                                                            <div class="check-outer">
                                                                <input class="select_property_parking" name='space'
                                                                       type="radio" value='1'>
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input class="select_property_parking" name='space'
                                                                       type="radio" value='0' checked><strong
                                                                        class='font-weight-bold'>No</strong>
                                                                <label class="flast_nameErr error red-star"></label>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="property_parking" style="display:none;">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Parking Permit Number<em class="red-star">*</em></label>
                                                                <input class="form-control capsOn capital" type="text"
                                                                       id="parking_number" name="parking_number[]">
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row spaceNumber">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Parking Space Number<em
                                                                            class="red-star">*</em></label>
                                                                <input class="form-control capsOn capital" type="text"
                                                                       id="parking_space" name="parking_space[]">
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                            <a class="pop-add-icon copySpaceNumber" href="javascript:;"><i
                                                                        class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="pop-add-icon removeSpaceNumber" href="javascript:;"
                                                               style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>No. of Keys Signed for at Move In</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="parking_keys" name="parking_keys">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Medical/Allergies</label>
                                                            <div class="check-outer">
                                                                <input class="select_property_medical" type="radio"
                                                                       name='medical' value='1'>
                                                                <label>Yes</label>
                                                            </div>

                                                            <div class="check-outer">
                                                                <input class="select_property_medical" type="radio"
                                                                       name='medical' value='0' checked>
                                                                <label class='font-weight-bold'>No</label>
                                                            </div>

                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>

                                                <div class='property_medical' style='display:none;'>
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Tenant's medical or allergy issues</label>
                                                                <input class="radio form-control" type="text" name='medical_issue[]'>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Note</label>
                                                                <textarea class="medical_note form-control" type="text"
                                                                          name='medical_note[]'></textarea>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                            <!--calander -->
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Date</label>
                                                                <input class="radio calander form-control" type="text"
                                                                       name='medical_date[]'>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>
                                                            <a class="pop-add-icon copyMedical" href="javascript:;"><i
                                                                        class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="pop-add-icon removeMedical" href="javascript:;"
                                                               style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                            <div class="clearfix"></div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Smoker</label>
                                                            <div class="check-outer">
                                                                <input class="select_property_smoker" type="radio"
                                                                       name='smocker' value='1'>
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input class="select_property_smoker" type="radio"
                                                                       name='smocker' value='0' checked>
                                                                <label class='font-weight-bold'>No</label>
                                                            </div>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Guarantor</label>
                                                            <div class="check-outer">
                                                                <input class="select_property_guarantor" type="radio"
                                                                       name='guarantor' value='1'>
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input class="select_property_guarantor" type="radio"
                                                                       name='guarantor' value='0' checked>
                                                                <label class='font-weight-bold'>No</label>
                                                            </div>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>

                                                <div class="property_guarantor" style="display:none">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <input class="guarantor_entity" type="checkbox"
                                                                       name='guarantor_entity'><strong
                                                                        class='font-weight-bold'>Check this box if this
                                                                    Guarantor is entity/company</strong>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                            </div>

                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>


                                                    <div class="property_guarantor_form1">

                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Salutation</label>
                                                                    <select class="form-control"
                                                                            name="guarantor_salutation[]">
                                                                        <option value="Select">Select</option>
                                                                        <option value="Dr.">Dr.</option>
                                                                        <option value="Mr.">Mr.</option>
                                                                        <option value="Mrs.">Mrs.</option>
                                                                        <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                                        <option value="Ms.">Ms.</option>
                                                                        <option value="Sir">Sir</option>
                                                                        <option value="Madam">Madam</option>
                                                                        <option value="Brother">Brother</option>
                                                                        <option value="Sister">Sister</option>
                                                                        <option value="Father">Father</option>
                                                                        <option value="Mother">Mother</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>First Name</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_firstname[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Middle Name</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_middlename[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Last Name</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_lastname[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Relationship</label>
                                                                    <select class="form-control"
                                                                            name="guarantor_relationship[]">
                                                                        <option value="">Select</option>
                                                                        <option value="1">Brother</option>
                                                                        <option value="2">Daughter</option>
                                                                        <option value="3">Employer</option>
                                                                        <option value="4">Father</option>
                                                                        <option value="5">Friend</option>
                                                                        <option value="6">Mentor</option>
                                                                        <option value="7">Mother</option>
                                                                        <option value="8">Neighbor</option>
                                                                        <option value="9">Nephew</option>
                                                                        <option value="10">Niece</option>
                                                                        <option value="11">Owner</option>
                                                                        <option value="12">Partner</option>
                                                                        <option value="13">Sister</option>
                                                                        <option value="14">Son</option>
                                                                        <option value="15">Spouse</option>
                                                                        <option value="16">Teacher</option>
                                                                        <option value="17">Other</option>

                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Zip/Postal Code</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_zipcode[]" id="guarantor_zipcode" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-3 col-md-3 countycodediv">
                                                                    <label>Country</label>
                                                                    
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_country[]" id="guarantor_country">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>State/Province</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_province[]" id="guarantor_province">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>City</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_city[]" id="guarantor_city">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Address1</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_address1[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Address2</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_address2[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Address3</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_address3[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Address4</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           name="guarantor_address4[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>

                                                        <div class="row guarantor-form1-phone-row">
                                                            <input type='hidden' name='clone_guarantor_form1'
                                                                   class="clone_guarantor_form1">
                                                            <div class="form-outer">
                                                                <div class="col-sm-11 col-md-11">
                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Phone Type</label>
                                                                        <select class="form-control"
                                                                                name="guarantor_phoneType_1[]">
                                                                            <option value="">Select</option>
                                                                            <option value="1">mobile</option>
                                                                            <option value="2">work</option>
                                                                            <option value="3">Fax</option>
                                                                            <option value="4">Home</option>
                                                                            <option value="5">Other</option>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Carrier</label>
                                                                        <select class="form-control"
                                                                                name="guarantor_carrier_1[]">
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>
                                                                    <div class="col-sm-12 col-md-2 countycodediv">
                                                                        <label>Country Code</label>
                                                                        <select class="form-control" name="guarantor_countryCode_1[]">
                                                                            <option value="">Select</option>
                                                                        </select>
                                                                        <span class="term_planErr error red-star"></span>
                                                                    </div>

                                                                    <div class="col-sm-3 col-md-3">

                                                                        <label>Phone Number</label>
                                                                        <input class="form-control capsOn phone_format" type="text"
                                                                               id="guarantor_phone"
                                                                               name="guarantor_phone_1[]">

                                                                        <span class="ffirst_nameErr error red-star"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1 col-md-1">
                                                                    <a class="add-icon guarantor-phonerow-form1-plus-sign"
                                                                       href="javascript:;"><i class="fa fa-plus-circle"
                                                                                              aria-hidden="true"></i></a>

                                                                    <a class="add-icon guarantor-phonerow-form1-remove-sign"
                                                                       href="javascript:;" style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class='multipleEmail-form1'>
                                                                    <div class="col-sm-3 col-md-3">
                                                                        <label>Email</label>
                                                                        <input class="form-control capsOn" type="text"
                                                                               id="guarantor_email"
                                                                               name="guarantor_email_1[]">
                                                                        <a class="add-icon email-form1-remove-sign"
                                                                           href="javascript:;">
                                                                            <i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                                        <a class="add-icon email-form1-plus-sign" href="javascript:;">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        <span class="ffirst_nameErr error red-star"></span>


                                                                    </div>
                                                                </div>

                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>SSN/SIN/ID </label>
                                                                    <input class="form-control add-input capsOn"
                                                                           type="text" id="guarantor_ssn"
                                                                           name="guarantor_ssn[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                            <div class="col-sm-3 col-md-3">
                                                            <label>Year Of Guarantee</label>
                                                            <select class="form-control" id="guarantor_guarantee"
                                                            name="guarantor_guarantee[]">
                                                            <option value="0">Select</option>
                                                                <?php for($year = 1; $year <= 100; $year++){ ?>
                                                                    <option value="<?php echo $year ?>"><?php echo $year ?></option>
                                                                <?php } ?>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                            <span class="term_planErr error red-star"></span>
                                                            </div>

                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Note</label>
                                                                    <textarea class="form-control capsOn"
                                                                              id="guarantor_note"
                                                                              name="guarantor_note[]"></textarea>
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <a class="add-icon guarantor-plus-sign"
                                                                   href="javascript:;"><i class="fa fa-plus-circle"
                                                                                          aria-hidden="true"></i></a>

                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="property_guarantor_form2" style="display:none">
                                                    <input type='hidden' class='clone_guarantor_form2'>
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Name of Entity/Company</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_entity"
                                                                       name="guarantor_form2_entity[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Zip/Postal Code</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_postalcode"
                                                                       name="guarantor_form2_postalcode[]" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3 countycodediv">
                                                                <label>Country</label>
                                                                <select class="form-control" name="guarantor_form2_country[]">
                                                                    <option value="">Select</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>State/province</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_province"
                                                                       name="guarantor_form2_province[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>City</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_city"
                                                                       name="guarantor_form2_city[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Address1</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_address1"
                                                                       name="guarantor_form2_address1[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Address2</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_address2"
                                                                       name="guarantor_form2_address2[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Address3</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_address3"
                                                                       name="guarantor_form2_address3[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Address4</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_address4"
                                                                       name="guarantor_form2_address4[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <!--<div class="col-sm-3 col-md-3">
                                                                <label>Phone Type <em class="red-star">*</em></label>
                                                                <select class="form-control"
                                                                        id="guarantor_form2_phoneType"
                                                                        name="guarantor_form2_phoneType_1[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1">mobile</option>
                                                                    <option value="2">work</option>
                                                                    <option value="3">Fax</option>
                                                                    <option value="4">Home</option>
                                                                    <option value="5">Other</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Carrier <em class="red-star">*</em></label>
                                                                <select class="form-control"
                                                                        id="guarantor_form2_carrier"
                                                                        name="guarantor_form2_carrier_1[]">
                                                                    <option value="">Select</option>
                                                                    <option value="Airtel">Airtel</option>
                                                                    <option value="Aircel">Aircel</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3 countycodediv">
                                                                <label>Country Code <em class="red-star">*</em></label>
                                                                <select class="form-control" name="guarantor_form2_countrycode_1[]">
                                                                    <option value="">Select</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Phone Number</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_phone"
                                                                       name="guarantor_form2_phone_1[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>-->
                                                            <a class="add-icon" href="javascript:;"
                                                               style="display:none"><i class="fa fa-plus-circle"
                                                                                       aria-hidden="true"></i></a>

                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-outer">

                                                            <div class="multipleEmail-form2">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Email</label>

                                                                    <input class="form-control capsOn" type="text"
                                                                           id="guarantor_form2_email[]"
                                                                           name="guarantor_form2_email_1[]">
                                                                    <a class="add-icon email-form2-remove-sign"
                                                                       href="javascript:;"><i class="fa fa-minus-circle"
                                                                                              aria-hidden="true"></i></a>

                                                                    <a class="add-icon email-form2-plus-sign"
                                                                       href="javascript:;"><i class="fa fa-plus-circle"
                                                                                              aria-hidden="true"></i></a>

                                                                </div>
                                                            </div>


                                                            <div class="col-sm-3 col-md-3">
                                                                <label>RelationShip</label>
                                                                <select class="form-control"
                                                                        id="guarantor_form2_relationship"
                                                                        name="guarantor_form2_relationship[]">
                                                                    <option value="">Select</option>
                                                                    <option value="1">Brother</option>
                                                                    <option value="2">Daughter</option>
                                                                    <option value="3">Employer</option>
                                                                    <option value="4">Father</option>
                                                                    <option value="5">Friend</option>
                                                                    <option value="6">Mentor</option>
                                                                    <option value="7">Mother</option>
                                                                    <option value="8">Neighbor</option>
                                                                    <option value="9">Nephew</option>
                                                                    <option value="10">Niece</option>
                                                                    <option value="11">Owner</option>
                                                                    <option value="12">Partner</option>
                                                                    <option value="13">Sister</option>
                                                                    <option value="14">Son</option>
                                                                    <option value="15">Spouse</option>
                                                                    <option value="16">Teacher</option>
                                                                    <option value="17">Other</option>

                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Entity FID/ID Number</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_fid"
                                                                       name="guarantor_form2_fid[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Main Contact Person's First Name</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_mainContact"
                                                                       name="guarantor_form2_mainContact[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Middle Name</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_middlename"
                                                                       name="guarantor_form2_middlename[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Last Name</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_lastname"
                                                                       name="guarantor_form2_lastname[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Email</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="guarantor_form2_email2"
                                                                       name="guarantor_form2_email2[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>

                                                    <div class="row guarantor-form2-phone-row">
                                                        <div class="row">
                                                            <div class="form-outer">
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Phone Type</label>
                                                                    <select class="form-control"
                                                                            id="guarantor_form2_phoneType"
                                                                            name="guarantor_form2_phoneType_1[]">
                                                                        <option value="">Select</option>
                                                                        <option value="1">mobile</option>
                                                                        <option value="2">work</option>
                                                                        <option value="3">Fax</option>
                                                                        <option value="4">Home</option>
                                                                        <option value="5">Other</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Carrier</label>
                                                                    <select class="form-control"
                                                                            id="guarantor_form2_carrier"
                                                                            name="guarantor_form2_carrier_1[]">
                                                                        <option value="">Select</option>
                                                                        <option value="1">Airtel</option>
                                                                        <option value="2">Aircel</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>
                                                                <div class="col-sm-3 col-md-3 countycodediv">
                                                                    <label>Country Code</label>
                                                                    <select class="form-control" name="guarantor_form2_countrycode_1[]">
                                                                        <option value="">Select</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Phone Number</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           id="guarantor_form2_phone"
                                                                           name="guarantor_form2_phone_1[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                </div>
                                                                <!--<div class="col-sm-3 col-md-3 ">
                                                                    <label>Phone Type <em
                                                                                class="red-star">*</em></label>
                                                                    <select class="form-control"
                                                                            id="guarantor_form2_phoneType"
                                                                            name="guarantor_form2_phoneType[]">
                                                                        <option value="">Select</option>
                                                                        <option value="1">mobile</option>
                                                                        <option value="2">work</option>
                                                                        <option value="3">Fax</option>
                                                                        <option value="4">Home</option>
                                                                        <option value="5">Other</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-3 col-md-3 ">
                                                                    <label>Carrier <em class="red-star">*</em></label>
                                                                    <select class="form-control"
                                                                            id="guarantor_form2_carrier"
                                                                            name="guarantor_form2_carrier[]">
                                                                        <option value="">Select</option>
                                                                        <option value="1">Airtel</option>
                                                                        <option value="2">Aircel</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-3 col-md-3 countycodediv">
                                                                    <label>Country Code <em class="red-star">*</em></label>
                                                                    <select class="form-control" name="guarantor_form2_countryCode[]">
                                                                        <option value="">Select</option>
                                                                    </select>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Phone Number</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           id="guarantor_form2_phone"
                                                                           name="guarantor_form2_phone_1[]">
                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                    <span class="term_planErr error red-star"></span>
                                                                </div>-->
                                                                <a class="add-icon guarantor-phonerow-form2-plus-sign"
                                                                   href="javascript:;"><i class="fa fa-plus-circle"
                                                                                          aria-hidden="true"></i></a>
                                                                <a class="add-icon guarantor-phonerow-form2-remove-sign"
                                                                   href="javascript:;" style="display:none"><i
                                                                            class="fa fa-plus-circle"
                                                                            aria-hidden="true"></i>minus</a>

                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-3 col-md-3 ">
                                                                <label>Year Of Guarantee<em
                                                                            class="red-star">*</em></label>
                                                                <select class="form-control"
                                                                        id="guarantor_form2_guarantee"
                                                                        name="guarantor_form2_guarantee[]">
                                                                    <option value="0">Select</option>
                                                                    <?php for($year = 1; $year <= 100; $year++){ ?>
                                                                        <option value="<?php echo $year ?>"><?php echo $year ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Note</label>
                                                                <textarea class="form-control capsOn" type="text"
                                                                          id="guarantor_form2_note"
                                                                          name="guarantor_form2_note[]"></textarea>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-3 col-md-3">
                                                                <label>Files</label>
                                                                <input class="form-control" type="file"
                                                                       id="guarantor_form2_files"
                                                                       name="guarantor_form2_files[]">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <a class="add-icon guarantor-form2-plus-sign"
                                                               href="javascript:;"><i class="fa fa-plus-circle"
                                                                                      aria-hidden="true">+</i></a>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Collection</label>
                                                            <div class="check-outer">
                                                                <input class="select_property_collection" type="radio"
                                                                       name='select_property_collection' value='1'>
                                                                <label>Yes</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input class="select_property_collection" type="radio"
                                                                       name='select_property_collection' value='0'
                                                                       checked>
                                                                <label class='font-weight-bold'>No</label>
                                                            </div>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>

                                                <div class="property_collection" style="display:none;">
                                                    <div class="form-outer">
                                                        <div class="col-sm-12 col-md-12">
                                                            <div class="col-sm-2 col-md-2">
                                                                <label>Collection ID </label>
                                                                <input class="form-control capsOn capital" type="text"
                                                                       id="collection_collectionId"
                                                                       name="collection_collectionId">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-2 col-md-2">
                                                                <label>Reason
                                                                    <a class="pop-add-icon collectionreason" href="javascript:;">
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </a>
                                                                </label>
                                                                <select class="form-control" id="guarantor_reason" name="collection_reason"></select>
                                                                <div class="add-popup" id="collectionreason1">
                                                                    <h4>Add New Reason</h4>
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Add New Reason <em class="red-star">*</em></label>
                                                                                <input class="form-control reason_source" type="text" placeholder="Add New Reason">
                                                                                <span class="red-star" id="reason_source"></span>
                                                                            </div>
                                                                            <div class="btn-outer">
                                                                                <button type="button" class="blue-btn add_single1" data-table="tenant_collection_reason" data-cell="reason" data-class="reason_source" data-name="collection_reason">Save</button>
                                                                                <input type="button" class="grey-btn" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-2 col-md-2">
                                                                <label>Description</label>
                                                                <textarea class="form-control capsOn"
                                                                          id="collection_description"
                                                                          name="collection_description"></textarea>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-2 col-md-2">
                                                                <label>Status</label>
                                                                <select class="form-control" id="collection_status"
                                                                        name="collection_status">
                                                                    <option value="">Select</option>
                                                                    <option value="1">Open</option>
                                                                    <option value="2">Close</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-2 col-md-2">
                                                                <?php $collectioncurr = $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>
                                                                <label>Amount Due (<?php echo $collectioncurr; ?>)</label>
                                                                <input class="form-control capsOn" type="text"
                                                                       id="collection_amountDue"
                                                                       name="collection_amountDue">
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>

                                                            <div class="col-sm-2 col-md-2">
                                                                <label>Note</label>
                                                                <textarea class="form-control capsOn" type="text"
                                                                          id="collection_note"
                                                                          name="collection_note"></textarea>
                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                <span class="term_planErr error red-star"></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>
                                                <strong class="left">Emergency Contact Details</strong>
                                            </h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="tenant-emergency-contact">
                                                <div class="row">
                                                    <div class="form-outer">

                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Emergency Contact Name</label>
                                                            <input class="form-control capsOn capital" type="text" id="emergency"
                                                                   name="emergency_contact_name[]" maxlength="50">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Relationship</label>
                                                            <select class="form-control" id="relationship"
                                                                    name="emergency_relation[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Brother</option>
                                                                <option value="2">Daughter</option>
                                                                <option value="3">Employer</option>
                                                                <option value="4">Father</option>
                                                                <option value="5">Friend</option>
                                                                <option value="6">Mentor</option>
                                                                <option value="7">Mother</option>
                                                                <option value="8">Neighbor</option>
                                                                <option value="9">Nephew</option>
                                                                <option value="10">Niece</option>
                                                                <option value="11">Owner</option>
                                                                <option value="12">Partner</option>
                                                                <option value="13">Sister</option>
                                                                <option value="14">Son</option>
                                                                <option value="15">Spouse</option>
                                                                <option value="16">Teacher</option>
                                                                <option value="17">Other</option>

                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-12 col-md-2 countycodediv">
                                                            <label>Country Code</label>
                                                            <select name="emergency_country[]" class="form-control emergencycountry">
                                                                <option value="0">Select</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">

                                                            <label>Phone</label>
                                                            <input class="form-control capsOn" type="text" id="phoneNumber"
                                                                   name="emergency_phone[]" maxlength="12">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="clearfix"></div>

                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Email</label>
                                                            <input class="form-control capsOn add-input" type="text" id="email1"
                                                                   name="emergency_email[]" maxlength="50">
                                                            <a class="add-icon add-emergency-contant" href="javascript:;">
                                                                <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            <a class="add-icon remove-emergency-contant" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Tenant Credential Control</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="tenant-credentials-control">
                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="credential-clone">
                                                        
                                                           
                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Credential Name</label>
                                                                    <input class="form-control capsOn" type="text"
                                                                           id="credentialName" name="credentialName[]">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-3 col-md-3 apx-inline-popup credintials">
                                                                    <label>Credential Type
                                                                        <a class="pop-add-icon tenantCredentialType" href="javascript:;">
                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                        </a>
                                                                    </label>
                                                                    <select class="form-control" name="credentialType[]">
                                                                        <option value="">Select</option>
                                                                        <option value="1">Bond</option>
                                                                        <option value="2">Certification</option>
                                                                    </select>
                                                                    <div class="add-popup" id="tenantCredentialType1">
                                                                        <h4>Add Credential Type</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Credential Type <em class="red-star">*</em></label>
                                                                                    <input class="form-control credential_source" type="text" placeholder="Ex: License">
                                                                                    <span class="red-star" id="credential_source"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button type="button" class="blue-btn add_single1" data-table="tenant_credential_type" data-cell="credential_type" data-class="credential_source" data-name="credentialType[]">Save</button>
                                                                                    <input type="button" class="grey-btn" value="Cancel">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Acquire Date</label>
                                                                    <input class="form-control capsOn calander" type="text"
                                                                           id="acquireDate" name="acquireDate[]">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>

                                                                <div class="col-sm-3 col-md-3">
                                                                    <label>Expiration Date</label>
                                                                    <input class="form-control capsOn calander" type="text"
                                                                           id="expirationDate" name="expirationDate[]">
                                                                    <span class="flast_nameErr error red-star"></span>
                                                                </div>
                                                            
                                                    
                                                        <div class='notice-period'>
                                                            <div class="col-sm-3 col-md-3 ">
                                                                <label>Notice Period</label>
                                                                <select class="form-control add-input" id="term_plan"
                                                                        name="noticePeriod">
                                                                    <option value="">Select</option>
                                                                    <option value="1">5 day</option>
                                                                    <option value="2">1 Month</option>
                                                                    <option value="3">2 Month</option>
                                                                    <option value="4">3 Month</option>
                                                                </select>
                                                                <span class="term_planErr error red-star"></span>
                                                                <a class="add-icon add-notice-period" href="javascript:;"><i
                                                                            class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                <a class="add-icon remove-notice-period" href="javascript:;" style="display: none;">
                                                                    <i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i>
                                                                </a>
                                                            </div>
                                                        </div>                                                        
                                                   
                                                        
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Custom Fields</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row ">

                                                <div class="col-sm-3 col-md-3 ">
                                                    <label>PO<em class="red-star ">*</em></label>
                                                    <input class="form-control capsOn " type="text " id="flast_name "
                                                           name="flast_name ">
                                                    <span class="flast_nameErr error red-star "></span>
                                                </div>

                                                <div class="clearfix "></div>


                                            </div>

                                            <div class="row ">
                                                <div class="form-outer ">
                                                    <div class="custom_field_html">
                                                    </div>
                                                    <div class="clearfix "></div>
                                                </div>

                                            </div>

                                            <button type="button" id="add_custom_field" data-toggle="modal"
                                                    data-backdrop="static" data-target="#myModal" class="blue-btn">Add
                                                Custom Field
                                            </button>

                                        </div>
                                    </div>
                                   </form>

                                   <form id="addAdditionalTenantForm">


                                    <div class="check-outer">
                                        <input type="hidden" name="additionalTenantKey" class="additionalTenantKey">
                                        <input type="checkbox" name="addAdditionalTenant" class="addAdditionalTenant">
                                        <label> Add Additional Tenant</label>
                                    </div>

                                    <div class="additionalTenantHtml" style="display:none;">
                                        <div class="form-outer">

                                            <div class="form-hdr">
                                                <h3>Additional Tenant</h3>
                                            </div>
                                            <div class="form-data apx-adformbox-content addition_tenant_block">
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Salutation</label>
                                                            <select class="form-control" name="additional_salutation" id="additional_salutation">
                                                                <option value="Select">Select</option>
                                                                <option value="Dr.">Dr.</option>
                                                                <option value="Mr.">Mr.</option>
                                                                <option value="Mrs.">Mrs.</option>
                                                                <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                                <option value="Ms.">Ms.</option>
                                                                <option value="Sir">Sir</option>
                                                                <option value="Madam">Madam</option>
                                                                <option value="Brother">Brother</option>
                                                                <option value="Sister">Sister</option>
                                                                <option value="Father">Father</option>
                                                                <option value="Mother">Mother</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>First Name <em class="red-star">*</em></label>
                                                            <input class="form-control capsOn" type="text"
                                                                   name="additional_firstname">
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Middle Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   name="additional_middlename">
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Last Name <em class="red-star">*</em></label>
                                                            <input class="form-control capsOn" type="text"
                                                                   name="additional_lastname">
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3 maiden_name_add_hide" style="display: none;">

                                                            <label>Maiden Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="maidenname" name="maidenname" maxlength="50">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3">

                                                            <label>Nick Name</label>
                                                            <input class="form-control capsOn" type="text" id="nickname"
                                                                   name="additional_nickname">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Gender</label>
                                                            <select class="form-control" id="additional_gender" class="gender"
                                                                    name="additional_gender">
                                                                <option value="">Select</option>
                                                                <option value="1">Male</option>
                                                                <option value="2">Female</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>

                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>SSN/SIN/ID</label>
                                                            <div class='additional_multipleSsn'>
                                                                <input class="form-control add-input capsOn" type="text"
                                                                       name="additional_ssn">

                                                           

                                                                <span class="ffirst_nameErr error red-star"></span>
                                                                 <a class="add-icon additional_ssn-plus-sign"
                                                               href="javascript:;"><i class="fa fa-plus-circle"
                                                                                      aria-hidden="true"></i></a>

                                                     <a class="add-icon additional_ssn-remove-sign"
                                                            href="javascript:;" style="display: none;"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                            </div>
                                                           
                                                        </div>








                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Relationship</label>
                                                            <select class="form-control" name="additional_relationship">
                                                                <option value="">Select</option>
                                                                <option value="1">Daughter</option>
                                                                <option value="2">Father</option>
                                                                <option value="3">Friend</option>
                                                                <option value="4">Mother</option>
                                                                <option value="5">Owner</option>
                                                                <option value="6">Partner</option>
                                                                <option value="7">Son</option>
                                                                <option value="8">Spouse</option>
                                                                <option value="9">Other</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                            <label>Ethnicity
                                                                <a class="pop-add-icon additionalEthnicity" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a>
                                                            </label>
                                                            <select class="form-control" name="additional_ethncity"></select>
                                                            <div class="add-popup" id="additionalEthnicity1">
                                                                <h4>Add New Ethnicity</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New Ethnicity <em class="red-star">*</em></label>
                                                                            <input class="form-control ethnicity_src1" type="text" placeholder="Add New Ethnicity">
                                                                            <span class="red-star" id="ethnicity_src1"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_ethnicity" data-cell="title" data-class="ethnicity_src1" data-name="additional_ethncity">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                            <label>Marital Status
                                                                <a class="pop-add-icon additionalMaritalStatus" href="javascript:;">
                                                                    <i class="fa fa-plus-circle"
                                                                            aria-hidden="true"></i></a></label>
                                                            <select class="form-control additional_maritalStatus" name="additional_maritalStatus"></select>
                                                            <div class="add-popup" id="additionalMaritalStatus1">
                                                                <h4>Add New Marital Status</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New Marital Status<em class="red-star">*</em></label>
                                                                            <input class="form-control maritalstatus_src1" type="text" placeholder="Add New Marital Status">
                                                                            <span class="red-star" id="maritalstatus_src1"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_marital_status" data-cell="marital" data-class="maritalstatus_src1" data-name="additional_maritalStatus">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                            <label>Hobbies
                                                                <a class="pop-add-icon additionalHobbies" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a>
                                                            </label>
                                                            <select class="form-control" name="additional_hobbies[]"
                                                                    multiple></select>
                                                            <div class="add-popup" id="additionalHobbies1">
                                                                <h4>Add New Hobbies</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New Hobbies <em class="red-star">*</em></label>
                                                                            <input class="form-control hobbies_src1" type="text" placeholder="Add New Hobbies">
                                                                            <span class="red-star" id="hobbies_src1"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="hobbies" data-cell="hobby" data-class="hobbies_src1" data-name="additional_hobbies[]">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                            <label>Veteran status
                                                                <a class="pop-add-icon additionalVeteranStatus" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a>
                                                            </label>
                                                            <select class="form-control additional_veteranStatus" name="additional_veteranStatus"></select>
                                                            <div class="add-popup" id="additionalVeteranStatus1">
                                                                <h4>Add New VeteranStatus</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New VeteranStatus <em class="red-star">*</em></label>
                                                                            <input class="form-control veteran_src1" type="text" placeholder="Add New VeteranStatus">
                                                                            <span class="red-star" id="veteran_src1"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_veteran_status" data-cell="veteran" data-class="veteran_src1" data-name="additional_veteranStatus">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row additional_phone-row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Phone Type</label>
                                                            <select class="form-control additional_phoneType"
                                                                    name="additional_phoneType[]">
                                                                <option value="">Select</option>
                                                                <option value="1">mobile</option>
                                                                <option value="2">work</option>
                                                                <option value="3">Fax</option>
                                                                <option value="4">Home</option>
                                                                <option value="5">Other</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Carrier <em class="red-star">*</em></label>
                                                            <select class="form-control additional_carrier"
                                                                    name="additional_carrier[]"></select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-12 col-md-2 countycodediv">
                                                            <label>Country Code</label>
                                                            <select name="additional_countryCode[]" class="form-control ">
                                                                <option value="0"></option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3">

                                                            <label>Phone Number</label>
                                                            <input class="form-control phone_format add-input capsOn additional_phone"
                                                                   type="text" name="additional_phone[]">
                                                            <a class="add-icon additional-phonerow-plus-sign"
                                                               href="javascript:;"><i class="fa fa-plus-circle"
                                                                                      aria-hidden="true"></i></a>
                                                            <a class="add-icon additional-phonerow-remove-sign" href="javascript:;"><i  style="display:none" class="fa fa-minus-circle" aria-hidden="true"></i></a>

                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Email</label>



                                                            <div class='additional_multipleEmail'>
                                                                <input class="form-control add-input capsOn" type="text"
                                                                       name="additional_email[]" style="margin: 5px 0px;">
                                                                    <a class="add-icon additional_email-plus-sign"
                                                               href="javascript:;"><i class="fa fa-plus-circle"
                                                                                      aria-hidden="true"></i></a>


                                                                                       <a class="add-icon additional_email-remove-sign" href="javascript:;" style="display: none;">
                                                          <i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i>
                                                        </a>
                                                            </div>
                                                           

                                                        </div>




                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-outer">

                                                        <div class="col-sm-3 col-md-3 apx-inline-popup">
                                                            <label>Referral Source
                                                                <a class="pop-add-icon additionalReferralResource" href="javascript:;">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                </a>
                                                            </label>
                                                            <select class="form-control" name="additional_referralSource">
                                                                <option value="">Select</option>
                                                                <option value="1">Bond</option>
                                                                <option value="2">Certification</option>
                                                            </select>
                                                            <div class="add-popup" id="additionalReferralResource1">
                                                                <h4>Add New Referral Source</h4>
                                                                <div class="add-popup-body">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-12">
                                                                            <label>New Referral Source <em class="red-star">*</em></label>
                                                                            <input class="form-control reff_source1" type="text" placeholder="New Referral Source">
                                                                            <span class="red-star" id="reff_source1"></span>
                                                                        </div>
                                                                        <div class="btn-outer">
                                                                            <button type="button" class="blue-btn add_single1" data-table="tenant_referral_source" data-cell="referral" data-class="reff_source1" data-name="additional_referralSource">Save</button>
                                                                            <input type="button" class="grey-btn" value="Cancel">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Tenant status</label>
                                                            <select name="additional_tenantStatus" class="form-control">
                                                                <option value="0">Select</option>
                                                                <option value="1">Active</option>
                                                                <option value="0">Inactive</option>
                                                            </select>
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>&nbsp;</label>

                                                            <div class="check-outer">
                                                                <input class=" capsOn" type="checkbox"
                                                                       name="additional_financallyResponsible">
                                                                <label>Financally Responsible?</label>
                                                                <span class="flast_nameErr error red-star"></span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                        <!--additional form starts form here -->


                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3>
                                                    Emergency Contact Details
                                                </h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                    <div class="form-outer additional-tenant-emergency-contact">

                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Emergency Contact Name</label>
                                                            <input class="form-control capsOn" type="text"
                                                                   id="emergency" name="additional_emergency[]">
                                                            <span class="flast_nameErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>Relationship</label>
                                                            <select class="form-control" id="relationship"
                                                                    name="emergency_additional_relationship[]">
                                                                <option value="">Select</option>
                                                                <option value="1">Daughter</option>
                                                                <option value="2">Father</option>
                                                                <option value="3">Friend</option>
                                                                <option value="4">Mother</option>
                                                                <option value="5">Owner</option>
                                                                <option value="6">Partner</option>
                                                                <option value="7">Son</option>
                                                                <option value="8">Spouse</option>
                                                                <option value="9">Other</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 countycodediv">
                                                            <label>Country Code</label>
                                                            <select class="form-control" name="emergency_additional_countryCode[]">
                                                                <option value="">Select</option>
                                                            </select>
                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="col-sm-3 col-md-3 ">

                                                            <label>Phone</label>
                                                            <input class="form-control capsOn phone_format" type="text"
                                                                   id="phoneNumber"
                                                                   name="emergency_additional_phoneNumber[]">
                                                            <span class="ffirst_nameErr error red-star"></span>

                                                            <span class="term_planErr error red-star"></span>
                                                        </div>

                                                        <div class="clearfix"></div>

                                                    <div class="form-outer">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Email</label>
                                                            <input class="form-control add-input capsOn" type="text"
                                                                   name="additional_emergency_email[]">
                                                            <span class="flast_nameErr error red-star"></span>
                                                              <a class="add-icon additional-add-emergency-contant" href="javascript:;">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                        <a class="add-icon additional-remove-emergency-contant" style="display:none;"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>



                                                    </div>
                                                    <div class="col-sm-12"><input type="submit" class="blue-btn saveAdditionalTenant" value="Add Additional tenant">
                                                    </div>

                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    </form>

                                    <div class="btn-outer apex-btn-block left">
                                        <!--<a class="blue-btn savetenantnext" value="">Next</a>-->
                                        <input type="submit" class="blue-btn savetenant" value="Next">
                                        <input type="submit" class="blue-btn savetenant" value="Save">
                                        <button type="button" onclick="goBack()" id="add_company_button" class="grey-btn">Cancel</button>
                                    </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
<!-- Wrapper Ends -->

<div class="container">
    <div class="modal fade" id="financial-info" role="dialog">
        <div class="modal-dialog modal-md" style="width: 60%;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <a class="close closeAnnouncement pop-close" data-dismiss="modal" href="javascript:;"> <i
                                class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>

                    <h4 class="modal-title">Online Payment Details</h4>
                </div>
                <div class="modal-body">
                    <div class="apx-adformbox-content">
                        <form method="post" id="financialInfo">
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>First Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="ffirst_name"
                                                name="ffirst_name">
                                        <span class="ffirst_nameErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Middle Name</label>
                                        <input class="form-control capsOn" type="text" id="fmiddle_name"
                                               name="fmiddle_name">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Last Name <em class="red-star">*</em></label>
                                        <input class="form-control capsOn" type="text" id="flast_name"
                                               name="flast_name">
                                        <span class="flast_nameErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Email <em class="red-star">*</em></label>
                                        <input class="form-control add-input" type="email" id="femail" name="femail">
                                        <span class="femailErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4 fphone_num">
                                        <label>Phone <em class="red-star">*</em></label>
                                        <input type="tel" maxlength="12" placeholder="000-000-0000" class="form-control"
                                               fixedLength="10" id="fphone_number" name="fphone_number" value="">
                                        <span class="fphone_numberErr error red-star"></span>
                                        <input type="hidden" value="" id="phone_number_prefix"
                                               name="phone_number_prefix">
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Birth Date<em class="red-star">*</em></label>
                                        <input class="form-control capsOn calander" type="text" id="fbirth_date"
                                               name="fbirth_date">
                                        <span class="fbirth_dateErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Postal Code <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fzipcode" name="fzipcode" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>"
                                               maxlength="9">
                                        <span class="fzipcodeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>City <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcity" name="fcity" value=""
                                               maxlength="9">
                                        <span class="fcityErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>State <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fstate" name="fstate" value=""
                                               maxlength="9">
                                        <span class="fstateErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Address <em class="red-star">*</em></label>
                                        <textarea class="form-control" id="faddress" name="faddress"></textarea>
                                        <span class="faddressErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 10px;">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Business Type<em class="red-star">*</em></label>
                                        <select class="form-control" id="fbusiness" name="fbusiness" >
                                            <option value="individual" selected>Individual</option>
                                        </select>
                                        <span class="fbusinessErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Social Security Number(Last 4 Digits) <em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fssn" name="fssn" value=""
                                               maxlength="4">
                                        <span class="fssnErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Account Holder Name<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="faccount_holder"
                                               name="faccount_holder" value="">
                                        <span class="faccount_holderErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Account Holder Type<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="faccount_holdertype"
                                               name="faccount_holdertype" value="individual" maxlength="9" >
                                        <span class="faccount_holdertypeErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Account Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="faccount_number"
                                               name="faccount_number" value="">
                                        <span class="faccount_numberErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-outer">
                                    <div class="col-sm-4 col-md-4">
                                        <label>ISO Country Code<em class="red-star">*</em></label>

                                        <input type="text" class="form-control" id="fiso" name="fiso" value="US"
                                               readonly>
                                        <span class="fisoErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Currency<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="fcurrency" name="fcurrency"
                                               value="USD" readonly>
                                        <span class="fcurrencyErr error red-star"></span>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Routing Number<em class="red-star">*</em></label>
                                        <input type="text" class="form-control" id="frouting_number"
                                               name="frouting_number" value="USD" readonly>
                                        <span class="frouting_numberErr error red-star"></span>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" id="savefinancial" class="blue-btn">Submit</button>
                            <a href="javascript:void(0)" class="grey-btn" id="closePop">Cancel</a>


                            <div class="property_guarantor_forcopy_form1">
                                <input type='hidden' name='clone_guarantor_form1' class="clone_guarantor_form1">
                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3">
                                            <label>Salutation</label>
                                            <select class="form-control" id="guarantor_salutation"
                                                    name="guarantor_salutation[]">
                                                <option value="Select">Select</option>
                                                <option value="Dr.">Dr.</option>
                                                <option value="Mr.">Mr.</option>
                                                <option value="Mrs.">Mrs.</option>
                                                <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                <option value="Ms.">Ms.</option>
                                                <option value="Sir">Sir</option>
                                                <option value="Madam">Madam</option>
                                                <option value="Brother">Brother</option>
                                                <option value="Sister">Sister</option>
                                                <option value="Father">Father</option>
                                                <option value="Mother">Mother</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>First Name</label>
                                            <input class="form-control capsOn capital" type="text" id="guarantor_firstname"
                                                   name="guarantor_firstname[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Middle Name</label>
                                            <input class="form-control capsOn capital" type="text" id="guarantor_middlename"
                                                   name="guarantor_middlename[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Last Name</label>
                                            <input class="form-control capsOn capital" type="text" id="guarantor_lastname"
                                                   name="guarantor_lastname[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3">
                                            <label>Relationship</label>
                                            <select class="form-control" id="guarantor_relationship"
                                                    name="guarantor_relationship[]">
                                                <option value="">Select</option>
                                                <option value="1">Daughter</option>
                                                <option value="2">Father</option>
                                                <option value="3">Friend</option>
                                                <option value="4">Mother</option>
                                                <option value="5">Owner</option>
                                                <option value="6">Partner</option>
                                                <option value="7">Son</option>
                                                <option value="8">Spouse</option>
                                                <option value="9">Other</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Zip/Postal Code</label>
                                            <input class="form-control capsOn" type="text" id="guarantor_zipcode"
                                                   name="guarantor_zipcode[]" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3 countycodediv">
                                            <label>Country</label>
                                            <select class="form-control" name="guarantor_country[]">
                                                <option value="">Select</option>
                                            </select>
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>State/Province</label>
                                            <input class="form-control capsOn" type="text" id="guarantor_province"
                                                   name="guarantor_province[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3">
                                            <label>City</label>
                                            <input class="form-control capsOn" type="text" id="guarantor_city"
                                                   name="guarantor_city[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Address1></label>
                                            <input class="form-control capsOn capital" type="text" id="guarantor_address1"
                                                   name="guarantor_address1[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Address2</label>
                                            <input class="form-control capsOn capital" type="text" id="guarantor_address2"
                                                   name="guarantor_address2[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Address3</label>
                                            <input class="form-control capsOn capital" type="text" id="guarantor_address3"
                                                   name="guarantor_address3[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Address4</label>
                                            <input class="form-control capsOn capital" type="text" id="guarantor_address4"
                                                   name="guarantor_address4[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row guarantor-form1-phone-row">
                                    <div class="form-outer">
                                        <div class="cols-sm-11 col-md-11">
                                            <div class="col-sm-3 col-md-3">
                                                <label>Phone Type</label>
                                                <select class="form-control guarantor_phoneType" name="guarantor_phoneType">
                                                    <option value="">Select</option>
                                                    <option value="1">mobile</option>
                                                    <option value="2">work</option>
                                                    <option value="3">Fax</option>
                                                    <option value="4">Home</option>
                                                    <option value="5">Other</option>
                                                </select>
                                                <span class="term_planErr error red-star"></span>
                                            </div>

                                            <div class="col-sm-3 col-md-3">
                                                <label>Carrier</label>
                                                <select class="form-control guarantor_carrier" name="guarantor_carrier">
                                                    <option value="">Select</option>
                                                    <option value="1">Airtel</option>
                                                    <option value="2">Aircel</option>
                                                </select>
                                                <span class="term_planErr error red-star"></span>
                                            </div>

                                            <div class="col-sm-3 col-md-3 countycodediv">
                                                <label>Country Code</label>
                                                <select class="form-control guarantor_countryCode" name="guarantor_countryCode[]">
                                                    <option value="">Select</option>
                                                </select>
                                                <span class="term_planErr error red-star"></span>
                                            </div>

                                            <div class="col-sm-3 col-md-3">

                                                <label>Phone Number</label>
                                                <input class="form-control add-input capsOn guarantor_phone" type="text"
                                                       name="guarantor_phone[]" maxlength="12">
                                                <a class="add-icon guarantor-phonerow-form1-plus-sign"
                                                   href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                <a class="add-icon guarantor-phonerow-form1-remove-sign" href="javascript:;"
                                                   style="display:none"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>

                                                <span class="ffirst_nameErr error red-star"></span>

                                                <span class="term_planErr error red-star"></span>
                                            </div>
                                        </div>
                                        <div class="cols-sm-1 col-md-1">

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-outer">
                                        <div class='multipleEmail-form1'>
                                            <div class="col-sm-3 col-md-3">
                                                <label>Email</label>
                                                <input class="form-control capsOn guarantor_email" type="text"
                                                       id="guarantor_email" name="guarantor_email">
                                                <a class="add-icon email-form1-remove-sign" href="javascript:;"><i
                                                            class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                <span class="ffirst_nameErr error red-star"></span>

                                                <a class="add-icon email-form1-plus-sign" href="javascript:;"><i
                                                            class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                            </div>
                                        </div>


                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3">
                                            <label>SSN/SIN/ID </label>
                                            <input class="form-control add-input capsOn" type="text" id="guarantor_ssn"
                                                   name="guarantor_ssn[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3">
                                            <label>Years of Guarantee </label>
                                            <input class="form-control capsOn" type="text" id="guarantor_guarantee"
                                                   name="guarantor_guarantee[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3">
                                            <label>Note</label>
                                            <textarea class="form-control capsOn" id="guarantor_note"
                                                      name="guarantor_note[]"></textarea>
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <a class="add-icon guarantor-remove-sign" href="javascript:;"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="property_guarantor_forcopy_form2">
                                <input type='hidden' class='clone_guarantor_form2'>
                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3">
                                            <label>Name of Entity/Company</label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_entity"
                                                   name="guarantor_form2_entity[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Zip/Postal Code</label>
                                            <input class="form-control capsOn" type="text"
                                                   id="guarantor_form2_postalcode" name="guarantor_form2_postalcode[]" value="<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3 countycodediv">
                                            <label>Country</label>
                                            <select class="form-control" name="guarantor_form2_country[]">
                                                <option value="">Select</option>
                                            </select>
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>State/province</label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_province"
                                                   name="guarantor_form2_province[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3">
                                            <label>City <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_city"
                                                   name="guarantor_form2_city[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Address1 <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_address1"
                                                   name="guarantor_form2_address1[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Address2 <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_address2"
                                                   name="guarantor_form2_address2[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Address3 <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_address3"
                                                   name="guarantor_form2_address3[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Address4 <em class="red-star">*</em></label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_address4"
                                                   name="guarantor_form2_address4[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <a class="add-icon" href="javascript:;" style="display:none"><i
                                                    class="fa fa-plus-circle" aria-hidden="true"></i></a>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-outer">
                                        <div class="multiple-email-form2">
                                            <div class="col-sm-3 col-md-3">
                                                <label>Email</label>
                                                <input class="form-control capsOn guarantor_form2_email" type="text"
                                                       id="guarantor_form2_email">
                                                <span class="ffirst_nameErr error red-star"></span>
                                                <span class="term_planErr error red-star"></span>
                                            </div>
                                        </div>


                                        <div class="col-sm-3 col-md-3">
                                            <label>RelationShip <em class="red-star">*</em></label>
                                            <select class="form-control" id="guarantor_form2_relationship"
                                                    name="guarantor_form2_relationship[]">
                                                <option value="">Select</option>
                                                <option value="1">Daughter</option>
                                                <option value="2">Father</option>
                                                <option value="3">Friend</option>
                                                <option value="4">Mother</option>
                                                <option value="5">Owner</option>
                                                <option value="6">Partner</option>
                                                <option value="7">Son</option>
                                                <option value="8">Spouse</option>
                                                <option value="9">Other</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Entity FID/ID Number</label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_fid"
                                                   name="guarantor_form2_fid[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3">
                                            <label>Main Contact Person's First Name</label>
                                            <input class="form-control capsOn" type="text"
                                                   id="guarantor_form2_mainContact"
                                                   name="guarantor_form2_mainContact[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Middle Name</label>
                                            <input class="form-control capsOn" type="text"
                                                   id="guarantor_form2_middlename" name="guarantor_form2_middlename[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Last Name</label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_lastname"
                                                   name="guarantor_form2_lastname[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="col-sm-3 col-md-3">
                                            <label>Email</label>
                                            <input class="form-control capsOn" type="text" id="guarantor_form2_email2"
                                                   name="guarantor_form2_email2[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row guarantor-form2-phone-row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3 ">
                                            <label>Phone Type <em class="red-star">*</em></label>
                                            <select class="form-control guarantor_phoneType_form2">
                                                <option value="">Select</option>
                                                <option value="1">mobile</option>
                                                <option value="2">work</option>
                                                <option value="3">Fax</option>
                                                <option value="4">Home</option>
                                                <option value="5">Other</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3 ">
                                            <label>Carrier <em class="red-star">*</em></label>
                                            <select class="form-control guarantor_carrier_form2">
                                                <option value="">Select</option>
                                                <option value="1">Airtel</option>
                                                <option value="2">Aircel</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3 countycodediv">
                                            <label>Country Code <em class="red-star">*</em></label>
                                            <select class="form-control guarantor_countryCode_form2">
                                                <option value="">Select</option>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3">
                                            <label>Phone Number</label>
                                            <input class="form-control capsOn guarantor_phoneNumber_form2" type="text">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <a class="add-icon guarantor-phonerow-form2-plus-sign" href="javascript:;"><i
                                                    class="fa fa-plus-circle" aria-hidden="true">+</i></a>
                                        <a class="add-icon guarantor-phonerow-form2-remove-sign" href="javascript:;"
                                           style="display:none"><i class="fa fa-plus-circle" aria-hidden="true"></i>minus</a>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-outer">
                                        <div class="col-sm-3 col-md-3 ">
                                            <label>Year Of Guarantee</label>
                                            <select class="form-control" id="guarantor_form2_guarantee"
                                                    name="guarantor_form2_guarantee[]">
                                                <option value="0">Select</option>
                                                <?php for($year = 1; $year <= 100; $year++){ ?>
                                                    <option value="<?php echo $year ?>"><?php echo $year ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3">
                                            <label>Note</label>
                                            <textarea class="form-control capsOn" type="text" id="guarantor_form2_note"
                                                      name="guarantor_form2_note[]"></textarea>
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>

                                        <div class="col-sm-3 col-md-3">
                                            <label>Files</label>
                                            <input class="form-control" type="file" id="guarantor_form2_files"
                                                   name="guarantor_form2_files[]">
                                            <span class="ffirst_nameErr error red-star"></span>
                                            <span class="term_planErr error red-star"></span>
                                        </div>
                                        <a class="add-icon guarantor-form2-remove-sign" href="javascript:;"><i
                                                    class="fa fa-plus-circle" aria-hidden="true">&times;</i></a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>

                            /*Only for copy content*/

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>


<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name"
                                               name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text"
                                               name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer">
                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Jquery Starts -->

<script>var staticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Vehicledummy.png">';</script>
<script>var petStaticImage = '<img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/Petdummy.jpeg">';</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/intlTelInput.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenant.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/tenant/tenant.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantspopup.js"></script>


<script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
<script>
    $('.company-top').addClass('active');
    $('.cropItData').hide();
    jQuery('.phone_format').mask('000-000-0000', {reverse: true});
    jQuery('input[name="pet_phoneNumber[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="service_phoneNumber[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="additional_phone[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="guarantor_phone_1[]"]').mask('000-000-0000', {reverse: true});
    jQuery('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});

    $('.calander').datepicker({
        yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('input[name="birth"]').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(".calander").val(date);


    $('.calander1').datepicker({
        yearRange: '1900:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('.calander1').datepicker({dateFormat: jsDateFomat});
    var date = $.datepicker.formatDate(jsDateFomat, new Date());

    $(".calander1").val(date);
</script>

<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>



