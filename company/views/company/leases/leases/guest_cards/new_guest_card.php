<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 bread-search-outer">
                    <div class="breadcrumb-outer">
                        NewGuestCard >> <span>New Guest Card</span>
                    </div>
                    <div class="easy-search">
                        <input placeholder="Easy Search" type="text"/>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="content-section">
                        <!--Tabs Starts -->
                        <div class="main-tabs">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#guest-cards" >Guest Cards</a></li>
                                <li role="presentation"><a href="/RentalApplication/RentalApplications/1" >Rental Applications</a></li>
                                <li role="presentation"><a href="#leases" >Leases</a></li>
                                <li role="presentation"><a href="#moveIn" >Move-In</a></li>
                            </ul>
                            <div class="atoz-outer">
                                A-Z <span>All</span>
                            </div>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="guest-cards">
                                    <div class="accordion-form">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> General Information</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <form id="generallease">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Salutation</label>
                                                                            <select class="form-control" name="salutation" id="salutation">
                                                                                <option value="">Select</option>
                                                                                <option value="Dr.">Dr.</option>
                                                                                <option value="Mr.">Mr.</option>
                                                                                <option value="Mrs.">Mrs.</option>
                                                                                <option value="Mr. & Mrs.">Mr. & Mrs.</option>
                                                                                <option value="Ms.">Ms.</option>
                                                                                <option value="Sir">Sir</option>
                                                                                <option value="Madam">Madam</option>
                                                                                <option value="Brother">Brother</option>
                                                                                <option value="Sister">Sister</option>
                                                                                <option value="Father">Father</option>
                                                                                <option value="Mother">Mother</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>First Name <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" name="first_name" id="first_name"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Middle Name</label>
                                                                            <input class="form-control" type="text" name="middle_name" id="middle_name"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Last Name <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text" name="last_name" id="last_name"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Maiden Name</label>
                                                                            <input class="form-control" type="text" name="maiden_name" id="maiden_name"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Nick Name</label>
                                                                            <input class="form-control" type="text" name="nick_name" id="nick_name"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Gender</label>
                                                                            <select class="form-control" name="gender" id="general"><option>Select</option>
                                                                                <option value="1">Male</option>
                                                                                <option value="2">Female</option>
                                                                                <option value="3">Prefer Not to Say</option>
                                                                                <option value="4">Other</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>SSN/SIN/ID</label>
                                                                            <div class='multipleSsn' id="multipleSsn">
                                                                                <input class="form-control add-input capsOn" type="text"
                                                                                       id="ssn" name="ssn_sin_id[]" style="margin: 5px 0;">
                                                                                <a class="add-icon ssn-remove-sign" href="javascript:;"
                                                                                   style="display:none"><i class="fa fa-minus-circle" aria-hidden="true" style="display: inline;"></i></a>

                                                                                <span class="ffirst_nameErr error red-star"></span>

                                                                                <a class="add-icon ssn-plus-sign" href="javascript:;"><i
                                                                                            class="fa fa-plus-circle"
                                                                                            aria-hidden="true"></i></a>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Address 1</label>
                                                                            <input class="form-control" type="text" name="address1" id="address1"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Address 2</label>
                                                                            <input class="form-control" type="text" name="address2" id="address2"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Address 3</label>
                                                                            <input class="form-control" type="text" name="address3" id="address3"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Address 4</label>
                                                                            <input class="form-control" type="text" name="address4" id="address4"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Zip/Postal Code</label>
                                                                            <input class="form-control" type="text" name="zipcode" id="zipcode"/ value="<?php echo $_SESSION[SESSION_DOMAIN]['default_zipcode'] ?>">
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Country</label>
                                                                            <input class="form-control" type="text" name="country" id="country"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>State / Province</label>
                                                                            <input class="form-control" type="text" name="state" id="state"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>City</label>
                                                                            <input class="form-control" type="text" name="city" id="city"/>
                                                                        </div>
                                                                        <div class="col-sm-3 col-md-3">
                                                                            <label>Email <em class="red-star">*</em></label>

                                                                            <div class='multipleEmail'>
                                                                                <input class="form-control add-input capsOn" type="text" name="email[]" id="email">
                                                                                <a class="add-icon email-remove-sign" href="javascript:;">
                                                                                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                                                </a>
                                                                                <a class="add-icon email-plus-sign" href="javascript:;">
                                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                </a>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Preferred Contact Time</label>
                                                                            <select name="preferred" id="preferred">
                                                                                <option value="8:00am">8:00 am</option>
                                                                                <option value="8:05am">8:05 am</option>
                                                                                <option value="8:10am">8:10 am</option>
                                                                                <option value="8:15am">8:15 am</option>
                                                                                <option value="8:15am">8:15 am</option>
                                                                                <option value="8:20am">8:20 am</option>
                                                                                <option value="8:25am">8:25 am</option>
                                                                                <option value="8:30am">8:30 am</option>
                                                                                <option value="8:35am">8:35 am</option>
                                                                                <option value="8:40am">8:40 am</option>
                                                                                <option value="8:45am">8:45 am</option>
                                                                                <option value="8:50am">8:50 am</option>
                                                                                <option value="8:55am">8:55 am</option>
                                                                                <option value="9:00am">9:00 am</option>
                                                                                <option value="9:05am">9:05 am</option>
                                                                                <option value="9:10am">9:10 am</option>
                                                                                <option value="9:15am">9:15 am</option>
                                                                                <option value="9:20am">9:20 am</option>
                                                                                <option value="9:25am">9:25 am</option>
                                                                                <option value="9:30am">9:30 am</option>
                                                                                <option value="9:35am">9:35 am</option>
                                                                                <option value="9:40am">9:40 am</option>
                                                                                <option value="9:45am">9:45 am</option>
                                                                                <option value="9:50am">9:50 am</option>
                                                                                <option value="9:55am">9:55 am</option>
                                                                                <option value="10:00am">10:00 am</option>
                                                                                <option value="10:05am">10:05 am</option>
                                                                                <option value="10:10am">10:10 am</option>
                                                                                <option value="10:15am">10:15 am</option>
                                                                                <option value="10:20am">10:20 am</option>
                                                                                <option value="10:25am">10:25 am</option>
                                                                                <option value="10:30am">10:30 am</option>
                                                                                <option value="10:35am">10:35 am</option>
                                                                                <option value="10:40am">10:40 am</option>
                                                                                <option value="10:45am">10:45 am</option>
                                                                                <option value="10:50am">10:50 am</option>
                                                                                <option value="10:55am">10:55 am</option>
                                                                                <option value="11:00am">11:00 am</option>
                                                                                <option value="11:05am">11:05 am</option>
                                                                                <option value="11:00am">11:00 am</option>
                                                                                <option value="11:05am">11:05 am</option>
                                                                                <option value="11:10am">11:10 am</option>
                                                                                <option value="11:15am">11:15 am</option>
                                                                                <option value="11:20am">11:20 am</option>
                                                                                <option value="11:25am">11:25 am</option>
                                                                                <option value="11:30am">11:30 am</option>
                                                                                <option value="11:35am">11:35 am</option>
                                                                                <option value="11:40am">11:40 am</option>
                                                                                <option value="11:45am">11:45 am</option>
                                                                                <option value="11:50am">11:50 am</option>
                                                                                <option value="11:55am">11:55 am</option>
                                                                                <option value="12:00am">12:00 am</option>
                                                                            </select>
<!--                                                                            <input class="form-control" type="text" name="preferred" id="preferred"/>-->
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Referral Source <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control" id="referral_id" name="referral_source" id="referral_source"><option>Ad/Sponsor</option></select>
                                                                        </div>
                                                                        <div class="primary-tenant-phone-row">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-3 col-md-3 ">
                                                                                    <label>Phone Type</label>
                                                                                    <select class="form-control" name="phoneType[]" id="phone_type">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">Mobile</option>
                                                                                        <option value="2">Work</option>
                                                                                        <option value="3">Fax</option>
                                                                                        <option value="4">Home</option>
                                                                                        <option value="5">Other</option>
                                                                                    </select>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>

                                                                                <div class="col-sm-3 col-md-3 ">
                                                                                    <label>Carrier <em class="red-star">*</em></label>
                                                                                    <select class="form-control" name="carrier[]" id="guestCarrier"></select>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>

                                                                                <div class="col-sm-3 col-md-3 countycodediv">
                                                                                    <label>Country Code</label>
                                                                                    <select class="form-control" name="countryCode[]" id="guestCountries">
                                                                                        <option value="">Select</option>
                                                                                    </select>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>

                                                                                <div class="col-sm-3 col-md-3 ">
                                                                                    <label>Phone Number <em class="red-star">*</em></label>
                                                                                    <input class="form-control capsOn add-input phone_format" type="text"
                                                                                           name="phoneNumber[]" id="phone_number">
                                                                                    <a class="add-icon" href="javascript:;"><i
                                                                                                class="fa fa-plus-circle"
                                                                                                aria-hidden="true"></i></a>
                                                                                    <a class="add-icon" href="javascript:;" style=""><i
                                                                                                class="fa fa-minus-circle"
                                                                                                aria-hidden="true"></i></a>
                                                                                    <span class="ffirst_nameErr error red-star"></span>
                                                                                    <span class="term_planErr error red-star"></span>
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Ethnicity <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control" id="guestEthnicity" name="ethnicity"></select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Martial Status <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control" id="guestMarital" name="martial_status"></select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Hobbies <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control" name="hobbies" id="guestHobbies" multiple></select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Veteran Status <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                            <select class="form-control" id="guestVeteran" name="veteran_status"></select>
                                                                        </div>
<!--                                                                        <div class="col-xs-12">-->
<!--                                                                            <div class="btn-outer">-->
<!--                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnGuestId"/>-->
<!--                                                                                <button type="button" id="add_guest_cancel_btn" class="grey-btn">Cancel</button>-->
<!--                                                                            </div>-->
<!--                                                                        </div>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </form>
                                                            <input type="hidden" value="" name="guestcard_id" class="form-control" id="guestcard_id"/>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Unit Preferences</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Search Property <em class="red-star">*</em></label>
                                                                            <select class="form-control" name="property_name" id="PropertyId"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Search Unit <em class="red-star">*</em></label>
                                                                            <select class="form-control unitId" name="building" id="unitId"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Bedrooms</label>
                                                                            <select class="form-control" name="bedroom" id="bedroomId">
                                                                                <option>Select</option>
                                                                                <option>1</option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                                <option>6</option>
                                                                                <option>7</option>
                                                                                <option>8</option>
                                                                                <option>9</option>
                                                                                <option>10</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Bathroom <em class="red-star">*</em></label>
                                                                            <select class="form-control" name="bathroom" id="bathroomId">
                                                                                <option>Select</option>
                                                                                <option>1</option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                                <option>6</option>
                                                                                <option>7</option>
                                                                                <option>8</option>
                                                                                <option>9</option>
                                                                                <option>10</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Floor</label>
                                                                            <select class="form-control" name="floor" id="floorId">
                                                                                <option>Select</option>
                                                                                <option>1</option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                                <option>6</option>
                                                                                <option>7</option>
                                                                                <option>8</option>
                                                                                <option>9</option>
                                                                                <option>10</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Expected Move In Date</label>
                                                                            <input class="form-control calendar" type="text" name="expected_move_in" id="datepicker"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Minimum Rent ($)</label>
                                                                            <input class="form-control" type="text" id=""/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Maximum Rent ($)</label>
                                                                            <input class="form-control" type="text"/>
                                                                        </div>

                                                                        <div class="div-full">
                                                                            <div class="check-outer">
                                                                                <input type="checkbox">
                                                                                <label>Pet Friendly</label>
                                                                            </div>
                                                                            <div class="check-outer">
                                                                                <input type="checkbox">
                                                                                <label>Parking</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="btn-outer">
                                                                                <button class="blue-btn guestCardSearch" id="guestSearch">Search</button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="grid-outer" id="search_table" style="display: none;">
                                                                            <center class="table-responsive">
                                                                                <table class="table table-hover table-dark" id="search_records">
                                                                                </table>
                                                                                <center>   <span id="no_records" style="display: none;">No Records</span></center>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Emergency Contact Details</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Emergency Contact Name</label>
                                                                            <select class="form-control"><option>Select</option></select>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Relationship <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Country Code</label>
                                                                            <input class="form-control" type="text"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Phone Number <em class="red-star">*</em></label>
                                                                            <input class="form-control" type="text"/>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>Email</label>
                                                                            <input class="form-control" type="text"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Notes</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-4">
                                                                            <textarea class="notes capital"></textarea>
                                                                            <a class="add-icon-textarea" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> File Library</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-sm-4">
                                                                            <button class="green-btn">Add Files...</button>
                                                                            <button class="orange-btn">Remove All Files...</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span> Custom Fields</a> <a class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-xs-12 col-sm-4 col-md-3">
                                                                            <label>No Custom Fields</label>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-4 col-md-3 text-right pull-right">
                                                                            <label></label>
                                                                            <button class="blue-btn">New Custom Field</button>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Accordian Ends -->
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer apex-btn-block left text-right">
                                    <!--<a class="blue-btn savetenantnext" value="">Next</a>-->
                                    <input type="submit" class="blue-btn savelease" value="Next">
                                    <input type="submit" class="blue-btn savelease" value="Save">
                                    <button type="button" onclick="goBack()" id="add_company_button" class="grey-btn">Cancel</button>
                                </div>
                            </div>
                            <!--tab Ends -->

                        </div>

                    </div>
                </div>
            </div>
    </section>
</div>
<!-- Wrapper Ends -->



<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    $(function() {
        $('#datepicker').datepicker();
    });

    // $('.calander').datepicker({
    //     yearRange: '1919:2019',
    //     changeMonth: true,
    //     changeYear: true,
    //     dateFormat: jsDateFomat
    // });
    // $('input[name="expected_move_in"]').datepicker({
    //     yearRange: '1900:2019',
    //     changeMonth: true,
    //     changeYear: true,
    //     dateFormat: jsDateFomat
    // });
    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SITE_URL; ?>/js/company/leases/guestCard/guestCard.js"></script>
<script>
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
