<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="wrapper" xmlns="http://www.w3.org/1999/html">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <section class="main-content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="content-rt">
                        <div class="content-rt-hdr">
                            <div class="breadcrumb">
                                <h3>Tenant >> <span>  Tenant Transfer</span>
                                    <input type="text" class="form-control input-sm easy-search" id="easy_search" placeholder="Easy Search">
                                    <input type="hidden" value="<?php echo $_GET['tenant_id']; ?>" name="tenantapplytaxid" class="tenantapplytaxid">
                                </h3>
                            </div>
                        </div>

                        <div class="content-data">
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <div class="container">
                                    <div class="form-outer">
                                        <div class="apx-table">
                                        <div class="table-responsive">
                                            <table id="tenantTrasTable"></table>
                                        </div>
                                        </div>

                                    </div>


                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>
<!-- Wrapper Ends -->


<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });

        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });

    });



</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/people/tenant/tenantTransfer.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->