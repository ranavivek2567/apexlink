<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid" id="log_tabb">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8" >
                            <div class="breadcrumb-outer">
                                Maintenance &gt;&gt; <span>Mileage log</span>
                            </div>
                        </div>
                        <div class="col-sm-4 pull-right">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>MAINTENANCE</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Work Order</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Recurring Work Order</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="/WorkOrder/WorkOrders" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">View All Work Orders</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="/ticket/tickets" id="" class="list-group-item list-group-item-success strong collapsed">View All Tickets</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="/Inspection/Inspection" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Inspection</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="/LostAndFound/LostAndFound" class="list-group-item list-group-item-success strong collapsed" >Lost & Found Manager</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/MileageLog/MileageLog" class="list-group-item list-group-item-success strong collapsed" >Mileage Log</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/Maintenance/Inventory" class="list-group-item list-group-item-success strong collapsed" >Inventory Tracker</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/PurchaseOrder/PurchaseOrder" class="list-group-item list-group-item-success strong collapsed" >New Purchase Order</a>
                                <!-- Seven Ends-->
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/ticket/tickets">Tickets</a></li>
                            <li role="presentation"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                            <li role="presentation"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                            <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                            <li role="presentation" class="active"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                            <li role="presentation"><a href="/Inspection/Inspection">Inspections</a></li>
                            <li role="presentation"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane active" id="tickets-mileage">
                                <!-- Sub Tabs Starts-->
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#company-vehicle" aria-controls="home" role="tab" data-toggle="tab">Company Vehicle Mileage Log</a></li>
                                        <li role="presentation"><a href="#employee-vehicle" aria-controls="profile" role="tab" data-toggle="tab">Employee Vehicle Mileage Log</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="company-vehicle">
                                            <div class="property-status postion-relative">
                                                <div class="row">
                                                    <div id="tabledropdown1" class="panel-collapse collapse list_container">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive" id="list_search">
                                                                <table class="table table-hover table-dark" id="vendorlist1">
                                                                    <thead>
                                                                    <tr class="header">
                                                                        <th style="display: none;"></th>
                                                                        <th>Employee Name</th>
                                                                        <th>Employee ID</th>
                                                                        <th>Phone</th>
                                                                        <th>Email</th>
                                                                        <th style="display: none;"></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <input type="hidden" id="pm_name"/>
                                                    <div class="col-sm-6 col-md-3">
                                                        <label>Select/Add Employee Vehicle <span class="addvehicle"><i class="fa fa-plus-circle" aria-hidden="true"></i></span></label>
                                                        <input type="text" class="form-control" placeholder=" Click here to select" id="search_vehicle" name="search_vehicle" autocomplete="off"/>
                                                        <input type="hidden" class="form-control" placeholder=" Click here to select" id="search_idd" name="search_idd"/>
                                                    </div>

                                                    <input  type="hidden" maxlength="50" placeholder="Location" id="update_idd"/>
                                                    <input  type="hidden" maxlength="50" placeholder="Location" id="update_vidd"/>

                                                    <div class="modal fade" id="add_vehiclelog" role="dialog">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                                    <h4 class="modal-title">Add New Employee Log</h4>
                                                                </div>
                                                                <div class="modal-body pull-left">
                                                                    <div class="">
                                                                        <div class="form-outer">
                                                                            <form method="post" id="comp_employeelogg">
                                                                                <div class="">

                                                                                    <div class="row">
                                                                                        <div class="col-sm-3">
                                                                                            <label>
                                                                                                Date <em class="red-star">*</em></label>
                                                                                            <input class="form-control calander3" type="text" maxlength="20" name="date" placeholder="date" readonly/>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                            <label>
                                                                                                Driver </label>
                                                                                            <input class="form-control capital " type="text" maxlength="20" name="driver" placeholder="Driver" />
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                            <label>
                                                                                                Select Vehicle </label>
                                                                                            <select class="form-control " type="text" maxlength="20" name="select_vehicle"  id="select_vehicle"></select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">


                                                                                        <div class="form-outer">

                                                                                            <div class="col-sm-3">
                                                                                                <label>Name of Company</label>
                                                                                                <input class="form-control capital" type="text" maxlength="20" name="comp_namee" id="comp_name" placeholder="Company Name"/>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <label>Branch or Location</label>
                                                                                                <input class="form-control capital" type="text" maxlength="50" name="locationn" placeholder="Location"/>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <label>Employee full Name  <em class="red-star">*</em></label>
                                                                                                <select class="form-control capital" type="text" maxlength="20" name="full_namee" placeholder="Full Name" id="full_namee"></select>
                                                                                            </div>

                                                                                            <div class="col-sm-3">
                                                                                                <label> Employee ID</label>
                                                                                                <input class="form-control capital" type="text" maxlength="20" name="emp_idd" id="emp_idd" placeholder="Employee ID" readonly/>
                                                                                            </div>



                                                                                            <div class="col-sm-12">
                                                                                                <div class="form-data form-data2">

                                                                                                    <div class="add-newemp-log">
                                                                                                        <h5 >
                                                                                                            Journey Details
                                                                                                        </h5>
                                                                                                    </div>

                                                                                                    <div class="entity-company-check-div">
                                                                                                        <div class="row">

                                                                                                            <div class="col-sm-3">
                                                                                                                <label>To <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital" type="text" placeholder="To" maxlength="50" name="end_loca"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>From <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital" name="start_loca" type="text" id="end" placeholder="From"/>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                    </div>

                                                                                                    <div class="add-newemp-log">
                                                                                                        <h5 >
                                                                                                            Odometer Reading
                                                                                                        </h5>
                                                                                                    </div>

                                                                                                    <div class="entity-company-check-div">
                                                                                                        <div class="row">

                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Start Reading <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital" type="number" placeholder="Start Reading" maxlength="50" name="start_readingg" id="start_readingg" pattern="^[0-9]*$" />
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Stop Reading <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital" name="stop_readingg" type="number" id="stop_readingg" placeholder="Stop Reading" pattern="^[0-9]*$"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label> Mileage (Distance)</label>
                                                                                                                <input class="form-control capital" name="mileagee" type="text" id="mileagee" placeholder="Mileage" readonly/>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                    </div>

                                                                                                    <div class="row notes_date_right_div" >
                                                                                                        <div class="col-sm-6">
                                                                                                            <label>Purpose/Notes</label>
                                                                                                            <textarea class="form-control capital notes_date_right" rows="4" cols="50" name="note" type="" id="notes" placeholder="Notes"></textarea>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                            <div class="btn-outer text-right">
                                                                                <input type="button" value="Save" class="blue-btn" id="save_vehiclelog">
                                                                                <input type="button" class="clear-btn clearFormReset" value="Clear">
                                                                                <input type="button" class="grey-btn cancel_Popup" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <form id="raddioo" >
                                                    <div class=" weekly-biweekly">
                                                        <div class="check-outer">
                                                            <input name="alt" type="radio" value="week" checked="checked">
                                                            <label>Weekly/Bi-Weekly/Monthly</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input name="alt" type="radio" value="date">
                                                            <label>By Date Range</label>
                                                        </div>
                                                    </div>
                                                    </form>
                                                    <div class="col-sm-6 col-md-2" id="byweek">
                                                        <select class="form-control " id="searchvaluess">
                                                            <option value="0">Select</option>
                                                            <option  value="1">Weekly</option>
                                                            <option  value="2">Bi-Weekly</option>
                                                            <option  value="3">Monthly</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6 col-md-2 monthlyy" style="display: none" >

                                                        <input class="form-control capital calander1" type="text" maxlength="20" name="date" placeholder="date"/>
                                                    </div>

                                                    <div  id="bydate" style="display: none;">

                                                            <div class="col-sm-2">

                                                                <input class="form-control capital calander" type="text" maxlength="20" name="date" placeholder="date"/>
                                                            </div>
                                                            <div class="col-sm-2">

                                                                <input class="form-control calander3" type="text" maxlength="20" name="date" placeholder="date"/>
                                                            </div>
                                                        </div>



                                                    <div class="modal fade" id="edit_vehiclelog" role="dialog">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                                    <h4 class="modal-title">Edit Employee Log</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="add-popup-body">
                                                                        <div class="form-outer">
                                                                            <form method="post" id="edit_companyvehiclelog">
                                                                                <div class="panel-body">

                                                                                    <div class="row">
                                                                                        <div class="col-sm-3">
                                                                                            <label>
                                                                                                Date <em class="red-star">*</em></label>
                                                                                            <input class="form-control calander3" type="text" maxlength="20" name="date" placeholder="date" id="date" readonly/>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                            <label>
                                                                                                Driver </label>
                                                                                            <input class="form-control capital " type="text" maxlength="20" name="driver" placeholder="Driver" id="driver"/>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                            <label>
                                                                                                Select Vehicle </label>
                                                                                            <select class="form-control vehicle_name" type="text" maxlength="20" name="select_vehicle"  id="vehicle_namee"></select>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">


                                                                                        <div class="form-outer">
                                                                                            <div class="col-sm-12">

                                                                                                <div class="col-sm-3">
                                                                                                    <label>Name of Company</label>
                                                                                                    <input class="form-control capital" type="text" maxlength="20" name="comp_namee" id="company_name" placeholder="Company Name"/>
                                                                                                </div>
                                                                                                <div class="col-sm-3">
                                                                                                    <label>Branch or Location</label>
                                                                                                    <input class="form-control capital" type="text" maxlength="50" name="locationn" placeholder="Location" id="branch"/>
                                                                                                </div>
                                                                                                <div class="col-sm-3">
                                                                                                    <label>Employee full Name  <em class="red-star">*</em></label>
                                                                                                    <select class="form-control capital emp_namee" type="text" maxlength="20" name="full_namee" placeholder="Full Name" id="emp_namme"></select>
                                                                                                </div>

                                                                                                <div class="col-sm-3">
                                                                                                    <label> Employee ID</label>
                                                                                                    <input class="form-control capital" type="text" maxlength="20" name="emp_idd" placeholder="Employee ID" id="user_id" readonly/>
                                                                                                </div>


                                                                                            </div>

                                                                                            <div class="form-outer">
                                                                                                <div class="form-data form-data2">

                                                                                                    <div class="">
                                                                                                        <h5 >
                                                                                                            Journey Details
                                                                                                        </h5>
                                                                                                    </div>

                                                                                                    <div class="entity-company-check-div">
                                                                                                        <div class="col-sm-12">

                                                                                                            <div class="col-sm-3">
                                                                                                                <label>To <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital" type="text" placeholder="To" maxlength="50" name="end_loca" id="end_location"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>From <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital" name="start_loca" type="text" id="start_location" placeholder="From" id=""/>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                    </div>

                                                                                                    <div class="">
                                                                                                        <h5 >
                                                                                                            Odometer Reading
                                                                                                        </h5>
                                                                                                    </div>

                                                                                                    <div class="entity-company-check-div">
                                                                                                        <div class="col-sm-12">

                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Start Reading <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital" type="number" placeholder="Start Reading" maxlength="50" name="start_readingg" id="initial_reading" pattern="^[0-9]*$" />
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Stop Reading <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital" name="stop_readingg" type="number" id="final_reading" placeholder="Stop Reading" pattern="^[0-9]*$"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label> Mileage (Distance)</label>
                                                                                                                <input class="form-control capital mileage" name="mileagee" type="number"  placeholder="Mileage" readonly/>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                    </div>

                                                                                                    <div class="row">
                                                                                                        <div class="col-sm-3">
                                                                                                            <label>Purpose/Notes</label>
                                                                                                            <textarea class="form-control capital" rows="4" cols="50" name="note" type="" id="notes" placeholder="Notes"></textarea>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </form>
                                                                            <div class="btn-outer text-right">
                                                                                <input type="button" value="Update" class="blue-btn" id="update_vehiclelog">
                                                                                <input type="button" class="clear-btn formreset1" value="Reset">
                                                                                <input type="button" class="grey-btn cancel_edit__Popup" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
<!--                                                                    <button type="button" class="btn btn-default yes-cancel">Yes</button>-->
<!--                                                                    <button type="button" class="btn btn-default no-cancel" data-dismiss="modal">No</button>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="col-sm-2">
                                                        <div class="btn-outer">
                                                            <a align="center" id="mymodalcheck" class="blue-btn btn-block">Search</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="form-hdr">
                                                    <h3>Mileage Log</h3>
                                                </div>
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="accordion-grid">
                                                                            <div class="panel-group" id="accordion">
                                                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                            <div class="grid-outer">
                                                                                                <div class="table-responsive">
                                                                                                    <div class="apx-table listinggridDiv">
                                                                                                        <div class="table-responsive">
                                                                                                            <table id="vehicle_log" class="table table-bordered"></table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                  </div>
                                                                            </div>
                                                                         </div>
                                                                    </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="employee-vehicle">
                                            <div class="property-status postion-relative">
                                                <div class="row">
                                                    <div id="tabledropdown2" class="panel-collapse collapse vendor_list_container">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive" id="vendor_list_search">
                                                                <table class="table table-hover table-dark" id="vendorlist">
                                                                    <thead>
                                                                    <tr class="header">
                                                                        <th style="display: none;"></th>
                                                                        <th>Employee Name</th>
                                                                        <th>Employee ID</th>
                                                                        <th>Phone</th>
                                                                        <th>Email</th>
                                                                        <th style="display: none;"></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6 col-md-2">
                                                        <label>Select/Add Employee Vehicle <span class="pop-add-icon addemployee"><i class="fa fa-plus-circle" aria-hidden="true"></i></span></label>
                                                        <input type="text" class="form-control" placeholder=" Click here to select" id="search" name="search" autocomplete="off"/>
                                                        <input type="hidden" class="form-control" placeholder=" Click here to select" id="search_id" name="search_id"/>
                                                    </div>

                                                    <div class="modal fade" id="addmileagelog" role="dialog">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content pull-left">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                                    <h4 class="modal-title">Add New Employee Log</h4>
                                                                </div>
                                                                <div class="modal-body pull-left">
                                                                    <div class="">
                                                                        <div class="form-outer">
                                                                            <form method="post" id="addmileagelogg">     <div class="">

                                                                                    <div class="row">
                                                                                        <div class="col-sm-3">
                                                                                            <label>
                                                                                                Date <em class="red-star">*</em></label>
                                                                                            <input class="form-control calander3" type="text" maxlength="20" name="date" placeholder="date" readonly/>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="form-outer">

                                                                                            <div class="col-sm-3">
                                                                                                <label>Name of Company</label>
                                                                                                <input class="form-control capital" type="text" maxlength="20" name="comp_name" id="compp_name" placeholder="Company Name"/>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <label>Branch or Location</label>
                                                                                                <input class="form-control capital" type="text" maxlength="50" name="location" placeholder="Location"/>
                                                                                            </div>
                                                                                            <div class="col-sm-3">
                                                                                                <label>Employee full Name  <em class="red-star">*</em></label>
                                                                                                <select class="form-control capital" type="text" maxlength="20" name="full_name" placeholder="Full Name" id="full_name"></select>
                                                                                            </div>

                                                                                            <div class="col-sm-3">
                                                                                                <label> Employee ID</label>
                                                                                                <input class="form-control capital" type="text" maxlength="20" name="emp_id" id="emp_id" placeholder="Employee ID" readonly/>
                                                                                            </div>



                                                                                            <div class="col-sm-12">
                                                                                                    <div class="form-outer">
                                                                                                    <div class="form-hdr">
                                                                                                        <h4 class="modal-title" >
                                                                                                            Add New Vehicle
                                                                                                        </h4>
                                                                                                    </div>

                                                                                                <div class="form-data form-data2 pad-none">

                                                                                                    <div class="panel-body pull-left">
                                                                                                        <div class="row">
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Vehicle Name<em class="red-star">*</em>
                                                                                                                </label>
                                                                                                                <input class="form-control vehicle_name capital" type="text" value=""  maxlength="6" name="vehicle_name"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Vehicle#</label>
                                                                                                                <input class="form-control capital" type="text"  maxlength="50" name="vehicle_id" id="vehicle_no"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Vehicle Type</label>
                                                                                                                <input class="form-control capital capital" name="vehicle_type" type="text" id="vehicle_type" placeholder="Vehicle Type"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Make</label>
                                                                                                                <input class="form-control capital capital" name="vehicle_make" type="text" id="vehicle_make" placeholder="Make"/>
                                                                                                            </div>

                                                                                                        </div>


                                                                                                        <div class="row">

                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Model </label>
                                                                                                                <input class="form-control model capital" type="text" value=""  maxlength="6" name="model"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>VIN#</label>
                                                                                                                <input class="form-control capital" type="text" placeholder="VIN#" maxlength="50" name="vin"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Registration #</label>
                                                                                                                <input class="form-control capital" name="registration_no" type="text" id="registration_no" placeholder="Registration #"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>License Plate#</label>
                                                                                                                <input class="form-control capital" name="license_no" type="text" id="license_no" placeholder="License Plate#"/>
                                                                                                            </div>


                                                                                                        </div>
                                                                                                        <div class="row">

                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Color </label>
                                                                                                                <input class="form-control color capsOn" type="text" value=""  maxlength="6" name="color"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Year Of Vehicle</label>
                                                                                                                <select class="form-control capital" type="year" placeholder="Year Of Vehicle" maxlength="50" name="year_vehicle" id="year_vehicle"></select>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Date Purchased</label>
                                                                                                                <input class="form-control capital calander" type="text" maxlength="20" name="date_purchases"/>
                                                                                                            </div>


                                                                                                        </div>
                                                                                                        <div class="add-newemp-log">

                                                                                                        <h5 >
                                                                                                            Journey Details
                                                                                                        </h5>
                                                                                                    </div>

                                                                                                        <div class="entity-company-check-div ">
                                                                                                            <div class="row">

                                                                                                                <div class="col-sm-3">
                                                                                                                    <label>To <em class="red-star">*</em></label>
                                                                                                                    <input class="form-control capital" type="text" placeholder="To" maxlength="50" name="end_loc"/>
                                                                                                                </div>
                                                                                                                <div class="col-sm-3">
                                                                                                                    <label>From <em class="red-star">*</em></label>
                                                                                                                    <input class="form-control capital" name="start_loc" type="text" id="end" placeholder="From"/>
                                                                                                                </div>

                                                                                                            </div>

                                                                                                        </div>


                                                                                                        <div class="add-newemp-log">
                                                                                                            <h5 >
                                                                                                                Odometer Reading
                                                                                                            </h5>
                                                                                                        </div>

                                                                                                        <div class="entity-company-check-div">
                                                                                                            <div class="row">

                                                                                                                <div class="col-sm-3">
                                                                                                                    <label>Start Reading <em class="red-star">*</em></label>
                                                                                                                    <input class="form-control capital" type="number" placeholder="Start Reading" maxlength="50" name="start_reading" id="start_reading" pattern="^[0-9]*$" />
                                                                                                                </div>
                                                                                                                <div class="col-sm-3">
                                                                                                                    <label>Stop Reading <em class="red-star">*</em></label>
                                                                                                                    <input class="form-control capital" name="stop_reading" type="number" id="stop_reading" placeholder="Stop Reading" pattern="^[0-9]*$"/>
                                                                                                                </div>
                                                                                                                <div class="col-sm-3">
                                                                                                                    <label> Mileage (Distance)</label>
                                                                                                                    <input class="form-control capital" name="mileage" type="number" id="mileage" placeholder="Mileage" readonly/>
                                                                                                                </div>

                                                                                                            </div>

                                                                                                        </div>

<!--                                                                                                        <div class="row">-->
<!--                                                                                                            <div class="col-sm-3">-->
<!--                                                                                                                <label>Amount(US $)</label>-->
<!--                                                                                                                <input class="form-control capital" name="amount" type="number" id="amount" placeholder="Amount"/>-->
<!--                                                                                                            </div>-->
<!--                                                                                                        </div>-->
                                                                                                    </div>


                                                                                                </div>
                                                                                                    </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div></form>
                                                                            <div class="btn-outer text-right">
                                                                                <input type="button" value="Save" class="blue-btn" id="savelog">
                                                                                <input type="button" class="clear-btn clearFormReset" value="Clear">
                                                                                <input type="button" class="grey-btn cancelPopup" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>


                                                    <form id="radioo" >
                                                    <div class="weekly-biweekly">
                                                        <div class="check-outer">
                                                            <input name="case" type="radio" value="weekk" checked="checked">
                                                            <label>Weekly/Bi-Weekly/Monthly</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input name="case" type="radio" value="datee">
                                                            <label>By Date Range</label>
                                                        </div>
                                                    </div>
                                                        </form>
                                                    <div class="col-sm-6 col-md-2" id="week">

                                                        <select class="form-control searchvalues2" id="searchvalues">
                                                            <option value="0">Select</option>
                                                            <option value="1">Weekly</option>
                                                            <option value="2">Bi-Weekly</option>
                                                            <option value="3">Monthly</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6 col-md-2 monthly" style="display: none" >

                                                        <input class="form-control capital calander2" type="text" maxlength="20" name="date" placeholder="date"/>
                                                    </div>

                                                        <div id="datte" style="display: none;">
                                                            <div class="col-sm-2">

                                                                <input class="form-control capital calander" type="text" maxlength="20" name="date" placeholder="date" id="date-range1"/>
                                                            </div>
                                                            <div class="col-sm-2">

                                                                <input class="form-control capital calander" type="text" maxlength="20" name="date" placeholder="date" id="date-range2"/>
                                                                <input class="form-control capital calander" type="hidden" maxlength="20" name="date" placeholder="date" id="hidden-date"/>
                                                            </div>
                                                        </div>


                                                    <div class="col-sm-2">

                                                        <div class="btn-outer">
                                                            <button class="blue-btn btn-block" id="filter2">Search</button>
                                                        </div>
                                                    </div>



                                                    <div class="modal fade" id="edit_companylog" role="dialog">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content pull-left">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                                    <h4 class="modal-title">Edit Employee Log</h4>
                                                                </div>
                                                                <div class="modal-body pull-left">
                                                                    <div class="">
                                                                        <div class="form-outer">
                                                                            <form method="post" id="editmileagelogg">     <div class="">

                                                                                    <div class="row">
                                                                                        <div class="col-sm-3">
                                                                                            <label>Date <em class="red-star">*</em></label>
                                                                                            <input class="form-control capital calander date" type="text" maxlength="20" name="date" placeholder="date" readonly/>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="row">
                                                                                        <div class="form-outer">
                                                                                            <div class="">

                                                                                                <div class="col-sm-3">
                                                                                                    <label>Name of Company</label>
                                                                                                    <input class="form-control capital company_name" type="text" maxlength="20" name="comp_name" id="company_namee" placeholder="Company Name"/>
                                                                                                </div>
                                                                                                <div class="col-sm-3">
                                                                                                    <label>Branch or Location</label>
                                                                                                    <input class="form-control capital branch" type="text" maxlength="50" name="location" placeholder="Location"/>
                                                                                                </div>
                                                                                                <div class="col-sm-3">
                                                                                                    <label>Employee full Name  <em class="red-star">*</em></label>
                                                                                                    <select class="form-control capital emppp_name" type="text" name="full_name" placeholder="Full Name" id="em_name">

                                                                                                    </select>
                                                                                                </div>

                                                                                                <div class="col-sm-3">
                                                                                                    <label> Employee ID</label>
                                                                                                    <input class="form-control capital user_id" type="text" maxlength="20" name="emp_id" id="emp_id" placeholder="Employee ID" readonly/>
                                                                                                </div>

                                                                                            </div>
                                                                                            <div class="col-sm-12">

                                                                                            <div class="form-outer">
                                                                                                <div class="form-hdr">
                                                                                                    <h4 class="modal-title">
                                                                                                        Edit Vehicle
                                                                                                    </h4>
                                                                                                </div>
                                                                                                <div class="form-data form-data2">

                                                                                                    <div class="row">


                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Vehicle Name<em class="red-star">*</em>
                                                                                                                </label>
                                                                                                                <input class="form-control vehicle_name vehicle_name capital" type="text" value=""  maxlength="6" name="vehicle_name"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Vehicle#</label>
                                                                                                                <input class="form-control capital vehicle_id" type="text"  maxlength="50" name="vehicle_id" id="vehicle_id" readonly/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Vehicle Type</label>
                                                                                                                <input class="form-control capital vehicle_type capital" name="vehicle_type" type="text" id="vehicle_type" placeholder="Vehicle Type"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Make</label>
                                                                                                                <input class="form-control capital vehicle_make capital" name="vehicle_make" type="text" id="vehicle_make" placeholder="Make"/>
                                                                                                            </div>


                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Model </label>
                                                                                                                <input class="form-control model capital" type="text" value=""  maxlength="6" name="model" id="model"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>VIN#</label>
                                                                                                                <input class="form-control capital vin_no" type="text" placeholder="VIN#" maxlength="50" name="vin" id="vin_no"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Registration #</label>
                                                                                                                <input class="form-control capital registration" name="registration_no" type="text" id="registration" placeholder="Registration #"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>License Plate#</label>
                                                                                                                <input class="form-control capital licence_plate" name="license_no" type="text" id="licence_plate" placeholder="License Plate#"/>
                                                                                                            </div>



                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Color </label>
                                                                                                                <input class="form-control color capsOn" type="text" value=""  maxlength="6" name="color" id="color"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Year Of Vehicle</label>
                                                                                                                <input class="form-control capital year_of_vehicle" type="year" placeholder="Year Of Vehicle" maxlength="50" name="year_vehicle" id="year_of_vehicle" readonly/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Date Purchased</label>
                                                                                                                <input class="form-control capital calander date_purchased" type="text" maxlength="20" name="date_purchases"  id="date_purchased" readonly/>
                                                                                                            </div>

                                                                                                        </div>



                                                                                                    <div class="add-newemp-log">
                                                                                                        <h5 >
                                                                                                            Journey Details
                                                                                                        </h5>
                                                                                                    </div>

                                                                                                    <div class="entity-company-check-div">
                                                                                                        <div class="row">

                                                                                                            <div class="col-sm-3">
                                                                                                                <label>To <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital start_location" type="text" placeholder="To" maxlength="50" name="end_loc" id="start_location"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>From <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital end_location" name="start_loc" type="text" placeholder="From" id="end_location"/>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                    </div>


                                                                                                    <div class=" add-newemp-log">
                                                                                                        <h5 >
                                                                                                            Odometer Reading
                                                                                                        </h5>
                                                                                                    </div>

                                                                                                    <div class="entity-company-check-div">
                                                                                                        <div class="row">

                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Start Reading <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital initial_reading" type="number" placeholder="Start Reading" maxlength="50" name="start_reading" id="" pattern="^[0-9]*$" />
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label>Stop Reading <em class="red-star">*</em></label>
                                                                                                                <input class="form-control capital final_reading" name="stop_reading" type="number" id="" placeholder="Stop Reading" pattern="^[0-9]*$"/>
                                                                                                            </div>
                                                                                                            <div class="col-sm-3">
                                                                                                                <label> Mileage (Distance)</label>
                                                                                                                <input class="form-control capital " name="mileage" type="number" id="miileage" placeholder="Mileage" readonly/>
                                                                                                            </div>

                                                                                                        </div>

                                                                                                    </div>

<!--                                                                                                    <div class="row">-->
<!--                                                                                                        <div class="col-sm-3">-->
<!--                                                                                                            <label>Amount(US $)</label>-->
<!--                                                                                                            <input class="form-control capital amount" name="amount" type="text" id="amount" placeholder="Amount"/>-->
<!--                                                                                                        </div>-->
<!--                                                                                                    </div>-->


                                                                                                </div>
                                                                                            </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div></form>
                                                                            <div class="btn-outer text-right">
                                                                                <input type="button" value="Update" class="blue-btn" id="update_emp_log">
                                                                                <input type="button" class="clear-btn formreset2" value="Reset">
                                                                                <input type="button" class="grey-btn cancel_Popup" value="Cancel">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
<!--                                                                    <button type="button" class="btn btn-default yes-cancel">Yes</button>-->
<!--                                                                    <button type="button" class="btn btn-default no-cancel" data-dismiss="modal">No</button>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="form-hdr">
                                                    <h3>Mileage Log</h3>
                                                </div>
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                              <div class="panel panel-default">
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                          <div class="panel-body pad-none">
                                                                              <div class="accordion-grid">
                                                                                    <div class="panel-group" id="accordion">
                                                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                                            <div class="grid-outer">
                                                                                                <div class="table-responsive">
                                                                                                    <div class="apx-table listinggridDiv">
                                                                                                        <div class="table-responsive">
                                                                                                            <table id="employee_log" class="table table-bordered"></table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                  </div>
                                                                            </div>
                                                                       </div>
                                                                  </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </div>
                                <!-- Sub tabs ends-->
                            </div>

                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>

</div>
<!-- Wrapper Ends -->




<!-- Footer Ends -->
    <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/mileage_log/mileagelog.js"></script>
<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    $('.calander').datepicker({
        yearRange: '1919:2050',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    $('.calander3').datepicker({
        yearRange: '1919:2050',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });
    var date = $.datepicker.formatDate(jsDateFomat, new Date());
      $('.calander3').val(date);
      $('.calander').val(date);

    $(document).on('focus',".acquireDateclass,.expirationDateclass", function(){
        $(this).datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
    });
    $('.calander1').datepicker({
        yearRange: '1919:2050',
        viewMode: "months",
        minViewMode: "months",
        dateFormat: 'yy-mm'
    });
    $('.calander2').datepicker({
        yearRange: '1919:2050',
        viewMode: "months",
        minViewMode: "months",
        dateFormat: 'yy-mm'
    });
    $('.calander2').val(date);
    var dd=new Date();
    dd= dd.getFullYear()+'-'+'0'+(dd.getMonth()+1);
    $('.calander2').val(dd);
    $('.calander1').val(dd);

    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#comp_employeelogg",['date','comp_namee','vehicle_id'],'form',false);
        resetFormClear("#addmileagelogg",['date','comp_name','vehicle_id'],'form',false);
    });
    //editmileagelogg
    //edit_companyvehiclelog
var defaultFormData='';
    $(document).on('click','.formreset1',function () {
        resetEditForm("#edit_companyvehiclelog",[],true,defaultFormData,[]);
    });
    $(document).on('click','.formreset2',function () {
        resetEditForm("#editmileagelogg",[],true,defaultFormData,[]);
    });

    $("#edit_vehiclelog").addClass("modalScroll");
    $("#edit_companylog").addClass("modalScroll");
    $("#add_vehiclelog").addClass("modalScroll");
    $("#addmileagelog").addClass("modalScroll");
    $("body").addClass("modalopen-hide");
    // var map;
    // initMap();
    // function initMap() {
    //     map = new google.maps.Map(document.getElementById('end'), {
    //         center: {lat: -34.397, lng: 150.644},
    //         zoom: 8
    //     });
    // }

</script>



<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>