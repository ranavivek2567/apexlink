<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-2 visible-xs">

                </div>
                <div class="col-sm-3 col-xs-8">
                    <div class="logo"><img src="images/logo.png"></div>
                </div>
                <div class="col-sm-9 col-xs-2">
                    <div class="hdr-rt">
                        <!-- TOP Navigation Starts -->

                        <nav class="navbar navbar-default">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class="hidden-xs">Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018 <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a> <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a></li>
                                    <li><a href="#">Link</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i>Settings <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link disabled" href="#"><i class="fa fa-question" aria-hidden="true"></i>Help</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link disabled" href="#"><i class="fa fa-wrench" aria-hidden="true"></i>Support</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link disabled" href="#"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                    </li>
                                </ul>

                            </div><!-- /.navbar-collapse -->
                        </nav>
                        <!-- TOP Navigation Ends -->

                    </div>
                </div>
            </div>
        </div>

    </header>

    <!-- MAIN Navigation Starts -->
    <section class="main-nav">
        <nav class="navbar navbar-default">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header visible-xs">
                <a id="show" class="icon-bar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                    <a class="nav-close visible-xs" id="close" href="#"><i class="fa fa-times" aria-hidden="true"></i> </a>

                    <li><a href="spotlight.html">Spotlight</a></li>
                    <li><a href="properties.html">Properties</a></li>
                    <li><a href="people.html">People</a></li>
                    <li><a href="leases.html">Leases</a></li>
                    <li class="active"><a href="maintenance.html">Maintenance</a></li>
                    <li><a href="communication.html">Communication</a></li>
                    <li><a href="Marketing.html">Marketing</a></li>
                    <li><a href="accounting.html">Accounting</a></li>
                    <li><a href="reports.html">Reports</a></li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </nav>
    </section>
    <!-- MAIN Navigation Ends -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="content-data">
                    <div class="main-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/ticket/tickets">Tickets</a></li>
                            <li role="presentation"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                            <li role="presentation" class="active"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                            <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                            <li role="presentation"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                            <li role="presentation"><a href="/Inspection/Inspection">Inspections</a></li>
                            <li role="presentation"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>

                        </ul>
                    </div>
                    <div class="view_mainenance_section">
                        <div class="content-section">

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Purchase Order <a class="back" href="/PurchaseOrder/PurchaseOrderListing"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a></h3>
                                </div>
                                <div class="form-data detail-outer">
                                    <div class="col-sm-12" style="min-height: 30px;">
                                        <div class="col-sm-6" style="min-height: 30px;">
                                            <label class="text-right">PO Number:</label><span class="po_number">df5445d2</span>
                                        </div>
                                        <div class="col-sm-6" style="min-height: 30px;">
                                            <label class="text-right">Required By Date:</label><span class="required_by">24-09-209</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" style="min-height: 30px;">
                                        <div class="col-sm-6" style="min-height: 30px;">
                                            <label class="text-right">Current Date:</label><span class="currentdate">12-05-2015</span>
                                        </div>
                                        <div class="col-sm-6" style="min-height: 30px;">
                                            <label class="text-right">Status:</label><span class="status">Created</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Property Information</h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-12">
                                        <table id="property_table" style="width: 100%;" class="table table-hover table-dark">
                                            <thead>
                                                <tr>
                                                    <th>Property</th>
                                                    <th>Building</th>
                                                    <th>Units</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="sectionOneEdit">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>General</h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-12">
                                        <div class="grey-add-table">
                                            <div class="table-responsive">
                                                <table class="table table-dark" id="amnt_table">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Number</th>
                                                        <th scope="col">GL Account</th>
                                                        <th scope="col">Description</th>
                                                        <th scope="col">Item Amount (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                                        <th scope="col">Total (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>)</th>
                                                        <th><input type="button" class="blue-btn add_more_list" value="Add More" disabled style="cursor: not-allowed;"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td><input class="form-control" type="text" name="qty_number[]" value="1"/></td>
                                                        <td><select class="form-control" name="gl_account[]"><option value="0">Select</option></select></td>
                                                        <td><input class="form-control" type="text" name="description[]"/></td>
                                                        <td><input class="form-control" type="text" name="item_amount[]" value="0.00"/></td>
                                                        <td><input class="form-control" type="text" name="total_amount[]" value="0.00"/></td>
                                                        <td>
                                                            <a class="minus-icon remove_clone" href="javascript:void(0);" style="color:#000 !important;"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <div class="col-sm-12">
                                                    <div class="col-sm-6"></div>
                                                    <div class="col-sm-6" style="padding-left: 0px;">
                                                        <div class="col-sm-5 totallabel">Total (<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>):</div>
                                                        <div class="col-sm-6 totalamt">0.00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="sectionOneEdit">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Vendor Information</h3>
                                </div>
                                <div class="form-data">
                                    <div class="col-sm-12">
                                        <table id="vendor_table" style="width: 100%;" class="table table-hover table-dark">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="sectionOneEdit">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Vendor Instructions</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-4 col-md-3 clear">
                                            <textarea class="form-control" style="width: 100%;" name="vendor_instruction"></textarea>
                                        </div>
                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="sectionOneEdit">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>
                            </div>

                                <!-- Form Outer Ends -->
                            <div class="form-outer">
                                <div class="files_main">
                                        <div class="form-outer">
                                            <div class="form-hdr"><h3>File</h3></div>
                                            <div class="form-data">
                                                <div class="apx-table">
                                                    <div id="collapseSeventeen" class="panel-collapse in">
                                                        <div class="grid-outer">
                                                            <div class="apx-table">
                                                                <div class="table-responsive">
                                                                    <table id="viewpurchaseordertable" class="table table-bordered"></table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                            </div>
                            <!-- Form Outer Ends -->

                            <div class="form-outer">
                                <div class="form-hdr">
                                    <h3>Custom Fields</h3>
                                </div>
                                <div class="form-data">
                                    <div class="row">

                                    </div>
                                    <div class="edit-foot">
                                        <a href="javascript:;" class="sectionOneEdit">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<script>
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var currencySign = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/purchase_orders/purchase_orders.js"></script>
<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>