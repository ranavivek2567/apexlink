<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Maintenance &gt;&gt; <span>Tickets</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>MAINTENANCE</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" >New Work Order</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >New Recurring Work Order</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">View All Work Orders</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="#" id="" class="list-group-item list-group-item-success strong collapsed">View All Tickets</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Inspection</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="#" class="list-group-item list-group-item-success strong collapsed" >Lost & Found Manager</a>
                                <!-- Seven Ends-->
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" ><a href="/ticket/tickets">Tickets</a></li>
                            <li role="presentation"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                            <li role="presentation"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                            <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                            <li role="presentation" class="active"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                            <li role="presentation"><a href="/Inspection/Inspection">Inspections</a></li>
                            <li role="presentation"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>

                        </ul>
                        <div class="atoz-outer">
                            A-Z <span>All</span>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="tickets-ticket">
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Tickets</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col"><input type="checkbox"/></th>
                                                                            <th scope="col">Tenant Name</th>
                                                                            <th scope="col">Unit Number</th>
                                                                            <th scope="col">Property</th>
                                                                            <th scope="col">Ticket Number</th>
                                                                            <th scope="col">Ticket Type</th>
                                                                            <th scope="col">Category</th>
                                                                            <th scope="col">Created Date</th>
                                                                            <th scope="col">Images/Photos(s)</th>
                                                                            <th scope="col">Status</th>
                                                                            <th scope="col">Actions</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td><a class="grid-link" href="javascript:;">Stacy Clark</a></td>
                                                                            <td>BillA 1</td>
                                                                            <td><a class="grid-link" href="javascript:;">Bill Farms</a></td>
                                                                            <td>#BIBGH</td>
                                                                            <td>One Time</td>
                                                                            <td>Incident</td>
                                                                            <td>Feb11, 2019 (Mon.)</td>
                                                                            <td>Images/Photos</td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td><a class="grid-link" href="javascript:;">Stacy Clark</a></td>
                                                                            <td>BillA 1</td>
                                                                            <td><a class="grid-link" href="javascript:;">Bill Farms</a></td>
                                                                            <td>#BIBGH</td>
                                                                            <td>One Time</td>
                                                                            <td>Incident</td>
                                                                            <td>Feb11, 2019 (Mon.)</td>
                                                                            <td>Images/Photos</td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td><a class="grid-link" href="javascript:;">Stacy Clark</a></td>
                                                                            <td>BillA 1</td>
                                                                            <td><a class="grid-link" href="javascript:;">Bill Farms</a></td>
                                                                            <td>#BIBGH</td>
                                                                            <td>One Time</td>
                                                                            <td>Incident</td>
                                                                            <td>Feb11, 2019 (Mon.)</td>
                                                                            <td>Images/Photos</td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td><a class="grid-link" href="javascript:;">Stacy Clark</a></td>
                                                                            <td>BillA 1</td>
                                                                            <td><a class="grid-link" href="javascript:;">Bill Farms</a></td>
                                                                            <td>#BIBGH</td>
                                                                            <td>One Time</td>
                                                                            <td>Incident</td>
                                                                            <td>Feb11, 2019 (Mon.)</td>
                                                                            <td>Images/Photos</td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td><a class="grid-link" href="javascript:;">Stacy Clark</a></td>
                                                                            <td>BillA 1</td>
                                                                            <td><a class="grid-link" href="javascript:;">Bill Farms</a></td>
                                                                            <td>#BIBGH</td>
                                                                            <td>One Time</td>
                                                                            <td>Incident</td>
                                                                            <td>Feb11, 2019 (Mon.)</td>
                                                                            <td>Images/Photos</td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td><a class="grid-link" href="javascript:;">Stacy Clark</a></td>
                                                                            <td>BillA 1</td>
                                                                            <td><a class="grid-link" href="javascript:;">Bill Farms</a></td>
                                                                            <td>#BIBGH</td>
                                                                            <td>One Time</td>
                                                                            <td>Incident</td>
                                                                            <td>Feb11, 2019 (Mon.)</td>
                                                                            <td>Images/Photos</td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td><a class="grid-link" href="javascript:;">Stacy Clark</a></td>
                                                                            <td>BillA 1</td>
                                                                            <td><a class="grid-link" href="javascript:;">Bill Farms</a></td>
                                                                            <td>#BIBGH</td>
                                                                            <td>One Time</td>
                                                                            <td>Incident</td>
                                                                            <td>Feb11, 2019 (Mon.)</td>
                                                                            <td>Images/Photos</td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- First Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="tickets-work-order">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="check-outer">
                                                <input name="alt" type="radio">
                                                <label>One Time</label>
                                            </div>
                                            <div class="check-outer">
                                                <input name="alt" type="radio">
                                                <label>Recurring</label>
                                            </div>
                                            <div class="col-sm-4">
                                                <select class="fm-txt form-control"> <option>Active</option>
                                                    <option></option>
                                                    <option></option>
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="btn-outer text-right">
                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Work Order</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> List of Work Orders</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th><input type="checkbox"/></th>
                                                                            <th>Work Order Number</th>
                                                                            <th>Work Order Category</th>
                                                                            <th>Vendor</th>
                                                                            <th>Property</th>
                                                                            <th>Created On</th>
                                                                            <th>Caller/RequestedBy</th>
                                                                            <th>Amount(US $)</th>
                                                                            <th>Priority</th>
                                                                            <th>Status</th>
                                                                            <th>Actions</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>49js6j</td>
                                                                            <td>General Inquiry</td>
                                                                            <td><a class="grid-link" href="javascript:;">Prince Ross</a></td>
                                                                            <td><a class="grid-link" href="javascript:;">Achme 5</a></td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>49js6j</td>
                                                                            <td>General Inquiry</td>
                                                                            <td><a class="grid-link" href="javascript:;">Prince Ross</a></td>
                                                                            <td><a class="grid-link" href="javascript:;">Achme 5</a></td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>49js6j</td>
                                                                            <td>General Inquiry</td>
                                                                            <td><a class="grid-link" href="javascript:;">Prince Ross</a></td>
                                                                            <td><a class="grid-link" href="javascript:;">Achme 5</a></td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>49js6j</td>
                                                                            <td>General Inquiry</td>
                                                                            <td><a class="grid-link" href="javascript:;">Prince Ross</a></td>
                                                                            <td><a class="grid-link" href="javascript:;">Achme 5</a></td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>49js6j</td>
                                                                            <td>General Inquiry</td>
                                                                            <td><a class="grid-link" href="javascript:;">Prince Ross</a></td>
                                                                            <td><a class="grid-link" href="javascript:;">Achme 5</a></td>
                                                                            <td>11/23/2018 (Fri.)</td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>Open</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Second Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="tickets-purchase-order">
                                <div class="property-status">
                                    <div class="row">
                                        <div class="col-sm-2">

                                        </div>
                                        <div class="col-sm-10">
                                            <div class="btn-outer text-right">
                                                <button onclick="window.location.href='new-property.html'" class="blue-btn">New Purchase Order</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> List of Purchase Orders</a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="table-responsive">
                                                                    <table class="table table-hover table-dark">
                                                                        <thead>
                                                                        <tr>
                                                                            <th scope="col">PO Number</th>
                                                                            <th scope="col">Property</th>
                                                                            <th scope="col">Building</th>
                                                                            <th scope="col">Unit</th>
                                                                            <th scope="col">Required By Date</th>
                                                                            <th scope="col">Vendor</th>
                                                                            <th scope="col">Work Orders</th>
                                                                            <th scope="col">Invoice Numbers</th>
                                                                            <th scope="col">PO Status</th>
                                                                            <th scope="col">Created By</th>
                                                                            <th scope="col">Created Date</th>
                                                                            <th scope="col">Action</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>100027</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>2</td>
                                                                            <td>Feb 28, 2018 (Thu)</td>
                                                                            <td>Freeman Christi</td>
                                                                            <td>YWLWP2</td>
                                                                            <td>Mark Smith</td>
                                                                            <td>523455</td>
                                                                            <td>Created</td>
                                                                            <td>Sonny Kessebeh</td>
                                                                            <td>Feb 21, 2019 (Thu)</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>100027</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>2</td>
                                                                            <td>Feb 28, 2018 (Thu)</td>
                                                                            <td>Freeman Christi</td>
                                                                            <td>YWLWP2</td>
                                                                            <td>Mark Smith</td>
                                                                            <td>523455</td>
                                                                            <td>Created</td>
                                                                            <td>Sonny Kessebeh</td>
                                                                            <td>Feb 21, 2019 (Thu)</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>100027</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>2</td>
                                                                            <td>Feb 28, 2018 (Thu)</td>
                                                                            <td>Freeman Christi</td>
                                                                            <td>YWLWP2</td>
                                                                            <td>Mark Smith</td>
                                                                            <td>523455</td>
                                                                            <td>Created</td>
                                                                            <td>Sonny Kessebeh</td>
                                                                            <td>Feb 21, 2019 (Thu)</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>100027</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>2</td>
                                                                            <td>Feb 28, 2018 (Thu)</td>
                                                                            <td>Freeman Christi</td>
                                                                            <td>YWLWP2</td>
                                                                            <td>Mark Smith</td>
                                                                            <td>523455</td>
                                                                            <td>Created</td>
                                                                            <td>Sonny Kessebeh</td>
                                                                            <td>Feb 21, 2019 (Thu)</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><input type="checkbox"/></td>
                                                                            <td>100027</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>Yellow Stone</td>
                                                                            <td>2</td>
                                                                            <td>Feb 28, 2018 (Thu)</td>
                                                                            <td>Freeman Christi</td>
                                                                            <td>YWLWP2</td>
                                                                            <td>Mark Smith</td>
                                                                            <td>523455</td>
                                                                            <td>Created</td>
                                                                            <td>Sonny Kessebeh</td>
                                                                            <td>Feb 21, 2019 (Thu)</td>
                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Third Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="tickets-lost-found">
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#lostfound-one" aria-controls="home" role="tab" data-toggle="tab">Dashboard</a></li>
                                        <li role="presentation"><a href="#lostfound-two" aria-controls="profile" role="tab" data-toggle="tab">Items</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="lostfound-one">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Property</label>
                                                        <select class="fm-txt form-control">
                                                            <option></option>
                                                            <option></option>
                                                            <option></option>
                                                        </select>

                                                    </div>
                                                    <div class="col-sm-3 row">
                                                        <label>&nbsp;</label>
                                                        <button onclick="window.location.href='new-property.html'" class="blue-btn">Add Lost</button>
                                                        <button onclick="window.location.href='new-property.html'" class="blue-btn">Add Found</button>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="lost-found-items">
                                                <div class="lost-found-columns">
                                                    <span class="count">0</span>
                                                    <label>Lost Items</label>
                                                    <span class="item-btn">
                                          <button class="white-btn">Show all Items</button>
                                          </span>
                                                </div>
                                                <div class="lost-found-columns">
                                                    <span class="count">0</span>
                                                    <label>Found Items</label>
                                                    <span class="item-btn">
                                          <button class="white-btn">Show all Items</button>
                                          </span>
                                                </div>
                                                <div class="lost-found-columns">
                                                    <span class="count">25</span>
                                                    <label>Claimed Items</label>
                                                    <span class="item-btn">
                                          <button class="white-btn">Show all Items</button>
                                          </span>
                                                </div>
                                                <div class="lost-found-columns">
                                                    <span class="count">71</span>
                                                    <label>Unclaimed Items</label>
                                                    <span class="item-btn">
                                          <button class="white-btn">Show all Items</button>
                                          </span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Lost Items</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Date</th>
                                                                                                <th scope="col">Item Type</th>
                                                                                                <th scope="col">Property</th>
                                                                                                <th scope="col">More</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Found Items</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Date</th>
                                                                                                <th scope="col">Item Type</th>
                                                                                                <th scope="col">Property</th>
                                                                                                <th scope="col">More</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="lostfound-two">
                                            <div class="sub-tabs">
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation" class="active"><a href="#lostfound-sub-one" aria-controls="home" role="tab" data-toggle="tab">Lost Items</a></li>
                                                    <li role="presentation"><a href="#lostfound-sub-two" aria-controls="profile" role="tab" data-toggle="tab">Found Items</a></li>
                                                    <li role="presentation"><a href="#lostfound-sub-three" aria-controls="home" role="tab" data-toggle="tab">Claimed Items</a></li>
                                                    <li role="presentation"><a href="#lostfound-sub-four" aria-controls="profile" role="tab" data-toggle="tab">Unclaimed Items</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div role="tabpanel" class="tab-pane active" id="lostfound-sub-one">
                                                        <div class="property-status">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select class="fm-txt form-control"> <option>Property</option>
                                                                        <option></option>
                                                                        <option></option>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select class="fm-txt form-control"> <option>All</option>
                                                                        <option></option>
                                                                        <option></option>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <label>&nbsp;</label>
                                                                    <input type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <div class="btn-outer">
                                                                        <button class="blue-btn">Search</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="accordion-grid">
                                                            <div class="accordion-outer">
                                                                <div class="bs-example">
                                                                    <div class="panel-group" id="accordion">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Lost Items</a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-hover table-dark">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th scope="col">Item Number</th>
                                                                                                    <th scope="col">Description</th>
                                                                                                    <th scope="col">Category</th>
                                                                                                    <th scope="col">Lost Date</th>
                                                                                                    <th scope="col">Expiration Date</th>
                                                                                                    <th scope="col">Status</th>
                                                                                                    <th scope="col">Full View</th>
                                                                                                </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane active" id="lostfound-sub-two">
                                                        <div class="property-status">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select class="fm-txt form-control"> <option>Property</option>
                                                                        <option></option>
                                                                        <option></option>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select class="fm-txt form-control"> <option>All</option>
                                                                        <option></option>
                                                                        <option></option>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <label>&nbsp;</label>
                                                                    <input type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <div class="btn-outer">
                                                                        <button class="blue-btn">Search</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="accordion-grid">
                                                            <div class="accordion-outer">
                                                                <div class="bs-example">
                                                                    <div class="panel-group" id="accordion">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Found Items</a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-hover table-dark">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th scope="col">Item Number</th>
                                                                                                    <th scope="col">Description</th>
                                                                                                    <th scope="col">Category</th>
                                                                                                    <th scope="col">Lost Date</th>
                                                                                                    <th scope="col">Expiration Date</th>
                                                                                                    <th scope="col">Status</th>
                                                                                                    <th scope="col">Full View</th>
                                                                                                </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane active" id="lostfound-sub-three">
                                                        <div class="property-status">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select class="fm-txt form-control"> <option>Property</option>
                                                                        <option></option>
                                                                        <option></option>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select class="fm-txt form-control"> <option>All</option>
                                                                        <option></option>
                                                                        <option></option>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <label>&nbsp;</label>
                                                                    <input type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <div class="btn-outer">
                                                                        <button class="blue-btn">Search</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="accordion-grid">
                                                            <div class="accordion-outer">
                                                                <div class="bs-example">
                                                                    <div class="panel-group" id="accordion">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Claimed Items</a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-hover table-dark">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th scope="col">Item Number</th>
                                                                                                    <th scope="col">Description</th>
                                                                                                    <th scope="col">Category</th>
                                                                                                    <th scope="col">Lost Date</th>
                                                                                                    <th scope="col">Expiration Date</th>
                                                                                                    <th scope="col">Status</th>
                                                                                                    <th scope="col">Full View</th>
                                                                                                </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane active" id="lostfound-sub-four">
                                                        <div class="property-status">
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select class="fm-txt form-control"> <option>Property</option>
                                                                        <option></option>
                                                                        <option></option>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <select class="fm-txt form-control"> <option>All</option>
                                                                        <option></option>
                                                                        <option></option>
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <label>&nbsp;</label>
                                                                    <input type="text" placeholder="Search by item #, serial#, Desc, Brand Name and other Details" class="form-control"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>&nbsp;</label>
                                                                    <div class="btn-outer">
                                                                        <button class="blue-btn">Search</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="accordion-grid">
                                                            <div class="accordion-outer">
                                                                <div class="bs-example">
                                                                    <div class="panel-group" id="accordion">
                                                                        <div class="panel panel-default">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">
                                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> List of Unclaimed Items</a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                <div class="panel-body pad-none">
                                                                                    <div class="grid-outer">
                                                                                        <div class="table-responsive">
                                                                                            <table class="table table-hover table-dark">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th scope="col">Item Number</th>
                                                                                                    <th scope="col">Description</th>
                                                                                                    <th scope="col">Category</th>
                                                                                                    <th scope="col">Lost Date</th>
                                                                                                    <th scope="col">Expiration Date</th>
                                                                                                    <th scope="col">Status</th>
                                                                                                    <th scope="col">Full View</th>
                                                                                                </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>l-00421</td>
                                                                                                    <td>Black Phone</td>
                                                                                                    <td>Electronic Items</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>Dec 27, 2018 (Thu) 12:30:53 PM</td>
                                                                                                    <td>InActive</td>
                                                                                                    <td><i class="fa fa-arrows-alt" aria-hidden="true"></i></td>
                                                                                                </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Fourth Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="tickets-mileage">
                                <!-- Sub Tabs Starts-->
                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#company-vehicle" aria-controls="home" role="tab" data-toggle="tab">Company Vehicle Mileage Log</a></li>
                                        <li role="presentation"><a href="#employee-vehicle" aria-controls="profile" role="tab" data-toggle="tab">Employee Vehicle Mileage Log</a></li>

                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="company-vehicle">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Select/Add Employee Vehicle <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span></label>
                                                        <input type="text" class="form-control" placeholder=" Click here to select" />
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="check-outer">
                                                            <input name="alt" type="radio">
                                                            <label>Weekly/Bi-Weekly/Monthly</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input name="alt" type="radio">
                                                            <label>By Date Range</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>&nbsp;</label>
                                                        <select class="form-control">
                                                            <option>Select</option>
                                                            <option>Option</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>&nbsp;</label>
                                                        <div class="btn-outer">
                                                            <button onclick="window.location.href='new-property.html'" class="blue-btn btn-block">Search</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Vehicle Mileage Log</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-dark">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th scope="col">Vehicle Name</th>
                                                                                        <th scope="col">Date</th>
                                                                                        <th scope="col">Driver</th>
                                                                                        <th scope="col">Journey Details(To - From)</th>
                                                                                        <th scope="col">Odometer Reading(Start - Stop Reading)</th>
                                                                                        <th scope="col">Number of Miles</th>
                                                                                        <th scope="col">Actions</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td>Oct 31, 2018 (Wed)</td>
                                                                                        <td>James Smith</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>42</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td>Oct 31, 2018 (Wed)</td>
                                                                                        <td>James Smith</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>42</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td>Oct 31, 2018 (Wed)</td>
                                                                                        <td>James Smith</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>42</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td>Oct 31, 2018 (Wed)</td>
                                                                                        <td>James Smith</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>42</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td></td>
                                                                                        <td>Oct 31, 2018 (Wed)</td>
                                                                                        <td>James Smith</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>42</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>

                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Regular Rent Ends -->
                                        <div role="tabpanel" class="tab-pane" id="employee-vehicle">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <label>Select/Add Employee Vehicle <span><i class="fa fa-plus-circle" aria-hidden="true"></i></span></label>
                                                        <input type="text" class="form-control" placeholder=" Click here to select" />
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="check-outer">
                                                            <input name="alt" type="radio">
                                                            <label>Weekly/Bi-Weekly/Monthly</label>
                                                        </div>
                                                        <div class="check-outer">
                                                            <input name="alt" type="radio">
                                                            <label>By Date Range</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>&nbsp;</label>
                                                        <select class="form-control">
                                                            <option>Select</option>
                                                            <option>Option</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <label>&nbsp;</label>
                                                        <div class="btn-outer">
                                                            <button onclick="window.location.href='new-property.html'" class="blue-btn btn-block">Search</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Employee Mileage Log</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-dark">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th scope="col">Employee Full Name</th>
                                                                                        <th scope="col">Employee ID</th>
                                                                                        <th scope="col">Vehicle</th>
                                                                                        <th scope="col">Journey Details(To - From)</th>
                                                                                        <th scope="col">Date</th>
                                                                                        <th scope="col">Odometer Reading(Start - Stop Reading)</th>
                                                                                        <th scope="col">Number of Miles</th>
                                                                                        <th scope="col">Actions</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td>James Smith</td>
                                                                                        <td>30</td>
                                                                                        <td>Car</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>Nov 15, 2018 (Thu.)</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>2048</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>James Smith</td>
                                                                                        <td>30</td>
                                                                                        <td>Car</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>Nov 15, 2018 (Thu.)</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>2048</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>James Smith</td>
                                                                                        <td>30</td>
                                                                                        <td>Car</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>Nov 15, 2018 (Thu.)</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>2048</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>James Smith</td>
                                                                                        <td>30</td>
                                                                                        <td>Car</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>Nov 15, 2018 (Thu.)</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>2048</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>James Smith</td>
                                                                                        <td>30</td>
                                                                                        <td>Car</td>
                                                                                        <td>2360 Egderton Street, Little Canada, MN, USA - 23 Lake Street South, Forest lake, MN, USA</td>
                                                                                        <td>Nov 15, 2018 (Thu.)</td>
                                                                                        <td>12345 - 12499</td>
                                                                                        <td>2048</td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Sub tabs ends-->
                            </div>
                            <!-- Fifth Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="tickets-inspection">
                                <div class="heading-above-btn">
                                    <button class="blue-btn pull-right">New Inspection</button>
                                </div>
                                <div class="panel panel-default clear">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion">Inspection</a>
                                        </h4>
                                    </div>
                                    <div>
                                        <div class="panel-body">
                                            <div class="form-outer">
                                                <div class="property-status">
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                            <label>Property</label>
                                                            <select class="fm-txt form-control"> <option>Active</option>
                                                                <option></option>
                                                                <option></option>
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>Building</label>
                                                            <select class="fm-txt form-control"> <option>This Property has no Building</option>
                                                                <option></option>
                                                                <option></option>
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <label>Unit</label>
                                                            <select class="fm-txt form-control"> <option>This Building has no Unit</option>
                                                                <option></option>
                                                                <option></option>
                                                                <option></option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="accordion-grid">
                                                    <div class="accordion-outer">
                                                        <div class="bs-example">
                                                            <div class="panel-group" id="accordion">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> List of Inspections</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                                                        <div class="panel-body pad-none">
                                                                            <div class="grid-outer">
                                                                                <div class="table-responsive">
                                                                                    <table class="table table-hover table-dark">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th scope="col">Serial Number</th>
                                                                                            <th scope="col">Date of Inspection</th>
                                                                                            <th scope="col">Property Name</th>
                                                                                            <th scope="col">Property Address</th>
                                                                                            <th scope="col">Building</th>
                                                                                            <th scope="col">Unit Number</th>
                                                                                            <th scope="col">Inspection Name</th>
                                                                                            <th scope="col">Inspection Type</th>
                                                                                            <th scope="col">Action</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td>1</td>
                                                                                            <td>Dec 15 (Sat.)</td>
                                                                                            <td><a class="grid-link" href="javascript:;">Short Property</a></td>
                                                                                            <td></td>
                                                                                            <td>N/A</td>
                                                                                            <td>N/A</td>
                                                                                            <td>Monthly Inspection</td>
                                                                                            <td>Exterior</td>
                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>1</td>
                                                                                            <td>Dec 15 (Sat.)</td>
                                                                                            <td><a class="grid-link" href="javascript:;">Short Property</a></td>
                                                                                            <td></td>
                                                                                            <td>N/A</td>
                                                                                            <td>N/A</td>
                                                                                            <td>Monthly Inspection</td>
                                                                                            <td>Exterior</td>
                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>1</td>
                                                                                            <td>Dec 15 (Sat.)</td>
                                                                                            <td><a class="grid-link" href="javascript:;">Short Property</a></td>
                                                                                            <td></td>
                                                                                            <td>N/A</td>
                                                                                            <td>N/A</td>
                                                                                            <td>Monthly Inspection</td>
                                                                                            <td>Exterior</td>
                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>1</td>
                                                                                            <td>Dec 15 (Sat.)</td>
                                                                                            <td><a class="grid-link" href="javascript:;">Short Property</a></td>
                                                                                            <td></td>
                                                                                            <td>N/A</td>
                                                                                            <td>N/A</td>
                                                                                            <td>Monthly Inspection</td>
                                                                                            <td>Exterior</td>
                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>1</td>
                                                                                            <td>Dec 15 (Sat.)</td>
                                                                                            <td><a class="grid-link" href="javascript:;">Short Property</a></td>
                                                                                            <td></td>
                                                                                            <td>N/A</td>
                                                                                            <td>N/A</td>
                                                                                            <td>Monthly Inspection</td>
                                                                                            <td>Exterior</td>
                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>1</td>
                                                                                            <td>Dec 15 (Sat.)</td>
                                                                                            <td><a class="grid-link" href="javascript:;">Short Property</a></td>
                                                                                            <td></td>
                                                                                            <td>N/A</td>
                                                                                            <td>N/A</td>
                                                                                            <td>Monthly Inspection</td>
                                                                                            <td>Exterior</td>
                                                                                            <td><select class="form-control"><option>Select</option></select></td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <!-- Sixth Tab Ends-->

                            <div role="tabpanel" class="tab-pane" id="tickets-tracker">

                                <div class="sub-tabs">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#ticket-inventory" aria-controls="home" role="tab" data-toggle="tab">Add Item to Inventory</a></li>
                                        <li role="presentation"><a href="#ticket-inventory2" aria-controls="profile" role="tab" data-toggle="tab">Inventory Setup</a></li>

                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="ticket-inventory">

                                            <div class="panel panel-default clear">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion">New Inventory</a>
                                                    </h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>inventory Photo/Image</label>
                                                                <div class="upload-logo">
                                                                    <div class="img-outer"></div>
                                                                    <a href="javascript:;"><i class="fa fa-pencil-square" aria-hidden="true"></i>Choose Image</a>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Category <em class="red-star">*</em> <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Subcategory <em class="red-star">*</em> <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Brand <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Supplier <em class="red-star">*</em> <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Description</label>
                                                                <textarea class="form-control"></textarea>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Number of Item Purchased <em class="red-star">*</em> </label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Size</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Volume<a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Cost per Item (US$)</label>
                                                                <input class="form-control add-input" type="text">
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Purchase Cost (US$)</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Purchase Date</label>
                                                                <input class="form-control add-input" type="text">
                                                                <a class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                            </div>

                                                            <div class="col-sm-2">
                                                                <label>Expected Delivery Date</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Stock Reorder Level</label>
                                                                <input class="form-control" type="text">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label>Property <em class="red-star">*</em></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Building <em class="red-star">*</em></label>
                                                                <select class="form-control"></select>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>Sublocation <a class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                <select class="form-control"></select>
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion">Custom Fields</a>
                                                            </h4>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-sm-2">
                                                                        <label>No Custom Fields</label>
                                                                    </div>
                                                                    <div class="col-sm-2 text-right pull-right">
                                                                        <label></label>
                                                                        <button class="blue-btn">New Custom Field</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="heading-above-btn">
                                                            <div class="btn-outer">
                                                                <button class="blue-btn">Save & Close</button>
                                                                <button class="grey-btn">Save & Add Another Field</button>
                                                                <button class="grey-btn">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-down"></span> Inventory on Hand</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive">
                                                                                        <table class="table table-hover table-dark">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Property</th>
                                                                                                <th scope="col">Building</th>
                                                                                                <th scope="col">Inventory Sublocation</th>
                                                                                                <th scope="col">Category</th>
                                                                                                <th scope="col">Subcategory</th>
                                                                                                <th scope="col">Quantity</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>First Floor</td>
                                                                                                <td>General Supplies</td>
                                                                                                <td>Fire Extinguisher</td>
                                                                                                <td>6</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>First Floor</td>
                                                                                                <td>General Supplies</td>
                                                                                                <td>Fire Extinguisher</td>
                                                                                                <td>6</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>Yellow Stone</td>
                                                                                                <td>First Floor</td>
                                                                                                <td>General Supplies</td>
                                                                                                <td>Fire Extinguisher</td>
                                                                                                <td>6</td>
                                                                                                <td><select class="form-control"><option>Select</option></select></td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                        <div role="tabpanel" class="tab-pane active" id="ticket-inventory2">
                                            <div class="property-status">
                                                <div class="row">
                                                    <div class="col-sm-2">
                                                        <select class="fm-txt form-control"> <option>All</option>
                                                            <option></option>
                                                            <option></option>
                                                            <option></option>
                                                        </select>

                                                    </div>
                                                    <div class="col-sm-10">
                                                        <div class="btn-outer text-right">
                                                            <button onclick="window.location.href='new-property.html'" class="blue-btn">Search</button>
                                                            <button onclick="window.location.href='new-property.html'" class="blue-btn">Download Sample</button>
                                                            <button onclick="window.location.href='new-property.html'" class="blue-btn">Import Sample</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="pull-right glyphicon glyphicon-menu-up"></span> Inventory List</a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <div class="table-responsive">
                                                                                <table class="table table-hover table-dark">
                                                                                    <thead>
                                                                                    <tr>
                                                                                        <th><input type="checkbox"></th>
                                                                                        <th>Item #/Code</th>
                                                                                        <th>Category</th>
                                                                                        <th>Subcategory</th>
                                                                                        <th>Description</th>
                                                                                        <th>Brand</th>
                                                                                        <th>Supplier</th>
                                                                                        <th>Quantity</th>
                                                                                        <th>Cost/Unit</th>
                                                                                        <th>Low Stock Record</th>
                                                                                        <th>Property</th>
                                                                                        <th>Building</th>
                                                                                        <th>Inventory Sublocations</th>
                                                                                        <th>Upload Photo</th>
                                                                                        <th>Actions</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    <tr>
                                                                                        <td><input type="checkbox"></td>
                                                                                        <td><a class="grid-link" href="javascript:;">Item #/Code</a></td>
                                                                                        <td>Fire Extinguisher</td>
                                                                                        <td>Prince Ross</td>
                                                                                        <td>main floor fire extinguisher</td>
                                                                                        <td>Fire Alert</td>
                                                                                        <td>Amazon</td>
                                                                                        <td>6</td>
                                                                                        <td>55.00</td>
                                                                                        <td>2</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>First Floor</td>
                                                                                        <td><a class="grid-link" href="javascript:;">No Images Uploaded</a></td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><input type="checkbox"></td>
                                                                                        <td><a class="grid-link" href="javascript:;">Item #/Code</a></td>
                                                                                        <td>Fire Extinguisher</td>
                                                                                        <td>Prince Ross</td>
                                                                                        <td>main floor fire extinguisher</td>
                                                                                        <td>Fire Alert</td>
                                                                                        <td>Amazon</td>
                                                                                        <td>6</td>
                                                                                        <td>55.00</td>
                                                                                        <td>2</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>First Floor</td>
                                                                                        <td><a class="grid-link" href="javascript:;">No Images Uploaded</a></td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><input type="checkbox"></td>
                                                                                        <td><a class="grid-link" href="javascript:;">Item #/Code</a></td>
                                                                                        <td>Fire Extinguisher</td>
                                                                                        <td>Prince Ross</td>
                                                                                        <td>main floor fire extinguisher</td>
                                                                                        <td>Fire Alert</td>
                                                                                        <td>Amazon</td>
                                                                                        <td>6</td>
                                                                                        <td>55.00</td>
                                                                                        <td>2</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>First Floor</td>
                                                                                        <td><a class="grid-link" href="javascript:;">No Images Uploaded</a></td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><input type="checkbox"></td>
                                                                                        <td><a class="grid-link" href="javascript:;">Item #/Code</a></td>
                                                                                        <td>Fire Extinguisher</td>
                                                                                        <td>Prince Ross</td>
                                                                                        <td>main floor fire extinguisher</td>
                                                                                        <td>Fire Alert</td>
                                                                                        <td>Amazon</td>
                                                                                        <td>6</td>
                                                                                        <td>55.00</td>
                                                                                        <td>2</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>First Floor</td>
                                                                                        <td><a class="grid-link" href="javascript:;">No Images Uploaded</a></td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><input type="checkbox"></td>
                                                                                        <td><a class="grid-link" href="javascript:;">Item #/Code</a></td>
                                                                                        <td>Fire Extinguisher</td>
                                                                                        <td>Prince Ross</td>
                                                                                        <td>main floor fire extinguisher</td>
                                                                                        <td>Fire Alert</td>
                                                                                        <td>Amazon</td>
                                                                                        <td>6</td>
                                                                                        <td>55.00</td>
                                                                                        <td>2</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>Yellow Stone</td>
                                                                                        <td>First Floor</td>
                                                                                        <td><a class="grid-link" href="javascript:;">No Images Uploaded</a></td>
                                                                                        <td><select class="form-control"><option>Select</option></select></td>
                                                                                    </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- Seventh Tab Ends-->
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>

</div>
<!-- Wrapper Ends -->



<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>


<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>