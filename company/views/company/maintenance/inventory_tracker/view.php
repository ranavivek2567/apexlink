<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Maintenance &gt;&gt; <span>Inventory</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>MAINTENANCE</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Work Order</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Recurring Work Order</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="/WorkOrder/WorkOrders" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">View All Work Orders</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="/ticket/tickets" id="" class="list-group-item list-group-item-success strong collapsed">View All Tickets</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="/Inspection/Inspection" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Inspection</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="/LostAndFound/LostAndFound" class="list-group-item list-group-item-success strong collapsed" >Lost & Found Manager</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/MileageLog/MileageLog" class="list-group-item list-group-item-success strong collapsed" >Mileage Log</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/Maintenance/Inventory" class="list-group-item list-group-item-success strong collapsed" >Inventory Tracker</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/PurchaseOrder/PurchaseOrder" class="list-group-item list-group-item-success strong collapsed" >New Purchase Order</a>
                                <!-- Seven Ends-->
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/ticket/tickets">Tickets</a></li>
                            <li role="presentation"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                            <li role="presentation"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                            <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                            <li role="presentation"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                            <li role="presentation"><a href="/Inspection/Inspection">Inspections</a></li>
                            <li role="presentation" class="active"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>


                        </ul>
                      
                        <!-- Tab panes -->

                        <form id="addInventory">

                        <div class="tab-content">
                        <input class="form-control type" type="hidden" name='type' value="add">
                        <input class="form-control inventory_id" type="hidden" name='inventory_id'>

                            <div role="tabpanel" class="tab-pane active" id="tickets-tracker">

                                <div class="main-tabs">
                                    
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                         <li role="presentation" class="active"><a href="/Maintenance/Inventory">Add Item to Inventory</a></li>
                                        <li role="presentation"><a href="/Maintenance/InventorySetup">Inventory Setup</a></li>

                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="ticket-inventory">

                                            <div class="panel panel-default clear">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion">New Inventory</a>
                                                    </h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">

                                                        <div class="form-outer">
                                     <div class="col-sm-12">
                                       
                                            <div class="row">
                                                <div class="form-outer contactTenant" style="display:none">
                                                    <div class="col-sm-3 col-md-3">
                                                        <div class=''>
                                                            <strong class='font-weight-bold'>Contact for this tenant <em class="red-star">*</em></strong>
                                                            <input class="form-control capital" type="text" name='contactTenant'>
                                                        </div>
                                                        <span class="ffirst_nameErr error red-star"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="upload-logo clearfix">
                                                <div class="inventory_image img-outer"><img src="<?php echo COMPANY_SUBDOMAIN_URL ?>/images/dummy-img.jpg"></div>

                                                <a href="javascript:;">
                                                    <i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Image</a>
                                                <span>(Maximum File Size Limit: 1MB)</span>
                                            </div>
                                            <div class="image-editor">
                                                <input type="file" class="cropit-image-input" name="tenant_image">
                                                <div class='cropItData' style="display: none;">
                                                    <span class="closeimagepopupicon">X</span>
                                                    <div class="cropit-preview"></div>
                                                    <div class="image-size-label">Resize image</div>
                                                    <input type="range" class="cropit-image-zoom-input">
                                                    <input type="hidden" name="image-data" class="hidden-image-data"/>
                                                    <input type="button" class="export" value="Done"
                                                           data-val="tenant_image">
                                                </div>
                                            </div>
                                        </div>

                                      
                                                        
                                 <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Category <em class="red-star">*</em> <a class="pop-add-icon" data-popup = "inventoryCategoryPopup" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                 <select class="form-control inventoryCategory" name="inventoryCategory" id="inventoryCategory">
                                      <option value="">Select</option> 
                                                                     
                                   </select>
                                    <div class="add-popup inventoryCategoryPopup" id='inventoryCategoryPopup'>
                                                    <h4>Add Category</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Category<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20"  id='data-inventoryCategory'/>
                                                                <span class="customErrorCategory required"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='addNewCategory' class="blue-btn addSingleData" value="Save" data-table='company_maintenance_subcategory' data-cell="category" data-value="data-inventoryCategory" data-error="customErrorCategory" data-select = "inventoryCategory" data-hide="inventoryCategoryPopup"/>
                                                                <button type="button" class="clear-btn ">Clear</button>
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                       </div>


                                 <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Subcategory <em class="red-star">*</em> <a class="pop-add-icon" data-popup = "inventorysubCategoryPopup" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                 <select class="form-control subcategory" name="subcategory" id="subcategory">
                                      <option value="">Select</option>  
                                                                    
                                   </select>
                                    <div class="add-popup subcategoryPopup" id='inventorysubCategoryPopup'>
                                                    <h4>Add Subcategory</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Subcategory<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20"  id='data-subcategory'/>
                                                                <span class="customErrorCategory required"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='addNewSubcategory' class="blue-btn addSubcategoryData" value="Save" data-table='company_maintenance_subcategory' data-cell="sub_category" data-extracell1 = "parent_id" data-value="data-subcategory" data-error="customErrorCategory" data-select = "subcategory" data-hide="subcategoryPopup"/>
                                                                <button type="button" class="clear-btn">Clear</button>
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                       </div>



                        <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Brand <em class="red-star">*</em> <a class="pop-add-icon" data-popup = "inventoryBrandPopup" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                 <select class="form-control brand" name="brand" id="brand">
                                      <option value="">Select</option>  
                                                             
                                   </select>
                                    <div class="add-popup brandPopup" id='inventoryBrandPopup'>
                                                    <h4>Brand</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Add Brand<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20"  id='data-category'/>
                                                                <span class="customErrorCategory required"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='addNewBrand' class="blue-btn addbrandData" value="Save" data-table='company_maintenance_brand' data-cell="brand" data-value="data-category" data-error="customErrorCategory" data-extracell1 = "category_id" data-extracell2 = "sub_category_id" data-select = "brand" data-hide="brandPopup"/>
                                                                <button type="button" class="clear-btn">Clear</button>
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                       </div>



                        <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Supplier <em class="red-star">*</em> <a class="pop-add-icon" data-popup = "inventorySupplierPopup" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                 <select class="form-control supplier" name="supplier" id="supplier">
                                      <option value="">Select</option>                           
                                   </select>
                                    <div class="add-popup categoryPopup" id='inventorySupplierPopup'>
                                                    <h4>Supplier</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Add Supplier<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20"  id='data-supplier'/>
                                                                <span class="customErrorCategory required"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='addNewSupplier' class="blue-btn addSupplierData" value="Save" data-table='company_maintenance_supplier' data-cell="supplier" data-extracell1 = "category_id" data-extracell2 = "sub_category_id" data-extracell3 = "brand_id"  data-value="data-supplier" data-error="customErrorCategory" data-select = "supplier" data-hide="categoryPopup"/>
                                                                <button type="button" class="clear-btn">Clear</button>
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                       </div>



                                                            <div class="col-md-2 col-sm-4">
                                                                <label>Description</label>
                                                                <textarea class="form-control description capital" name="description"></textarea>
                                                            </div>
                                                            <div class="col-md-2 col-sm-4">
                                                                <label>Number of Item Purchased <em class="red-star">*</em> </label>
                                                                <input class="form-control item_purchased " type="text" name="nameOfItemPurchased">
                                                            </div>
                                                            <div class="col-md-2 col-sm-4">
                                                                <label>Size</label>
                                                                <input class="form-control size" type="text" name="size">
                                                            </div>


                         <div class="col-xs-12 col-sm-4 col-md-3">
                                    <label>Volume <em class="red-star">*</em> <a class="pop-add-icon" data-popup = "inventoryVolumePopup" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                 <select class="form-control volume volume_id" name="volume" id="volume">
                                   </select>
                                    <div class="add-popup categoryPopup" id='inventoryVolumePopup'>
                                                    <h4>Volume</h4>
                                                    <div class="add-popup-body">

                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <label>Add Volume<em class="red-star">*</em></label>
                                                                <input class="form-control customValidatePortfoio capital" type="text" data_required="true" data_max="20"  id='data-volume'/>
                                                                <span class="customErrorCategory required"></span>
                                                            </div>
                                                      
                                                            <div class="btn-outer text-right">
                                                                <input type="button" id='addNewVolume' class="blue-btn addSingleData" value="Save" data-table='maintenance_inventory_volume' data-cell="volume" data-value="data-volume" data-error="customErrorCategory" data-select = "volume" data-hide="categoryPopup"/>
                                                                <button type="button" class="clear-btn">Clear</button>
                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                       </div>



                                                            <div class="col-md-2 col-sm-4">
                                                                <label>Cost per Item (US$)</label>
                                                                <input class="form-control add-input cost_per_item" type="text" name="costPerItem">
                                                               
                                                            </div>
                                                            <div class="col-md-2 col-sm-4">
                                                                <label>Purchase Cost (US$)</label>
                                                                <input class="form-control purchase_cost" type="text" name="purchaseCost" readonly="readonly">
                                                            </div>
                                                            <div class="col-md-2 col-sm-4">
                                                                <label>Purchase Date</label>
                                                                <input class="form-control calander purchase_date" type="text" name="purchaseDate">
                                                             
                                                            </div>

                                                            <div class="col-md-2 col-sm-4">
                                                                <label>Expected Delivery Date</label>
                                                                <input class="form-control calander expected_deliver_date" type="text" name="expectedDeliveryDate">
                                                            </div>
                                                            <div class="col-md-2 col-sm-4">
                                                                <label>Stock Reorder Level</label>
                                                                <input class="form-control stock_reorder_level" type="text" name="stockRecorderLevel">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label>Property <em class="red-star">*</em></label>
                                                                <select class="form-control property" name="property">
                                                                    

                                                                </select>
                                                            </div>
                                                            <div class="col-md-2 col-sm-4">
                                                                <label>Building <em class="red-star">*</em></label>
                                                                <select class="form-control building" name="building"></select>
                                                            </div>



                                                <div class="col-xs-12 col-sm-4 col-md-3">
                                                    <label>Sublocation <em class="red-star">*</em> <a class="pop-add-icon" data-popup = "inventorySublocationPopup" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                    <select class="form-control inventorySublocation sub_location_id" name="inventorySublocation" id="inventorySublocation">
                                                      
                                                    </select>
                                                    <div class="add-popup sublocationPopup" id='inventorySublocationPopup'>
                                                                    <h4>Sublocation</h4>
                                                                    <div class="add-popup-body">

                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <label>Add Sublocation<em class="red-star">*</em></label>
                                                                                <input class="form-control customValidatePortfoio" type="text" data_required="true" data_max="20"  id='data-sublocation'/>
                                                                                <span class="customErrorCategory required"></span>
                                                                            </div>
                                                                      
                                                                            <div class="btn-outer text-right">
                                                                                <input type="button" id='addNewSublocation' class="blue-btn addSingleData" value="Save" data-table='maintenance_inventory_sublocation' data-cell="sublocation" data-value="data-sublocation" data-error="customErrorCategory" data-select = "inventorySublocation" data-hide="sublocationPopup"/>
                                                                                <button type="button" class="clear-btn">Clear</button>
                                                                                <input type="button" class="grey-btn cancelPopup" value='Cancel' />
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                       </div>

                                                        </div>

                                                    </div>

                                                    <div class="item_codes"></div>

                                                    <div class="panel panel-default">
                                                        <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3>Custom Fields</h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row ">
                                                <div class="clearfix "></div>
                                            </div>
                                            <div class="row">
                                                <div class="form-outer col-sm-6 col-md-6 ">
                                                    <div class="custom_field_html"></div>
                                                    <div class="clearfix "></div>
                                                </div>
                                            </div>
                                            <button type="button" class="pull-right blue-btn" id="add_custom_field" data-toggle="modal"
                                                    data-backdrop="static" data-target="#myModal" class="blue-btn">Add
                                                Custom Field
                                            </button>
                                        </div>
                                    </div>

                                                        <div class="heading-above-btn">
                                                            <div class="btn-outer text-right">
                                                                <button class="blue-btn">Save & Close</button>
                                                                <button class="grey-btn">Save & Add Another Field</button>
                                                                <input type="button" class="clear-btn clearFormReset" value="Clear"/>
                                                                <input type='button'class="grey-btn inventory_cancel" value="Cancel">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Inventory on Hand</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive inventoryTableOne">
                                                                                        <table class="table table-hover table-dark" id="inventory_hand_table">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Property</th>
                                                                                                <th scope="col">Building</th>
                                                                                                <th scope="col">Inventory Sublocation</th>
                                                                                                <th scope="col">Category</th>
                                                                                                <th scope="col">Subcategory</th>
                                                                                                <th scope="col">Quantity</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                            
                                    </div>

                                </div>

                            </div>
                            <!-- Seventh Tab Ends-->
                        </div>
                       </form>


                      

                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>

</div>
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                               <input type="hidden" name="module" id="customFieldModule" value="inventory">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name"
                                               name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text"
                                               name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">
                                <input type="submit" name="submit" class="blue-btn" id='saveCustomField' value="Save">
                                <input type="button" class="clear-btn" id='ClearCustomField' value="Clear">
                                <input type="button" class="grey-btn" data-dismiss="modal" value="Cancel">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>

 <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
 <script>var upload_url = "<?php echo SITE_URL; ?>";</script>
<!-- Wrapper Ends -->



<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
     $(document).on("click",".clear_form",function() {
         bootbox.confirm({
             message: "Do you want to clear this form ?",
             buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
             callback: function (result) {
                 if (result == true) {
                     window.location.href = base_url + "/Maintenance/Inventory";
                 }
             }
         });
     });

       $(document).on("click",".add-popup .clear-btn",function(){
           var modal_id = $(this).parents('.add-popup').attr('id');
           resetFormClear('#'+modal_id,[], 'div',false);
       });
    var defaultFormData='';

    $(document).on("click","#ClearCustomField",function() {
        $('#custom_field')[0].reset();
    });
   /* $(document).on('click','.formreset',function () {
        resetEditForm("#addInventory",['inventoryCategory','subcategory','brand','supplier'],true,defaultFormData,[]);
    });*/
    $(document).on('click','.clearFormReset ',function () {
        bootbox.confirm({
            message: "Do you want to clear this form ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href = base_url + "/Maintenance/Inventory";
                }
            }
        });
    });
</script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/inventory/inventory.js"></script>



<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>