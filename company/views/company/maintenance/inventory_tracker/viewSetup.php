<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>


<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="bread-search-outer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Maintenance &gt;&gt; <span>Inventory Tracker List</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-data">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                        <div class="right-links">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </div>
                        <div id="RightMenu" class="box2">
                            <h2>MAINTENANCE</h2>
                            <div class="list-group panel">
                                <!-- Two Ends-->
                                <a href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Work Order</a>
                                <!-- Two Ends-->

                                <!-- Three Starts-->
                                <a id="" href="/WorkOrder/AddWorkOrder" class="list-group-item list-group-item-success strong collapsed" >New Recurring Work Order</a>
                                <!-- Three Ends-->

                                <!-- Four Starts-->
                                <a href="/WorkOrder/WorkOrders" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">View All Work Orders</a>
                                <!-- Four Ends-->

                                <!-- Five Starts-->
                                <a href="/ticket/tickets" id="" class="list-group-item list-group-item-success strong collapsed">View All Tickets</a>
                                <!-- Five Ends-->

                                <!-- Six Starts-->
                                <a href="/Inspection/Inspection" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">New Inspection</a>
                                <!-- Six Ends-->

                                <!-- Seven Starts-->
                                <a id="" href="/LostAndFound/LostAndFound" class="list-group-item list-group-item-success strong collapsed" >Lost & Found Manager</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/MileageLog/MileageLog" class="list-group-item list-group-item-success strong collapsed" >Mileage Log</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/Maintenance/Inventory" class="list-group-item list-group-item-success strong collapsed" >Inventory Tracker</a>
                                <!-- Seven Ends-->
                                <!-- Seven Starts-->
                                <a id="" href="/PurchaseOrder/PurchaseOrder" class="list-group-item list-group-item-success strong collapsed" >New Purchase Order</a>
                                <!-- Seven Ends-->
                            </div>
                        </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <!--Tabs Starts -->
                    <div class="main-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="/ticket/tickets">Tickets</a></li>
                            <li role="presentation"><a href="/WorkOrder/WorkOrders">Work Orders</a></li>
                            <li role="presentation"><a href="/PurchaseOrder/PurchaseOrderListing" >Purchase Orders</a></li>
                            <li role="presentation"><a href="/LostAndFound/LostAndFound">Lost & Found Manager</a></li>
                            <li role="presentation"><a href="/MileageLog/MileageLog">Mileage Log</a></li>
                            <li role="presentation"><a href="/Inspection/Inspection">Inspections</a></li>
                            <li role="presentation" class="active"><a href="/Maintenance/Inventory">Inventory Tracker</a></li>


                        </ul>
                      
                        <div class="tab-content">
                            <ul class="nav nav-tabs" role="tablist">
                                <!--<li role="presentation"><a href="/Maintenance/Inventory">Add Item to Inventory</a></li>
                                <li role="presentation"><a href="/Maintenance/InventorySetup">Inventory Setup</a></li>-->
                                <li role="presentation"><a href="/Maintenance/Inventory">Add Item to Inventory</a></li>
                                <li role="presentation"><a href="/Maintenance/InventorySetup">Inventory Setup</a></li>

                            </ul>

                            <input class="form-control type" type="hidden" name='type' value="add">
                            <input class="form-control inventory_id" type="hidden" name='inventory_id'>
                            <div role="tabpanel" class="tab-pane active" id="tickets-tracker">
                                <div class="property-status property-status-new">
                                    <div class="row">
                                        <div class="col-md-2 tenant_type_status">
                                            <label>Status</label>
                                            <select class="fm-txt form-control inventory_status jqGridStatusClass"  id="jqgridOptions" data-module="INVENTRYSETUPLISTING" >
                                                 <option value="All">All</option>
                                                <option value="All">Available</option>
                                                <option value="Unavailable">Unavailable</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-10" style="padding-top: 20px;">
                                            <div class="btn-outer text-right">
                                                <div class="col-lg-2 col-sm-6"></div>
                                                <div class="col-lg-3 col-sm-6 text-right">
                                                    <input type="text" class="form-control seachsample" name="seachsample" id="seachsample" placeholder="Search Text Here">
                                                </div>
                                                <div class="col-lg-7 buttons-in-cust">
                                                <a href="javascript:void(0);" class="blue-btn searchInventory">Search</a>
                                                <a target="_blank" href="<?php echo SITE_URL; ?>company/uploads/InventorySample.xlsx" download class="blue-btn">Download Sample</a>
                                                <a href="javascript:void(0)" class="blue-btn" id="import_tenant">Import Sample</a>
                                                <a class="blue-btn downloadexcelAll" id="downloadexcelAll">Export Sample</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div  style="display: none;" id="import_tenant_type_div" class="import_tenant_type_div-new">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion">Import Inventory</a>
                                                        </h4>
                                                    </div>
                                                    <div class="panel-body">
                                                        <form name="importTenantTypeForm" id="importTenantTypeFormId" enctype="multipart/form-data">
                                                            <div class="row">
                                                                <div class="form-outer">
                                                                    <div class="col-xs-12 col-sm-4 col-md-3">
                                                                        <input type="file" name="import_file" id="import_file"   accept=".csv, .xls, .xlsx" required/>
                                                                        <span class="error"></span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="btn-outer">
                                                                        <input type="submit" class="blue-btn importinventorytracker" value="Submit">
                                                                        <a href="javascript:void(0)" id="import_tenant_cancel_btn" class="grey-btn">Cancel</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form id="addInventory">
                                    <div class="sub-tabs">

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="ticket-inventory">
                                            <div class="panel panel-default clear">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion">New Inventory</a>
                                                    </h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="accordion-grid">
                                                        <div class="accordion-outer">
                                                            <div class="bs-example">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                                                    <span class="pull-right"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Inventory on Hand</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                            <div class="panel-body pad-none">
                                                                                <div class="grid-outer">
                                                                                    <div class="table-responsive inventoryTableTwo">
                                                                                        <table class="table table-hover table-dark" id="inventory_hand_table2">
                                                                                            <thead>
                                                                                            <tr>
                                                                                                <th scope="col">Property</th>
                                                                                                <th scope="col">Building</th>
                                                                                                <th scope="col">Inventory Sublocation</th>
                                                                                                <th scope="col">Category</th>
                                                                                                <th scope="col">Subcategory</th>
                                                                                                <th scope="col">Quantity</th>
                                                                                                <th scope="col">Actions</th>
                                                                                            </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                        </table>


                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                    <div class="btn-outer text-right">
                                                                        <a href="javascript:void(0)" class="blue-btn downloadpdf" id="downloadpdf">PDF</a>
                                                                        <a href="javascript:void(0)" class="blue-btn downloadexcel" id="downloadexcel">Excel</a>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                            <!-- Seventh Tab Ends-->
                        </div>
                    </div>
                    <!--Tabs Ends -->

                </div>
            </div>
        </div>
    </section>

</div>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.cropit.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/maintenance/inventory/inventory.js"></script>
 <script> var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";</script>
 <script>var upload_url = "<?php echo SITE_URL; ?>";</script>
<!-- Wrapper Ends -->



<!-- Footer Ends -->

<script>
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });



</script>




<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>