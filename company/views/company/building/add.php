<?php
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
    <div id="wrapper">
        <!-- Top navigation start -->
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
        ?>
        <!-- MAIN Navigation Ends -->
        <section class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12 bread-search-outer">
                        <div class="col-sm-8">
                            <div class="breadcrumb-outer">
                                Building Module >> <span>Add Building</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="easy-search">
                                <input placeholder="Easy Search" type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">

                        <div class="content-section">
                            <div class="add-hdr-links">
                                <ul>
                                    <li class="disabled">
                                        <a href="javascript:;">
                                            <label class="icon1"></label>
                                            Add Property
                                        </a>
                                    </li>
                                    <li class="active">
                                        <a href="javascript:;">
                                            <label class="icon2"></label>
                                            Add Building
                                        </a>
                                    </li>
                                    <li >
                                        <a id="unit_icon" href="javascript:;">
                                            <label class="icon3"></label>
                                            Add Units
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                    </div>
                    <div class="col-sm-12">
                        <div class="accordion-form">
                            <div class="accordion-outer">
                                <form name="add_building" id="add_building"  >
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" ><!--<span class="glyphicon glyphicon-menu-down"></span>-->Add Building</a> <a onclick="goBack()" class="back" href="javascript:;"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back</a>
                                                    </h4>
                                                </div>
                                                <div >
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="div-full">
                                                                <div class="name-id-greybox">
                                                                    <div class="name-id-greybox-inner">
                                                                        <label>
                                                                            Property Name :
                                                                        </label>
                                                                        <span id="property_name_span"> Himmon Groll</span>
                                                                    </div>
                                                                    <div class="name-id-greybox-inner">
                                                                        <label>
                                                                            ID :
                                                                        </label>
                                                                        <span id="property_id_span"> UZ8R1T</span>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="property_id" id="property_id" value="">
                                                            <div class="form-outer">
                                                                <div class="col-sm-2">
                                                                    <label>Building ID <em class="red-star">*</em></label>
                                                                    <input class="form-control" name="building_id" id="building_id" maxlength="20" placeholder="Eg: AB01234C " type="text" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Legal Name </label>
                                                                    <input class="form-control" id="legal_name" placeholder="Eg: The Fairmont Waterfront" maxlength="150" name="legal_name" type="text" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Building Name <em class="red-star">*</em></label>
                                                                    <input class="form-control" id="building_name" name="building_name" placeholder="Eg: Aber B1" maxlength="150" type="text" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>No. of Units <em class="red-star">*</em> </label>
                                                                    <input class="form-control" id="no_of_units" placeholder="Eg: 8"  maxlength="4" name="no_of_units"  type="text" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Building Address <em class="red-star">*</em></label>
                                                                    <input class="form-control" placeholder="Eg: 8 Avenue" maxlength="250" name="address" id="address" type="text"/>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Smoked Allowed </label>
                                                                    <select class="form-control" id="smoking_allowed" name="smoking_allowed">
                                                                        <option value="0">No</option>
                                                                        <option value="1">Yes</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Pet Friendly <a id="Newpet" class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class='form-control' id='pet_options' name='pet_friendly'>
                                                                        <option disabled selected value> -- select an option -- </option>
                                                                    </select>
                                                                    <div class="add-popup" id='NewpetPopup'>
                                                                        <h4>Add New Pet Friendly</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer custom-popup-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Pet Friendly<em class="red-star">*</em></label>
                                                                                    <input placeholder="Add Pet Friendly" id="pet_friendly" class="form-control capitalize_popup_input petFriendlyValidate customValidatePetFriendly" data_required="true" data_max="150" type="text"  name='@pet_friendly'/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="btn-outer">
                                                                                    <button class="blue-btn" id="NewpetPopupSave">Save</button>
                                                                                    <button type="button" class="clear-btn clearFormReset" id="clear_pet_friendly_popup_cancel">Clear</button>
                                                                                    <input type="button"  class="grey-btn" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2 multiplephonediv" >
                                                                    <label>Phone Number</label>
                                                                    <input class="form-control add-input p-number-format" placeholder="Phone Number" id="phone_number" maxlength="12" name="phone_number[]" type="text" />
                                                                    <a class="add-icon" id="multiplephoneno" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                    <a class="add-icon" id='multiplephonenocross' style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Note</label>
                                                                    <input class="form-control capital" name="note" placeholder="Eg: Note " maxlength="150" id="note" type="text" />
                                                                </div>
                                                                <div class="col-sm-2 multiplefaxdiv">
                                                                    <label>Fax Number</label>
                                                                    <input placeholder="Fax Number" class="form-control add-input p-number-format" maxlength="12" id="fax_number" name="fax_number[]" type="text" />
                                                                    <a class="add-icon" id="multiplefaxno" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                    <a class="add-icon" id='multiplefaxnocross' style='display: none;' href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                </div>

                                                                <div class="col-sm-2 clear">
                                                                    <label>Last Renovation</label>
                                                                    <input class="form-control" readonly placeholder="Eg: 01/01/2017" id="last_renovation_date" name="last_renovation_date" type="text" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <label>Last Renovation Time</label>
                                                                    <input class="form-control" readonly placeholder="Eg: 04:36 PM" type="text" id="last_renovation_time" name="last_renovation_time" />
                                                                </div>
                                                                <div class="col-sm-4 textarea-form notes_date_right_div">
                                                                    <label>Last Renovation Description </label>
                                                                    <textarea class="form-control notes_date_right" id="last_renovation_description" name="last_renovation_description"></textarea>
                                                                </div>
                                                                <div class="row col-sm-12" id="school_district_html" >
                                                                </div>
                                                                <div class="row col-sm-12" id="school_district_muncipiality" style="display:none" >
                                                                    <div class="col-sm-2">
                                                                        <label>School District</label>
                                                                        <input class="form-control" type="text" disabled />
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <label>Code</label>
                                                                        <input class="form-control" type="text" disabled />
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <label>Notes</label>
                                                                        <input class="form-control" type="text" disabled />
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4 col-md-3 key_optionsDivClass" id="key_optionsDiv">
                                                                    <label>Key/Access Code Info. <a  class="pop-add-icon Newkey" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <select class='form-control add-input key_access_codes_info key_access_codes_list' id='key_options' name='key_access_codes_info[]'></select>
                                                                    <div class="key_access_codeclass"  style="display: none;">

                                                                        <textarea name="key_access_code_desc[]" placeholder="Key Access Code" id='key_access_code' class="form-control key_access_codetext key_access_codetextdynm"></textarea>

                                                                    </div>
                                                                    <a id="NewkeyPlus" class="add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                                                                    <a style='display:none' id="NewkeyMinus" class="add-icon" href="javascript:;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>
                                                                    <div class="add-popup NewkeyPopup" id="NewkeyPopupid">
                                                                        <h4>Add New Key Access Code</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>Key Access Code<em class="red-star">*</em></label>
                                                                                    <input class="form-control keyAccessCode customValidateKeyAccessCode key-access-input" data_required="true" data_max="150" placeholder="Key Access Code Value" type="text" name='@key_code'/>
                                                                                    <span class="customError required"></span>
                                                                                </div>

                                                                                <div class="btn-outer text-right">
                                                                                    <button type="button" class="blue-btn NewkeyPopupSave" data_id="" >Save</button>
                                                                                    <button  type="button" class="clear-btn clearFormReset" id="clear_add_new_key_access">Clear</button>
                                                                                    <input type="button"  class="grey-btn NewkeyPopupCancel" value='Cancel'/>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="col-sm-4 textarea-form">
                                                                    <label>Amenities <a id='Newamenities' class="pop-add-icon" href="javascript:;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a></label>
                                                                    <div id="amenties_box" class="form-control"></div>
                                                                    <div class="add-popup" id='NewamenitiesPopup' style="width: 67%;">
                                                                        <h4>Add New Amenity</h4>
                                                                        <div class="add-popup-body">
                                                                            <div class="form-outer">
                                                                                <div class="col-sm-12">
                                                                                    <label>New Amenity Code<em class="red-star">*</em></label>
                                                                                    <input class="form-control customValidateAmenities"   data_max="15" placeholder="Eg:- 12345 (Optional)" type="text" name="@code"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label> Amenity Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control customValidateAmenities"  data_required="true" data_max="50" placeholder="Add New Amenity Name" type="text" name="@name"/>
                                                                                    <span class="customError required"></span>
                                                                                </div>
                                                                                <div class="btn-outer text-right">
                                                                                    <button class="blue-btn" id="NewamenitiesPopupSave">Save</button>
                                                                                    <input type="button"  class="clear-btn clearAmenityForm" value='Clear' />
                                                                                    <input type="button"  class="grey-btn" value='Cancel' />
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4 textarea-form notes_date_right_div">
                                                                    <label>Building Description </label>
                                                                    <textarea class="form-control notes_date_right" placeholder="Eg: This is a family friendly building with onsite laundry. Suites are newly renovated with balconies." name="description" id="description" rows="2"></textarea>
                                                                </div>

                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="photo-upload">
                                                                    <label>Photos</label>
                                                                    <!--                                                                <input type="button" class="blue-btn" value="Click here to Upload "></input>-->
                                                                    <button type="button" id="uploadPhotoVideo" class="green-btn">Click Here to Upload</button>
                                                                    <input id="photosVideos" type="file" name="photo_videos[]" accept="image/*, .mp4, .mkv, .avi" multiple style="display: none;">
                                                                    <!--                                                                <button type="button" id="removePhotoVideo" class="orange-btn">Remove All Photos</button>-->
                                                                    <!--                                                                <button type="button" id="savePhotoVideo" class="blue-btn">Save </button>-->
                                                                </div>
                                                                <div class="row" id="photo_video_uploads">
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-6">
                                                                <div class="photo-upload">
                                                                    <label>File Library</label>
                                                                    <button type="button" id="add_libraray_file" class="green-btn">Add Files...</button>
                                                                    <input id="file_library" type="file" name="file_library[]" accept=".doc,.pdf,.xlsx,.txt,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" multiple style="display: none;">
                                                                </div>
                                                                <div class="row" id="file_library_uploads">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" > Key Tracker</a>
                                                </h4>
                                            </div>
                                            <div >
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="latefee-check-outer">
                                                            <div class="col-sm-12 latefee-check-hdr">
                                                                <div class="check-outer" >
                                                                    <input type="radio" value="add_key" class="key_tracker_radio" name="@key_tracker" checked />
                                                                    <label class="blue-label">Add key</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 latefee-check-hdr" >
                                                                <div class="check-outer">
                                                                    <input type="radio"  value="track_key" class="key_tracker_radio" name="@key_tracker" />
                                                                    <label class="blue-label">Track Key</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="div-full" id="addnewkey">
                                                            <a  style="cursor: pointer" target="_self" >Add Key</a>
                                                        </div>
                                                        <div id="add_key_tracker" class="form-outer" style="display: none">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Key ID</label>
                                                                <input type="text" placeholder="Key Tag" maxlength="50" name="@key_id" id="key_id" class="form-control" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Description</label>
                                                                <input type="text" class="form-control" placeholder="This key is for 1st building" maxlength="50" name="@key_description" id="key_description" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Total Keys</label>
                                                                <input type="number"  min="1"  name="@total_keys" placeholder="Eg: 10" maxlength="50" id="total_keys" class="form-control" />
                                                            </div>
                                                        </div>

                                                        <div class="div-full" id="addtrackkey" style="display: none" >
                                                            <a  style="cursor: pointer" target="_self">Track Key</a>
                                                        </div>
                                                        <div class="form-outer" id="track_key_tracker" style="display: none">
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Name</label>
                                                                <input type="text" class="form-control owner_pre_vendor1 customValidateTrackKey"  data_required="true" data_max="50" maxlength="50" id="track_key_holder"/>
                                                                <!--                                                                <input type="hidden" class="customValidateTrackKey"  data_required="true" value="" name="@track_key_holder" id="track_key_holder_id">-->
                                                                <span class="customError required"></span>

                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Email</label>
                                                                <input type="text" class="form-control customValidateTrackKey"  data_required="true" data_max="50" maxlength="50" data_email name="@email" id="trackemail"/>
                                                                <span class="customError required"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Company Name</label>
                                                                <input type="text" class="form-control customValidateTrackKey" data_required="true" data_max="50" maxlength="50" name="@company_name" id="trackcompany_name"/>
                                                                <span class="customError required"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Phone #</label>
                                                                <input type="text" class="form-control phone_number customValidateTrackKey" data_required="true"  data_max="12" maxlength="12" name="@phone" id="trackphone"/>
                                                                <span class="customError required"></span>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Address1</label>
                                                                <input type="text" class="form-control customValidateTrackKey" data_required="true" data_max="50"  maxlength="50" name="@Address1" id="trackAddress1"/>
                                                                <span class="customError required"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Address2</label>
                                                                <input type="text" class="form-control customValidateTrackKey" data_required="true" data_max="50" maxlength="50" name="@Address2" id="trackAddress2"/>
                                                                <span class="customError required"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Address3</label>
                                                                <input type="text" class="form-control customValidateTrackKey" data_required="true" data_max="50" maxlength="50" name="@Address3" id="trackAddress3"/>
                                                                <span class="customError required"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Address4</label>
                                                                <input type="text" class="form-control customValidateTrackKey" data_required="true" data_max="50" maxlength="50" name="@Address4" id="trackAddress4"/>
                                                                <span class="customError required"></span>
                                                            </div>

                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Key#</label>
                                                                <input type="text" class="form-control customValidateTrackKey" data_required="true" data_max="50"  maxlength="50" name="@key_number" id="trackkey_number"/>

                                                                <span class="customError required"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Key Quantity</label>
                                                                <input type="text" class="form-control number_only customValidateTrackKey" data_required="true" data_max="20" maxlength="20" name="@key_quality" id="trackkey_quality"/>
                                                                <span class="customError required"></span>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Pick Up Date</label>
                                                                <input type="text" class="form-control" readonly name="@pick_up_date" id="pick_up_date" />
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Pick Up Time</label>
                                                                <input type="text" class="form-control" readonly name="@pick_up_time" id="pick_up_time" readonly=""/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Return Date</label>
                                                                <input type="text" class="form-control" readonly name="@return_date" id="return_date" readonly=""/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Return Time</label>
                                                                <input type="text" class="form-control" readonly name="@return_time" id="return_time" readonly=""/>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-4 col-md-3">
                                                                <label>Key Designator</label>
                                                                <input type="text" class="form-control customValidateTrackKey" data_required="true" maxlength="50" name="@key_designator" id="key_designator"/>
                                                                <span class="customError required"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" >Custom Fields</a>
                                                </h4>
                                            </div>
                                            <div  >
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="form-outer">
                                                            <div class="col-sm-12">
                                                                <div class="collaps-subhdr">
                                                                    <div class="custom_field_html">
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-sm-6 custom_field_msg">
                                                                            No Custom Fields
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="btn-outer text-right">
                                                                                <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 latefee-check-hdr ">
                                                                <div class="check-outer">
                                                                    <input type="checkbox" id="no_seprate_unit_checkbox" name="no_seprate_unit_checkbox"/>
                                                                    <label class="blue-label">No Separate Unit</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-10 details"  id="no_seprate_unit" style="display: none;">
                                                                <label>Details</label>
                                                                <div class="check-outer" type="text">
                                                                    <div class="col-sm-3">
                                                                        <label>Select Unit Type <span class="required_star">*<span></label>
                                                                        <span id="dynamic_unit_type">
                                                                        <select class="form-control" id="unit_type" name="unit_type">
                                                                                        <option value="">Select</option>
                                                                                    </select>
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Base Rent (R$) <em class="red-star">*</em> </label>
                                                                        <input placeholder="Eg: 2000" class="form-control" maxlength="8" type="text" id="base_rent" name="base_rent"  >
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Market Rent (R$) <em class="red-star">*</em> </label>
                                                                        <input placeholder="Eg: 2000" class="form-control" maxlength="8" type="text" name="market_rent" id="market_rent" value="">
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label>Security Deposit (R$) <em class="red-star">*</em> </label>
                                                                        <input placeholder="Eg: 2000" class="form-control" maxlength="8" type="text" id="security_deposit" name="security_deposit" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 latefee-check-hdr">
                                                                <div class="btn-outer text-right">
                                                                    <button class="blue-btn" type="submit">Save</button>
                                                                    <button type="button" class="clear-btn clearFormReset" id="clear_building_cancel">Clear</button>
                                                                    <button type="button" class="grey-btn" id="add_building_cancel">Cancel</button>
                                                                </div>
                                                            </div>
                                </form>
                                <div class="col-sm-12">
                                    <label class="filter-label">Filter By:</label>
                                    <div class="col-xs-12 col-sm-4 col-md-2">
                                        <select id="jqGridStatus" class="form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="listinggridDiv">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="table-responsive">
                                                    <table id="building-table" class="table table-bordered"></table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                        <div class="panel-body pad-none">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Wrapper Ends -->

    <!-- start custom field model -->
    <div class="container">
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content" style="width: 100%;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Custom Field</h4>
                    </div>
                    <div class="modal-body" style="height: 380px;">
                        <div class="form-outer col-sm-12">
                            <form id="custom_field">
                                <input type="hidden" id="customFieldModule" name="module" value="building">
                                <input type="hidden" name="id" id="custom_field_id" value="">
                                <div class="row custom_field_form">
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Field Name <em class="red-star">*</em></label>
                                        </div>
                                        <div class="col-sm-9 field_name">
                                            <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                            <span class="required error"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Data Type</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="form-control data_type" id="data_type" name="data_type">
                                                <option value="text">Text</option>
                                                <option value="number">Number</option>
                                                <option value="currency">Currency</option>
                                                <option value="percentage">Percentage</option>
                                                <option value="url">URL</option>
                                                <option value="date">Date</option>
                                                <option value="memo">Memo</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Default value</label>
                                        </div>
                                        <div class="col-sm-9 default_value">
                                            <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                    <div class="custom_field_row">
                                        <div class="col-sm-3">
                                            <label>Required Field</label>
                                        </div>
                                        <div class="col-sm-9 is_required">
                                            <select class="form-control" name="is_required" id="is_required">
                                                <option value="1">Yes</option>
                                                <option value="0" selected="selected">No</option>
                                            </select>
                                            <span class="error required"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer text-right">

                                    <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                    <button type="button" class="clear-btn clearFormReset" id="clear_building_custom">Clear</button>
                                    <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End custom field model -->

    <script type="text/javascript">
        var date_format = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format'] ?>";
        var default_name = "<?php echo $_SESSION[SESSION_DOMAIN]['default_name']; ?>";
        var default_form_data = '';
        var defaultIgnoreArray = [];
        $(document).ready(function() {
            setTimeout(function () {
                default_form_data = $("#add_building").serializeArray();
                defaultIgnoreArray = ['@key_tracker'];
                console.log(default_form_data);
            }, 300);
        });
    </script>
    <script>

        $(document).on('click','#clear_building_cancel',function(){
            bootbox.confirm("Do you want to clear this form?", function (result) {
                if (result == true) {
                    $("#_easyui_textbox_input1").val("");
                    resetFormClear('#add_building', ['building_id', 'legal_name', 'building_name', 'no_of_units', 'address','@pick_up_date','@pick_up_time','@return_date','@return_time','@key_designator','@key_tracker','unit_type','last_renovation_time'], 'form', false,default_form_data,defaultIgnoreArray);
                    // $( ".multiplephonediv:not(:first),.multiplefaxdiv:not(:first),.key_optionsDivClass:not(:first)" ).each(function(){
                    //     $(this).remove();
                    // });
                    $("#photo_video_uploads").html('');
                    $("#file_library_uploads").html('');
                    photo_videos = [];
                    imgArray = [];
                    file_library = [];
                    imgArray = [];
                    // $("#multiplephoneno,#multiplefaxno").css("display","block");
                    $("input[name='amenities[]']:checkbox").prop('checked',false);
                }
            });


        });
        $(document).on('click','#clear_pet_friendly_popup_cancel',function(){
            resetFormClear('#NewpetPopup',[],'div',false,default_form_data,defaultIgnoreArray);
        });
        $(document).on('click','#clear_add_new_key_access',function(){
            resetFormClear('#NewkeyPopupid',[],'div',false,default_form_data,defaultIgnoreArray);
        });

        $(document).on('click','#clear_building_custom',function(){
            resetFormClear('#custom_field',['data_type','is_required'],'form',false,default_form_data,defaultIgnoreArray);
        });





        var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
        $('#properties_top').addClass('active');
        $(function () {

            $('.nav-tabs').responsiveTabs();
        });

        <!--- Main Nav Responsive-->
        $("#show").click(function () {
            $("#bs-example-navbar-collapse-2").show();
        });
        $("#close").click(function () {
            $("#bs-example-navbar-collapse-2").hide();
        });
        <!--- Main Nav Responsive-->


        $(document).ready(function () {
            $(".slide-toggle").click(function () {
                $(".box").animate({
                    width: "toggle"
                });
            });
        });

        $(document).ready(function () {
            $(".slide-toggle2").click(function () {
                $(".box2").animate({
                    width: "toggle"
                });
            });
        });
        function goBack() {
            window.location.href='/Property/PropertyModules';
            //window.history.back();
        }

        <!--- Accordians-->
        $(document).ready(function () {
            // Add minus icon for collapse element which is open by default
            $(".collapse.in").each(function () {
                $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-menu-up").removeClass("glyphicon-menu-down");
            });

            // Toggle plus minus icon on show hide of collapse element
            $(".collapse").on('show.bs.collapse', function () {
                $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-down").addClass("glyphicon-menu-up");
            }).on('hide.bs.collapse', function () {
                $(this).parent().find(".glyphicon").removeClass("glyphicon-menu-up").addClass("glyphicon-menu-down");
            });


            $(document).on('click','.clearAmenityForm',function () {
                $('.customValidateAmenities').val('');
            });
        });
        <!--- Accordians-->

        $(document).on('click','#uploadPhotoVideo',function(){
            $('#photosVideos').val('');
            $('#photosVideos').trigger('click');
        });

        $(document).on('click','#add_libraray_file',function(){
            $('#file_library').val('');
            $('#file_library').trigger('click');
        });
        var upload_url = "<?php echo SITE_URL; ?>";
    </script>

    <!--    <script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/company/building.js"></script>-->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/building/building.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/commonPopup.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingFileLibrary.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingPhotoVideos.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.easyui.min.js"></script>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/easyui.css" rel="stylesheet">

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>