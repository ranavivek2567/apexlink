<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/constants.php");
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<div id="printSection"></div>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->


    <main class="apxpg-main">
        <section class="main-content">


            <div class="container-fluid">
                <div class="row building-listpage">
                    <div class="bread-search-outer">
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Property Module &gt;&gt; <span>Building Listing</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 list_property_name"><a href=""  id="property_link_name"></a></div>
                    <div class="content-data apxpg-allcontent"">
                    <!--- Right Quick Links ---->
                    <div class="right-links-outer hide-links">
                    <div class="right-links">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </div>
                    <div id="RightMenu" class="box2">
                        <h2>Properties</h2>
                        <div class="list-group panel">
                            <!-- Two Ends-->
                            <a href="/Property/AddProperty" class="list-group-item list-group-item-success strong collapsed" >New Property</a>
                            <!-- Two Ends-->

                            <!-- Three Starts-->
                            <a id="new_building_href" href="" class="list-group-item list-group-item-success strong collapsed" >New Building</a>
                            <!-- Three Ends-->

                            <!-- Four Starts-->
                            <a href="" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Manage Work Orders</a>
                            <!-- Four Ends-->

                            <!-- Five Starts-->
                            <a href="#" id="property_inspection_link" class="list-group-item list-group-item-success strong collapsed">Property Inspection</a>
                            <!-- Five Ends-->

                            <!-- Six Starts-->
                            <a href="#" class="list-group-item list-group-item-success strong collapsed" data-toggle="collapse" data-parent="#LeftMenu">Manage Property Group</a>
                            <!-- Six Ends-->

                            <!-- Seven Starts-->
                            <a id="new_unit_href" href="#" class="list-group-item list-group-item-success strong collapsed" >Add Unit</a>
                            <!-- Seven Ends-->
                        </div>
                    </div>
                    </div>
                    <!--- Right Quick Links ---->
                    <div class="main-tabs apx-tabs">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" ><a href="#property" id="property_tab_link">Property</a></li>
                            <li role="presentation" class="active"><a href="#building" aria-controls="profile" role="tab" data-toggle="tab">Building</a></li>
                            <li role="presentation"><a href="#" id="new_property_unit_list_href"  >Unit</a></li>
                        </ul>
                        <div class="property-status">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label></label>
                                    <select id="jqGridStatus" class="fm-txt form-control">
                                        <option value="All" selected="selected">All</option>
                                        <option value="1">Active</option>
                                        <option value="0">InActive</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-grid">
                            <div class="accordion-outer">
                                <div class="bs-example">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div id="collapseOne" class="panel-collapse collapse  in">
                                                <div class="panel-body pad-none">
                                                    <div class="grid-outer">
                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table id="property-building-table" class="table table-bordered"></table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>

<!-- Footer Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var upload_url = "<?php echo SITE_URL; ?>";
</script>
<script>

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->

    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });
    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

</script>
    <script>
        $(document).ready(function () {
            function getParameterByName( name ){
                var regexS = "[\\?&]"+name+"=([^&#]*)",
                    regex = new RegExp( regexS ),
                    results = regex.exec( window.location.search );
                if( results == null ){
                    return "";
                } else{
                    return decodeURIComponent(results[1].replace(/\+/g, " "));
                }
            }
            var prop_id =  getParameterByName('id');
            localStorage.setItem('prop_id',prop_id);

        });
    </script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/flag/flag.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/building/buildingList.js"></script>

<!-- Jquery Starts -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>