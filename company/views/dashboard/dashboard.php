<?php


if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");

include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/dashboard/spotlightModals.php");

?>
<link rel="stylesheet" type="text/css" href="<?php echo SUBDOMAIN_URL; ?>/company/css/Chart.css">

<!-- HTML Start -->
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>

    <!-- Top navigation end -->

    <!-- Content Start-->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row bread-search-outer">
                <div class=" col-xs-8">
                    <div class="dash-setting">
                        <a href="javascript:;" data-toggle="modal" data-target="#Spotlight-setting">
                            <img class="setting-image" src="<?php echo SUBDOMAIN_URL; ?>/company/images/setting_image.png" alt="" title="">
                        </a>
                    </div>

                </div>
                <div class="col-xs-4">
                    <div class="easy-search">
                        <input placeholder="Easy Search" type="text"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="dashboard-flex-graph">
                    <div id="firstSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Receivables / Payables</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn receivableChartFilter Filterchart">Chart Filter</button>
                                    <button class="blue-btn filterby" data-id="Receivablesfilterby" data-toggle="modal" data-target="#receivablesReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="receivablesChart" data-tableid="receivablesTable" tableshowID ="ajaxreceivablesTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="receivablesChart" data-tableid="receivablesTable" tableshowID ="ajaxreceivablesTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                    <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                    <ul class="listing-download-list" data-id="receivablesChart">
                                        <li id="printImageData" class="printImageData">Print chart</li>
                                        <li><a id="download" class="download" href="">Download PNG image</a></li>
                                        <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                        <li><a id="download-pdf2" class="download-pdf2">Download PDF image</a></li>
                                        <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            <canvas id="receivablesChart" class="chartjs-render-monitor"></canvas>

                            <!--jqgrid for work order-->
                            <div class="accordion-grid ajaxreceivablesTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="receivablesTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--jqgrid for work order-->


                        </div>
                    </div>
                    <div id="secSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Work Orders</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn  filterby" data-id="workfilterby" data-toggle="modal" data-target="#WorkReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="workOrder" data-tableid="WorkOrderTable" tableshowID ="ajaxWorkOrderTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="workOrder" data-tableid="WorkOrderTable" tableshowID ="ajaxWorkOrderTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                    <ul class="listing-download-list" data-id="workOrder">
                                        <li id="printImageData" class="printImageData">Print chart</li>
                                        <li><a id="download" class="download" href="">Download PNG image</a></li>
                                        <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                        <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                        <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                                <div class="imageDownload" id="imageDownload"></div>
                                            </a>
                                        </li>
                                    </ul>
                            </div>
                            <canvas id="workOrder"></canvas>

                            <!--jqgrid for work order-->
                            <div class="accordion-grid ajaxWorkOrderTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="WorkOrderTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--jqgrid for work order-->
                        </div>
                    </div>

                    <div id="thirdSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Move-Ins</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn  filterby" data-id="MoveInFilterby" data-toggle="modal" data-target="#MoveInReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="moveInsCharts" data-tableid="MoveInTable" tableshowID ="ajaxMoveInTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="moveInsCharts" data-tableid="MoveInTable" tableshowID ="ajaxMoveInTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <!-- <img src="--><?php //echo SUBDOMAIN_URL; ?><!--/company/images/graph-img1.png" alt="" title="">-->
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="moveInsCharts">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="moveInsCharts"></canvas>
                            <div class="accordion-grid ajaxMoveInTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="MoveInTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="forthSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Unit Vacancies</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn unitChartFilter Filterchart">Chart Filter</button>
                                    <button class="blue-btn  filterby" data-id="Unitvaccanyfilterby" data-toggle="modal" data-target="#UnitVaccanyReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="UnitVacanChart" data-tableid="UnitVacanciesTable" tableshowID ="unitVacanciesTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="UnitVacanChart" data-tableid="UnitVacanciesTable" tableshowID ="unitVacanciesTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="UnitVacanChart">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="UnitVacanChart" class="chartjs-render-monitor"></canvas>
                            <div class="accordion-grid unitVacanciesTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="UnitVacanciesTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="fifthSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Properties (Vacancy Rates Over 5%)</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn filterby" data-id="PropertyVaccanyRatesfilterby" data-toggle="modal" data-target="#ProVaccantRateReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="propertiesChart" data-tableid="PropertySpotTable" tableshowID ="ajaxPropertyTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="propertiesChart" data-tableid="PropertySpotTable" tableshowID ="ajaxPropertyTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="propertiesChart">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="propertiesChart" class="chartjs-render-monitor"></canvas>
                            <!--jqgrid for work order-->
                            <div class="accordion-grid ajaxPropertyTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="PropertySpotTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--jqgrid for work order-->
                        </div>
                    </div>
                    <div id="sixSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Move-Outs</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn filterby" data-id="MoveoutFilterby"  data-toggle="modal" data-target="#MoveInReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="moveOutsCharts" data-tableid="MoveOutTable" tableshowID ="ajaxMoveOutTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="moveOutsCharts" data-tableid="MoveOutTable" tableshowID ="ajaxMoveOutTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="moveOutsCharts">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="moveOutsCharts"></canvas>
                            <div class="accordion-grid ajaxMoveOutTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="MoveOutTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="sevenSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Credential Expiration</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn  filterby" data-id="credentialfilterby"  data-toggle="modal" data-target="#credentialReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="CredentialExp" data-tableid="CredentialExpTable" tableshowID ="ajaxCredentialExpTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="CredentialExp" data-tableid="CredentialExpTable" tableshowID ="ajaxCredentialExpTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="CredentialExp">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="CredentialExp" ></canvas>

                            <div class="accordion-grid ajaxCredentialExpTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="CredentialExpTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="eightSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Inventory Expiration</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn  filterby" data-id="InventoryFilterby" data-toggle="modal" data-target="#inventoryReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="InventoryExp" data-tableid="ajaxInventoryExpTable" tableshowID ="InventoryExpTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="InventoryExp" data-tableid="ajaxInventoryExpTable" tableshowID ="InventoryExpTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="InventoryExp">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="accordion-grid InventoryExpTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="ajaxInventoryExpTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <canvas id="InventoryExp"></canvas>
                        </div>
                    </div>
                    <div id="nineSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Birthday List</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn  filterby" data-id="birthdayfilterby" data-toggle="modal" data-target="#BirthdayReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="birthdayList" data-tableid="BirthdayTable" tableshowID ="ajaxbirthdayTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="birthdayList" data-tableid="BirthdayTable" tableshowID ="ajaxbirthdayTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="birthdayList">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="accordion-grid ajaxbirthdayTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="BirthdayTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <canvas id="birthdayList" ></canvas>
                        </div>
                    </div>
                    <div id="tenSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Tasks and Reminders</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn  filterby" data-id="TaskFilterby"  data-toggle="modal" data-target="#taskReminderReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="taskReminder" data-tableid="taskReminderTable" tableshowID ="ajaxTaskReminderTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="taskReminder" data-tableid="taskReminderTable" tableshowID ="ajaxTaskReminderTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="taskReminder">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="taskReminder"></canvas>
                            <div class="accordion-grid ajaxTaskReminderTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="taskReminderTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="elevenSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Tenant Insurance</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn filterby" data-id="tenantinsfilterby"  data-toggle="modal" data-target="#TenantInsReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="tenantInsuranceChart" data-tableid="tenantInsuraceTable" tableshowID ="ajaxTenantInsuranceTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="tenantInsuranceChart" data-tableid="tenantInsuraceTable" tableshowID ="ajaxTenantInsuranceTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="tenantInsuraceTable">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="accordion-grid ajaxTenantInsuranceTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="tenantInsuraceTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <canvas id="tenantInsuranceChart"></canvas>
                        </div>
                    </div>
                    <div id="twelveSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Lease Status</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn filterby " data-id="leasefilterby"  data-toggle="modal" data-target="#LeaseReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="leaseStatusChart" data-tableid="leaseStatusSpotTable" tableshowID ="ajaxleaseStatusTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="leaseStatusChart" data-tableid="leaseStatusSpotTable" tableshowID ="ajaxleaseStatusTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="leaseStatusChart">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="leaseStatusChart"></canvas>
                            <!--jqgrid for property Insurence Start-->
                            <div class="accordion-grid ajaxleaseStatusTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="leaseStatusSpotTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- jqgrid for property Insurence End -->
                        </div>
                    </div>
                    <div id="thirteenSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Property Insurance Status</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn  filterby" data-id="proInsfilterby" data-toggle="modal" data-target="#PropertyInsuranceReportFilter">Filter by</button>
                                    <button class="blue-btn rptbtn" data-id="proptyInsStusChart" data-tableid="PropertyInsurenceSpotTable" tableshowID ="ajaxPropertyInsurenceTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="proptyInsStusChart" data-tableid="PropertyInsurenceSpotTable" tableshowID ="ajaxPropertyInsurenceTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="proptyInsStusChart">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="proptyInsStusChart"></canvas>

                            <!--jqgrid for property Insurence Start-->
                            <div class="accordion-grid ajaxPropertyInsurenceTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="PropertyInsurenceSpotTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- jqgrid for property Insurence End -->
                        </div>
                    </div>
                    <div id="fourteenSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Paid / Unpaid Invoices</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn filterby " data-id="InvoicesFilterby" data-toggle="modal" data-target="#PropertyInsuranceReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="paidUnpaidInvoicesChart" data-tableid="InvoicesTable" tableshowID ="ajaxInvoicesTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="paidUnpaidInvoicesChart" data-tableid="InvoicesTable" tableshowID ="ajaxInvoicesTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="paidUnpaidInvoicesChart">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="paidUnpaidInvoicesChart"></canvas>
                            <div class="accordion-grid ajaxInvoicesTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="InvoicesTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="fifteenSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Paid / Unpaid Bill Status</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn  filterby" data-id="billfilterby" data-toggle="modal" data-target="#BillReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="paidUnpaidBillChart" data-tableid="BillDataTable" tableshowID ="ajaxBillTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="paidUnpaidBillChart" data-tableid="BillDataTable" tableshowID ="ajaxBillTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="paidUnpaidBillChart">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="paidUnpaidBillChart"></canvas>
                            <div class="accordion-grid ajaxBillTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="BillDataTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="sixteenSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Announcement  Status</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn announCFbtn Filterchart">Chart Filter</button>
                                    <button class="blue-btn filterby" data-id="Announcementfilterby" data-toggle="modal" data-target="#AnnouncemntReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="Announcement" data-tableid="announcementDataTable" tableshowID ="ajaxAnnouncementTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="Announcement" data-tableid="announcementDataTable" tableshowID ="ajaxAnnouncementTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="Announcement">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="Announcement"></canvas>
                            <!--jqgrid for work order-->
                            <div class="accordion-grid ajaxAnnouncementTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="announcementDataTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--jqgrid for announcement End-->

                            <!-- <div id="announcementDivCanvas"><img src="<?php echo SUBDOMAIN_URL; ?>/company/images/graph-img2.png" alt="" title=""></div>
                                               --> </div>
                    </div>
                    <div id="seventeenSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Delinquencies</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn  filterby" data-id="delinquencyfilterby" data-toggle="modal" data-target="#DelinquencyeReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="delinquency" data-tableid="delinquencyDataTable" tableshowID ="ajaxdelinquencyTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="delinquency" data-tableid="delinquencyDataTable" tableshowID ="ajaxdelinquencyTable">View Chart</button>

                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="delinquency">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="delinquency"></canvas>
                            <!--jqgrid for work order-->
                            <div class="accordion-grid ajaxdelinquencyTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="delinquencyDataTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--jqgrid for announcement End-->
                        </div>
                    </div>
                    <div id="eighteenSpotChart" class="dashboard-graph">
                        <div class="dashboard-graph-hdr">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 mob-change">
                                    <h3>Notification</h3>
                                </div>
                                <div class="col-xs-6 col-sm-6 mob-change text-right">
                                    <button class="blue-btn notifiCFbtn Filterchart">Chart Filter</button>
                                    <button class="blue-btn filterby" data-id="Notifictnfilterby" data-toggle="modal" data-target="#NotificationReportFilter">Filter By</button>
                                    <button class="blue-btn rptbtn" data-id="notification" data-tableid="NotificationDataTable" tableshowID ="ajaxNotificationTable">View Report</button>
                                    <button class="blue-btn  chartbtn" data-id="notification" data-tableid="NotificationDataTable" tableshowID ="ajaxNotificationTable">View Chart</button>
                                </div>
                            </div>
                        </div>
                        <div class="dashboard-graph-data">
                            <div class="listing-download-icon">
                                <span><i class="fa fa-bars fa-2x" aria-hidden="true"></i></span>
                                <ul class="listing-download-list" data-id="notification">
                                    <li id="printImageData" class="printImageData">Print chart</li>
                                    <li><a id="download" class="download" href="">Download PNG image</a></li>
                                    <li><a id="download1" class="download1"  href="">Download JPEG image</a></li>
                                    <li id="download-pdf2" class="download-pdf2">Download PDF image</li>
                                    <li  style="height: 15px"><a id="download2" class="download2" href="">Download SVG vector image
                                            <div class="imageDownload" id="imageDownload"></div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <canvas id="notification" ></canvas>
                            <div class="accordion-grid ajaxNotificationTable" style="display:none;">
                                <div class="accordion-outer">
                                    <div class="bs-example">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel panel-default">
                                                <div id="collapseOne" class="panel-collapse collapse in">
                                                    <div class="panel-body pad-none">
                                                        <div class="accordion-grid">
                                                            <div class="panel-group" id="accordion">
                                                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                    <div class="grid-outer">
                                                                        <div class="table-responsive">
                                                                            <div class="apx-table listinggridDiv">
                                                                                <div class="table-responsive">
                                                                                    <table id="NotificationDataTable" class="table table-bordered"></table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


<!-- Content End-->
</div>
<!-- HTML END -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";



    $(document).on('click','.unitChartFilter',function () {
        $('#unitVacancyChartFilter').modal('show');
    });

    /* $(document).on('click','.unitChartFilter',function () {
        $('#receivablesChartFilter').modal('show');
    });*/

    $(document).on('click','.download1111',function () {
        var ns = 'http://www.w3.org/2000/svg'
        var div = document.getElementById('drawing')
        var svg = document.createElementNS(ns, 'svg')
        svg.setAttributeNS(null, 'width', '100%')
        svg.setAttributeNS(null, 'height', '100%')
        div.appendChild(svg)
        var rect = document.createElementNS(ns, 'rect')
        rect.setAttributeNS(null, 'width', 100)
        rect.setAttributeNS(null, 'height', 100)
        rect.setAttributeNS(null, 'fill', '#f06')
        svg.appendChild(rect)

        var link = document.createElement('a');
        link.download = "my-image.svg";
        link.href = "my-image.svg";
        link.click();
    });
    var default_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";

</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.multiselect.js"></script>
<script  src="<?php echo SUBDOMAIN_URL; ?>/company/js/spotlight/Chart.js"></script>
<script  src="<?php echo SUBDOMAIN_URL; ?>/company/js/spotlight/Chart.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>

<!--<script src="https://cdn.jsdelivr.net/npm/rgb-color@2.1.2/rgb-color.js" integrity="sha256-akpJSk5vOtpoIxwepN148MQfXqkhrEGZj1eumc4PcL4=" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php /*echo SUBDOMAIN_URL; */?>/company/js/canvg/canvg.js"></script>-->

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/canvg/1.5/canvg.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/stackblur-canvas/2.2.0/stackblur.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/amstockchart/3.10.0/exporting/rgbcolor.js"></script>-->
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.5.0/fabric.min.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/3.0.13/svg.js"></script>
<script  src="<?php echo SUBDOMAIN_URL; ?>/company/js/spotlight/spotlight.js"></script>
<script  src="<?php echo SUBDOMAIN_URL; ?>/company/js/spotlight/spotlightTables.js"></script>


