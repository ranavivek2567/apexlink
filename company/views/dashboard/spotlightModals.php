<div class="modal fade" id="WorkReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="work_report_filter" id="work_report_filter" >
                    <!-- <div class="col-md-12">-->
                    <label class="mrg-btm-modal">Property </label>
                    <select class="is_required form-control multiSelectfield  select_property" name="select_property[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">From </label>
                    <input class="form-control from_date calander " type="text" name="fromdate"  />
                    <label class="mrg-btm-modal">To </label>
                    <input class="form-control to_date calander " type="text" name="todate"  />
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn work_report_submit" >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="workfilterby" >Reset </button>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="BirthdayReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="birthday_report_filter" id="birthday_report_filter" >
                    <!-- <div class="col-md-12">-->
                    <label  class="mrg-btm-modal">Month </label>
                    <select class="is_required form-control " name="select_month"  id="select_month">
                        <option value="" >Select month</option>
                        <option value="1">Jan</option>
                        <option value="2">Feb</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">Aug</option>
                        <option value="9">Sep</option>
                        <option value="10">Oct</option>
                        <option value="11">Nov</option>
                        <option value="12">Dec</option>
                    </select>
                    <label  class="mrg-btm-modal">Module </label>
                    <select class="is_required form-control " name="module"  id="module">
                        <option value="all" selected>All</option>
                        <option value="2">Tenant</option>
                        <option value="4">Owner</option>
                        <option value="3">Vendor</option>
                        <option value="5">Contact</option>
                        <option value="8">Employee</option>
                    </select>

                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn birth_report_submit" >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="birthdayfilterby" >Reset </button>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="credentialReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="credential_report_filter" id="credential_report_filter" >
                    <!-- <div class="col-md-12">-->
                    <label  class="mrg-btm-modal">Credential Type </label>
                    <select class="is_required form-control multiSelectfield " name="credentialType[]"  id="credentialType" multiple>
                    </select>
                    <label  class="mrg-btm-modal">From </label>
                    <input class="form-control from_date calander" type="text" name="fromdate"  />
                    <label  class="mrg-btm-modal">To </label>
                    <input class="form-control to_date calander" type="text" name="todate"  >
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn credential_report_submit" >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="credentialfilterby" >Reset </button>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="inventoryReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="inventory_report_filter" id="inventory_report_filter" >
                    <label class="mrg-btm-modal">Property </label>
                    <select class="is_required form-control multiSelectfield  property" name="select_property[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">From </label>
                    <input class="form-control from_date calander fromdate" type="text" name="fromdate"  />
                    <label class="mrg-btm-modal">To </label>
                    <input class="form-control to_date calander todate" type="text" name="todate"  />
                    <label class="mrg-btm-modal">Category </label>
                    <select class="is_required form-control multiSelectfield " name="category[]"  id="category" multiple>
                    </select>
                    <label class="mrg-btm-modal">Sub Category </label>
                    <select class="is_required form-control multiSelectfield  " name="sub_category[]"  id="sub_category" >
                        <option value="">Select Subcategory</option>
                    </select>
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn inventory_report_submit" >Submit </button>
                        <button type="button" class="clear-btn reset_btn" data-id="InventoryFilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="taskReminderReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="task_report_filter" id="task_report_filter" >
                    <label class="mrg-btm-modal">Property </label>
                    <select class="is_required form-control multiSelectfield select_property" name="select_property[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">Status </label>
                    <select class="is_required form-control" name="status" id="status">
                        <option value="all">All</option>
                        <option value="1">Not Assigned</option>
                        <option value="2">Not Started</option>
                        <option value="3">In Progress</option>
                        <option value="4">Completed</option>
                        <option value="5">Cancelled and Resigned</option>
                    </select>
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn task_report_submit" >Submit </button>
                        <button type="button" class="clear-btn reset_btn" data-id="TaskFilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="DelinquencyeReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="delinquency_report_filter" id="delinquency_report_filter" >
                    <label class="mrg-btm-modal">Complaint Type </label>
                    <select class="is_required form-control multiSelectfield select_property" name="complaint_type[]"  id="complaint_type" multiple>
                    </select>
                    <label class="mrg-btm-modal">From </label>
                    <input class="form-control from_date calander fromdate" type="text" name="fromdate"  />
                    <label class="mrg-btm-modal">To </label>
                    <input class="form-control to_date calander todate" type="text" name="todate"  />
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn delinquency_report_submit" >Submit </button>
                        <button type="button" class="clear-btn reset_btn" data-id="delinquencyfilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!--chart filter Start-->
<!--chart filter Start-->
<div class="modal fade" id="unitVacancyChartFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Chart Filters</h4>
            </div>
            <div class="modal-body">
                <div class="form-outer form-outer2">
                    <form method="post" id="comp_unitSpotChart">
                        <div class="row">
                            <div class="col-sm-12 clear" style="width: 100%">
                                <label class="mrg-btm-modal"> Portfolio </label>
                                <select class="form-control " type="text" maxlength="20" name="portfolio_List"  id="portfolio_List"></select>
                            </div>
                        </div>
                    </form>
                    <div class="btn-outer text-right">
                        <input type="button" value="Apply Filters" class="blue-btn" id="save_vehiclelog">
                        <input type="button" class="grey-btn cancel_Popup" value="Cancel">
                    </div>
                </div>
            </div>
            <div class="modal-footer border-0">
            </div>
        </div>
    </div>
</div>
<!--chart filter End-->

<!--chart filter Start-->
<div class="modal fade" id="announcementChartFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Chart Filters</h4>
            </div>
            <div class="modal-body">
                <div class="form-outer">
                    <form method="post" id="comp_annoSpotChart">
                        <div class="row">
                            <div class="col-sm-12 clear" style="width: 100%">
                                <label class="mrg-btm-modal"> Year </label>
                                <select class="form-control" type="text" maxlength="20" name="years"  id="years"></select>
                            </div>
                            <div class="">
                                <div class="col-sm-12 clear" style="width: 100%">
                                    <label class="mrg-btm-modal"> Month </label>
                                    <select id="months" type="text" maxlength="20" class="form-control" name="annoMonth">
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="btn-outer text">
                        <input type="button" value="Apply Filters" class="blue-btn" id="annomntChFilter">
                        <input type="button" class="grey-btn cancel_Popup1" value="Cancel">
                    </div>
                </div>
            </div>
            <div class="modal-footer border-0">
            </div>
        </div>
    </div>
</div>
<!--chart filter End-->


<div class="modal fade" id="MoveInReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="movein_report_filter" id="movein_report_filter" >
                    <label class="mrg-btm-modal">Portfolio </label>
                    <select class="is_required form-control portfolio" name="portfolio[]" multiple>
                    </select>
                    <label class="mrg-btm-modal">Property </label>
                    <select class="is_required form-control multiSelectfield select_property" name="property[]"   multiple>
                    </select>
                    <br><br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn " >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="MoveInFilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="UnitVaccanyReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="unitvaccany_report_filter" id="unitvaccany_report_filter" >
                    <label class="mrg-btm-modal">Portfolio </label>
                    <select class="is_required form-control multiSelectfield portfolio" name="portfolio[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">Property </label>
                    <select class="is_required form-control multiSelectfield property" name="property[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">From </label>
                    <input class="form-control from_date calander" type="text" name="fromdate"  />
                    <label class="mrg-btm-modal">To </label>
                    <input class="form-control to_date calander" type="text" name="todate"  />
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn unitvaccant_report_submit" >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="Unitvaccanyfilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="LeaseReportFilter" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="lease_report_filter" id="lease_report_filter" >
                    <label class="mrg-btm-modal">Tenant </label>
                    <select class="is_required form-control multiSelectfield tenant " name="tenant[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">Status </label>
                    <select class="is_required form-control  " name="status"  id="status">
                        <option value="all">All</option>
                        <option value="1">Active</option>
                        <option value="0">Expired</option>
                    </select>
                    <label class="mrg-btm-modal">From </label>
                    <input class="form-control from_date calander fromdate" type="text" name="fromdate"  />
                    <label class="mrg-btm-modal">To </label>
                    <input class="form-control to_date calander todate" type="text" name="todate"  />
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn lease_report_submit" >Submit </button>
                        <button type="button" class="clear-btn reset_btn" data-id="leasefilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="TenantInsReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="tenantIns_report_filter" id="tenantIns_report_filter" >
                    <label class="mrg-btm-modal">Tenant </label>
                    <select class="is_required form-control multiSelectfield  tenant " name="tenant[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">Status </label>
                    <select class="is_required form-control " name="status"  id="status">
                        <option value="all">All</option>
                        <option value="1">Active</option>
                        <option value="0">Expired</option>
                    </select>
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn tenantins_report_submit" >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="tenantinsfilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="PropertyInsuranceReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="proIns_report_filter" id="proIns_report_filter" >
                    <label class="mrg-btm-modal">Property </label>
                    <select class="is_required form-control multiSelectfield  select_property" name="select_property[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">Status </label>
                    <select class="is_required form-control " name="status"  id="status">
                        <option value="all">All</option>
                        <option value="1">Active</option>
                        <option value="0">Expired</option>
                    </select>
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn" >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="proInsfilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="AnnouncemntReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="announce_report_filter" id="announce_report_filter" >
                    <!-- <div class="col-md-12">-->
                    <label class="mrg-btm-modal">Credential Type </label>
                    <select class="form-control" type="text" name="title"  id="title">
                        <option value="1">System Maintenance</option>
                        <option value="0">Other</option>
                        <option value="all" selected>All</option>
                    </select>
                    <label class="mrg-btm-modal">From </label>
                    <input class="form-control from_date calander fromdate" type="text" name="fromdate"  />
                    <label class="mrg-btm-modal">To </label>
                    <input class="form-control to_date calander todate" type="text" name="todate"  />
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn announcemnt_report_submit" >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="Announcementfilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ProVaccantRateReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="provaccrate_report_filter" id="provaccrate_report_filter" >
                    <!-- <div class="col-md-12">-->
                    <label class="mrg-btm-modal">Property </label>
                    <select class="is_required form-control multiSelectfield select_property" name="select_property[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">Property Manager </label>
                    <select class="is_required form-control multiSelectfield  property_manager" name="property_manager[]"   multiple>
                    </select>
                    <br>
                    <div class="text-right">
                    <button type="submit" class="blue-btn provaccrate_report_submit" >Submit </button>
                    <button type="button" class="clear-btn reset_btn" data-id="PropertyVaccanyRatesfilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="BillReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="bill_report_filter" id="bill_report_filter" >
                    <!-- <div class="col-md-12">-->
                    <label class="mrg-btm-modal">Vendor </label>
                    <select class="is_required form-control multiSelectfield vendors" name="vendor[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">Status </label>
                    <select class="is_required form-control multiSelectfield " name="status" id="status" >
                        <option value="all" selected>All</option>
                        <option value="1">Active</option>
                        <option value="0">Expired</option>
                    </select>
                    <br>
                    <div class="text-right">
                        <button type="submit" class="blue-btn bill_report_submit" >Submit </button>
                        <button type="button" class="clear-btn reset_btn" data-id="billfilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="Spotlight-setting" class="modal fade" role="dialog">
    <div class="modal-dialog spotlight-pop">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Spotlight Settings</h4>
            </div>
                <div class="modal-body">
                    <form name="addSettingSpotChart" id="addSettingSpotChart">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">

                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon1.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Receivables / Payables</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="firstSpotChart" name="firstSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon6.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Birthday List</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="nineSpotChart" name="nineSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon4.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Work Order Status</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="secSpotChart" name="secSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon3.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Tasks and Reminders</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="tenSpotChart" name="tenSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon6.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Move-Ins</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="thirdSpotChart" name="thirdSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon2.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Tenant Insurance</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="elevenSpotChart" name="elevenSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon2.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Unit Vacancies</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="forthSpotChart" name="forthSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon1.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Lease Status</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="twelveSpotChart" name="twelveSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon3.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Property (Vacancy Rates over 5%)</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="fifthSpotChart" name="fifthSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon3.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Property Insurance Status</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="thirteenSpotChart" name="thirteenSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon6.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Move Outs</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="sixSpotChart" name="sixSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon1.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Paid / Unpaid Invoices</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="fourteenSpotChart" name="fourteenSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon3.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Credential Expiration</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="sevenSpotChart" name="sevenSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon1.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Paid / Unpaid Bill Status</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="fifteenSpotChart" name="fifteenSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon5.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Inventory Expiration</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="eightSpotChart" name="eightSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon6.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Announcement Status</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="sixteenSpotChart" name="sixteenSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon6.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Delinquencies</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="seventeenSpotChart" name="seventeenSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="row">
                                    <div class="col-xs-2 text-right">
                                        <img src="<?php echo SUBDOMAIN_URL; ?>/company/images/spotlighticon6.png" alt="" title="">
                                    </div>
                                    <div class="col-xs-7 pad-none">
                                        <label>Notifications</label>
                                    </div>
                                    <div class="col-xs-3 pad-none">
                                        <label class="switch">
                                            <input type="checkbox" id="eighteenSpotChart" name="eighteenSpotChart" value="1" checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="modal-footer">
                                <input type="button" id="saveSettingBtn" class="blue-btn saveSettingBtn" value="Save changes">
                                <button type="button" class="grey-btn close-bttn" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>

<!--Receviable model Start-->
<div class="modal fade" id="receivablesReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="receivables_report_filter" id="receivables_report_filter" >
                    <label class="mrg-btm-modal">Portfolio </label>
                    <select class="is_required form-control multiSelectfield portfolio" name="portfolio[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">Property </label>
                    <select class="is_required form-control multiSelectfield property" name="property[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">From </label>
                    <input class="form-control from_date calander" type="text" name="fromdate"  />
                    <label class="mrg-btm-modal">To </label>
                    <input class="form-control to_date calander" type="text" name="todate"  />
                    <br>
                    <div class="text-right">
                        <button type="submit" class="blue-btn receivables_report_submit">Submit </button>
                        <button type="button" class="clear-btn reset_btn" data-id="Receivablesfilterby" >Reset </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--Receviable model End-->

<!--chart filter Start-->
<div class="modal fade" id="receivablesChartFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Chart Filters</h4>
            </div>
            <div class="modal-body">
                <div class="form-outer form-outer2">
                    <form method="post" id="comp_receivableSpotChart">
                        <div class="row">
                            <div class="col-sm-12 clear" style="width: 100%">
                                <label class="mrg-btm-modal"> Portfolio </label>
                                <select class="form-control " type="text" maxlength="20" name="portfolio_List"  id="portfolio_List"></select>
                            </div>
                        </div>
                    </form>
                    <div class="btn-outer text-right">
                        <input type="button" value="Apply Filters" class="blue-btn" id="save_receivable">
                        <input type="button" class="grey-btn cancel_Popup4" value="Cancel" data-dismiss="modal">
                    </div>
                </div>
            </div>
            <div class="modal-footer border-0">
            </div>
        </div>
    </div>
</div>
<!--chart filter End-->

<!--chart filter Start-->
<div class="modal fade" id="notificationChartFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Chart Filters</h4>
            </div>
            <div class="modal-body">
                <div class="form-outer">
                    <form method="post" id="comp_notifSpotChart">
                        <div class="row">
                            <div class="col-sm-12 clear" style="width: 100%">
                                <label class="mrg-btm-modal"> Year </label>
                                <select class="form-control" type="text" maxlength="20" name="years"  id="years"></select>
                            </div>
                            <div class="">
                                <div class="col-sm-12 clear" style="width: 100%">
                                    <label class="mrg-btm-modal"> Month </label>
                                    <select id="months" type="text" maxlength="20" class="form-control" name="annoMonth">
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="btn-outer text-right">
                        <input type="button" value="Apply Filters" class="blue-btn" id="notifiChFilter">
                        <input type="button" class="grey-btn cancel_Popup1" value="Cancel">
                    </div>
                </div>
            </div>
            <div class="modal-footer border-0">
            </div>
        </div>
    </div>
</div>
<!--chart filter End-->


<!--chart filter notification Start-->
<div class="modal fade" id="NotificationReportFilter" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Filters</h4>
            </div>
            <div class="modal-body" style="padding:8px;">
                <form name="notification_report_filter" id="notification_report_filter" >
                    <label class="mrg-btm-modal">Property </label>
                    <select class="is_required form-control multiSelectfield  property" name="select_property[]"   multiple>
                    </select>
                    <label class="mrg-btm-modal">Type </label>
                    <select class="is_required form-control multiSelectfield" name="module_type"  id="module_type">
                        <option value="all">All</option>
                        <option value="2">Tenant</option>
                        <option value="4">Owner</option>
                        <option value="3">Vendor</option>
                    </select>
                    <label class="mrg-btm-modal">From </label>
                    <input class="form-control from_date calander fromdate" type="text" name="fromdate"  />
                    <label class="mrg-btm-modal">To </label>
                    <input class="form-control to_date calander todate" type="text" name="todate"  />
                    <br>
                    <div class="text-right">
                        <button type="submit" class="blue-btn notification_report_submit" >Apply Filter </button>
                        <button type="button" class="clear-btn reset_btn" data-id="NotificationFilterby">Reset</button>
                        <button type="button" class="" data-id="">Cancel </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--chart filter notification End-->
