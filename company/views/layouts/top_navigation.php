<?php
/**
 * Created by PhpStorm.
 * User: ptripathi
 * Date: 1/21/2019
 * Time: 6:00 PM
 */
include_once(ROOT_URL.'/config.php');
include_once (ROOT_URL.'/constants.php');
include_once( ROOT_URL."/company/helper/helper.php");
include_once( "$_SERVER[DOCUMENT_ROOT]/company/helper/ddl.php");

if (basename($_SERVER['PHP_SELF']) == basename(__FILE__))
{
    $url = BASE_URL."login";
    header('Location: '.$url);
}

?>
<header>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2 visible-xs">

            </div>
            <div class="col-sm-3 col-xs-8">
                <div class="logo">
                    <img src="<?php echo COMPANY_SUBDOMAIN_URL;?><?php echo !empty($_SESSION[SESSION_DOMAIN]['company_logo'])? $_SESSION[SESSION_DOMAIN]['company_logo'] :  '/images/logo.png' ?>" height="" width=""></div>
            </div>
            <div class="col-sm-9 col-xs-2">
                <div class="hdr-rt">
                    <!-- TOP Navigation Starts -->

                    <nav class="navbar navbar-default">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <?php if(!isset($_SESSION[SESSION_DOMAIN]['cuser_id'])){ ?>

                                <?php } else { ?>
                                    <li class="hidden-xs">Welcome: <span id="user_name"><?php echo !empty($_SESSION[SESSION_DOMAIN]['super_admin_name'])? $_SESSION[SESSION_DOMAIN]['super_admin_name'] :  $_SESSION[SESSION_DOMAIN]['name'] ?></span>, <span id="formated_date"><?php echo $_SESSION[SESSION_DOMAIN]['formated_date'];?></span>
                                        <a href="javascript:;" data-toggle="modal" data-target="#myModalCalc" data-backdrop="false" data-keyboard="false"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                                        <span class="company_calendar">
                                             <a href="/Calendar/Calendar"><img src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/calendar_image.png">
                                            <h4 class="clscalendarimg" id="hCalendarDate">
                                                <?php
                                                if(isset($_SESSION[SESSION_DOMAIN]['actual_date'])){
                                                    $session_date= $_SESSION[SESSION_DOMAIN]['actual_date'];
                                                    $show_date=date('d', strtotime($session_date));
                                                    echo $show_date;
                                                }
                                                ?>
                                            </h4></a>
                                        </span>
                                    </li>

                                    <!--                                    <li><a href="/Setting/Settings/" class="nav-link disabled"><i class="fa fa-cog" aria-hidden="true"></i>Admin</a>-->
                                    <!--                                    </li>-->

                                    <li class="nav-item admin_help_class">
                                        <a class="nav-link" id="admin_id" href="#"><i class="fa fa-cog" aria-hidden="true"></i>Admin<span class="arrow"></span></a>
                                        <ul id="admin-submenu" style="display:none;">
                                            <li><a href="/Setting/Settings/" class="clsSuggestion help_dp" ><span class="helpHeader"></span>Admin Settings</a></li>
                                            <li><a href="/BillingAndSubscription/SubscriptionInfo" href="javascript:void(0)"><span class="Suggestion help_dp"></span>Billing & Subscriptions</a></li>
                                        </ul>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="nav-link" href="#"><i class="fa fa-question" aria-hidden="true"></i>Help</a>
                                    </li> -->

                                    <li class="nav-item help_class">
                                        <a class="nav-link" id="help_id" href="#"><i class="fa fa-question" aria-hidden="true"></i>Help<span class="arrow"></span></a>
                                        <ul id="help-submenu" style="display:none;">
                                            <li><a target="_blank" href="https://apexlink.com/kb.php?h=TRUE" class="clsSuggestion help_dp" ><span class="helpHeader"></span>Help Center</a></li>
                                            <li><a href="javascript:void(0)" class="clsSuggestion help_dp" data-toggle="modal" id="apexklink_newT"><span class="Suggestion help_dp"></span>Apexlink New</a></li>
                                            <li><a href="javascript:void(0)" class="clsSuggestion help_dp" data-toggle="modal" data-target="#helpThird"><span class="Suggestion"></span>Apexlink Connect</a></li>
                                        </ul>
                                    </li>
                                    <li class="nav-item notify_class">
                                        <a class="nav-link" href="#" id="notify_admin"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a>
                                        <div class="arrow_box" style="display: none;">
                                            <div class="notiTxt-dd">
                                                <ul id="BubbleDrop" class="dropdown-menu notiTxt-dd" role="menu">
                                                </ul>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/MasterData/FlagBank">
                                            <div class="cls-dv-Flag"><span id="spnFlagCount" class="cls-spnFlag"><?php echo $_SESSION[SESSION_DOMAIN]['total_flag_count']; ?></span></div>
                                            Flag Bank

                                            <img src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/Flag.png"> </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link support_class" ><i class="fa fa-wrench" aria-hidden="true"></i>Support<span class="arrow"></span></a>
                                        <ul id="support-submenu" style="display: none;">
                                            <li><a  class="clsSuggestion" data-toggle="modal" id="support_validation_id" data-target="#myModalSupportRequest" data-backdrop="static" data-keyboard="false"><span class="Suggestion"></span>Support Request</a></li>
                                            <li><a  class="clsSuggestion" data-toggle="modal" id="suggest_feature_validation_id" data-target="#myModalSuggestFeature" data-backdrop="static" data-keyboard="false"><span class="Suggestion"></span>Suggest a Feature</a></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item">
                                        <form name="logout" method="post" action="">
                                            <a class="nav-link disabled" role="button" type="submit" name="logout" id='logout' value="logout" ><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                        </form>
                                    </li>
                                <?php } ?>
                            </ul>

                        </div><!-- /.navbar-collapse -->
                    </nav>
                    <!-- TOP Navigation Ends -->

                </div>
            </div>
        </div>
    </div>

</header>
<!-- MAIN Navigation Starts -->
<section class="main-nav">
    <nav class="navbar navbar-default">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header visible-xs">
            <a id="show" class="icon-bar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <a class="nav-close visible-xs" id="close" href="#"><i class="fa fa-times" aria-hidden="true"></i> </a>

                <li><a class="spotlight"  href="/Dashboard/Dashboard">Spotlight</a></li>
                <li id="properties_top"><a class="properties" href="/Property/PropertyModules">Properties</a></li>
                <li id="people_top"><a class="people" href="/Tenantlisting/Tenantlisting">People</a></li>
                <li id="leases_top"><a class="leases" href="/GuestCard/ListGuestCard">Leases</a></li>
                <li id="maintenance_top"><a class="maintenance" href="/ticket/tickets" >Maintenance</a></li>
                <li id="accounting_top"><a class="accounting" href="/Accounting/Accounting">Accounting</a></li>
                <li id="communication_top"><a class="communication" href="/Communication/InboxMails">Communication</a></li>
                <li id="marketing"><a class="marketing" href="/Marketing/MarketingListing">Marketing</a></li>
                <li id="reports"><a class="reports" href="/Reporting/Reporting">Reports</a></li>

                <div class="trail" id="trail">
                    <div class="trail-inner">
                        <h6>Your trial will expire in:</h6>
                        <div class="trail-timer">
                            <ul class="list-inline">
                                <li>
                                    <div class="timer-outer">
                                        <h6 id="countDownDays">14</h6>
                                        <p>Days</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="timer-outer">
                                        <h6 id="countDownHours">14</h6>
                                        <p>Hours</p>
                                    </div>
                                </li> <li>
                                    <div class="timer-outer">
                                        <h6 id="countDownMins">25</h6>
                                        <p>Min</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="timer-outer">
                                        <h6 id="countDownSec">10</h6>
                                        <p>Sec</p>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <a id="listAllAnnouncements" class="listAllAnnouncements pull-right" href="/MasterData/AllAnnouncements"><i class="fa fa-bullhorn" aria-hidden="true"></i></a>
            </ul>
        </div>
    </nav>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL;?>/js/validation/users/users.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL;?>/js/header.js"></script>

    <script>
        $(document).on('click','.support_class',function(){
            $('#support-submenu').toggle();
        });


        $(document).ready(function () {
            var url = window.location;
            // Will only work if string in href matches with location
            $('ul.nav a[href="' + url + '"]').parent().addClass('active');

            // Will also work for relative and absolute hrefs
            $('ul.nav a').filter(function () {
                return this.href == url;
            }).parent().addClass('active').parent().parent().addClass('active');
        });

        $(document).on('click','#clearSuggestFeatureForm',function(){
            bootbox.confirm("Do you want to Clear this form?", function (result) {
                if (result == true) {
                    $('#suggest_features_form_id #suggest_feature').val('');
                }
            });
        });

        $(document).on('click','#clearSupportRequestForm',function(){
            bootbox.confirm("Do you want to Clear this form?", function (result) {
                if (result == true) {
                    $('#support_report_form_id #message').val('');
                    $('#support_report_form_id #request_subject').val('');
                }
            });
        });

    </script>
</section>
<!-- MAIN Navigation Ends -->

<!-Suggest a feature Modal Start->
<div class="modal fade support-newmodal" id="myModalSuggestFeature" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Suggest a Feature</h4>
            </div>
            <div class="modal-body" style="padding:0px;">
                <form name="suggest_features" id="suggest_features_form_id" >
                    <div class="support-data">
                        <label>Name <em class="red-star">*</em></label>
                        <input name="name" id="name" class="form-control" value="<?php echo $_SESSION[SESSION_DOMAIN]['name'];?>" readonly/>
                        <label>Entity/Company </label>
                        <input name="company_name" id="company_name" class="form-control" value="<?php echo $_SESSION[SESSION_DOMAIN]['company_name'];?>" readonly/>
                        <label>Email <em class="red-star">*</em></label>
                        <input name="email" id="email" class="form-control" value="<?php echo $_SESSION[SESSION_DOMAIN]['email'];?>" readonly/>
                        <label>Phone </label>
                        <input name="phone_number" id="phone_number" class="form-control" value="<?php echo $_SESSION[SESSION_DOMAIN]['phone_number'];?>" readonly/>
                        <label>Subject <em class="red-star">*</em></label>
                        <input name="subject" id="subject" class="form-control subject_class capital" value="Suggest Features" readonly/>
                        <label>Suggest Features <em class="red-star">*</em></label>
                        <textarea name="suggest_feature" placeholder="Please suggest a new feature that you would like to see in ApexLink" id="suggest_feature" class="form-control capital" autofocus rows="4" cols="50"></textarea>
                        <button type="button" id="clearSuggestFeatureForm" class="clear-btn pull-right" >Clear</button>
                        <button type="submit" class="blue-btn pull-right" >Submit Suggestion</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-Suggest a feature Modal End->


<!-Support request Modal Start->
<div class="modal fade support-newmodal" id="myModalSupportRequest" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Support Request</h4>
            </div>
            <div class="modal-body" style="padding:0px;">
                <form name="support_report" id="support_report_form_id" >
                    <div class="support-data">
                        <label>Name <em class="red-star">*</em></label>
                        <input name="name" id="request_name" class="form-control" value="<?php echo $_SESSION[SESSION_DOMAIN]['name'];?>" readonly/>
                        <label>Entity/Company </label>
                        <input name="company_name" id="request_company_name" class="form-control" value="<?php echo $_SESSION[SESSION_DOMAIN]['company_name'];?>" readonly/>
                        <label>Email <em class="red-star">*</em></label>
                        <input name="email" id="request_email" class="form-control" value="<?php echo $_SESSION[SESSION_DOMAIN]['email'];?>" readonly/>
                        <label>Phone </label>
                        <input name="phone_number" id="request_phone_number" class="form-control" value="<?php echo $_SESSION[SESSION_DOMAIN]['phone_number'];?>" readonly/>
                        <label>Subject <em class="red-star">*</em></label>
                        <input type="text" name="subject" id="request_subject" autofocus class="form-control subject_class capital"/>
                        <label>Message <em class="red-star">*</em></label>
                        <textarea name="message" id="message" class="form-control capital" rows="4" cols="50"></textarea>
                        <button type="button" id="clearSupportRequestForm" class="clear-btn pull-right" >Clear</button>
                        <button type="submit" class="blue-btn pull-right" >Submit Support</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

