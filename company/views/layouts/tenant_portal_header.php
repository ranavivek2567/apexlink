<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>ApexLink</title>
    <meta name="author" content="APEXLINK">
    <meta name="description" content="APEXLINK">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/images/favicon.ico" alt="apex-icon" type="image/x-icon" sizes="16x16">
    <!-- Bootstrap -->

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/ui.jqgrid.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/jquery.timepicker.min.css">

    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/passwordscheck.css" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/intlTelInput.css">
    <link rel="stylesheet" href="<?php echo SUPERADMIN_SITE_URL;?>/css/calculator.css"/>
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/owl.theme.default.min.css"/>
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.multiselect.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo COMPANY_SUBDOMAIN_URL;?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />





    <!-- include libraries(jQuery, bootstrap) -->
    <!-- include summernote css/js -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
    <!--<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>-->
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/summernote.js" defer></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery-ui.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.timepicker.min.js"></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.jqgrid.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.responsivetabs.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.validate.min.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/toastr.min.js"></script>

    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/bootbox.js"></script>
    <script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/jquery.mask.min.js"></script>
    <script defer src="<?php echo SUPERADMIN_SITE_URL; ?>/js/calculator.js"></script>
    <script defer src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/owl.carousel.js"></script>
 <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>

    <script defer>
        var subdomain_url  = "<?php echo COMPANY_SUBDOMAIN_URL; ?>";
        //Toast Messages
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": true,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "400",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        setTimeout(function () {
            var announcement_skip_ids = localStorage.getItem("announcement_ids");
            //getAnnouncements(announcement_skip_ids);
            setInterval(function () {
                var announcement_skip_ids = localStorage.getItem("announcement_ids");
                //getAnnouncements(announcement_skip_ids);
            }, 60000);

        }, 1000);

        function getAnnouncements(announcement_skip_ids) {
            $.ajax
            ({
                type: 'post',
                url: '/common-ajax',
                data: {
                    class: "CommonAjax",
                    action: "showAnnouncement",
                    announcement_skip_ids: announcement_skip_ids
                },
                success: function (result) {
                    var result = JSON.parse(result);
                    //console.log(result);
                    if (result != 'empty') {
                        announcement_ids = [];
                        $.each(result, function (key, value) {
                            var values;
                            if (value.seenStatus != 'seen') {
                                if (value.title == 'System Maintainance') {
                                    values = value.id;
                                    $('#announcementSys_id').val(value.id);
                                    $('#SysstartDate').text(value.start_date);
                                    $('#SysstartTime').text(value.start_time);
                                    $('#SysendTime').text(value.end_time);
                                    $('#SystemMaintenance').modal('show');
                                } else {
                                    values = value.id;
                                    $('#announcement_id').val(value.id);
                                    $('#announcementTitle').text(value.title);
                                    $('#announcementStartDate').text(value.start_date);
                                    $('#announcementEndDate').text(value.end_date);
                                    $('#announcementStartTime').text(value.start_time);
                                    $('#announcementEndTime').text(value.end_time);
                                    $('#announcementDescription').text(value.description);
                                    $('#announcement').modal('show');
                                }
                                announcement_ids.push(values);
                            }
                        });
                        if (result) {
                            localStorage.setItem("announcement_ids", JSON.stringify(announcement_ids));
                        }
                    }
                }
            });
        }

        $(document).on('click', '.closeSystemAnnouncement', function () {
            var announcement_id = $("#announcementSys_id").val();
            $.ajax
            ({
                type: 'post',
                url: '/common-ajax',
                data: {
                    class: "CommonAjax",
                    action: "updateUserAnnouncement",
                    announcement_id: announcement_id
                },
                success: function (result) {
                    var result = JSON.parse(result);
                }

            });
        });

        $(document).on('click', '.closeAnnouncement', function () {
            var announcement_id = $("#announcement_id").val();
            $.ajax
            ({
                type: 'post',
                url: '/common-ajax',
                data: {
                    class: "CommonAjax",
                    action: "updateUserAnnouncement",
                    announcement_id: announcement_id
                },
                success: function (result) {
                    var result = JSON.parse(result);
                }

            });
        });


        $(document).ready(function () {
            //  window.history.pushState(null, "", window.location.href);
            //   window.onpopstate = function () {
            //       window.history.pushState(null, "", window.location.href);
            //   };
        });


        jQuery.extend(jQuery.validator.messages, {
            required: "* This field is required.",
        });

    </script>
    <style media="print">
        body{display:none;}
    </style>
</head>
<body>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/modal/companyModal.php");
?>

    <section class="main-nav">
        <input type="hidden" name="tenant_id" class="tenant_id" value="<?php echo $_GET['tenant_id']; ?>">
         <nav class="navbar navbar-default">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header visible-xs">
                  <a id="show" class="icon-bar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                <ul class="nav navbar-nav">
                  <a class="nav-close visible-xs" id="close" href="#"><i class="fa fa-times" aria-hidden="true"></i> </a>
                  <li class="active" id="account"><a href="<?php echo DOMAIN_URL ?>/Tenant-portal?tenant_id=<?php echo $_GET['tenant_id']; ?>">My Account</a></li>
                  <li id="transaction"><a href="<?php echo DOMAIN_URL ?>/TenantPortal/Tenant/TenantTranactions?tenant_id=<?php echo $_GET['tenant_id']; ?>">Transactions</a></li>
                  <li id="maintenance"><a href="<?php echo DOMAIN_URL ?>/TenantPortal/maintenance?tenant_id=<?php echo $_GET['tenant_id']; ?>">Maintenance</a></li>
                  <li id="libraries"><a href="<?php echo DOMAIN_URL ?>/TenantPortal/file-library?tenant_id=<?php echo $_GET['tenant_id']; ?>">File Library</a></li>
                  <li id="insurance"><a href="<?php echo DOMAIN_URL ?>/TenantPortal/portal-insurance?tenant_id=<?php echo $_GET['tenant_id']; ?>">Renter's Insurance</a></li>
                  <li id="communication"><a href="<?php echo DOMAIN_URL ?>/TenantPortal/Communication/SentEmails?tenant_id=<?php echo $_GET['tenant_id']; ?>">Communication</a></li>
                </ul>

              </div><!-- /.navbar-collapse -->
          </nav>
        </section>
