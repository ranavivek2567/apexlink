<header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-2 visible-xs">
                    
                </div>
                <div class="col-sm-3 col-xs-8">
                    <div class="logo"><img src="<?php echo SUPERADMIN_SITE_URL;?>/images/logo.png"></div>
                </div>
                <div class="col-sm-9 col-xs-2">
                    <div class="hdr-rt">
                        <!-- TOP Navigation Starts -->

                        <nav class="navbar navbar-default">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1 testclass">
                               <ul class="nav navbar-nav">
                                   <li class="hidden-xs">Welcome: <span id="fullname"></span> (<span class="property_name"></span>), <span class="current_date"></span>
                                   <a href="javascript:;" data-toggle="modal" data-target="#myModalCalc" data-backdrop="false" data-keyboard="false"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                                   </li>
                               <li class="nav-item">
                                   <a target = "blank" href="https://apexlink.com/kb.php?e=TRUE">? HELP</a>
                               </li>
                              <!-- <li class="nav-item">
                                   <a href="javascript:void(0)"><i class="fa fa-bell" aria-hidden="true"></i> Notifications</a>
                               </li>-->
                                   <li class="nav-item notify_class">
                                       <a class="nav-link" href="#" id="notify_admin1"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a>
                                       <div class="arrow_box" style="display: none;">
                                           <div class="notiTxt-dd">
                                               <ul id="BubbleDrop" class="dropdown-menu notiTxt-dd" role="menu">
                                               </ul>
                                               <div class="clr"></div>
                                           </div>
                                       </div>
                                   </li>
                               <li class="nav-item">
                                    <a class="nav-link disabled portalLogout"  href="JavaScript:Void(0);"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                               </li>
                              </ul> 

                            </div><!-- /.navbar-collapse -->
                        </nav>
                        <!-- TOP Navigation Ends -->

                    </div>
                </div>
            </div>
        </div>
        
    </header>
 