<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-2 visible-xs">

            </div>
            <div class="col-sm-3 col-xs-8">
                <div class="logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL; ?><?php echo!empty($_SESSION[SESSION_DOMAIN]['Owner_Portal']['company_logo']) ? $_SESSION[SESSION_DOMAIN]['Owner_Portal']['company_logo'] : '/images/logo.png' ?>" height="60" width="160"></div>
            </div>
            <div class="col-sm-9 col-xs-2">
                <div class="hdr-rt">
                    <!-- TOP Navigation Starts -->

                    <nav class="navbar navbar-default">

                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
<?php //echo '<pre>'; print_r($_SESSION[SESSION_DOMAIN]['Owner_Portal']); echo '</pre>'; ?>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="hidden-xs">Welcome: <span id="user_name"><?php echo !empty($_SESSION[SESSION_DOMAIN]['Owner_Portal']['super_admin_name'])? $_SESSION[SESSION_DOMAIN]['Owner_Portal']['super_admin_name'] :  $_SESSION[SESSION_DOMAIN]['Owner_Portal']['name'] ?></span>, <span id="formated_date"><?php echo $_SESSION[SESSION_DOMAIN]['Owner_Portal']['formated_date'];?></span>
                                    <a href="javascript:;" data-toggle="modal" data-target="#myModalCalc" data-backdrop="false" data-keyboard="false"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                                </li>
                                <li class="nav-item help_class">
                                    <a class="nav-link" target="_blank" href="https://apexlink.com/kb.php?w=TRUE"><i class="fa fa-question" aria-hidden="true"></i>Help<span class="arrow"></span></a>
<!--                                    <ul id="help-submenu" style="display:none;">-->
<!--                                        <li><a target="_blank" href="https://apexlink.com/kb.php?w=TRUE" class="clsSuggestion help_dp" ><span class="helpHeader"></span>Help Center</a></li>-->
<!--                                        <li><a href="javascript:void(0)" class="clsSuggestion help_dp" data-toggle="modal" id="apexklink_newT"><span class="Suggestion help_dp"></span>Apexlink New</a></li>-->
<!--                                        <li><a href="javascript:void(0)" class="clsSuggestion help_dp" data-toggle="modal" data-target="#helpThird"><span class="Suggestion"></span>Apexlink Connect</a></li>-->
<!--                                    </ul>-->
                                </li>
                                <!--<li class="nav-item notify_class">
                                    <a class="nav-link" href="#" id="notify_admin"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a>
                                    <div class="arrow_box" style="display: none;">
                                        <div class="notiTxt-dd">
                                            <ul id="BubbleDrop" class="dropdown-menu notiTxt-dd" role="menu">

                                            </ul>
                                            <div class="clr"></div>
                                        </div>
                                    </div>
                                </li>-->

                                <li class="nav-item notify_class">
                                    <a class="nav-link" href="#" id="notify_admin2"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a>
                                    <div class="arrow_box" style="display: none;">
                                        <div class="notiTxt-dd">
                                            <ul id="BubbleDrop" class="dropdown-menu notiTxt-dd" role="menu">
                                            </ul>
                                            <div class="clr"></div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled" href="#" id="owner_portal_logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                                </li>
                            </ul>

                        </div><!-- /.navbar-collapse -->
                    </nav>
                    <!-- TOP Navigation Ends -->

                </div>
            </div>
        </div>
    </div>

</header>

<!-- MAIN Navigation Starts -->
<section class="main-nav">
    <nav class="navbar navbar-default">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header visible-xs">
            <a id="show" class="icon-bar" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <a class="nav-close visible-xs" id="close" href="#"><i class="fa fa-times" aria-hidden="true"></i> </a>
                <li id="my_account_top"><a class="my_account"  href="/Owner/MyAccount/AccountInfo">My Account</a></li>
                <li id="statement_top"><a class="statement" href="/Owner/OwnerTransactionStatement/OwnerStatement">Statement</a></li>
                <li id="bills_top"><a class="bills" href="/Owner/MyAccount/BillInfo">Bills</a></li>
                <li id="maintenance_top"><a class="maintenance" href="/Owner/Ticket/Tickets" >Maintenance</a></li>
                <li id="file_library_top"><a class="file_library" href="/Owner/FileLibrary/Library">File Library</a></li>
                <li id="property_owned_top"><a class="property_owned" href="/Owner/PropertiesOwned/Properties">Property Owned</a></li>
                <li id="communication_top"><a class="communication" href="/Owner/Communication/SentEmails">Communication</a></li>
                <li id="owner_draw_top"><a class="owner_draw" href="/Owner/OwnerPayments/OwnerDraw">Owner Draw</a></li>
                <li id="owner_contribution_top"><a class="owner_contribution" href="/Owner/OwnerPayments/OwnerContribution">Owner Contribution</a></li>
                <a id="listAllAnnouncements" class="listAllAnnouncements pull-right" href="/Owner/MyAccount/AllAnnouncements"><i class="fa fa-bullhorn" aria-hidden="true"></i></a>
            </ul>

        </div><!-- /.navbar-collapse -->
    </nav>
</section>
<script>
    $(document).on('click','#help_id',function(){
        $('#help-submenu').toggle();
    });


    $(document).ready(function () {
        /*add notification start*/
        $('#notify_admin2').click(function () {
           // alert("reach here");
           /* var tenant_id = $(".tenant_id").val();*/

            $.ajax({
                type: 'post',
                url: '/notification-ajax',
                data: {
                    class: 'notificationsAjax',
                    action: 'notificationslist',
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    console.log(response);
                    if (response.status == 'success' && response.code == 200) {
                        //$('.userNotification').html(response.html);
                        var html_data='';
                       // alert(html_data);
                        html_data +='<li><strong>'+response.html[0].module_title+'</strong>: '+response.html[0].notifi_description+' </li> <li> <strong>'+response.html[1].module_title+'</strong>:'+response.html[1].notifi_description+'</li><li><a id="ViewAllNotification" href="/Tenant-portal/Tenant/Notifications">View All</a></li>';
                        $('#BubbleDrop').html(html_data);

                    } else {
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });




            /*var html_data='';
            html_data +='<li><strong>Lease Signed</strong>: New Lease (ID -144) has been generated for Property ID-DW7VJ0 </li> <li> <strong>Lease Signed</strong>: New Lease (ID -141) has been generated for Property ID-NTDB5F</li><li><a id="ViewAllNotification" href="/Alert/Notifications">View All</a></li>';
                 $('#BubbleDrop').html(html_data);*/
            $('.notify_arrow_box').slideToggle( "slow");
            $('.arrow_box').slideToggle( "slow");
            $('#BubbleDrop').slideToggle( "slow");
        });
        /*add notification end*/
    })
</script>