<?php


if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}

include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");

?>
<link rel="stylesheet" type="text/css" href="<?php echo SUBDOMAIN_URL; ?>/company/css/Chart.css">

<!-- HTML Start -->
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>

    <!-- Top navigation end -->

            <!-- Content Start-->
    <section class="main-content">
        <div class="container-fluid">
            <div class="bread-search-outer">
                <div class="row">
                    <div class="col-xs-8">

                    </div>
                    <div class="col-xs-4">
                        <div class="easy-search">
                            <input placeholder="Easy Search" type="text"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-data">

                <div class="panel panel-default">
                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                        <div class="panel-body pad-none">
                            <div class="grid-outer">
                                <div class="apx-table">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                <span class="pull-right"></span> Notifications</a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse  in">
                                        <div class="panel-body user-alerts">
                                            <div class="grid-outer">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-dark">
                                                        <thead>
                                                        <tr class="clsUserAlert">
                                                            <th width="50%" align="left" scope="col">Title</th>
                                                            <th width="50%" align="left" scope="col">Description</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="userNotification">
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <!-- Content End-->
</div>
<!-- HTML END -->
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/notification/notifiApex.js"></script>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var jsDateFomat = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    var currencySymbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var default_symbol  = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
</script>



