<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>

<?php
include_once(COMPANY_DIRECTORY_URL. "/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<div id="wrapper">
    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="logo-terms">
                        <div class="login-logo-terms"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo-login.png"/></div>
                    </div>
                </div>
            </div>
    </header>
    <!-- Top navigation end -->
    <!--Content Start-->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 terms-out">
                    <!--                    <h1 style="color:red; text-align:center;">ApexLink Terms of Use</h1>-->
                    <!--                    <h5 style="color:darkolivegreen;">Updated: May 20, 2020.</h5>-->
                    ApexLink Privacy Policy
                    Updated: May 20, 2020

                    We understand that our users will be placing sensitive information into our system, which is why privacy is one of the utmost concerns of ApexLink, Inc. This document describes what types of information we may collect and how we will use it. By using our Site and Services, you consent to the information collection and handling practices outlined in this policy as well as in our Terms of Use.

                    General Data Protection Regulation (GDPR)
                    Information Collection
                    Some of the information you may submit is personally identifiable information, but much of it is not. Personally identifiable information is information that identifies a particular person. Examples include your name, phone number, address, and social security number. It is possible to browse our website without actively submitting any personally identifiable information.

                    ApexLink's property management application services ("Services") allow you to organize and consolidate a wide range of business and personal data for property management purposes. Through use of the Services, Users may transmit detailed information about clients, tenants, vendors, and other associated parties. We refer to this data as "User Data." Various forms of communication with ApexLink customer service, support and other staff may be stored. All Users of our software will need to supply information connected to their business operation. ApexLink requires tenant information so that the information can be organized, populated and maintained for a variety of user functions. All payment data that is uploaded by the user is encrypted, stored and only utilized within client-authorized sessions in ApexLink Property Manager. This notice applies to any personally identifiable information or other User Data that we may collect through our Applications.

                    Automatically Collected Information
                    Every time you visit our website, some information is automatically collected from you. This information may include, but is not limited to, some or all of the following items: your Internet Protocol (IP) address and/or domain; type and version of Internet browser software and operating system you use; date, time, and duration of your site access; specific pages, buttons, images, videos, links, or forms that you access while visiting the site; type of device (e.g. iPad, BlackBerry, iPhone, Android); mobile carrier and/or Internet Service Provider; and demographic information concerning the country of origin of your Computer and the language(s) used by it.

                    Cookies
                    Cookies are small text files that are placed on your computer/device to distinguish you from other visitors to our Site. The use of cookies is a standard practice among websites to collect or track information about your activities while using the website. We and/or our third party advertising service providers may place cookies or similar files on your hard drive for many of the reasons listed in this Privacy Policy while visiting our Site. Most people do not know that cookies are being placed on their computers when they visit websites because browsers are typically set to accept cookies. You can choose to have your browser warn you every time a cookie is being sent to you or you can choose not to accept cookies. You can also delete cookies from your computer at any time. If you refuse cookies, the functionality of our Site may be impacted or become non-functional. Since every browser and computer is different, you will need to follow your browser's instructions for disabling or deleting cookies. Any cookies used are programmed to self-terminate after a designated time period in the event that the User ceases use of the Services.

                    Information Collected via Flash Objects and Other Similar Technology
                    In addition to using cookies, we and/or our third party advertising service providers may also use similar technologies to track users' interactions with our Site. Some of these technologies include web beacons (transparent graphical images placed on a website), pixels, tags, and Flash objects (also referred to as "Local Shared Objects"). Please refer to your browser's instructions to remove cached Sites, history, and images from your computer. Deleting or disabling cookies will not remove Flash objects or Local Shared Objects. Instead refer to Adobe's website for more information on how to control or disable these items.

                    Additional Information on Online Behavioral Advertising;
                    Some of the advertisements that click-through to our Site and/or may be on our Site contain cookies that allow for the monitoring of your response to these advertisements (sometimes referred to as interest-based advertisements). Our advertisements may also appear on other websites that use the same advertising service providers as us. These advertising service providers may use your browsing history across websites to choose which advertisements to display to you.

                    If you do not wish to have us and/or our third party advertising service providers know which advertisements and subsequent websites you have viewed, you may opt-out at AboutAds (established by the Digital Advertising Alliance). The Digital Advertising Alliance website contains important information on cookies, behavioral advertising, and what opting out will and will not do and choices you can make regarding interest-based advertisements.

                    Please note: opting out of behavioral advertising will not stop you from receiving advertisements. You will still see the same number of advertisements as before, but they may not be as relevant to you. If you use other computers or browsers and want to opt out of interest-based advertisements, you will need to repeat this process for each computer or browser. If you delete your cookies and want to continue to be opted out of interest based advertisements you will have to repeat this opt-out process.

                    Use of Personally Identifiable Information
                    We may use these technologies on our site to verify your identity, remember personal settings including your preferences, to offer you additional options or enhance your online experience, and to improve our products and Services. We may also use them for marketing site personalization, tracking of online applications and programs and/or tracking the effectiveness of advertisements for our products and services that we may place on our Site or other linked and/or partner websites. Cookies used for tracking advertising effectiveness do not collect personally identifiable information. In order to better serve you, some of these technologies allow us the ability to view your past interactions with our sites and/or online mortgage/banking environment for customer service, troubleshooting, risk analysis and fraud detection, as well as other related purposes.

                    We Collect your Information in Order to:
                    meet our responsibilities to you;
                    follow your instructions;
                    inform you of new services or products; and
                    make sure our business suits your needs.
                    Your Use of the Software Application
                    We may collect and examine information regarding your activity within the Service. We only use this information to find out which areas of the software application people visit most. This helps us to add more value to our Services. This information is limited to activity within the software application.

                    Communication and Marketing
                    By using our services or mobile app, you consent to our use your personal or other information to tell you about products, services and special offers from us or other companies that may interest you. We may do this by post, email or text message (SMS). If you later decide that you do not want us to do this, please contact us to opt-out and we will stop doing so.

                    When you send an e-mail or other electronic communication to us, you are communicating with us electronically and consent to receive reply communications from us or our providers electronically. We may retain the content of the e-mail, electronic communication, your e-mail address, unique identifier (such as Facebook or Twitter user name), and our response in order to better service your needs or for audit, legal, regulatory or other business-related reasons.

                    Third Parties
                    We may ask other organizations to provide support services to us. If such a circumstance arises, the third party would have to agree to our privacy policies if they need access to any personal information to carry out their services.

                    Our Site may contain links to third party websites or applications. These links and pointers to third party websites or applications are not part of our Site. ApexLink does not make any representations or warranties regarding these third party websites. We are not responsible for any losses or damages in connection with the information, security, privacy practices, availability, content or accuracy of materials of such third party websites or applications. These third party websites or applications might have Terms of Use or Privacy Policies that differ from ours and third party websites may provide less privacy and/or security than our Site. We implore you to review the Terms of Use and Privacy Policy of all third party websites before you share any personally identifiable information with them.

                    Change of Control
                    Personal information may be transferred to a third party as a result of a sale, acquisition, merger, reorganization or other change of control. If we sell, merge or transfer any part of our business, or substantially all of its assets are acquired, customer information will of course be one of the transferred assets.

                    Permissible Revelation of Personal Information

                    We will not reveal personal information to anyone outside our company or certain of our service providers without your permission, unless:
                    we must do so by law or in terms of a court order;
                    it is in the public interest; or
                    we need to do so to protect our rights.

                    Information Security
                    ApexLink implements and maintains physical, administrative, and technical safeguards designed to secure user information and prevent unauthorized access to or disclosure of that information. Under our security policies and practices, access to sensitive personal information is authorized only for those who have a business need for such access, and sensitive records are retained only as long as reasonably necessary for business or legal purposes. ApexLink strives to protect the user information that we collect and store, however, no security program is 100% secure and we cannot guarantee that our safeguards will prevent every unauthorized attempt to access, use, or disclose personal information. ApexLink maintains security incident response policies and procedures to handle incidents involving unauthorized access to private information we collect or store.

                    Security of Log-In Credentials
                    We maintain strict rules to help prevent others from guessing your password. We also recommend that you change your password periodically. Your password must be 8 or more characters with at least one letter and one number required. You are responsible for maintaining the security of your Login ID and Password. You may not provide these credentials to any third party. If you believe that they have been stolen or been made known to others, you must contact us immediately, but in any event you should change your password immediately via the Service. We are not responsible if someone else accesses your account through information they have obtained from you or through a violation by you of this Privacy Policy or our Terms and Conditions or other Agreement.

                    Terms and Conditions
                    Please also visit our Terms and Conditions section establishing the use, disclaimers, and limitations of liability governing the use of our website at www.ApexLink.com.

                    Modifications to this Policy
                    We reserve the right to update or modify this privacy policy. We will put all changes on our website. The date last revised appears at the top of the Policy. Changes take effect immediately upon posting. The latest version of our privacy and security statement will replace all earlier versions of it, unless it states otherwise.

                    If you have any questions or concerns about the ApexLink Privacy Policy or its implementation, you may contact us at info@apexlink.com or:

                    ApexLink, Inc.
                    ATTN: Legal Department
                    P.O. Box 608
                    Marco Island, FL 34146
                    <!--                    <p class="terms-content">Thank you for using our website, products, and services (“Services”). The Services are provided by ApexLink, Inc. (“ApexLink”), a Florida corporation. ApexLink reserves the right to amend or change the terms of this Agreement from time to time with or without notice to you. It is your sole responsibility to access and review these terms regularly.-->
                    <!--                    <br><br>PLEASE READ THIS AGREEMENT CAREFULLY BEFORE USING THE SERVICES. BY REGISTERING FOR AN ACCOUNT, SUBSCRIBING TO OUR SERVICES, ACCESSING OUR WEBSITE, OR BY EXECUTING A SEPARATE WRITTEN AGREEMENT WITH APEXLINK, THE USER EXPRESSLY AGREES TO THE FOLLOWING TERMS OF SERVICE GOVERNING THE USE OF APEXLINK’S SERVICES, AS WELL AS OUR PRIVACY POLICY. APEXLINK’S CONSENT TO YOUR USE OF THE SERVICES IS STRICTLY CONDITIONED ON YOUR CONSENT TO ALL OF THE TERMS AND CONDITIONS CONTAINED IN THIS AGREEMENT. IF YOU DO NOT CONSENT TO ALL TERMS YOU ARE NOT PERMITTED TO USE OUR SERVICES AND MUST CEASE DOING SO IMMEDIATELY.-->
                    <!--                    <br><br>As used herein, the terms “ApexLink”, “us,” “we,” or “our” refer to ApexLink, Inc., and the terms “you,” “your(s)” shall refer to any User of the services, namely, the subscriber and any of his/her employees, officers, agents, or other parties accessing the website or the Services by way of a subscription. If you are entering into this Agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity to this Agreement, in which case, “User” shall also refer to said entity. If you do not have the authority to act as an agent of a User entity, you may not use the Services.-->
                    <!--                        <br><br>-->
                    <!--                        1.  Your Use of the Services; Restrictions; Delivery.-->
                    <!--                    <br><br>-->
                    <!--                    1.01. ApexLink hereby grants you a revocable, nonexclusive, nontransferable, worldwide right to access and use the Services solely for the User’s own internal business purposes, subject to this Agreement and the terms of the Subscription voluntarily selected and authorized by the User. Any rights not expressly granted by this Agreement are hereby reserved by ApexLink.-->
                    <!--                    <br><br>-->
                    <!--                    1.02.You must abide by any policies, instructions, or other directions made available to you within and for the duration of your use of the Services. You may not interfere with or attempt to access our Services using a method other than the interface and instructions provided to you or reverse engineer or access the Service in order to (i) build a competitive product or service, (ii) build a product using similar ideas, features, functions or graphics of the Service, or (iii) copy any ideas, features, functions or graphics of the Service. You may only use our Services as permitted by law, including any international laws and regulations. We may suspend or stop providing our Services to you if you do not comply with our terms or policies or if we are investigating or found suspected misconduct.-->
                    <!--                    <br><br>-->
                    <!--                    1.03. Using our services does not give you ownership of any Intellectual Property Rights in our Services or the Content you access. You may not copy or use Content from our Services or modify or make derivative works based on our Services or Content unless you obtain permission in a signed writing from ApexLink or are otherwise permitted by law, in which case you must notify ApexLink at least thirty (30) days prior to such lawfully permitted use not provided for in this Agreement, to allow for verification of such use’s lawfulness. You may not remove, obscure, or alter any legal notices displayed in or along with our Services.-->
                    <!--                    <br><br>-->
                    <!--                    1.04. These terms do not grant you the right to use any branding or logos of ApexLink or those used within our Services. Unless specifically provided hereunder User has no right to (i) license, sublicense, sell, resell, transfer, assign, distribute or otherwise commercially exploit or make available to any third party the Services or the Content in any way; (ii) co-brand the Service, Content or any portion thereof (i.e., display any name, logo, trademark or other means of attribution or identification of any User in any way that suggests a partnership, affiliation, or sponsorship between ApexLink and any User or third party); or (iii) create Internet “links” to the Service or “frame” or “mirror” any Content on any other server or wireless or Internet-based device (whereby the Service or portion thereof will appear on the same screen with a portion of another website).-->
                    <!--                    <br><br>-->
                    <!--                    1.05. Conditional upon payment of the applicable subscription fees, ApexLink will provide and deliver web-based access to the Services, subject strictly to the terms of this Agreement.-->
                    <!--                    <br><br>-->
                    <!--                    <h3>2. User’s Responsibilities..</h3>-->
                    <!--                    <br><br>-->
                    <!--                    2.01. User is solely responsible for all activity occurring with regard to User’s account(s) and shall abide by all applicable local, state, national and foreign laws, treaties and regulations in connection with use of the Service, including those related to data privacy, international communications and the transmission of technical or personal data.-->
                    <!--                    <br><br>-->
                    <!--                    2.02. The license grant contained herein includes the right of the User to authorize a limited number of employees, representatives, consultants, contractors, or agents from a single organization (“Entity Users”) to utilize the selected services for internal business purposes only. Such license grant is limited by this term of use agreement, and may not be shared by more than one organization. The User is responsible for all actions taken by such Entity Users and ensuring that all Entity Users comply with these Terms of Use and all other terms of the subscription agreement, and for notifying ApexLink of any unauthorized access or use of ApexLink’s services or software. The User shall also manage their authorized Entity Users, removing or changing the status of unauthorized Entity Users as necessary, and for the immediate termination of access of any such Entity User who you believe may have violated the terms of this Agreement.-->
                    <!--                    <br><br>-->
                    <!--                    2.03. User shall keep ApexLink informed as to the User’s most current personal contact and billing information, including an active e-mail address, notifying ApexLink immediately of any changes to such information. User shall be responsible for the protection of his/her own log-in credentials and password, and must notify ApexLink immediately of any unauthorized disclosure or use of his/her password or account and any other known or suspected breaches of security. User may not use the log in credentials of or impersonate another ApexLink User or provide false identification information to ApexLink to gain access to or use the Service.-->
                    <!--                    <br><br>-->
                    <!--                    2.04. User is solely responsible for content transmitted, uploaded, or otherwise communicated to or via the Services (“User Content”), including all visual, written, or audible communications by User through the Services, and all personal and business-related data (“User Data”). User agrees not to use the Services to communicate any message or material that is harassing, libelous, threatening, obscene, indecent, violating of the intellectual property rights of any party, or is otherwise unlawful under any applicable law or regulation, domestic or foreign. User must use best efforts to immediately cease any copying or distributing of content that is known or suspected by User to violate any provision of this Agreement or of ApexLink’s Privacy Policy. User further agrees to cooperate with ApexLink in causing any unauthorized activity to immediately cease.-->
                    <!--                    <br><br>-->
                    </p>

                </div>
            </div>
        </div>
</div>
</section>
<?php include_once(COMPANY_DIRECTORY_URL."/views/layouts/admin_footer.php"); ?>
</body>

</html>