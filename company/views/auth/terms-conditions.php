<!DOCTYPE html>
<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
?>

<?php
include_once(COMPANY_DIRECTORY_URL. "/views/layouts/admin_header.php");
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/css/intlTelInput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/redmond/jquery-ui.min.css">
<div id="wrapper">
    <header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="logo-terms">
                    <div class="login-logo-terms"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo-login.png"/></div>
            </div>
        </div>
    </div>
    </header>
    <!-- Top navigation end -->
    <!--Content Start-->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 terms-out">

                    <div class="terms-content">
                        <h1 class="term-head-red">ApexLink Terms of Use</h1>
                        <h4 class="term-head-green">Updated: May 20, 2020.</h4>
                        <p>Thank you for using our website, products, and services (“Services”). The Services are provided by ApexLink, Inc. (“ApexLink”), a Florida corporation. ApexLink reserves the right to amend or change the terms of this Agreement from time to time with or without notice to you. It is your sole responsibility to access and review these terms regularly.</p>
                    <p>PLEASE READ THIS AGREEMENT CAREFULLY BEFORE USING THE SERVICES. BY REGISTERING FOR AN ACCOUNT, SUBSCRIBING TO OUR SERVICES, ACCESSING OUR WEBSITE, OR BY EXECUTING A SEPARATE WRITTEN AGREEMENT WITH APEXLINK, THE USER EXPRESSLY AGREES TO THE FOLLOWING TERMS OF SERVICE GOVERNING THE USE OF APEXLINK’S SERVICES, AS WELL AS OUR PRIVACY POLICY. APEXLINK’S CONSENT TO YOUR USE OF THE SERVICES IS STRICTLY CONDITIONED ON YOUR CONSENT TO ALL OF THE TERMS AND CONDITIONS CONTAINED IN THIS AGREEMENT. IF YOU DO NOT CONSENT TO ALL TERMS YOU ARE NOT PERMITTED TO USE OUR SERVICES AND MUST CEASE DOING SO IMMEDIATELY.</p>
                    <p>As used herein, the terms “ApexLink”, “us,” “we,” or “our” refer to ApexLink, Inc., and the terms “you,” “your(s)” shall refer to any User of the services, namely, the subscriber and any of his/her employees, officers, agents, or other parties accessing the website or the Services by way of a subscription. If you are entering into this Agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity to this Agreement, in which case, “User” shall also refer to said entity. If you do not have the authority to act as an agent of a User entity, you may not use the Services.</p>
<!--                    point-1-->
                        <h4 class="term-head-blue">1. Your Use of the Services; Restrictions; Delivery.</h4>
                    </div>

                </div>
            </div>
            </div>
        </div>
    </section>
<?php include_once(COMPANY_DIRECTORY_URL."/views/layouts/admin_footer.php"); ?>
</body>

</html>