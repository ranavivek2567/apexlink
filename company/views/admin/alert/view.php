<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_header.php");
?>
<!-- Wrapper Starts -->
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/top_navigation.php");
    ?>
    <!-- MAIN Navigation Ends -->


    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="welcome-text visible-xs">
                        <div class="welcome-text-inner">
                            <div class="col-xs-9">    Welcome:  Sonny Kesseben (ACL Properties), June 01, 2018
                            </div>
                            <div class="col-xs-3">
                                <a herf="javascript:;"><i class="fa fa-calendar" aria-hidden="true"></i></a>
                                <a href="javascript:;"><i class="fa fa-calculator" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="breadcrumb-outer">
                                        Alerts >> <span>User Alerts</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span> Tenant Portal Alerts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlert">
                                                                        <th width="20%" align="left" scope="col">Tenant Portal Alerts</th>
                                                                        <th width="15%" align="left" scope="col">Users</th>
                                                                        <th width="15%" align="left" scope="col">Send Alerts</th>
                                                                        <th width="25%" align="left" scope="col">Description</th>
                                                                        <th width="7%"  align="left" scope="col">Status</th>
                                                                        <th width="18%" align="left" scope=col">Actions</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="useralert">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span> Owner Portal Alerts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlertOwner">
                                                                        <th width="20%" align="left" scope="col">Owner Portal Alerts</th>
                                                                        <th width="15%" align="left" scope="col">Users</th>
                                                                        <th width="15%" align="left" scope="col">Send Alerts</th>
                                                                        <th width="25%" align="left" scope="col">Description</th>
                                                                        <th width="7%" align="left" scope="col">Status</th>
                                                                        <th width="18%" align="left" scope="col">Actions</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="useralertowner">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span> Vendor Portal Alerts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlertVendor">
                                                                        <th width="20%" align="left" scope="col">Vendor Portal Alerts</th>
                                                                        <th width="15%" align="left" scope="col">Users</th>
                                                                        <th width="15%" align="left" scope="col">Send Alerts</th>
                                                                        <th width="25%" align="left" scope="col">Description</th>
                                                                        <th width="7%" align="left" scope="col">Status</th>
                                                                        <th width="18%" align="left" scope="col">Actions</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="useralertvendor">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span>Lease Alerts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlertLease">
                                                                        <th width="20%" align="left" scope="col">Lease Alerts</th>
                                                                        <th width="15%" align="left" scope="col">Users</th>
                                                                        <th width="15%" align="left" scope="col">Send Alerts</th>
                                                                        <th width="25%" align="left" scope="col">Description</th>
                                                                        <th width="7%" align="left" scope="col">Status</th>
                                                                        <th width="18%" align="left" scope="col"> Actions </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="useralertlease">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span>Maintenance Alerts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlertMaintenance">
                                                                        <th width="20%" align="left" scope="col">Payment Alerts</th>
                                                                        <th width="15%" align="left" scope="col">Users</th>
                                                                        <th width="15%" align="left" scope="col">Send Alerts</th>
                                                                        <th width="25%" align="left" scope="col">Description</th>
                                                                        <th width="7%" align="left" scope="col">Status</th>
                                                                        <th width="18%" align="left" scope="col"> Actions </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="useralertmaintenance">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span>Lead Alerts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlertLead">
                                                                        <th width="20%" align="left" scope="col">Payment Alerts</th>
                                                                        <th width="15%" align="left" scope="col">Users</th>
                                                                        <th width="15%" align="left" scope="col">Send Alerts</th>
                                                                        <th width="25%" align="left" scope="col">Description</th>
                                                                        <th width="7%" align="left" scope="col">Status</th>
                                                                        <th width="18%" align="left" scope="col"> Actions </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="useralertlead">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span>Payment Alerts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlertPayment">
                                                                        <th width="20%" align="left" scope="col">Payment Alerts</th>
                                                                        <th width="15%" align="left" scope="col">Users</th>
                                                                        <th width="15%" align="left" scope="col">Send Alerts</th>
                                                                        <th width="25%" align="left" scope="col">Description</th>
                                                                        <th width="7%" align="left" scope="col">Status</th>
                                                                        <th width="18%" align="left" scope="col"> Actions </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="useralertpayment">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span>Communication Alerts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlertCommunication">
                                                                        <th width="20%" align="left" scope="col">Communication Alerts</th>
                                                                        <th width="15%" align="left" scope="col">Users</th>
                                                                        <th width="15%" align="left" scope="col">Send Alerts</th>
                                                                        <th width="25%" align="left" scope="col">Description</th>
                                                                        <th width="7%" align="left" scope="col">Status</th>
                                                                        <th width="18%" align="left" scope="col"> Actions </th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="useralertcommunication">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="panel-body pad-none">
                                        <div class="grid-outer">
                                            <div class="apx-table">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                            <span class="pull-right"></span>Business Alerts</a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                    <div class="panel-body user-alerts">
                                                        <div class="grid-outer">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover table-dark">
                                                                    <thead>
                                                                    <tr class="clsUserAlertBusiness">
                                                                        <th width="20%" align="left" scope="col">Business Alerts</th>
                                                                        <th width="15%" align="left" scope="col">Users</th>
                                                                        <th width="15%" align="left" scope="col">Send Alerts</th>
                                                                        <th width="25%" align="left" scope="col">Description</th>
                                                                        <th width="7%" align="left" scope="col">Status</th>
                                                                        <th width="18%" align="left" scope="col">Actions</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody class="useralertbusiness">
                                                                    </tbody>
                                                                </table>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- start custom field model -->
                            <div class="container">
                                <div class="modal fade useralertmodal" id="useralertmodel" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content" style="width: 100%;">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">×</button>
                                                <h4 class="modal-title">Edit Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-outer form-outer2">
                                                    <form id="update_alert_form">
                                                        <div class="fr" style="float:right;">
                                                            <input type="hidden" id="hiddenAlertId" spellcheck="true" autocomplete="off" value="1">
                                                            <input type="hidden" id="hiddenAlertType" spellcheck="true" autocomplete="off">
                                                            *Required Fields
                                                        </div>
                                                        <div class="row custom_field_form">
                                                            <div class="custom_field_row">
                                                                <div class="col-sm-2 text-right">
                                                                    <label>Alert Name:</label>
                                                                </div>
                                                                <div class="col-sm-9 value-lh field_alert_name">

                                                                </div>
                                                            </div>
                                                            <div class="custom_field_row">
                                                                <div class="col-sm-2 text-right">
                                                                    <label>Description:</label>
                                                                </div>
                                                                <div class="col-sm-9 value-lh field_alert_description">

                                                                </div>
                                                            </div>
                                                            <div class="custom_field_row">
                                                                <div class="col-sm-2 text-right">
                                                                    <label>Status <em class="red-star">*</em></label>
                                                                </div>
                                                                <div class="col-sm-9 field_alert_status">
                                                                    <div class="row">
                                                                        <div class="col-sm-4">
                                                                            <select id="status" class="fm-txt form-control" name="status">
                                                                                <option selected="selected" value="">Select</option>
                                                                                <option value="1">Active</option>
                                                                                <option value="0">InActive</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="custom_field_row">
                                                                <div class="col-sm-2 text-right">
                                                                    <label>To Be Sent <em class="red-star">*</em></label>
                                                                </div>
                                                                <div class="col-sm-9 no_of_days_before">
                                                                    <div class="row">
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control to_be_sent" type="text" maxlength="100" id="no_of_days_before" name="no_of_days_before" placeholder="Alert New Guest Card">
                                                                            <span class="required error"></span>
                                                                        </div>
                                                                        <div class="days-before">Days Before</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="custom_field_row">
                                                                <div class="col-sm-2 text-right">
                                                                    <label>Subject <em class="red-star">*</em></em></label>
                                                                </div>
                                                                <div class="col-sm-9 field_alert_subject">
                                                                    <div class="row">
                                                                        <div class="col-sm-7">
                                                                            <input class="form-control" type="text" maxlength="100" id="alert_subject" name="subject" placeholder="">
                                                                            <span class="required error"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="custom_field_row">
                                                                <div class="col-sm-2 text-right">
                                                                    <label>Send to <em class="red-star">*</em></em></label>
                                                                </div>
                                                                <div class="col-sm-9 value-lh field_send_to">
                                                                    <input type="checkbox" class="checkbox_check" name="send_to_users" value="" id="checkbox1">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="btn-outer">
                                                            <input type="hidden" value="" name="alert_edit_id" class="form-control" id="alert_edit_id"/>
                                                            <button type="submit" value="save" class="blue-btn" id='saveUserAlert'>Save</button>
                                                            <button type="button" class="grey-btn" data-dismiss="modal" id="userAlertCancel">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="modal fade" id="useralertnamemodel" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">×</button>
                                                <h4 class="modal-title">Preview Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-outer2 col-sm-12">
                                                    <form id="custom_field">
                                                        <input type="hidden" name="id" id="custom_field_id" value="">
                                                        <div class="row custom_field_form">
                                                            <div class="custom_field_row">
                                                                <div class="col-sm-2 text-right">
                                                                    <label>Alert Name:</label>
                                                                </div>
                                                                <div class="col-sm-9 field_alert_name2">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="modal fade" id="useralerttestmail" role="dialog">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">×</button>
                                                <h4 class="modal-title">Test Mail Alert</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-outer2 col-sm-12">
                                                    <form id="useralert_testmail">
                                                        <input type="hidden" name="id" id="custom_field_id" value="">
                                                        <div class="row custom_field_form">
                                                            <div class="custom_field_row">
                                                                <div class="col-sm-2 text-right">
                                                                    <label>Send To: <em class="red-star">*</em></label>
                                                                </div>
                                                                <div class="col-sm-6 field_alert_testmail">
                                                                    <input class="form-control emailtest" type="text" maxlength="100" id="email" name="email" placeholder="Email">
                                                                    <input type="hidden" value="" name="module_type" id="module_type">
                                                                    <input type="hidden" value="" name="alert_name" id="test_alert_name">
                                                                    <span class="required error"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="btn-outer">
                                                            <input type="hidden" value="" name="alert_edit_id" class="form-control" id="alert_edit_id"/>
                                                            <button type="submit" value="save" class="blue-btn" id='testMailSave'>Send</button>
                                                            <button type="button" class="grey-btn" data-dismiss="modal" id="testMailCancel">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav7').addClass('in');
    $('.alertsetup').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->

    $(document).ready(function(){        
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/userAlerts.js"></script>
<!--<script src="--><?php //echo COMPANY_SUBDOMAIN_URL; ?><!--/js/validation/propertytype/propertytype.js"></script>-->
<!-- Jquery Starts -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>