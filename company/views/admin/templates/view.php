<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<body>
<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end --> <!-- MAIN Navigation Ends -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>
                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="breadcrumb-outer">
                                        Templates >> <span>Email Template</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <div id="addAmenitiesDivId" style="display: none;">
                                                </div>

                                                <!--List Amenities div ends here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="panel-heading">
                                                                        <!--<h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#">
                                                                                <span class="pull-right"></span> List of Templates</a>
                                                                        </h4>-->
                                                                    </div>
                                                                    <div class="accordion-grid">
                                                                        <div class="accordion-outer">
                                                                            <div class="bs-example">
                                                                                <div class="panel-group" id="accordion">
                                                                                    <div class="panel panel-default">
                                                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                                                            <div class="panel-body pad-none">
                                                                                                <div class="grid-outer">
                                                                                                    <div class="apx-table">
                                                                                                        <div class="table-responsive">
                                                                                                            <table id="emailTemplates-table" class="table table-bordered"></table>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!--List Amenities div ends here-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Content Data Ends ---->
                                </div>
                            </div>
                        </div>
                    </div>
    </section>
</div>
<!-- Wrapper Ends -->

<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
<!-- Footer Ends -->
<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav9').addClass('in');
    $('.template').addClass('active');
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/trashBin/trashAmenities.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/adminUsers/Template/template_email.js"></script>
</body>
