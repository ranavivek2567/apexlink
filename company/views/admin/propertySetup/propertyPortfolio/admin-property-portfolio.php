<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <?php
    include_once(COMPANY_DIRECTORY_URL. "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php"); ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Property Setup >> <span>Portfolio</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status">
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="btn-outer text-right">
                                            <button id="add_portfolio" class="blue-btn" >Add Portfolio</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <form id="addPortfolioForm">
                                    <div id="portfolio_add_div" style="display:none;">
                                        <div class="form-outer">
                                            <div class="form-hdr">
                                                <h3 id="headerDiv">
                                                    Add Portfolio
                                                </h3>
                                            </div>
                                            <div class="form-data">
                                                <div class="row">
                                                        <input type="hidden" name="id" id="record_id">
                                                        <div class="col-sm-6 col-md-4">
                                                            <label>Portfolio ID <em class="red-star">*</em></label>
                                                            <input class="form-control" name="portfolio_id" id="portfolio_id" placeholder="Eg: AZXR4L" maxlength="100" type="text" value=""/>
                                                        </div>
                                                        <div class="col-sm-6 col-md-4">
                                                            <label>Name <em class="red-star">*</em></label>
                                                            <input class="form-control" name="portfolio_name" id="portfolio_name" maxlength="500" placeholder="Eg: Garden Style" type="text"/>
                                                        </div>
                                                        <div class="col-sm-6 col-md-4">

                                                            <div class="check-outer">
                                                                <input name="is_default" id="is_default" type="checkbox"/>
                                                                <label>Set as Default</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="collaps-subhdr">
                                                                <h3>Custom Fields</h3>
                                                                <div class="custom_field_html">
                                                                </div>
                                                                <div class="form-outer2">
                                                                <div class="row">
                                                                    <div class="col-sm-6 custom_field_msg">
                                                                        No Custom Fields
                                                                    </div>
                                                                    <div class="col-sm-6 pull-right">
                                                                        <div class=" text-right">
                                                                            <button type="button" id="add_custom_field" data-toggle="modal" data-backdrop="static" data-target="#myModal" class="blue-btn">Add Custom Field</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                </div>
                                            </div>
                                                <div class="btn-outer text-right">
                                                    <button class="blue-btn" type="submit" id="saveBtnId">Save</button>
                                                    <button class="clear-btn clearFormReset" type="button" >Clear</button>
                                                    <button class="grey-btn" type="button" id="cancelPortfolio_add">Cancel</button>
                                                </div>
                                        </div>
                                    </div>



                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">

                                                    <div class="panel panel-default">
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <div class="panel-body pad-none">
                                                                <div class="grid-outer">
                                                                    <div class="apx-table">
                                                                        <div class="table-responsive">
                                                                            <table id="portfolio-table" class="table table-bordered">
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!-- Content Data Ends ---->
                </div>
            </div>
        </div>
</div>

<!-- start custom field model -->
<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Custom Field</h4>
                </div>
                <div class="modal-body" style="height: 380px;">
                    <div class="form-outer col-sm-12">
                        <form id="custom_field">
                            <input type="hidden" id="customFieldModule" name="module" value="portfolio">
                            <input type="hidden" name="id" id="custom_field_id" value="">
                            <div class="row custom_field_form">
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Field Name <em class="red-star">*</em></label>
                                    </div>
                                    <div class="col-sm-9 field_name">
                                        <input class="form-control" type="text" maxlength="100" id="field_name" name="field_name" placeholder="">
                                        <span class="required error"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Data Type</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="form-control data_type" id="data_type" name="data_type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="currency">Currency</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="url">URL</option>
                                            <option value="date">Date</option>
                                            <option value="memo">Memo</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Default value</label>
                                    </div>
                                    <div class="col-sm-9 default_value">
                                        <input class="form-control default_value" id="default_value" type="text" name="default_value" placeholder="">
                                        <span class="error required"></span>
                                    </div>
                                </div>
                                <div class="custom_field_row">
                                    <div class="col-sm-3">
                                        <label>Required Field</label>
                                    </div>
                                    <div class="col-sm-9 is_required">
                                        <select class="form-control" name="is_required" id="is_required">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="selected">No</option>
                                        </select>
                                        <span class="error required"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-outer text-right">

                                <button type="submit" class="blue-btn" id='saveCustomField'>Save</button>
                                <button type="button" class="grey-btn" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End custom field model -->
</section>
</div>
<!-- Wrapper Ends -->



<!-- Footer Ends -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    $('#leftnav2').addClass('in');
    $('.portfolio').addClass('active');

    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#addPortfolioForm",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#addPortfolioForm",['portfolio_id'],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/portfolio.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/custom_fields.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/propertySetup/portfolio.js"></script>
<style>
    .row.custom_field_class input {
        width: 258px;
    }
</style>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>