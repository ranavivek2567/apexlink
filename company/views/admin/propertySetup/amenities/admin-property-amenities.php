<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end --> <!-- MAIN Navigation Ends -->
    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>
                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="breadcrumb-outer">
                                        Property Setup >> <span>Amenities</span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <div class="property-status">
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="btn-outer text-right">
                                            <button id="addAmenitiesBtnId" class="blue-btn">Add Amenities</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <div id="addAmenitiesDivId" style="display: none;">
                                    <div class="form-outer">
                                        <div class="form-hdr">
                                            <h3 id="headerDiv"> New Amenity</h3>
                                        </div>
                                        <div class="form-data">
                                            <form name="addAmenity" id="addAmenityFormId">
                                                <div class="row">
                                                        <div class="col-sm-6 col-md-4">
                                                            <label>Amenity Name <em class="red-star">*</em></label>
                                                            <input id="amenity_name" name="name" maxlength="100" placeholder="Eg: Ceiling Fan" class="form-control" type="text"/>
                                                            <span id="nameErr" class="error"></span>
                                                        </div>
                                                        <div class="col-sm-6 col-md-4">
                                                            <label>Amenity Code</label>
                                                            <input id="code" name="code" maxlength="10" placeholder="Eg: 121" class="form-control" type="text"/>
                                                            <span id="codeErr" class="error"></span>
                                                        </div>
                                                        <div class="col-sm-6 col-md-4">
                                                            <label>Select Type <em class="red-star">*</em></label>
                                                            <div class="check-outer">
                                                                <input id="amenity1" class="type_checkbox" value="1" name="type" type="checkbox"/>
                                                                <label>Property</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input id="amenity2" class="type_checkbox" value="2" name="type" type="checkbox"/>
                                                                <label>Building</label>
                                                            </div>
                                                            <div class="check-outer">
                                                                <input id="amenity3" class="type_checkbox" value="3" name="type" type="checkbox"/>
                                                                <label>Unit</label>
                                                            </div>
                                                            <div style="margin-top: 30px;">
                                                                <span id="typeErr"  class="error"></span>
                                                            </div>
                                                        </div>

                                                </div>

                                        </div>
                                        <div class="btn-outer text-right">
                                            <input class="form-control" type="hidden" name="hidden_amenity_id" id="hidden_amenity_id"/>
                                            <input class="blue-btn" type="submit" value="Save" id="saveAmenityBtnId"/>
                                            <button class="clear-btn clearFormReset">Clear</button>
                                            <button id="addAmenityCancelBtn" class="grey-btn">Cancel</button>
                                        </div>

                                        </form>
                                    </div>
                                    <!-- Form Outer Ends-->

                                </div>

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">

                                                <!--List Amenities div ends here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="Amenities-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List Amenities div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav2').addClass('in');
    $('.amenity').addClass('active');
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#addAmenityFormId",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#addAmenityFormId",[],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/propertySetup/amenities.js"></script>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>
