<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php

?>

<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>
<!-- HTML Start -->
<body>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="row">
                                <!--                                <div class="col-sm-8">-->
                                <!--                                    <div class="breadcrumb-outer">-->
                                <!--                                        Entity/Company Setup >> <span>Existing Plans</span>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                                <div class="col-sm-4 pull-right">
                                    <div class="easy-search">
                                        <input placeholder="Easy Search" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content-data apxpg-allcontent existplan_outer">
                            <div class="main-tabs apx-tabs">
                                <div class="tab-content listing_users">
                                    <div role="tabpanel" class="" id="existPlan_tab">

                                        <div class="accordion-form">
                                            <div class="accordion-grid">
                                                <div class="accordion-outer">
                                                    <div class="bs-example">
                                                        <div class="panel-group" id="accordion">
                                                            <div class="panel panel-default">
                                                                <div id="collapseOne" class="panel-collapse collapse  in">
                                                                    <div class="panel-body pad-none">
                                                                        <div class="grid-outer">
                                                                            <!--Apex table-->
                                                                            <div class="apx-table apx-paymenttable">
                                                                                <div class="table-responsive">
                                                                                    <table id="existPlan-table" class="table table-bordered nowrap"></table>
                                                                                </div>
                                                                            </div>
                                                                            <!--End Apex Table-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-outer existPlan_buttons">
<!--                                        <a href="javascript:;">-->
<!--                                            <input type="button" class="blue-btn renew_existPlan" value="Renew Plan">-->
<!--                                        </a>-->
                                        <!--<button class="grey-btn">Cancel</button>-->
                                        <a href="javascript:;">
                                            <input type="button" class="blue-btn upgrade_existPlan" value="Upgrade Plan">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="renew_plan_data_div" style="display: none;">

                            <form name="form_renew_plan_data" id="form_renew_plan_data" method="post">
                                <div class="content-data apxpg-allcontent">
                                    <div class="form-outer apx-adformbox">
                                        <div class="form-hdr apx-adformbox-title">
                                            <h3>Subscription Summary</h3>
                                        </div>
                                        <div class="form-data renew_plan_data-outer apx-adformbox-content">
                                            <div class="col-sm-12">
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0)"  id="disable_remove">
                                                        <img src="<?php echo SUPERADMIN_SITE_URL; ?>/images/home.png ?>" alt="">
                                                    </a>
                                                </div>
                                                <div class="col-sm-8 renew_plan_data">
                                                    <h5  id=""><b></b></h5>
                                                    <table>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p>Propertyware start date : <span class="lblStartDate"></span></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p>Propertyware Expire date : <span class="lblExpireDate"></span></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p>Subscription Fee : <span class="spnPlanDollar"></span></p>
                                                            </td>
                                                        </tr>

                                                        </tbody></table>

                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="greyhdr-box">
                                                    <div class="greyhdr-box-top">
                                                        <h2>
                                                            Total Balance Due
                                                        </h2>
                                                        <a href="javascript:;"></a>
                                                    </div>
                                                    <div class="greyhdr-box-bot">
                                                        <label>
                                                            Total Unpaid
                                                        </label>
                                                        <span class="planTermAmmout"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-outer apx-adformbox">
                                        <div class="form-hdr apx-adformbox-title">
                                            <h3>
                                                Renew Plan Information
                                            </h3>
                                        </div>
                                        <div class="form-data apx-adformbox-content">
                                            <div class="row">
                                                <div class="form-outer">
                                                    <div class="col-sm-12 col-md-12">
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>New Plan</label>
                                                            <span>
                                                            <select name="subscription_plan" id="NewPlanName" style="cursor: pointer;" readonly="" class="form-control renewPlanName" name="subscription_plan">
                                                                <option value="">Select</option>
                                                                <option value="1" units="1-25" noofunitid="1" pricepunit="1" price="75">1-25 Plan</option>
                                                                <option value="2" units="1-50" noofunitid="2" pricepunit="1" price="75">26-50 Plan</option>
                                                                <option value="3" units="1-100" noofunitid="3" pricepunit="1" price="75">51-100 Plan</option>
                                                                <option value="4" units="1-200" noofunitid="4" pricepunit="1" price="75">101-200 Plan</option>
                                                                <option value="5" units="1-300" noofunitid="5" pricepunit="1" price="75">201-300 Plan</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>No.Of Unit</label>
                                                            <span>
                                                            <input type="text" name="no_of_units" id="NoOfUnit" style="cursor: pointer;" spellcheck="true" readonly class="form-control renewNoFUnits" name="no_of_units">
                                                        </span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>
                                                                Payment Type
                                                            </label>
                                                            <span>
                                                            <select name="term_plan" id="Paymenttype" style="cursor: pointer;" readonly class="form-control renewPaymentType" name="term_plan">
                                                                <option value="1">Monthly</option>
                                                                <option value="2">Quarterly</option>
                                                                <option value="3">Half-Yearly</option><option value="4">Yearly</option>
                                                            </select>
                                                        </span>
                                                        </div>
                                                        <div class="col-sm-3 col-md-3">
                                                            <label>Plan Price</label>
                                                            <span>
                                                                <input type="text" name="plan_price" style="cursor: pointer;" spellcheck="true" readonly class="form-control renewPlanPrice" name="plan_price">

                                                        </span>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-12 col-md-12">
                                                        <div class="col-sm-3 col-md-3 ">
                                                            <label>
                                                                Pay Plan Price
                                                            </label>
                                                            <span>
                                                            <input type="text" name="pay_plan_price" id="lblTotalammount" style="cursor: pointer;" spellcheck="true" readonly class="form-control renewPayPlanPrice" name="	pay_plan_price">
                                                        </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-outer apx-adformbox">
                                        <div class="form-hdr apx-adformbox-title">
                                            <h3>
                                                <strong class="left">
                                                    Renew Plan
                                                </strong>
                                            </h3>
                                        </div>
                                        <div class="form-data apx-adformbox-content">

                                            <div class="col-sm-3 col-md-3 ">
                                                <label>Payment Method</label>
                                                <select id="country_code" class="form-control" name="">
                                                    <option value="">Select</option>
                                                    <option value="2">Credit/Debit Card</option>
                                                    <option value="1">ACH</option>
                                                </select>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="btn-outer">
                                        <a href="javascript:;">
                                            <input type="submit" name="submit" class="blue-btn SavePlans" value="Save">
                                        </a>
                                        <!--<button class="grey-btn">Cancel</button>-->
                                        <a href="javascript:;">
                                            <input type="button" class="grey-btn cancel_existPlan" value="Cancel">
                                        </a>
                                    </div>
                            </form>
                                </div>


                        </div>
                        <div class="upgrade_plan_data_div" style="display: none;">
                            <form id="form_upgrade_plan_data" method="post">
                            <div class="content-data apxpg-allcontent">
                                <div class="apx-adformbox">
                                    <div class="form-hdr apx-adformbox-title">
                                        <h3>Subscription Summary</h3>
                                    </div>
                                    <div class="form-data renew_plan_data-outer apx-adformbox-content" style="margin-bottom: 10px;">
                                        <div class="col-sm-12">
                                            <div class="col-sm-1">
                                                <a href="javascript:void(0)"  id="disable_remove">
                                                    <img src="<?php echo SUPERADMIN_SITE_URL; ?>/images/home.png ?>" alt="">
                                                </a>
                                            </div>
                                            <div class="col-sm-8 renew_plan_data">
                                                <h5  id="lblSubscriptionType"><b></b></h5>
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <p>Subscription Start Date  : <span class="lblStartDate"></span></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p>Subscription Expire Date : <span class="lblExpireDate"></span></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p>Subscription Fee : <span class="spnPlanDollar"></span></p>
                                                        </td>
                                                    </tr>

                                                    </tbody></table>

                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="greyhdr-box">
                                                <div class="greyhdr-box-top">
                                                    <h2>
                                                        Total Balance Due
                                                    </h2>
                                                    <a href="javascript:;"></a>
                                                </div>
                                                <div class="greyhdr-box-bot">
                                                    <label>
                                                        Total Unpaid
                                                    </label>
                                                    <span class="planTermAmmout"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="apx-adformbox form-outer">
                                    <div class="form-hdr apx-adformbox-title">
                                        <h3>
                                            <strong class="left">
                                                Upgrade Plan Information
                                            </strong>
                                        </h3>
                                    </div>

                                    <div class="form-data apx-adformbox-content">
                                        <div class="row">
                                            <div class="form-outer">
                                                <div class="col-sm-12 col-md-12">
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>New Plan  <em class="red-star">*</em></label>
                                                        <span>
                                                            <select name="subscription_plan" class="form-control UpgradeNewPlanName" id="NewPlanName" style="cursor: pointer;">

                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>No.Of Unit</label>
                                                        <span id="unitInput">
                                                            <input type="text" name="no_of_units"  style="cursor: pointer;" spellcheck="true" class="form-control UpgradeNewPlanUnits" readonly>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>
                                                            Payment Type  <em class="red-star">*</em>
                                                        </label>
                                                        <span>
                                                            <input type="hidden" value="" id="UsersdiscountPrice">
                                                            <select name="term_plan"  style="cursor: pointer;" class="form-control UpgradeNewTermPlan">
                                                                <option value="1">Monthly</option>
                                                                <option value="4">Yearly</option>
                                                            </select>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <label>Plan Price  <em class="red-star">*</em></label>
                                                        <span>
                                                                 <input type="text" name="plan_price" id="NoOfUnit" style="cursor: pointer;" spellcheck="true" class="form-control UpgradeNewPlanPrice" readonly>

                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-md-12">

                                                    <div class="col-sm-3 col-md-3 ">
                                                        <label>
                                                            Pay Plan Price
                                                        </label>
                                                        <span>
                                                            <input type="text" name="pay_plan_price" id="lblTotalammount" style="cursor: pointer;" spellcheck="true"  class="form-control UpgradeNewPayPlanPrice" readonly>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-outer apx-adformbox">
                                    <div class="form-hdr apx-adformbox-title">
                                        <h3>
                                            <strong class="left">
                                                Payment Method
                                            </strong>
                                        </h3>
                                    </div>
                                    <div class="form-data apx-adformbox-content">
                                        <div class="check-outer amenitieschck-radio">
                                            <input  id="credit_card" type="radio" name="card" value="card">
                                            <label>Credit Card</label>
                                        </div>
                                        <div class="check-outer amenitieschck-radio">
                                            <input  id="ach" type="radio" name="card" value="ach">
                                            <label>ACH</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-outer text-right">
                                    <a href="javascript:;">
                                        <input type="submit" class="blue-btn SavePlansUpgrade" value="Save">
                                    </a>
                                    <a href="javascript:;">
                                        <input type="button" class="clear-btn clearFormReset" value="Clear">
                                    </a>
                                    <!--<button class="grey-btn">Cancel</button>-->
                                    <a href="javascript:;">
                                        <input type="button" class="grey-btn cancel_upgradePlan" value="Cancel">
                                    </a>
                                </div>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>
<!-- Wrapper Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>

<!-- Jquery Starts -->

<script>

    $('#leftnav1').addClass('in');
    $('.existing_plan').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

</script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payment/existingPlan.js"></script>

<script>
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $(document).ready(function(){
        $(".existplan_outer .renew_existPlan").click(function(){
            $(".renew_plan_data_div").css('display','block');
            $(".existplan_outer .listing_users").css('display','none');
        });
        $(".existplan_outer .upgrade_existPlan").click(function(){
            $(".upgrade_plan_data_div").css('display','block');
            $(".existplan_outer .listing_users").css('display','none');
        });
        $(".cancel_existPlan").click(function(){
            $(".renew_plan_data_div").css('display','none');
            $(".existplan_outer .listing_users").css('display','block');
        });
        $(".cancel_upgradePlan").click(function(){
            $(".upgrade_plan_data_div").css('display','none');
            $(".existplan_outer .listing_users").css('display','block');
        });

    });
</script>
<!-- Jquery Ends -->

<script>

    $(document).ready(function(){

        /**
         * Function for view payment on clicking row
         * */

        $(document).on('click','#existPlan-table tr td:not(:last-child)',function(){
            var id = $(this).closest('tr').attr('id');
            window.location.href = '/User/UserPlans'+id;
        })


        $('.existPlan-top').addClass('active');
        $(document).on('click','.clearFormReset',function () {
            resetFormClear("#form_upgrade_plan_data",['subscription_plan','term_plan']);
        });

    });
</script>

</body>
