<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<style>
    .holder_name
{
    text-transform:capitalize;
}

label.error {
color: red !important;
   font-size: 12px;
    font-weight: 500;
    width: 100% !important;
}

</style>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>
<div id="wrapper">
    <?php
    include_once(COMPANY_DIRECTORY_URL. "/views/layouts/top_navigation.php");
    ?>
    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/sidebar.php");
                ?>
                <div class="col-sm-8 col-md-10 main-content-rt">

                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Entity/Company Setup >> <span>Account Setup</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="content-data">

                            <!-- Main tabs -->
                            <div class="main-tabs">
                                <form id="companySetup">
                                    <div class="accordion-form">
                                        <div class="accordion-outer">
                                            <div class="bs-example">
                                                <div class="panel-group" id="accordion">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a class="acord" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fa fa-angle-down" aria-hidden="true"></i></span> Entity/Company Info</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse  in">
                                                            <div class="panel-body">
                                                                <div class="row">
                                                                    <div class="form-outer">
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <label>Entity/Company Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control" name="company_name" id="company_name" placeholder="Eg: ApexLink" type="text" value="<?php echo $company_name ?>"/>
                                                                                    <span class="company_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>First Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control" name="first_name" id="first_name" placeholder="Eg: Jason" type="text" value="<?php echo $first_name ?>"/>
                                                                                    <span class="first_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>Middle Name</label>
                                                                                    <input class="form-control" name="middle_name" id="middle_name" type="text" placeholder="Eg: Statham" value="<?php echo $middle_name ?>"/>
                                                                                    <span class="middle_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>Last Name <em class="red-star">*</em></label>
                                                                                    <input class="form-control" name="last_name" id="last_name" placeholder="Eg: Holder" type="text" value="<?php echo $last_name ?>"/>
                                                                                    <span class="last_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>Maiden Name</label>
                                                                                    <input class="form-control" name="maiden_name" id="maiden_name" type="text" placeholder="Eg: Statham" value="<?php echo $maiden_name ?>"/>
                                                                                    <span class="maiden_nameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>Nick Name</label>
                                                                                    <input class="form-control" name="nick_name" id="nickname" type="text" placeholder="Eg: Dom" value="<?php echo $nick_name ?>"/>
                                                                                    <span class="nicknameErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>No. Of Units <em class="red-star">*</em></label>
                                                                                    <input disabled="" name="number_of_units" id="no_of_units" class="form-control" type="text" value="<?php echo $number_of_units ?>"/>
                                                                                    <span class="no_of_unitsErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>Plan Size <em class="red-star">*</em></label>
                                                                                    <input disabled="" name="plan_size" id="plan_size" class="form-control" type="text" value="<?php echo $plan_size ?>"/>
                                                                                    <span class="plan_sizeErr error red-star"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <label>Website </label>
                                                                                    <input class="form-control" name="website" id="website" placeholder="Eg: https://apexlink.com" type="text" value="<?php echo $website ?>" />
                                                                                    <span class="websiteErr error red-star"></span>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>Entity/Company Logo </label>
                                                                                    <div class="upload-logo">
                                                                                        <div class="img-outer" >
                                                                                            <?php if($company_logo == ''){?>
                                                                                            <img id="com_logo" height='70' width="70" src="<?php echo COMPANY_SUBDOMAIN_URL.'/images/logo.png'?>"/>
                                                                                            <input type="hidden" name="no_logo" id="no_logo" value="/images/logo.png" />
                                                                                            <?php } else { ?>
                                                                                                <img id="com_logo" src="<?php echo COMPANY_SUBDOMAIN_URL."/".$company_logo ?>" height='70' width="70"/>
                                                                                                <input type="hidden" name="previous_logo" id="previous_logo" value="<?php echo $company_logo ?>" />
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                        <a id="logo_upload" href="javascript:;"><i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Logo</a>
                                                                                        <span>(Maximum File Size Limit: 1MB)</span>
                                                                                        <input type="file" class="img_upload" name="company_logo" id="company_logo" data="previous_logo" style="display: none;" />
                                                                                        <span class="company_logoErr error red-star"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-12">
                                                                                    <label>Upload Signature </label>
                                                                                    <div class="upload-logo">
                                                                                        <div class="img-outer">
                                                                                            <?php if($signature == ''){?>
                                                                                                <img id="sign_logo" height='70' width="70" src="<?php echo COMPANY_SUBDOMAIN_URL.'/images/avatar.png'?>"/>
                                                                                            <?php } else { ?>
                                                                                                <img id="sign_logo" src="<?php echo COMPANY_SUBDOMAIN_URL."/".$signature ?>" height='70' width="70"/>
                                                                                                <input type="hidden" name="previous_signature" id="previous_signature" value="<?php echo $signature ?>" />
                                                                                            <?php } ?>
                                                                                            </div>
                                                                                        <a id="signature_upload" href="javascript:;"><i class="fa fa-pencil-square" aria-hidden="true"></i>Change/Update Signature</a>
                                                                                        <span>(The best signature dimensions 140 X 60 pixels and Maximumfile size limit upto 1 MB.)</span>
                                                                                        <input type="file" class="img_upload" name="signature" id="signature" data="previous_signature" style="display: none;" />
                                                                                        <span class="upload_signatureErr error red-star"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a class="acord" data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="fa fa-angle-down"></span> Address Info</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseFour" class="panel-collapse collapse">
                                                            <div class="panel-body">
                                                                <div class="form-outer">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Zip / Postal Code <em class="red-star">*</em></label>
                                                                            <input class="form-control" name="zipcode" id="zipcode" maxlength="9" placeholder="Eg: 35801" type="text" value="<?php echo $zipcode ?>"/>
                                                                            <span class="zip_codeErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>City <em class="red-star">*</em></label>
                                                                            <input class="form-control" name="city" id="city" type="text" placeholder="Eg: Huntsville" value="<?php echo $city ?>"/>
                                                                            <span class="cityErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>State / Province <em class="red-star">*</em></label>
                                                                            <input class="form-control" name="state" id="state" type="text" placeholder="Eg: AL" value="<?php echo $state ?>"/>
                                                                            <span class="stateErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Country <em class="red-star">*</em></label>
                                                                            <input class="form-control" name="country" id="country" type="text" placeholder="Eg: United States" value="<?php echo $country ?>"/>
                                                                            <span class="countryErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Address 1 <em class="red-star">*</em></label>
                                                                            <input class="form-control" name="address1" id="address1" type="text" placeholder="Eg: 260 Michelson Glrv NW Cal" value="<?php echo $address1 ?>"/>
                                                                            <span class="address1Err error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Address 2  <em class="red-star">*</em></label>
                                                                            <input class="form-control" name="address2" id="address2" type="text" placeholder="Eg: 260 Michelson Glrv NW Cal" value="<?php echo $address2 ?>"/>
                                                                            <span class="address2Err error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Address 3</label>
                                                                            <input class="form-control" name="address3" id="address3" type="text" placeholder="Eg: 260 Michelson Glrv NW Cal" value="<?php echo $address3 ?>"/>
                                                                            <span class="address3Err error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Address 4</label>
                                                                            <input class="form-control" name="address4" id="address4" type="text" placeholder="Eg: 260 Michelson Glrv NW Cal" value="<?php echo $address4 ?>"/>
                                                                            <span class="address4Err error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Tax ID#  <em class="red-star">*</em></label>
                                                                            <input class="form-control number_only" name="tax_id" id="taxid" type="text" maxlength="10" placeholder="Eg: 20-3329001" value="<?php echo $tax_id ?>"/>
                                                                            <span class="taxidErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Account Admin Phone No.</label>
                                                                            <input class="form-control phone_number" name="account_admin_phone_number" id="admin_phone_number" maxlength="12" type="text" placeholder="Eg: 154-175-4301" value="<?php echo $account_admin_phone_number ?>"/>
                                                                            <span class="admin_phone_numberErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Account Admin Name  <em class="red-star">*</em></label>
                                                                            <input class="form-control" name="account_admin_name" id="admin_name" type="text" placeholder="Eg: Mr. James Doe" value="<?php echo $account_admin_name ?>"/>
                                                                            <span class="admin_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Tax Payer Name</label>
                                                                            <input class="form-control" name="tax_payer_name" id="tax_payer_name" type="text" placeholder="Eg: Mr. James Doe" value="<?php echo $tax_payer_name ?>"/>
                                                                            <span class="tax_payer_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>1099 Transmission Control code (e-Filling)</label>
                                                                            <input class="form-control" name="transmission_control_code" id="transmission_code" maxlength="5" type="text" placeholder="Eg: 11A89" value="<?php echo $transmission_control_code ?>"/>
                                                                            <span class="transmission_codeErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Account Admin Email Address</label>
                                                                            <input class="form-control" name="account_admin_email_address" id="admin_email" type="text" placeholder="Eg: jholder@gmail.com" value="<?php echo $account_admin_email_address ?>" />
                                                                            <span class="admin_emailErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Application Fee (<?php echo $default_symbol?>)</label>
                                                                            <input class="form-control number_only" name="application_fees" id="application_fee" type="text" placeholder="Eg: <?php echo $default_symbol?>40.00" value="<?php echo $application_fees ?>"/>
                                                                            <span class="application_feeErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Contact Name for 1099 e-File</label>
                                                                            <input class="form-control" name="contact_name_1099_efile" id="contact_name" type="text" placeholder="Eg: Mr. James Doe" value="<?php echo $contact_name_1099_efile ?>"/>
                                                                            <span class="contact_nameErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Rent Amount (<?php echo $default_symbol?>)</label>
                                                                            <input class="form-control number_only" name="rent_amount" id="rent_amount" type="text" placeholder="Eg: <?php echo $default_symbol?>2500.00" value="<?php echo $rent_amount ?>"/>
                                                                            <span class="rent_amountErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Currency</label>
                                                                            <select class="form-control" name="currency" id="currency">
                                                                                <?php foreach ($currency['data'] as $key=>$value){ ?>
                                                                                    <?php if($value['id'] == $currencyData){ ?>
                                                                                        <option value="<?php echo $value['id'] ?>" selected=""><?php echo $value['currency'].'('.$value['symbol'].')' ?></option>
                                                                                    <?php } else { ?>
                                                                                        <option value="<?php echo $value['id'] ?>"><?php echo $value['currency'].'('.$value['symbol'].')' ?></option>
                                                                                <?php }} ?>
                                                                            </select>
                                                                            <span class="currencyErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Fax</label>
                                                                            <input class="form-control" name="fax" id="fax" type="text" placeholder="Eg: 403-297-2706" maxlength="12" value="<?php echo $fax ?>"/>
                                                                            <span class="faxErr error red-star"></span>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Default Payment Method</label>
                                                                            <select class="form-control" name="default_payment_method" id="payment_method">
                                                                                <option value="1">Check</option>
                                                                                <option value="2">Cash</option>
                                                                                <option value="3">Debit Card</option>
                                                                                <option value="4">Auto Debit</option>
                                                                                <option value="5">MO</option>
                                                                                <option value="6">Discount</option>
                                                                                <option value="7">Outward Check</option>
                                                                                <option value="8">Outward Check Issued</option>
                                                                                <option value="9">Section8</option>
                                                                                <option value="10">SCRIE</option>
                                                                                <option value="11">DRIE</option>
                                                                                <option value="12">EFT</option>
                                                                                <option value="13">ACH</option>
                                                                            </select>
                                                                            <span class="payment_methodErr error red-star"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Accordian Ends -->
                                        <div class="btn-outer text-right">
                                            <input type="submit" class="blue-btn" value="Update">
                                            <button type="button" id="company_setup_cancel" class="grey-btn">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                                <input type="hidden" id="zip_code_validate" value="1">
                        </div>
                        </div>
                        <!-- Content Data Ends ---->
                        <div class="form-outer content-data">
                                        <div class="form-hdr">
                                            <h3>
                                                <strong class="left">Payment Settings</strong>
                                            </h3>
                                        </div>
                                        <div class="form-data">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Payment Method <em class="red-star">*</em></label>
                                                    <select class="form-control setType" id="payment_method">
                                                      <option value='1'>Credit Card/Debit Card</option>
                                                      <option value='2'>ACH</option>
                                                    </select>
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>&nbsp;</label>

                                                </div>
                                            </div>

                                            <div class="row cards">
                                                <div class="cardDetails col-sm-12"></div>
                                                <form id="addCards">
                                                    <div class="detail-outer ">
                                                        <div class="col-md-5 clear">
                                                            <label class="black-label">Card Type<em class="red-star">*</em></label>
                                                            <img src="/company/images/card-payment.png" style="width: 190px;">
                                                            <span class="ffirst_nameErr error red-star"></span>
                                                        </div>
                                                        <div class="col-md-5 clear">
                                                            <label class="black-label">Card Number <em class="red-star">*</em></label>
                                                            <span>
                                                                <input class="form-control hide_copy" name="card_number"  type="text" maxlength="16"/>

                                                            </span>
                                                        </div>


                                                    <div class="col-md-5 clear">
                                                        <label class="black-label">Expiration Date <em class="red-star">*</em></label>
                                                        <span>
                                                            <input class="form-control exp_date calander" name="exp_date"  type="text" readonly/>

                                                        </span>
                                                    </div>

                                                    <div class="col-md-5 clear">
                                                        <label class="black-label">CVC <em class="red-star">*</em></label>
                                                        <span>
                                                        <input class="form-control" name="cvc"  type="text"/>

                                                        </span>
                                                    </div>

                                                    <div class="col-md-5 clear">
                                                        <label class="black-label">Cardholder Name <em class="red-star">*</em></label>
                                                        <span>
                                                            <input class="form-control holder_name" name="holder_name" type="text"/>
                                                            <em class="nicknameErr error red-star"></em>
                                                        </span>
                                                    </div>

<!---->
<!--                                                   <div class="col-sm-5 clear">-->
<!--                                                        <label class="black-label">Autopay </label>-->
<!--                                                        <span>-->
<!--                                                            <input class="auto_pay" name="auto_pay" type="checkbox" value="ON"/>-->
<!---->
<!--                                                        </span>-->
<!--                                                    </div>-->



                                                        </div>
                                                        <div class="col-sm-12">
                                                        <input type="submit" class="blue-btn" value="Confirm">
                                                        <input type="button" class="blue-btn cancel" value="Cancel">
                                                    </div>
                                                    </form>

                                             </div>
                                              <div class="row accounts" style="display:none;">
                            <div class="accountDetails"></div>


                </div>
                                        </div>

                                        </div>









                    </div>
                </div>
            </div>
        </div>



    </section>



</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
<script language="javascript" src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyAytvEH1v5VqbYMGrjBCkvFLT5JKjHs6ww"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/validation/company_setup.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/company_setup.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/payments/plaidIntialisation.js"></script>
<script>
    $(document).on('focusout','.amount',function(){
        var id = this.id;
        if($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
            var bef = $('#' + id).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $('#' + id).val(value);
        } else {
            var bef = $('#' + id).val().replace(/,/g, '');
            $('#' + id).val(numberWithCommas(bef));
        }
    });


    $(".no_special").keydown(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    /**
     * If the letter is not digit in fax then don't type anything.
     */
    $(".number_only").keydown(function (e) {
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });

    $(document).on('click','#company_setup_cancel',function(){
        bootbox.confirm("Do you want to cancel this action now ?", function (result) {
            if (result == true) {
                window.location.href = '/User/AccountSetup';
            }
        });
    });


    //setting payment method
    $('#payment_method').val('<?php echo $default_payment_method ?>');

    $('#leftnav1').addClass('in');
    $('.account_setup').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    <!--- Main Nav Responsive -->
    $("#show").click(function(){
        $("#bs-example-navbar-collapse-2").show();
    });
    $("#close").click(function(){
        $("#bs-example-navbar-collapse-2").hide();
    });
    <!--- Main Nav Responsive -->


    $(document).ready(function(){
        $(".slide-toggle").click(function(){
            $(".box").animate({
                width: "toggle"
            });
        });
    });

    $(document).ready(function(){
        $(".slide-toggle2").click(function(){
            $(".box2").animate({
                width: "toggle"
            });
        });
    });

    $(document).on('click','#logo_upload',function(){
        $('#company_logo').trigger('click');
    });

    $(document).on('click','#signature_upload',function(){
        $('#signature').trigger('click');
    });

    function readURL(input,showto) {

        if (input.files && input.files[0]) {
            var type = input.files[0]['type'];
            var reader = new FileReader();
            if(type == 'image/png' || type == 'image/jpeg' || type == 'image/gif') {
                reader.onload = function (e) {
                    $('#' + showto).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                toastr.warning('Please select file with .jpeg | .png | .gif extension only!');
            }
        }
    }

    $("#company_logo").change(function() {
        readURL(this,'com_logo');
    });

    $("#signature").change(function() {
        readURL(this,'sign_logo');
    });

    <?php
    if (isset($_SESSION[SESSION_DOMAIN]["message"])) {
    $message = $_SESSION[SESSION_DOMAIN]["message"];
    ?>
        toastr.success("<?php echo $message ?>");
    <?php
    unset($_SESSION[SESSION_DOMAIN]["message"]);
    }
    ?>

</script>

<script type="text/javascript">
    
$(document).on("change",".setType",function(){
  var value = $(this).val();
  if(value=='1')
  {

    $(".accounts").hide();
    $(".cards").show();

  }
  else
  {
      var validator = $("#addCards").validate();
      validator.resetForm();
    $(".accounts").show();
    $(".cards").hide();
  }

});

$(document).ready(function(){

$('.exp_date').datepicker({
        yearRange: '2019:2040',
        changeMonth: true,
        changeYear: true,
        dateFormat: 'mm/yy'
    });


});
  


</script>
<!-- Jquery ends -->

<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/layouts/admin_footer.php");
?>