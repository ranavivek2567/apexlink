<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Accounting Preferences</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add unit type div starts here-->
                                                <div  id="add_unit_type_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Accounting Preferences
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <form name="add_property_type" id="add_unit_type_form">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="account_preference_view">
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Fiscal Year First Month :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Cash Account (Credit) for Bill Posting :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Rent Charges :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Check Receive Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Debit Type Receive Payment Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Vacancy Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Waive-Off on Payment Received Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Bad Debt Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Write-Off Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Money Order Payment Received Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Auto Debit Payment Received Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Cash Payment Received Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">NSF Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Check Bounced Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Check Issue to Vendor/Tenant Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Refund Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Interest Received from Bank Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Bank Adjustment Fee Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Concession Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Transfer Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Late Fee Charge :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">Discount Account Id :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                            <div class="col-sm-6">
                                                                                <label class="blue-label">EFT Generate Date :</label>
                                                                                <span class="first_name"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row apx-border-row">
                                                                        <div class="col-md-2 text-right pull-right col-xs-12">
                                                                            <a id="editAccountPreferences" class="apx-edt-btn"><i class="fa fa-edit"></i> Edit</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Add unit type div ends here-->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    $('#leftnav10').addClass('in');
    $('.accounting_preferences1').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/trashBin/unit_type.js"></script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>