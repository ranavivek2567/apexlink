<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->
    <link href="<?php echo COMPANY_SUBDOMAIN_URL; ?>/css/jquery.multiselect.css" rel="stylesheet" />
    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Users >> <span>vehicle Type</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div id="manage_groups_id" style="display: none;">
                                <div class="panel-heading">
                                    <h4 id="headerDiv" class="panel-title form-hdr" >
                                        New Group
                                    </h4>
                                </div>
                                <form id="add_manage_group">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-outer">
                                                <div class="col-sm-3">
                                                    <label>Group Name <em class="red-star">*</em></label>
                                                    <input name="grp_name" id="grp_name" class="form-control" type="text" readonly/>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Description</label>
                                                    <input name="description" id="description" class="form-control" type="text"/>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Users</label>
                                                    <select class="form-control" name="pm[]" id="select_user" multiple>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="pull-right col-sm-6">
                                    <input type="hidden" class="hidden_grp_id">
                                    <input type="button" name="copy_user_role_id" id="save_group" class="blue-btn" value="Save">
                                    <input type="button" class="cancel_grp grey-btn" value="Cancel"></div></div>

                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add unit type div starts here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="manageVehicle-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List unit type div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    $('#leftnav4').addClass('in');
    $('.manage_groups').addClass('active');
</script>  <script src="<?php echo COMPANY_SITE_URL; ?>/js/jquery.multiselect.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/adminUsers/manageGroups.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
<script>
    $('input[id="ddate"]').datepicker({
        dateFormat     : datepicker
    });
    $('input[id="date"]').datepicker({
        dateFormat     : 'yy'

    });


</script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>