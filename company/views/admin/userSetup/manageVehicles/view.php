<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Users >> <span>Manage Vehicles</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="btn-outer text-right">
                                            <button id="addVehicleButton" class="blue-btn">Add New Vehicle</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add unit type div starts here-->
                                                <div  style="display: none;" id="add_vehicle_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Add New Vehicle
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <form name="add_vehicle_type" id="add_vehicle_type">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Vehicle Name </label>
                                                                                    <input name="vehicle_name" id="vehicle_name" maxlength="100"   class="form-control disable_edit first_capital" type="text"/>
                                                                                    <span id="vehicle_nameErr"></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Vehicle #</label>
                                                                                    <input name="vehicle" id="vehicle" maxlength="12" class="form-control"/>
                                                                                    <span id="vehicleErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Vehicle Type</label>
                                                                                    <input name="vehicle_type" id="vehicle_type" maxlength="100" class="form-control first_capital"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Make</label>
                                                                                    <input name="make" id="make" maxlength="100" class="form-control first_capital"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Model</label>
                                                                                    <input name="model" id="model" maxlength="100" class="form-control first_capital"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>VIN #</label>
                                                                                    <input name="vin" id="vin" maxlength="100" class="form-control first_capital"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Registration #</label>
                                                                                    <input name="registration" id="registration" maxlength="100" class="form-control first_capital"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>License Plate Number</label>
                                                                                    <input name="plate_number" id="plate_number" maxlength="100" class="form-control first_capital"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Color</label>
                                                                                    <input name="color" id="color" maxlength="100" class="form-control first_capital"/>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Year of Vehicle</label>
                                                                                    <select class="fm-txt form-control" id="year_of_vehicle" name="year_of_vehicle"></select>
                                                                                </div>
                                                                                <div class="col-sm-3">
                                                                                    <label>Date Purchased</label>
                                                                                    <input name="date_purchased" id="date_purchased" maxlength="100" class="form-control"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Starting Mileage</label>
                                                                                    <input name="starting_mileage" id="starting_mileage" maxlength="100" class="form-control"/>
                                                                                    <span id="starting_mileageErr" class="error"></span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-3">
                                                                                    <label>Amount(<?php echo $_SESSION[SESSION_DOMAIN]["default_currency_symbol"]?>)</label>
                                                                                    <input name="amount" id="amount" maxlength="10" class="form-control amount"/>
                                                                                    <span id="amountErr" class="error"></span>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="btn-outer mg-btm-20 text-right" >
                                                                    <input type="hidden" name="vehicle_edit_id" class="form-control" id="vehicle_edit_id" />
                                                                    <input type="submit" value="Save" class="blue-btn" id="saveBtn"/>
                                                                    <button type="button" class="clear-btn clearFormReset">Clear</button>
                                                                    <button type="button" id="add_vehicle_cancel_btn" class="grey-btn">Cancel</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="panel-body pad-none">
                                                    <div class="grid-outer">
                                                        <div class="apx-table">
                                                            <div class="table-responsive">
                                                                <table id="manageVehicle-table" class="table table-bordered"></table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--List unit type div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script>
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var datepicker = "<?php echo $_SESSION[SESSION_DOMAIN]['datepicker_format']; ?>";
    $('#leftnav4').addClass('in');
    $('.vehicle_type').addClass('active');
    var defaultFormData='';
    $(document).on('click','.formreset',function () {
        resetEditForm("#add_vehicle_type",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#add_vehicle_type",['year_of_vehicle'],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/adminUsers/manageVehicle.js"></script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/common.js"></script>
<script>
    $('input[id="date_purchased"]').datepicker({
        dateFormat     : datepicker
    });
    $('input[id="date"]').datepicker({
        dateFormat     : 'yy'

    });


</script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>