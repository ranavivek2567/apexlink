<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span> Allocation Order</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">
                            <div class="main-tabs">
                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <form id="allocation_order_form_id">
                                                    <div id="allocation-order-table">
                                                        <table id="chargeCodeList" class="table table-bordered">
                                                            <thead>
                                                            <tr id="">
                                                                <th class="table-heading">Charge Code</th>
                                                                <th class="table-heading">Description</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="order_tbody">
                                                            </tbody>
                                                        </table>
                                                        <div class="btn-outer text-right">
                                                            <input type="hidden" name="account_type_id" class="form-control" id="account_type_id"/>
                                                            <input type="submit" value="Update" class="blue-btn" id="saveBtnId"/>
                                                            <button type="button" id="cancelAllocationOrderBtn" class="grey-btn">Cancel</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/plugin/jquery.tablednd.js"></script>
<script>
    $('#leftnav3').addClass('in');
    $('.allocation_order').addClass('active');

    $(document).ready(function() {

        chargeCodeList();

        /**
         * Get all charge code for ddl
         */
        function chargeCodeList(){
            $.ajax({
                type: 'post',
                url: '/AllocationOrder-Ajax',
                data: {
                    class: 'allocationOrder',
                    action: 'chargeCodeList',
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        var html1 = '';
                        $('.order_tbody').html('');
                        $.each(response.data, function (key, value) {
                            html1 = '<tr id="'+value.id+'">\n' +
                                '<td ">'+value.charge_code+'</td>\n' +
                                '<td ">'+value.description+'</td>\n' +
                                '</tr>';
                            $('.order_tbody').append(html1);
                        });

                    } else {
                        toastr.error(response.message);
                    }
                }
            });
        }
        var ids = [];

        setTimeout(function(){
            $("#chargeCodeList").tableDnD({
                onDragClass: "myDragClass",
                onDrop: function(table, row) {
                    var rows = table.tBodies[0].rows;
                    for (var i=0; i<rows.length; i++) {
                        ids[i] = rows[i].id+" ";
                    }
                },
                onDragStart: function(table, row) {
                }
            });
        }, 800);

        $("#allocation_order_form_id").on('submit', function (e) {
            e.preventDefault();
            var priority_ids = (ids.length === 0) ? '' : ids;
            $.ajax({
                type: 'post',
                url: '/AllocationOrder-Ajax',
                data: {
                    class: 'allocationOrder',
                    action: 'updatePriorityOfChargeCode',
                    priority:priority_ids,
                },
                success: function (response) {
                    var result = $.parseJSON(response);
                    if (result.status == 'success' || result.code == 200) {
                        toastr.success(result.message);
                    } else {
                        toastr.warning('This record cannot update due to technical error. Please try later.');
                    }
                }
            });
        });
        /** Cancel button click*/
        $(document).on('click','#cancelAllocationOrderBtn',function(){
            bootbox.confirm({
                message: "Do you want to cancel this action now?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        chargeCodeList();
                        ids = [];
                        setTimeout(function(){ $("#chargeCodeList").tableDnDUpdate(); }, 800);
                    }
                }
            });
        });
    });
</script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>