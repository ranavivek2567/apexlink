<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */
if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");

?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Tax Setup</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status">
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>

                                    </div>
                                    <div class="col-md-10">
                                        <div class="btn-outer text-right">
                                            <button type="submit" id="addUnitTypeButton" class="blue-btn">Add New Tax</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add unit type div starts here-->
                                                <div  style="display: none;" id="add_tax_type_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Add Tax Type
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <form name="add_tax" id="add_tax_form">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Select Tax <em class="red-star">*</em></label>
                                                                                    <select id="tax_name" name="tax_name" class="form-control" >
                                                                                        <option value="">Select</option>
                                                                                        <option value="GST">GST</option>
                                                                                        <option value="HST">HST</option>
                                                                                        <option value="QST">QST</option>
                                                                                        <option value="PST">PST</option>
                                                                                        <option value="VAT">VAT</option>
                                                                                    </select>
                                                                                    <span id="unit_typeErr"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-md-3">
                                                                                    <label>Type <em class="red-star">*</em></label>
                                                                                    <select id="tax_type" name="tax_type" class="form-control" >
                                                                                        <option value="">Select</option>
                                                                                        <option value="Percentage">Percentage</option>
                                                                                        <option value="Flat">Flat</option>
                                                                                    </select>
                                                                                </div>
                                                                                <div id="tax_value">
                                                                                    <div  class="col-sm-6 col-md-3">
                                                                                        <label>Value <em class="red-star">*</em></label>
                                                                                        <input name="value" id="taxvalue" maxlength="10"  class="form-control" type="text"/>
                                                                                        <span id="unit_typeErr"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-3 btn-outer text-right">
                                                                                <input type="hidden" value="insert" name="tax_type_id" class="form-control" id="tax_type_id"/>
                                                                                <input type="hidden" name="current_currency" class="form-control" id="current_currency" value="<?php echo $_SESSION[SESSION_DOMAIN]["default_currency_symbol"]?>"/>
                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                                                                <button type="button" class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" id="add_unit_cancel_btn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Add unit type div ends here-->

                                                <!--List unit type div ends here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="TaxSetup-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List unit type div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $('#leftnav3').addClass('in');
    $('.tax_setup').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });


    $(document).on('change', '#tax_type', function () {
        var tax_type = $(this).val();
        if(tax_type == 'Percentage'){
            $('#tax_value').html('<div id="tax_value">' +
                '<div class="col-sm-3"  id="tax_value">\n' +
                ' <label>Value(%) <em class="red-star">*</em></label>\n' +
                '<input name="value" id="taxvalue"  class="form-control" type="text"/>\n' +
                '<span id="unit_typeErr"></span>'+
                '</div>');
            new AutoNumeric('#taxvalue', {
                allowDecimalPadding: false,
                maximumValue  : '100',
            });
        }
        if(tax_type == 'Flat'){
            $('#tax_value').html('<div id="tax_value">' +
                '<div class="col-sm-3">\n' +
                '<label>Value(<?php echo $_SESSION[SESSION_DOMAIN]["default_currency_symbol"]?>) <em class="red-star">*</em></label>\n' +
                '<input name="value" id="taxvalue"  class="form-control" type="text"/>\n' +
                '<span id="unit_typeErr"></span>' +
                '</div>');
            new AutoNumeric('#taxvalue', {
                allowDecimalPadding: true,
                maximumValue  : '9999999999',
            });
        }
        if(tax_type == ''){
            $('#tax_value').html('<div id="tax_value">\n' +
                '<div  class="col-sm-3">\n' +
                '<label>Value <em class="red-star">*</em></label>\n' +
                '<input name="value" id="taxvalue" maxlength="10" class="form-control" type="text"/>\n' +
                '<span id="unit_typeErr"></span>\n' +
                '</div>\n' +
                '</div>');
        }
    });
     var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#add_tax_form",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#add_tax_form",['tax_name','tax_type'],'form',false);
    });
</script>
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/taxSetup.js"></script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>