<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/18/2019
 * Time: 11:19 AM
 */

if (!isset($_SESSION[SESSION_DOMAIN]['cuser_id']) && ($_SESSION[SESSION_DOMAIN]['cuser_id'] == '')) {
    $url = SUBDOMAIN_URL;
    header('Location: ' . $url);
}
?>
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_header.php");
?>

<div id="wrapper">
    <!-- Top navigation start -->
    <?php
    include_once(COMPANY_DIRECTORY_URL . "/views/layouts/top_navigation.php");
    ?>
    <!-- Top navigation end -->

    <section class="main-content">
        <div class="container-fluid">
            <div class="row flex">
                <?php
                include_once(COMPANY_DIRECTORY_URL . "/views/layouts/sidebar.php");
                ?>

                <div class="col-sm-8 col-md-10 main-content-rt">
                    <div class="content-rt">
                        <div class="bread-search-outer">
                            <div class="col-sm-8">
                                <div class="breadcrumb-outer">
                                    Accounting >> <span>Bank Account</span>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="easy-search">
                                    <input placeholder="Easy Search" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="content-data">

                            <div class="property-status">
                                <div class="row">
                                    <div class="col-md-2">
                                        <select id="jqGridStatus" data-module="ADMIN-SETTINGS" class="jqGridStatusClass fm-txt form-control">
                                            <option value="All">All</option>
                                            <option value="1">Active</option>
                                            <option value="0">InActive</option>
                                        </select>

                                    </div>
                                    <div class="col-md-10">
                                        <div class="btn-outer text-right">
                                            <button id="addBankAccountButton" class="blue-btn">Add Bank Account</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Main tabs -->
                            <div class="main-tabs">

                                <div class="accordion-grid">
                                    <div class="accordion-outer">
                                        <div class="bs-example">
                                            <div class="panel-group" id="accordion">
                                                <!--Add unit type div starts here-->
                                                <div  style="display: none;" id="add_Bank_account_div">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 id="headerDiv" class="panel-title">
                                                                Add Bank Account
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                            <form id="add_Bank_account_form">
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="form-outer">
                                                                            <div class="col-sm-12">
                                                                                <div class="row">
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label>Portfolio <em class="red-star">*</em></label>
                                                                                    <select id="portfolio" name="portfolio" class="fm-txt form-control">
                                                                                        <option value="">Select</option>
                                                                                    </select>
                                                                                    <span id="portfolioErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label>Bank Name  <em class="red-star">*</em></label>
                                                                                    <input name="bank_name" id="bank_name" maxlength="100" placeholder="Eg: Wells Fargo"   class="form-control hide_copy" type="text"/>
                                                                                    <span id="bank_nameErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label>Bank A/c No.  <em class="red-star">*</em></label>
                                                                                    <input name="bank_account_number" id="bank_account_number" maxlength="20" placeholder="Eg: 123A12345678"   class="form-control hide_copy" type="text"/>
                                                                                    <span id="bank_account_numberErr" class="error"></span>
                                                                                </div>
</div>
                                                                            </div>

                                                                            <div class="col-sm-12">
                                                                            <div class="row">
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label>FDI No.  <em class="red-star">*</em></label>
                                                                                    <input name="fdi_number" id="fdi_number" maxlength="10" placeholder="Eg: 123A12345678"   class="form-control" type="text"/>
                                                                                    <span id="fdi_numberErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label>Branch Code <em class="red-star">*</em></label>
                                                                                    <input name="branch_code" id="branch_code" maxlength="10" placeholder="Eg: AB12 12345 "   class="form-control" type="text"/>
                                                                                    <span id="branch_codeErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label>Initial Amount(<?php echo $_SESSION[SESSION_DOMAIN]["default_currency_symbol"]?>)  <em class="red-star">*</em></label>
                                                                                    <input name="initial_amount" id="initial_amount" placeholder="Eg: 1000"   class="form-control" type="text"/>
                                                                                    <span id="initial_amountErr" class="error"></span>
                                                                                </div>
</div>
                                                                            </div>

                                                                            <div class="col-sm-12">
                                                                            <div class="row">
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label>Last Used Check Number  <em class="red-star">*</em></label>
                                                                                    <input name="last_used_check_number" id="last_used_check_number" maxlength="100" placeholder="Eg: 1234"   class="form-control" type="text"/>
                                                                                    <span id="last_used_check_numberErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label>Status <em class="red-star">*</em></label>
                                                                                    <select id="status" name="status" class="fm-txt form-control">
                                                                                        <option value="">Select</option>
                                                                                        <option value="1">Active</option>
                                                                                        <option value="0">InActive</option>
                                                                                    </select>
                                                                                    <span id="statusErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label>Bank Routing  <em class="red-star">*</em></label>
                                                                                    <input name="routing_number" id="routing_number_rec" maxlength="10" placeholder="Eg: 110000000"   class="form-control" type="text"/>
                                                                                    <span id="routing_numberErr" class="error"></span>
                                                                                </div>
                                                                                <div class="col-sm-6 col-xs-12 col-md-3">
                                                                                    <label></label>
                                                                                    <div class="check-outer">
                                                                                        <input name="is_default" id="is_default" type="checkbox"/>
                                                                                        <label>Set as Default</label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-12 btn-outer text-right">
                                                                                <input type="hidden" name="bank_account_id" class="form-control" id="bank_account_id"/>
                                                                                <input type="submit" value="Save" class="blue-btn" id="saveBtnId"/>
                                                                                <button type="button"  class="clear-btn clearFormReset">Clear</button>
                                                                                <button type="button" id="bank_account_cancel_btn" class="grey-btn">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Add unit type div ends here-->

                                                <!--List unit type div ends here-->
                                                <div class="panel panel-default">
                                                    <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                        <div class="panel-body pad-none">
                                                            <div class="grid-outer">
                                                                <div class="apx-table">
                                                                    <div class="table-responsive">
                                                                        <table id="BankAccount-table" class="table table-bordered"></table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--List unit type div ends here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Content Data Ends ---->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Wrapper Ends -->

<!-- Jquery Starts -->
<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/autonumeric.js"></script>
<script>
    var pagination = "<?php echo $_SESSION[SESSION_DOMAIN]['pagination']; ?>";
    var default_currency_symbol = "<?php echo $_SESSION[SESSION_DOMAIN]['default_currency_symbol']; ?>";
    $('#leftnav3').addClass('in');
    $('.bank_account').addClass('active');
    $(function() {
        $('.nav-tabs').responsiveTabs();
    });

    new AutoNumeric('#initial_amount', {
        allowDecimalPadding: true,
        maximumValue  : '9999999999',
    });
    new AutoNumeric('#last_used_check_number', {
        allowDecimalPadding: false,
        digitGroupSeparator : ''
    });
    var defaultFormData = '';
    $(document).on('click','.formreset',function () {
        resetEditForm("#add_Bank_account_form",[],true,defaultFormData,[]);
    });
    $(document).on('click','.clearFormReset',function () {
        resetFormClear("#add_Bank_account_form",['portfolio','status'],'form',false);
    });
</script>

<script src="<?php echo COMPANY_SUBDOMAIN_URL; ?>/js/company/accounting/bankAccount.js"></script>
<!-- Jquery Ends -->
<?php
include_once(COMPANY_DIRECTORY_URL . "/views/layouts/admin_footer.php");
?>