$(document).ready(function() {
    stripePaymentVariables();
    /*var serverTime = getServerTime();
    var securityHeaderToken = getSecurityHeaders(serverTime);
    applicationRenterStatu(serverTime, securityHeaderToken);*/

    $(document).on('keydown', '.number_only_for_extension', function (e) {
        if (e.which != 187 && e.which != 46 && e.which != 37 && e.which != 39 &&e.which != 116 && e.which != 16 && e.which != 17 &&  e.which != 107 && e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });

    var id = $('#renter_signup #tenantid').val();
    if(id != '' && id !=undefined) {
        getTenantDetail(id);
    }

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});
    jQuery('.cardNumber').mask('0000-0000-0000-0000', {reverse: true});

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content');
      //  allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);
        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }

    });

    $('.showprevious ').click(function(){
        $('#step-1').show();
        $('#step-2').hide();
       var href = $('.stepwizard-step .btn-default').attr('href');
        $('.stepwizard-step .btn-primary').removeClass('btn-primary');
        $('div.setup-panel div a[href="#step-1"]').parent().children("a").addClass('btn-primary');
    });

    $(document).on('blur','.pass',function () {
        var pass1 = $('.sm_password').val();
        var pass2 = $('.csm_password').val();
        var resp = checkConfirmPassword(pass1,pass2);
        $(".pass_error").html('');
        if(resp == 'Please enter Password'){
            $(".pass_error").html(resp);
        } else {
            $(".cpass_error").html(resp);
        }
    });

    function checkConfirmPassword(password1,password2) {
        if (password1 == '')
            return "Please enter Password";
        else if (password2 == '')
            return "Please enter confirm password";
        else if (password1 != password2) {
            return "Password did not match: Please try again...";
        }else{
            return "";
        }
    }

    $('div.setup-panel div a.btn-primary').trigger('click');
    $(".close").click(function() {
        $("label.error").hide();
        $(".error").removeClass("error");
    });

    $("#renter_signup").validate({
        rules: {
            'email':{required:true},
            'accept_term':{required:true}
        },
        messages: {
            "email": "Enter Email ID",
            "accept_term": "Please accept terms and conditions",
        }
    });
    $("#renterPayment_frm").validate({
        rules: {
            'email':{required:true},
            'cardNumber':{required:true},
            'expirydate':{required:true},
            'CCV':{required:true},
            'cardName':{required:true},
            'country':{required:true},
            'postalCode':{required:true},
        },
        messages: {
            "email": "Enter Email ID",
            "cardNumber": "Enter card Number",
            "expirydate": "Enter card expiry date",
            "CCV": "Enter card CCV",
            "cardName": "Enter name as on Card",
            "country": "Enter Country Name",
            "postalCode": "Enter postal Code",
        }
    });

    $(document).on("click",".save_tenantsmartmove",function(e) {
        e.preventDefault();
        if ($('#renter_signup').valid()) {
            var form = $('#renter_signup')[0];
            var formData = new FormData(form);
            formData.append('class','SmartMove');
            formData.append('action','saveRenterSignUp');
            signUpRenterSmartMove(formData);
        }
        return false;
    });

    $(document).on("change","select[name='security_question_1']",function(){
        var currentvalue = $(this).val();
        var secondQuestion = $("select[name='security_question_2']").val();
        var thirdQuestion = $("select[name='security_question_3']").val();
        if (currentvalue != '' && (currentvalue == secondQuestion || currentvalue == thirdQuestion)){
            $(this).val("");
        }
    });


    $(document).on("click",".getRenterDetail",function(e) {
        e.preventDefault();
        var tenantid = $('#renter_account_form #tenantid').val();
        if ($('#renter_account_form').valid()) {
            var form = $('#renter_account_form')[0];
            var formData = new FormData(form);
            formData.append('class','SmartMove');
            formData.append('action','saveRenterAccountdetail');
            formData.append('id',tenantid);
            addRenterAccountdetail(formData);
            nexttab(this);
        }
        return false;
    });

    $("#renter_account_form").validate({
        rules: {
            'sm_password':{required:true},
            'csm_password':{required:true},
            'security_question_1':{required:true},
            'security_question_2':{required:true},
            'security_question_3':{required:true},
            'secQstnAns1':{required:true},
            'secQstnAns2':{required:true},
            'secQstnAns3':{required:true}
        },

        messages: {
            "sm_password": "Enter password",
            "csm_password": "Enter password to confirm",
            "security_question_1" :"Select question",
            "security_question_2" :"Select question",
            "security_question_3" :"Select question",
            "secQstnAns1" :"Fill Answer",
            "secQstnAns2" :"Fill Answer",
            "secQstnAns3" :"Fill Answer",
        }
    });

    $("#renter_personalInfo_form").validate({
        rules: {
            'ssn_sin_id':{required:true},
            'cssn_sin_id':{required:true,equalTo : "#ssn_sin_id"},
            'first_name':{required:true},
            'last_name':{required:true},
            'primary_phone':{required:true},
            'address1':{required:true},
            'city':{required:true},
            'state':{required:true},
            'income':{required:true},
            'zipcode':{required:true}
        },
        messages: {
            "ssn_sin_id": "Fill SSN",
            "cssn_sin_id": "Fill SSN to confirm",
            "first_name": "Fill first name",
            "last_name": "Fill last name",
            "primary_phone" :"Fill primary Phone Number",
            "address1" :"Fill street address",
            "city" :"Fill city",
            "state" :"Fill state",
            "income" :"Fill Income",
            "zipcode" :"Fill zipcode"
        }
    });

    $(document).on('blur','.checkssn',function (){
        var cssn_sin_id = $('.cssn_sin_id').val();
        var ssn_sin_id = $('.ssn_sin_id').val();
        $('.ssn').html('');
        if (ssn_sin_id == '')
            msg = "Please enter SSN";
        else if (cssn_sin_id == '')
            msg = "Please enter confirm SSN";
       else if( ssn_sin_id == cssn_sin_id)
            msg ="SSN Mathched";
        else
            msg ="SSN Not Mathched";
        if(msg == 'Please enter SSN')
            $('.ssn').html(msg);
        else
            $('.cssn').html(msg);

    });

    $(document).on('click','.getIncomeInfo',function(e){
        $('#renter_confrirmation_form .incomeInfo').text($("#renter_personalInfo_form input[name='income']").val());
        $('#renter_confrirmation_form .otherincome').text($("#renter_personalInfo_form input[name='otherincome']").val());
        $('#renter_confrirmation_form .assetsInfo').text($("#renter_personalInfo_form input[name='assets']").val());
        $('#renter_confrirmation_form .employed_status').text($("#renter_personalInfo_form select[name='empmntstatus']").val());

        if ($('#renter_personalInfo_form').valid()) {
            var form = $('#renter_personalInfo_form')[0];
            var formData = new FormData(form);
            formData.append('class','SmartMove');
            formData.append('action','saveRenterPersonalInfo');
            addRenterPersonalInfo(formData);
            nexttab(this);

           /* var income = $('.income-information .Income').val();
            var incomeP = $("input[name='incomePeriod']:checked").val();
            var OincomeP = $("input[name='otherIncomePeriod']:checked").val();
            var otherincome = $('.income-information .otherincome').val();

            var assets = $('.assets').val();
            $('#renter_confrirmation_form .incomeInfo').html(income+" "+incomeP);
            $('#renter_confrirmation_form .Otherincome').html(otherincome+" "+OincomeP);
            $('#renter_confrirmation_form .assetsInfo').html(assets);

            var SSN = $('.ssn_sin_id').val();
            var Ext =  $('.ext').val();
            var Address1 =  $('.address1').val();
            var Address2  =  $('.address2 ').val();

            $('.ssn_sin_id').html(SSN);
            $('.Ext').html(Ext);
            $('.Address1').html(Address1);
            $('.Address2').html(Address2);
            var empmntstatus = $('.empmntstatus:selected').html();
            $('#renter_confrirmation_form .empmntstatus').html(empmntstatus);*/
            return false;
        }
        return false;
    });

    $(document).on('click','.renter_Confrm ',function(e){
        e.preventDefault();
        var tenantId = $("#renter_confrirmation_form #tenantid").val();
        getIDMAExams(tenantId);
        nexttab(this);
        return false;
        var rowid = $("#renter_confrirmation_form #rowid").val();
        var renterDetails = getRenterDetailsForApi(tenantId, rowid);
        if (renterDetails.status != ""){
            var getRecAppStatus = getRecordForApplicationStatus(tenantId);
            console.log("getRecAppStatus",getRecAppStatus);
            if (getRecAppStatus != "" && getRecAppStatus != undefined){
                var appInfo = getRecAppStatus[0].appInfo;
                var applicationData = getRecAppStatus[0].applicationStatus;
                var applicationStatus = applicationData.data.ApplicationStatus;
                var idmaVerificationStatus = applicationData.data.IdmaVerificationStatus;
                var renterEmailAddress = applicationData.data.RenterEmailAddress;
                var applicationId = applicationData.data.ApplicationId;
                nexttab(this);
                getIDMAExams(tenantId);
            }
        }else{
            toastr.success(renterDetails.message);
            return false;
        }
        return false;
        //nexttab(this);

    });


    $(document).on("change","select[name='security_question_2']",function(){
        var currentvalue = $(this).val();
        var secondQuestion = $("select[name='security_question_1']").val();
        var thirdQuestion = $("select[name='security_question_3']").val();
        if (currentvalue != '' && (currentvalue == secondQuestion || currentvalue == thirdQuestion)){
            $(this).val("");
        }
    });
    $(document).on("change","select[name='security_question_3']",function(){
        var currentvalue = $(this).val();
        var secondQuestion = $("select[name='security_question_2']").val();
        var thirdQuestion = $("select[name='security_question_1']").val();
        if (currentvalue != '' && (currentvalue == secondQuestion || currentvalue == thirdQuestion)){
            $(this).val("");
        }
    });

    $(document).on('click','.saveRenterInfo',function(e) {
        var id = $('#renter_confrirmation_form #tenantid').val();
        var rowId = $('#renter_confrirmation_form #rowid').val();
        var form = $('#renter_confrirmation_form')[0];
        var formData = new FormData(form);
        formData.append('class', 'SmartMove');
        formData.append('action', 'saveRenterIdentityVerify');
        addRenterIdentityVerify(formData, id, rowId);
    });

    $(document).on('click','.sendpayment',function () {
        if ($('#renterPayment_frm').valid()) {
            var form = $('#renterPayment_frm')[0];
            var formData = new FormData(form);
            formData.append('class', 'SmartMove');
            formData.append('action', 'saveRenterPayment');
            RenterPayment(formData);
            return false;
        }
    });
});

function getIDMAExams(tenantId){
    $.ajax({
        type: 'POST',
        url: '/RenterSmartMove',
        data: {
            class: "SmartMove",
            action: "getIDMAExams",
            id: tenantId
        },
        async: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success" && res.code == 200) {
                var id = res.content.Id;
                var securityToken = res.content.SecurityToken;
                var timeOfRequest = res.content.TimeOfRequest;
                var email = res.content.Email;
                var evaluation = res.content.Evaluation;
                var questions = res.content.Questions;
                var questionsHtml = createIDMAQuestionsHtml(questions);
                $(".questions-block").html(questionsHtml);
                console.log(questionsHtml);
            }
        }
    });
}

function createIDMAQuestionsHtml(questions) {
    var html = '<div class="col-sm-4">';
    for(var i = 0; i < questions.length; i++){
        html += '<div class="setup-content3-item">';

        html +=     '<p id="'+questions[i].Id+'"><span>'+i+1+'</span> '+questions[i].Text+'</p>';
        var answers = questions[i].Answers;
        console.log("answers",answers);
        for(var j = 0; j < answers.length; j++){
            console.log("answers==",answers[j]);
            html += '<label class="radio">';
            html +=     '<input type="radio" name="answers" value="'+answers[j].Id+'" checked><em>'+answers[j].Text+'</em>';
            html += '</label>';
        }
        html += '</div>';
    }
    html += '</div>';
    return html;
}

function getRecordForApplicationStatus(tenantId){
    var applicationStatusRes = [];
    $.ajax({
        type: 'POST',
        url: '/RenterSmartMove',
        data: {
            class: "SmartMove",
            action: "getApplicationStatusData",
            id: tenantId
        },
        async: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success" && res.code == 200) {
                var appInfo = res.data.appInfo;
                var applicationStatus = res.applicationStatus;
                var newObj = {'appInfo': appInfo, 'applicationStatus' : applicationStatus};
                applicationStatusRes.push(newObj);
            }
        }
    });
    return applicationStatusRes;
}


function nexttab(e){
    var curStep = $(e).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
        curInputs = curStep.find("input[type='text'],input[type='url']"),
        isValid = true;

    $(".form-group").removeClass("has-error");
    for(var i=0; i<curInputs.length; i++){
        if (!curInputs[i].validity.valid){
            isValid = false;
            $(curInputs[i]).closest(".form-group").addClass("has-error");
        }
    }
    if (isValid)
        nextStepWizard.removeAttr('disabled').trigger('click');
}

function getTenantDetail(id){
    $.ajax({
        type: 'post',
        url: '/RenterSmartMove',
        data: {
            class: "SmartMove",
            action: "getEmailId",
            id: id
        },
        async: false,
       success: function (response) {
             var res = JSON.parse(response);
             if (res.status == "success") {
                 console.log(res.data.tenantInfo.email);
                $('#renter_signup #email').val(res.data.tenantInfo.email);
                $('.renteremail').html(res.data.tenantInfo.email);
                $('.renterspm').html(res.data.company_name.company_name);
                $('.firstaddress').html(res.data.tenantInfo.unit_prefix +'-'+res.data.tenantInfo.unit_no +' '+res.data.tenantInfo.address1);
                $('.secondaddress').html(res.data.tenantInfo.city +','+res.data.tenantInfo.state+' '+res.data.tenantInfo.zipcode);
                if(res.data.tenantInfo.email !='' &&  typeof res.data.tenantInfo.email != 'undefined'){
                    $('#renter_signup #email').prop('readonly',true);
                }
             }
        }
    });
}

function signUpRenterSmartMove(formdata){
    $.ajax({
        type: 'POST',
        url: '/RenterSmartMove',
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.code == 200 && res.status == 'success') {
                toastr.success(res.message);
                setTimeout(function(){
                    var tokenId = getParameters('token');
                    if (tokenId != "") {
                        window.location.href ="/RenterInfo?token="+tokenId+'&tagId='+res.last_insert_id;
                    }
                }, 2000);
            }else{
                toastr.success(res.message);
            }
        }
    });
    return false;
}

function addRenterPersonalInfo(formdata){
    $.ajax({
        type: 'POST',
        url: '/RenterSmartMove',
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success" && res.code == 200) {
                if(res.data.tenantInfo.email !='undefined' &&  typeof res.data.tenantInfo.email !=  undefined && res.data.tenantInfo.email !=''){
                    $('#TenantDetail_frm #email').val(res.data.tenantInfo.email);
                    $('#TenantDetail_frm #email').prop('readonly',true);
                }
                window.location.href ="/RenterPersonalInfo";
            }
               var post_data={name:''};

        }
    });
    return false;
}
function addRenterAccountdetail(formdata) {
    $.ajax({
        type: 'POST',
        url: '/RenterSmartMove',
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success" && res.code == 200) {
                var tenantid = res.data.tenantid;
                getRenterDetails(tenantid);
                console.log("tenantid = " + tenantid);
            }
        }
    });
    return false;
}

function getRenterDetailsForApi(tenantId, rowId){
    var renterDetails;
    $.ajax({
        type: 'POST',
        url: '/RenterSmartMove',
        async: false,
        data: {
            id: tenantId,
            rowId: rowId,
            class: "SmartMove",
            action: "getRenterDetails2"
        },
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success" && res.code == 201) {
                renterDetails = res.data;
            }else{
                renterDetails = "";
            }
        }
    });
    return renterDetails;
}

function getRenterDetails(tenantId){
    $.ajax({
        type: 'POST',
        url: '/RenterSmartMove',
        data: {
            id: tenantId,
            class: "SmartMove",
            action: "getRenterDetails"
        },
        async: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success" && res.code == 200) {
                var record = res.data;
                $('#renter_personalInfo_form .first_name').val(record.first_name);
                $('#renter_personalInfo_form .last_name').val(record.last_name);
                $('#renter_personalInfo_form .middle_name').val(record.middle_name);
                $('#renter_personalInfo_form .dob').val(record.dob);
                $('#renter_personalInfo_form .ssn_sin_id').val(record.ssn_sin_id);
                //$('#renter_personalInfo_form .cssn_sin_id').val(record.ssn_sin_id);
                $('#renter_personalInfo_form .city').val(record.city);
                $('#renter_personalInfo_form .state').val(record.state);
                $('#renter_personalInfo_form .zipcode').val(record.zipcode);
                $('#renter_personalInfo_form .primary_phone').val(record.phone_number);
                /*$('#renter_personalInfo_form .phone_number').val(record.phone_number);
                $('#renter_personalInfo_form .phone_number1').val(record.phone_number);*/
                $('#renter_personalInfo_form .address1').val(record.address1);
                $('#renter_personalInfo_form .address2').val(record.address2);

                $('#renter_confrirmation_form .first_name').text(record.first_name);
                $('#renter_confrirmation_form .last_name').text(record.last_name);
                $('#renter_confrirmation_form .middle_name').text(record.middle_name);
                $('#renter_confrirmation_form .dob').text(record.dob);
                $('#renter_confrirmation_form .ssn_sin_id').text(record.ssn_sin_id);
                /*$('#renter_confrirmation_form .cssn_sin_id').text(record.ssn_sin_id);*/
                $('#renter_confrirmation_form .city').text(record.city);
                $('#renter_confrirmation_form .state').text(record.state);
                $('#renter_confrirmation_form .zipcode').text(record.zipcode);
                $('#renter_confrirmation_form .phone_number').text(record.phone_number);
                $('#renter_confrirmation_form .address1').text(record.address1);
                $('#renter_confrirmation_form .address2').text(record.address2);
            }
        }
    });
    return false;
}

function addRenterIdentityVerify(formData, tenantId, rowId) {
    $.ajax({
        type: 'POST',
        url: '/RenterSmartMove',
        data: formData,
        async: false,
        contentType: false,
        processData: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success" && res.code == 200 ) {
                var name = res.data.company_name;
                window.location.href= window.location.origin+"/RenterPayment?id="+rowId+"&ttoken="+tenantId+"&CPN="+name;
                return false;
            }
            window.location.href = window.location.href;
        }
    });
    return false;
}

function RenterPayment(formdata){
    $.ajax({
        method: 'POST',
        url: '/RenterSmartMove',
        data: formdata,
        async: false,
        contentType: false,
        processData: false,
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == "success") {
                console.log(res.message);
            }
        }
    });
    return false;
}

function getParameters(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}

function getServerTime(){
    var serverTime = "";
    $.ajax({
        url: 'https://cors-anywhere.herokuapp.com/https://smlegacygateway-integration.mysmartmove.com/LandlordApi/v3/ServerTime',
        type: 'get',
        async: false,
        success: function (response, status, xhr) {
            var serverTimes = response.split(".");
            serverTime = serverTimes[0];
            console.log("servertime response="+serverTime);
        },
        error: function (jqXHR, exception) {
            var msg = ''; 
            if (jqXHR.status === 0) {
                msg = 'Not connect. Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            console.log("Servertime response in Error function = "+msg);
        }
    });
    return serverTime;
}

function getSecurityHeaders(serverTime){
    var securityKey = '387KmgTwB6eUZx8Uwqllp9aao5nNGtMNBRiEYGZc+wAX383gfieO/L5HiNvEE5Lf7DH448ujhItBuU7BJ8CvqA==';
    var securityHeaderToken = "";
    $.ajax({
        type: 'post',
        url: '/smartmove',
        async: false,
        data: {
            class: "SmartMove",
            action: "generateSmartMoveSecurityHeader",
            securityKey: securityKey,
            serverTime: serverTime
        },
        success: function (response) {
            var res = JSON.parse(response);
            securityHeaderToken = res.hash_key;
        }
    });
    return securityHeaderToken;
}

function createRenter( renter,serverTime, securityToken) {
    var createRenterRes;
    var headerSetUp = {'Authorization':'smartmovepartner partnerId="212", serverTime:"'+serverTime+'", securityToken="'+securityToken+'"'};
    $.ajax({
        url: 'https://cors-anywhere.herokuapp.com/https://smlegacygateway-integration.mysmartmove.com/RenterApi/v3/Renter',
        type: 'post',
       // contentType: "application/json",
        dataType: 'json',
        /*accepts: {json: 'application/json'},*/
        data: renter,
        async: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", headerSetUp);
        },
        success: function (data) {
            createRenterRes = data;
        },error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect1. Verify Network.1';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            createRenterRes = msg;
            console.log("msg = "+msg);
        }
    });
    return createRenterRes;
}

function acceptRenterApplicationStatus( applicants, applicationId, serverTime, securityToken) {
    var applicationStatusRes;
    var headerSetUp = {'Authorization':'smartmovepartner partnerId="212", serverTime:"'+serverTime+'", securityToken="'+securityToken+'"'};
    var apiurl = 'https://cors-anywhere.herokuapp.com/https://smlegacygateway-integration.mysmartmove.com/RenterApi/v3/ApplicationRenterStatus';
    //var apiurl = 'https://smlegacygateway-integration.mysmartmove.com/RenterApi/v3/ApplicationRenterStatus';
    $.ajax({
        url: apiurl.concat('/Accept?email=john@yopmail.com&applicationId='+applicationId), 
        type: 'put',             
        dataType: 'json',
        accepts: {json: 'application/json'},
        async: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", headerSetUp);
        },
        success: function (data) {
            applicationStatusRes = data;
            console.log("Application renter status");
        },error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect1. Verify Network.1';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            applicationStatusRes = msg;
            console.log("msg = "+msg);
        }
    });
    return applicationStatusRes;
}

function checkRenterApplicationStatus( applicants, applicationId, serverTime, securityToken) {
    var applicationStatusRes;
    var headerSetUp = {'Authorization':'smartmovepartner partnerId="212", serverTime:"'+serverTime+'", securityToken="'+securityToken+'"'};
    var apiurl = 'https://cors-anywhere.herokuapp.com/https://smlegacygateway-integration.mysmartmove.com/RenterApi/v3/ApplicationRenterStatus';
    $.ajax({             
        url: apiurl.concat('?email="'+applicants+'"&applicationId="'+applicationId+'"'), 
        type: 'get',             
        dataType: 'json',
        /*accepts: {json: 'application/json'},*/
        async: false,
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", headerSetUp);
        },
        success: function (data) {
            applicationStatusRes = data;
            console.log("Application renter status");
        },error: function (jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect1. Verify Network.1';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            applicationStatusRes = msg;
            console.log("msg = "+msg);
        }
    });
    return applicationStatusRes;
}

function stripePaymentVariables(){
    var stripe = Stripe('pk_test_jF5tXlX0qLcCR1YAX9WB8FaL');
    // Create an instance of Elements.
    var elements = stripe.elements();
    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    var cardElemLength = $("#card-element").length;
    if (cardElemLength > 0) {
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            $(".submit_payment_class").attr("disabled",true);
            event.preventDefault();
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    var token_id = result.token.id;
                    stripeTokenHandler(token_id);
                }
           });
        });
    }
}

function stripeTokenHandler(token) {
    //var tokenId = getParameters('ttoken');
    var tokenId = $("input[name='tenant_id_input']").val();
    var currency = 'USD';
    var Amount = '35';
    var user_type = 'TENANT';
    var user_id = tokenId;
    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'smartMovePayment',
            "class": 'Stripe',
            "token_id":token,
            "currency":currency,
            "amount": Amount,
            "user_id":user_id,
            "user_type":user_type
        }, beforeSend: function() {
            $('#loadingmessage').show();
        },
        success: function (response) {
            $('#loadingmessage').hide();
            var response = JSON.parse(response);
            if(response.status=='success'){
                toastr.success('Payment has been done successfully');
                saveTransactionDetails(response.charge_id, response.charge_data,user_id);
                /*var serverTime = getServerTime();
                var securityHeaderToken = getSecurityHeaders(serverTime);*/
                //createRenter( renter, serverTime, securityHeaderToken);

            }else{
                toastr.error(response.message);
            }
        }
    });
}

function saveTransactionDetails(charge_id,charge_data, user_id){
    $.ajax({
        type: 'post',
        url: '/RenterSmartMove',
        data: {
            class: "SmartMove",
            action: "saveTransactionDetails",
            charge_id: charge_id,
            charge_data: charge_data,
            user_id: user_id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status=='success' && response.code == 200){
                window.location.href = window.location.origin+"/SM/V3/Renters?token="+user_id;
                return false;
            }
        }
    });
}

function getStateSortForm(state){
    var stateArray = {"AL":"Alabama","AK":"Alaska","AZ":"Arizona","AR":"Arkansas","CA":"California","CO":"Colorado","CT":"Connecticut","DE":"Delaware","FL":"Florida","GA":"Georgia","HI":"Hawaii","ID":"Idaho","IL":"Illinois","IN":"Indiana","IA":"Iowa","KS":"Kansas","KY":"Kentucky","LA":"Louisiana","ME":"Maine","MD":"Maryland","MA":"Massachusetts","MI":"Michigan","MN":"Minnesota","MS":"Mississippi","MO":"Missouri","MT":"Montana","NE":"Nebraska","NV":"Nevada","NH":"New Hampshire","NJ":"New Jersey","NM":"New Mexico","NY":"New York","NC":"North Carolina","ND":"North Dakota","OH":"Ohio","OK":"Oklahoma","OR":"Oregon","PA":"Pennsylvania","RI":"Rhode Island","SC":"South Carolina","SD":"South Dakota","TN":"Tennessee","TX":"Texas","UT":"Utah","VT":"Vermont","VA":"Virginia","WA":"Washington","WV":"West Virginia","WI":"Wisconsin","WY":"Wyoming","AS":"American Samoa","DC":"District of Columbia","FM":"Federated States of Micronesia","GU":"Guam","MH":"Marshall Islands","MP":"Northern Mariana Islands","PW":"Palau","PR":"Puerto Rico","VI":"Virgin Islands"};
    for (var prop in stateArray) { 
        if (stateArray.hasOwnProperty(prop)) { 
            if (stateArray[prop] === state) 
            return prop; 
        } 
    } 
}