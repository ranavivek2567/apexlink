<div id="welcome-newproperty" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body ">
                <table>
                    <tr>
                        <table style="width: 95%">
                            <tr>
                                <td style="padding-bottom: 15px;">
                                    <img src="SITEURL/images/logo.png" alt="emaillogo" border="3" height="45px" width="270px">
                                </td>
                            </tr>
                        </table>
                    </tr>
                    <tr>
                        <table style="width: 95%">
                            <tr>
                                <td style="margin-top: 35px;  display: inline-block; font-style: normal;  font-weight: 600;font-size: 20px;">
                                    Hello OWNERNAME,
                                </td>
                            </tr>
                            <tr>
                                <td style="margin-top: 28px;  display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">
                                    You can now make a lease decision regarding the SmartMove application - 999984542708, for the property located at:
                                </td>
                            </tr>
                            <tr>
                                <td style="margin-top: 35px;  display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">
                                   HOUSENAME
                                </td>
                            </tr>
                            <tr>
                                <td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">
                                   STREETADDRESS
                                </td>
                            </tr>
                            <tr>
                                <td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">
                                   CITYSTATEZIP,
                                </td>
                            </tr>
                            <tr>
                                <td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px; margin-top: 30px">
                                    Please note that the recommendation and the prospective tenant's credit and criminal reports remain valid for 60 days. <b style="font-weight: 800">Please Click here to access your account and make a lease decision regarding this application.</b>  If you have any questions regarding the email, please contact Customer Support at 1-866-775-0961.
                                </td>
                            </tr>
                            <tr>
                                <td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px; margin-top: 30px">
                                    Sincerly
                                </td>
                            </tr>
                            <tr>
                                <td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 18px;">
                                    The SmartMove Team
                                </td>
                            </tr>
                            <tr>
                                <td style="display: inline-block; font-style: normal;  font-weight: 400;font-size: 15px; margin-top: 30px">
                                    Powered by TransUnion, a trusted source of consumer data, SmartMove is a quick and easy way for rental applications like you to safely provide background information to property owners for consideration in the leasing process. At no point will the property owner have to access to your SSN and other sensitive personal information. And your credit rating will not be affected in any way.

                                </td>
                            </tr>
                        </table>
                    </tr>
                    <tr>
                        <table style="width: 95%; background: #0095c0; margin-top: 30px;">
                            <tr>
                                <td style="width: 72%; padding: 10px 0 0 20px;">
                                    <img src="SITEURL/images/white-logo.png" alt="footerlogo" border="3" height="45px" width="270px">
                                </td>
                                <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400; padding: 20px 0 0 0;">
                                    6430  S. Fiddler's Green Circle
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 72%;">

                                </td>
                                <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400;">
                                    Suite #500 Greenwood Village
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 72%; font-size: 12px; font-style: normal; color: #ffffff; font-weight: 300; padding: 10px 0 25px 20px;">
                                    @2015 TRANSUNION RENTAL SCREENING SOLUTIONS. INC. ALL RIGHT RESERVED
                                </td>
                                <td style="width: 28%; font-size: 14px; font-style: normal; color: #ffffff; font-weight: 400; padding: 10px 0 25px 0px;">
                                    CO 80111
                                </td>
                            </tr>
                        </table>
                    </tr>
                </table>

            </div>

        </div>

    </div>
</div>
