<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/smartmove/renter/renter_header.php");
$id = 0;
if (isset($_GET['token']) && $_GET['token'] != ""){
    $id = $_GET['token'];
}
?>
<div id="main-page" class="modal fade smart-move-modal in" role="dialog" style="display:block">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header smart-move-header-top">
                <header class="smart-move-header">
                    <div class="header-top">
                        <div class="container">
                            <div class="row d-flex align-item-center">
                                <div class="col-sm-6">
                                    <a href="index.html" class="logo"><img src="<?php echo COMPANY_SUBDOMAIN_URL;?>/images/logo.png" alt="logo" class="img-responsive"></a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="header-bottom">
                        <nav class="navbar">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Screen a Tanent</a></li>
                                    <li><a href="#">For Renters</a></li>
                                    <li><a href="#">News</a></li>
                                    <li><a href="#">Support</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </header>
            </div>
                <div class="modal-body p-0 sm-signup-form-body">
                    <h2 class="sm-main-heading">Sign Up for SmartMove</h2>
                    <form method="post" id="renter_signup">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>Create Your Account</h4>
                                <div class="form-group">
                                    <label>Account Type: <span class="required">*</span></label>
                                    <select class="form-control" name="renter">
                                        <option value="2" selected="">Renter</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Email: <span class="required">*</span></label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Enter Your Email Address">
                                </div>
                                <div class="form-group form-group-newarea">
                                    <div class="col-sm-12 renter-form-application-container renter-form-application-container-new">
                                        <div class="col-sm-12">Renter Application Found for: <span class="renteremail">renter@email.com</span></div>
                                        <div class="col-sm-12"><span class="renterspm">LANDLORD</span> has requested access to your credit and criminal background information as part of your application to rent the property at:</div>
                                        <div class="col-sm-12 firstaddress">555 N Street</div>
                                        <div class="col-sm-12 secondaddress">City, State ZIP</div>
                                        <div class="col-sm-12">To being the process using SmartMove's secure online system, accept the Renter Service's Agreement by clicking "Accept" below.</div>
                                    </div>
                                </div>
                                <div class="form-group form-group-newarea">
                                    <label>Service Agreement:</label>
                                    <textarea class="form-control" name="service_agreement">Welcome to TransUnion® SmartMove® (“SmartMove” or “Service”), (“SmartMove” or ”Service”), found at the web site, www.mysmartmove.com, or through this integration platform (the “Site”) - a product of TransUnion Rental Screening Solutions (“TURSS”). This Terms of Use Agreement (“Agreement”) contains the terms and conditions upon which you (“you,” or the “member”) may access SmartMove or any products or services offered through the Site. You agree to be legally bound by these terms.
General Terms
TURSS provides access to SmartMove to allow you to authorize and agree to provide your credit and public record information (“Credit Information”) to third parties through this platform.  From time to time TURSS and or the owner of this platform may revise these terms and conditions. Revisions will be effective when posted or as otherwise stated. Additional terms and conditions may apply to specifics of other products and services, or to the participation in future contests or surveys.
After agreeing to the terms of this Agreement, you will be requested to authenticate your identity (using information from public records and credit information related to the identifying information that you provide (e.g. Social Security Number, Date of Birth) (“Identifying Information”)) and acknowledge that upon successful completion of identity verification, your Credit Information will be scored by TURSS and provided to third parties indicated on your request via this platform.  You acknowledge and agree that TURSS and the platform provider is not responsible for any actions or decisions made by any third parties with whom you have agreed to share your Credit Information.
The images, text, screens, web pages, materials, data, content and other information (“Content”) used and displayed through SmartMove and/or the Site are the property of TURSS or its licensors and are protected by copyright, trademark and other laws. In addition to its rights in individual elements of the Content within the Site, TURSS owns intellectual property rights in the selection, coordination, arrangement and enhancement of such Content. None of the Content may be copied, displayed, distributed, downloaded, licensed, modified, published, reposted, reproduced, reused, sold, transmitted, used to create a derivative work or otherwise used for public or commercial purposes without the express written permission of TURSS or the owner of the Content.
Disclaimer of Warranties and Liabilities
SMARTMOVE AND THE SITE, INCLUDING ALL CONTENT, CREDIT INFORMATION, PRODUCTS AND SERVICES MADE AVAILABLE ON OR ACCESSED THROUGH THIS SITE, ARE PROVIDED TO YOU “AS IS”. TO THE FULLEST EXTENT PERMISSIBLE UNDER APPLICABLE LAW, NEITHER TURSS NOR ITS AFFILIATES NOR ITS DATA PROVIDERS MAKE ANY REPRESENTATIONS OR WARRANTIES OF ANY KIND WHATSOEVER AS TO THE CONTENT, CREDIT INFORMATION, PRODUCTS OR SERVICES AVAILABLE ON OR ACCESSED THROUGH SMARTMOVE AND THE SITE, THAT YOU OR A THIRD PARTY WILL HAVE CONTINUOUS, UNINTERRUPTED OR SECURE ACCESS TO SMARTMOVE OR THE SITE, PRODUCTS OR SERVICES OR THAT SMARTMOVE, THE SITE, PRODUCTS OR SERVICES WILL BE ERROR-FREE. IN ADDITION, TURSS AND ITS AFFILIATES AND ITS DATA PROVIDERS DISCLAIM ALL EXPRESS OR IMPLIED WARRANTIES, INCLUDING TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT AND INFORMATIONAL CONTENT. THEREFORE, YOU AGREE THAT YOUR ACCESS TO AND USE OF SMARTMOVE, THE SITE, PRODUCTS, SERVICES AND CONTENT ARE AT YOUR OWN RISK. ADDITIONALLY, YOU AGREE THAT THE CREDIT INFORMATION THAT YOU AUTHORIZE TURSS TO SHARE WITH THIRD PARTIES IS NOT ERROR-FREE AND MAY INCLUDE INFORMATION THAT DOES NOT PERTAIN TO YOU AND TURSS AND ITS AFFILIATES AND ITS DATA PROVIDERS ARE NOT RESPONSIBLE OR LIABLE FOR ANY ACTION OR DECISION TAKEN BY A THIRD PARTY BASED ON THE CREDIT INFORMATION.  BY USING OUR SITE, YOU ACKNOWLEDGE AND AGREE THAT NEITHER TURSS NOR ITS AFFILIATES NOR ITS DATA PROVIDERS HAVE ANY LIABILITY TO YOU (WHETHER BASED IN CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE) FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL OR SPECIAL DAMAGES ARISING OUT OF OR IN ANY WAY CONNECTED WITH YOUR ACCESS TO OR USE OF SMARTMOVE, THE SITE, CONTENT, PRODUCTS OR SERVICES (EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), INCLUDING LIABILITY ASSOCIATED WITH ANY VIRUSES WHICH MAY INFECT YOUR COMPUTER EQUIPMENT. YOU ACKNOWLEDGE AND AGREE THAT TURSS’S DATA PROVIDERS ARE A THIRD PARTY BENEFICIARY OF THE PROVISIONS OF THIS SECTION, WITH RIGHT OF ENFORCEMENT.
TURSS reserves the right to publish or use any responses, questions or comments emailed to this address for promotional or other purposes without any further permission, notice or payment of any kind to the sender. All such submissions are the property of TURSS.
Registration and Accurate Information
If you decide to register on the Site or use SmartMove, you may be required to register, provide personal information, and select a user name and password. You agree to provide accurate information in your registration and not to share your password with third parties. You agree not to impersonate another person or to select or use a user name or password of another person. You agree to notify TURSS promptly of any unauthorized use of your account and of any loss, theft or disclosure of your password. Failure to comply with these requirements shall constitute a breach of these terms and conditions and shall constitute grounds for immediate termination of your account and your right to use the Site.
Our Privacy Policy
By using the Site or purchasing products or services, you agree that we may use and share your personal information in accordance with the terms of our Privacy Statement. Our privacy statement can be reached at https://www.mysmartmove.com/privacy-policy.html
Fair Credit Reporting Act
The Fair Credit Reporting Act allows you to obtain from each credit reporting agency a disclosure of all the information in your credit file at the time of the request. Full disclosure of information in your file at a credit reporting agency must be obtained directly from such credit reporting agency. The credit reports provided or requested through our Site are not intended to constitute the disclosure of information by a credit reporting agency as required by the Fair Credit Reporting Act or similar laws.
You are entitled to receive a free copy of your credit report from a credit reporting agency if:
  • You have been denied or were otherwise notified of an adverse action related to credit, insurance, employment, or a government granted license or other government granted benefit within the past sixty (60) days based on information in a credit report provided by such agency.
  • You have been denied house/apartment rental or were required to pay a higher deposit than usually required within the past sixty (60) days based on information in a consumer report provided by such agency.
  • You certify in writing that you are a recipient of public welfare assistance.
  • You certify in writing that you have reason to believe that your file at such credit reporting agency contains inaccurate information due to fraud.
In addition, if you are a resident of Colorado, Maine, Maryland, Massachusetts, New Jersey, or Vermont, you may receive one free copy of your credit report each year from the credit bureaus. If you are a resident of Georgia, you may receive two free copies of your credit report each year from the credit bureaus. Otherwise, a consumer reporting agency may impose a reasonable charge for providing you with a copy of your credit report.
The Fair Credit Reporting Act provides that you may dispute inaccurate or incomplete information in your credit report. YOU ARE NOT REQUIRED TO PURCHASE YOUR CREDIT REPORT FROM THE SITE OR SMARTMOVE IN ORDER TO DISPUTE INACCURATE OR INCOMPLETE INFORMATION IN YOUR REPORT OR TO RECEIVE A COPY OF YOUR REPORT FROM EQUIFAX, EXPERIAN OR TRANSUNION, THE THREE NATIONAL CREDIT REPORTING AGENCIES, OR FROM ANY OTHER CREDIT REPORTING AGENCY.

Term and Termination; Modification
This Agreement will take effect at the time you click “I Accept,” and shall terminate (a) when either party gives notice of its intention to terminate to the other party hereto, to TURSS via the toll-free number set forth on the web site, or (b) if TURSS discontinues providing SmartMove or access to the Site.

TURSS may (i) change the terms of this Agreement or the feature of the Service, or (ii) change the Site, including eliminating or discontinuing any content or feature of the Site, restricting the hours of availability, or limiting the amount of use permitted, by posting notice of such modification on a page of the Site before the modification takes effect. All changes shall be effective immediately upon posting of such notice. If you use SmartMove and/or the Site after TURSS has notified you of a change in the Agreement you agree to be bound by all of the changes. You are expected to review the Site periodically to ensure familiarity with any posted notices of modification.

Policy Regarding Children
We define children as individuals under the age of 18. Our Web Site is not intended for the use of children and we do not intend to collect information about children through our Web Site. You must be at least 18 to access any products through this website.

Notices
You should send any notices or other communications regarding SmartMove or our Site to TransUnion Rental Screening Solutions, 6430 S. Fiddler’s Green Circle, Suite 500, Greenwood Village, CO 80111.

Except as otherwise provided, we may send any notices to you to the most recent e-mail address you have provided to us or, if you have not provided an e-mail address, to any e-mail or postal address that we believe is your address. If you wish to update your registration information, please log in to your account and visit the ‘Your Account’ section from the main menu.

Applicable Law
The laws applicable to the interpretation of these terms and conditions shall be the laws of the State of Illinois, USA, and applicable federal law, without regard to any conflict of law provisions. TURSS can provide credit reports only on individuals who have established credit in the United States. Those who choose to access this Site from outside the United States do so on their own initiative and are responsible for compliance with local laws. You agree that any and all disputes arising under this Agreement or out of TURSS’ provision of services to you, if submitted to a court of law, shall be submitted to the state and federal courts of Northern District of Illinois, USA.
Permission Statement
You understand that by clicking on the “I Accept” button immediately following this notice, you are providing “written instructions” to TransUnion Rental Screening Solutions to obtain information from your personal credit profile from TransUnion and public records sources. You authorize TransUnion Rental Screening Solutions to obtain such information and to score such information and provide it to certain identified third parties who are requesting this information about you. Before you may use TransUnion SmartMove, we must obtain “written instructions” from you to give us permission to obtain your TransUnion credit history and public records report and share them with certain identified third parties.

You further understand that, by providing such written instruction, such third parties may include multiple subscribers to the SmartMove service who have separately certified a permissible purpose to obtain your information (each a “Joint User”).  Such Joint Users, may include, for example, a landlord and a landlord’s leasing agent; multiple employees of a property management company; or other Joint Users who have a shared reason to obtain and review your report in connection with an application for rental housing.

Permission Confirmation
Please confirm your authorization for TransUnion Rental Screening Solutions to obtain your credit profile from TransUnion and public records sources and to score such information and provide it to certain identified third parties who are requesting this information about you.
</textarea>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="accept_term" value="1">I accept the smartmove service agreement <span class="required">*</span> <span class="termcond"></span></label></div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="tenantid" id="tenantid" value="<?php echo $id; ?>" />
                                    <button type="button" class="btn mr-2 greenplan-button" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn  yellow-button save_tenantsmartmove" data-toggle="modal" data-target="#multistep-signup-modal" data-dismiss="modal">Submit</button>
                                </div>
                            </div>
                            <div class="col-sm-5 pull-right">
                                <h4>Tenant Screening you can trust:</h4>
                                <p class="text">SmartMove is a product developed by TransUnion, a trusted source of
                                    consumer data and a global leader in credit and information management.</p>
                                <p class="text">SmartMove ensures that:</p>
                                <ul class="list">
                                    <li>Your credit rat ng is completely unaffected</li>
                                    <li>The screening process is quick easy—and all online</li>
                                    <li>Only limited Information is provided to the property manager — your SSN and account numbers are not provided</li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
</div>
<?php
include_once($_SERVER['DOCUMENT_ROOT'] . "/company/views/smartmove/renter/renter_footer.php");
