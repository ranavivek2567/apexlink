<table border="0" cellspacing="0" cellpadding="0" align="center" style="border:1px solid #dddddd; width:640px">
    <tbody>
    <tr style="background-color:#00b0f0; height:20px">
        <td>&nbsp; </td>
    </tr>
    <tr style="border-collapse:collapse; background-color:#fff">
        <td class="x_w580" width="580" style="font-family:'Helvetica Neue',Arial,Helvetica,Geneva,sans-serif; border-collapse:collapse">
            <div style="margin-top: 1px; margin-bottom: 10px; text-align:center">
                <p class="clsDefaultImage">
                    <img alt="Apexlink" width="100" src="#logo#">
                </p>
            </div>
        </td>
    </tr>
    <tr style="background-color:#00b0f0; height:20px">
        <td>&nbsp; </td>
    </tr>
    <tr>
        <td style="padding:20px">
            <h3 style="color:#00b0f0; font-family:arial; font-size:x-large; text-align:center">
                Welcome to ApexLink&nbsp;</h3>
            <p style="font-family:arial">Hi #user_name#,</p>
            <strong style="font-family:arial">Enter following code to login apexlink: #token_2fa#</strong>
            <p style="font-family:arial">Remember, if you need any assistance, you can contact us 24/7.</p>
            <p style="font-family:arial">Welcome,</p>
            <p style="font-family:arial">Your ApexLink Support Team</p>
        </td>
    </tr>
    <tr>
        <td align="center" bgcolor="#05a0e4" style="padding:10px; font-size:12px; color:#ffffff; font-weight:bold">
            ApexLink Property Manager●&nbsp;<a href="UrlBlockedError.aspx" style="color:#fff; text-decoration:none" target="_blank">support@apexlink.com ●772-212-1950</a>
        </td>
    </tr>
    </tbody>
</table>