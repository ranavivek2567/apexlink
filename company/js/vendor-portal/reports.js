$(document).ready(function () {

    $(document).on('click', '#ancVendor1099SummaryFilterOpen', function () {
        $("#vendorSummaryReportModal").modal('show');
         $.ajax({
            type: 'post',
            url: '/vendorReportsAjax',
            data: {
                class: 'reportsView',
                action: 'taxHtml'},
            success: function (response) {
                var res = JSON.parse(response);
                $('.selectTaxhtml').html(res.data);
                var element = $(".selectPropertyhtml");
                element.html(res.propertyhtml).multiselect("destroy").multiselect({
                    includeSelectAllOption: true,
                    nonSelectedText: 'Select Property'
                });

            },
        });
    })

    $(document).on('click', '.cancelvendorReport', function ()
    {
        $("#vendorSummaryReportModal").modal('hide');

    })

    $(document).on("submit", "#vendorSummaryForm", function (e) {
        e.preventDefault();
        if ($('#vendorSummaryForm').valid()) {
            var selectTaxhtml = $('.selectTaxhtml').val();
            var selectPropertyhtml = $('.selectPropertyhtml').val();
            var formData = {
                'selectTaxhtml': selectTaxhtml,
                'selectPropertyhtml': selectPropertyhtml

            };
            $.ajax({
                type: 'post',
                url: '/vendorReportsAjax',
                data: {
                    class: 'reportsView',
                    action: 'getVendorSummaryReport',
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                        window.location.href = '/VendorPortal/Reporting/Vendor1099SummaryReport';
                        $('.rentroll').html(response.html);

                }
            });
        }
    })

});







