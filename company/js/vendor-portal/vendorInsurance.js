fetchVendorDetails(vendor_unique_id);
function fetchVendorDetails(vendor_id) {
    $.ajax({
        type: 'post',
        url: '/vendor-add-ajax',
        data: {class: 'addVendor', action: "getVendorDetail", vendor_id: vendor_id},
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == 'success' && res.code == 200) {
                $(".credentialMainDiv").html(res.CredentialInfo);
                $('.CredentialTypeplus').hide();
                $('.credential-control-plus').hide();
                $('.credential-control-minus').hide();
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

