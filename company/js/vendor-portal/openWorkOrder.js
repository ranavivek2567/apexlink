$(document).ready(function () {
    //

    //
    if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }
    function statusFmatter1(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = ['Edit','View'];

            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    // /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);
        var base_url = window.location.origin;
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var email = $("tr#"+id).find("td").eq(2).text();
        console.log('email',email);
        var user_id = $(this).attr('data-user_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        var is_default = $(this).attr('is_default');
        if (opt == 'Edit' || opt == 'EDIT') {
            window.location.href = base_url + '/VendorPortal/WorkOrderVendor?id='+id;
            var ele = $('#FormWorkOrder .send_alert').val();

        }
        if (opt == 'VIEW' || opt == 'View') {
            $("#listing_id").hide();
            $("#workorder_id").val(id);
            var w_id=$("#workorder_id").val();
            workorderdata(w_id);
            fileLibrary(w_id);
            $("#view_id").show();

        }

    });

    if(localStorage.getItem("table_green_tableid")){
        setTimeout(function(){
            var tableId = localStorage.getItem("table_green_tableid");
            jQuery(tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery(tableId).find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('table_green_id');
            localStorage.removeItem('table_green_tableid');
        }, 2000);
    }

    $(document).on('click', '#workorder_table tr:eq(1) td:not(td:eq(0),td:eq(11))', function () {
        var idd = $(this).closest('tr').find('td:last-child').find('select').attr('data_id');


        $("#listing_id").hide();
        $("#workorder_id").val(idd);

        var w_id=$("#workorder_id").val();

        workorderdata(idd);
        fileLibrary(w_id);
        $("#view_id").show();

    });



    $(document).on('click', '.edit_redirection', function () {
        var base_url=window.location.origin;
        var id=$("#workorder_id").val();

        window.location.href=base_url+'/WorkOrder/EditWorkOrder?id='+id;

    });



    $(document).on('click', '.edit_redirection', function () {
        var base_url=window.location.origin;
        var id=$("#workorder_id").val();

        window.location.href=base_url+'/WorkOrder/EditWorkOrder?id='+id;

    });


    $('input[type=radio][name=alt]').change(function() {
        if (this.value == 0) {

            $("#filter_div").hide();


        }
        else if (this.value == 1) {


            $("#filter_div").show();

        }
    });


    // $(document).on('click','.getAlphabet',function(){
    //     var grid = $("#guestcard_table"),f = [];
    //     var value = $(this).attr('data_id');
    //     var search = $(this).text();
    //     if(value != '0'){
    //         f.push({field:"name",op:"bw",data:search});
    //         grid[0].p.search = true;
    //         $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
    //         grid.trigger("reloadGrid",[{page:1,current:true}]);
    //     }
    // });



    function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {

            var data = '';
            var data = '<input type="checkbox" name="maintenance_checkbox[]" class="maintenance_checkbox" id="maintenance_checkbox' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
            return data;
        }
    }

    $(document).on('change','.common_radio',function(){
        var radio_value = '0';
        console.log(radio_value);
        if($('#one_time').is(':checked')){
            radio_value = $('#one_time').val();
        } else if ($('#recurring').is(':checked')){
            radio_value = $('#recurring').val();
        }

        var grid = $("#workorder_table"),f = [];
        var freq = $('#ddlWorkOrderListingFrequency').find(":selected").val();

        if(radio_value == '0'){
            f.push({field: "work_order.work_order_type", op: "eq", data: radio_value});
        } else {
            f.push({field: "work_order.work_order_type", op: "eq", data: radio_value},{field: "work_order.rec_month", op: "eq", data: freq});
        }

        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);

    });





    $("#select_all_complaint_checkbox").click(function () {
        if($(this).is(':checked')){
            $("#maintenance_checkbox tr:eq(1) td:eq(0)").prop('checked',true);
        }

    });
    $("#maintenance_checkbox").click(function () {
        $(".maintenance_checkbox").prop('checked', true);
    });

    workorder();
    function workorder() {
        var table = 'work_order';
        var columns = ['Work Order Number','email', 'Work Order Category', 'Vendor','Property','Created On','Caller/RequestedBy','Amount('+default_currency_symbol+')','Priority ','Status','Action'];
        var select_column = ['Edit','Email','TEXT','View','Add In-touch','In-Touch History','Delete'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column: 'user_id', value: vendor_portal_id, condition: '=',table:'work_order'}];
        var extra_columns = ['work_order.deleted_at', 'work_order.updated_at'];
        var joins = [{table: 'work_order', column: 'work_order_cat', primary: 'id', on_table: 'company_workorder_category'},
            {table: 'work_order', column: 'property_id', primary: 'id', on_table: 'general_property'},
            {table: 'work_order', column: 'priority_id', primary: 'id', on_table: 'company_priority_type'},
            {table: 'work_order', column: 'status_id', primary: 'id', on_table: 'company_workorder_status'},
            {table: 'work_order', column: 'vendor_id', primary: 'id', on_table: 'users'}];
        var columns_options = [
            {name:'Work Order Number',index:'work_order_number', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'email',index:'email', width:100,hidden:true,searchoptions: {sopt: conditions},table:'users'},
            {name:'Work Order Category',index:'category', width:200,searchoptions: {sopt: conditions},table:'company_workorder_category'},
            {name:'Vendor',index:'name', width:150,searchoptions: {sopt: conditions},table:'users'},
            {name:'Property',index:'property_name', width:100,searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Created On',index:'created_at', width:200,searchoptions: {sopt: conditions},table:table},
            {name:'Caller/RequestedBy',index:'request_id', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Amount',index:'estimated_cost', width:100,searchoptions: {sopt: conditions},table:table,formatter:currencyform},
            {name:'Priority',index:'priority', width:100,searchoptions: {sopt: conditions},table:'company_priority_type'},
            {name:'Status',index:'work_order_status', width:100,searchoptions: {sopt: conditions},table:'company_workorder_status'},
            {name:'Action',index:'',title:false, width:200,searchoptions: {sopt: conditions},table:table,formatter:statusFmatter1},
        ];

        var ignore_array = [];
        jQuery("#workorder_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Work Orders",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

    function currencyform(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var val="";
            if(cellvalue !== null && cellvalue!== 'undefined' && cellvalue!==''){
                //     var symbol = default_symbol;
                var amt = changeToFloat(cellvalue.toString());
                val=default_currency_symbol+amt;
            }
            else{
                val="";
            }
            return val;
        }
    }



    function deleteorder(id) {
        $.ajax({
            type: 'post',
            url: '/work-order-ajax',
            data: {
                class: 'workOrder',
                action: 'deleteworkorder',
                id : id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    onTop(true);
                    $('#workorder_table').trigger('reloadGrid');

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    function fileLibrary(id) {
        var table = 'work_file_library';
        var columns = ['Name', 'Preview'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        // var extra_columns = []
        var extra_where = [{column: 'work_order_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Name', index: 'name', width: 600, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'Preview', index: 'extension', width: 600, align: "center", searchoptions: {sopt: conditions}, search: false, table: table, formatter: imageFormatter},
        ];
        var ignore_array = [];
        jQuery("#workorder_files").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                // extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            // rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Vendor Files",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {},
            {},
            {},
            {top: 10, left: 400, drag: true, resize: false}
        );
    }


    /**
     * jqGrid function to format file extension
     * @param status
     */
    function imageFormatter(cellvalue, options, rowObject) {
        var upload_url = window.location.origin;
        if (rowObject !== undefined) {
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            } else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            }
            return '<img class="img-upload-tab" width=30 height=30 src="' + src + '">';
        }
    }



    function workorderdata(id){
        $.ajax({
            type: 'post',
            url: '/work-order-ajax',
            data: {
                class: 'workOrder',
                action: 'getworkorderdata',
                id : id
            },
            success: function (response) {
                var data = JSON.parse(response);
                console.log(data.data);

                if (data.status == "success"){

                    var html = "";
                    html +=  '<thead>'+
                        '<tr  style="border:1px solid #C9C9C9;">'+
                        '<th scope="col" class="col-sm-6" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Property'+' </th>'+
                        '<th scope="col" class="col-sm-6" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Unit Number'+'</th>'+
                        '</tr>'+
                        '</thead>'+
                        '<tr style="border-left:1px solid #afafaf; border-right:1px solid #afafaf;">'+
                        '<td class="col-sm-6" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;" >'+ data.data.property_name+'</td>'+
                        '<td class="col-sm-6" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data.data.units+'</td>'+

                        '</tr>';

                    $("#unit_table").append(html);

                    var html1 = "";
                    html1 +=  '<thead>'+
                        '<tr  style="border:1px solid #C9C9C9;">'+
                        '<th scope="col" class="col-sm-4" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+' Name'+' </th>'+
                        '<th scope="col" class="col-sm-4" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Email'+'</th>'+
                        '<th scope="col" class="col-sm-4" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Phone Number'+'</th>'+
                        '</tr>'+
                        '</thead>'+
                        '<tr style="border-left:1px solid #afafaf; border-right:1px solid #afafaf;">'+
                        '<td class="col-sm-4" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;" >'+ data.data.tenant_name+'</td>'+
                        '<td class="col-sm-4" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data.data.email+'</td>'+
                        '<td class="col-sm-4" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data.data.phone+'</td>'+

                        '</tr>';

                    $("#tenant_table").append(html1);


                    var html2 = "";

                    html2 +=  '<thead>'+
                        '<tr  style="border:1px solid #C9C9C9;">'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+' Name'+' </th>'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Category'+'</th>'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Email'+'</th>'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Phone'+'</th>'+
                        '</tr>'+
                        '</thead>'+
                        '<tr style="border-left:1px solid #afafaf; border-right:1px solid #afafaf;">'+
                        '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;" >'+ data.data1.vendor_name+'</td>'+
                        '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data.data1.category+'</td>'+
                        '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data.data1.email+'</td>'+
                        '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data.data1.phone+'</td>'+

                        '</tr>';


                    $("#vendor_table").append(html2);

                    $.each(data.data, function (key,value) {

                        $('.'+key).text(value);

                    });


                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
        });
    }






    function fileFormatter(cellValue, options, rowObject){
        var upload_url = window.location.origin+"/";
        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;
            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + 'company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + 'company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + 'company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + 'company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }
            return imageData;
        }
    }





});
