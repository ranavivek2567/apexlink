jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
});

// function to validate first password
$.validator.addMethod("pwcheck", function(value, element, error) {
    if (error) {
        $.validator.messages.pwcheck = '';
        return false;
    }
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(value)
});
$.validator.addMethod("password_regex", function(value, element) {
    var password_regex = this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9])/.test(value);
    return password_regex;
}, "* Atleast one aphanumeric and a numeric character required");

$( "#change_pass_form" ).validate({
    rules: {
        password:{
            required:true,
            password_regex : true
        },
        newpassword:{
            required:true,
            minlength: 8,
            password_regex : true
        },
        confirmpassword:{
            required:true,
            minlength: 8,
            password_regex : true,
            equalTo :'#newpassword2'
        }
    },
    messages: {
        newpassword: {
            minlength: "* Minimum 8 characters allowed",
        },
        confirmpassword: {
            equalTo: "* Fields do not match",
            minlength: "* Minimum 8 characters allowed",
        },

    }
});


/* change password validation trigger */

$(document).on("click",".changePassBtn", function(e){
    e.preventDefault();
    var current_password = $("#current_password").val();
    var password = $("#newpassword2").val();
    var cpassword = $("input[name='confirmpassword']").val();

    var validation = $("#change_pass_form").valid();
    // console.log("current_password>>>>>>"+current_password);
    if (validation !== false) {
        changeAdminPassword(current_password, password, cpassword);
    }
    return false;
});

function changeAdminPassword(current_password, password, cpassword) {
    $.ajax({
        type: 'post',
        url: '/Vendor-portal-ajax',
        data: {
            class: "vendorPortal",
            action: "changePasswordVendor",
            current_password: current_password,
            nPassword: password,
            cPassword: cpassword,
            id: vendor_id,
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success("Password changed successfully");
                $("#current_password").val('');
                $("#newpassword2").val('');
                $("input[name='confirmpassword']").val('');
            } else if (response.status == 'error' && response.code == 503) {
                toastr.error(response.message);
            }else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}




