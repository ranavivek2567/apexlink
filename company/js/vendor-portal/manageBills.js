$(document).ready(function () {

    jqGrid('All');
});



function jqGrid(status) {
    var table = 'company_bills';
    var columns = ['Vendor Name', 'Vendor_id', 'Due Date', 'Reference Number', 'Original Amount('+amount_symbol+')','Discounted Amount('+amount_symbol+')','Amount','Amount Due After Discount('+amount_symbol+')','Generated Date','Status','Action'];
    var select_column = [];
    var joins = [{table: 'company_bills', column: 'vendor_id', primary: 'id', on_table: 'users'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'vendor_id', value: vendor_portal_id, condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {name: 'Vendor Name', index: 'name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: 'users'},
        {name: 'Vendor_id', index: 'vendor_id',hidden:true, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Due Date', index: 'due_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,change_type:'date'},
        {name: 'Reference Number', index: 'refrence_number', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:referenceFormatter},
        {name: 'Original Amount', index: 'amount', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:amountDiv},
        {name: 'Discounted Amount', index: 'memo', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:discountDiv},
        {name: 'Amount', index: 'amount', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Amount Due After Discount', index: 'amount', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:afterDiscountDiv},
        {name: 'Generated Date', index: 'bill_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,change_type:'date'},
        {name: 'Status', index: 'status', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,formatter:statusFormater},
        {name: 'Action', index: 'select', width: 200, title: false, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table, formatter: actionFmatter}

    ];
    var ignore_array = [];
    jQuery("#paybills_table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'company_bills.updated_at',
        sortorder: 'desc',
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Pay Bills",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 400, drag: true, resize: false} // search options
    );
}
function afterDiscountDiv(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return amount_symbol+cellValue;
    }
}
function creditCardNumber(cellValue, options, rowObject){
    return '';
}
function statusFormater(cellValue, options, rowObject){
    if(rowObject !== undefined){
        if(cellValue == '1'){
            return 'paid';
        }
        return 'Due';
    }
}
function discountDiv(cellValue, options, rowObject){
    if(rowObject !== undefined){
        var html = '<span class="span_discount cursor">'+amount_symbol+'0.00</span><input style="display: none;" class="amount number_only input_discount form-control" id="input_'+rowObject.id+'" name="discount[]" value="">';
        return html;
    }
}
function amountDiv(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return '<span data_input="'+cellValue+'">'+amount_symbol+cellValue+'</span>';
    }
}
function referenceFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return '<a target="_blank" style="text-decoration: underline;color:#551A8B !important;" href="/Vendor/EditBill?id='+rowObject.id+'">'+cellValue+'</a>';
    }
}
function actionFmatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        var data = '';

             data  += '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option><option value="view">VIEW DETAILS</option>';



        return data;
    }

}


jQuery(document).on('change', '#paybills_table .select_options', function () {
    var select_options = $(this).val();
    var data_id = $(this).attr('data_id');
    if(select_options == 'view'){
        window.location.href = '/VendorPortal/viewBill?id='+data_id;
        }else if(select_options == 'print'){
        window.location.href = '/MasterData/AddPropertyType';
    }

});
$(document).ready(function () {

    split_bills();
    function split_bills() {
        var table = 'company_bills';
        var columns = ['Property Name', 'Units', 'Property Amount('+amount_symbol+')', 'Description'];
        var select_column = [];
        var joins = [{table: 'company_bills', column: 'property_id', primary: 'id', on_table: 'general_property'},{table: 'general_property', column: 'id', primary: 'property_id', on_table: 'unit_details'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [{column: 'vendor_id', value: vendor_portal_id, condition: '='}];
        var extra_columns = [];
        var columns_options = [
            {name: 'Property Name', index: 'property_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: 'general_property'},
            {name: 'Units', index: 'unit_no', width: 200, align: "center", searchoptions: {sopt: conditions}, table: 'unit_details'},
            {name: 'Property amount', index: 'property_price', width: 200, align: "center", searchoptions: {sopt: conditions}, table: 'general_property'},
            {name: 'Description', index: 'description', width: 200, align: "center", searchoptions: {sopt: conditions}, table: 'general_property'},

        ];
        var ignore_array = [];
        jQuery("#splitbills-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'company_bills.updated_at',
            sortorder: 'desc',
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Pay Bills",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

    viewData();
    function viewData() {
        $.ajax({
            type: 'post',
            url: '/VendorPortal/NewBillAjax',
            data: {
                class: "ViewVendor",
                action: "getVendorDetail"
            },
            success: function (response) {
                var res = $.parseJSON(response);
                if (res.status == "success") {
                    $.each(res.data,function (key,value) {
                       $("."+key).text(value);
                    })
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});
