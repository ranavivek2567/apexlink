$('#apexklink_newT').on('click',function () {
    $.ajax
    ({
        type: 'post',
        url: '/ApexlinkNewAjax',
        data: {
            class: "UserAjax",
            action: "whatsNewList",
            // data :window.location.pathname
        },
        success: function (response) {
            var whatsnewdata = $.parseJSON(response);

            if (whatsnewdata.status == "success")
            {
                $('#myModalApexNew').modal('show');
                html = '';
                $(whatsnewdata.data).each(function (key,whats) {
                    html += '<div class="new-apex-inner">\n' +
                        '   <h5><b>'+whats.title+'</b></h5>\n' +
                        '   <p>'+whats.description+'</p>\n' +
                        '   <div class="new-apex-link">\n' +
                        '      <a class="ancrFeedback" href="javascript:;">Submit Feedback</a>\n' +
                        '   </div>\n' +
                        '   <div class="div-new-feedback" style="display: none;">\n' +
                        '<form class="apexlink_new" name="apexlink_new" method="post">  <label>Feedback</label>\n' +
                        '      <span>\n' +
                        '      <input type="hidden" id="list_id" name="list_id" value="'+whats.id+'">'+
                        '      <input type="hidden" id="id_hidden" name="user_id" value="'+whats.user_id+'">'+
                        '      <textarea name="feedback_message" class="clsCapitaliseChr"></textarea><label id="feedback_message_err" class="error"></label>\n' +
                        '      </span>\n' +
                        '      <div class="btn-outer">\n' +
                        '         <input value="Submit" class="btn-blue btnSubmitFeedback"  type="button">\n' +
                        '         <input class="btn-black btnCancelFeedback" value="Cancel" type="button">\n' +
                        '      </div></form>\n' +
                        '   </div>\n' +
                        '</div>\n';
                });

                $('.new-apex-data').html(html);
            } else
            {
                toastr.error(data.message);
            }
        },
        error: function (whatsnewdata) {
            var errors = $.parseJSON(whatsnewdata.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });


});

$(document).on('click','.ancrFeedback',function(){
    $(this).parent().next().toggle();
});

$(document).on('click','.btnCancelFeedback',function(){
    $(this).parents('.div-new-feedback').hide();
});

$(document).on('click','.btnSubmitFeedback',function(e){
    e.preventDefault();
    var apex_data = $(this).parents('.apexlink_new').serializeArray();
    var apex_elem = $(this).parents('.apexlink_new');
    var feedback_message =$(this).parent().prev().find("textarea[name='feedback_message']").val();
    var userId = $("#id_hidden").val();
    var list_id = $(this).parent().prev().find("#list_id").val();
    //alert(list_id);
    $.ajax
    ({
        type: 'post',
        url: '/ApexlinkNewAjax',
        data: {
            class: "UserAjax",
            //action: "sendMail",
            action: "apexnewfeedback",
            data :apex_data,
            id: userId,
            feedback_message:feedback_message,
            list_id:list_id
        },
        success: function (response) {
            //console.log("++++++++++++");
            var data = $.parseJSON(response);
            if (data.status == "error")
            {
                //console.log("fsdfsdfs");
                toastr.error(data.message);
                $.each(data, function (key, value) {
                    apex_elem.find('#feedback_message_err').text('Please fill the required value.');
                });
            } else
            {
                //console.log("34525324");
                toastr.success('Mail Sent Successfully');
                $('#myModalApexNew').modal('hide');
            }
        },
        error: function (data) {
            //console.log("------------");
            var errors = $.parseJSON(data.responseText);
            debugger
            $.each(errors, function (key, value) {
                // alert(key+value);
                debugger
                console.log($(this).parents('.apexlink_new'));
                apex_elem.find('#' + key + '_err').text(value);
            });
        }
    });
});

$(document.body).click( function(e) {
    var container = $(".help_class");
    var container1 = $(".notify_class");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        $('#help-submenu').hide();
    }

    // if (!container1.is(e.target) && container1.has(e.target).length === 0)
    // {
    //     $('.arrow_box').hide();
    // }

});

$(document.body).click( function(e) {
    var container = $(".admin_help_class");
    var container1 = $(".admin_notify_class");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        $('#admin-submenu').hide();
    }

    // if (!container1.is(e.target) && container1.has(e.target).length === 0)
    // {
    //     $('.arrow_box').hide();
    // }

});