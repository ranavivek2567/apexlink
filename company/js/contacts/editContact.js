/*get id from url*/
var id = getParameterByName('id');
/**
 *change function to perform various actions(edit ,delete)
 * @param status
 */
$(document).on('change','#buildingFileLibrary-table .select_options',function () {
    var select_options = $(this).val();
    var data_id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if(select_options == 'Email')
    {
        file_upload_email('users','email',data_id,id)
    }else if(select_options == 'Delete') {
        $('#buildingFileLibrary-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        var row_num = $(this).parent().parent().parent().index() ;
        jQuery('#buildingFileLibrary-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#buildingFileLibrary-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        var data_id = $(this).attr("data_id");
        bootbox.confirm({
            message: "Are you sure you want to delete this file ?",
            buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/Employee/deleteLibraryFiles',
                        data: {class: 'employeeEdit', action: 'deleteLibraryFiles', id: data_id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                    $("#buildingFileLibrary-table").trigger('reloadGrid');
                }

            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});

$("#people_top").addClass("active");


$(document).on('click','.contact_cancel',function(){
    bootbox.confirm("Do you want to cancel this action now ?", function (result) {
        if (result == true) {
            window.location.href = window.location.origin+'/People/ContactListt';
        }
    });
});




/*save employe id in hidden field */
$("#employee_edit_id").val(id);

// $(document).on('change','.additional_phoneType ',function(){
//     var phone_type = $(this).val();
//     if(phone_type == '2' || phone_type == '5')
//     {
//         $('#work_phone_extension-error').text('');
//         $(this).closest('div').next().show();
//         $(this).closest('div').next().find('#work_phone_extension').removeAttr("disabled");
//     } else {
//         $(this).closest('div').next().hide();
//         $(this).closest('div').next().find('#work_phone_extension').prop('disabled', true);
//     }
// });

$(document).on('change','.additional_phoneType',function(){
    var phone_type = $(this).val();

    if(phone_type == '2' || phone_type == '5')
    {
        $('#work_phone_extension-error').text('');
        console.log( $(this).closest('div'));
        $(this).parent().next('div').next('div').show();
    } else {
        $(this).parent().next('div').next('div').hide();
    }
});


/**
 * Get Parameters by id
 * @param status
 */
function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

/*get item from local storage to scroll to that div*/
if(localStorage.getItem("AccordionHref")) {
    var message = localStorage.getItem("AccordionHref");
    if(message == '#info' || message == '#vehicles' || message == '#emergency-contact' || message == '#credential_control' || message=='#notes'|| message =='#flag_bank' || message =='#custom' || message == '#filelibrary') {
        $('html, body').animate({
            'scrollTop': $(message).position().top
        });
    }
    localStorage.removeItem('AccordionHref');
}




/*show and hide radio button content*/
$(document).on("click",'.add_new_vehicle ',function(){
    $('.property_vehicle').show(500);
    $('#save_employee_vehicle').html('Save');
});

/*edit employee validation*/
$("#addEmployee").validate({
    rules: {
        firstname: {
            required:true
        },
        lastname: {
            required:true
        },
        parking_keys: {
            number:true
        },
        'phoneNumber[]': {
            required: true,
        },
        referralSource: {
            required:true
        },
        additional_firstname: {
            required:true
        }
    }
});

/*submit handler for edit employee*/

$("#addEmployee").on('submit',function(event){
    event.preventDefault();
    $(".customadditionalEmailValidation").each(function () {
        res = validations(this);
    });
    $(".customValidationCarrier").each(function () {
        res = validations(this);
    });

    $(".customPhonenumbervalidations").each(function () {
        res = validations(this);
    });

    $(".customOtherPhonevalidations").each(function () {
        res = validations(this);
    });
    if($("#addEmployee").valid()){
        var employee_image = $('.employee_image').html();
        var employeeImage = JSON.stringify(employee_image);
        var form = $('#addEmployee')[0];
        var formData = new FormData(form);
        formData.append('action','update');
        formData.append('class','EmployeeAjax');
        formData.append('employee_image',employeeImage);
        formData.append('employee_id',$("#employee_edit_id").val());
        action = 'insert';
        var custom_field = [];
        $(".custom_field_html input").each(function(){
            var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
            custom_field.push(data);
        });

        var length1 = $('#file_library_uploads > div').length;
        combine_photo_document_array =  file_library;
        var dataaa = Array.from(new Set(combine_photo_document_array)); // #=> ["foo", "bar"]

        if(length1 > 0 ) {
            var data1 = convertSerializeDatatoArray();
            var combine__data_photo_document_array =   data1;
            $.each(dataaa, function (key, value) {
                if(compareArray(value,combine__data_photo_document_array) == 'true'){
                    formData.append(key, value);
                }
            });
        }
        var a =  JSON.stringify(custom_field);
        formData.append('custom_field',a);
        $.ajax({
            url:'/Contact/edit',
            type: 'POST',
            data: formData,cache: false,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                var emailRes = true;
                var customRes = true;
                var carrierRes = true;
                var phoneRes = true;
                var otherPhone = true;
                // checking portfolio validations
                $(".customadditionalEmailValidation").each(function() {
                    emailRes = validations(this);

                });
                $(".customValidationCarrier").each(function() {
                    carrierRes = validations(this);

                });
                $(".customPhonenumbervalidations").each(function () {
                    phoneRes = validations(this);
                });
                // checking custom field validations
                $(".custom_field_html input").each(function () {
                    customRes = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                });

                $(".customOtherPhonevalidations").each(function () {
                    otherPhone = validations(this);
                });

                if (emailRes === false || customRes === false || carrierRes === false || phoneRes === false || otherPhone === false) {
                    xhr.abort();
                    return false;
                }
                console.log(otherPhone);
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success" && response.table === "all"){
                    localStorage.setItem("Message", "Contact Updated Successfully");
                    localStorage.setItem("contactrowcolor",true)
                    window.location.href = window.location.origin+'/People/ContactListt';
                }else{
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
});

// if(localStorage.getItem("AccordionHref")) {
//     var message = localStorage.getItem("AccordionHref");
//     if(message == '#noteshistory') {
//         $('html, body').animate({
//             'scrollTop': $(message).position().top
//         });
//     }
//     localStorage.removeItem('AccordionHref');
// }

//Email validation
$(document).on('keyup','.customadditionalEmailValidation',function(){
    validations(this);
});

//Carrier validation
$(document).on('change','.customValidationCarrier',function(){
    validations(this);
});

//phone number validation
$(document).on('keyup','.customPhonenumbervalidations',function(){
    validations(this);
});

$(document).on('keyup','.customOtherPhonevalidations',function(){
    validations(this);
});


$($(".clone-row").get(0)).removeClass("clone-row");
/*function to save new vehicle */
$(document).on('click','#save_employee_vehicle', function (e) {
    e.preventDefault();
    var formData = $('#new_vehicle :input').serializeArray();
    var user_id = id;
    var vehicle_id = $("#vehicle_id").val();

    var length =   $("#new_vehicle :input").filter(function () {
        return $.trim($(this).val()).length == 0
    }).length;
    if(length == 12){
        bootbox.alert({
            message: "Please enter data in atleast one field.",
        });
        return false;
    }

    $.ajax({
        type: 'post',
        url: '/Employee/saveNewVehicle',
        data: {form: formData,
            class: 'EmployeeEdit',
            action: 'addNewVehicle',
            employee_id:user_id,
            vehicle_id:vehicle_id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {

                $("#employee-veichle").trigger('reloadGrid');
                setTimeout(function(){
                    jQuery('#employee-veichle').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#employee-veichle').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }, 1000);

                toastr.success(response.message);
                $('.property_vehicle :input').val('');
                $('.property_vehicle').hide('')
            }   else if (response.status == 'error' && response.code == 500) {
                toastr.error(response.message);
                return false;
            }else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

/**
 * jqGrid Vehicles function
 * @param status
 */
vehicles();
function vehicles(status) {
    var table = 'tenant_vehicles';
    var columns = ['Name', 'Make', 'Plate','Colour','Year','Vin','Registration #','Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var extra_where = [{column: 'user_id', value: id, condition: '='}];
    var columns_options = [
        {name: 'Name',index: 'vehicle_name',width: 90,align: "center",searchoptions: {sopt: conditions},table: table},
        {name: 'Make', index: 'make', width: 100, searchoptions: {sopt: conditions}, table: table},
        {name: 'Plate', index: 'license', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Colour', index: 'color', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Year', index: 'year', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Vin', index: 'vin', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Registration', index: 'registration', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionVehicleListFmatter}
    ];
    var ignore_array = [];
    jQuery("#employee-veichle").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Vehicles",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: false, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 200, drag: true, resize: false} // search options
    );
}

/**
 * jqGrid function to format action column
 * @param status
 */

function actionVehicleListFmatter (cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        var editable = $(cellvalue).attr('editable');
        var select = '';
        select = ['Edit','Delete'];

        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

/* To check phone format */
jQuery('.phone_format').mask('000-000-0000', {reverse: true});

/*For Cloning vehicle  button content*/
$(document).on("click",".copyVehicle",function(){
    var clone = $(".property_vehicle:first").clone();
    clone.find('input[type=text]').val('');
    $(".property_vehicle").last().after(clone);
    $(".property_vehicle:not(:eq(0))  .removeVehicle").show();
    $(".property_vehicle:not(:eq(0))  .copyVehicle").remove();
    clone.find('.vehicle_date_purchased').each(function () {
        $(this).removeAttr('id').removeClass('hasDatepicker'); // added the removeClass part.
        $('.vehicle_date_purchased').datepicker({dateFormat: date_format,changeYear: true,
            yearRange: "-100:+20",
            changeMonth: true,});
    });
});

/*To remove vehicle*/
$(document).on("click",".removeVehicle",function(){
    var phoneRowLenght = $(".property_vehicle");
    $(this).parents(".property_vehicle").remove();
});

/*To change gender by salutation*/
$(document).on("change","#salutation",function(){
    var value = $(this).val();
    if(value == 1){
        $('#gender').prop('selectedIndex',0);
        $('.hidemaiden').hide();
    }
    if(value==2)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    }
    if(value==3)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==4)
    {
        $('#gender').prop('selectedIndex',0);
        $('.hidemaiden').hide();
    } if(value==5)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==6)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    } if(value==7)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    }
    if(value==8)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    } if(value==9)
    {
        $("#gender").val("2");
        $('.hidemaiden').show();
    }if(value==10)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    }if(value==11)
    {
        $("#gender").val("1");
        $('.hidemaiden').hide();
    }


});

function getsalutationData(value){

    if(value == 1)
    {
        $('.hidemaiden').hide();
    }
    if(value==2)
    {
        $('.hidemaiden').hide();
    }
    if(value==3)
    {
        $('.hidemaiden').show();
    }
    if(value==4)
    {
        $('.hidemaiden').hide();
    }
    if(value==5)
    {
        $('.hidemaiden').show();
    }
    if(value==6)
    {
        $('.hidemaiden').hide();
    }
    if(value==7)
    {
        $('.hidemaiden').show();
    }
    if(value==8)
    {
        $('.hidemaiden').show();
    }
    if(value==9)
    {
        $('.hidemaiden').show();
    }
}

/*for multiple SSN textbox*/
$(document).on("click",".ssn-plus-sign",function(){
    var clone = $(".multipleSsn:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".multipleSsn").last().after(clone);
    $(".multipleSsn:not(:eq(0))  .ssn-plus-sign").remove();
    $(".multipleSsn:not(:eq(0))  .ssn-remove-sign").show();

});
/* for multiple SSN textbox*/

/*remove ssn textbox*/
$(document).on("click",".ssn-remove-sign",function(){
    $(this).parents(".multipleSsn").remove();
});
/*remove ssn textbox*/

/*To clone additional ssn*/
$(document).on("click",".additional_ssn-plus-sign",function(){
    var clone = $(".additional_multipleSsn:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".additional_multipleSsn").first().after(clone);

    clone.find(".additional_ssn-plus-sign").remove();
    $(".additional_multipleSsn:not(:eq(0))  .additional_ssn-remove-sign").show();

});
/*To remove additional ssn*/
$(document).on("click",".additional_ssn-remove-sign",function(){
    $(this).parents(".additional_multipleSsn").remove();
});
/*To show ethnicity add popup*/
$(document).on("click",'#additional_ethnicity',function(){
    $("#additional_ethnicityPopUp").show();
});
/*To hide ethnicity add popup*/
$(document).on("click",'#additional_ethncity_cancel',function(){
    $("#additional_ethnicityPopUp").hide();
    $("#additional_addThncity").val('');
});
/*Datepicker for dob*/
$("#date_of_birth").datepicker({dateFormat: date_format,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true,
    maxDate: new Date(),
}).datepicker("setDate", dob);
/*Get zipcode/address set from admin*/
$(document).on('focusout','#zip_code',function(){
    getAddressInfoByZip($(this).val());
});

/*Get zipcode/address set from admin*/
$("#zipcode").keydown(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        getAddressInfoByZip($(this).val());
    }
});

/*Set timeout function to show added values in fields*/
setTimeout(function(){
    if(referral_source !='') {
        $("#additional_referralSource [value=" + referral_source + "]").attr("selected", "true");
    }
    if(ethnicity !='') {
        $("select[name=ethncity] [value=" + ethnicity + "]").attr("selected", "true");
    }
    if(maritial_status !='') {
        $("select[name=maritalStatus] [value=" + maritial_status + "]").attr("selected", "true");
    }
    if(hobbies !='') {
        $.each(hobbies, function (key, value) {
            $("input[type='checkbox'][value='" + value + "']").prop('checked', true);
        });

        $(".multiselect-selected-text").html(hobbies.length+' selected')
        $.each(hobbies, function(i,e){
            $("#edit_hobbies option[value='" + e + "']").prop("selected", true);
        });
    }
    if(veteran_status !='') {
        $("select[name=veteranStatus] [value=" + veteran_status + "]").attr("selected", "true");
    }
}, 2000);


/**
 *Get location on the basis of zipcode
 */
function getAddressInfoByZip(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function(){
                        response(addr);
                    }, 2000);

                } else {
                    response({success:false});
                }
            } else {
                response({success:false});
            }
        });
    } else {
        response({success:false});
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response(obj){
    if(obj.success){
        $('#country').val(obj.country);
        $('#citys').val(obj.city);
        $('#states').val(obj.state);

    } else {
        $('#citys').val('');
        $('#states').val('');
        $('#country').val('');
    }
}

/*To show marital status popup*/
$(document).on("click",'#additional_maritalStatus',function(){
    $("#additional_maritalStatusPopUp").show();

});
/*To hide marital status popup*/
$(document).on("click",'#additional_maritalStatus_cancel',function(){
    $("#additional_maritalStatusPopUp").hide();
    $("#additional_addMaritalStatus").val('');
});

/*To show add  hobbies popup*/
$(document).on("click",'#additional_hobbies',function(){
    $("#additional_hobbiesPopUp").show();
});
/*To hide add  hobbies popup*/
$(document).on("click",'#additional_hobbies_cancel',function(){
    $("#additional_hobbiesPopUp").hide();
    $("#additional_addNewHobbies").val('');
});

/*To show add  veteran status popup*/
$(document).on("click",'#additional_veteranStatus',function(){
    $("#additional_veteranStatusPopUp").show();
});
/*To hide add  veteran status popup*/
$(document).on("click",'#additional_veteranStatus_cancel',function(){
    $("#additional_veteranStatusPopUp").hide();
    $("#additional_addNewVeteranStatus").val('');
});

/*To Clone additional phone row*/
$(document).on("click",".additional-phonerow-plus-sign",function(){
    var clone = $(".additional_phone-row:first").clone();
    clone.find('input[type=text]').val('');
    $(".additional_phone-row").first().after(clone);
    $(".additional_phone-row:not(:eq(0))  .fa-plus-circle").hide();
    $(".additional_phone-row:not(:eq(0))  .additional-phonerow-remove-sign .fa-times-circle").show();
    var phoneRowLenght = $("#addTenant .additional_phone-row");
    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
    if (phoneRowLenght.length == 2) {
        clone.find(".fa-plus-circle").hide();
    }else if (phoneRowLenght.length == 3) {
        clone.find(".fa-plus-circle").hide();
        $(".additional_phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".additional_phone-row:not(:eq(0)) .fa-plus-circle").show();
    }
});

/*To remove additional phone row*/
$(document).on("click",".additional-phonerow-remove-sign",function(){
    var phoneRowLenght = $(".additional_phone-row");
    if (phoneRowLenght.length == 2) {
        $(".additional_phone-row:eq(0) .fa-plus-circle").show();
    }else if (phoneRowLenght.length == 3) {
        $(".additional_phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".additional_phone-row:not(:eq(0)) .fa-plus-circle").show();
    }
    $(this).parents(".additional_phone-row").remove();

});

$(document).on("click",".add-employee-notes",function(){
    var clone = $(".employee_notes:first").clone();
    clone.find('textarea').val('');
    $(".employee_notes").last().after(clone);
    $(".employee_notes:not(:eq(0))  .fa-plus-circle").hide();
    $(".employee_notes:not(:eq(0))  .remove-employee-notes").show();
    var employeeNotesLenght = $("#addEmployee .employee_notes");
    if (employeeNotesLenght.length == 2) {
        clone.find(".fa-plus-circle").hide();
    }else if (employeeNotesLenght.length == 3) {
        clone.find(".fa-plus-circle").hide();
        $(".employee_notes:eq(0) .fa-plus-circle").hide();
    }else{
        $(".employee_notes:not(:eq(0)) .fa-plus-circle").show();
    }
});

/*To remove employe notes*/
$(document).on("click",".remove-employee-notes",function(){
    var employeeNotesLenght = $(".employee_notes");
    if (employeeNotesLenght.length == 2) {
        $(".employee_notes:eq(0) .fa-plus-circle").show();
    }else if (employeeNotesLenght.length == 3) {
        $(".employee_notes:eq(0) .fa-plus-circle").show();
    }else{
        $(".employee_notes:not(:eq(0)) .fa-plus-circle").hide();
    }
    $(this).parents(".employee_notes").remove();

});

/*To Clone additional email*/
$(document).on("click",".additional_email-plus-sign",function(){
    var emailRowLenght = $(".additional_multipleEmail");

    if (emailRowLenght.length == 2) {
        $(".additional_email-plus-sign").hide();
    }else{
        $(".additional_email-plus-sign").show();
    }
    var clone = $(".additional_multipleEmail:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".additional_multipleEmail").last().after(clone);
    $(".additional_multipleEmail:not(:eq(0))  .additional_email-plus-sign").remove();
    $(".additional_multipleEmail:not(:eq(0))  .additional_email-remove-sign").show();

});
/*To Remove additional phone row*/
$(document).on("click",".additional_email-remove-sign",function(){
    var emailRowLenght = $(".additional_multipleEmail");

    if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
        $(".additional_email-plus-sign").show();
    }else{
        $(".additional_email-plus-sign").hide();
    }
    $(this).parents(".additional_multipleEmail").remove();
});

/*phone row form1 remove*/
$(document).on("click",".guarantor-phonerow-form1-remove-sign",function(){

    $(this).parents(".guarantor-form1-phone-row").remove();

});

$(document).on("click",".multipleEmail-form1 .email-form1-plus-sign .fa-plus-circle",function(){
    var emailRowLenght = $(".multipleEmail-form1");
    if (emailRowLenght.length == 3) {
        $(".email-form1-plus-sign").hide();
    }
    var clone = $(".multipleEmail-form1:first").clone();
    clone.find('input[type=text]').val('');
    $(".multipleEmail-form1").first().after(clone);
    $(".multipleEmail-form1:not(:eq(0))  .email-form1-plus-sign .fa-plus-circle").remove();
    $(".multipleEmail-form1:not(:eq(0))  .email-form1-remove-sign .fa-times-circle").show();

});

$(document).on("click",".email-form1-remove-sign",function(){
    var emailRowLenght = $(".multipleEmail-form1");
    if (emailRowLenght.length == 3) {
        $(".email-form1-plus-sign").show();
    }
    $(this).parents(".multipleEmail-form1").remove();
});

$(document).on("click",".email-form2-plus-sign",function(){

    var clone = $(".multipleEmail-form2:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(this).parents(".multipleEmail-form2").after(clone);
    clone.find(".email-form2-plus-sign").hide();
    clone.find(".email-form2-remove-sign").show();
});

$(document).on("click",".email-form2-remove-sign",function(){
    $(this).parents(".multipleEmail-form2").remove();
});

$(document).on("click",".closeimagepopupicon",function () {
    $(".popup-bg").hide();
    $(this).parent().hide();
});

$(document).on("change",".cropit-image-input",function(){
    photo_videos = [];
    var fileData = this.files;
    var type= fileData.type;
    var elem = $(this);
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var uploadType = 'image';
        var validSize = '1030';
        var arrayType = validImageTypes;
        if ($.inArray(type, arrayType) < 0) {
            toastr.warning('Please select file with valid extension only!');
        } else {
            if (parseInt(size) > validSize) {
                var validMb =  1;
                toastr.warning('Please select documents less than '+validMb+' mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                photo_videos.push(value);
                var src = '';
                var reader = new FileReader();
                $('#photo_video_uploads').html('');
                $(".popup-bg").show();
                elem.next().show();
            }
        }
    });

});

$(document).on("click",".primary-tenant-phone-row .fa-plus-circle",function(){
    var clone = $(".primary-tenant-phone-row:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    clone.addClass('clone-row')
    $(".primary-tenant-phone-row").first().after(clone);
    clone.find('.work_extension_div').hide();
    clone.find('.additional_phoneType ').val('1');
    $(".primary-tenant-phone-row:not(:eq(0))  .fa-plus-circle").hide();
    $(".primary-tenant-phone-row:not(:eq(0))  .fa-times-circle").show();
    var phoneRowLenght = $("#addEmployee .primary-tenant-phone-row");
    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
    if (phoneRowLenght.length == 2) {
        clone.find(".fa-plus-circle").hide();
    }else if (phoneRowLenght.length == 3) {
        clone.find(".fa-plus-circle").hide();
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
    }
});

$(document).on("click",".primary-tenant-phone-row .fa-times-circle",function(){
    var phoneRowLenght = $(".primary-tenant-phone-row");
    if (phoneRowLenght.length == 2) {
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").show();
    }else if (phoneRowLenght.length == 3) {
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").show();
        // $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
    }

    $(this).parents(".primary-tenant-phone-row").remove();
});

/*notice period section copy*/
$(document).on("click",".add-notice-period",function(){
   // alert(credential_notice_period);
    var clone = $(".tenant-credentials-control:first").clone();
    clone.find('input[type=text]').val('');
    clone.find('select[name="credentialType[]"]').val('1');
    clone.find('select[name="noticePeriod[]"]').val(credential_notice_period);
    $(".tenant-credentials-control").last().after(clone);
    var length = $(".tenant-credentials-control").length;

    clone.find('#acquireDateid')
        .removeClass('hasDatepicker')
        .removeData('datepicker')
        .removeAttr('id')
        .attr('id','acquireDateid_'+length)
        .datepicker({yearRange: '+0:+100', changeMonth: true, changeYear: true, dateFormat: date_format})
        .datepicker("setDate", new Date());
    clone.find('#expirationDateid')
        .removeClass('hasDatepicker')
        .removeData('datepicker')
        .removeAttr('id')
        .attr('id', 'expirationDateid_' + length)
        .datepicker({yearRange: '+0:+100', changeMonth: true, changeYear: true, dateFormat: date_format})
        .datepicker("setDate", new Date());

    clone.find(".add-notice-period").hide();
    clone.find(".remove-notice-period").show();
});

/*Acquire date format*/

$(".acquireDateclass").datepicker({dateFormat: date_format,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true,
});

/*To add expire date datepicker*/
$(".expirationDateclass").datepicker({dateFormat: date_format,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true});
/*Acquire date format*/
$(document).on("change", ".acquireDateclass", function () {
    var credentialControlLength = $("#credential_control .tenant-credentials-control");
    var classes = $(this).attr('class');
    var dynclass= $(this).attr('id');
    var dynmid = $("#"+dynclass).closest('div').next().find('.expirationDateclass').attr('id');
    var valuedatepicker=$(this).val();
    $("#"+dynmid).datepicker({dateFormat: date_format,
        minDate: valuedatepicker,
    });
});

$(document).on("change", ".expirationDateclass", function () {

    var dynclass = $(this).attr('id');
    var dynmid = $("#" + dynclass).closest('div').prev().find('.acquireDateclass').attr('id');
    var valuedatepicker = $(this).val();
    $("#"+dynmid).datepicker({dateFormat: date_format,
        maxDate: valuedatepicker,
    });
});
$(document).on("change", ".expirationDateclass", function () {
    var phoneRowLenght = $("#credential_control .row tenant-credentials-control");
    var dynclass = $(this).attr('id');
    var dynmid = $("#" + dynclass).closest('div').prev().find('.acquireDateclass').attr('id');
    var valuedatepicker = $(this).val();
    $("#"+dynmid).datepicker({dateFormat: date_format,
        maxDate: valuedatepicker,
    });
});


$(document).on("click",".remove-notice-period",function(){
    $(this).parents(".tenant-credentials-control").remove();
});

/*
date picker for veichle purchase date
 */
$('.vehicle_date_purchased').datepicker({dateFormat: date_format,
    changeYear: true,
    yearRange: "-100:+20",
    changeMonth: true,
});

/*To convert image size*/
function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}


$(document).ready(function(){
    // var cropper = $('.image-editor').cropit({
    //     imageState: {
    //         src: subdomain_url+'500/400',
    //     },
    // });
    // // Handle rotation
    // document.getElementById('rotate-ccw').addEventListener('click', function() {
    //     $('.image-editor').cropit('rotateCCW')
    // });
    //
    // document.getElementById('rotate-cw').addEventListener('click', function() {
    //     $('.image-editor').cropit('rotateCW')
    // });
    //
    //
    // $(document).on("click",'.export',function(){
    //     $(this).parents('.cropItData').hide();
    //     $(this).parent().prev().val('');
    //     var dataVal = $(this).attr("data-val");
    //     var imageData = $(this).parents('.image-editor').cropit('export');
    //     var image = new Image();
    //     image.src = imageData;
    //     $(".popup-bg").hide();
    //     $(this).parent().parent().parent().prev().find('div').html(image);
    // });

    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });

    /* Getting Building Property And Unit*/

    $(document).on("change","#building",function (e) {
        e.preventDefault();
        getUnitsByBuildingsID($("#property").val() , $(this).val());
        return false;
    });

    $(document).on("change","#property",function (e) {
        e.preventDefault();
        getBuildingsByPropertyID($(this).val());
        return false;
    });


});

/*To clone emergency contact*/

$(document).on("click",".add-emergency-contant",function(){

    var clone = $(".tenant-emergency-contact:first").clone();
    clone.find('input[type=text]').val('');
    clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});;
    $(".tenant-emergency-contact").first().after(clone);
    clone.find(".add-emergency-contant").hide();
    clone.find(".remove-emergency-contant").show();

});

/*To remove emergency contact*/
$(document).on("click",".remove-emergency-contant",function(){
    $(this).parents(".tenant-emergency-contact").remove();
});

$(document).on("click",".additional-add-emergency-contant",function(){

    var clone = $(".additional-tenant-emergency-contact:first").clone();
    clone.find('input[type=text]').val('');
    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
    $(".additional-tenant-emergency-contact").last().after(clone);
    clone.find(".additional-add-emergency-contant").hide();
    clone.find(".additional-remove-emergency-contant").show();

});

$(document).on("click",".additional-remove-emergency-contant",function(){
    $(this).parents(".additional-tenant-emergency-contact").remove();
});

/*To reset form */
$(document).on("click",".employee_reset",function(){
    bootbox.confirm({
        message: "Do you want to Reset ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.reload();
            }
        }
    });

});

/*To Cancel employee vehicle*/
$(document).on("click",".employee_vehicle_cancel",function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('.property_vehicle').hide(500);
                $('.property_vehicle :input').val('');
            }
        }
    });
});

/**
 *change function to perform various actions(edit ,activate,deactivate)
 * @param status
 */
jQuery(document).on('change','#employee-veichle .select_options',function () {
    $('#employee-veichle').find('.green_row_left, .green_row_right').each(function(){
        $(this).removeClass("green_row_left green_row_right");
    });
    var row_num = $(this).parent().parent().index() ;
    jQuery('#employee-veichle').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
    jQuery('#employee-veichle').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
    var select_options = $(this).val();
    var data_id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if(select_options == 'Edit')

    {

        //validator.resetForm();
        $('#employee-veichle').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('#employee-veichle').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#employee-veichle').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        $.ajax({
            type: 'post',
            url: '/Employee/getVehicleDetail',
            data: {
                class: 'EmployeeEdit',
                action: 'getVehicleDetail',
                vehicle_id:data_id
            },
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    $('.property_vehicle').show(500);

                    $("#vehicle_id").val(data_id)
                    $("#vehicle_name").val(response.data.data.vehicle_name);
                    $("#vehicle_number").val(response.data.data.vehicle_number);
                    $("#vehicle_type").val(response.data.data.type);
                    $("#vehicle_make").val(response.data.data.make);
                    $("#vehicle_model").val(response.data.data.model);
                    $("#vehicle_vin").val(response.data.data.vin);
                    $("#vehicle_registration").val(response.data.data.registration);
                    $("#vehicle_plate_number").val(response.data.data.license);
                    $("#vehicle_color").val(response.data.data.color);
                    if(response.data.data.year != '') {
                        $("#vehicle_year[value=" + response.data.data.year + "]").attr("selected", "true");
                    }
                    if(response.data.data.date_purchased != '') {
                        $('.vehicle_date_purchased').val(response.data.data.date_purchased);
                    }
                    $("#save_employee_vehicle").html('Update');
                } else if(response.status == 'error' && response.code == 503) {
                    toastr.error(response.message);
                }else {
                    toastr.warning('Record not updated due to technical issue.');
                }
            }
        });

    }else if(select_options == 'Delete') {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/Employee/deleteVehicleDetail',
                        data: {
                            class: 'EmployeeEdit',
                            vehicle_id:data_id
                        },
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $("#employee-veichle").trigger('reloadGrid');
                            } else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});

$("#save_file_library").click(function(){
    $("#file_library_images").submit(); // Submit the form
});

/*To clone emergency contact*/
$("#file_library_images").on('submit',function(event){
    event.preventDefault();
    if($("#file_library_images").valid()){
        var form = $('#file_library_images')[0];
        var formData = new FormData(form);
        formData.append('action','addLibraryFiles');
        formData.append('class','employeeEdit');
        formData.append('employee_id',$("#employee_edit_id").val());



        var length1 = $('#file_library_uploads > div').length;
        if(length1==0) return false;
        combine_photo_document_array =  file_library;
        var dataaa = Array.from(new Set(combine_photo_document_array)); // #=> ["foo", "bar"]

        if(length1 > 0 ) {
            var data1 = convertSerializeDatatoArray();
            var combine__data_photo_document_array =   data1;
            $.each(dataaa, function (key, value) {
                if(compareArray(value,combine__data_photo_document_array) == 'true'){
                    formData.append(key, value);
                }
            });
        }
        $.ajax({
            url:'/Employee/addLibraryFiles',
            type: 'POST',
            data: formData,cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success" ){
                    $("#file_library_uploads").html('');
                    toastr.success(response.message);
                    $("#buildingFileLibrary-table").trigger('reloadGrid');
                }else{
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });

    }
});

/**
 * jqGrid Intialization File Library function
 * @param status
 */
jqGridFileLibrary('All');
function jqGridFileLibrary(status) {
    var employee_id = $("#employee_edit_id").val();
    var table = ' tenant_chargefiles';
    var columns = ['Name','Preview','Location','File extension','Action'];
    var select_column = ['Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'file_type',value:'2',condition:'='},{column:'user_id',value:employee_id,condition:'='},{column: 'type', value: 'N', condition: '='}];
    var columns_options = [
        {name:'Name',index:'filename',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_extension',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        {name:'Location',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
        {name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionLibraryFmatter,title:false}
    ];
    var ignore_array = [];
    jQuery("#buildingFileLibrary-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Contact Files",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * jqGrid function to format action column
 * @param status
 */

function actionLibraryFmatter (cellvalue, options, rowObject){
    // if(rowObject !== undefined) {
    //
    //     var select = '';
    //     select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
    //     var data = '';
    //     if(select != '') {
    //         $.each(select, function (key, val) {
    //             data += val
    //         });
    //     }
    //     return data;
    // }

    if(rowObject !== undefined) {
        var editable = $(cellvalue).attr('editable');
        var select = '';
        var file_type =  rowObject.View;
        var location = rowObject.Location;
        var path = upload_url+'company/'+location;

        var imageData = '';
        var src = '';
        if(file_type == '1'){

            imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
        } else {
            if (rowObject.File_extension == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'docx' || rowObject.File_extension == 'doc') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }else if (rowObject.File_extension == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }
        }

        select = ['Email','Delete'];
        var data = '';

        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-path="'+path+'" data-src="'+src+'"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}
/*Image formatter for file library*/
function imageFormatter(cellvalue, options, rowObject){


    if(rowObject !== undefined) {

        var path = upload_url+'company/'+rowObject.Location;
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=40 height=40 src="' + src + '">';
    }
}

/*Open file location in next window*/
$(document).on('click',".open_file_location",function(){
    var location =    $(this).attr("data-location");
    window.open(location, '_blank');
});

/*ajax to delete file libraries*/
$(document).on("click",".delete_file_library",function(){

    $('#buildingFileLibrary-table').find('.green_row_left, .green_row_right').each(function(){
        $(this).removeClass("green_row_left green_row_right");
    });
    var row_num = $(this).parent().parent().parent().index() ;
    jQuery('#buildingFileLibrary-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
    jQuery('#buildingFileLibrary-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
    var data_id = $(this).attr("data_id");
    bootbox.confirm({
        message: "Are you sure you want to delete this file ?",
        buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    type: 'post',
                    url: '/Employee/deleteLibraryFiles',
                    data: {class: 'employeeEdit', action: 'deleteLibraryFiles', id: data_id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            toastr.success(response.message);
                            // window.location.href = '/MasterData/AddPropertyType';
                        }else if(response.status == 'error' && response.code == 503) {
                            toastr.error(response.message);
                            // window.location.href = '/MasterData/AddPropertyType';
                        }else {
                            toastr.warning('Record not updated due to technical issue.');
                        }
                    }
                });
                $("#buildingFileLibrary-table").trigger('reloadGrid');
            }

        }
    });
});


$(document).on("click",".addToRecepent",function(){
    $('#torecepents').modal('show');
});


$(document).on("change",".selectUsers",function(){
    var type = $(this).val();
    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            $(".userDetails").html(response);

        }
    });
});

$(document).on("click","#SendselectToUsers",function(){
    var check_data = [];
    $('.getEmails:checked').each(function () {
        $('.to').tagsinput('add', $(this).attr('data-email'));
        $('.to').tagsinput('add', $(this).attr('data-email'));
    });
    $('#torecepents').modal('hide');
    // $('.getEmails').prop('checked', false);
});

$(document).on("click",".addCcRecepent",function(){
    $('#ccrecepents').modal('show');
});

$(document).on("change",".selectCcUsers",function(){
    var type = $(this).val();
    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getCCUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            $(".userCcDetails").html(response);
        }
    });



});

$(document).on("click",".addBccRecepent",function(){
    $('#bccrecepents').modal('show');

});

$(document).on("change",".selectBccUsers",function(){
    var type = $(this).val();
    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getBCCUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            $(".userBccDetails").html(response);
        }
    });
});


$(document).on("click",".getBCCEmails",function(){
    if(this.checked)
    {
        var email = $(this).attr('data-email');
        $('.bcc').tagsinput('add', email);
    }
    else
    {
        var email = $(this).attr('data-email');
        $('.bcc').tagsinput('remove', email);
    }


});

$(document).on("click","#SendselectCcUsers",function(){
    var check_data = [];
    $('.getCCEmails:checked').each(function () {
        $('.cc').tagsinput('add', $(this).attr('data-email'));
        $('.cc').tagsinput('add', $(this).attr('data-email'));
    });
    $('#ccrecepents').modal('hide');
});

$(document).on("click","#SendselectBccUsers",function(){
    var check_data = [];
    $('.getBCCEmails:checked').each(function () {
        $('.bcc').tagsinput('add', $(this).attr('data-email'));
        $('.bcc').tagsinput('add', $(this).attr('data-email'));
    });
    $('#bccrecepents').modal('hide');
});

/*Function to go back*/
function goBack() {
    window.location.href='/People/ContactListt';
    //window.history.back();
}



function getBuildingsByPropertyID(propertyID){

    $.ajax({
        type: 'post',
        url: '/ContactAjax',
        data: {
            class: "contactPopup",
            action: "getContactBuildingDetail",
            propertyID: propertyID
        },
        success: function (response) {
            var data = $.parseJSON(response);
            $('#building').html("");
            if (data.status == "success") {
                if (data.data.length > 0){
                    var buildingOption = "<option value='0'>Undecided</option>";
                    $.each(data.data, function (key, value) {
                        buildingOption += "<option value='"+value.id+"' data-id='"+value.building_id+"'>"+value.building_name+"</option>";
                    });

                    $('#building').html(buildingOption);
                }else{
                    var buildingOption = "<option value='0'>Select</option>";
                    $('#building').html(buildingOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


function getUnitsByBuildingsID(propertyID, buildingID){
    $.ajax({
        type: 'post',
        url: '/ContactAjax',
        data: {
            class: "contactPopup",
            action: "getContactUnitDetail",
            propertyID: propertyID,
            buildingID: buildingID
        },
        success: function (response) {
            var data = $.parseJSON(response);
            $('#unit').html("");
            if (data.status == "success") {
                if (data.data.length > 0){
                    var unitOption = "<option value=''>Undecided</option>";
                    $.each(data.data, function (key, value) {
                        var uniName = value.unit_prefix+"-"+value.unit_no;
                        unitOption += "<option value='"+value.id+"' data-id='"+value.unit_prefix+"'>"+uniName+"</option>";
                    });
                    $('#unit').html(unitOption);
                }else{
                    var unitOption = "<option value=''>Select</option>";
                    $('#unit').html(unitOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}


