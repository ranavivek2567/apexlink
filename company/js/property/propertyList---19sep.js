$(document).ready(function () {

    //jqGrid status
    $('#jqGridStatus').on('change', function () {
        var selected = this.value;
        var deleted_at = true;
        $('#property-table').jqGrid('GridUnload');
        // if (selected == 4)
        //     deleted_at = false;
        jqGrid(selected);
    });

    $(document).on('change', '#property-table .select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        //New code start
        switch (opt) {
            case "Edit":
                window.location.href = "/Property/EditProperty?id=" + id;
                break;
            case "Active Tenants":
                window.location.href = "/Tenantlisting/Tenantlisting";
                break;
            case "Active Guests":
                window.location.href = "/GuestCard/ListGuestCard";
                break;
            case "Book Now":
                window.location.href = "/GuestCard/ListGuestCard";
                break;
            case "Tenant Living":
                window.location.href = "/Tenantlisting/Tenantlisting";
                break;
            case "Add New Tenant":
                window.location.href = "/Tenantlisting/add";
                break;
            case "Work Order":
                window.location.href = "/WorkOrder/WorkOrders";
                break;
            case "Application":
                window.location.href = "/RentalApplication/RentalApplication";
                break;
            case "Vacancy":
                window.location.href = "/Unit/UnitModule?id=" + id;
                break;
            case "Unit Directory":
                window.location.href = "/Unit/UnitModule?id=" + id;
                break;
            case "Inspection":
                localStorage.setItem("inspection_value", "add_inspection");
                window.location.href = "/Property/PropertyInspection?id=" + id;
                break;
            case "General Ledger":
               // window.location.href = "/Property/GeneralLedger?id=" + id;
                break;
            case "File Library":
                localStorage.setItem("scrollDown",'#propertyFileLibrary');
                window.location.href = "/Property/PropertyView?id=" + id;
                break;
            case "Add Inventory":
                localStorage.setItem("AccordionHref",'#collapseTwenty12');
                window.location.href = "/Property/EditProperty?id=" + id;
                break;
            case "Waiting List":
                localStorage.setItem("scrollDown",'#propertyWatingList');
                window.location.href = "/Property/PropertyView?id=" + id;
                break;
            case "FlagBank":
                localStorage.setItem("scrollDown",'#propertyFlag');
                window.location.href = "/Property/PropertyView?id=" + id;
                break;
            case "InTouch":
                window.location.href = "/Communication/InTouch";
                break;
            case "Notes & History":
                localStorage.setItem("scrollDown",'#notesHistory');
                window.location.href = "/Property/PropertyView?id=" + id;
                break;
            case "InTouchHistory":
                window.location.href = "/Communication/InTouch";
                break;
            case "Archive Property":
                bootbox.confirm({
                    message: "Do you want to Archive the current record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/property-ajax',
                                data: {class: 'propertyDetail', action: "archiveProperty", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    triggerReload();
                                }
                            });
                        }
                    }
                });
                break;
            case "Resign This Property":
                bootbox.confirm({
                    message: "Do you want to Resign the current record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/property-ajax',
                                data: {class: 'propertyDetail', action: "resignProperty", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    triggerReload();
                                }
                            });
                        }
                    }
                });
                break;
            case "Print Envelope":
                $.ajax({
                    type: 'post',
                    url: '/property-ajax',
                    data: {class: 'propertyDetail', action: 'getCompanyData','building_id':id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            $("#PrintEnvelope").modal('show');
                            $("#company_name").text(response.data.data.company_name)
                            $("#address1").text(response.data.data.address1)
                            $("#address2").text(response.data.data.address2)
                            $("#address3").text(response.data.data.address3)
                            $("#address4").text(response.data.data.address4)
                            $(".city").text(response.data.data.city)
                            $(".state").text(response.data.data.state)
                            $(".postal_code").text(response.data.data.zipcode)
                            $("#building_name").text(response.building.data.building_name)
                            $("#building_address").text(response.building.data.address)
                        }else {
                            toastr.warning('Record not updated due to technical issue.');
                        }
                    }
                });
                break;
            case "Activate This Property":
                bootbox.confirm({
                    message: "Do you want to Activate the current record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/property-ajax',
                                data: {class: 'propertyDetail', action: "activateProperty", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    triggerReload();
                                }
                            });
                        }
                    }
                });
                break;
            case "Delete This Property":
                bootbox.confirm({
                    message: "Do you want to delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            bootbox.confirm({
                                message: "Deleting this entry will take it out of the system completely and no longer be recovered.Do you want to continue to delete this entry?",
                                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                                callback: function (result) {
                                    if (result == true) {
                                        $.ajax({
                                            type: 'post',
                                            url: '/property-ajax',
                                            data: {class: 'propertyDetail', action: "deleteProperty", id: id},
                                            success: function (response) {
                                                var response = JSON.parse(response);
                                                if (response.status == 'success' && response.code == 200) {
                                                    toastr.success(response.message);
                                                } else {
                                                    toastr.error(response.message);
                                                }
                                                triggerReload();
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
                break;
            default:
                $('#property-table .select_options').prop('selectedIndex',0);
        }
        //New code end
    });


    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('1');
    function jqGrid(status) {
        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'general_property.update_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'general_property.property_name';
        }
        var table = 'general_property';
        var columns = ['Property Name','clone_status','Property Address', 'Buildings', 'Units', 'Type', 'Vacant', 'Property Manager', 'Status', 'Owner', 'Short-Term Rental', 'Action'];
        var select_column = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
        var joins = [{table: 'general_property', column: 'property_type', primary: 'id', on_table: 'company_property_type'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['general_property.deleted_at', 'general_property.address1', 'general_property.address2', 'general_property.address3', 'general_property.address4', 'general_property.manager_id', 'general_property.owner_id'];
        var extra_where = [];
        var columns_options = [
            {name: 'Property Name', title:false, index: 'property_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'clone_status', title:false,hidden:true ,index: 'clone_status', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Property Address', index: 'address_list', width: 100, searchoptions: {sopt: conditions}, table: table, change_type: 'combine_column_line', extra_columns: ['address1', 'address2', 'address3', 'address4'], update_column: 'address_list',original_index: 'address1', classes: 'pointer'},
            {name: 'Buildings', index: 'building_exist', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'Units', index: 'unit_exist', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyUnitBuilding, classes: 'pointer'},
            {name: 'Type', index: 'property_type', width: 80, align: "center", searchoptions: {sopt: conditions}, table: 'company_property_type', classes: 'pointer'},
            {name: 'Vacant', index: 'status', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Property Manager', index: 'manager_list', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'serialize', secondTable: 'users', index2: 'name', update_column: 'manager_list', original_index: 'manager_id', classes: 'pointer'},
            {name: 'Status', index: 'status', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyStatus, classes: 'pointer'},
            {name: 'Owner', index: 'owner_list', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'serialize', secondTable: 'users', index2: 'name', update_column: 'owner_list', original_index: 'owner_id', classes: 'pointer',attr:[{name:'flag',value:'property'}]},
            {name: 'Short-Term Rental', index: 'is_short_term_rental', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyShortTermRental, classes: 'pointer'},
            {name: 'Action', index: 'select', title: false, width: 80, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table,formatter:actionFmatter}
        ];
        var ignore_array = [];
        jQuery("#property-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Properties",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 1)
            return "Active";
        else if (cellValue == '0')
            return "InActive";
        else
            return '';
    }

    function flagFormatter(cellValue, options, rowObject){
        if(rowObject !== undefined){
            console.log(rowObject.Action);
            var flagValue = $(rowObject.Action).attr('flag');
            var flag = '';
            if(flagValue == 'yes'){

            } else {

            }
        }
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Yes";
        else if (cellValue == '0')
            return "No";
        else
            return '';
    }

    function actionFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Vacancy', 'Unit Directory', 'Inspection','Notes & History', 'File Library', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            //   if(rowObject.Status == '1')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            //  if(rowObject.Status == '2')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            if(rowObject.Status == '3')  select = ['Activate This Property', 'Print Envelope','Delete This Property'];
            //   if(rowObject.Status == '4')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            //  if(rowObject.Status == '5')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            if(rowObject.Status == '6')  select = ['Print Envelope','Delete This Property'];
            if(rowObject.Vacant == '1')  select = ['Edit', 'Active Tenants','Add New Tenant','Work Order','Application','Vacancy', 'Unit Directory', 'Inspection','General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            if(rowObject.is_short_term_rental == '1')  select = ['Edit','Active Tenants','Add New Tenant','Work Order','Application','Vacancy', 'Unit Directory', 'Inspection','General Ledger','Notes & History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }

    }

    alphabeticSearch();
    function alphabeticSearch() {
        $.ajax({
            type: 'post',
            url: '/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'general_property',
                column: 'property_name'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.code == 200) {
                    var html = '';

                    $.each(response.data, function (key, val) {
                        var color = '#05A0E4'
                        if (val == 0)
                            color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:' + color + '" data_id="' + val + '">' + key + '</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });
    }

    $(document).on('click', '#AZ', function () {
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click', '#allAlphabet', function () {
        var grid = $("#property-table");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData, {filters: ""});
        grid.trigger("reloadGrid", [{page: 1, current: true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });

    $(document).on('click', '.getAlphabet', function () {
        var grid = $("#property-table"), f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if (value != '0') {
            f.push({field: "property_name", op: "bw", data: search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });


    /**
     * Function for view company on clicking row
     */
    $(document).on('click', '#property-table tr td', function () {
        var id = $(this).closest('tr').attr('id');

        if ($(this).index() == 0 || $(this).index() == 11) {
            return false;
        } else if($(this).index() == 3) {
            var index =$(this).index();
            var td = $(this).closest('tr').find('td:eq('+index+')').html();
            td = $(td).text();
            if(td != '0') {
                window.location.href = '/Building/BuildingModule?id=' + id
            }
        } else if($(this).index() == 4){
            var index =$(this).index();
            var td = $(this).closest('tr').find('td:eq('+index+')').html();
            td = $(td).text();
            if(td != '0') {
                window.location.href = '/Unit/UnitModule?id=' + id;
            }
        } else {
            window.location.href = '/Property/PropertyView?id=' + id;
        }
    });

    /**
     * download sample function
     */
    var base_url = window.location.origin;
    $(document).on("click", '#export_sample_property_button', function() {
        window.location.href = base_url + "/propertyView-ajax?status=" + "&&action=exportSampleExcel";
    });

    /**
     * export function
     */
    $(document).on("click", '#export_property_button', function() {
        var status = $("#jqGridStatus option:selected").val();
        var table = 'general_property';
        window.location.href = base_url + "/propertyView-ajax?status=" + status + "&&table=" + table + "&&action=exportExcel";
    });

    /**
     * Import function
     */
    $(document).on('click', '#import_property_button', function(e) {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $("#ImportProperty").show(500);
        return false;
    });

    /** Import unit type excel  */
    $("#importPropertyForm").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function () {
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'propertyView');
            formData.append('action', 'importExcel');
            $.ajax
            ({
                type: 'post',
                url: '/propertyView-ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);
                        $("#import_unit_type_div").hide(500);
                        triggerReload();
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    $(document).on("click", "#import_property_cancel", function(e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {
                confirm: {
                    label: 'Yes'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function(result) {
                if (result == true) {
                    $("#ImportProperty").hide(500);
                }
            }
        });
    });

    /**
     * function to format short term rental
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyShortTermRental(cellValue, options, rowObject) {
        if (cellValue == 0)
            return "No";
        else if (cellValue == 1)
            return "Yes";
    }

    /**
     * function to format property status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyStatus(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Active";
        else if (cellValue == '2')
            return "Active";
        else if (cellValue == '3')
            return "Archive";
        else if (cellValue == '4')
            return "Active";
        else if (cellValue == '5')
            return "Active";
        else if (cellValue == '6')
            return "Resign";
        else if (cellValue == '7')
            return "Active";
        else if (cellValue == '8')
            return "Active";
        else
            return '';
    }

    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var clone = rowObject.clone_status;
            var flag = '';
            if (flagValue == 'yes') {
                if(clone == 1){
                    var cellValue = cellValue.replace("_copy", "");
                    return '<span> <span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span><span style="color:red;">_copy</span><a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
                } else {
                    return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#propertyWatingList" href="javascript:void(0);" data_url="/Property/PropertyView?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
                }
            } else {
                if(clone == 1){
                    var cellValue = cellValue.replace("_copy", "");
                    return '<span><span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span><span style="color:red;">_copy</span></span>';
                } else {
                    return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
                }

            }
        }

    }

    /**
     * function to format property unit and building
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyUnitBuilding(cellValue, options, rowObject) {
        if(cellValue == ''){
            return '<span data_redirect="no" style="text-decoration: underline;">0</span>';
        } else {
            return '<span data_redirect="yes" style="text-decoration: underline;">' + cellValue + '</span>';
        }
    }

    /**
     * function to trigger Reload
     */
    function triggerReload() {
        var grid = $("#property-table");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData, {filters: ""});
        grid.trigger("reloadGrid", [{page: 1, current: true}]);
    }

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var base_url = window.location.origin;
        var mywindow = window.open('', 'my div');
        $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
            'font-weight:bold;} .right-detail{\n' +
            '        position:relative;\n' +
            '        left:+400px;\n' +
            '    }</style>');
        $(mywindow.document.body).html( '<body>' + data + '</body>');
        mywindow.document.close();
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        if(mywindow.close()){

        }
        $("#PrintEnvelope").modal('hide');
        return true;
    }

    $(document).on('click','.classFlagRedirect',function(e){
        event.preventDefault();
        localStorage.setItem("scrollDown",$(this).attr('data_href'));
        var url = $(this).attr('data_url');
        window.location.href = url;
    });
});