$(document).ready(function () {
    $("#sendEmail").validate({
        rules: {
            to: {
                required: true
            },
            cc: {
                required: true
            },
            body: {
                required: true
            },
        },
        submitHandler: function (e) {
            var tenant_id = $(".tenant_id").val();
            var form = $('#sendEmail')[0];
            var formData = new FormData(form);
            var path = $(".attachments").attr('href');
            /*  alert(path);*/
            var to = $(".to").val();
            formData.append('to_users', to);
            formData.append('action', 'sendFileLibraryattachEmail');
            formData.append('class', 'TenantAjax');
            formData.append('path', path);
            $.ajax({
                url: '/Tenantlisting/getInitialData',
                type: 'POST',
                data: formData,
                success: function (data) {
                    info = JSON.parse(data);
                    if (info.status == "success") {
                        toastr.success("Email has been sent successfully");
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });

    $("#sendEmailModal").validate({
        rules: {
            to: {
                required: true
            },
            cc: {
                required: true
            },
            body: {
                required: true
            },
        }
    });

    $(document).on('click','.compose-email-btn',function(e){
        e.preventDefault();
        if($("#sendEmailModal").validate()){
            var tenant_id = $(".tenant_id").val();
            var form = $('#sendEmailModal')[0];
            var formData = new FormData(form);
            var path = $(".attachments").attr('href');
            /*  alert(path);*/
            var to = $(".to").val();
            formData.append('to_users', to);
            formData.append('action', 'sendFileLibraryattachEmail');
            formData.append('class', 'TenantAjax');
            formData.append('path', path);
            $.ajax({
                url: '/Tenantlisting/getInitialData',
                type: 'POST',
                data: formData,
                success: function (data) {
                    info = JSON.parse(data);
                    if (info.status == "success") {
                        toastr.success("Email has been sent successfully");
                        $('#sendMailModal').modal('hide');
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });



    $(document).on("click",".addToRecepent",function(){
        $('#torecepents').modal('show');
    });
    $(document).on("click",".addCcRecepent",function(){
        $('#ccrecepents').modal('show');
    });
    $(document).on("click",".addBccRecepent",function(){
        $('#bccrecepents').modal('show');

    });
    $(document).on("change",".selectCcUsers",function(){
        var type = $(this).val();
        $.ajax({
            url:'/EditOwnerAjax',
            type: 'POST',
            data: {
                "type": type,
                "action": 'getCCUsers',
                "class": 'EditOwnerAjax'
            },
            success: function (response) {
                $(".userCcDetails").html(response);
            }
        });



    });
    $(document).on("click","#SendselectCcUsers",function(){
        var check_data = [];
        $('.getCCEmails:checked').each(function () {
            $('.cc').tagsinput('add', $(this).attr('data-email'));
        });
        $('#ccrecepents').modal('hide');
    });
    $(document).on("click","#SendselectBccUsers",function(){
        var check_data = [];
        $('.getBCCEmails:checked').each(function () {
            $('.bcc').tagsinput('add', $(this).attr('data-email'));
        });
        $('#bccrecepents').modal('hide');
    });
    $(document).on("change",".selectBccUsers",function(){
        var type = $(this).val();
        $.ajax({
            url:'/EditOwnerAjax',
            type: 'POST',
            data: {
                "type": type,
                "action": 'getBCCUsers',
                "class": 'EditOwnerAjax'
            },
            success: function (response) {
                $(".userBccDetails").html(response);
            }
        });
    });
    $(document).on("change",".selectUsers",function(){
        var type = $(this).val();
        $.ajax({
            url:'/EditOwnerAjax',
            type: 'POST',
            data: {
                "type": type,
                "action": 'getUsers',
                "class": 'EditOwnerAjax'
            },
            success: function (response) {
                $(".userDetails").html(response);

            }
        });



    });
    $(document).on("click","#SendselectToUsers",function(){
        var check_data = [];
        $('.getEmails:checked').each(function () {
            $('.to').tagsinput('add', $(this).attr('data-email'));
        });
        $('#torecepents').modal('hide');
        // $('.getEmails').prop('checked', false);
    });

    $('.summernote').summernote({
        addclass: {
            debug: false,
            classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
        },
        width: '100%',
        height: '300px',
        //margin-left: '15px',
        toolbar: [
            // [groupName, [list of button]]
            ['img', ['picture']],
            ['style', ['style', 'addclass', 'clear']],
            ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
            ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
            ['extra', ['video', 'table', 'height']],
            ['misc', ['undo', 'redo', 'codeview', 'help']]
        ]
    });


});

