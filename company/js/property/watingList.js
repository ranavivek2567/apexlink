//display add portfolio div
$(document).on('click','#wating_list',function () {
    $('#watingList_id').val('');
    $('#saveWatingList').text('Save');
    $('#watingListForm').trigger("reset");
    $('#entityCompanyName').hide();
    var validator = $( "#watingListForm" ).validate();
    validator.resetForm();
});

//on flag cancel
$(document).on('click','#cancelWatingList',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#watingList_id').val('');
                $('#watingListModel').modal('hide');
            }
        }
    });
});

$(document).on('change','#wating_use_entity_name',function(){
    if($('#wating_use_entity_name').prop('checked')){
        $('#entityCompanyName').show();
    } else {
        $('#entityCompanyName').hide();
    }
});



//adding custom fields
$('#watingListForm').on('submit',function(event) {
    event.preventDefault();
    //checking custom field validation
    if ($('#watingListForm').valid()) {
        var formData = $('#watingListForm').serializeArray();
        $.ajax({
            type: 'post',
            url: '/watingList-ajax',
            data: {form: formData,class:'WatingList',action:'createWatingList'},
            success: function (response) {
                var response = JSON.parse(response);
                if(response.code == 200){
                    $('#flagFormDiv').hide(500);
                    $('#propertyWatingList-table').trigger('reloadGrid');
                    toastr.success(response.message);
                    $('#watingListModel').modal('hide');
                    setTimeout(function () {
                        jQuery('#propertyWatingList-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#propertyWatingList-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 500);
                } else if(response.code == 200) {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});
jQuery('#wating_phone').mask('000-000-0000', {reverse: true});
selectBoxBuilding();
function selectBoxBuilding(){
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {id: $("#property_editunique_id").val(), class: 'CommonAjax', action: 'getBuildings'},
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.code == 200) {
                var select = '';
                $.each(data.data, function (key, value) {
                    select += '<option value="' + value.id + '">' + value.building_name + '</option>';
                });
                $('#wating_building_id').append(select);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


jqGridWatingList('All');
/**
 * jqGridFlag Initialization function
 * @param status
 */
function jqGridWatingList(status) {
    var table = 'property_wating_list';
    var columns = ['Lead Name','Date', 'Phone Number', 'Email ID', 'Preferred', 'Pets', 'Notes','Property Name','Building','Unit','Action'];
    var select_column = ['Edit','Delete','Convert to Contact','Convert to Tenant'];
    var joins = [{table:'property_wating_list',column:'building_id',primary:'id',on_table:'building_detail'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'property_id',value:$("#property_editunique_id").val(),condition:'='}];
    var extra_columns = ['property_wating_list.deleted_at'];
    var columns_options = [
        {name:'Lead Name',index:'first_name',align:"center",searchoptions: {sopt: conditions},table:table,change_type:'name'},
        {name:'Date',index:'created_at',searchoptions: {sopt: conditions},table:table},
        {name:'Phone Number',index:'phone',align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Email ID',index:'email', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preferred',index:'preferred',width:100, align:"center",searchoptions: {sopt: conditions},table:table,formatter:preferredFormater },
        {name:'Pets',index:'pets',width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:petFormater},
        {name:'Notes',index:'note',width:110, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Property Name',index:'property_name',width:110, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Building',index:'building_name',width:110, align:"center",searchoptions: {sopt: conditions},table:'building_detail'},
        {name:'Unit',index:'unit',width:110, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select',width:110, title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    var ignore_array = [];
    jQuery("#propertyWatingList-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'property_wating_list.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Wating List",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}

/**
 * function to change prefered format
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function preferredFormater (cellValue, options, rowObject){
    if (cellValue == 1)
        return "1stBR";
    else if(cellValue == 2)
        return "2ndBR";
    else if(cellValue == 3)
        return "3rdBR";
    else if(cellValue == 4)
        return "4thBR";
    else if(cellValue == 5)
        return "5thBR";
    else if(cellValue == 6)
        return "6thBR";
    else if(cellValue == 7)
        return "Commercial";
    else if(cellValue == 8)
        return "House";
    else if(cellValue == 9)
        return "Single/Family";
    else if(cellValue == 10)
        return "Multi Family";
    else if(cellValue == 11)
        return "Studio";
    else if(cellValue == 12)
        return "Office";
    else if(cellValue == 13)
        return "Other";
    else
        return '';
}

/**
 * function to change pet format
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function petFormater (cellValue, options, rowObject){
    if (cellValue == '1')
        return "Yes";
    else if(cellValue == '2')
        return "No";
    else if(cellValue == '3')
        return "Others";
    else
        return '';
}


/**
 * function to change action column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function actionFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject.Completed == 1) select = ['Edit','Delete'];
        if(rowObject.Completed == 0) select = ['Edit','Delete','Completed'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }

}

/**  List Action Functions  */
$(document).on('change', '#propertyWatingList-table .select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {
        var validator = $( "#flagForm" ).validate();
        validator.resetForm();
        $('#propertyWatingList-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('#propertyWatingList-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#propertyWatingList-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        $.ajax({
            type: 'post',
            url: '/watingList-ajax',
            data: {id: id,class:'WatingList',action:'getWatingList'},
            success: function (response) {
                $("#watingListModel").modal('show');
                $('#saveWatingList').text('Update');
                var data = $.parseJSON(response);
                if (data.code == 200) {

                    $("#watingList_id").val(data.data.id);
                    edit_date_time(data.data.updated_at);
                    $.each(data.data, function (key, value) {

                        if(key == 'use_entity_name'){
                            var checked = (value=='1')?true:false;
                            $('#wating_use_entity_name').prop('checked',checked);
                        } else {
                            $('#wating_' + key).val(value);
                        }
                    });
                } else if (data.code == 500){
                    toastr.error(data.message);
                } else{
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        //$("#portfolio-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/watingList-ajax',
                        data: {id: id,class:'WatingList',action:'deleteWatingList'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#propertyWatingList-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }
            }
        });
    } else {
        $('#propertyWatingList-table').trigger('reloadGrid');
    }
    $('#propertyWatingList-table .select_options').prop('selectedIndex',0);
});
