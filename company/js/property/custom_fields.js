var base_url = window.location.origin;
var login_user_id =  localStorage.getItem("login_user_id");
var status =  localStorage.getItem("active_inactive_status");

//custom field button
$(document).on('click','#add_custom_field',function(){
    custom_field_validator.resetForm();
    $('#default_value').prop('readonly',false);
    $('#default_value').datepicker('destroy');
});

//adding custom fields
$('#custom_field_submit').on('click',function(event) {
   event.preventDefault();
   var text = $('#custom_field_submit').text();
   var property_id = ($("#property_editunique_id").val() !== undefined)?$("#property_editunique_id").val():property_unique_id;
   var custom_field = [];
   $(".custom_field_html input").each(function(){
       var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
       custom_field.push(data);
   });

   $.ajax({
       type: 'post',
       url: '/property/custom_field',
       data: {class: 'propertyCustomfield', action: 'addCustomFields', form: custom_field,property_id: property_id},
       beforeSend: function(xhr) {
           // checking custom field validations
           $(".custom_field_html input").each(function() {
               var res = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
               if(res === false) {
                   xhr.abort();
                   return false;
               }
           });
       },
       success: function (response) {
           var response = JSON.parse(response);
           if(response.code == 200){
               if(text == 'Submit') {
                   bootbox.confirm({
                       message: "Do you want to add Buildings to this Property?",
                       buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                       callback: function (result) {
                           if (result == true) {
                               window.location.href = '/Building/AddBuilding?id=' + property_id;
                           } else {
                               localStorage.setItem('Message', 'Property Added Successfully.');
                               localStorage.setItem('rowcolor', 'rowColor');
                               window.location.href = '/Property/PropertyModules';
                           }
                       }
                   });
               } else {
                   localStorage.setItem('Message', 'Property Updated Successfully.');
                   localStorage.setItem('rowcolor', 'rowColor');
                   window.location.href = '/Property/PropertyModules';
               }
           } else {
               toastr.error('Error occured while updating!');
           }
       },
       error: function (data) {
           console.log(data);
       }
   });
});



