
function searchFilters(){
    var grid = $("#lostItem-table"),f = [];
	var start_date = $('#cash_start_date').val();
	var end_date = $('#cash_end_date').val();
	if(start_date != '' && end_date != ''){
		f.push({field: "transactions.created_at", op: "dateBetween", data: start_date, data2:end_date});
		grid[0].p.search = true;	
	} else {
		grid[0].p.search = false;
	}
    $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

//To get the URL Parameter
var getPropId = getParameters("id");
if(getPropId != "" || getPropId != undefined){
	jqGrid_GetLostItemList('all', getPropId);
}else{
	jqGrid_GetLostItemList('all');
}

function jqGrid_GetLostItemList(status, getPropId = "") {
	var sortby = 'accounting_ledger.updated_at';
	var table = 'accounting_ledger';
	var columns = ['Date','Period ','Property','Ref#','Debit Account','Credit Account','Transaction Description','Amount'];
	var select_column = ['Edit','Delete','Cancel'];
	 var joins = [
	 				{table:'accounting_ledger',column:'transaction_id',primary:'id',on_table:'transactions'},
	 				{table:'accounting_ledger',column:'module_id',primary:'id',on_table:'general_property'},
	 				{table:'transactions',column:'bank_id',primary:'id',on_table:'company_accounting_bank_account'},
	 			];
	var conditions = ["eq","bw","ew","cn","in"];

	if(getPropId!=""){
		// console.log("status="+getPropId);
		var extra_where = [{column:'property_id',value:getPropId,condition:'=',table:'transactions'}];
	} else{
		var extra_where = [];
	}
	var extra_columns = [];
	var columns_options = [
		// {name:'id',index:'id', width:90,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Date',index:'updated_at', width:200,align:"left",searchoptions: {sopt: conditions},table:table},
		{name:'Period',index:'created_at', width:250,align:"left",searchoptions: {sopt: conditions},table:'transactions'},
		{name:'Property',index:'property_name', width:180,align:"left",searchoptions: {sopt: conditions},table:'general_property'},
		{name:'Ref#',index:'reference_no', width:180,align:"left",searchoptions: {sopt: conditions},table:'transactions'},
		{name:'Debit Account',index:'bank_name', width:180,align:"left",searchoptions: {sopt: conditions},table:'company_accounting_bank_account'},
	    {name:'Credit Account',index:'bank_name', width:200,align:"left",searchoptions: {sopt: conditions},table:'company_accounting_bank_account'},
	    {name:'Transaction Description',index:'payment_response',align:"left", width:200,searchoptions: {sopt: conditions},table:'transactions'},
	    {name:'Amount',index:'amount', width:200,align:"left",searchoptions: {sopt: conditions},table:'transactions'}
	];
	var ignore_array = [];
	jQuery("#lostItem-table").jqGrid({
		url: '/List/jqgrid',
		datatype: "json",
		height: '100%',
		autowidth: true,
		colNames: columns,
		colModel: columns_options,
		pager: true,
		mtype: "POST",
		postData: {
			q: 1,
			class: 'jqGrid',
			action: "listing_ajax",
			table: table,
			select: select_column,
			columns_options: columns_options,
			status: status, 
			ignore:ignore_array,
			joins:joins,
			extra_columns:extra_columns,
			extra_where:extra_where,
			 deleted_at: 'true'
		},
		viewrecords: true,
		sortname: sortby,
		sortorder: "desc",
		sorttype:'date',
		sortIconsBeforeText: true,
		headertitles: true,
		rowNum: '8',
		rowList: [5, 10, 20, 30, 50, 100, 200],
		caption: "",
		pginput: true,
		pgbuttons: true,
		navOptions: {
			edit: false,
			add: false,
			del: false,
			search: true,
			filterable: true,
			refreshtext: "Refresh",
			reloadGridOptions: {fromServer: true}
		}
	}).jqGrid("navGrid",
		{
			edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
		},
		{}, // edit options
		{}, // add options
		{}, //del options
		{top:10,left:200,drag:true,resize:false} // search options
	);

}

function getParameters(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}