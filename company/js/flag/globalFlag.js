//add rule for select drop down
$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
}, "* This field is required.");

//add rule for number validation
$.validator.addMethod("checkNumber", function(value, element, arg){
    if(value == "") return true;
    var st = value.replace("-", "");
    if(isNaN(st.replace("-", ""))){
        return false;
    } else {
        return true;
    }
}, "Only number are allowed!");

//add property form client side validations
$("#flagForm").validate({
    rules: {
        '.textbox-text': {
            required:true
        },
        flag_name: {
            required:true
        },
        flag_by: {
            required:true
        }
    }
});
$(document).on('change','#flag_type',function(){
    searchFilters();
});

function searchFilters(){
    var grid = $("#flagBankTable"),f = [];
    var type = $('#flag_type option:selected').val()
    f.push({field: "flags.object_type", op: "eq", data: type});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData, {filters: JSON.stringify(f), status: 'all'});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}


$(document).on('click','#addFlag',function(){
    $('#flagSaveBtnId').text('Save');
    $("#clearFlagSaveBtnId").text('Clear');
    $('#building_radio').prop('disabled',true);
    $('#unit_radio').prop('disabled',true);
    if($('input[name="flag_type"]:checked').length > 0){
        showFlagDiv('input[name="flag_type"]:checked')
    } else {
        if($('#unit_radio:checked').length > 0) {
            $('#flagForm').trigger('reset');
            var flag_type = $('#property_radio').val();
            $('#dynamicLabel').html($('#property_radio').attr('data_label')+' <em class="red-star">*</em>');
            $('.dynamicInput').attr('id',flag_type);
            $('.dynamicInput').attr('name','');
            $('#object_type').val(flag_type);
            $('#add_flag_type_div').show();
            $('#flagTypeDivModal').modal('hide');
            initateComboGrid();
            getNameAndPhone();
            triggerDatePicker();
            $('.addFlagButton').hide();
            var html = '<div class="col-xs-12 col-sm-3 combo_div">';
            html += '<label id="dynamicLabelBuilding">Building Name <em class="red-star">*</em></label>';
            html += '<input class="form-control capital dynamicInput" name="" id="building_combo_input" placeholder="Building Name" maxlength="100" type="text" value="" style="text-transform: capitalize;" aria-required="true">';
            html += '</div>';
            html += '<div class="col-xs-12 col-sm-3 combo_div">';
            html += '<label id="dynamicLabelUnit">Unit Name <em class="red-star">*</em></label>';
            html += '<input class="form-control capital dynamicInput" name="object_name" id="unit_combo_input" placeholder="Unit Name" maxlength="100" type="text" value="" style="text-transform: capitalize;" aria-required="true">';
            html += '</div>';
            $('.dynamicDiv').html(html);
        } else if($('#building_radio:checked').length > 0){
            $('#flagForm').trigger('reset');
            var flag_type = $('#property_radio').val();
            $('#dynamicLabel').html($('#property_radio').attr('data_label')+' <em class="red-star">*</em>');
            $('.dynamicInput').attr('id',flag_type);
            $('.dynamicInput').attr('name','');
            $('#object_type').val(flag_type);
            $('#add_flag_type_div').show();
            $('#flagTypeDivModal').modal('hide');
            initateComboGrid();
            getNameAndPhone();
            triggerDatePicker();
            $('.addFlagButton').hide();
            var html = '<div class="col-xs-12 col-sm-3 combo_div">';
            html += '<label id="dynamicLabelBuilding">Building Name <em class="red-star">*</em></label>';
            html += '<input class="form-control capital dynamicInput" name="object_name" id="building_combo_input" placeholder="Building Name" maxlength="100" type="text" value="" style="text-transform: capitalize;" aria-required="true">';
            html += '</div>';
            $('.dynamicDiv').html(html);
        } else {
            showFlagDiv('#property_radio');
        }
    }
    $('#tenant_radio').prop('checked',true);
    $('#building_radio').prop('checked',false);
    $('#property_radio').prop('checked',false);
    $('#unit_radio').prop('checked',false);
});

$(document).on('click','#clearFlagSaveBtnId',function(){
    bootbox.confirm("Do you want to Clear this form?", function (result) {
        if (result == true) {
            var flag_id = $("#flag_id").val();
            if (flag_id != '') {
                getDataBYId(flag_id);
            } else {
                $('#flagForm').trigger('reset');
                $('#flag_flag_by').val(default_name);
                $('#flag_country_code12').val(220);
                $('#flag_phone_number').val(default_number);
                triggerDatePicker();
            }
        }
    });
});

function showFlagDiv(flagt){
    $('#dynamicDiv').html('');
    $('#flagForm').trigger('reset');
    var flag_type = $(flagt).val();
    $('#dynamicLabel').html($(flagt).attr('data_label')+' <em class="red-star">*</em>');
    $('.dynamicInput').attr('id',flag_type);
    $('#object_type').val(flag_type);
    $('#add_flag_type_div').show();
    $('#flagTypeDivModal').modal('hide');
    initateComboGrid();
    getNameAndPhone();
    triggerDatePicker();
    $('.addFlagButton').hide();
}


//display add portfolio div
// $(document).on('click','#new_flag',function () {
//     $('#flagFormDiv').show(500);
//     $('#flag_id').val('');
//     $('#flagSaveBtnId').text('Save');
//     $('#flagForm').trigger("reset");
//     var validator = $( "#flagForm" ).validate();
//     validator.resetForm();
// });

function triggerDatePicker() {
    $("#flag_flag_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
}

//on flag cancel
$(document).on('click','#flagCancel',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#flag_id').val('');
                $('#add_flag_type_div').hide();
                $('.addFlagButton').show();
            }
        }
    });
});

//adding custom fields
$('#flagForm').on('submit',function(event) {
    event.preventDefault();
    //checking custom field validation
    if ($('#flagForm').valid()) {
        var formData = $('#flagForm').serializeArray();
        var object_type = $('#object_type').val();
        var object_id = $('#object_id').val();
        $.ajax({
            type: 'post',
            url: '/flag-ajax',
            data: {form: formData,class:'Flag',action:'create_flag',object_id:object_id,object_type:object_type},
            success: function (response) {
                var response = JSON.parse(response);

                if(response.code == 200){
                    $('.addFlagButton').show();
                    $('#add_flag_type_div').hide(500);
                    getFlagCount();
                    $('#flagBankTable').trigger('reloadGrid');
                    toastr.success(response.message);
                } else if(response.code == 500) {
                    toastr.error(response.message);
                }
                
                setTimeout(function(){
                    jQuery('#flagBankTable').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#flagBankTable').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }, 1000);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});

jqGridFlag('All');
/**
 * jqGridFlag Initialization function
 * @param status
 */
function jqGridFlag(status) {
    var table = 'flags';
    var columns = ['Flagged For','Date','Phone Number','Flag Name', 'Flag Reason', 'Completed', 'Flag Type','object_id', 'Note', 'Action'];
    var select_column = ['Edit','Delete','Completed'];
    var joins = [];
    var joins = [
                    {table: 'flags', column: 'object_id', primary: 'id', on_table: 'users'},
                    {table: 'flags', column: 'object_id', primary: 'id', on_table: 'general_property'}
                ];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [];
    var extra_columns = ['flags.deleted_at', 'flags.updated_at'];
    var columns_options = [
        {name:'Flagged For',index:'object_name',align:"center",searchoptions: {sopt: conditions},table:table, change_type:'flagged_for_name'},
        {name:'Date',index:'date',align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Phone Number',index:'flag_phone_number',align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Flag Name',index:'flag_name',searchoptions: {sopt: conditions},table:table},
        {name:'Flag Reason',index:'flag_reason', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed',width:180, align:"center",searchoptions: {sopt: conditions},table:table,formatter: completedFormatter},
        {name:'Flag Type',index:'object_type', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'object_id',index:'object_id', hidden:true, width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Note',index:'flag_note',width:200, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select',width:200, title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select',search:false,table:table,formatter: actionFmatter}
    ];
    var ignore_array = [];
    
    jQuery("#flagBankTable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'flags.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * function to change completed format
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function completedFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "True";
    else if(cellValue == 0)
        return "False";
    else
        return '';
}

/**
 * function to change action column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function actionFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject.Completed == 1) select = ['Edit','Delete'];
        if(rowObject.Completed == 0) select = ['Edit','Delete','Completed'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }

}

function getDataBYId(id){
    $.ajax({
        type: 'post',
        url: '/flag-ajax',
        data: {id: id,class:'Flag',action:'get_flags'},
        success: function (response) {
            $("#add_flag_type_div").show(500);
            $('#flagSaveBtnId').text('Update');
            var data = $.parseJSON(response);
            if (data.code == 200) {
                edit_date_time(data.created_at);
                $("#flag_id").val(data.data.id);
                $("#clearFlagSaveBtnId").text('Reset');

                $('#flagForm [name="date"]').val(data.data.date);
                $('#flagForm [name="flag_name"]').val(data.data.flag_name);
                $('#flagForm [name="country_code"]').val(data.data.country_code);
                $('#flagForm [name="flag_phone_number"]').val(data.data.flag_phone_number);
                $('#flagForm [name="flag_reason"]').val(data.data.flag_reason);
                $('#flagForm [name="completed"]').val(data.data.completed);
                $('#flagForm [name="flag_by"]').val(data.data.flag_by);
                $('#flagForm [name="flag_note"]').val(data.data.flag_note);
                $('#object_type').val(data.data.object_type);
                $('#object_id').val(data.data.object_id);
                $('#dynamicLabel').html(getObjectlabel(data.data.object_type)+' <em class="red-star">*</em>');
                $('.dynamicInput').attr('id',data.data.object_type);
                initateComboGrid();
                $('.textbox-text').css('width','100%');
                getFlagCount();
                var elem = $('#dynamicLabel').parent().find('.textbox span').next('input').attr('id');
                console.log('elem', elem);
                if (elem != undefined) {
                    $('#' + elem).val('');
                    $('#' + elem).val(data.data.flagged_for_name.name);
                }
                // elem.combogrid('setValue',data.data.flagged_for_name.name);
            } else if (data.code == 500){
                toastr.error(data.message);
            } else{
                toastr.error(data.message);

            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
/**  List Action Functions  */
$(document).on('change', '#flagBankTable .select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {
        var validator = $( "#flagForm" ).validate();
        validator.resetForm();
        $('#flagBankTable').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('#flagBankTable').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#flagBankTable').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        getDataBYId(id);
        //$("#portfolio-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'deleteFlag'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                getFlagCount();
                                $('#flagBankTable').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    } else if (opt == 'Completed' || opt == 'COMPLETED') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to complete this flag ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'flagCompleted'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                getFlagCount();
                                $('#flagBankTable').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});

function getObjectlabel(name){
    var object_name = '';
    switch (name) {
        case "tenant":
            object_name = 'Tenant Name';
            break;
        case "owner":
            object_name = 'Owner Name';
            break;
        case "vendor":
            object_name = 'Vendor Name';
            break;
        case "contact":
            object_name = 'Contact Name';
            break;
        case "employee":
            object_name = 'Employee Name';
            break;
        default:
            object_name = 'Flagged Object';
    }
    return object_name;
}
$(document).on('change','#property_radio',function(){
    if($('#property_radio').length > 0){
        $('#building_radio').prop('disabled',false);
        $('input[name="flag_type"]').prop('checked',false);
    }
});
$(document).on('change','#building_radio',function(){
    if($('#building_radio').length > 0){
        $('#property_radio').prop('checked',true);
        $('#unit_radio').prop('disabled',false);
    }
});
$(document).on('change','#unit_radio',function(){
    if($('#unit_radio').length > 0){
        $('#property_radio').prop('checked',true);
        $('#building_radio').prop('checked',true);
        $('#unit_radio').prop('disabled',false);
    }
});

$(document).on('click','input[name="flag_type"]',function(){
    $('#property_radio').prop('checked',false);
    $('#building_radio').prop('checked',false);
    $('#building_radio').prop('disabled',true);
    $('#unit_radio').prop('checked',false);
    $('#unit_radio').prop('disabled',true);
});