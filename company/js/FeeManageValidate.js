//add property form client side validations
$("#AddLateFee").validate({
   rules: {
       apply_one_time:{
           required: true,
           number: true
       },
       grace_period: {
           number:true,
           required: true,
           max: 30

       },
       apply_daily: {
           required: true,
           number: true
       },
       apply_one_time_flat_fee: {
           required: true,
           maxAmount: 10
       },
       apply_daily_flat_fee: {
           required: true,
           maxAmount: 10
       }
   },
   messages: {
       grace_period: {
           max: "Maximum 30 days should allow"
       }
   }
});