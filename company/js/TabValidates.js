// maintenance information
$("#addMaintenanceinformationForm").validate({
    rules: {
        maintenance_speed_time: {
            required: true
        },
        amount: {
            required: true,
            number: true
        },
        Insurance_expiration: {
            required: true
        },
        warranty_expiration: {
            required: true
        },
        special_instructions: {
            required: true
        }
    }
});
// mortgage information
$("#addMortgageinformationForm").validate({
    rules: {
        mortgage_amount: {
            required: true,
            number: true
        },
        property_assessed_amount: {
            required: true,
            number: true
        },
        mortgage_start_date: {
            required: true
        },
        term: {
            required: true,
            number: true
        },
        interest_rate: {
            required: true,
        },
        financial_institution: {
            required: true
        }
    }
});
// property loan
$("#addPropertyloanForm").validate({
    rules: {
        loan_status: {
            required: true
        },
        vendor_name: {
            required: true
        },
        loan_amount: {
            required: true
        },
        loan_start_date: {
            required: true
        },
        loan_maturity_date: {
            required: true
        },
        monthly_due_date: {
            required: true
        },
        payment_amount: {
            required: true
        },
        interest_only: {
            required: true
        },
        loan_terms: {
            required: true
        },
        loan_type: {
            required: true
        },
        loan_rate: {
            required: true
        },
    }
});
// add notes form
$("#addPropertynotesForm").validate({
    rules: {
        notes: {
            required: true
        }
    }
});
// add property insurance form
$("#addPropertyInsurance").validate({
    rules: {
        'company_insurance_type[]': {
            valueNotEquals: 'default'
        },
        'insurance_company_name[]': {
            required: true
        },
        'policy[]': {
            required: true
        },
        'agent_name[]': {
            required: true
        },
        'insurance_country_code[]': {
            valueNotEquals: 'default'
        },
        'phone_number[]': {
            required: true
        },
        "property_insurance_email[]": {
            required: true,
            email: true
        },
        "coverage_amount[]": {
            required: true
        },
        "annual_premium[]": {
            required: true
        },
        "start_date[]": {
            required: true
        },
        "end_date[]": {
            required: true
        },
        "renewal_date[]": {
            required: true
        },
        "notes[]": {
            required: true
        }
    }
});
// owner preferred vendor form
$("#owneredPreferredForm").validate({
    rules: {
        vendor_name: {
            required: true
        },
        vendor_address: {
            required: true
        }
    }
});
// blacklist vendors 
$("#blacklistvendorForm").validate({
    rules: {
        vendor_name: {
            required: true
        },
        vendor_address: {
            required: true
        },
        blacklist_date: {
            required: true
        },
        blacklist_reason: {
            required: true
        }
    }
});

// bank details tab
$("#addBankdetailForm").validate({
    rules: {
        'account_name[]': {
            valueNotEquals: 'default'
        },
        'chart_of_account[]': {
            valueNotEquals: 'default'
        },
        'account_type[]': {
            valueNotEquals: 'default'
        },
        'bank_name[]': {
            required: true
        },
        'default_security_deposit[]': {
            valueNotEquals: 'default'
        },
        'bank_account_min_reserve_amount[]': {
            maxAmount: 10
        }
    }
});

$("#addManagementFeeForm").validate({
    rules: {
        management_type: {
            required: true
        },
        management_value: {
            required: true
        },
        minimum_management_fee: {
            required: true
        }
    }
});
$("#add_chart_account_form_id").validate({
    rules: {
        account_type_id: {
            required: true
        },
        account_code: {
            required: true,
            number: true,
        },
        account_name: {
            required: true
        },
        reporting_code: {
            required: true,
        }
    },
        messages: {
        account_code: {
            number: "* Please add numerical values only",
        }
    }
});
$("#KeycheckoutForm").validate({
    rules: {
        key_holder: {
            required: true
        },
        checkout_desc: {
            required: true,
        },
        checkout_keys: {            
            number: true,
            required: true
        }
    }
});       