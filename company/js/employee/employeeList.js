$(document).ready(function() {


    // $(document).on('mouseover','.tooltipgridclass',function(){
    //     $(this).closest('td').css("overflow", "unset");
    // });
    $(document).on('mouseover','.tooltipgridclass',function(){
            $(this).closest('td').css("overflow", "unset");
            // $(this).closest('td').find('span').removeClass('tooltiptextclass').addClass('tooltiptextbotclass');

    });
    $(document).on('mouseout','.tooltipgridclass',function(){
        $(this).closest('td').css("overflow", "hidden");
    });

    $("#people_top").addClass("active");

    /*  var base_url = window.location.origin;*/
    if(localStorage.getItem("rowcolorTenant"))
    {
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }

    $(document).on("change", "#people-employee #jqGridStatus", function (e) {
        var status = $(this).val();
        $('#employee-table').jqGrid('GridUnload');

        jqGrid(status,true);

    });
    $(document).on('click','#AZ',function(){
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click','#allAlphabet',function(){
        var grid = $("#employee-table");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });
    $(document).on('click','.getAlphabet',function(){
        var grid = $("#employee-table"),f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if(value != '0'){
            f.push({field:"name",op:"bw",data:search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    })
    alphabeticSearch();

    function alphabeticSearch(){
        $.ajax({
            type: 'post',
            url:'/Companies/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'users',
                column: 'name',
                where: [{column:'user_type',value:'8',condition:'=',table:'users'}]},
            success : function(response){
                var response = JSON.parse(response);
                if(response.code == 200){
                    var html = '';

                    $.each(response.data, function(key,val) {
                        var color = '#05A0E4';
                        if(val == 0) color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });
    }

    $(document).on('click','#employee-table tr td',function(e){
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        if ($(this).index() == 0 || $(this).index() == 6 ) {
          return false;
        }else{
            window.location.href = base_url + '/Employee/View?id='+id;
        }


        /* ('tr td:not(:last-child)')*/
    });

    $(document).on('click','.classFlagRedirect',function(e){
        event.preventDefault();
        var base_url = window.location.origin;
        localStorage.setItem("AccordionHref",'#flag_bank');
        var url = $(this).attr('data_url');
        window.location.href = url;

    });

    if(localStorage.getItem('ElasticSearch')){
        var elasticSearchData = localStorage.getItem('ElasticSearch');
        setTimeout(function(){
            var grid = $("#employee-table"),f = [];
            f.push({field: "users.id", op: "eq", data: elasticSearchData,int:'true'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('ElasticSearch');
        },1500);
    }


    /*  $('tr td:not(:last-child)');*/
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid(status, deleted_at) {
        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'users';
        var columns = ['Employee Name','Phone','Phone_Note', 'Email','Created Date','Status','Action'];
        var select_column = ['Edit','Email','Email History','TEXT','TEXT History','In-Touch History','Flag Bank','File Library','Notes & History','Add In-touch','Run Background Check','Archive Employee','Delete Employee','Print Envelope'];
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'employee_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'user_type',value:'8',condition:'=',table:'users'}];
        var columns_options = [

            {name:'Employee Name',index:'name',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter: employeeName,classes: 'pointer',attr:[{name:'flag',value:'employee'}]},
            // {name: 'Phone', index: 'id', width: 100,title:false, searchoptions: {sopt: conditions}, table: table, classes: 'pointer',change_type:'line_multiple',index2:'phone_number',join:{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'id',formatter:addToolTip,cellattr:cellAttri},
            {name: 'Phone', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor',change_type:'line_multiple',index2:'phone_number',index3:'other_work_phone_extension',join:{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'id',formatter:addToolTip,cellattr:cellAttri}, /**cellattr:cellAttrdata**/
            {name: 'Phone_Note', index: 'phone_number_note', classes: 'cursor', hidden:true, width: 80, searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
            {name:'Email',index:'email', width:80, align:"center",classes: 'cursor', searchoptions: {sopt: conditions},table:table},
            {name:'Created Date',index:'created_at', width:80, classes: 'cursor', align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80,align:"center", classes: 'cursor',searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];
        var ignore_array = [];
        jQuery("#employee-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Employees",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    /**
     * jqGrid function to format tooltip
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function addToolTip(cellValue, options, rowObject) {
        if(rowObject !== undefined){
            console.log(rowObject);
            if(rowObject.Phone_Note == ''){
                return cellValue;
            } else {
                console.log(rowObject.Phone_Note);
                return '<div class="tooltipgridclass">'+cellValue+'<span class="tooltiptextbotclass">'+rowObject.Phone_Note+'</span></div>';
            }
        }
    }


    /**
     *  function to format employee name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function employeeName(cellValue, options, rowObject) {
        // return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';

        if(rowObject !== undefined) {

            var flagValue = $(rowObject.Action).attr('flag');

            var id = $(rowObject.Action).attr('data_id');

            var flag = '';

            if (flagValue == 'yes') {

                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold; text-transform: capitalize;">' + cellValue + '<a id="flag" class="classFlagRedirect" data_url="/Employee/View?id='+id+'"  href="javascript:void(0);" data_url="/Employee/View?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';

            } else {

                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold; text-transform: capitalize;">' + cellValue + '</span>';

            }

        }
    }


    function nameFmatter(cellValue, options, rowObject){
        return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
    }



    function statusFormatter (cellValue, options, rowObject) {

        if (cellValue == 0)
            return "Inactive";
        else if (cellValue == '1')
            return "Active";
        else
            return '';
    }


    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject['Status'] == '0')  select = ['Email History','Run Background Check','Activate Employee','Delete Employee','Print Envelope']
            if(rowObject['Status'] == '1')  select = ['Edit','Email','Email History','TEXT','TEXT History','In-Touch History','Flag Bank','File Library','Notes & History','Add In-touch','Run Background Check','Archive Employee','Delete Employee','Print Envelope']
            var data = '';

            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data_email="' + rowObject.Email + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }

            return data;
        }
    }
    $(document).on('click','#import_employee',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_tenant_type_div').show(500);
    });
    $(document).on("click", "#import_tenant_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_tenant_type_div").hide(500);
                }
            }
        });
    });
    $("#importTenantTypeFormId").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importPropertyForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'EmployeeListAjax');
            formData.append('action', 'importExcel');
            $.ajax({
                type: 'post',
                url: '/employeeListAjax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);

                        $("#import_tenant_type_div").hide(500)
                        $('#employee-table').trigger('reloadGrid');
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                        // $('.error').html(response.message);
                        // $.each(response.message, function (key, value) {
                        //     $('.'+key).html(value);
                        // });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });


    function deleteEmployee(id) {
        $.ajax({
            type: 'post',
            url: '/employeeListAjax',
            data: {
                class: "EmployeeListAjax",
                action: "deleteEmployee",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#employee-table').trigger( 'reloadGrid' );
                    alphabeticSearch();
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /**
     * jqGrid function to format tooltip
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function cellAttri(cellValue, options, rowObject) {
        if(rowObject !== undefined){

            if(rowObject.Phone_Note != ''){
                console.log('aaaaa>>',rowObject.Phone_Note);
                return 'title = " "';
            }
        }
    }


    function changeStatus(id,status) {
        $.ajax({
            type: 'post',
            url: '/employeeListAjax',
            data: {
                class: "EmployeeListAjax",
                action: "changeStatus",
                user_id: id,
                status:status
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#employee-table').trigger( 'reloadGrid' );
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /** Export unit type excel  */
    $(document).on("click",'#export_tenant',function(){
        var base_url = window.location.origin;
        var status = $("#jqGridStatus option:selected").val();
        var table = 'users';
        var table1 = 'tenant_phone';
        var table2 = 'employee_details';
        window.location.href = base_url+"/employeeListAjax?status="+status+"&&table="+table+"&&table1="+table1+"&&table2="+table2+"&&action=exportExcel";
        $(this).off('click');
        return false;
    });


    $(document).on("change", "#employee-table .select_options", function (e) {
        $('#employee-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        var row_num = $(this).parent().parent().index() ;
        jQuery('#employee-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#employee-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
      //  e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var email = $(this).attr('data_email');
        switch (action) {
            case "Delete Employee":
                bootbox.confirm("Are you sure you want to delete this entry?", function (result) {
                    if (result == true) {
                        deleteEmployee(id);
                    }
                });
                break;
            case "Archive Employee":
                bootbox.confirm("Are you sure you want to archive the current record?", function (result) {
                    if (result == true) {
                        var status = 0;
                        changeStatus(id,status);
                    }
                });
                break;
            case "Activate Employee":
                bootbox.confirm("Are you sure you want to activate the current record?", function (result) {
                    if (result == true) {
                        var status = 1;
                        changeStatus(id,status);
                    }
                });
                break;
            case "Edit":
                 window.location.href = base_url + '/Employee/Edit?id='+id;
                break;
            case "Flag Bank":
                localStorage.setItem("AccordionHref",'#flag_bank');
                window.location.href = base_url+'/Employee/View?id='+id;
                break;
            case "File Library":
                localStorage.setItem("AccordionHref",'#filelibraryss');
                window.location.href = base_url+'/Employee/View?id='+id;
                break;
            case "Notes & History":
                localStorage.setItem("AccordionHref",'#noteshistory');
                window.location.href = base_url+'/Employee/View?id='+id;
                break;
            case "Work Order":
                window.location.href = base_url + '/WorkOrder/WorkOrders?WorkOrder='+id;
                break;
            case "Add In-touch":
                window.location.href = "/Communication/NewInTouch?tid="+id+"&category=Person";
                //window.location.href = base_url + '/Communication/InTouch';
                break;
            case "In-Touch History":
                var propertyname=$('#'+id).find('td:first').text();
                localStorage.setItem("propertyname",propertyname);
                //window.location.href = "/Communication/InTouch";
                window.location.href = base_url + '/Communication/InTouch';
                break;
            case "Email History":

                window.location.href = base_url + '/Communication/SentEmails';
                break;
            case "Email":

                localStorage.setItem('table_green_id',id);
                localStorage.setItem('table_green_url','/People/GetEmployeeList');
                localStorage.setItem('predefined_mail',email);
                localStorage.setItem('table_green_tableid',"#gview_employee-table");


                window.location.href = base_url + '/Communication/ComposeEmail';
                break;
            case "TEXT":
                var urlemail='/People/GetEmployeeList';
                localStorage.setItem('predefined_text',email);
                localStorage.setItem('table_green_tableid', '#gview_employee-table');
                localStorage.setItem('table_green_id',id);
                localStorage.setItem('table_green_url',urlemail);

                window.location.href = base_url + '/Communication/AddTextMessage';
                break;
            case "TEXT History":
                window.location.href = base_url + '/Communication/TextMessage';
                break;
            case "Run Background Check":
               // alert("pdf generation");
                $('#backGroundCheckPop').modal('show');
                break;
            case "Print Envelope":
                $.ajax({
                    type: 'post',
                    url: '/employeeListAjax',
                    data: {class: 'EmployeeListAjax', action: 'getEmployeeData','employee_id':id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            $("#PrintEmployeeEnvelope").modal('show');
                            $("#user_company_name").text(response.data.data.company_name)
                            $("#user_address1").text(response.data.data.address1)
                            $("#user_address2").text(response.data.data.address2)
                            $("#user_address3").text(response.data.data.address3)
                            $("#user_address4").text(response.data.data.address4)
                            $(".city").text(response.data.data.city)
                            if(response.data.data.state != '') {
                                $(".state").text(', ' + response.data.data.state)
                            }
                            $(".postal_code").text(response.data.data.zipcode)
                            $("#employee_first_name").text(response.employee.data.first_name)
                            $("#employee_last_name").text(response.employee.data.last_name)
                            $("#employee_middle_name").text(response.employee.data.middle_name)
                            $("#employee_address1").text(response.employee.data.address1)
                            $("#employee_address2").text(response.employee.data.address2)
                            $("#employee_address3").text(response.employee.data.address3)
                            $("#employee_address4").text(response.employee.data.address4)
                            $(".employee_city").text(response.employee.data.city)
                            if(response.employee.data.state != '') {
                                $(".employee_state").text(', ' + response.employee.data.state)
                            }
                            $(".employee_postal_code").text(response.employee.data.zipcode)
                        }else {
                            toastr.warning('Record not updated due to technical issue.');
                        }
                    }
                });
                break;
            default:
                window.location.href = base_url + '/Setting/ApexNewUser';
        }
        $('.select_options').prop('selectedIndex',0);
    });



});

/*function to print element by id */
function PrintElem(elem)
{
    Popup($(elem).html());
}
/*function to print element by id */
function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#print_complaint").modal('hide');
    $("#building-complaints").trigger('reloadGrid');
    return true;
}

$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {
        $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.open('https://www.victig.com', '_blank');
    }
});