//on change data_type custom_field
$(document).on('change','.data_type',function(){
    var selected_val = $(this).val();
    $('#default_value').val('');
    if(selected_val == 'date') {
        $('#default_value').prop('readonly', true);
        $('#default_value').datepicker({
            dateFormat: datepicker
        });
    } else {
        $('#default_value').prop('readonly', false);
        $('#default_value').datepicker( "destroy" );
        $('#default_value').removeClass("hasDatepicker");
        $('#default_value').unbind();
    }
    if(selected_val == 'currency'){
        $('#default_value').addClass("amount");
    } else {
        $('#default_value').removeClass("amount");
    }
});
//add rule for custom validation
var msg = '';
var messager = function() {
    return msg;
};

$.validator.addMethod("filedName", function(value, element) {
    return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
}, "Field Name must contain only letters, numbers, or dashes.");

$.validator.addMethod("custom", function(value, element, arg){
    arg = $('.data_type option:selected').val();
    if(value == "") return true;
    else if(arg == 'text'){
        if(value.length > 150){
            msg = 'Please enter character less than 150';
            return false;
        } else {
            return true;
        }
    } else if(arg == 'number') {
        if (isNaN(value)) {
            msg = 'Only number are allowed!';
            return false;
        } else {
            if(value > 20){
                msg = 'Please enter less than 20';
                return false;
            } else {
                return true;
            }
        }
    } else if(arg == 'currency'){
        var bef = value.replace(/,/g, '');
        if (isNaN(bef)) {
            msg = 'Only number are allowed!';
            return false;
        } else {
            return true;
        }
    } else if(arg == 'percentage'){
        if (isNaN(value)) {
            msg = 'Only number are allowed!';
            return false;
        } else {
            if(value > 100){
                msg = 'Maximum value is 100';
                return false;
            } else {
                return true;
            }
        }
    } else if(arg == 'url'){
        var url_val = $.validator.methods.url.bind(this);
        if(url_val(value, element)){
            return true;
        } else {
            msg = 'Please enter a valid URL';
            return false;
        }
    } else if(arg == 'date'){
        return true;
    }  else if(arg == 'memo'){
        return true;
    }
},messager);

//add propertySetup form client side validations
var custom_field_validator = $("#custom_field").validate({
    rules: {
        field_name: {
            maxlength: 50,
            required:true,
            filedName:true
        },
        default_value: {
            custom:true
        }
    }
});

