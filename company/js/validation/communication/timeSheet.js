//add property form client side validations
$("#time_sheet_form").validate({
    rules: {
        employee_id: {
            valueNotEquals:'default'
        },
        position: {
            valueNotEquals:'default'
        },
        email: {
            required:true
        }
    }
});

