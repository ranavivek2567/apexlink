//add property form client side validations
$("#saveNewBill").validate({
    rules: {
        vendor_id: {
            required:true
        },
        bill_date: {
            required:true
        },
        address: {
            required:true
        },
        amount: {
            required:true
        },
        due_date: {
            required:true
        },
        work_order: {
            required:true
        },
        portfolio_id: {
            required:true
        },
        property_id: {
            required:true
        }
    }
});



