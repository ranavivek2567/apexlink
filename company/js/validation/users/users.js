

$(document).ready(function () {

    /*To Reset form */
    $(document).on("click", "#cancel", function () {
        $("#login")[0].reset();
        var validator = $("#login").validate();
        validator.resetForm();
    });
    /*To Reset form */
    $(document).on("click", "#forgot_cancel,#reset_cancel", function () {
        window.location.href = '/';
    });

    $(document).on('click','#help_id',function(){
        $('#help-submenu').toggle();
    });
    $(document).on('click','#admin_id',function(){
        $('#admin-submenu').toggle();
    });
});


// login validation
 $("#login").validate({
    rules: {
        email: {
            required: true,
            email: true,
            maxlength: 50
        },
        password: {
            required: true,
        }
    },
    submitHandler: function (form) {

        var email = $("#email").val();
        var pass = $("#password").val();
        var super_admin_name = $("#super_admin_name").val();
       // console.log('super_admin_name>>>>', super_admin_name);
        if($("#remember").is(':checked'))
            var remember = 'on';  // checked
        else
            var remember = '';  // unchecked
        var systemDate = new Date();
        var year = systemDate.getFullYear();
        var month = systemDate.getMonth()+1;
        var date = systemDate.getDate();
        var hour= systemDate.getHours();
        var minutes= systemDate.getMinutes();
        var seconds= systemDate.getSeconds();
        var dateformats = year +'-'+month+'-'+date+' '+hour+':'+minutes+':'+seconds;
        $.ajax
        ({
            type: 'post',
            url: 'company-user-ajax',
            data: {
                class: "UserAjax",
                action: "login",
                email: email,
                password: pass,
                super_admin_name : super_admin_name,
                remember: remember,
                timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                dateformats:dateformats
            },
            success: function (response) {
                var data = $.parseJSON(response);

                if (data.status == "success")
                {
                    var redirect = (data.go_to_site_link != '' && data.go_to_site_link !== undefined)? data.go_to_site_link :data.data.enabled_2fa;
                    
                    if(redirect == 'on' || redirect == '0'){
                       
                        localStorage.setItem("login_user_id", data.data.id);
                            // alert(data.active_inactive_status.status);
                        localStorage.setItem("active_inactive_status",(data.active_inactive_status.status == undefined   ) ? 'all': data.active_inactive_status.status );
                        if (data.data.last_user_url) {
                            window.location.href = data.data.last_user_url;
                        } else {
                            window.location.href = "/Dashboard/Dashboard";
                        }
                    }

                    if(redirect == '1') {
                        window.location.href = "/2fa";
                    }
                } else if(data.status == 'error_sus' && data.code == 502){
                    $('#spotlight_model').modal('show');
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});

//forgot password validation
$("#forgot_password").validate({
    rules: {
        email: {
            required: true,
            email: true,
            maxlength: 50
        }
    },
    messages: {
        email: {
            email: "* Invalid email address",
        }
    },
    submitHandler: function (form) {
        var email = $("#email").val();
        $.ajax
        ({
            type: 'post',
            url: '/User/forgotPassword',
            data: {
                class: "forgotPasswordAjax",
                action: "forgotPassword",
                email: email
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if(data.status == 'success') {
                    var to = [];
                    to.push(data[0].email);
                    var subject = 'Forgot Password';
                    var message = data[0].message;
                    $.ajax
                    ({
                        type: 'post',
                        //  beforeSend: function(request) {
                        //     request.setRequestHeader("Access-Control-Allow-Origin", '*');
                        //     request.setRequestHeader("Access-Control-Allow-Credentials", true);
                        //     request.setRequestHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS");
                        //     request.setRequestHeader("Access-Control-Allow-Headers","Content-Type");
                        //   },
                        url: 'https://cors-anywhere.herokuapp.com/http://rentconnect.net/apex-email/index.php/api/email',
                        data: {
                            action: "SendMailPhp",
                            to: to,
                            subject: subject,
                            portal: 2,
                            message: message,
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                toastr.success('The Email was sent successfully.');
                                $('.login-inner').hide();
                                $('#click_here_login_link').show();

                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // alert(key+value);
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                } else {
                    toastr.warning(data.message);
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



});


$.validator.addMethod("password_regex", function(value, element) {
    var password_regex = this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9])/.test(value);
    return password_regex;
}, "* Atleast one aphanumeric and a numeric character required");

//reset password validation
$("#reset_password").validate({
    rules: {
        new_password: {
            required: true,
            minlength: 8,
            password_regex : true
        },
        confirm_password: {
            required: true,
            minlength: 8,
            password_regex : true,
            equalTo: "#new_password"
        },
    },
    messages: {
        new_password: {
            minlength: "* Minimum 8 characters allowed",
        },
        confirm_password: {
            minlength: "* Minimum 8 characters allowed",
            equalTo:  "* Fields do not match",
        },
    },
    submitHandler: function (form) {
        var email = $("#email").val();
        var password = $("#new_password").val();
        var forgot_password_token = $("#forgot_password_token").val();
        $.ajax
        ({
            type: 'post',
            url: '/User/resetPassword',
            data: {
                class: "forgotPasswordAjax",
                action: "resetPassword",
                email: email,
                password: password,
                forgot_password_token: forgot_password_token
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success")
                {
                    toastr.success(data.message);
                    $('#reset_password_div').hide();
                    $('#click_here_login_id').show();
                } else
                {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

});

//Logout validation
$(document).on("click", "#logout", function () {
//  debugger
$.ajax
({
    type: 'post',
    url: '/company-user-ajax',
    data: {
        class: "UserAjax",
        action: "logout",
        data :window.location.pathname+window.location.search
    },
    success: function (response) {
        var data = $.parseJSON(response);
        if (data.status == "success")
        {
            localStorage.removeItem("announcement_ids");
            window.location.href = "/";
        } else
        {
            toastr.error(data.message);
        }
    },
    error: function (data) {
        var errors = $.parseJSON(data.responseText);
        $.each(errors, function (key, value) {
            // alert(key+value);
            $('#' + key + '_err').text(value);
        });
    }
});
}); 

$(document).ready(function () {
    $('#notify_admin').click(function () {
        // var route_url = "{{URL::to('getNotification')}}";
        // $.ajax({
        //     type:"GET",
        //     url: route_url,
        //     dataType: "json",
        //     success:function(res) {
        //         if(res.code == 200) {
        //             console.log(res);
        //         }
        //         var html_data ="<li class=\"divider\"></li>\n";
        //         var data = res.data;
        //         console.log(data );
        //         if(data.length > 0)
        //         {
        //             $(data).each(function (key,value) {
        //                 html_data += "<li>\n" +
        //                     "<strong>"+ value.title +"</strong>\n" +
        //                     "<p>"+ value.description +"</p>"+
        //                     "</li> <li class=\"divider\"></li>\n" +
        //                     "";
        //             });
        //             html_data += "<li><a id=\"ViewAllNotification\" href=\"{{URL::to('/Alert/Notifications')}}\" class=\"innerLink pull-right\">View All</a></li>\n";

        //         } else {
        //             html_data += "<li>No New Notification</li>";
        //         }
        //         $('#BubbleDrop').html(html_data);
        //     },
        //     error:function (res) {
        //         var html_data ="<li class=\"divider\"></li>\n";
        //         html_data += "<li><p style='color: red;'>Something went wrong! Please try after some time</p></li>";
        //         $('#BubbleDrop').html(html_data);
        //     }
        //     //static data

        // });
        $.ajax({
            type: 'post',
            url: '/notification-ajax',
            data: {
                class: 'notificationsAjax',
                action: 'notificationslist',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    //$('.userNotification').html(response.html);
                    var html_data='';
                    html_data +='<li><strong>'+response.html[0].module_title+'</strong>: '+response.html[0].notifi_description+' </li> <li> <strong>'+response.html[1].module_title+'</strong>:'+response.html[1].notifi_description+'</li><li><a id="ViewAllNotification" href="/Alert/Notifications">View All</a></li>';
                    $('#BubbleDrop').html(html_data);

                } else {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });




        /*var html_data='';
        html_data +='<li><strong>Lease Signed</strong>: New Lease (ID -144) has been generated for Property ID-DW7VJ0 </li> <li> <strong>Lease Signed</strong>: New Lease (ID -141) has been generated for Property ID-NTDB5F</li><li><a id="ViewAllNotification" href="/Alert/Notifications">View All</a></li>';
             $('#BubbleDrop').html(html_data);*/
        $('.notify_arrow_box').slideToggle( "slow");
        $('.arrow_box').slideToggle( "slow");
        $('#BubbleDrop').slideToggle( "slow");
    });
});
