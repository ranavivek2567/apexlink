$.validator.addMethod('alphanumb', function(value, element) {
    return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
}, "Letters, numbers, and underscores only please.");

//add rule for select drop down
$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
}, "* This field is required.");

//add rule for number validation
$.validator.addMethod("checkNumber", function(value, element, arg){
    if(value == "") return true;
    var st = value.replace("-", "");
    if(isNaN(st.replace("-", ""))){
        return false;
    } else {
        return true;
    }
}, "Only number are allowed!");

//rule for filesize validation
$.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than 1 mb.');

//rule for image upload required
$.validator.addMethod('image_upload_required', function (value, element, param) {
    var previous = $(element).attr('data');
    var pre = $('#'+previous).val();
    if(pre === undefined) return ($(element).val() != '');
    else{ return true;}

}, '* This field is required.');

//add rule for number validation
$.validator.addMethod("maxAmount", function(value, element, arg){
    var am = parseInt(value.replace(/,/g, ''));
    if(am > 9999999999){
        return false;
    }
    return true;
}, "Please enter less than 10 digits.");

$.validator.addMethod("numberFalse", function(value, element, arg){
    if($(element).val() == '') return true;
    if(isNaN(value)){
        return true;
    }
    return false;
}, "Please enter valid details.");

jQuery.validator.addMethod("zipcode", function(value, element, arg) {
    if($('#'+arg).val() == '1' || $(element).val() == ''){
        return true;
    }
    return false;
}, "Please provide a valid zipcode.");


//add property form client side validations
$("#companySetup").validate({
    ignore: [],
    rules: {
        company_name: {
            required:true,
            maxlength:50,
            numberFalse:true
        },
        first_name: {
            required:true,
            maxlength:20,
            numberFalse:true
        },
        last_name: {
            required:true,
            maxlength:100,
            numberFalse:true
        },
        // middle_name:{
        //     maxlength:50,
        //     alphanumb: true
        // },
        // maiden_name:{
        //     maxlength:100
        // },
        // nick_name:{
        //     maxlength:100
        // },
        number_of_units: {
            required:true,
            number:true
        },
        plan_size: {
            required:true,
            number:true
        },
        // website: {
        //     maxlength:100,
        //     url: true
        // },
        zipcode: {
            required:true,
            zipcode:'zip_code_validate'
        },
        state: {
            required:true,
            maxlength:50
        },
        address1: {
            required:true,
            maxlength:250
        },
        city: {
            required:true,
            maxlength:50
        },
        country: {
            required:true,
            maxlength:50
        },
        // address2: {
        //     required:true,
        //     maxlength:250
        // },
        // address3: {
        //     maxlength:250
        // },
        // address4: {
        //     maxlength:250
        // },
        tax_id: {
            required:true,
            number:true
        },
        account_admin_name: {
            required:true,
            maxlength:50,
            numberFalse:true
        }
        // tax_payer_name: {
        //     maxlength:50,
        //     numberFalse:true
        // },
        // account_admin_email_address: {
        //     email:true,
        //     maxlength:250
        // },
        // application_fees: {
        //     maxAmount:10
        // },
        // contact_name_1099_efile: {
        //     maxlength:30,
        //     numberFalse:true
        // },
        // rent_amount: {
        //     maxAmount:10
        // },
        // currency: {
        //     valueNotEquals:'default'
        // },
        // payment_method: {
        //     valueNotEquals:'default'
        // },
        // company_logo: {
        //     extension: "png|jpeg|gif|jpg",
        //     filesize :1000000
        // },
        // signature: {
        //     extension: "png|jpeg|gif|jpg",
        //     filesize :1000000
        // }
    },
    invalidHandler: function(event, validator, element) {
        var errors = validator.numberOfInvalids();
        var ele = validator.errorList[0].element;
        var parse =$(ele).parentsUntil("#accordion").find('.acord').attr('aria-expanded');
        if(parse == 'false') {
            $(ele).parentsUntil("#accordion").find('.acord').click();
        }
    }
});


$(document).ready(function(){
  
cardLists();
bankLists();
checkAutoPay();
});


$("#addCards").validate({
    rules: {
        card_number: {
            required:true,
            number:true,
            maxlength: 19,
            minlength: 9
        },
       exp_date: {
            required: true,
        },
         cvc: {
            required: true,
            number:true,
            minlength:3,
            maxlength:5
            
        },
         holder_name: {
            required: true,
        },
     },
    submitHandler: function (e) {
        var form = $('#addCards')[0];
        var formData = new FormData(form);
        formData.append('action','addCardDetails');
        formData.append('class','Stripe');
        action = 'addAdditionalTenant';
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status=="true")
                {
                 toastr.success(response.message);
                   cardLists();
                   $("#addCards").find("input[type=text]").val("");

                }
                else
                {
                 toastr.error(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


function cardLists()
{

        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
            "action": 'getAllCards',
            "class": 'Stripe'
            },
            success: function (response) {
              
                
                 $(".cardDetails").html(response);
               
               
            }
        });  

}





function bankLists()
{

        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
            "action": 'getAllBanks',
            "class": 'Stripe'
            },
            success: function (response) {
              
                
                 $(".accountDetails").html(response);
               
               
            }
        });  

}

function checkAutoPay()
{

         $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
            "action": 'checkAutoPay',
            "class": 'Stripe'
            },
            success: function (data_res) {
                var info = JSON.parse(data_res);
             if(info.data=="ON")
              {

             $('.auto_pay').prop('checked', true);
              }
              else
              {
                               
             $('.auto_pay').prop('checked', false);
              }
               
               
            }
        });     

 }



$(document).on("change",".cardAction",function(){
 var cardAction = $(this).val();
 var card_id = $(this).attr('data-cardId');
 if(cardAction=='delete')
 {

     $(".cardAction").val('');
  bootbox.confirm("Do you want to delete this card?", function (result) {

        if (result == true) {
                  $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
            "action": 'deleteCard',
            "class": 'Stripe',
            "card_id":card_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                 toastr.success(response.message);

                  
                  cardLists();
                  bankLists();

                
               
               
            }
        });
        }
    });   
 }
 else if(cardAction=='default')
 {
     $(".cardAction").val('');
     bootbox.confirm("Do you want to set this method to default?", function (result) {
        if (result == true) {
            $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
            "action": 'defaultCard',
            "class": 'Stripe',
            "card_id":card_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                 toastr.success(response.message);
                  
                cardLists();
                bankLists();
             }
          }); 
         }
       });
       }
    });






$("#addAccounts").validate({    
    rules: {
        routing_number: {
            required:true,
            number:true,
        },
       account_number: {
            required: true,
              number:true,
        },
         account_type: {
            required: true,
           
        },
         holder_name: {
            required: true,
        },
     },
    submitHandler: function (e) {
        var form = $('#addAccounts')[0];
        var formData = new FormData(form);
        formData.append('action','addAccountDetails');
        formData.append('class','Stripe');
        action = 'addAdditionalTenant';
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status=="true")
                {
                 toastr.success(response.message);
                   cardLists();
                }
                else
                {
                 toastr.error(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});

$(document).on("click",".cancel",function(){
  bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
       location.reload();
        }
    });

});



