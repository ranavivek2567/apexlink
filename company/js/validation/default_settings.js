//add rule for select drop down
$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
}, "* * This field is required.");

//add rule for number validation
$.validator.addMethod("checkNumber", function(value, element, arg){
    if(value == "") return true;
    var st = value.replace("-", "");
    if(isNaN(st.replace(/-/g, ""))){
        return false;
    } else {
        return true;
    }
}, "* Only number are allowed!");

//add rule for number validation
$.validator.addMethod("maxAmount", function(value, element, arg){
    var am = parseInt(value.replace(/,/g, ''));
    if(am > 9999999999){
        return false;
    }
    return true;
}, "* Please enter less than 10 digits.");

jQuery.validator.addMethod("zipcode", function(value, element, arg) {
    if($('#'+arg).val() == '1' || $(element).val() == ''){
        return true;
    }
    return false;
}, "* Please provide a valid zipcode.");


//add clock settings form client side validations
$("#default_settings").validate({
    rules: {
        default_rent: {
            maxAmount:10
        },
        application_fees: {
            maxAmount:10
        },
        zip_code: {
            maxlength: 9,
            zipcode:'zip_code_validate'
        },
        country: {
            maxlength: 30
        },
        state: {
            maxlength: 30
        },
        city: {
            maxlength: 30
        }
    }
});

$("#zip_code_master_form").validate({
    rules: {
        zip_code: {
            required:true
            // zipcode:'zip_code_master_validate'
        },
        city: {
            required:true
        },
        state: {
            required:true
        },
        county: {
            required:true
        },
        country: {
            required:true
        }
    }
});

