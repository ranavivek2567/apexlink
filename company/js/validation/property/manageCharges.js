//add property form client side validations
$("#manageCharge").validate({
    rules: {
        charge_code: {
            required:true
        },
        unit_type: {
            required:true
        },
        frequency: {
            required:true
        },
        type: {
            required:true
        }

    }
});

