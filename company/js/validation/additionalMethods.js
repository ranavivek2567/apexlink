$.validator.addMethod('alphanumb', function(value, element) {
    return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
}, "Letters, numbers, and underscores only please.");

//add rule for select drop down
$.validator.addMethod("valueNotEquals", function(value, element, arg){
    return arg !== value;
}, "* This field is required.");

//add rule for number validation
$.validator.addMethod("checkNumber", function(value, element, arg){
    if(value == "") return true;
    var st = value.replace("-", "");
    if(isNaN(st.replace("-", ""))){
        return false;
    } else {
        return true;
    }
}, "Only number are allowed!");

//rule for filesize validation
$.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than 1 mb.');

//rule for image upload required
$.validator.addMethod('image_upload_required', function (value, element, param) {
    var previous = $(element).attr('data');
    var pre = $('#'+previous).val();
    if(pre === undefined) return ($(element).val() != '');
    else{ return true;}

}, '* This field is required.');

//add rule for number validation
$.validator.addMethod("maxAmount", function(value, element, arg){
    var am = parseInt(value.replace(/,/g, ''));
    if(am > 9999999999){
        return false;
    }
    return true;
}, "Please enter less than 10 digits.");

$.validator.addMethod("numberFalse", function(value, element, arg){
    if($(element).val() == '') return true;
    if(isNaN(value)){
        return true;
    }
    return false;
}, "Please enter valid details.");

jQuery.validator.addMethod("zipcode", function(value, element, arg) {
    if($('#'+arg).val() == '1' || $(element).val() == ''){
        return true;
    }
    return false;
}, "Please provide a valid zipcode.");