﻿$(document).ready(function () {
    setTimeout(function(){
        var tab = '';
    var tab = localStorage.getItem("tab");
    if(tab!="" || tab!=null)
    {
       goesToTabByReset(tab);
    }

    },300)
    


    getCharges();
    $(".viewtenantpage").hide();
    $(".noticePeriod").hide();

    $('.tenant_tab').on('click', function () {
        var table_id = $(this).attr('data_tab');
        if (table_id == 'additional_table') {
            var $grid = $("#TenantVehicle-table").setGridWidth($(window).width() - 100);
            var $grid = $("#TenantPet-table").setGridWidth($(window).width() - 100);
            var $grid = $("#TenantService-table").setGridWidth($(window).width() - 100);
            var $grid = $("#TenantMedical-table").setGridWidth($(window).width() - 100);
            var $grid = $("#TenantGuarantor-table").setGridWidth($(window).width() - 100);
            var $grid = $("#TenantCollection-table").setGridWidth($(window).width() - 100);
            var $grid = $("#TenantParking-table").setGridWidth($(window).width() - 100);
        } else {
            if (table_id !== undefined && table_id != '') {
                var $grid = $("#" + table_id).setGridWidth($(window).width() - 100);
            }
        }
    });


    $(document).on("click", ".tenant-detail-one", function () {
        $(".editpageviewsection").hide();
        $(".viewtenantpage").show();
    });

    $(document).on("click", ".add_reff_popup", function () {
        $("#selectPropertyReferralResource1").show();
    });

    $(document).on("click", ".add_ethn_popup", function () {
        $("#selectPropertyEthnicity1").show();
    });

    $(document).on("click", ".add_martl_popup", function () {
        $("#selectPropertyMaritalStatus1").show();
    });

    $(document).on("click", ".add_hoby_popup", function () {
        $("#selectPropertyHobbies1").show();
    });

    $(document).on("click", ".add_vtrn_popup", function () {
        $("#selectPropertyVeteranStatus1").show();
    });

    $(document).on("click", ".credntl_popup", function () {
        $("#tenantCredentialType1").show();
    });


    $(document).on("click", ".sectionOneEdit", function () {
        $(".editpageviewsection").show();
        $(".viewtenantpage").hide();

        $(".gn_noticePeriod").hide();

        $(".noticePeriod").show();


        $(".tenant_status").show();
        $(".gn_status").hide();
    });


    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getCharges',
        data: {
            class: "TenantAjax",
            action: "getCharges"
        },
        success: function (response) {

            var data = $.parseJSON(response);
            if (data.status == "success") {
                var charges = data.data.charges;
                if (charges.length > 0) {
                    var chargesOptions = '';
                    for (var i = 0; i < charges.length; i++) {
                        chargesOptions += "<option value='" + charges[i].id + "'>" + charges[i].charge_code + "</option>";
                    }
                    $("#addChargeForm .tax_chargeCode").html(chargesOptions);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });


    var tenant_id = $(".tenant_id").val();
    $.ajax({
        url: '/editTenant?action=getChargeListing&class=EditTenant&tenant_id=' + tenant_id,
        type: 'GET',
        success: function (data) {
            $('.chargeData').html(data);
        },
        cache: false,
        contentType: false,
        processData: false
    });


    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        data: {
            class: "TenantAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.country.length > 0) {
                    var countryOption = "<option value='0'>Select</option>";
                    var countryOption1 = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        countryOption += "<option value='" + value.id + "' data-id='" + value.code + "'>" + value.name + "</option>";
                        countryOption1 += "<option value='" + value.id + "' data-id='" + value.code + "'>" + value.name + " ( " + value.code + " )" + "</option>";
                    });
                    $('.edit_general_country').html(countryOption);
                    $('select[name="service_countryCode"]').html(countryOption);
                    $('select[name="pet_countryCode"]').html(countryOption);
                    $('select[name="guarantor_country"]').html(countryOption);
                    $('select[name="guarantor_form2_country"]').html(countryOption);
                    $('select[name="countryCode_emer_occupancy"]').html(countryOption1);

                }

                if (data.data.propertylist.length > 0) {
                    var propertyOption = "<option value='0'>Select</option>";
                    $.each(data.data.propertylist, function (key, value) {
                        propertyOption += "<option value='" + value.id + "' data-id='" + value.property_id + "'>" + value.property_name + "</option>";
                    });
                    $('#addTenant #property').html(propertyOption);
                }

                if (data.data.phone_type.length > 0) {
                    var phoneOption = "";
                    $.each(data.data.phone_type, function (key, value) {
                        phoneOption += "<option value='" + value.id + "'>" + value.type + "</option>";
                    });
                    $('.primary-tenant-phone-row select[name="phoneType[]"]').html(phoneOption);
                    $('.addition_tenant_block select[name="additional_phoneType"]').html(phoneOption);
                }

                if (data.data.carrier.length > 0) {
                    var carrierOption = "";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='" + value.id + "'>" + value.carrier + "</option>";
                    });
                    $('.primary-tenant-phone-row select[name="carrier[]"]').html(carrierOption);
                    $('.addition_tenant_block select[name="additional_carrier"]').html(carrierOption);
                    $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                    $('.additional_carrier').html(carrierOption);
                }

                if (data.data.referral.length > 0) {
                    var referralOption = "";
                    $.each(data.data.referral, function (key, value) {
                        referralOption += "<option value='" + value.id + "'>" + value.referral + "</option>";
                    });
                    $('.edit_general_referral').html(referralOption);
                }

                if (data.data.ethnicity.length > 0) {
                    var ethnicityOption = "";
                    $.each(data.data.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='" + value.id + "'>" + value.title + "</option>";
                    });
                    $('select[name="ethncity"]').html(ethnicityOption);
                    $('.addition_tenant_block select[name="additional_ethncity"]').html(ethnicityOption);
                }

                if (data.data.marital.length > 0) {
                    var maritalOption = "<option value='0'>Select</option>";
                    $.each(data.data.marital, function (key, value) {
                        maritalOption += "<option value='" + value.id + "'>" + value.marital + "</option>";
                    });
                    $('.edit_general_marital').html(maritalOption);
                }

                if (data.data.hobbies.length > 0) {
                    var hobbyOption = "";
                    $.each(data.data.hobbies, function (key, value) {
                        hobbyOption += "<option value='" + value.id + "'>" + value.hobby + "</option>";
                    });
                    $('.edit_general_hobby').html(hobbyOption);
                    $('.edit_general_hobby').multiselect({includeSelectAllOption: true, nonSelectedText: 'Select'});
                }

                if (data.data.veteran.length > 0) {
                    var veteranOption = "";
                    $.each(data.data.veteran, function (key, value) {
                        veteranOption += "<option value='" + value.id + "'>" + value.veteran + "</option>";
                    });
                    $('.edit_general_veteran').html(veteranOption);
                }

                if (data.data.collection_reason.length > 0) {
                    var reasonOption = "";
                    $.each(data.data.collection_reason, function (key, value) {
                        reasonOption += "<option value='" + value.id + "'>" + value.reason + "</option>";
                    });
                    $('.property_collection select[name="collection_reason"]').html(reasonOption);
                    $('#collection_reason').html(reasonOption);
                }

                if (data.data.credential_type.length > 0) {
                    var typeOption = "";
                    $.each(data.data.credential_type, function (key, value) {
                        typeOption += "<option value='" + value.id + "'>" + value.credential_type + "</option>";

                    });
                    $('.edit_general_credentials').html(typeOption);
                }

                if (data.data.complaints.length > 0) {
                    var typeOption = "";
                    $.each(data.data.complaints, function (key, value) {
                        typeOption += "<option value='" + value.id + "'>" + value.complaint_type + "</option>";

                    });
                    $('select[name="complaint_type"]').html(typeOption);
                }


            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });


    $.ajax({
        url: '/editTenant?action=getTenantInformation&class=EditTenant&tenant_id=' + tenant_id,
        type: 'GET',
        success: function (data) {
            var tenantInfo = JSON.parse(data);
            var noticePeriod = tenantInfo.gn_noticePeriod;
            var license_state = tenantInfo.tenantAdditionalInfo.tenant_license_state;
            /*for append html as value*/
            $('.noticePeriod').val(noticePeriod);
            $('.license_state').text(license_state);

            $('.phoneInfo').html(tenantInfo.phoneInfo);
            $('.emergencyInfo').html(tenantInfo.emergencyInfo);

            $('.salutation').val(tenantInfo.gn_salutation);
            $('.ethnicity').val(tenantInfo.ethnicity_id);
            $('.veteran').val(tenantInfo.veteran_id);

            $('#flagform11 #flaggedfor').val(tenantInfo.gn_tenant_name);
            $('#flagform11 input[name="phone_number"]').val(tenantInfo.gn_tenant_phone);
            // $('#flagform11 input[name="flag_name"]').val(tenantInfo.gn_tenant_phone);
            var hobbies = tenantInfo.hobbies;


            $.each(tenantInfo, function (key, value) {
                if (key == "gn_salutation") {
                    if (value == "Mrs." || value == "Madam" || value == "Sister" || value == "Mother") {
                        $(".maiden_name_hide").show();
                    }
                    $('.' + key).parent().hide();
                }

                if (key == "gn_address1" && value == "") {
                    $('.' + key).parent().hide();
                }
                if (key == "gn_address2" && value == "") {
                    $('.' + key).parent().hide();
                }
                if (key == "gn_address3" && value == "") {
                    $('.' + key).parent().hide();
                }
                if (key == "gn_address4" && value == "") {
                    $('.' + key).parent().hide();
                }
                if (key == "gn_unit_name" && value == "") {
                    $('.' + key).parent().hide();
                }
                if (key == "gn_phone_number_note" && value != "") {
                    setTimeout(function () {
                        //edit_date_time(tenantInfo.gn_tenant_updated);
                    },2000);
                }
                if (value != "") {
                    if (key == "gn_address1" || key == "gn_address2" || key == "gn_address3" || key == "gn_address4") {
                        updateAddressValues(value, key);
                    } else {
                        $('.' + key).html(value);
                    }
                }
            });

            $.each(tenantInfo, function (key, value) {
                if (value != "") {
                    if (key == "gn_tenant_contact") {
                        $('.' + key).val(value);
                        $(".entity").prop("checked", "checked");
                        $(".tenant_contact").val(value);

                    }
                    $('.' + key).val(value);
                }
            });
            editTenantFullInfo();

            setTimeout(function () {
                $.each(hobbies, function (key, value) {
                    $(":checkbox[value='" + value + "']").prop("checked", true);
                    $(":checkbox[value='" + value + "']").parent().parent().parent().addClass("active");


                });
                $('.addCheckBoxName :checkbox').attr('name', 'checkBoxhobbies[]');

            }, 400);


        },
        cache: false,
        contentType: false,
        processData: false
    });


    /* parvesh code start 4-7-19*/

    $("#sendEmailTemplate").validate({
        rules: {
            sendEmail: {
                required: true
            }
        },
        submitHandler: function (e) {

            var tenant_id = $('.tenant_id').val();
            var form = $('#sendEmailTemplate')[0];
            var formData = new FormData(form);
            formData.append('action', 'sendEmailTemplate');
            formData.append('class', 'EditTenant');
            formData.append('tenant_id', tenant_id);
            action = 'update';


            $.ajax({
                url: '/editTenant',
                type: 'POST',
                data: formData,
                success: function (data) {
                    var info = JSON.parse(data);

                    if (info.status == 'success') {
                        toastr.success(info.message);
                    }else if(info.status == 'error'){
                        toastr.warning(info.message);
                    }

                },
                cache: false,
                contentType: false,
                processData: false
            });
            return false;
        }
    });

    /* parvesh code end 4-7-19*/

});


$(document).on("click", "#addChargeButton", function () {
    $(".chargeForm").fadeIn();


});


function getTenantRentInfo(tenantId) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getTenantRentInfo',
        data: {
            class: "TenantAjax",
            action: "getTenantRentInfo",
            id: tenantId
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                $("#edit_rent_form select[name='edit_rent_due_date']").val(data.data.rent_due_day);
                $("#edit_rent_form input[name='edit_rent_amount']").val(data.data.rent_amount);
                $("#edit_rent_form input[name='edit_cam_amount']").val(data.data.cam_amount);
                $("#edit_rent_form input[name='edit_security_amount']").val(data.data.security_deposite);
                $("#edit_rent_form select[name='edit_next_rent_inc']").val(data.data.next_rent_incr);
                $("#edit_rent_form input[name=edit_rent_incr][value=" + data.data.flat_perc + "]").prop('checked', true);
                $("#edit_rent_form input[name='edit_incr_amount']").val(data.data.amount_incr);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function saveTenantRentInfo() {
    var formData = $('#edit_rent_form').serializeArray();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveTenantRentInfo',
        data: {
            class: "TenantAjax",
            action: "saveTenantRentInfo",
            form: formData
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

$(document).on("click", "#people-tenant .nav-tabs .rent_tab", function (e) {
    e.preventDefault();
    getTenantRentInfo($(".tenant_id").val());
    return false;
});

$(document).on("click", "#edit_rent_form .save_edit_rent", function (e) {
    e.preventDefault();
    $(".edit_rent_user_id").val($(".tenant_id").val());
    $("#edit_rent_form").trigger('submit');
    return false;
});

$(document).on("submit", "#edit_rent_form", function (e) {
    e.preventDefault();
    saveTenantRentInfo();
    return false;
});

$('.number_only').keydown(function (e) {
    if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
        return false;
    }
});

$(document).on('focusout', '#rent_amount,#cam_amount,#security_amount,#increase_amount', function () {

    var id = this.id;
    if ($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
        var bef = $('#' + id).val().replace(/,/g, '');
        var value = numberWithCommas(bef) + '.00';
        $('#' + id).val(value);
    } else {
        var bef = $('#' + id).val().replace(/,/g, '');
        $('#' + id).val(numberWithCommas(bef));
    }
});


function editTenantFullInfo() {
    var tenant_id = $('.tenant_id').val();
    $("#editTenant").validate({
        rules: {
            salutation: {
                required: true
            },
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            'email[]': {
                required: true,
                email: true,
                /*remote: {
                    url: '/editTenant',
                    type: 'POST',
                    data: {action: 'checkEmailIsExists', id: tenant_id},
                }*/
            }
        },
        messages: {
            'email[]': {
                required: "Please enter your email address.",
                email: "Please enter a valid email address.",
                remote: "Email already in use!"
            }
        },
        submitHandler: function (e) {

            var tenant_id = $('.tenant_id').val();
            var tenant_image = $('.tenant_image').html();

            var tenantImage = JSON.stringify(tenant_image);

            var noticePeriod = $(".noticePeriod").val();
            var tenantStatus = $(".tenant_status").val();
            //alert(tenantStatus);


            var form = $('#editTenant')[0];
            var formData = new FormData(form);
            formData.append('action', 'update');
            formData.append('class', 'EditTenant');
            formData.append('tenant_image', tenantImage);
            formData.append('tenant_id', tenant_id);
            formData.append('notice_period', noticePeriod);
            formData.append('tenant_status', tenantStatus);
            action = 'update';


            $.ajax({
                url: '/editTenant',
                type: 'POST',
                data: formData,
                success: function (data) {
                    var checkUrl = $("#url_check").val();

                    var info = JSON.parse(data);
                    if (info.status == 'success') {
                        localStorage.setItem("Message", "Record updated successfully");
                        localStorage.setItem("rowcolorTenant", 'green');
                        if (checkUrl == 'tenant') {
                            window.location.href = window.location.origin + "/Tenantlisting/Tenantlisting";
                        }
                        if (checkUrl == 'lease') {

                            window.location.href = window.location.origin + "/Lease/ViewEditLease/";
                        }
                    }else if(info.status == 'error'){
                        toastr.warning(info.message);
                    }


                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });

}


$(document).ready(function () {
    getTenantVehicle('All');
    getPets('All');
    getServices('All');
    getMedicalAllergy('All');
    getCollection('All');
    getParking('All');
    getGuarantor('All');
    getFiles('All');
    getTenantComplaints('All');

    $(document).on("click", ".ChargeBtn1", function () {
        $(this).hide();
        $("#addChargeButton").parent().show();
    });

    $(document).on("change", "input[name='edit_rent_incr']", function () {
        var value = $(this).val();
        if (value == "flat") {
            $("#edit_incr_amount").prev().html("Amount (" + currencySign + ")");
        } else {
            $("#edit_incr_amount").prev().html("Percentage (%)");
        }
    });

});


function getTenantVehicle(status) {
    var table = 'tenant_vehicles';
    var columns = ['id', 'Name', 'Make', 'Plate', 'Color', 'Year', 'VIN', 'Registration #', 'Vehicles Image/Photo', 'Image1', 'Image2', 'Image3', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'user_id', value: tenant_id, condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'id',
            index: 'id',
            width: 150,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'Name', index: 'type', width: 150, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Make', index: 'make', width: 150, searchoptions: {sopt: conditions}, table: table},
        {name: 'Plate', index: 'license', width: 150, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Color', index: 'color', width: 150, align: "center", searchoptions: {sopt: conditions}, table: table,},
        {name: 'Year', index: 'year', width: 150, searchoptions: {sopt: conditions}, table: table},
        {name: 'VIN', index: 'vin', width: 150, searchoptions: {sopt: conditions}, table: table},
        {name: 'Registration', index: 'registration', width: 150, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'vehicles image/Photo',
            index: 'make',
            width: 150,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table,
            formatter: vehicleImageFormatter
        },
        {
            name: 'Image1',
            index: 'photo1',
            width: 150,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Image2',
            index: 'photo2',
            width: 150,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Image3',
            index: 'photo3',
            width: 150,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 150,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#TenantVehicle-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Vehicles",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


function vehicleImageFormatter(cellValue, options, rowObject) {


    if (rowObject !== undefined) {
        var image1 = rowObject.Image1;
        var image2 = rowObject.Image1;
        var image3 = rowObject.Image1;
        if (image3 == "" && image2 == "" && image1 == "") {
            return "No Image Uploaded";
        }
        else {
            return "<a href='javascript:void(0);' data-id='" + rowObject.id + "' class='getImagesByTable' data-tablename='tenant_vehicles'>vehicles image/Photo</a>";
        }


    }
}

$(document).on("click", ".getImagesByTable", function () {
    var tablename = $(this).attr('data-tablename');
    var id = $(this).attr('data-id');
    var imageType = '';
    if (tablename == "tenant_vehicles") {
        imageType = ' :Vehicle Photo/Image';
    } else if (tablename == "tenant_pet") {
        imageType = ' :Pet Photo/Image';
    } else if (tablename == "tenant_service_animal") {
        imageType = ' :Service Animal Photo/Image';
    } else if (tablename == "hoa_violation") {
        imageType = ' :HOA Violation Photo/Image';
    } else if (tablename == "tenant_maintenance") {
        imageType = ' :Maintenance Photo/Image';
    }else{
        imageType = '';
    }
    $.ajax({
        url: '/editTenant?action=getImageByTable&class=EditTenant&id=' + id + '&tablename=' + tablename,
        type: 'GET',
        success: function (data) {
            var res = JSON.parse(data);
            var image = res.getData;
            var tenantName = res.userName;
            setTimeout(function () {
                $('#imageModel').modal('show');
                $('#imageModel .modal-title').text(tenantName+imageType);
                $('#getImage1').html(image.img1);
                $('#getImage2').html(image.img2);
                $('#getImage3').html(image.img3);
            }, 400);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});


function getTenantComplaints(status) {
    var table = 'complaints';
    var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">', 'Complaint ID', 'Complaint', 'Complaint Type', 'Notes', 'Date', 'Acion'];
    var select_column = ['Edit', 'Delete'];
    var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'user_id', value: tenant_id, condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'Id',
            index: 'id',
            width: 190,
            align: "center",
            sortable: false,
            searchoptions: {sopt: conditions},
            table: table,
            search: false,
            formatter: actionCheckboxFmatterComplaint
        },
        {
            name: 'Complaint ID',
            index: 'complaint_id',
            width: 190,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'Complaint', index: 'complaint_by_about', width: 190, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Complaint Type',
            index: 'complaint_type',
            width: 190,
            align: "center",
            searchoptions: {sopt: conditions},
            table: 'complaint_types'
        },
        {name: 'Notes', index: 'complaint_note', width: 190, align: "center", searchoptions: {sopt: conditions}, table: table,},
        {name: 'Date', index: 'complaint_date', width: 190, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 190,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#TenantComplaint-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: 'complaints.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Tenant Complaints",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


$(document).on("click", "#select_all_complaint_checkbox", function () {
    if (this.checked) {


        $(".complaint_checkbox").prop("checked", true);

    }
    else {
        $(".complaint_checkbox").prop("checked", false);
    }

});


function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {

        var data = '';
        var data = '<input type="checkbox" name="complaint_checkbox[]" class="complaint_checkbox" id="complaint_checkbox_' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
        return data;
    }
}

$("#select_all_complaint_checkbox").click(function () {
    $(".complaint_checkbox").prop('checked', $(this).prop('checked'));
});


$(document).on("click", '#print_email_button', function () {

    favorite = [];
    var no_of_checked = $('[name="complaint_checkbox[]"]:checked').length
    if (no_of_checked == 0) {
        toastr.error('Please select atleast one Complaint.');
        return false;
    }
    $.each($("input[name='complaint_checkbox[]']:checked"), function () {
        favorite.push($(this).attr('data_id'));
    });

    $.ajax({
        type: 'post',
        url: '/editTenant',
        data: {class: 'EditTenant', action: 'getComplaintsData', 'complaint_ids': favorite, 'id': tenant_id},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $("#print_complaint").modal('show');
                $("#modal-body-complaints").html(response.html)

            } else {
                toastr.warning('Record not updated due to technical issue.');
            }
        }
    });
});

function PrintElem(elem) {
    Popup($(elem).html());
}

/*function to print element by id */
function Popup(data) {
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.body).html('<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if (mywindow.close()) {

    }
    $("#print_complaint").modal('hide');
    return true;
}


function getPets(status) {
    var table = 'tenant_pet';
    var columns = ['id', 'Name', 'ID', 'Type', 'Age', 'Sex', 'Weight', 'Note', 'Pet Color', 'Chip ID',
        'Vet/Hosp Name', 'Phone Number', 'Last Visit', 'Next Visit', 'Birth Date', 'Image1', 'Image2', 'Image3', 'Pets image/Photo', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'user_id', value: tenant_id, condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'id',
            index: 'id',
            width: 108,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'Name', index: 'name', width: 108, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'ID', index: 'pet_id', width: 108, searchoptions: {sopt: conditions}, table: table},
        {name: 'Type', index: 'type', width: 108, align: "center", searchoptions: {sopt: conditions}, table: table},
        /*{name:'Age',index:'age', width:108, align:"center",searchoptions: {sopt: conditions},table:table,},*/
        {
            name: 'Age',
            index: 'dob',
            width: 108,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table,
            formatter: getAge
        },
        {
            name: 'Sex',
            index: 'gender',
            width: 108,
            searchoptions: {sopt: conditions},
            table: table,
            formatter: genderFormatter
        },
        {name: 'Weight', index: 'weight', width: 108, searchoptions: {sopt: conditions}, table: table},
        {name: 'Note', index: 'note', width: 108, searchoptions: {sopt: conditions}, table: table},
        {name: 'Color', index: 'color', width: 108, searchoptions: {sopt: conditions}, table: table},
        {name: 'Chip ID', index: 'chip_id', width: 108, searchoptions: {sopt: conditions}, table: table},
        {name: 'Vet/Hosp', index: 'hospital_name', width: 108, searchoptions: {sopt: conditions}, table: table},
        {name: 'Phone Number', index: 'phone_number', width: 108, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Last Visit',
            index: 'last_visit',
            width: 108,
            change_type: 'date',
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Next Visit',
            index: 'next_visit',
            width: 108,
            change_type: 'date',
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Birth Date',
            index: 'dob',
            width: 108,
            change_type: 'date',
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Pets image/Photo',
            index: 'dob',
            width: 108,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table,
            formatter: petImageFormatter
        },
        {
            name: 'Image1',
            index: 'image1',
            width: 108,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Image2',
            index: 'image2',
            width: 108,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Image3',
            index: 'image3',
            width: 108,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 95,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#TenantPet-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Pets",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


function petImageFormatter(cellValue, options, rowObject) {


    if (rowObject !== undefined) {
        var image1 = rowObject.Image1;
        var image2 = rowObject.Image1;
        var image3 = rowObject.Image1;
        if (image3 == "" && image2 == "" && image1 == "") {
            return "No Image Uploaded";
        }
        else {
            return "<a href='javascript:void(0);' data-id='" + rowObject.id + "' class='getImagesByTable' data-tablename='tenant_pet'>Pet Image/Photo</a>";
        }


    }


}


function getServices(status) {
    var table = 'tenant_service_animal';
    var columns = ['id', 'Name', 'ID', 'Type', 'Age', 'Sex', 'Weight', 'Note', 'Color', 'Chip ID',
        'Vet/Hosp', 'Phone Number', 'Last Visit', 'Next Visit', 'Birth Date', 'Image1', 'Image2', 'Image3', 'Pets image/Photo', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'user_id', value: tenant_id, condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'id',
            index: 'id',
            width: 107,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'Name', index: 'name', width: 107, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'ID', index: 'animal_id', width: 107, searchoptions: {sopt: conditions}, table: table},
        {name: 'Type', index: 'type', width: 107, align: "center", searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Age',
            index: 'dob',
            width: 107,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table,
            formatter: getAge
        },
        {
            name: 'Sex',
            index: 'gender',
            width: 107,
            searchoptions: {sopt: conditions},
            table: table,
            formatter: genderFormatter
        },
        {name: 'Weight', index: 'weight', width: 107, searchoptions: {sopt: conditions}, table: table},
        {name: 'Note', index: 'note', width: 107, searchoptions: {sopt: conditions}, table: table},
        {name: 'Color', index: 'color', width: 107, searchoptions: {sopt: conditions}, table: table},
        {name: 'Chip ID', index: 'chip_id', width: 107, searchoptions: {sopt: conditions}, table: table},
        {name: 'Vet/Hosp', index: 'hospital_name', width: 107, searchoptions: {sopt: conditions}, table: table},
        {name: 'Phone Number', index: 'phone_number', width: 107, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Last Visit',
            index: 'last_visit',
            width: 107,
            change_type: 'date',
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'Next Visit', index: 'next_visit', width: 107, searchoptions: {sopt: conditions}, table: table},
        {name: 'Birth Date', index: 'dob', width: 107, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Pets image/Photo',
            index: 'dob',
            width: 107,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table,
            formatter: serviceImageFormatter
        },
        {
            name: 'Image1',
            index: 'image1',
            width: 107,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Image2',
            index: 'image2',
            width: 107,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Image3',
            index: 'image3',
            width: 107,
            align: "center",
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 109,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#TenantService-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Service/Companion Animals",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


function getMedicalAllergy(status) {
    var table = 'tenant_medical_allergies';
    var columns = ['Tenant’s Medical/Allergy', 'Date', 'Notes', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'user_id', value: tenant_id, condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'Tenant’s Medical/Allergy',
            index: 'allergy',
            width: 430,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'Date', index: 'date', width: 430, searchoptions: {sopt: conditions}, table: table},
        {name: 'Notes', index: 'note', width: 430, align: "center", searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 425,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#TenantMedical-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Medical Allergies",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


function getCollection(status) {
    var table = 'tenant_collection';
    var columns = ['Reason', 'Description', 'Amount Due', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [{table: 'tenant_collection', column: 'reason', primary: 'id', on_table: 'tenant_collection_reason'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'user_id', value: tenant_id, condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'Reason',
            index: 'reason',
            width: 430,
            align: "center",
            searchoptions: {sopt: conditions},
            table: 'tenant_collection_reason'
        },
        {name: 'Description', index: 'description', width: 430, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Amount Due',
            index: 'amount_due',
            width: 430,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 425,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#TenantCollection-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: 'tenant_collection.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Collection",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


function getParking(status) {
    var table = 'tenant_parking';
    var columns = ['Date', 'Parking Permit Number', 'Parking Space Number', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'user_id', value: tenant_id, condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'Date',
            index: 'created_at',
            width: 430,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Parking Permit Number',
            index: 'permit_number',
            width: 430,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Parking Space Number',
            index: 'space_number',
            width: 430,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 425,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#TenantParking-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Parking",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


function imageFormatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        var filename = rowObject.files;
        var nameArr = filename.split('.');
        var extentsion = nameArr[1];

        var src = '';
        if (extentsion == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
            srcFile = upload_url + 'company/uploads/tenant_files/' + rowObject.files;
        } else if (extentsion == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
            srcFile = upload_url + 'company/uploads/tenant_files/' + rowObject.files;
        } else if (extentsion == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
            srcFile = upload_url + 'company/uploads/tenant_files/' + rowObject.files;
        } else if (extentsion == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
            srcFile = upload_url + 'company/uploads/tenant_files/' + rowObject.files;
        }
        else if (extentsion == undefined) {
            return '<h3>No File Exist</h3>';
        }
        else {
            src = upload_url + 'company/uploads/tenant_files/' + rowObject.files;
            srcFile = upload_url + 'company/uploads/tenant_files/' + rowObject.files;
        }

        return '<a href="' + srcFile + '"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';

    }

}


function getGuarantor(status) {
    var table = 'tenant_guarantor';
    var columns = ['Name', 'Relationship', 'Address', 'Email', 'Years of Guaranee', 'files', 'Notes', 'File Uploaded', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'user_id', value: tenant_id, condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {
            name: 'Name',
            index: 'first_name',
            width: 215,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'Relationship', index: 'relationship', width: 215, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'Address',
            index: 'address1',
            width: 215,
            align: "center",
            searchoptions: {sopt: conditions},
            table: table
        },
        {name: 'Email', index: 'email1', width: 215, align: "center", searchoptions: {sopt: conditions}, table: table,},
        {
            name: 'Years of Guaranee',
            index: 'guarantee_years',
            width: 215,
            searchoptions: {sopt: conditions},
            table: table,
            formatter: genderFormatter
        },
        {name: 'Notes', index: 'note', width: 215, searchoptions: {sopt: conditions}, table: table},
        {name: 'files', index: 'file_name', width: 215, hidden: true, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'File Uploaded',
            index: 'note',
            width: 215,
            searchoptions: {sopt: conditions},
            table: table,
            formatter: imageFormatter
        },
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 210,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table
        }
    ];
    var ignore_array = [];
    jQuery("#TenantGuarantor-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Guarantor",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


function getFiles(status) {


    var table = 'tenant_chargefiles';
    var columns = ['Name', 'View', 'File_location', 'File_extension', 'Action'];
    var select_column = ['Email', 'Delete'];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'user_id', value: tenant_id, condition: '='}, {
        column: 'type',
        value: 'N',
        condition: '='
    }];
    var extra_columns = [];
    var joins = [];
    var columns_options = [
        {name: 'Name', index: 'filename', width: 300, searchoptions: {sopt: conditions}, table: table},
        {
            name: 'View',
            index: 'file_type',
            width: 300,
            searchoptions: {sopt: conditions},
            table: table,
            formatter: fileFormatter
        },
        {
            name: 'File_location',
            index: 'file_location',
            width: 100,
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'File_extension',
            index: 'file_extension',
            width: 100,
            hidden: true,
            searchoptions: {sopt: conditions},
            table: table
        },
        {
            name: 'Action',
            index: 'select',
            title: false,
            width: 300,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table,
            formatter:actionLibraryFmatter
        }
    ];


    var ignore_array = [];
    jQuery("#TenantFiles-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_where: extra_where,
            extra_columns: extra_columns,
            deleted_at: 'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "File Library",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 0, left: 400, drag: true, resize: false} // search options
    );
}


function actionLibraryFmatter (cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        var editable = $(cellvalue).attr('editable');
        var select = '';
        var file_type =  rowObject.View;
        var location = rowObject.File_location;
        var path = upload_url+'company/'+location;

        var imageData = '';
        var src = '';
        if(file_type == '1'){

            imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
        } else {
            if (rowObject.File_extension == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'docx' || rowObject.File_extension == 'doc') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }else if (rowObject.File_extension == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }
        }

        select = ['Email','Delete'];
        var data = '';

        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-path="'+path+'" data-src="'+src+'"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

function fileFormatter(cellValue, options, rowObject) {
    if (rowObject !== undefined) {
        var file_type = rowObject.View;
        var location = rowObject.File_location;
        var path = upload_url + 'company/' + location;
        var imageData = '';
        if (file_type == '1') {

            imageData = '<a href="' + path + '"><img width=200 height=200 src="' + path + '"></a>';
        } else {
            if (rowObject.File_extension == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
                imageData = '<a href="' + path + '"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
                imageData = '<a href="' + path + '"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
                imageData = '<a href="' + path + '"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
                imageData = '<a href="' + path + '"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }
        }
        return imageData;
    }
}


function genderFormatter(cellValue, options, rowObject) {

    if (rowObject !== undefined) {
        var gender = rowObject.Sex;
        if (gender == 1) {
            return "Male";
        }
        else if (gender == 0) {
            return "";
        }
        else {
            return "Female";
        }


    }
}


function serviceImageFormatter(cellValue, options, rowObject) {


    if (rowObject !== undefined) {
        var image1 = rowObject.image1;
        var image2 = rowObject.image2;
        var image3 = rowObject.image3;
        if (image3 == "" && image2 == "" && image1 == "") {
            return "No Image Uploaded";
        }
        else {

            return "<a href='javascript:void(0);' data-id='" + rowObject.id + "' class='getImagesByTable' data-tablename='tenant_service_animal'>Service Image/Photo</a>";
        }

    }


}


$(document).on("click", ".vehicle_main .addbtn", function () {


    $(".vehicle_main form").fadeIn();
    $('.vehicle_action').val('add');

});


$(document).on("click", ".vehicle_main .cancelbtn", function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});


$(document).on("click", ".file_main .cancelbtn", function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});


$(document).on("change", ".vehicle_main table .select_options", function () {
   
   
    var action = $(this).val();
    var id = $(this).attr('data_id');
    if (action == "Edit") {

        $(".vehicle_main form").fadeIn();
        $('.vehicle_action').val('edit');
        $('.vehicle_id').val(id);


        $.ajax({
            url: '/editTenant?action=getVehicleInfo&class=EditTenant&id=' + id,
            type: 'GET',
            success: function (data) {

                var vehicleInfo = JSON.parse(data);
                $("#vehicle_type").val(vehicleInfo.type);
                $("#vehicle_make").val(vehicleInfo.make);
                $("#vevicle_license").val(vehicleInfo.license);
                $("#vevicle_year").val(vehicleInfo.year);
                $("#vehicle_vin").val(vehicleInfo.vin);
                $("#vehicle_registration").val(vehicleInfo.registration);
                $("#vehicle_image1").html(vehicleInfo.photo1);
                $("#vehicle_image2").html(vehicleInfo.photo2);
                $("#vehicle_image3").html(vehicleInfo.photo3);
                $("#vehicle_color").val(vehicleInfo.color);
                 default_form_data = $("#addEditVehicle").serializeArray();

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    else if (action == "Delete") {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    deleteTableData('tenant_vehicles', id);
                    $('#TenantVehicle-table').trigger('reloadGrid');

                }

            }
        });


    }
});


$(document).on("change", ".files_main table .select_options", function () {
    var action = $(this).val();
    var id = $(this).attr('data_id');
    var elem = $(this);
    if (action == "Delete") {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    deleteTableData('tenant_chargefiles', id);
                    setTimeout(function () {
                        $('#TenantFiles-table').trigger('reloadGrid');

                    }, 400);

                }

            }
        });


    }

    if (action == "Email") {
        var tenantId = $("input[name='tenant_id']").val();
        file_upload_email('users','email',id,tenantId);
        elem.val("default");
    }

});

$("#sendEmailTenant").validate({
    rules: {
        to: {
            required:true
        }
    },
    submitHandler: function (e) {
        var tenant_id = $(".tenant_id").val();
        var form = $('#sendEmailTenant')[0];
        var formData = new FormData(form);
        var path = $(".attachments").attr('href');
        /*  alert(path);*/
        var to = $(".to").val();
        formData.append('to_users',to);
        formData.append('action','sendFileLibraryattachEmail');
        formData.append('class','TenantAjax');
        formData.append('path', path);
        $.ajax({
            url: '/Tenantlisting/getInitialData',
            type: 'POST',
            data: formData,
            success: function (data) {
                info =  JSON.parse(data);
                if(info.status=="success"){
                    toastr.success("Email has been sent successfully");
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});



$(document).on("click", ".complaint-btn", function () {

    $("#addEditComplaint").fadeIn();
    $('.complaint_action').val('add');
    $('.complaint-btn').hide();

});


$(document).on("change", ".complaint_main table .select_options", function () {
    var action = $(this).val();
    var id = $(this).attr('data_id');
    $(".complaint_unique_id").val(id);
    if (action == "Edit") {

        $.ajax({
            url: '/editTenant?action=getComplaintInfo&class=EditTenant&id=' + id,
            type: 'GET',
            success: function (data) {
                var info = JSON.parse(data);
                $("#complaint_id").val(info.complaint_id);
                $("#complaint_type").val(info.type);
                $("#complaint_date").val(info.date);
                $("#complaint_note").val(info.notes);
                default_form_data = $("#addEditComplaint").serializeArray();
                console.log(info.updated);
                console.log('11111');
                setTimeout(function () {
                    console.log(info.updated);
                    console.log('222222');
                    edit_date_time(info.updated);

                },2000);
            },
            cache: false,
            contentType: false,
            processData: false
        });
        $(".complaint_main form").fadeIn();
        $('.complaint_action').val('edit');
    }
    else if (action == "Delete") {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    deleteTableData('tenant_medical_allergies', id);
                    $('#TenantMedical-table').trigger('reloadGrid');

                }

            }
        });


    }

});


$(document).on('click', '#add_libraray_file', function () {
    $('#file_library').val('');
    $('#file_library').trigger('click');
});

var file_library = [];
$(document).on('change', '#file_library', function () {
    file_library = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if (size > 1030) {
            toastr.warning('Please select documents less than 1 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
            if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                file_library.push(value);
                var src = '';
                var reader = new FileReader();
                $('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                    $("#file_library_uploads").append(
                        '<div class="row" style="margin:20px">' +
                        '<div class="col-sm-12 img-upload-library-div">' +
                        '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '></div>' +
                        '<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                        '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                        '<div class="col-sm-3 show-library-list-imgs-size' + key + '">' + size + '</div>' +
                        '<div class="col-sm-3"><span id=' + key + ' class="delete_pro_img cursor"><button class="btn-warning">Delete</button></span></div></div></div>');
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});


$('#saveLibraryFiles').on('click', function () {
    var length = $('#file_library_uploads > div').length;
    if (length > 0) {
        var data = convertSerializeDatatoArray();
        var uploadform = new FormData();
        uploadform.append('class', 'propertyFilelibrary');
        uploadform.append('action', 'file_library');
        uploadform.append('property_id', property_unique_id);
        var count = file_library.length;
        $.each(file_library, function (key, value) {
            if (compareArray(value, data) == 'true') {
                uploadform.append(key, value);
            }
            if (key + 1 === count) {
                saveLibraryFiles(uploadform);
            }
        });
    } else {

    }
});

$(document).on('click', '.delete_pro_img', function () {
    $(this).parent().parent().parent('.row').remove();
});

$(document).on('click', '#remove_library_file', function () {
    $('#file_library_uploads').html('');
    $('#file_library').val('');
});

function saveLibraryFiles(uploadform) {
    $.ajax({
        type: 'post',
        url: '/property/file_library',
        data: uploadform,
        processData: false,
        contentType: false,
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $('#file_library_uploads').html('');
                $('#propertFileLibrary-table').trigger('reloadGrid');
                toastr.success('Files uploaded successfully.');
            } else if (response.code == 500) {
                toastr.warning(response.message);
            } else {
                toastr.success('Error while uploading files.');
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function convertSerializeDatatoArray() {
    var newData = [];
    $(".fileLibraryInput").each(function (index) {
        var name = $(this).val();
        var size = $(this).attr('data_id');
        newData.push({'name': name, 'size': size});
    });
    return newData;
}

function compareArray(data, compare) {
    for (var i = 0; i < compare.length; i++) {
        if (compare[i].name == data['name'] && compare[i].size == data['size']) {
            return 'true';
        }
    }
    return 'false';
}

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas = [];
    formulas['k'] = (bytes / 1024).toFixed(1);
    formulas['M'] = (bytes / 1048576).toFixed(1);
    formulas['G'] = (bytes / 1073741824).toFixed(1);
    return formulas[to];
}


$(document).on("click", ".saveChargeFile", function () {
    var tenant_id = $('.tenant_id').val();
    var form = $('#addChargeNote')[0];
    var formData = new FormData(form);
    formData.append('action', 'insertChargeFileNote');
    formData.append('class', 'tenantAjax');
    formData.append('tenant_id', tenant_id);

    $.ajax({
        url: '/tenantAjax',
        type: 'POST',
        data: formData,
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == "success") {
                toastr.success(response.message);
                setTimeout(function () {
                    $('#TenantFiles-table').trigger('reloadGrid');
                    $('#remove_library_file').trigger('click');


                }, 400);

            }
        },
        cache: false,
        contentType: false,
        processData: false
    });

});


function deleteTableData(tablename, id) {

    $.ajax({
        url: '/editTenant?action=deleteRecords&class=EditTenant&id=' + id + '&tablename=' + tablename,
        type: 'GET',
        success: function (data) {
            var info = JSON.parse(data);
            toastr.success(info.message);

        },
        cache: false,
        contentType: false,
        processData: false
    });

}


function isEmpty(mainArr, removeKeyArr) {

    for (var i = 0; i < mainArr.length; i++) {
        if (mainArr[i].key == "key1") {

        }
    }
}

//add propertySetup form client side validations
$("#addEditVehicle").validate({
    rules: {},
    submitHandler: function (e) {

        var form = $('#addEditVehicle')[0];
        var formData = new FormData(form);

        var vehicle_type = $("#vehicle_type").val();
        var vehicle_make = $("#vehicle_make").val();
        var vevicle_license = $("#vevicle_license").val();
        var vehicle_color = $("#vehicle_color").val();
        var vevicle_year = $("#vevicle_year").val();
        var vehicle_vin = $("#vehicle_vin").val();
        var vehicle_registration = $("#vehicle_registration").val();

        if (vehicle_type == "" && vehicle_make == "" && vevicle_license == "" && vehicle_color == "" && vevicle_year == "" && vehicle_vin == "" && vehicle_registration == "") {
            toastr.warning('Please enter data for atleast one field!');
            return false;
        }

        var tenant_id = $(".tenant_id").val();

        var vehicle_image1 = $('#vehicle_image1').html();
        var vehicleimg1 = JSON.stringify(vehicle_image1)
        formData.append('vehicle_image1', vehicleimg1);

        var vehicle_image2 = $('#vehicle_image2').html();
        var vehicleimg2 = JSON.stringify(vehicle_image2)
        formData.append('vehicle_image2', vehicleimg2);

        var vehicle_image3 = $('#vehicle_image3').html();
        var vehicleimg3 = JSON.stringify(vehicle_image3)
        formData.append('vehicle_image3', vehicleimg3);
        formData.append('tenant_id', tenant_id);
        formData.append('action', 'addEditVehicle');
        formData.append('class', 'EditTenant');


        $.ajax({
            url: '/editTenant',
            type: 'POST',
            data: formData,
            success: function (data) {
                info = JSON.parse(data);
                if (info.status == "success") {
                    $('#TenantVehicle-table').trigger('reloadGrid');
                    toastr.success(info.message);
                    var curdAction = $(".vehicle_action").val();
                    if (curdAction == 'add') {
                        $("#addEditVehicle").find("input[type='text']").val('');
                    }
                    $("#addEditVehicle").trigger('reset');
                    setTimeout(function () {
                        jQuery('#TenantVehicle-table').find('tr:eq(1)').find('td:eq(1)').addClass("green_row_left");
                        jQuery('#TenantVehicle-table').find('tr:eq(1)').find('td:eq(12)').addClass("green_row_right");
                    },500);

                }else{
                    toastr.warning(info.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


$(document).on("click", ".pet_main .addbtn", function () {

    $(".pet_main form").fadeIn();
    $('.pet_action').val('add');

});


$(document).on("click", ".pet_main .cancelbtn", function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});

$(document).on("change", ".pet_birth", function () {
    var petBirht = $(this).val();
    var dateObject = $.datepicker.parseDate('mm/dd/yy', petBirht);
    var newdate = dateObject.getFullYear() + "," + (dateObject.getMonth() + 1) + "," + dateObject.getDate();
    var past = new Date(newdate);
    var years = calcDate(past);
    if (years == 0) {
        $("#pet_age").val('1');
    } else if (years > 0) {
        $("#pet_age").val(years);
    } else {
        $("#pet_age").val('1');
    }
});

$(document).on("change", "#service_birth", function () {
    var petBirht = $(this).val();
    var dateObject = $.datepicker.parseDate('mm/dd/yy', petBirht);
    var newdate = dateObject.getFullYear() + "," + (dateObject.getMonth() + 1) + "," + dateObject.getDate();
    var past = new Date(newdate);
    var years = calcDate(past);
    if (years == 0) {
        $("#service_year").val('1');
    } else if (years > 0) {
        $("#service_year").val(years);
    } else {
        $("#service_year").val('1');
    }
});


$(document).on("change", ".pet_main table .select_options", function () {


    var id = $(this).attr('data_id');
    $('.pet_unique_id').val(id);
    var action = $(this).val();
    if (action == "Edit") {

        $.ajax({
            url: '/editTenant?action=getPetInfo&class=EditTenant&id=' + id,
            type: 'GET',
            success: function (data) {
                var petInfo = JSON.parse(data);

                $("#pet_name").val(petInfo.type);
                $("#pet_id").val(petInfo.chip_id);
                $("#pet_type").val(petInfo.type);
                $("#pet_age").val(petInfo.age);
                $("#pet_gender").val(petInfo.gender);
                $("#pet_weight").val(petInfo.weight);
                $("#pet_weight_unit").val(petInfo.weight_unit);
                $("#pet_note").val(petInfo.note);
                $("#pet_color").val(petInfo.color);
                $("#pet_chipid").val(petInfo.chip_id);
                $("#pet_vet").val(petInfo.hospital_name);
                $("#pet_countryCode").val(petInfo.country_code);
                $("#pet_phoneNumber").val(petInfo.phone_number);


                $(".pet_birth").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: petInfo.dob
                });


                $(".pet_lastVisit").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: petInfo.last_visit
                });


                $(".pet_expiration_date").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: petInfo.shots_expire_date
                });

                $(".pet_nextVisit").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: petInfo.next_visit
                });


                $("#pet_medical_condition_note").val(petInfo.medical_condition_note);


                $(".pet_date_given").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: petInfo.rabies_given_date
                });

                // $(".pet_expiration_date").val(petInfo.shots_expire_date);

                $(".pet_expiration_date").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: petInfo.shots_expire_date
                });

                $(".pet_follow_up").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: petInfo.shots_followup_date
                });

                $("#shot_pet_note").val(petInfo.shots_note);
                $("#pet_image1").html(petInfo.image1);
                $("#pet_image2").html(petInfo.image2);
                $("#pet_image3").html(petInfo.image3);
                 default_form_data = $("#addEditPet").serializeArray();
                setTimeout(function () {
                    edit_date_time(petInfo.updated_at);
                },2000);
            },
            cache: false,
            contentType: false,
            processData: false
        });


        $(".pet_main form").fadeIn();
        $('.pet_action').val('edit');
    }
    else if (action == "Delete") {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    deleteTableData('tenant_pet', id);
                    $('#TenantPet-table').trigger('reloadGrid');

                }

            }
        });


    }

});


$("#addEditPet").validate({
    rules: {},
    submitHandler: function (e) {

        var form = $('#addEditPet')[0];
        var formData = new FormData(form);
        var tenant_id = $(".tenant_id").val();
        formData.append('tenant_id', tenant_id);


        var pet_image1 = $('#pet_image1').html();

        var petimg1 = JSON.stringify(pet_image1);

        formData.append('pet_image1', petimg1);

        var pet_image2 = $('#pet_image2').html();
        var petimg2 = JSON.stringify(pet_image2);
        formData.append('pet_image2', petimg2);

        var pet_image3 = $('#pet_image3').html();
        var petimg3 = JSON.stringify(pet_image3);
        formData.append('pet_image3', petimg3);


        formData.append('action', 'addEditPet');
        formData.append('class', 'EditTenant');


        $.ajax({
            url: '/editTenant',
            type: 'POST',
            data: formData,
            success: function (data) {
                info = JSON.parse(data);
                if (info.status == "success") {
                    $('#TenantPet-table').trigger('reloadGrid');
                    toastr.success(info.message);
                    var curdAction = $(".pet_action").val();
                    if (curdAction == 'add') {
                        $("#addEditPet").find("#pet_name,#pet_id,#pet_weight,#pet_vet,#pet_phoneNumber,#pet_chipid,#pet_color,#pet_type").val('');
                    }
                }

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


$(document).on("click", ".service_main .addbtn", function () {

    $(".service_main form").fadeIn();
    $('.service_action').val('add');

});


$(document).on("click", ".service_main .cancelbtn", function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});


$(document).on("change", ".service_main table .select_options", function () {
    var action = $(this).val();
    var id = $(this).attr('data_id');
    $('.service_unique_id').val(id);
    if (action == "Edit") {

        var action = $(this).val();
        $.ajax({
            url: '/editTenant?action=getAnimalInfo&class=EditTenant&id=' + id,
            type: 'GET',
            success: function (data) {


                var serviceInfo = JSON.parse(data);
                $("#service_name").val(serviceInfo.name);
                $("#service_id").val(serviceInfo.animal_id);
                $("#service_type").val(serviceInfo.type);
                $(".service_birth").val(serviceInfo.dob);
                $("#service_year").val(serviceInfo.age);
                $("#service_gender").val(serviceInfo.gender);
                $("#service_weight").val(serviceInfo.weight);
                $("#service_weight_unit").val(serviceInfo.weight_unit);
                $("#service_note").html(serviceInfo.note);
                $("#service_color").val(serviceInfo.color);
                $("#service_chipid").val(serviceInfo.chip_id);
                $("#service_vet").val(serviceInfo.hospital_name);
                if (serviceInfo.country_code != null) {
                    $("#service_countryCode").val(serviceInfo.country_code);
                } else {
                    $("#service_countryCode").val('220');
                }

                $("#service_phoneNumber").val(serviceInfo.phone_number);


                $(".service_lastVisit").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: serviceInfo.last_visit
                });


                $(".service_expiration_date").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: serviceInfo.shots_expire_date
                });

                $(".service_nextVisit").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: serviceInfo.next_visit
                });


                $("#service_medical_condition_note").val(serviceInfo.medical_condition_note);


                // $(".pet_expiration_date").val(petInfo.shots_expire_date);

                $(".service_date_given").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: serviceInfo.rabies_given_date
                });

                $(".service_expiration_date").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: serviceInfo.shots_expire_date
                });

                $(".service_follow_up").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: serviceInfo.shots_followup_date
                });

                $("#service_pet_note").val(serviceInfo.shots_note);
                $("#service_image1").html(serviceInfo.image1);
                $("#service_image2").html(serviceInfo.image2);
                $("#service_image3").html(serviceInfo.image3);
                default_form_data = $("#addEditService").serializeArray();
                
                /*     $.each(tenantInfo, function (key, value) {
           if(value!="")
                  {
         $('.' + key).val(value);
                  }
               });*/
            },
            cache: false,
            contentType: false,
            processData: false
        });

        $(".service_main form").fadeIn();
        $('.service_action').val('edit');
    }
    else if (action == "Delete") {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    deleteTableData('tenant_service_animal', id);
                    $('#TenantService-table').trigger('reloadGrid');

                }

            }
        });


    }

});


$("#addEditService").validate({
    rules: {},
    submitHandler: function (e) {

        var form = $('#addEditService')[0];
        var formData = new FormData(form);
        var tenant_id = $(".tenant_id").val();
        formData.append('tenant_id', tenant_id);


        var service_image1 = $('#service_image1').html();
        var serviceimg1 = JSON.stringify(service_image1);
        formData.append('service_image1', serviceimg1);


        var service_image2 = $('#service_image2').html();
        var serviceimg2 = JSON.stringify(service_image2);
        formData.append('service_image2', serviceimg2);


        var service_image3 = $('#service_image3').html();
        var serviceimg3 = JSON.stringify(service_image3);
        formData.append('service_image3', serviceimg3);


        formData.append('action', 'addEditService');
        formData.append('class', 'EditTenant');


        $.ajax({
            url: '/editTenant',
            type: 'POST',
            data: formData,
            success: function (data) {
                var info = JSON.parse(data);
                if (info.status == "success") {
                    $('#TenantService-table').trigger('reloadGrid');
                    toastr.success(info.message);
                    var curdAction = $(".service_action").val();
                    if (curdAction == 'add') {
                        $("#addEditService").find("input[type='text']").val('');
                    }
                }

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


$(document).on("click", ".medical_main .addbtn", function () {

    $(".medical_main form").fadeIn();
    $('.medical_action').val('add');

});


$(document).on("click", ".medical_main .cancelbtn", function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});

$(document).on("change", ".medical_main table .select_options", function () {
    var action = $(this).val();
    var id = $(this).attr('data_id');
    $(".medical_id").val(id);
    if (action == "Edit") {

        $.ajax({
            url: '/editTenant?action=getMedicalInfo&class=EditTenant&id=' + id,
            type: 'GET',
            success: function (data) {
                var medicalInfo = JSON.parse(data);
                $("#medical_issue").val(medicalInfo.allergy);
                $("#medical_note").val(medicalInfo.note);
                $(".medical_date").datepicker({
                    dateFormat: jsDateFomat,
                    setDate: medicalInfo.date
                });
                 default_form_data = $("#addEditMedical").serializeArray();

            },
            cache: false,
            contentType: false,
            processData: false
        });
        $(".medical_main form").fadeIn();
        $('.medical_action').val('edit');
    }
    else if (action == "Delete") {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    deleteTableData('tenant_medical_allergies', id);
                    $('#TenantMedical-table').trigger('reloadGrid');

                }

            }
        });


    }

});


$("#addEditMedical").validate({
    rules: {},
    submitHandler: function (e) {

        var form = $('#addEditMedical')[0];
        var formData = new FormData(form);
        var tenant_id = $(".tenant_id").val();
        formData.append('tenant_id', tenant_id);

        formData.append('action', 'addEditMedical');
        formData.append('class', 'EditTenant');


        $.ajax({
            url: '/editTenant',
            type: 'POST',
            data: formData,
            success: function (data) {
                var info = JSON.parse(data);
                if (info.status == "success") {
                    $('#TenantMedical-table').trigger('reloadGrid');
                    toastr.success(info.message);
                    var curdAction = $(".medical_action").val();
                    if (curdAction == 'add') {
                        $("#medical_issue").val('');
                        $("#medical_note").val('');

                    }

                }

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


$(document).on("click", ".collection_main .addbtn", function () {

    $(".collection_main form").fadeIn();
    $('.collection_action').val('add');

});


$(document).on("click", ".collection_main .cancelbtn", function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});


$("#addEditCollection").validate({
    rules: {
        medical_issue: {
            required: true
        },

    },
    submitHandler: function (e) {

        var form = $('#addEditCollection')[0];
        var formData = new FormData(form);
        var tenant_id = $(".tenant_id").val();
        formData.append('tenant_id', tenant_id);

        formData.append('action', 'addEditCollection');
        formData.append('class', 'EditTenant');


        $.ajax({
            url: '/editTenant',
            type: 'POST',
            data: formData,
            success: function (data) {
                $('#TenantCollection-table').trigger('reloadGrid');
                toastr.success(info.message);


            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


$(document).on("change", ".collection_main table .select_options", function () {
    var action = $(this).val();
    var id = $(this).attr('data_id');
    $(".collection_id").val(id);
    if (action == "Edit") {

        $.ajax({
            url: '/editTenant?action=getCollectionInfo&class=EditTenant&id=' + id,
            type: 'GET',
            success: function (data) {

                var collectionInfo = JSON.parse(data);
                $("#collection_collectionId").val(collectionInfo.collection_id);
                $("#collection_reason").val(collectionInfo.reason);
                $("#collection_description").val(collectionInfo.description);
                $("#collection_status").val(collectionInfo.status);
                $("#collection_amountDue").val(collectionInfo.amount_due);
                $("#collection_note").val(collectionInfo.notes);
                 default_form_data = $("#addEditCollection").serializeArray();

            },
            cache: false,
            contentType: false,
            processData: false
        });
        $(".collection_main form").fadeIn();
        $('.collection_action').val('edit');
    }
    else if (action == "Delete") {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    deleteTableData('tenant_collection', id);
                    $('#TenantCollection-table').trigger('reloadGrid');
                }

            }
        });


    }

});


$(document).on("click", ".parking_main .addbtn", function () {

    $(".parking_main form").fadeIn();
    $('.parking_action').val('add');

});


$(document).on("click", ".parking_main .cancelbtn", function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});

$(document).on("change", ".parking_main table .select_options", function () {
    var action = $(this).val();
    var id = $(this).attr('data_id');
    $(".parking_id").val(id);


    if (action == "Edit") {
        $.ajax({
            url: '/editTenant?action=getParkingInfo&class=EditTenant&id=' + id,
            type: 'GET',
            success: function (data) {
                var parkingInfo = JSON.parse(data);
                $("#parking_space").val(parkingInfo.space_number);
                $("#parking_number").val(parkingInfo.permit_number);
                 default_form_data = $("#addEditParking").serializeArray();
            },
            cache: false,
            contentType: false,
            processData: false
        });
        $(".parking_main form").fadeIn();
        $('.parking_action').val('edit');
    }
    else if (action == "Delete") {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    deleteTableData('tenant_medical_allergies', id);
                    $('#TenantMedical-table').trigger('reloadGrid');
                }

            }
        });


    }

});


$("#addEditParking").validate({
    rules: {},
    submitHandler: function (e) {

        var form = $('#addEditParking')[0];
        var formData = new FormData(form);
        var tenant_id = $(".tenant_id").val();
        formData.append('tenant_id', tenant_id);

        formData.append('action', 'addEditParking');
        formData.append('class', 'EditTenant');


        $.ajax({
            url: '/editTenant',
            type: 'POST',
            data: formData,
            success: function (data) {
                var parkingInfo = JSON.parse(data);
                if (parkingInfo.status == 'success') {
                    toastr.success(parkingInfo.message);
                }


            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


$(document).on("click", ".main_guarantor .addbtn", function () {

    $(".main_guarantor form").fadeIn();
    $('.main_guarantor_action').val('add');

});


$(document).on("click", ".main_guarantor .cancelbtn", function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});


$(document).on("change", ".main_guarantor table .select_options", function () {
    var action = $(this).val();
    var id = $(this).attr('data_id');
    var tenant_id = $(".tenant_id").val();
    $(".guarantor_id").val(id);
    if (action == "Edit") {
        $.ajax({
            url: '/editTenant?action=getGuarantorInfo&class=EditTenant&id=' + id + '&tenant_id=' + tenant_id,
            type: 'GET',
            success: function (data) {


                var info = JSON.parse(data);
                /*guarantor form1*/
                $("#guarantor_salutation").val(info.guarantor_info.salutation);
                $("#guarantor_firstname").val(info.guarantor_info.first_name);
                $("#guarantor_middlename").val(info.guarantor_info.middle_name);
                $("#guarantor_lastname").val(info.guarantor_info.last_name);
                $("#guarantor_relationship").val(info.guarantor_info.relationship);
                $("#guarantor_zipcode").val(info.guarantor_info.zip_code);
                $("#guarantor_country").val(info.guarantor_info.country);
                $("#guarantor_province").val(info.guarantor_info.state);
                $("#guarantor_city").val(info.guarantor_info.city);
                $("#guarantor_address1").val(info.guarantor_info.address1);
                $("#guarantor_address2").val(info.guarantor_info.address2);
                $("#guarantor_address3").val(info.guarantor_info.address3);
                $("#guarantor_address4").val(info.guarantor_info.address4);
                $("#guarantor_guarantee").val(info.guarantor_info.guarantee_years);
                $("#guarantor_note").val(info.guarantor_info.note);
                $("#guarantor_phoneInfo").html(info.phone_info);
                $("#guarantor_generalEmails").html(info.generalEmails);


                /*guarantor form2*/
                $("#guarantor_form2_entity").val(info.guarantor_info.company_name);
                $("#guarantor_form2_postalcode").val(info.guarantor_info.zip_code);
                $("#guarantor_form2_country").val(info.guarantor_info.country);
                $("#guarantor_form2_province").val(info.guarantor_info.state);
                $("#guarantor_form2_city").val(info.guarantor_info.city);
                $("#guarantor_form2_address1").val(info.guarantor_info.address1);
                $("#guarantor_form2_address2").val(info.guarantor_info.address2);
                $("#guarantor_form2_address3").val(info.guarantor_info.address3);
                $("#guarantor_form2_address4").val(info.guarantor_info.address4);
                $("#guarantor_form2_phoneInfo").html(info.phone_info_form2);
                $("#guarantor_form2_relationship").val(info.guarantor_info.relationship);
                $("#guarantor_form2_fid").val(info.guarantor_info.entity_fid_number);
                $("#guarantor_form2_relationship").val(info.guarantor_info.relationship);
                $("#guarantor_form2_mainContact").val(info.guarantor_info.first_name);
                $("#guarantor_form2_middlename").val(info.guarantor_info.middle_name);
                $("#guarantor_form2_lastname").val(info.guarantor_info.last_name);
                $("#guarantor_generalEmailForm2").html(info.generalEmailsForm2);
                 default_form_data = $("#addEditGuarantor").serializeArray();

            },
            cache: false,
            contentType: false,
            processData: false
        });

        $(".main_guarantor form").fadeIn();
        $('.main_guarantor_action').val('edit');
    }
    else if (action == "Delete") {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    deleteTableData('tenant_guarantor', id);
                    setTimeout(function () {
                        $('#TenantGuarantor-table').trigger('reloadGrid');

                    }, 400);

                    var info = JSON.parse(result);
                    toastr.success(info.message);


                }

            }
        });


    }

});


$("#addEditGuarantor").validate({
    rules: {
        guarantor_salutation: {
            required: true
        },

    },
    submitHandler: function (e) {
        var tenant_id = $(".tenant_id").val();
        var form = $('#addEditGuarantor')[0];
        var formData = new FormData(form);
        var tenant_id = $(".tenant_id").val();
        formData.append('tenant_id', tenant_id);

        if ($('.guarantor_entity').is(':checked')) {

            action = "addEditGuarantorForm2";
        }
        else {

            action = "addEditGuarantorForm1";
        }

        formData.append('action', action);
        formData.append('class', 'EditTenant');


        $.ajax({
            url: '/editTenant',
            type: 'POST',
            data: formData,
            success: function (data) {

                var info = JSON.parse(data);
                toastr.success(info.message);
                $('#TenantGuarantor-table').trigger('reloadGrid');

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});


$("#addChargeForm").validate({
    rules: {
        chargeCode: {
            required: true
        },
        frequency: {
            required: true
        },
        amount: {
            required: true,
        },
        startDate: {
            required: true
        },
        endDate: {
            required: true
        },

    },
    submitHandler: function (e) {

        var tenant_id = $('.tenant_id').val();


        var form = $('#addChargeForm')[0];
        var formData = new FormData(form);
        formData.append('action', 'insertChargeFormData');
        formData.append('class', 'EditTenant');
        formData.append('tenant_id', tenant_id);
        action = 'insertCharge';
        $.ajax({

            url: '/editTenant',
            type: 'POST',
            data: formData,
            success: function (data) {


                toastr.success("Data Added Successfully");
                $("#amount").val('');
                $('.chargeData').html(data);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }


});


//add propertySetup form client side validations
$("#addEditComplaint").validate({
    rules: {
        complaint_id: {
            required: true
        },

    },
    submitHandler: function (e) {

        var form = $('#addEditComplaint')[0];
        var formData = new FormData(form);
        var tenant_id = $(".tenant_id").val();
       


        formData.append('tenant_id', tenant_id);

        formData.append('action', 'addEditComplaint');
        formData.append('class', 'EditTenant');


        $.ajax({
            url: '/editTenant',
            type: 'POST',
            data: formData,
            success: function () {
                toastr.success("Record Added Successfully");
                var rString = randomString(9, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
                $("#complaint_id").val(rString);
                $("#complaint_note").text('');
                $("#complaint_note").val('');


                $("#addEditComplaint #complaint_type").val('1');
                var currentdate = $.datepicker.formatDate(jsDateFomat, new Date());
                $("#addEditComplaint #complaint_date").val(currentdate);
                $('#TenantComplaint-table').trigger('reloadGrid');

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

$(document).on("click", ".edit_vehicle_button", function () {

    $("#add_vehicle_button").show();
    $(".edit_vehicle_button").hide();


});
$(document).on("click", ".edit_pet_button", function () {

    $("#add_pet_button").show();
    $(".edit_pet_button").hide();


});
$(document).on("click", "#edit_animal_button", function () {

    $("#add_animal_animal").show();
    $("#edit_animal_button").hide();


});
$(document).on("click", ".edit_medical_button", function () {

    $("#add_button_allergies").show();
    $(".edit_medical_button").hide();


});
$(document).on("click", ".edit_guarantor_button", function () {

    $("#add_button_gurantor").show();
    $(".edit_guarantor_button").hide();


});
$(document).on("click", ".edit_button_collection", function () {

    $("#add_collection_button").show();
    $(".edit_button_collection").hide();


});
$(document).on("click", ".edit_button_parking", function () {

    $("#add_button_parking").show();
    $(".edit_button_parking").hide();


});

$("#addLateFee").validate({
    rules: {
        lateFeeCharge: {
            required: true,
            number: true
        },
        gracePeriod: {
            required: true,
            number: true
        },

    },
    submitHandler: function (e) {

        var tenant_id = $(".tenant_id").val();


        var form = $('#addLateFee')[0];
        var formData = new FormData(form);
        formData.append('action', 'insertCharge');
        formData.append('class', 'tenantAjax');
        formData.append('tenant_id', tenant_id);
        action = 'insertCharge';
        $.ajax({

            url: '/tenantAjax',
            type: 'POST',
            data: formData,
            success: function (data) {
                var info = JSON.parse(data);
                $("#addLateFee").find("input[type=text]").val("");
                $("#addLateFee").find("input[type=radio]").prop("checked", false);
                $("#addLateFee").find("input[type=checkbox]").prop("checked", false);
                toastr.success(info.message);
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }


});


$(document).on("click", ".savechargecodepopup", function () {
    var chargeCode = $("#chargecode_popup input[name='charge_code']").val();
    var creditValue = $("#chargecode_popup select[name='credit_account']").val();
    var debitValue = $("#chargecode_popup select[name='debit_account']").val();
    var status = $("#chargecode_popup select[name='status']").val();
    var description = $("#chargecode_popup textarea[name='description']").val();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveChargeCodes',
        data: {
            class: "TenantAjax",
            action: "saveChargeCodes",
            chargeCode: chargeCode,
            creditValue: creditValue,
            debitValue: debitValue,
            status: status,
            description: description
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                $("#chargecode_popup").trigger('reset');
                $("#addchargescode").modal('hide');
                var lastId = data.last_insert_id;
                var option = "<option value='" + lastId + "' selected>" + data.data.charge_code + "</option>";
                $("#addTaxDetails .tax_chargeCode").append(option);
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

});


$("#addTaxDetails").validate({
    rules: {
       /* tax_name: {
            required: true
        },
        tax_type: {
            required: true
        },
        tax_value: {
            required: true,
            number: true
        },
        tax_chargeCode: {
            required: true
        },*/

    },
    submitHandler: function (e) {

        var tenant_id = $(".tenant_id").val();


        var form = $('#addTaxDetails')[0];
        var formData = new FormData(form);
        formData.append('action', 'insertTaxDetails');
        formData.append('class', 'tenantAjax');
        formData.append('tenant_id', tenant_id);

        $.ajax({

            url: '/tenantAjax',
            type: 'POST',
            data: formData,
            success: function (data) {
                var info = JSON.parse(data);
                $("#addTaxDetails").find("input[type=text]").val('');
                toastr.success(info.message);


            },
            cache: false,
            contentType: false,
            processData: false
        });
    }


});


function getCharges() {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getCharges',
        data: {
            class: "TenantAjax",
            action: "getCharges"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var charges = data.data.charges;
                if (charges.length > 0) {
                    var chargesOptions = '';
                    for (var i = 0; i < charges.length; i++) {
                        chargesOptions += "<option value='" + charges[i].id + "'>" + charges[i].charge_code + "</option>";
                    }
                    $("#addTaxDetails .tax_chargeCode").html(chargesOptions);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


$(document).on("click", ".taxChargeBtn", function () {
    $(".chargeTaxForm").show();
    $(".chargeTaxDetails").hide();
});


$(document).on("click", ".editAdditionalTenantForm", function (e) {

    e.preventDefault();


    var form = $('#editAdditionalTenantForm')[0];
    var formData = new FormData(form);

    var tenant_id = $(".tenant_id").val();
    var birth = $(".additionalBirth").val();
    var additionalGender = $(".additionalGender").val();
    var additionalLicense = $(".additionalLicense").val();
    var additionalLicense_state = $(".additionalLicense_state").val();
    var keys_signed = $(".keys_signed").val();
    var vehicle = $("input[name='vehicle']:checked").val();
    var smoker = $("input[name='smoker']:checked").val();
    var pet = $("input[name='pet']:checked").val();
    var medical = $("input[name='medical']:checked").val();


    formData.append('tenant_id', tenant_id);

    formData.append('action', 'updateAdditionalInfoTenant');
    formData.append('class', 'EditTenant');
    formData.append('birth', birth);
    formData.append('additionalGender', additionalGender);
    formData.append('additionalLicense', additionalLicense);
    formData.append('additionalLicense_state', additionalLicense_state);
    formData.append('keys_signed', keys_signed);
    formData.append('vehicle', vehicle);
    formData.append('smoker', smoker);
    formData.append('pet', pet);
    formData.append('medical', medical);


    $.ajax({
        url: '/editTenant',
        type: 'POST',
        data: formData,
        success: function (data) {

            $(".tenant_dob").html(birth);
            var getGender = $(".additionalGender option:selected").html();
            $(".gender").html(getGender);
            $(".movein_key_signed").html(keys_signed);
            $(".license").html(additionalLicense);
            $(".license_state").html(additionalLicense_state);


            if (vehicle == 1) {
                $(".vehicle").html('Yes');
            }
            else {
                $(".vehicle").html('No');
            }

            if (smoker == 1) {
                $(".smoker").html('Yes');
            }
            else {
                $(".smoker").html('No');
            }

            if (pet == 1) {
                $(".pet").html('Yes');
            }
            else {
                $(".pet").html('No');
            }

            if (medical == 1) {
                $(".medical_allergy").html('Yes');
            }
            else {
                $(".medical_allergy").html('No');
            }

            toastr.success('Record updated successfully');

            $(".tenantAdditionalInfo").hide();
            $(".additionalInfoData").show();


        },
        cache: false,
        contentType: false,
        processData: false
    });


});


$(document).on("click", ".edit_additional_button a", function () {

    $(".tenantAdditionalInfo").show();
    $(".additionalInfoData").hide();
});
$(document).on("click", ".edit_vehicle_button", function () {

    $("#add_vehicle_button").show();
    $(".edit_vehicle_button").hide();


});
$(document).on("click", ".edit_pet_button", function () {

    $("#add_pet_button").show();
    $(".edit_pet_button").hide();


});
$(document).on("click", "#edit_animal_button", function () {

    $("#add_animal_animal").show();
    $("#edit_animal_button").hide();


});
$(document).on("click", ".edit_medical_button", function () {

    $("#add_button_allergies").show();
    $(".edit_medical_button").hide();


});
$(document).on("click", ".edit_guarantor_button", function () {

    $("#add_button_gurantor").show();
    $(".edit_guarantor_button").hide();


});
$(document).on("click", ".edit_button_collection", function () {

    $("#add_collection_button").show();
    $(".edit_button_collection").hide();


});
$(document).on("click", ".edit_button_parking", function () {

    $("#add_button_parking").show();
    $(".edit_button_parking").hide();


});



function calcDate(date2) { // birthday is a date
    var ageDifMs = Date.now() - date2.getTime();
    var ageDate = new Date(ageDifMs);
    return ageDate.getUTCFullYear() - 1970;
}


function getAge(cellValue, options, rowObject) {
    if (cellValue !== undefined) {

        var past = new Date(cellValue);
        var ageDifMs = Date.now() - past.getTime();
        var ageDate = new Date(ageDifMs);
        var age = ageDate.getUTCFullYear() - 1970;
        if (age == 0) {
            return 1;
        }
        return age;
    }
}

function updateAddressValues(num1, className) {
    if (num1 != "" && num1 != null) {
        var splitString = num1.split(" ");
        for (var i = 0; i < splitString.length; i++) {
            var blankString = 0;
            if (splitString[i].toLowerCase().indexOf("th") !== -1) {
                blankString = splitString[i].replace("th", "");
                blankString = parseInt(blankString);
                if (!isNaN(blankString)) {
                    var numberTo = numberToOrdinal(blankString);
                    var replaceWord = blankString + "th";
                    var newSuper = num1.replace(replaceWord, numberTo);
                    $("." + className).html(newSuper);
                } else {
                    $("." + className).html(num1);
                }
            } else if (splitString[i].toLowerCase().indexOf("st") !== -1) {
                blankString = splitString[i].replace("st", "");
                blankString = parseInt(blankString);
                if (!isNaN(blankString)) {
                    var numberTo = numberToOrdinal(blankString);
                    var replaceWord = blankString + "st";
                    var newSuper = num1.replace(replaceWord, numberTo);
                    $("." + className).html(newSuper);
                } else {
                    $("." + className).html(num1);
                }
            } else if (splitString[i].toLowerCase().indexOf("nd") !== -1) {
                blankString = splitString[i].replace("nd", "");
                blankString = parseInt(blankString);
                if (!isNaN(blankString)) {
                    var numberTo = numberToOrdinal(blankString);
                    var replaceWord = blankString + "nd";
                    var newSuper = num1.replace(replaceWord, numberTo);
                    $("." + className).html(newSuper);
                } else {
                    $("." + className).html(num1);
                }
            } else if (splitString[i].toLowerCase().indexOf("rd") !== -1) {
                blankString = splitString[i].replace("th", "");
                blankString = parseInt(blankString);
                if (!isNaN(blankString)) {
                    var numberTo = numberToOrdinal(blankString);
                    var replaceWord = blankString + "rd";
                    var newSuper = num1.replace(replaceWord, numberTo);
                    $("." + className).html(newSuper);
                } else {
                    $("." + className).html(num1);
                }
            } else {
                $("." + className).html(num1);
            }
        }
    }
}

function numberToOrdinal(number) {
    if (number < 0 || number > 10001) {
        return false;
    }
    var suffix = '';
    if (number < 20) {
        if (number < 10) {
            if (number == 0) {
                suffix = '';
            } else if (number == 1) {
                suffix = '<sup>st</sup>';
            } else if (number == 2) {
                suffix = '<sup>nd</sup>';
            } else if (number == 3) {
                suffix = '<sup>rd</sup>';
            } else {
                suffix = '<sup>th</sup>';
            }
        } else {
            suffix = '<sup>th</sup>';
        }
    } else {
        var tens = number.toString();
        tens = tens[-2];
        var unit = number.toString();
        unit = unit[-1];
        if (tens == "1") {
            suffix = "<sup>th</sup>";
        } else {
            if (unit == "1") {
                suffix = '<sup>st</sup>';
            } else if (unit == "2") {
                suffix = '<sup>nd</sup>';
            } else if (unit == "3") {
                suffix = '<sup>rd</sup>';
            } else {
                suffix = '<sup>th</sup>';
            }
        }
    }
    return number.toString() + suffix;
}

function sendFileLibraryEmail(tenantId, rowId){
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        async: false,
        data: {
            class: "TenantAjax",
            action: "sendFileLibraryEmail",
            tenantId: tenantId,
            rowId: rowId
        },
        success: function (response) {
            var res = JSON.parse(response);
            if (res.status == 'success' && res.code == 200){
                toastr.success(res.message);
            }
        }
    });
}



$(document).on("click",".resetEditGereral",function(){
       var tab = $(this).attr('data-tab');
       localStorage.setItem("tab", tab);
       location.reload();

});


function goesToTabByReset(tab)
{
  if(tab=='general')
  {
    $(".sectionOneEdit").trigger("click");
  } 

  if(tab=='additionalInfo')
  {
    
    
   $('a[href="#tenant-detail-two"]').trigger('click');
    $(".edit_additional_button a").trigger("click");
  }
    if(tab=='rentDetails')
  {
    
    
   $('a[href="#tenant-detail-twelve"]').trigger('click');
    
  }
  if(tab=="noteHistory")
  {
    $('a[href="#tenant-detail-seven"]').trigger('click'); 
  }

  if(tab=="renterInsurance")
  {
     $('a[href="#tenant-detail-ten"]').trigger('click'); 
  }
  


  

  localStorage.removeItem("tab");


}




$(document).on("click",".resetVehicle",function(){
   
  var defaultIgnoreArray = [];
  var type = $(".vehicle_action").val();
  if(type=='add'){

      resetFormClear('#addEditVehicle',[],'form',false,defaultFormData,defaultIgnoreArray);
  }
  else
  {

     resetEditForm("#addEditVehicle",[],true,default_form_data,[]);
  }


    

});


$(document).on("click",".resetComplaint",function(){
   
  var defaultIgnoreArray = [];
  var type = $(".complaint_action").val();
  if(type=='add'){

      resetFormClear('#addEditComplaint',['complaint_id','complaint_date'],'form',false,defaultFormData,defaultIgnoreArray);
  }
  else
  {

     resetEditForm("#addEditComplaint",[],true,default_form_data,[]);
  }


    

});











$(document).on("click",".resetPet",function(){
   
  var defaultIgnoreArray = [];
  var type = $(".pet_action").val();
  if(type=='add'){

      resetFormClear('#addEditPet',[],'form',false,defaultFormData,[]);
  }
  else
  {

     resetEditForm("#addEditPet",[],true,default_form_data,[]);
  }


    

});






$(document).on("click",".resetService",function(){
   
  var defaultIgnoreArray = [];
  var type = $(".service_action").val();
  if(type=='add'){

      resetFormClear('#addEditService',[],'form',false,defaultFormData,[]);
  }
  else
  {

     resetEditForm("#addEditService",[],true,default_form_data,[]);
  }


    

});




$(document).on("click",".resetMedical",function(){
   
  var defaultIgnoreArray = [];
  var type = $(".main_guarantor_action").val();
  if(type=='add'){

      resetFormClear('#addEditGuarantor',[],'form',false,defaultFormData,[]);
  }
  else
  {

     resetEditForm("#addEditGuarantor",[],true,default_form_data,[]);
  }


    

});





$(document).on("click",".resetGuarantar",function(){
   
  var defaultIgnoreArray = [];
  var type = $(".service_action").val();
  if(type=='add'){

      resetFormClear('#addEditService',[],'form',false,defaultFormData,[]);
  }
  else
  {

     resetEditForm("#addEditService",[],true,default_form_data,[]);
  }


    

});





$(document).on("click",".resetCollection",function(){
   
  var defaultIgnoreArray = [];
  var type = $(".collection_action").val();
  if(type=='add'){

      resetFormClear('#addEditCollection',[],'form',false,defaultFormData,[]);
  }
  else
  {

     resetEditForm("#addEditCollection",[],true,default_form_data,[]);
  }


    

});




$(document).on("click",".resetParking",function(){
   
  var defaultIgnoreArray = [];
  var type = $(".parking_action").val();
  if(type=='add'){

      resetFormClear('#addEditParking',[],'form',false,defaultFormData,[]);
  }
  else
  {

     resetEditForm("#addEditParking",[],true,default_form_data,[]);
  }


    

});




$(document).on("click",".clrCharge",function(){
       bootbox.confirm("Do you want to clear this form?", function (result) {
        if (result == true) {
           var type = $("#amount").val('');
        }

});
   });    






