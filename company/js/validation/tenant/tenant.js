$(document).ready(function(){
setTimeout(function(){

    $(function() {
       $(".pet_date_given").datepicker({
          yearRange: '1919:2030',
            changeMonth: true,
            changeYear: true,
            dateFormat: jsDateFomat,
            onSelect: function(dateText, inst) {
                var givenDate = $(this).val();
                var response = getOneYearDate(givenDate,'pet');

                
            }
        });
    });
    getZipCode('#guarantor_zipcode',$('#guarantor_zipcode').val(),'#guarantor_city','#guarantor_province','#guarantor_country','','');
$(function() {
    $(".pet_rabies_given_date").datepicker({
      yearRange: '1919:2030',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function(dateText, inst) {
          var givenDate = $(this).val();
            var response = getOneYearDate(givenDate,'rabies');
            
        }
    });
});


$(function() {
    $(".service_date_given").datepicker({
      yearRange: '1919:2030',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function(dateText, inst) {
          var givenDate = $(this).val();
            var response = getOneYearDate(givenDate,'service_shot');
            
        }
    });
});


$(function() {
    $(".animal_date_given").datepicker({
      yearRange: '1919:2030',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function(dateText, inst) {
          var givenDate = $(this).val();
            var response = getOneYearDate(givenDate,'service_rabies');
            
        }
    });
});



$(function() {
    $(".pet_lastVisit").datepicker({
      yearRange: '1919:2030',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function(dateText, inst) {
          var givenDate = $(this).val();
            var response = getOneYearDate(givenDate,'pet_nextvisit');
            
        }
    });
});



$(function() {
    $(".service_lastVisit").datepicker({
      yearRange: '1919:2030',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function(dateText, inst) {
          var givenDate = $(this).val();
            var response = getOneYearDate(givenDate,'animal_nextvisit');
            
        }
    });
});



$(function() {
    $(".service_lastVisit").datepicker({
      yearRange: '1919:2030',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        onSelect: function(dateText, inst) {
          var givenDate = $(this).val();
            var response = getOneYearDate(givenDate,'animal_nextvisit');
            
        }
    });
});




 }, 3000);





//add propertySetup form client side validations
$(document).on("click",".savetenant",function(){
   
  $("#addTenant").validate();
})


//add propertySetup form client side validations
$(document).on("click",".saveAdditionalTenant",function(){

  $("#addAdditionalTenant").validate();
});




    $(document).on("click",".rotate-ccw",function(){
     $(this).parents('.image-editor').cropit('rotateCCW'); 
    });


        $(document).on("click",".rotate-cw",function(){
     $(this).parents('.image-editor').cropit('rotateCW'); 
    });


$("#addStep1Form").validate({
    rules: {
        cfirst_name: {
            required:true
        },
        clast_name: {
            required:true
        },
        ccard_number: {
            required:true,
            number:true
        },
        cexpiry_year: {
            required:true
        },
        cexpiry_month: {
            required:true
        },
        ccvv: {
            required:true,
            number:true,
        },
        cCompany: {
            required:true
        },
        cphoneNumber: {
            required:true
        },
        caddress1: {
            required:true
        },
        // caddress2: {
        //     required:true
        // },
        ccity: {
            required:true
        },
        cstate: {
            required:true
        },
        czip_code: {
            required:true
        },
        ccountry: {
            required:true
        }
    },
    submitHandler: function (e) {
        var ccard_number=$("#ccard_number").val();
        var ccvv=$("#ccvv").val();
        var cexpiry_month=$("#cexpiry_month").val();
        var cexpiry_year=$("#cexpiry_year").val();

        $("#hidden_ccard_number").val(ccard_number);
        $("#hidden_ccvv").val(ccvv);
        $("#hidden_cexpiry_month").val(cexpiry_month);
        $("#hidden_cexpiry_year").val(cexpiry_year);

        $("#financial-info").modal("hide");

    }
});

    function onStartDateChange(dateText) {
        var d = $.datepicker.parseDate('mm/dd/yy', dateText);
        var currentDate = myDateFormatter(d);
        var time_tenure = $(".lease_tenure").val();
        var time_duration = parseInt($(".lease_term").val(), 10);
        if (time_tenure == "2") {
            d.setFullYear(d.getFullYear() + time_duration);
            d.setDate(d.getDate() - 1);
        }else{
            d.setMonth(d.getMonth() + time_duration);
            d.setDate(d.getDate() - 1);
        }

        var outdate = myDateFormatter(d);

        $('.move_out_date').datepicker('setDate', outdate);

        $(".end_date").attr("disabled", false);
        $(".end_date").datepicker("setDate", outdate);
        $("#end_date").val(getSlashDateFormat(d));
        $("#move_out_date").val(getSlashDateFormat(d));
        //$(".end_date").attr("disabled", true);

        if ($("#end_date").val() != "") {
            var leaseNoticePeriod = $(".notice_period").val();

            if (leaseNoticePeriod != "") {
                d.setDate(d.getDate() - parseInt(leaseNoticePeriod));
            }else{
                d.setDate(d.getDate());
            }

            var newdate = myDateFormatter(d);
            $(".notice_date").attr("readonly",false);
            //$(".notice_date").val(newdate).attr("readonly",true);
            $(".notice_date").val(newdate);
        }

        $('#start_date').val(dateText);
        $('.start_date').val(currentDate);
    }

$("#addTenant").validate({
    rules: {
        contactTenant: {
            required:".checkifcompany:checked"
        },
        property: {
            required:true
        },
        building: {
            required:true
        },
        unit: {
           required:true
        },
        carrier: {
           required:true
        },
        firstname: {
           required:true
        },
        lastname: {
           required:true
        },
        'email[]': {
            required: true,
            email: true,
        },
        'carrier[]': {
            required: true,
        },
        'phoneNumber[]': {
            required: true,
        },

        additional_firstname: {
            required:true
        }, 
       'additional_email[]': {
            required: true,
            email: true,
        },    
     },
    submitHandler: function (e) {
        var tenant_image = $('.tenant_image').html();
         var tenantImage = JSON.stringify(tenant_image)
        var form = $('#addTenant')[0];
        var formData = new FormData(form);
        formData.append('action','insert');
        formData.append('class','tenantAjax');
        formData.append('tenant_image',tenantImage);
        action = 'insert';

        var custom_field = [];
        $(".custom_field_html input").each(function(){
            var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
            custom_field.push(data);
        });
        var a =  JSON.stringify(custom_field);
        formData.append('custom_field',a);
        var vehicle_image1 = [];
        $(".vehicle_image1").each(function(index) {
            img1 = $(this).html();
            vehicle_image1.push(img1);
        });
        var vehicleimg1 =  JSON.stringify(vehicle_image1);
        formData.append('vehicle_image1',vehicleimg1);
        var vehicle_image2 = [];
        $(".vehicle_image2").each(function(index) {
            img2 = $(this).html();
            vehicle_image2.push(img2);
        });
        var vehicleimg2 =  JSON.stringify(vehicle_image2)
        formData.append('vehicle_image2',vehicleimg2);
        var vehicle_image3 = [];
        $(".vehicle_image3").each(function(index) {
            img3 = $(this).html();
            vehicle_image3.push(img3);
        });
        var vehicleimg3 =  JSON.stringify(vehicle_image3)
        formData.append('vehicle_image3',vehicleimg3);
        var pet_image1 = [];
        $(".pet_image1").each(function(index) {
            img1 = $(this).html();
            pet_image1.push(img1);
        });
        var petimg1 =  JSON.stringify(pet_image1);
        formData.append('pet_image1',petimg1);
        var pet_image2 = [];
        $(".pet_image2").each(function(index) {
            img2 = $(this).html();
            pet_image2.push(img2);
        });
        var petimg2 =  JSON.stringify(pet_image2);
        formData.append('pet_image2',petimg2);
        var pet_image3 = [];
        $(".pet_image3").each(function(index) {
            img3 = $(this).html();
            pet_image3.push(img3);
        });
        var petimg3 =  JSON.stringify(pet_image3);
        formData.append('pet_image3',petimg3);
        var service_image1 = [];
        $(".service_image1").each(function(index) {
            img1 = $(this).html();
            service_image1.push(img1);
        });
        var serviceimg1 =  JSON.stringify(service_image1);
        formData.append('service_image1',serviceimg1);
        var service_image2 = [];
        $(".service_image1").each(function(index) {
            img2 = $(this).html();
            service_image2.push(img2);
        });
        var serviceimg2 =  JSON.stringify(service_image2);
        formData.append('service_image2',serviceimg2);
        var service_image3 = [];
        $(".service_image3").each(function(index) {
            img3 = $(this).html();
            service_image3.push(img3);
        });
        var serviceimg3 =  JSON.stringify(service_image3);
        formData.append('service_image3',serviceimg3);


         formData.append('service_image3',serviceimg3);

          var additionalKey = $(".additionalTenantKey").val();
          formData.append('additional_key',additionalKey);

        $.ajax({
            url:'/tenantAjax',
            type: 'POST',
            data: formData,
            success: function (response) {

                var response = JSON.parse(response);
                if(response.status == "success" && response.table === "all"){
                    var returnId = response.tenant_id;
                    if ($('.addAdditionalTenant').is(':checked')) {
                        addExtraTenantInfo(returnId);
                    }
                    $("input[name='tenant_session_id']").val(returnId);
                    toastr.success(response.message);
                    $(".content-data").html("");
                    $(".content-data").load("/Tenantlisting/getTenantLeasePage");
                    setTimeout(function () {
                        initializeLeasePage();
                    },1000);

                }
                if(response.code == 400 && response.status=== "error"){
                    toastr.warning(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
function initializeLeasePage(){
    var tenantId = $(".tenant_session_id").val();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getRentInfo',
        data: {
            class: "TenantAjax",
            action: "getRentInfo",
            id: tenantId
        },
        success: function (response) {
            var d2 = new Date();
            var month2 = d2.getMonth() +1 ;
            var year2=d2.getFullYear();
            var firstDay = new Date(year2, month2, 1);
            var dd=new Date(firstDay);
            var week=(dd.getDay());
            week=(days[week]).substr(0,3);
            dd= (dd.getMonth()+1)+'/'+dd.getDate()+'/'+dd.getFullYear()+' '+'('+week+'.'+')';
            var data = $.parseJSON(response);
            if (data.status == "success") {
                setTimeout(function () {
                    $("#move_in_date").val(localStorage.getItem("moveInDate"));
                },1000);
                setTimeout(function () {
                    onStartDateChange(dd);
                },1000);
                var rentData = data.data.unit.base_rent;
                var securityData = data.data.unit.security_deposit;
                if(rentData != '' && (rentData.toString().indexOf(".") === -1)) {
                    var bef = rentData.toString().replace(/,/g, '');
                    var value = numberWithCommas(bef) + '.00';
                    setTimeout(function () {
                        $("#rent_amount").val(value);
                        $("#rent_amount").next().show();
                    },1000);
                } else {
                    var bef = rentData.toString().replace(/,/g, '');
                    setTimeout(function () {
                        $("#rent_amount").val(numberWithCommas(bef));
                        $("#rent_amount").next().show();
                    },1000);
                }

                if(securityData != '' && (securityData.toString().indexOf(".") === -1)) {
                    var securityDatabef = securityData.toString().replace(/,/g, '');
                    var securityDatavalue = numberWithCommas(securityDatabef) + '.00';
                    setTimeout(function () {
                        $("#security_amount").val(securityDatavalue);
                        $("#security_amount").next().show();
                    },1000);
                } else {
                    var securityDatabef = securityData.toString().replace(/,/g, '');
                    setTimeout(function () {
                        $("#security_amount").val(numberWithCommas(securityDatabef));
                        $("#security_amount").next().show();
                    },1000);
                }
            }
        }
    });

    $(document).on("blur","#cam_amount, #increase_amount", function () {
        $(this).next().show();
    });

    $('.move_in_date,.move_out_date,.end_date,.notice_date').datepicker({dateFormat: jsDateFomat});
    $(".start_date").datepicker({
        minDate: 0,
        beforeShowDay: function (date) {
            if (date.getDate() == 1) {
                return [true, ''];
            }
            return [false, ''];
        },
        onSelect: function(dateText, instance) {
            onStartDateChange(dateText);
        }
    });

    $(document).on("change",".lease_tenure",function(){
        var value       = $(this).val();
        var time_duration = parseInt($(".lease_term").val(), 10);
        changeTenureTerm(value,time_duration);
    });

    $(document).on("blur",".lease_term",function(){
        var value           = parseInt($(this).val());
        var leaseTenure     = $(".lease_tenure").val();

        var value = $(this).val();
        var notice_period = $('.notice_period').val();
        if (notice_period == "60" && value <= 1 ){
            $(".notice_period,.notice_date").val('');
            bootbox.alert("Notice Period should not greater then lease term 30");
            return false;
        }

        if (notice_period == "90" && value == 2 ){
            $(".notice_period,.notice_date").val('');
            bootbox.alert("Notice Period should not greater then lease term 60");
            return false;
        }

        changeTenureTerm(leaseTenure,value);
    });

    $(document).on("change",".notice_period",function(e){
        e.preventDefault();
        var value = $(this).val();
        var lease_term = $('.lease_term').val();
        if (value == "60" && lease_term <= 1 ){
            bootbox.alert("Notice Period should not greater then lease term 30");
            return false;
        }

        if (value == "90" && lease_term == 2 ){
            bootbox.alert("Notice Period should not greater then lease term 60");
            return false;
        }

        var endDate = $("#end_date").val();
        var d = $.datepicker.parseDate('mm/dd/yy', endDate);
        if (endDate == "") {
            alert("Lease End Date is Blank !");
            $(this).val("");
            return false;
        }else{
            if (value == "30") {
                d.setDate(d.getDate() - 30);
                var newdate = myDateFormatter(d);
                $(".notice_date").val(newdate);
            }else if (value == "60") {
                d.setDate(d.getDate() - 60);
                var newdate = myDateFormatter(d);
                $(".notice_date").val( newdate);
            }else if (value == "90") {
                d.setDate(d.getDate() - 90);
                var newdate = myDateFormatter(d);
                $(".notice_date").val( newdate);
            }else{
                $(".notice_date").val("");
            }
        }
    });

    $('.number_only').keydown(function (e) {
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });
    $('.number_only2').keydown(function (e) {
        if (e.which != 110 && e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });

    $(document).on('focusout','#rent_amount,#cam_amount,#security_amount,#increase_amount,#tax_value',function(){
        var id = this.id;
        if($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
            var bef = $('#' + id).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $('#' + id).val(value);
        } else {
            var bef = $('#' + id).val().replace(/,/g, '');
            $('#' + id).val(numberWithCommas(bef));
        }
    });
        $(document).on('blur','#rent_amount',function () {
        var val=$(this).val();
            val=(Math.round(val * 100) / 100).toFixed(2);
            $("#security_amount").val(val);
            $(this).next().show();
            $("#security_amount").next().show();
        });
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $(document).on("change",".increased_by", function () {
        if ($(this).val() == "flat"){

            $(".label_amount").text("Amount ("+currencySign+")");
        }else{
            $(".label_amount").text("Percentage (%)");
        }
    });

    /* lease form validation */

    $(document).on("click",".save_lease_btn",function(e){
        e.preventDefault();
        var tenatntleaseuserid = $(".tenant_session_id").val();
        if (tenatntleaseuserid == "") {
            tenatntleaseuserid = $("input[name='tenant_session_id']").val();
        }
        $(".lease_user_id").val(tenatntleaseuserid);
        if ($('#save_lease_form').valid()){
            $("#save_lease_form").trigger("submit");
            $(".content-data").html("");
            $(".content-data").load("/Tenantlisting/getTenantChargePage");
        }
        return false;
    });

    $("#save_lease_form").on("submit",function(e){
        e.preventDefault();
        var formData = new FormData(this);
        saveTenantLease(formData);
        return false;
    });

    function onStartDateChange(dateText) {
        var d = $.datepicker.parseDate('mm/dd/yy', dateText);
        var currentDate = myDateFormatter(d);
        var time_tenure = $(".lease_tenure").val();
        var time_duration = parseInt($(".lease_term").val(), 10);
        if (time_tenure == "2") {
            d.setFullYear(d.getFullYear() + time_duration);
            d.setDate(d.getDate() - 1);
        }else{
            d.setMonth(d.getMonth() + time_duration);
            d.setDate(d.getDate() - 1);
        }

        var outdate = myDateFormatter(d);

        $('.move_out_date').datepicker('setDate', outdate);

        $(".end_date").attr("disabled", false);
        $(".end_date").datepicker("setDate", outdate);
        $("#end_date").val(getSlashDateFormat(d));
        $("#move_out_date").val(getSlashDateFormat(d));
        //$(".end_date").attr("disabled", true);

        if ($("#end_date").val() != "") {
            var leaseNoticePeriod = $(".notice_period").val();

            if (leaseNoticePeriod != "") {
                d.setDate(d.getDate() - parseInt(leaseNoticePeriod));
            }else{
                d.setDate(d.getDate());
            }

            var newdate = myDateFormatter(d);
            $(".notice_date").attr("readonly",false);
            //$(".notice_date").val(newdate).attr("readonly",true);
            $(".notice_date").val(newdate);
        }

        $('#start_date').val(dateText);
        $('.start_date').val(currentDate);
    }

    function myDateFormatter(dateObject){
        var newdate = dateObject.getFullYear() + "-" + (dateObject.getMonth() + 1) + "-" + dateObject.getDate();
        var tmp = [];
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getDateFormat',
            data: {class: "TenantAjax",action: "getDateformat",date: newdate,},
            async: false,
            global:false,
            success: function (response) {
                tmp = $.parseJSON(response);
            }
        });
        return tmp;
    }

    function getSlashDateFormat(dateObject){
        var newdate = (dateObject.getMonth() + 1) + "/" + dateObject.getDate() + "/" + dateObject.getFullYear();
        return newdate;
    }

    $("#save_lease_form").validate({
        rules: {
            'move_in_date':{required:true},
            'start_date1':{required:true},
            'lease_term':{required:true},
            'end_date1':{required:true},
            'notice_date':{required: true},
            'rent_amount':{required: true}
        },
        messages: {
            "move_in_date": "Select move in date",
            "start_date1": "Select start date",
            "lease_term": "Select lease term",
            "end_date1": "Enter end date",
            "notice_date": "Enter notice date",
            "rent_amount": "Enter rent amunt",
        }
    });

}


function addExtraTenantInfo(id)
{
        var form = $('#addAdditionalTenantForm')[0];
        var formData = new FormData(form);
        formData.append('action','addAdditionalTenant');
        formData.append('class','tenantAjax');
        formData.append('tenantId',id);
        action = 'addAdditionalTenant';
        $.ajax({
            url:'/tenantAjax',
            type: 'POST',
            data: formData,
            success: function (response) {
               
            },
            cache: false,
            contentType: false,
            processData: false
        });
}

$("#addAdditionalTenantForm").validate({
    rules: {
        additional_firstname: {
            required:true
        },
       'additional_email[]': {
            required: true,
            email: true,
        },
     },
    submitHandler: function (e) {
        var form = $('#addAdditionalTenantForm')[0];
        var formData = new FormData(form);
        formData.append('action','addAdditionalTenant');
        formData.append('class','tenantAjax');
        action = 'addAdditionalTenant';
        $.ajax({
            url:'/tenantAjax',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status=="success")
                {
                 $(".additionalTenantKey").val(response.key);
                 $('#addAdditionalTenantForm').find('input:text').val('');
                 toastr.success('Additional Tenant Added Successfully!');

                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});






/*jquery date picker*/
$(function(){


    $('.move_in_date,.move_out_date').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        yearRange: '-100:+200'
    });
   var date = $.datepicker.formatDate(jsDateFomat, new Date());
   
   //$(".calander").val(date);




   /* $(".calander").datepicker({
        dateFormat: jsDateFomat,
        setDate: $.datepicker.formatDate(jsDateFomat, new Date())
    });*/
});


 $(document).on("click",".primary-tenant-phone-row .fa-plus-circle",function(){
    var clone = $(".primary-tenant-phone-row:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".primary-tenant-phone-row").first().after(clone);
    $(".primary-tenant-phone-row:not(:eq(0))  .fa-plus-circle").hide();
    $(".primary-tenant-phone-row:not(:eq(0))  .fa-minus-circle").show();
    var phoneRowLenght = $("#addTenant .primary-tenant-phone-row");
     clone.find('.phone_format').mask('000-000-0000', {reverse: true});
     $(".ext_phone:not(:eq(0))").hide();
     if (phoneRowLenght.length == 2) {
         clone.find(".fa-plus-circle").hide();
     }else if (phoneRowLenght.length == 3) {
         clone.find(".fa-plus-circle").hide();
         $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
     }else{
        $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
    }
});

$(document).on("click",".primary-tenant-phone-row .fa-minus-circle",function(){

    var phoneRowLenght = $(".primary-tenant-phone-row");
    if (phoneRowLenght.length == 2) {
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").show();
    }else if (phoneRowLenght.length == 3) {
        $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
    }

  $(this).parents(".primary-tenant-phone-row").remove();
});







 $(document).on("click",".primary-tenant-phone-row2 .fa-plus-circle",function(){
    var clone = $(".primary-tenant-phone-row2:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    clone.find('select[name="phoneType[]"]').val('1'); //harjinder

    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
    clone.find('.ext_phone').hide();

    $(".primary-tenant-phone-row2").first().after(clone);
    $(".primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").hide();
    $(".primary-tenant-phone-row2:not(:eq(0)) .fa-minus-circle").show();
    var phoneRowLenght = $("#editTenant .primary-tenant-phone-row2");

     if (phoneRowLenght.length == 2) {
         clone.find(".fa-plus-circle").hide();
         clone.find(".fa-minus-circle").show();
     }else if (phoneRowLenght.length == 3) {
         clone.find(".fa-minus-circle").show();
         $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").hide();
     }else{
        $(".primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").show();
    }
});

$(document).on("click",".primary-tenant-phone-row2 .fa-minus-circle",function(){

    var phoneRowLenght = $(".primary-tenant-phone-row2");
    if (phoneRowLenght.length == 2) {
        $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").show();
    }else if (phoneRowLenght.length == 3) {
        $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").hide();
    }else{
        $(".primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").show();
    }

  $(this).parents(".primary-tenant-phone-row2").remove();
});








/*for multiple email textbox*/
$(document).on("click",".email-plus-sign",function(){
    var emailRowLenght = $(".multipleEmail");
    if (emailRowLenght.length == 2) {
        $(".email-plus-sign .fa-plus-circle").hide();
    }
    var clone = $(".multipleEmail:first").clone();
    clone.find('input[type=text]').val('');
    $(".multipleEmail").first().after(clone);
    $(".multipleEmail:not(:eq(0))  .email-plus-sign .fa-plus-circle").remove();
    $(".multipleEmail:not(:eq(0))  .email-remove-sign,.multipleEmail:not(:eq(0))  .email-remove-sign  .fa-minus-circle").show();

});

/*for multiple email textbox*/


/*Remove Email Textbox*/
  $(document).on("click",".email-remove-sign",function(){
        var emailRowLenght = $(".multipleEmail");
        
        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
           $(".email-plus-sign .fa-plus-circle").show();
        }
        $(this).parents(".multipleEmail").remove();
    });

 /*Remove Email Textbox*/





/*for multiple SSN textbox*/
 $(document).on("click",".ssn-plus-sign",function(){
       var clone = $(".multipleSsn:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".multipleSsn").first().after(clone);
        $(".multipleSsn:not(:eq(0))  .ssn-plus-sign").remove();
        $(".multipleSsn:not(:eq(0))  .ssn-remove-sign").show();
        
    });
 /* for multiple SSN textbox*/



/*remove ssn textbox*/
$(document).on("click",".ssn-remove-sign",function(){
         $(this).parents(".multipleSsn").remove();
    });
/*remove ssn textbox*/



/*Hide Unhide (primary tenant info tab) first html content*/
$(document).on("change",".entity",function(){
if(this.checked)
{
  $('.contactTenant').show(800);
  $('.tenant_hide_row').hide(800);
  
}
else
{
    $('.contactTenant').hide(800);
    $('.tenant_hide_row').show(800);
}

});
/*Hide Unhide (primary tenant info tab) first html content*/


$(document).on("change",".addAdditionalTenant",function(){
if(this.checked)
{
  $('.additionalTenantHtml').show();

}
else
{
    $('.additionalTenantHtml').hide();
}

});








/*show and hide radio button content*/
$(document).on("click",'.select_property_vehicle',function(){
    
 if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.property_vehicle').show();
           }
           else
           {
           $('.property_vehicle').hide();
           }
        }

});


$(document).on("click",'.select_property_pet',function(){

    
 if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.property_pet').show();
           }
           else
           {
           $('.property_pet').hide();
           }
        }

});



$(document).on("click",'.select_property_service',function(){
    
 if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.property_service').show();
           }
           else
           {
           $('.property_service').hide();
           }
        }

});




$(document).on("click",'.select_property_parking',function(){
    if( $(this).is(":checked") ){
        var val = $(this).val();
        if(val=='1'){
            $('.property_parking').show();
        }else{
            $('.property_parking').hide();
        }
    }
});



$(document).on("click",'.select_property_medical',function(){
if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.property_medical').show();
           }
           else
           {
           $('.property_medical').hide();
           }
        }

});

$(document).on("click",'.select_property_collection',function(){

    
if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.property_collection').show();
           }
           else
           {
           $('.property_collection').hide();
           }
        }

});

/*show and hide radio button content*/



/*for small popup*/




    $(".add-popup").hide();
    $(document).on("click",'.tab_alert',function(){
        toastr.warning("Please add select property information first!");

    });


$(document).on("click",'#ethnicity',function(){
 $("#ethnicityPopUp").show();
});

$(document).on("click",'#ethncity_cancel',function(){
 $("#ethnicityPopUp").hide();
 $("#addThncity").val('');
});



$(document).on("click",'#maritalStatus',function(){
 $("#maritalStatusPopUp").show();
 
});

$(document).on("click",'#maritalStatus_cancel',function(){
 $("#maritalStatusPopUp").hide();
$("#addMaritalStatus").val('');
});



$(document).on("click",'#hobbies',function(){
 $("#hobbiesPopUp").show();
});

$(document).on("click",'#hobbies_cancel',function(){
 $("#hobbiesPopUp").hide();
 $("#addNewHobbies").val('');
});



$(document).on("click",'#veteranStatus',function(){
 $("#veteranStatusPopUp").show();
});

$(document).on("click",'#veteranStatus_cancel',function(){
 $("#veteranStatusPopUp").hide();
 $("#addNewVeteranStatus").val('');
});



$(document).on("click",'.credentialType',function(){
 $(this).closest('.apx-inline-popup').find(".credentialTypePopUp").show();
});

$(document).on("click",'.credentialType_cancel',function(){
 $(".credentialTypePopUp").hide();
 $(".addCredentialType").val('');
});




$(document).on("click",'#referralSource',function(){

 $("#referralSourcePopUp").show();
});

$(document).on("click",'#referralSource_cancel',function(){
 $("#referralSourcePopUp").hide();
 $("#addreferralSource").val('');
});




/*for small popup*/


/*For Cloning radio button content*/
 $(document).on("click",".copyVehicle",function(){
    var clone = $(".property_vehicle:first").clone();
    clone.find('input[type=text]').val('');
    clone.find('.vehicle_image1').html(staticImage);
    clone.find('.vehicle_image2').html(staticImage);
    clone.find('.vehicle_image3').html(staticImage);

    clone.find(".image-editor").cropit({
        imageState: {
            src: subdomain_url+'500/400',
        },
    });

    $(".property_vehicle").first().after(clone);

    $(".property_vehicle:not(:eq(0))  .removeVehicle").show();
    $(".property_vehicle:not(:eq(0))  .copyVehicle").remove();

});


  $(document).on("click",".removeVehicle",function(){
        var phoneRowLenght = $(".property_vehicle");
        $(this).parents(".property_vehicle").remove();
    });


 $(document).on("click",".copyPet",function(){
    var clone = $(".property_pet:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
       clone.find(".image-editor").cropit({
        imageState: {
            src: subdomain_url+'500/400',
        },
    });
    clone.find('.pet_image1').html(petStaticImage);
    clone.find('.pet_image2').html(petStaticImage);
    clone.find('.pet_image3').html(petStaticImage);
    $(".property_pet").first().after(clone);
    $(".property_pet:not(:eq(0))  .removePet").show();
    $(".property_pet:not(:eq(0))  .copyPet").remove();

        reIntializeDatepicker(clone);
  
 

   
 });






function reIntializeDatepicker(clone)
{

clone.find(".calander").
removeClass('hasDatepicker').
removeData('datepicker').
unbind().datepicker({
        dateFormat: jsDateFomat,
        setDate: $.datepicker.formatDate(jsDateFomat, new Date()),
        yearRange: '-100:+200'
    });

clone.find(".calander").val(date);

}



  $(document).on("click",".removePet",function(){
        var phoneRowLenght = $(".property_pet");
        $(this).parents(".property_pet").remove();
    });


   $(document).on("click",".copyServiceCompanion",function(){
    var clone = $(".property_service:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
        clone.find(".image-editor").cropit({
        imageState: {
            src: subdomain_url+'500/400',
        },
    });
    clone.find('.service_image1').html(petStaticImage);
    clone.find('.service_image2').html(petStaticImage);
    clone.find('.service_image3').html(petStaticImage);
    $(".property_service").first().after(clone);
    $(".property_service:not(:eq(0))  .removeServiceCompanion").show();
    $(".property_service:not(:eq(0))  .copyServiceCompanion").remove();
 });


  $(document).on("click",".removeServiceCompanion",function(){
        var phoneRowLenght = $(".property_service");
        $(this).parents(".property_service").remove();
    });


  $(document).on("click",".copySpaceNumber",function(){
    var clone = $(".spaceNumber:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".spaceNumber").first().after(clone);
    $(".spaceNumber:not(:eq(0))  .removeSpaceNumber").show();
      $(".spaceNumber:not(:eq(0))  .copySpaceNumber").remove();
 });


  $(document).on("click",".removeSpaceNumber",function(){
        var phoneRowLenght = $(".spaceNumber");
        $(this).parents(".spaceNumber").remove();
    });


    $(document).on("click",".copyMedical",function(){
        var clone = $(".property_medical:first").clone();
        clone.find('input[name="medical_issue[]"]').val(''); //harjinder
        clone.find('textarea').val(''); //harjinder
        $(".property_medical").first().after(clone);
        $(".property_medical:not(:eq(0))  .copyMedical").remove();
        $(".property_medical:not(:eq(0))  .removeMedical").show();
     });


  $(document).on("click",".removeMedical",function(){
        var phoneRowLenght = $(".property_medical");
        $(this).parents(".property_medical").remove();
    });



/*for cloing radio button content*/





/*guarantor starts here*/




$(document).on("click",'.select_property_guarantor',function(){
    
if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.property_guarantor').show();
           }
           else
           {
           $('.property_guarantor').hide();
           }
        }

});




$(document).on("click",'.service_shots',function(){
    
if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.animal_shot_date').show();
           }
           else
           {
           $('.animal_shot_date').hide();
           }
        }

});




$(document).on("click",'.pet_name_shot',function(){
    
if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.pet_shot_date').show();
           }
           else
           {
           $('.pet_shot_date').hide();
           }
        }

});






$(document).on("click",'.pet_rabies',function(){
    
if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.pet_rabies_html').show();
           }
           else
           {
           $('.pet_rabies_html').hide();
           }
        }

});



$(document).on("click",'.animal_rabies',function(){
    
if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.animal_rabies_html').show();
           }
           else
           {
           $('.animal_rabies_html').hide();
           }
        }

});



$(document).on("click",'.pet_medical_condition',function(){
    
if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.pet_medical_html').show();
           }
           else
           {
           $('.pet_medical_html').hide();
           }
        }

});


$(document).on("click",'.service_medical_condition',function(){
    
if( $(this).is(":checked") ){
            var val = $(this).val(); 
           if(val=='1')
           {
            $('.service_medical_html').show();
           }
           else
           {
           $('.service_medical_html').hide();
           }
        }

});











/*guarantor has two form*/
$(document).on("change",".guarantor_entity",function(){
if(this.checked)
{
  $('.property_guarantor_form2').show();
  $('.property_guarantor_forcopy_form2').show();
  $('.property_guarantor_form1').hide();
   $('.property_guarantor_forcopy_form1').hide();
}
else
{
  $('.property_guarantor_form2').hide();
  $('.property_guarantor_forcopy_form2').hide();

  $('.property_guarantor_form1').show();
  $('.property_guarantor_forcopy_form1').show();
}

});





/* guarantor form1 remove*/
    $(document).on("click",".guarantor-remove-sign",function(){
        $(this).parents(".property_guarantor_form1").remove();
    });
 /* guarantor form1 remove*/   

/*guarantor phone row form1 add*/
     $(document).on("click",".guarantor-phonerow-form1-plus-sign",function(){
        var phoneRowLenght = $(".guarantor-form1-phone-row");
        var clone = $(".guarantor-form1-phone-row:first").clone();
         clone.find('input[type=text]').val('');
         clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(this).parents(".guarantor-form1-phone-row").after(clone);
        clone.find(".guarantor-phonerow-form1-plus-sign").hide();
        clone.find(".guarantor-phonerow-form1-remove-sign").show();
    });
/* guarantor phone row form1 add*/


/*phone row form1 remove*/
      $(document).on("click",".guarantor-phonerow-form1-remove-sign",function(){
       
       $(this).parents(".guarantor-form1-phone-row").remove();
        
          });
/*phone row form1 remove*/








  









/*guarantor form2 dynamic clone*/
      $(document).on("click",".guarantor-form2-plus-sign",function(){

       var length = $('.clone_guarantor_form2').length;
      
     
       
    var clone = $(".property_guarantor_forcopy_form2").clone();
     clone.removeClass("property_guarantor_forcopy_form2").addClass("property_guarantor_form2");
        $(".property_guarantor_form2").first().after(clone);
        
       clone.find('.guarantor_phoneType_form2').attr('name','guarantor_phoneType_form2_'+length+'[]'); 
       clone.find('.guarantor_carrier_form2').attr('name','guarantor_carrier_form2_'+length+'[]');
       clone.find('.guarantor_countryCode_form2').attr('name','guarantor_countryCode_form2_'+length+'[]');
       clone.find('.guarantor_phoneNumber_form2').attr('name','guarantor_phoneNumber_form2_'+length+'[]'); 
        clone.find('.guarantor_form2_email').attr('name','guarantor_form2_email_'+length+'[]'); 

       
 });

      /*guarantor form 2 remove*/

   $(document).on("click",".guarantor-form2-remove-sign",function(){
        $(this).parents(".property_guarantor_form2").remove();
    });






         
/*guarantor phone row form 2 add*/
       $(document).on("click",".guarantor-phonerow-form2-plus-sign",function(){
      
        var phoneRowLenght = $(".guarantor-form2-phone-row");

         var clone = $(".guarantor-form2-phone-row:first").clone();

        clone.find('input[type=text]').val(''); 
        $(this).parents(".guarantor-form2-phone-row").after(clone);
          clone.find(".guarantor-phonerow-form2-plus-sign").hide();
        clone.find(".guarantor-phonerow-form2-remove-sign").show();
        
    });



/*guarantor phone row form 2 remove*/
  $(document).on("click",".guarantor-phonerow-form2-remove-sign",function(){
       
       $(this).parents(".guarantor-form2-phone-row").remove();
        
          });


  /*notice period section copy*/
$(document).on("click",".add-notice-period",function(){
    var clone = $(".tenant-credentials-control:first").clone();
    clone.find('input[name="credentialName[]"]').val('');
    clone.find('input[name="noticePeriod[]"]').val('');
    clone.find('select[name="credentialType[]"]').val('1');
    $(".tenant-credentials-control").first().after(clone);
    clone.find(".add-notice-period").hide();
    clone.find(".remove-notice-period, .remove-notice-period i").show();
});


 $(document).on("click",".remove-notice-period",function(){
        $(this).parents(".tenant-credentials-control").remove();
    });


$(document).on("change","#salutation",function(){
   var value = $(this).val();
   if(value=="Mr.")
   {
       $(".maiden_name_hide").hide();
     $("#gender").val("1");
   }
    if(value=="Mrs.")
   {
       $(".maiden_name_hide").show();
     $("#gender").val("2");
   } if(value=="Ms.")
   {
       $(".maiden_name_hide").show();
     $("#gender").val("2");
   } if(value=="Sir")
   {
       $(".maiden_name_hide").hide();
     $("#gender").val("1");
   } if(value=="Madam")
   {
       $(".maiden_name_hide").show();
     $("#gender").val("2");
   } if(value=="Brother")
   {
       $(".maiden_name_hide").hide();
     $("#gender").val("1");
   } if(value=="Sister")
   {
       $(".maiden_name_hide").show();
     $("#gender").val("2");
   } if(value=="Father")
   {
       $(".maiden_name_hide").hide();
     $("#gender").val("1");
   } if(value=="Mother")
   {
       $(".maiden_name_hide").show();
     $("#gender").val("2");
   }
    if(value=="Mr. & Mrs.")
    {
        $(".maiden_name_hide").hide();
        $("#gender").val("");
    }
    if(value=="Dr.")
    {
        $(".maiden_name_hide").hide();
        $("#gender").val("");
    }
    if(value=="Select")
    {
        $(".maiden_name_hide").hide();

    }
});
$(document).on("change","#additional_salutation",function(){
    var value = $(this).val();
    if(value=="Mr.")
    {
        $("#additional_gender").val("1");
    }
    if(value=="Mrs.")
    {
        $("#additional_gender").val("2");
    } if(value=="Ms.")
    {
        $("#additional_gender").val("2");
    } if(value=="Sir")
    {
        $("#additional_gender").val("1");
    } if(value=="Madam")
    {
        $("#additional_gender").val("2");
    } if(value=="Brother")
    {
        $("#additional_gender").val("1");
    } if(value=="Sister")
    {
        $("#additional_gender").val("2");
    } if(value=="Father")
    {
        $("#additional_gender").val("1");
    } if(value=="Mother")
    {
        $("#additional_gender").val("2");
    }
    if(value=="Mr. & Mrs.")
    {
        $("#additional_gender").val("");
    }
    if(value=="Dr.")
    {
        $("#additional_gender").val("");
    }

});





/*ankur*/
  $(document).on("click",".additional_ssn-plus-sign",function(){
       var clone = $(".additional_multipleSsn:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".additional_multipleSsn").first().after(clone);

         clone.find(".additional_ssn-plus-sign").remove();
        $(".additional_multipleSsn:not(:eq(0))  .additional_ssn-remove-sign").show();
        
    });

  $(document).on("click",".additional_ssn-remove-sign",function(){
         $(this).parents(".additional_multipleSsn").remove();
    });

$(document).on("click",'#additional_ethnicity',function(){
 $("#additional_ethnicityPopUp").show();
});

$(document).on("click",'#additional_ethncity_cancel',function(){
 $("#additional_ethnicityPopUp").hide();
 $("#additional_addThncity").val('');
});







$(document).on("click",'#additional_maritalStatus',function(){
 $("#additional_maritalStatusPopUp").show();
 
});

$(document).on("click",'#additional_maritalStatus_cancel',function(){
 $("#additional_maritalStatusPopUp").hide();
$("#additional_addMaritalStatus").val('');
});



$(document).on("click",'#additional_hobbies',function(){
 $("#additional_hobbiesPopUp").show();
});

$(document).on("click",'#additional_hobbies_cancel',function(){
 $("#additional_hobbiesPopUp").hide();
 $("#additional_addNewHobbies").val('');
});



$(document).on("click",'#additional_veteranStatus',function(){
 $("#additional_veteranStatusPopUp").show();
});

$(document).on("click",'#additional_veteranStatus_cancel',function(){
 $("#additional_veteranStatusPopUp").hide();
 $("#additional_addNewVeteranStatus").val('');
});




 $(document).on("click",".additional-phonerow-plus-sign",function(){
         var clone = $(".additional_phone-row:first").clone();
         clone.find('input[type=text]').val('');
         $(".additional_phone-row").first().after(clone);
         $(".additional_phone-row:not(:eq(0))  .fa-plus-circle").hide();
         $(".additional_phone-row:not(:eq(0))  .additional-phonerow-remove-sign .fa-minus-circle").show();
         var phoneRowLenght = $("#addTenant .additional_phone-row");
         clone.find('.phone_format').mask('000-000-0000', {reverse: true});
         if (phoneRowLenght.length == 2) {
             clone.find(".fa-plus-circle").hide();
         }else if (phoneRowLenght.length == 3) {
             clone.find(".fa-plus-circle").hide();
             $(".additional_phone-row:eq(0) .fa-plus-circle").hide();
         }else{
             $(".additional_phone-row:not(:eq(0)) .fa-plus-circle").show();
         }
    });




      $(document).on("click",".additional-phonerow-remove-sign",function(){
          var phoneRowLenght = $(".additional_phone-row");
          if (phoneRowLenght.length == 2) {
              $(".additional_phone-row:eq(0) .fa-plus-circle").show();
          }else if (phoneRowLenght.length == 3) {
              $("additional_phone-row:eq(0) .fa-plus-circle").hide();
          }else{
              $(".additional_phone-row:not(:eq(0)) .fa-plus-circle").show();
          }
            $(this).parents(".additional_phone-row").remove();
        
      });




 $(document).on("click",".additional_email-plus-sign",function(){
        var emailRowLenght = $(".additional_multipleEmail");
        
        if (emailRowLenght.length == 2) {
            $(".additional_email-plus-sign").hide();    
        }else{
            $(".additional_email-plus-sign").show();    
        }
        var clone = $(".additional_multipleEmail:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".additional_multipleEmail").first().after(clone);
        $(".additional_multipleEmail:not(:eq(0))  .additional_email-plus-sign").remove();
        $(".additional_multipleEmail:not(:eq(0))  .additional_email-remove-sign").show();
        
    });


   $(document).on("click",".additional_email-remove-sign",function(){
        var emailRowLenght = $(".additional_multipleEmail");
        
        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".additional_email-plus-sign").show();    
        }else{
            $(".additional_email-plus-sign").hide();    
        }
        $(this).parents(".additional_multipleEmail").remove();
    });


    $(document).on("click",'#additional_referralSource',function(){
    $("#additional_referralSourcePopUp").show();
    });

    $(document).on("click",'#additional_referralSource_cancel',function(){
    $("#additional_referralSourcePopUp").hide();
    $("#additional_addreferralSource").val('');
    });





/*phone row form1 remove*/
      $(document).on("click",".guarantor-phonerow-form1-remove-sign",function(){
       
       $(this).parents(".guarantor-form1-phone-row").remove();
        
          });





/*ankur*/

$(document).on("click",".multipleEmail-form1 .email-form1-plus-sign .fa-plus-circle",function(){
    var emailRowLenght = $(".multipleEmail-form1");
    if (emailRowLenght.length == 3) {
        $(".email-form1-plus-sign").hide();
    }
    var clone = $(".multipleEmail-form1:first").clone();
    clone.find('input[type=text]').val('');
    $(".multipleEmail-form1").first().after(clone);
    $(".multipleEmail-form1:not(:eq(0))  .email-form1-plus-sign .fa-plus-circle").remove();
    $(".multipleEmail-form1:not(:eq(0))  .email-form1-remove-sign .fa-minus-circle").show();

});

 $(document).on("click",".email-form1-remove-sign",function(){
     var emailRowLenght = $(".multipleEmail-form1");
     if (emailRowLenght.length == 3) {
         $(".email-form1-plus-sign").show();
     }
     $(this).parents(".multipleEmail-form1").remove();
  });


  $(document).on("click",".email-form2-plus-sign",function(){

    var clone = $(".multipleEmail-form2:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(this).parents(".multipleEmail-form2").after(clone);
    clone.find(".email-form2-plus-sign").hide();
    clone.find(".email-form2-remove-sign").show();
  });


  $(document).on("click",".email-form2-remove-sign",function(){
  $(this).parents(".multipleEmail-form2").remove();
  });






     


     /*Guarantor form1 clone*/
  $(document).on("click",".guarantor-plus-sign",function(){

        var clone = $(".property_guarantor_forcopy_form1").clone();
        clone.removeClass("property_guarantor_forcopy_form1").addClass("property_guarantor_form1");
        $(".property_guarantor_form1").first().after(clone);

        var length = $('.property_guarantor_form1').length;

         //alert(length);
        
       clone.find('.guarantor_carrier').attr('name','guarantor_carrier_'+length+'[]'); 
       clone.find('.guarantor_phoneType').attr('name','guarantor_phoneType_'+length+'[]');
       clone.find('.guarantor_countryCode').attr('name','guarantor_countryCode_'+length+'[]');
       clone.find('.guarantor_phone').attr('name','guarantor_phone_'+length+'[]');
       clone.find('.guarantor_email').attr('name','guarantor_email_'+length+'[]');
      clone.find('.phone_format').mask('000-000-0000', {reverse: true});
       clone.find('.guarantor-phonerow-form1-plus-sign').show(); 
       clone.find('.guarantor-phonerow-form1-remove-sign').hide(); 

     });

/* guarantor phone row form1 add*/




$(document).on("click",".applyOneTime",function(){

if(this.checked)
{

  $("#lateFeeCharge-error").remove();
  $('.applyDaily').prop('checked', false);
  $('.oneTimeRadio1').prop('checked', true);
  
   $(".dailyRadio").attr("disabled", true);
   $(".dailyFeeLabel").attr("disabled", true);
   $(".dailyRadio").attr("disabled", true);
   $(".dailyRentLabel").attr("disabled", true);
   $(".oneTimeRadio1").attr('checked', 'checked');
   $(".oneTimeFeeLabel").removeAttr("disabled"); 
   $(".oneTimeRadio2").removeAttr("disabled"); 
   $(".oneTimeRadio1").removeAttr("disabled"); 
   $('.oneTimeFeeLabel').attr('name', 'lateFeeCharge');
   $('.removeAttr').val(''); 

  }
else
{
 $("#lateFeeCharge-error").remove();
 $(".oneTimeRadio1").prop('checked', false);
 $(".oneTimeRadio2").prop('checked', false);
 $(".oneTimeRadio1").attr('disabled', true);
 $(".oneTimeRadio2").attr('disabled', true);
 $(".oneTimeRentLabel").attr("disabled", true);
 $(".oneTimeFeeLabel").attr("disabled", true);
 $(".oneTimeRentLabel").attr("disabled", true);
  $(".removeAttr").removeAttr("name");
  $('.removeAttr').val('');

 

}


});



$(document).on("click",".oneTimeRadio",function(){
        if( $(this).is(":checked") ){
            var val = $(this).val(); 
           

           if(val=='1')
           {
            $("#lateFeeCharge-error").remove();
          $(".removeAttr").removeAttr("name");
          $('.removeAttr').val('');
           $(".oneTimeRentLabel").attr("disabled", true);
           $(".oneTimeFeeLabel").attr("disabled", false);
           $('.oneTimeFeeLabel').attr('name', 'lateFeeCharge'); 

        
           }
           else
           {
            $("#lateFeeCharge-error").remove();
            $(".removeAttr").removeAttr("name");
            $('.removeAttr').val('');
            $(".oneTimeFeeLabel").attr("disabled", true); 
            $(".oneTimeRentLabel").attr("disabled", false);
            $('.oneTimeRentLabel').attr('name', 'lateFeeCharge'); 
           }
        }

});





$(document).on("click",".applyDaily",function(){

if(this.checked)
{
  $("#lateFeeCharge-error").remove();
  $('.dailyFeeLabel').attr('name', 'lateFeeCharge'); 
  $('.removeAttr').val('');
  $('.applyOneTime').prop('checked', false);
  $('.dailyRadio1').prop('checked', true);
  
   $(".oneTimeRadio").attr("disabled", true);
   $(".oneTimeFeeLabel").attr("disabled", true);
   $(".oneTimeRadio").attr("disabled", true);
   $(".oneTimeRentLabel").attr("disabled", true);
   $(".dailyRadio1").attr('checked', 'checked');
   $(".dailyFeeLabel").removeAttr("disabled"); 
   $(".dailyRadio2").removeAttr("disabled"); 
   $(".dailyRadio1").removeAttr("disabled"); 
   $('.removeAttr').val('');

  }
else
{
  $("#lateFeeCharge-error").remove();
  $('.removeAttr').val('');
 $(".dailyRadio1").prop('checked', false);
 $('.removeAttr').val('');
 $(".dailyRadio2").prop('checked', false);
 $(".dailyRadio1").attr('disabled', true);
 $(".dailyRadio2").attr('disabled', true);
 $(".dailyRentLabel").attr("disabled", true);
 $(".dailyFeeLabel").attr("disabled", true);
 $(".dailyRentLabel").attr("disabled", true);
 $(".removeAttr").removeAttr("name");

 

}


});



$(document).on("click",".dailyRadio",function(){
        if( $(this).is(":checked") ){
            var val = $(this).val(); 
        
           if(val=='1')
           {
            $("#lateFeeCharge-error").remove();
            $('.removeAttr').val('');
           $(".removeAttr").removeAttr("name");
           $(".dailyRentLabel").attr("disabled", true);
           $(".dailyFeeLabel").attr("disabled", false); 
           $('.dailyFeeLabel').attr('name', 'lateFeeCharge'); 
        
           }
           else
           {
            $("#lateFeeCharge-error").remove();
            $('.removeAttr').val('');
            $(".removeAttr").removeAttr("name");
            $(".dailyFeeLabel").attr("disabled", true); 
            $(".dailyRentLabel").attr("disabled", false);
            $('.dailyRentLabel').attr('name', 'lateFeeCharge'); 
           }
        }

});






    $(document).on("change",".cropit-image-input",function(){
      photo_videos = [];
     var fileData = this.files;
     var type= fileData.type;
     var elem = $(this);
      $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var uploadType = 'image';
        var validSize = '1030';
        var arrayType = validImageTypes;
   
        if ($.inArray(type, arrayType) < 0) {
            toastr.warning('Please select file with valid extension only!');
        } else {

            if (parseInt(size) > validSize) {
                var validMb =  1;
                toastr.warning('Please select documents less than '+validMb+' mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                photo_videos.push(value);
                var src = '';
                var reader = new FileReader();
                $('#photo_video_uploads').html('');
                
              
                $(".popup-bg").show();
                 elem.next().show();
               
            }

        }
        
    });
   
});

    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }



    $('.image-editor').cropit({
        imageState: {
            src: subdomain_url+'500/400',
        },
    });
    
    $(document).on("click",'.export',function(){
        $(this).parents('.cropItData').hide();
        $(this).parent().prev().val('');
        var dataVal = $(this).attr("data-val");
        var imageData = $(this).parents('.image-editor').cropit('export');
        var image = new Image();
        image.src = imageData;
        $(".popup-bg").hide();
        $(this).parent().parent().prev().find('div').html(image);
      

    });






    $(document).on("click",".add-emergency-contant",function(){

      var clone = $(".tenant-emergency-contact:first").clone();
      clone.find('input[type=text]').val('');
      clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});;
      $(".tenant-emergency-contact").first().after(clone);
      clone.find(".add-emergency-contant").hide();
        clone.find(".remove-emergency-contant, .remove-emergency-contant i").show();

    });
    


    $(document).on("click",".remove-emergency-contant",function(){
        $(this).parents(".tenant-emergency-contact").remove();
    });




    $(document).on("click",".additional-add-emergency-contant",function(){
  
        var clone = $(".additional-tenant-emergency-contact:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(".additional-tenant-emergency-contact").first().after(clone);
        clone.find(".additional-add-emergency-contant").hide();
        clone.find(".additional-remove-emergency-contant").show();

    });

    $(document).on("click",".additional-remove-emergency-contant",function(){


    $(this).parents(".additional-tenant-emergency-contact").remove();

    });

    $( "#guarantor_zipcode" ).focusout(function() {
        getZipCode('#guarantor_zipcode',$(this).val(),'#guarantor_city','#guarantor_province','#guarantor_country','','');
    });

    $(document).on("change","#phoneType12",function(){
        var val=$(this).val();
        if(val=='2' || val=='5'){

            $(this).parents(".primary-tenant-phone-row").find(".ext_phone").show();

        }else{
            $(this).parents(".primary-tenant-phone-row").find(".ext_phone").hide();


        }

    });

    $(document).on("change",'select[name="phoneType[]"]',function(){
        var val=$(this).val();
        if(val=='2' || val=='5'){

            $(this).parents(".primary-tenant-phone-row2").find(".ext_phone").show();

        }else{
            $(this).parents(".primary-tenant-phone-row2").find(".ext_phone").hide();


        }

    });


/*function getRedirectionData(id) {
    $.ajax({
        type: 'post',
        url: '/RentalApplication/Ajax',
        data: {
            class: "RenatlApplicationAjax",
            action: "getRedirectionData",
            id:id
        },
        success: function (response) {
            var response = JSON.parse(response);
            $("#property").val(response.data2.prop_id);


        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}*/








    function getOneYearDate(givenDate,type)
    {
             $.ajax({
               type: 'post',
               url: '/tenantPortal',
               data: {
                   class: 'TenantPortal',
                   action: 'getOneYearDate',
                   date:givenDate
               },
               success: function (response) {
                   var response = JSON.parse(response);
                   if(type=='pet')
                   {
                   $('.pet_expiration_date').val(response);
                   }
                   else if(type=='rabies')
                   {
                    $('.pet_rabies_expiration_date').val(response);
                   }
                   else if(type=='service_shot')
                   {
                    $('.service_expiration_date').val(response);
                   }
                   else if(type=='service_rabies')
                   {
                    $('.animal_expiration_date').val(response);
                   }
                    else if(type=='pet_nextvisit')
                   {
                    $('.pet_nextVisit').val(response);
                   }
                    else if(type=='animal_nextvisit')
                   {
                    $('.service_nextVisit').val(response);
                   }




               }
           });


    }




    $(document).on('click','.tenant-detail-four',function () {
    var name=localStorage.getItem('ten_name');

    var type = "user";
    var Name = $(this).attr('data-username');
    $(".userFullAccount").fadeIn();
    $(".allCharges").fadeIn();
    if(type=='user')
    {
        var user_id = $('.tenantapplytaxid').val();
        $.ajax({
            url:'/Tenantlisting/payment',
            type: 'POST',
            data: {

                "action": 'getReceiableData',
                "class": 'TenantAjax',
                "type":type,
                "user_id":user_id,
                "username":name

            },
            success: function (response) {
                //localStorage.removeItem('ten_name');
                $(".userFullAccount").html(response);
                getAllChargesData(type,user_id,name);


            }
        });
    }
    else
    {
        var invoice_id = $(this).attr('data-invoice_id');
        $.ajax({
            url:'/Tenantlisting/payment',
            type: 'POST',
            data: {

                "action": 'getReceiableData',
                "class": 'TenantAjax',
                "invoice_id":invoice_id,
                "type":type,
                "username":Name

            },
            success: function (response) {


                $(".userFullAccount").html(response);

                getAllChargesData(type,invoice_id,Name);

            }
        });

    }

});

    function getAllChargesData(type,id,Name)
    {

        if(type=='user')
        {
            $.ajax({
                url:'/Tenantlisting/payment',
                type: 'POST',
                data: {

                    "action": 'getAllChargesData',
                    "class": 'TenantAjax',
                    "type":type,
                    "user_id":id,
                    "username":Name

                },
                success: function (response) {

                    $(".allCharges").html(response);
                    updateAllPayments();

                }
            });

        }
        else
        {
            $.ajax({
                url:'/Tenantlisting/payment',
                type: 'POST',
                data: {

                    "action": 'getAllChargesData',
                    "class": 'TenantAjax',
                    "type":type,
                    "invoice_id":id,
                    "username":Name

                },
                success: function (response) {

                    $(".allCharges").html(response);
                    updateAllPayments();
                }
            });

        }
    }

    function updateAllPayments()
    {

        $("#updateAllCharges").validate({
            rules: {
            },
            submitHandler: function (e) {
                var form = $('#updateAllCharges')[0];
                var formData = new FormData(form);
                formData.append('action','updateAllCharges');
                formData.append('class','TenantAjax');

                var hiddenDueAmt = $(".hiddenDueAmount").val();
                var OverUnderPay = $(".overUnderPay").val();
                if(OverUnderPay<0)
                {
                    toastr.error("Currently you are paying less amount");
                    return false;
                }
                var ref = $(".ref").val();
                var check = $(".check").val();
                var paymnt_type = $(".paymnt_type").val();
                var paid_amount = $(".paidAmt").val();
                var hiddenChargeType = $(".hiddenChargeType").val();

                formData.append('class','TenantAjax');
                formData.append('hiddenDueAmount',hiddenDueAmt);
                formData.append('overPay',OverUnderPay);
                formData.append('ref',ref);
                formData.append('check',check);
                formData.append('paymnt_type',paymnt_type);
                formData.append('paid_amount',paid_amount);
                formData.append('hiddenChargeType',hiddenChargeType);
                $.ajax({
                    url:'/Tenantlisting/payment',
                    type: 'POST',
                    data: formData,
                    success: function (response) {
                        var response = JSON.parse(response);
                        if(response.status=='false')
                        {
                            toastr.error(response.message);
                        }
                        else
                        {
                            toastr.success(response.message);
                            $(".allCharges").fadeOut();
                            $(".userFullAccount").fadeOut();
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
    }

    $(document).on("blur keyup",".paidAmt",function(){
        var hiddenDueAmount = $('.hiddenDueAmount').val();
        var paidAmt = $(".paidAmt").val();
        var hiddenOverPay = $(".hiddenOverPay").val();
        var hiddenUncheckAmt = $('.hiddenUncheckedAmt').val();
        var overPayUnderPay = parseFloat(hiddenOverPay) + parseFloat(paidAmt) +parseFloat(hiddenUncheckAmt) -parseFloat(hiddenDueAmount);
        $(".overUnderPay").val(overPayUnderPay);

    });


    $(document).on("click", ".pay_checkbox", function () {
        var value = $(this).val();
        if (this.checked) {

            $(".waiveOfAmount_"+value).attr('name','waiveOfAmount[]');
            $(".waiveOfComment_"+value).attr('name','waiveOfComment[]');
            $(".currentPayment_"+value).attr('name','currentPayment[]');
            var currntDueAmt = $(".current_due_amt_"+value).html();
            $(".currentPayment_"+value).val(currntDueAmt);
            var overPay = $(".overUnderPay").val();
            var curntOvrPay = parseFloat(overPay) - parseFloat(currntDueAmt);
            $(".overUnderPay").val(curntOvrPay);
            var hiddenUncheckAmt = $('.hiddenUncheckedAmt').val();
            var currentHiddenUncheckAmt  =  parseFloat(hiddenUncheckAmt) -  parseFloat(currntDueAmt);
            $('.hiddenUncheckedAmt').val(currentHiddenUncheckAmt);

        }
        else {
            $(".waiveOfAmount_"+value).removeAttr('name');
            $(".waiveOfComment_"+value).removeAttr('name');
            $(".currentPayment_"+value).removeAttr('name');
            $(".currentPayment_"+value).val(0);
            var currntDueAmt = $(".current_due_amt_"+value).html();

            var overPay = $(".overUnderPay").val();
            var curntOvrPay = parseFloat(overPay) + parseFloat(currntDueAmt);
            $(".overUnderPay").val(curntOvrPay);
            var hiddenUncheckAmt = $('.hiddenUncheckedAmt').val();
            var currentHiddenUncheckAmt  =  parseFloat(hiddenUncheckAmt) +  parseFloat(currntDueAmt);
            $('.hiddenUncheckedAmt').val(currentHiddenUncheckAmt);
        }
    });

    $(document).on("change",".paymnt_type",function(){
        var type = $(this).val();
        var user_id = $(".hiddenUser_id").val();
        if(type=='card')
        {
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'checkForCard',
                    "class": 'accounting',
                    "type":type,
                    "user_id":user_id

                },
                success: function (response) {
                    var info = JSON.parse(response);
                    if(info.status=='false')
                    {
                        toastr.error(info.message);
                        $(".paymnt_type").val('check');
                    }
                }
            });

        }
        else if(type=='ACH')
        {
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'checkForACH',
                    "class": 'accounting',
                    "type":type,
                    "user_id":user_id

                },
                success: function (response) {
                    var info = JSON.parse(response);
                    if(info.status=='false')
                    {
                        toastr.error(info.message);
                        $(".paymnt_type").val('check');
                    }

                }
            });

        }

    });

    $(document).on('keypress keyup','.numberonly',function(){
        $(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }

    });

    $(document).on("click",".CancelBtn",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $(".allCharges").fadeOut();
                $(".userFullAccount").fadeOut();
            }
        });
    });


    $(document).on("click",".goback_func",function () {
        window.history.back();
    });


        $(document).on("click",".goback_func_cancel",function () {
      
           bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href =  window.location.origin+'/Tenantlisting/Tenantlisting'
            }
        });
     
    });

    // var firstDay = new Date(year2, month2, 1);
    // var lastDay = new Date(year2, month3, 0);

    function dateRange(startDate, endDate) {
        var start      = startDate.split('-');
        var end        = endDate.split('-');
        var startYear  = parseInt(start[0]);
        var endYear    = parseInt(end[0]);
        var dates      = [];

        for(var i = startYear; i <= endYear; i++) {
            var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
            var startMon = i === startYear ? parseInt(start[1])-1 : 0;
            for(var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j+1) {
                var month = j+1;
                var displayMonth = month < 10 ? '0'+month : month;
                dates.push([i, displayMonth, '01'].join('-'));
            }
        }
        return dates;
    }

    $(document).on('click','.tenant-detail-five',function () {
        ten_ledger();
    });
    $(document).on('click','#search_ledger',function () {
        ten_ledger();
    });
    function ten_ledger() {
        var user_id = $('.tenantapplytaxid').val();
        var a=$('.st_date').val();

        a = new Date(a);
        var yr=a.getFullYear();
        var mon=a.getMonth();
        var day=a.getDate();
        var st_date=yr+'-'+mon+'-'+day;
        var b= $('.end_date').val();
        b=new Date(b);
        var yr1=b.getFullYear();
        var mon1=b.getMonth() + 1;
        var end_date=yr1+'-'+mon1;

        var firstDay = new Date(yr, mon, 1);
        var lastDay = new Date(yr1, mon1, 0);
        st_date=firstDay.getFullYear()+'-'+(firstDay.getMonth()) +'-'+firstDay.getDate();
        end_date=lastDay.getFullYear()+'-'+(lastDay.getMonth()+1) +'-'+lastDay.getDate();
        $.ajax({
            url:'/Tenantlisting/payment',
            type: 'POST',
            data: {
                "action": 'getledgerdata',
                "class": 'TenantAjax',
                "user_id":user_id,
                'st_date':st_date,
                'end_date':end_date
            },
            success: function (response) {
                var res=JSON.parse(response);

                var html="";
                for(var i=0;i<res.data.length;i++){
                    html +='<tr class="ledger_tr">' +
                        '                                                                    <td>'+res.dates[i]+'</td>' +
                        '                                                                    <td>'+res.data[i].charge_code+'</td>' +
                        '                                                                    <td>'+res.data[i].description+'</td>' +
                        '                                                                    <td>'+res.data[i].debit+'</td>' +
                        '                                                                    <td>'+res.data[i].credit+'</td>' +
                        '                                                                </tr>';
                }
                $("#tenant_ledger").html('').append(html);
            }
        });

    }

    $(document).on('click','#exportexcel',function () {
        exportTableToExcel();
    });
    $(document).on('click','#exportpdf',function () {
        exppdf();
    });
    $(document).on('click','#printsample',function () {
        exppdf();
    });
    function exportTableToExcel(tableID, filename = ''){
        var innerHTML=$("#ledger_table").html();
        var html = innerHTML;
        var blob = new Blob(['\ufeff', html], {
            type: 'application/vnd.ms-excel'
        });

        // Specify link url
        var url = 'data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html);

        // Specify file name
        filename = filename?filename+'.xls':'excel.xls';


        // Create download link element
        var downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if(navigator.msSaveOrOpenBlob ){
            navigator.msSaveOrOpenBlob(blob, filename);
        }else{
            // Create a link to the file
            downloadLink.href = url;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }

        document.body.removeChild(downloadLink);
    }


    function exppdf() {
        var htmls=$("#ledger_table").html();

        $.ajax({
            type: 'post',
            url: '/Tenantlisting/payment',
            data: {
                class: "TenantAjax",
                action: "getPdfContent",
                htmls: htmls
            },
            success: function (res) {
                var res= JSON.parse(res);

                if (res.code == 200) {
                    $('.reports-loader').css('visibility','hidden');
                    var link=document.createElement('a');
                    document.body.appendChild(link);
                    link.target="_blank";
                    link.download="Report.pdf";
                    link.href=res.data.record;
                    link.click();
                    $("#wait").css("display", "none");
                } else if(res.code == 500) {
                    //toastr.warning(response.message);
                } else {
                    //toastr.error(response.message);
                }
            }
        });
    }




    $(document).on('click','#clearTenantAddForm',function(){

        resetFormClear('#addTenant',['moveInDate','acquireDate[]','expirationDate[]','pet_lastVisit[]',
            'pet_nextVisit[]','pet_date_given[]','pet_expiration_date[]',
            'pet_follow_up[]','service_birth[]','service_nextVisit[]','service_lastVisit[]',
            'service_date_given[]','service_expiration_date[]','service_follow_up[]','medical_date[]'],'form',false,defaultFormData,defaultIgnoreArray);
    });


    $(document).on("click",".add-popup .clear-btn",function(){
        var modal_id = $(this).parents('.add-popup').attr('id');
        resetFormClear('#'+modal_id,[], 'div',false);
    });

});




































