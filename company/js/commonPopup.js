/* initialisation on load */

$(document).ready(function () {
    $.ajax({
        type: 'post',
        url: '/Employee/getInitialData',
        data: {
            class: "EmplyeeAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.state.state != "") {
                    $("#driverProvince").val(data.data.state.state);
                }

                if (data.data.country.length > 0) {
                    var countryOption = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        countryOption += "<option value='" + value.id + "' data-id='" + value.code + "'>" + value.name + " (" + value.code + ")" + "</option>";
                    });
                    $('#additional_country').html(countryOption);
                    $('.additional_country').html(countryOption);
                    $('.emergency_additional_countryCode').html(countryOption);
                    $('#flag_country_code').html(countryOption);

                }

                // if (data.data.propertylist.length > 0) {
                //     var propertyOption = "<option value=''>Select</option>";
                //     $.each(data.data.propertylist, function (key, value) {
                //         propertyOption += "<option value='" + value.id + "' data-id='" + value.property_id + "'>" + value.property_name + "</option>";
                //     });
                //     $('#addTenant #property').html(propertyOption);
                // }

                if (data.data.phone_type.length > 0) {
                    var phoneOption = "";
                    $.each(data.data.phone_type, function (key, value) {
                        phoneOption += "<option value='" + value.id + "'>" + value.type + "</option>";
                    });
                }

                if (data.data.carrier.length > 0) {
                    var carrierOption = "<option value=''>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='" + value.id + "'>" + value.carrier + "</option>";
                    });
                    $('.additional_carrier').html(carrierOption);
                }

                if (data.data.referral.length > 0) {
                    var referralOption = "<option value=''>Select</option>";
                    $.each(data.data.referral, function (key, value) {
                        referralOption += "<option value='" + value.id + "'>" + value.referral + "</option>";
                    });
                    $('#additional_referralSource').html(referralOption);
                }

                if (data.data.ethnicity.length > 0) {
                    var ethnicityOption = "<option value=''>Select</option>";
                    $.each(data.data.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='" + value.id + "'>" + value.title + "</option>";
                    });
                    $('select[name="ethncity"]').html(ethnicityOption);
                    $('.addition_tenant_block select[name="additional_ethncity"]').html(ethnicityOption);
                }

                if (data.data.marital.length > 0) {
                    var maritalOption = "";
                    var maritalOption = "<option value=''>Select</option>";
                    $.each(data.data.marital, function (key, value) {
                        maritalOption += "<option value='" + value.id + "'>" + value.marital + "</option>";
                    });
                    $('select[name="maritalStatus"]').html(maritalOption);
                    $('.addition_tenant_block select[name="maritalStatus"]').html(maritalOption);
                    $('.additional_maritalStatus').html(maritalOption);
                }

                if (data.data.hobbies.length > 0) {
                    var hobbyOption = "";
                    $.each(data.data.hobbies, function (key, value) {
                        hobbyOption += "<option value='" + value.id + "'>" + value.hobby + "</option>";
                    });
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').html(hobbyOption);
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').multiselect({includeSelectAllOption: true, nonSelectedText: 'Select'});
                }
             


                if (data.data.veteran.length > 0) {
                    var veteranOption = "";
                    var veteranOption = "<option value=''>Select</option>";

                    $.each(data.data.veteran, function (key, value) {
                        veteranOption += "<option value='" + value.id + "'>" + value.veteran + "</option>";
                    });
                    $('select[name="veteranStatus"]').html(veteranOption);
                    $('.additional_veteranStatus').html(veteranOption);
                }
                if (data.data.credential_type.length > 0) {
                    var typeOption = "";
                    var typeOption = "<option value=''>Select</option>";

                    $.each(data.data.credential_type, function (key, value) {
                        typeOption += "<option value='" + value.id + "'>" + value.credential_type + "</option>";
                    });
                    $('.tenant-credentials-control select[name="credentialType[]"]').html(typeOption);
                }


                if (data.data.propertylist.length > 0){
                    var propertyOption = "<option value='0'>Undecided</option>";
                    $.each(data.data.propertylist, function (key, value) {
                        propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
              //      console.log(propertyOption);
                    $('#property').html(propertyOption);
                }


            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });


//key access code
    $(document).on("click", "#NewkeyPlus", function () {
        var length = $('.key_optionsDivClass').length;
        var nodeOne = $("#key_optionsDiv").clone();


        nodeOne.find(".key_access_codetext").text(' ');
        nodeOne.find('.key_access_codes_list').addClass('key_access_codes_info_' + length);
        nodeOne.find('.key_access_codes_list').removeClass('key_access_codes_info');

        nodeOne.find('.key_access_codetext').addClass('key_access_codetextdynm_' + length);
        nodeOne.find('.key_access_codetext').removeClass('key_access_codetext');

        nodeOne.find('.customValidateKeyAccessCode').attr('class', 'form-control keyAccessCode key-access-input customValidateKeyAccessCode' + length);
        nodeOne.find('.NewkeyPopupSave').attr('data_id', length);
        nodeOne.find('#key_options').attr('id', 'key_options' + length);
        nodeOne.insertAfter(".key_optionsDivClass:last");
        nodeOne.find("#NewkeyPlus").remove();
        nodeOne.find("#NewkeyMinus").show();
        nodeOne.find(".key_optionsDivClass").val(' ');
        nodeOne.find('.key_access_codeclass').hide();
        nodeOne.find(':selected').removeAttr('selected');
        //   debugger
        //$(this).parents('.key_optionsDivClass').next().find('#key_access_code').text('');
    });


    $(document).on("change", ".key_access_codes_info", function () {
        $(this).next().find('textarea').text('');
    });
    $(document).on("click", ".NewkeyPopupCancel", function () {
        $(this).closest('.NewkeyPopup').hide();
    });
    /*functions on load */
    fetchAllPetdata(false);
    fetchAllkeydata(false, '.key_access_codes_list');
    fetchAllAmenities(false);
    fetchAllComplaintType(false);
    fetchAllAmenitiesProperty(false);
    fetchAllCallType(false);
    /*functions on load */


    //add pet friendly popup
    $(document).on('click', '#NewpetPopupSave', function (e) {
        e.preventDefault();
        var formData = $('#NewpetPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-pet-popup',
            data: {form: formData,
                class: 'commonPopup',
                action: 'addPetPopup'},
            beforeSend: function (xhr) {
                // checking portfolio validations
                $(".customValidatePetFriendly").each(function () {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewpetPopup').hide(500);
                    toastr.success(response.message);
                    fetchAllPetdata(response.lastid);

                } else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    //add new complaint type
    $('#NewpetComplaintSave').on('click', function (e) {
        e.preventDefault();
        var formData = $('#ComplaintTypePopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-complaint-type-popup',
            data: {form: formData,
                class: 'commonPopup',
                action: 'addComplaintType'},
            beforeSend: function (xhr) {
                // checking portfolio validations
                $(".customValidateComplaint").each(function () {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#ComplaintTypePopup').hide(500);
                    toastr.success(response.message);
                    fetchAllComplaintType(response.lastid);

                } else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });


    //add key access popup
    $(document).on('click', '.NewkeyPopupSave', function () {
        var nodeNew = $(this).parents('.NewkeyPopup')
        var length = $(this).attr('data_id');
        var formData = nodeNew.find('.key-access-input').val();
        $.ajax({
            type: 'post',
            url: '/add-key-popup',
            data: {form: formData,
                class: 'commonPopup',
                action: 'addKeyPopup'},
            beforeSend: function (xhr) {
                // checking portfolio validations
                $('.customValidateKeyAccessCode' + length).each(function () {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    nodeNew.hide();
                    toastr.success(response.message);
                    if (length == 0) {
                        fetchAllkeydata(response.lastid, '.key_access_codes_info');
                        $('.key_access_codeclass').show();
                    } else {
                        fetchAllkeydata(response.lastid, '.key_access_codes_info_' + length);
                        $('.key_access_codetextdynm_' + length).parent().show();


                    }

                } else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    //add amenity popup
    $(document).on('click', '#NewamenitiesPopupSave', function (e) {
        e.preventDefault();
        var formData = $('#NewamenitiesPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-amenity-popup',
            data: {form: formData,
                class: 'commonPopup',
                action: 'addAmenityPopup'},
            beforeSend: function (xhr) {
                // checking portfolio validations
                $(".customValidateAmenities").each(function () {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewamenitiesPopup').hide(500);
                    toastr.success(response.message);
                    var lastid = response.lastid;
                    fetchAllAmenities(response.lastid)
                    fetchAllAmenitiesProperty(response.lastid);
                    //$("#"+lastid).attr('checked',true);
                    setTimeout(function () {
                        $("#" + lastid).attr("checked", true)
                    }, 1000);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on('click', '#Newpet', function () {
        $("#NewpetPopup").show();
        $("#NewpetPopup :input[type = 'text']").val('');
        $("#NewpetPopup span.required").text('');
    });



    $("#complainttype").click(function () {
        $("#ComplaintTypePopup").show();
    });


    $(document).on('click', ".grey-btn", function () {

        var box = $(this).closest('.add-popup');
        $(box).hide();

    });

    $(document).on('click', '#NewamenitiesPopupSave1', function (e) {

        e.preventDefault();
        var formData = $('#NewamenitiesPopup1 :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-amenity-popup',
            data: {form: formData,
                class: 'commonPopup',
                action: 'addAmenityPopup'},
            beforeSend: function (xhr) {
                // checking portfolio validations
                $(".customValidateAmenities").each(function () {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewamenitiesPopup1').hide(500);
                    toastr.success(response.message);
                    var lastid = response.lastid;
                    fetchAllAmenities(response.lastid)
                    fetchAllAmenitiesProperty(response.lastid);
                    //$("#"+lastid).attr('checked',true);
                    setTimeout(function () {
                        $("#" + lastid).attr("checked", true)
                    }, 1000);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click", ".Newkey", function () {
        $('.customValidateKeyAccessCode').val('');
        $(this).parents('.key_optionsDivClass').find(".NewkeyPopup").show();
        $(this).parents('.NewkeyPopup').find(".key-access-input").val(' ');
    });

    $(document).on('click', '#Newamenities', function () {
        $("#NewamenitiesPopup").show();
    });

    $(document).on('click', '#Newamenities1', function () {
        $("#NewamenitiesPopup1").show();
    });

    $(document).on("click", ".additionalReferralResource", function () {
        $("#additionalReferralResource1").show(200);
    });

    $(document).on("click", ".selectPropertyEthnicity", function () {
        $("#selectPropertyEthnicity1").show(200);
    });

    $(document).on("click", ".selectPropertyMaritalStatus", function () {
        $("#selectPropertyMaritalStatus1").show(200);
    });

    $(document).on("click", ".selectPropertyHobbies", function () {
        $("#selectPropertyHobbies1").show();
    });

    $(document).on("click", ".selectPropertyVeteranStatus", function () {
        $("#selectPropertyVeteranStatus1").show();
    });

    $(document).on("click", ".tenantCredentialType", function () {
        $("#tenantCredentialType1").show();
    });

    $(document).on("click", ".selectCallType", function () {
        $("#selectCallType").show(200);
    });

    $(document).on("click", ".add_single1", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var dataValidationClass = $(this).attr("data-validation-class");
        var val = $(this).parent().prev().find("." + className).val();
        savePopUpData(tableName, colName, val, selectName, dataValidationClass);
        var val = $(this).parent().prev().find("input[type='text']").val('');
    });



});

function  savePopUpData(tableName, colName, val, selectName, validationClass) {
    $.ajax({
        type: 'post',
        url: '/common-popup-ajax',
        data: {
            class: "commonPopup",
            action: "savePopUpData",
            tableName: tableName,
            colName: colName,
            val: val
        }, beforeSend: function (xhr) {
            // checking portfolio validations
            $("." + validationClass).each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='" + data.data.last_insert_id + "' selected>" + data.data.col_val + "</option>";
                $("select[name='" + selectName + "']").append(option);
                if (selectName == "hobbies" || selectName == "additional_hobbies[]") {
                    $("select[name='" + selectName + "[]']").append(option);
                    $("select[name='" + selectName + "[]']").multiselect('destroy');
                    $("select[name='" + selectName + "[]']").multiselect({includeSelectAllOption: true, nonSelectedText: 'Select'});
                    $("select[name='" + selectName + "[]']").parent().parent().next().hide();
                } else {
                    $("select[name='" + selectName + "']").next().hide();
                }
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


function fetchAllPetdata(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-allpetdata',
        data: {
            class: 'commonPopup',
            action: 'fetchPetdata'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#pet_options').html(res.data);
            if (id != false) {
                $('#pet_options').val(id);
            }

        },
    });
}


function fetchAllkeydata(id, element) {
    $.ajax({
        type: 'post',
        url: '/fetch-keydata',
        data: {
            class: 'commonPopup',
            action: 'fetchkeydata'},
        success: function (response) {
            var res = JSON.parse(response);
            var previous = (element !== undefined) ? $(element).val() : $('.key_access_codes_list ').val();
            $(element).html(res.data);
            if (id != false) {
                $(element).val(id);
            }
        },
    });

}

function fetchAllAmenities(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-all-amenities',
        data: {
            class: 'commonPopup',
            action: 'fetchAllAmenities'},
        success: function (response) {
            var res = JSON.parse(response);
            //$('#amenties_box').html(res.data);
            $('#amenties_box1').html(res.data);
        },
    });

}


function fetchAllAmenitiesProperty(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-all-amenities',
        data: {
            class: 'commonPopup',
            action: 'fetchAllAmenitiesProperty'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#amenties_box').html(res.data);
            //$('#amenties_box1').html(res.data);
        },
    });
}

function fetchAllComplaintType(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-all-complaint-type',
        data: {
            class: 'commonPopup',
            action: 'fetchAllComplaintType'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#complaint_type_options').html(res.data);
            if (id != false) {
                $('#complaint_type_options').val(id);
            }


        },
    });

}


function fetchAllCallType(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-all-call-type',
        data: {
            class: 'commonPopup',
            action: 'fetchAllCallType'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#call_type').html(res.data);
            if (id != false) {
                $('#call_type').val(id);
            }


        },
    });

}
