//add property form client side validations
$("#addManagementinfoForm").validate({
    rules: {
        management_company_name: {
            required: true
        },
        management_start_date: {
            required: true
        },
        management_end_date: {
            required: true
        },
        reason_management_end: {
            required: true
        },
        description: {
            required: true
        }
    },
    messages: {
        portfolioName: {
            valueNotEquals: "Please select non-default value!"
        },
        countryCode: {
            valueNotEquals: "Please select non-default value!"
        },
        garageAvalible: {
            valueNotEquals: "Please select non-default value!"
        }
    }
});
