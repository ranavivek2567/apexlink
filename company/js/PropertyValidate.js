//add property form client side validations
$("#addPropertyForm").validate({
    rules: {
        property_name: {
            required: true
        },

        property_id: {
            required: true
        },
        portfolio_id: {
            required: true
        },
        no_of_buildings: {
            required: true,
            number: true
        },
        no_of_units: {
            required: true,
            number: true
        },
        zipcode: {
            number:true,
            maxlength: 6
        },
        property_squareFootage: {
            number:true
        },
        school_district_code: {
            number:true
        },
        country_code: {
            valueNotEquals: "default"
        },
        property_price:{
            maxAmount:10
        },
        garage_available: {
            valueNotEquals: "default"
        },
        base_rent:{
            maxAmount:10,
            required: true
        },
        market_rent:{
            maxAmount:10,
            required: true
        },
        security_deposit:{
            maxAmount:10,
            required: true
        },manager_id:{
            required: true
        }
    },
    messages: {
        portfolioName: {
            valueNotEquals: "Please select non-default value!"
        },
        countryCode: {
            valueNotEquals: "Please select non-default value!"
        },
        garageAvalible: {
            valueNotEquals: "Please select non-default value!"
        }
    }
});
// warranty validation
$("#addwarrantyinforform").validate({
    rules: {
        key_fixture_name: {
            required: true
        },
        fixture_type: {
            required: true
        },
        assessed_age: {
            required: true,
            number: true
        },
        maintenance_reminder: {
             required: true
        },
        warranty_expiration_date: {
            required: true,
        },
        insurance_expiration_date: {
            required: true,
        },
        manufacturer_name: {
            required: true,
        },
        manufacturer_phonenumber: {
            required: true
        },
        add_notes: {
            required: true
        }
    },
    messages: {
        portfolioName: {
            valueNotEquals: "Please select non-default value!"
        },
        countryCode: {
            valueNotEquals: "Please select non-default value!"
        },
        garageAvalible: {
            valueNotEquals: "Please select non-default value!"
        }
    }
});

//portfolio custom validation
$(document).on('keyup', '.customValidatePortfoio', function () {
    validations(this);
});

//manager custom validation
$(document).on('keyup', '.customValidateManager', function () {
    validations(this);
});

//Attach group validation
$(document).on('keyup', '.customValidateGroup', function () {
    validations(this);
});

//Property type validation
$(document).on('keyup', '.customValidatePropertyType', function () {
    validations(this);
})

//Property style validation
$(document).on('keyup', '.customValidatePropertyStyle', function () {
    validations(this);
});

//Property subtype validation
$(document).on('keyup', '.customValidatePropertySubType', function () {
    validations(this);
});

//Property subtype validation
$(document).on('keyup', '.customValidatePetFriendly', function () {
    validations(this);
});

//Property subtype validation
$(document).on('keyup', '.customValidateKeyAccessCode', function () {
    validations(this);
});

//Property subtype validation
$(document).on('keyup', '.customValidateGarageAvailable', function () {
    validations(this);
});

//Property subtype validation
$(document).on('keyup', '.customValidateAmenities', function () {
    validations(this);
});

//Property subtype validation
$(document).on('keyup', '.customValidateUnitType', function () {
    validations(this);
});

//Property subtype validation
$(document).on('keyup','.customValidateAddFixture',function(){
    validations(this);
});

//Property subtype validation
$(document).on('keyup','.customValidateNewReasonPopup',function(){
    validations(this);
});

//Property AccountType validation
$(document).on('keyup','.customValidateAccountName',function(){
    validations(this);
});

//Property AccountType validation
$(document).on('keyup','.customValidateAccountType',function(){
    validations(this);
});
$(document).on('keydown','#phone_number',function(){
    $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
});
