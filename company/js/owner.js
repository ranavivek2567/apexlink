//add property form client side validations
    $("#addOwnerForm").validate({
        rules: {
            
            "fiscal_year_end[]": {
                required: true
            },
            "owner_percentowned[]":{
                number:true
            }
        }
    });

