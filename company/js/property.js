var base_url = window.location.origin;
var login_user_id = localStorage.getItem("login_user_id");
var status = localStorage.getItem("active_inactive_status");

//no building
//building unit check
$(document).on ('change','#is_short_term_rental',function () {
   var is_short=$(this).val();
   if(is_short == '1'){
       var html='Daily price '+default_currency_symbol;
        $('#price_label_id').html(html);
   }else{
       var html='Property Price '+default_currency_symbol;
       $('#price_label_id').html(html);
   }
});
$(document).on('click', '#default_building_unit', function () {
    var checked = $(this).prop('checked');
    var building = $('#no_of_building').val();
    var unit = $('#no_of_units').val();
    if (checked) {
        if ((building > 1) || (unit > 1)) {
            $(this).prop('checked', false);
            toastr.warning('Number of Building and Unit  must be equal to 1.')
        } else {
            $(".details").show();
        }
    } else {
        $(this).prop('checked', false);
        $(".details").hide();
    }
});



//building unit check
$(document).on('focusout', '#no_of_building', function () {
    var checked = $('#default_building_unit').prop('checked');
    var building = $('#no_of_building').val();
    if (checked) {
        if (building > 1) {
            $('#no_of_building').val('1');
            toastr.warning('Unselect No seprate Building/Unit checkbox first.');
        }
    }
});


$(document).on('focusout', '#no_of_units', function () {
    var checked = $('#default_building_unit').prop('checked');
    var unit = $('#no_of_units').val();
    if (checked) {
        if (unit > 1) {
            $('#no_of_units').val('1');
            toastr.warning('Unselect No seprate Building/Unit checkbox first.');
        }
    }
});

$(document).on('click', '#AddNewUnitTypeModalPlus2', function (e) {
    e.preventDefault();
    $("#NewunitPopup").show();
});

$(document).on('click', '.NewReasonPlus', function (e) {
    e.preventDefault();
    $("#NewReasonPopup").show();
});



//add reason popup
$(document).on('click', '.NewReasonSave', function (e) {
    e.preventDefault();
    var formData = $('#NewReasonPopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addReasonpopup'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateNewReasonPopup").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#NewReasonPopup').hide(500);
                toastr.success(response.message);
                fetchAllReason(response.lastid);
            } else if (response.status == 'error' && response.code == 500) {
                if (response.message == 'Reason already exists!')
                    toastr.warning(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});
function fetchAllUnittypes(id) {
    var propertyEditid = $("#property_editunique_id").val();

    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchAllUnittypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#unit_type').html(res.data);
            if (id != false) {
                $('#unit_type').val(id);

            }
            $("#default_rent").val(res.default_rent);


        },
    });

}
$(document).on('click', '#NewunitPopupSave', function (e) {
    e.preventDefault();
    var formData = $('#NewunitPopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addUnittype'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateUnitType").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#NewunitPopup').hide(500);
                toastr.success(response.message);
                fetchAllUnittypes(response.lastid);
            } else if (response.status == 'error' && response.code == 500) {
                if (response.message == 'Portfolio already exists!') bootbox.alert(response.message);return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});


// property insurance grid view


//jqGridBKeytrack('All', true);

buildingComplaints();
//add property field button

$(document).ready(function () {
    jQuery('.phone_number_general').mask('000-000-0000', {reverse: true});
    workorder();
    $('#properties_top').addClass('active');

    $('#property_name').on('blur', function () {
        $('#legal_name').val(this.value);
    });
    $('#base_rent').on('blur', function () {
        $('#security_deposit').val(this.value);
    });

    $(".last_renovation_time").timepicker({timeFormat: 'h:mm:a'});

    var id = getParameterByName('id');


    //open a modal
    $(document).on('click', '#ancNewRenovation', function (e) {
        $("#renovation-info").modal('show');
        $("#Re_addRenovationForm").trigger("reset");
        $(".last_renovation").datepicker({dateFormat: datepicker, changeMonth: true,
            changeYear: true});
        $("#last_renovation_time_id").timepicker({timeFormat: 'h:mm p', defaultTime: new Date()});

    });
    $(document).on('click', '#saveRenovationinfo', function (e) {
        e.preventDefault();
        var formData = $('#Re_addRenovationForm :input').serializeArray();
        var property_id = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {form: formData,
                property_id: property_id,
                class: 'propertyDetail',
                action: 'addRenovationdetails'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $("#renovation-info").modal('hide');
                    $("#renovationdate").val(response.checkDatatime);
                    $("#renovationtime").timepicker({timeFormat: 'h:mm p', defaultTime: response.checkDatatime2});
                    $("#last_renovation_description").val(response.checkData.last_renovation_description);


                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });
    //add insurance href
    $(document).on('click', '#insuranceOuterDivhref', function () {
        $("#insuranceOuterDivOpen").toggle(500);
        $("#addPropertyInsurance").trigger('reset');
        $('#AddPropertyInsuranceSave1').text('Save');
        //property insurance
        $(".ins_start_date").datepicker({dateFormat: datepicker,
            changeYear: true,
            minDate: 0,
            yearRange: "-100:+20",
            changeMonth: true,
            onClose: function (selectedDate) {
                $(".ins_end_date").datepicker("option", "minDate", selectedDate);
            }}).datepicker("setDate", new Date());

        $(".ins_end_date").datepicker({dateFormat: datepicker,
            changeYear: true,
            minDate: 0,
            yearRange: "-100:+20",
            changeMonth: true,
            onClose: function (selectedDate) {
                $(".start_date").datepicker("option", "maxDate", selectedDate);
            }}).datepicker("setDate", new Date());

        $(".ins_renewal_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());

    });

    $(document).on('click', '#warrantyinforhref', function () {
        $("#warrantyinfoDivopen").toggle(500);
        $("#addwarrantyinforform").trigger('reset');
        $('#warrantyinforClickB').text('Save');

    });

    $(document).on('click', '#addnoteshref', function () {
        $('#notesId').val('');
        $('#AddPropertynotesButton').text('Save');
        $("#addnotesopenDiv").toggle(500);
    });

    $(document).on('click', '.cancelPropertynotesButton', function () {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#addnotesopenDiv").hide(500);
            }
        });
       
    });
    //for current time acc to timezone
    getcurrentdatetime();

    //management fee checks
    $('#magt_type').change(function () {
        $('#management_fee_value').val('');
        $('#minimum_management_fee').val('');
        if ($(this).val() == 'Percentage and a min. set fee') {
            $('#minimum_fee').show();
        } else {
            $('#minimum_fee').hide();
        }
        $('#management_fee_value').removeAttr('max');
        if ($(this).val() == 'Flat') {
            $('#management_fee_value').addClass('amount number_only');
            $('#value_type').html('Value( ' + default_currency_symbol + ' )<em class="red-star">*</em>');
        } else if ($(this).val() == 'Percent') {
            $('#management_fee_value').removeClass('amount number_only');
            $('#management_fee_value').attr('max', '100');
            $('#value_type').html('Value(%)<em class="red-star">*</em>');
        } else if ($(this).val() == 'Percentage and a min. set fee') {
            $('#management_fee_value').attr('max', '100');
            $('#minimum_management_fee').addClass('amount number_only');
            $('#value_type').html('Value(%)<em class="red-star">*</em>');
        } else {
            $('#value_type').html('Value<em class="red-star">*</em>');
        }

    });
    //key access code
    $(document).on("change", ".key_access_codes_list", function () {

        var value = $(this).val();

        // console.log($(this)); return false;
        // $(this).find('.key_access_codetext').val('');

        //console.log($(this).next('.key_access_codeclass').find('textarea').val(''));
        $(this).next('.key_access_codeclass').find('textarea').val('');
        if (parseInt(value) >= 1) {
            $(this).parent().find('.key_access_codeclass').show();
            //$(this).find('textarea').text(' ');
            console.log(value);
        } else {
            $(this).parent().find('.key_access_codeclass').hide();
            //$(this).find('textarea').text(' ');
        }
    });

    //garage available
    $(document).on("change", "#garage_options", function () {

        var value = $(this).val();

        if (value == "1") {
            $('.garageDesc').hide();
            $('.garageVehicle').hide();

        } else if (value == "2") {
            $('.garageDesc').hide();
            $('.garageVehicle').show();
            $('.garage_veh').val('');
        } else {
            $('.garageDesc').show();
            $('.garageVehicle').hide();
            $('.garage_text').val('');
        }
    });
    //school distinct
    $(document).on("change", ".school_district_municipality_select", function () {
        var value = $(this).val();
        if (value != '') {
            $(this).parent().next('.hidden_school_fields').show();
            console.log(value);
        } else {
            $(this).parent().next('.hidden_school_fields').hide();
            console.log('select');
        }
    });
    $(document).on('click', '#AddNewSchoolDistrictDiv', function () {
        var clone = $(".school_district_municipality_div .clone-1:first").clone();
        $(".school_district_municipality_div .clone-1").last().after(clone);
        $(".school_district_municipality_div .clone-1").last().find(".school_district_code").val('');
        $(".school_district_municipality_div .clone-1:not(:eq(0)) #AddNewSchoolDistrictDiv").remove();
        $(".school_district_municipality_div .clone-1:not(:eq(0)) .RemoveNewSchoolDistrictDiv").show();
    });
    $(document).on("click", ".RemoveNewSchoolDistrictDiv", function () {
        $(this).parents(".clone-1").remove();
    });

    //hide key access box

    $(document).on("click", "#NewkeyMinus", function () {
        $(this).parents('#key_optionsDiv').remove();
    });



//combo grid for owner_pre_vendor
    $('.owner_pre_vendor').combogrid({
        panelWidth: 500,
        url: '/get-vendorsdata',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getVendorsdata',
            class: 'propertyDetail'
        },
        columns: [[
                {field: 'id', hidden: true},
                {field: 'name', title: 'Name', width: 60},
                {field: 'address1', title: 'Address', width: 120},
                {field: 'email', title: 'Email', width: 120},
            ]],
        onSelect: function (index, row) {
            var desc = row.address1;  // the product's description
            $("#vendor_address").val(desc);
            $("#vendor_name").val(row.name);
            $("#key_holder_id").val(row.id);
            $(".owner_pre_vendorID").val(row.id);
            $("#vendor_address").attr("disabled", false);
        }
    });
    $("#_easyui_textbox_input2").attr("placeholder", "Click here to pick a vendor");
    $("#_easyui_textbox_input2").addClass("form-control");
    $("#_easyui_textbox_input2").css("width",'100%');
    $("#_easyui_textbox_input2").css("height",'34px');

//combo grid for blacklistvendor_name
    $('#blacklistvendor_name').combogrid({
        panelWidth: 500,
        url: '/get-vendorsdata',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getVendorsdata',
            class: 'propertyDetail'
        },
        columns: [[
                {field: 'id', hidden: true},
                {field: 'name', title: 'Name', width: 60},
                {field: 'address1', title: 'Address', width: 120},
                {field: 'email', title: 'Email', width: 120},
            ]],
        onSelect: function (index, row) {
            var desc = row.address1;  // the product's description
            $("#blacklistvendor_address").val(desc);
            $("#blacklistvendor_name").val(row.name);
            $(".blacklist_vendorID").val(row.id);
            $("#blacklistvendor_address").attr("disabled", false);
        }
    });
    $("#_easyui_textbox_input3").attr("placeholder", "Click here to pick a vendor");

    //combo grid on track key
    $('#edittrackkey_name').combogrid({
        panelWidth: 500,
        url: '/get-vendorsdata',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getVendorsdata',
            class: 'propertyDetail'
        },
        columns: [[
                {field: 'id', hidden: true},
                {field: 'name', title: 'Name', width: 60},
                {field: 'address1', title: 'Address', width: 120},
                {field: 'email', title: 'Email', width: 120},
            ]],
        onSelect: function (index, row) {
            var desc = row.address1;  // the product's description
            $(".edittrackemail").val(row.email);
            $(".edittrackcompany_name").val(row.company_name);
            $(".edittrackphone").val(row.phone_number);
        }
    });
    $("#_easyui_textbox_input4").attr("placeholder", "Key Tag");

    //combo grid on track key
    $('#trackkey_name').combogrid({
        panelWidth: 500,
        url: '/get-vendorsdata',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getVendorsdata',
            class: 'propertyDetail'
        },
        columns: [[
                {field: 'id', hidden: true},
                {field: 'name', title: 'Name', width: 60},
                {field: 'address1', title: 'Address', width: 120},
                {field: 'email', title: 'Email', width: 120},
            ]],
        onSelect: function (index, row) {
            var desc = row.address1;  // the product's description
            $(".edittrackemail").val(row.email);
            $(".edittrackcompany_name").val(row.company_name);
            $(".edittrackphone").val(row.phone_number);
        }
    });


    fetchAllUnittypes(false);
    fetchAllPortfolio(false);
    fetchAllPropertytype(false);
    fetchAllPropertystyle(false);
    fetchAllPropertysubtype(false);
    fetchAllgarage(false);
    fetchAllManagers(false);
    fetchAllAttachgroups(false);
    setTimeout(function () {
        getAllOwners();
    }, 3000);

    fetchAllinsurancetypes(false);
    fetchAllpolicytypes(false);
    fetchAllFixture(false);
    fetchAllReason(false);

    fetchChartAccounts(false);
    accountTypeList();
    fetchAllAccountType(false);
//    checkTabValidation();

    //cancel add property
    $(document).on("click", "#cancel_property", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Property/PropertyModules';
            }
        });
    });

    //cancel add property
    $(document).on("click", "#cancel-notes", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Property/PropertyModules';
            }
        });
    });

    //cancel add property
    $(document).on("click", ".cancel-all", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Property/PropertyModules';
            }
        });
    });

    // cancel track key
    $(document).on('click', '#key_tracker_cancel', function () {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $('#addTrackDiv').hide(500);
                }
            }
        });
    });
    //check all amenities
    $(document).on("click", "#selectAllamennity", function () {
        $(".inputAllamenity").prop('checked', true);
        if ($('.all').prop("checked") == false) {
            $(".inputAllamenity").prop('checked', false);
        }
    });

    /*for multiple phone number textbox*/
    $(document).on("click", "#multiplephoneno", function () {
        var newnode = $("#multiplephoneIDD").clone().insertAfter("div.multiplephonediv:last");
        newnode.find('.phone_number_general').val('');
        $(".multiplephonediv:not(:eq(0))  #multiplephoneno").remove();
        $(".multiplephonediv:not(:eq(0))  #multiplephonenocross").show();
    });
    /*remove multiple phone number textbox*/
    $(document).on("click", "#multiplephonenocross", function () {
        $(this).parents('.multiplephonediv').remove();
        $("#multiplephoneno").show();

    });

    /*for multiple property tax id textbox*/
    $(document).on("click", "#multiplepropertytax", function () {
        var newnode = $("#multiplepropertydivId").clone().insertAfter("div.multiplepropertydiv:last");
        newnode.find('#property_tax_id').val('');
        $(".multiplepropertydiv:not(:eq(0))  #multiplepropertytax").remove();
        $(".multiplepropertydiv:not(:eq(0))  #multiplepropertytaxcross").show();



    });
    /*remove multiple property tax id textbox*/
    $(document).on("click", "#multiplepropertytaxcross", function () {
        $(this).parents('.multiplepropertydiv').remove();
        $("#multiplephoneno").show();

    });


    $(document).on('keyup', '.validateCustomeInput', function () {
        var id = this.id;
        var arg = $(this).attr('data_type');
        var data_required = $(this).attr('data_required');
        var data_value = $(this).val();
        validateCustomField(data_value, data_required, arg, this, id);
    });

    $(document).on('click', '#Newprofile', function () {
        $("#NewpstylePopup").show();
    });

    $(document).on('click', '#Newportfolio', function () {
        $('#portfolio_id').val(getRandomNumber(6));
        $("#NewportfolioPopup").show();
    });
    $(document).on('click', '#Newattach', function () {
        $("#NewattachPopup").show();
    });

    $(document).on('click', '#Newtype', function () {
        $("#NewtypePopup").show();
    });
    $(document).on('click', '#Newsubtype', function (e) {
        e.preventDefault();
        $("#NewsubtypePopup").show();
    });
    $(document).on('click', '#Newgarage', function () {
        $("#NewgaragePopup").show();
    });
    $(document).on('click', '#Newmanager', function () {
        $("#NewmanagerPopup").show();
    });
    /**
     * If the letter is not digit in fax then don't type anything.
     */
    $("#fax").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /**
     * To change the format of fax
     */
    $("input[name='fax']").keydown(function () {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });

 
    /**
     * Auto fill the city, state and country when focus loose on zipcode field.
     */
    $("#zipcode").focusout(function () {

//        getAddressInfoByZip1($(this).val());
        getZipCode('#zipcode', $(this).val(), '.zipcity', '.zipstate', '.zipcountry', null, null);
    });
    $("#wating_zipcode").focusout(function () {

//        getAddressInfoByZip1($(this).val());
        getZipCode('#wating_zipcode', $(this).val(), '.zipcity', '.zipstate', '.zipcountry', null, null);
    });

    /**
     * Auto fill the city, state and country when enter key is clicked.
     */
    $("#zipcode").keydown(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            getAddressInfoByZip1($(this).val());
        }
    });
    //add value to input for save and next popup
    $(document).on('click', '#SavenNextId', function () {
        $("#saveandnext").val(1);
    });
    $(document).on('click', '#SavenNextownerId', function () {
        $("#saveandnextowner").val(1);
    });

    //add genreal property information
    $('#addPropertyForm').on('submit', function (e) {
        e.preventDefault();
        var formData = $('#addPropertyForm').serializeArray();
        var property_clone_id=$("#property_clone_id").val();
        var property_clone_status=$("#property_clone_status").val();
        var custom_field = [];
        if ($("#addPropertyForm").valid()) {
            if ($("#saveandnext").val() == 1) {

                addPropertyForm();
                // setTimeout(function () {
                //     $("#2collapseTwo").trigger("click");
                // }, 1000);


            }
            else if ($('#default_building_unit').prop('checked') === false) {

            bootbox.confirm({
                message: "Do you want to add Buildings to this Property?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        addPropertyForm();

                    } else {
                        if ($('#default_building_unit').prop('checked') === false) {
                            toastr.error("Please check Property with no building");
                        }
                    }
                }
            });
        } else {
                addPropertyForm();
            }
        }

    });
    function addPropertyForm(){
        var formData = $('#addPropertyForm').serializeArray();
        var property_clone_id=$("#property_clone_id").val();
        var property_clone_status=$("#property_clone_status").val();
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/add-property',
            data: {form: formData,
                propertyEditid: propertyEditid,
                property_clone_id:property_clone_id,
                property_clone_status:property_clone_status,
                class: 'propertyDetail',
                action: 'addProperty'},
            success: function (response) {

                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {

                    var updateGeneral = $('#general_property_submit').val();


                    if (updateGeneral !== undefined && updateGeneral == 'Update') {
                        var res = update_users_flag('general_property',propertyEditid);
                        localStorage.setItem('rowcolor', 'rowColor');
                     if(res.code==200){
                         setTimeout(function () {
                             window.location.href = '/Property/PropertyModules';
                      },100)
                     }
                    }

                    if ($("#saveandnext").val() == 1) {
                        setTimeout(function () {

                            $("#2collapseTwo").trigger("click");

                            property_unique_id = response.pId;

                        }, 1000);


                    } else {
                        if ($("#property_editunique_id").val() === undefined) {
                            property_unique_id = $("#property_editunique_id").val();
                            if ($('#default_building_unit').prop('checked') === false) {
                                window.location.href = '/Building/AddBuilding?id=' + response.pId;
                            } else {

                                localStorage.setItem("Message", 'The record Added successfully.');
                                localStorage.setItem('rowcolor', 'rowColor');
                                window.location.href = '/Property/PropertyModules';
                            }
                        } else {
                            property_unique_id = response.pId;
                            localStorage.setItem("Message", 'The record updated successfully.');
                            localStorage.setItem('rowcolor', 'rowColor');
                            window.location.href = '/Property/PropertyModules';
                        }
                    }

                    $("#property_unique_id").val(response.pId);

                    tabValidation();
                    getChargePrefrences();
                    getManageCharges();

                    if ($("#propertPhotovideos-table")[0].grid) {
                        $('#propertPhotovideos-table').jqGrid('GridUnload');
                    } else {
                        jqGridPhotovideos('All');
                    }

                    if ($("#propertFileLibrary-table")[0].grid) {
                        $('#propertFileLibrary-table').jqGrid('GridUnload');
                    } else {
                        jqGridFileLibrary('All');
                    }
                    if ($("#key-table")[0].grid) {
                        $('#key-table').jqGrid('GridUnload');
                    } else {
                        jqGrid('All', true);
                    }
                    if ($("#trackkey-table")[0].grid) {
                        $('#trackkey-table').jqGrid('GridUnload');
                    } else {
                        jqGridTrackkey('All', true);
                    }

                    if ($("#blacklisted-table")[0].grid) {
                        $('#blacklisted-table').jqGrid('GridUnload');
                    } else {
                        jqGridBVendors('All', true);
                    }

                    if ($("#vendors-table")[0].grid) {
                        $('#vendors-table').jqGrid('GridUnload');
                    } else {
                        jqGridVendors('All', true);
                    }

                    if ($("#notes-table")[0].grid) {
                        $('#notes-table').jqGrid('GridUnload');
                    } else {
                        jqGridNotes('All', true);
                    }

                    if ($("#insurance-table")[0].grid) {
                        $('#insurance-table').jqGrid('GridUnload');
                    } else {
                        jqGridInsurance('All', true);
                    }

                    if ($("#fixtures-table")[0].grid) {
                        $('#fixtures-table').jqGrid('GridUnload');
                    } else {
                        jqGridFixtures('All', true);
                    }

                }else if (response.status == 'warning' && response.code == 500) {
                    toastr.warning(response.message);
                }


                else if (response.status == 'error' && response.code == 400) {


                    $('.error').html('');
                    $.each(response.data, function (key, value) {

                        $('.' + key).html(value);
                    });
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                console.log(response);
            }
        });
    }


    //add portfolo popup
    $(document).on('click', '#NewportfolioPopupSave', function (e) {
        e.preventDefault();

        var formData = $('#NewportfolioPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-portfolio-popup',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addPortfolioPopup'},
            beforeSend: function (xhr) {


                // checking portfolio validations
                $(".customValidatePortfoio").each(function () {
                    console.log(validations(this));
                    var res = validations(this);

                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewportfolioPopup').hide(500);
                    toastr.success(response.message);
                    fetchAllPortfolio(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Portfolio already exists!')
                        toastr.warning(response.message);
                    return false;
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });
});
//add manager popup
$(document).on('click', '#NewmanagerPopupSave', function (e) {
    e.preventDefault();
    var formData = $('#NewmanagerPopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/add-manager-popup',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addManagerPopup'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateManager").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#dynamic_manager').html('');
                var optionHtml = '<select name="manager_id" class="form-control select_manager" multiple="multiple">';
                $.each(response.getdata, function (key, value) {
                    console.log(value);
                    if (response.selected == value.id) {
                        var selected = 'selected="selected"';
                    } else {
                        var selected = '';
                    }
                    if (value.last_name) {
                        optionHtml += '<option ' + selected + ' value="' + value.id + '">' + value.first_name + ' ' + value.last_name + '</option>';
                    } else {
                        optionHtml += '<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>';
                    }

                });
                $('#dynamic_manager').html(optionHtml);
                $('.select_manager').multiselect({
                    includeSelectAllOption: true,
                    nonSelectedText: 'Manager Name'
                });
                $('#NewmanagerPopup').hide(500);
                toastr.success(response.message);
            }
            else if(response.status == 'error' && response.code == 500){
                if (response.message == 'Email already exists!')
                    toastr.warning(response.message);
                return false;
            }  else if(response.status == 'error' && response.code == 400){
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.'+key).html(value);
                });
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});




$(document).on('click', ".accounttypeplus", function (e) {
    e.preventDefault();
    $(this).parent().parent().find('.NewaccountType').show();
});
$(document).on('click', ".accounttypeplusSave", function (e) {
    e.preventDefault();
    //var formData = $('.NewaccountType :input').serializeArray();
    var formData = $(this).parent().parent().parent().parent().parent().find('.NewaccountType :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addaccountType'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateAccountType").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                //$("#NewaccountType").hide();
                $('.NewaccountType').hide();
                fetchAllAccountType(response.lastid);
                toastr.success(response.message);
            } else if (response.status == 'error' && response.code == 500) {
                $('.error').html('');
                toastr.warning(response.message);
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });
});
//chart of account functionality
$(document).on("click", "#add_chart_account_cancel_btn", function (e) {
    e.preventDefault();
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $("#add_chart_account_div").hide(500);
            }
        }
    });
});

function accountTypeList() {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'getAllAccountType',
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                $('#account_type_id').empty().append("<option value=''>Select</option>");
                $.each(response.data, function (key, value) {

                    $('#account_type_id').append($("<option data-rangefrom=" + value.range_from + " data-rangeto =" + value.range_to + " value = " + value.id + ">" + value.account_type_name + "</option>"));
                });
            } else {
                toastr.error(response.message);
            }
        }
    });
}

function accountSubTypeList(accountSubType) {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'getAllAccountSubType',
            id: accountSubType,
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                $('#sub_account').empty().append("<option value=''>Select</option>");
                $.each(response.data, function (key, value) {
                    $('#sub_account').append($("<option value = " + value.id + ">" + value.account_sub_type + "</option>"));
                })
            } else {
                toastr.error(response.message);
            }
        }
    });
}
$(document).on('change', '#account_type_id', function () {
    var accountSubType = this.value;
    console.log('range>>>>', $('#account_type_id').find(':selected').attr('data-rangefrom'));
    accountSubTypeList(accountSubType);
});

$(document).on("submit", "#add_chart_account_form_id", function (e) {
    e.preventDefault();
    if ($('#add_chart_account_form_id').valid()) {
        var account_type_id = $('#account_type_id').val();
        var account_code = $('#account_code').val();
        var account_name = $("#com_account_name").val();
        var reporting_code = $("#reporting_code").val();
        var sub_account = $("#sub_account").val();
        var status = $("#status").val();
        var range_from = $('#account_type_id').find(':selected').attr('data-rangefrom');
        var range_to = $('#account_type_id').find(':selected').attr('data-rangeto');
        var posting_status = $("#posting_status").val();
        var chart_account_edit_id = $("#chart_account_edit_id").val();

        var formData = {
            'account_type_id': account_type_id,
            'account_code': account_code,
            'account_name': account_name,
            'reporting_code': reporting_code,
            'sub_account': sub_account,
            'posting_status': posting_status,
            'status': status,
            'range_from': range_from,
            'range_to': range_to,
            'chart_account_edit_id': chart_account_edit_id
        };
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                class: 'propertyDetail',
                action: 'insertChartofaccount',
                form: formData
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {

                    $("#add_chart_account_form_id").trigger('reset');
                    $("#chartofaccountmodal").modal('hide');

                    fetchChartAccounts(response.lastid);
                    toastr.success(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 503) {
                    toastr.warning(response.message);
                }
            }
        });
    }
});
//add attach group popup
$(document).on('click', '#NewattachPopupSave', function (e) {
    e.preventDefault();
    var formData = $('#NewattachPopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/add-attachgroup-popup',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addAttachgroupPopup'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateGroup").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#dynamic_groups').html('');
                var optionHtml = '<select name="attach_groups" class="form-control attach_groups" multiple>';
                $.each(response.getdata, function (key, value) {
                    if (response.selected == value.id) {
                        var selected = 'selected="selected"';
                    } else {
                        var selected = '';
                    }
                    optionHtml += '<option ' + selected + ' value="' + value.id + '">' + value.group_name + '</option>';

                });
                $('#dynamic_groups').html(optionHtml);
                $('.attach_groups').multiselect({includeSelectAllOption: true, nonSelectedText: 'Property Groups'});
                $('#NewattachPopup').hide(500);
                toastr.success(response.message);
            } else if (response.status == 'error' && response.code == 500) {
                if (response.message == 'Property Group already exists!')
                    bootbox.alert(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});
//add property type popup
$(document).on('click', '#NewtypePopupSave', function (e) {
    e.preventDefault();
    var formData = $('#NewtypePopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/add-propertytype-popup',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addPropertytpePopup'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidatePropertyType").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#NewtypePopup').hide(500);
                toastr.success(response.message);
                fetchAllPropertytype(response.lastid);
            } else if (response.status == 'error' && response.code == 500) {
                if (response.message == 'Property Type already exists!')
                    bootbox.alert(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});
//add property style popup
$(document).on('click', '#NewpstylePopupSave', function (e) {
    e.preventDefault();
    var formData = $('#NewpstylePopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/add-propertystyle-popup',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addPropertystylePopup'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidatePropertyStyle").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#NewpstylePopup').hide(500);
                toastr.success(response.message);
                fetchAllPropertystyle(response.lastid);

            } else if (response.status == 'error' && response.code == 500) {
                if (response.message == 'Property Style already exists!')
                    bootbox.alert(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});
//add property sub type popup
$(document).on('click', '#NewsubtypePopupSave', function (e) {
    e.preventDefault();
    var formData = $('#NewsubtypePopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/add-propertysubtype-popup',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addPropertysubtypePopup'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidatePropertySubType").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#NewsubtypePopup').hide(500);
                toastr.success(response.message);
                fetchAllPropertysubtype(response.lastid);

            } else if (response.status == 'error' && response.code == 500) {
                if (response.message == 'Property Sub-Type already exists!')
                    toastr.warning(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

//add garage avail popup
$(document).on('click', '#NewgaragePopupSave', function (e) {
    e.preventDefault();
    var formData = $('#NewgaragePopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/add-garageavail-popup',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addGarageavailPopup'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateGarageAvailable").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#NewgaragePopup').hide(500);
                toastr.success(response.message);
                fetchAllgarage(response.lastid);
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});
//clone bank detail tab
$(document).on("click", ".bankdetailClonePlus", function () {
    var newnode = $('#BankDetailCloneId').clone().find("input:text,textarea").val("").end().insertAfter("div.BankDetailCloneDiv:last");
    $(".bankdetailClonePlus:not(:eq(0))").after('<div class="email-add-remove-row remove-icon add-icon delete-wholesection"><i aria-hidden="true" class="fa fa-times-circle addremoveBankAccount" style="color: red;"></i></div>');
    $(".bankdetailClonePlus:not(:eq(0))").remove();
    // $(".is_default").prop('checked',false);
    // $(".input_default").val('0');
});
$(document).on("click", ".addremoveBankAccount", function () {
    $(this).parents(".BankDetailCloneDiv").remove();
});
//clone for link owner tab grey box
$(document).on("click", "#multiplegreybox", function () {
    var total = 0;
    $(".percentage_owned").each(function(key,value){
        total += parseFloat($(value).val());
    });
    if(total < 100) {
        var newnode = $("#add_owners_div").clone().insertAfter("div.owners_div:last");
        $(".grey-box-add-inner:not(:eq(0))  #multiplegreybox").remove();
        $(".grey-box-add-inner:not(:eq(0))  #multiplegreycross").show();
        newnode.find('.percentage_owned').next().html('');
        var actual_count = 100 - total;
        newnode.find(".percentage_owned").val(actual_count);
    }
});
//clone for property insurance
$(document).on("click", '#propertyInsurancePlus', function (e) {
    var length = $('.grey-box-add-inner').length;
    var newinsuranceDiv = $("#insuranceOuterDivId .grey-box-add-inner").clone();
    newinsuranceDiv.find('.propertyInsurancePlus').remove();
    newinsuranceDiv.find('.propertyInsuranceCross').show();
    newinsuranceDiv.find('.customValidateNewInsurance').attr('class', 'form-control customValidateNewInsurance' + length);
    newinsuranceDiv.find('.customValidateNewPolicy').attr('class', 'form-control customValidateNewPolicy' + length);
    newinsuranceDiv.find('.NewInsurancePopupSave').attr('data_id', length);
    newinsuranceDiv.find('.NewPolicyPopupSave').attr('data_id', length);
    newinsuranceDiv.insertAfter("#insuranceOuterDivId:last");
    newinsuranceDiv.find('input').val(' ');
    newinsuranceDiv.find('.ins_start_date').each(function () {
        $(this).removeAttr('id').removeClass('hasDatepicker'); // added the removeClass part.
        $('.ins_start_date').datepicker({dateFormat: datepicker, defaultTime: new Date()});
    });
    newinsuranceDiv.find('.ins_end_date').each(function () {
        $(this).removeAttr('id').removeClass('hasDatepicker'); // added the removeClass part.
        $('.ins_end_date').datepicker({dateFormat: datepicker});
    });
    newinsuranceDiv.find('.ins_renewal_date').each(function () {
        $(this).removeAttr('id').removeClass('hasDatepicker'); // added the removeClass part.
        $('.ins_renewal_date').datepicker({dateFormat: datepicker});
    });
    $('.NewInsurancePopupSave').val('Save');
    $('.NewInsuranceCancel ').val('Cancel');
    $('.NewPolicyPopupSave').val('Save');
    $('.NewPolicyPopupCancel').val('Cancel');
});
$(document).on("click", '#propertyInsuranceCross', function (e) {
    $(this).parents('.grey-box-add-inner').remove();
    $("#propertyInsurancePlus").show();

});

$(document).on("click", "#multiplegreycross", function () {
    $(this).closest("div.owners_div").remove();
    var data = '';
    $(".ownerSelectclass").each(function () {

        if ($('option:selected', this).text() != 'Select') {
            if (data == '') {
                data += $('option:selected', this).text();
            } else {
                data += ", " + $('option:selected', this).text();
            }
        }
    });
    $('.vendor_idclass').val(data);

});
$(document).on("click", "#AddPropertyInsuranceSave1", function (e) {
    $('#AddPropertyInsuranceInput').val('save');
    $("#addPropertyInsurance").submit();
    return false;
});
$(document).on("click", "#AddPropertyInsuranceSave2", function (e) {
    $('#AddPropertyInsuranceInput').val('savenext');
    $("#addPropertyInsurance").submit();
    return false;
});
$("#addPropertyInsurance").on("submit", function (e) {
    e.preventDefault();
    if ($('#addPropertyInsurance').valid()) {
        var property_id1 = $("#property_unique_id").val();
        var propertyEditid = $("#property_editunique_id").val();
        var property_ins_id = $("#PropertyInsuranceid").val();
        var form = $('#addPropertyInsurance')[0];
        var formData = new FormData(form);
        formData.append('action', 'addPropertyinsurance');
        formData.append('class', 'propertyDetail');
        formData.append('property_id', property_id1);
        formData.append('propertyEditid', propertyEditid);
        formData.append('property_ins_id', property_ins_id);
        var button_value = $('#AddPropertyInsuranceInput').val();

        $.ajax({
            type: 'post',
            url: '/add-propertyinsurance',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                var response = JSON.parse(result);

                if ((response.status == 'success') && (button_value == 'save')) {
                    $("#PropertyInsuranceid").val(response.id);
                    toastr.success('This record saved successfully.');
                    $("#insuranceOuterDivOpen").hide(500);


                    setTimeout(function () {
                        jQuery('#insurance-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#insurance-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 500);

                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success('This record saved successfully.');
                    $("#12collapseTwelve").trigger('click');
                    $("#addPropertyInsurance").trigger('reset');
                    $("#insuranceOuterDivOpen").hide();

                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
                $("#insurance-table").trigger('reloadGrid');
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});



//add warranty info tab

$(".last_renovation").datepicker({dateFormat: datepicker});
$("#maintenance_reminder").datepicker({dateFormat: datepicker});
$("#warranty_expiration_date").datepicker({dateFormat: datepicker});
$("#insurance_expiration_date").datepicker({dateFormat: datepicker});
$(document).on("click", "#warrantyinforClickB", function (e) {
    $('#warrantyinforClick').val('save');
    $("#addwarrantyinforform").submit();
    return false;
});
$(document).on("click", "#warrantyinforClickB1", function (e) {
    $('#warrantyinforClick').val('savenext');
    $("#addwarrantyinforform").submit();
    return false;
});
$("#addwarrantyinforform").on("submit", function (e) {

    e.preventDefault();
    if ($('#addwarrantyinforform').valid()) {
        var formData = $('#addwarrantyinforform :input').serializeArray();
        var button_value = $('#warrantyinforClick').val();
        var propertyEditid = $("#property_editunique_id").val();

        $.ajax({
            type: 'post',
            url: '/add-warrantyinfomation',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addWarrantyinfomation',
                property_id: property_unique_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {
                    setTimeout(function () {
                        jQuery('#fixtures-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#fixtures-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 500);
                    toastr.success('This record saved successfully.');
                    $("#warrantyinfoDivopen").hide(500);

                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success('This record saved successfully.');
                    $('#13collapseThirteen').trigger("click");
                    $("#warrantyinfoDivopen").hide();

                } else if (response.code == 500) {
                    toastr.warning(response.message);
                } else if (response.status == 'failed') {
                    toastr.success('failed.Please try again.');
                }
                $("#fixtures-table").trigger('reloadGrid');
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});
function jqGridFixtures(status, deleted_at) {

    var propertyedit = $("#property_editunique_id").val();
    var propertyunique = $("#property_unique_id").val();
    if (propertyedit == undefined) {
        var property = propertyunique;
    } else {
        var property = propertyedit;
    }
    var table = 'warranty_information';
    var columns = ['Key Fixture Name', 'Fixture Type', 'Maintenance Reminder', 'Assessed Age', 'Model', 'Warranty Expiration', 'Insurance Expiration', 'Condition', 'Status', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [{table: 'warranty_information', column: 'fixture_type', primary: 'id', on_table: 'fixture_types'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'property_id', value: property, condition: '='}];
    var extra_columns = ['warranty_information.deleted_at', 'warranty_information.updated_at'];
    var columns_options = [
        {name: 'Key Fixture Name', index: 'key_fixture_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Fixture Type', index: 'fixture_type', width: 200, align: "center", searchoptions: {sopt: conditions}, table: 'fixture_types'},
        {name: 'Maintenance Reminder', index: 'maintenance_reminder', company_insurance_typewidth: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Assessed Age', index: 'assessed_age', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Model', index: 'model', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Warranty Expiration', index: 'warranty_expiration_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Insurance Expiration', index: 'insurance_expiration_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Condition', index: 'condition_fixture', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Status', index: 'status', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: statusFormatter},
        {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table}
    ];
    var ignore_array = [];
    jQuery("#fixtures-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'warranty_information.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Fixtures",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
    );
}
$(document).on('change', '#fixtures-table .select_options', function () {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var formData = $('#addwarrantyinforform :input').serializeArray();
    $('#warrantyinforClickB').text('Update');
    if (opt == 'Edit' || opt == 'EDIT') {

        $("#propertyInsurancePlus").hide();
        $("#warrantyinfoDivopen").show(500);
        $.ajax({
            type: 'post',
            url: '/get-warrantyInfo',
            data: {
                form: formData,
                id: id,
                class: 'propertyDetail',
                action: 'getWarrantyInfo'},
            success: function (response) {
                $('#warrantyinforClickB').val('Update');
                $("#Warrantyinfoid").val(id);
                var data = $.parseJSON(response);
                if (data.code == 200) {
                    $("#key_fixture_name").val(data.data.key_fixture_name);
                    $("#fixture_type").val(data.data.fixture_type);
                    $('#model').val(data.data.model);
                    $('#assessed_age').val(data.data.assessed_age);
                    $('#maintenance_reminder').val(data.maintenance_reminder);
                    $('#warranty_expiration_date').val(data.warranty_expiration_date);
                    $('#insurance_expiration_date').val(data.insurance_expiration_date);
                    $('#manufacturer_name').val(data.data.manufacturer_name);
                    $('#condition_fixture').val(data.data.condition_fixture);
                    $('#manufacturer_phonenumber').val(data.data.manufacturer_phonenumber);
                    $('#add_notes').val(data.data.add_notes);

                    setTimeout(function(){ edit_date_time(data.data.updated_at);  }, 2000);



                    defaultFormData = $('#addwarrantyinforform').serializeArray();

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        $("#fixtures-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/delete-warrantyInfo',
                        data: {
                            id: id,
                            class: 'propertyDetail',
                            action: 'deleteWarrantyInfo'
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else if (response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $('#fixtures-table').trigger('reloadGrid');
                        }
                    });
                }
                $('#fixtures-table').trigger('reloadGrid');
            }
        });
    }
});

//add fixture functionality
$(document).on('click', '#FixturePopupPlus', function () {
    $("#AddFixturePopup").show();
});
$(document).on('click', '#FixturePopupSave', function (e) {
    e.preventDefault();
    var formData = $('#AddFixturePopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/add-fixturetype',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addFixturetype'},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateAddFixture").each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#AddFixturePopup').hide(500);
                fetchAllFixture(response.lastid)
                toastr.success(response.message);

            } else if (response.status == 'error' && response.code == 500) {
                if (response.message == 'Fixture Type already exists!')
                    toastr.warning(response.message);
                return false;
            }else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }
        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

//add owner preferred vendor
$(document).on("click", "#owneredPreferredClickR", function () {

    var id = [];
    $('.checkboxgrid:checkbox:checked').each(function (i) {
        id[i] = $(this).val();
    });
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            id: id,
            class: 'propertyDetail',
            action: 'deleteVendor'
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
            } else if (response.code == 500) {
                toastr.warning(response.message);
            } else {
                toastr.error(response.message);
            }
            $('#vendors-table').trigger('reloadGrid');
        }
    });


});
$(document).on("click", "#owneredPreferredClickS", function (e) {
    $('#owneredPreferredClickval').val('save');
    $("#owneredPreferredForm").submit();
    return false;
});
$(document).on("click", "#owneredPreferredClickSN", function (e) {
    $('#owneredPreferredClickval').val('savenext');
    $("#owneredPreferredForm").submit();
    return false;
});
$("#owneredPreferredForm").on("submit", function (e) {

    e.preventDefault();
    if ($('#owneredPreferredForm').valid()) {
        var formData = $('#owneredPreferredForm :input').serializeArray();
        var button_value = $('#owneredPreferredClickval').val();
        var propertyEditid = $("#property_editunique_id").val();
        var property_id = $("#property_unique_id").val();
        var owner_pre_vendorID = $(".owner_pre_vendorID").val();

        $.ajax({
            type: 'post',
            url: '/add-owneredvendors',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addOwneredvendors',
                propertyEditid: propertyEditid,
                property_id: property_id,
                owner_pre_vendorID:owner_pre_vendorID
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success')) {
                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';
                    } else {
                        toastr.success(response.message);

                    }
                    $("#vendors-table").trigger('reloadGrid');

                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success('This record saved successfully.');
                    $("#vendors-table").trigger('reloadGrid');
                    $('#15collapseFifteen').trigger("click");

                } else if (response.status == 'failed') {
                    toastr.warning('failed.Please try again.');
                } else if (response.code == 500) {
                    toastr.warning('Vendors already exits!');
                }


            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});
//jquery grid for property insurance
function jqGridVendors(status, deleted_at) {

    var propertyedit = $("#property_editunique_id").val();
    var propertyunique = $("#property_unique_id").val();
    if (propertyedit == undefined) {
        var property = propertyunique;
    } else {
        var property = propertyedit;
    }
    var table = 'owner_blacklist_vendors';
    var columns = [' ', 'Vendor', 'Address'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'status', value: "1", condition: '='}, {column: 'property_id', value: property, condition: '='}];
    var extra_columns = ['owner_blacklist_vendors.deleted_at', 'owner_blacklist_vendors.updated_at'];
    var columns_options = [
        {name: ' ', index: 'id', width: 100, align: "center", editoptions: {value: "True:False"}, editrules: {required: true},
            formatter: function (cellvalue, options, rowObject) {
                return '<input type="checkbox" class="checkboxgrid" value=' + cellvalue + '>';
            },
            formatoptions: {disabled: false}, editable: true, searchoptions: {sopt: conditions}, table: table},
        {name: 'Vendor', index: 'vendor_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Address', index: 'vendor_address', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
    ];
    var ignore_array = [];
    jQuery("#vendors-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: false,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: false,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: false,
        headertitles: false,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "",
        pginput: false,
        pgbuttons: false,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: false,
            filterable: false,
            refreshtext: "",
            reloadGridOptions: {fromServer: false}
        }
    }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
    );
}

// add blacklist vendors tab
$(document).on("click", "#BlacklistVendorClickR", function () {

    var id = [];
    $(':checkbox:checked').each(function (i) {
        id[i] = $(this).val();
    });
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            id: id,
            class: 'propertyDetail',
            action: 'deleteBlacklistVendor'
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
            } else if (response.code == 500) {
                toastr.warning(response.message);
            } else {
                toastr.error(response.message);
            }
            $('#blacklisted-table').trigger('reloadGrid');
        }
    });


});
$(document).on("click", "#BlacklistVendorClickS", function (e) {
    $('#BlacklistVendorClickval').val('save');
    $("#blacklistvendorForm").submit();
    return false;
});
$(document).on("click", "#BlacklistVendorClickSN", function (e) {
    $('#BlacklistVendorClickval').val('savenext');
    $("#blacklistvendorForm").submit();
    return false;
});
$("#blacklist_Date").datepicker({dateFormat: datepicker});
$("#blacklistvendorForm").on("submit", function (e) {

    e.preventDefault();
    if ($('#blacklistvendorForm').valid()) {
        var formData = $('#blacklistvendorForm :input').serializeArray();
        var button_value = $('#BlacklistVendorClickval').val();
        var propertyEditid = $("#property_editunique_id").val();
        var property_id = $("#property_unique_id").val();
        var blacklist_vendorID = $(".blacklist_vendorID").val();



        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addBlacklistvendors',
                propertyEditid: propertyEditid,
                property_id: property_id,
                blacklist_vendorID:blacklist_vendorID
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success')) {
                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';
                    } else {
                        toastr.success(response.message);
                    }
                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success('This record saved successfully.');
                    $('#16collapseSixteen').trigger("click");


                            var $grid = $("#propertPhotovideos-table").setGridWidth($(window).width()-70);


                } else if (response.status == 'failed') {
                    toastr.warning('failed.Please try again.');
                } else if (response.code == 500) {
                    toastr.warning('Vendors already exits!');
                }
                $("#blacklisted-table").trigger('reloadGrid');
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }

});
//jquery grid for property insurance
function jqGridBVendors(status, deleted_at) {
    var propertyedit = $("#property_editunique_id").val();
    var propertyunique = $("#property_unique_id").val();
    if (propertyedit == undefined) {
        var property = propertyunique;
    } else {
        var property = propertyedit;
    }

    var table = 'owner_blacklist_vendors';
    var columns = [' ', 'Vendor', 'Address', 'Date', 'Reason for BlackListing'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'status', value: "2", condition: '='}, {column: 'property_id', value: property, condition: '='}];
    var extra_columns = ['owner_blacklist_vendors.deleted_at', 'owner_blacklist_vendors.updated_at'];
    var columns_options = [
        {name: ' ', index: 'id', width: 100, align: "center", editoptions: {value: "True:False"}, editrules: {required: true},
            formatter: function (cellvalue, options, rowObject) {
                return '<input type="checkbox" class="checkboxgrid" value=' + cellvalue + '>';
            },
            formatoptions: {disabled: false}, editable: true, searchoptions: {sopt: conditions}, table: table},
        {name: 'Vendor', index: 'vendor_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Address', index: 'vendor_address', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Date', index: 'blacklist_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Reason for BlackListing', index: 'blacklist_reason', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
    ];
    var ignore_array = [];
    jQuery("#blacklisted-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: false,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: false,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: false,
        headertitles: false,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "",
        pginput: false,
        pgbuttons: false,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: false,
            filterable: false,
            refreshtext: "",
            reloadGridOptions: {fromServer: false}
        }
    }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
    );
}
function buildingComplaints(status) {
    var propertyEditid = $("#property_editunique_id").val();
    var table = 'complaints';
    var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">', 'Complaint Id', 'Complaint Type', 'Notes', 'Date', 'Other', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = ['complaints.status'];
    var extra_where = [{column: 'object_id', value: propertyEditid, condition: '='}, {column: 'module_type', value: "property", condition: '='}];
    var columns_options = [
        {name: 'Id', index: 'id', width: 170, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
        {name: 'Complaint Id', index: 'complaint_id', width: 170, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Complaint Type', index: 'complaint_type', width: 170, align: "center", searchoptions: {sopt: conditions}, table: "complaint_types"},
        {name: 'Notes', index: 'complaint_note', width: 170, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Date', index: 'complaint_date', width: 170, searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Other', index: 'other_notes', width: 250, align: "center", searchoptions: {sopt: conditions}, search: false, table: table},
        {name: 'Action', index: 'select', title: false, width: 170, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table, formatter:actionsFmatter}
    ];
    var ignore_array = [];
    jQuery("#building-complaints").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'complaints.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Complaints",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
    );
}
function actionCheckboxFmatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {

        var data = '';
        var data = '<input type="checkbox" name="complaint_checkbox[]" class="complaint_checkbox" id="complaint_checkbox_' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
        return data;
    }
}


function actionsFmatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        var editable = $(cellvalue).attr('editable');
        var select = '';
        select = ['Edit', 'Delete'];
        var data = '';

        if (select != '') {

            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if (editable == '0' && (val == 'delete' || val == 'Delete')) {
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

//track key lising

function jqGridTrackkey(status, deleted_at) {

    var propertyedit = $("#property_editunique_id").val();
    var propertyunique = $("#property_unique_id").val();
    if (propertyedit == undefined) {
        var property = propertyunique;
    } else {
        var property = propertyedit;
    }
    var table = 'track_key';
    var columns = ['Name', 'Email', 'Company Name', 'Phone#', 'Pick Up Date', 'Pick Up Time', 'Return Date', 'Return Time', 'Key Designator', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'property_id', value: property, condition: '='}];
    var extra_columns = ['track_key.deleted_at', 'track_key.updated_at'];
    var columns_options = [
        {name: 'Name', index: 'key_name', width: 100, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Email', index: 'trackemail', width: 400, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Company Name', index: 'company_name', width: 400, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Phone#', index: 'phone', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Pick Up Date', index: 'pick_up_date', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Pick Up Time', index: 'pick_up_time', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'time'},
        {name: 'Return Date', index: 'return_date', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Return Time', index: 'return_time', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'time'},
        {name: 'Key Designator', index: 'key_designator', title: false, width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table}

    ];
    var ignore_array = [];
    jQuery("#trackkey-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Track Key",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
    );
}

$(document).on('click','#linkOwnerLandlord',function(){
   $('#addOwnerForm').submit(); 
});

$(document).on("click",'#3collapseThree',function () {
    portfolio_default_id = $("#portfolio_name_options").val();
    $(".is_default").prop('checked',true);
    $(".input_default").val('1');
    fetchAllAccountName(portfolio_default_id,false);

})
//add link owner tab
$("#addOwnerForm").on("submit", function (e) {
    e.preventDefault();
    var total_percentage = 0;
    var inputs_percentage = $(".percentage_owned");
    for (var i = 0; i < inputs_percentage.length; i++) {

        total_percentage += parseInt($(inputs_percentage[i]).val());
        // return false;
    }
    if (total_percentage > 100) {
        toastr.warning('Percentage cannot be greater than 100%.');
        return false;
    }



    if ($('#addOwnerForm').valid()) {
        var propertyEditid = $("#property_editunique_id").val();
        var property_id = $("#property_unique_id").val();
        var user_id = [];
        $(".ownerSelectclass").each(function () {
            user_id.push($(this).val());
        });
        var formData = $('#addOwnerForm').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-owner-tab',
            data: {form: formData,
                user_id: user_id,
                class: 'propertyDetail',
                action: 'addOwnertab',
                property_id: property_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if (response.status == 'success' && response.code == 200) {
                    if ($("#saveandnextowner").val() == 1) {
                        $('#3collapseThree').trigger("click");
                        portfolio_default_id = $("#portfolio_name_options").val();
                        $(".is_default").prop('checked',true);
                        $(".input_default").val('1');
                        fetchAllAccountName(portfolio_default_id,false);
                        // alert(portfolio_default_id);
                    }
                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';

                    } else {
                        toastr.success(response.message);
                    }


                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    toastr.warning(response.message);

                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});

//apply one time and daily fee management

$("#apply_one_time_id").change(function () {
    if (this.checked) {
        $("#apply_daily_id").prop('checked', false);
        $("#daily_flat_fee").attr('disabled', true);
        $("#daily_monthly_fee").attr('disabled', true);
        $("#daily_flat_fee_desc").attr('disabled', true);
        $("#daily_monthly_fee_desc").attr('disabled', true);
        $("#daily_flat_fee_desc").val('');
        $("#daily_monthly_fee_desc").val('');


        $("#one_time_flat_fee").attr('disabled', false);
        $("#one_time_flat_fee").prop('checked', true);
        $("#one_time_fee_radio").attr('disabled', false);
        $("#one_time_flat_fee_desc").attr('disabled', false);
        $("#one_time_monthly_fee_desc").attr('disabled', true);
        $('#grace_period').attr('disabled', false);

    } else {
        $("#apply_daily_id").prop('checked', true);
        $("#one_time_flat_fee").attr('disabled', true);
        $("#one_time_fee_radio").attr('disabled', true);
        $("#one_time_flat_fee_desc").attr('disabled', true);
        $("#one_time_monthly_fee_desc").attr('disabled', true);

        $("#daily_flat_fee").attr('disabled', false);
        $("#daily_monthly_fee").attr('disabled', false);
        $("#daily_flat_fee_desc").attr('disabled', false);
        $("#daily_monthly_fee_desc").attr('disabled', false);
    }
});

$("#apply_daily_id").change(function () {
    if (this.checked) {
        $("#apply_one_time_id").prop('checked', false);
        $("#one_time_flat_fee").attr('disabled', true);
        $("#one_time_fee_radio").attr('disabled', true);
        $("#one_time_flat_fee_desc").val('');
        $("#one_time_flat_fee_desc").attr('disabled', true);
        $("#one_time_monthly_fee_desc").attr('disabled', true);
        $("#one_time_monthly_fee_desc").val('');


        $("#daily_flat_fee").attr('disabled', false);
        $("#daily_flat_fee").prop('checked', true);
        $("#daily_monthly_fee").attr('disabled', false);
        $("#daily_flat_fee_desc").attr('disabled', false);
        $("#daily_monthly_fee_desc").attr('disabled', true);
        $('#grace_period').attr('disabled', false);


    } else {
        $("#apply_one_time_id").prop('checked', true);
        $("#daily_flat_fee").attr('disabled', true);
        $("#daily_monthly_fee").attr('disabled', true);
        $("#daily_flat_fee_desc").attr('disabled', true);
        $("#daily_monthly_fee_desc").attr('disabled', true);

        $("#one_time_flat_fee").attr('disabled', false);
        $("#one_time_fee_radio").attr('disabled', false);
        $("#one_time_flat_fee_desc").attr('disabled', false);
        $("#one_time_monthly_fee_desc").attr('disabled', false);
    }
});

$("#one_time_flat_fee").change(function () {
    if (this.checked) {
        $("#one_time_monthly_fee_desc").attr("disabled", true);
        $("#one_time_flat_fee_desc").attr("disabled", false);
        $("#one_time_monthly_fee_desc").val('');
    }
});

$("#one_time_fee_radio").change(function () {
    if (this.checked) {
        $("#one_time_flat_fee_desc").attr("disabled", true);
        $("#one_time_monthly_fee_desc").attr("disabled", false);
        $("#one_time_flat_fee_desc").val('');


    }
});

$("#daily_flat_fee").change(function () {
    if (this.checked) {
        $("#daily_flat_fee_desc").attr("disabled", false);
        $("#daily_monthly_fee_desc").attr("disabled", true);
        $("#daily_monthly_fee_desc").val("");
    }
});

$("#daily_monthly_fee").change(function () {
    if (this.checked) {
        $("#daily_flat_fee_desc").attr("disabled", true);
        $("#daily_monthly_fee_desc").attr("disabled", false);
        $("#daily_flat_fee_desc").val("");

    }
});
$(document).on("click", "#AddLatefeeButton", function (e) {
    $('#LatefeebuttonClicked').val('save');
    $("#AddLateFee").submit();
    return false;
});
$(document).on("click", "#AddLatefeeButton1", function (e) {
    $('#LatefeebuttonClicked').val('savenext');
    $("#AddLateFee").submit();
    return false;
});
$("#AddLateFee").on("submit", function (e) {

    e.preventDefault();
    if ($('#AddLateFee').valid()) {
        var formData = $('#AddLateFee :input').serializeArray();
        var button_value = $('#LatefeebuttonClicked').val();
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/add-latefee',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addLatefee',
                property_id: property_unique_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {
                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';
                    } else {
                        toastr.success(response.message);
                    }
                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success(response.message);
                    $('#5collapseFive').trigger("click");


                } else if (response.status == 'failed') {
                    toastr.success('failed.Please try again.');
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});
//add properties bank details
$(document).on("click", "#owneredPreferredClickS", function (e) {
    $('#BankdetailClickval').val('save');
    $("#addBankdetailForm").submit();
    return false;
});
$(document).on("click", "#BankdetailClickSN", function (e) {
    $('#BankdetailClickval').val('savenext');
    $("#addBankdetailForm").submit();
    return false;
});
$(document).on("click", "#BankdetailClickS", function (e) {
    $('#BankdetailClickval').val('save');
    $("#addBankdetailForm").submit();
    return false;
});
$("#addBankdetailForm").on("submit", function (e) {
    e.preventDefault();
    if ($('#addBankdetailForm').valid()) {
        var property_id = $("#property_unique_id").val();
        var propertyEditid = $("#property_editunique_id").val();

        var form = $('#addBankdetailForm')[0];
        var formData = new FormData(form);
        formData.append('action', 'addBankdetails');
        formData.append('class', 'propertyDetail');
        formData.append('property_id', property_id);
        formData.append('form', $(form).serialize());
        formData.append('propertyEditid', propertyEditid);

        var button_value = $('#BankdetailClickval').val();

        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {
                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';
                    } else {
                        toastr.success(response.message);
                    }
                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success(response.message);
                    $('#4collapseFour').trigger("click");

                } else if (response.status == 'failed') {
                    toastr.warning('failed.Please try again.');
                } else if (response.code == 500) {
                    toastr.warning('Vendors already exits!');
                }
                $("#vendors-table").trigger('reloadGrid');

            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});
$(document).on('click','.is_default',function(){
    $('.input_default').val('0');
    $(this).next('input').val('1');
});

//Add key and key tracker
$(document).on("click", "#AddKeyButton", function (e) {
    $('#KeybuttonClicked').val('save');
    $("#addkeytags").submit();
    return false;
});
$(document).on("click", "#AddKeyButton1", function (e) {
    $('#KeybuttonClicked').val('savenext');
    $("#addkeytags").submit();
    return false;
});
$('.addkeytracker').change(function () {
    var rel = $(this).val();
    if (rel == 'addkey') {
        $('#trackkeyhref').hide();
        $('#addkeyhref').show();
        $("#addTrackDiv").hide();

        $('.addkeylistingstyle').show();
        $('.trackkeylistingstyle').hide();
    } else {
        $('#addkeyhref').hide();
        $('#trackkeyhref').show();
        $("#addKeyDiv").hide();
        $("#addKeyDiv").hide();
        $('.addkeylistingstyle').hide();
        $('.trackkeylistingstyle').show();
    }
});

$(document).on('click', '#addkeyhref', function () {
   $("#KeycheckoutDiv").hide();
 $("#addKeyDiv").show();
   $("#addTrackDiv").hide();
   $("#key_tag").val('');
   $(".description1 ").val('');
   $("#total_keys").val('');
   $('#AddKeyButton').text('Save');
   $('#AddKeyButton').val('save');

});


//track key
$(document).on('click', '#trackkeyhref', function () {
    $("#addTrackDiv").show();
    $("#addKeyDiv").hide();
    $('#TrackkeydetailForm').trigger('reset');
     $('#TrackKeyButton').text('Save');
    $("#pick_up_date").datepicker({dateFormat: datepicker,
        changeYear: true,
        minDate: 0,
        yearRange: "-100:+20",
        changeMonth: true,
        onClose: function (selectedDate) {
            $("#return_date").datepicker("option", "minDate", selectedDate);
        }}).datepicker("setDate", new Date());

    $("#pick_up_time").timepicker({timeFormat: 'h:mm a', defaultTime: new Date()});
     $('#key_designator').val(default_name);
    jQuery('.phone_number').mask('000-000-0000', {reverse: true});
});
//add key tag
$("#addkeytags").on("submit", function (e) {
    e.preventDefault();
    if ($('#addkeytags').valid()) {
        var formData = $('#addkeytags :input').serializeArray();
        var button_value = $('#KeybuttonClicked').val();
        var propertyEditid = $("#property_editunique_id").val();

        $.ajax({
            type: 'post',
            url: '/add-keytag',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addKeytag',
                property_id: property_unique_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {
                    setTimeout(function () {
                        jQuery('#key-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#key-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 500);
                    $("#addKeyDiv").hide(500);
                    toastr.success('This record saved successfully.');
                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success('This record saved successfully.');
                    $('#6collapseSix').trigger("click");

                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
                $("#key-table").trigger('reloadGrid');
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});
//add track key tag
$(document).on("click", "#TrackKeyButton", function (e) {
    $('#TrackbuttonClicked').val('save');
    $("#TrackkeydetailForm").submit();
    return false;
});
$(document).on("click", "#TrackKeyButton1", function (e) {
    
    $('#TrackbuttonClicked').val('savenext');
    $("#TrackkeydetailForm").submit();
    return false;
});
$("#TrackkeydetailForm").on("submit", function (e) {
    e.preventDefault();
    if ($('#TrackkeydetailForm').valid()) {
        var formData = $('#TrackkeydetailForm :input').serializeArray();
        var button_value = $('#TrackbuttonClicked').val();
        var propertyEditid = $("#property_editunique_id").val();
        var property_id = $("#property_unique_id").val();
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addTrackKeydetail',
                property_id: property_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {

                    setTimeout(function () {
                        jQuery('#trackkey-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#trackkey-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 500);
                    $("#addKeyDiv").hide(500);
                    $("#key_id").val(response.lastid);
                    toastr.success(response.message);
                    $("#addTrackDiv").hide(500);


                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    // $('#TrackkeydetailForm').trigger('reset');
                    // var trackValidator = $("#TrackkeydetailForm").validate();
                    // validator.resetForm();
                    toastr.success(response.message);
                    $('#6collapseSix').trigger("click");
                     $("#addTrackDiv").hide(500);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                }
                $("#trackkey-table").trigger('reloadGrid');
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});
function jqGrid(status, deleted_at) {

    var propertyedit = $("#property_editunique_id").val();
    var propertyunique = $("#property_unique_id").val();
    if (propertyedit == undefined) {
        var property = propertyunique;
    } else {
        var property = propertyedit;
    }
    var table = 'key_tracker';
    var columns = ['ID', 'Key Tag', 'Available Keys', 'Status', 'Description', 'Total Keys', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'property_id', value: property, condition: '='}];
    var extra_columns = ['key_tracker.deleted_at', 'key_tracker.updated_at'];
    var columns_options = [
        {name: 'id', index: 'id', width: 100, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Key Tag', index: 'key_tag', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'AvailableKeys', index: 'available_keys', width: 300, align: "center", hidden: true, searchoptions: {sopt: conditions}, table: table},
        {name: 'Status', index: 'status', width: 300, align: "center", hidden: true, searchoptions: {sopt: conditions}, table: table},
        {name: 'Description', index: 'description', width: 300, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Total Keys', index: 'total_keys', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: totalkeyFormatter},
        {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table, formatter:actionFormatter},
    ];
    var ignore_array = [];
    jQuery("#key-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Keys",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
    );
}
function actionFormatter(cellValue, options, rowObject) {
    if (rowObject !== undefined) {
        if (rowObject.Status == 1) {
            var select = ['Edit', 'Delete', 'checkout', 'return'];
            var data = '';
            if (select != '') {
                var data = '<select ' +
                        ' class="form-control select_options" data_id="' + rowObject.id + '">>><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
        var propertyEditid = $("#property_editunique_id").val();
        if (propertyEditid !== undefined) {

            var select = ['Edit', 'Delete', 'checkout'];
            var data = '';
            if (select != '') {
                var data = '<select ' +
                        ' class="form-control select_options" data_id="' + rowObject.id + '">>><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        } else {
            var select = ['Edit', 'Delete'];
            var data = '';
            if (select != '') {
                var data = '<select ' +
                        ' class="form-control select_options" data_id="' + rowObject.id + '">>><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
}
function totalkeyFormatter(cellValue, options, rowObject) {
   if (rowObject !== undefined) {
       var propertyEditid = $("#property_editunique_id").val();
       if (propertyEditid !== undefined) {
           if (rowObject.AvailableKeys == '') {
               var avail = 0
           } else {
               var avail = rowObject.AvailableKeys
           }
           var totalkeys = avail + '/' + cellValue;
           if(avail == cellValue){
           return "<span class='addcheckoutcolor'>" +totalkeys + "</span>";
       }
       else{
          return "<span class='addcheckoutcolorblue' rel="+cellValue+">" +totalkeys + "</span>";
       }
       } else {
           return cellValue;
       }
   }
}

//jquery grid for property insurance
function jqGridInsurance(status, deleted_at) {
    var propertyedit = $("#property_editunique_id").val();
    var propertyunique = $("#property_unique_id").val();
    if (propertyedit == undefined) {
        var property = propertyunique;
    } else {
        var property = propertyedit;
    }
    var table = 'property_insurance';
    var columns = ['Insurance Company', 'Insurance Type', 'Phone Number', 'Insurance Agent Name', 'Policy #', 'Coverage Amount', 'Renewal Date', 'Annual Premium', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [{table: 'property_insurance', column: 'company_insurance_type', primary: 'id', on_table: 'company_property_insurance'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column: 'property_id', value: property, condition: '='}];
    var extra_columns = ['property_insurance.deleted_at', 'property_insurance.updated_at'];
    var columns_options = [
        {name: 'Insurance Company', index: 'insurance_company_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Insurance Type', index: 'insurance_type', width: 200, align: "center", searchoptions: {sopt: conditions}, table: 'company_property_insurance'},
        {name: 'Phone Number', index: 'phone_number', company_insurance_typewidth: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Insurance Agent Name', index: 'agent_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Policy #', index: 'policy', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Coverage Amount', index: 'coverage_amount', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Renewal Date', index: 'renewal_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'Annual Premium', index: 'annual_premium', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table}
    ];
    var ignore_array = [];
    jQuery("#insurance-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Property Insurance",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
    );
}
$(document).on('change', '#insurance-table .select_options', function () {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var formData = $('#addPropertyInsurance :input').serializeArray();
    $('#AddPropertyInsuranceSave1').text('Update');
    if (opt == 'Edit' || opt == 'EDIT') {

        $("#propertyInsurancePlus").hide();
        $("#insuranceOuterDivOpen").show(500);
        $.ajax({
            type: 'post',
            url: '/get-propertyinsurance',
            data: {
                form: formData,
                id: id,
                class: 'propertyDetail',
                action: 'getPropertyinsurance'},
            success: function (response) {

                $("#addKeyDiv").show(500);
                $('#AddPropertyInsuranceSave1').val('Update');
                $("#PropertyInsuranceid").val(id);
                var data = $.parseJSON(response);
                if (data.code == 200) {
//                    console.log(data.data.policy_name);
                    setTimeout(function () {
                        $(".SelectPolicyTypesClass").val(data.data.policy_name);

                    }, 500);


                    $("#SelectInsuranceTypes").val(data.data.company_insurance_type);

                    $("#insurance_company_name").val(data.data.insurance_company_name);
                    $('#policy').val(data.data.policy);
                    $('#property_insurance_email').val(data.data.property_insurance_email);
                    $('#SelectPolicyTypes').val(data.data.total_keys);
                    $('#agent_name').val(data.data.agent_name);
                    $('#insurance_country_code').val(data.data.insurance_country_code);
                    $('#ins_phone_number').val(data.data.phone_number);
                    $('#coverage_amount').val(data.data.coverage_amount);
                    $('#annual_premium').val(data.data.annual_premium);
                    $('.ins_start_date').val(data.start_date);
                    $('.ins_renewal_date').val(data.renewal_date);
                    $('#ins_notes').val(data.data.notes);
                    $('.ins_end_date').val(data.end_date);

                    defaultFormData = $('#addPropertyInsurance').serializeArray();

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        $("#insurance-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/delete-propertyinsurance',
                        data: {
                            id: id,
                            class: 'propertyDetail',
                            action: 'deletePropertyinsurance'
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else if (response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $('#insurance-table').trigger('reloadGrid');
                        }
                    });
                }
                $('#insurance-table').trigger('reloadGrid');
            }
        });
    }
});
//jquery grid for property notes
function jqGridNotes(status, deleted_at) {

    var propertyedit = $("#property_editunique_id").val();
    var propertyunique = $("#property_unique_id").val();
    if (propertyedit == undefined) {
        var property = propertyunique;
    } else {
        var property = propertyedit;
    }

    var table = 'property_notes';
    var columns = ['ID', 'Notes', 'Created At', 'Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = ['property_notes.deleted_at', 'property_notes.updated_at'];
    var extra_where = [{column: 'property_id', value: property, condition: '='}];
    var columns_options = [
        {name: 'id', index: 'id', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Notes', index: 'notes', width: 415, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Created At', index: 'created_at', width: 415, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Action', index: 'select', title: false, width: 250, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table}
    ];
    var ignore_array = [];
    jQuery("#notes-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Property Notes",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
    );
}
function statusFormatter(cellValue, options, rowObject) {
    if (cellValue == 1)
        return "Active";
    else if (cellValue == '0')
        return "InActive";
    else
        return '';
}
function isDefaultFormatter(cellValue, options, rowObject) {
    if (cellValue == '1')
        return "Yes";
    else if (cellValue == '0')
        return "No";
    else
        return '';
}
/**  List Action Functions  */




$(document).on('click', '#cancelPortfolio_add', function () {
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#addKeyDiv').hide(500);
            }
        }
    });
});
$(document).on('change', '#key-table .select_options', function () {
    setTimeout(function(){ $(".select_options").val("default"); }, 200);
    var opt = $(this).val();
    var id = $(this).attr('data_id');

    if (opt == 'Edit' || opt == 'EDIT') {

        var formData = $('#addkeytags :input').serializeArray();
        $('#AddKeyButton').text('Update');
        $("#KeycheckoutDiv").hide();
        $("#addKeyDivdiv1").show();
        $.ajax({
            type: 'post',
            url: '/get-key',
            data: {
                form: formData,
                id: id,
                class: 'propertyDetail',
                action: 'getKey'},
            success: function (response) {

                $("#addKeyDiv").show(500);
                $('#AddKeyButton').val('Update');
                $("#key_id").val(id);
                var data = $.parseJSON(response);
                if (data.code == 200) {
                    $("#key_tag").val(data.data.key_tag);
                    $(".description1").val(data.data.description);
                    $('#total_keys').val(data.data.total_keys);

                    defaultFormData = $("#addkeytags").serializeArray();
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        $("#key-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {

        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/delete-key',
                        data: {
                            id: id,
                            class: 'propertyDetail',
                            action: 'deleteKey'
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else if (response.code == 500) {
                                toastr.warning(response.message);
                            }
                            $('#key-table').trigger('reloadGrid');
                        }
                    });
                }

            }
        });
    } else if (opt == 'checkout' || opt == 'CHECKOUT') {
       $("#KeycheckoutDiv").show();
       $("#KeycheckoutForm").trigger('reset');
         $("#addKeyDiv").hide();
          $("#checkoutid").val(id);


   } else if (opt == 'return' || opt == 'RETURN') {
        $("#return-key-div").modal('show');
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                propertyEditid: propertyEditid,
                id: id,
                class: 'propertyDetail',
                action: 'getReturnkeyData'},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.code == 200) {
                    $("#tbodyCheckOutDetail").html(response.html);
                    console.log(response.html);
                } else if (response.status == "error") {
                    toastr.error(response.message);
                } else {
                    toastr.error(response.message);
                }
                $('#key-table').trigger('reloadGrid');
            }
        });
    }
});

$("#KeycheckoutForm").on("submit", function (e) {
   e.preventDefault();
   var formData = $('#KeycheckoutForm :input').serializeArray();
   var propertyEditid = $("#property_editunique_id").val();
   var checkoutid = $("#checkoutid").val();

   $.ajax({
       type: 'post',
       url: '/property-ajax',
       data: {
           form: formData,
           id: checkoutid,
           class: 'propertyDetail',
           action: 'keyCheckout',
           propertyEditid: propertyEditid
       },
       success: function (response) {
           var response = JSON.parse(response);
           if (response.code == 200) {
               toastr.success(response.message);
                $("#KeycheckoutDiv").hide(500);
           } else if (response.code == 500) {
               toastr.error(response.message);
           } else if (response.code == 400) {
               toastr.error(response.message);
           }
       }
   });
   $('#key-table').trigger('reloadGrid');
});



//track key
$(document).on('change', '#trackkey-table .select_options', function () {

    setTimeout(function(){ $(".select_options").val("default"); }, 200);
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var formData = $('#TrackkeydetailForm :input').serializeArray();
    $('#TrackKeyButton').text('Update');
    if (opt == 'Edit' || opt == 'EDIT') {

        $("#addTrackDiv").show(500);
        $("#pick_up_date").datepicker({dateFormat: datepicker,
            changeYear: true,
            minDate: 0,
            yearRange: "-100:+20",
            changeMonth: true,
            onClose: function (selectedDate) {
                $("#return_date").datepicker("option", "minDate", selectedDate);
            }}).datepicker("setDate", new Date());

        $("#pick_up_time").timepicker({timeFormat: 'h:mm a', defaultTime: new Date()});
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                form: formData,
                id: id,
                class: 'propertyDetail',
                action: 'getTrackKey'},
            success: function (response) {

                $('#TrackKeyButton').val('Update');
                $("#trackkey_id").val(id);
                var data = $.parseJSON(response);
                if (data.code == 200) {
                    $("#_easyui_textbox_input4").val(data.data.key_name);
                    $(".edittrackemail").val(data.data.trackemail);
                    $('#trackcompany_name').val(data.data.company_name);
                    $('#trackphone').val(data.data.phone);
                    $('#trackAddress1').val(data.data.Address1);
                    $('#trackAddress2').val(data.data.Address1);
                    $('#trackAddress3').val(data.data.Address1);
                    $('#trackAddress4').val(data.data.Address1);
                    $('#trackkey_number').val(data.data.key_number);
                    $('#trackkey_quality').val(data.data.key_quality);
                    $('#pick_up_date').val(data.pick_up_date);
                    $('#pick_up_time').val(data.pick_up_time);
                    $('#return_date').val(data.return_date);
                    $('#return_time').val(data.return_time);
                    $('#key_designator').val(default_name);

                    defaultFormData =  $('#TrackkeydetailForm').serializeArray();
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        $("#trackkey-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/property-ajax',
                        data: {
                            id: id,
                            class: 'propertyDetail',
                            action: 'deleteTrackKey'
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else if (response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $('#trackkey-table').trigger('reloadGrid');
                        }
                    });
                }
                $('#trackkey-table').trigger('reloadGrid');
            }
        });
    }
});
//complaint tab
/*function to print element by id */
function PrintElem(elem)
{
    Popup($(elem).html());
}
/*function to print element by id */
function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.body).html('<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if (mywindow.close()) {

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}
/*jquery to open new complaint box */
$(document).on("click", "#new_complaint_button", function () {
    $("#new_complaint").show(500)
    $("#complaint_save").html('Save')
    $('#complaint_type_options').val('');
    $('#complaint_note').val('');
    $('#edit_complaint_id').val('');
    $("#complaint_id").val(Math.random().toString(36).slice(2).substr(0, 6).toUpperCase());
    /*date picker for complaint date */
    $("#complaint_date").datepicker({dateFormat: datepicker});

});

/*jquery to open modal for complaint box to print */
$(document).on("click", '#print_email_button', function () {
    favorite = [];
    var no_of_checked = $('[name="complaint_checkbox[]"]:checked').length
    if (no_of_checked == 0) {
        toastr.error('Please select atleast one Complaint.');
        return false;
    }
    $.each($("input[name='complaint_checkbox[]']:checked"), function () {
        favorite.push($(this).attr('data_id'));
    });

    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {class: 'propertyDetail', action: 'getComplaintsData', 'complaint_ids': favorite},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $("#print_complaint").modal('show');
                $("#modal-body-complaints").html(response.html)
            } else {
                toastr.warning('Record not updated due to technical issue.');
            }
        }
    });
});
$(document).on('click', '#complaint_cancel', function () {
    $("#new_complaint").hide(500);
})

/*date picker for complaint date */
$("#complaint_date").datepicker({dateFormat: datepicker});

/*function to save new complaint */
$(document).on('click', '#complaint_save', function (e) {

    e.preventDefault();
    var formData = $('#new_complaint_form :input').serializeArray();
    var propertyEditid = $("#property_editunique_id").val();

    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addNewComplaint',
            edit_building_id: propertyEditid
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#new_complaint').hide(500);
                $("#building-complaints").trigger('reloadGrid');
                toastr.success(response.message);
            } else if (response.status == 'error' && response.code == 500) {
                toastr.error(response.message);
                return false;
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('#' + key).text(value);
                });
            } else if (response.status == 'error' && response.code == 400) {
                toastr.warning(response.message);
            }

        },
        error: function (response) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

});

/*to show other notes text box for other notes */
$(document).on("change", "#complaint_type_options", function () {
    var complaint_type = $("#complaint_type_options option:selected").text();
    if (complaint_type == "Other") {
        $("#other_notes_div").show();
    } else {
        $("#other_notes_div").hide();
    }
});

$("#select_all_complaint_checkbox").click(function () {
    $(".complaint_checkbox").prop('checked', $(this).prop('checked'));
});



/**
 *change function to perform various actions(edit ,delete)
 * @param status
 */
jQuery(document).on('change', '#building-complaints .select_options', function () {
    var select_options = $(this).val();
    var data_id = $(this).attr('data_id');
    if (select_options == 'Edit')
    {
        $('#new_complaint :input').val('');
        $('#new_complaint').show(500);
        $('#edit_complaint_id').val(data_id);
        $('#complaint_save').html('Update');
        $('#complaint_id').prop('readonly', 'readonly');


        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {class: 'propertyDetail', action: 'getComplaintDetail', id: data_id},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {

                    $.each(response.data.data, function (key, value) {
                        $('#' + key + ':input').val(value);
                        edit_date_time(value.updated_at);
                        if (key == 'complaint_type_id' && value != '') {
                            $('#complaint_type_options option[value=' + value + ']').attr('selected', 'selected');
                        }

                    });

                    defaultFormData = $("#new_complaint_form").serializeArray();
                } else if (response.status == 'error' && response.code == 503) {
                    toastr.error(response.message);
                } else {
                    toastr.warning('Record not updated due to technical issue.');
                }
            }
        });
        // window.location.href = '/MasterData/AddPropertyType';

    } else if (select_options == 'Print Envelope') {
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {class: 'propertyDetail', action: 'getCompanyData', 'building_id': data_id},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $("#PrintEnvelope").modal('show');
                    $("#company_name").text(response.data.data.company_name)
                    $("#address1").text(response.data.data.address1)
                    $("#address2").text(response.data.data.address2)
                    $("#address3").text(response.data.data.address3)
                    $("#address4").text(response.data.data.address4)
                    $(".city").text(response.data.data.city)
                    $(".state").text(response.data.data.state)
                    $(".postal_code").text(response.data.data.zipcode)
                    $("#building_name").text(response.building.data.building_name)
                    $("#building_address").text(response.building.data.address)


                } else {
                    toastr.warning('Record not updated due to technical issue.');
                }
            }
        });

    } else if (select_options == 'Delete') {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/property-ajax',
                        data: {class: 'propertyDetail', action: 'deleteComplaint', id: data_id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success('Complaint deleted successfully.');
                            } else if (response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            } else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                $("#building-complaints").trigger('reloadGrid');
            }
        });
    }
    $('.select_options').prop('selectedIndex', 0);
});
//add management tab
$(document).on("click", "#AddManagementfeeButton", function (e) {
    $('#ManagementFeeclick').val('save');
    $("#addManagementFeeForm").submit();
    return false;
});
$(document).on("click", "#AddManagementfeeButton1", function (e) {
    $('#ManagementFeeclick').val('savenext');
    $("#addManagementFeeForm").submit();
    return false;
});
$("#addManagementFeeForm").on("submit", function (e) {
    e.preventDefault();
    if ($('#addManagementFeeForm').valid()) {
        var formData = $('#addManagementFeeForm :input').serializeArray();
        var button_value = $('#ManagementFeeclick').val();
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/add-managementfee',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addManagementFee',
                property_id: property_unique_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {
                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';
                    } else {
                        toastr.success(response.message);
                    }
                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success(response.message);
                    $('#7collapseSeven').trigger("click");

                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});

//add management info
$(document).on("click", "#AddManagementinfoButton", function (e) {
    $('#AddManagementinfoclick').val('save');
    $("#addManagementinfoForm").submit();
    return false;
});
$(document).on("click", "#AddManagementinfoButton1", function (e) {
    $('#AddManagementinfoclick').val('savenext');
    $("#addManagementinfoForm").submit();
    return false;
});
$("#management_start_date").datepicker({dateFormat: datepicker,
    changeYear: true,
    minDate: 0,
    yearRange: "-100:+20",
    changeMonth: true,
    onClose: function (selectedDate) {
        $("#management_end_date").datepicker("option", "minDate", selectedDate);
    }}).datepicker("setDate", new Date());
;
$("#management_end_date").datepicker({dateFormat: datepicker, changeYear: true,
    minDate: 0,
    yearRange: "-100:+20",
    changeMonth: true,
    onClose: function (selectedDate) {
        $("#management_start_date").datepicker("option", "maxDate", selectedDate);
    }});





$("#return_date").datepicker({dateFormat: datepicker, changeYear: true,
    minDate: 0,
    yearRange: "-100:+20",
    changeMonth: true,
    onClose: function (selectedDate) {
        $("#pick_up_date").datepicker("option", "maxDate", selectedDate);
    }});


$("#return_time").timepicker({timeFormat: 'h:mm a'});
$("#addManagementinfoForm").on("submit", function (e) {
    e.preventDefault();
    if ($('#addManagementinfoForm').valid()) {
        var formData = $('#addManagementinfoForm :input').serializeArray();
        var button_value = $('#AddManagementinfoclick').val();
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/add-managementinfo',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addManagementinfo',
                property_id: property_unique_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {
                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';
                    } else {
                        toastr.success(response.message);
                    }
                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success(response.message);
                    $('#8collapseEight').trigger("click");
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});

//add maintenance information
$(document).on("click", "#AddMaintenanceinfoButton", function (e) {
    $('#AddMaintenanceinfoclick').val('save');
    $("#addMaintenanceinformationForm").submit();
    return false;
});
$(document).on("click", "#AddMaintenanceinfoButton1", function (e) {
    $('#AddMaintenanceinfoclick').val('savenext');
    $("#addMaintenanceinformationForm").submit();
    return false;
});
$("#InsuranceExpiration").datepicker({dateFormat: datepicker});
$("#HomeWarrantyExpiration").datepicker({dateFormat: datepicker});

$("#addMaintenanceinformationForm").on("submit", function (e) {
    e.preventDefault();

    if ($('#addMaintenanceinformationForm').valid()) {
        var formData = $('#addMaintenanceinformationForm :input').serializeArray();
        var button_value = $('#AddMaintenanceinfoclick').val();
        var property_id = $("#property_unique_id").val();
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/add-maintenanceinformation',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addMaintenanceinformation',
                property_id: property_unique_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {

                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';
                    } else {
                        toastr.success(response.message);
                    }

                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success(response.message);
                    $('#9collapseNine').trigger("click");

                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});

$(".maintenance_speed_time").change(function () {
    if (this.value == 'No Spending Limit')
    {
        $("#maintenance_amount").attr('disabled', true);
    } else {
        $("#maintenance_amount").attr('disabled', false);
    }
});
//add mortgage information
$(document).on("click", "#AddMortgageinfoButton", function (e) {
    $('#AddMortgageinfoclick').val('save');
    $("#addMortgageinformationForm").submit();
    return false;
});
$(document).on("click", "#AddMortgageinfoButton1", function (e) {
    $('#AddMortgageinfoclick').val('savenext');
    $("#addMortgageinformationForm").submit();
    return false;
});
$("#mortgage_start_date").datepicker({dateFormat: datepicker});
$("#addMortgageinformationForm").on("submit", function (e) {
    e.preventDefault();

    if ($('#addMortgageinformationForm').valid()) {
        var formData = $('#addMortgageinformationForm :input').serializeArray();
        var button_value = $('#AddMortgageinfoclick').val();
        var propertyEditid = $("#property_editunique_id").val();
        $.ajax({
            type: 'post',
            url: '/add-Mortgageinfo',
            data: {form: formData,
                propertyEditid: propertyEditid,
                class: 'propertyDetail',
                action: 'addMortgageinfo',
                property_id: property_unique_id},
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {
                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';
                    } else {
                        toastr.success(response.message);
                    }
                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    toastr.success(response.message);
                    $('#10collapseTen').trigger("click");

                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});
//add property loan
$(".loan_status_class").change(function () {
    if (this.value == '1') {
        $("#loanDiv").show(500);
    } else {
        $("#loanDiv").hide(500);
    }
});

$(document).on("click", "#AddPropertyloanButton", function (e) {
    $('#AddPropertyloanclick').val('save');
    $("#addPropertyloanForm").submit();
    return false;
});
$(document).on("click", "#AddPropertyloanButton1", function (e) {
    $('#AddPropertyloanclick').val('savenext');
    $("#addPropertyloanForm").submit();
    return false;
});
$("#loan_start_date").datepicker({dateFormat: datepicker,
    changeYear: true,
    minDate: 0,
    yearRange: "-100:+20",
    changeMonth: true,
    onClose: function (selectedDate) {
        $("#loan_maturity_date").datepicker("option", "minDate", selectedDate);
    }
});
$("#loan_maturity_date").datepicker({dateFormat: datepicker,
    changeYear: true,
    minDate: 0,
    yearRange: "-100:+20",
    changeMonth: true,
    onClose: function (selectedDate) {
        $("#loan_start_date").datepicker("option", "maxDate", selectedDate);
    }});
$("#addPropertyloanForm").on("submit", function (e) {
    e.preventDefault();
    if ($('#addPropertyloanForm').valid()) {
        var formData = $('#addPropertyloanForm :input').serializeArray();
        var button_value = $('#AddPropertyloanclick').val();
        var propertyEditid = $("#property_editunique_id").val();
        var property_id = $("#property_unique_id").val();
        $.ajax({
            type: 'post',
            url: '/add-propertyloan',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addPropertyloan',
                property_id: property_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {

                    if ($("#property_editunique_id").val() !== undefined) {
                        localStorage.setItem("Message", 'The record updated successfully.');
                        localStorage.setItem('rowcolor', 'rowColor');
                        window.location.href = '/Property/PropertyModules';
                    } else {
                        toastr.success(response.message);
                    }

                } else if ((response.status == 'success') && (button_value == 'savenext')) {

                    toastr.success(response.message);
                    $('#11collapseEleven').trigger("click");
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});
// property notes
$(document).on("click", "#AddPropertynotesButton", function (e) {
    $('#AddPropertynotesclick').val('save');
    $("#addPropertynotesForm").submit();
    return false;
});
$(document).on("click", "#AddPropertynotesButton1", function (e) {
    $('#AddPropertynotesclick').val('savenext');
    $("#addPropertynotesForm").submit();
    return false;
});
$("#addPropertynotesForm").on("submit", function (e) {
    e.preventDefault();
    $('#addnotesopenDiv').hide();
    if ($('#addPropertynotesForm').valid()) {

        var property_unique_id = $("#property_unique_id").val();
        var propertyEditid = $("#property_editunique_id").val();
        var formData = $('#addPropertynotesForm :input').serializeArray();
        var button_value = $('#AddPropertynotesclick').val();

        $.ajax({
            type: 'post',
            url: '/add-propertynotes',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addPropertynotes',
                property_id: property_unique_id,
                propertyEditid: propertyEditid
            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (button_value == 'save')) {
                    if ($("#property_editunique_id").val() !== undefined) {
                                        toastr.success(response.message);

                   } else {
                        $("#addnotesopenDiv").hide(500);
                        toastr.success(response.message);
                    }

                    setTimeout(function () {
                        jQuery('#notes-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#notes-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 500);
                } else if ((response.status == 'success') && (button_value == 'savenext')) {
                    $('#19collapseNinteen').trigger("click");
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.success('failed.Please try again.');
                }
                $("#notes-table").trigger('reloadGrid');
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
});
$(document).on('change', '#notes-table .select_options', function () {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var formData = $('#addkeytags :input').serializeArray();
    $('#AddPropertynotesButton').text('Update');
    if (opt == 'Edit' || opt == 'EDIT') {
        $.ajax({
            type: 'post',
            url: '/get-notes',
            data: {
                form: formData,
                id: id,
                class: 'propertyDetail',
                action: 'getNotes'},
            success: function (response) {

                $("#addnotesopenDiv").show(500);
                $(".textarea-form").show(500);
                $('#AddPropertynotesButton').val('Update');
                $("#notes_id").val(id);
                var data = $.parseJSON(response);
                if (data.code == 200) {
                    $("#notesId").val(data.data.notes);
                    setTimeout(function(){
                        edit_date_time(data.data.updated_at);
                    }, 2000);

                    defaultFormData = $('#addPropertynotesForm').serializeArray();
                } else if (data.status == "error") {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        $("#notes-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/delete-notes',
                        data: {
                            id: id,
                            class: 'propertyDetail',
                            action: 'deleteNotes'
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else if (response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $("#notes-table").trigger('reloadGrid');
                        }
                    });
                }
                $("#notes-table").trigger('reloadGrid');
            }
        });
    }
});
$("#returnkeyForm").validate({
    rules: {
        returnCheckbox: {
            required: true
        }
    }
});
//return key checkout
$(document).on("click", "#returnbuttonId", function (e) {
    e.preventDefault();

    var val = [];

    $('.chkKeyReturn:checkbox:checked').each(function (i) {
        val[i] = $(this).val();
    });
    var propertyEditid = $("#property_editunique_id").val();
    var formData = $('#returnkeyForm :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'returnCheckout',
            val: val,
            propertyEditid: propertyEditid
        },
        success: function (result) {
            var response = JSON.parse(result);
            if (response.status == 'success') {
                $('#return-key-div').modal('hide');
                toastr.success('This record saved successfully.');
            } else if (response.status == 'close' && response.code == 500) {
                $('#return-key-div').modal('hide');
            }else {
                toastr.success('failed.Please try again.');
            }
            $("#key-table").trigger('reloadGrid');

        },
    });
});
//property insurance
$(document).on("click", ".NewInsurancePlus", function () {
    $(this).parents('.grey-box-add-inner').find('.NewInsurancePopup').show();
});

$(document).on("click", ".NewInsuranceCancel", function () {
    $(this).parents('.grey-box-add-inner').find('.NewInsurancePopup').hide();
});
$(document).on('click', '.NewInsurancePopupSave', function (e) {
    var property_unique_id=($("#property_editunique_id").val() !== undefined) ? $("#property_editunique_id").val() : $("#property_unique_id").val();
    var length = $(this).attr('data_id');
    var formData = $(this).parents('.grey-box-add-inner').find('.NewInsurancePopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/add-insurancetype',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addInsurancetype',
            property_id: property_unique_id},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateNewInsurance" + length).each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (result) {
            var response = JSON.parse(result);
            if (response.status == 'success') {
                fetchAllinsurancetypes(response.lastid);
                toastr.success('This record saved successfully.');
            } else if (response.status == 'error' && response.code == 500) {
                if (response.message == 'Insurance Type already exists')
                    toastr.warning(response.message);
                return false;
            } else {
                toastr.success('failed.Please try again.');
            }
            $('.NewInsurancePopup').hide(500);
            // $(this).parents('.grey-box-add-inner').find('.NewInsurancePopup').hide();
        }, error: function (jqXHR, status, err) {
            console.log(err);
        },
    });
});
//policy insurance
$(document).on('click', '.NewPolicyPlus', function () {
    $(this).parents('.grey-box-add-inner').find('.NewPolicyPopup').show();
});

$(document).on('click', '.NewPolicyPopupCancel', function () {
    $(this).parents('.grey-box-add-inner').find('.NewPolicyPopup').hide();
});

$(document).on('click', '#NewPolicyPlus', function (e) {
    $("#NewPolicyPopup").show();
});
$(document).on('click', '.NewPolicyPopupSave', function (e) {
    var property_unique_id= ($("#property_editunique_id").val() !== undefined) ? $("#property_editunique_id").val() : $("#property_unique_id").val();;
    var length = $(this).attr('data_id');
    var formData = $(this).parents('.grey-box-add-inner').find('.NewPolicyPopup :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/add-policytype',
        data: {form: formData,
            class: 'propertyDetail',
            action: 'addPolicytype',
            property_id: property_unique_id},
        beforeSend: function (xhr) {
            // checking portfolio validations
            $(".customValidateNewPolicy" + length).each(function () {
                var res = validations(this);
                if (res === false) {
                    xhr.abort();
                    return false;
                }
            });
        },
        success: function (result) {
            var response = JSON.parse(result);
            if (response.status == 'success') {

                fetchAllpolicytypes(response.lastid);

                toastr.success('This record saved successfully.');
            } else if (response.status == 'error' && response.code == 500) {
                toastr.warning(response.message);
            } else {
                toastr.success('failed.Please try again.');
            }
            $('.NewPolicyPopup').hide(500);
        }, error: function (jqXHR, status, err) {
            console.log(err);
        },
    });
});



$(".ins_end_date").datepicker({dateFormat: datepicker,
    changeYear: true,
    minDate: 0,
    yearRange: "-100:+20",
    changeMonth: true,
    onClose: function (selectedDate) {
        $(".ins_start_date").datepicker("option", "maxDate", selectedDate);
    }});


$(".maintenance_speed_time").change(function () {
    if (this.value == 'No Spending Limit')
    {
        $("#maintenance_amount").attr('disabled', true);
    } else {
        $("#maintenance_amount").attr('disabled', false);
    }
});
/**
 *Get location on the basis of zipcode
 */
function getAddressInfoByZip1(zip) {

    if (zip.length >= 5 && zip != 'undefined') {
        var addr = {};
        $.ajax({
            type: 'post',
            url: '/fetch-zipcode-popup',
            data: {zip: zip,
                class: 'propertyDetail',
                action: 'fetchZipcode'},
            success: function (res) {
                response1(res);

            },
        });

    } else {
        response1({success: false});
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response1(obj) {
    var objres = JSON.parse(obj);
    if (objres.status == 'success') {
        $('.zipcity').val(objres.city);
        $('.zipstate').val(objres.state);
        $('.zipcountry').val(objres.country);
    }
}

$(function () {

    $('#property_id').val(getRandomNumber(6));
    $('#portfolio_id').val(getRandomNumber(6));
    if ($('#loan_number').val() == '') {
        $('#loan_number').val(getRandomNumber(6));
    }

    // $('#city').val('Anchorage');
    // $('#state').val('Alaska ');
    // $('#country').val('United States');
    // $('#zipcode').val('99501');
    setTimeout(function(){
       $('.zipcity').val('Anchorage');
       $('.zipstate').val('Alaska ');
       $('.zipcountry').val('United States');
       $('.zipcode').val('99501');
    }, 500);
    /**
     * If the letter is not digit in fax then don't type anything.
     */
    $("#fax").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /**
     * To change the format of fax
     */
    $("input[name='fax']").keydown(function () {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });


    /**
     * Auto fill the city, state and country when focus loose on zipcode field.
     */
    $("#zipcode").focusout(function () {
        getAddressInfoByZip1($(this).val());
    });

    /**
     * Auto fill the city, state and country when enter key is clicked.
     */
    $("#zipcode").keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            getAddressInfoByZip1($(this).val());
        }
    });
    
    $('#NewpetComplaintSave').val('Save');

    $('div#ComplaintTypePopup .cancelPopup').val('Cancel');


});


function getRandomNumber(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}



function validateCustomField(data_value, data_required, arg, element, id) {
    var msg = '';
    if (data_value == "") {
        if (data_required == 1) {
            msg = 'This field is required';
            $('#' + id).after("<span>" + msg + "</span>");
            return false;
        }
    }
    if (arg == 'text') {
        if (data_value.length > 150) {
            msg = 'Please enter character less than 150';
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'number') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 20) {
                msg = 'Please enter less than 20';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'currency') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            console.log(msg);
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'percentage') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            console.log(msg);
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 100) {
                msg = 'Maximum value is 100';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'url') {
        var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
        var regex = new RegExp(expression);
        if (data_value.match(regex)) {
        } else {
            msg = 'Please enter a valid URL';
            $('#' + id).next('.customError').text(msg);
            return false;
        }
    }
    $('#' + id).next('.customError').text('');
    return true;
}
function fetchAllPortfolio(id) {

    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-portfolioname',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPortfolioname'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#portfolio_name_options').html(res.data);


            if (id != false) {
                $('#portfolio_name_options').val(id);

            }

        },
    });

}
function fetchAllPropertytype(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertytype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertytype'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#property_type_options').html(res.data);
            if (id != false) {
                $('#property_type_options').val(id);
            }

        },
    });

}
function fetchAllPropertystyle(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertytype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertystyle'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#property_style_options').html(res.data);
            if (id != false) {
                $('#property_style_options').val(id);
            }
        },
    });

}
function fetchAllPropertysubtype(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertysubtype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertysubtype'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#property_subtype_options').html(res.data);
            if (id != false) {
                $('#property_subtype_options').val(id);
            }

        },
    });

}


    $.ajax({
        type: 'post',
        url: '/fetch-garagedata',
        data: {
            class: 'propertyDetail',
            action: 'fetchGaragedata'},function fetchAllgarage(id) {

        success: function (response) {
            var res = JSON.parse(response);
            $('#garage_options').html(res.data);
            if (id != false) {
                $('#garage_options').val(id);
                $('.garageDesc').show();
                $('.garageVehicle').hide();
                $('.garageDesc').val('');
            }


        },
    });

}
function fetchAllManagers(id) {

    $.ajax({
        type: 'post',
        url: '/fetch-managers',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllmanagers'},
        success: function (response) {
            var res = JSON.parse(response);
            var element = $("#select_mangers_options");
            element.html(res.data).multiselect("destroy").multiselect({
                includeSelectAllOption: true,
                nonSelectedText: 'Select Manager'
            });

            if (manager_id != '') {
                element.multiselect('select', manager_id);
            } else {
                element.multiselect('select', id);
            }

        },
    });

}
function fetchAllinsurancetypes(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-insurancetype',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllinsurancetypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#SelectInsuranceTypes').html(res.data);
            if (id !== false) {
                $('#SelectInsuranceTypes').val(id);
            }
        },
    });
}

function fetchAllpolicytypes(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-policytype',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllpolicytypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#SelectPolicyTypes').html(res.data);
            if (id !== false) {
                $('#SelectPolicyTypes').val(id);
            }
        },
    });
}
function fetchAllAttachgroups(id) {

    $.ajax({
        type: 'post',
        url: '/fetch-attachgroup',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllAttachgroups'},
        success: function (response) {
            var res = JSON.parse(response);
            var element = $("#select_group_options");
            element.html(res.data).multiselect("destroy").multiselect({
                includeSelectAllOption: true,
                nonSelectedText: 'Select Attach Group'
            });

            if (attachGroup_id != '') {
                element.multiselect('select', attachGroup_id);
            } else {
                element.multiselect('select', id);
            }
        },
    });

}


$(document).on("change", '.ownerSelectclass', function () {
    var selected_value = '';
    var owners = [];
    var owner_id = $(".ownerSelectclass");
    selected_value = $(this).val();
    $.each($(".ownerSelectclass option:selected"), function () {
        owners.push($(this).val());
    });

    if (owner_id.length > 1) {
        var itemtoRemove = selected_value;
        owners.splice($.inArray(itemtoRemove, owners), 1);
        if (jQuery.inArray(selected_value, owners) != -1) {
            $(this).prop('selectedIndex', 0);
            // the element is not in the array
        } else {

        }
    }
});
//fetch all owners
function getAllOwners() {
    var property_id = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-owners',
        data: {
            class: 'propertyDetail',
            action: 'fetchOwners',
            property_id: property_id,
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.ownerSelectclass').html(res.data);
        },
    });
}
function fetchAllFixture(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-all-fixture',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllfixture'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#fixture_type').html(res.data);
            if (id != false) {
                $('#fixture_type').val(id);
            }

        },
    });
}

function fetchAllReason(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchAllReason'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#selectreasondiv').html(res.data);
            if (id != false) {
                $('#selectreasondiv').val(id);
            }

        },
    });
}
function fetchAllAccountName(id,lastid) {


    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'fetchAccountName',
            pid:id
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.bankAccountName').html(res.data);
            if (lastid != false) {
                $('.bankAccountName').val(lastid);
            }


        },
    });

}
function fetchAllAccountType(id) {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllAccountType'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.prop_account_type').html(res.data);
            if (id != false) {
                $('.prop_account_type').val(id);
            }

        },
    });

}
function fetchChartAccounts(id) {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'fetchChartAccounts'},
        success: function (response) {
            var res = JSON.parse(response);
            $('.bankChartsOfAccounts').html(res.data);
            $('#default_security_bank_account').html(res.data);
            if (id != false) {
                $('.bankChartsOfAccounts').val(id);
            }

        },
    });

}
function getcurrentdatetime() {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'getcurrentdatetime'},
        success: function (response) {
            var response = JSON.parse(response);
            if ($('#renovationdate').val() == '') {
                $('#renovationdate').val(response.timeZone);
            }



            if ($("#last_renovation_timeaddid").val() == '') {
                $("#last_renovation_timeaddid").timepicker({timeFormat: 'h:mm p', defaultTime: new Date()});
            }
            $("#last_renovation_date_id").val(response.timeZone);
            $("#pick_up_date").val(response.timeZone);
            $("#pick_up_time").val(response.timeZone);
            $("#loan_start_date").val(response.timeZone);
        }


    });
}
function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

$(document).on('click', '.cancelPopup', function () {
    $(this).parent().parent().parent().find('input[type=text]').val('');
    $(this).parent().parent().parent().find('span').text('');
});

function clearPopUps(id) {
    $(id).find('input[type=text]').val('');
    $(id).find('span').text('');
}

$(document).on('click', '.topicons', function () {
    var property_id_data = ($("#property_editunique_id").val() !== undefined) ? $("#property_editunique_id").val() : property_unique_id;

    if (property_id_data == '') {
        $('.tabcheckvalidation').attr('href', 'javascript:;');
        bootbox.alert('Please Add General Information First!');
    } else {
        $(".topicons").each(function () {
            var dataHref = $(this).attr('datahref');
            $(this).attr('href', dataHref + '?id=' + property_id_data);
            window.location.href = dataHref + '?id=' + property_id_data;

        });
    }
    $(this)
});

$(document).on('click', '.late_fee_radio', function () {
    var validator = $("#AddLateFee").validate();
    validator.resetForm();
});

$('#interest_rate').autoNumeric('init', {  vMax: '99.99' });

$(document).on('focusout','.amount',function(){
    
    var id = this.id;
    if($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
        var bef = $('#' + id).val().replace(/,/g, '');
        var value = numberWithCommas(bef) + '.00';
        $('#' + id).val(value);
    } else {
        var bef = $('#' + id).val().replace(/,/g, '');
        $('#' + id).val(numberWithCommas(bef));
    }
});

$('#interest_rate').autoNumeric('init', {  vMax: '99.99' });

$(document).on('click', '.addcheckoutcolorblue', function () {
$("#return-key-div-history").modal('show');
var propertyEditid = $("#property_editunique_id").val();
var id = $(this).attr('rel');
       $.ajax({
           type: 'post',
           url: '/property-ajax',
           data: {
               propertyEditid: propertyEditid,
               id: id,
               class: 'propertyDetail',
               action: 'getReturnkeyDatahistory'},
           success: function (response) {
               var response = JSON.parse(response);
               if (response.code == 200) {
                   $("#tbodyCheckOutDetailhistory").html(response.html);
               } else if (response.status == "error") {
                   toastr.error(response.message);
               } else {
                   toastr.error(response.message);
               }
               $('#key-table').trigger('reloadGrid');
           }
       });
});

new AutoNumeric('#loan_amount', {
                allowDecimalPadding: true,
                maximumValue  : '9999999999',
            });
            
            new AutoNumeric('#payment_amount', {
                allowDecimalPadding: true,
                maximumValue  : '9999999999',
            });

fetchCountries();

function fetchCountries() {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'countries'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#country_code').html(res.data);
            $("#insurance_country_code").html(res.data);
            $("#flag_country_code12").html(res.data);

        },
    });
}

$(document).on('click','#_easyui_textbox_input2',function(){
    $('.owner_pre_vendor').combogrid('showPanel');
});

$(document).on('click','#_easyui_textbox_input3',function(){
    $('#blacklistvendor_name').combogrid('showPanel');
});

$(document).on('click','#16collapseSixteen',function(){
        var $grid = $("#propertPhotovideos-table").setGridWidth($(window).width()-70);
});

/** Add/Edit new Bank Account */
$("#add_bank_account_form").validate({
    rules: {
        portfolio: {
            required: true
        },
        bank_name: {
            required: true
        },
        bank_account_number: {
            required: true,
            minlength: 7
        },
        fdi_number: {
            required: true
        },
        routing_number: {
            required: true,
            number: true
        },
        branch_code: {
            required: true
        },
        initial_amount: {
            required: true,
            number: true
        },
        last_used_check_number: {
            required: true,
            number: true
        },
        status: {
            required: true
        }
    },
    messages: {
        portfolio: {
            required: "* This field is required",
        },
        bank_account_number: {
            minlength: "* Please enter 7 digit account number",
        }
    },
    submitHandler: function () {
        var portfolio = $('#portfolio_bank_name_hddn').val();
        var bank_name = $('#bank_name_rec').val();
        var bank_account_number = $('#bank_account_number').val();
        var fdi_number = $('#fdi_number').val();
        var routing_number = $('#routing_number_rec').val();

        var branch_code = $('#branch_code').val();
        var initial_amount = $('#initial_amount').val();
        var last_used_check_number = $('#last_used_check_number').val();
        var status = $('#status').val();
        var bank_account_id = $("#bank_account_id").val();
        var is_default;
        if ($('#is_default').is(":checked")) {
            is_default = '1';   // it is checked
        } else {
            is_default = '0';
        }
        var formData = {
            'portfolio': portfolio,
            'bank_name': bank_name,
            'bank_account_number': bank_account_number,
            'fdi_number': fdi_number,
            'routing_number':routing_number,
            'branch_code': branch_code,
            'initial_amount': initial_amount,
            'last_used_check_number': last_used_check_number,
            'status': status,
            'is_default': is_default,
            'bank_account_id': bank_account_id
        };

        $.ajax({
            type: 'post',
            url: '/BankAccount-Ajax',
            data: {
                class: 'BankAccountAjax',
                action: 'insert',
                form: formData
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#start_reconcile_modal').modal({
                        show: true,
                        backdrop: 'static',
                        keyboard: false,
                    });

                    $('#add_more_bank_account_modal').modal('hide');
                    portfolio_default_id = $("#portfolio_name_options").val();
                    fetchAllAccountName(portfolio_default_id,response.lastInsertData.id);

                    toastr.success('The record saved successfully.');

                }else if (response.status == 'error' && response.code == 400) {
                    toastr.error(response.message);
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'warning' && response.code == 503) {
                    toastr.warning(response.message);
                }
            }
        });
    }
});
$(document).on('click', '#add_more_bank_account_icon', function () {
    $('#add_bank_account_form')[0].reset();
    $('#add_bank_account_form label.error').text('');
    getAllPortfolioDDl();
    $('#add_more_bank_account_modal').modal({
        show: true,
        backdrop: 'static',
        keyboard: false,
    })

});

$(document).on('click', '#add_bank_account_cancel_btn', function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('#start_reconcile_modal').modal({
                show: false,
                backdrop: 'static',
                keyboard: false,
            });
            $('#start_reconcile_modal').modal('show');
            $('#add_more_bank_account_modal').modal('hide');
        }
    });
});

function getAllPortfolioDDl(){
    var propertyedit = $("#property_editunique_id").val();
    var propertyunique = $("#property_unique_id").val();
    if (propertyedit == undefined) {
        var property = propertyunique;
    } else {
        var property = propertyedit;
    }

    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'getPropertyPortfolio',
            property:property
        },
        success : function(response){
            var response =  JSON.parse(response);

            if(response.status == 'success') {

                $('#portfolio_bank_name').val(response.data.portfolio_name);

                $('#portfolio_bank_name_hddn').val(response.data.id);
            }
        }
    });
}

function workorder() {
    var id = getParameterByName('id');
    var checkbox = '<input type="checkbox" id="select_all_complaint_checkbox">';
    var table = 'work_order';
    var columns = [checkbox,'Work Order Number','email', 'Work Order Category', 'Vendor','Property','Created On','Caller/RequestedBy','Amount(AFN)','Priority ','Status','Action'];
    var select_column = ['Edit','Email','TEXT','View','Add In-touch','In-Touch History','Delete'];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where =[{column: 'property_id', value: id, condition: '='}];
    var extra_columns = ['work_order.deleted_at', 'work_order.updated_at'];
    var joins = [{table: 'work_order', column: 'work_order_cat', primary: 'id', on_table: 'company_workorder_category'},
        {table: 'work_order', column: 'property_id', primary: 'id', on_table: 'general_property'},
        {table: 'work_order', column: 'priority_id', primary: 'id', on_table: 'company_priority_type'},
        {table: 'work_order', column: 'status_id', primary: 'id', on_table: 'company_workorder_status'},
        {table: 'work_order', column: 'vendor_id', primary: 'id', on_table: 'users'}];
    var columns_options = [
        {name:'Id',index:'id', width:100, sortable: false, searchoptions: {sopt: conditions},search: false,table:table,formatter:actionCheckboxFmatterComplaint},
        {name:'Work Order Number',index:'work_order_number', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'email',index:'email', width:100,hidden:true,searchoptions: {sopt: conditions},table:'users'},
        {name:'Work Order Category',index:'category', width:200,searchoptions: {sopt: conditions},table:'company_workorder_category'},
        {name:'Vendor',index:'name', width:150,searchoptions: {sopt: conditions},table:'users'},
        {name:'Property',index:'property_name', width:100,searchoptions: {sopt: conditions},table:'general_property'},
        {name:'Created On',index:'created_at', width:200,searchoptions: {sopt: conditions},table:table},
        {name:'Caller/RequestedBy',index:'request_id', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Amount(AFN)',index:'estimated_cost', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Priority',index:'priority', width:100,searchoptions: {sopt: conditions},table:'company_priority_type'},
        {name:'Status',index:'work_order_status', width:100,searchoptions: {sopt: conditions},table:'company_workorder_status'},
        {name:'Action',index:'',title:false, width:200,searchoptions: {sopt: conditions},table:table,formatter:statusFmatterWork},
    ];

    var ignore_array = [];
    jQuery("#property_work_order").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Work Orders",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

function statusFmatterWork(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        var select = ['Edit','Email','TEXT','View','Add In-touch','In-Touch History','Delete'];

        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}
function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {

        var data = '';
        var data = '<input type="checkbox" name="maintenance_checkbox[]" class="maintenance_checkbox" id="maintenance_checkbox' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
        return data;
    }
}


$(document).on('click','.clearFormReset',function(){
    var formid=$(this).attr('rel');
    window.location.href = base_url+"/Property/AddProperty";
    //'vendor_name','management_start_date','maintenance_speed_time','is_short_term_rental','portfolio_id','property_id','property_for_sale','zipcode','country','state','city','country_code','property_type','property_style','property_subtype','last_renovation_time','no_of_buildings','no_of_units','smoking_allowed','pet_friendly','garage_available'
      //  resetFormClear('#'+formid,[],'form',true);

});
$(document).on('click','.clearFormResetkey',function(){
    var formid=$(this).attr('rel');
    var text=$("#AddKeyButton").text();
    if(text == 'Update'){
        resetEditForm("#"+formid,[],true,defaultFormData,[]);
    }else{
        resetFormClear('#'+formid,[],'form',false);
    }

});
$(document).on('click','.clearFormResetTrack',function(){
    var formid=$(this).attr('rel');
    var text=$("#TrackKeyButton").text();
    if(text == 'Update'){
        resetEditForm("#"+formid,[],true,defaultFormData,[]);
    }else{
        resetFormClear('#'+formid,[],'form',false);
    }

});
$(document).on('click','.clearFormResetIns',function(){
    var formid=$(this).attr('rel');
    var text=$("#AddPropertyInsuranceSave1").text();
    if(text == 'Update'){
        resetEditForm("#"+formid,[],true,defaultFormData,[]);
    }else{
        resetFormClear('#'+formid,[],'form',false);
    }

});
$(document).on('click','.clearFormResetFixture',function(){
    var formid=$(this).attr('rel');
    var text=$("#warrantyinforClickB").text();
    if(text == 'Update'){
        resetEditForm("#"+formid,[],true,defaultFormData,[]);
    }else{
        resetFormClear('#'+formid,[],'form',false);
    }

});
$(document).on('click','.clearEditFormResetNotes',function(){
    var formid=$(this).attr('rel');
    var text=$("#AddPropertynotesButton").text();
    if(text == 'Update'){
        resetEditForm("#"+formid,[],true,defaultFormData,[]);
    }else{
        resetFormClear('#'+formid,[],'form',false);
    }

});
$(document).on('click','.clearEditFormResetFlag',function(){
    var formid=$(this).attr('rel');
    var dataid=$(this).attr('data-id');
    var text=$("#"+dataid).text();
    if(text == 'Update'){
        resetEditForm("#"+formid,[],true,defaultFormData,[]);
    }else{
        resetFormClear('#'+formid,['flag_by','date','flag_name','country_code','flag_phone_number','completed'],'form',false);
    }

});
$(document).on('click','.clearFormResetBank',function(){
    var formid=$(this).attr('rel');
        resetFormClear('#'+formid,[],'form',false);
});
$(document).on('click','.clearEditFormReset',function(){
    var href=$(this).attr('rel');
    bootbox.confirm("Do you want to reset this form?", function (result) {
        if (result == true) {
            localStorage.setItem("AccordionHref",href);

            location.reload();
            return true;
        } else {
            bootbox.hideAll()
        }
        return false;
    });

});
$(document).on("click",".clearFormResetPopup",function(){
    var modal_id=$(this).attr('rel');

    if(modal_id =='NewportfolioPopupClass'){
        $('#portfolio_name').val('');
    } else{

        resetFormClear('.'+modal_id,[],'div',false);
    }


});

$(document).on('click','.clearEditFormReset2',function () {
    var formid=$(this).attr('rel');
    resetEditForm("#"+formid,[],true,defaultFormData,[]);
});
$(document).on('change','#select_mangers_options',function () {
    var email=$("#select_mangers_options option:selected").attr('data-id');
    $("#manager_email").val(email);

});

function sendComplaintsEmail(elem)
{
    var mail_length = $('#print_complaint #modal-body-complaints>table').length;


    var owner_email = $('#email').val();
    SendMail($(elem).html(), mail_length, owner_email);
}


function SendMail(data, mail_length, owner_email){
    var propertyID = getParameterByName('id');
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'SendComplaintMail',
            data_html: data,
            'complaint_ids': favorite,
            'propertyID':propertyID,
            mail_length : mail_length,
            owner_email : owner_email,
        },
        success : function(response){
            var response =  JSON.parse(response);
            if(response.status == 'success' && response.code == 200) {
                toastr.success('Mail sent successfully.');
                $("#print_complaint").modal('hide');
            }else {
                toastr.warning('Mail not send due to technical issue.');
            }
        }
    });
}