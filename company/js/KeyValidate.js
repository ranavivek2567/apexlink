//add property form client side validations
$("#addkeytags").validate({
    rules: {
        key_tag:{
            required: true
        },
        description: {
            required: true
        },
        total_keys: {
            required: true,
            number: true
        }
    }
});


$("#TrackkeydetailForm").validate({
    rules: {
        key_name:{
            required: true
        },
        email: {
            required: true,
            email:true
        },
        company_name: {
             required: true
        },
        phone: {
            required: true,
            
        },
        key_number: {
            required: true,
            
        },
        key_quality: {
            required: true,
            
        },
        pick_up_date: {
            required: true,
            
        },
        return_date: {
            required: true,
            
        },
        return_time: {
            required: true,
            
        },
        key_designator: {
            required: true,
            
        },
        Address1 : {
            required: true,
        },
        Address2 : {
            required: true,
        },
        Address3 : {
            required: true,
        },
        Address4 : {
            required: true,
        }
    },
    messages: {
        portfolioName: {
            valueNotEquals: "Please select non-default value!"
        },
        countryCode: {
            valueNotEquals: "Please select non-default value!"
        },
        garageAvalible: {
            valueNotEquals: "Please select non-default value!"
        }
    }
});