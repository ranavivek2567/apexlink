$(document).ready(function(){

    var base_url = window.location.origin;
    /*
    function on load
     */
    var id =  getParameterByName('id');
    getPropertyDetail(id);
    jqGrid('All',id);

    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#property-building-table').jqGrid('GridUnload');
        jqGrid(selected,id);
    });





    /**
     *change function to perform various actions(edit ,activate,deactivate)
     * @param status
     */
    jQuery(document).on('change','.select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        if(select_options == 'Edit')
        {
            window.location.href =  base_url+'/Building/EditBuilding?id='+data_id;

        }else if (select_options == 'Flag Bank'){
            localStorage.setItem("AccordionHref",'#flags');
            window.location.href = base_url+'/Building/EditBuilding?id='+data_id;
        }else if (select_options == 'Inspection'){
            localStorage.setItem("inspection_value", "add_inspection");
            window.location.href = base_url+'/Building/BuildingInspection?id='+data_id;
        }else if (select_options == 'Add In-Touch'){
            window.location.href = "/Communication/NewInTouch?tid="+id+"&category=Building";
            // window.location.href =   base_url+'/Communication/InTouch';
        }else if (select_options == 'In-Touch History'){
            var propertyname=$('#'+data_id).find('td:first').text();
            localStorage.setItem("propertyname",propertyname);
            window.location.href =   base_url+'/Communication/InTouch';
        }else if (select_options == 'Print Envelope'){
            $.ajax({
                type: 'post',
                url: '/get-company-data',
                data: {class: 'buildingDetail', action: 'getCompanyData','building_id':data_id},
                success : function(response){
                    var response =  JSON.parse(response);
                    if(response.status == 'success' && response.code == 200) {
                        $("#PrintEnvelope").modal('show');
                        $("#company_name").text(response.data.data.company_name)
                        $("#address1").text(response.data.data.address1)
                        $("#address2").text(response.data.data.address2)
                        $("#address3").text(response.data.data.address3)
                        $("#address4").text(response.data.data.address4)
                        $(".city").text(response.data.data.city)
                        $(".state").text(response.data.data.state)
                        $(".postal_code").text(response.data.data.zipcode)
                        $("#building_name").text(response.building.data.building_name)
                        $("#building_address").text(response.building.data.address)
                    }else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                }
            });


        }else if(select_options == 'Deactivate')
        {
            bootbox.confirm({
                message: "Do you want to deactivate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'deactivate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Building deactivated successfully.');
                                }else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Activate') {
            bootbox.confirm({
                message: "Do you want to activate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'activate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Building activated successfully.');
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Delete') {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/building-ajax',
                            data: {class: 'buildingDetail', action: 'delete', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Building deleted successfully.');
                                } else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });

    /*
    function to get property detail
     */
    function getPropertyDetail(id) {
        $.ajax({
            type: 'post',
            url: '/get-property-detail ',
            data: {
                class: 'propertyDetail',
                action: 'gerPropertyDetail',
                id:id
            },
            success: function (response) {
                var res = JSON.parse(response);
                if(res) {
                    $("#property_link_name" ).html(res.property_detail.property_name);
                    $("#property_link_name" ).attr('href',base_url+'/Property/PropertyModules');
                    $("#property_tab_link" ).attr('href',base_url+'/Property/PropertyModules');
                    $("#new_property_unit_list_href").attr("href", base_url+"/Unit/UnitModule?id="+id);
                    $("#new_unit_href").attr("href", base_url+"/Unit/AddUnit?id="+id);
                    $("#new_building_href").prop("href", base_url+"/Building/AddBuilding?id="+id);
                    $("#property_inspection_link").prop("href", base_url+"/Property/PropertyInspection?id="+id);





                }
            },
        });

    }

});

/**
 * function to format linked units
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function linkedUnits(cellValue, options, rowObject) {
    if(cellValue == ''){
        cellValue ='0';
    }
    return '<span style="text-decoration: underline;">' + cellValue + '</span>';
}

/**
 *  function to format property name
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function buildingName(cellValue, options, rowObject) {
   // return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';

    if(rowObject !== undefined) {

        var flagValue = $(rowObject.Action).attr('flag');

        var id = $(rowObject.Action).attr('data_id');

        var flag = '';

        if (flagValue == 'yes') {

            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';

        } else {

            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';

        }

    }
}

/**
 * jqGrid Initialization function
 * @param status
 */

function jqGrid(status,id) {
    var table = 'building_detail';
    var columns = ['Building Name','Building Id', '# of Units', 'Linked Units', 'Status', 'Actions'];
    var select_column = ['Edit','Flag Bank','Inspection','Add In-Touch','In-Touch History','Deactivate','Activate','Print Envelope','Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['building_detail.deleted_at'];
    var extra_where = [{column: 'property_id', value: id, condition: '='}];
    var columns_options = [
        {name:'Building Name',index:'building_name',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter: buildingName,classes: 'pointer',attr:[{name:'flag',value:'building'}]},
        {name:'Building Id',index:'building_id', width:100,searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        {name:'# of units',index:'no_of_units', width:80,align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        {name:'Linked Units',index:'linked_units', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer',formatter:linkedUnits},
        {name:'status',index:'status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter,classes: 'pointer'},
        {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFmatter}
    ];
    var ignore_array = [];
    jQuery("#property-building-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Building",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}

/**
 * jqGrid function to format is_default column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function isDefaultFormatter (cellValue, options, rowObject){
    if (cellValue == '1')
        return "Yes";
    else if(cellValue == '0')
        return "No";
    else
        return '';
}

alphabeticSearch()
function alphabeticSearch(){
    $.ajax({
        type: 'post',
        url:'/List/jqgrid',
        data: {class: 'jqGrid',
            action: "alphabeticSearch",
            table: 'general_property',
            column: 'property_name'},
        success : function(response){
            var response = JSON.parse(response);
            if(response.code == 200){
                var html = '';

                $.each(response.data, function(key,val) {
                    var color = '#05A0E4'
                    if(val == 0) color = '#c5c5c5';
                    html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                });
                $('.AtoZ').html(html);
            }
        }
    });
}

/*
    function to get parameter by id
 */
function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

/**
 * Function for view building on clicking row
 */
$(document).on('click', '#property-building-table tr td', function () {
    var id =  getParameterByName('id');
    var base_url = window.location.origin;
    var data_id = $(this).closest('tr').attr('id');
    if ($(this).index() == 5) {
        return false;
    }else if($(this).index() == 3){
        window.location.href = base_url+'/Unit/UnitModule?id='+id+'&bid='+data_id;
    } else {
        window.location.href = base_url+'/Building/View?id='+data_id;
    }
});




/**
 * Function for reload property building table
 */


/**
 * jqGrid function to format status
 * @param status
 */
function statusFmatter (cellvalue, options, rowObject){
    if (cellvalue == 1)
        return "Active";
    else if(cellvalue == 0)
        return "InActive";
    else
        return '';
}

/*
    Function to print envelope
 */
function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}

/**
 * jqGrid function to format action column
 * @param status
 */

function actionFmatter (cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        //console.log(rowObject.status);
        var editable = $(cellvalue).attr('editable');
        var select = '';
        if(rowObject.status == 1)  select = ['Edit','Flag Bank','Inspection','Add In-Touch','In-Touch History','Deactivate','Print Envelope','Delete'];
        if(rowObject.status == 0 || rowObject.Status == '')  select = ['Edit','Inspection','Add In-Touch','Activate','Print Envelope'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}
function triggerReload(){
    var grid = $("#property-building-table");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}