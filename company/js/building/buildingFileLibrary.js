var file_library = [];
var imgArray = [];
$(document).on('change','#file_library',function(){
   // file_library = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if(size > 1030) {
            toastr.warning('Please select documents less than 1 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
            if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                if($.inArray(value['name'], imgArray) === -1)
                {
                    file_library.push(value);
                }

                var src = '';
                var reader = new FileReader();
                //$('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                    if($.inArray(value['name'], imgArray) === -1)
                    {
                        $("#file_library_uploads").append(
                            '<div class="row" style="margin:20px">' +
                            '<div class="col-sm-12 img-upload-library-div">' +
                            '<div  class="col-sm-3"><img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '></div>' +
                            '<div style="margin-top: 36px;" class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div style="margin-top: 36px;" class="col-sm-3 show-library-list-imgs-size' + key + '"><strong>' + size + '</strong></div>' +
                            '<div style="margin-top: 36px;" class="col-sm-3"><span id=' + key + ' class="delete_pro_img cursor"><button class="orange-btn">Delete</button></span></div></div></div>');
                            imgArray.push(value['name']);
                    } else {
                        toastr.warning('File already exists!');
                    }
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});

$('#saveLibraryFiles').on('click',function(){
    var length = $('#file_library_uploads > div').length;
    if(length > 0) {
        var data = convertSerializeDatatoArray();
        var uploadform = new FormData();
        uploadform.append('class', 'propertyFilelibrary');
        uploadform.append('action', 'file_library');
        var count = file_library.length;
        $.each(file_library, function (key, value) {
             if(compareArray(value,data) == 'true'){
                 uploadform.append(key, value);
             }
            if(key+1 === count){
                saveLibraryFiles(uploadform);
            }
        });
    } else {

    }
});

$(document).on('click','.delete_pro_img',function(){
    $(this).parent().parent().parent('.row').remove();
    toastr.success('The record deleted successfully.');
});

$(document).on('click','.employee_cancel',function(){
    bootbox.confirm("Do you want to cancel this action now ?", function (result) {
        if (result == true) {
            window.location.href = window.location.origin+'/People/GetEmployeeList';
        }
    });
});

// $(document).on('click','#remove_library_file',function(){
//     $('#file_library_uploads').html('');
//     $('#file_library').val('');
// });

$(document).on('click','#remove_library_file',function(){
    bootbox.confirm("Do you want to remove all files?", function (result) {
        if (result == true) {
            toastr.success('The record deleted successfully.');
            $('#file_library_uploads').html('');
            $('#file_library').val('');
            imgArray = [];
        }
    });
});

function saveLibraryFiles(uploadform){
    $.ajax({
        type: 'post',
        url: '/property/file_library',
        data:uploadform,
        processData: false,
        contentType: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
                $('#file_library_uploads').html('');
                $('#propertFileLibrary-table').trigger('reloadGrid');
                toastr.success('Files uploaded successfully.');
            } else if(response.code == 500){
                toastr.warning(response.message);
            } else {
                toastr.success('Error while uploading files.');
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function convertSerializeDatatoArray(){
    var newData = [];
    $(".fileLibraryInput").each(function( index ) {
        var name = $(this).val();
        var size = $(this).attr('data_id');
        newData.push({'name':name,'size':size});
    });
    return newData;
}

function compareArray(data,compare){
    for(var i =0;i < compare.length;i++){
        if(compare[i].name == data['name'] && compare[i].size == data['size']){
            return 'true';
        }
    }
    return 'false';
}

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}

/**
 * jqGrid Intialization function
 * @param status
 */
jqGridFileLibrary('All');
function jqGridFileLibrary(status) {
    var building_id = $("#edit_building_id").val();
    var table = 'building_file_uploads';
    var columns = ['Name','File','Location','File extension','Action'];
    var select_column = ['Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'file_type',value:'2',condition:'='},{column:'building_id',value:building_id,condition:'='}];
    var columns_options = [
        {name:'Name',index:'file_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'file_extension',width:200,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        {name:'Location',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
        {name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select', width:150,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionLibraryFmatter,title:false}
    ];
    var ignore_array = [];
    jQuery("#buildingFileLibrary-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of File Library",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * jqGrid function to format action column
 * @param status
 */

// function actionLibraryFmatter (cellvalue, options, rowObject){
//     if(rowObject !== undefined) {
//
//         var select = '';
//         select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
//         var data = '';
//         if(select != '') {
//             $.each(select, function (key, val) {
//                 data += val
//             });
//         }
//         return data;
//     }
// }

/**
 * jqGrid function to format action column
 * @param status
 */

function actionLibraryFmatter (cellvalue, options, rowObject){
    // if(rowObject !== undefined) {
    //
    //     var select = '';
    //     select = ['<span><i class="fa fa-trash delete_file_library pointer" data_id="' + rowObject.id + '" style="font-size:24px"></i></span>'];
    //     var data = '';
    //     if(select != '') {
    //         $.each(select, function (key, val) {
    //             data += val
    //         });
    //     }
    //     return data;
    // }

    if(rowObject !== undefined) {
        var editable = $(cellvalue).attr('editable');
        var select = '';
        var file_type =  rowObject.View;
        var location = rowObject.Location;
        var path = upload_url+'company/'+location;

        var imageData = '';
        var src = '';
        if(file_type == '1'){

            imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
        } else {
            if (rowObject.File_extension == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.File_extension == 'docx' || rowObject.File_extension == 'doc') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }else if (rowObject.File_extension == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }
        }

        select = ['Email','Delete'];
        var data = '';

        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-path="'+path+'" data-src="'+src+'"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

function imageFormatter(cellvalue, options, rowObject){


    if(rowObject !== undefined) {

        var path = upload_url+'company/'+rowObject.Location;
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=100 height=100 src="' + src + '">';
    }
}

$(document).on('change', '#propertFileLibrary-table .select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Email' || opt == 'EMAIL') {


    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/property/file_library',
                        data: {class: 'propertyFilelibrary', action: "deleteFile", id: id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
                $('#propertFileLibrary-table').trigger('reloadGrid');
            }
        });
    }
});

/**
 *change function to perform various actions(edit ,delete)
 * @param status
 */
$(document).on('change','#buildingFileLibrary-table .select_options',function () {
    var select_options = $(this).val();
    var data_id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    var data_path = $(this).attr('data-path');
    var data_src = $(this).attr('data-src');
    if(select_options == 'Email')
    {
        file_upload_email('users','email',data_id,1,data_path,data_src);
    }else if(select_options == 'Delete') {
        $('#buildingFileLibrary-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        var row_num = $(this).parent().parent().parent().index() ;
        jQuery('#buildingFileLibrary-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#buildingFileLibrary-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        var data_id = $(this).attr("data_id");

        bootbox.confirm({
            message: "Are you sure you want to delete this file ?",
            buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/building-ajax',
                        data: {class: 'buildingDetail', action: 'deleteFile', id: data_id},
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                                // window.location.href = '/MasterData/AddPropertyType';
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                triggerFileReload();
            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});

$(document).on("click",".addToRecepent",function(){
    $('#torecepents').modal('show');
});


$(document).on("change",".selectUsers",function(){
    var type = $(this).val();
    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            $(".userDetails").html(response);

        }
    });
});

$(document).on("click","#SendselectToUsers",function(){
    var check_data = [];
    $('.getEmails:checked').each(function () {
        $('.to').tagsinput('add', $(this).attr('data-email'));
        $('.to').tagsinput('add', $(this).attr('data-email'));
    });
    $('#torecepents').modal('hide');
    // $('.getEmails').prop('checked', false);
});

$(document).on("click",".addCcRecepent",function(){
    $('#ccrecepents').modal('show');
});

$(document).on("change",".selectCcUsers",function(){
    var type = $(this).val();
    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getCCUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            $(".userCcDetails").html(response);
        }
    });



});

$(document).on("click",".addBccRecepent",function(){
    $('#bccrecepents').modal('show');

});

$(document).on("change",".selectBccUsers",function(){
    var type = $(this).val();
    $.ajax({
        url:'/EditOwnerAjax',
        type: 'POST',
        data: {
            "type": type,
            "action": 'getBCCUsers',
            "class": 'EditOwnerAjax'
        },
        success: function (response) {
            $(".userBccDetails").html(response);
        }
    });
});


$(document).on("click",".getBCCEmails",function(){
    if(this.checked)
    {
        var email = $(this).attr('data-email');
        $('.bcc').tagsinput('add', email);
    }
    else
    {
        var email = $(this).attr('data-email');
        $('.bcc').tagsinput('remove', email);
    }


});

$(document).on("click","#SendselectCcUsers",function(){
    var check_data = [];
    $('.getCCEmails:checked').each(function () {
        $('.cc').tagsinput('add', $(this).attr('data-email'));
        $('.cc').tagsinput('add', $(this).attr('data-email'));
    });
    $('#ccrecepents').modal('hide');
});

$(document).on("click","#SendselectBccUsers",function(){
    var check_data = [];
    $('.getBCCEmails:checked').each(function () {
        $('.bcc').tagsinput('add', $(this).attr('data-email'));
        $('.bcc').tagsinput('add', $(this).attr('data-email'));
    });
    $('#bccrecepents').modal('hide');
});