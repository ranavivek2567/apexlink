$(document).ready(function () {
    var base_url = window.location.origin;
    var login_user_id =  localStorage.getItem("login_user_id");


    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $("#addPropertyTypeForm").hide(500);
        $('#properttype-table').jqGrid('GridUnload');
        changeGridStatus(selected);
        jqGrid(selected);
    });

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status) {
        var table = 'company_property_type';
        var columns = ['Property Type','Description', 'Set as Default', 'Status','Action'];
        var select_column = ['Edit','Deactivate'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_property_type.status','company_property_type.is_editable'];
        var columns_options = [
            {name:'Property Type',index:'property_type', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Is Default',index:'is_default', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:isDefaultFmatter},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            //{name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false}
            {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFmatter,title:false}
        ];
        var ignore_array = [];
        jQuery("#properttype-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_property_type",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns
            },
            viewrecords: true,
            sortname:'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Property Types",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:200,drag:true,resize:false} // search options
        );
    }

    $(document).on("click",'#export_property_type_button',function(){
        var status = $("#jqGridStatus option:selected").val();
        var table =  'company_property_type';
        window.location.href = base_url+"/export-excel?status="+status+"&&table="+table+"&&action=exportExcel";


    });

    $(document).on("click",'#export_sample_property_type_button',function(){
        var checkPermissionData = checkPermissions(permissions,module,'Download Sample');
        if(checkPermissionData == 'Access Denied') {
            return false;
        }
        window.location.href = base_url+"/export-excel?status="+"&&action=exportSampleExcel";
    });

    /**
     * jqGrid function to format status
     * @param status
     */
    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Active";
        else if(cellvalue == '0')
            return "InActive";
        else
            return '';
    }
    /**
     * jqGrid function to format is_default column
     * @param status
     */
    function isDefaultFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Yes";
        else if(cellvalue == '0')
            return "No";
        else
            return '';
    }



    /**
     * jqGrid function to format action column
     * @param status
     */

    function actionFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var editable = $(cellvalue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            if(rowObject.Status == '0' && rowObject.is_editable == '0')  select = ['Edit','Activate'];
            if(rowObject.Status == '0' && rowObject.is_editable == '1')  select = ['Edit','Activate','Delete'];
            if(rowObject.Status == '1' && rowObject.is_editable == '0')  select = ['Edit','Activate'];
            if(rowObject.Status == '1' && rowObject.is_editable == '1')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {

                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    /**
     *change function to perform various actions(edit ,activate,deactivate)
     * @param status
     */
    jQuery(document).on('change','.select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        if(select_options == 'Edit')
        {
            var checkPermissionData = checkPermissions(permissions,module,'Edit');
            if(checkPermissionData == 'Access Denied') {
                $('.select_options').prop('selectedIndex', 0);
                return false;
            }
            $('#property_type_span').html('Edit Property Type');
            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            jQuery('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            var validator = $( "#add_property_type" ).validate();
            validator.resetForm();
            $("#save_property_type").val('Update');
            $(".clear-btn").val('Reset');
            $(".clear-btn").addClass('formreset');
            $(".clear-btn").removeClass('clearFormReset');
            $.ajax
            ({
                type: 'post',
                url: '/propertytype-ajax',
                data: {
                    class: "PropertyTypeAjax",
                    action: "getPropertyTypeData",
                    id:data_id,
                },
                success: function (response) {

                    $("#addPropertyTypeForm").show(500);
                    var response = $.parseJSON(response);
                    var property_type_data = response['data']['property_type_data'];
                    if(property_type_data)
                    {
                        $('#form_type').val('edit');
                        $('#property_type_id').val(data_id);
                        $.each(property_type_data, function (key, value) {
                            $('#' + key).val(value);
                            // $('.' + key).text(value);
                            if(key == 'is_default' && value == 1){
                                $('#' + key).prop("checked",true);

                            }else{
                                $('#' + key).prop("checked",false);
                            }
                            if(key == 'is_editable' && value == 0){
                                jQuery("#property_type").prop("readonly", true);
                            }
                        });
                        defaultFormData = $('#add_property_type').serializeArray();
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }else if(select_options == 'Deactivate')
        {
            bootbox.confirm({
                message: "Do you want to deactivate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/propertytype-ajax',
                            data: {class: 'PropertyTypeAjax', action: 'deactivate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Property type deactivated successfully.');
                                    triggerReload();
                                    onTop(true);
                                    // window.location.href = '/MasterData/AddPropertyType';
                                }else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                    // window.location.href = '/MasterData/AddPropertyType';
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                }
            });
        }else if(select_options == 'Activate') {
            bootbox.confirm({
                message: "Do you want to activate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/propertytype-ajax',
                            data: {class: 'CompanyUserAjax', action: 'activate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Property type activated successfully.');
                                    triggerReload();
                                    onTop(true);
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                }
            });
        }else if(select_options == 'Delete') {
            var checkPermissionData = checkPermissions(permissions,module,'Delete');
            if(checkPermissionData == 'Access Denied') {
                $('.select_options').prop('selectedIndex', 0);
                return false;
            }
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/propertytype-ajax',
                            data: {class: 'PropertyTypeAjax', action: 'delete', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Company deleted successfully.');
                                    triggerReload();
                                } else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });

    $(document).on('click','#add_property_type_button',function (e) {
        var checkPermissionData = checkPermissions(permissions,module,'Add property Type');
        if(checkPermissionData == 'Access Denied') {
            return false;
        }
        $('#property_type_span').html('Add Property Type');
        $('#description').html('');
        $('#save_property_type').val('Save');
        $('#form_type').val('add');
        $('#property_type_id').val('');
        $("#addPropertyTypeForm").show(500);

        $("#property_type").prop("readonly",false);
        var validator = $( "#add_property_type" ).validate();
        validator.resetForm();
    })

    $(document).on('click','#import_property_type_button',function (e) {
        var checkPermissionData = checkPermissions(permissions,module,'Import Property Type');
        if(checkPermissionData == 'Access Denied')return false;
        $("#import_file").val('');
        $("#import_file-error").text('');
        $("#ImportProperty").show(500);
        return false;
    })



    $(document).on("click", "#property_type_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#addPropertyTypeForm").hide(500);
                }
            }
        });
    });


    $(document).on("click", "#import_type_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#ImportProperty").hide(500);
                }
            }
        });
    });

    $(document).on("click", "#create_stripe_accounr", function (e) {
        $.ajax({
            type: 'post',
            url: '/propertytype-ajax',
            data: {
                class: "PropertyTypeAjax",
                action: "enableAccount",
            },
            success: function (response) {
                var response =  JSON.parse(response);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });


});

function triggerReload(){
    var grid = $("#properttype-table");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

