var base_url = window.location.origin;
var login_user_id =  localStorage.getItem("login_user_id");
var status =  localStorage.getItem("active_inactive_status");

$("#portfolio_id").keypress(function(e){
    var keyCode = e.which;
    /*
      8 - (backspace)
      32 - (space)
      48-57 - (0-9)Numbers
    */
    // Not allow special
    if ( !( (keyCode >= 48 && keyCode <= 57)
        ||(keyCode >= 65 && keyCode <= 90)
        || (keyCode >= 97 && keyCode <= 122) )
        && keyCode != 8 && keyCode != 32) {
        e.preventDefault();
    }
});

//custom field button
$(document).on('click','#add_custom_field',function(){
    custom_field_validator.resetForm();
    $('#default_value').prop('readonly',false);
    $('#default_value').datepicker('destroy');
});

//jqGrid status
$('#jqGridStatus').on('change',function(){
    var selected = this.value;
    $('#portfolio-table').jqGrid('GridUnload');
    changeGridStatus(selected);
    jqGrid(selected);
    $("#portfolio_add_div").hide(500);
});

//display add portfolio div
$(document).on('click','#add_portfolio',function () {
    $('#portfolio_add_div').show();
    headerDiv.innerText = "Add Portfolio";
    $('#record_id').val('');
    $('#saveBtnId').text('Save');
    $('#addPortfolioForm').trigger("reset");
    var validator = $( "#addPortfolioForm" ).validate();
    validator.resetForm();
    $('#portfolio_id').val(genratePropertyUniqueId(6));
    getCustomfield();
});

$(document).on('click','#cancelPortfolio_add',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#record_id').val('');
                $('#portfolio_add_div').hide(500);
            }
        }
    });
});

//adding custom fields
$('#addPortfolioForm').on('submit',function(event) {
    event.preventDefault();
    //checking custom field validation
    if ($('#addPortfolioForm').valid()) {
        var formData = $('#addPortfolioForm').serializeArray();
        var custom_field = [];
        $(".custom_field_html input").each(function(){
            var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
            custom_field.push(data);
        });

        $.ajax({
            type: 'post',
            url: '/portfolio/add',
            data: {form: formData,custom_field:custom_field},
            beforeSend: function(xhr) {
                // checking custom field validations
                $(".custom_field_html input").each(function() {
                    var res = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                    if(res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $("#portfolio_add_div").hide(500);
                    toastr.success(response.message);
                } else if(response.status == 'error' && response.code == 500){
                    if(response.message == 'Portfolio already exists!') bootbox.alert(response.message); return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#'+key).text(value);
                    });
                } else if(response.status == 'error' && response.code == 400){
                    toastr.warning(response.message);
                }
                $("#portfolio-table").trigger('reloadGrid');
                onTop(true);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});

//function to genrate random unique id
function genratePropertyUniqueId(no){
    var characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var randomString = '';
    for (var i = 0; i < no; i++) {
        randomString += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return randomString;
}

/**
 * jqGrid Initialization function
 * @param status
 */
function jqGrid(status, deleted_at) {
    var table = 'company_property_portfolio';
    var columns = ['Portfolio ID','Portfolio Name', 'Status', 'Set as Default', 'Action'];
    var select_column = ['Edit','status','Add in-touch','in-touch-history','Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['company_property_portfolio.deleted_at','company_property_portfolio.is_editable'];
    var columns_options = [
        {name:'Portfolio ID',index:'portfolio_id', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Portfolio Name',index:'portfolio_name', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
        {name:'Set as Default',index:'is_default', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:isDefaultFormatter},
        {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    var ignore_array = [];
    jQuery("#portfolio-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:deleted_at
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Portfolios",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function statusFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "Active";
    else if(cellValue == '0')
        return "InActive";
    else
        return '';
}

/**
 * jqGrid function to format is_default column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function isDefaultFormatter (cellValue, options, rowObject){
    if (cellValue == '1')
        return "Yes";
    else if(cellValue == '0')
        return "No";
    else
        return '';
}

/**  List Action Functions  */
$(document).on('change', '.select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    $('#saveBtnId').text('Update');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {
        var validator = $( "#addPortfolioForm" ).validate();
        validator.resetForm();
        $('.table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        $(".clear-btn").text('Reset');
        $(".clear-btn").addClass('formreset');
        $(".clear-btn").removeClass('clearFormReset');
        $.ajax({
            type: 'post',
            url: '/portfolio/get',
            data: {id : id},
            success: function (response) {
                $("#portfolio_add_div").show(500);
                headerDiv.innerText = "Edit Portfolio";
                $('#saveBtnId').val('Update');
                var data = $.parseJSON(response);

                if (data.code == 200) {
                    $("#portfolio_id").val(data.data.portfolio_id);
                    $("#portfolio_name").val(data.data.portfolio_name);
                    if(data.data.is_default == '1') $("#is_default").prop('checked',true);
                    if(data.data.is_default == '0') $("#is_default").prop('checked',false);
                    $("#is_default").val(data.data.is_default);
                    $('#record_id').val(data.data.id);
                    getCustomfield(data.custom_data);
                      $('#portfolio-table').trigger('reloadGrid');
                    defaultFormData = $('#addPortfolioForm').serializeArray();
                } else if (data.status == "error"){
                    toastr.error(data.message);
                } else{
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        //$("#portfolio-table").trigger('reloadGrid');
    } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to " + opt + " this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    var status = opt == 'activate' ? '1' : '0';
                    $.ajax({
                        type: 'post',
                        url: '/portfolio/updateStatus',
                        data: {
                            status: status,
                            id: id
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                  $('#portfolio-table').trigger('reloadGrid');
                                   onTop(true);
                            } else {
                                toastr.error(response.message);
                            }
                           
                        }
                    });
                }
            
            }
        });
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/portfolio/delete',
                        data: {
                            id: id
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                  $('#portfolio-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        
                        }
                    });
                }
            
            }
        });
    }
    else if(opt == 'Add in-touch' || opt == 'Add in-touch'){
        window.location.href = "/Communication/NewInTouch?tid="+id+"&category=Portfolio";
    }
    else if(opt == 'in-touch-history' || opt == 'in-touch-history'){
        var propertyname=$('#'+id).find('td:nth-child(2)').text();
        localStorage.setItem("propertyname",propertyname);
        window.location.href = "/Communication/InTouch";
    }
    $('.select_options').prop('selectedIndex',0);
});
