$(document).ready(function() {
    var base_url = window.location.origin;
    $("#amenity_name").keypress(function(e) {
        var keyCode = e.which; /*          8 - (backspace)          32 - (space)          48-57 - (0-9)Numbers        */
        /* Not allow special */
        if (!((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)) && keyCode != 8 && keyCode != 32) {
            e.preventDefault();
        }
    });
    /** Show add new propertySetup amenities div on add new button click */
    $(document).on('click', '#addAmenitiesBtnId', function() {
        $('#amenity_name').val('').prop('disabled', false);
        $('#nameErr').text('');
        $('#name-error').text('');
        $('#amenity_name-error').text('');
        $("#code").val('');
        $('#codeErr').text('');
        $('#code-error').text('');
        $('.type_checkbox').prop('checked', false);
        $('#typeErr').text('');
        $('#type-error').text('');
        $("#hidden_amenity_id").val('');
        headerDiv.innerText = "New Amenity";
        $('#saveAmenityBtnId').val('Save');
        $('#addAmenitiesDivId').show(500);
    });
    /** Hide add new propertySetup amenities div on cancel button click */
    $(document).on("click", "#addAmenityCancelBtn", function(e) {
        e.preventDefault();
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {
                confirm: {
                    label: 'Yes'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function(result) {
                if (result == true) {
                    $("#addAmenitiesDivId").hide(500);
                }
            }
        });
    });

    /** jqGrid status */
    $('#jqGridStatus').on('change', function() {
        var selected = this.value;
        $('#Amenities-table').jqGrid('GridUnload');
        changeGridStatus(selected);
        jqGrid(selected, true);
        $("#addAmenitiesDivId").hide(500);
    });
    /**     * jqGrid Initialization function     * @param status     */
    function jqGrid(status, deleted_at) {
        var table = 'company_property_amenities';
        var columns = ['Amenity Name', 'Amenity Code', 'Amenity Type', 'Status', 'Action'];
        var select_column = ['Edit', 'Deactivate', 'Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['company_property_amenities.status', 'company_property_amenities.is_editable', 'company_property_amenities.deleted_at'];
        var columns_options = [{
            name: 'Amenity Name',
            index: 'name',
            width: 90,
            align: "center",
            searchoptions: {
                sopt: conditions
            },
            table: table
        }, {
            name: 'Amenity Code',
            index: 'code',
            width: 100,
            align: "center",
            searchoptions: {
                sopt: conditions
            },
            table: table
        }, {
            name: 'Amenity Type',
            index: 'type',
            width: 80,
            align: "center",
            searchoptions: {
                sopt: conditions
            },
            table: table,
            formatter: amenityTypeFormatter
        }, {
            name: 'Status',
            index: 'status',
            width: 80,
            align: "center",
            searchoptions: {
                sopt: conditions
            },
            table: table,
            formatter: statusFormatter
        }, {
            name: 'Action',
            index: 'select',
            title: false,
            width: 80,
            align: "right",
            sortable: false,
            cellEdit: true,
            cellsubmit: 'clientArray',
            editable: true,
            formatter: 'select',
            edittype: 'select',
            search: false,
            table: table,
            formatter: actionFormatter
        }];
        var ignore_array = [];
        jQuery("#Amenities-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_property_amenities",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Amenities",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {
                    fromServer: true
                }
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {},
            {},
            {},
            {top:200,left:200,drag:true,resize:false}
        );
    }
    /** jqGrid function to format amenity type     * @param cellValue     * @param options     * @param rowObject     * @returns {string}     */
    function amenityTypeFormatter(cellValue, options, rowObject) {
        if (typeof cellValue !== "undefined" && cellValue !== "undefined") {
            var nameArr = cellValue.split(',');
        }
        var string = "";
        if ($.isArray(nameArr)) {
            $.each(nameArr, function(i, l) {
                if (l == 1) {
                    string += 'Property,';
                }
                if (l == 2) {
                    string += 'Building,';
                }
                if (l == 3) {
                    string += 'Unit';
                }
            });
            string = string.replace(/,\s*$/, "");
            return string;
        } else {
            return string;
        }
    } /**     * jqGrid function to format status     * @param cellValue     * @param options     * @param rowObject     * @returns {string}     */
    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 1) return "Active";
        else if (cellValue == '0') return "InActive";
        else return '';
    } /**     * jqGrid function to format action column     * @param cellValue     * @param options     * @param rowObject     * @returns {string}     */
    function actionFormatter(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            var editable = $(cellValue).attr('editable');
            var select = '';
            if (rowObject.Status == 1) select = ['Edit', 'Deactivate', 'Delete'];
            if (rowObject.Status == '0' || rowObject.Status == '') select = ['Edit', 'Activate', 'Delete'];
            var data = '';
            if (select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function(key, val) {
                    if (editable == '0' && (val == 'delete' || val == 'Delete')) {
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    $("#addAmenityFormId").validate({
        rules: {
            name: {
                required: true
            },
            code: {
                number: true
            },
            type: {
                required: true
            },
        },
        messages: {
            code: {
                number: "* Numbers only",
            }
        },
        submitHandler: function() {
            var name = $('#amenity_name').val();
            var code = $('#code').val();
            var hidden_amenity_id = $('#hidden_amenity_id').val();
            var type_string = '';
            $.each($(".type_checkbox:checked"), function() {
                type_string += $(this).val() + ',';
                return type_string;
            });
            /* replace last comma from string 'type_string' */
            type_string = type_string.replace(/,\s*$/, "");
            var formData = {
                'name': name,
                'code': code,
                'type': type_string,
                'hidden_amenity_id': hidden_amenity_id
            };
            if (hidden_amenity_id) {
                var action = 'update';
            } else {
                var action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/Amenities-Ajax',
                data: {
                    class: 'AmenitiesAjax',
                    action: action,
                    form: formData
                },
                success: function(response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#addAmenitiesDivId").hide(500);
                        $("#Amenities-table").trigger('reloadGrid');
                        toastr.success(response.message);
                        onTop(true);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function(key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            })
        }
    });

    /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function() {
            $(".select_options").val("default");
        }, 200);
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index();
        if (opt == 'Edit' || opt == 'EDIT') {
            $("#addAmenitiesDivId").show(500);
            headerDiv.innerText = "Edit Amenity";
            $('#saveAmenityBtnId').val('Update');
            $('.type_checkbox').prop('checked', false);
            $('#nameErr').text('');
            $('#amenity_name-error').text('');
            $('#name-error').text('');
            $('#codeErr').text('');
            $('#code-error').text('');
            $('#typeErr').text('');
            $('#type-error').text('');
            $('.table').find('.green_row_left, .green_row_right').each(function() {
                $(this).removeClass("green_row_left green_row_right");
            });
            $('.table').find('tr:eq(' + row_num + ')').find('td:eq(0)').addClass("green_row_left");
            $('.table').find('tr:eq(' + row_num + ')').find('td').last().addClass("green_row_right");
            $(".clear-btn").text('Reset');
            $(".clear-btn").addClass('formreset');
            $(".clear-btn").removeClass('clearFormReset');
            $.ajax({
                type: 'post',
                url: '/Amenities-Ajax',
                data: {
                    class: "AmenitiesAjax",
                    action: "view",
                    id: id,
                },
                success: function(response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success") {
                        $("#amenity_name").val(data.data.name);
                        $("#code").val(data.data.code);
                        $("#hidden_amenity_id").val(data.data.id);
                        if (data.data.is_editable == 0) {
                            $('#amenity_name').prop('disabled', true);
                        } else {
                            $('#amenity_name').prop('disabled', false);
                        }
                        var typeString = data.data.type;
                        var typeArr = typeString.split(',');
                        if ($.isArray(typeArr)) {
                            $.each(typeArr, function(key, value) {
                                if (value == 1) {
                                    $('#amenity1').prop('checked', true);
                                }
                                if (value == 2) {
                                    $('#amenity2').prop('checked', true);
                                }
                                if (value == 3) {
                                    $('#amenity3').prop('checked', true);
                                }
                            });
                            defaultFormData = $('#addAmenityFormId').serializeArray();
                        }
                    } else if (data.status == "error") {
                        toastr.error(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function(data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function(key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to " + opt + " this record ?",
                buttons: {
                    confirm: {
                        label: 'Yes'
                    },
                    cancel: {
                        label: 'No'
                    }
                },
                callback: function(result) {
                    if (result == true) {
                        var status = opt == 'activate' ? '1' : '0';
                        $.ajax({
                            type: 'post',
                            url: '/Amenities-Ajax',
                            data: {
                                class: 'AmenitiesAjax',
                                action: 'updateStatus',
                                status: status,
                                id: id
                            },
                            success: function(response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                    $('#Amenities-table').trigger('reloadGrid');
                                    onTop(true);
                                } else {
                                    toastr.error(response.message);
                                    onTop(true);
                                }
                            }
                        });
                    }

                }
            });
        } else if (opt == 'Delete' || opt == 'DELETE') {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {
                    confirm: {
                        label: 'Yes'
                    },
                    cancel: {
                        label: 'No'
                    }
                },
                callback: function(result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/Amenities-Ajax',
                            data: {
                                class: 'AmenitiesAjax',
                                action: 'delete',
                                id: id
                            },
                            success: function(response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                    $('#Amenities-table').trigger('reloadGrid');
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }

                }
            });
        }
    });
});