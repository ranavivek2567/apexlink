$(document).ready(function () {
    $("#group_name").keypress(function(e){
        var keyCode = e.which;
        /*
          8 - (backspace)
          32 - (space)
          48-57 - (0-9)Numbers
        */
        // Not allow special
        if ( !( (keyCode >= 48 && keyCode <= 57)
            ||(keyCode >= 65 && keyCode <= 90)
            || (keyCode >= 97 && keyCode <= 122) )
            && keyCode != 8 && keyCode != 32) {
            e.preventDefault();
        }
    });

    var base_url = window.location.origin;
    var login_user_id =  localStorage.getItem("login_user_id");


    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#propertygroups-table').jqGrid('GridUnload');
        changeGridStatus(selected);
        jqGrid(selected);
        $("#addPropertyGroups").hide(500);
    });

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status) {
        var table = 'company_property_groups';
        var columns = ['Property Group Name','Description', 'Set as Default', 'Status','Action'];
        var select_column = ['Edit','Deactivate','Delete','Add In-Touch','In - Touch History'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_property_groups.status','company_property_groups.is_editable'];
        var columns_options = [
            {name:'Group Name',index:'group_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Is Default',index:'is_default', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:isDefaultFmatter},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            //{name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false}
            {name:'Action',index:'select', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFmatter,title:false}
        ];
        var ignore_array = [];
        jQuery("#propertygroups-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            sortname: 'updated_at',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_property_groups",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns
            },
            viewrecords: true,
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Property Groups",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

    $(document).on("click",'#export_property_groups_button',function(){
        var status = $("#jqGridStatus option:selected").val();
        var table =  'company_property_groups';
        window.location.href = base_url+"/export-groups-excel?status="+status+"&&table="+table+"&&action=exportExcel";
    });

    $(document).on("click",'#export_sample_property_groups_button',function(){
        window.location.href = base_url+"/export-groups-excel?status="+"&&action=exportSampleExcel";
    });




    /**
     * jqGrid function to format status
     * @param status
     */
    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Active";
        else if(cellvalue == '0')
            return "InActive";
        else
            return '';
    }
    /**
     * jqGrid function to format is_default column
     * @param status
     */
    function isDefaultFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Yes";
        else if(cellvalue == '0')
            return "No";
        else
            return '';
    }



    /**
     * jqGrid function to format action column
     * @param status
     */

    function actionFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            var editable = $(cellvalue).attr('editable');
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete','Add In-Touch','In - Touch History'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            if(rowObject.Status == '0' && rowObject.is_editable == '0')  select = ['Edit','Activate'];
            if(rowObject.Status == '0' && rowObject.is_editable == '1')  select = ['Edit','Activate','Delete'];
            if(rowObject.Status == '1' && rowObject.is_editable == '0')  select = ['Edit','Activate'];
            if(rowObject.Status == '1' && rowObject.is_editable == '1')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    /**
     *change function to perform various actions(edit ,activate,deactivate)
     * @param status
     */
    jQuery(document).on('change','.select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        if(select_options == 'Edit')
        {
            $('#property_groups_span').html('Edit Property Group');
            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            jQuery('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            var validator = $( "#add_property_groups_form" ).validate();
            validator.resetForm();
            $("#save_property_groups").text('Update');
            $(".clear-btn").text('Reset');
            $(".clear-btn").addClass('formreset');
            $(".clear-btn").removeClass('clearFormReset');
            $.ajax({
                type: 'post',
                url: '/propertygroups-ajax',
                data: {
                    class: "PropertyGroupsAjax",
                    action: "getPropertyGroupsData",
                    id:data_id,
                },
                success: function (response) {
                    $("#addPropertyGroups").show(500);
                    var response = $.parseJSON(response);
                    var property_type_data = response['data']['property_groups_data'];
                    if(property_type_data) {
                        $('#form_type').val('edit');
                        $('#property_groups_id').val(data_id);
                        $.each(property_type_data, function (key, value) {
                            $('#' + key).val(value);
                            //$('.' + key).text(value);
                            if(key == 'is_default' && value == 1){
                                $('#' + key).prop("checked",true);
                            }else{
                                $('#' + key).prop("checked",false);
                            }
                            if(key == 'is_editable' && value == 0){
                                jQuery("#property_type").prop("readonly", true);
                               // $('#' + key).attr('readonly', true);

                            }
                        });
                        defaultFormData = $('#add_property_groups_form').serializeArray();
                    }
                     triggerReload();
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }else if(select_options == 'Deactivate') {
            bootbox.confirm({
                message: "Do you want to deactivate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/propertygroups-ajax',
                            data: {class: 'PropertyGroupsAjax', action: 'deactivate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Group Deactivated successfully');
                                    onTop(true);
                                    bootbox.hideAll()
                                     triggerReload();
                                }else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                    bootbox.hideAll()
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Activate') {
            bootbox.confirm({
                message: "Do you want to activate the record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/propertygroups-ajax',
                            data: {class: 'PropertyGroupsAjax', action: 'activate', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Group activated successfully');
                                    onTop(true);
                                    bootbox.hideAll();
                                     triggerReload();
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }else if(select_options == 'Delete')
        {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/propertygroups-ajax',
                            data: {class: 'PropertyGroupsAjax', action: 'delete', id: data_id},
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success('Group deleted successfully');
                                    bootbox.hideAll();
                                     triggerReload();
                                } else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                    bootbox.hideAll()
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                    triggerReload();
                }
            });
        }
        else if(select_options == 'Add In-Touch'){
            window.location.href = "/Communication/NewInTouch?tid="+data_id+"&category=Group";
        }
        else if(select_options == 'In - Touch History'){
            var propertyname=$('#'+data_id).find('td:first').text();
            localStorage.setItem("propertyname",propertyname);
            window.location.href = "/Communication/InTouch";
        }
        $('.select_options').prop('selectedIndex',0);
    });

    $(document).on('click','#add_property_groups_button',function (e) {
        $('#property_groups_span').html('Add Property Group');
        $('#description').html('');
        $('#property_groups_id').val('');
        $('#form_type').val('add');
        $('#save_property_groups').text('Save');
        $("#addPropertyGroups").show(500);
        var validator = $( "#add_property_groups_form" ).validate();
        validator.resetForm();
    })

    $(document).on('click','#import_property_groups_button',function (e) {
        $("#import_file").val('');
        $("#import_file-error").text('');
        $("#ImportPropertyGroups").show(500);
        return false;
    })



    $(document).on("click", "#add_groups_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#addPropertyGroups").hide(500);
                }
            }
        });
    });


    $(document).on("click", "#import_groups_cancel", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#ImportPropertyGroups").hide(500);
                }
            }
        });
    });


});

 function triggerReload(){
    var grid = $("#propertygroups-table");
    grid[0].p.search = false;
    $.extend(grid[0].p.postData,{filters:""});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

