$(document).ready(function (){
    /** Show add new unit type div on add new button click */
    $(document).on('click','#addUnitTypeButton',function () {
        $('#unit_type').val('').prop('disabled', false);
        $('#description').val('');
        $("#unit_type_id").val('');
        $('#unit_typeErr').text('');
        headerDiv.innerText = "Add New Vehicle";
        $('#is_default').prop('checked', false);
        $('#saveBtnId').val('Save');
        $('#add_unit_type_div').show(500);
    });
    /** Hide add new unit type div on cancel button click */
    $(document).on("click", "#add_unit_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#add_unit_type_div").hide(500);
            } else {
            }
        });
    });
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All')
    function jqGrid(status, deleted_at) {
        var table = 'company_manage_vehicle';
        var columns = ['Vehicle Name','Vehicle #', 'Vehicle Type', 'Make', 'Model','VIN #','Year','Date Purchased','Vehicle Prize('+default_currency_symbol+')','Action'];
        var select_column = ['Edit','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var pagination=[];

        var columns_options = [

            {name:'Vehicle Name',index:'vehicle_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Vehicle #',index:'vehicle', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Vehicle Type',index:'vehicle_type', width:80,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Make',index:'make', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Model',index:'model', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'VIN #',index:'vin', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Year',index:'year_of_vehicle', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Date Purchased',index:'date_purchased', width:80, align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Vehicle Prize('+default_currency_symbol+')',index:'amount', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},

        ];
        var ignore_array = [];
        jQuery("#manageVehicle-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_manage_vehicle",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Vehicles",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }

    /*  insert into vehicle type*/

    $(document).on("click","#saveBtns", function(e) {
        e.preventDefault();

         //var vehicle_name = $("input[name='vehicle_name']").val();
        // var vehicle = $("input[name='vehicle']").val();
        // var vehicle_type = $("input[name='vehicle_type']").val();
        // var make = $("input[name='make']").val();
        // var model = $("input[name='model']").val();
        // var vin = $("input[name='vin']").val();
        // var registration = $("input[name='registration']").val();
        // var plate_number = $("input[name='plate_number']").val();
        // var year_of_vehicle = $("input[name='year_of_vehicle']").val();
        // var date_purchased = $("input[name='date_purchased']").val();
        // var starting_mileage = $("input[name='starting_mileage']").val();
        // var amount = $("input[name='amount']").val();
        // var created_at = $("text[name='created_at']").val();
        // var updated_at = $("text[name='updated_at']").val();
        var formdata = $('#add_vehicle_type').serializeArray();
        var user_id_hidden = $("input[name='user_id_hidden12']").val();


        // console.log(vehicle_type);
        $.ajax({
            type: 'post',
            url: '/MasterData/manageVehicle-Ajax',
            data: {
                class: 'ManageVehicleAjax',
                action: "insert",
                form: formdata,
                user_id_hidden:user_id_hidden

            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {

                    $('#manageVehicle-table').trigger('reloadGrid');
                    $("#add_vehicle_type").trigger("reset");
                    $("#add_unit_type_div").hide(500);
                    localStorage.setItem("Message", "Record Inserted Successfully")
                    localStorage.setItem("rowcolor", true)
                    window.location.reload();


                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
            }
        });
    });
    function getVehicleDetail(id){
        $.ajax({
            type: 'post',
            url: '/MasterData/manageVehicle-Ajax',
            data: {
                class: "ManageVehicleAjax",
                action: "getvehicleType",
                cuser_id: id
            },
            success: function (response) {

                var response = JSON.parse(response);

                $("#add_vehicle_type input[name='vehicle_name']").val(response.vehicle_name);
                $("#add_vehicle_type input[name='vehicle']").val(response.vehicle);
                $("#add_vehicle_type input[name='vehicle_type']").val(response.vehicle_type);
                $("#add_vehicle_type input[name='make']").val(response.make);
                $("#add_vehicle_type input[name='model']").val(response.model);
                $("#add_vehicle_type input[name='vin']").val(response.vin);
                $("#add_vehicle_type input[name='color']").val(response.color);
                $("#add_vehicle_type input[name='registration']").val(response.registration);
                $("#add_vehicle_type input[name='plate_number']").val(response.plate_number);
                $("#add_vehicle_type input[name='year_of_vehicle']").val(response.year_of_vehicle);
                $("#add_vehicle_type input[name='date_purchased']").val(response.date_purchased);
                $("#add_vehicle_type input[name='starting_mileage']").val(response.starting_mileage);
                $("#add_vehicle_type input[name='amount']").val(response.amount);


                localStorage.setItem("Message","Record updated successfully")
                localStorage.setItem("rowcolor",true)

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function deletevehicleType(id) {
        $.ajax({
            type: 'post',
            url: '/MasterData/manageVehicle-Ajax',
            data: {
                class: "PropertyTypeAjax",
                action: "deletevehicleType",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The record deleted successfully.");
                    $('#manageVehicle-table').trigger( 'reloadGrid' );
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("change", "#manageVehicle-table .select_options", function (e) {
        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');
        console.log(id);

        console.log('-----');

        switch(action) {

            case "Edit":
                $("#add_vehicle_type #user_id_hidden12").val(id);
                $("#add_unit_type_div").show();
                getVehicleDetail(id);
                break;

            case "Delete":

                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        deletevehicleType(id);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }

        return false;
    });
});