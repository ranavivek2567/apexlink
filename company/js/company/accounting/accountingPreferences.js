/**
 * Add/Edit account preferences detail
 */
$("#add_account_preference_form").validate({
    rules: {
        eft_generate_date: {
            required: true,
            number: true
        },
        warn_transaction_days_past: {
            number: true
        },
        warn_transaction_days_future: {
            number: true
        },
        transfer_fee_charge: {
            number: true
        },
    },
    messages: {
        eft_generate_date : {
            number: "* Please add numerical values only",
        },
        warn_transaction_days_past : {
            number: "* Please add numerical values only",
        },
        warn_transaction_days_future : {
            number: "* Please add numerical values only",
        },
        transfer_fee_charge : {
            number: "* Please add numerical values only",
        },
    },
    submitHandler: function() {
        var formData = $("#add_account_preference_form").serializeArray();

        $.ajax({
            type: 'post',
            url: '/AccountPreference-Ajax',
            data: {
                class: 'accountingPreferences',
                action: 'update',
                form: formData
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#viewMode').show();
                    viewAccountPreference('view');
                    $('#editMode').hide();
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 503) {
                    toastr.warning(response.message);
                }
            }
        })
    }
});


onLoadDataOfAccountPreference();
var data_id = '';
function onLoadDataOfAccountPreference(){
    $.ajax({
        type: 'post',
        url: '/AccountPreference-Ajax',
        data: {
            class: 'accountingPreferences',
            action: 'getOnLoadData',
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                if(response.data == ''){
                    data_id = '';
                } else {
                    data_id = response.data.id;
                }
            } else {
                toastr.error(response.message);
            }
        }
    });
}

viewAccountPreference('view', '');

function viewAccountPreference(action, data_id){
    if(action == 'view'){
        var method = 'view';
    } else {
        var method = 'viewForEdit';
    }

    $.ajax({
        type: 'post',
        url: '/AccountPreference-Ajax',
        data: {
            class: 'accountingPreferences',
            action: method,
            id : data_id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                console.log('aaa',action);
                console.log('bbb',response.data);
                if(response.data == '' && action == 'view')
                {
                    $('.fiscal_year_first_month').text('Select');
                    $('.cash_account_credit_bill_posting').text('Select');
                    $('.rent_charges').text('Select');
                    $('.check_receive_charge').text('Select');
                    $('.debit_type_receive_payment_charge').text('Select');
                    $('.vacancy_charge').text('Select');
                    $('.waiveoff_on_payment_received_charge').text('Select');
                    $('.bad_debit_charge').text('Select');
                    $('.write_off_charge').text('Select');
                    $('.auto_debit_payment_receive_charge').text('Select');
                    $('.cash_payment_received_charge').text('Select');
                    $('.nsf_charge').text('Select');
                    $('.check_bounce_charge').text('Select');
                    $('.check_issue_vendor_tenant_charge').text('Select');
                    $('.refund_charge').text('Select');
                    $('.interest_received_from_bank_charge').text('Select');
                    $('.bank_adjustment_fee_charge').text('Select');
                    $('.concession_charge').text('Select');
                    $('.late_fee_charge').text('Select');
                    $('.discount_account_id').text('Select');
                    $('.money_order_payment_received_charge').text('Select');
                    $('.eft_charge').text('Select');
                    $('.cam_charges').text('Select');
                    $('.sd_refund_charge').text('Select');
                }
                $.each(response.data, function (key, value) {
                    if(action == 'view'){
                        if(value) {
                            $('.'+key).text(value);
                        } else {
                            if( key == 'warn_transaction_days_past') {
                                $('.warn_transaction_days_past').text('No');
                            }else if(key == 'warn_transaction_days_future' ){
                                $('.warn_transaction_days_future').text('No');
                            } else {
                                $('.' + key).text('Select');
                            }
                        }

                        if( key == 'auto_invoice') {
                            (value == 1) ? $('.autoInvoiceDivForView').show() : $('.autoInvoiceDivForView').hide();
                        }
                        if( key == 'lease_expire_soon_email') {
                            (value == 1) ? $('.'+key).text('Yes') : $('.'+key).text('No');
                        }
                        if( key == 'use_parenthesis_instead_of_a_minus_sign') {
                            (value == 1) ? $('.'+key).text('Yes') : $('.'+key).text('No');
                        }
                        if( key == 'color_negative_balances_in_red') {
                            (value == 1) ? $('.'+key).text('Yes') : $('.'+key).text('No');
                        }
                        if( key == 'auto_invoice') {
                            (value == 1) ? $('.'+key).text('Yes') : $('.'+key).text('No');
                        }
                        if( key == 'warn_transaction_days_past' && value != '') {
                            $('.warn_transaction_days_past').text('Yes-'+value+' days');
                        }
                        if( key == 'warn_transaction_days_future' && value != '') {
                            $('.warn_transaction_days_future').text('Yes-'+value+' days');
                        }
                    }
                    if(action == 'viewForEdit'){
                        $('#'+key).val(value);

                        if(key == 'warning_past_status'){
                            if (value == 1){
                                $('#warning_past_status').attr('checked', true);
                                $(".warnIfInPastClass").attr('readonly',false);
                            } else {
                                $(".warnIfInPastClass").attr({readonly:true, placeholder : 'Days', value : ''});
                                $('#warn_transaction_days_past').val('');
                            }
                        }

                        if(key == 'warning_future_status'){
                            if (value == 1){
                                $('#warning_future_status').attr('checked', true);
                                $(".warnIfInFutureClass").attr({readonly:false, placeholder : 'Days', value : '0'});
                            } else {
                                $(".warnIfInFutureClass").attr({readonly:true, placeholder : 'Days', value : ''});
                                $('#warn_transaction_days_future').val('');
                            }
                        }
                        if( key == 'color_negative_balances_in_red') {
                            (value == 1) ? $('#ColorNegativeBalanceYES').prop('checked', true) : $('#ColorNegativeBalanceNO').prop('checked', true);
                        }

                        if( key == 'lease_expire_soon_email') {
                            (value == 1) ? $('#LeaseExpiryYES').attr('checked', true) : $('#LeaseExpiryNo').attr('checked', true);
                        }

                        if( key == 'use_parenthesis_instead_of_a_minus_sign') {
                            (value == 1) ? $('#UseParenthesisInsteadOfAMinusSignYES').attr('checked', true) : $('#UseParenthesisInsteadOfAMinusSignNO').attr('checked', true);
                        }

                        if( key == 'auto_invoice') {
                            if (value == 1) {
                                $('.divAutoInvoiceDays').show();
                                $('#AutoInvoiceYES').attr('checked', true);
                            } else {
                                $('.divAutoInvoiceDays').hide();
                                $('#AutoInvoiceNO').attr('checked', true);
                            }
                        }
                    }
                });


                if( response.data.warning_future_status == 1 && response.data.warn_transaction_days_future == '') {
                     $('.warn_transaction_days_future').text('Yes-0 days');
                }
                if( response.data.warning_past_status == 1 && response.data.warn_transaction_days_past == '') {
                    $('.warn_transaction_days_past').text('Yes-0 days');
                }
            } else {
                toastr.error(response.message);
            }
        }
    });
}



$('#editLink').on('click', function(){
    $('#viewMode').hide();
    $('#editMode').show();
    viewAccountPreference('viewForEdit', data_id);
});
$('#cancelEdit').on('click', function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('#viewMode').show();
            $('#editMode').hide();
        }
    });
});

/**
 * Enable/Disable 'warn If In Past' div
 */
$(document).on('change', '#warning_past_status', function () {
    this.value = this.checked ? 1 : 0;
    if (this.value == 1){
        $(".warnIfInPastClass").attr('readonly',false);
    } else {
        $(".warnIfInPastClass").val('');
        $(".warnIfInPastClass").attr({readonly:true, placeholder : 'Days'});
        $('#warn_transaction_days_past').val('');
    }
});

/**
 * Enable/Disable 'warn If In Future' div
 */
$(document).on('change', '#warning_future_status', function () {
    this.value = this.checked ? 1 : 0;
    if (this.value == 1){
        $(".warnIfInFutureClass").attr('readonly',false);
    } else {
        $(".warnIfInFutureClass").val('');
        $(".warnIfInFutureClass").attr({readonly:true, placeholder : 'Days'});
        $('#warn_transaction_days_future').val('');
    }
});

/**
 * Show & Hide 'Auto Invoice' div
 * @type {string}
 */
var AutoInvoiceVal = '';
$('.AutoInvoiceClass').on('click', function () {
    AutoInvoiceVal =  $(this).val();
    if(AutoInvoiceVal == 1){
        $('.divAutoInvoiceDays').show();
    } else {
        $('.divAutoInvoiceDays').hide();
    }
});

creditAccountList();
chargeCodeList();

function chargeCodeList(){
    $.ajax({
        type: 'post',
        url: '/ChargeCode-Ajax',
        data: {
            class: 'ChargeCodeAjax',
            action: 'getAllChargeCodeList',
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#rent_charges').empty().append("<option value=''>Select</option>");
                $('#check_receive_charge').empty().append("<option value=''>Select</option>");
                $('#debit_type_receive_payment_charge').empty().append("<option value=''>Select</option>");
                $('#vacancy_charge').empty().append("<option value=''>Select</option>");
                $('#waiveoff_on_payment_received_charge').empty().append("<option value=''>Select</option>");
                $('#bad_debit_charge').empty().append("<option value=''>Select</option>");
                $('#write_off_charge').empty().append("<option value=''>Select</option>");
                $('#auto_debit_payment_receive_charge').empty().append("<option value=''>Select</option>");
                $('#cash_payment_received_charge').empty().append("<option value=''>Select</option>");
                $('#nsf_charge').empty().append("<option value=''>Select</option>");
                $('#check_bounce_charge').empty().append("<option value=''>Select</option>");
                $('#check_issue_vendor_tenant_charge').empty().append("<option value=''>Select</option>");
                $('#refund_charge').empty().append("<option value=''>Select</option>");
                $('#interest_received_from_bank_charge').empty().append("<option value=''>Select</option>");
                $('#bank_adjustment_fee_charge').empty().append("<option value=''>Select</option>");
                $('#concession_charge').empty().append("<option value=''>Select</option>");
                $('#late_fee_charge').empty().append("<option value=''>Select</option>");
                $('#money_order_payment_received_charge').empty().append("<option value=''>Select</option>");
                $('#eft_charge').empty().append("<option value=''>Select</option>");
                $('#cam_charges').empty().append("<option value=''>Select</option>");
                $('#sd_refund_charge').empty().append("<option value=''>Select</option>");
                $.each(response.data, function (key, value) {
                    $('#rent_charges').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#check_receive_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#debit_type_receive_payment_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#vacancy_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#waiveoff_on_payment_received_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#bad_debit_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#auto_debit_payment_receive_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#write_off_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#cash_payment_received_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#nsf_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#check_bounce_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#check_issue_vendor_tenant_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#refund_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#interest_received_from_bank_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#bank_adjustment_fee_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#concession_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#late_fee_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#money_order_payment_received_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#eft_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#cam_charges').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                    $('#sd_refund_charge').append($("<option value = "+value.id+">"+value.charge_code+' - '+value.description+"</option>"));
                })
            } else {
                toastr.error(response.message);
            }
        }
    });
}

function creditAccountList(){
    $.ajax({
        type: 'post',
        url: '/ChargeCode-Ajax',
        data: {
            class: 'ChargeCodeAjax',
            action: 'getAllCreditList',
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $('#cash_account_credit_bill_posting').empty().append("<option value=''>Select</option>");
                $('#discount_account_id').empty().append("<option value=''>Select</option>");
                $.each(response.data, function (key, value) {
                    var name = value.credit_accounts;
                    var name = name.replace('-', ' : ');

                    $('#cash_account_credit_bill_posting').append($("<option value = "+value.id+">"+name+"</option>"));
                    $('#discount_account_id').append($("<option value = "+value.id+">"+name+"</option>"));
                })
            } else {
                toastr.error(response.message);
            }
        }
    });
}