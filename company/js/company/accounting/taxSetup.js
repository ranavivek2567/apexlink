$(document).ready(function () {
    var base_url = window.location.origin;

    var status =  localStorage.getItem("active_inactive_status");
    console.log(status);
    if(status !== undefined) {
        if ($("#TaxSetup-table")[0].grid) {
            $('#TaxSetup-table').jqGrid('GridUnload');
        }
        //intializing jqGrid
        if(status == 'all'){
            jqGrid('All');
        }  else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#TaxSetup-table")[0].grid) {
            $('#TaxSetup-table').jqGrid('GridUnload');
        }
        jqGrid('All');
    }

    /** Show add new unit type div on add new button click */
    $(document).on('click','#addUnitTypeButton',function () {
        // $('#tax_name').val('').prop('disabled', false);
        $('#tax_name').val('');
        $('#tax_type').val('');
        $("#taxvalue").val('');
        $("#tax_type_id").val('');
        $('#status').text('');
        $('#tax_type-error').text('');

        headerDiv.innerText = "Add Tax Type";
        $('#is_default').prop('checked', false);
        $('#saveBtnId').val('Save');
        $('#tax_value').html('<div id="tax_value">' +
            ' <div class="col-sm-3">\n' +
            ' <label>Value <em class="red-star">*</em></label>\n' +
            '<input name="value" id="taxvalue" maxlength="100" value="" class="form-control" type="text"/>\n' +
            '<span id="unit_typeErr"></span>' +
            '</div>');

        $('#add_tax_type_div').show(500);
    });


    /** Hide add new unit type div on cancel button click */
    $(document).on("click", "#add_unit_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#add_tax_type_div").hide(500);
            } else {
            }
        });
    });

    /** Hide import excel div on cancel button click */
    $(document).on("click", "#import_unit_cancel_btn", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#import_unit_type_div").hide(500);
            } else {
            }
        });
    });

    /** jqGrid status change*/
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#TaxSetup-table').jqGrid('GridUnload');
        $('#add_tax_type_div').hide(500);
        changeGridStatus(selected);
        jqGrid(selected, true);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        var table = 'company_accounting_tax_setup';
        var columns = ['Tax Name','Type', 'Value', 'Status', 'Action'];
        var select_column = ['Edit','Deactivate','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var pagination;
        var extra_columns = ['company_accounting_tax_setup.status', 'company_accounting_tax_setup.deleted_at'];
        var columns_options = [
            {name:'Tax Name',index:'tax_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Type',index:'tax_type', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Value',index:'value', width:80,align:"center",searchoptions: {sopt: conditions},table:table, formatter:ammountFormatter},
            {name:'Status',index:'status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFormatter}
        ];
        var ignore_array = [];
        jQuery("#TaxSetup-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_accounting_tax_setup",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Tax",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {top:200,left:200,drag:true,resize:false}
        );
    }
    /**
     * jqGrid function to currency status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ammountFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            if (cellValue != '') {
                if(rowObject.Type == 'Flat'){
                    return default_currency_symbol+cellValue;
                } else {
                   return cellValue;
                }
            } else {
                return '';
            }
        }
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }

    /**
     * jqGrid function to format action column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            var is_default = rowObject;
            var editable = $(cellValue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select ' +
                    ' class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }


    /** Add/Edit new Tax type */
    $("#add_tax_form").validate({
        rules: {
            tax_name: {
                required: true
            },
            tax_type: {
                required: true
            },
            value: {
                required: true
            }
        },
        submitHandler: function () {
            var tax_name = $('#tax_name').val();
            var tax_type = $('#tax_type').val();
            var value = $('#taxvalue').val();
            var tax_type_id = $(this).next("#tax_type_id").val();
            var type =$('#saveBtnId').val();
            if (typeof tax_type_id === "undefined"){
                tax_type_id= $("#tax_type_id").val();
            }
            var formData = {
                'tax_name': tax_name,
                'tax_type': tax_type,
                'value': value,
                'tax_type_id': tax_type_id
            };
            var action;
            if (type == 'Update') {
                action = 'update';
            } else {
                action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/MasterData/Tax-Ajax',
                data: {
                    class: 'TaxTypeAjax',
                    action: action,
                    form: formData
                },
                success: function (response) {
                    // console.log(response);
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#add_tax_type_div").hide(500);
                        $("#TaxSetup-table").trigger('reloadGrid');
                        toastr.success(response.message);
                        onTop(true);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        if (opt == 'Edit' || opt == 'EDIT') {

            $("#add_tax_type_div").show(500);
            headerDiv.innerText = "Edit Tax Type";
            $('#saveBtnId').val('Update');
            $('#tax_name-error').text('');
            $('#tax_type-error').text('');
            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            $('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            $('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            $(".clear-btn").text('Reset');
            $(".clear-btn").addClass('formreset');
            $(".clear-btn").removeClass('clearFormReset');
            $.ajax
            ({
                type: 'post',
                url: '/MasterData/Tax-Ajax',
                data: {
                    class: "TaxTypeAjax",
                    action: "view",
                    id : id,
                },
                success: function (response) {
                    console.log(response);
                    var data = $.parseJSON(response);
                    if (data.status == "success")
                    {
                        $("#tax_name").val(data.data.tax_name);
                        $("#tax_type").val(data.data.tax_type);
                        $("#value").val(data.data.value);

                        $("#tax_type_id").val(data.data.id);

                        if(data.data.is_editable == 0) {
                            $('#unit_type').prop('disabled', true);
                        } else {
                            $('#unit_type').prop('disabled', false);
                        }
                        if(data.data.tax_type == 'Percentage')
                        {
                            $('#tax_value').html('<div id="tax_value">' +
                                '<div class="col-sm-3"  id="tax_value">\n' +
                                ' <label>Value(%) <em class="red-star">*</em></label>\n' +
                                '<input name="value" id="taxvalue" maxlength="100" value="'+data.data.value+'" class="form-control" type="text"/>\n' +
                                '<span id="unit_typeErr"></span>'+
                                '</div>');
                            new AutoNumeric('#taxvalue', {
                                allowDecimalPadding: false,
                                maximumValue  : '100',
                            });
                        }
                        if(data.data.tax_type == 'Flat') {
                            var currency = data.currency;
                            console.log('data>>', data);
                            $('#tax_value').html('<div id="tax_value">' +
                                ' <div class="col-sm-3">\n' +
                                ' <label>Value('+currency+') <em class="red-star">*</em></label>\n' +
                                '<input name="value" id="taxvalue" maxlength="100" value="' + data.data.value + '" class="form-control" type="text"/>\n' +
                                '<span id="unit_typeErr"></span>' +
                                '</div>');
                            new AutoNumeric('#taxvalue', {
                                allowDecimalPadding: true,
                                maximumValue  : '9999999999',
                            });
                        }
                        defaultFormData = $('#add_tax_form').serializeArray();
                    }else if(data.status == "error"){
                        toastr.error(data.message);
                    } else{
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });


        } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
            opt = opt.toLowerCase();
            bootbox.confirm("Do you want to " + opt + " this record ?", function (result) {
                if (result == true) {
                    var status = opt == 'activate' ? '1' : '0';

                    $.ajax({
                        type: 'post',
                        url: '/MasterData/Tax-Ajax',
                        data: {
                            class: 'TaxTypeAjax',
                            action: 'updateStatus',
                            status: status,
                            id: id
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $('#TaxSetup-table').trigger('reloadGrid');
                            localStorage.setItem("rowcolor", 'add colour');
                        }
                    });
                } else {
                    $('#Plans-table').trigger('reloadGrid');
                }
            });

        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            if (status == '1') {
                toastr.warning('A default set value cannot be deleted.');
            } else {
                bootbox.confirm("Do you want to delete this record ?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/MasterData/Tax-Ajax',
                            data: {
                                class: 'TaxTypeAjax',
                                action: 'delete',
                                id: id
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                                $('#TaxSetup-table').trigger('reloadGrid');
                            }
                        });
                    } else {
                        $('#TaxSetup-table').trigger('reloadGrid');
                    }
                });
            }
        } else{
        }
    });

});