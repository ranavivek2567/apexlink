$(document).ready(function() {

    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    var redirect_owner_id = getParameterByName('id');
    var redirect_owner_id_portal=localStorage.getItem('redirect_owner_id_portal');
    var redirect_owner_name = localStorage.getItem('redirect_owner_name');
    if (redirect_owner_id != null && redirect_owner_id!= ''){

        $('#owner_id').val(redirect_owner_name);
        $("#selected_owner_id").val(redirect_owner_id);
        var portfolioData = getDataByID(redirect_owner_id, '','owner');
        $('#portfolio_id').html(portfolioData);


    }
    if (redirect_owner_id_portal != null && redirect_owner_id_portal!= ''){
        $('#owner_id').val(redirect_owner_name);
        $("#selected_owner_id").val(redirect_owner_id_portal);
        var portfolioData = getDataByID(redirect_owner_id_portal, '','owner');
        $('#portfolio_id').html(portfolioData);


    }

    $('#contribution_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, minDate: new Date(), dateFormat: jsDateFormat})
        .datepicker("setDate", new Date());

    $('#cancel_add_owner_contribution_btn').on('click', function () {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                if(last_url.includes("GenerateOwnerContributionPortal")==true || last_url.includes("OwnerPayments")==true){
                    window.location.href = '/Owner/OwnerPayments/OwnerContribution';
                }else{
                    window.location.href = '/Accounting/OwnerContribution';
                }
            }
        });
    });

    //combo grid for owners
    if (redirect_owner_id_portal == null){
        combogrid();
    }
     function combogrid() {
         $('#owner_id').combogrid({
             panelWidth: 300,
             url: '/OwnerContribution-Ajax',
             idField: 'name',
             textField: 'name',
             mode: 'remote',
             fitColumns: true,
             queryParams: {
                 action: 'getOwnerDataForComboGrid',
                 class: 'ownerContribution'
             },
             columns: [[
                 {field: 'id', hidden: true},
                 {field: 'name', title: 'Name', width: 60},
                 // {field: 'address1', title: 'Address', width: 120},
                 {field: 'email', title: 'Email', width: 80},
                 {field: 'stripe_customer_id', hidden: true},
             ]],
             onSelect: function (index, row) {
                 $("#selected_owner_id").val(row.id);
                 $('#owner_customer_id').val(row.stripe_customer_id);
                 // var selected_owner_id = $("#selected_owner_id").val();
                 var portfolioData = getDataByID(row.id, '', 'owner');
                 // if (portfolioData.propertyLength == 1){
                     $('#property_id').html(portfolioData.propertyOption);
                 // }
                 // if (portfolioData.bankLength == 1){
                 //     $('#bank_account').html(portfolioData.bankOption);
                 // }
                 // if (portfolioData.bankLength == 0){
                     $('#bank_account').html(portfolioData.bankOption);
                 // }
                 $('#portfolio_id').html(portfolioData.portfolioOption);
                 toastr.error('Please enter payment details.');
                 console.log('portfolioData',portfolioData);
             }
         });
     }

    $("#_easyui_textbox_input1").attr("placeholder", "Click here to pick a owner");
    $("#_easyui_textbox_input1").addClass("form-control");
    $(".panel-htop .combo-panel").css("margin-top",'0px');
    $(".panel-htop .combo-panel").css("margin-left",'0px');

    $(document).on('click','#_easyui_textbox_input1',function(){
        $('#owner_id').combogrid('showPanel');
    });

    $(document).on('change','#portfolio_id',function(e){
        var selected_owner_id = $("#selected_owner_id").val();
        var propertyData = getDataByID($(this).val(), selected_owner_id,'portfolio');
        // $('#property_id').html(propertyData);
        $('#property_id').html(propertyData.propertyOption);
        $('#bank_account').html(propertyData.bankOption);
    });

    $(document).on('change','#property_id',function(e){
        var propertyData = getDataByID($(this).val(), '','property');
        $('#bank_account').html(propertyData);
    });

    function getDataByID(data_id, prev_id, data_type){
        if (data_id == '' || data_id == 'null'){
            return;
        }

        var returnElement = '';
        $.ajax({
            type: 'post',
            url: '/OwnerContribution-Ajax',
            async: false,
            data: {
                class: "ownerContribution",
                action: "getDataByID",
                id: data_id,
                prev_id: prev_id,
                data_type : data_type
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if(data_type == 'owner') {
                        var portfolioOption = "<option value=''>Select Portfolio</option>";
                        var  propertyLength, bankLength  = "";
                        var propertyOption = "<option value=''>Select Property</option>";
                        var bankOption = "<option value=''>Select Bank</option>";
                        // var propertyLength = "";
                        if (data.data.portfolio_data.length > 0) {
                            $.each(data.data.portfolio_data, function (key, value) {
                                if (value.portfolio_name != undefined && value.id != undefined) {
                                    if (data.data.portfolio_data.length == 1) {
                                        portfolioOption += "<option selected value='" + value.id + "'>" + value.portfolio_name + "</option>";
                                        // var propertyData = getDataByID($(this).val(), value.id,'portfolio');
                                        // $('#property_id').html(propertyData);
console.log('m>>', data.data);
                                        if (data.data.property_data.length == 1){
                                            propertyLength = 1;
                                            propertyOption += "<option selected value='" +data.data.property_data[0].id + "'>" + data.data.property_data[0].property_name + "</option>";
                                        }
                                        if (data.data.property_data.length == 0){
                                            propertyLength = 0;
                                            propertyOption = "<option value=''>This Portfolio has no Property</option>";
                                        }
                                        if (data.data.bank_account_data.length == 1){
                                            bankLength = 1;
                                            bankOption += "<option selected value='" +data.data.bank_account_data[0].id + "'>" + data.data.bank_account_data[0].bank_name + "</option>";
                                        }
                                        if (data.data.bank_account_data.length == 0){
                                            bankLength = 0;
                                            bankOption = "<option value=''>This Portfolio has no Bank Account</option>";
                                        }
                                    } else {
                                        portfolioOption += "<option value='" + value.id + "'>" + value.portfolio_name + "</option>";
                                    }
                                }
                            });
                        }
                        // if (data.data.portfolio_data.length == 1){
                            returnElement = {
                                'portfolioOption': portfolioOption,
                                'propertyLength': propertyLength,
                                'propertyOption': propertyOption,
                                'bankLength': bankLength,
                                'bankOption': bankOption
                            };
                        // } else {
                        //     returnElement = portfolioOption;
                        // }
                    } else if(data_type == 'portfolio') {
                        var  propertyLength, bankLength  = "";
                        var propertyOption = "<option value=''>Select Property</option>";
                        var bankOption = "<option value=''>Select Bank</option>";
                        if (data.data.property_data.length > 0) {
                            propertyOption = "<option value=''>Select Property</option>";
                            $.each(data.data.property_data, function (key, value) {
                                if (value.property_name != undefined && value.id != undefined) {
                                    if (data.data.property_data.length == 1) {
                                        propertyOption += "<option selected value='" + value.id + "'>" + value.property_name + "</option>";
                                        // var propertyData = getDataByID($(this).val(), value.id,'portfolio');
                                        // $('#property_id').html(propertyData);
                                        // if (data.data.property_data.length == 1){
                                        //     propertyLength = 1;
                                        //     propertyOption += "<option selected value='" +data.data.property_data[0].id + "'>" + data.data.property_data[0].property_name + "</option>";
                                        // }
                                        // if (data.data.property_data.length == 0){
                                        //     propertyLength = 0;
                                        //     propertyOption = "<option value=''>This Portfolio has no Property</option>";
                                        // }
                                        if (data.data.bank_account_data.length == 1){
                                            bankLength = 1;
                                            bankOption += "<option selected value='" +data.data.bank_account_data[0].id + "'>" + data.data.bank_account_data[0].bank_name + "</option>";
                                        }
                                        if (data.data.bank_account_data.length == 0){
                                            bankLength = 0;
                                            bankOption = "<option value=''>This Portfolio has no Bank Account</option>";
                                        }
                                    } else {
                                        propertyOption += "<option value='" + value.id + "'>" + value.property_name + "</option>";
                                    }
                                }
                            });
                        } else {
                            propertyOption = "<option value=''>This Portfolio has no Property</option>";
                        }
                        returnElement = {
                            // 'portfolioOption': portfolioOption,
                            // 'propertyLength': propertyLength,
                            'propertyOption': propertyOption,
                            'bankLength': bankLength,
                            'bankOption': bankOption
                        };
                    } else if(data_type == 'property'){
                        var bankAccountOption = "";
                        if (data.data.bank_account_data.length > 0) {
                            bankAccountOption = "<option value=''>Select Bank</option>";
                            $.each(data.data.bank_account_data, function (key, value) {
                                if (value.bank_name != undefined && value.id != undefined) {
                                    if (data.data.bank_account_data.length == 1) {
                                        bankAccountOption += "<option selected value='" + value.id + "'>" + value.bank_name + "-" + value.bank_account_number + "</option>";
                                    } else {
                                        bankAccountOption += "<option value='" + value.id + "'>" + value.bank_name + "-" + value.bank_account_number + "</option>";
                                    }
                                }
                            });
                        } else {
                            bankAccountOption = "<option value=''>This Portfolio has no Bank Account</option>";
                        }
                        returnElement = bankAccountOption;
                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        return returnElement;
    }

    $(document).on('blur','#total_amount',function(){
        var total_amount = $(this).val();
        total_amount = (total_amount != '') ? parseFloat(total_amount) : 0;

        var card_charge_amount = ((2.9/100)*total_amount)+0.30;
        card_charge_amount = card_charge_amount.toFixed(3);
        card_charge_amount = parseFloat(card_charge_amount);

        var stripe_card_charge = numberWithCommas(card_charge_amount);

        var stripe_card_total = total_amount + card_charge_amount;
        stripe_card_total = stripe_card_total.toFixed(2);
        stripe_card_total = numberWithCommas(stripe_card_total);

        $('.stripe_card_charge').text(stripe_card_charge);
        $('.stripe_card_total').text( stripe_card_total);

        var ach_charge_amount;
        if (total_amount > 625){
            ach_charge_amount = 5;
        } else {
            ach_charge_amount = ((0.80 / 100) * total_amount);
        }

        var stripe_ach_charge = ach_charge_amount.toFixed(2);
        stripe_ach_charge = numberWithCommas(stripe_ach_charge);

        var stripe_ach_total = total_amount + ach_charge_amount;
        stripe_ach_total = stripe_ach_total.toFixed(2);
        stripe_ach_total = numberWithCommas(stripe_ach_total);

        $('.stripe_ach_charge').text(stripe_ach_charge);
        $('.stripe_ach_total').text( stripe_ach_total);

    });

    $(document).on('keypress','.money',function(e){
        if(this.value.length==10) return false;
    });

    $(document).on('focusout','.money',function(){
        if($(this).val() != '' && $(this).val().indexOf(".") == -1) {
            var bef = $(this).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $(this).val(value);
        } else {
            var bef = $(this).val().replace(/,/g, '');
            $(this).val(numberWithCommas(bef));
        }
    });

    getInitialData();
    function getInitialData() {
        $.ajax({
            type: 'post',
            url: '/OwnerContribution-Ajax',
            data: {
                class: "ownerContribution",
                action: "getInitialData"
            },
            success: function (response) {

                var res = $.parseJSON(response);
                var chart_accounts = res.data.chart_accounts;


                if (res.status == "success") {
                    $('#chart_of_account_id').empty().append("<option value=''>Select</option>");
                    $.each(chart_accounts, function (key, value) {
                        if (value.id == 44) {
                            $('#chart_of_account_id').append($("<option selected value = " + value.id + ">" + value.account_code + " - " + value.account_name + "</option>"));
                        } else {
                            $('#chart_of_account_id').append($("<option value = " + value.id + ">" + value.account_code + " - " + value.account_name + "</option>"));
                        }
                    });
                } else if (res.status == "error") {
                    toastr.error(res.message);
                } else {
                    toastr.error(res.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("change","#transaction_type",function(){
        var transaction_type = $(this).val();
        var selected_owner_id = $("#selected_owner_id").val();

        if (transaction_type == 'Check' || transaction_type == ''){
            $('.cash_div').hide();
            $('.check_div').show();
            $('.stripe_charge_div').hide();
            $('.card_div').hide();
            $('.ach_div').hide();
        }
        if (transaction_type == 'Cash'){
            $('.check_div').hide();
            $('.cash_div').show();
            $('.stripe_charge_div').hide();
            $('.card_div').hide();
            $('.ach_div').hide();
        }

        if (transaction_type == 'ACH' || transaction_type == 'Credit Card/Debit Card') {
            if (selected_owner_id != '') {
                $.ajax({
                    type: 'post',
                    url: '/OwnerContribution-Ajax',
                    data: {
                        class: "ownerContribution",
                        action: "checkForPayment",
                        "payment_type": transaction_type,
                        "user_id": selected_owner_id
                    },
                    success: function (response) {
                        var res = JSON.parse(response);
                        if (res.status == 'false') {
                            toastr.error(res.message);
                            if (transaction_type == 'Credit Card/Debit Card') {
                                cardTypeDiv(transaction_type);
                            }
                            if (transaction_type == 'ACH') {
                                achTypeDiv(transaction_type);
                            }
                        } else {
                            if (res.payment_type == 'card') {
                                $('.card_type').text(res.data.brand);
                                $('.card_number').text('**** **** **** ' + res.data.last4);
                                $('.exp_year').text(res.data.exp_year);
                                $('.exp_month').text(res.data.exp_month);
                                $('.cvc_number').text('***');
                                $('#owner_customer_id').val(res.customer_id);
                                if (transaction_type == 'Credit Card/Debit Card') {
                                    cardTypeDiv(transaction_type);
                                }
                            }
                            if (res.payment_type == 'ACH') {
                                $('.account_holder_name').text(res.data.account_holder_name);
                                $('.account_holder_type').text(res.data.account_holder_type);
                                $('.bank_name').text(res.data.bank_name);
                                $('.routing_number').text(res.data.routing_number);
                                $('.ach_account_number').text('**** **** **** ' + res.data.last4);
                                $('#owner_customer_id').val(res.customer_id);
                                if (transaction_type == 'ACH') {
                                    achTypeDiv(transaction_type);
                                }
                            }
                        }
                    }
                });
            } else {
                if (transaction_type == 'ACH') {
                    achTypeDiv(transaction_type);
                }
                if (transaction_type == 'Credit Card/Debit Card') {
                    cardTypeDiv(transaction_type);
                }
            }
        }
    });

    function achTypeDiv(transaction_type){
        if (transaction_type == 'ACH') {
            $('.check_div').hide();
            $('.cash_div').show();
            $('.stripe_charge_div').show();
            $('.stripe_charge_card_div').hide();
            $('.stripe_charge_ach_div').show();
            $('.card_div').hide();
            $('.ach_div').show();
        }
    }

    function cardTypeDiv(transaction_type){
        if (transaction_type == 'Credit Card/Debit Card') {
            $('.check_div').hide();
            $('.cash_div').show();
            $('.stripe_charge_div').show();
            $('.stripe_charge_card_div').show();
            $('.stripe_charge_ach_div').hide();
            $('.card_div').show();
            $('.ach_div').hide();
        }
    }
    $("#addOwnerContributionFormId").validate({
        rules: {
            contribution_date: {
                required: true
            },
            owner_id: {
                required: true
            },
            portfolio_id: {
                required: true
            },
            property_id: {
                required: true
            },
            chart_of_account_id: {
                required: true
            },
            total_amount: {
                required: true
            },
            transaction_type: {
                required: true
            },
            check_number: {
                required: true
            }
        }
    });

    $(document).on("click","#ownerContributionSaveBtn",function(){
        var owner_id = $("input[name='owner_id']").val();
        // console.log('aaaaaa', owner_id); return;
        if (owner_id == ''){
            setTimeout(function () {
                $('#owner_id-error').text('* This field is required.');
                $('#owner_id-error').css({"display":"block"});
            },100);
        } else {
            $('#owner_id-error').text('');
        }

        if ($("#addOwnerContributionFormId").valid()){
            var form = $('#addOwnerContributionFormId')[0];
            var formData = new FormData(form);
            var transaction_type = $('#transaction_type').val();
            var payment_type = '';

            if (transaction_type == 'Check' || transaction_type == 'Cash'){
                bootbox.confirm("Would you like to save this record? It will update the bank register.", function (result) {
                    if (result == true) {
                        saveOwnerContributionAjax(formData, payment_type);
                    }
                });
            }  else {
                if (transaction_type == 'Credit Card/Debit Card'){
                    payment_type = 'card';
                }
                if (transaction_type == 'ACH'){
                    payment_type = 'ACH';
                }
                saveOwnerContributionAjax(formData, payment_type);
            }
            if (transaction_type == 'ACH'){
                payment_type = 'ACH';
            }
        } else {
            console.log('bbbbbb');
        }
    });

    function saveOwnerContributionAjax(formData, payment_type) {
        formData.append('class', 'ownerContribution');
        formData.append('action', 'createPaymentOnSubmit');
        formData.append('payment_type', payment_type);
        console.log('formData>>', formData);

        $.ajax({
            url: '/OwnerContribution-Ajax',
            type: 'post',
            data:  formData,
            contentType: false,
            processData: false,
            success: function (response) {
                var res = JSON.parse(response);
                console.log('res>>>>',res);
                if (res.status == 'false') {
                    toastr.error(res.message);
                } else {
                    if (res.payment_type == 'card') {
                        $('.card_type').text(res.data.brand);
                        $('.card_number').text('**** **** **** ' + res.data.last4);
                        $('.exp_year').text(res.data.exp_year);
                        $('.exp_month').text(res.data.exp_month);
                        $('.cvc_number').text('***');
                    }
                    if (res.payment_type == 'ACH') {
                        $('.account_holder_name').text(res.data.account_holder_name);
                        $('.account_holder_type').text(res.data.account_holder_type);
                        $('.bank_name').text(res.data.bank_name);
                        $('.routing_number').text(res.data.routing_number);
                        $('.ach_account_number').text('**** **** **** ' + res.data.last4);
                    }
                    localStorage.setItem("Message", 'Record created successfully.');
                    localStorage.setItem("rowcolor", true);
                    jqgridNewOrUpdated = 'true';
                    if(last_url.includes("/Owner/OwnerPayments/OwnerContribution")==true){
                        window.location.href = '/Owner/OwnerPayments/OwnerContribution';
                    }else{
                        window.location.href = '/Accounting/OwnerContribution';
                    }
                }
            }
        });
    }


    $(document).on('click','.clearOwnerContribute',function () {
        bootbox.confirm("Do you want to clear this form?", function(result) {
            if (result == true) {
                location.reload();
            }
        });
    });
});