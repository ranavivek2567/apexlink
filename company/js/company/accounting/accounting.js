$(document).ready(function(){


$(document).on("change",'.property',function(){
    var id = $(this).val();
    receivableFilter(id);   
   /* $("#tenantReceivables").trigger('reloadGrid');
*/
});
 

tenantReceivables('All');   

getPropertyListing();
})



function receivableFilter(id)
{
    var grid = $("#tenantReceivables"),f = [];
   var property_id = $('.property').val();
    f.push({field: "general_property.id", op: "in", data: property_id});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
}



function tenantReceivables()
{
  //   $("#tenantReceivables").trigger("reset");

    
    var table = 'transactions';
    var columns = ['Tenant ID','first_name','last_name','Tenant Name','Property','Building','Unit','unit_num','Last Payment Date('+default_currency_symbol+')','last_payment','Last Payment('+default_currency_symbol+')','Total Outstanding Amount('+default_currency_symbol+')','0-30('+default_currency_symbol+')','31-60('+default_currency_symbol+')','60-90('+default_currency_symbol+')','90+('+default_currency_symbol+')'];
    var select_column = ['Edit','Delete'];
    var joins = [
        {table:'transactions',column:'user_id ',primary:'id',on_table:'users'},
        {table:'transactions',column:'user_id',primary:'user_id',on_table:'tenant_property'},
        {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
        {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'},
        {table:'tenant_property',column:'building_id',primary:'id',on_table:'building_detail'},
        {table:'transactions',column:'user_id',primary:'user_id',on_table:'accounting_manage_charges'},
       
    ];
    var conditions = ["eq","bw","ew","cn","in"];
    //var extra_where = [{column:'user_id',value:tenantId,condition:'=',table:'hoa_violation'}];
    var extra_where = [{column: 'user_type', value: 'TENANT', condition: '='}];
    var extra_columns = [];
    var columns_options = [
        {name:'Tenant ID',index:'user_id',align:"center", width:170,searchoptions: {sopt: conditions},table:'transactions'},
        {name:'first_name',index:'first_name',align:"center",hidden:true, width:170,searchoptions: {sopt: conditions},table:'users'},
        {name:'last_name',index:'last_name',align:"center",hidden:true, width:170,searchoptions: {sopt: conditions},table:'users'},
        {name:'Tenant Name',index:'name',align:"center", width:170,searchoptions: {sopt: conditions},table:'users'},
        {name:'Property',index:'property_name', width:170,align:"center",searchoptions: {sopt: conditions},table:'general_property'},
        {name:'Building',index:'building_name', width:170,align:"center",searchoptions: {sopt: conditions},table:'building_detail'},
        {name:'Unit',index:'unit_prefix', width:170,align:"center",searchoptions: {sopt: conditions},table:'unit_details',formatter:getUnitNameFormatter},
        {name:'unit_num',index:'unit_no', width:170,align:"center",hidden:true,searchoptions: {sopt: conditions},table:'unit_details'},
        {name:'Last Payment Date('+default_currency_symbol+')',index:'created_at', width:170,align:"center",searchoptions: {sopt: conditions},table:'transactions'},
        {name:'last_payment',index:'total_charge_amount',hidden:true, width:170,align:"center",searchoptions: {sopt: conditions},table:'transactions'},
        {name:'Last Payment('+default_currency_symbol+')',index:'total_charge_amount', width:170,align:"center",searchoptions: {sopt: conditions},table:'transactions',formatter:lastPayment},
        {name:'Total Outstanding Amount('+default_currency_symbol+')',index:'total_due_amount', width:170,hidden:true,align:"center",searchoptions: {sopt: conditions},table:'accounting_manage_charges'},
        {name:'0-30('+default_currency_symbol+')',index:'total_due_amount', width:170,align:"center",searchoptions: {sopt: conditions},table:'accounting_manage_charges',formatter:getOutstanding},
        {name:'31-60('+default_currency_symbol+')',index:'total_due_amount', width:170,align:"center",searchoptions: {sopt: conditions},table:'accounting_manage_charges',formatter:getOutstanding},
        {name:'60-90('+default_currency_symbol+')',index:'total_due_amount', width:170,align:"center",searchoptions: {sopt: conditions},table:'accounting_manage_charges',formatter:getOutstanding},
        {name:'90+('+default_currency_symbol+')',index:'total_due_amount', width:170,align:"center",searchoptions: {sopt: conditions},table:'accounting_manage_charges',formatter:getOutstanding}

        
    ];
    var ignore_array = [];
    jQuery("#tenantReceivables").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,

            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'no'
        },
        viewrecords: true,
        sortname: 'users.name',
        sortorder: "ASC",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Tenants Receivable",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}

    function getUnitNameFormatter(cellvalue, options, rowObject)
    {
        if(rowObject!==undefined)
        {

        return rowObject.Unit+'-'+rowObject.unit_num;


        }

    }


        function getFullNameFormatter(cellvalue, options, rowObject)
    {

        if(rowObject!==undefined)
        {

        return rowObject.first_name+' '+rowObject.last_name;


        }

    }



    



function getOutstanding()
{
    return default_currency_symbol+'100.00';
}


    function lastPayment(cellvalue, options, rowObject)
    {
        if(rowObject!==undefined)
        {
            console.log(cellvalue);
            if(cellvalue==null || cellvalue=='0')
            {

            return default_currency_symbol+0.00;
            }
            else
            { 
                var amt = cellvalue.toString();
              return default_currency_symbol+changeToFloat(amt);

            }
          
        


        }

    }





function getPropertyListing()
{
   
         $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getPropertyListing',
                    "class": 'accounting'

                },
                success: function (response) {

                    $(".property").html(response);
                   


                }
            });  
}


