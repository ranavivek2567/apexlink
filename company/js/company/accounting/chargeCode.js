$(document).ready(function () {

    $("#unit_type").keypress(function(e){
        var keyCode = e.which;
        /*
          8 - (backspace)
          32 - (space)
          48-57 - (0-9)Numbers
        */
        /* Not allow special*/
        if ( !( (keyCode >= 48 && keyCode <= 57)
            ||(keyCode >= 65 && keyCode <= 90)
            || (keyCode >= 97 && keyCode <= 122) )
            && keyCode != 8 && keyCode != 32) {
            e.preventDefault();
        }
    });
    var base_url = window.location.origin;

    var status =  localStorage.getItem("active_inactive_status");
    if(status !== undefined) {
        if ($("#ChargeCode-table")[0].grid) {
            $('#ChargeCode-table').jqGrid('GridUnload');
        }
        /*initializing jqGrid*/
        if(status == 'all'){
            jqGrid('All');
        }  else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#ChargeCode-table")[0].grid) {
            $('#ChargeCode-table').jqGrid('GridUnload');
        }
        jqGrid('All');
    }

    /** Show add new AccountChargeCode div on add new button click */
    $(document).on('click','#addChargeCodeButton',function () {
        $('#charge_code').val('').prop('disabled', false);
        $('#description').val('');
        $("#charge_code_id").val('');
        $('#charge_code-error').text('');
        $('#charge_codeErr').text('');
        $('#credit_account-error').text('');
        $('#credit_accountErr').text('');
        $('#debit_account-error').text('');
        $('#debit_accountErr').text('');
        $('#status').val('');
        $('#status-error').text('');
        headerDiv.innerText = "Add Charge Code";
        $('#is_default').prop('checked', false);
        creditAccountList();
        debitAccountList();
        $('#saveBtnId').val('Save');
        $('#add_charge_code_div').show(500);
    });

    function creditAccountList(){
        $.ajax({
            type: 'post',
            url: '/ChargeCode-Ajax',
            data: {
                class: 'ChargeCodeAjax',
                action: 'getAllCreditList',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#credit_account').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#credit_account').append($("<option value = "+value.id+">"+value.credit_accounts+"</option>"));
                    })
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    function debitAccountList(){
        $.ajax({
            type: 'post',
            url: '/ChargeCode-Ajax',
            data: {
                class: 'ChargeCodeAjax',
                action: 'getAllDebitList',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#debit_account').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#debit_account').append($("<option value = "+value.id+">"+value.debit_accounts+"</option>"));
                    })
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /** Show import excel div on import excel button click */
    $(document).on('click','#importChargeCodeButton',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_unit_type_div').show(500);
    });

    /** Hide add new AccountChargeCode div on cancel button click */
    $(document).on("click", "#add_charge_code_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#add_charge_code_div").hide(500);
                }
            }
        });
    });

    /** Hide import excel div on cancel button click */
    $(document).on("click", "#import_unit_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_unit_type_div").hide(500);
                }
            }
        });
    });

    /** jqGrid status change*/
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#ChargeCode-table').jqGrid('GridUnload');
        $('#add_unit_type_div').hide(500);
        changeGridStatus(selected);
        jqGrid(selected, true);
    });

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        var table = 'company_accounting_charge_code';
        var columns = ['Charge Code','Credit Account', 'Debit Account', 'Description', 'Status', 'Action'];
        var select_column = ['Edit','Deactivate','Delete'];
        var joins = [{table:'company_accounting_charge_code',column:'credit_account',primary:'id',on_table:'company_credit_accounts'},{table:'company_accounting_charge_code',column:'debit_account',primary:'id',on_table:'company_debit_accounts'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['company_accounting_charge_code.status', 'company_accounting_charge_code.deleted_at','company_accounting_charge_code.is_editable', 'company_accounting_charge_code.updated_at'];
        var columns_options = [
            {name:'Charge Code',index:'charge_code', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Credit Account',index:'credit_accounts', width:100,searchoptions: {sopt: conditions},table:'company_credit_accounts'},
            {name:'Debit Account',index:'debit_accounts', width:100,searchoptions: {sopt: conditions},table:'company_debit_accounts'},
            {name:'Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFormatter}
        ];
        var ignore_array = [];
        jQuery("#ChargeCode-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_accounting_charge_code",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Charge Code",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {},
            {},
            {},
            {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == '1')
            return "Active";
        else if(cellValue == '0')
            return "InActive";
        else if (cellValue == '3')
            return "Archive";
        else
            return '';
    }

    /**
     * jqGrid function to format action column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            var is_default = rowObject;
            var editable = $(cellValue).attr('editable');
            var select = '';
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select ' +
                    ' class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }


    /** Add/Edit new AccountChargeCode */
    $("#add_charge_code_form").validate({
        rules: {
            charge_code: {
                required: true,
            },
            credit_account: {
                required:true
            },
            debit_account: {
                required:true
            },
            status: {
                required:true
            }
        },
        messages: {
            charge_code : {
                required: "* This field is required",
            }
        },
        submitHandler: function () {
            var charge_code = $('#charge_code').val();
            var description = $('#description').val();
            var credit_account = $('#credit_account').val();
            var debit_account = $('#debit_account').val();
            var status = $("#status").val();
            var charge_code_id = $("#charge_code_id").val();
            var is_default;
            if ($('#is_default').is(":checked")) {
                is_default = '1';
            } else {
                is_default = '0';
            }
            var formData = {
                'charge_code': charge_code,
                'description': description,
                'credit_account': credit_account,
                'debit_account': debit_account,
                'is_default': is_default,
                'status':status,
                'charge_code_id': charge_code_id
            };

            var action;
            if (charge_code_id) {
                action = 'update';
            } else {
                action = 'insert';
            }
            $.ajax({
                type: 'post',
                url: '/ChargeCode-Ajax',
                data: {
                    class: 'ChargeCodeAjax',
                    action: action,
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#add_charge_code_div").hide(500);
                        $("#ChargeCode-table").trigger('reloadGrid');
                        toastr.success(response.message);
                        onTop(true);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        if (opt == 'Edit' || opt == 'EDIT') {

            $("#add_charge_code_div").show(500);
            creditAccountList();
            debitAccountList();
            headerDiv.innerText = "Edit Charge Code";
            $('#saveBtnId').val('Update');
            $('#charge_code-error').text('');
            $('#charge_codeErr').text('');
            $('#credit_account-error').text('');
            $('#credit_accountErr').text('');
            $('#debit_account-error').text('');
            $('#debit_accountErr').text('');
            $('#status-error').text('');
            $('.table').find('.green_row_left, .green_row_right').each(function(){
                $(this).removeClass("green_row_left green_row_right");
            });
            $('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
            $('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
            $(".clear-btn").text('Reset');
            $(".clear-btn").addClass('formreset');
            $(".clear-btn").removeClass('clearFormReset');
            $.ajax
            ({
                type: 'post',
                url: '/ChargeCode-Ajax',
                data: {
                    class: "ChargeCodeAjax",
                    action: "view",
                    id : id,
                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success")
                    {
                        $("#charge_code").val(data.data.charge_code);
                        $("#description").val(data.data.description);
                        $("#credit_account").val(data.data.credit_account);
                        $("#debit_account").val(data.data.debit_account);
                        $("#status").val(data.data.status);
                        $("#charge_code_id").val(data.data.id);

                        if(data.data.is_editable == 0) {
                            $('#charge_code').prop('disabled', true);
                        } else {
                            $('#charge_code').prop('disabled', false);
                        }
                        if (data.data.is_default == 1) {
                            $('#is_default').prop('checked', true);
                        } else {
                            $('#is_default').prop('checked', false);
                        }
                        defaultFormData = $('#add_charge_code_form').serializeArray();
                    }else if(data.status == "error"){
                        toastr.error(data.message);
                    } else{
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });


        } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to " + opt + " this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        var status = opt == 'activate' ? '1' : '0';
                        $.ajax({
                            type: 'post',
                            url: '/ChargeCode-Ajax',
                            data: {
                                class: 'ChargeCodeAjax',
                                action: 'updateStatus',
                                status: status,
                                id: id
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                    $('#ChargeCode-table').trigger('reloadGrid');
                                    onTop(true);
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                }
            });
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            if (status == '1') {
                toastr.warning('A default set value cannot be deleted.');
            } else {
                bootbox.confirm({
                    message: "Do you want to delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/ChargeCode-Ajax',
                                data: {
                                    class: 'ChargeCodeAjax',
                                    action: 'delete',
                                    id: id
                                },
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                        $('#ChargeCode-table').trigger('reloadGrid');
                                    } else {
                                        toastr.error(response.message);
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });

    /** Export sample AccountChargeCode excel */
    $(document).on("click",'#exportSampleExcel',function(){
        window.location.href = base_url+"/ChargeCode-Ajax?action=exportSampleExcel";
    });

    /** Export AccountChargeCode type excel  */
    $(document).on("click",'#exportChargeCodeButton',function(){
        var status = $("#jqGridStatus option:selected").val();
        var table =  'company_accounting_charge_code';
        window.location.href = base_url+"/ChargeCode-Ajax?status="+status+"&&table="+table+"&&action=exportExcel";
    });

    /** Import AccountChargeCode excel  */
    $("#importChargeCodeFormId").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importChargeCodeForm').serializeArray();

            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            //console.log(myFiles);
            var formData = new FormData();
            //console.log(formData);
            formData.append('file', myFiles);
            formData.append('class', 'ChargeCodeAjax');
            formData.append('action', 'importExcel');
            $.ajax
            ({
                type: 'post',
                url: '/ChargeCode-Ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    //console.log(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);

                        $("#import_unit_type_div").hide(500)
                        $('#ChargeCode-table').trigger('reloadGrid');
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });
});