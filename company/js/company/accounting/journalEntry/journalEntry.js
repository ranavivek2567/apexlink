$(document).ready(function () {
    fetchAllPortfolio(false);
   // getPropertyListing();
    //getAllAccountDDl();
    getChartAccounts();
    /* on change property get building */
    $(document).on("change","#property",function (e) {
        e.preventDefault();
        getBuildingsByPropertyID($(this).val());
        return false;
    });


    $(document).on("change","#selected_portfolio",function (e) {
        e.preventDefault();
        // if($(this).val() != '') {
            getPropertyListing($(this).val());
            getAllAccountDDl($(this).val());
        // }
        // return false;
    });


    var clone = $(".additional-account-chart-details:first").clone();
    clone.find('.description,.money ').val('');
    $(".additional-account-chart-details").last().after(clone);
    clone.find(".additional-add-account-details").hide();
    clone.find(".remove-account-details").show();
    new AutoNumeric.multiple('.money', {
        allowDecimalPadding: true,
        maximumValue  : '9999999999',
    });


    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });

    $(document).on('click',"input[name='journal_entry_type']",function(e){
        var radioValue = $("input[name='journal_entry_type']:checked").val();
        if(radioValue == "one_time"){
            $("#journal_entry_type_select_div").hide();

        }else if(radioValue == "recurring"){
            $("#journal_entry_type_select_div").show();
        }
        $('#journal_entry_type_select').prop('selectedIndex',0);
    });

    /** Add/Edit new Bank Account */
    $("#add_bank_account_form").validate({
        rules: {
            portfolio: {
                required: true
            },
            bank_name: {
                required: true
            },
            routing_number: {
                required: true,
                number:true
            },
            bank_account_number: {
                required: true
            },
            fdi_number: {
                required: true
            },
            branch_code: {
                required: true
            },
            initial_amount: {
                required: true,
                number: true
            },
            last_used_check_number: {
                required: true,
                number: true
            },
            status: {
                required: true
            }
        },
        messages: {
            portfolio: {
                required: "* This field is required",
            },
            bank_account_number: {
                minlength: "* Please enter 7 digit account number",
            }
        },
        submitHandler: function () {
            var portfolio = $('#portfolio').val();
            var bank_name = $('#bank_name').val();
            var bank_account_number = $('#bank_account_number').val();
            var fdi_number = $('#fdi_number').val();
            var routing_number = $('#routing_number_rec').val();
            var branch_code = $('#branch_code').val();
            var initial_amount = $('#initial_amount').val();
            var last_used_check_number = $('#last_used_check_number').val();
            var status = $('#status').val();
            var bank_account_id = $("#bank_account_id").val();
            var is_default;
            if ($('#is_default').is(":checked")) {
                is_default = '1';   // it is checked
            } else {
                is_default = '0';
            }
            var formData = {
                'portfolio': portfolio,
                'bank_name': bank_name,
                'bank_account_number': bank_account_number,
                'fdi_number': fdi_number,
                'routing_number': routing_number,
                'branch_code': branch_code,
                'initial_amount': initial_amount,
                'last_used_check_number': last_used_check_number,
                'status': status,
                'is_default': is_default,
                'bank_account_id': bank_account_id
            };

            $.ajax({
                type: 'post',
                url: '/BankAccount-Ajax',
                data: {
                    class: 'BankAccountAjax',
                    action: 'insert',
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $('#start_reconcile_modal').modal({
                            show: true,
                            backdrop: 'static',
                            keyboard: false,
                        });
                        var lastInsertData = '<option value="'+response.lastInsertData.id+'" portfolio_id="'+response.lastInsertData.portfolio+'" selected>'+response.lastInsertData.bank_name+'</option>'
                        console.log('lastInsertData>>', lastInsertData);
                        $('.bank_account').append(lastInsertData);
                        $('#add_more_bank_account_modal').modal('hide');
                        toastr.success('The record saved successfully.');

                    } else if (response.status == 'error' && response.code == 400) {
                        toastr.error(response.message);
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'warning' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    new AutoNumeric('#initial_amount', {
        allowDecimalPadding: true,
        maximumValue  : '9999999999',
    });

    /* Getting Building Property And Unit*/

    $(document).on("change","#building",function (e) {
        e.preventDefault();
        getUnitsByBuildingsID($("#property").val() , $(this).val());
        return false;
    });

    $('#accounting_period').Monthpicker({
        OnAfterChooseMonth: function() {
            var custom_validation_error = validations(this);
        }
    });
//jqGrid status
    $(document).on('change','.common_ddl',function(){
        searchFilters();
    });

    $("#accounting_period").datepicker({dateFormat: jsDateFomat,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
        showButtonPanel: true,
        maxDate: new Date(),
    });

    $(document).on('click', '#add_more_bank_account_icon', function () {
        $('#add_bank_account_form')[0].reset();
        $('#add_bank_account_form label.error').text('');
        getAllPortfolioDDl();
        $('#add_more_bank_account_modal').modal({
            show: true,
            backdrop: 'static',
            keyboard: false,
        })
    });


    $(document).on("click",".additional-add-account-details",function(){
        var clone = $(".additional-account-chart-details:first").clone();
        clone.find('.description,.money ').val('');
        $(".additional-account-chart-details").last().after(clone);
        clone.find(".additional-add-account-details").hide();
        clone.find(".remove-account-details").show();
        new AutoNumeric.multiple('.money', {
            allowDecimalPadding: true,
            maximumValue  : '9999999999',
        });

    });

    $(document).on("click",".remove-account-details",function(){
        var tr_length =   $("tr.additional-account-chart-details").length;
        if(tr_length > 2) {
            $(this).parents(".additional-account-chart-details").remove();
        }else{
            toastr.error('Cannot delete mandatory rows.');
        }
        // calculateSum();
    });


    $("#addJournalEntry").validate({
        rules: {
            selected_portfolio: {
                required:true
            },
            property: {
                required:true
            },
            building: {
                required:true
            },
            accounting_period: {
                required:true
            },
            bank_account: {
                required:true
            },journal_entry_type_select: {
                required:true
            },
        }
    });


    $(document).on('keyup','.customValidationaccountingperiod',function(){
        var custom_validation_error = validations(this);
    });

    $(document).on('change','.customValidationchartaccount',function(){
        validations(this);

    });


    /*submit halder for add employee

 */

    $('.submit_button').click(function() {
        button_value = $(this).attr('value')
    });

    $(document).on("blur",".debit",function(){
        var total_debit_amount = [];
        var new_total_amount =0;
        $(".debit").each(function() {
            $(this).val().replace(/,/g, '');
            total_debit_amount.push($(this).val().replace(/,/g, ''));
        });
        var sum = total_debit_amount.reduce((pv,cv)=>{
            return pv + (parseFloat(cv)||0);
        },0);

        var sumvalue =  formatSum(sum);
        $("#TotalDebit").text(default_currency_symbol+''+sumvalue);
    });


    function formatSum(sum){
        if(sum != '' ) {
            var value = numberWithCommas(sum) + '.00';
            return value
        }else{
            return  numberWithCommas(sum);
        }
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }



    $(document).on("blur",".credit",function(){
        var total_credit_amount = [];
        $(".credit").each(function() {
            $(this).val().replace(/,/g, '');
            total_credit_amount.push($(this).val().replace(/,/g, ''));
        });
        var sum = total_credit_amount.reduce((pv,cv)=>{
            return pv + (parseFloat(cv)||0);
        },0);

        var sumvalue =  formatSum(sum);
        $("#TotalCredit").text(default_currency_symbol+''+sumvalue);
    });





    $("#addJournalEntry").on('submit',function(event){
        event.preventDefault();
        $(".customValidationaccountingperiod ").each(function () {
            res = validations(this);
        });

        $(".customValidationchartaccount ").each(function () {
            res = validations(this);
        });


        if($("#addJournalEntry").valid()){
            var form = $('#addJournalEntry')[0];
            var formData = new FormData(form);
            formData.append('action','insert');
            formData.append('class','JournalEntry');
            var length1 = $('#file_library_uploads > div').length;
            combine_photo_document_array =  file_library;
            var dataaa = Array.from(new Set(combine_photo_document_array)); // #=> ["foo", "bar"]
            if(length1 > 0 ) {
                var data1 = convertSerializeDatatoArray();
                var combine__data_photo_document_array =   data1;
                $.each(dataaa, function (key, value) {
                    if(compareArray(value,combine__data_photo_document_array) == 'true'){
                        formData.append(key, value);
                    }
                });
            }
            $.ajax({
                url: '/Accounting/journal-entry-ajax',
                type: 'POST',
                data: formData,cache: false,
                contentType: false,
                processData: false,
                beforeSend: function (xhr) {
                    var accounting_period = true;
                    var chart_account =true;

                    $(".customValidationaccountingperiod").each(function() {
                        accounting_period = validations(this);

                    });

                    $(".customValidationchartaccount").each(function() {
                        chart_account = validations(this);

                    });
                    if (accounting_period === false) {
                        $(".customAccPeriodError").html("* This field is required")
                        xhr.abort();
                        return false;
                    }else{
                        $(".customAccPeriodError").html("");
                    }

                    if (chart_account === false) {
                        xhr.abort();
                        return false;
                    }

                },
                success: function (response) {
                    var response = JSON.parse(response);

                    if(response.status == "success"){


                        localStorage.setItem("Message", response.message);
                        // localStorage.setItem("rowcolor",true)
                        localStorage.setItem("rowcolorTenant",'green');
                        if(button_value == "Save") {
                            window.location.href = window.location.origin + '/Accounting/JournalEntries';
                        }else{
                            window.location.href = window.location.origin + '/newJournalEntry';
                        }
                    }else{
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }

            });
        }
    });

    $(document).on("click", "#add_bank_account_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#add_more_bank_account_modal").modal('hide');
                }
            }
        });
    });

    $(document).on("click",".cancel",function() {
        bootbox.confirm({
            message: "Do you want to cancel this invoice ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href = base_url+'/Accounting/JournalEntries';
                }

            }
        });
    });

});

function getAllPortfolioDDl(){
    $.ajax({
        type: 'post',
        url: '/Accounting/journal-entry-ajax',
        data: {
            class: 'JournalEntry',
            action: 'getAllPortfolioDDl'
        },
        success : function(response){
            var response =  JSON.parse(response);
            if(response.status == 'success' && response.code == 200) {
                var portfolioOption = "<option value=''>Select</option>";
                if (response.data.length > 0) {
                    $.each(response.data, function (key, value) {
                        if(value.is_default == 1){
                            portfolioOption += "<option value='" + value.id + "' selected>" + value.portfolio_name + "</option>";
                        } else {
                            portfolioOption += "<option value='" + value.id + "' >" + value.portfolio_name + "</option>";
                        }
                    });
                }
                $('#portfolio').html(portfolioOption);
            }
        }
    });
}

function getAllAccountDDl(portfolio_id){
    $.ajax({
        type: 'post',
        url: '/Accounting/journal-entry-ajax',
        data: {
            class: 'JournalEntry',
            action: 'getAllAccountDDl',
            portfolio_id:portfolio_id
        },
        success : function(response){
            var response =  JSON.parse(response);
            if(response.status == 'success' && response.code == 200) {
                var accountOption = "<option value=''>Select</option>";
                if (response.data.length > 0) {
                    $.each(response.data, function (key, value) {
                        accountOption += "<option value='" + value.id + "' portfolio_id='" + value.portfolio + "'>" + value.bank_name + "</option>";
                    });
                }
                $('.bank_account').html(accountOption);

            }
        }
    });
}



function fetchAllPortfolio(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/Accounting/journal-entry-ajax',
        data: {
            propertyEditid: propertyEditid,
            class: 'JournalEntry',
            action: 'fetchPortfolioname'
        },
        success: function (response) {
            var res = JSON.parse(response);
            console.log(response);
            $('#selected_portfolio').html(res.data);
            var portfolio_id =  $("#selected_portfolio").val();
            if(portfolio_id !='') {
                getPropertyListing(portfolio_id);
                getAllAccountDDl(portfolio_id);
            }
            if (id != false) {
                $('#portfolio_name_options').val(id);



            }

        },
    });
}

function getPropertyListing(portfolio_id) {
    $.ajax({
        type: 'post',
        url: '/Accounting/journal-entry-ajax',
        data: {
            class: "JournalEntry",
            action: "getProperties",
            portfolio_id:portfolio_id
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.propertylist.length > 0){
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.propertylist, function (key, value) {
                        propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
                    $('#property').html(propertyOption);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


function getChartAccounts() {
    $.ajax({
        type: 'post',
        url: '/Accounting/journal-entry-ajax',
        data: {
            class: "JournalEntry",
            action: "getChartAccounts"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                if (data.data.accountlist.length > 0){
                    var accountListOption = "<option value=''>Select</option>";
                    $.each(data.data.accountlist, function (key, value) {
                        var selected= '';
                        if(value.is_default == '1'){
                            selected = 'selected'
                        }
                        accountListOption += "<option value='"+value.id+"' data-id='"+value.id+"' "+selected+" >"+value.account_code+' :'+value.account_name+"</option>";
                    });
                    $('.chart_account').html(accountListOption);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function getBuildingsByPropertyID(propertyID){
    $.ajax({
        type: 'post',
        url: '/Accounting/journal-entry-ajax',
        data: {
            class: "JournalEntry",
            action: "getContactBuildingDetail",
            propertyID: propertyID
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var buildingOption = "<option value=''>This property has no building</option>";
                if (data.data.length > 0){
                    buildingOption = "<option value=''>Select</option>";
                    $.each(data.data, function (key, value) {
                        buildingOption += "<option value='"+value.id+"' data-id='"+value.building_id+"'>"+value.building_name+"</option>";
                    });
                }
                $('#building').html(buildingOption);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

/* get unit ajax */
function getUnitsByBuildingsID(propertyID, buildingID){
    $.ajax({
        type: 'post',
        url: '/Accounting/journal-entry-ajax',
        data: {
            class: "JournalEntry",
            action: "getContactUnitDetail",
            propertyID: propertyID,
            buildingID: buildingID
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var unitOption = "<option value=''>This Building has no unit</option>";
                if (data.data.length > 0){
                     unitOption = "<option value=''>Select</option>";
                    $.each(data.data, function (key, value) {
                        var uniName = value.unit_prefix+"-"+value.unit_no;
                        unitOption += "<option value='"+value.id+"' data-id='"+value.unit_prefix+"'>"+uniName+"</option>";
                    });
                    $('#unit').html(unitOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}
