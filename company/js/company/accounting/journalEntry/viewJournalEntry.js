$(document).ready(function () {
    var id = getParameterByName('id');
    var base_url = window.location.origin;

    $.ajax({
        type: 'get',
        url: '/Accounting/journal-entry-ajax',
        data: {
            id: id,
            class: 'JournalEntry',
            action: 'View'},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                var data ='';
                $.each(response.data, function (key, value) {
                    // alert(value);

                    if(key=='key_access_codes_info'){
                    }else{
                        $('.' + key).html(value);
                        if(key=='invoice_to' && (value == null || value == '')){
                            $('.invoice_to').html(response.data.other_name);
                        }




                    }

                });
                    if(response.data.unit_prefix != '' && response.data.unit_prefix != null) {
                        data += '<span class="property_class">' + response.data.unit_prefix + '-' + response.data.floor_no + '</span>';
                    }
                    $('.unit_prefix').html(data);

                var account_data = response.account_detail;
                var files_data = response.files_data;
                var account_data_html ='';
                var files_data_html ='';
                var sum_debit = 0;
                var sum_credit = 0;
                $.each(account_data,function (key, value){
                    var description = '';
                    if(value.description != null){
                        description =value.description;
                    }
                    account_data_html += '<tr>';
                    account_data_html += '<td><a href="javascript:;">'+value.account_name+'</a></td>';
                    account_data_html += '<td>'+description+'</td>';
                    account_data_html += '<td><span>'+currencySymbol+'</span>'+value.debit+'</td>';
                    account_data_html +=  '<td><span>'+currencySymbol+'</span>'+value.credit+'</td>';
                    account_data_html += '</tr>';
                    sum_debit += parseInt(value.debit);
                    sum_credit += parseInt(value.credit);
                });

                account_data_html += '<tr><td><strong>Balance :</strong></td><td></td><td>'+ currencySymbol +''+ changeToFloat(sum_debit.toString())+'</td><td>'+ currencySymbol +''+ changeToFloat(sum_credit.toString())+'</td></tr>';
                $.each(files_data,function (key, value1){

                    var location = base_url+'/company/'+value1.file_location;
                    if (value1.file_extension == 'xlsx') {
                        location = upload_url + 'company/images/excel.png';
                    } else if (value1.file_extension == 'pdf') {
                        location = upload_url + 'company/images/pdf.png';
                    } else if (value1.file_extension == 'dox' || value1.file_extension == 'docx' ) {
                        location = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (value1.file_extension == 'txt') {
                        location = upload_url + 'company/images/notepad.jpg';
                    } else if (value1.file_extension == 'xml') {
                        location = upload_url + 'company/images/notepad.jpg';
                    } else {
                        location  = base_url+'/company/'+value1.file_location;
                    }
                    $.ajax({
                        url:location,
                        async: false,
                        error: function()
                        {

                             location = base_url+'/company/images/dummy-img.jpg';

                        },
                        success: function()
                        {

                             location = base_url+'/company/'+value1.file_location;

                        }
                    });
                    files_data_html += '<tr>';
                    files_data_html += '<td>'+value1.file_name+'</td>';
                    files_data_html += '<td><img width="100" height="100" src="'+ location +'"></td>';
                    files_data_html += '</tr>';
                });


                setTimeout(function(){
                    $(".account_details_html").html(account_data_html);
                    $(".files_details_html").html(files_data_html);
                }, 400);


            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }



});


