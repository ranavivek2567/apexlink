$(document).ready(function(){
	console.log("Mission Start... Do or Die!");
	jqGridTenantTransactionNSF('All');
    jqGridTenantSecurityDeposit('All');
    jqGridTenantPayment('All');

	$(document).on("change","#select_all_nsf", function(){
		if( $(this).is(':checked') ){   
			$(".tent_nsf_checkbox").prop("checked",true);
	    }else{
	    	$(".tent_nsf_checkbox").prop("checked",false);
	    }
	});

	$(document).on("change",".tent_nsf_checkbox", function(){
	    if ($('.tent_nsf_checkbox:checked').length == $('.tent_nsf_checkbox').length) {
	       	$("#select_all_nsf").prop("checked",true);
	    }else{
	    	$("#select_all_nsf").prop("checked",false);
	    }
	});

	$(document).on("change", "#tenant_transaction_nsf .select_options", function (e) {
        e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var elem = $(this);
        var userArr = [];
        userArr.push(id);
        postNSFRecord(userArr);
    });

    $(document).on("change", "#tenant_security_deposit .select_options", function (e) {
        e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var elem = $(this);
        var userArr = [];
        userArr.push(id);
        postSecurityDepositRecord(userArr);
    });

    $(document).on("change", "#tenant_payment .select_options", function (e) {
        e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var elem = $(this);
        var userArr = [];
        userArr.push(id);
        postPendingPayment(userArr);
    });

    $(document).on("click", ".postnsf", function (e) {
        var userArr = [];
    	$(".tent_nsf_checkbox:checked").each(function () {
	        var id = $(this).parent().parent().attr("id");
        	userArr.push(id);
	    });   
	    console.log(userArr);
        postNSFRecord(userArr);
        $("#select_all_nsf").prop("checked",false);
    });

    $(document).on("click", "#cancelnsf", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href = '/Accounting/Accounting/';
                }
            }
        });
    });

    $(document).on("click", ".pendingtabs li", function (e) {
        $(".pendingtabs li").removeClass("active");
        $(this).addClass("active");
        var tableId = $(this).attr("data_tab");
        $(".table-responsive").addClass("hide");
        $(".table-responsive #gbox_"+tableId).parent().removeClass("hide");
        
        if (tableId == "tenant_security_deposit") {
            $(".view_accounting-content h5.panel-title").text("Security Deposit");
        }else if (tableId == "tenant_payment") {
            $(".view_accounting-content h5.panel-title").text("Tenant Payment");
        }else{
            $(".view_accounting-content h5.panel-title").text("Tenant Transaction & NSF");
        }
    });

    
});

function jqGridTenantTransactionNSF(status, deleted_at) {
    var tenantId = $(".tenant_id").val();
    var currentAmount = "Amount("+default_currency_symbol+")";
    var table = 'tenant_charges';
    var columns = ['<input type="checkbox" id="select_all_nsf">','Tenant Name','Property Name','Building Name','Unit_no','Unit Name','Credit','Debit',currentAmount,'Period','Description','Action'];
    var select_column = ['Post'];
    var joins = [
    		{table:'tenant_charges',column:'user_id',primary:'id',on_table:'users'},
    		{table:'tenant_charges',column:'user_id',primary:'user_id',on_table:'tenant_property'},
	    	{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
	    	{table:'tenant_property',column:'building_id',primary:'id',on_table:'building_detail'},
	    	{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'},
	    	{table:'tenant_charges',column:'charge_code',primary:'id',on_table:'company_accounting_charge_code'},
	    	{table:'company_accounting_charge_code',column:'credit_account',primary:'id',on_table:'company_credit_accounts'},
	    	{table:'company_accounting_charge_code',column:'debit_account',primary:'id',on_table:'company_debit_accounts'}
	    ];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'status',value:2,condition:'=',table:'tenant_charges'}];
    var extra_columns = [];
    var columns_options = [
        {name: 'Id', index: 'id', width: 50, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
        {name:'Tenant Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:'users'},
        {name:'Property Name',index:'property_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
        {name:'Building Name',index:'building_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'building_detail'},
        {name:'Unit_no',index:'unit_no',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details'},
        {name:'Unit Number',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
        {name:'Credit',index:'credit_accounts',align:"center", width:170,searchoptions: {sopt: conditions},table:'company_credit_accounts'},
        {name:'Debit',index:'debit_accounts',align:"center", width:170,searchoptions: {sopt: conditions},table:'company_debit_accounts'},
        {name:'Amount',index:'amount',align:"center", width:170,searchoptions: {sopt: conditions},table:'tenant_charges',formatter:currencyFormatter},
        {name:'Period',index:'end_date', width:170,align:"center",searchoptions: {sopt: conditions},table:'tenant_charges',formatter:changeDateFormat},
        {name:'Description',index:'description', width:170,searchoptions: {sopt: conditions},table:'company_accounting_charge_code'},
        {name:'Action',index:'select', title: false, width:135,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    var ignore_array = [];
    jQuery("#tenant_transaction_nsf").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'tenant_charges.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Tenant Transaction & NSF",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}

function jqGridTenantSecurityDeposit(status, deleted_at) {
    var tenantId = $(".tenant_id").val();
    var currentAmount = "Amount("+default_currency_symbol+")";
    var table = 'users';
    var columns = ['<input type="checkbox" id="select_all_nsf">','Tenant Name','Property Name','Building Name','Unit_no','Unit Name','Credit','Debit',currentAmount,'Period','Description','Action'];
    var select_column = ['Post'];
    var joins = [
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_charges'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},
            {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
            {table:'tenant_property',column:'building_id',primary:'id',on_table:'building_detail'},
            {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'},
            {table:'tenant_charges',column:'charge_code',primary:'id',on_table:'company_accounting_charge_code'},
            {table:'company_accounting_charge_code',column:'credit_account',primary:'id',on_table:'company_credit_accounts'},
            {table:'company_accounting_charge_code',column:'debit_account',primary:'id',on_table:'company_debit_accounts'}
        ];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'user_type',value:2,condition:'=',table:'users'},{column:'record_status',value:0,condition:'=',table:'users'},{column:'payment_status',value:1,condition:'!=',table:'users'}];
    var extra_columns = [];
    var columns_options = [
        {name: 'Id', index: 'id', width: 50, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
        {name:'Tenant Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:'users'},
        {name:'Property Name',index:'property_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
        {name:'Building Name',index:'building_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'building_detail'},
        {name:'Unit_no',index:'unit_no',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details'},
        {name:'Unit Number',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
        {name:'Credit',index:'credit_accounts',align:"center", width:170,searchoptions: {sopt: conditions},table:'company_credit_accounts'},
        {name:'Debit',index:'debit_accounts',align:"center", width:170,searchoptions: {sopt: conditions},table:'company_debit_accounts'},
        {name:'Amount',index:'amount',align:"center", width:170,searchoptions: {sopt: conditions},table:'tenant_charges',formatter:currencyFormatter},
        {name:'Period',index:'end_date', width:170,align:"center",searchoptions: {sopt: conditions},table:'tenant_charges',formatter:changeDateFormat},
        {name:'Description',index:'description', width:170,searchoptions: {sopt: conditions},table:'company_accounting_charge_code'},
        {name:'Action',index:'select', title: false, width:135,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    var ignore_array = [];
    jQuery("#tenant_security_deposit").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'users.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Pending Security Deposit",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}

function jqGridTenantPayment(status) {
    var tenantId = $(".tenant_id").val();
    var currentAmount = "Amount("+default_currency_symbol+")";
    var table = 'transactions';
    var columns = ['<input type="checkbox" id="select_all_nsf">','Tenant Name','Property Name','Building Name','Unit_no','Unit Name','Credit','Debit',currentAmount,'Period','Description','Action'];
    var select_column = ['Post'];
    var joins = [
            {table:table,column:'user_id',primary:'id',on_table:'users'},
            {table:table,column:'user_id',primary:'user_id',on_table:'tenant_charges'},
            {table:table,column:'user_id',primary:'user_id',on_table:'tenant_property'},
            {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
            {table:'tenant_property',column:'building_id',primary:'id',on_table:'building_detail'},
            {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'},
            {table:'tenant_charges',column:'charge_code',primary:'id',on_table:'company_accounting_charge_code'},
            {table:'company_accounting_charge_code',column:'credit_account',primary:'id',on_table:'company_credit_accounts'},
            {table:'company_accounting_charge_code',column:'debit_account',primary:'id',on_table:'company_debit_accounts'}
        ];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [
        {column:'user_type',value:2,condition:'=',table:'users'},
        {column:'record_status',value:0,condition:'=',table:'users'},
        {column:'user_type',value:'TENANT',condition:'=',table:table},
        {column:'type',value:'RENT',condition:'=',table:table},
        {column:'payment_status',value:'ERROR',condition:'=',table:table}
    ];
    var extra_columns = [];
    var columns_options = [
        {name: 'Id', index: 'id', width: 50, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},
        {name:'Tenant Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:'users'},
        {name:'Property Name',index:'property_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
        {name:'Building Name',index:'building_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'building_detail'},
        {name:'Unit_no',index:'unit_no',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details'},
        {name:'Unit Number',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
        {name:'Credit',index:'credit_accounts',align:"center", width:170,searchoptions: {sopt: conditions},table:'company_credit_accounts'},
        {name:'Debit',index:'debit_accounts',align:"center", width:170,searchoptions: {sopt: conditions},table:'company_debit_accounts'},
        {name:'Amount',index:'amount',align:"center", width:170,searchoptions: {sopt: conditions},table:'tenant_charges',formatter:currencyFormatter},
        {name:'Period',index:'end_date', width:170,align:"center",searchoptions: {sopt: conditions},table:'tenant_charges',formatter:changeDateFormat},
        {name:'Description',index:'description', width:170,searchoptions: {sopt: conditions},table:'company_accounting_charge_code'},
        {name:'Action',index:'select', title: false, width:135,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    var ignore_array = [];
    jQuery("#tenant_payment").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at: 'no'
        },
        viewrecords: true,
        sortname: 'transactions.updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Pending Payment",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}

function actionCheckboxFmatter(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {
        return '<input type="checkbox" name="tent_nsf_checkbox[]" class="tent_nsf_checkbox" id="tent_nsf_checkbox_' + rowObject.id + '" data_id="' + rowObject.id + '"/>';
    }
}

function changeDateFormat(cellvalue, options, rowObject) {
    if (cellvalue !== undefined) {
        if (cellvalue == "") {
            return "N/A";
        }
    	var myDate = cellvalue.split('-');
        return myDate[0]+myDate[1];
    }else{
        console.log("cellvalue2="+cellvalue);
        return "N/A";    
    }
    return "N/A";
}

function currencyFormatter (cellValue, options, rowObject){
    if(cellValue!==undefined && cellValue!==""){
        return default_currency_symbol+''+cellValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+".00";
    } else {
        return "";
    }
}

function postNSFRecord(userIdArr){
    $.ajax({
        url:'/Accounting/AjaxPendingGLPosting',
        type: 'POST',
        data: {
            "class": 'AjaxPendingGLPosting',
            "action": 'updateChargesStatus',
            "user_id":userIdArr
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status=='success'){
			    toastr.success(response.message);
			   	$('#tenant_transaction_nsf').trigger( 'reloadGrid' );
            }else{
                toastr.success(response.message);
            }
        }
    });

}

function postSecurityDepositRecord(userIdArr){
    $.ajax({
        url:'/Accounting/AjaxPendingGLPosting',
        type: 'POST',
        data: {
            "class": 'AjaxPendingGLPosting',
            "action": 'updateSecurityDeposit',
            "user_id":userIdArr
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status=='success'){
                toastr.success(response.message);
                $('#tenant_security_deposit').trigger( 'reloadGrid' );
            }else{
                toastr.success(response.message);
            }
        }
    });

}

function postPendingPayment(userIdArr){
    $.ajax({
        url:'/Accounting/AjaxPendingGLPosting',
        type: 'POST',
        data: {
            "class": 'AjaxPendingGLPosting',
            "action": 'updatePendingPayment',
            "user_id":userIdArr
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status=='success'){
                toastr.success(response.message);
                $('#tenant_payment').trigger( 'reloadGrid' );
            }else{
                toastr.success(response.message);
            }
        }
    });

}