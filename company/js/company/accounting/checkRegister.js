$(document).ready(function(){
getCheckList();
getBankList();

    $(document).on("click",".check_search",function(){
        callCheckFilter();
    });

    function callCheckFilter(){
        var grid = $("#getCheckList"),f = [];
        var check_number = ($('.checkNumber').val() == '')?'all':$('.checkNumber').val();
        var start_date = ($('.start_date').val() == '')?'all':$('.start_date').val();
        var end_date = ($('.end_date').val() == '')?'all':$('.end_date').val();
        var check_status = ($('#checkStatus').val() == '')?'all':$('#checkStatus').val();
        var bank_id = ($('#bank_account').val() == '')?'all':$('#bank_account').val();
            f.push({
                field: " accounting_banking.check_number",
                op: "eq",
                data: check_number
            },{
                field: " accounting_banking.check_status",
                op: "eq",
                data: check_status
            },{
                field: " accounting_banking.bank_id",
                op: "eq",
                data: bank_id
            }, {field: " accounting_banking.date", op: "dateBetween", data: start_date, data2: end_date});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all',extra_where :[]});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }
});


function getCheckList()
{

    var table = 'accounting_banking';
    var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">','Check No','Date','name','Payee','other_name','Amount','user_type','Pay Type', 'Status'];
    var select_column = ['Edit','Delete','Cancel'];
    var joins = [{table:table,column:'user_id',primary:'id',on_table:'users',as :'u'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'frequency',value:'One Time',condition:'='}];
    var columns_options = [
        {name: 'Id', index: 'id', width: 190, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatterComplaint},
        {name:'Check No',index:'check_number', width:90,align:"left",searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'date', width:150,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'name',index:'first_name', width:150,hidden:true,searchoptions: {sopt: conditions},table:'u'},
        {name:'Payee',index:'first_name', width:150,searchoptions: {sopt: conditions},table:'u',formatter: getNameFormatter},
        {name:'other_name',index:'other_name', width:150,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Amount',index:'amount', width:150,searchoptions: {sopt: conditions},table:table},
        {name:'user_type',index:'userType', width:108,hidden:true,align:"center",searchoptions: {sopt: conditions},table:table,formatter: getTypeFormatter},
        {name:'Pay Type',index:'userType', width:108,align:"center",searchoptions: {sopt: conditions},table:table,formatter: getTypeFormatter},
        {name:'Status',index:'check_status', width:108,align:"center",searchoptions: {sopt: conditions},table:table},
    ];
    var ignore_array = [];
    jQuery("#getCheckList").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'accounting_banking.created_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Print check",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );




}



function actionCheckboxFmatterComplaint(cellvalue, options, rowObject) {
    if (rowObject !== undefined) {

        var data = '';
        var data = '<input type="checkbox" name="maintenance_checkbox[]" class="check_checkbox"  data_id="' + rowObject.id + '" data_userType="' + rowObject.user_type + '"/>';
        return data;
    }
}



 function getTypeFormatter(cellvalue, options, rowObject)
 {
    
    if(rowObject!==undefined)
  {
    if(rowObject.user_type==2)
    {
       return 'Tenant'; 
    }
    else if(rowObject.user_type==4)
    {
      return 'Owner';  
    }
    else if(rowObject.user_type==3)
    {
     return 'Vendor';   
    }
        else if(rowObject.user_type==0)
    {
     return 'Other';   
    }
    else
    {
    return 'Other';   
    }
 }
}



function getNameFormatter(cellvalue, options, rowObject)
{
    if(rowObject!==undefined)
  {

    if(rowObject.user_type==0)
    {
     return rowObject.other_name;   
    }
    else
    {
       return rowObject.name;    
    }
  
 }

}


    $(document).on("click",".check_checkbox",function(){
        if(this.checked==true)
        {
         $(".check_checkbox").prop('checked', false);
          $(this).prop('checked', true);
          var id = $(this).attr('data_id');
          var user_type = $(this).attr('data_usertype');
           $(".print_check").attr("data-check_id", id);
           $(".print_check").attr("data-user_type", user_type);
 
        }
        else
        {
            $(".check_checkbox").prop('checked', false);
          $(this).prop('checked', false);
          var id = $(this).attr('data_id');
           $(".print_check").attr("data-check_id", '0');
        }
         
   });


      $(document).on("click",".print_check",function(){
        var check_id = $(this).attr('data-check_id');
        var user_type = $(this).attr('data-user_type');
        if(check_id==0)
        {
          toastr.error("You need to check first");
          return false;
        }
        else
        {
                  $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'printCheckData',
                    "class": 'accounting',
                    'check_id':check_id,
                    'user_type':user_type

                },
                success: function (response) {
                   var response = JSON.parse(response);
                   $("#stAmountNumeric").html(response.data.amount);
                   $("#stPayeeNameAddress").html(response.data.other_address);
                   if(response.user_type!=0)
                   {
                      $("#stPayeeName").html(response.data.first_name+' '+response.data.last_name);  
                   }
                   else
                   {
                      $("#stPayeeName").html(response.data.name);
                   }
                  
                     
                   $('#CheckModal').modal('show');
                    
                }
            });
        }
     });


      setTimeout(function(){
  $('.calander').datepicker({
        yearRange: '2019:2040',
        changeMonth: true,
        changeYear: true,
        dateFormat: date
    }).datepicker("setDate", new Date());

},300);


      function getBankList()
      {
             $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {
                    "action": 'getBankList',
                    "class": 'accounting',
                },
                success: function (response) {
                   $("#bank_account").html(response);
                }
            });
      }






      

