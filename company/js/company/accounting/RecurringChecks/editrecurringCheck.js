$(document).ready(function() {
    // Recurring_Bill();
    getProperties();
    getUserNameByType(3);


    var editid = $('#check_id').val();

    if(editid !='' && editid !='undefined'){
      //  alert(editid);
        editRecurringCheck(editid);
    }
});


function getProperties()
{
    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {
            "action": 'getProperties',
            "class": 'accounting'
        },
        success: function (response) {

            var response = JSON.parse(response);
            $("#property").html(response.property);



        }
    });
}



function getBuildings(id) {
    var property_id = id;

    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {
            "action": 'getBuildings',
            "class": 'accounting',
            'property_id':property_id
        },
        async : false,
        success: function (response) {
            var response = JSON.parse(response);
            $("#building").html(response.building);
        }
    });
return false;
}
function BankList(id) {
    var property_id = id;
    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {
            "action": 'getBankListByProperty',
            "class": 'accounting',
            'property_id':property_id
        },
        async : false,
        success: function (response) {
            $("#selectBank").html(response);
        }
    });
}

function getUnits(id) {
    var building_id = id;
    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {
            "action": 'getUnits',
            "class": 'accounting',
            'building_id':building_id
        },
        async : false,
        success: function (response) {
            var response = JSON.parse(response);
            $("#unit").html(response.unit);
        }
    });

}
function getUserNameByType(type,property_id)
{
    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {

            "action": 'getUserNameByType',
            "class": 'accounting',
            'type':type,
            'property_id':property_id

        },
        async : false,
        success: function (response) {
            $(".payOrderOff").html(response);


        }
    });

}


$(document).on("change",".payOrderOff",function(){
    var user_id = $(this).val();
    var property_id = $("#property").val();
    var type = $("input[name='userType']:checked").val();
    $.ajax({
        url:'/accountingReceivable',
        type: 'POST',
        data: {

            "action": 'getAddress',
            "class": 'accounting',
            'property_id':property_id,
            'type':type,
            'user_id':user_id,

        },
        success: function (response) {

            $(".address").html(response);

        }
    });
});

function editRecurringCheck(id) {
   //  alert(id);
    $.ajax ({
        type: 'post',
        url: '/Accounting/recurring-check-ajax',
        data: {
            class: "RecurringChecks",
            action: "view",
            id: id,
        },
        success: function (response) {
            console.log(response);
            var data = $.parseJSON(response);
            var checkdata = data.data;
            $('#property option[value="'+checkdata['property_id']+'"]').prop('selected',true);
            getBuildings(checkdata['property_id']);
            $('#building option[value="'+checkdata['building_id']+'"]').prop('selected',true);
            $('#building').val(checkdata['building_id']);
            getUnits(checkdata['building_id']);
            $('#unit option[value="'+checkdata['unit_id']+'"]').prop('selected',true);
            $('#unit').val(checkdata['unit_id']);
            BankList(checkdata['property_id']);
            $('#selectBank option[value="'+checkdata['bank_id']+'"]').prop('selected',true);
            $('#memo').val(checkdata['memo']);
            $('.endingbalance ').val(checkdata['endingbalance']);
            $('.duration').val(checkdata.duration);

            $('.address').html(checkdata.other_address);
            $('input[name=userType][value="'+checkdata['userType']+'"]').prop('checked',true);
           $('#frequency option[value="'+checkdata['frequency']+'"]').prop('selected',true);
            getUserNameByType(checkdata['userType'],checkdata['property_id']);
            if(checkdata['userType']==0){
                $('.payOrderOff').hide();
                $(".orderOff").html("<input type='text' value='"+checkdata['other_name']+"' name='other_name' class='payOrderOff form-control' >");
            }
            else {
                $('.payOrderOff option[value="'+checkdata['user_id']+'"]').prop('selected',true);
            }

            $(".calander").datepicker({
                dateFormat: jsDateFomat,
                changeYear: true,
            //    yearRange: "-100:+20",
                changeMonth: true,
             //   minDate:  response.data.invoice_date
            }).datepicker("setDate", checkdata.date);

        }
    });
    return false;
}

$(document).on("click",".selectUser",function(){

    var val = $(this).val();
    var property_id = $("#property").val();
    if(val==0)
    {
        $(".orderOff").html("<input type='text' name='other_name' class='payOrderOff form-control' >");
        $(".memo").attr("readonly", false);
    }
    else
    {


        $(".orderOff").html('<select name="payOrderOff" class="form-control payOrderOff"><option>Select</option></select>');
        if(val==3 || val==2 && (property_id!=0 || property_id!=''))
        {
            getUserNameByType(val,property_id);
        }
        else
        {
            getUserNameByType(val,0);
        }

        $(".memo").attr("readonly", true);
    }


});

$("#createCheckForm").validate({
    rules: {
        property: {
            required:true
        },
        building: {
            required: true
        },
        unit: {
            required:true
        },
        selectBank: {
            required:true
        },
        Date: {
            required:true
        },
    }
});


$('#createCheckForm').submit(function () {
    if($("#createCheckForm").valid()){
        var form = $('#createCheckForm')[0];
        var formData = new FormData(form);
        formData.append('action','update');
        formData.append('class','RecurringChecks');
        $.ajax({
            type: 'post',
            url:'/Accounting/recurring-check-ajax',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success"){
                    localStorage.setItem("Message", response.message);
                    localStorage.setItem("rowcolor",true)
                    window.location.href = base_url+'/Accounting/RecurringChecks';
                }else{
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
        return false;
    }
    return false;
});

$(document).on('click','#cancel_frm',function () {
    bootbox.confirm({
        message: "Do you want to cancel this action ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = base_url + '/Accounting/RecurringChecks';
            }
        }
    });
});


$(document).on('click','#Reset',function () {
    var id = $('#check_id').val();
    bootbox.confirm({
        message: "Do you want to reset this form ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = base_url + '/Accounting/EditRecurringCheck?id='+id;
            }
        }
    });
});
