$(document).ready(function () {
    var id = getParameterByName('id');

    $.ajax({
        type: 'get',
        url: '/Accounting/recurring-invoice-ajax',
        data: {
            id: id,
            class: 'RecurringInvoice',
            action: 'View'},
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200) {
                $.each(response.data, function (key, value) {
                    if(key=='key_access_codes_info'){
                    } else {
                        if(key =='frequency'){
                            var frequency = value;
                            var freval;
                            if(frequency == 1){
                                freval = 'Weekly';
                            } else if(frequency == 2){
                                freval='Bi-Weekly';
                            } else if(frequency == 3){
                                freval='Monthly';
                            } else if(frequency == 4){
                                freval='Annually';
                            } else if(frequency == 5){
                                freval='Quartely';
                            } else if(frequency == 6){
                                freval='Semi-Annually';
                            } else if(frequency == 7){
                                freval='Daily';
                            } else if(frequency == 8){
                                freval='Bi-Monthly';
                            }
                            $('.' + key).html(freval);
                        }
                        else{
                            $('.' + key).html(value);
                        }

                        if(key=='invoice_to' && (value == null || value == '')){


                            $('.invoice_to').html(response.data.other_name);
                        }


                    }
                });

                var charge_data = response.charge_data;
                var charge_data_html ='';
                var propertyname ='';
                $.each(charge_data,function (key, value){
                    charge_data_html += '<tr>';
                    if(response.data.property_name!= null && response.data.property_name != 'undefined'){
                        propertyname =response.data.property_name;
                    } else {
                        propertyname ='';
                    }
                    charge_data_html += '<td><a href="javascript:;" style="color:#05A0E4 ! important;font-weight: bold">'+propertyname+'</a></td>';
                    charge_data_html += '<td>'+response.data.unit+'</td>';
                    charge_data_html += '<td>'+value.description+'</td>';
                    charge_data_html +=  '<td>'+value.description+'</td>';
                    var amt = changeToFloat(value.amount.toString());
                    charge_data_html +=  '<td>'+default_currency_symbol+amt+'</td>';
                    charge_data_html += '</tr>';
                });
                setTimeout(function(){
                    $(".charge_data_html").html(charge_data_html);
                    }, 400);


            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName(name) {
        var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
        if (results == null) {
            return "";
        } else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

});