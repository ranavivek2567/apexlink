$(document).ready(function(){
    getReceivableUsers();


    var invoice_id =  localStorage.getItem("invoice_id");
    var type =  localStorage.getItem("type");
    var Name =  localStorage.getItem("Name");
    $(".listOfReceivable").val(Name);



    // alert(invoice_id);
    // alert(type);
    // alert(Name);

    if(invoice_id !=null && type !=null && Name != null) {
        getRecivableDataFromInvoice(invoice_id, type, Name);
    }
});


$(document).on("keyup",".listOfReceivable",function(){
   getUsersFilter();

});




    function getRecivableDataFromInvoice(invoice_id='',type='',Name='') {
        var Localstorageuserid =  localStorage.getItem("user_id");
        $(".userFullAccount").fadeIn();
        $(".allCharges").fadeIn();
        if (type == 'user') {

            if(Localstorageuserid != ''){
                user_id =  Localstorageuserid
            }else{
                user_id = $(this).attr('data-user_id');
            }

            $.ajax({
                url: '/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getReceiableData',
                    "class": 'accounting',
                    "type": type,
                    "user_id": user_id,
                    "username": Name

                },
                success: function (response) {

                    $(".userFullAccount").html(response);
                    getAllChargesData(type, user_id, Name);


                }
            });


        }
        else {
            // var invoice_id = $(this).attr('data-invoice_id');
            $.ajax({
                url: '/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getReceiableData',
                    "class": 'accounting',
                    "invoice_id": invoice_id,
                    "type": type,
                    "username": Name

                },
                success: function (response) {


                    $(".userFullAccount").html(response);
                    getAllChargesData(type, invoice_id, Name);

                }
            });

        }

    }




function getReceivableUsers()
{

    var table = 'accounting_manage_charges';
    var columns = ['user_id','invoice_id','Name','last_name','Property','Type','amt_due','Amount Balance('+default_currency_symbol+')','Unit','unit_num','Invoice Number','Other_name','property_name'];
    var select_column = ['Edit','Delete'];
    var joins = [
        {table:'accounting_manage_charges',column:'user_id ',primary:'id',on_table:'users'},
        {table:'accounting_manage_charges',column:'user_id',primary:'user_id',on_table:'tenant_property'},
        {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
        {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'},
        {table:'accounting_manage_charges',column:'invoice_id',primary:'id',on_table:'accounting_invoices'},
       
    ];
    var conditions = ["eq","bw","ew","cn","in"];
    //var extra_where = [{column:'user_id',value:tenantId,condition:'=',table:'hoa_violation'}];
    var extra_where = [];
    var extra_columns = [];
    var columns_options = [
        {name:'user_id',index:'user_id',align:"center", width:170,hidden:true,searchoptions: {sopt: conditions},table:'accounting_manage_charges'},
        {name:'invoice_id',index:'invoice_id',align:"center", width:170,hidden:true,searchoptions: {sopt: conditions},table:'accounting_manage_charges'},
        {name:'Name',index:'name',align:"center", width:170,searchoptions: {sopt: conditions},table:'users',formatter:getNameFormatter},
        {name:'last_name',index:'last_name',align:"center", width:170,hidden:true,searchoptions: {sopt: conditions},table:'users'},
        {name:'Property',index:'property_name', width:170,align:"center",searchoptions: {sopt: conditions},table:'general_property',formatter:getPropertyFormatter},
        {name:'Type',index:'user_type', width:170,align:"center",searchoptions: {sopt: conditions},table:'users',formatter:getTypeFormatter},
        {name:'amt_due',index:'total_due_amount', width:170,align:"center",hidden:true,searchoptions: {sopt: conditions},table:'accounting_manage_charges'},
        {name:'Amount Balance(currency sign)',index:'total_due_amount', width:170,align:"center",searchoptions: {sopt: conditions},table:'accounting_manage_charges',formatter:signformatter},
        {name:'Unit',index:'unit_prefix', width:170,align:"center",searchoptions: {sopt: conditions},table:'unit_details',formatter:getUnitNameFormatter},
        {name:'unit_num',index:'unit_no', width:170,align:"center",hidden:true,searchoptions: {sopt: conditions},table:'unit_details'},
        {name:'Invoice Number',index:'invoice_number', width:170,align:"center",searchoptions: {sopt: conditions},table:'accounting_invoices'},
        {name:'Other_name',index:'other_name', width:170,hidden:true,align:"center",searchoptions: {sopt: conditions},table:'accounting_invoices'},
        {name:'property_name',index:'property_name', width:170,hidden:true,align:"center",searchoptions: {sopt: conditions},table:'general_property'},


        
    ];
    var ignore_array = [];
    jQuery("#accounting_invoices").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,

            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'no'
        },
        viewrecords: true,
        sortname: 'users.Name',
        sortorder: "asc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Tenants",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}





function getBalance(cellvalue, options, rowObject)
{
 return 1;   
}


function signformatter(cellvalue, options, rowObject) {
    if(rowObject !== undefined){
        var cell="";
        if(rowObject.amt_due==0)
        {
           return  default_currency_symbol+"0.0";
        }
        else
        {
            var amt = rowObject.amt_due.toString();
            var amount = changeToFloat(amt);
        }
        cell= default_currency_symbol+""+amount;
        return cell;
    }
}


function getTypeFormatter(cellvalue, options, rowObject)
 {

    
    if(rowObject!==undefined)
  {
    if(rowObject.Type==2)
    {
       return 'Tenant'; 
    }
    else if(rowObject.Type==4)
    {
      return 'Owner';  
    }
    else
    {
     return 'Others';   
    }
 }
}


function getUnitNameFormatter(cellvalue, options, rowObject)
{
        if(rowObject!==undefined)
  {
   
     return rowObject.Unit+'-'+rowObject.unit_num;
    
  
 }

}


function getUsersFilter()
{
    
    var grid = $("#accounting_invoices"),f = [];
    var value = $('#listOfReceivable').val();

    f.push(
        {field: "users.first_name", op: "eq", data: value,con:'OR'},
        {field: "general_property.property_name", op: "cn", data: value, con:'OR'},
        {field: "accounting_manage_charges.total_due_amount", op: "cn", data: value, con:'OR'},
        {field: "accounting_manage_charges.total_amount", op: "cn", data: value, con:'OR'},
        {field: "accounting_invoices.invoice_number", op: "cn", data: value}
        );
            grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);

}


function getNameFormatter(cellvalue, options, rowObject)
{
  if(rowObject!==undefined)
  { 
    if(rowObject.Other_name=='')
    {
        return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Name+'"  data-type="user" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.Name+'</strong></a>';
    }
    else
    {
        return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Other_name+'" data-type="other" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.Other_name+'</strong></a>';
    }
  }
}




function getPropertyFormatter(cellvalue, options, rowObject)
{
  if(rowObject!==undefined)
  { 
    if(rowObject.Other_name=='')
    {
        return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Name+'"  data-type="user" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.property_name+'</strong></a>';
    }
    else
    {
        return '<a href="JavaScript:Void(0);" class="getChargeTable" data-username ="'+rowObject.Other_name+'" data-type="other" data-user_id="'+rowObject.user_id+'" data-invoice_id="'+rowObject.invoice_id+'" style="color:#05A0E4 !important;"><strong>'+rowObject.property_name+'</strong></a>';
    }
  }
}



$(document).on("click",".getChargeTable, .reallocate",function(){
  
 var type = $(this).attr('data-type');
 var Name = $(this).attr('data-username');
 $(".userFullAccount").fadeIn();
 $(".allCharges").fadeIn();


 if(type=='user')
 {
 var user_id = $(this).attr('data-user_id');


            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getReceiableData',
                    "class": 'accounting',
                    "type":type,
                    "user_id":user_id,
                    "username":Name

                },
                success: function (response) {

                    $(".userFullAccount").html(response);
                    getAllChargesData(type,user_id,Name);
                 
                    


                }
            });
 

 }
 else
 {
 var invoice_id = $(this).attr('data-invoice_id');

            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getReceiableData',
                    "class": 'accounting',
                    "invoice_id":invoice_id,
                    "type":type,
                    "username":Name

                },
                success: function (response) {
                    

                    $(".userFullAccount").html(response);
                   getAllChargesData(type,invoice_id,Name);
                  


                }
            });

 }

  
});







function getAllChargesData(type,id,Name)
{
    localStorage.removeItem('invoice_id');
    localStorage.removeItem('type');
    localStorage.removeItem('Name');
     if(type=='user')
 {
   
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getAllChargesData',
                    "class": 'accounting',
                    "type":type,
                    "user_id":id,
                    "username":Name

                },
                success: function (response) {

                    $(".allCharges").html(response);
                    $('.reallocate').attr('data-type',type);
                    $('.reallocate').attr('data-username',Name);
                    $('.reallocate').attr('data-user_id',id);

                       updateAllPayments();
                   
                 }
            });
        
 

 }
 else
 {
    
 
            $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'getAllChargesData',
                    "class": 'accounting',
                    "type":type,
                    "invoice_id":id,
                    "username":Name

                },
                success: function (response) {

                    $(".allCharges").html(response);
                    $('.reallocate').attr('data-type',type);
                    $('.reallocate').attr('data-username',Name);
                    $('.reallocate').attr('data-invoice_id',id);
                      updateAllPayments(); 
                  }
            });




 }



}





$(document).on('keypress keyup','.numberonly',function(){
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }

});


$(document).on("blur keyup",".paidAmt",function(){

 
  var paidAmt = $(".paidAmt").val().replace(/,/g, '');
  if(paidAmt=="")
  {
    paidAmt = 0;
  }
  var hiddenDueAmount = $('.hiddenDueAmount').val().replace(/,/g, '');
  var hiddenOverPay = $(".hiddenOverPay").val().replace(/,/g, '');
  var hiddenUncheckAmt = $('.hiddenUncheckedAmt').val().replace(/,/g, '');

 

  var overPayUnderPay = parseFloat(hiddenOverPay) + parseFloat(paidAmt) +parseFloat(hiddenUncheckAmt) -parseFloat(hiddenDueAmount);
  var overPayAmt = overPayUnderPay.toString();
  var floatAmt = changeToFloat(overPayAmt); 
  $(".overUnderPay").val(floatAmt);

  
});




function updateAllPayments()
{
   
  $("#updateAllCharges").validate({
    rules: {
    },
    submitHandler: function (e) {
        var form = $('#updateAllCharges')[0];
        var formData = new FormData(form);
        formData.append('action','updateAllCharges');
        formData.append('class','accounting');

        var hiddenDueAmt = $(".hiddenDueAmount").val().replace(/,/g, '');
        if(hiddenDueAmt==0)
        {
            bootbox.confirm({
                message: "This Invoice has been paid in full, do you want to enter a Credit ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                        callback: function (result) {
                        if (result == true) {
                        var OverUnderPay = $(".overUnderPay").val().replace(/,/g, '');
                        if(OverUnderPay<0)
                        {
                        toastr.error("Currently you are paying less amount"); 
                        return false;
                        }
                        var ref = $(".ref").val();
                        var check = $(".check").val();
                        var paymnt_type = $(".paymnt_type").val();
                        var paid_amount = $(".paidAmt").val().replace(/,/g, '');
                        var hiddenChargeType = $(".hiddenChargeType").val();

                        formData.append('class','accounting');
                        formData.append('hiddenDueAmount',hiddenDueAmt);
                        formData.append('overPay',OverUnderPay);
                        formData.append('ref',ref);
                        formData.append('check',check);
                        formData.append('paymnt_type',paymnt_type);
                        formData.append('paid_amount',paid_amount);
                        formData.append('hiddenChargeType',hiddenChargeType);

                            $.ajax({
                            url:'/accountingReceivable',
                            type: 'POST',
                            data: formData,
                            success: function (response) {
                            var response = JSON.parse(response);
                            if(response.status=='false')
                            {
                            toastr.error(response.message); 
                            }
                            else
                            {
                            toastr.success(response.message); 
                            $(".allCharges").fadeOut();
                            $(".userFullAccount").fadeOut();
                            }


                            },
                            cache: false,
                            contentType: false,
                            processData: false
                            });

                               
                              }
            }
            });

        }
        else
        {
                        var OverUnderPay = $(".overUnderPay").val();
                        if(OverUnderPay<0)
                        {
                        toastr.error("Currently you are paying less amount"); 
                        return false;
                        }
                        var ref = $(".ref").val();
                        var check = $(".check").val();
                        var paymnt_type = $(".paymnt_type").val();
                        var paid_amount = $(".paidAmt").val().replace(/,/g, '');
                        var hiddenChargeType = $(".hiddenChargeType").val();

                        formData.append('class','accounting');
                        formData.append('hiddenDueAmount',hiddenDueAmt);
                        formData.append('overPay',OverUnderPay);
                        formData.append('ref',ref);
                        formData.append('check',check);
                        formData.append('paymnt_type',paymnt_type);
                        formData.append('paid_amount',paid_amount);
                        formData.append('hiddenChargeType',hiddenChargeType);

                            $.ajax({
                            url:'/accountingReceivable',
                            type: 'POST',
                            data: formData,
                            success: function (response) {
                            var response = JSON.parse(response);
                            if(response.status=='false')
                            {
                            toastr.error(response.message); 
                            }
                            else
                            {
                            toastr.success(response.message); 
                            $(".allCharges").fadeOut();
                            $(".userFullAccount").fadeOut();
                            }


                            },
                            cache: false,
                            contentType: false,
                            processData: false
                            });

        }

      


    }
  });

}




$(document).on("click", ".pay_checkbox", function () {
     var value = $(this).val();
    if (this.checked) {
       
        $(".waiveOfAmount_"+value).attr('name','waiveOfAmount[]');
        $(".waiveOfComment_"+value).attr('name','waiveOfComment[]');
        $(".currentPayment_"+value).attr('name','currentPayment[]');
        var currntDueAmt = $(".current_due_amt_"+value).html().replace("$", '');
        var currntDueAmt = currntDueAmt.replace("$", '');
        $(".currentPayment_"+value).val(currntDueAmt);
        var overPay = $(".overUnderPay").val();
        var curntOvrPay = parseFloat(overPay) - parseFloat(currntDueAmt);

        var curntOvrPay =    curntOvrPay.toString()
        var curntOvrPay = changeToFloat(curntOvrPay);
        $(".overUnderPay").val(curntOvrPay);
        var hiddenUncheckAmt = $('.hiddenUncheckedAmt').val();
        var currentHiddenUncheckAmt  =  parseFloat(hiddenUncheckAmt) -  parseFloat(currntDueAmt);
        $('.hiddenUncheckedAmt').val(currentHiddenUncheckAmt);



      

    }
    else {
        $(".waiveOfAmount_"+value).removeAttr('name');
        $(".waiveOfComment_"+value).removeAttr('name');
        $(".currentPayment_"+value).removeAttr('name');
        $(".currentPayment_"+value).val('0.00');
        var currntDueAmt = $(".current_due_amt_"+value).html().replace("$", '');

        var overPay = $(".overUnderPay").val();
        var curntOvrPay = parseFloat(overPay) + parseFloat(currntDueAmt);
        var curntOvrPay =    curntOvrPay.toString()
        var curntOvrPay = changeToFloat(curntOvrPay);

        $(".overUnderPay").val(curntOvrPay);
        var hiddenUncheckAmt = $('.hiddenUncheckedAmt').val();
        var currentHiddenUncheckAmt  =  parseFloat(hiddenUncheckAmt) +  parseFloat(currntDueAmt);
        $('.hiddenUncheckedAmt').val(currentHiddenUncheckAmt);
       
    }

});


$(document).on("change",".paymnt_type",function(){
  var type = $(this).val();
  var user_id = $(".hiddenUser_id").val();
  if(type=='card')
  {
                $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'checkForCard',
                    "class": 'accounting',
                    "type":type,
                    "user_id":user_id

                },
                success: function (response) {
                   var info = JSON.parse(response);
                   if(info.status=='false')
                   {
                   toastr.error(info.message); 
                    $(".paymnt_type").val('check');
                   }

                    


                }
            });
    
  }
  else if(type=='ACH')
  {
                $.ajax({
                url:'/accountingReceivable',
                type: 'POST',
                data: {

                    "action": 'checkForACH',
                    "class": 'accounting',
                    "type":type,
                    "user_id":user_id

                },
                success: function (response) {

                   var info = JSON.parse(response);
                   
                   if(info.status=='false')
                   {
                    toastr.error(info.message); 
                     $(".paymnt_type").val('check');
                   }

                }
            });

  }

});


$(document).on("click",".CancelBtn",function(){
  
  bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
              $(".allCharges").fadeOut();
                    $(".userFullAccount").fadeOut();
        }
    });


});


   $(document).on("click",".goback_func",function () {
         window.history.back();
     });










