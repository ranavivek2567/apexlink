$(document).ready(function() {

    var extten=0;
    extten=(Math.round(extten * 100) / 100).toFixed(2);
    $('.interest_rate').val(extten);
    $(document).on("keyup",".principal_amt",function () {
        $(".principal_amt_div .pre-span-text").show();

    });
    $(document).on("click",".goback_func",function () {
        window.location.href='/MultiPay/PaymentPlan';
    });
    if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(15)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }
    var html1="";
    for(var i=1;i<61;i++){

        html1 +="<option value='"+i+"'>"+i+"</option>"
    }
    $(".installments").append(html1);
    $("#installments").append(html1);

    $('#new_plan').click(function () {
            $('#plan_listing').hide();
            $('#add_plan').show();
        });
    $('#cancel_install').click(function () {
            $('#payment_recieved').hide();
        });

$('#cancel_genifo').click(function () {
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#plan_listing').show();
                $('#add_plan').hide();
                $("#paymen_plan_submit").trigger('reset')
            }
        }
    });
});

$('.cancel_genifo').click(function () {
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#plan_listing').show();
                $('#update_plan').hide();
            }
        }
    });
});
    paymentplan();
    function paymentplan() {
        var table = 'payment_plan';
        var columns = ['Payor Name','user_email','user_id','other_name','Payor Type','Principal Amount('+currencySign+')', 'Interest Rate (%)', 'Installment Number','Amount Due('+currencySign+')','Frequency','Start Date','Next Payment Date','Next Payment Amount ('+currencySign+')','Interest Amount ('+currencySign+')','Status','Action'];
        var select_column = ['Edit','Delete',' Payment Received','Email','Text','Payment Receipt'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{table: 'payment_plan', column: 'user_id', primary: 'id', on_table: 'users'}];
        var columns_options = [
            {name:'Payor_Name',index:'name', width:100,searchoptions: {sopt: conditions},table:'users',formatter:name_formatter},
            {name:'user_email',index:'email',hidden:true, width:100,searchoptions: {sopt: conditions},table:'users'},
            {name:'user_id',index:'user_id',hidden:true, width:100,searchoptions: {sopt: conditions},table:table},
            {name:'other_name',index:'other_name',hidden:true, width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Payor Type',index:'payor_type', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Principal_Amount',index:'principal_amount', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Interest_Rate',index:'interest', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Installment Number',index:'installments', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Amount Due',index:'amount_due', width:80, align:"left",searchoptions: {sopt: conditions},table:table,change_type:'amount_due'},
            {name:'Frequency',index:'frequency', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Start Date',index:'start_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Next Payment Date',index:'next_payment_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'next_date'},
            {name:'Next Payment Amount',index:'next_payment_amount', width:100,searchoptions: {sopt: conditions},table:table,change_type:'nextamount'},
            {name:'Interest Amount',index:'interest', width:100,searchoptions: {sopt: conditions},table:table,formatter:intamtformatter},
            {name:'Status',index:'status', width:100,searchoptions: {sopt: conditions},table:table,change_type:'payment_plan_status'},
            {name:'Action',index:'', width:100,title:false,searchoptions: {sopt: conditions},table:table,formatter:statusFmatter1},
        ];

        var ignore_array = [];
        jQuery("#payment_plan").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'payment_plan.updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Payment Plans",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

    function name_formatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var name="";
            if (rowObject.user_id == "" || rowObject.user_id == 0 || rowObject.user_id == undefined){
                name=rowObject.other_name;
            } else if(rowObject.user_id !== ""){
               name=rowObject.Payor_Name;
            }
            return name;
        }
    }

    function statusFmatter1(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = ['Edit','Delete',' Payment Received','Email','Text','Payment Receipt'];
            console.log('obj',rowObject);
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_optionss" data_id="' + rowObject.id + '" ><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    function intamtformatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var val=rowObject.Principal_Amount;
            var int=rowObject.Interest_Rate;
            var aa =(parseFloat(val)*parseFloat(int))/100;
            aa=(Math.round(aa * 100) / 100).toFixed(2);
        return aa;
        }
    }

    $(document).on('change', '.select_optionss', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);
        $('#payment_plan').trigger('reloadGrid');
        var base_url = window.location.origin;
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var email = $("tr#"+id).find("td").eq(1).text();
        console.log('email',email);
        var user_id = $(this).attr('data-user_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        var is_default = $(this).attr('is_default');
        if (opt == 'Edit' || opt == 'EDIT') {
            $(".pre-span-text").show();
            getdataforedit(id)
        }
        if (opt == 'Delete' || opt == 'DELETE') {
            getdatafordelete(id);
        }

        if (opt == ' Payment Received' || opt == ' PAYMENT RECEIVED') {
              $("#payment_recieved").modal('show');
              console.log('id',id);
              paymentdetails(id);
        }
        if (opt == 'Email' || opt == 'EMAIL') {
            localStorage.setItem('predefined_mail',email);
            localStorage.setItem('table_green_id',id);
            localStorage.setItem('table_green_tableid', '#short_term');
            localStorage.setItem('table_green_url','/MultiPay/PaymentPlan');
            window.location.href = base_url + '/Communication/ComposeEmail';
        }
        if (opt == 'Text' || opt == 'TEXT'){
            localStorage.setItem('table_green_id',id);
            localStorage.setItem('table_green_tableid', '#short_term');
            localStorage.setItem('table_green_url','/Communication/WaitingList');
            localStorage.setItem('predefined_text',email);
            window.location.href = base_url + '/Communication/AddTextMessage';
        }
        if (opt == 'Payment Receipt' || opt == 'PAYMENT RECEIPT'){
            $("#email").val(email);
            paymentreceiptdetails(id);
        }
    });

            $(document).on('change','#update_plan .payor_typee',function () {
                var id=$('.payor_typee').val();
                getusernames_edit(id);
            });

            $(document).on('change','.payor_type',function () {
                var id=$('.payor_type').val();
                getusernames(id);
            });

        function getusernames(id) {
            $.ajax({
                type: 'post',
                url: '/paymentplan-ajax',
                data: {
                    class: "paymentplan",
                    action: "getpayorname",
                        'id':id
                },
                success: function (response) {
                      var res = JSON.parse(response);
                              console.log(res);
                             if(res.type==1 || res.type==2 || res.type==3 || res.type==4){
                                 $('.payor_name').show();
                                 $("input[name='payor_name']").hide();
                                 $('.payor_name').html(' ').append(res.html);
                             }
                             else if(res.type==5){
                                 $('.payor_name').hide();
                                 $(".new_input").append(res.html);
                             }
                         },
                error: function (data){
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value){
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }

    function getusernames_edit(id) {
        $.ajax({
            type: 'post',
            url: '/paymentplan-ajax',
            data: {
                class: "paymentplan",
                action: "getpayorname",
                'id':id
            },
            success: function (response) {
                var res = JSON.parse(response);
                console.log(res);
                if(res.type==1 || res.type==2 || res.type==3 || res.type==4){
                    $('.payor_namee').show();
                    $("input[name='payor_namee']").hide();
                    $('.payor_namee').html(' ').append(res.html);
                }
                else if(res.type==5){
                    $('.payor_namee').hide();
                    $(".new_inputt").append(res.html);
                }
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value){
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    var monthNames = ["January", "February", "March", "April", "May","June","July",
        "August", "September", "October", "November","December"];
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

         $(document).on('focusout','.interest_rate,.principal_amt',function () {
             $(".amount_due_div .pre-span-text").show();
             var val=$('.principal_amt').val();
             val=(Math.round(val * 100) / 100).toFixed(2);
             $('.principal_amt').val(val);
             var int=$('.interest_rate').val();
             var inst=$(".installments").val();
             var extt=0;
             extt=(Math.round(extt * 100) / 100).toFixed(2);
           //var aa =(val*int)/100;
             if(int <= parseInt(100)){
             if(int !=="" && val!==""){
               var aa =(parseFloat(val)*parseFloat(int))/100;
               var tot= parseFloat(val)+parseFloat(aa);
               tot=(Math.round(tot * 100) / 100).toFixed(2);
               $('.amount_due').val(tot);
               if(inst !==""){
                   var v=parseFloat($('.amount_due').val())/parseInt(inst);
                   v=(Math.round(v * 100) / 100).toFixed(2);
                   $(".next_amt").val(v);
               }
             }
             else if(int !=="" && val==""){
                 $('.amount_due').val(extt);
             } else if(int =="" && val!==""){
                 val=(Math.round(val * 100) / 100).toFixed(2);
                 $('.amount_due').val(val);
             }
             console.log('tot',tot);
             }
             else {
                 $('.interest_rate').val('');
                 $('.amount_due').val('');
                 $(".next_amt").val('');
                 toastr.error("Interest cannot be greater than 100")
             }
             });
         $(document).on('focusout','#interest_rate, #principal_amt',function () {
             var val=$('#principal_amt').val();
             var int=$('#interest_rate').val();
             var inst=$("#installments").val();
             var extt=0;
             extt=(Math.round(extt * 100) / 100).toFixed(2);
             if(int <= parseInt(100)){
             if(int !="" && val!=""){
               var aa =(parseFloat(val)*parseFloat(int))/100;
               var tot= parseFloat(val)+parseFloat(aa);
               tot=(Math.round(tot * 100) / 100).toFixed(2);
               $('#amount_due').val(tot);
                 if(inst !==""){
                     var v=parseFloat($('#amount_due').val())/parseInt(inst);
                     v=(Math.round(v * 100) / 100).toFixed(2);
                     $("#next_amt").val(v);
                 }
             }
             else if(int !=="" && val==""){
                 $('#amount_due').val(extt);
             } else if(int =="" && val!==""){
                 val=(Math.round(val * 100) / 100).toFixed(2);
                 $('#amount_due').val(val);
             }
             console.log('tot',tot);
             }
             else {
                 $('#interest_rate').val('');
                 $('#amount_due').val('');
                 toastr.error("Interest cannot be greater than 100")
             }
             });


        $(document).on('change','.installments',function () {
            $(".next_amt_div .pre-span-text").show();
             var total=$('.amount_due').val();
             var inst=$(".installments").val();
             var next_amt="";
            if(total !=="" && inst!==""){
                next_amt=parseFloat(total)/parseInt(inst);
                next_amt=(Math.round(next_amt * 100) / 100).toFixed(2);
                $('.next_amt').val(next_amt);
            }
            else{
                $('.next_amt').val(0.00);
            }
           });

        $(document).on('change','#installments',function () {
             var total=$('#amount_due').val();
             var inst=$("#installments").val();
             var next_amt="";
            if(total !="" && inst!=""){
                next_amt=parseFloat(total)/parseInt(inst);
                next_amt=(Math.round(next_amt * 100) / 100).toFixed(2);
                $('#next_amt').val(next_amt);
            }
            else{
                $('#next_amt').val(0.00);
            }
           });

    $(document).on('change','.start_date, .frequency, .installments',function () {
        var freq=$('.frequency').val();
        var date=$('.start_date').val();
        var startDate = new Date(date);
        var inst=$(".installments").val();
        console.log(startDate);
        if(freq=='1'){
            startDate.setDate(startDate.getDate() + 7);
            var week=(startDate.getDay());
            week=(days[week]).substr(0,3);
            startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+week+'.'+')';
        }
        else if(freq=='2'){
            startDate.setDate(startDate.getDate() + 14);
            var biweek=(startDate.getDay());
            biweek=(days[biweek]).substr(0,3);
            startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+biweek+'.'+')';
        }else if(freq=='3'){
            startDate.setMonth((startDate.getMonth()+1));
            var monthly=(startDate.getDay());
            monthly=(days[monthly]).substr(0,3);
            startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+monthly+'.'+')';
        }else if(freq=='4'){
            startDate.setFullYear(startDate.getFullYear() + 1);
            var yearly=(startDate.getDay());
            yearly=(days[yearly]).substr(0,3);
            startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+yearly+'.'+')';
        }
        $('.next_date').val(startDate);

    });

  $(document).on('change','#start_date, #frequency, #installments',function () {
        var freq=$('#frequency').val();
        var date=$('#start_date').val();
        var startDate = new Date(date);
        var inst=$("#installments").val();
        console.log(startDate);
        if(freq=='1'){
            startDate.setDate(startDate.getDate() + 7);
            var week=(startDate.getDay());
            week=(days[week]).substr(0,3);
            startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+week+'.'+')';
        }
        else if(freq=='2'){
            startDate.setDate(startDate.getDate() + 14);
            var biweek=(startDate.getDay());
            biweek=(days[biweek]).substr(0,3);
            startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+biweek+'.'+')';
        }else if(freq=='3'){
            startDate.setMonth((startDate.getMonth()+1));
            var monthly=(startDate.getDay());
            monthly=(days[monthly]).substr(0,3);
            startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+monthly+'.'+')';
        }else if(freq=='4'){
            startDate.setFullYear(startDate.getFullYear() + 1);
            var yearly=(startDate.getDay());
            yearly=(days[yearly]).substr(0,3);
            startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+yearly+'.'+')';
        }
        $('#next_date').val(startDate);

    });


    $(document).on('change','.start_date, .frequency, .installments',function () {
        var arr=[];
        var freq=$('.frequency').val();
        var inst=$(".installments").val();
        var startDate=$('.start_date').val();
        var st=new Date(startDate);
        for(var i=1;i<=inst;i++){
            if(freq=='1'){
                startDate=new Date(st);
                startDate.setDate(startDate.getDate() + 7*i);
                startDate= startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
                arr.push(startDate);
            }
            else if(freq=='2'){
                startDate=new Date(st);
                startDate.setDate(startDate.getDate() + 14*i);
                startDate= startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
                arr.push(startDate);
            }else if(freq=='3'){
                startDate=new Date(st);
                startDate.setMonth((startDate.getMonth()+ 1*i));
                startDate= startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
                arr.push(startDate);
            }else if(freq=='4'){
                startDate=new Date(st);
                startDate.setFullYear(startDate.getFullYear() + 1*i);
                startDate= startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
                arr.push(startDate);
            }
        }
        $(".all_dates").val(JSON.stringify(arr));
    });

    $(document).on('change','#start_date, #frequency, #installments',function () {
        var arr=[];
        var freq=$('#frequency').val();
        var inst=$("#installments").val();
        var startDate=$('#start_date').val();
        var st=new Date(startDate);
        for(var i=1;i<=inst;i++){
            if(freq=='1'){
                startDate=new Date(st);
                startDate.setDate(startDate.getDate() + 7*i);
                startDate= startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
                arr.push(startDate);
            }
            else if(freq=='2'){
                startDate=new Date(st);
                startDate.setDate(startDate.getDate() + 14*i);
                startDate= startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
                arr.push(startDate);
            }else if(freq=='3'){
                startDate=new Date(st);
                startDate.setMonth((startDate.getMonth()+ 1*i));
                startDate= startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
                arr.push(startDate);
            }else if(freq=='4'){
                startDate=new Date(st);
                startDate.setFullYear(startDate.getFullYear() + 1*i);
                startDate= startDate.getFullYear()+'-'+(startDate.getMonth()+1)+'-'+startDate.getDate();
                arr.push(startDate);
            }
        }
        $("#all_dates").val(JSON.stringify(arr));
    });

        $("#paymen_plan_submit_edit").validate({
        rules: {
            'payor_type': {required: true},
            'payor_name': {required: true},
            'principal_amt': {required: true},
            'interest_rate': {required: true},
            'installments': {required: true},
            'amount_due': {required: true},
            'frequency': {required: true},
            'start_date': {required: true},
            'status': {required: true},

        }
    });

        $("#paymen_plan_submit").validate({
        rules: {
            'payor_type': {required: true},
            'payor_name': {required: true},
            'principal_amt': {required: true},
            'interest_rate': {required: true},
            'installments': {required: true},
            'amount_due': {required: true},
            'frequency': {required: true},
            'start_date': {required: true},
            'status': {required: true},

        }
    });
        $("#update_geninfo").click(function () {
            if($("#paymen_plan_submit").valid()){
            addpayors();
            }
        });

    function addpayors() {
        var dates=new Array();
        if(JSON.parse($(".all_dates").val()) !="" || JSON.parse($(".all_dates").val()) !== undefined) dates=JSON.parse($(".all_dates").val());
        console.log(dates);
        var formarray=$('#paymen_plan_submit').serializeArray();
        $.ajax({
            type: 'post',
            url: '/paymentplan-ajax',
            data: {
                class: "paymentplan",
                action: "savepayordata",
                'formdata':formarray,
                'dates':dates
            },
            success: function (response) {
                var res=JSON.parse(response);
                localStorage.setItem("Message", res.message);
                localStorage.setItem("rowcolorTenant", "rowcolorTenant");
                window.location.href=window.location.origin+'/MultiPay/PaymentPlan';
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value){
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    function paymentdetails(id) {
        $.ajax({
            type: 'post',
            url: '/paymentplan-ajax',
            data: {
                class: "paymentplan",
                action: "getpaymentdetails",
                'id':id
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
              var count=[];
              var html="";
              var check="";
              var dis="";
              var nam="";
              var amtt=[];
              var extra_amt=[];
              var insert_in="";
              var amt_insert="";
              var tr_class="";
              for(var i=0;i<res.count;i++){
               amtt.push(res.data[i].amount_paid);
               extra_amt.push(res.data[i].extra_amount);
               if (res.data[i].is_paid == "Yes") {
                   check="checked";
                   dis="disabled";
                   insert_in="";
                   nam="";
                   amt_insert="";
                   tr_class="";
                 }
               else{
                   count.push(1);
                   dis="";
                   check="";
                   insert_in="insert_in";
                   nam="is_checkk";
                   amt_insert="amt_insert";
                   tr_class="tr_class";
               }
               var startDate=new Date(res.data[i].dates);
               var week=(startDate.getDay());
               week=(days[week]).substr(0,3);
               startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+week+'.'+')';
               html +="  <tr class='"+tr_class+"'>" +
               "                                                    <td id="+res.data[i].id+"><input data-no="+i+" data-id="+res.data[i].id+" name='"+nam+"' type='checkbox' "+check+" "+dis+" class='"+insert_in+"'></td>" +
               "                                                    <td>"+(i+1)+"</td>" +
               "                                                    <td>"+startDate+"</td>" +
               "                                                    <td class='start_amt'>"+res.data[i].amount_paid+"</td>" +
               "                                                    <td>"+res.data[i].is_paid+"</td>" +
               "                                                    <td class='extra_amt'>"+res.data[i].extra_amount+"</td>" +
               "                                                    <td class='"+amt_insert+"'>"+res.data[i].amount_due+"</td>" +
               "                                                </tr>";

                  }
           $("#payment_list").html(' ').append(html);
          // $("#all_amt").val(JSON.stringify(amtt));
         //  $("#extra_amt").val(JSON.stringify(extra_amt));
                console.log('count',count);
                $(document).on('click','.insert_in',function () {
                    //$("input[name=is_checkk]").("input:checkbox:checked");
                    // $(".insert_in input:checkbox:checked");
                    if($('.insert_in').is(":checked")){
                        var $box = $(this);
                        if ($box.is(":checked")) {
                            var group = "input:checkbox[name='" + $box.attr("name") + "']";
                            $(group).prop("checked", false);
                            $box.prop("checked", true);
                        } else {
                            $box.prop("checked", false);
                        }
                        var ext2=0;
                         ext2=(Math.round(ext2 * 100) / 100).toFixed(2);
                      //  $('.insert_in').closest("tr").find('.start_amt').html('');
                      //  $('.insert_in').closest("tr").find('.extra_amt').html('');
                        var no= $(this).attr('data-no');
                        $(".st_amtt").remove();
                        $(this).closest('tr').nextAll('tr.tr_class').find('td.start_amt').text(res.data[no].amount_paid);
                        $(this).closest('tr').nextAll('tr.tr_class').find('td.extra_amt').text(ext2);
                      //  $(".start_amt").text(res.data[no].amount_paid);
                        $(".ext_amtt").remove();
                     //   $(".start_amt").text(res.data[no].extra_amount);
                        $(this).closest("tr").find('td.start_amt').html(' ').append(' <input class="form-control add-input st_amtt" name="start_amt" type="text">');
                        $(this).closest("tr").find('td.extra_amt').html(' ').append(' <input class="form-control add-input ext_amtt" name="extra_amt" type="text">');
                        $("input[name=start_amt]").val(res.data[no].amount_paid);
                        $("input[name=extra_amt]").val(res.data[no].extra_amount);
                        $("input[name=start_amt]").focus();
                    }else if($('.insert_in').prop('checked')==false){
                        $('.insert_in').closest("tr").find('.start_amt').html('');
                        $('.insert_in').closest("tr").find('.extra_amt').html('');
                    }
                });

                $(document).on('focusout','.st_amtt',function () {
                    console.log(res.data[0].amount_due);
                var val=$('.st_amtt').val();
                   var ext1=(Math.round(val * 100) / 100).toFixed(2);
                $("#list_amt").val(parseFloat(ext1));
                console.log(val);
                    var ext=parseFloat(val)-parseFloat(res.data[0].amount_paid);
                    ext=(Math.round(ext * 100) / 100).toFixed(2);
                if(parseFloat(val) > parseFloat(res.data[0].amount_paid) && parseFloat(val) < parseFloat(res.data[0].amount_due)){
                        $("input[name=start_amt]").val(res.data[0].amount_paid);
                    $("input[name=extra_amt]").focus();
                    $("input[name=extra_amt]").val(ext);
                }
                  else if(parseFloat(val) == parseFloat(res.data[0].amount_paid) && parseFloat(val) < parseFloat(res.data[0].amount_paid)){
                        $("input[name=start_amt]").val(val);
                        $("input[name=extra_amt]").focus();
                        $("input[name=extra_amt]").val(ext);
                    }
                   else if(parseFloat(val) < parseFloat(res.data[0].amount_paid) && parseFloat(val) < parseFloat(res.data[0].amount_due)){
                        $("input[name=start_amt]").val(val);
                    $("input[name=extra_amt]").focus();
                    $("input[name=extra_amt]").val(ext);
                    }
                    else if(parseFloat(val) > parseFloat(res.data[0].amount_due) && parseFloat(val) > parseFloat(res.data[0].amount_due)){
                        toastr.error("Amount Must be Less than Balance Amount");
                        $("input[name=start_amt]").val(ext);
                        $("input[name=extra_amt]").val(ext);
                    }
                    $("input[name=extra_amt]").focus();
                });

                $(document).on('focusout','.ext_amtt',function () {
                    var val=$('.st_amtt').val();
                    if(val !== undefined){
                        val=val;
                    }
                    else if(val == undefined){
                        val=$("tr").find('td.start_amt').eq(3).text();
                    }
                    var val1=$('.ext_amtt').val();
                    console.log(val1);
                    console.log(val);
                    var val2 = res.data[0].amount_due;
                    if(parseFloat(val) < parseFloat(val2)){
                    console.log(val2);
                    var val3=parseFloat(val2)-parseFloat(val)-parseFloat(val1);
                    val3=(Math.round(val3 * 100) / 100).toFixed(2);
                  //  $('.ext_amtt').closest("td").next("td").text(val3)
                    $("tr").find('td.amt_insert').text(val3);
                    var len = count.length-1;
                    var val4 = parseFloat(val3)/parseInt(len);
                    val4 = (Math.round(val4 * 100) / 100).toFixed(2);
                    $("tr").find('td.start_amt').text(val4);
                    $("#all_amt").val(val3);
                    $("#inst_amt").val(val4);
                    }
                    else if(parseFloat(val) == parseFloat(val2)){
                    console.log(val2);
                    var val3=val;
                    val3=(Math.round(val3 * 100) / 100).toFixed(2);
                    var ext1=0;
                        ext1=(Math.round(ext1 * 100) / 100).toFixed(2);
                  //  $('.ext_amtt').closest("td").next("td").text(val3)
                    $("tr").find('td.amt_insert').text(ext1);
                    var len = count.length-1;
                    // var val4 = parseFloat(val3)/parseInt(len);
                    // val4 = (Math.round(val4 * 100) / 100).toFixed(2);
                    $("tr").find('td.start_amt').eq(3).text(val3);
                    $('.st_amtt').closest('tr').nextAll('tr.tr_class').find('td.start_amt').text(ext1);
                    $("#all_amt").val(ext1);
                    $("#inst_amt").val(0);
                    }
                });
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value){
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    function getdatafordelete(id) {
        $.ajax({
            type: 'post',
            url: '/paymentplan-ajax',
            data: {
                class: "paymentplan",
                action: "getpaymentdetails",
                'id':id
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
                if(res.data1>0){
                    toastr.error("This record cannot be deleted because it has existing transactions")
                }
                   else if(res.data1==0){
                   var id=res.data[0].payment_id;
                   console.log(id);
                    $.ajax({
                        type: 'post',
                        url: '/paymentplan-ajax',
                        data: {
                            class: "paymentplan",
                            action: "deleteplan",
                            'id':id
                        },
                        success: function (response) {
                            var res = JSON.parse(response);
                            bootbox.confirm({
                                message: "Do you want to cancel this action now?",
                                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                                callback: function (result) {
                                    if (result == true) {
                                        toastr.success(res.message);
                                        $('#payment_plan').trigger('reloadGrid');
                                    }
                                }
                            });
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // alert(key+value);
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                }
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value){
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }




    $("#save_installment").click(function () {
       var id="";
        var final_amt="";
        var inst_amt="";
        $('.insert_in').each(function() {
            if ($(this).is(':checked')) {
           id= $(this).attr('data-id');
               final_amt = $("#all_amt").val();
               inst_amt = $("#inst_amt").val();
            console.log(id);
            }
        });
        update_installment(id,final_amt,inst_amt);
    });
    function update_installment(id,final_amt,inst_amt) {
       var formdata=$('#inst_submit').serializeArray();
       var list_amt=$("#list_amt").val();
       console.log('fd',formdata);
        $.ajax({
            type: 'post',
            url: '/paymentplan-ajax',
            data: {
                class: "paymentplan",
                action: "updatepaydetails",
                'id':id,
                'formdata':formdata,
                'inst_amt':inst_amt,
                'final_amt':final_amt,
                'list_amt':list_amt
            },
            success: function (response) {
                var res=JSON.parse(response);
                localStorage.setItem("Message", res.message);
                window.location.href=window.location.origin+'/MultiPay/PaymentPlan';
                setTimeout(function () {
                    jQuery('#payment_plan').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#payment_plan').find('tr:eq(1)').find('td:eq(15)').addClass("green_row_right");
                },1500);
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value){
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function getdataforedit(id) {
        $.ajax({
            type: 'post',
            url: '/paymentplan-ajax',
            data: {
                class: "paymentplan",
                action: "getpaymentdetails",
                'id':id
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
                if(res.data1>0){
                    toastr.error("This record cannot be deleted because it has existing transactions")
                }
                else if(res.data1==0){
                    $('#plan_listing').hide();
                    $("#update_plan").show();
                    $.each(res.data[0],function(k,v){
                        $("#"+k).val(v);
                    });
                    $(".principal_amount").val(res.data[0].principal_amount);
                    $(".interest").val(res.data[0].interest);
                   // $(".pre-span-text").show();
                    $(".next_payment_amount").val(res.data[0].next_payment_amount);
                    var startDate=new Date(res.data[0].start_date);
                    var week=(startDate.getDay());
                    week=(days[week]).substr(0,3);
                    startDate= (startDate.getMonth()+1)+'/'+startDate.getDate()+'/'+startDate.getFullYear()+' '+'('+week+'.'+')';
                    $("#start_date").val(startDate);
                    var pyname="";
                    var weeklyy="";
                    if(res.data[0].payor_type=="Tenant"){
                      pyname=1;
                    }else if(res.data[0].payor_type=="Owner"){
                      pyname=2;
                    }else if(res.data[0].payor_type=="Vendor"){
                      pyname=3;
                    }else if(res.data[0].payor_type=="Employee"){
                      pyname=4;
                    }else if(res.data[0].payor_type=="Other"){
                      pyname=5;
                    }
                    $(".payor_typee").val(pyname).trigger('change');
                    setTimeout(function () {
                        $(".payor_namee").val(res.data[0].user_id);
                    },50);

                    if(pyname==5){
                    setTimeout(function () {
                       $(".payor_name").val(res.data[0].other_name);
                    },50);
                    }
                    if(res.data[0].frequency=="Weekly"){
                        weeklyy=1;
                    }else if(res.data[0].frequency=="Bi-Weekly"){
                        weeklyy=2;
                    }else if(res.data[0].frequency=="Monthly"){
                        weeklyy=3;
                    }else if(res.data[0].frequency=="Annually"){
                        weeklyy=4;
                    }
                    $("#frequency").val(weeklyy).trigger('change');
                    console.log(res.data[0]);
                    var id=res.data[0].payment_id;
                    console.log(id);
                    defaultFormData=$('#payment_plan_submit_edit').serializeArray();
                    $(".update_geninfoo").click(function () {
                        if($("#payment_plan_submit_edit").valid()){
                           edit();
                        }
                    });


                    function edit(){
                        var dates=new Array();
                        if(JSON.parse($("#all_dates").val()) !="" || JSON.parse($("#all_dates").val()) !== undefined) dates=JSON.parse($("#all_dates").val());
                     var formarray1=$('#payment_plan_submit_edit').serializeArray();
                    $.ajax({
                        type: 'post',
                        url: '/paymentplan-ajax',
                        data: {
                            class: "paymentplan",
                            action: "updatepayorinfo",
                            'id':id,
                            'formdata':formarray1,
                            'dates':dates
                        },
                        success: function (response) {
                            var res = JSON.parse(response);
                            localStorage.setItem("Message", res.message);
                            localStorage.setItem("rowcolorTenant", "rowcolorTenant");
                            window.location.href=window.location.origin+'/MultiPay/PaymentPlan';
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    }
                }
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value){
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function paymentreceiptdetails(id) {
        $.ajax({
            type: 'post',
            url: '/paymentplan-ajax',
            data: {
                class: "paymentplan",
                action: "getpaymentdetailsreceipt",
                'id':id
            },
            success: function (response) {
                var res=JSON.parse(response);
                console.log(res);
                var dd=new Date();
                var week=(dd.getDay());
                week=(days[week]).substr(0,3);
                dd= (dd.getMonth()+1)+'/'+dd.getDate()+'/'+dd.getFullYear()+' '+'('+week+'.'+')';
                $("#payment_receipt").html(' ');
                if(res.count>0){
                    var html2="";
                    $("#receipt-pop").modal('show');
                    var int_amt=(parseFloat(res.data[0].principal_amount)*parseFloat(res.data[0].interest))/100;
                    int_amt=(Math.round(int_amt * 100) / 100).toFixed(2);
                    console.log('cc',res.count);
                for(var i=0;i<res.count;i++){
                    html2 +="  <tr>" +
                        "                                                    <td>"+(i+1)+"</td>" +
                        "                                                    <td>"+res.dates[i]+"</td>" +
                        "                                                    <td >"+res.data[i].amount_paid+"</td>" +
                        "                                                    <td>"+res.data[i].frequency+"</td>" +
                        "                                                    <td >"+res.data[i].extra_amount+"</td>" +
                        "                                                    <td >"+int_amt+"</td>" +
                        "                                                </tr>";
                }
                    html2 +="<tr>" +
                         "                                                    <td colspan=\"5\" align=\"center\">Total Amount Received ("+currencySign+")</td>" +
                        "                                                    <td >"+res.sum+"</td>" +
                        "                                                </tr>";

                var html3="";
                html3="<label>Name: "+res.data2.name+"</label>\n" +
                    "              <label>Payment Receipt</label>\n" +
                    "              <label>Date: "+dd+"</label>";
                console.log(html2);
                $("#payment_receipt").append(html2);
                $("#name_dis").html(' ').append(html3);
                }
                else {
                    toastr.error('No Payment has made yet');
                }
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value){
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


});
