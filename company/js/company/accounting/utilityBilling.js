
$(document).ready(function () {
    $(document).on('click','#add_utility_billing_btn', function () {
        $('.utility_tbody').html('');
        addNewUtilityBilling();
        $('#add_utility_billing_div').show();
        $('#list_utility_billing_div').hide();
    });

    $(document).on('click','#cancel_add_utility_btn, #cancel_edit_utility_btn', function () {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $('#add_utility_billing_div').hide();
                $('#edit_utility_billing_div').hide();
                $('#list_utility_billing_div').show();
                $('#utility_bills_listing').jqGrid('GridUnload');
                jqGrid('All','');
                $('#filterDate').val('');
            }
        });
    });

    $(document).on('click','.back_button', function () {
        $('#add_utility_billing_div').hide();
        $('#list_utility_billing_div').show();
        $('#utility_bills_listing').jqGrid('GridUnload');
        jqGrid('All','');
        $('#filterDate').val('');
    });

    function addNewUtilityBilling( ) {
        $.ajax({
            type: 'post',
            url: '/UtilityBilling-Ajax',
            data: {
                class: "utilityBilling",
                action: "getInitialData"
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    var propertyOption = "<option value=''>Select</option>";
                    if (data.data.propertyList.length > 0){
                        $.each(data.data.propertyList, function (key, value) {
                            propertyOption += "<option value='"+value.id+"' data-value='"+value.property_name+"'>"+value.property_name+"</option>";
                        });
                    }
                    var utility_options  =  '<option value="">Select</option>' +
                        '<option value="A/C">A/C</option>' +
                        '<option value="Cable TV">Cable TV</option>' +
                        '<option value="Electricity">Electricity</option>' +
                        '<option value="Gas">Gas</option>' +
                        '<option value="Heating Oil">Heating Oil</option>' +
                        '<option value="Internet/WI-FI">Internet/WI-FI</option>' +
                        '<option value="Sewer">Sewer</option>' +
                        '<option value="Telephone">Telephone</option>' +
                        '<option value="Waste Disposal">Waste Disposal</option>' +
                        '<option value="Water">Water</option>';

                    var html ='<tr id="utility_billing_row_id" class="utility_billing_row">\n' +
                        '<td>' +
                        '<select data_required="true" class="custom_property_validation form-control property_id" id="property_id" name="property_id[]">'+propertyOption+'</select>' +
                        '<span class="customError required"></span>' +
                        '</td>\n' +
                        '<td>' +
                        '<select data_required="true" class="custom_building_validation form-control building_id" id="building_id" name="building_id[]"><option value="">Select</option></select>' +
                        '<span class="customError required"></span>' +
                        '</td>\n' +
                        '<td>' +
                        '<select data_required="true" class="custom_unit_validation form-control unit_id" id="unit_id" name="unit_id[]"><option value="">Select</option></select>' +
                        '<span class="customError required"></span>' +
                        '</td>\n' +
                        '<td>' +
                        '<input  data_required="true" class="custom_tenant_validation form-control tenant_id" readonly id="tenant_id" name="tenant_id[]" placeholder="" type="text">' +
                        '<input id="user_id" class="user_id" name="user_id[]" type="hidden">' +
                        '<span class="customError required"></span>' +
                        '</td>\n' +
                        '<td>' +
                        '<select data_required="true" class="custom_utility_validation form-control utility" id="utility" name="utility[]">'+utility_options+'</select>' +
                        '<span class="customError required"></span>' +
                        '</td>\n' +
                        '<td>' +
                        '<input  data_required="true" class="custom_amount_validation form-control add-input utility_amount" id="utility_amount" name="utility_amount[]" autocomplete="off" placeholder="" type="text"><a class="add-icon remove_utility_row"  style="display: none;"><i class="fa fa-times-circle red-star" aria-hidden="true"></i></a>' +
                        '<span class="customError required"></span>' +
                        '</td>\n' +
                        '</tr>';

                    $('.utility_tbody').append(html);

                    new AutoNumeric('.utility_amount', {
                        allowDecimalPadding: true,
                        maximumValue  : '9999999999',
                    });

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
        });
    }

    //create utility billing validations
    $(document).on('keyup','.custom_property_validation',function(){ validations(this); });
    $(document).on('keyup','.custom_building_validation',function(){ validations(this); });
    $(document).on('keyup','.custom_unit_validation',function(){ validations(this); });
    $(document).on('keyup','.custom_tenant_validation',function(){ validations(this); });
    $(document).on('keyup','.custom_utility_validation',function(){ validations(this); });
    $(document).on('keyup','.custom_amount_validation',function(){ validations(this); });


    $("#apply_btn").on('click',function(event) {
        event.preventDefault();
        var propertyRes  = true;
        var buildingRes  = true;
        var unitRes  = true;
        var tenantRes  = true;
        var utilityTypeRes  = true;
        var utilityAmountRes = true;

        $(".custom_property_validation").each(function () { propertyRes = validations(this); });
        $(".custom_building_validation").each(function () { buildingRes = validations(this); });
        $(".custom_unit_validation").each(function () { unitRes = validations(this); });
        $(".custom_tenant_validation").each(function () { tenantRes = validations(this); });
        $(".custom_utility_validation").each(function () { utilityTypeRes = validations(this); });
        $(".custom_amount_validation").each(function () { utilityAmountRes = validations(this); });

        if (propertyRes === false || buildingRes === false || unitRes === false || tenantRes === false || utilityTypeRes === false || utilityAmountRes === false) {
            // when form not valid
            return false;
        } else {
            confirm_utility_bill_listingjqGrid(1);

            var table_id = $(this).attr('data_tab');
            if(table_id !== undefined && table_id != '') {
                setTimeout(function () {
                    var bodyWidht = $("#utility_bills_confirm_modal .panel-body").width();
                    var $grid = $("#utility_bills_confirm_modal .panel-body #" + table_id).setGridWidth(bodyWidht );
                },200);
            }

            $('#utility_bills_confirm_modal').modal({ backdrop: 'static', keyboard: false });
            $('#utility_bills_confirm_modal').modal('show');
        }
    });

    $("#confirm_btn").on('click',function(event) {
        confirmUtilityBills();
    });
    function confirmUtilityBills() {
        var form = $('#apply_utility_bill_form_id')[0];
        var formData = new FormData(form);

        formData.append('action', 'addUtilityBills');
        formData.append('class', 'utilityBilling');
        // formData.append('tenant_id',to);

        $.ajax({
            url: '/UtilityBilling-Ajax',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response)
                if (response.status == 'success' && response.code == 200) {
                    $('#utility_bills_confirm_modal').modal('hide');
                    toastr.success('The record saved successfully.');
                    $('#add_utility_billing_div').hide();
                    $('#list_utility_billing_div').show();
                    $('#filterDate').val('');

                    $('#utility_bills_listing').jqGrid('GridUnload');

                    jqGrid('All','','1');
                    setTimeout(function () {
                        jQuery('#utility_bills_listing').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#utility_bills_listing').find('tr:eq(1)').find('td:eq(8)').addClass("green_row_right");
                    }, 300);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("change",".property_id",function (e) {
        // console.log('aaaa', $(this).val());
        e.preventDefault();

        var html = "<option value=''>Select</option>";
        $(this).parent().next().find("select").html(html);
        $(this).parent().next().next().find("select").html(html);
        $(this).parent().next().next().next().find("input").val('');
        $(this).parent().next().next().next().find("input").attr('tenant_id','');

        if($(this).val() != '') {
            var returnElement = getDataByID($(this).val(), '', 'property');
            if (returnElement != undefined && returnElement != ""){
                $(this).parent().next().find("select").html(returnElement);
            }
        }
        return false;
    });

    $(document).on("change",".building_id",function (e) {
        e.preventDefault();
        var html = "<option value=''>Select</option>";
        $(this).parent().next().find("select").html(html);
        $(this).parent().next().next().find("input").val('');
        $(this).parent().next().next().find("input").attr('tenant_id','');

        var returnElement = getDataByID($(this).val(),$('.property_id').val(),'building');
        if (returnElement != undefined && returnElement != ""){
            $(this).parent().next().find("select").html(returnElement);
        }
        return false;
    });

    $(document).on("change",".unit_id",function (e) {
        e.preventDefault();
        $(this).parent().next().find("input").val('');
        $(this).parent().next().find("input").attr('tenant_id','');

        var returnElement = getDataByID($(this).val(),$('.property_id').val(),'unit');
        if (returnElement != undefined && returnElement != ""){
            $(this).parent().next().find("input:first").val(returnElement.name);
            $(this).parent().next().find("input:first").attr('tenant_id',returnElement.id);
            $(this).parent().next().find('.user_id').val(returnElement.id);
        }
        return false;
    });

    $(document).on("click",".add_new_utility_row",function(){
        var clone = $("#utility_billing_row_id:first").clone(false).insertAfter(".utility_billing_row:last");
        clone.find('input[type=text]').val('');
        clone.find('span.customError').text('');

        var rowLength = $(".utility_billing_row").length;
        clone.find('input[name="utility_amount[]"]').attr('id', 'utility_amount'+rowLength);
        var id = 'utility_amount'+rowLength;
        // setTimeout(function () {
        new AutoNumeric('#'+id).remove();
        new AutoNumeric('#'+id, {
            allowDecimalPadding: true,
            maximumValue  : '9999999999',
        });
        $(".utility_billing_row:not(:eq(0)) .remove_utility_row").css('display', 'block');
        // }, 500);
    });

    $(document).on("click",".remove_utility_row",function(){
        $(this).parents(".utility_billing_row").remove();
    });

    function getDataByID(data_id, prev_id, data_type){
        if (data_id == '' || data_id == 'null'){
            return;
        }
        var returnElement = '';
        $.ajax({
            type: 'post',
            url: '/UtilityBilling-Ajax',
            async: false,
            data: {
                class: "utilityBilling",
                action: "getDataByID",
                id: data_id,
                prev_id: prev_id,
                data_type : data_type
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if(data_type == 'property') {
                        var buildingOption = "<option value=''>Select</option>";
                        if (data.data.length > 0) {
                            $.each(data.data, function (key, value) {
                                buildingOption += "<option value='" + value.id + "'>" + value.building_name + "</option>";
                            });
                        }
                        returnElement = buildingOption;
                    } else if(data_type == 'building') {
                        var buildingOption = "<option value=''>Select</option>";
                        if (data.data.length > 0) {
                            $.each(data.data, function (key, value) {
                                var unit_prefix = (value.unit_prefix !='') ? value.unit_prefix+" - " : '';
                                buildingOption += "<option value='" + value.id + "'>" + unit_prefix + value.unit_no +"</option>";
                            });
                        }
                        returnElement = buildingOption;
                    } else if(data_type == 'unit'){
                        if (data.data.length > 0){
                            returnElement = data.data[0];
                        }
                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        return returnElement;
    }

    function createTableDataObj(){
        var thvalues = $('#added_utility_bills_table thead th').map(function(){
            return $(this).text();
        });
        var rows = $('#added_utility_bills_table tbody tr').map(function(){
            var result = {};
            var values = $(this).find('td select, td input').map(function(){
                if ($(this).is("select")){
                    return $(this).find(":selected").text();
                } else {
                    if ($(this).attr('id') == 'user_id'){
                        return;
                    } else if ($(this).attr('name') == 'utility_amount[]'){
                        return '$'+$(this).val();
                    } else {
                        return $(this).val();
                    }
                }
            });

            for(var i=0;i<thvalues.length;i++){
                result[thvalues[i]] = values[i];
            }
            return result
        }).toArray();
        return rows;
    }

    function confirm_utility_bill_listingjqGrid(status) {
        $('#confirm_utility_bill_listing').jqGrid('GridUnload');
        var table_rows = createTableDataObj();
        console.log('table_rows>>',table_rows);

        // var table = 'general_property';
        var columns = ['Property','Building','Unit','Tenant','Utility','Amount('+default_currency_symbol+')'];
        var conditions = ["eq","bw","ew","cn","in"];
        var columns_options = [
            {label:'Property', name:'Property',width:400,align:"left",searchoptions: {sopt: conditions}, formatter:propertyName},
            {label:'Building', name:'Building',width:400,align:"left",searchoptions: {sopt: conditions}},
            {label:'Unit', name:'Unit',width:400,align:"left",searchoptions: {sopt: conditions}},
            {label:'Tenant', name:'Tenant',width:400,align:"left",searchoptions: {sopt: conditions}, formatter:tenantName},
            {label:'Utility', name:'Utility',width:400,align:"left",searchoptions: {sopt: conditions}},
            {label:'Amount('+default_currency_symbol+')', name:'Amount ('+default_currency_symbol+')',width:400,align:"left",searchoptions: {sopt: conditions}},
        ];
        var ignore_array = [];
        jQuery("#confirm_utility_bill_listing").jqGrid({
            datatype: 'local',
            data:table_rows,
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            sortname: 'updated_at',
            mtype: "POST",
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            caption: "List of Utility Charges",
            pgbuttons : true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            viewrecords: true,
            navOptions: {
                edit:false,
                add:false,
                del:false,
                search:true,
                filterable: false,
                refreshtext: "Refresh"
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,refresh:true
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:400,drag:true,resize:false} // search options
        );
    }


    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('ALL', '','0');
    function jqGrid(status, filter_date,upRecord) {
        if(upRecord=="1"){
            var sortOrder = 'desc';
            var sortColumn = 'utility_billing.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'general_property.property_name';
        }
        var table = 'utility_billing';
        var columns = ['Property Name','Building Name','Unit Name','Unit Number', 'Tenant', 'Utility','Amount('+default_currency_symbol+')','Payment Status','Action'];
        var select_column = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','DeActivate Account','Print Envelope','Delete Owner'];
        // var joins = [{table:'users',column:'id',primary:'user_id',on_table:'owner_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var joins = [{table:'utility_billing',column:'user_id',primary:'id',on_table:'users'},{table:'utility_billing',column:'user_id',primary:'user_id',on_table:'tenant_property'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'building_id',primary:'id',on_table:'building_detail'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        // var extra_where = [{column:'deleted_at',value:'null',condition:'=',table:'utility_billing'}];
        var extra_where = [];
        if(filter_date != '') {
            extra_where = [{column: 'end_date', value: filter_date, condition: '<='}];
        }
        var columns_options = [
            {name:'Property Name',index:'property_name',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:'general_property',classes: 'pointer', formatter:propertyName},
            // {name:'Owner Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:table, classes:'pointer'},
            {name:'Building Name',index:'building_name', width:90,align:"center",searchoptions: {sopt: conditions},table:'building_detail', classes: 'cursor'},
            // {name:'Phone',index:'phone_number', width:90,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'phone_number_format',formatter:addToolTip, classes: 'cursor',cellattr:cellAttri},
            // {name: 'Unit Name', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table:'unit_details', classes: 'cursor'}, /**cellattr:cellAttrdata**/
            {name:'Unit Name',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details', formatter:unitNameFormatter},
            {name:'Unit_Number',index:'unit_no', hidden: true, width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Tenant',index:'name', width:80, align:"center",searchoptions: {sopt: conditions},table:'users',classes: 'pointer', formatter:tenantName},
            {name:'Utility',index:'utility_type', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor'},
            {name:'Amount('+default_currency_symbol+')',index:'utility_amount', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor', formatter:currencyFormatter},
            {name:'Payment Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter, classes: 'cursor'},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select',search:false,table:table,formatter:actionFormatter},

        ];
        var ignore_array = [];

        jQuery("#utility_bills_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "utility_billing",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Utility Charges",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }

    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

    function tenantName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }
    function currencyFormatter (cellValue, options, rowObject){
        if(cellValue!==undefined && cellValue!==""){
            return default_currency_symbol+''+cellValue;
        } else {
            return "";
        }
    }
    function unitNameFormatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            if(rowObject.Unit_Number != '' && rowObject.Unit_Number != 'null') {
                var unit_prefix = (cellvalue !='') ? cellvalue+" - " : '';
                return unit_prefix+rowObject.Unit_Number;
            }else {
                return cellvalue;
            }
        }
    }
    function actionFormatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {

            var select = '';
            var $owners = "Owner's Portal";

            select = ['Edit','Delete'];

            var data = '';

            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    function statusFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            if (cellValue == 'Pending')
                return '<span class="redText">'+cellValue+'</span>';
            else
                return 'Paid';
        }
    }

    $(document).on("change", "#utility_bills_listing .select_options", function (e) {
        e.preventDefault();
        setTimeout(function(){ $(".select_options").val("default"); }, 200);
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var elem = $(this);
        switch (action) {
            case "Edit":
                getDataById(id);
                $('#edit_utility_billing_div').show();
                $('#add_utility_billing_div').hide();
                $('#list_utility_billing_div').hide();
                break;
            case "Delete":
                bootbox.confirm("Do you want to delete this record?", function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/UtilityBilling-Ajax',
                            data: {
                                class: 'utilityBilling',
                                action: 'delete',
                                id:id
                            },
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    $('#utility_bills_listing').trigger( 'reloadGrid' );
                                    toastr.success('The record deleted successfully.');
                                }else {
                                    toastr.warning('Not redirecting due to technical issue.');
                                }
                            }
                        });
                    }
                });
                break;
            default:
                break;
        }
    });

    var td= new Date();
    var maxDate = new Date(td.getFullYear(), td.getMonth() + 2, 0);

    function getDataById(id){
        $.ajax({
            type: 'post',
            url: '/UtilityBilling-Ajax',
            data: {
                class: 'utilityBilling',
                action: 'getDataByIdUtility',
                id:id
            },
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    $('.view_data').show();
                    $('#edit_utility_bill_form_id input[type = "text"]').hide();

                    $('#edit_utility_type').text(response.data.utility_type);
                    $('#edit_frequency').text('Monthly');
                    $('#edit_start_date').text(response.data.start_date);
                    $('#edit_end_date').text(response.data.end_date);
                    $('#edit_amount').text(response.data.utility_amount);
                    $('#utility_bill_id').val(response.data.id);

                    $('#start_date').datepicker({yearRange: '-0:+1', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, maxDate: response.data.end_date})
                        .datepicker("setDate", response.data.start_date);

                    $('#end_date').datepicker({yearRange: '-0:+1', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, minDate: response.data.start_date ,maxDate: response.data.end_date })
                        .datepicker("setDate", response.data.end_date);

                    $('#edit_utility_amount').val(response.data.utility_amount);
                    new AutoNumeric('#edit_utility_amount', {
                        allowDecimalPadding: true,
                        maximumValue  : '9999999999',
                    });
                }else {
                    toastr.warning('Not redirecting due to technical issue.');
                }
            }
        });
    }

    $(document).on('click', '.edit_data', function () {
        $(this).parent().hide();
        $(this).parent().next().show();
    });

    // console.log('maxDate>>>',maxDate);

    $(document).on("change", "#start_date", function () {
        var value = $(this).val();
        if (value > ($('#end_date').val())){
            $('#end_date').datepicker({dateFormat: jsDateFomat}).datepicker("setDate", value);
        }
        $("#end_date").datepicker({minDate: value,maxDate: maxDate });
    });

    $("#update_btn").on('click',function(event) {
        editUtilityBills();
    });
    function editUtilityBills() {
        var editForm = $('#edit_utility_bill_form_id')[0];
        var editFormData = new FormData(editForm);

        editFormData.append('action', 'editUtilityBills');
        editFormData.append('class', 'utilityBilling');
        // formData.append('tenant_id',to);

        $.ajax({
            url: '/UtilityBilling-Ajax',
            type: 'POST',
            data: editFormData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response)
                if (response.status == 'success' && response.code == 200) {
                    // $('#utility_bills_confirm_modal').modal('hide');
                    toastr.success('The record updated successfully.');
                    $('#edit_utility_billing_div').hide();
                    $('#list_utility_billing_div').show();
                    $('#filterDate').val('');
                    // $('#utility_bills_listing').trigger( 'reloadGrid' );

                    $('#utility_bills_listing').jqGrid('GridUnload');

                    jqGrid('All','','1');
                    setTimeout(function () {
                        jQuery('#utility_bills_listing').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('#utility_bills_listing').find('tr:eq(1)').find('td:eq(8)').addClass("green_row_right");
                    }, 300);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $('#filterDate').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat});

    $(document).on('change', '#filterDate', function () {
        var filterDate = $(this).val();
        $('#utility_bills_listing').jqGrid('GridUnload');
        jqGrid('All',filterDate);
    });

    $(document).on('click', '#clear_add_utility_form', function () {
        bootbox.confirm("Do you want to Clear this form?", function (result) {
            if (result == true) {
                $('.utility_tbody').html('');
                addNewUtilityBilling();
                $('#add_utility_billing_div').show();
                $('#list_utility_billing_div').hide();
            }
        });
    });

    $(document).on('click', '#clear_edit_utility_btn', function () {
        bootbox.confirm("Do you want to Clear this form?", function (result) {
            if (result == true) {
                var utility_bill_id = $('#utility_bill_id').val();
                getDataById(utility_bill_id);
                $('#edit_utility_billing_div').show();
                $('#add_utility_billing_div').hide();
                $('#list_utility_billing_div').hide();
            }
        });
    });

});