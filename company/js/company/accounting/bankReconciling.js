$(document).ready(function () {

    $("input").removeClass("capital");
    $(document).on('click', '#reconcile_now_btn', function () {
        setTimeout(function () {
            $("#start_reconcile_form_id")[0].reset();
        },100);

        getAllAccountDDl();
        $('#start_reconcile_modal').modal({
            show: true,
            backdrop: 'static',
            keyboard: false,
        })
    });
    $(document).on('click', '#start_reconcile_cancel_btn', function () {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $('#start_reconcile_modal').modal('hide');
                $('#add_new_reconciliation_div').show();
                $('#list_bank_reconciliation_div').hide();
            }
        });
    });
    $(document).on('click', '.adjustment_button', function () {
        $('#money_adjustment_modal').modal({
            show: true,
            backdrop: 'static',
            keyboard: false,
        });

        setTimeout(function () {
            $("#adjustment_form_id").trigger('reset');
            $("#adjustment_form_id label.error").text('');
        },200);


    });

    $(document).on('click', '#cancel_add_reconciliation_btn', function () {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $('#add_new_reconciliation_div').hide();
                $('#list_bank_reconciliation_div').show();
            }
        });
    });
    $(document).on('click', '.add_reconciliation_back_btn', function () {
        $('#add_new_reconciliation_div').hide();
        $('#list_bank_reconciliation_div').show();
    });
    $(document).on('click', '#start_reconciliation_cross_btn', function () {
        $('#start_reconcile_modal').modal('hide');
        $('#add_new_reconciliation_div').show();
        $('#list_bank_reconciliation_div').hide();
    });

    $(document).on('click', '#add_more_bank_account_icon', function () {
        $('#add_bank_account_form')[0].reset();
        $('#add_bank_account_form label.error').text('');
        getAllPortfolioDDl();
        $('#add_more_bank_account_modal').modal({
            show: true,
            backdrop: 'static',
            keyboard: false,
        })
    });

    $(document).on('click', '#add_more_bank_account_icon2', function () {
        $('#add_bank_account_form')[0].reset();
        $('#add_bank_account_form label.error').text('');
        getAllPortfolioDDl();
        $('#add_more_bank_account_modal').modal({
            show: true,
            backdrop: 'static',
            keyboard: false,
        });
        $('#add_bank_account_form').find('#add_bank_account_cancel_btn').removeAttr('id');
        $('#add_bank_account_form').find('.blue-btn').attr('id', 'add_bank_account_save_btn');
        $('#add_bank_account_form').find('.grey-btn').attr('id', 'add_bank_account_cancel_btn2');
    });

    $(document).on('click', '#add_bank_account_cancel_btn2', function () {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $('#add_more_bank_account_modal').modal('hide');
            }
        });
    });



    $(document).on('click', '#add_bank_account_cancel_btn', function () {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $('#start_reconcile_modal').modal({
                    show: false,
                    backdrop: 'static',
                    keyboard: false,
                });
                $('#start_reconcile_modal').modal('show');
                $('#add_more_bank_account_modal').modal('hide');
            }
        });
    });

    function getAllAccountDDl(){
        $.ajax({
            type: 'post',
            url: '/AccountReconcile-Ajax',
            data: {
                class: 'bankReconcile',
                action: 'getAllAccountDDl'
            },
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    var accountOption = "<option value=''>Select</option>";
                    if (response.data.length > 0) {
                        $.each(response.data, function (key, value) {
                            accountOption += "<option value='" + value.id + "' portfolio_id='" + value.portfolio + "'>" + value.bank_name + "</option>";
                        });
                    }
                    $('.bank_account').html(accountOption);
                }
            }
        });
    }

    function getAllPortfolioDDl(){
        $.ajax({
            type: 'post',
            url: '/AccountReconcile-Ajax',
            data: {
                class: 'bankReconcile',
                action: 'getAllPortfolioDDl'
            },
            success : function(response){
                var response =  JSON.parse(response);
                if(response.status == 'success' && response.code == 200) {
                    var portfolioOption = "<option value=''>Select</option>";
                    if (response.data.length > 0) {
                        $.each(response.data, function (key, value) {
                            if(value.is_default == 1){
                                portfolioOption += "<option value='" + value.id + "' selected>" + value.portfolio_name + "</option>";
                            } else {
                                portfolioOption += "<option value='" + value.id + "' >" + value.portfolio_name + "</option>";
                            }
                        });
                    }
                    $('#portfolio').html(portfolioOption);
                }
            }
        });
    }

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('ALL', '');
    function jqGrid(status, filter_date) {
        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'utility_billing.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'general_property.property_name';
        }
        var table = 'utility_billing';
        var columns = ['Property Name','Building Name','Unit Name','Unit Number', 'Tenant', 'Utility','Amount('+default_currency_symbol+')','Payment Status','Action'];
        var select_column = ['Edit','Email','Text','Work Order','Add In-touch','In-Touch History','Owner Draw','Owner Contribution','Owner statement','Owner Notes','File Library','Flag Bank','Email History','Text History','Run Background Check','Owner Portal','Send Password Activation Email','Owner Complaints','Archive Owner','Resign Owner','DeActivate Account','Print Envelope','Delete Owner'];
        // var joins = [{table:'users',column:'id',primary:'user_id',on_table:'owner_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var joins = [{table:'utility_billing',column:'user_id',primary:'id',on_table:'users'},{table:'utility_billing',column:'user_id',primary:'user_id',on_table:'tenant_property'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'building_id',primary:'id',on_table:'building_detail'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        // var extra_where = [{column:'deleted_at',value:'null',condition:'=',table:'utility_billing'}];
        var extra_where = [];
        if(filter_date != '') {
            extra_where = [{column: 'end_date', value: filter_date, condition: '<='}];
        }
        var columns_options = [
            {name:'Property Name',index:'property_name',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:'general_property',classes: 'pointer'},
            // {name:'Owner Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:table, classes:'pointer'},
            {name:'Building Name',index:'building_name', width:90,align:"center",searchoptions: {sopt: conditions},table:'building_detail', classes: 'cursor'},
            // {name:'Phone',index:'phone_number', width:90,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'phone_number_format',formatter:addToolTip, classes: 'cursor',cellattr:cellAttri},
            // {name: 'Unit Name', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table:'unit_details', classes: 'cursor'}, /**cellattr:cellAttrdata**/
            {name:'Unit Name',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details', formatter:unitNameFormatter},
            {name:'Unit_Number',index:'unit_no', hidden: true, width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Tenant',index:'name', width:80, align:"center",searchoptions: {sopt: conditions},table:'users',classes: 'pointer'},
            {name:'Utility',index:'utility_type', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor'},
            {name:'Amount('+default_currency_symbol+')',index:'utility_amount', width:80, align:"center",searchoptions: {sopt: conditions},table:table, classes: 'cursor', formatter:currencyFormatter},
            {name:'Payment Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter, classes: 'cursor'},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select',search:false,table:table,formatter:actionFormatter},

        ];
        var ignore_array = [];

        jQuery("#utility_bills_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "utility_billing",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Utility Charges",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }
    function currencyFormatter (cellValue, options, rowObject){
        if(cellValue!==undefined && cellValue!==""){
            return default_currency_symbol+''+cellValue;
        } else {
            return "";
        }
    }
    function unitNameFormatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            if(rowObject.Unit_Number != '' && rowObject.Unit_Number != 'null') {
                return cellvalue+' - '+rowObject.Unit_Number;
            }else {
                return cellvalue;
            }
        }
    }
    function actionFormatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {

            var select = '';
            var $owners = "Owner's Portal";

            select = ['Edit','Delete'];

            var data = '';

            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    function statusFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            if (cellValue == 'Pending')
                return '<span class="redText">'+cellValue+'</span>';
            else
                return 'Paid';
        }
    }


    /*** Add file library ***/
    $(document).on('click','#add_libraray_file',function(){
        $('#addFileLibrary').val('');
        $('#addFileLibrary').trigger('click');
        // console.log('sasasa');
    });

    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }

    var fileLibrary = [];
    var imgArray = [];
    $(document).on('change','#addFileLibrary',function(){
        // fileLibrary = [];
        $.each(this.files, function (key, value) {
            var type = value['type'];
            console.log('type>>',type);
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            if(size > 4097) {
                toastr.warning('Please select documents less than 4 mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
                if (type == 'image/jpeg' || type == 'image/jpg' || type == 'image/png' || type == 'image/gif' || type == 'application/msword' || type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                    if($.inArray(value['name'], imgArray) === -1)
                    {
                        fileLibrary.push(value);
                    }
                    var src = '';
                    var reader = new FileReader();
                    //  $('#file_library_uploads').html('');
                    reader.onload = function (e) {
                        if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            src = upload_url + 'company/images/excel.png';
                        } else if (type == 'application/pdf') {
                            src = upload_url + 'company/images/pdf.png';
                        } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'application/msword') {
                            src = upload_url + 'company/images/word_doc_icon.jpg';
                        } else if (type == 'text/plain') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else if (type == 'text/xml') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else {
                            src = e.target.result;
                        }
                        if($.inArray(value['name'], imgArray) === -1)
                        {
                            $("#add_file_library_uploads").append(
                                '<div class="row" style="margin:0 20px">' +
                                '<div class="col-sm-12 img-upload-library-div" style="margin: 0">' +
                                '<div class="col-sm-4"><img class="img-upload-tab' + key + '" width=40 height=40 src=' + src + '></div>' +
                                '<div style="margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-name ' + key + '">' + value['name'] + '</div>' +
                                '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                                '<div  style="    margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-size' + key + '">' + size + '</div>' +
                                '</div></div>');
                            imgArray.push(value['name']);
                        } else {
                            toastr.warning('File already exists!');
                        }
                    };
                    reader.readAsDataURL(value);
                } else {
                    toastr.warning('Please select file with .jpg | .png | .jpeg | .gif | .xlsx | .pdf | .docx | .txt | .xml extension only!');
                }
            }
        });
    });


    $(document).on('click','#remove_library_file_add',function(){
        bootbox.confirm("Do you want to remove all files?", function (result) {
            if (result == true) {
                $('#add_file_library_uploads').html('');
                toastr.success('The record deleted successfully.');
                $('#addFileLibrary').val('');
                imgArray = [];
            }
        });
    });
    /*** Add file library ***/

    /*** Add new research adjustment ***/
    $(document).on('click','#add_new_research_btn',function(){
        $('#add_new_research_div').show();
    });

    $(document).on('click','#add_new_research_cancel_btn',function(){
        bootbox.confirm("Do you want to remove all files?", function (result) {
            if (result == true) {
                $('#add_new_research_div').hide();
            }
        });
    });


    // new AutoNumeric('#f1_beg_balance', { allowDecimalPadding: true, maximumValue  : '9999999999'});
    // new AutoNumeric('#f1_end_balance', { allowDecimalPadding: true, maximumValue  : '9999999999'});
    // new AutoNumeric('#f1_service_charge', { allowDecimalPadding: true, maximumValue  : '9999999999'});
    // new AutoNumeric('#f1_interest_earned', { allowDecimalPadding: true, maximumValue  : '9999999999'});

    // const anElement =  AutoNumeric.multiple('.money', null, {
    //     allowDecimalPadding: true,
    //     maximumValue  : '9999999999',
    //     unformatOnSubmit: false
    // });

    // const autoNumericElements = AutoNumeric.multiple('.money', {
    //     allowDecimalPadding: true,
    //     caretPositionOnFocus: "start",
    //     createLocalList: false,
    //     emptyInputBehavior: "null",
    //     maximumValue: "9999999999",
    //     unformatOnSubmit: true
    // });

    $(document).on('keypress','.money',function(e){
        if(this.value.length==10) return false;
    });

    $(document).on('focusout','.money',function(){
        if($(this).val() != '' && $(this).val().indexOf(".") == -1) {
            var bef = $(this).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $(this).val(value);
        } else {
            var bef = $(this).val().replace(/,/g, '');
            $(this).val(numberWithCommas(bef));
        }
    });

    $('.statement_ending_date').datepicker({yearRange: '-100:+1', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, maxDate: new Date()});
    $('.research_date').datepicker({yearRange: '-100:+1', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, maxDate: new Date()});

    /*** Add new research adjustment ***/

    /** Add/Edit new Bank Account */
    $("#add_bank_account_form").validate({
        rules: {
            portfolio: {
                required: true
            },
            bank_name: {
                required: true
            },
            bank_account_number: {
                required: true,
                minlength: 7
            },
            fdi_number: {
                required: true
            },
            branch_code: {
                required: true
            },
            initial_amount: {
                required: true,
                number: true
            },
            last_used_check_number: {
                required: true,
                number: true
            },
            status: {
                required: true
            }
        },
        messages: {
            portfolio: {
                required: "* This field is required",
            },
            bank_account_number: {
                minlength: "* Please enter 7 digit account number",
            }
        },
        submitHandler: function () {
            var portfolio = $('#portfolio').val();
            var bank_name = $('#bank_name').val();
            var bank_account_number = $('#bank_account_number').val();
            var fdi_number = $('#fdi_number').val();
            var branch_code = $('#branch_code').val();
            var initial_amount = $('#initial_amount').val();
            var last_used_check_number = $('#last_used_check_number').val();
            var status = $('#status').val();
            var bank_account_id = $("#bank_account_id").val();
            var is_default;
            if ($('#is_default').is(":checked")) {
                is_default = '1';   // it is checked
            } else {
                is_default = '0';
            }
            var formData = {
                'portfolio': portfolio,
                'bank_name': bank_name,
                'bank_account_number': bank_account_number,
                'fdi_number': fdi_number,
                'branch_code': branch_code,
                'initial_amount': initial_amount,
                'last_used_check_number': last_used_check_number,
                'status': status,
                'is_default': is_default,
                'bank_account_id': bank_account_id
            };

            $.ajax({
                type: 'post',
                url: '/BankAccount-Ajax',
                data: {
                    class: 'BankAccountAjax',
                    action: 'insert',
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        var lastInsertData = '<option value="'+response.lastInsertData.id+'" portfolio_id="'+response.lastInsertData.portfolio+'" selected>'+response.lastInsertData.bank_name+'</option>'

                        $('.bank_account').append(lastInsertData);
                        toastr.success('The record saved successfully.');

                        var save_btn_id = $('#add_bank_account_form').find('.blue-btn').attr('id');
                        if ( save_btn_id != undefined && save_btn_id == 'add_bank_account_save_btn'){
                            $('#add_more_bank_account_modal').modal('hide');
                        } else {
                            $('#start_reconcile_modal').modal({
                                show: true,
                                backdrop: 'static',
                                keyboard: false,
                            });
                            $('#add_more_bank_account_modal').modal('hide');
                        }
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    new AutoNumeric('#initial_amount', {
        allowDecimalPadding: true,
        maximumValue  : '9999999999',
    });
    /** Add/Edit new Bank Account */

    /** Start reconcile modal save */
    $("#start_reconcile_form_id").validate({
        rules: {
            bank_account: {
                required: true
            }
        },
        submitHandler: function () {

            var bank_name = $('#start_reconcile_form_id  .bank_account').val();
            var formData = {
                'bank_name': bank_name
            };

            $.ajax({
                type: 'post',
                url: '/AccountReconcile-Ajax',
                data: {
                    class: 'bankReconcile',
                    action: 'checkPartialReconcileExist',
                    bank_name: bank_name
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.error('Partial Reconcile already exist for this bank. Please complete it.');
                    } else if (response.status == 'warning' && response.code == 503) {



                        $('#add_new_reconciliation_div .bank_account').val($('#start_reconcile_form_id .bank_account').val());
                        $('#add_new_reconciliation_div .statement_ending_date').val($('#start_reconcile_form_id .statement_ending_date').val());
                        $('#add_new_reconciliation_div .beg_balance').val($('#start_reconcile_form_id .beg_balance').val());
                        $('#add_new_reconciliation_div .end_balance').val($('#start_reconcile_form_id .end_balance').val());
                        $('#add_new_reconciliation_div .fee_adjustment').val($('#start_reconcile_form_id .service_charge').val());
                        $('#add_new_reconciliation_div .interest_adjustment').val($('#start_reconcile_form_id .interest_earned').val());
                        var regex = /[,\s]/g;

                        var interest_earned =  $('#start_reconcile_form_id .interest_earned').val();
                        interest_earned = interest_earned.replace(regex, '');
                        var service_charge =  $('#start_reconcile_form_id .service_charge').val();
                        service_charge = service_charge.replace(regex, '');

                        var calculated_system_balance =  interest_earned - service_charge;

                        calculated_system_balance = parseFloat(Number(calculated_system_balance).toFixed(2));
                        calculated_system_balance = numberWithCommas(calculated_system_balance);

                        $('#add_new_reconciliation_div .calculated_system_balance').val(calculated_system_balance);
                        $('#add_new_reconciliation_div .difference').text(calculated_system_balance);

                        $('#start_reconcile_modal').modal('hide');
                        $('#add_new_reconciliation_div').show();
                        $('#list_bank_reconciliation_div').hide();

                    } else {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    $("#adjustment_form_id").validate({
        rules: {
            adjustment_amount: {
                required: true
            }
        },
        submitHandler: function () {

            var adjustment_type = $('#adjustment_form_id  .adjustment_type').val();
            var adjustment_description = $('#adjustment_form_id  .adjustment_description').val();
            var reference_number = $('#adjustment_form_id  .reference_number').val();
            var adjustment_amount = $('#adjustment_form_id  .adjustment_amount').val();

            var formData = {
                'adjustment_type': adjustment_type,
                'adjustment_description': adjustment_description,
                'reference_number': reference_number,
                'adjustment_amount': adjustment_amount
            };


            $.ajax({
                type: 'post',
                url: '/AccountReconcile-Ajax',
                data: {
                    class: 'bankReconcile',
                    action: 'addAdjustmentMoney',
                    formData: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {

                        var html = '<tr>\n' +
                            '<td><input type="checkbox" checked></td>\n' +
                            '<td>'+response.data.date+'</td>\n' +
                            '<td>'+response.data.period+'</td>\n' +
                            '<td>'+response.data.reference_number+'</td>\n' +
                            '<td>'+response.data.type+'</td>\n' +
                            '<td>'+response.data.description+'</td>\n' +
                            '<td>'+adjustment_type+'</td>\n' +
                            '<td>'+response.data.amount+'</td>\n' +
                            '</tr>'

                        if(response.data.type == 'Deposits'){
                            $('.money_in_td_div').append(html);
                        } else {
                            $('.money_out_td_div').append(html);
                        }
                        $('#money_adjustment_modal').modal('hide');

                    } else if (response.status == 'warning' && response.code == 503) {

                    } else {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });

    /** Start reconcile modal save */


    bankFileLibrary();
    function bankFileLibrary(status) {

        var id=$(".owner_id").val();
        var table = 'tenant_chargefiles';
        var columns = ['Name','Preview'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        // var extra_columns = [];
        var extra_where = [{column: 'user_id', value: id, condition: '='}];
        var columns_options = [
            {name: 'Name',index: 'filename',width: 100,align: "center",searchoptions: {sopt: conditions},table: table},
            {name:'Preview',index:'file_extension',width:100,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter}

        ];
        var ignore_array = [];
        jQuery("#file-library").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                //  extra_columns: extra_columns,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Files",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {top: 10, left: 200, drag: true, resize: false}
        );
    }

});
$(document).on('click','.clearAddAccountForm',function () {
    bootbox.confirm("Are you sure you want to clear this form?", function (result) {
        if (result == true) {
            $('#add_bank_account_form')[0].reset();
        }
    });
});
