$(document).ready(function () {

    // $(document).on('click', '#update_process_management_fee', function () {
    //
    // });


    listOwnerContributionJqGrid('');
    function listOwnerContributionJqGrid(selected_owner_id, deleted_at) {
        console.log('selected_owner_id', selected_owner_id);
        var sortOrder = 'asc';
        var sortColumn = 'management_fees.updated_at';
        var table = 'general_property';
        var select_column = '';
        var columns = [ '<label style="text-align: center"><input type="checkbox" id="select_all_properties_checkbox"></label>',
            'Property Name', 'property_id', 'property_for_sale', 'Commission Type', 'Value('+default_currency_symbol+')', 'Minimum'];
        var joins = [{table: 'general_property', column: 'id', primary: 'property_id', on_table: 'management_fees'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [];
        var columns_options = [
            { name: 'id', index: 'id', width: 60, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFormatter},
            { name: 'Property Name', index: 'property_name', width: 60, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer', formatter: propertyName},
            /*hidden fields*/
            // {name: 'property_id', index: 'property_id', hidden: true, width: 130, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'property_for_sale', index: 'property_for_sale', hidden: true,  width: 60, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'created_at', index: 'created_at', hidden: true, width: 130, align: "center", searchoptions: {sopt: conditions}, table: table},
            /*hidden fields*/
            {name: 'Commission Type', index: 'management_type', width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'management_fees',  formatter: commissionTypeFormatter},
            {name: 'Amount', index: 'management_value', width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'management_fees',  formatter: amount},
            {name: 'Minimum', index: 'minimum_management_fee', width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'management_fees', formatter: minimumAmount}
        ];
        var ignore_array = [];

        jQuery("#list_process_management_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "general_property",
                select: select_column,
                columns_options: columns_options,
                // status: '',
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: true,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Process Management Fees",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format checkbox column
     * @param status
     */
    function actionCheckboxFormatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            console.log(rowObject);
            var data = '<div class="property_checkbox" style="text-align: center">' +
                '<input type="checkbox" name="property_checkbox[]" class="property_checkbox" id="property_checkbox' + rowObject.id + '" data_id="' + rowObject.id + '" property_id="' + rowObject.id + '"/>' +
                '</div>';
            return data;
        }
    }

    /**
     *  Function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            if (rowObject.property_for_sale == 'Yes') {
                return '<span style="color:red;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            } else{
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }
    }


    /**
     * Commission Type  formatter
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string|*}
     */
    function commissionTypeFormatter(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            var type = rowObject['Commission Type'];
            if (type != ''){
                return type;
            } else {
                return '-';
            }
        }
    }

    /**
     * Amount formatter
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string|*}
     */
    function amount(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            var type = rowObject['Commission Type'];
            if (cellValue != '') {
                // return default_currency_symbol + cellValue;
                return cellValue;
            } else {
                return '-';
            }
        }
    }

    /**
     * Minimum amount formatter
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string|*}
     */
    function minimumAmount(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            if (cellValue == ''){
                return '-';
            } else {
                return default_currency_symbol + cellValue;
            }

        }
    }


    /**
     * Check/Uncheck all properties for update
     */
    $(document).on("change","#select_all_properties_checkbox",function(e){
        var inputs = $('#list_process_management_table .property_checkbox input[type=checkbox]');
        if(e.originalEvent === undefined) {
            var allChecked = true;
            inputs.each(function(){
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            inputs.prop('checked', this.checked);
        }
    });

    /**
     * Trigger Check/Uncheck all properties for update
     */
    $(document).on('change', '#list_process_management_table .property_checkbox input[type=checkbox]', function(){
        $('#select_all_properties_checkbox').trigger('change');
    });

    /**
     * On change function for management_type
     */
    $('#management_type').change(function () {
        $('#management_value').val('');
        $('#minimum_management_fee').val('');
        if ($(this).val() == 'Percentage and a min. set fee') {
            $('#minimum_fee').show();
        } else {
            $('#minimum_fee').hide();
        }
        $('#management_value').removeAttr('max');
        if ($(this).val() == 'Flat') {
            $('#flat_value_div label.error').text('');
            $('#flat_value_div').show();
            $('#percent_value_div').hide();
        } else if ($(this).val() == 'Percent') {
            $('#management_value_flat').val('');
            $('#percent_value_div label.error').text('');
            $('#flat_value_div').hide();
            $('#percent_value_div').show();

        } else if ($(this).val() == 'Percentage and a min. set fee') {
            $('#management_value_percent').val('');
            $('#minimum_management_fee').val('');
            $('#percent_value_div label.error').text('');
            $('#minimum_fee label.error').text('');
            $('#flat_value_div').hide();
            $('#percent_value_div').show();
        } else {
            $('#value_type').html('Value<em class="red-star">*</em>');
        }

    });

    /**
     * Open div to update the properties data
     */
    $(document).on("click", '#update_process_management_fee', function () {
        favorite = [];
        var no_of_checked = $('[name="property_checkbox[]"]:checked').length;
        if (no_of_checked == 0) {
            toastr.warning('Please select at least one property from grid.');
            // getDataByID('','', 'all_property_listing', favorite);
            return false;
        } else {
            $('#flat_value_div label.error').text('');
            $('#management_type').val('Flat');
            $('#minimum_management_fee').val('');
            $('#flat_value_div').show();
            $('#percent_value_div, #minimum_fee').hide();
            $.each($("input[name='property_checkbox[]']:checked"), function () {
                var abc=$(this).attr('property_id');
                $("#property_id").val(abc).prop("checked", true);
                favorite.push($(this).attr('property_id'));
            });
            getDataByID('','', 'all_property_listing', favorite);
            $('#update_process_management_div').show();
        }

    });


    /**
     * Get data by id function
     * @param data_id
     * @param prev_data
     * @param data_type
     * @param extra_array
     */
    function getDataByID(data_id, prev_data, data_type, extra_array){
        $.ajax({
            type: 'post',
            url: '/ProcessManagementFee-Ajax',
            async: false,
            data: {
                class: "processManagementFee",
                action: "getDataByID",
                id: data_id,
                prev_data: prev_data,
                data_type : data_type,
                async: false
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {

                    if(data_type == 'all_property_listing') {
                        var propertyOption = "";
                        var valueee=[];
                        var propList = data.data.property_listing;

                        if (propList.length > 0) {
                            for (var i = 0; i < propList.length; i++){
                                var checkIndex = $.inArray(propList[i].id.toString(), extra_array);
                                if (checkIndex != -1 ){
                                    propertyOption += "<option value='" + propList[i].id + "' selected>" + propList[i].property_name + "</option>";
                                }else{
                                    propertyOption += "<option value='" + propList[i].id + "'>" + propList[i].property_name + "</option>";
                                }
                            }
                            $('#property_id').html(propertyOption);
                        }
                        $('#property_id').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                        $("#property_id").multiselect( 'refresh' );
                    }
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /**
     * Update process management fee validate function
     */
    $('#update_process_management_form_id').validate({
        rules: {
            property_id : {
                required : true
            },
            management_type : {
                required : true
            },
            management_value_flat : {
                required : true,
            },
            management_value_percent : {
                required : true,
                max: 100
            },
            minimum_management_fee : {
                required : true,
            },
        },
        messages: {
            management_value_percent : {
                max: '* Please enter valid percentage digit.'
            }
        }

    });

    /**
     * Update process management fee function
     */
    $(document).on("click","#update_btn_id", function(e) {
        e.preventDefault();
        if ($('#update_process_management_form_id').valid()) {

            var formData = $('#update_process_management_form_id').serializeArray();
            var property_id = $('#property_id').val();
            $.ajax({
                type: 'post',
                url: '/ProcessManagementFee-Ajax',
                data: {
                    class: 'processManagementFee',
                    action: 'updateProcessManagement',
                    data: formData,
                    property_id: property_id
                },
                success: function (response) {
                    var res = JSON.parse(response);
                    if(res.code == 200){
                        $('#list_process_management_table').trigger('reloadGrid');
                        $('#update_process_management_div').hide();
                        $('#select_all_properties_checkbox').prop('checked', false);
                        toastr.success(res.message);
                    }
                },
            });
        }
    });
});

