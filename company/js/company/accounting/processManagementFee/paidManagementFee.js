$(document).ready(function () {

    var regex = /[,\s]/g;
    /**
     * Shows datepicker for date filter in list of properties
     */
    var currentTime = new Date();
    var maxDate = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);
    $('#process_management_fee_date').datepicker( {
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        // showButtonPanel: true,
        dateFormat: 'mm-yy',
        maxDate: maxDate,
        onClose: function(dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }
    }).datepicker("setDate", new Date());

    /**
     * To show properties in  the run management table
     */
    $(document).on('click', '#run_management_fee_btn', function(){
        var process_management_fee_date = $('#process_management_fee_date').val();
        getInitialData('', process_management_fee_date, 'all_properties_list_run_management', '', 'paid_fee');
        $('#process_amount').val('');
    });

    var process_management_fee_date = $('#process_management_fee_date').val();
    getInitialData('', process_management_fee_date, 'all_properties_list_run_management', '', 'paid_fee');
    getInitialData('', '', 'all_property_listing_ddl', '', 'paid_fee');

    function getInitialData(data_id, prev_data, data_type, extra_array, module_type){
        $.ajax({
            type: 'post',
            url: '/ProcessManagementFee-Ajax',
            async: false,
            data: {
                class: "processManagementFee",
                action: "getDataByID",
                id: data_id,
                prev_data: prev_data,
                data_type : data_type,
                extra_array: extra_array,
                module_type: module_type,
                async: false
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {

                    if (data_type == 'all_properties_list_run_management') {
                        var propertyOption = "";
                        var propList = data.data.property_listing;

                        if (propList.length > 0) {
                            var html = '';
                            var propertyDDLHtml = '';
                            var total = 0;
                            for (var i = 0; i < propList.length; i++) {
                                console.log('propList', propList);
                                var process_management_fee = (propList[i].process_management_fee != null && propList[i].process_management_fee != '') ? propList[i].process_management_fee : '0.00';
                                html += '<tr class="table-row">\n' +
                                    '    <td>\n' +
                                    '        <span>' + propList[i].property_name + '</span>\n' +
                                    '    </td>\n' +
                                    '    <td>\n' +
                                    '        <span>' +default_currency_symbol+ process_management_fee + '</span>\n' +
                                    '    </td>\n' +
                                    '</tr>';
                                process_management_fee = process_management_fee.replace(regex, '');
                                total = parseFloat(total) + parseFloat(process_management_fee);
                            }
                            total = total.toFixed(2);
                            total = numberWithCommas(total);
                            html += '<tr>' +
                                '<td align="right" style="font-size:18px;" colspan="1" class="">Total Process Management Fees</td>' +
                                '<td style="font-size:18px;" colspan="1">' + default_currency_symbol + total + '</td>' +
                                '</tr>';

                            $('#properties_listing').html(html);
                            $('.total_management_fee_amount').text(total);

                        } else {
                            var html = '<tr class="norecord"><td align="center" colspan="7">No records found</td></tr>'
                            $('#properties_listing').html(html);
                        }
                    } else if (data_type == 'all_property_listing_ddl') {
                        var propList = data.data.property_listing;

                        if (propList.length > 0) {
                            var html = '';
                            var propertyDDLHtml = '';
                            var total = 0;
                            for (var i = 0; i < propList.length; i++) {
                                if (extra_array == '') {
                                    propertyDDLHtml += '<option value="' + propList[i].id + '">' + propList[i].property_name + '</option>'
                                }
                            }
                            $('#property_id').html(propertyDDLHtml);
                            $('#property_id').multiselect({
                                includeSelectAllOption: true,
                                nonSelectedText: 'Select Property'
                            });
                            $("#property_id") .multiselect('refresh');
                        }
                    } else if (data.status == "error") {
                        toastr.error(data.message);
                    } else {
                        toastr.error(data.message);
                    }
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    /**
     * Change property field to filter paid management table
     */
    $(document).on('change', '#property_id', function(){
        var selected_properties = '';
        console.log('aaa>>>', $(this).val());
        if($("#property_id option:selected").val() !== undefined) {
            $.each($("#property_id option:selected"), function () {
                selected_properties = selected_properties + "'" + $(this).val() + "',";
            });
            selected_properties = selected_properties.replace(/^,|,$/g, '');
        }
        var process_management_fee_date = $('#process_management_fee_date').val();
        getInitialData('', process_management_fee_date, 'all_properties_list_run_management', selected_properties,'paid_fee');
    });

    /**
     * Change date field to filter paid management table
     */
    $(document).on('blur', '#process_management_fee_date', function(){
        var selected_properties = '';
        if($("#property_id option:selected").val() !== undefined) {
            $.each($("#property_id option:selected"), function () {
                selected_properties = selected_properties + "'" + $(this).val() + "',";
            });
            selected_properties = selected_properties.replace(/^,|,$/g, '');
            // selected_properties = '(' + selected_properties + ')';
        }

        var process_management_fee_date = $('#process_management_fee_date').val();
        getInitialData('', process_management_fee_date, 'all_properties_list_run_management', selected_properties, 'paid_fee');
    });


});