$(document).ready(function () {
    var redirect_owner_name = localStorage.getItem('redirect_owner_name');
    var redirect_owner_id          = localStorage.getItem('redirect_owner_id');
    if (redirect_owner_id != null && redirect_owner_id!= ''){
        $('#owner_name').val(redirect_owner_name);
        $("#selected_owner_id").val(redirect_owner_id);
    }
    localStorage.removeItem('redirect_owner_id');
    localStorage.removeItem('redirect_owner_name');

    $('#start_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat})
        .datepicker("setDate", new Date());

    $('#end_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat})
        .datepicker("setDate", new Date());


    $(document).on('click','#_easyui_textbox_input1',function(){
        $('#owner_name').combogrid('showPanel');
    });


    //combo grid for owners
    $('#owner_name').combogrid({
        panelWidth: 500,
        url: '/OwnerDraw-Ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getOwnerDataForComboGrid',
            class: 'ownerDraw'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Name', width: 60},
            {field: 'address1', title: 'Address', width: 120},
            {field: 'email', title: 'Email', width: 120},
        ]],
        onSelect: function (index, row) {
            var desc = row.address1;  // the product's description
            $("#selected_owner_id").val(row.id);
            $(".owner_pre_vendorID").val(row.id);
            $("#vendor_address").attr("disabled", false);
        }
    });
    $("#_easyui_textbox_input1").attr("placeholder", "Click here to pick a owner");
    $("#_easyui_textbox_input1").addClass("form-control");
    $("#_easyui_textbox_input1").css("width",'100%');
    $("#_easyui_textbox_input1").css("height",'34px');

    $(document).on('click','#next_btn_id',function(){
        var owner_name = $('#owner_name').val();
        var start_date = $('#start_date').val();
        var end_date   = $('#end_date').val();
        var selected_owner_id = $('#selected_owner_id').val();

        if(owner_name == ''){
            //     toastr.warning('Please select at least one owner!');
            $('#list_owner_draw_div').show();
            $('#owner_listing').jqGrid('GridUnload');

            jqGrid('', start_date, end_date);
        } else {
            $('#list_owner_draw_div').show();
            $('#owner_listing').jqGrid('GridUnload');

            jqGrid(selected_owner_id, start_date, end_date);
        }
    });

    /**
     * jqGrid Initialization function
     */
    // jqGrid(1);
    function jqGrid(selected_owner_id, start_date, end_date) {
        var sortOrder = 'asc';
        var sortColumn = 'general_property.property_name';
        var table = 'owner_property_owned';
        // var columns = ['Property Name','Property_id','Owner Name', 'Period Total','minimum_reserve', 'ownership_percentage', 'Recommended Disbursement', '<input type="checkbox" id="select_all_owners_checkbox">'];
        var columns = ['Property Name','Property_id','Owner Name','Owner_id','bank_name_id', 'bank_name', 'Period Total', 'Recommended Disbursement', '<input type="checkbox" id="select_all_owners_checkbox">'];
        var select_column = [ ];
        var joins = [
            {table:'owner_property_owned',column:'user_id',primary:'id',on_table:'users'},
            {table:'owner_property_owned',column:'property_id',primary:'id',on_table:'general_property'},
            {table:'owner_property_owned',column:'property_id',primary:'property_id',on_table:'property_bank_details'},
            // {table:'owner_property_owned',column:'user_id',primary:'user_id',on_table:'transactions'},
        ];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        if (selected_owner_id != '') {
            var extra_where = [{column:'user_type',value:'4',condition:'=',table:'users'},{column:'user_id',value:selected_owner_id,condition:'=',table:'owner_property_owned'}];
        } else {
            // var extra_where = [{column: 'user_type', value: '4', condition: '=', table: 'users'}, {column:'created_at',value:start_date,condition:'>=',table:'transactions'}, {column:'created_at',value:end_date,condition:'<=',table:'transactions'}];
            var extra_where = [{column: 'user_type', value: '4', condition: '=', table: 'users'}];
        }
        var columns_options = [
            {name:'Property Name',index:'property_name',title:false, width:60,align:"center",searchoptions: {sopt: conditions},table:'general_property', classes: 'pointer',formatter: propertyName},
            {name:'Property_id',index:'id',title:false, hidden:true, alias:'property_id',width:60,align:"center",searchoptions: {sopt: conditions},table:'general_property', classes: 'pointer'},
            {name:'Owner Name',index:'name', width:60,align:"center",searchoptions: {sopt: conditions},table:'users', classes: 'cursor',formatter: ownerName},
            {name:'owner_id',index:'id',title:false, hidden:true, alias:'owner_id',width:60,align:"center",searchoptions: {sopt: conditions},table:'users', classes: 'pointer'},
            {name:'bank_name',index:'property_id',title:false, hidden:true,width:60,align:"center",change_type:'defaultBankAccount',searchoptions: {sopt: conditions}, classes: 'pointer',table:table},
            {name:'bank_name_id',index:'property_id',title:false, hidden:true,width:60,align:"center",change_type:'defaultBankAccountId',searchoptions: {sopt: conditions}, classes: 'pointer',table:table},
            // {name:'bank_name_id',index:'id',title:false, alias: 'bank_name_id', hidden:true,width:60,align:"center",searchoptions: {sopt: conditions}, classes: 'pointer',table:'property_bank_details'},

            // Period Total Column Data
            {name:'Period Total',index:'property_percent_owned', width:130,align:"center",searchoptions: {sopt: conditions},table:table,formatter:periodTotalFmatter,change_type:'periodTotal',dateBetween:{'start':start_date,'end':end_date}},
            {name:'Recommended Disbursement',index:'property_percent_owned', width:130,align:"center",searchoptions: {sopt: conditions},table:table, formatter:recommendedDisbursementFmatter,change_type:'recommendedDisbursement',dateBetween:{'start':start_date,'end':end_date}},
            {name:'Id',index:'id', width:20,align:"center",sortable:false,searchoptions: {sopt: conditions},table:table,search:false,formatter:actionCheckboxFmatter},
        ];
        var ignore_array = [];

        jQuery("#owner_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "owner_property_owned",
                select: select_column,
                columns_options: columns_options,
                // status: '',
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Owner Draw",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }


    $(document).on("change","#select_all_owners_checkbox",function(e){
        var inputs = $('#owner_listing .owner_checkbox input[type=checkbox]');
        if(e.originalEvent === undefined) {
            var allChecked = true;
            inputs.each(function(){
                allChecked = allChecked && this.checked;
            });
            this.checked = allChecked;
        } else {
            inputs.prop('checked', this.checked);
        }
    });

    $(document).on('change', '#owner_listing .owner_checkbox input[type=checkbox]', function(){
        $('#select_all_owners_checkbox').trigger('change');
    });

    $(document).on('click', '#process_owner_draw_cross_btn', function(){
        $('#process_owner_draw_modal').modal('hide');
    });

    /**
     * jqGrid function to format checkbox column
     */
    function periodTotalFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var rowObj = rowObject;
            var start_date = $('#start_date').val();
            var end_date   = $('#end_date').val();
            cellvalue = cellvalue.replace('start_date', start_date);
            cellvalue = cellvalue.replace('end_date', end_date);
            cellvalue = cellvalue.replace('owner_name', rowObj['Owner Name']);
            return cellvalue;
        }
    }

    /**
     * jqGrid function to format checkbox column
     * @param status
     */
    function recommendedDisbursementFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            return cellvalue;
        }
    }

    /**
     * jqGrid function to format checkbox column
     * @param status
     */
    function bankNameFormatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            console.log(cellvalue);
            return cellvalue;
        }
    }

    $(document).on('blur', '.owner_draw_amount', function(){
        var total_val = $(this).val();
        var minimum_reserve = $(this).parents('table .income-owner-table').find('.minimum_reserve').html();
        var current_bank_balance = $(this).parents('table .income-owner-table').find('.current_bank_balance').html();
        var bal = current_bank_balance - minimum_reserve;
        if (minimum_reserve > current_bank_balance ) {
            $(this).val('0.00');
            toastr.warning('Owner Bank Balance is less than Minimum Reserve amount!')
        } else if (total_val >= bal ) {
            $(this).val('0.00');
            toastr.warning('Please enter lesser amount to Owner Bank Balance!')
        } else {        }
    });

    $(document).on('keypress','.money',function(e){
        if(this.value.length==10) return false;
    });

    $(document).on('focusout','.money',function(){
        if($(this).val() != '' && $(this).val().indexOf(".") == -1) {
            var bef = $(this).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $(this).val(value);
        } else {
            var bef = $(this).val().replace(/,/g, '');
            $(this).val(numberWithCommas(bef));
        }
    });

    /**
     * Function to format owner name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ownerName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }
    /**
     *  Function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }
    /**
     * jqGrid function to format checkbox column
     * @param status
     */

    function actionCheckboxFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var data = '';
            var data = '<div class="owner_checkbox"><input type="checkbox" name="owner_checkbox[]" class="owner_checkbox" id="owner_checkbox'+rowObject.owner_id+'" owner_id="' + rowObject.owner_id + '" property_id="' + rowObject.Property_id + '"/></div>';
            return data;
        }
    }

    $(document).on("click","#cancel_add_owner_draw_btn",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Accounting/OwnerDraw'
            }
        });
    });
    var dataArr = [];
    $(document).on('click', '#processOwnerDraw', function(){
        var no_of_checked = $('[name="owner_checkbox[]"]:checked').length;
        if (no_of_checked == 0) {
            toastr.warning('Please select at least one record.');
            return false;
        } else {
            var all_checked_inputs = $('#owner_listing .owner_checkbox input[type=checkbox]:checked');
            dataArr = [];
            var html = '';
            all_checked_inputs.each(function(){
                var property_id =$(this).parents('tr').find('td').eq(1).text();
                var owner_name =$(this).parents('tr').find('td').eq(2).find('span').text();
                var owner_id =$(this).parents('tr').find('td').eq(3).text();
                var bank_name =$(this).parents('tr').find('td').eq(4).text();
                var bank_name_id =$(this).parents('tr').find('td').eq(5).text();
                var owner_draw_amount =$(this).parents('tr').find('.owner_draw_amount').val();

                html += '<tr>' +
                    '<td>'+bank_name+'</td>' +
                    '<td>'+owner_draw_amount+'</td>' +
                    '<td>'+owner_name+'</td>' +
                    '<td></td>' +
                    '</tr>';
                dataArr.push({'property_id':property_id, 'owner_id':owner_id,'bank_name':bank_name,'bank_name_id':bank_name_id,'owner_draw_amount':owner_draw_amount});
            });
            $('#process_owner_draw_modal .owner_draw_process_tbody').html(html);

            $('#process_owner_draw_modal').modal({
                show: true,
                backdrop: 'static',
                keyboard: false,
            });

            // checkForPayment('Check');
            // console.log('dataArr>>', dataArr);
        }
    });

    $(document).on("change","#payment_type",function(){
        var transaction_type = $(this).val();
        var selected_owner_id = $("#selected_owner_id").val();

        if (transaction_type == 'Check' || transaction_type == ''){

        }
        if (transaction_type == 'Cash'){

        }

        if (transaction_type == 'ACH' || transaction_type == 'Credit Card/Debit Card') {
            checkForPayment(transaction_type, '');
        }
    });

    /**
     *Check payment mode set for PM account or not
     */
    function checkForPayment(payment_type, selected_owner_id) {
        $.ajax({
            type: 'post',
            url: '/OwnerDraw-Ajax',
            data: {
                class: "ownerDraw",
                action: "checkForPayment",
                "payment_type": payment_type,
                "user_id": selected_owner_id
            },
            success: function (response) {
                var res = JSON.parse(response);
                console.log('res>>>', res);
                if (res.status == 'false') {
                    toastr.error(res.message);
                    if (payment_type == 'Credit Card/Debit Card') {
                        $("#owner_draw_pay_btn").attr("disabled", true);
                    }
                    if (payment_type == 'ACH') {
                        $("#owner_draw_pay_btn").attr("disabled", true);
                    }
                    if (payment_type == 'Check'){
                        $("#owner_draw_pay_btn").attr("disabled", true);
                    }
                } else {
                    $("#owner_draw_pay_btn").attr("disabled", false);
                }
            }
        });
    }

    /**
     * Make payment on Pay button click
     */
    $(document).on("click","#owner_draw_pay_btn",function(){
        var payment_type = $('#payment_type').val();
        bootbox.confirm({
            message: "Click OK if wants to pay.",
            buttons: {confirm: {label: 'OK'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    confirmPaymentOnSubmit(payment_type, dataArr);
                }
            }
        });
    });

    /**
     * Confirm final payment on OK button click
     */
    function confirmPaymentOnSubmit(payment_type, dataArr) {
        $.ajax({
            type: 'post',
            url: '/OwnerDraw-Ajax',
            data: {
                class: "ownerDraw",
                action: "createPaymentOnSubmit",
                "payment_type": payment_type,
                "data_array": dataArr
            },
            success: function (response) {
                var res = JSON.parse(response);
                if (res.status == 'false') {
                    toastr.error(res.message);
                    if (payment_type == 'Credit Card/Debit Card') {
                        // cardTypeDiv(transaction_type);
                    }
                    if (payment_type == 'ACH') {
                        // achTypeDiv(transaction_type);
                    }
                    if (payment_type == 'Check'){
                        $("#owner_draw_pay_btn").attr("disabled", true);
                    }
                } else {
                    window.location.href = '/Accounting/OwnerDraw';
                }
            }
        });
    }

});