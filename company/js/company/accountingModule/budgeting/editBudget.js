getBudgetInfo(budget_id);
function getBudgetInfo(){
    $.ajax({
        type: 'post',
        url: '/budgeting-ajax',
        data: {
            class: 'Budgeting',
            action: 'getBudgetInfo',
            budget_id: budget_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                $("#newBudgetForm [name='budget_name']").val(res.data.budget_name);
                $("#newBudgetForm [name='starting_month']").val(res.data.starting_month);
                setTimeout(function(){
                    $("#newBudgetForm [name='portfolio_id']").val(res.data.portfolio_id);
                    fetchAllProperty(res.data.portfolio_id,res.data.property_id);
                    makeDiscriptionHeading(res.data.starting_month);
                }, 1000);
                $('.yearDesc').html('Total - FY ' +res.data.starting_year);
                $('.trIncomeTotal').val(changeToFloat(res.data.income_amount.toString()));
                $('.trExpenseTotal').val(changeToFloat(res.data.expense_amount.toString()));
                $('.grandTotal').val(changeToFloat(res.data.net_income.toString()));
                $("#newBudgetForm [name='starting_year']").val(res.data.starting_year);
            }
        },
    });
}
getBudgetInfoDetail(budget_id);
function getBudgetInfoDetail(){
    $.ajax({
        type: 'post',
        url: '/budgeting-ajax',
        data: {
            class: 'Budgeting',
            action: 'getBudgetInfoDetails',
            budget_id: budget_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                var netIncomeTr ='<tr id="incomeTrRow"><td>Income ('+default_currency_symbol+')</td>';
                var netIncomeValue = 0;
                var netExpenseValue = 0;
                $.each(res.data2[0],function(key,value){
                    var originalValue = value;
                    var value = changeToFloat(value.toString());
                    if(key != 12){
                        netIncomeTr += '<td><input class="form-control trIncome" value="'+value+'" readonly type="text"/></td>';
                    } else {
                        netIncomeTr += '<td><input class="form-control trIncomeTotal" value="'+value+'" readonly type="text"/></td>';
                        netIncomeValue = originalValue;
                    }
                });
                netIncomeTr += '</tr>';

                var netExpenseTr = '<tr id="expenseTrRow"><td>Expense ('+default_currency_symbol+')</td>';
                $.each(res.data2[1],function(key,value){
                    var originalValue = value;
                    var value = changeToFloat(value.toString());
                    if(key != 12){
                        netExpenseTr += '<td><input class="form-control trExpense capital"  value="'+value+'" readonly="" type="text"></td>';
                    } else {
                        netExpenseTr += '<td><input class="form-control trExpenseTotal capital" value="'+value+'" readonly="" type="text"></td>';
                        netExpenseValue = originalValue;
                    }
                });
                netExpenseTr += '</tr>';
                var netTotal = changeToFloat((netIncomeValue-netExpenseValue).toString());
                var grandTotal = '<tr><td>Net Income ('+default_currency_symbol+')</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td><input class="form-control grandTotal capital" value="'+netTotal+'" readonly="" type="text"></td></tr>';
                $('#editNetIncomeTable').html(netIncomeTr+netExpenseTr+grandTotal);
                var income = '<tr><td colspan="14" class="bg-grey">Income <em class="red-star">*</em> ('+default_currency_symbol+')</td></tr>';
                var expense = '<tr><td colspan="14" class="bg-grey">Expenses <em class="red-star">*</em> ('+default_currency_symbol+')</td></tr>';
                $.each(res.data,function(key,value){
                    if(value.account_type_id == '2') {
                        expense += '<tr id="'+value.charts_of_account_id+'" data_type="expense">';
                        expense += '<td data_index="0" data_type="expense">'+value.account_code+'-'+value.account_name+'</td>';
                        $.each(value.trData,function(key1,value1){
                            var value1 = changeToFloat(value1.toString());
                            if(key1 != 12) {
                                expense += '<td data_index="' + (key1 + 1) + '" data_type="expense"><input class="form-control number_only amount_num trAmount" value="' + value1 + '" type="text"/></td>';
                            } else {
                                expense += '<td data_index="' + (key1 + 1) + '" data_type="expense"><input class="form-control number_only amount_num trAmountTotal" value="' + value1 + '" readonly type="text"/></td>';
                            }
                        });
                        expense += '</tr>';
                    } else if(value.account_type_id == '5') {
                        income += '<tr id="'+value.charts_of_account_id+'" data_type="income">';
                        income += '<td data_index="0" data_type="income">'+value.account_code+'-'+value.account_name+'</td>';
                        $.each(value.trData,function(key1,value1){
                            var value1 = changeToFloat(value1.toString());
                            if(key1 != 12) {
                                income += '<td data_index="' + (key1 + 1) + '" data_type="income"><input class="form-control number_only amount_num trAmount" value="' + value1 + '" type="text"/></td>';
                            } else {
                                income += '<td data_index="' + (key1 + 1) + '" data_type="income"><input class="form-control number_only amount_num trAmountTotal" value="' + value1 + '" type="text"/></td>';
                            }
                        });
                        income += '</tr>';
                    }
                });
                $('#appendCOA').html(income+expense);
            }
        },
    });
}