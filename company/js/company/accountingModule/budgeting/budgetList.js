if(localStorage.getItem('ElasticSearch')){
    var elasticSearchData = localStorage.getItem('ElasticSearch');
    setTimeout(function(){
        var grid = $("#budgetTable"),f = [];
        f.push({field: "company_budget.id", op: "eq", data: elasticSearchData,int:'true'});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        localStorage.removeItem('ElasticSearch');
    },1500);
}

jqGrid('all');
function jqGrid(status) {
    var table = 'company_budget';
    var columns = ['Budget Name','Portfolio','Property', 'Year-Month', 'Month','Created Date', 'Income ('+default_currency_symbol+')', 'Expense ('+default_currency_symbol+')', 'Net Income ('+default_currency_symbol+')', 'Action'];
    var select_column = ['Edit'];
    var joins = [{table: 'company_budget', column: 'portfolio_id', primary: 'id', on_table: 'company_property_portfolio'},{table: 'company_budget', column: 'property_id', primary: 'id', on_table: 'general_property'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var extra_where = [];
    var columns_options = [
        {name: 'Budget Name',  index: 'budget_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
        {name: 'Portfolio' ,index: 'portfolio_name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: 'company_property_portfolio', classes: 'pointer'},
        {name: 'Property', index: 'property_name', width: 100, searchoptions: {sopt: conditions}, table: 'general_property',  classes: 'pointer',formatter:propertyFormatter},
        {name: 'Year-Month', index: 'starting_year', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table,  classes: 'pointer',formatter:yearMonthFormatter},
        {name: 'Month', index: 'starting_month', width: 80,hidden:true, align: "center", searchoptions: {sopt: conditions}, table: table,  classes: 'pointer'},
        {name: 'Created Date', index: 'created_at', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table,  classes: 'pointer'},
        {name: 'Income ('+default_currency_symbol+')', index: 'income_amount', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:incomeFormatter},
        {name: 'Expense ('+default_currency_symbol+')', index: 'expense_amount', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:expenseFormatter},
        {name: 'Net Income ('+default_currency_symbol+')', index: 'net_income', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:netFormatter},
        {name: 'Action', index: 'select', title: false, width: 80, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table}
    ];
    var ignore_array = [];
    jQuery("#budgetTable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'company_budget.updated_at',
        sortorder: 'desc',
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Budgeting",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 400, drag: true, resize: false} // search options
    );
}

function incomeFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return default_currency_symbol+changeToFloat(cellValue.toString());
    }
}

function expenseFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return default_currency_symbol+changeToFloat(cellValue.toString());
    }
}

function netFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return default_currency_symbol+changeToFloat(cellValue.toString());
    }
}

function yearMonthFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return cellValue+'-'+rowObject.Month
    }
}

function propertyFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return '<span style="color:#30A0E4;text-decoration: underline">'+cellValue+'</span>';
    }
}

fetchAllPortfolio('','#portfolio_id');
fetchAllPortfolio('','#copy_portfolio_id');
fetchAllPortfolio('','#search_portfolio_id');
function fetchAllPortfolio(id,element) {
    $.ajax({
        type: 'post',
        url: '/fetch-portfolioname',
        data: {
            propertyEditid: '',
            class: 'propertyDetail',
            action: 'fetchPortfolioname'},
        success: function (response) {
            var res = JSON.parse(response);
            $(element).html(res.data);
            $(element).val('');
        },
    });
}

function fetchAllProperty(portfolio_id,id,element) {
    $.ajax({
        type: 'post',
        url: '/global-get-module-ajax',
        data: {
            class: 'GlobalGetModule',
            action: 'getAllProperties',
            portfolio_id: portfolio_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="all">Select Property</option>';
                $.each(res.data,function(key,value){
                    if(id == value.id) {
                       html += '<option value="' + value.id + '" selected>' + value.property_name + '</option>';
                    } else {
                        html += '<option value="' + value.id + '">' + value.property_name + '</option>';
                    }
                });
                $(element).html(html);
            }
        },
    });
}

$(document).on('change','#portfolio_id',function(){
    fetchAllProperty($(this).val(),'','#property_id');
});
$(document).on('change','#copy_portfolio_id',function(){
    fetchAllProperty($(this).val(),'','#copy_property_id');
});
$(document).on('change','#search_portfolio_id',function(){
    fetchAllProperty($(this).val(),'','#search_property_id');
});

$(document).on('change','.commonFilters',function(){
    searchFilters();
});

function searchFilters(){
    var grid = $("#budgetTable"),f = [];
    var portfolio_id = ($('#portfolio_id').val() == 'default')?'all':$('#portfolio_id').val();
    var property_id = $('#property_id').val();
    f.push({field: "company_budget.portfolio_id", op: "eq", data: portfolio_id},{field: "company_budget.property_id", op: "eq", data: property_id});
    grid[0].p.search = true;
    $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

$(document).on('change', '#budgetTable .select_options', function () {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    //New code start
    switch (opt) {
        case "Edit":
            window.location.href = "/Accounting/Budgeting/EditBudget?id=" + id;
            break;
        default:
            $('#property-table .select_options').prop('selectedIndex',0);
    }
    //New code end
});