//add property form client side validations
$("#copyBudgetForm").validate({
    rules: {
        budget_name: {
            required:true
        },
        starting_month: {
            required:true
        },
        starting_year: {
            required:true
        },
        portfolio_id: {
            valueNotEquals:'default'
        },
        property_id: {
            required:true
        },
        search_portfolio_id: {
            valueNotEquals:'default'
        },
        search_property_id: {
            required:true
        },
        budget: {
            required:true
        }
    }
});

$(document).on('change','#search_property_id',function(){
    getBudget($('#search_portfolio_id').val(),$('#search_property_id').val(),'');
});

function getBudget(portfolio_id,property_id,id){
    $.ajax({
        type: 'post',
        url: '/budgeting-ajax',
        data: {
            class: 'Budgeting',
            action: 'getBudget',
            portfolio_id: portfolio_id,
            property_id: property_id},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200) {
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    if(id == value.id) {
                        html += '<option value="' + value.id + '" selected>' + value.budget_name + '</option>';
                    } else {
                        html += '<option value="' + value.id + '">' + value.budget_name + '</option>';
                    }
                });
                $('#budget').html(html);
            }
        },
    });
}

$(document).on('submit','#copyBudgetForm',function(e){
    e.preventDefault();
    if($('#copyBudgetForm').valid()){
        $.ajax({
            type: 'post',
            url: '/budgeting-ajax',
            data: {
                class: 'Budgeting',
                action: 'copyBudget',
                data: $('#copyBudgetForm').serializeArray()},
            success: function (response) {
                var res = JSON.parse(response);
                if(res.code == 200) {
                    $('#myModal').modal('hide');
                    toastr.success(res.message);
                    $("#budgetTable").trigger('reloadGrid');
                    setTimeout(function(){
                        jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('.table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                    }, 1000);
                }
            },
        });
    }
});

