var billIds = [];
var checkboxes = [];
$("#paymentForm").validate({
    rules: {
        date: {
            required:true
        },
        bank: {
            required:true
        },
        payment_type: {
            required:true
        },
        check_number: {
            required:true
        },
        amount: {
            required:true
        }
    }
});

const allEqual = arr => arr.every( v => v === arr[0] );

$(document).on('click','.checkboxgrid',function(){
    var checked = $(this).prop('checked');
    if($(this).attr('data_status') == '1') return false;
    checkboxes = [];
    if(checked){
        billIds = [];
        var tempData =[]
        var user_id = '';
        var amount = 0;
        var dataUsers = [];
        $('.checkboxgrid').each(function(key,value){
            if($(value).prop('checked')) {
                dataUsers.push($(value).attr('data_value'));
                tempData.push($(value).val());
                var originalAmount = $(value).parent().parent().find('td:eq(8)').text().replace('$', '');
                originalAmount = originalAmount.replace(/,/g, '');
                amount += parseFloat(originalAmount);
                checkboxes.push('1');
            }
        });
        if(allEqual( dataUsers )){
            $('#userData').val(dataUsers[0]);
            $('#userData').attr('data_type','Vendor');
            $('#checkoutAmount').val(amount);
            billIds = tempData;
        } else {
            bootbox.alert('Please select the same vendor!');
            $('#checkoutAmount').val('');
            $('.checkboxgrid').prop('checked', false);
            return false;
        }

    }
});


$(document).on('click','#paySelectedBill',function(){
    var user_id = [];
    var bill_id = [];
    $('.checkboxgrid').each(function(key,value){
        if($(value).prop('checked')) {
            user_id.push($(value).attr('data_value'));
            bill_id.push($(value).val());
        }
    });

});

getBankList('');
function getBankList(id){
    $.ajax({
        type: 'post',
        url: '/budgeting-ajax',
        data: {
            class: 'Budgeting',
            action: 'getBankDetails'},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    if(id == value.id) {
                        html += '<option value="' + value.id + '" selected>' + value.bank_name + '</option>';
                    } else {
                        html += '<option value="' + value.id + '">' + value.bank_name + '</option>';
                    }
                });
                $('#bankName').html(html);
            }
        },
    });
}

$(document).on('change','#payment_type',function(){
    $('#bankAccount').hide();
    $('#checkNumber').hide();
    var user_id = $('#userData').val();
    var type = $('#userData').attr('data_type');
    if($(this).val() == 'Check'){
        $('#bankAccount').show();
        $('#checkNumber').show();
    } else if($(this).val() == 'ACH') {
        if(user_id != '') {
            checkForACH(user_id, type);
        }
    } else if($(this).val() == 'Credit'){
        if(user_id != '') {
            checkForCard(user_id, type)
        }
    }
});

function checkForCard(user_id,type){
    $.ajax({
        type: 'post',
        url: '/accountingReceivable',
        data: {
            class: 'accounting',
            action: 'checkForCard',
            user_id: user_id,
            type:type},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.status == 'false'){
                bootbox.alert(res.message);
                $('#paySelectedBill').prop('disabled',true);
            } else {
                $('#paySelectedBill').prop('disabled',false);
            }
        },
    });
}

function checkForACH(user_id,type){
    $.ajax({
        type: 'post',
        url: '/accountingReceivable',
        data: {
            class: 'accounting',
            action: 'checkForACH',
            user_id: user_id,
            type:type},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.status == 'false'){
                bootbox.alert(res.message);
                $('#paySelectedBill').prop('disabled',true);
            } else {
                $('#paySelectedBill').prop('disabled',false);
            }
        },
    });
}


$(document).on('submit','#paymentForm',function(e){
    e.preventDefault();
    if($('#paymentForm').valid()){
        if($('#payment_type').val() == 'Credit' || $('#payment_type').val() == 'ACH'){
            $('#payNow').modal('show');
            $('.paidAmount').text($('#checkoutAmount').val());
        } else {
            saveTransactionDetail();
        }
    }
});


function saveTransactionDetail(){
    if(checkboxes.length == 0) {
        $('#checkoutAmount').val('') ;
        bootbox.alert('Please select bill for payment.');
        return false;
    }
    $.ajax({
        type: 'post',
        url: '/payBills-ajax',
        data: {
            class: 'accounting',
            action: 'payBillTransaction',
            data: $('#paymentForm').serializeArray(),
            bills:billIds},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                toastr.success(res.message);
                $('#paymentForm').trigger('reset');
            }
        },
    });
}


function stripeTokenHandler(token) {
    $(".submit_payment_class").attr("disabled",true);
    var currency = 'USD';
    var Amount = $('#checkoutAmount').val();
    var user_type = 'vendor';
    var vendor_id = $('#userData').val();
    // Insert the token ID into the form so it gets submitted to the server
    $.ajax({
        url:'/payBills-ajax',
        type: 'POST',
        data: {
            "action": 'payBillCheckout',
            "class": 'Paybills',
            "token_id":token,
            "currency":currency,
            "amount": Amount,
            "vendor_id":vendor_id,
            "user_type":user_type,
            "data":billIds,
        }, beforeSend: function() {
            // setting a timeout
            $('#loadingmessage').show();
        },
        success: function (response) {
            $('#loadingmessage').hide();
            $(".submit_payment_class").attr("disabled",false);
            var response = JSON.parse(response);
            if(response.status=='success') {
                $('#payNow').modal('hide');
                $('#paybills_table').trigger('reloadGrid');
                toastr.success(response.message);
            } else {
                toastr.error(response.message);
            }
        }
    });
}

// function checkDefaultSource(cusid,type){
//     $.ajax({
//         type: 'post',
//         url: '/accountingReceivable',
//         data: {
//             class: 'accounting',
//             action: 'checkDefaultSource',
//             user_id: user_id,
//             type:type},
//         success: function (response) {
//             var res = JSON.parse(response);
//             if(res.status == 'false'){
//                 bootbox.alert(res.message);
//                 $('#paySelectedBill').prop('disabled',true);
//             } else {
//                 $('#paySelectedBill').prop('disabled',false);
//             }
//         },
//     });
// }