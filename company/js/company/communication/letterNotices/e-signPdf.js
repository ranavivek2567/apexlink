$(document).ready(function () {
    function getBase64FromBlob(url){

        $.ajax({
            type: 'get',
            url: url,
            dataType:  "binary",
            success: function (data) {
                var reader = new FileReader();
                reader.readAsDataURL(data);
                console.log("here");
                reader.onloadend = function() {
                    var base64data = reader.result;
                    console.log("base64data")
                    console.log(base64data)
                }
            },
            error: function (data) {
                console.log("in error");
            }
        });

    }

    function applyFont(font, classname) {
        font = font.replace(/\+/g, ' ');
        font = font.split(':');
        var fontFamily = font[0];
        var fontWeight = font[1] || 400;
        $(classname).css({fontFamily:"'"+fontFamily+"'", fontWeight:fontWeight});
        $(".typingtext").attr("data-font",fontFamily);
    }

    $('input[name="selectfonts"]').on('change', function() {
        var value = $(this).val();
        applyFont(value,".enterplaceholder,.typingtext");
    });

    $(document).on("change","input[name='typing']",function () {
        if($(this).val() == "t"){
            $(".sketchingarea").hide();
            $(".typingarea").show();
        }else{
            $(".sketchingarea").show();
            $(".typingarea").hide();
        }
    });
    $(document).on("click",".esignaturediv", function () {
        $("#eSignModal").modal('show');
        $('#defaultSignature').signature({syncField: '#signatureJSON'});
        $('#defaultSignature').signature('option', 'syncFormat', 'PNG');
    });

    $(document).on("click",'.clearSignature',function() {
        $('#defaultSignature').signature('clear');
    });

    $('#redrawSignature').signature({disabled: true});
    $('#redrawSignature canvas').prop('id',"canvas2");

    $(document).on("click","#eSignModal .esignaturediv a",function(e){
        e.preventDefault();
        console.log("test");
        $('#redrawSignature').signature('enable').signature('draw', $('#signatureJSON').val());
        var dataUser = $(this).attr("data-user");
        var dataType = $(this).attr("data-type");
        var dataParent = $(this).attr("data-parent");
        var fontText = $(".typingtext").val();
        var fontfamily = $(".typingtext").attr("data-font");
        console.log("dataUser >> ",dataUser,"dataType >>",dataType,"dataParent >>",dataParent,"fontText >>",fontText,"fontfamily >>",fontfamily)
        setTimeout(function () {
            var can = document.getElementById("canvas2");
            var pngUrl = can.toDataURL();
            console.log(pngUrl);
            //saveSignatureImage(pngUrl,dataUser,dataType,dataParent,fontText,fontfamily);
        },1000);
    });

    var HelloButton = function (context) {
        var ui = $.summernote.ui;
        var button = ui.button({
            contents: '<label class="custom-file-upload"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></label>',
            tooltip: 'Attach file',
            click: function () {
                $('#imagemodal').modal("show");
                $("#upload-button").on('click', function() {
                    $("#file-to-upload").trigger('click');
                });

                // When user chooses a PDF file
                $("#file-to-upload").on('change', function() {
                    if(['application/pdf'].indexOf($("#file-to-upload").get(0).files[0].type) == -1) {
                        alert('Error : Not a PDF');
                        return;
                    }

                    $("#upload-button").hide();
                    var url = URL.createObjectURL($("#file-to-upload").get(0).files[0]);
                    console.log("url");

                    console.log(url);
                    //getBase64FromBlob(url);
                    showData(url);
                });

                function showData(url){
                    var __PDF_DOC,__CURRENT_PAGE,__TOTAL_PAGES,__PAGE_RENDERING_IN_PROGRESS = 0,__CANVAS = $('#pdf-canvas').get(0),
                        __CANVAS_CTX = __CANVAS.getContext('2d');
                    var url = url;
                    var currPage   = 1;
                    var __TOTAL_PAGES = 0;
                    var __PDF_DOC = null;
                    var pdfjsLib = window['pdfjs-dist/build/pdf'];
                    pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
                    var loadingTask = pdfjsLib.getDocument({ url: url});

                    loadingTask.promise.then(function(pdf) {
                        __PDF_DOC = pdf;
                        __TOTAL_PAGES = pdf.numPages;/*How many pages it has*/
                        pdf.getPage( 1 ).then( handlePages );/*Start with first page*/
                    }, function (reason) {
                        console.error(reason);
                    });

                    function handlePages(page){
                        var viewport = page.getViewport({scale:1.5});/*This gives us the page's dimensions at full scale*/
                        var canvas = document.createElement( "canvas" );
                        var context = canvas.getContext('2d');
                        canvas.style.display = "block";
                        canvas.style.zIndex = "100";
                        canvas.height = viewport.height;
                        canvas.width = viewport.width;
                        page.render({canvasContext: context, viewport: viewport});/*Draw it on the canvas*/
                        /*document.body.appendChild( canvas );*/
                        document.getElementById("convasData").appendChild(canvas);/*Add it to the web page. */
                        currPage++;
                        if ( __PDF_DOC !== null && currPage <= __TOTAL_PAGES ){
                            __PDF_DOC.getPage( currPage ).then( handlePages );
                        }
                    }
                }


            }
        });
        return button.render();/* return button as jquery object */
    }

    $('.summernote').summernote({
        addclass: {
            debug: false,
            classTags: [{title:"Button","value":"btn btn-success"},"jumbotron", "lead","img-rounded","img-circle", "img-responsive","btn", "btn btn-success","btn btn-danger","text-muted", "text-primary", "text-warning", "text-danger", "text-success", "table-bordered", "table-responsive", "alert", "alert alert-success", "alert alert-info", "alert alert-warning", "alert alert-danger", "visible-sm", "hidden-xs", "hidden-md", "hidden-lg", "hidden-print"]
        },
        width: '100%',
        height: '300px',
        //margin-left: '15px',
        toolbar: [
            ['mybutton', ['hello']],
            // [groupName, [list of button]]
            ['img', ['picture']],
            ['style', ['style', 'addclass', 'clear']],
            ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
            ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']],
            ['extra', ['video', 'table', 'height']],
            ['misc', ['undo', 'redo', 'codeview', 'help']]
                ['insert', ['link', 'picture', 'video', 'file']]
        ],
        buttons: {
            hello: HelloButton
        }
    });


    $(document).on('click','#imagemodal .close',function () {
        var contentData = $("#convasData").html();
        $("#convasData").find("canvas").attr("id","canvas_id");
        $("#convasData").find("canvas").attr("class","canvas_id");
        var canvas = document.getElementsByClassName("canvas_id");

        var image;
        var count = 1;
        $( ".canvas_id" ).each(function(  ) {
            image = $(this)[0].toDataURL("image/png").replace("image/png", "image/octet-stream");
            var newImage = "<img src='"+image+"' style='width:100%; height:100%;'>";
            $("#create_new_letter_div .summernote").summernote("insertImage", image, "test.png");
        });
        setTimeout(function(){

            var html = $('<div class="esignaturediv" id="esignaturediv" style="border:1px solid #000; width:100px; height:30px;">E-Signature Here</div>');
            $("#create_new_letter_div .summernote").summernote("insertNode", html[0]);
            //$('#create_new_letter_div .summernote').summernote('insertText', '<p>Hello, world</p>');
        },2000);

        /*console.log(canvas);
        var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        console.log(image);
        var newImage = "<img src='"+image+"' style='width:100%; height:100%;'>";
        $("#create_new_letter_div .summernote").summernote("code",'<div class="wrapper">'+newImage+'</div>');*/
        /*context.invoke('editor.insertText', '<div class="allData">'+contentData+'</div>');*/
    });

});