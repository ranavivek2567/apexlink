$(document).ready(function () {
    $(window).on("resize", function () {
        var $grid = $("#list"),
            newWidth = $grid.closest(".ui-jqgrid").parent().width();
        $grid.jqGrid("setGridWidth", newWidth, true);
    });
    //jqGrid status
    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#LandLord-Documents-table').jqGrid('GridUnload');
        jqGridLandLord(selected);
    });
    jqGridLandLord('All');
    var protocol = location.port;
    if (protocol != "") {
        var webpath = location.protocol+"//"+location.hostname+":"+protocol;
    }else{
        var webpath = location.protocol+"//"+location.hostname;
    }
    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGridLandLord(status) {
        /*if(jqgridNewOrUpdated == 'true')
      {
           var sortOrder = 'desc';
           var sortColumn = 'letters_notices_template.updated_at';
       } else {
           var sortOrder = 'asc';
           var sortColumn = 'letters_notices_template.letter_name';
       }*/
        var table = 'letters_notices_template';
        var columns = ['Letter Name','Description','editable','Action'];
        var select_column = [];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['letters_notices_template.deleted_at, letters_notices_template.updated_at, letters_notices_template.is_editable'];
        var extra_where = [{column:'letter_type',value:'3',condition:'=',table:'letters_notices_template'}];
        var columns_options = [
            {name: 'Letter Name', index: 'letter_name', width: 455,  align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer letterNotice'},
            {name: 'Description', index: 'description', width: 450,  align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            /*{name: 'Action', index: 'select', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:'actions',editable:true},*/
            {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'id',classes: 'textCenterTd', title: false, width:450,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: imageRedirect, edittype: 'select',search:false,table:table},

        ];
        var ignore_array = [];
        jQuery("#LandLord-Documents-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            //sortname: 'updated_at',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns:extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: 'desc',
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of LandLord Documents",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:400,drag:true,resize:false} // search options
        );
    }

    function imageRedirect(cellValue, options, rowObject){

        if(rowObject !== undefined){
            // console.log(rowObject['editable']);
            var html = "";
            if(rowObject['editable'] == '0'){
                html += "<span class='getLeaseTemplateId' data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/edit-icon.png'  class='edit_doc_icon' title='edit' alt='my image' style='cursor: pointer; '/></a></span>";
            }else if (rowObject['editable'] == '1'){
                html += "<span class='getLeaseTemplateId' data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/edit-icon.png'  class='edit_doc_icon' title='edit' alt='my image' style='cursor: pointer; '/></a></span><span data_id='"+rowObject.id+"'><a><img src='"+webpath+"/company/images/red_x_icon.png'  class='delete_doc_icon' title='delete' alt='my image' style='cursor: pointer; '/></a></span>";
            }else {
                html += "fdsfdsfsdfsdfds";
            }
            return html;
        }

    }
    $(document).on('click','#LandLord-Documents tr td:not(:last-child)',function(e){
        e.preventDefault();
        var id = $(this).closest("tr").attr('id');
        // alert(id);
        docTemplateDataDetails(id);

    });
    function docTemplateDataDetails(id){
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: 'lettersNoticesAjax',
                action: 'docTemplateDataDetails',
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                // console.log(response.data);
                var html ="";
                //console.log("------------------");
                if (response.code == 200) {
                        $("#create_new_letter_div #first_name").val(response.data.templateTitle.letter_name);
                        $("#create_new_letter_div .summernote").summernote("code", response.data.templateTitle.template_html);
                        $("#template_id").val(response.data.templateTitle.id);
                        $.each(response.data.templateTags, function (key, value) {
                            $('#tag_name').append($("<option value = " + value.tag_name + ">" + value.tag_name + "</option>"));
                        });
                        $("#create_new_letter_div #letter_type").val(response.data.templateTitle.letter_type);
                        $("#create_new_letter_div #middle_name").val(response.data.templateTitle.description);
                        $.each(response.data.templateTags, function (key, value) {
                            $('#tag_name').append($("<option value = " + value.tag_name + ">" + value.tag_name + "</option>"));
                        });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    $(document).on("click","#LandLord-Documents-table .delete_doc_icon",function () {
        var id = $(this).parent().parent().attr('data_id');
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/LettersNoticesDoc-Ajax',
                        data: {id: id,class:'lettersNoticesAjax',action:'deleteLettersNotices'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#LandLord-Documents-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    });



    $(document).on('click','#LandLord-Documents-table tr .letterNotice',function(e){
        $("#searchboxData").hide();
        $("#searchboxDataOwner").show();
        $("#LandLord-Documents").hide();
    });

    $(document).on('click','#LandLord-Documents-table tr td:not(:last-child)',function(e){
        e.preventDefault();
        var id = $(this).closest("tr").attr('id');
        $("#searchTemplateId").val(id);
    });

    $(document).on('click', '.active_tab', function () {
        // /$("#searchboxDataOwner").hide();
        var active_tab = $("#active_tabs li.active a").attr('href');
        $(active_tab+"-table").trigger('reloadGrid');
        /*console.log('active_tabs>>', active_tab);*/
        $("#searchboxDataOwner").hide();
        $('#active_tab-table').jqGrid('GridUnload');
        $(active_tab).show();
        var $grid = $("#Tenant-Documents-table"),
            newWidth = $grid.closest(".ui-jqgrid").parent().width();
        $grid.jqGrid("setGridWidth", newWidth, true);
    });
    $(document).on('click', '#searchboxDataOwner .back-building', function () {
        var active_tab = $("#active_tabs li.active a").attr('href');
       /* console.log('active_tabs>>', active_tab);*/
        $("#searchboxDataOwner").hide();
        $(active_tab).show();
    });

    /*owner search listing*/
    getOwnerList();
    function getOwnerList(){
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: 'lettersNoticesAjax',
                action: 'getOwnerList',
                // data :window.location.pathname
            },
            success: function (response) {
                var html="";
                var response = JSON.parse(response);
                /*console.log(response.data);
                console.log("++++stop++++++");*/
                for (var i = 0; i < response.data.length; i++) {
                    html += ' <option class="abc" value="' + response.data[i].id + '" data-id="' + response.data[i].id + '">' + response.data[i].first_name +' '+response.data[i].last_name + '</option>';
                    $("#owner_listing_search").html("<option value='all'>Select</option>" + html);
                   //getInspectionBuildingDetails $("#propertyListAll").html("<option value='all'>Select</option>" + html);
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /*owner search listing*/
    $(document).on("change","#searchboxDataOwner #owner_listing_search",function(){
        var conceptName = $('#searchboxDataOwner #owner_listing_search').find(":selected").text();
        var conceptId = $('#searchboxDataOwner #owner_listing_search').find(":selected").attr("data-id");
        getPropOwnerName(conceptName,conceptId);
    });

    function getPropOwnerName(conceptName,conceptId) {
        $.ajax({
            type: 'POST',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                class: "lettersNoticesAjax",
                action: "getOwnerPropertyList",
                id:conceptId
                // data :window.location.pathname
            },
            success: function (response) {
                var html = ""
                var response = JSON.parse(response);
                /*console.log(response.data);*/
                if(response.data.length > 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        html += ' <option value="' + response.data[i].id + '">' + response.data[i].property_name + '</option>';
                    }
                    $("#ownerPropertyListAll").html("<option value='0'>Select</option>" + html);
                }else {
                    $("#ownerPropertyListAll").html("<option value='0'>This Owner has no Property</option>");
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }


    /**
     * jqGrid status changes
     */
    $(document).on('change','.common_ddl',function(){
        var grid = $("#letterNoticesOwner-table"),f = [];
        var owner_list = $('#owner_listing_search').val();
        var ownerPropertyListAll = $('#ownerPropertyListAll').val();
      /* // console.log(ownerPropertyListAll);debugger;
       // var status = $('#jqGridStatus').val();*/
        f.push({field: "owner_name.id", op: "eq", data: owner_list},{field: "property_name.id", op: "eq", data: ownerPropertyListAll});

        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    });



    //jqGrid status
    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        $('#letterNoticesOwner-table').jqGrid('GridUnload');
        jqGridOwner1(selected);
    });

    jqGridOwner1('All');
    function jqGridOwner1(status) {

        var table = 'owner_property_owned';
        var checkbox = '<input type="checkbox" id="checkAllletterLandLord" name="checkAllletterLandLord" checked="checked">';
        var columns = ['Landlord Name','Property Name','Tenant','Action',checkbox];
        var select_column = [];
       /* var joins = [{table:'owner_property_owned',column:'user_id',primary:'id',on_table:'users',as:'owner_name'},{table:'owner_property_owned',column:'property_id',primary:'id',on_table:'general_property',as:'property_name'},{table:'tenant_property',column:'user_id',primary:'id',on_table:'users',as:'tenant_name'}];*/
        var joins = [{table:'owner_property_owned',column:'user_id',primary:'id',on_table:'users',as:'owner_name'},{table:'owner_property_owned',column:'property_id',primary:'id',on_table:'general_property',as:'property_name'},{table:'owner_property_owned',column:'property_id',primary:'property_id',on_table:'tenant_property',as:'tenant_property'},{table:'tenant_property',column:'user_id',primary:'id',on_table:'users',as:'tenant_property_name'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [];
        var columns_options = [
            {name:'Landlord Name',index:'name',title:false, width:300,align:"center",searchoptions: {sopt: conditions},table:'owner_name',formatter: ownerName,alias:'landlord_name',classes: 'pointer',attr:[{name:'flag',value:'owner'}]},
            {name:'Property Name',index:'property_name', width:300,align:"center",searchoptions: {sopt: conditions},table:'property_name', classes: 'cursor',alias:'property_name'},
            {name:'Tenant',index:'name', width:255,align:"center",searchoptions: {sopt: conditions},table:'tenant_property_name',change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id',alias:'tenant_id'},
            {name:'Action',index:'id', title: false, width:250,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: pdfRedirect, edittype: 'select',search:true,table:table},
            {name: 'Id', index: 'id', width: 250, align: "center", sortable: false, searchoptions: {sopt: conditions}, table: table, search: false, formatter: actionCheckboxFmatter},

        ];
        var ignore_array = [];

        jQuery("#letterNoticesOwner-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns:extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'owner_property_owned.updated_at',
            sortorder: 'desc',
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Owners",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * Function to format owner name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ownerName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }
    }

    function actionCheckboxFmatter(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<input type="checkbox" name="checkLetter[]" class="checkLetter" id="checkLetter_' + rowObject.id + '" data_id="' + rowObject.id + '" checked="checked"/>';
        }
    }

    function pdfRedirect(cellValue, options, rowObject){

        if(rowObject !== undefined){
            // console.log(rowObject['editable']);
            var html = "";
            html += "<span data_id='"+rowObject.id+"' temp_id=''>" +
                "<a class='clsDownloadPDFFromSearch' data_type='download' href='javaScript:void(0);'>" +
                "<img src='"+webpath+"/company/images/pdfDownload.png'  class='pdfDownload_doc_icon' title='download' alt='my image' style='cursor: pointer; width: 18px;height: 18px;'/>" +
                "</a></span>" +
                "<span class='tempalateIdLetter' data_id='"+rowObject.id+"' temp_id=''><a class='clsDownloadPDFFromSearch' data_type='view' href='javascript:void(0);'>" +
                "<img src='"+webpath+"/company/images/PDF-Icon.png'  class='delete_doc_icon' title='Preview' alt='my image' style='cursor: pointer; width: 18px;height: 18px;'/></a></span>";

            return html;
        }
    }

    $(document).on('click','#LandLord-Documents-table tr',function(e){
        e.preventDefault();
        var id = $(this).closest("tr").attr('id');
        $("#searchTemplateIdOwner").val(id);
    });

    $(document).on('click','#letterNoticesOwner-table tr td .clsDownloadPDFFromSearch',function(e){

        var tenant_id = $(this).parents("tr").attr('id');
        var dataType = $(this).attr("data_type");
        var template_id = $("#searchTemplateIdOwner").val();
        getHtmlPdfConverter(tenant_id,template_id, dataType);
    });

    function getHtmlPdfConverter(tenant_id,template_id, dataType){
        $.ajax({
            type: 'post',
            url: '/LettersNoticesDoc-Ajax',
            data: {
                tenant_id: tenant_id,
                template_id:template_id,
                class:'lettersNoticesAjax',
                action:'getHtmlPdfConverter'
            },
            success: function (response) {
                var response = JSON.parse(response);


                if (response.status == 'success' && response.code == 200) {
                  /*  console.log("here");
                    console.log(response);*/
                    if (dataType == "download"){
                        var splitName = response.data.split("/");
                        var fileName = splitName.reverse();
                        var link=document.createElement('a');
                        document.body.appendChild(link);
                        link.target="_blank";
                        link.download=fileName[0];
                        link.href=response.data;
                        link.click();
                    }else{
                        window.open(response.data, "_blank");
                    }

                    setTimeout(function () {
                    }, 2000);
                } else if(response.code == 500) {
                    toastr.warning(response.message);
                } else {
                    toastr.error(response.message);
                }

            }
        });
    }
    $("#checkAllletterLandLord").click(function () {
        $(".checkLetter").prop('checked', $(this).prop('checked'));
    });
var testClass;
    $(document).on('click','#active_tabs .active_tab',function(e){
        testClass = $(this).hasClass("esignBtn");
        //testUser(testClass);
    });

    //function testUser(testClass) {
        $(document).on("click", '#sendMailLetterLandLord', function () {
            $(".loader").show();
            var searchTemplateId = $(".searchbox_ownerPropertyAll #searchTemplateIdOwner").val();
            // alert(searchsad);debugger;
            favorite = [];
            var no_of_checked = $('#letterNoticesOwner-table [name="checkLetter[]"]:checked').length

            if (no_of_checked == 0) {
                toastr.error('Please select atleast one LandLord.');
                return false;
            }
            $.each($("#letterNoticesOwner-table input[name='checkLetter[]']:checked"), function () {
                favorite.push($(this).attr('data_id'));
            });
            if (testClass){
                var actionName = 'getTemplateDataEmail2';
            }else{
                var actionName = 'getTemplateDataEmail';
            }
            $.ajax({
                type: 'post',
                url: '/LettersNoticesDoc-Ajax',
                data: {
                    class: 'lettersNoticesAjax',
                    action: actionName,
                    'tenantTemplate_ids': favorite,
                    'searchTemplateId': searchTemplateId,
                    'esignClass':testClass
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    /* if (response.status == 'success' && response.code == 200) {
                         $("#print_hoa").modal('show');
                         $("#modal-body-hoa").html(response.html)
                     } else {
                         toastr.warning('Record not updated due to technical issue.');
                     }*/
                }
            });
        });
    //}

});