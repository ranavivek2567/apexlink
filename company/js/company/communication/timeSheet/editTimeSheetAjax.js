editTimeSheet();

jQuery('#employeePhone').mask('000-000-0000', {reverse: true});
/**
 * function to get time sheet details.
 */
function editTimeSheet(){
    $.ajax({
        type: 'post',
        url: '/timeSheet-Ajax',
        data: {
            class: 'TimeSheetAjax',
            action: 'getTimeSheetDetail',
            id: getParameterByName('id')
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.code == 200){
                setTimeout(function () {
                    edit_date_time(response.data.updated_at);
                },1000);
                $.each(response.data, function (key, value) {
                     if(key == 'employee_id'){
                         $("#employee_id").val(value);
                     } else if(key == 'email') {
                         $("#employeeEmail").val(value).attr('readonly',true);
                     } else if(key == 'position'){
                         $("#position").val(value).attr('readonly',true);
                     } else if(key == 'id'){
                        $('#editTimeSheetId').val(value);
                     }
                     else if(key == 'note'){
                         $('#note').text(value);
                     }else {
                         $("input[name=" + key + "]").val(value);
                     }
                });
            }
        }
    });
}

function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}