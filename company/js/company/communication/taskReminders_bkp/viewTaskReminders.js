/*  $('tr td:not(:last-child)');*/
/**
 * jqGrid Initialization function
 * @param status
 */
jqGrid('All');
function jqGrid(status, deleted_at) {
    var table = 'task_reminders';
    var columns = ['Title','Property','Building', 'Unit Number','Status','Assigned To','Due Date','Action'];
    var select_column = ['Edit','View','Delete'];
    var joins = [{table:'task_reminders',column:'property',primary:'id',on_table:'general_property'},{table:'task_reminders',column:'building',primary:'id',on_table:'building_detail'},{table:'task_reminders',column:'assigned_to',primary:'id',on_table:'users'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [];
    var columns_options = [
        {name:'Title',index:'title',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name: 'Property', index: 'property_name', width: 100,title:false, searchoptions: {sopt: conditions}, table: 'general_property', classes: 'pointer',formatter: propertyName},
        {name: 'Building', index: 'building_name', width: 80, searchoptions: {sopt: conditions}, table: 'building_detail', classes: 'pointer'},
        {name:'Unit',index:'unit', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
        {name:'Assigned To',index:'name', width:80,align:"center",searchoptions: {sopt: conditions},table:'users'},
        {name:'Due Date',index:'due_date', width:80,align:"center",change_type:'date',searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
    ];
    var ignore_array = [];
    jQuery("#task-reminder-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'task_reminders.updated_at',
        sortorder: 'desc',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Employees",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}

/**
 *  function to format employee name
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function propertyName(cellValue, options, rowObject) {
    // return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';

    if(rowObject !== undefined) {

            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';



    }
}



/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function statusFmatter (cellvalue, options, rowObject){
    //  alert(rowObject);
    if (cellvalue == 1)
        return "Not Assigned";
    else if(cellvalue == 2)
        return "Not Started";
    else if(cellvalue == 3)
        return "In Progress";
    else if(cellvalue == 4)
        return "Completed";
    else if(cellvalue == 5)
        return "Canceled and Resigned";
}


function statusFmatter1 (cellvalue, options, rowObject){
    //alert(cellvalue);
    // alert(rowObject);
    if(rowObject !== undefined) {
        var select = '';
         select = ['Edit', 'View', 'Delete'];;
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        // alert(data);
        return data;
    }
}


$(document).on("change", "#task-reminder-table .select_options", function (e) {
//  e.preventDefault();
var base_url = window.location.origin;
var action = this.value;
var id = $(this).attr('data_id');
switch (action) {
    case "Edit":
        getTaskReminderById(id);
        $("#add_task_reminer_modal").modal("show");
        break;
    case "View":
        $("#add_task_reminer_modal").modal("show");
        break;
    case "Delete":
        bootbox.confirm("Are you sure you want to delete this record?", function (result) {
            if (result == true) {
                deleteTaskReminder(id);
            }
        });
        break;
    default:
        window.location.href = base_url + '/Communication/NewTaskAndReminders';
}
$('.select_options').prop('selectedIndex',0);
});

function getTaskReminderById(id) {
    $.ajax({
        type: 'post',
        url: '/employeeListAjax',
        data: {
            class: "EmployeeListAjax",
            action: "deleteEmployee",
            user_id: id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
                $('#employee-table').trigger( 'reloadGrid' );
                alphabeticSearch();
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });
}


function deleteTaskReminder(id) {
    $.ajax({
        type: 'post',
        url: '/employeeListAjax',
        data: {
            class: "EmployeeListAjax",
            action: "deleteEmployee",
            user_id: id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
                $('#employee-table').trigger( 'reloadGrid' );
                alphabeticSearch();
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });
}

$(document).on('click','#task-reminder-table tr td',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    if ($(this).index() == 7 ) {
        return false;
    }else{
        $("#add_task_reminer_modal").modal("show");
    }
});
