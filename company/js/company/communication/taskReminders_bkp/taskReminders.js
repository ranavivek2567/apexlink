$(document).ready(function () {

    var unit_popup = false;
    $(document).on("click",".add_announcement_cancel",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Announcement/Announcements';
            }
        });

    });

    $(document).on("click",".blue-btn",function(){
      //  $('input[name="unit_checkbox[]"]').removeAttr('checked');

            var valu = '';
            var data = '';
            var unit_ids_array = [];
            $('input[name="unit_checkbox[]"]:checked').each(function(index,val) {
                var $this = $(this);
              //  val = index + 2;
                valu = $this.attr("data-label");
                data +=   valu +',';
                unit_ids_array.push($this.attr("value"));
            });
           data  = data.replace(/,\s*$/, "");
           $("#unit_ids").val(unit_ids_array);
           $("#unit").val(data);
        $("#myModal").modal("hide");

    });

    $(document).on("click",".unit_chekbox",function(){
     //   var total=$(this).find('input[name="unit_checkbox[][]"]:checked').length;
        var total_checkbox = $('input[name="unit_checkbox[]"]').length;
        var checkedBoxes = $('input[name="unit_checkbox[]"]:checked').length;
       if(total_checkbox == checkedBoxes){
           $('.select_all_checkbox').attr('checked',true);
       }
    });


    $(document).on("focus", "#unit", function () {
        if(unit_popup == true) {
            $('input[name="unit_checkbox[]"]:checked').each(function(index,val) {
                var $this = $(this);
                $this.removeAttr("checked");
            });
            $("#myModal").modal("show");
        }
    });


    $(document).on("click",".close,.cancel_unit",function(){
        $("#myModal").modal("hide");
    });
    $('#product-options').modal('hide');


    $.ajax({
        type: 'post',
        url: '/Announcement-Ajax',
        data: {
            class: "taskReminderAjax",
            action: "fetchPropertymanagers"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.propertyList.length > 0){
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.propertyList, function (key, value) {
                        propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
                    $('.property').html(propertyOption);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        }
    });

    $.ajax({
        type: 'post',
        url: '/Announcement-Ajax',
        data: {
            class: "taskReminderAjax",
            action: "getInitialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.propertyList.length > 0){
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.propertyList, function (key, value) {
                        propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
                    $('.property').html(propertyOption);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        }
    });
    $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});

    $(document).on("change",".property",function (e) {
        e.preventDefault();
        $('.building').empty();
        $('.unit').empty();
        if($(this).val() != '') {
            console.log('aaaa', $(this).val());
            getDataByID($(this).val(), '', 'property');
            unit_popup = true;
        } else {
            $('.building').html("<option value=''>Select</option>");
            $('.unit').html("<option value=''>Select</option>");
        }

        // getDataByID($('.announcement_for').val(),$(this).val(),'users');
        // $('.user_name').empty();
        // return false;
    });

    $(document).on("change",".building",function (e) {
        e.preventDefault();
        getDataByID($(this).val(),$('.property').val(),'building');
        return false;
    });

    $(document).on("change",".announcement_for",function (e) {
        e.preventDefault();
        getDataByID($(this).val(),$('.property').val(),'users');
        return false;
    });


    function getDataByID(data_id, prev_id, data_type){
        if (data_id == '' || data_id == 'null'){
            return;
        }
        var newvar = [];
        $.ajax({
            type: 'post',
            url: '/taskReminder-Ajax',
            data: {
                class: "taskReminderAjax",
                action: "getDataByID",
                id: data_id,
                prev_id: prev_id,
                data_type : data_type
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {

                    if(data_type == 'property') {
                        var buildingOption = "<option value=''>Select</option>";
                        $('.unit').html(buildingOption);
                        if (data.data.length > 0) {
                            $.each(data.data, function (key, value) {
                                buildingOption += "<option value='" + value.id + "' data-id='" + value.building_id + "'>" + value.building_name + "</option>";
                            });
                        }
                        $('.building').html(buildingOption);
                    } else if(data_type == 'building') {
                        var unitOption = "<option value=''>Select</option>";
                        var unitCheckboxHtml ="";
                        if (data.data.length > 0) {
                            $.each(data.data, function (key, value) {
                                unitOption += "<option value='" + value.id + "' data-id='" + value.id + "'>" + value.unit_prefix + "</option>";
                                unitCheckboxHtml += '<div class="check-outer"><input type="checkbox" name="unit_checkbox[]" class="unit_chekbox" data-label="'+value.unit_prefix+'" value="'+ value.id+'"/><label>'+value.unit_prefix+'</label></div>';
                            });
                        }else{
                            var unitCheckboxHtml = "<span>No Unit Present</span>";
                            $('.unit_select_all_checkbox_html').css({"display":"none"});
                        }
                        console.log(unitCheckboxHtml);
                        $('.unit').html(unitOption);
                        $('.unit_checkbox_html').html(unitCheckboxHtml);
                    } else if(data_type == 'users'){
                        var usersOption = "";
                        if (data.data.length > 0){
                            $.each(data.data, function (key, value) {
                                usersOption += "<option value='"+value.id+"'>"+value.name+"</option>";
                            });
                            $('select[name="user_name[]"]').html(usersOption);
                            $('select[name="user_name[]"]').multiselect('destroy');
                            $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                            if(data.data)
                            {
                                newvar = data.data;
                            }


                        } else {
                            $('select[name="user_name[]"]').html('');
                            $('select[name="user_name[]"]').multiselect('destroy');
                            $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});

                        }

                    }

                } else if (data.status == "error") {

                    toastr.error(data.message);
                } else {

                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });


    }
    $('#add_announcement_div .start_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#add_announcement_div .end_date").datepicker("option", "minDate", selectedDate);
        }
    }).datepicker("setDate", new Date());

    $('#add_announcement_div .end_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#add_announcement_div .start_date").datepicker("option", "maxDate", selectedDate);
        }
    }).datepicker("setDate", new Date());

    $('.start_time').timepicker({
        timeFormat: 'h:mm p',
        interval: 5,
        // minTime: '2',
        // maxTime: '6:00pm',
        defaultTime: '8',
        startTime: '8:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('.end_time').timepicker({
        timeFormat: 'h:mm p',
        interval: 5,
        // minTime: '2',
        // maxTime: '6:00pm',
        defaultTime: '10',
        startTime: '10:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });


    //add announcement
    $("#add_announcement_form").validate({
        rules: {
            announcement_for: {
                required: true
            },
            'user_name[]': {
                required: true
            },
            title: {
                required: true
            },
            start_date: {
                required: true
            },
            start_time: {
                required: true
            },
            end_date: {
                required: true
            },
            end_time: {
                required: true
            },
            description: {
                required: true
            }
        },
        submitHandler: function (form) {
            event.preventDefault();

            var timezone_offset_minutes = new Date().getTimezoneOffset();
            timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

            var form = $('#add_announcement_form')[0];
            var formData = new FormData(form);
            var data = convertSerializeDatatoArray();


            $.each(fileLibrary, function (key, value) {
                if (compareArray(value, data) == 'true') {
                    formData.append(key, value);
                }
            });

            formData.append('class', 'announcementAjax');

            formData.append('action', 'addAnnouncement');
            formData.append('timezone', timezone_offset_minutes);
            // var formData = $('#add_announcement_form').serializeArray();
            console.log('formData>>', formData);

            $.ajax({
                url: '/Announcement-Ajax',
                type: 'post',
                data:  formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        localStorage.setItem("Message","Record created successfully.")
                        localStorage.setItem("rowcolor",true)
                        // toastr.success("Announcement Created Successfully.");
                        window.location.href = '/Announcement/Announcements';
                    }else if(response.status == 'error' && response.code == 502){
                        toastr.error(response.message);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                }
            });
        }
    });




    /*** <h3>File Library Div JS Start</h3> ***/
    $(document).on('click','.add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });

    var file_library = [];
    var imgArray = [];
    $(document).on('change','.file_library',function(){
        // file_library = [];
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            if(size > 4097) {
                toastr.warning('Please select documents less than 4 mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
                if (type == 'image/jpeg' || type == 'image/jpg' || type == 'image/png' || type == 'image/gif' || type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                    if($.inArray(value['name'], imgArray) === -1)
                    {
                        file_library.push(value);
                    }
                    var src = '';
                    var reader = new FileReader();
                    //  $('#file_library_uploads').html('');
                    reader.onload = function (e) {
                        if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            src = upload_url + 'company/images/excel.png';
                        } else if (type == 'application/pdf') {
                            src = upload_url + 'company/images/pdf.png';
                        } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                            src = upload_url + '/company/images/word_doc_icon.jpg';
                        } else if (type == 'text/plain') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else if (type == 'text/xml') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else {
                            src = e.target.result;
                        }
                        if($.inArray(value['name'], imgArray) === -1)
                        {
                            $('.remove_library_file').attr('disabled',false);
                            $('.SaveAllimagesFileOwner').attr('disabled',false);
                            $("#file_library_uploads").append(
                                '<div class="row" style="margin:0 20px">' +
                                '<div class="col-sm-12 img-upload-library-div" style="margin: 0">' +
                                '<div class="col-sm-4"><img class="img-upload-tab' + key + '" width=40 height=40 src=' + src + '></div>' +
                                '<div style="margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-name ' + key + '">' + value['name'] + '</div>' +
                                '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                                '<div  style="    margin-top: 10px;" class="col-sm-4 center_img show-library-list-imgs-size' + key + '">' + size + '</div>' +
                                '</div></div>');
                            imgArray.push(value['name']);
                        } else {
                            toastr.warning('File already exists!');
                        }
                    };
                    reader.readAsDataURL(value);
                } else {
                    toastr.warning('Please select file with .jpg | .png | .jpeg | .gif | .xlsx | .pdf | .docx | .txt | .xml extension only!');
                }
            }
        });
    });

    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }

    $(document).on('click','.remove_library_file',function(){
        bootbox.confirm("Do you want to remove all files?", function (result) {
            if (result == true) {
                toastr.success('The record deleted successfully.');
                $('.file_library_uploads').html('');
                $('.file_library').val('');
            }
        });

    });
    /*** <h3>File Library Div JS Ends</h3> ***/


    /*** <h3>Edit Announcement Div JS Starts</h3> ***/
    var edit_active =  localStorage.getItem("edit_active");
    if(edit_active){
        $('#add_announcement_div').hide();
        $('#edit_announcement_div').show();
    } else {
        $('#edit_announcement_div').hide();
        $('#add_announcement_div').show();
    }

    jqGridFileLibrary();

    function jqGridFileLibrary(status) {
        // var property_id = ($("#property_editunique_id").val() !== undefined)?$("#property_editunique_id").val():property_unique_id;
        var property_id = '1';
        var table = 'announcement_file_library';
        var columns = ['Name','Preview','Location','Action'];
        var select_column = ['Email','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [];
        // var extra_where = [{column:'file_type',value:'2',condition:'='},{column:'property_id',value:id,condition:'='}];
        var columns_options = [
            {name:'Name',index:'filename',width:400,align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Preview',index:'file_extension',width:450,align:"left",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
            {name:'Location',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
            {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,title:false}
        ];
        var ignore_array = [];
        jQuery("#announcementFileLibrary-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            sortname: 'updated_at',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Files",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }


    /**
     * function to format document
     * @param cellvalue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function imageFormatter(cellvalue, options, rowObject){

        if(rowObject !== undefined) {
            var path = upload_url+'company/'+rowObject.Location;
            var src = '';
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
            } else if (rowObject.Preview == 'docx') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
            } else {
                src = path;
            }
            return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=30 height=30 src="' + src + '">';
        }
    }

    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
    var edit_id =  getParameterByName('id');
    if(edit_id){
        console.log('id>>', edit_id);
        $.ajax({
            type: 'post',
            url: '/Announcement-Ajax',
            data: {
                class: "announcementAjax",
                action: "getAnnouncementById",
                id: edit_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var res = response.data;
                    getDataByID(res.property, '', 'property');       //Building listing
                    getDataByID(res.property,res.building,'building');       //Unit listing
                    var userData = getDataByID(res.announcement_for,res.property,'users');  //Users listing

                    console.log('getAnnouncementById>>', response);
                    console.log('userData>>', userData);
                    setTimeout(function () {
                        $.each(res, function (key, value) {
                            $('#edit_announcement_div .'+key).val(value);
                            if (key == 'start_time') {
                                $('#edit_announcement_div .start_time').timepicker('setTime', value);
                            }
                            if (key == 'end_time') {
                                $('#edit_announcement_div .end_time').timepicker('setTime', value);
                            }
                            else if(key == 'user_name')
                            {
                                var userDataInitial = [];
                                $('#edit_announcement_div .user_name option').each(function (key,value) {
                                    userDataInitial.push({'id':$(this).val(),'text':$(this).text()});
                                });

                                selectUserNames(userDataInitial,value);
                            }

                        });

                    }, 300);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    function selectUserNames(userDataInitial,value) {

        if (userDataInitial.length > 0){
            var nameOption = "";

            $.each(userDataInitial, function (nameKey, nameData) {
                if($.inArray(nameData.id, value) !== -1) {
                    nameOption += "<option value='"+nameData.id+"' selected>"+nameData.text+"</option>";
                } else {
                    nameOption += "<option value='"+nameData.id+"'>"+nameData.text+"</option>";
                }
            });
            setTimeout(function () {
                $("#user_name_e").multiselect("clearSelection");
                $('#user_name_e').html(nameOption);

                $('#user_name_e').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                $("#user_name_e").multiselect( 'refresh' );
            },400);
        }
    }


    $('#start_date_edit').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#end_date_edit").datepicker("option", "minDate", selectedDate);
            $("#start_date_edit").val(selectedDate);
        }
    });

    $('#end_date_edit').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#start_date_edit").datepicker("option", "maxDate", selectedDate);
            $("#end_date_edit").val(selectedDate);
        }
    });

    //edit announcement
    $("#edit_announcement_form").validate({
        rules: {
            announcement_for: {
                required: true
            },
            'user_name[]': {
                required: true
            },
            title: {
                required: true
            },
            start_date: {
                required: true
            },
            start_time: {
                required: true
            },
            end_date: {
                required: true
            },
            end_time: {
                required: true
            },
            description: {
                required: true
            }
        },
        submitHandler: function (form) {
            event.preventDefault();

            var timezone_offset_minutes = new Date().getTimezoneOffset();
            timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

            var form = $('#edit_announcement_form')[0];
            var formData = new FormData(form);
            var data = convertSerializeDatatoArray();


            $.each(fileLibrary, function (key, value) {
                if (compareArray(value, data) == 'true') {
                    formData.append(key, value);
                }
            });

            formData.append('class', 'announcementAjax');

            formData.append('action', 'addAnnouncement');
            formData.append('timezone', timezone_offset_minutes);
            // var formData = $('#add_announcement_form').serializeArray();
            console.log('formData>>', formData);

            $.ajax({
                url: '/Announcement-Ajax',
                type: 'post',
                id  : edit_id,
                data:  formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        localStorage.setItem("Message","Record updated successfully.")
                        localStorage.setItem("rowcolor",true)
                        // toastr.success("Announcement Created Successfully.");
                        window.location.href = '/Announcement/Announcements';
                    }else if(response.status == 'error' && response.code == 502){
                        toastr.error(response.message);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                }
            });
        }
    });

    /*** <h3>Edit Announcement Div JS Ends</h3> ***/

});