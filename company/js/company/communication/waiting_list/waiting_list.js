$(document).ready(function () {

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});
    $(document).on("click",".goback_func",function () {
        window.history.back();
    });

    if(localStorage.getItem("table_green_tableid")){
        setTimeout(function(){
            var tableId = localStorage.getItem("table_green_tableid")
            jQuery(tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery(tableId).find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('table_green_id');
            localStorage.removeItem('table_green_tableid');
        }, 2000);
    }

    $(document).on("click", "#waitingBtnPopup", function () {

      //  $('.postal_code').focusout();
        $('#save_list').val('save');
        $('.hidden_form').val('save');
        $('.company').hide();
        $('#waiting_form').trigger('reset');
        $("#postal_code_zip").val("10005").trigger("focusout");
        $('#waitingListPopup').modal('show')

    });

    $(document).on("click", ".cancel_Popup", function () {
        bootbox.confirm("Do you want to cancel this action?", function (result) {
            if (result == true) {
                $('#waitingListPopup').modal('hide')
            }
        });

    });
    $(document).on("click", "#check", function () {
        if ($("#check").is(":checked")) {
            $('.company').show();
            $("#company-error").show();
        } else {
            $('.company').hide();
        }
    });
    $(document).on('blur','.company_name',function () {
        if($(this).val() !==""){
        $("#company-error").hide();
        }
    });
    waitinglist();
    function waitinglist() {
        var idd= $('.hidden_form2').val();

        var table = 'users';
        var columns = ['Lead Name','Date', 'Phone Number', 'Email ID','Preferred','Pets','Notes','Property Name','unit_no ','Building','Unit','Action'];
        var select_column = ['Edit','Delete','Convert To Contact','Convert To Tenant','Flag Bank','Email','Text'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column: 'user_type', value: '11', condition: '='}];
        var extra_columns = ['users.deleted_at', 'users.updated_at'];
        var joins = [{table: 'users', column: 'id', primary: 'user_id', on_table: 'waiting_list'},{table: 'waiting_list', column: 'property_id', primary: 'id', on_table: 'general_property'},{table: 'waiting_list', column: 'property_id', primary: 'id', on_table: 'building_detail'},{table: 'waiting_list', column: 'unit_id', primary: 'id', on_table: 'unit_details'}
          ];
        var columns_options = [
            {name:'Lead Name',index:'name', width:100,searchoptions: {sopt: conditions},table:table,formatter:colorformatter1},
            {name:'Date',index:'created_at', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Phone Number',index:'phone_number', width:200,searchoptions: {sopt: conditions},table:table},
            {name:'Email ID',index:'email', width:150,searchoptions: {sopt: conditions},table:table},
            {name:'Preferred',index:'preferred_id', width:100,searchoptions: {sopt: conditions},table:'waiting_list',formatter:colorformatter},
            {name:'Pets',index:'pet_id', width:200,searchoptions: {sopt: conditions},table:'waiting_list',formatter:colorformatter},
            {name:'Notes',index:'notes', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Property Name',index:'property_name', width:100,searchoptions: {sopt: conditions},table:'general_property',formatter:colorformatter1},
            {name:'Unit_no',index:'unit_no',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Building',index:'building_name', width:100,searchoptions: {sopt: conditions},table:'building_detail'},
            {name:'Unit',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:'Action',index:'', width:200,title:false,searchoptions: {sopt: conditions},table:table,formatter:statusFmatter1},
        ];

        var ignore_array = [];
        jQuery("#waiting_list").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Names",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

    //
    function statusFmatter1(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = ['Edit','Delete','Convert To Contact','Convert To Tenant','Flag Bank','Email','Text'];

            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    function colorformatter(cellvalue, options, rowObject) {
        if(cellvalue != undefined){
            var html="";
            html="<span style='text-decoration: underline;color: #FF7F50; font-weight: bold;'>"+cellvalue+"</span>"
            return html;
        }
    }

    function colorformatter1(cellvalue, options, rowObject) {
        if(cellvalue != undefined){
            var html="";
            html="<span style='text-decoration: underline;color: #05A0E4; font-weight: bold;'>"+cellvalue+"</span>"
            return html;
        }
    }

    $(document).on('change', '.select_options', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);
        var base_url = window.location.origin;

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var email = $("tr#"+id).find("td").eq(3).text();
        var name = $("tr#"+id).find("td").eq(0).text();

        var user_id = $(this).attr('data-user_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        var is_default = $(this).attr('is_default');
        if (opt == 'Edit' || opt == 'EDIT') {
            $('#save_list').val('Update');
            $('.hidden_form2').val(id);
            $('.hidden_form').val('Update');
            $.ajax({
                type: 'post',
                url: '/waiting-list-ajax',
                data: {
                    class: 'waitinglist',
                    action: 'getAllProperties',

                },
                success: function (response) {
                    var res = JSON.parse(response);
                    $('.property').html(res.html);
                    setTimeout(function () {
                        viewedit(id);
                    },100);
                },
            });

            $('#waitingListPopup').modal('show');


        }
        if (opt == 'Delete' || opt == 'Delete') {
            bootbox.confirm("Are you sure you want to delete this entry?", function (result) {
                if (result == true) {
                    deletelist(id);

                } else {
                    $('#waiting_list').trigger( 'reloadGrid' );
                }
            });
        }
        if (opt == 'Convert To Contact' || opt == 'Convert To Contact') {
          //  console.log('base',base_url);
            window.location.href = base_url + '/People/AddContact?id='+id;
        }
        if (opt == 'Convert To Tenant' || opt == 'Convert To Tenant') {

            window.location.href = base_url + '/Tenantlisting/add?id='+id;
            getRedirectionData(id);
        }
        if (opt == 'Flag Bank' || opt == 'Flag Bank'){

            window.location.href = base_url + '/Tenant/ViewEditWatingList?id='+id;
            localStorage.setItem("waitingname", name);
        }
        if (opt == 'Email' || opt == 'EMAIL'){

            localStorage.setItem('predefined_mail',email);
            localStorage.setItem('table_green_id',id);
            localStorage.setItem('table_green_tableid', '#waiting_list');
            localStorage.setItem('table_green_url','/Communication/WaitingList');
            window.location.href = base_url + '/Communication/ComposeEmail';
        }
        if (opt == 'Text' || opt == 'TEXT'){
            localStorage.setItem('table_green_id',id);
            localStorage.setItem('table_green_tableid', '#waiting_list');
            localStorage.setItem('table_green_url','/Communication/WaitingList');
            localStorage.setItem('predefined_text',email);
            window.location.href = base_url + '/Communication/AddTextMessage';
        }

    });


    $(document).on('click','#save_list',function () {
        if($('.hidden_form').val() == 'save') {
            if($('#waiting_form').valid()){
            addwaitinglist();
            }
        }   else if($('.hidden_form').val() == 'Update') {

            var idd=$('.hidden_form2').val();
            console.log('ddd',idd);
            if($('#waiting_form').valid()) {
                editwaitinglist(idd);
            }
        }
    });


    function addwaitinglist(){

        var formData = $('#waitingListPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/waiting-list-ajax',
            data: {
                class: 'waitinglist',
                action: 'newWaitinglist',
                formData : formData,
            },
            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {
                    toastr.success("This record saved successfully");
                    onTop(true);
                    setTimeout(function () {
                        $('#waitingListPopup').modal('hide');
                    },600);
                    $('#waiting_list ').trigger( 'reloadGrid' )
                }
                else {
                    toastr.error(response.message);
                }
            }
        });
    }




    function editwaitinglist(id){

        var formData = $('#waitingListPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/waiting-list-ajax',
            data: {
                class: 'waitinglist',
                action: 'edit_list',
                formData : formData,
                id : id
            },
            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success") {

                    toastr.success("This record updated successfully");
                    onTop(true);
                    setTimeout(function () {
                        $('#waitingListPopup').modal('hide');
                    },600);

                    $('#waiting_list ').trigger( 'reloadGrid' )

                }
                else {

                }
            }
        });
    }


    $(document).on('click','#waitingBtnPopup',function () {
        $.ajax({
            type: 'post',
            url: '/waiting-list-ajax',
            data: {
                class: 'waitinglist',
                action: 'getAllProperties',

            },
            success: function (response) {
                var res = JSON.parse(response);
                $('.property').html(res.html);
            },
        });
    });

     $(document).on('change','.property',function () {
         var idd= $('.property').val();
         $.ajax({
             type: 'post',
             url: '/waiting-list-ajax',
             data: {
                 class: 'waitinglist',
                 action: 'getAllBuildings',
                 id : idd,
             },
             success: function (response) {
                 var res = JSON.parse(response);
                 $('.building').html(res.html1);

             },
         });
     });

     $(document).on('change','.building',function () {
         var idd= $('.building').val();
         $.ajax({
             type: 'post',
             url: '/waiting-list-ajax',
             data: {
                 class: 'waitinglist',
                 action: 'getAllUnits',
                 id : idd,
             },
             success: function (response) {
                 var res = JSON.parse(response);
                 $('.unit').html(res.html);
             },
         });
     });

    function deletelist(id) {
        $.ajax({
            type: 'post',
            url: '/waiting-list-ajax',
            data: {
                class: 'waitinglist',
                action: 'deletelist',
                id : id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#waiting_list').trigger('reloadGrid');
                    onTop(true);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    function viewedit(id){

        $.ajax({
            type: 'post',
            url: '/waiting-list-ajax',
            data: {
                class: 'waitinglist',
                action: 'getlistdata',
                id : id,
            },

            success: function (response) {
                var data = JSON.parse(response);


                if (data.status == "success"){

                    //$('.building').append($('<option value="'+data.data.property+'">'+data.data.property+'</option>').attr('selected',true))
                    $('.property').val(data.data.property).trigger('change');
                    setTimeout(function () {
                        $('.building').val(data.data.building);
                        $('.unit').val(data.data.unit);
                    },520);
                   // $('.unit').append($('<option value="'+data.data.unit+'">'+data.data.unit+'</option>').attr('selected',true))
                    $('#preferred').val(data.data.preferred_id);
                    $('#pets').val(data.data.pet_id);
                    $('#first').val(data.data1.first_name);
                    $('#last').val(data.data1.last_name);
                    edit_date_time(data.data1.updated_at);
                    $.each(data.data, function (key,value) {

                        $('.'+key).val(value);

                    });


                }
            }
        });
    }



    $("#waiting_form").validate({
        rules: {
            'first_name': {required: true},
            'last_name': {required: true},


        }
    });

    function getRedirectionData(id) {
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "getRedirectionData",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                $("#property").val(response.data2.prop_id).trigger("change");
                setTimeout(function () {
                    $("#building").val(response.data2.build_id).trigger("change");
                },500);
                setTimeout(function () {
                    $("#unit").val(response.data2.unit_id);
                },600);
                $("#salutation").val(response.data.salutation);
                $("#firstname").val(response.data.first_name);
                $("#lastname").val(response.data.last_name);
                $("#maidenname").val(response.data.maiden_name);
                $("#nickname").val(response.data.nick_name);
                $("#middlename").val(response.data.mi);
                $("#phoneType12").val(response.data.phone_type);
                $("#carrierCheck").val(response.data.carrier);
                $("#countryCodeCheck").val(response.data.code);
                $("#phoneNumberCheck").val(response.data.phone_number);
                $("#emailCheck").val(response.data.email);
                $("#referralSourceCheck").val(response.data.referral_source);
                var dateParts = response.data2.exp_move_in.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var exp_move_in= $.datepicker.formatDate(jsDateFomat, jsDate);
                $("#moveInDate").val(exp_move_in);
                $("#ethncityCheck").val(response.data.ethnicity);
                $("#maritalStatusCheck").val(response.data.maritial_status);
                $("#veteranStatusCheck").val(response.data.veteran_status);
                $("#ssn").val(response.data.ssn_sin_id);
                $("#emergency").val(response.data3.emergency_contact_name);
                $("#relationship").val(response.data3.emergency_relation);
                $("#emergency_countryCheck").val(response.data3.emergency_country_code);
                $("#phoneNumber").val(response.data3.emergency_phone_number);
                $("#email1").val(response.data3.emergency_email);



            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    $(function () {       $('.postal_code').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });       $(".postal_code").focusout(function () {
        getZipCode('.postal_code',$(this).val(),'.city','.state','.country','','#zip_code_validate');
        // getAddressInfoByZip($(this).val());
    });       $(".postal_code").keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            getZipCode('.postal_code',$(this).val(),'.city','.state','.country','','#zip_code_validate');
        }
    });
    });




});