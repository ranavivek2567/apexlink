/*  $('tr td:not(:last-child)');*/
/**
 * jqGrid Initialization function
 * @param status
 */
jqGrid('All');
function jqGrid(status, deleted_at) {
   // alert(login_user);
    var table = 'task_reminders';
    var columns = ['Title','User ID','Property','Building','unit_ids','Unit Number','Status','Assigned To','Due Date','Action'];
    var select_column = ['Edit','View','Delete'];
    var joins = [{table:'task_reminders',column:'property',primary:'id',on_table:'general_property'},{table:'task_reminders',column:'building',primary:'id',on_table:'building_detail'},{table:'task_reminders',column:'assigned_to',primary:'id',on_table:'users'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'status',value:4,condition:'!=',table:table}];
    var columns_options = [
        {name:'Title',index:'title',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'User ID',hidden:true, index: 'user_id', width: 100,title:false, searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
        {name:'Property', index: 'property_name', width: 100,title:false, searchoptions: {sopt: conditions}, table: 'general_property', classes: 'pointer',formatter: propertyName},
        {name:'Building', index: 'building_name', width: 80, searchoptions: {sopt: conditions}, table: 'building_detail', classes: 'pointer'},
        {name:'unit_ids', hidden:true,index: 'unit_ids', width: 80, searchoptions: {sopt: conditions}, table: 'task_reminders', classes: 'pointer'},
        {name:'Unit',index:'unit', width:80, align:"center",searchoptions: {sopt: conditions},table:table, change_type: 'serialize', secondTable: 'unit_details', index2: 'unit_prefix', update_column: 'unit', original_index: 'unit_ids', classes: 'pointer',type:'task&r'},
        {name:'Status',index:'status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
        {name:'Assigned To',index:'name', width:80,align:"center",searchoptions: {sopt: conditions},table:'users'},
        {name:'Due Date',index:'due_date', width:80,align:"center",change_type:'date',searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
    ];
    var ignore_array = [];
    jQuery("#task-reminder-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'task_reminders.updated_at',
        sortorder: 'desc',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Tasks and Reminders",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}


/**
 *  function to format employee name
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function propertyName(cellValue, options, rowObject) {
    // return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';

    if(rowObject !== undefined) {

            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';



    }
}



/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function statusFmatter (cellvalue, options, rowObject){
    //  alert(rowObject);
    if (cellvalue == 1)
        return "Not Assigned";
    else if(cellvalue == 2)
        return "Not Started";
    else if(cellvalue == 3)
        return "In Progress";
    else if(cellvalue == 4)
        return "Completed";
    else if(cellvalue == 5)
        return "Canceled and Resigned";
}


function statusFmatter1 (cellvalue, options, rowObject){
    //alert(cellvalue);
    // alert(rowObject);
    console.log(rowObject);
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject['User ID'] == login_user)  select = ['Edit', 'View', 'Delete'];
        if(rowObject['User ID'] != login_user)  select = ['View'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

$.ajax({
    type: 'post',
    url: '/taskReminder-Ajax',
    data: {
        class: "taskReminderAjax",
        action: "fetchPropertymanagers"
    },
    success: function (response) {
        var data = $.parseJSON(response);
        if (data.status == "success") {

            if (data.data.length > 0){
                $('.assigned_to').html(data.data);
            }

        } else if (data.status == "error") {
            toastr.error(data.message);
        } else {
            toastr.error(data.message);
        }
    }
});

$(document).on("change", "#task-reminder-table .select_options", function (e) {
//  e.preventDefault();
var base_url = window.location.origin;
var action = this.value;
var id = $(this).attr('data_id');
switch (action) {
    case "Edit":
        getTaskReminderById(id);
        $("#task_reminder_edit_id").val(id);
        $("#add_task_reminer_modal").modal("show");
        break;
    case "View":
        getTaskReminderById(id);
        $("#task_reminder_edit_id").val(id);
        $("#add_task_reminer_modal").modal("show");
        break;
    case "Delete":
        bootbox.confirm("Are you sure you want to delete this record?", function (result) {
            if (result == true) {
                deleteTaskReminder(id);
            }
        });
        break;
    default:
        window.location.href = base_url + '/Communication/NewTaskAndReminders';
}
$('.select_options').prop('selectedIndex',0);
});


function getTaskReminderById(id) {
    $.ajax({
        type: 'post',
        url: '/taskReminder-Ajax',
        data: {
            class: "taskReminderAjax",
            action: "getTaskReminderById",
            id: id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                if(response.data.data.user_id != login_user){
                    $("#title,#property ,#building,#due_date,#assigned_to,#details").css('pointer-events','none'); // this makes it unclickable
                    $("#title,#property ,#building,#assigned_to,#details") .attr("readonly", true);
                    $('#edit_units').multiselect('disable');
                }else{
                    $("#title,#property ,#building,#due_date,#assigned_to,#details").attr("readonly", false);
                }
               $("#property").val(response.data.data.property)
                var selected_property = $("#property").val();
                var selected_building = response.data.data.building;
                $("#due_date").val(response.data.data.due_date);
                $("#details").val(response.data.data.details);
                $("#status").val(response.data.data.status);
                $("#title").val(response.data.data.title);
                getDataByID(selected_building,selected_property,'property');
                setTimeout(function () {
                $("#building").val(selected_building);
                $("#building").val(selected_building);
                getDataByID(selected_building,selected_property,'building');
                    unit_ids_array=[];
                if(response.data.data.unit_ids != '') {
                    // var unit_ids_array = response.data.data.unit_ids.split(",");
                    var unit_ids_array = response.data.data.unit_ids;
                    console.log(unit_ids_array);
                    setTimeout(function () {
                   // $(".multiselect-selected-text").html(unit_ids_array.length+' selected')
                        $.each(unit_ids_array, function (key, value) {

                            $("input[type='checkbox'][value="+value+"]").prop('checked', true);
                        });
                        $(".multiselect-selected-text").html(unit_ids_array.length+' selected')
                        $.each(unit_ids_array, function(i,e){
                            $('#edit_units option[value='+e+']').prop("selected", 'selected');

                          //  $('.id_100 option[value=val2]').attr('selected','selected');
                        });

                    }, 500);
                }
                    $("#assigned_to").val(response.data.data.assigned_to);
;                }, 300);
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

$('#due_date').datepicker({
    yearRange: '-0:+100',
    changeMonth: true,
    changeYear: true,
    dateFormat: jsDateFomat,
    minDate:new Date(),
});

$(document).on('click','#task-reminder-table tr td',function(e){
    e.preventDefault();
    var base_url = window.location.origin;
    var id = $(this).closest("tr").attr('id');
    if ($(this).index() == 9 ) {
        return false;
    }else{
        getTaskReminderById(id);
        $("#task_reminder_edit_id").val(id);
        $("#add_task_reminer_modal").modal("show");
    }
});

$.ajax({
    type: 'post',
    url: '/taskReminder-Ajax',
    data: {
        class: "taskReminderAjax",
        action: "getInitialData"
    },
    success: function (response) {
        var data = $.parseJSON(response);
        if (data.status == "success") {

            if (data.data.propertyList.length > 0){
                var propertyOption = "<option value=''>Select</option>";
                $.each(data.data.propertyList, function (key, value) {
                    propertyOption += "<option data-tokens="+value.property_name+" value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                });
                $('.property').html(propertyOption);
            }
        } else if (data.status == "error") {
            toastr.error(data.message);
        } else {
            toastr.error(data.message);
        }
    }
});


$(document).on("change",".property",function (e) {
    e.preventDefault();
    $('.building').empty();
    $('.unit').empty();
    if($(this).val() != '') {
        getDataByID($(this).val(), $(this).val(), 'property');
        unit_popup = true;
    } else {
        $('.building').html("<option value=''>Select</option>");
        $('.unit').html("<option value=''>Select</option>");
    }
});

$(document).on("change",".building",function (e) {
    e.preventDefault();
    selected = $('#property').val()
    getDataByID($(this).val(),selected,'building','second');
    return false;
});

function getDataByID(data_id, prev_id, data_type,page=''){
    if (data_id == '' || data_id == 'null'){
        return;
    }
    $.ajax({
        type: 'post',
        url: '/taskReminder-Ajax',
        data: {
            class: "taskReminderAjax",
            action: "getDataByID",
            id: data_id,
            prev_id: prev_id,
            data_type : data_type
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if(data_type == 'property') {
                    var buildingOption = "<option value=''>Select</option>";
                    $('.unit').html(buildingOption);
                    if (data.data.length > 0) {
                        $.each(data.data, function (key, value) {
                            buildingOption += "<option value='" + value.id + "'>" + value.building_name + "</option>";
                        });
                    }
                    $('.building').html(buildingOption);
                } else if(data_type == 'building') {
                    var  unitOption = '';
                    var concatunit_prefix = '';
                    if (data.data.length > 0) {

                        $.each(data.data, function (key, value) {
                            buildingOption += "<option value='" + value.id + "'>" + value.unit_prefix +" - " + value.unit_no +"</option>";
                        });
                        if( data.data !='') {
                            $.each(data.data, function(i,e){
                                concatunit_prefix =  concatePrefix(e.unit_prefix,e.unit_no)
                                unitOption += "<option value='" + e.id + "'>" + concatunit_prefix +"</option>";
                            });
                        }
                        $('select[name="units[]"]').html(unitOption);
                        if(page == "second"){
                            $('select[name="units[]"]').multiselect("rebuild");
                        }else{
                            $('select[name="units[]"]').multiselect({includeSelectAllOption: true, nonSelectedText: 'Select'});
                        }

                    }else{
                        $('select[name="units[]"]').html("");
                        if(page == "second"){
                            $('select[name="units[]"]').multiselect("rebuild");
                        }else{
                            $('select[name="units[]"]').multiselect({includeSelectAllOption: true, nonSelectedText: 'Select'});
                        }
                       console.log(data.data);


                    }


                } else if(data_type == 'users'){
                    var usersOption = "";
                    if (data.data.length > 0){
                        $.each(data.data, function (key, value) {
                            usersOption += "<option value='"+value.id+"'>"+value.name+"</option>";
                        });
                        $('select[name="user_name[]"]').html(usersOption);
                        $('select[name="user_name[]"]').multiselect('destroy');
                        $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                    } else {
                        $('select[name="user_name[]"]').html('');
                        $('select[name="user_name[]"]').multiselect('destroy');
                        $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                    }
                }

            } else if (data.status == "error") {

                toastr.error(data.message);
            } else {

                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function concatePrefix(unitprefix,unitno) {
    return (unitprefix ? unitprefix + '-'+ unitno : unitno);
}

$(document).on("click",".task_reminder_cancel",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Communication/NewTaskAndReminders';
            }
        });
    });


    //edit announcement
    $("#add_task_reminder_form").validate({
        rules: {
            title: {
                required: true
            },
            property: {
                required: true
            },
            building: {
                required: true
            },
            "units[]": {
                required: true
            },
            details: {
                required: true
            }
        },
        submitHandler: function (form) {
            $(".unit_select_all_checkbox_html").blur();
            event.preventDefault();
            var edit_unit = $('#edit_units').val()
            var form = $('#add_task_reminder_form')[0];
            var formData = new FormData(form);
            formData.append('class', 'taskReminderAjax');
            formData.append('action', 'addTaskReminder');
            var edit_id = $("#task_reminder_edit_id").val();
            $.ajax({
                url: '/taskReminder-Ajax',
                type: 'post',
                data:  formData,
                id  : edit_id,
                contentType: false,
                processData: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        localStorage.setItem("Message","Record updated successfully.")
                        localStorage.setItem("rowcolor",true)
                        // toastr.success("Announcement Created Successfully.");
                        window.location.href = '/Communication/NewTaskAndReminders';
                    }else if(response.status == 'error' && response.code == 502){
                        toastr.error(response.message);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                }
            });
        }
    });

    function deleteTaskReminder(id) {
        $.ajax({
            type: 'post',
            url: '/taskReminder-Ajax',
            data: {
                class: "taskReminderAjax",
                action: "deleteTaskReminder",
                task_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#task-reminder-table').trigger( 'reloadGrid' );
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('click','.ClearTaskReminder',function () {
      var id =  $("#task_reminder_edit_id").val();
        getTaskReminderById(id);

    });


