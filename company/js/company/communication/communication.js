$(document).ready(function(){

    $('.e-tabs').on('click', function () {
        var table_id = $(this).attr('data_tab');
        if (table_id !== undefined && table_id != '') {
            var $grid = $("#" + table_id).setGridWidth($(window).width() - 32);
        }
    });

    EdatajqGrid1('All');
    EdatajqGrid2('All');
    EdatajqGrid3('All');
    EdatajqGrid4('All');
    $("#all-e-sign").show();
    $(document).on('click','#receivedesign',function(){
        $(".tab-pane").hide();
        $("#received-e-sign").show();
    });
    $(document).on('click','#sentesign',function(){
        $(".tab-pane").hide();
        $("#sent-e-sign").show();
    });
    $(document).on('click','#approvedesign',function(){
        $(".tab-pane").hide();
        $("#approved-e-sign").show();
    });
    $(document).on('click','#allesign',function(){
        $(".tab-pane").hide();
        $("#all-e-sign").show();
    });

    $(document).on("change","#esignTable1 tr select",function(){
        var value = $(this).val();
        if (value != "default"){
            var inputValue = '2';
            if (value == "Approve"){
                inputValue = '2';
            }else{
                inputValue = '3';
            }
            var userId = $(this).attr("data_id");
            updateESignHistoryState(userId, inputValue, "esignTable1");
            $(this).val("default");
        }
    });

    $(document).on("change","#esignTable2 tr select",function(){
        var value = $(this).val();
        if (value != "default"){
            var inputValue = '2';
            if (value == "Approve"){
                inputValue = '2';
            }else{
                inputValue = '3';
            }
            var userId = $(this).attr("data_id");
            updateESignHistoryState(userId, inputValue, "esignTable2");
            $(this).val("default");
        }
    });

    $(document).on("click",".leasedocumentlink",function () {
        var url = $(this).attr("data-url");
       localStorage.setItem("createPDF","createPDF");
        window.open(url, '_blank');
        return false;
    });
});

function updateESignHistoryState(userId, value, tableId){
    $.ajax({
        type: 'post',
        url: '/EsignatureUser/updateESignHistoryState',
        data: {
            class: "GenerateLease",
            action: "updateTenantESignHistoryRecord",
            user_id: userId,
            value: value
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                toastr.success(data.message);
                $('.esignTable').trigger('reloadGrid');
                onTop1(true,tableId);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
            //$('#esignTable1').trigger( 'reloadGrid' );
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function onTop1(rowdata,tableId){
    if(rowdata){
        setTimeout(function(){
            jQuery('#'+tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('#'+tableId).find('tr:eq(1)').find('td').last().addClass("green_row_right");
        }, 1000);
    }
}

function EdatajqGrid1(status, deleted_at) {
    /*if(jqgridNewOrUpdated == 'true'){
		var sortOrder = 'desc';
		var sortColumn = 'users.updated_at';
	} else {
		var sortOrder = 'asc';
		var sortColumn = 'users.name';
	}*/

    var sortOrder = 'desc';
    var sortColumn = 'e_sign_history.updated_at';

    var table = 'users';
    var columns = ['Recipient Name', 'Recipient Email','Date', 'Document Name', 'Status', 'Action'];
    if (status == "1") {
        var select_column = ['Approve', 'Decline'];    
    }else{
        var select_column = [];
    }
    
    var joins = [{type:'RIGHT JOIN',table: 'users', column: 'id', primary: 'user_id', on_table: 'e_sign_history'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var extra_where = [{column: 'user_type', value: '2', condition: '=', table: 'users'}];
    var columns_options = [
        /*{name:'Recipient Name',index:'id', width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter:titleCase},*/
        {name:'Recipient Name',index:'id', width:90,align:"left",searchoptions: {sopt: conditions},table:table,change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id'},
		{name:'Recipient Email',index:'email', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'done_date', width:100,searchoptions: {sopt: conditions},table:'e_sign_history'},
        {name:'Document Name',index:'id', width:80, align:"center",searchoptions: {sopt: conditions},table:'users',formatter:getLink},
        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:'e_sign_history',formatter:statusFormatter},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
    ];
    var ignore_array = [];
    jQuery("#esignTable1").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "users",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:deleted_at,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: sortOrder,
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "E-sign History",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );

}
function EdatajqGrid2(status, deleted_at) {
    /*if(jqgridNewOrUpdated == 'true'){
		var sortOrder = 'desc';
		var sortColumn = 'users.updated_at';
	} else {
		var sortOrder = 'asc';
		var sortColumn = 'users.name';
	}*/

    var sortOrder = 'desc';
    var sortColumn = 'users.updated_at';

    var table = 'users';
    var columns = ['Recipient Name', 'Recipient Email','Date', 'Document Name', 'Status', 'Action'];
    if (status == "1") {
        var select_column = ['Approve', 'Decline'];
    }else{
        var select_column = [];
    }

    var joins = [{type:'RIGHT JOIN',table: 'users', column: 'id', primary: 'user_id', on_table: 'e_sign_history'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var extra_where = [{column: 'user_type', value: '2', condition: '=', table: 'users'},{column: 'status', value: '1', condition: '=', table: 'e_sign_history'}];
    var columns_options = [
        {name:'Recipient Name',index:'id', width:90,align:"left",searchoptions: {sopt: conditions},table:table,change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id'},
		{name:'Recipient Email',index:'email', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'done_date', width:100,searchoptions: {sopt: conditions},table:'e_sign_history'},
        {name:'Document Name',index:'id', width:80, align:"center",searchoptions: {sopt: conditions},table:'users',formatter:getLink},
        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:'e_sign_history',formatter:statusFormatter},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
    ];
    var ignore_array = [];
    jQuery("#esignTable2").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "users",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:deleted_at,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: sortOrder,
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "E-sign History",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );

}
function EdatajqGrid3(status, deleted_at) {

	/*if(jqgridNewOrUpdated == 'true'){
		var sortOrder = 'desc';
		var sortColumn = 'users.updated_at';
	} else {
		var sortOrder = 'asc';
		var sortColumn = 'users.name';
	}*/

    var sortOrder = 'desc';
    var sortColumn = 'users.updated_at';

    var table = 'users';
    var columns = ['Recipient Name', 'Recipient Email','Date', 'Document Name', 'Status', 'Action'];
    if (status == "1") {
        var select_column = ['Approve', 'Decline'];
    }else{
        var select_column = [];
    }

    var joins = [{type:'RIGHT JOIN',table: 'users', column: 'id', primary: 'user_id', on_table: 'e_sign_history'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var extra_where = [{column: 'user_type', value: '2', condition: '=', table: 'users'},{column: 'status', value: '0', condition: '=', table: 'e_sign_history'}];
    //var pagination =[];
    var columns_options = [
        /*{name:'Recipient Name',index:'id', width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter:titleCase},*/
        {name:'Recipient Name',index:'id', width:90,align:"left",searchoptions: {sopt: conditions},table:table,change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id'},
		{name:'Recipient Email',index:'email', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'done_date', width:100,searchoptions: {sopt: conditions},table:'e_sign_history'},
        {name:'Document Name',index:'id', width:80, align:"center",searchoptions: {sopt: conditions},table:'users',formatter:getLink},
        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:'e_sign_history',formatter:statusFormatter},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
    ];
    var ignore_array = [];
    jQuery("#esignTable3").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "users",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:deleted_at,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: sortOrder,
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "E-sign History",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );

}
function EdatajqGrid4(status, deleted_at) {

	/*if(jqgridNewOrUpdated == 'true'){
		var sortOrder = 'desc';
		var sortColumn = 'users.updated_at';
	} else {
		var sortOrder = 'asc';
		var sortColumn = 'users.name';
	}*/
    var sortOrder = 'desc';
    var sortColumn = 'users.updated_at';

    var table = 'users';
    var columns = ['Recipient Name', 'Recipient Email','Date', 'Document Name', 'Status', 'Action'];
    var select_column = [];


    var joins = [{type:'RIGHT JOIN',table: 'users', column: 'id', primary: 'user_id', on_table: 'e_sign_history'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var extra_where = [{column: 'user_type', value: '2', condition: '=', table: 'users'},{column: 'status', value: '2', condition: '=', table: 'e_sign_history'}];
    //var pagination =[];
    var columns_options = [
        /*{name:'Recipient Name',index:'id', width:90,align:"center",searchoptions: {sopt: conditions},table:table,formatter:titleCase},*/
        {name:'Recipient Name',index:'id', width:90,align:"left",searchoptions: {sopt: conditions},table:table,change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id'},
		{name:'Recipient Email',index:'email', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'done_date', width:100,searchoptions: {sopt: conditions},table:'e_sign_history'},
        {name:'Document Name',index:'id', width:80, align:"center",searchoptions: {sopt: conditions},table:'users',formatter:getLink},
        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:'e_sign_history',formatter:statusFormatter},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
    ];
    var ignore_array = [];
    jQuery("#esignTable4").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "users",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:deleted_at,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: sortColumn,
        sortorder: sortOrder,
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "E-sign History",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );

}

function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject['Status'] == '0')  select = [];
            if(rowObject['Status'] == '1')  select = ['Approve','Decline'];
			if(rowObject['Status'] == '2')  select = [];
			if(rowObject['Status'] == '3')  select = [];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }else{
				data += 'N/A';
			}

            return data;
        }
    }
	
function titleCase( cellvalue, options, rowObject) {
    if (cellvalue !== undefined) {
        var string = "";
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getUserNameById',
            data: {
                class: "TenantAjax",
                action: "getUserNameById",
                id: cellvalue
            },
            async: false,
            success: function (response) {
                var res = JSON.parse(response);
                if (res.status == "success") {
                    string = res.data;
                }
            }
        });
        return string;
    }
}

function statusFormatter(cellValue, options, rowObject) {
    if (cellValue != undefined){
        if (cellValue == '0')
            return "Sent";
        else if (cellValue == '1')
            return "Recieved";
        else if (cellValue == '2')
            return "Completed";
        else if (cellValue == '3')
            return "Decline";
        else
            return '';
    }
}

function getLink(cellValue, options, rowObject) {
    if (cellValue != undefined){
        var url = window.location.origin+"/EsignatureUser/EsignatureLease?id="+cellValue;
        var link = '<a href="javascript:void(0)" class="leasedocumentlink" data-url="'+url+'">Lease AuthorizationAgreement</a>';
        return link;
    }
}

