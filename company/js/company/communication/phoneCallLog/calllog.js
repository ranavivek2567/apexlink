$(document).ready(function () {


    var unit_popup = false;
    $(document).on("click",".cancel_phone_call_log",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Communication/PhoneCall';
            }
        });
    });

    $(document).on("click",".blue-btn",function(){
      //  $('input[name="unit_checkbox[]"]').removeAttr('checked');
            var valu = '';
            var data = '';
            var unit_ids_array = [];
            $('input[name="unit_checkbox[]"]:checked').each(function(index,val) {
                var $this = $(this);
              //  val = index + 2;
                valu = $this.attr("data-label");
                data +=   valu +',';
                unit_ids_array.push($this.attr("value"));
            });
           data  = data.replace(/,\s*$/, "");
           $("#unit_ids").val(unit_ids_array);
           $("#unit").val(data);
        $("#myModal").modal("hide");

    });

    $(document).on("click",".unit_chekbox",function(){
     //   var total=$(this).find('input[name="unit_checkbox[][]"]:checked').length;
        var total_checkbox = $('input[name="unit_checkbox[]"]').length;
        var checkedBoxes = $('input[name="unit_checkbox[]"]:checked').length;
       if(total_checkbox == checkedBoxes){
           $('.select_all_checkbox').attr('checked',true);
       }
    });


    $(document).on("click", "#unit", function () {
        if(unit_popup == true) {
            $('input[name="unit_checkbox[]"]:checked').each(function(index,val) {
                var $this = $(this);
                $this.removeAttr("checked");
            });
            $("#myModal").modal("show");
        }
    });


    $(document).on("click",".close,.cancel_unit",function(){
        $("#myModal").modal("hide");
    });
    $('#product-options').modal('hide');



    $('select[name="user_name[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});

    $('#due_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
        onSelect: function (selectedDate) {
            $("#add_announcement_div .end_date").datepicker("option", "minDate", selectedDate);
        }
    }).datepicker("setDate", new Date());




    $('#next_date').datepicker({
        yearRange: '-0:+100',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        minDate:new Date(),
    }).datepicker("setDate", Date.today().addDays(1) );


    //add announcement
    $("#add_phone_call_log_form").validate({
        rules: {
            caller_name: {
                required: true
            }
        },
        submitHandler: function (form) {
            event.preventDefault();
            var form = $('#add_phone_call_log_form')[0];
            var formData = new FormData(form);
            formData.append('class', 'phoneCallLogAjax');
            formData.append('action', 'addphoneCallLog');
            $.ajax({
                url: '/phoneCallLog-Ajax',
                type: 'post',
                data:  formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        localStorage.setItem("Message","Record created successfully.")
                        localStorage.setItem("rowcolor",true)
                        // toastr.success("Announcement Created Successfully.");
                        window.location.href = '/Communication/PhoneCall';
                    }else if(response.status == 'error' && response.code == 502){
                        toastr.error(response.message);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                }
            });
        }
    });

    /* jquery for phone number format

 */
    jQuery('.phone_format').mask('000-000-0000', {reverse: true});


    /*To add datepicker for date of birth

 */

    $("#date").datepicker({dateFormat: jsDateFomat,
        changeYear: true,
        yearRange: "-100:+20",
        changeMonth: true,
        minDate: new Date(),
    }).datepicker("setDate", new Date());

    $('#time').timepicker({
        timeFormat: 'h:mm p',
        interval: 5,
        defaultTime:  new Date(),
        startTime: '8:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#time').timepicker({ zindex: 9999999});

    var theText = "General";

    // $("#call_type").val(11);

    setTimeout(function(){
    $('#call_type option[value=11]').attr('selected','selected');
    },2000)

//combo grid for owner_pre_vendor

    $(document).on('click','#_easyui_textbox_input1',function(){
        $('.all_call_users').combogrid('showPanel');
    })

    $('.all_call_users').combogrid({
        placeholder: "Select...",
        panelWidth: 500,
        url: '/phoneCallLog-Ajax',
        idField: 'name',
        textField: 'name',
        mode: 'remote',
        fitColumns: true,
        queryParams: {
            action: 'getUsersData',
            class: 'phoneCallLogAjax'
        },
        columns: [[
            {field: 'id', hidden: true},
            {field: 'name', title: 'Name', width: 120},
            {field: 'type', title: 'Type', width: 120},
            {field: 'email', title: 'Email', width: 300},
            {field: 'phone_number', title: 'Phone', width: 200},
        ]],
        onSelect: function (index, row) {


        }
    });


    /*  $('tr td:not(:last-child)');*/
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid(1);
    function jqGrid(status, deleted_at) {

        var date = new Date().toISOString().slice(0,10);
        var tomorrow = new Date();
        var tomorrow_date = tomorrow.addDays(1).toISOString().slice(0,10);
        var table = 'phone_call_logs';
        var columns = ['Caller','Phone','Date', 'Time','Call Type','Length of Calls','Call Category','Purpose of Calls','Action Taken','Follow-up Needed','Action'];
        var select_column = [];
        var joins = [{table:'phone_call_logs',column:'call_type_id',primary:'id',on_table:'call_type'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];

        var extra_where = [{column:'created_at',value:date,condition:'>=',table:table},{column:'created_at',value:tomorrow_date,condition:'<',table:table}];
        var columns_options = [

            {name:'Caller',index:'caller_name',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer',formatter: callerName},
            {name:'Phone',index:'phone_number',title:false, width:90,align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
            {name:'Date',index:'due_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Time',index:'time', width:80, align:"right",searchoptions: {sopt: conditions},table:table,change_type:'time'},
            {name: 'Call Type', index: 'call_type', width: 80, searchoptions: {sopt: conditions}, table: 'call_type', classes: 'pointer'},
            {name:'Length of Calls',index:'call_length', width:80, align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'Call Category',index:'incoming_outgoing', width:80, align:"right",searchoptions: {sopt: conditions},table:table,formatter:callCategoryFormatter},
            {name:'Purpose of Calls',index:'call_purpose', width:80, align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'Action Taken',index:'action_taken', width:80, align:"right",searchoptions: {sopt: conditions},table:table},
            {name:'Follow-up Needed',index:'follow_up_needed', width:80, align:"right",searchoptions: {sopt: conditions},table:table,formatter:followUpFormatter},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];
        var ignore_array = [];
        jQuery("#phone-call-logs").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: "phone_call_logs.updated_at",
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Phone Call Log",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }


    /**
     *  function to format caller Name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function callerName(cellValue, options, rowObject) {
        // return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';

        if(rowObject !== undefined) {

            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;text-transform:capitalize;">' + cellValue + '</span>';



        }
    }

    function callCategoryFormatter (cellValue, options, rowObject) {

        if (cellValue == '1')
            return "Incoming";
        else if (cellValue == '2')
            return "Outgoing";
        else
            return '';
    }


    function followUpFormatter (cellValue, options, rowObject) {

        if (cellValue == '1')
            return "No";
        else if (cellValue == '2')
            return "Yes";
        else
            return '';
    }

    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            select = ['Edit','Delete']

            var data = '';

            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }

            return data;
        }
    }
    $(".call_category") // select the radio by its id
        .change(function(){ // bind a function to the change event
            if( $(this).is(":checked") ){ // check if the radio is checked
                var val = $(this).val(); // retrieve the value
               if(val == 1){
                $("#call_category_filter").css("display","block");
                $("#date_range").css("display","none");

               }else if(val == 2){
                   $("#call_category_filter").css("display","none");
                   $("#date_range").css("display","block");
               }
            }
        });

    $('#start_date').datepicker({
        yearRange: '100:+20',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        maxDate:new Date()
    }).datepicker("setDate", new Date());

    $('#end_date').datepicker({
        yearRange: '100:+20',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat,
        maxDate:new Date()
    }).datepicker("setDate", new Date());

    $(document).on("change", "#phone-call-logs .select_options", function (e) {
//  e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        switch (action) {
            case "Edit":
                getPhoneCallLogById(id);
                $("#edit_phone_call_log_id").val(id);
                $("#addCaller").modal("show");
                $(".phone_call_log_save_update").text("Update");

                break;
            case "Delete":
                bootbox.confirm("Are you sure you want to delete this record?", function (result) {
                    if (result == true) {
                        deleteCallLog(id);
                    }
                });
                break;
            default:
                window.location.href = base_url + '/Communication/NewTaskAndReminders';
        }
        $('.select_options').prop('selectedIndex',0);
    });



    function getPhoneCallLogById(id) {
        $("#edit_phone_call_log_id").val(id);
        $.ajax({
            type: 'post',
            url: '/phoneCallLog-Ajax',
            data: {
                class: "phoneCallLogAjax",
                action: "getPhoneCallLogById",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $.each(response.data.data, function (key, value) {
                        $('#' + key).val(value);
                    });


                    $("input[name=incoming_outgoing][value=" + response.data.data.incoming_outgoing + "]").attr('checked', 'checked');
                    $("#call_phone_number").val(response.data.data.phone_number);
                    $("#date").val(response.data.data.due_date);
                    $("#call_type").val(response.data.data.call_type_id)
                    $("input[name=follow_up][value=" + response.data.data.follow_up_needed + "]").attr('checked', 'checked');
                    $("#notes").html(response.data.data.notes)
                    setTimeout(function () {
                        edit_date_time(response.data.data.updated_at);
                    },2000);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("click",".emptyCallLogModal",function(){
        var user_name = $("#_easyui_textbox_input1").val();
        var call_category = $('input[name=call_category]:checked').val();
        $(".phone_call_log_save_update").text("Save");


        if(user_name != '' && call_category == 1){
            $("#caller_name").val(user_name);
        }
        $("#action_taken").val("N/A");
        $("#edit_phone_call_log_id").val("");
        $("#call_for").val("");
        $("#call_type").val(11)
        $("#call_purpose").val("");
        // $("#action_taken").val("");
        $("#call_length").val("");

        $("input[name=incoming_outgoing][value='"+1+"']").attr('checked', 'checked');
        $("input[name=follow_up][value='"+1+"']").attr('checked', 'checked');
        $("#notes").val("");
        $('#due_date').datepicker("setDate", new Date());
    });
   // $(".emptyCallLogModal")

    $('input[type=radio][name=incoming_outgoing]').change(function() {
        if (this.value == 1) {
            var user_name = $("#_easyui_textbox_input1").val();
            $("#caller_name").val(user_name);
            $("#call_for").val("");
        }
        else if (this.value == 2) {
            var user_name = $("#_easyui_textbox_input1").val();
            $("#call_for").val(user_name);
            $("#caller_name").val(login_user_name);
        }
    });



    $(document).on("click",".preview_button",function(){


    $.ajax({
        type: 'post',
        url: '/phoneCallLog-Ajax',
        data: {
            class: "phoneCallLogAjax",
            action: "getTodayPhoneCallLog"
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                // update other modal elements here too
                $('#phone_call_log_content').html(response.data);

                // show modal
                $('#PhoneCallLogModal').modal('show');
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    });

    $(document).on("click",".email_button",function(){


        $.ajax({
            type: 'post',
            url: '/phoneCallLog-Ajax',
            data: {
                class: "phoneCallLogAjax",
                action: "getEmailUsers"
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    // update other modal elements here too
                    $('#phone_email_content').html(response.data);

                    // show modal
                    // $('#PhoneCallLogModal').modal('show');
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $(document).on("click",".ok_send_email",function(){
        var email_array = [];
        var user_id_array = [];
        var user_name_array = [];
        $('input[name="user_email_checkbox[]"]:checked').each(function(index,val) {
            var $this = $(this);
            email_array.push($this.attr("data-email"));
            user_id_array.push($this.attr("data-id"));
            user_name_array.push($this.attr("data-name"));
        });

        $.ajax({
            type: 'post',
            url: '/phoneCallLog-Ajax',
            data: {
                class: "phoneCallLogAjax",
                action: "sendPhoneCallMail",
                ids: user_id_array,
                emails:email_array,
                names:user_name_array
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                     $('#EmailModal').modal('hide');

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

        return false;
    });


    function callLogFilter(){
        var grid = $("#phone-call-logs"),f = [];
        var user_name = ($("#_easyui_textbox_input1").val() == '')?'all':$("#_easyui_textbox_input1").val();
        var call_type = ($("#incoming_outgoing_call_type").val() == '')?'all':$("#incoming_outgoing_call_type").val();
        var call_category = $('input[name=call_category]:checked').val();
        if(call_category == '1') {
            f.push({
                field: "phone_call_logs.caller_name",
                op: "in",
                data: user_name
            }, {field: "phone_call_logs.incoming_outgoing", op: "eq", data: call_type});
        }else if(call_category == '2') {
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            f.push({field: "phone_call_logs.caller_name", op: "cn", data: user_name},{field: "phone_call_logs.created_at", op: "dateBetween", data: start_date, data2:end_date});
        }
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all',extra_where :[]});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }


    function searchFilters(){
        var grid = $("#timeSheetTable"),f = [];
        var type = $('#_easyui_textbox_input1').val() != ''?$('#_easyui_textbox_input1').val():'all';
        var checked_value = $('input[name=filter_search]:checked').val()
        if(checked_value == '0'){
            var data = $('#ByWeekFilter option:selected').val();
            f.push({field: "daily_visitor_log.visitor", op: "cn", data: type},{field: "daily_visitor_log.date", op: "dateByString", data: data});
        } else if(checked_value == '1') {
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            f.push({field: "daily_visitor_log.visitor", op: "cn", data: type},{field: "daily_visitor_log.date", op: "dateBetween", data: start_date, data2:end_date});
        }
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }


    $(document).on("click",".callLogSearch",function(){
        callLogFilter();
    });





});

function deleteCallLog(id) {
    $.ajax({
        type: 'post',
        url: '/phoneCallLog-Ajax',
        data: {
            class: "phoneCallLogAjax",
            action: "deleteCallLog",
            call_log_id: id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
                $('#phone-call-logs').trigger( 'reloadGrid' );
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });
}



/*
Function to print envelope
*/
function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}
