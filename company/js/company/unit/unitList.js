$(document).ready(function(){

    var base_url = window.location.origin;
    /**
     * Get Parameters by id
     * @param status
     */
    function getParameterByName( name ){
        var regexS = "[\\?&]"+name+"=([^&#]*)",
            regex = new RegExp( regexS ),
            results = regex.exec( window.location.search );
        if( results == null ){
            return "";
        } else{
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    var id =  getParameterByName('id');
    console.log('id>>', id);
    //jqGrid status
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        var deleted_at = true;
        $('#property-unit-table').jqGrid('GridUnload');
        if(selected == 4) deleted_at = false;
        jqGrid(selected, id);
    });

    /**
     *change function to perform various actions(edit ,activate,deactivate)
     * @param status
     */
    jQuery(document).on('change','.select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        if(select_options == 'Edit')
        {
            window.location.href = '/Unit/EditUnit?id='+data_id;
        }else if (select_options == 'Flag Bank'){
            //localStorage.setItem("scrollDown",'#Flag');
            window.location.href = '/Unit/EditUnit?id='+data_id+'#new_flag';

        }else if (select_options == 'Inspection'){
            localStorage.setItem("inspection_value", "add_inspection");
            window.location.href = '/Unit/UnitInspection?id='+data_id;
        }else if (select_options == 'Add In-Touch'){
            // window.location.href =   '/Communication/InTouch';
            window.location.href = "/Communication/NewInTouch?tid="+data_id+"&category=Unit";

        }else if (select_options == 'In-Touch History'){
            var propertyname=$('#'+data_id).find('td:first').text();
            localStorage.setItem("propertyname",propertyname);
            window.location.href = "/Communication/InTouch";
        }else if (select_options == 'Print Envelope'){
            $.ajax({
                url: "/propertyUnitAjax",
                method: 'post',
                data: {
                    class: "PropertyUnitAjax",
                    action: "getUnitCompanyData",
                    unit_id: data_id,
                },
                success : function(response){
                    var response =  JSON.parse(response);
                    console.log('response', response);
                    if(response.status == 'success' && response.code == 200) {
                        $("#PrintEnvelope").modal('show');
                        $("#company_name").text(response.data.data.company_name);
                        $("#address1").text(response.data.data.address1);
                        $("#address2").text(response.data.data.address2);
                        $("#address3").text(response.data.data.address3);
                        $("#address4").text(response.data.data.address4);
                        $(".city").text(response.data.data.city);
                        $(".state").text(response.data.data.state);
                        $(".postal_code").text(response.data.data.zipcode);
                        $("#building_name").text(response.building_detail.building_name);
                        $("#building_address").text(response.building_detail.address);
                    }else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                }
            });

        }else if (select_options == 'Deactivate' || select_options == 'DEACTIVATE' || select_options == 'Activate' || select_options == 'ACTIVATE') {
            select_options = select_options.toLowerCase();
            bootbox.confirm({
                message: "Do you want to " + select_options + " this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        var status = select_options == 'activate' ? '1' : '0';
                        $.ajax({
                            url: "/propertyUnitAjax",
                            method: 'post',
                            data: {
                                class: "PropertyUnitAjax",
                                action: "updateStatus",
                                'id': data_id,
                                'status': status,
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                    $('#property-unit-table').trigger('reloadGrid');
                                    onTop(true);
                                } else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                } else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                }
            });
        }else if(select_options == 'Delete') {
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            url: "/propertyUnitAjax",
                            method: 'post',
                            data: {
                                class: "PropertyUnitAjax",
                                action: "delete",
                                'id': data_id,
                            },
                            success : function(response){
                                var response =  JSON.parse(response);
                                if(response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                    $('#property-unit-table').trigger('reloadGrid');
                                } else if(response.status == 'error' && response.code == 503) {
                                    toastr.error(response.message);
                                }else {
                                    toastr.warning('Record not updated due to technical issue.');
                                }
                            }
                        });
                    }
                }
            });
        }
        $('.select_options').prop('selectedIndex',0);
    });



    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All', id);
    function jqGrid(status, id) {
        var bid =  getParameterByName('bid');

        var table = 'unit_details';
        var columns = ['Floor #','Unit #', 'Building Name & ID', 'Building Name', 'Building ID', 'Status','# of Bedrooms','# of Bathrooms','Unit Status', 'Actions'];
        var select_column = ['Edit','Flag Bank','Inspection','Add In-Touch','In-Touch History','Deactivate','Activate','Print Envelope','Delete'];
        var joins = [{table:'unit_details',column:'building_id',primary:'id',on_table:'building_detail'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['unit_details.deleted_at', 'unit_details.updated_at'];
        var extra_where = [{column: 'property_id', value: id, condition: '='}];
        if(bid)
        {
            var extra_where = [{column: 'building_id', value: bid, condition: '='}];
        }

        var columns_options = [
            {name:'Floor #',index:'floor_no', width:90,align:"center",searchoptions: {sopt: conditions},table:table, formatter: propertyName, classes: 'pointer'},
            {name:'Unit #',index:'unit_check', align:"center", width:100,searchoptions: {sopt: conditions},table:table, classes: 'pointer'},
            {name:'Building Name & ID',index:'building_name_and_id', width:80,align:"center",searchoptions: {sopt: conditions},table:table, change_type: 'combine_column_hyphen', extra_columns: ['building_name', 'building_id'], update_column: 'building_name_and_id',original_index: 'building_name'},
            {name:'Building ID',index:'building_id', width:80,hidden:true, align:"center",searchoptions: {sopt: conditions},table:'building_detail',attr:[{name:'flag',value:'unit'}]},
            {name:'Building Name',index:'building_name', hidden: true, width:80,align:"center",searchoptions: {sopt: conditions},table:'building_detail'},
            {name:'status',index:'building_unit_status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:buildingStatusFormatter},
            {name:'# of Bedrooms',index:'bedrooms_no', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'# of Bathrooms',index:'bathrooms_no', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Unit Status',index:'status', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFmatter}
        ];
        var ignore_array = [];
        jQuery("#property-unit-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                extra_where:extra_where,
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Units",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {top:10,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter (cellValue, options, rowObject){
        if (cellValue == '1')
            return "Yes";
        else if(cellValue == '0')
            return "No";
        else
            return '';
    }

    alphabeticSearch()
    function alphabeticSearch(){
        $.ajax({
            type: 'post',
            url:'/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'general_property',
                column: 'property_name'},
            success : function(response){
                var response = JSON.parse(response);
                if(response.code == 200){
                    var html = '';

                    $.each(response.data, function(key,val) {
                        var color = '#05A0E4'
                        if(val == 0) color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });
    }

    $(document).on('click','#AZ',function(){
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click','#allAlphabet',function(){
        var grid = $("#property-unit-table");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });

    $(document).on('click','.getAlphabet',function(){
        var grid = $("#property-unit-table"),f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if(value != '0'){
            f.push({field:"company_name",op:"bw",data:search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    });


    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            //alert(flagValue);
            var id = $(rowObject.Action).attr('data_id');
           // alert(id);
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#flags" href="javascript:void(0);" data_url="/Unit/UnitView?id='+id+'#flags" ><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }

    }

    function flagFormatter(cellValue, options, rowObject){
        if(rowObject !== undefined){
            console.log(rowObject.Action);
            var flagValue = $(rowObject.Action).attr('flag');
            var flag = '';
            if(flagValue == 'yes'){

            } else {

            }
        }
    }

    $(document).on('click','.classFlagRedirect',function(e){
       // alert("hihihihi");
        event.preventDefault();
        localStorage.setItem("scrollDown",$(this).attr('data_href'));
        var url = $(this).attr('data_url');
       // alert(url)
        window.location.href = url;
    });


    /**
     * Function for view company on clicking row
     */
   /* $(document).on('click','#property-unit-table tr td:not(:last-child)',function(){
        console.log('dsds',  $(this).closest('tr').attr('id'));
        var id = $(this).closest('tr').attr('id');
        //window.location.href = '/Unit/UnitView?id='+id;
    });*/


    $(document).on('click', '#property-unit-table tr td', function () {
        var id = $(this).closest('tr').attr('id');
        if ($(this).index() == 0 || $(this).index() == 9) {
            return false;
        } else {
            window.location.href = '/Unit/UnitView?id=' + id;
        }
    });


    /**
     * jqGrid function to format status
     * @param status
     */
    function buildingStatusFormatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Vacant Available";
        else if(cellvalue == 2)
            return "Unrentable";
        else if(cellvalue == 4)
            return "Occupied";
        else if(cellvalue == 5)
            return "Notice Available";
        else if(cellvalue == 6)
            return "Vacant Rented";
        else if(cellvalue == 7)
            return "Notice Rented";
        else if(cellvalue == 8)
            return "Under Make Ready";
        else
            return '';
    }
    /**
     * jqGrid function to format status
     * @param status
     */
    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Active";
        else if(cellvalue == 0)
            return "InActive";
        else
            return '';
    }

    /**
     * jqGrid function to format action column
     * @param status
     */

    function actionFmatter (cellvalue, options, rowObject){

        if(rowObject !== undefined) {
            var status = rowObject['Unit Status'];
            var editable = $(cellvalue).attr('editable');
            var select = '';
            if(status == 1)  select = ['Edit','Flag Bank','Inspection','Add In-Touch','In-Touch History','Deactivate','Print Envelope','Delete'];
            if(status == 0 || rowObject.Status == '')  select = ['Edit','Inspection','Add In-Touch','Activate','Print Envelope'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    /** function to get property detail   */
    getPropertyDetail(id);
    function getPropertyDetail(id) {
        $.ajax({
            type: 'post',
            url: '/get-property-detail ',
            data: {
                class: 'propertyDetail',
                action: 'gerPropertyDetail',
                id:id
            },
            success: function (response) {
                var res = JSON.parse(response);
                if(res) {
                    $("#property_tab_link" ).attr('href',base_url+'/Property/PropertyModules');
                    $("#building_tab_link" ).attr('href',base_url+'/Building/BuildingModule?id='+id);
                    $("#new_property_unit_list_href").attr("href", base_url+"/Unit/UnitModule?id="+id);
                    $("#new_unit_href").attr("href", base_url+"/Unit/AddUnit?id="+id);
                    $("#new_building_href").prop("href", base_url+"/Building/AddBuilding?id="+id);
                    $("#property_inspection_link").prop("href", base_url+"/Property/PropertyInspection?id="+id);
                }
            },
        });

    }
});

function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}
