$(document).ready(function () {
    if(localStorage.getItem("scrollDown")) {
        var message = localStorage.getItem("scrollDown");
        $('html, body').animate({
            'scrollTop' : $(message).position().top
        });
        localStorage.removeItem('scrollDown');
    }
    if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }
    if(localStorage.getItem("messageGuest")){
        setTimeout(function(){
            toastr.success("This record updated successfully");
            localStorage.removeItem('messageGuest');
        }, 2000);
    }
    if(localStorage.getItem("rowcolorGuest")){
        setTimeout(function(){
            jQuery('#guestcard_table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('#guestcard_table').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('rowcolorGuest');
        }, 2000);
    }
    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 1)
            return "Active";
        else if (cellValue == '0')
            return "Inactive";
        else if (cellValue == '2')
            return "Archived";
        else if (cellValue == '3')
            return "View Scheduled";
        else if (cellValue == '4')
            return "Application Generated";
        else if (cellValue == '5')
            return "Lease Generated";
        else
            return '';
    }

    function statusFormatter1(cellValue, options, rowObject) {
        if (cellValue == 0)
            return "No";
        else if (cellValue == '1')
            return "Yes";
        else
            return '';
    }

    function statusFmatter1(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = '';

            if(rowObject['Status'] == '1')  select = ['Edit','Start Application','Email','Email History','Text','Text History','File Library','Notes History','Flag Bank','Run Background Check','Archive Guest','Delete Guest','Print Envelope'];
            if(rowObject['Status'] == '2')  select = ['Run Background Check','Activate Guest','Print Envelope','Delete Guest'];
            if(rowObject['Status'] == '0')  select = ['Edit','Start Application','Email','Email History','Text','Text History','File Library','Notes History','Flag Bank','Run Background Check','Archive Guest','Delete Guest','Print Envelope'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    function statusFmatter2 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject['is_editable'] == '1')  select = ['Edit','Deactivate'];
            if(rowObject['is_editable'] == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    var base_url = window.location.origin;
    $(document).on("change", "#jqGridStatus", function (e) {
        var status = $(this).val();
        $('#priorityType-table').jqGrid('GridUnload');
        jqGrid(status,true);
    });



    if(localStorage.getItem("table_green_tableid")){
        setTimeout(function(){
            var tableId = localStorage.getItem("table_green_tableid")
            jQuery(tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery(tableId).find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('table_green_id');
            localStorage.removeItem('table_green_tableid');
        }, 2000);
    }

    /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var user_id = $(this).attr('data-user_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        var is_default = $(this).attr('is_default');
        var elem = $(this);
        var email = $("tr#"+id).find("td").last().text();
        var phone = $("tr#"+id).find("td").last().prev().prev().text();

        if (opt == 'Edit' || opt == 'EDIT') {
            window.location.href = '/GuestCard/NewGuestCard/Edit?id=' + id +'&user_id='+user_id;
        }
        if (opt == 'Start Application' || opt == 'Start Application') {
            window.location.href = '/RentalApplication/RentalApplication?id='+id;
        }
        if (opt == 'Email' || opt == 'Email') {
            localStorage.setItem('predefined_mail',email);
            localStorage.setItem('table_green_id',id);
            localStorage.setItem('table_green_tableid', '#guestcard_table');
            localStorage.setItem('table_green_url','/GuestCard/ListGuestCard');
            window.location.href = '/Communication/ComposeEmail';
        }if (opt == 'Email History' || opt == 'Email History') {
                window.location.href = '/Communication/SentEmails';
        }if (opt == 'Text' || opt == 'Text') {
            localStorage.setItem('predefined_mail',email);
            localStorage.setItem('table_green_id',id);
            localStorage.setItem('table_green_tableid', '#guestcard_table');
            localStorage.setItem('table_green_url','/GuestCard/ListGuestCard');
            localStorage.setItem('phoneNumber_guest',phone);
            window.location.href = '/Communication/AddTextMessage';
        }if (opt == 'Text History' || opt == 'Text History') {

                window.location.href = '/Communication/TextMessage';
        }if (opt == 'File Library' || opt == 'FILE LIBRARY'){
            /*07-02-2020*/
            localStorage.setItem("scrollDown",'#customfeilddata');
            /*07-02-2020*/

            $("#guestlisting").hide();
            $("#guestfilelibrary").show();
            $("#hidden_id_view").val(user_id);
            $("#hidden_id_view2").val(id);
            getFiles(user_id);
            jqGridFlags(user_id);
            InformationData(user_id);
            $('html, body').animate({ scrollTop: $('#filelibrary').offset().top }, 1000);
            return false;
        }
        if (opt == 'Notes History' || opt == 'Notes History'){
            $("#guestlisting").hide();
            $("#guestfilelibrary").show();
            $("#hidden_id_view").val(user_id);
            $("#hidden_id_view2").val(id);

            getFiles(user_id);
            jqGridFlags(user_id);
            InformationData(user_id);
            $('html, body').animate({ scrollTop: $('#notes').offset().top }, 1000);
            return false;
        }

        if (opt == 'Run Background Check' || opt == 'Run Background Check') {
            $('#backGroundCheckPopCondition').modal('show');
            $('#btnBackgroundSelection').attr('tent_id',id);
            elem.val('default');
        }
        if (opt == 'Archive Guest' || opt == 'Archive Guest') {
            archiveguestCard(id,user_id);
        }
        if (opt == 'Delete Guest' || opt == 'Delete Guest') {
            bootbox.confirm("Are you sure you want to delete this entry?", function (result) {
                if (result == true) {
                    deleteguestCard(id);
                } else {
                    $('#guestcard_table').trigger( 'reloadGrid' );
                }
            });
        }
        if (opt == 'Print Envelope' || opt == 'Print Envelope') {
            localStorage.setItem('edit_active','');
            $("#print_envelope_guest").modal('show');
            addPrintAddress(id);
            elem.val("default");
            //break;
        }
        if (opt == 'Flag Bank' || opt == 'Flag Bank'){
            $("#guestlisting").hide();
            $("#guestfilelibrary").show();
            $("#hidden_id_view").val(user_id);
            $("#hidden_id_view2").val(id);
            getFiles(user_id);
            jqGridFlags(user_id);
            InformationData(user_id);
            $('html, body').animate({ scrollTop: $('#flags').offset().top }, 1000);
        }
        if (opt == 'Activate Guest' || opt == 'Activate Guest') {
            activateGuest(id,user_id);
        }
    });
    function addPrintAddress(id){

        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: "GuestCardAjax",
                action: "getallRecordguestCard",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var address1 = response.data.address1;
                var address2 = response.data.address2;
                var address3 = response.data.address3;
                var address4 = response.data.address4;
                var city = response.data.city;
                var state = response.data.state;
                var country = response.data.country;
                var zipcode = response.data.zipcode;
                var name = response.data.name;
                // var unitnubmer = response.unit.unit_prefix+"-"+response.unit.unit_no;

                console.log("address1>>",address1,"address2>>",address2,"address3>>",address3,"address4>>",address4);



                var html1 = "";
                var newAddress = "";
                var newAddress2 = "";
                var htmlsdasad  = '';
                if(name != ""){
                    html1 += '<label>'+name+'</label>';
                }

              /*  if(address1 != "" && address2 == "" && address3 == "" && address4 == ""){
                    newAddress += '<label>'+address1+'</label>';
                }
                if(address1 != "" && address2 != "" && address3 == "" && address4 == ""){
                    newAddress += '<label>'+address1+" "+address2+'</label>';
                }
                if(address1 != "" && address2 != "" && address3 != "" && address4 == ""){
                    newAddress += '<label>'+address1+" "+address2+" "+address3+'</label>';
                }
                if(address1 != "" && address2 != "" && address3 != "" && address4 != ""){
                    newAddress += '<label>'+address1+" "+address2+" "+address3+" "+address4+'</label>';
                }*/
                if(address1 != ""){
                    html1 += '<label>'+address1+'</label>';
                }
                if(address2 != ""){
                    html1 += '<label>'+address2+'</label>';
                }
                if(address3 != ""){
                    html1 += '<label>'+address3+'</label>';
                }



                /*html1 += '<label>'+newAddress+'</label>';*/

                if(city != "" && state == "" && country == "" && zipcode == "" ){
                    newAddress2 += '<label>'+city+'</label>';
                }
                if(city != "" && state != "" && country == "" &&  zipcode == ""){
                    newAddress2 += '<label>'+city +' '+state +'</label>';
                }
                if(city != "" && state != "" && country != "" && zipcode == ""){
                    newAddress2 += '<label>'+city +' '+state +' ' +country+'</label>';
                }

                if(city != "" && state != "" && country != "" && zipcode != ""){
                    newAddress2 += '<label>'+city +' '+state +' '+country+', ' +zipcode+'</label>';
                }



                 html1 += newAddress2;


                /* ----------------- */
                var html2 = "";
                var newAddress3 = "";
                var newAddress4 = "";
                var mergedAddress = "";
                var comp = response.company;


                if(name != ""){
                    html2 += '<label>'+comp.name+'</label>';
                }

              /*  if(comp.address1 != "" && comp.address2 == "" && comp.address3 == "" && comp.address4 == ""){
                    newAddress3 += '<label>'+comp.address1+'</label>';
                }
                if(comp.address1 != "" && comp.address2 != "" && comp.address3 == "" && comp.address4 == ""){
                    newAddress3 += '<label>'+comp.address1+" "+comp.address2+'</label>';
                }
                if(comp.address1 != "" && comp.address2 != "" && comp.address3 != "" && comp.address4 == ""){
                    newAddress3 += '<label>'+comp.address1+" "+comp.address2+" "+comp.address3+'</label>';
                }
                if(comp.address1 != "" && comp.address2 != "" && comp.address3 != "" && comp.address4 != ""){
                    newAddress3 += '<label>'+comp.address1+" "+comp.address2+" "+comp.address3+" "+comp.address4+'</label>';
                }*/

                if(comp.address1 != ""){
                    html2 += '<label>'+comp.address1+'</label>';
                }
                if(comp.address2 != ""){
                    html2 += '<label>'+comp.address2+'</label>';
                }
                if(comp.address3 != ""){
                    html2 += '<label>'+comp.address3+'</label>';
                }
                if(comp.address4 != ""){
                    html2 += '<label>'+comp.address4+'</label>';
                }

                /*html2 += '<label>'+newAddress3+'</label>';*/


                if(comp.city != "" && comp.state == "" && comp.country == "" && comp.zipcode == ""){
                    newAddress4 += '<label>'+comp.city+'</label>';
                }
                if(comp.city != "" && comp.state != "" && comp.country == "" && comp.zipcode == ""){
                    newAddress4 += '<label>'+comp.city +' '+comp.state +'</label>';
                }
                if(comp.city != "" && comp.state != "" && comp.country != "" && comp.zipcode == ""){
                    newAddress4 += '<label>'+comp.city +' '+comp.state +', ' +comp.country+'</label>';
                }
                if(comp.city != "" && comp.state != "" && comp.country != "" && comp.zipcode != ""){
                    newAddress4 += '<label>'+comp.city +' '+comp.state +' ' +comp.country+',' +comp.zipcode+'</label>';
                }

                /*html2 += '<label>'+newAddress4+'</label>';*/

                html2  += newAddress4;



                setTimeout(function(){
                    console.log("cxzczxc 11 >>",html1);
                    console.log("cxzczxc 22 >>",html2);
                    $("#print_envelope_guest .print_tenant_address").html(html1);
                    $("#print_envelope_guest .print_company_address").html(html2);
                },200);
            }
        });
    }
    $(document).on('click','#AZ',function(){
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click','#allAlphabet',function(){
        var grid = $("#guestcard_table");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });

    $(document).on('click','.getAlphabet',function(){
        var grid = $("#guestcard_table"),f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if(value != '0'){
            f.push({field:"name",op:"bw",data:search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    });

    alphabeticSearch();
    function alphabeticSearch(){
        $.ajax({
            type: 'post',
            url:'/Companies/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'users',
                column: 'first_name',
                where: [{column:'user_type',value:'6',condition:'=',table:'users'}]},
            success : function(response){
                var response = JSON.parse(response);
                console.log(response);
                if(response.code == 200){
                    var html = '';

                    $.each(response.data, function(key,val) {
                        var color = '#05A0E4';
                        if(val == 0) color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });
    }
  
    $(document).on('click', '.tab-content #guest-cards .sub-tabs ul li a', function () {
        var grid = $("#guestcard_table"), f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if (value != '0') {
            f.push({field: "lease_guest_card.status", op: "eq", data: value});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });
   

    function jqGrid(status, deleted_at) {
        
       // console.log("pagination="+pagination);
        if(jqgridNewOrUpdated == 'true'){
            var sortOrder = 'desc';
            var sortColumn = 'lease_guest_card.updated_at';
        } else {
            var sortOrder = 'desc';
            var sortColumn = 'lease_guest_card.updated_at';
        }
        console.log('name',sortColumn);
        var table = 'lease_guest_card';
        var columns = ['Prospect Name', 'Property Name','user_id', 'Guest Card Number', ' Move In Date', 'Status','Unit_no', 'Building Unit ID','Phone number', 'Action','email'];
        var select_column = ['Edit', 'Start Application', 'Email', 'Email History', 'Text', 'Text History', 'File Library', 'Notes History', 'Flag Bank', 'Run Background Check', 'Archive Guest', 'Delete Guest', 'Print Envelope'];
        var joins = [{table: 'lease_guest_card', column: 'user_id', primary: 'id', on_table: 'users'},
            {table: 'lease_guest_card', column: 'unit_id', primary: 'id', on_table: 'unit_details'},
            {table: 'unit_details', column: 'property_id', primary: 'id', on_table: 'general_property'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        var extra_where = [{column: 'user_type', value: '6', condition: '=', table: 'users'}];

       // var pagination =[];

        var columns_options = [

        /*{name:'Prospect Name',index:'name', width:90,align:"center",searchoptions: {sopt: conditions},table:'users',},*/
        {name:'Prospect Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:'users'},
        {name:'Property Name',index:'property_name', width:100,searchoptions: {sopt: conditions},table:'general_property'},
        {name:'user_id',index:'user_id', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Guest Card Number',index:'id', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Move In Date',index:'expected_move_in',searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter: statusFormatter},
        {name:'Unit_no',index:'unit_no',hidden:true, width:80,align:"center",searchoptions: {sopt: conditions},table:'unit_details'},
        {name:'Building Unit ID',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
        /*{name:'Building Unit ID',index:'id', width:80,align:"center",searchoptions: {sopt: conditions},table:'unit_details',formatter:getUnitDetails,alias:'unit_data_id'},*/
            {name:'Phone number',index:'phone_number',hidden:true, width:80,align:"center",searchoptions: {sopt: conditions},table:'users'},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},
            {name:'email',index:'email',hidden:true, width:90,align:"left",searchoptions: {sopt: conditions},table:'users'},
        ];
        var ignore_array = [];
        jQuery("#guestcard_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "lease_guest_card",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Guest Cards",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    
    jqGrid('All');
	
	function getUnitDetails( cellvalue, options, rowObject){
        var string = "";
        if (cellvalue != undefined) {
            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getUnitById',
                data: {
                    class: "TenantAjax",
                    action: "getUnitById",
                    id: cellvalue
                },
                async: false,
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        string = res.data.unit_prefix + "-" + res.data.unit_no;
                    }
                }
            });
        }
        return string;
    }


    function deleteguestCard(id) {
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: "GuestCardAjax",
                action: "deleteguestCard",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#guestcard_table').trigger('reloadGrid');
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function archiveguestCard(id,user_id) {
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: "GuestCardAjax",
                action: "archiveguestCard",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    var res = update_users_flag('users',user_id);
                    if(res.code == 200){
                        getGuestCardStatus();
                        $('#guestcard_table').trigger('reloadGrid');
                   setTimeout(function () {
                       jQuery('#guestcard_table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                       jQuery('#' +
                           '' +
                           '').find('tr:eq(1)').find('td:eq(8)').addClass("green_row_right");
                   },50) ;
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function activateGuest(id,user_id) {
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: "GuestCardAjax",
                action: "activateGuest",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    var res = update_users_flag('users',user_id);
                    if(res.code == 200){
                        getGuestCardStatus();
                        $('#guestcard_table').trigger('reloadGrid');
                        setTimeout(function () {
                            jQuery('#guestcard_table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                            jQuery('#guestcard_table').find('tr:eq(1)').find('td:eq(8)').addClass("green_row_right");
                        },50) ;
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    getGuestCardStatus();

    function getGuestCardStatus() {
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: "GuestCardAjax",
                action: "getGuestCardStatus"
            },
            success: function (response) {
                var response = JSON.parse(response);
console.log("sdsadasdas >>",response);
                var activeCount = response.data.active.length;
                var archivedCount = response.data.Archived.length;
                if (response.status == 'success' && response.code == 200) {
                    $("#guest-cards .active-count").text(activeCount);
                    $("#guest-cards .archived-count").text(archivedCount);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    function getFiles(id) {
        //ar id = $('.select_options').attr('data_id');
        console.log(id);
        // var tenant_id = $(".guestedit_id").val();
        var table = 'tenant_chargefiles';
        var columns = ['Id', 'File Name', 'Preview','File_location','File_extension','Action'];
        var select_column = ['Edit','Delete','Send'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column:'user_id', value :id, condition:'='}];
        var extra_columns = [];
        var joins = [];
        var columns_options = [
            {name:'Id',index:'id', width:300,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'File Name',index:'filename', width:300,searchoptions: {sopt: conditions},table:table},
            {name:'Preview',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:fileFormatter},
            {name:'File_location',index:'file_location', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'file_type', width:300,hidden:true,searchoptions: {sopt: conditions},table:table,formatter:selectFormatter},
        ];


        var ignore_array = [];
        jQuery("#TenantFiles-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "File Library",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    function fileFormatter(cellValue, options, rowObject){
        var upload_url = upload_url;
        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;
            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + 'company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + 'company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + 'company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + 'company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }
            return imageData;
        }
    }

    function selectFormatter(cellValue, options, rowObject){
        var upload_url = upload_url;
        if(rowObject !== undefined) {
            var file_type =  rowObject.View;
            var location = rowObject.File_location;
            var path = upload_url+'company/'+location;

            var imageData = '';
            if(file_type == '1'){

                imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
            } else {
                if (rowObject.File_extension == 'xlsx') {
                    src = upload_url + 'company/images/excel.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'pdf') {
                    src = upload_url + 'company/images/pdf.png';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                } else if (rowObject.File_extension == 'docx') {
                    src = upload_url + 'company/images/word_doc_icon.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }else if (rowObject.File_extension == 'txt') {
                    src = upload_url + 'company/images/notepad.jpg';
                    imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
                }
            }

            var html = "<select editable='1' class='form-control select_options' data-id='"+rowObject.Id+"' data-path = '"+path+"' data-src= '"+src+"'>"
            html +=  "<option value=''>Select</option>";
            html +=  "<option value='Delete'>Delete</option>";
            html +=  "<option value='Send'>SEND</option>";
            html +="</select>";
            return html;
        }
    }

    function jqGridFlags(id) {
        var table = 'flags';
        var columns = ['Date','Flag Name', 'Phone Number', 'Flag Reason','Completed','Note','Action'];
        var select_column = ['Edit','Delete','Completed'];
        //var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_phone'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        //var extra_where = [{column:'object_type',value:'tenant',condition:'=',table:'flags'},{column:'object_id',value:tenantId,condition:'=',table:'flags'}];
        var extra_where = [];
       // var pagination=[];
        var columns_options = [
            {name:'Date',index:'date', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Flag Name',index:'flag_name', width:180,searchoptions: {sopt: conditions},table:table},
            {name:'Phone Number',index:'flag_phone_number', width:180, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Flag Reason',index:'flag_reason', width:180, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Completed',index:'completed', width:180, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Note',index:'flag_note', width:180, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:180,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},

        ];
        var ignore_array = [];
        jQuery("#guestFlags-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "flags",
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Flags",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    function InformationData(user_id){
        $.ajax({
            type: 'post',
            url: '/guest-card-edit-ajax',
            data: {
                class: 'GuestCardEditAjax',
                action: 'guestCardUserID',
                user_id : user_id
            },
            success: function (response) {
                var data = JSON.parse(response);
                if (data.status == "success"){
                    var data2=data.data2;
                    var emergency_detail=data.ViewEmergencyInfo;
                    var html = "";
                    html =  '<thead>'+
                        '<tr style="border:1px solid #C9C9C9;">'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Unit Number'+'</th>'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Property'+' </th>'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Rent'+'</th>'+
                        '<th scope="col" class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+'Security Deposit'+'</th>'+
                        '</tr>'+
                        '</thead>'+
                        '<tr style="border-left:1px solid #afafaf; border-right:1px solid #afafaf;">'+
                        '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data2.unit_prefix+' '+data2.unit_no +'</td>'+
                        '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data2.property_name+'</td>'+
                        '<td class="col-sm-3" style="border-right:1px solid #dadada;border-bottom:1px solid #afafaf;">'+ data2.market_rent+'</td>'+
                        '<td class="col-sm-3" style="border-right:0px solid #dadada;border-bottom:1px solid #afafaf;">'+ data2.security_deposit + '</td>'+
                        '</tr>';

                    $("#unit_table").html(html);

                    $.each(data.data, function (key,value) {

                        if(data.data.gender==1){
                          $(".gender").text('Male')
                        }
                       else if(data.data.gender==2){
                            $(".gender").text('Female')
                        }
                        else if(data.data.gender==3){
                            $(".gender").text('Prefer Not to Say')
                        }
                        else if(data.data.gender==4){
                            $(".gender").text('Other')
                        }

                        $('.'+key).text(value);

                    });
                    $.each(data.data2, function (key,value) {
                        $('.'+key).text(value);
                    });
                    $("#emergency_detail").html(emergency_detail);
                    $(".expected_movein").text(data.data1.expected_move_in);
                    $("#notes").html(data.viewNotes);
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
        });
    }

    $(document).on('click','.edit_redirection',function(){
        var user_idd=$("#hidden_id_view").val();
        var user_idd2=$("#hidden_id_view2").val();
        var base_url = window.location.origin;
        window.location.href = base_url +'/GuestCard/NewGuestCard/Edit?id=' + user_idd2 +'&user_id='+user_idd;
    });
});

$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {
        $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.open('https://www.victig.com', '_blank');
    }
});