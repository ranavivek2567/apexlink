$(document).ready(function() {
  /*  var base_url = window.location.origin;*/
  jqGrid('All');
    if(localStorage.getItem("rowcolorTenant"))
    {
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(9)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }

    $(document).on("change", ".tenant_type_status #jqgridOptions", function (e) {
        var status = $(this).val();
        $('#lease_listing').jqGrid('GridUnload');
        jqGrid(status,true);

    });
    $(document).on('click','#AZ',function(){
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click','#allAlphabet',function(){
        var grid = $("#lease_listing");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });
    $(document).on('click','.getAlphabet',function(){
        var grid = $("#lease_listing"),f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if(value != '0'){
            f.push({field:"name",op:"bw",data:search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    })
    alphabeticSearch();

    function alphabeticSearch(){
        $.ajax({
            type: 'post',
            url:'/Companies/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'users',
                column: 'first_name',
                where: [{column:'record_status',value:'0',condition:'=',table:'users'}]},
            success : function(response){
                var response = JSON.parse(response);
                if(response.code == 200){
                    var html = '';

                    $.each(response.data, function(key,val) {
                        var color = '#05A0E4';
                        if(val == 0) color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });

    }
    $(document).on('click','#lease_listing tr td:not(:last-child)',function(e){
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
       /* ('tr td:not(:last-child)')*/
    });
  /*  $('tr td:not(:last-child)');*/

    function titleCase( cellvalue, options, rowObject) {
        if (cellvalue !== undefined) {

            var string = "";
            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getUserNameById',
                data: {
                    class: "TenantAjax",
                    action: "getUserNameById",
                    id: cellvalue
                },
                async: false,
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        string = res.data;
                    }
                }
            });
            return string;
        }
    }

    if(localStorage.getItem('ElasticSearch')){
        var elasticSearchData = localStorage.getItem('ElasticSearch');
        setTimeout(function(){
            var grid = $("#lease_listing"),f = [];
            f.push({field: "users.id", op: "eq", data: elasticSearchData,int:'true'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('ElasticSearch');
        },1500);
    }


    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        if(jqgridNewOrUpdated == 'true'){
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'users';
        var rentCurrSymbol = "Rent ("+currencySymbol+")";
        var columns = ['Tenant Name','Property Name','Unit Number',rentCurrSymbol,'Lease ID', 'Days Remaining','Status','Action'];
        //if(status===0) {
            var select_column = ['Edit','Print','Add New Lease','File Library','Notes & History','Run Background Check','Delete Lease'];
        //}
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},
            {table:'users',column:'id',primary:'user_id',on_table:'tenant_phone'},
            {table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},
            {table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'},{column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'}];
        //var pagination=[];
        var columns_options = [

            /*{name:'Tenant Name',index:'id', width:90,align:"left",searchoptions: {sopt: conditions},table:table,formatter:titleCase},*/
            {name:'Tenant Name',index:'id', width:90,align:"left",searchoptions: {sopt: conditions},table:table,change_type: 'tenantname1', extra_columns: ['id'],original_index: 'id'},
            {name:'Property Name',index:'property_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
            /*{name:'Unit Number',index:'unit_id', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_property',formatter:getUnitDetails},*/
            {name:'Unit Number',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:'Rent (USh)',index:'rent_amount', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:currencyFormatter},
            {name:'Lease ID',index:'user_id', width:100,searchoptions: {sopt: conditions},table:'tenant_lease_details'},
            {name:'Days Remaining',index:'days_remaining', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter2},
            {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'tenant_details',formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];
        var ignore_array = [];
        jQuery("#lease_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Leases",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }

    function getUnitDetails( cellvalue, options, rowObject){
        var string = "";
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getUnitById',
            data: {
                class: "TenantAjax",
                action: "getUnitById",
                id: cellvalue
            },
            async: false,
            success: function (response) {
                var res = JSON.parse(response);
                if (res.status == "success") {
                    string = res.data.unit_prefix+"-"+res.data.unit_no;
                }
            }
        });
        return string;
    }

    function statusFormatter (cellValue, options, rowObject){

        if (cellValue == '1')
            return "Active";
        else if(cellValue == '0')
            return "Inactive";
        else if(cellValue == '6')
            return "Move In";
        else if(cellValue == '7')
            return "Renewed";
        else
            return '';

    }function statusFormatter2 (cellValue, options, rowObject){
        today=new Date();
        dt1 = new Date(today);
        dt2 = new Date(cellValue);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }
    function currencyFormatter (cellValue, options, rowObject){
        if(cellValue!==undefined && cellValue!==""){
            return currencySymbol+''+cellValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+".00";
        } else {
            return "";
        }
    }
    function statusFormatter3 (cellValue, options, rowObject){
           return 1000;
    }
    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject['Status'] == '1')  select = ['Edit','Print','Add New Lease','File Library','Notes & History','Run Background Check','Delete Lease'];
            if(rowObject['Status'] == '6')  select = ['Edit','Print','Add New Lease','File Library','Notes & History','Run Background Check','Renew This Lease','Delete Lease'];
            if(rowObject['Status'] == '7')  select = [];
            if(rowObject['Status'] == '')  select = select = ['Edit','Print','Add New Lease','File Library','Notes & History','Run Background Check','Renew This Lease','Delete Lease'];
            var data = '';

            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }

            return data;
        }
    }
    $(document).on('click','#import_tenant',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_tenant_type_div').show(500);
    });
    $(document).on("click", "#import_tenant_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_tenant_type_div").hide(500);
                }
            }
        });
    });
    $("#importTenantTypeFormId").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importPropertyForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'TenantTypeAjax');
            formData.append('action', 'importExcel');
            $.ajax({
                type: 'post',
                url: '/tenantTypeAjax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);

                        $("#import_tenant_type_div").hide(500)
                        $('#lease_listing').trigger('reloadGrid');
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                        // $('.error').html(response.message);
                        // $.each(response.message, function (key, value) {
                        //     $('.'+key).html(value);
                        // });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });


    function deletetenant(id) {
        $.ajax({
            type: 'post',
            url: '/tenantTypeAjax',
            data: {
                class: "TenantTypeAjax",
                action: "deletetenant",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#lease_listing').trigger( 'reloadGrid' );
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /** Export unit type excel  */
    $(document).on("click",'#export_tenant',function(){
        var base_url = window.location.origin;
        var status = $("#jqGridStatus option:selected").val();
        var table = 'users';
        var table1 = 'general_property';
        var table2 = 'tenant_phone';
        var table3 = 'tenant_details';
        var table4 = 'tenant_lease_details';
        var table5 = 'unit_details';
        var table6 = 'building_detail';
        var table7 = 'tenant_property';
        window.location.href = base_url+"/tenantTypeAjax?status="+status+"&&table="+table+"&&table1="+table1+"&&table2="+table2+"&&table3="+table3+"&&table4="+table4+"&&table4="+table4+"&&table5="+table5+"&&table6="+table6+"&&table7="+table7+"&&action=exportExcel";
        $(this).off('click');
        return false;
    });


    $(document).on("change", "#lease_listing .select_options", function (e) {
        e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var elem = $(this);
        switch (action) {
            case "Add New Lease":
                window.location.href = base_url + '/Lease/Leases';
                break;
            case "Delete Lease":
                bootbox.confirm("Are you sure you want to delete this entry?", function (result) {
                    if (result == true) {
                        deletetenant(id);
                    } else {
                        $('#lease_listing').trigger( 'reloadGrid' );
                    }
                });
                break;
            case "Edit":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-one";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Print":
              toastr.warning('Coming soon');
                $('#lease_listing').trigger( 'reloadGrid' );

                break;
            case "File Library":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-eight";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Payment History":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-five";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Notes & History":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-seven";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Enter Credit":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-four";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "New Invoice":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-three";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "CAM Charge":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-three";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Send Tenant Statement":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-five";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "HOA Violation Tracking":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-forteen";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Apply Payment":
                /*localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-fifteen";
                localStorage.setItem('edit_active',editActiveTab);*/
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Work Order":
                window.location.href = base_url + '/WorkOrder/WorkOrders?WorkOrder='+id;
                break;
            case "Add In-touch":
                /*localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-fifteen";
                localStorage.setItem('edit_active',editActiveTab);*/
                window.location.href = base_url + '/Communication/InTouch';
                break;
            case "In-Touch History":
                /*localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-fifteen";
                localStorage.setItem('edit_active',editActiveTab);*/
                window.location.href = base_url + '/Communication/InTouch';
                break;
            case "Email History":
                window.location.href = base_url + '/Communication/SentEmails';
                break;
            case "TEXT History":
                window.location.href = base_url + '/Communication/TextMessage';
                break;
            case "Run Background Check":

                $('#backGroundCheckPopCondition').modal('show');
                $('#btnBackgroundSelection').attr('tent_id',id);
                elem.val('default');
                break;
            case "Print Envelope":
                alert("print");
                break;
            case "ONLINE PAYMENT":
                alert("online Payment");
                break;
            default:
                window.location.href = base_url + '/Setting/ApexNewUser';
        }
    });
    function deletetenant(id) {
        $.ajax({
            type: 'post',
            url: '/tenantTypeAjax',
            data: {
                class: "TenantTypeAjax",
                action: "deletetenant",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#lease_listing').trigger( 'reloadGrid' );
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

});

$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {
        $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.location.href = 'https://www.victig.com';
    }
});