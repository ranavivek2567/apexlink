$(document).ready(function () {
    // get id from url for Property Detail
    var id = getParameterByName('id');
    if (id) {
        
        setTimeout(()=> {
             selectedProperty(id);
        }, 2000);
    }
    
    // // get id from url for Property Detail Ends
    $(".current_zip").val("10001");
    $(".current_employer_zip").val("10001");
    $(".previous_zip").val("10001");
    $(".previous_employer_zip").val("10001");
    $(".co_zipcode").val("10001");
    $(document).on("click","#add_rental_button_cancel",function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    var baseUrl = window.location.origin;
                    window.location.href = baseUrl+'/RentalApplication/RentalApplications';
                }
            }
        });
    });

    // get id from url for Property Detail Starts
    function selectedProperty(id) {
         $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: 'RenatlApplicationAjax',
                action: 'getrentalpropdetails',
                id:id,
            },

            success: function (data) {
             
                var response = JSON.parse(data);

                    $("#Rentalcity").val(response.data.city);
                    $("#Rentalzipcode").val(response.data.zipcode); 
                    $("#Rentaladdress").val(response.data.address1); 
                    $("#Rentaladdress2").val(response.data.address2);
                    $("#Rentaladdress3").val(response.data.address3);
                    $("#Rentaladdress4").val(response.data.address4);
                    $("#Rentalstate").val(response.data.state);

                    if (response.data.id !==""){
                        $('#comp_Rentalproperty').val(response.data.id);
                    }

                
            }
        });
    }
    // get id from url for Property Detail Ends
    $(document).on("click","#add_rental_button_Reset",function(){
                    // var baseUrl = window.location.origin;
                    /*window.location.href = baseUrl+'/RentalApplication/RentalApplications';*/
                    window.location.reload();
    });
    $(document).on("click",".collectionreaso1",function(){

        $("#collectionreason121").show();
    });

    searchingProp();
    function myDateFormatter(dateObject){
        var newdate = dateObject.getFullYear() + "-" + (dateObject.getMonth() + 1) + "-" + dateObject.getDate();
        var tmp = [];
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getDateFormat',
            data: {class: "TenantAjax",action: "getDateformat",date: newdate,},
            async: false,
            global:false,
            success: function (response) {
                tmp = $.parseJSON(response);
            }
        });
        return tmp;
    }
    $(document).on("change",".move_in_date",function(){

        var checkdate=$(".move_in_date").val();
        var term=$(".req_lease_term").val();
        var period=$(".req_lease_term").val();
        var d = $.datepicker.parseDate('mm/dd/yy', checkdate);
        var abc=  d.getDate();
        d.setFullYear(d.getFullYear() + 1);
        if(abc !="31"){

            d.setMonth(d.getMonth() + 1);
            d.setDate(0);
        }
        var outdate=myDateFormatter(d)
        $(".move_out_day").val(outdate);
    });
    /*  $(document).on("change",".move_in_date",function(){

          var checkdate=$(".move_in_date").val();
          var term=$(".req_lease_term").val();
          var period=$(".req_lease_term").val();
          var d = $.datepicker.parseDate('mm/dd/yy', checkdate);
          var abc=  d.getDate();
          d.setFullYear(d.getFullYear() + 1);
          if(abc !="31"){

              d.setMonth(d.getMonth() + 1);
              d.setDate(0);
          }
          var outdate=myDateFormatter(d)
          $(".move_out_day").val(outdate);
      });*/
    jQuery('.phone_format').mask('000-000-0000', {reverse: true});
    $('.number_only').keydown(function (e) {
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });
    /**
     * for multiple SSN textbox
     */
    $(document).on("click","#multipleSsn .ssn-plus-sign",function(){
        var clone = $(".multipleSsn:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('input[type=number]').val('');
        $(".multipleSsn").first().after(clone);
        $(".multipleSsn:not(:eq(0))  .ssn-plus-sign").remove();
        $(".multipleSsn:not(:eq(0))  .ssn-remove-sign").show();
    });
    /**
     * remove
     */
    $(document).on("click",".ssn-remove-sign",function(){
        $(this).parents(".multipleSsn").remove();
    });

    /**
     * multiple mail textbox
     */
    $(document).on("click", ".email-plus-sign-emergency", function () {
        var emailRowLenght = $(this).parents(".multipleEmailadditional").parent().find(".multipleEmailadditional");
        if (emailRowLenght.length == 2) {
            $(this).parents(".multipleEmailadditional:first").find(".email-plus-sign-emergency").hide();
        }
        var clone = $(this).parents(".multipleEmailadditional:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(this).parents(".multipleEmailadditional").first().after(clone);
        clone.find(".email-plus-sign-emergency").remove();
        clone.find(".fa-minus-circle-emergency").show();

    });

    /*for multiple email textbox*/


    /*Remove Email Textbox*/
    $(document).on("click", ".fa-minus-circle-emergency", function () {
        var emailRowLenght = $(this).parents(".multipleEmailadditional").parent().find(".multipleEmailadditional");
        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".email-plus-sign-emergency").show();
        }
        $(this).parents(".multipleEmailadditional").remove();
    });

    /*Remove Email Textbox*/
    $(document).on("click", ".email-plus-sign-short-term", function () {
        var emailRowLenght = $(".multipleEmailShortTerm");
        if (emailRowLenght.length == 2) {

            $(".email-plus-sign-short-term").hide();
        }
        var clone = $(".multipleEmailShortTerm:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".multipleEmailShortTerm").first().after(clone);
        $(".multipleEmailShortTerm:not(:eq(0))  .email-plus-sign-short-term").remove();
        $(".multipleEmailShortTerm:not(:eq(0))  .fa-minus-circle-short-term").show();

    });

    /*for multiple email textbox*/


    /*Remove Email Textbox*/
    $(document).on("click", ".fa-minus-circle-short-term", function () {
        var emailRowLenght = $(".multipleEmailShortTerm");

        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".email-plus-sign-short-term").show();
        }
        $(this).parents(".multipleEmailShortTerm").remove();
    });

    /*Remove Email Textbox*/


    /**
     * referral source data fetching
     */

    referralSourceList();
    function referralSourceList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'referralSourceData',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#referral_id').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#referralsource').append($("<option value = "+value.id+">"+value.referral+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * phone type fetching data
     */




    //
    // /**
    //  *Get location on the basis of zipcode
    //  */
    // function getAddressInfoByZip1(zip) {
    //
    //     if (zip.length >= 5 && zip != 'undefined') {
    //         var addr = {};
    //         $.ajax({
    //             type: 'post',
    //             url: '/common-ajax',
    //             data: {zip_code: zip,
    //                 class: 'CommonAjax',
    //                 action: 'getZipcode'},
    //             success: function (res) {
    //                 response1(res);
    //
    //             },
    //         });
    //
    //     } else {
    //         response1({success: false});
    //     }
    // }
    //
    // /**
    //  * Set values in the city, state and country when focus loose from zipcode field.
    //  */
    // function response1(obj) {
    //     var objres = JSON.parse(obj);
    //     if (objres.status == 'success') {
    //         $('#city').val(objres.city);
    //         $('#state').val(objres.state);
    //         $('#country').val(objres.country);
    //     }
    // }
    //
    //
    // $(function () {
    //
    //
    //     /**
    //      * Auto fill the city, state and country when focus loose on zipcode field.
    //      */
    //     $("#zipcode").focusout(function () {
    //         getAddressInfoByZip1($(this).val());
    //     });
    //
    //     /**
    //      * Auto fill the city, state and country when enter key is clicked.
    //      */
    //     $("#zipcode").keydown(function (event) {
    //         var keycode = (event.keyCode ? event.keyCode : event.which);
    //         if (keycode == '13') {
    //             getAddressInfoByZip1($(this).val());
    //         }
    //     });
    //
    //
    // });
    //
    //
    //
    //
    // /**
    //  * Auto fill the city, state and country when focus loose on zipcode field.
    //  */
    // $(function () {       $('#addForwordingAddress_Moveout #zipcode').on('keyup keypress', function (e) {
    //     var keyCode = e.keyCode || e.which;
    //     if (keyCode === 13) {
    //         e.preventDefault();
    //         return false;
    //     }
    // });       /**
    //  * Auto fill the city, state and country when focus loose on zipcode field.
    //  */
    // $("#addForwordingAddress_Moveout #FrwdAddress_Zip").focusout(function () {
    //     getZipCode('#zipcode',$(this).val(),'#city','#state','#country','','#zip_code_validate');
    //     // getAddressInfoByZip($(this).val());
    // });       /**
    //  * Auto fill the city, state and country when enter key is clicked.
    //  */
    // $("#addForwordingAddress_Moveout #FrwdAddress_Zip").keydown(function (event) {
    //     var keycode = (event.keyCode ? event.keyCode : event.which);
    //     if (keycode == '13') {
    //         getZipCode('#zipcode',$(this).val(),'#city','#state','#country','','#zip_code_validate');
    //     }
    // });
    // });
    // /**
    //  * Auto fill the city, state and country when focus loose on zipcode field.
    //  */
    //
    //
    //




    phoneTypeList();
    function phoneTypeList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestPhoneType',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestPhoneType').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#phone_type_rental').append($("<option value = "+value.id+">"+value.type+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * carrier fetching data
     */
    guestCarrierList();
    function guestCarrierList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'carrierdata',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestCarrier').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestCarrier').append($("<option value = "+value.id+">"+value.carrier+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }


    /**
     * guest card countries fetching data
     */

    guestCountriesList();
    function guestCountriesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestCountries',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestCountries').empty().append("<option value='0'>Select</option>");
                    var html = '';
                    $.each(response.data, function (key, value) {
                        var name = value.name+' ('+value.code+ ')';
                        if(value.id  == '220'){
                            html += '<option value="' + value.id + '" selected>' +name+ '</option>';
                        }else{
                            html += '<option value="' + value.id + '">' +name+ '</option>';
                        }
                    });
                    $('#guestCountries').append(html);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest card Ethnicity fetching data
     */

    guestEthnicityList();
    function guestEthnicityList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestEthnicity',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestEthnicity').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestEthnicity').append($("<option value = "+value.id+">"+value.title+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest marital status fetching data
     */
    guestMaritalStatusList();
    function guestMaritalStatusList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestMaritalStatus',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestMarital').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestMarital').append($("<option value = "+value.id+">"+value.marital+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest hobbies fetching data
     */
    guestHobbiesList();
    function guestHobbiesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestHobbies',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestHobbies').empty();
                    $.each(response.data, function (key, value) {
                        $('#guestHobbies').append($("<option value = "+value.id+">"+value.hobby+"</option>"));
                    });
                    $("#guestHobbies").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * guest veteran status fetching data
     */
    guestVeteranList();
    function guestVeteranList(){

        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestVeteranStatus',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#guestVeteran').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#guestVeteran').append($("<option value = "+value.id+">"+value.veteran+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * clone phone number
     */

    $(document).on("click", ".additionalincome .fa-plus-circle", function () {
        var clone = $(".additionalincome:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".additionalincome").first().after(clone);
        $(".additionalincome:not(:eq(0))  .fa-plus-circle").hide();
        $(".additionalincome:not(:eq(0))  .fa-minus-circle").show();
        var phoneRowLenght = $(".");
        console.log(phoneRowLenght);
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        } else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".additionalincome:eq(0) .fa-plus-circle").hide();
        } else {
            $(".additionalincome:not(:eq(0)) .fa-plus-circle").show();
        }
    });
    $(document).on("click", ".additionalincome .fa-minus-circle", function () {

        $(this).parents(".additionalincome").remove();
    });







    $(document).on("click", ".otheroccupants .fa-plus-circle", function () {
        var clone = $(".otheroccupants:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        clone.find('input[type=number]').val(''); //harjinder
        $(".otheroccupants").first().after(clone);
        $(".otheroccupants  .fa-plus-circle:not(:eq(0))").remove();
        $(".otheroccupants  .fa-minus-circle:not(:eq(0))").show();

    });
    /* for multiple SSN textbox*/


    /*remove ssn textbox*/
    $(document).on("click", ".otheroccupants .fa-minus-circle", function () {
        $(this).parents(".otheroccupants").remove();
    });

    $("#hide").show();

    $(document).on("click",".leavingreason",function(){
        $("#leavingreason1").show();
    });




    //
    $('input[type=checkbox][name=rentalhistory]').change(function() {
        var check=$("#rental_history_form").serializeArray();
        console.log(check);
        if (check[10]['value'] == 1) {
            $("#previousrental").show();
        }
        else {

            $("#previousrental").hide();

        }
    });

    $('input[type=checkbox][name=employmenthistory]').change(function() {
        var check=$("#employment_history_form").serializeArray();
        console.log('checkkk',check);
        if (check[10]['value'] == '1') {
            $("#previousemployer").show();
        }
        else {

            $("#previousemployer").hide();

        }
    });


//emergencyemail
    $(document).on("click", ".add_email", function () {
        var clone = $(".emailaddition:first").clone();
        clone.find('input[type=text]').val('');
        $(".emailaddition").first().after(clone);
        $(".emailaddition:not(:eq(0))  .add_email").remove();
        $(".emailaddition:not(:eq(0))  .remove_email").show();

    });

    $(document).on("click", ".remove_email", function () {
        $(this).parents(".emailaddition").remove();
    });
//emergencyphone

    $(document).on("click", ".add_emergencyphone", function () {
        var clone = $(".emergencyphone:first").clone();
        clone.find('input[type=text]').val('');
        $(".emergencyphone").first().after(clone);
        $(".emergencyphone:not(:eq(0))  .add_emergencyphone").remove();
        $(".emergencyphone:not(:eq(0))  .remove_emergencyphone").show();

    });

    $(document).on("click", ".remove_emergencyphone", function () {
        $(this).parents(".emergencyphone").remove();
    });



//signatoremail
    $(document).on("click", ".addsigner_email", function () {
        var clone = $(".cosignator_email:first").clone();
        clone.find('input[type=text]').val('');
        $(".cosignator_email").first().after(clone);
        $(".cosignator_email:not(:eq(0)) .addsigner_email").remove();
        $(".cosignator_email:not(:eq(0)) .removesigner_email").show();

    });

    $(document).on("click", ".removesigner_email", function () {
        $(this).parents(".cosignator_email").remove();
    });
//signatorphone

    $(document).on("click", ".add_signerphone", function () {
        var clone = $(".signer_phone:first").clone();
        clone.find('input[type=text]').val('');
        $(".signer_phone").first().after(clone);
        $(".signer_phone:not(:eq(0)) .add_signerphone").remove();
        $(".signer_phone:not(:eq(0)) .remove_signerphone").show();

    });

    $(document).on("click", ".remove_signerphone", function () {
        $(this).parents(".signer_phone").remove();
    });
    //
    // $(document).on("click",".primary-tenant-phone-row .fa-minus-circle",function(){
    //
    //     var phoneRowLenght = $(".primary-tenant-phone-row");
    //     if (phoneRowLenght.length == 2) {
    //         $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").show();
    //     }else if (phoneRowLenght.length == 3) {
    //         $(".primary-tenant-phone-row:eq(0) .fa-plus-circle").hide();
    //     }else{
    //         $(".primary-tenant-phone-row:not(:eq(0)) .fa-plus-circle").show();
    //     }
    //
    //     $(this).parents(".primary-tenant-phone-row").remove();
    // });

    /**
     * general information Insert Data
     */
    $(document).on("click",".savelease",function(){
        var check = jQuery.trim($("#search_records").html());
        var check2=  $("input[name='unit_id']:checked").length;
        if(check=="")
        {
            toastr.error("Please search and select a Unit.");
        }
        if(check!="" && check2==0){
            toastr.error("Please search and select a Unit.");
        }

        if(check!="" && check2==1){

            generalInformationData();
        }
        //   generalInformationData();
    });
    function generalInformationData(){
        var formData = $('#generallease').serializeArray();
        var formData2 = $('#emergency_details').serializeArray();
        var notes=$('#notes_form').serializeArray();
        var expected_move_in=$('.expected_move_in').val();
        console.log(expected_move_in);
        var checked=$("input:radio[name=unit_id]:checked").val();
        var unit_id = 0;
        if(checked!=undefined) {
            console.log("2");
            if(checked=='on') {
                console.log("3");
                var checked1=$("input:radio[name=unit_id]:checked");
                unit_id = checked1.parent().next().text();         // Retrieves the text within <td>
            }
            else
            {
                console.log("4");
                unit_id=0;
            }
            console.log("5");
        }else{
            console.log("6");
            unit_id = 0;
        }

        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'insert',
                formData: formData,
                unit_id: unit_id,
                formData2:formData2,
                notes: notes,
                expected_move_in: expected_move_in
            },
            success: function (response) {
                var data = JSON.parse(response);
                console.log(data);
                if (data.status == "success") {
                    $("#salutation").val('');
                    toastr.success("This record saved successfully");
                    setTimeout(function () {
                        onTop(true);
                        window.location.href = window.location.origin+"/GuestCard/ListGuestCard";
                    }, 500);

                }
                if(data.status =="error"){
                    var firstName=$("#first_name").val();
                    var lastName=$("#last_name").val();
                    var email=$(".emailll").val();

                    var carrier=$("#guestCarrier").val();
                    var phone=$(".phoner").val();
                    console.log('email',email);
                    if(firstName==""){
                        $(".val_message_first").show();
                    }
                    else
                    {
                        $(".val_message_first").hide();
                    }
                    if(lastName==""){
                        $(".val_message_last").show();
                    }else{
                        $(".val_message_last").hide();
                    }
                    if(email==""){
                        $(".val_message_email").show();
                    }
                    else{
                        $(".val_message_email").hide();
                    }
                    if(carrier==""){
                        $(".val_message_carrier").show();
                    }
                    else{
                        $(".val_message_carrier").hide();
                    }
                    if(phone==""){
                        $(".val_message_phone").show();
                    }
                    else{
                        $(".val_message_phone").hide();
                    }
                }
            }
        });
    }

    /**
     * guest properties DDL
     */
    guestProperties();
    function guestProperties(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestProperty',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#PropertyId').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#PropertyId').append($("<option value = "+value.id+">"+value.property_name+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }
    $(document).on("change","#PropertyId",function () {
        var prop_id = this.value;
        guestUnit(prop_id);
    });

    /**
     * guest unit DDL
     * @param prop_id
     */
    function guestUnit(prop_id){
        $('#PropertyId').val();
        //console.log($('#PropertyId').val());
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'guestUnitBuilding',
                prop_id: prop_id
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#unitId').empty().append("<option value=''>Select</option>");
                    $.each(response.data, function (key, value) {
                        $('#unitId').append($("<option value = "+value.id+">"+value.unit_prefix+"-"+value.unit_no+"</option>"));
                    });
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }



    /**
     * guest card Emergency countries fetching data
     */

    emerCountriesList();
    function emerCountriesList(){
        $.ajax({
            type: 'post',
            url: '/guest-card-ajax',
            data: {
                class: 'GuestCardAjax',
                action: 'emerCountries',
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#emerCountries').empty().append("<option value=''>Select</option>");
                    var html = '';
                    $.each(response.data, function (key, value) {
                        var name = value.name+' ('+value.code+ ')';
                        if(value.id  == '220'){
                            html += '<option value="' + value.id + '" selected>' +name+ '</option>';
                        }else{
                            html += '<option value="' + value.id + '">' +name+ '</option>';
                        }
                    });
                    $('#emerCountries').append(html);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * emergency contact detail clone
     */
    $(document).on("click",".add-emergency-contant",function(){
        var clone = $(".lease-emergency-contact:first").clone();
        clone.find('input[type=text]').val('');
        clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});;
        $(".lease-emergency-contact").first().after(clone);
        clone.find(".add-emergency-contant").hide();
        clone.find(".remove-emergency-contant").show();
    });
    $(document).on("click",".remove-emergency-contant",function(){
        $(this).parents(".lease-emergency-contact").remove();
    });


    $(document).on("click",".addNote",function(){

        var clone = $(".chargeNoteHtml:first").clone();
         clone.find('.chargeNoteClone').val('');
   //     clone.find('input[type=textarea]').val(''); //harjinder
        $(".chargeNoteHtml").first().after(clone);
        $(".chargeNoteHtml:not(:eq(0))  .addNote").remove();
        $(".chargeNoteHtml:not(:eq(0))  .removeNote").show();
    });



    $(document).on("click",".removeNote",function(){
        var phoneRowLenght = $(".primary-tenant-phone-row");
        $(this).parents(".chargeNoteHtml").remove();
    });


    $(document).on("click",".emergency_div_plus",function(){
        // alert("hello");
        var count = $(".lease-emergency-div");
        var divLength = count.length;
        console.log(divLength);
        var clone = $(".lease-emergency-div:first").clone();
        clone.find('input[type=textarea]').val(''); //harjinder
        clone.find('input[type=number]').val(''); //harjinder
        clone.find('input[type=text]').val(''); //harjinder
        clone.find('input[type=text]').val(''); //harjinder
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
       // $(".co_zipcode").val("10001");
        clone.find("input[name='email_0[]']").attr("name","email_"+divLength+'[]');
       // $(".co_zipcode").val("10001");
        $(".lease-emergency-div").first().after(clone);
        $(".lease-emergency-div:not(:eq(0))  .emergency_div_plus").remove();
        $(".lease-emergency-div:not(:eq(0))  .emergency_div_minus").show();
    });



    $(document).on("click",".emergency_div_minus",function(){
        $(this).parents(".lease-emergency-div").remove();
    });
    $(document).on("click","#phone_type_rental",function(){
        var val= $(this).val();
        if(val=='2' || val=='5'){
            $(this).parents("#short_term_rental").find(".ext_phone").show();
        }else{
            $(this).parents("#short_term_rental").find(".ext_phone").hide();
        }
    });


    /*Changes on 22/08/19 Start*/
    $(document).on("change","#salutation",function(){
        var value = $(this).val();
        if(value=="Dr.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("0");
        }
        if(value=="Mr.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Mrs.")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Mr. & Mrs.")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("4");
        }
        if(value=="Ms.")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Sir")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Madam")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Brother")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Sister")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="Father")
        {
            $(".maiden_name_hide").hide();
            $("#general").val("1");
        }
        if(value=="Mother")
        {
            $(".maiden_name_hide").show();
            $("#general").val("2");
        }
        if(value=="")
        {
            $(".maiden_name_hide").hide();

        }
    });

    /*Changes on 22/08/19 End*/


    /*Changes Country code*/
    $(function () {
        $('#generallease .zipcode').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        $("#generallease .zipcode").focusout(function () {
            getZipCode('.zipcode',$(this).val(),'.city','.state','.country','','#zip_code_validate');
            // getAddressInfoByZip($(this).val());
        });

        $("#generallease .zipcode").keydown(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                getZipCode('.zipcode',$(this).val(),'#FrwdAddress_City','#FrwdAddress_State','#FrwdAddress_Country','','#zip_code_validate');
            }
        });
    });
    /*Changes Country code*/
    $(document).on("click", "#shorttermrentals", function () {
        $("#shortTermRental").modal(show);
    });




    function searchingProp() {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "searchingProp",
            },
            success: function (response) {
                var html = "";
                var response = JSON.parse(response);
                html += ' <option value="">' + 'Select Property' + '</option>';
                for (var i = 0; i < response.data.prop.length; i++) {

                    html += ' <option value="' + response.data.prop[i].id + '">' + response.data.prop[i].property_name + '</option>';
                }
                $(".prop_short").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('change', '.prop_short', function () {
        var id = $(".prop_short").val();
        console.log(id);
        searchingBuilding(id);
        getpropdetails(id);
    });

    function searchingBuilding(id) {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "searchingBuilding",
                id: id
            },
            success: function (response) {
                var html = "";
                var response = JSON.parse(response);
                html += ' <option value="">' + 'Select Building' + '</option>';
                for (var i = 0; i < response.data.prop.length; i++) {
                    html += ' <option value="' + response.data.prop[i].id + '">' + response.data.prop[i].building_name + '</option>';
                }
                $(".build_short").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on('change', '.build_short', function () {
        var id = $(".build_short").val();
        searchingUnit(id);
    });

    function searchingUnit(id) {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "searchingUnit",
                id: id
            },
            success: function (response) {
                var html = "";
                var response = JSON.parse(response);
                html += ' <option value="">' + 'Select Unit' + '</option>';
                for (var i = 0; i < response.data.prop.length; i++) {
                    html += ' <option value="' + response.data.prop[i].id + '">' + response.data.prop[i].unit_prefix + "-" + response.data.prop[i].unit_no + '</option>';
                }
                $(".unit_short").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on('change', '.unit_short', function () {
        var id = $(".unit_short").val();
        getdetails(id);
    });
    function getdetails(id) {
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "getdetails",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                $(".marketRent").val(response.data.market_rent);
                $(".baseRent").val(response.data.base_rent);
                $(".secdeposite").val(response.data.security_deposit);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function getpropdetails(id) {
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "getpropdetails",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                $(".address1").val(response.data.address1);
                $(".address2").val(response.data.address2);
                $(".address3").val(response.data.address3);
                $(".address4").val(response.data.address4);
                $(".zip__code").val(response.data.zipcode);
                $(".cityRental").val(response.data.city);
                $(".stateRental").val(response.data.state);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("click", ".primary-tenant-phone-row-short .fa-plus-circle", function () {
        var clone = $(".primary-tenant-phone-row-short:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".primary-tenant-phone-row-short").first().after(clone);
        $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-tenant-phone-row-short:not(:eq(0))  .fa-minus-circle").show();
        var phoneRowLenght = $(".primary-tenant-phone-row-short");
        console.log(phoneRowLenght);
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(".ext_phone:not(:eq(0))").hide();
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        } else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-short:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on("click", ".primary-tenant-phone-row-short .fa-minus-circle", function () {

        var phoneRowLenght = $(".primary-tenant-phone-row-short");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").show();
        } else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row-short:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-short:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-tenant-phone-row-short").remove();
    });
    $(document).on("click", ".primary-tenant-phone-row-emergency .fa-plus-circle", function () {
        var clone = $(".primary-tenant-phone-row-emergency:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".primary-tenant-phone-row-emergency").first().after(clone);
        $(".primary-tenant-phone-row-emergency:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-tenant-phone-row-emergency:not(:eq(0))  .fa-minus-circle").show();
        var phoneRowLenght = $(".primary-tenant-phone-row-emergency");
        console.log(phoneRowLenght);
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        } else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-tenant-phone-row-emergency:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-emergency:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on("click", ".primary-tenant-phone-row-emergency .fa-minus-circle", function () {

        var phoneRowLenght = $(".primary-tenant-phone-row-emergency");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row-emergency:eq(0) .fa-plus-circle").show();
        } else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row-emergency:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-emergency:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-tenant-phone-row-emergency").remove();
    });
    $("#rent_details_form").validate({
        rules: {
            prop_short: {
                required: true
            },
            build_short: {
                required: true
            },
            unit_short: {
                required: true,
            },
        },
    });

    $("#save_rental_general").validate({
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            carrier: {
                required: true,
            },
            phone_number: {
                required: true,
            },
        },
        /* errorPlacement: function(error, element) {
             if (element.attr("name") == "email")
                 error.insertAfter(".err-class");
         }*/
    });
    $(document).on('click', '.saveRenatlApplications', function () {
        var firstForm = $("#save_rental_general").valid();
        var secondForm = $("#rent_details_form").valid();
        if (firstForm && secondForm) {   // test for validity
            saverentalApplicaions();
        } else {
            toastr.warning("Please fill all required fields!")
            return false;
        }

    });
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "getresonleaving",
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == "success") {
                console.log(response.data);
                if (response.data.length > 0){
                    var reasonLeavingOption = "<option value='0'>Select</option>";
                    $.each(response.data, function (key, value) {
                        reasonLeavingOption += "<option value='"+value.id+"'>"+value.reasonName+"</option>";
                    });
                    $('#current_reason').html(reasonLeavingOption);
                    $('#previous_reason').html(reasonLeavingOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        data: {
            class: "TenantAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);

            if (data.status == "success") {

                if (data.data.state.state != ""){
                    $("#driverProvince").val(data.data.state.state);
                }

                if (data.data.country.length > 0){
                    var countryOption = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        if(value.id  == '220'){
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"' selected>"+value.name+" ("+value.code+")"+"</option>";
                        }else{
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                        }

                    });
                    $('#addTenant .countycodediv select').html(countryOption);
                    $('.emergencycountry').html(countryOption);
                    $('#guar_coun_code').html(countryOption);
                }

                if (data.data.propertylist.length > 0){
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.propertylist, function (key, value) {
                        propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                    });
                    $('#addTenant #property').html(propertyOption);
                }

                if (data.data.phone_type.length > 0){
                    var phoneOption = "";
                    $.each(data.data.phone_type, function (key, value) {
                        phoneOption += "<option value='"+value.id+"'>"+value.type+"</option>";
                    });
                    $('.primary-tenant-phone-row select[name="phoneType[]"]').html(phoneOption);
                    $('.addition_tenant_block select[name="additional_phoneType"]').html(phoneOption);
                    $('#guar_phone_type').html(phoneOption);
                }

                if (data.data.carrier.length > 0){

                    var carrierOption = "<option value=''>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                    });
                    $('.primary-tenant-phone-row select[name="carrier[]"]').html(carrierOption);
                    $('#guar_carrier').html(carrierOption);
                }
                if (data.data.carrier.length > 0){

                    var carrierOption = "<option value='0'>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                    });
                    $('.addition_tenant_block select[name="additional_carrier"]').html(carrierOption);
                    $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                    $('.additional_carrier').html(carrierOption);
                    $(' select[name="guarantor_form2_carrier_1[]"]').html(carrierOption);
                }

                if (data.data.referral.length > 0){
                    var referralOption = "";
                    $.each(data.data.referral, function (key, value) {
                        referralOption += "<option value='"+value.id+"'>"+value.referral+"</option>";
                    });
                    $('select[name="referralSource"]').html(referralOption);
                    $('.addition_tenant_block select[name="additional_referralSource"]').html(referralOption);
                }

                if (data.data.ethnicity.length > 0){
                    var ethnicityOption = "";
                    $.each(data.data.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='"+value.id+"'>"+value.title+"</option>";
                    });
                    $('select[name="ethncity"]').html(ethnicityOption);
                    $('.addition_tenant_block select[name="additional_ethncity"]').html(ethnicityOption);
                }

                if (data.data.marital.length > 0){
                    var maritalOption = "";
                    $.each(data.data.marital, function (key, value) {
                        maritalOption += "<option value='"+value.id+"'>"+value.marital+"</option>";
                    });
                    $('select[name="maritalStatus"]').html(maritalOption);
                    $('.addition_tenant_block select[name="maritalStatus"]').html(maritalOption);
                    $('.additional_maritalStatus').html(maritalOption);
                }

                if (data.data.hobbies.length > 0){
                    var hobbyOption = "";
                    $.each(data.data.hobbies, function (key, value) {
                        hobbyOption += "<option value='"+value.id+"'>"+value.hobby+"</option>";
                    });
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').html(hobbyOption);
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                }

                if (data.data.veteran.length > 0){
                    var abc = "";
                    var veteranOption = "<option value='0'>Select</option>";
                    $.each(data.data.veteran, function (key, value) {
                        veteranOption += "<option value='"+value.id+"'>"+value.veteran+"</option>";
                    });
                    $('select[name="veteranStatus"]').html(veteranOption);
                    $('.additional_veteranStatus').html(veteranOption);
                }

                if (data.data.collection_reason.length > 0){
                    var reasonOption = "";
                    $.each(data.data.collection_reason, function (key, value) {
                        reasonOption += "<option value='"+value.id+"'>"+value.reason+"</option>";
                    });
                   // $('#previous_reason').html(reasonOption);
                    //$('#current_reason').html(reasonOption);
                }

                if (data.data.credential_type.length > 0){
                    var typeOption = "";
                    var typeOption = "<option value='0'>Select</option>";
                    $.each(data.data.credential_type, function (key, value) {
                        typeOption += "<option value='"+value.id+"'>"+value.credential_type+"</option>";
                    });
                    $('.tenant-credentials-control select[name="credentialType[]"]').html(typeOption);
                }


            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });
    var file_library = [];
    $(document).on('change','#file_library',function(){
        file_library = [];
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            var upload_url = window.location.origin;
            if(size > 1030) {
                toastr.warning('Please select documents less than 1 mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
                if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                    file_library.push(value);
                    var src = '';
                    var reader = new FileReader();
                    $('#file_library_uploads').html('');
                    reader.onload = function (e) {
                        if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            src = upload_url + '/company/images/excel.png';
                        } else if (type == 'application/pdf') {
                            src = upload_url + '/company/images/pdf.png';
                        } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                            src = upload_url + '/company/images/word_doc_icon.jpg';
                        } else if (type == 'text/plain') {
                            src = upload_url + '/company/images/notepad.jpg';
                        } else if (type == 'text/xml') {
                            src = upload_url + '/company/images/notepad.jpg';
                        } else {
                            src = e.target.result;
                        }
                        $("#file_library_uploads").append(
                            '<div class="row" style="margin:20px">' +
                            '<div class="col-sm-12 img-upload-library-div">' +
                            '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '></div>' +
                            '<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div class="col-sm-3 show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '<div class="col-sm-3"><span id=' + key + ' class="delete_pro_img cursor"><button class="btn-warning">Delete</button></span></div></div></div>');
                    };
                    reader.readAsDataURL(value);
                } else {
                    toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
                }
            }
        });
    });

    function isa_convert_bytes_to_specified(bytes, to) {
        var formulas =[];
        formulas['k']= (bytes / 1024).toFixed(1);
        formulas['M']= (bytes / 1048576).toFixed(1);
        formulas['G']= (bytes / 1073741824).toFixed(1);
        return formulas[to];
    }
    $(document).on('click','.delete_pro_img',function(){
        $(this).parent().parent().parent('.row').remove();
    });

    $(document).on('click','#remove_library_file',function(){
        $('#file_library_uploads').html('');
        $('#file_library').val('');
    });

    function saverentalApplicaions() {
        var formData=$("#save_rental_general").serializeArray();
        var formData2=$("#rent_details_form").serializeArray();
        var formData3=$("#rental_history_form").serializeArray();
        var formData4=$("#employment_history_form").serializeArray();
        var formData5=$("#emergency_details").serializeArray();
        var formData6=$("#rental_emergency_details").serializeArray();
        var formData7=$("#guarranter_form").serializeArray();
        var notes=$("#notes_form").serializeArray();
        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: "RenatlApplicationAjax",
                action: "saverentalApplicaions",
                formData:formData,
                formData2:formData2,
                formData3:formData3,
                formData4:formData4,
                formData5:formData5,
                formData6:formData6,
                formData7:formData7,
                notes:notes
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.code == 400){
                    toastr.warning(response.message);
                }else{
                    localStorage.setItem("Message", 'Record added successfully.');
                    localStorage.setItem("rowcolorRental",'green');
                    var baseUrl = window.location.origin;
                    window.location.href = baseUrl+'/RentalApplication/RentalApplications';
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function getZipCode(element,zipcode,city,state,country,county,validation){
        $(validation).val('1');
        $.ajax({
            type: 'post',
            url: '/common-ajax',
            data: {zip_code: zipcode,class: 'CommonAjax', action: 'getZipcode'},
            success: function (response) {
                var data = JSON.parse(response);
                if(data.code == 200){
                    if(city !== undefined)$(city).val(data.data.city);
                    if(state !== undefined)$(state).val(data.data.state);
                    if(country !== undefined)$(country).val(data.data.country);
                    if(county !== undefined)$(county).val(data.data.county);
                } else {
                    getAddressInfoByZip(element,zipcode,city,state,country,county,validation);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
    getZipCode('.current_zip','10001','.current_city','.current_state','#selectProperty #propertyCountry_name','','');
    $(".current_zip").focusout(function () {
        getZipCode('.current_zip ',$(this).val(),'.current_city','.current_state','#selectProperty #propertyCountry_name','','');
    });
    getZipCode('.current_employer_zip','10001','.current_employee_city','.current_employee_state','#selectProperty #propertyCountry_name','','');
    $(".current_employer_zip").focusout(function () {
        getZipCode('.current_employer_zip ',$(this).val(),'.current_employee_city','.current_employee_state','#selectProperty #propertyCountry_name','','');
    });
    getZipCode('.previous_zip','10001','.previous_city','.previous_state','#selectProperty #propertyCountry_name','','');
    $(".previous_zip").focusout(function () {
        getZipCode('.previous_zip ',$(this).val(),'.previous_city','.previous_state','#selectProperty #propertyCountry_name','','');
    });
    getZipCode('.previous_employer_zip','10001','.previous_employee_city','.previous_employee_state','#selectProperty #propertyCountry_name','','');
    $(".previous_employer_zip").focusout(function () {
        getZipCode('.previous_employer_zip ',$(this).val(),'.previous_employee_city','.previous_employee_state','#selectProperty #propertyCountry_name','','');
    });
    getZipCode('.co_zipcode','10001','.cocity','.costate','#selectProperty #propertyCountry_name','','');
    $(".co_zipcode").focusout(function () {
        getZipCode('.co_zipcode ',$(this).val(),'.cocity','.costate','#selectProperty #propertyCountry_name','','');
    });

// get url id starts
function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
            regex = new RegExp(regexS),
            results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

// get url id ends
    propinfo();
    function propinfo(){

        var id=$('#id_from_other').val();
        console.log(id);

        $.ajax({
            type: 'post',
            url: '/RentalApplication/Ajax',
            data: {
                class: 'RenatlApplicationAjax',
                action: 'getpropdata',
                id : id
            },

            success: function (response) {
                var data = JSON.parse(response);

                if (data.status == "success"){

                    setTimeout(function(){
                        $('.prop_short').val(data.data1.property_id).trigger("change");
                    }, 300);

                        setTimeout(function(){
                            $('.build_short').val(data.data1.building_id).trigger("change");
                        }, 500);

                        setTimeout(function(){
                            console.log(data.data1.id);
                            $('.unit_short').val(data.data1.id).trigger("change");
                        }, 600);

                        $.each(data.data2,function (key,value) {
                           $('.'+key).val(value);

                        });

                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
        });
    }

});