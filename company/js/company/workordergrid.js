$(document).ready(function () {

    workorder();
    function workorder() {
        var table = 'work_order';
        var columns = ['Work Order Number', 'Work Order Category', 'Vendor','Property','Created On','Caller/RequestedBy','Amount(AFN)','Priority ','Status'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [];
        var extra_columns = ['work_order.deleted_at', 'work_order.updated_at'];
        var joins = [{table: 'work_order', column: 'work_order_cat', primary: 'id', on_table: 'company_workorder_category'},
            {table: 'work_order', column: 'property_id', primary: 'id', on_table: 'general_property'},
            {table: 'work_order', column: 'priority_id', primary: 'id', on_table: 'company_priority_type'},
            {table: 'work_order', column: 'status_id', primary: 'id', on_table: 'company_workorder_status'},
            {table: 'work_order', column: 'vendor_id', primary: 'id', on_table: 'users'}];
        var columns_options = [
            {name:'Work Order Number',index:'work_order_number', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Work Order Category',index:'category', width:200,searchoptions: {sopt: conditions},table:'company_workorder_category'},
            {name:'Vendor',index:'name', width:150,searchoptions: {sopt: conditions},table:'users'},
            {name:'Property',index:'property_name', width:100,searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Created On',index:'created_at', width:200,searchoptions: {sopt: conditions},table:table},
            {name:'Caller/RequestedBy',index:'request_id', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Amount(AFN)',index:'estimated_cost', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Priority',index:'priority', width:100,searchoptions: {sopt: conditions},table:'company_priority_type'},
            {name:'Status',index:'work_order_status', width:100,searchoptions: {sopt: conditions},table:'company_workorder_status'},
        ];

        var ignore_array = [];
        jQuery("#workorder_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Work Orders",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

});
