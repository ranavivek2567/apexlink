var base_url = window.location.origin;
var login_user_id =  localStorage.getItem("login_user_id");
var status =  localStorage.getItem("active_inactive_status");

$("#portfolio_id").keypress(function(e){
    var keyCode = e.which;
    /*
      8 - (backspace)
      32 - (space)
      48-57 - (0-9)Numbers
    */
    /* Not allow special */
    if ( !( (keyCode >= 48 && keyCode <= 57)
        ||(keyCode >= 65 && keyCode <= 90)
        || (keyCode >= 97 && keyCode <= 122) )
        && keyCode != 8 && keyCode != 32) {
        e.preventDefault();
    }
});

if(status !== undefined) {
    if ($("#ManageUser-table")[0].grid) {
        $('#ManageUser-table').jqGrid('GridUnload');
    }
    /*intializing jqGrid*/
    if(status == 'all'){
        jqGrid('All');
    }  else {
        jqGrid(status);
    }
    $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

}else{
    if ($("#ManageUser-table")[0].grid) {
        $('#ManageUser-table').jqGrid('GridUnload');
    }
    jqGrid('All');
}

/*custom field button*/
$(document).on('click','#add_custom_field',function(){
    custom_field_validator.resetForm();
    $('#default_value').prop('readonly',false);
    $('#default_value').datepicker('destroy');
});

/*jqGrid status*/
$('#jqGridStatus').on('change',function(){
    var selected = this.value;
    $('#ManageUser-table').jqGrid('GridUnload');
    changeGridStatus(selected);
    jqGrid(selected);
});


$('#admin_user_id').on('change',function(){
    var role_id = $(this).find(':selected').attr('role_id');
    console.log('aaaa', $(this).find(':selected').attr('role_id'));
    getAllUserRoles(role_id, 'onChange');
});

$('#select_role').on('change',function(){
    $('#admin_user_id').val('');
});

/** Get all user roles */
function getAllUsers(){
    $.ajax({
        type: 'post',
        url: '/ManageUser-Ajax',
        data: {
            class: "manageUsers",
            action: "getAllUsers",
        },
        success: function (response) {
            var response = JSON.parse(response);
            var data = response.data;
            $('#admin_user_id').empty().append('<option value="">Select User</option>');
            $.each(data, function (key, value) {
                $("#admin_user_id").append('<option value="' + value['id'] + '"  role_id="' + value['role'] + '">' + value['user_name'] + '</option>');
            });
        }
    });
}


/*display add portfolio div*/
$(document).on('click','#addNewUser',function () {
    $('.property-status').hide(500);
    $('#addNewUserDiv').show();
    headerDiv.innerText = "New User";
    $('#saveBtnId').text('Save');
    $('#edit_user_id').val('');
    $('#record_id').val('');
    $('#addNewUserFormId').trigger("reset");
    var validator = $( "#addNewUserFormId" ).validate();
    var add = 'add';
    getAllUserRoles( null, add);
    getAllUsers();
    validator.resetForm();
    getCustomfield();
});

/**
 * To change the format of phone number
 */
$(".phone_number_format").keydown(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
    $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
});

/** Add new user with/without custom fields **/
$('#addNewUserFormId').validate({
    rules:{
        first_name: {
            required : true
        },
        last_name: {
            required : true
        },
        email: {
            required : true,
            email: true
        },
        role: {
            required : true
        },
        status: {
            required : true
        },
        work_phone: {
            required : true
        },
        mobile_number: {
            required : true
        },
        country_code: {
            required : true
        },

    }
});
$('#addNewUserFormId').on('submit',function(event) {
    event.preventDefault();
    /*checking custom field validation*/
    if ($('#addNewUserFormId').valid()) {
        var formData = $('#addNewUserFormId').serializeArray();

        var custom_field = [];
        $(".custom_field_html input").each(function(){
            var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
            custom_field.push(data);
        });
        var edit_user_id = $('#edit_user_id').val();

        if(edit_user_id){
            var action = 'update';
        } else {
            var action = 'insert';
        }

        $.ajax({
            type: 'post',
            url: '/ManageUser-Ajax',
            data: {
                class: "manageUsers",
                action: action,
                form: formData,
                custom_field:custom_field
            },
            beforeSend: function(xhr) {
                /* checking custom field validations*/
                $(".custom_field_html input").each(function() {
                    var res = validateCustomField($(this).val(), $(this).attr('data_required'), $(this).attr('data_type'), this, this.id);
                    if(res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $('#addNewUserDiv').hide(500);
                    $('.property-status').show(500);
                    $('#list_mode').show(500);
                    // $('.only_edit_mode').show(500);
                    $('#only_list_mode').show(500);
                    // $('#view_mode').hide(500);
                    toastr.success(response.message);
                    $("#ManageUser-table").trigger('reloadGrid');
                    onTop(true,'');
                } else if(response.status == 'error' && response.code == 500){
                    toastr.warning(response.message);
                } else if(response.status == 'error' && response.code == 400){
                    $.each(response.data, function (key, value) {
                        if(key == 'emailErr'){
                            toastr.error(value);
                        } else {
                            $('#' + key).text(value);
                        }
                    });
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }
});
getAllUserRoles(null,null);

/** Get all user roles */
function getAllUserRoles(role_id, action){
    console.log('view>>',role_id, 'ac>>', action);

    $.ajax({
        type: 'post',
        url: '/UserRoles-Ajax',
        data: {
            class: "manageUserRoles",
            action: "getAllUserRoles",
            role_id : role_id
        },
        success: function (response) {
            var response = JSON.parse(response);
            var data = response.data;
            // console.log('data>>',data);
            if(action == 'view') {
                console.log('view>>',data.role_name);
                $(".role").text(data.role_name);
            } else  if(role_id != null || action == 'onChange') {
                console.log('select_role>>',data.id);
                $("#select_role").val(data.id);
            } else if(action !== null) {
                $('#select_role').empty().append('<option value="">Select Role</option>');
                $.each(data, function (key, value) {
                    $("#select_role").append('<option value="' + value['id'] + '">' + value['role_name'] + '</option>');
                });
            } else {
                $.each(data, function (key, value) {
                    $(".select_role_type").append('<option value="' + value['id'] + '">' + value['role_name'] + '</option>');
                    $('.select_role_type').multiselect('destroy').multiselect({
                        includeSelectAllOption: true,
                        nonSelectedText: 'Select Role Type'
                    });
                });
            }
        }
    });
}

/** On select role type change**/
$("select.select_role_type").on('change', function(){
    var selectedRoles = '';
    $('#ManageUser-table').jqGrid('GridUnload');
    if($(".select_role_type option:selected").val() !== undefined) {
        $.each($(".select_role_type option:selected"), function () {
            selectedRoles = selectedRoles + "'" + $(this).val() + "',";
        });
        selectedRoles = selectedRoles.replace(/^,|,$/g, '');
        selectedRoles = '(' + selectedRoles + ')';
    }
    jqGrid(status,selectedRoles);
});

/**
 * jqGrid Initialization function
 * @param status
 */
function jqGrid(status,selectedRoles) {
    var table = 'users';
    var columns = ['Name', 'Email','Phone', 'Last Login', 'Role', 'Status', 'Action'];
    var extra_dropdown = [];
    var select_column = ['Edit','status','Delete'];
    var ignore_array = [{column:'users.id',value:'1'}];
    var joins = [{table:'users',column:'role',primary:'id',on_table:'company_user_roles'}];
    var conditions = ["eq","bw","ew","cn","in"];
    // if(selectedRoles){
    //     var extra_where = [{column:'user_type',value:1,condition:'='},{column:'role', value : selectedRoles,condition:'IN'}];
    // } else {
    //     var extra_where = [{column:'user_type',value:1,condition:'='}];
    // }
    var extra_where = [{column:'user_type',value:'1',condition:'='}];
    var extra_columns = ['users.user_type', 'users.updated_at'];
    var columns_options = [
        {name:'Name',index:'name', width:200,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'username'},
        {name:'Email',index:'email', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'mobile_number', width:180,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Last Login',index:'last_login', width:80,align:"right",searchoptions: {sopt: conditions},table:table,change_type:'last_login'},
        {name:'Role',index:'role_name', width:80, align:"center",searchoptions: {sopt: conditions},table:'company_user_roles'},
        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
        {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table}
    ];
    jQuery("#ManageUser-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            extra_dropdown:extra_dropdown,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'true',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Manage User",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {top:10,left:200,drag:true,resize:false}
    );
}

/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function statusFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "Active";
    else if(cellValue == '0')
        return "InActive";
    else
        return '';
}

/**
 * jqGrid function to format is_default column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function isDefaultFormatter (cellValue, options, rowObject){
    if (cellValue == '1')
        return "Yes";
    else if(cellValue == '0')
        return "No";
    else
        return '';
}

function getUserViewForEdit(id){
    getAllUsers();
    getAllUserRoles( null, 'edit');
    var validator = $( "#addNewUserFormId" ).validate();
    validator.resetForm();
    $.ajax({
        url: "/ManageUser-Ajax",
        method: 'post',
        data: {
            class: "manageUsers",
            action: "getUserById",
            'id': id,
        },
        success: function (response) {

            $('#addNewUserDiv').show(500);
            $('.property-status').hide(500);
            headerDiv.innerText = "Edit User";
            $('#saveBtnId').text('Update');
            var data = $.parseJSON(response);
            if (data.code == 200) {
                $("#edit_user_id").val(data.data.id);
                $.each(data.data, function (key, value) {
                    $('#' + key ).val(value);
                    if ( key == 'email'){
                        $(".user_email").val(value);
                    }
                });

                console.log('data.data>>', data.data);
                console.log('select_role>>', data.data.role);
                console.log('admin_user_id>>', data.data.admin_user_id);
                $("#select_role").val(data.data.role);
                $("#admin_user_id").val(data.data.admin_user_id);

                $('#record_id').val(data.data.id);
                getCustomfield(data.custom_data);
            } else if (data.status == "error"){
                toastr.error(data.message);
            } else{
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
/**  List Action Functions  */
$(document).on('change', '.select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    $('#saveBtnId').text('Update');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {

        $('.table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        jQuery('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        getUserViewForEdit(id);
    } else if (opt == 'Deactivate' || opt == 'DEACTIVATE' || opt == 'Activate' || opt == 'ACTIVATE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to " + opt + " this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    var status = opt == 'activate' ? '1' : '0';
                    $.ajax({
                        url: "/ManageUser-Ajax",
                        method: 'post',
                        data: {
                            class: "manageUsers",
                            action: "updateStatus",
                            'id': id,
                            'status': status,
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#ManageUser-table').trigger('reloadGrid');
                                onTop(true);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
            }
        });
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: "/ManageUser-Ajax",
                        method: 'post',
                        data: {
                            class: "manageUsers",
                            action: "delete",
                            'id': id,
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#ManageUser-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});

$("#ManageUser-table tr").on('click', function () {
    var tr = $(this)[0];
    var trID = tr.id;
    alert("trID=" + trID);
});

$(document).on('click','#ManageUser-table tr td:not(:last-child)',function(){
    var id = $(this).closest('tr').attr('id')
    $.ajax({
        url: "/ManageUser-Ajax",
        method: 'post',
        data: {
            class: "manageUsers",
            action: "getUserById",
            'id': id,
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.code == 200) {
                $('#list_mode').hide(500);
                $('#view_mode').show(500);
                $("#view_edit_user_id").val(data.data.id);
                $.each(data.data, function (key, value) {
                    $('.' + key ).text(value);
                    if ( key == 'role'){
                        getAllUserRoles(value, 'view');
                    }
                    if ( key == 'status'){
                        if ( value == 1){
                            $('.' + key ).text('Active');
                        } else {
                            $('.' + key ).text('InActive');
                        }
                    }
                });
                $("#select_role").val(data.data.role);
                $("#admin_user_id").val(data.data.admin_user_id);

                $('#record_id').val(data.data.id);
                getCustomFieldForViewMode(data.custom_data);
            } else if (data.status == "error"){
                toastr.error(data.message);
            } else{
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
});

/** Back button of view mode*/
$(document).on('click','#back_btn',function(){
    $('#list_mode').show(500);
    $('#only_list_mode').show(500);
    $('#view_mode').hide(500);
    $('.property-status').show(500);
});

/** Edit button click*/
$(document).on('click','#editUserBtn',function(){
    var id = $("#view_edit_user_id").val();
    getUserViewForEdit(id);
    $('#list_mode').show(500);
    $('.only_edit_mode').show(500);
    $('#only_list_mode').hide(500);
    $('#view_mode').hide(500);
});

/** Cancel button click*/
$(document).on('click','#cancelAddUsersBtn',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                var view_edit_user_id = $("#view_edit_user_id").val();
                if (view_edit_user_id){
                    $('.only_edit_mode').hide(500);
                    $(".custom_field_html :input").prop("readonly", true);
                    $('#view_mode').show(500);
                } else {
                    $('#addNewUserDiv').hide(500);
                    $('.property-status').show(500);
                    $('#view_mode').hide(500);
                }
            }
        }
    });
});