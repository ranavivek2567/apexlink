var billIds = [];
var checkboxes = [];
$("#selectDate").datepicker({dateFormat: jsDateFomat}).datepicker("setDate", new Date());
$("#searchDate").datepicker({dateFormat: jsDateFomat}).datepicker("setDate", new Date());
$("#paymentForm").validate({
    rules: {
        date: {
            required:true
        },
        bank: {
            required:true
        },
        payment_type: {
            required:true
        },
        check_number: {
            required:true
        },
        amount: {
            required:true
        }
    }
});

const allEqual = arr => arr.every( v => v === arr[0] );

$(document).on('click','.checkboxgrid',function(){
    var checked = $(this).prop('checked');
    if($(this).attr('data_status') == '1') return false;
    checkboxes = [];
    if(checked){
        billIds = [];
        var tempData =[]
        var user_id = '';
        var amount = 0;
        var dataUsers = [];

        $('.checkboxgrid').each(function(key,value){
            if($(value).prop('checked')) {
                dataUsers.push($(value).attr('data_value'));
                tempData.push($(value).val());
                var originalAmount = $(value).parent().parent().find('td:eq(6)').text().replace('$', '');
                originalAmount = originalAmount.replace(/,/g, '');
                amount += parseFloat(originalAmount);
                checkboxes.push('1');
            }
        });
        if(allEqual( dataUsers )){
            $('#userData').val(dataUsers[0]);
            $('#userData').attr('data_type','Vendor');
            $('#checkoutAmount').val(amount);
            billIds = tempData;
        } else {
            bootbox.alert('Please select the same vendor!');
            $('#checkoutAmount').val('');
            $('.checkboxgrid').prop('checked', false);
            return false;
        }

    }
});


$(document).on('click','#paySelectedBill',function(){
    var user_id = [];
    var bill_id = [];
    $('.checkboxgrid').each(function(key,value){
        if($(value).prop('checked')) {
            user_id.push($(value).attr('data_value'));
            bill_id.push($(value).val());
        }
    });

});

getBankList('');
function getBankList(id){
    $.ajax({
        type: 'post',
        url: '/budgeting-ajax',
        data: {
            class: 'Budgeting',
            action: 'getBankDetails'},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                var html = '<option value="">Select</option>';
                $.each(res.data,function(key,value){
                    if(id == value.id) {
                        html += '<option value="' + value.id + '" selected>' + value.bank_name + '</option>';
                    } else {
                        html += '<option value="' + value.id + '">' + value.bank_name + '</option>';
                    }
                });
                $('#bankName').html(html);
            }
        },
    });
}

$(document).on('change','#payment_type',function(){
    $('#bankAccount').hide();
    $('#checkNumber').hide();
    var user_id = $('#userData').val();
    var type = $('#userData').attr('data_type');
    if($(this).val() == 'Check'){
        $('#bankAccount').show();
        $('#checkNumber').show();
    } else if($(this).val() == 'ACH') {
        if(user_id != '') {
            checkForACH(user_id, type);
        }
    } else if($(this).val() == 'Credit'){
        if(user_id != '') {
            checkForCard(user_id, type)
        }
    }
});

function checkForCard(user_id,type){
    $.ajax({
        type: 'post',
        url: '/accountingReceivable',
        data: {
            class: 'accounting',
            action: 'checkForCard',
            user_id: user_id,
            type:type},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.status == 'false'){
                bootbox.alert(res.message);
                $('#paySelectedBill').prop('disabled',true);
            } else {
                $('#paySelectedBill').prop('disabled',false);
            }
        },
    });
}

function checkForACH(user_id,type){
    $.ajax({
        type: 'post',
        url: '/accountingReceivable',
        data: {
            class: 'accounting',
            action: 'checkForACH',
            user_id: user_id,
            type:type},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.status == 'false'){
                bootbox.alert(res.message);
                $('#paySelectedBill').prop('disabled',true);
            } else {
                $('#paySelectedBill').prop('disabled',false);
            }
        },
    });
}


$(document).on('submit','#paymentForm',function(e){
    e.preventDefault();
    if($('#paymentForm').valid()){
        if($('#payment_type').val() == 'Credit' || $('#payment_type').val() == 'ACH'){
            $('#payNow').modal('show');
            $('.paidAmount').text($('#checkoutAmount').val());
        } else {
            saveTransactionDetail();
        }
    }
});


function saveTransactionDetail(){
    if(checkboxes.length == 0) {
        $('#checkoutAmount').val('') ;
        bootbox.alert('Please select bill for payment.');
        return false;
    }
    $.ajax({
        type: 'post',
        url: '/payBills-ajax',
        data: {
            class: 'accounting',
            action: 'payBillTransaction',
            data: $('#paymentForm').serializeArray(),
            bills:billIds},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.code == 200){
                toastr.success(res.message);
                $('#payment_type').val('');
                $('#checkoutAmount').val('');
                $('#bankName').val('');
                $('#paymentForm [name="check_number"]').val('');
                $('#paybills_table').trigger('reloadGrid');
            }
        },
    });
}


function stripeTokenHandler(token) {
    $(".submit_payment_class").attr("disabled",true);
    var currency = 'USD';
    var Amount = $('#checkoutAmount').val();
    var user_type = 'vendor';
    var vendor_id = $('#userData').val();
    // Insert the token ID into the form so it gets submitted to the server
    $.ajax({
        url:'/payBills-ajax',
        type: 'POST',
        data: {
            "action": 'payBillCheckout',
            "class": 'Paybills',
            "token_id":token,
            "currency":currency,
            "amount": Amount,
            "vendor_id":vendor_id,
            "user_type":user_type,
            "data":billIds,
        }, beforeSend: function() {
            // setting a timeout
            $('#loadingmessage').show();
        },
        success: function (response) {
            $('#loadingmessage').hide();
            $(".submit_payment_class").attr("disabled",false);
            var response = JSON.parse(response);
            if(response.status=='success') {
                $('#payNow').modal('hide');
                $('#paybills_table').trigger('reloadGrid');
                toastr.success(response.message);
            } else {
                toastr.error(response.message);
            }
        }
    });
}

//jquery grid for property insurance
jqGrid('all',vendor_id);
function jqGrid(status,vendor_id) {
    var table = 'company_bills';
    var columns = [' ', 'Due Date', 'Reference Number', 'Original Amount('+amount_symbol+')','Discounted Amount('+amount_symbol+')','Terms','Amount Due After Discount('+amount_symbol+')','Status'];
    var select_column = [];
    var joins = [{table: 'company_bills', column: 'vendor_id', primary: 'id', on_table: 'users'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [{column:'vendor_id',value:vendor_id,condition:'='}];
    var extra_columns = [];
    var columns_options = [
        {name: ' ', index: 'id', align: "center", editoptions: {value: "True:False"}, editrules: {required: true},
            formatter: function (cellvalue, options, rowObject) {
                if(rowObject !== undefined) {
                    if(rowObject.Status == '1'){
                        return '';
                    } else {
                        return '<input type="checkbox" class="checkboxgrid" data_value="' + rowObject.Vendor_id + '" value=' + cellvalue + ' data_status="' + rowObject.Status + '">';
                    }
                }
            },
            formatoptions: {disabled: false}, editable: true, searchoptions: {sopt: conditions}, table: table,search:false},
        {name: 'Due Date', index: 'due_date', align: "center", searchoptions: {sopt: conditions}, table: table,change_type:'date'},
        {name: 'Reference Number', index: 'refrence_number', align: "center", searchoptions: {sopt: conditions}, table: table,formatter:referenceFormatter},
        {name: 'Original Amount', index: 'amount', align: "center", searchoptions: {sopt: conditions}, table: table, formatter:amountDiv},
        {name: 'Discounted Amount', index: 'discount_amount', align: "center", searchoptions: {sopt: conditions}, table: table,formatter:discountDiv},
        {name: 'Terms', index: 'term', align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Amount Due After Discount', index: 'amount', align: "center", searchoptions: {sopt: conditions}, table: table,formatter:afterDiscountDiv},
        {name: 'Status', index: 'status', align: "center", searchoptions: {sopt: conditions}, table: table,formatter:statusFormater},
    ];
    var ignore_array = [];
    jQuery("#paybills_table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'company_bills.updated_at',
        sortorder: 'desc',
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "Pay Bills",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 400, drag: true, resize: false} // search options
    );
}

/**
 *
 * @param cellValue
 * @param options
 * @param rowObject
 */
function discountDiv(cellValue, options, rowObject){
    if(rowObject !== undefined){
        var html = '<span class="span_discount cursor">'+amount_symbol+'0.00</span><input style="display: none;" class="amount number_only input_discount form-control" id="input_'+rowObject.id+'" name="discount[]" value="0.00">';
        return html;
    }
}

function referenceFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        return '<a target="_blank" style="text-decoration: underline;color:#551A8B !important;" href="/Vendor/EditBill?id='+rowObject.id+'">'+cellValue+'</a>';
    }
}

/**
 *
 * @param cellValue
 * @param options
 * @param rowObject
 */
function afterDiscountDiv(cellValue, options, rowObject){
    if(rowObject !== undefined){
        var amount = changeToFloat(cellValue.toString());
        return amount_symbol+amount;
    }
}

function creditCardNumber(cellValue, options, rowObject){
    return '';
}

function statusFormater(cellValue, options, rowObject){
    if(rowObject !== undefined){
        if(cellValue == '1'){
            return '<b>Paid</b>';
        }
        return '<b>Due</b>';
    }
}

/**
 *
 * @param cellValue
 * @param options
 * @param rowObject
 */
function amountDiv(cellValue, options, rowObject){
    if(rowObject !== undefined){
        var amount = changeToFloat(cellValue.toString());
        return '<span data_input="'+amount+'">'+amount_symbol+amount+'</span>';
    }
}

$(document).on('click','.span_discount',function(){
    $(this).hide();
    $(this).parent().find('input').css('display','block');
});

$(document).on('focusout','.input_discount',function(){
    var amount_value = $(this).val() != '' ? $(this).val() : '0.00';
    var value = amount_value.replace(/,/g, '');
    var amountValue = $(this).parent().prev().find('span').attr('data_input');
    amountValue = amountValue.replace(/,/g, '');
    if (parseFloat(value) > parseFloat(amountValue)) {
        bootbox.alert("Please Enter Valid Amount!");
        var span_amount = '0.00';
        $(this).val('0.00');
    } else {
        var realAmount = parseFloat(amountValue) - parseFloat(value);
        realAmount = changeToFloat(realAmount.toString());
        var span_amount = changeToFloat(value.toString());
        $(this).parent().next().next().text(amount_symbol + realAmount);
    }
});
workorder('all',vendor_id);
function workorder(status,vendor_id) {
    var id = vendor_id;
    var checkbox = '<input type="checkbox" id="select_all_complaint_checkbox">';
    var table = 'work_order';
    var columns = ['Work Order Number', 'Work Order Category', 'Vendor','Property','Created On','Caller/RequestedBy','Amount(AFN)','Priority ','Status'];
    var select_column = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where =[{column: 'vendor_id', value: id, condition: '='}];
    var extra_columns = ['work_order.deleted_at', 'work_order.updated_at'];
    var joins = [{table: 'work_order', column: 'work_order_cat', primary: 'id', on_table: 'company_workorder_category'},
        {table: 'work_order', column: 'property_id', primary: 'id', on_table: 'general_property'},
        {table: 'work_order', column: 'priority_id', primary: 'id', on_table: 'company_priority_type'},
        {table: 'work_order', column: 'status_id', primary: 'id', on_table: 'company_workorder_status'},
        {table: 'work_order', column: 'vendor_id', primary: 'id', on_table: 'users'}];
    var columns_options = [
        {name:'Work Order Number',index:'work_order_number',searchoptions: {sopt: conditions},table:table},
        {name:'Work Order Category',index:'category',searchoptions: {sopt: conditions},table:'company_workorder_category'},
        {name:'Vendor',index:'name',searchoptions: {sopt: conditions},table:'users'},
        {name:'Property',index:'property_name',searchoptions: {sopt: conditions},table:'general_property'},
        {name:'Created On',index:'created_at',searchoptions: {sopt: conditions},table:table},
        {name:'Caller/RequestedBy',index:'request_id',searchoptions: {sopt: conditions},table:table},
        {name:'Amount(AFN)',index:'estimated_cost',searchoptions: {sopt: conditions},table:table},
        {name:'Priority',index:'priority',searchoptions: {sopt: conditions},table:'company_priority_type'},
        {name:'Status',index:'work_order_status',searchoptions: {sopt: conditions},table:'company_workorder_status'}
    ];

    var ignore_array = [];
    jQuery("#workOrder-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Work Orders",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:200,drag:true,resize:false} // search options
    );
}

$(document).on("click", ".cancelActionBtn", function (e) {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            window.location.reload();
        }
    });
});

$(document).on('click','#searchBillTable',function(){
    searchFilters();
});

function searchFilters(){
    var grid = $("#paybills_table"),f = [];
    var termNet = $('#termNet').val();
    var search_date = formatDate($('#searchDate').val());
    f.push({field: "company_bills.due_date", op: "lt", data: search_date},{field: "company_bills.term", op: "eq", data: termNet},{field: "company_bills.status", op: "eq", data: '0'});
    grid[0].p.search = true;
    $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
    grid.trigger("reloadGrid",[{page:1,current:true}]);
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}


