fetchPaymentTabData();
function fetchPaymentTabData(){
    var vendor_id=vendor_unique_id;
    $.ajax({
        type: 'post',
        url: '/vendor-view-ajax',
        data: {
            class: "ViewVendor",
            action: "getVendorDetail",
            vendor_id:vendor_id
        },
        success: function (response) {

            var res = $.parseJSON(response);
            if (res.status == "success") {
                $('#tax_payer_name_payment').val(res.dataVendors.vendor_detail.tax_payer_name);
                $('#tax_payer_id_payment').val(res.dataVendors.vendor_detail.tax_payer_id);
                $('#consolidate_check_payment').val(res.dataVendor.vendor_additional_detail.consolidate_checks);
                $('.default_security_bank_account_payment').val(res.dataVendor.vendor_additional_detail.default_GL);
                $('#order_limit_payment').val(res.dataVendor.vendor_additional_detail.order_limit);
                $('.account_name_payment').val(res.dataVendor.vendor_additional_detail.account_name);
                $('#bank_account_number_payment').val(res.dataVendor.vendor_additional_detail.bank_account_number);
                $('#routing_number_payment').val(res.dataVendor.vendor_additional_detail.routing_number);
                $('#eligible_payment').val(res.dataVendor.vendor_additional_detail.eligible_for_1099);
                $('#comment_payment').val(res.dataVendor.vendor_additional_detail.comment);
                vendorPaymentFormData = $('#payment_setting_form').serializeArray();
            } else if (res.status == "error") {
                toastr.error(res.message);
            } else {
                toastr.error(res.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

$("#payment_setting_form").on('submit',function(event){
    event.preventDefault();
    var form = $("#payment_setting_form").serializeArray();
    var vendor_id=vendor_unique_id;
    if($("#payment_setting_form").valid()){
        $.ajax({
            type: 'post',
            url: '/vendorEdit/payment',
            data: {
                class: "paymentVendor",
                action: "editPaymentVendor",
                form: form,
                vendor_id:vendor_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    localStorage.setItem("Message",response.message);
                    localStorage.setItem("activeTab",'#paymentTab');
                    window.location.href = window.location.origin + '/Vendor/ViewVendor?id='+vendor_id;
                } else if (response.message == 'Validation Errors!' && response.code == 400) {
                    toastr.warning('Email already exits!');
                } else if (response.code == 500) {
                    toastr.warning(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
});

$("#sendEmailTemplate").validate({
    rules: {
        sendEmail: {
            required:true
        }
    },
    submitHandler: function (e) {
        var tenant_id = vendor_unique_id;
        var form = $('#sendEmailTemplate')[0];
        var formData = new FormData(form);
        formData.append('action','sendEmailTemplate');
        formData.append('class','VendorPortal');
        formData.append('tenant_id',tenant_id);
        formData.append('temp_key',"newVendorWelcome_Key");
        $.ajax({
            url:'/Vendor-portal-ajax',
            type: 'POST',
            data: formData,
            success: function (data) {
                var info = JSON.parse(data);
                if(info.status=='success')
                {
                    toastr.success(info.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});

jQuery(document).ready(function($){
    if(localStorage.getItem("activeTab") !== undefined || localStorage.getItem("activeTab") !== null)
    {
        var message = localStorage.getItem("activeTab");
        $(message).find('a').click();
        localStorage.removeItem('activeTab');
    }
    if(localStorage.getItem("scrollDown") !== undefined || localStorage.getItem("scrollDown") !== null) {
        var message = localStorage.getItem("scrollDown");
        if(message !== null) {
            $('html, body').animate({
                'scrollTop': $(message).position().top + 200
            });
        }
        localStorage.removeItem('scrollDown');
    }
});

$(".acquireDateclass").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());

$(".expirationDateclass").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());