$(document).on('click','#add_libraray_file',function(){
    $('#file_library').val('');
    $('#file_library').trigger('click');
});
var file_library = [];
var imgArray = [];
$(document).on('change','#file_library',function(){
   // file_library = [];
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        if(size > 4097) {
            toastr.warning('Please select documents less than 4 mb!');
        } else {
            size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
            if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                  if($.inArray(value['name'], imgArray) === -1)
                {
                    file_library.push(value);
                }
                var src = '';
                var reader = new FileReader();
              //  $('#file_library_uploads').html('');
                reader.onload = function (e) {
                    if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                        src = upload_url + 'company/images/excel.png';
                    } else if (type == 'application/pdf') {
                        src = upload_url + 'company/images/pdf.png';
                    } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                        src = upload_url + 'company/images/word_doc_icon.jpg';
                    } else if (type == 'text/plain') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else if (type == 'text/xml') {
                        src = upload_url + 'company/images/notepad.jpg';
                    } else {
                        src = e.target.result;
                    }
                   if($.inArray(value['name'], imgArray) === -1)
                    {
                        
                    $("#file_library_uploads").append(
                        '<div class="row" style="margin:20px">' +
                        '<div class="col-sm-12 img-upload-library-div">' +
                        '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=80 height=88 src=' + src + '></div>' +
                        '<div style="margin-top: 36px;" class="col-sm-3 center_img show-library-list-imgs-name ' + key + '">' + value['name'] + '</div>' +
                        '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                        '<div  style="    margin-top: 36px;" class="col-sm-3 center_img show-library-list-imgs-size' + key + '">' + size + '</div>' +
                        '<div class="col-sm-3 center_img"><span id=' + key + ' class="delete_pro_img cursor"><button class="orange-btn">Delete</button></span></div></div></div>');
                        imgArray.push(value['name']);
                    } else {
                        toastr.warning('File already exists!');
                    }
                };
                reader.readAsDataURL(value);
            } else {
                toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
            }
        }
    });
});

$('#saveLibraryFiles').on('click',function(){
    var length = $('#file_library_uploads > div').length;
    if(length > 0) {
        var data = convertSerializeDatatoArray();
        var uploadform = new FormData();
        var vendor_edit_id = $(".vendor_edit_id").val();
        uploadform.append('class', 'fileLibrary');
        uploadform.append('action', 'file_library');
        uploadform.append('vendor_edit_id', vendor_edit_id);
        var count = file_library.length;
        $.each(file_library, function (key, value) {
             if(compareArray(value,data) == 'true'){
                 uploadform.append(key, value);
             }
            if(key+1 === count){
                saveLibraryFiles(uploadform);
            }
        });
    } else {

    }
});

$(document).on('click','.delete_pro_img',function(){
    toastr.success('The record deleted successfully.');
    imgArray.pop($(this).parent().parent().find('.show-library-list-imgs-name').text());
    $(this).parent().parent().parent('.row').remove();
});

$(document).on('click','#remove_library_file',function(){
    bootbox.confirm("Do you want to remove all files?", function (result) {
        if (result == true) {
            toastr.success('The record deleted successfully.');
            $('#file_library_uploads').html('');
            $('#file_library').val('');
            file_library = [];
            imgArray = [];
        }
    });

});

function saveLibraryFiles(uploadform){
    $.ajax({
        type: 'post',
        url: '/Vendor/file-library',
        data:uploadform,
        processData: false,
        contentType: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
                $('#file_library_uploads').html('');
                $('#fileLibraryTable').trigger('reloadGrid');
                toastr.success('Files uploaded successfully.');
            } else if(response.code == 500){
                toastr.warning(response.message);
            } else {
                toastr.success('Error while uploading files.');
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function convertSerializeDatatoArray(){
    var newData = [];
    $(".fileLibraryInput").each(function( index ) {
        var name = $(this).val();
        var size = $(this).attr('data_id');
        newData.push({'name':name,'size':size});
    });
    return newData;
}

function compareArray(data,compare){
    for(var i =0;i < compare.length;i++){
        if(compare[i].name == data['name'] && compare[i].size == data['size']){
            return 'true';
        }
    }
    return 'false';
}

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}

/**
 * jqGrid Intialization function
 * @param status
 */




/**
 * function to format document
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function imageFormatter(cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        var path = upload_url+'company/'+rowObject.Location;
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=30 height=30 src="' + src + '">';
    }
}

$(document).on('click',".open_file_location",function(){
    var location =    $(this).attr("data-location");
    window.open(location, '_blank');
});

$(document).on('change', '#fileLibraryTable .select_options', function() {
    console.log('asdgsdghsdh');
    var opt = $(this).val();
    var id = $(this).attr('data-id');
    console.log('id_delete',id);
    var row_num = $(this).parent().parent().index() ;
    var url = $(this).attr('data-path');
    var src = $(this).attr('data-src');
    if (opt == 'Send' || opt == 'SEND') {
        console.log('sdgsdfhgsd');
        file_upload_email('users','email',id,1,url,src);
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/Vendor/file-library',
                        data: {class: 'fileLibrary', action: "deleteFile", id: id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                        }
                    });
                }
                $('#fileLibraryTable').trigger('reloadGrid');
            }
        });
    }
});


/**
 * jqGrid Intialization function
 * @param status
 */
jqGridFileLibrary('all',true);
function jqGridFileLibrary(status,action) {
    var vendor_id = vendor_unique_id;
    console.log(vendor_id);
    var table = 'tenant_file_library';
    var columns = ['File Name','Preview','Location','Action'];
    var select_column = ['Email','Delete'];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [{column:'user_id',value:vendor_id,condition:'='}];
    var columns_options = [
        {name:'Name',index:'name',align:"left",searchoptions: {sopt: conditions},table:table},
        {name:'Preview',index:'extension',align:"left",searchoptions: {sopt: conditions},search:false,table:table,formatter:imageFormatter},
        {name:'Location',index:'path',align:"center",searchoptions: {sopt: conditions},search:false,table:table,hidden:true},
        {name:'Action',index:'select',hidden:action,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: fileFormatter, edittype: 'select',search:false,table:table,title:false}
    ];
    var ignore_array = [];
    jQuery("#fileLibraryTable").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'updated_at',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where
        },
        viewrecords: true,
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Vendor Files",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        },loadComplete : function () {
            $('.ui-jqgrid-pg-left').css('width','unset');
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false} // search options
    );
}


/**
 * function to format document
 * @param cellvalue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function imageFormatter(cellvalue, options, rowObject){

    if(rowObject !== undefined) {
        var path = upload_url+'company/'+rowObject.Location;
        var src = '';
        if (rowObject.Preview == 'xlsx') {
            src = upload_url + 'company/images/excel.png';
        } else if (rowObject.Preview == 'pdf') {
            src = upload_url + 'company/images/pdf.png';
        } else if (rowObject.Preview == 'docx') {
            src = upload_url + 'company/images/word_doc_icon.jpg';
        }else if (rowObject.Preview == 'txt') {
            src = upload_url + 'company/images/notepad.jpg';
        }
        return '<img class="img-upload-tab open_file_location" data-location="'+path+'" width=30 height=30 src="' + src + '">';
    }
}
$(document).on("click",".edit_vendorlink",function(){
    console.log('dasfgdsghsd');
   $(".add_libraray_Div").show();
    if ($("#fileLibraryTable")[0].grid) {
        $('#fileLibraryTable').jqGrid('GridUnload');
    }
    jqGridFileLibrary('All', false);
});

function fileFormatter(cellValue, options, rowObject)
{
    if(rowObject !== undefined) {
        var file_type =  rowObject.View;
        var location = rowObject.Location;
        var path = upload_url+'company/'+location;
        var imageData = '';
        var src = '';
        if(file_type == '1'){
            imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
        } else {
            if (rowObject.Preview == 'xlsx') {
                src = upload_url + 'company/images/excel.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.Preview == 'pdf') {
                src = upload_url + 'company/images/pdf.png';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            } else if (rowObject.Preview == 'docx' || rowObject.Preview == 'doc') {
                src = upload_url + 'company/images/word_doc_icon.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }else if (rowObject.Preview == 'txt') {
                src = upload_url + 'company/images/notepad.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
            }
        }
        var html = "<select editable='1' class='form-control file_select_options select_options' data-id='"+rowObject.id+"' data-path = '"+path+"' data-src= '"+src+"'>"
        html +=  "<option value=''>Select</option>";
        html +=  "<option value='Delete'>Delete</option>";
        html +=  "<option value='Send'>Email</option>";
        html +="</select>";
        return html;
        return imageData;
    }
}