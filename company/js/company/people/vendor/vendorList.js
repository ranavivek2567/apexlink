/* new popup js starts here */
$("#financialCardInfo").validate({
    rules: {
        cfirst_name: {
            required:true
        },
        clast_name: {
            required:true
        },
        ccard_number: {
            required:true,
            number:true
        },
        cexpiry_year: {
            required:true
        },
        cexpiry_month: {
            required:true
        },
        ccvv: {
            required:true,
            number:true,
        },

    },
    submitHandler: function (e) {
        $('#loadingmessage').show();
        var form = $('#financialCardInfo')[0];
        var formData = new FormData(form);
        var vendor_id=$("#stripe_vendor_id").val();
        formData.append('action','addVendorCardDetails');
        formData.append('class','Stripe');
        formData.append('vendor_id',vendor_id);
        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                $('#loadingmessage').hide();
                if(response.status== 200)
                {
                    $('#loadingmessage').hide();
                    localStorage.setItem("Message","Card has been added Successfully");
                    localStorage.setItem('rowcolor_vendor', 'rowColor');


                }
                else
                {
                    $('#loadingmessage').hide();
                    toastr.error(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});

$(document).on('click','#billing-subscription .paymentDiv',function(){
    $(".basic-payment-detail").hide();
    $(".apx-adformbox-content").show();
    $(this).addClass("active");
    $(".basicDiv").removeClass("active");
});

$(document).on('click','#billing-subscription .basicDiv',function(){
    $("#fmcc,#fcmcc").val(6513);
    setTimeout(function(){
        var account_type= $("#business_type").val();
        var verification_status = $("#account_verification").val();
        if(account_type == "company"){
            $(".basic-payment-detail").show();
        } else if(account_type == "individual"){
            $(".basic-user-payment-detail").show();
            $(".fmcc").show();
            $(".furl").show();
        } else {
            // $(".fmcc").hide();
            // $(".furl").hide();
            $('#billing-subscription').modal('hide');
            $('#financial-infotype').modal('show');
        }

        // if(verification_status == 'verified'){
        //     $(".basic-payment-detail").hide();
        //     $(".basic-user-payment-detail").hide();
        // }

        $(".apx-adformbox-content").hide();
        $(".basicDiv").addClass("active");
        $(".paymentDiv").removeClass("active");
    }, 500);
});


$.ajax({
    type: 'post',
    url: '/payment-ajax',
    data: {
        class: 'PaymentAjax',
        action: "getMccTypes"
    },
    success: function (response) {
        var data = $.parseJSON(response);
        if (data.status == "success") {

            if (data.data.mcc_types.length > 0) {
                var mccOption = "<option value='0'>Select</option>";
                $.each(data.data.mcc_types, function (key, value) {
                    mccOption += "<option value='" + value.code + "' data-id='" + value.code + "'>" + value.name + " - " + value.code + " " + "</option>";
                });
                $('#fmcc,#fcmcc').html(mccOption);
            }

        } else if (data.status == "error") {
            toastr.error(data.message);
        } else {
            toastr.error(data.message);
        }
    },
    error: function (data) {
        var errors = $.parseJSON(data.responseText);
        $.each(errors, function (key, value) {
            $('#' + key + '_err').text(value);
        });
    }
});

jQuery('.phone_format').mask('000-000-0000', {reverse: true});
var vendor_id=$("#stripe_vendor_id").val();

    $(document).on("change",".setType",function(){
        var value = $(this).val()
        if(value=='1')
        {
            $(".accounts").hide();
            $(".cards").show();
        }
        else
        {
            $(".accounts").show();
            $(".cards").hide();
        }

    });


function cardLists(id)
{
    var tenant_id = id;
    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getTenantAllCards',
            "class": 'Stripe',
            "tenant_id":tenant_id
        },
        success: function (response) {


            $(".cardDetails").html(response);


        }
    });

}

function bankLists(id)
{
    var tenant_id = id;
    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getTenantAllBanks',
            "class": 'Stripe',
            "tenant_id":tenant_id
        },
        success: function (response) {


            $(".accountDetails").html(response);


        }
    });

}


var min = new Date().getFullYear(),
    max = min - 49,
    select = document.getElementById('fyear');
for (var i = max; i<=min; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


var min = 1,
    max =31,
    select = document.getElementById('fday');

for (var i = min; i<=max; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var month = 0;
// var i = 1;
for (; month < monthNames.length; month++) {

    var new_month = month+1;
    $('#fmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
}





var min = new Date().getFullYear(),
    max = min - 49,
    select = document.getElementById('fpyear');
for (var i = max; i<=min; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


var min = 1,
    max =31,
    select = document.getElementById('fpday');

for (var i = min; i<=max; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var month = 0;
// var i = 1;
for (; month < monthNames.length; month++) {

    var new_month = month+1;
    $('#fpmonth').append('<option value='+new_month+'>' + monthNames[month] + '</option>');
}

/**
 * If the letter is not digit in phone number then don't type anything.
 */
$("#phone_number,#fphone_number,#fssn,#fpphone_number,#fpssn").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});


var min = new Date().getFullYear(),
    max = min + 49,
    select = document.getElementById('cexpiry_year');

for (var i = min; i<=max; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


var min = 1,
    max =12,
    select = document.getElementById('cexpiry_month');

for (var i = min; i<=max; i++){
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}


$(document).on('click','#savefinancial',function (e) {
    e.preventDefault();
    var financial_data = $( "#financialInfo" ).serializeArray();
    var company_id = $("#company_user_id").val();
    $.ajax
    ({
        type: 'post',
        url: '/payment-ajax',
        data: {
            class: "PaymentAjax",
            action: "validateFinancialData",
            data:financial_data,
            company_id:company_id
        },
        success: function (response) {
            var response = $.parseJSON(response);
            if(response.code == 400)
            {
                $.each(response.data, function (key) {
                    $('.' + key).text('* This field is required');
                });
            }else{
                localStorage.setItem('financial_data',JSON.stringify(financial_data));


                toastr.clear();
                updateUserAccount(financial_data);

            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('.' + key + 'Err').text(value);
            });
        }
    });




});





$(document).on('click','#savecompanyfinancial',function (e) {
    e.preventDefault();
    var financial_data = $( "#financialCompanyInfo" ).serializeArray();
    console.log(financial_data);


    $.ajax
    ({
        type: 'post',
        url: '/payment-ajax',
        data: {
            class: "paymentAjax",
            action: "validateCompanyFinancialData",
            data:financial_data
        },
        success: function (response) {
            var response = $.parseJSON(response);
            if(response.code == 400)
            {
                $.each(response.data, function (key) {
                    $('.' + key).text('* This field is required');
                });
            }else{
                localStorage.setItem('financial_data',JSON.stringify(financial_data));


                toastr.clear();
                updateCompanyAccount(financial_data);

            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('.' + key + 'Err').text(value);
            });
        }
    });




});

function updateUserAccount(financial_data){
    $('#loadingmessage').show();
    var company_id = $("#company_user_id").val();
    $.ajax({
        type: 'post',
        url: '/payment-ajax',
        data: {class: 'PaymentAjax', action: "updateUserConnectedAccount",financial_data:financial_data,"company_id":company_id },
        success : function(response){
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                $('#loadingmessage').hide();
                localStorage.removeItem("financial_data");
                $('#financial-info').modal('hide');
                $('#billing-subscription').modal('hide');
                toastr.success("Account Updated Successfully.");
                setTimeout(function(){
                    window.location.reload();
                }, 3000);


            } else if(response.status == 'error' && response.code == 400){
                if(response.message == 'online payment error')
                {
                    toastr.error('Please fill all online payment required fields.');
                }else{
                    console.log()
                }
                $('#loadingmessage').hide();
                $('.error').html('');
            }
            else if(response.status == 'failed' && response.code == 400){
                toastr.error(response.message);
            }
            $('#loadingmessage').hide();
            $('.error').html('');
        },
        error: function (response) {
            // console.log(response);
        }
    });
}


/* new popup js ends here */

$(document).on('mouseover','.tooltipgridclass',function(){
    $(this).closest('td').css("overflow", "unset");
});
$(document).on('mouseout','.tooltipgridclass',function(){
    $(this).closest('td').css("overflow", "hidden");
});

$(document).ready(function () {

    jQuery('#cphoneNumber').mask('000-000-0000', {reverse: true});
    $('#vendorTable .tooltip-cell').tooltip({
        container:'body',
        placement: 'bottom',
        title: 'Click to edit',
        trigger: 'hover'
    });

    //jqGrid status
    $('#jqGridStatus').on('change', function () {
        var selected = this.value;
        var deleted_at = true;
        $('#vendorTable').jqGrid('GridUnload');
        // if (selected == 4)
        //     deleted_at = false;
        jqGrid(selected);
    });

    fetchAllVendorType();

    function fetchAllVendorType() {
        $.ajax({
            type: 'post',
            url: '/vendor-add-ajax',
            data: {
                class: 'addVendor',
                action: 'fetchAllVendorType'},
            success: function (response) {
                var res = JSON.parse(response);
                $('#vendor_type').html(res.data);
                $('#vendor_type').prop('selectedIndex',0);
                $("#vendor_type option:first").attr('value','all');
            },
        });
    }

    /**
     * Function for view company on clicking row
     */
    $(document).on('click', '#vendorTable tr td:not(td:last-child)', function () {
        var idgg = $(this).closest('tr').find('td:last-child').find('select').attr('vendor_id');
        var vendor_status = $(this).closest('tr').find('td:last-child').find('select').attr('vendor_status');
        if ($(this).index() == 0 || $(this).index() == 9 || vendor_status == '3' || vendor_status == '2') {
            return false;
        } else {
            window.location.href = '/Vendor/ViewVendor?id='+idgg;
        }
    });

    $(document).on('change','.common_ddl',function(){
        searchFilters();
    });


    if(localStorage.getItem('ElasticSearch')){
        var elasticSearchData = localStorage.getItem('ElasticSearch');
        setTimeout(function(){
            var grid = $("#vendorTable"),f = [];
            f.push({field: "users.id", op: "eq", data: elasticSearchData,int:'true'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('ElasticSearch');
        },1500);
    }

    $(document).on('change', '#vendorTable .select_options', function () {
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var vendor_id = $(this).attr('vendor_id');
        cardLists(vendor_id);
        bankLists(vendor_id);
        var rating = $(this).attr('rating');
        var vendor_name = $(this).attr('vendor_name');
        var vendor_email=$(this).attr('vendor_email');
        localStorage.setItem('ven_id',id);
        switch (opt) {
            case "Edit":
                window.location.href = "/Vendor/EditVendor?id="+vendor_id;
                break;
            case "Pay vendor":
                localStorage.setItem("activeTab",'#payablesTab');
                localStorage.setItem("scrollDown",'#people-vendor-two');
                window.location.href ='/Vendor/ViewVendor?id='+id;
                break;
            case "Work order":
                localStorage.setItem('redirection_module', 'vendor_module');
                window.location.href = base_url + '/WorkOrder/AddWorkOrder?id='+vendor_id;
                break;
            case "New Bill":
                localStorage.setItem('vendor_id', vendor_id);
                localStorage.setItem('ven_id', vendor_id);
                window.location.href = base_url + '/Vendor/NewBill';
                break;
            case "Email":
                var urlemail='/Vendor/Vendor';
                localStorage.setItem('table_green_tableid', '#vendorTable');
                localStorage.setItem('predefined_mail',vendor_email);
                localStorage.setItem('table_green_id',vendor_id);
                localStorage.setItem('table_green_url',urlemail);
                window.location.href = base_url + '/Communication/ComposeEmail';
                break;
            case "Email History":
                window.location.href = "/Communication/SentEmails";
                break;
            case "Text":
                var urlemail='/Vendor/Vendor';
                localStorage.setItem('predefined_text',vendor_email);
                localStorage.setItem('table_green_tableid', '#vendorTable');
                localStorage.setItem('table_green_id',vendor_id);
                localStorage.setItem('table_green_url',urlemail);

                window.location.href = base_url + '/Communication/AddTextMessage';
                break;
            case "Text History":
                window.location.href = "/Communication/TextMessage";
                break;
            case "File History":
                localStorage.setItem("activeTab",'#libraryTab');
                localStorage.setItem("scrollDown",'#people-vendor-six');
                window.location.href ='/Vendor/ViewVendor?id='+id;
                break;
            case "Notes History":
                localStorage.setItem("activeTab",'#notesTab');
                localStorage.setItem("scrollDown",'#people-vendor-five');
                window.location.href ='/Vendor/ViewVendor?id='+id;
                break;
            case "Run Background Check":
                $('#backGroundCheckPop').modal('show');
                break;
            case "Send Password Activation Mail":
                var email = $(this).attr('vendor_email');
                var user_type = '3';
                $.ajax
                ({
                    type: 'post',
                    url: '/OwnerPortalLogin-Ajax',
                    data: {
                        class: "portalLoginAjax",
                        action: "resetPasswordEmailByAdmin",
                        email: email,
                        user_type : user_type
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if(data.code == 200 && data.status == 'success') {
                            toastr.success('Mail sent successfully.');
                        } else {
                            toastr.warning(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            // alert(key+value);
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                break;
            case "Flag Bank":
                localStorage.setItem("activeTab",'#flagTab');
                localStorage.setItem("scrollDown",'#people-vendor-flagbank');
                window.location.href ='/Vendor/ViewVendor?id='+vendor_id;
                break;
            case "Add InTouch":
                window.location.href = "/Communication/NewInTouch?tid="+vendor_id+"&category=Person";
                break;
            case "InTouchHistory":
                var propertyname=$('#'+id).find('td:first').text();
                localStorage.setItem("propertyname",propertyname);
                window.location.href = "/Communication/InTouch";
                break;
            case "Archive Vendor":
                bootbox.confirm({
                    message: "Do you want to Archive the current record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/vendor-add-ajax',
                                data: {class: 'addVendor', action: "archiveVendor", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    searchFilters();
                                }
                            });
                        }
                    }
                });
                break;
            case "Resign Vendor":
                bootbox.confirm({
                    message: "Do you want to Resign the current record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $('#ReasonOwner').prop('selectedIndex',0);
                            $('#reasonNotes').text('');
                            $('#resignReasonId').val(id);
                            $('#resignReason').modal('show');
                        }
                    }
                });
                break;
            case "Vendor Portal":
                $.ajax({
                    type: 'post',
                    url: '/Vendor-portal-ajax',
                    data: {class: 'VendorPortal', action: 'getVendorPortal','vendor_id':vendor_id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            window.open(base_url +response.portal_url, '_blank');
                        }else {
                            toastr.warning('Not redirecting due to technical issue.');
                        }
                    }
                });
                break;
            case "Vendor Rating":
                $('#starVendor_id').val(vendor_id);
                $('#Ratingname').text(vendor_name);
                var html = vendorStarRating(rating);
                $('.vendorRatingDiv').html(html);
                $('#starDiv').modal('show');
               // $('#vendorTable').trigger( 'reloadGrid' );
                break;
            case "Print Envelope":
                $.ajax({
                    type: 'post',
                    url: '/employeeListAjax',
                    data: {class: 'EmployeeListAjax', action: 'getEmployeeData','employee_id':vendor_id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            $("#PrintEmployeeEnvelope").modal('show');
                            $("#user_company_name").text(response.data.data.company_name);
                            $("#user_address1").text(response.data.data.address1);
                            $("#user_address2").text(response.data.data.address2);
                            $("#user_address3").text(response.data.data.address3);
                            $("#user_address4").text(response.data.data.address4);
                            $("#employee_first_name").text(response.employee.data.first_name);
                            $("#employee_last_name").text(response.employee.data.last_name);
                            $("#employee_address1").text(response.employee.data.address1);
                            $("#employee_address2").text(response.employee.data.address2);
                            $("#employee_address3").text(response.employee.data.address3);
                            $("#employee_address4").text(response.employee.data.address4);
                            $(".employee_city").text(response.employee.data.city);
                            $(".employee_state").text(response.employee.data.state);
                            $(".employee_postal_code").text(response.employee.data.zipcode);
                        }else {
                            toastr.warning('Record not updated due to technical issue.');
                        }
                    }
                });
                break;
            case "Activate This Vendor":
                bootbox.confirm({
                    message: "Do you want to Activate the current record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/vendor-add-ajax',
                                data: {class: 'addVendor', action: "activateVendor", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    searchFilters();
                                }
                            });
                        }
                    }
                });
                break;
            case "Reactivate Vendor":
                bootbox.confirm({
                    message: "Are you sure you want to reactivate vendor ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/vendor-add-ajax',
                                data: {class: 'addVendor', action: "activateVendor", id: id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    searchFilters();
                                }
                            });
                        }
                    }
                });
                break;
            case "Delete Vendor":
                bootbox.confirm({
                    message: "Do you want to delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/vendor-add-ajax',
                                data: {class: 'addVendor', action: "deleteVendor", id: id,vendor_id: vendor_id},
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                    searchFilters();
                                }
                            });
                        }
                    }
                });
                break;
            case "Online Payment":
                $(".basic-user-payment-detail").hide();
                fetchCustomerCardDetail(vendor_id);
                $("#billing-subscription").modal('show');
                $("#stripe_vendor_id").val(vendor_id);
                $("#company_user_id").val(vendor_id);
                checkUserAccountVerification(vendor_id);
                $(".basic-payment-detail").hide();
                $(".apx-adformbox-content").show();
                $("#paymentDiv").addClass("active");
                $(".basicDiv").removeClass("active");

                //
                 getTenantInfo(vendor_id);
                //  $("#financial-info").modal("show");
                //  $("#addVendorCardDetailsForm").trigger('reset');
                //  $('#payment_method').val('1');
                //  step2Detail(vendor_id);

                break;
            default:
                $('#vendorTable .select_options').prop('selectedIndex',0);
        }

        $('#vendorTable .select_options').prop('selectedIndex',0);
    });


    function checkUserAccountVerification(vendor_id){
        $.ajax({
            type: 'post',
            url: '/payment-ajax',
            data: {
                class: 'PaymentAjax',
                action: "getUserAccountVerification",
                user_id : vendor_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success'){
                    if(response.account_status == 'Verified'){
                        var message = 'Verified';

                        $("#business_type").val(response.account_data.business_type)
                        setTimeout(function() {
                            $("#account_verification").val('verified');


                        },2000);
                        // $('.account_verification_status').addClass("verified")
                        // $(".basic-payment-detail").hide();
                        $(".basic-user-payment-detail").hide();
                        // $("#basicDiv").css("display","none");
                        // $(".basicDiv_li").css("display","none");
                        // $(".basic_div_span").css("display","none");
                        // $('.payment_li').addClass('after-hide');
                        // $(".right_tick").css("display","block");
                        setTimeout(function() {
                            if(response.user_account_detail.data.business_type == 'individual') {
                                $("#financialInfo :input").prop("disabled", true);
                                $("#savefinancial").prop("disabled", true);
                                $("#account_verification").val('verified');
                                autofillIndividualData(response);

                            }
                            if(response.user_account_detail.data.business_type == 'company'){
                                $("#financialCompanyInfo :input").prop("disabled", true);
                                $("#financialCompanyInfo").prop("disabled", true);
                                $("#account_verification").val('verified');
                                autofillCompanyData(response);
                            }
                        },2000);


                    }else if(response.account_status == 'not_exists'){
                        $("#business_type").val("");
                        setTimeout(function() {
                            $("#account_verification").val('not_exists');
                        },2000);

                    }else{
                        $("#business_type").val(response.account_data.business_type)
                        setTimeout(function() {
                            if(response.user_account_detail.data.business_type == 'individual') {
                                $("#account_verification").val('notverified');
                                autofillIndividualData(response);
                            }
                            if(response.user_account_detail.data.business_type == 'company'){
                                $("#account_verification").val('notverified');
                                autofillCompanyData(response);
                            }
                        },2000);

                        $('.account_verification_status').addClass("not_verified");
                        var message = 'Not Verified .  To recieve payments submit account details';
                        $("#account_verification").val('');
                        $('.unverified-icon').css("display","block");
                    }
                    $('.account_verification_status').html(message);


                } else {
                    toastr.error(response.message);
                }
            },
            error: function (data) {

            }
        });
    }




    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('all');
    function jqGrid(status) {
        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }
        var table = 'vendor_additional_detail';
        var columns = ['Vendor Name','VendorName','Phone', 'Open Work Order', 'YTD Payment ('+default_currency_symbol+')', 'Type', 'Rate ('+default_currency_symbol+') /hr', 'Rating', 'Email','Phone_Note','Portal_Status', 'Status', 'Action'];
        var select_column = ['Edit', 'Pay vendor', 'Work orders', 'New Bill', 'Email', 'Email History', 'Text', 'Text History', 'Add InTouch', 'InTouchHistory','File History','Notes History','Flag Bank','Run Background Check','Send Password Activation Mail', 'Archive Vendor', 'Resign Vendor','Vendor Rating', 'Print Envelope','Delete Vendor','Online Payment'];
        var joins = [{table: 'vendor_additional_detail', column: 'vendor_id', primary: 'id', on_table: 'users'},{table: 'vendor_additional_detail', column: 'vendor_type_id', primary: 'id', on_table: 'company_vendor_type'}];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['vendor_additional_detail.vendor_id'];
        var extra_where = [{column: 'user_type', value: '3', condition: '=',table:'users'},{column: 'deleted_at', value: '', condition: 'IS NULL',table:'users'}];
        var columns_options = [
            {name: 'Vendor Name', title:true, index: 'name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',name_id:'vendor_id',formatter:vendorName},
            {name: 'VendorName', hidden:true,title:true, index: 'name', width: 90, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',name_id:'vendor_id',formatter:vendorName},
            {name: 'Phone', index: 'vendor_id',title:false, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor',change_type:'line_multiple',index2:'phone_number',index3:'other_work_phone_extension',join:{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'vendor_id',formatter:addToolTip,cellattr:cellAttri}, /**cellattr:cellAttrdata**/
            {name: 'Open Work Order', index: 'phone_type', width: 80, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',formatter:workOrderFormatter},
            {name: 'YTD Payment('+default_currency_symbol+')', index: 'phone_type', width: 80, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer',formatter:paymentFormatter},
            {name: 'Type', index: 'vendor_type', width: 80, align: "left", searchoptions: {sopt: conditions}, table: 'company_vendor_type', classes: 'pointer'},
            {name: 'Rate('+default_currency_symbol+') /hr', index: 'vendor_rate', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:rateFormatter},
            {name: 'Rating', index: 'rating', width: 100, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:starFormatter},
            {name: 'Email', index: 'email', width: 80, align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
            {name: 'Phone_Note', index: 'phone_number_note', hidden:true, width: 80, searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
            {name: 'Portal_Status', index: 'portal_status', width: 80, hidden:true,align: "left", searchoptions: {sopt: conditions}, table: 'users', classes: 'pointer'},
            {name: 'Status', index: 'status', width: 80, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',attr:[{name:'vendor_id',value:'vendor_id'},{name:'rating',value:'rating'},{name:'flag',value:'vendor',optional_id:'vendor_id'}],formatter: propertyStatus},
            {name: 'Action', index: 'select', title: false, width: 80, align: "right",formatter: 'select', sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, edittype: 'select', search: false, table: table,formatter:actionFmatter}
        ];
        var ignore_array = [];
        jQuery("#vendorTable").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            width: '100%',
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                extra_where:extra_where,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Vendors",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
        );
    }
    function cellAttri(cellValue, options, rowObject) {
        if(rowObject !== undefined){

            if(rowObject.Phone_Note != ''){
                return 'title = " "';
            }
        }
    }

    function starFormatter(cellValue, options, rowObject){
        // if (rowObject !== undefined) {
        //     $('#starVendorName').val(rowObject.VendorName);
        //     $("#Ratingname").html('');
        //    $("#Ratingname").html(rowObject.VendorName);
        // }
        var html = '';
        switch (cellValue) {
            case '1':
                html = '<form><fieldset class="listing_star starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label class="faded-star" title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label class="faded-star" title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
                break;
            case '2':
                html = '<form><fieldset class="listing_star starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label class="faded-star" title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
                break;
            case '3':
                html = '<form><fieldset class="listing_star starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
                break;
            case '4':
                html = '<form><fieldset class="listing_star starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> </form>';
                break;
            case '5':
                html = '<form><fieldset class="listing_star starability-growRotate"><input type="radio" name="rating" value="1" /><label  title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label title="Amazing">5 star</label> </fieldset> </form>';
                break;
            default:
                html = '<form><fieldset class="listing_star starability-growRotate"><input type="radio" name="rating" value="1" /><label class="faded-star" title="Terrible">1 stars</label> <input type="radio" name="rating" value="2" /> <label class="faded-star" title="Not good">2 stars</label> <input type="radio" name="rating" value="3" /> <label class="faded-star" title="Average">3 stars</label> <input type="radio" name="rating" value="4" /> <label class="faded-star" title="Very good">4 stars</label> <input type="radio" name="rating" value="5" /> <label class="faded-star" title="Amazing">5 star</label> </fieldset> <span style="padding:30px;color:grey;font-family:Arial;font-size: 13px;">Not Yet Rated</span></form>';
        }
        return html;
    }

    /**
     * jqGrid function to format tooltip
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function addToolTip(cellValue, options, rowObject) {
        if(rowObject !== undefined){
            if(rowObject.Phone_Note == ''){
                return cellValue;
            } else {
                return '<div class="tooltipgridclass">'+cellValue+'<span class="tooltiptextbotclass">'+rowObject.Phone_Note+'</span></div>';
            }
        }
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 1)
            return "Active";
        else if (cellValue == '0')
            return "InActive";
        else
            return '';
    }


    function flagFormatter(cellValue, options, rowObject){
        if(rowObject !== undefined){
            var flagValue = $(rowObject.Action).attr('flag');
            var flag = '';
            if(flagValue == 'yes'){

            } else {

            }
        }
    }

    function workOrderFormatter(cellValue, options, rowObject){
        return '0';
    }

    function paymentFormatter(cellValue, options, rowObject){
        return default_currency_symbol+'0.00';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Yes";
        else if (cellValue == '0')
            return "No";
        else
            return '';
    }

    function actionFmatter (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var vendor_id = $(cellvalue).attr('vendor_id');
            var data_id = $(cellvalue).attr('data_id');
            var rating = $(cellvalue).attr('rating');
            var flag = $(cellvalue).attr('flag');
            var vendor_name = rowObject.VendorName;
            var portal_satus = rowObject.Portal_Status;
            if(portal_satus == '1') {
                var select = ['Edit', 'Pay vendor', 'Work order', 'New Bill', 'Email', 'Email History', 'Text', 'Text History', 'Notes History','Add InTouch', 'InTouchHistory', 'File History', 'Flag Bank', 'Run Background Check', 'Send Password Activation Mail', 'Archive Vendor', 'Resign Vendor', 'Vendor Portal', 'Vendor Rating', 'Print Envelope', 'Delete Vendor', 'Online Payment'];
            } else {
                var select = ['Edit', 'Pay vendor', 'Work order', 'New Bill', 'Email', 'Email History', 'Text', 'Text History', 'Notes History','Add InTouch', 'InTouchHistory', 'File History', 'Flag Bank', 'Run Background Check', 'Send Password Activation Mail', 'Archive Vendor', 'Resign Vendor', 'Vendor Rating', 'Print Envelope', 'Delete Vendor', 'Online Payment'];
            }
            //   if(rowObject.Status == '1')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            //  if(rowObject.Status == '2')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            if(rowObject.Status == '2')  select = ['Activate This Vendor','Run Background Check','Print Envelope','Delete Vendor','Online Payment'];
            //   if(rowObject.Status == '4')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            //  if(rowObject.Status == '5')  select = ['Edit', 'Tenant Living', 'Add New Tenant', 'Work orders', 'Application', 'Vacancy', 'Unit Directory', 'Inspection', 'General Ledger','Notes History', 'File Library', 'Add Inventory', 'Waiting List', 'FlagBank', 'InTouch', 'InTouchHistory', 'Archive Property', 'Resign This Property', 'Print Envelope','Delete This Property'];
            if(rowObject.Status == '3')  select = ['Run Background Check','Reactivate Vendor','Print Envelope','Delete Vendor','Online Payment'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" vendor_email="'+rowObject.Email+'" vendor_status="'+rowObject.Status+'" vendor_name="'+vendor_name+'" flag="'+flag+'" vendor_id="'+vendor_id+'" rating="'+rating+'" data_id="' + data_id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }

    }

    alphabeticSearch();
    function alphabeticSearch() {
        $.ajax({
            type: 'post',
            url: '/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'users',
                column: 'name',
                where: [{column:'user_type',value:'3',condition:'=',table:'users'}]},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.code == 200) {
                    var html = '';
                    $.each(response.data, function (key, val) {
                        var color = '#05A0E4'
                        if (val == 0)
                            color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:' + color + '" data_id="' + val + '">' + key + '</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });
    }

    $(document).on('click', '#AZ', function () {
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click', '#allAlphabet', function () {
        var grid = $("#vendorTable");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData, {filters: ""});
        grid.trigger("reloadGrid", [{page: 1, current: true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });

    $(document).on('click', '.getAlphabet', function () {
        var grid = $("#vendorTable"), f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if (value != '0') {
            f.push({field: "users.name", op: "bw", data: search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
            grid.trigger("reloadGrid", [{page: 1, current: true}]);
        }
    });


    /**
     * Function for view company on clicking row
     */
// $(document).on('click', '#vendorTable tr td', function () {
//     var id = $(this).closest('tr').attr('id');
//
//     if ($(this).index() == 0 || $(this).index() == 10) {
//         return false;
//     } else if($(this).index() == 2) {
//         var index =$(this).index();
//         var td = $(this).closest('tr').find('td:eq('+index+')').html();
//         td = $(td).text();
//         if(td != '0') {
//             window.location.href = '/Building/BuildingModule?id=' + id
//         }
//     } else if($(this).index() == 3){
//         var index =$(this).index();
//         var td = $(this).closest('tr').find('td:eq('+index+')').html();
//         td = $(td).text();
//         if(td != '0') {
//             window.location.href = '/Unit/UnitModule?id=' + id;
//         }
//     } else {
//         window.location.href = '/Property/PropertyView?id=' + id;
//     }
// });

    /**
     * download sample function
     */
    var base_url = window.location.origin;
    $(document).on("click", '#export_sample_property_button', function() {
        window.location.href = base_url + "/vendor-add-ajax?status=" + "&&action=exportSampleExcel";
    });

    /**
     * export function
     */
    $(document).on("click", '#export_property_button', function() {
        var status = $("#grid_status option:selected").val();
        var vendor = $("#vendor_type option:selected").val();
        var table = 'vendor_additional_detail';
        window.location.href = base_url + "/vendor-add-ajax?status=" + status + "&&vendor=" + vendor + "&&table=" + table + "&&action=exportExcel";
    });

    /**
     * Import function
     */
    $(document).on('click', '#import_property_button', function(e) {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $("#ImportVendor").show(500);
        return false;
    });

    /** Import unit type excel  */
    $("#importVendorForm").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function () {
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'addVendor');
            formData.append('action', 'importExcel');
            $.ajax
            ({
                type: 'post',
                url: '/vendor-add-ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);
                        $("#import_unit_type_div").hide(500);
                        triggerReload();
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    $(document).on("click", "#import_property_cancel", function(e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {
                confirm: {
                    label: 'Yes'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function(result) {
                if (result == true) {
                    $("#ImportVendor").hide(500);
                }
            }
        });
    });

    /**
     * function to format short term rental
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyShortTermRental(cellValue, options, rowObject) {
        if (cellValue == 0)
            return "No";
        else if (cellValue == 1)
            return "Yes";
    }

    /**
     * function to format property status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyStatus(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Active";
        else if (cellValue == '2')
            return "Archive";
        else if (cellValue == '3')
            return "Past";
        else if (cellValue == '4')
            return "Active";
        else if (cellValue == '5')
            return "Active";
        else if (cellValue == '6')
            return "Past";
        else if (cellValue == '7')
            return "Active";
        else if (cellValue == '8')
            return "Active";
        else
            return '';
    }

    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function vendorName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('vendor_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#flagTab" href="javascript:void(0);" data_url="/Vendor/ViewVendor?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }
    }

    /**
     * function to format property unit and building
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyUnitBuilding(cellValue, options, rowObject) {
        if(cellValue == ''){
            return '<span data_redirect="no" style="text-decoration: underline;">0</span>';
        } else {
            return '<span data_redirect="yes" style="text-decoration: underline;">' + cellValue + '</span>';
        }
    }

    /**
     * function to trigger Reload
     */
    function triggerReload() {
        var grid = $("#vendorTable");
        grid[0].p.search = false;
        $.extend(grid[0].p.postData, {filters: ""});
        grid.trigger("reloadGrid", [{page: 1, current: true}]);
    }

    /*function to print element by id */
    function PrintElem(elem)
    {
        Popup($(elem).html());
    }
    /*function to print element by id */
    function Popup(data)
    {
        var base_url = window.location.origin;
        var mywindow = window.open('', 'my div');
        $(mywindow.document.body).html( '<body>' + data + '</body>');
        mywindow.document.close();
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        if(mywindow.close()){

        }
        $("#print_complaint").modal('hide');
        $("#vendorTable").trigger('reloadGrid');
        return true;
    }
    function searchFilters(){
        var grid = $("#vendorTable"),f = [];
        var type = $('#vendor_type').val();
        var status = $('#grid_status').val();
        f.push({field: "vendor_additional_detail.vendor_type_id", op: "eq", data: type},{field: "vendor_additional_detail.status", op: "eq", data: status});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function nameFmatter(cellValue, options, rowObject){

        return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';


    }

    function rateFormatter(cellValue, options, rowObject){
        if(cellValue != '') {
            return default_currency_symbol+cellValue;
        } else {
            return '';
        }
    }

// $(document).on('click','.classFlagRedirect',function(e){
//     event.preventDefault();
//     localStorage.setItem("scrollDown",$(this).attr('data_href'));
//     var url = $(this).attr('data_url');
//     window.location.href = url;
// });
    $(document).on('change','#ReasonOwner',function(){
        var val = $(this).val();
        if(val == 'Other'){
            $('#reasonNoteDiv').show();
        } else {
            $('#reasonNoteDiv').hide();
        }
    });
    $(document).on('click','#addResignReason',function(){
        var id = $('#resignReasonId').val();
        var reasonOwner = $('#ReasonOwner').val();
        var reasonNotes = $('#reasonNotes').val();
        $.ajax({
            type: 'post',
            url: '/vendor-add-ajax',
            data: {class: 'addVendor', action: "resignVendor", id: id,reasonOwner:reasonOwner,reasonNotes:reasonNotes},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#resignReason').modal('hide');
                } else {
                    toastr.error(response.message);
                }
                searchFilters();
            }
        });
    });

    $(document).on('click','.starRatingClass',function(){
        $('.starRatingClass').removeClass('faded-star');
    });

    $('#starDiv').on('hidden.bs.modal', function () {
        $('.starRatingClass').addClass('faded-star');
    })


    $('#starForm').on('submit',function(e){
        e.preventDefault();
        var id = $('#starVendor_id').val();
        var data= $('#starForm').serializeArray();
        var rating = (data != '')?data[0]['value']:'0';
        $.ajax({
            type: 'post',
            url: '/vendor-add-ajax',
            data: {class: 'addVendor', action: "rateVendor", id: id,rating:rating},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#starForm').trigger("reset");
                    $('#starDiv').modal('hide');
                  setTimeout(function () {
                      $("#vendorTable").jqGrid().setGridParam({ sortname: 'vendor_additional_detail.updated_at', sortorder: 'desc' });
                  },1000);
                  setTimeout(function () {
                      $("#vendorTable").trigger("reloadGrid");
                  },1200);
                  setTimeout(function () {
                        onTop(true);
                  },1500);

                } else {
                    toastr.error(response.message);
                }
                searchFilters();
            }
        });
    });

    $(document).on('click','#resetRating',function(){
        var html = vendorStarRating('0');
        $('.vendorRatingDiv').html(html);
    });

    function vendorStarRating(cellValue){
        var html = '';
        switch (cellValue) {
            case '1':
                html = '<input type="radio"  id="rate1" name="rating" value="1" /><label for="rate1" class="starRatingClass" title="Terrible"></label><input type="radio" id="rate2" name="rating" value="2" /><label for="rate2" class="faded-star starRatingClass" title="Not good">2 stars</label><input type="radio" id="rate3" name="rating" value="3" /><label for="rate3" class="faded-star starRatingClass" title="Average">3 stars</label><input type="radio" id="rate4" name="rating" value="4" /><label for="rate4" class="faded-star starRatingClass" title="Very good">4 stars</label><input type="radio" id="rate5" name="rating" value="5" /><label for="rate5" class="faded-star starRatingClass" title="Amazing">5 star</label>';
                break;
            case '2':
                html = '<input type="radio"  id="rate1" name="rating" value="1" /><label for="rate1" class=" starRatingClass" title="Terrible"></label><input type="radio" id="rate2" name="rating" value="2" /><label for="rate2" class="starRatingClass" title="Not good">2 stars</label><input type="radio" id="rate3" name="rating" value="3" /><label for="rate3" class="faded-star starRatingClass" title="Average">3 stars</label><input type="radio" id="rate4" name="rating" value="4" /><label for="rate4" class="faded-star starRatingClass" title="Very good">4 stars</label><input type="radio" id="rate5" name="rating" value="5" /><label for="rate5" class="faded-star starRatingClass" title="Amazing">5 star</label>';
                break;
            case '3':
                html = '<input type="radio"  id="rate1" name="rating" value="1" /><label for="rate1" class="starRatingClass" title="Terrible"></label><input type="radio" id="rate2" name="rating" value="2" /><label for="rate2" class="starRatingClass" title="Not good">2 stars</label><input type="radio" id="rate3" name="rating" value="3" /><label for="rate3" class="starRatingClass" title="Average">3 stars</label><input type="radio" id="rate4" name="rating" value="4" /><label for="rate4" class="faded-star starRatingClass" title="Very good">4 stars</label><input type="radio" id="rate5" name="rating" value="5" /><label for="rate5" class="faded-star starRatingClass" title="Amazing">5 star</label>';
                break;
            case '4':
                html = '<input type="radio"  id="rate1" name="rating" value="1" /><label for="rate1" class="starRatingClass" title="Terrible"></label><input type="radio" id="rate2" name="rating" value="2" /><label for="rate2" class="starRatingClass" title="Not good">2 stars</label><input type="radio" id="rate3" name="rating" value="3" /><label for="rate3" class="starRatingClass" title="Average">3 stars</label><input type="radio" id="rate4" name="rating" value="4" /><label for="rate4" class="starRatingClass" title="Very good">4 stars</label><input type="radio" id="rate5" name="rating" value="5" /><label for="rate5" class="faded-star starRatingClass" title="Amazing">5 star</label>';
                break;
            case '5':
                html = '<input type="radio"  id="rate1" name="rating" value="1" /><label for="rate1" class="starRatingClass" title="Terrible"></label><input type="radio" id="rate2" name="rating" value="2" /><label for="rate2" class="starRatingClass" title="Not good">2 stars</label><input type="radio" id="rate3" name="rating" value="3" /><label for="rate3" class="starRatingClass" title="Average">3 stars</label><input type="radio" id="rate4" name="rating" value="4" /><label for="rate4" class="starRatingClass" title="Very good">4 stars</label><input type="radio" id="rate5" name="rating" value="5" /><label for="rate5" class="starRatingClass" title="Amazing">5 star</label>';
                break;
            default:
                html = '<input type="radio"  id="rate1" name="rating" value="1" /><label for="rate1" class="faded-star starRatingClass" title="Terrible"></label><input type="radio" id="rate2" name="rating" value="2" /><label for="rate2" class="faded-star starRatingClass" title="Not good">2 stars</label><input type="radio" id="rate3" name="rating" value="3" /><label for="rate3" class="faded-star starRatingClass" title="Average">3 stars</label><input type="radio" id="rate4" name="rating" value="4" /><label for="rate4" class="faded-star starRatingClass" title="Very good">4 stars</label><input type="radio" id="rate5" name="rating" value="5" /><label for="rate5" class="faded-star starRatingClass" title="Amazing">5 star</label>';
        }
        return html;
    }

    $(document).on('click','.classFlagRedirect',function(e){
        event.preventDefault();
        localStorage.setItem("scrollDown",$(this).attr('data_href'));
        localStorage.setItem("activeTab",$(this).attr('data_href'));
        var url = $(this).attr('data_url');
        window.location.href = url;
    });

});

function fetchUserCardDetail(id) {
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {
            class: 'CommonAjax',
            action: 'getCustomerDetails',
            id: id},
        success: function (response) {
            var res = JSON.parse(response);
            console.log(res.data.data);
            if(res.code == 200){
                $('#addVendorCardDetailsForm [name ="cfirst_name"]').val(res.data.data.first_name);
                $('#addVendorCardDetailsForm [name ="clast_name"]').val(res.data.data.last_name);
                $('#addVendorCardDetailsForm [name ="cCompany"]').val(res.data.data.company_name);
                $('#addVendorCardDetailsForm [name ="cphoneNumber"]').val(res.data.data.phone_number);
                $('#addVendorCardDetailsForm [name ="caddress1"]').val(res.data.data.address1);
                $('#addVendorCardDetailsForm [name ="caddress2"]').val(res.data.data.address2);
                $('#addVendorCardDetailsForm [name ="ccity"]').val(res.data.data.city);
                $('#addVendorCardDetailsForm [name ="cstate"]').val(res.data.data.state);
                $('#addVendorCardDetailsForm [name ="czip_code"]').val(res.data.data.zipcode);
                $('#addVendorCardDetailsForm [name ="ccountry"]').val(res.data.data.country);
            }
        },
    });
}

function fetchCustomerCardDetail(id) {
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {
            class: 'CommonAjax',
            action: 'getCustomerDetails',
            id: id},
        success: function (response) {
            var res = JSON.parse(response);

            if(res.code == 200){
                $('#financialCardInfo [name ="cfirst_name"]').val(res.data.data.first_name);
                $('#financialCardInfo [name ="clast_name"]').val(res.data.data.last_name);
                $('#financialCardInfo [name ="cCompany"]').val(res.data.data.company_name);
                $('#financialCardInfo [name ="cphoneNumber"]').val(res.data.data.phone_number);
                $('#financialCardInfo [name ="caddress1"]').val(res.data.data.address1);
                $('#financialCardInfo [name ="caddress2"]').val(res.data.data.address2);
                $('#financialCardInfo [name ="ccity"]').val(res.data.data.city);
                $('#financialCardInfo [name ="cstate"]').val(res.data.data.state);
                $('#financialCardInfo [name ="czip_code"]').val(res.data.data.zipcode);
                $('#financialCardInfo [name ="ccountry"]').val(res.data.data.country);
            }
        },
    });
}



$(document).on("blur",".czip_code",function(){
    var value = $(this).val();
    getZipCode('.czip_code', value, '#ccity', '.cstate', '.ccountry', null, null);
});

function getZipCode(element,zipcode,city,state,country,county,validation){
    $(validation).val('1');
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {zip_code: zipcode,class: 'CommonAjax', action: 'getZipcode'},
        success: function (response) {
            var data = JSON.parse(response);
            if(data.code == 200){
                if(city !== undefined)$(city).val(data.data.city);
                if(state !== undefined)$(state).val(data.data.state);
                if(country !== undefined)$(country).val(data.data.country);
                if(county !== undefined)$(county).val(data.data.county);
            } else {
                getAddressInfoByZip(element,zipcode,city,state,country,county,validation);
            }
        },
        error: function (data) {
        }
    });
}




function getAddressInfoByZip(element,zip,city,state,country,county,validation){
    var addr = {};

    addr.element = element;
    addr.elecity = city;
    addr.elestate = state;
    addr.elecountry = country;
    addr.elecounty = county;
    addr.validation = validation;
    if(zip.length >= 5 && typeof google != 'undefined'){
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    response(addr);
                } else {
                    addr.success = false;
                    response(addr);
                }
            } else {
                addr.success = false;
                response(addr);
            }
        });
    } else {
        addr.success = false;
        response(addr);
    }
}





function autofillCompanyData(resposnes){

    $('#financialCompanyInfo [name ="furl"]').val(resposnes.user_account_detail.data.url);
    $('#financialCompanyInfo [name ="fbusiness"]').val(resposnes.user_account_detail.data.business_type);
    $('#financialCompanyInfo [name ="faccount_number"]').val(resposnes.user_account_detail.data.account_number);
    $('#financialCompanyInfo [name ="frouting_number"]').val(resposnes.user_account_detail.data.routing_number);
    $('#financialCompanyInfo [name ="fccity"]').val(resposnes.user_account_detail.data.company_city);
    $('#financialCompanyInfo [name ="fcaddress"]').val(resposnes.user_account_detail.data.company_address1);
    $('#financialCompanyInfo [name ="fcaddress2"]').val(resposnes.user_account_detail.data.company_address2);
    $('#financialCompanyInfo [name ="fczipcode"]').val(resposnes.user_account_detail.data.company_postal_code);
    $('#financialCompanyInfo [name ="fcstate"]').val(resposnes.user_account_detail.data.company_state);
    $('#financialCompanyInfo [name ="fcname"]').val(resposnes.user_account_detail.data.company_name);
    $('#financialCompanyInfo [name ="fcphone_number"]').val(resposnes.user_account_detail.data.company_phone_number);
    $('#financialCompanyInfo [name ="fctax_id"]').val(resposnes.user_account_detail.data.tax_id);
    $("#company_document_id").val(resposnes.user_account_detail.data.company_document_id);
    $('#financialCompanyInfo [name ="fcity"]').val(resposnes.user_account_detail.data.city);
    $('#financialCompanyInfo [name ="faddress"]').val(resposnes.user_account_detail.data.address_1);
    $('#financialCompanyInfo [name ="faddress2"]').val(resposnes.user_account_detail.data.address_2);
    $('#financialCompanyInfo [name ="fzipcode"]').val(resposnes.user_account_detail.data.postal_code);
    $('#financialCompanyInfo [name ="fstate"]').val(resposnes.user_account_detail.data.state);
    $('#financialCompanyInfo [name ="fday"]').val(resposnes.user_account_detail.data.day);
    $('#financialCompanyInfo [name ="fmonth"]').val(resposnes.user_account_detail.data.month);
    $('#financialCompanyInfo [name ="fyear"]').val(resposnes.user_account_detail.data.year);
    $('#financialCompanyInfo [name ="femail"]').val(resposnes.user_account_detail.data.email);
    $('#financialCompanyInfo [name ="ffirst_name"]').val(resposnes.user_account_detail.data.first_name);
    $('#financialCompanyInfo [name ="flast_name"]').val(resposnes.user_account_detail.data.last_name);
    $('#financialCompanyInfo [name ="fphone_number"]').val(resposnes.user_account_detail.data.phone);
    $('#financialCompanyInfo [name ="fphone_number"]').val(resposnes.user_account_detail.data.phone);
    $('#financialCompanyInfo [name ="fssn"]').val(resposnes.user_account_detail.data.ssn_last);
}

function autofillIndividualData(resposnes){
    $('#financialInfo [name ="furl"]').val(resposnes.user_account_detail.data.url);
    $('#financialInfo [name ="fbusiness"]').val(resposnes.user_account_detail.data.business_type);
    $('#financialInfo [name ="faccount_number"]').val(resposnes.user_account_detail.data.account_number);
    $('#financialInfo [name ="frouting_number"]').val(resposnes.user_account_detail.data.routing_number);
    $('#financialInfo [name ="fcity"]').val(resposnes.user_account_detail.data.city);
    $('#financialInfo [name ="faddress"]').val(resposnes.user_account_detail.data.address_1);
    $('#financialInfo [name ="faddress2"]').val(resposnes.user_account_detail.data.address_2);
    $('#financialInfo [name ="fzipcode"]').val(resposnes.user_account_detail.data.postal_code);
    $('#financialInfo [name ="fstate"]').val(resposnes.user_account_detail.data.state);
    $('#financialInfo [name ="fday"]').val(resposnes.user_account_detail.data.day);
    $('#financialInfo [name ="fmonth"]').val(resposnes.user_account_detail.data.month);
    $('#financialInfo [name ="fyear"]').val(resposnes.user_account_detail.data.year);
    $('#financialInfo [name ="femail"]').val(resposnes.user_account_detail.data.email);
    $('#financialInfo [name ="ffirst_name"]').val(resposnes.user_account_detail.data.first_name);
    $('#financialInfo [name ="flast_name"]').val(resposnes.user_account_detail.data.last_name);
    $('#financialInfo [name ="fphone_number"]').val(resposnes.user_account_detail.data.phone);
    $('#financialInfo [name ="fphone_number"]').val(resposnes.user_account_detail.data.phone);
    $('#financialInfo [name ="fssn"]').val(resposnes.user_account_detail.data.ssn_last);

}

$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {
        $(this).prop('checked', false);
        window.open('https://www.victig.com', '_blank');
    }
});


function getTenantInfo(id)
{
    $.ajax({
        type: 'post',
        url: '/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getTenantZipCode',
            id:id},
        success: function (response) {
            var info = JSON.parse(response);
            setTimeout(function () {
                $("#czip_code").val(info.zipcode);
                var name = "("+info.name+")";
                $(".userName").html(name);
                $(".holder_name").val(info.name);
                getZipCode('#czip_code', info.zipcode, '.zipcity', '.zipstate', '.zipcountry', null, null);
            },500);



        },
    });


}