$(document).ready(function () {
    $('#jqgridOptions').on('change', function () {
        var selected = this.value;
        // var deleted_at = true;
        $('#tenantTrasTable').jqGrid('GridUnload');
        jqGrid(selected);
    });

    jqGrid('All');
function jqGrid(status) {
    //var tenant_id = $("input[name='tenantapplytaxid']").val();
    var table = 'tenant_transfer';
    var columns = ['Tenant Name','State/Province','City', 'Current Property','Current Unit','New State','New City','New Property','New Unit','New Occupant','Rental Approved','Status','User_id','Action'];
    //if(status===0) {
    var select_column = [''];
    //}
    var joins = [{table:'tenant_transfer',column:'user_id',primary:'id',on_table:'users'},{table:'tenant_transfer',column:'current_property',primary:'id',on_table:'general_property', as :'gp'},{table:'tenant_transfer',column:'new_property',primary:'id',on_table:'general_property', as :'gprop'},{table:'tenant_transfer',column:'current_unit',primary:'id',on_table:'unit_details',as:'udc'},{table:'tenant_transfer',column:'new_unit',primary:'id',on_table:'unit_details',as:'udn'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [];
    var pagination=[];
    var columns_options = [

        {name:'Tenant Name',index:'first_name', width:120,align:"center",searchoptions: {sopt: conditions},table:'users'},
        {name:'State/Province',index:'state',align:"center", width:120,searchoptions: {sopt: conditions},table:'gp'},
        {name:'City',index:'city', width:120, align:"center",searchoptions: {sopt: conditions},table:'gp'},
        {name:'Current Property',index:'property_name', width:120, align:"center",searchoptions: {sopt: conditions},table:'gp'},
        {name:'Current Unit',index:'unit_prefix', width:120, align:"center",searchoptions: {sopt: conditions},table:'udc'},
        {name:'New State',index:'state', width:120, align:"center",searchoptions: {sopt: conditions},table:'gprop',alias:'gprop_state'},
        {name:'New City',index:'city', width:120, align:"center",searchoptions: {sopt: conditions},table:'gprop',alias:'gprop_city'},
        {name:'New Property',index:'property_name', width:120, align:"center",searchoptions: {sopt: conditions},table:'gprop',alias:'gprop_name'},
        {name:'New Unit',index:'unit_prefix', width:120, align:"center",searchoptions: {sopt: conditions},table:'udn',alias:'udn_unit'},
        {name:'New Occupant',index:'id', width:120, align:"center",searchoptions: {sopt: conditions},table:table,formatter:occupantFmatter},
        {name:'Rental Approved',index:'rental_approved', width:120, align:"center",searchoptions: {sopt: conditions},table:table,formatter:rentFmattr},
        {name:'Status',index:'status', width:120, align:"center",searchoptions: {sopt: conditions},table:table,formatter: statusFmatter},
        {name:'User_id',index:'user_id', width:120,hidden:true, align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'', title: false, width:120,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:selectFmatter1},

    ];
    var ignore_array = [];
    jQuery("#tenantTrasTable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "tenant_transfer",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'tenant_transfer.updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Tenants",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}
    function statusFmatter (cellValue, options, rowObject){

     //console.log("cell",cellValue);
        if(cellValue == '1'){
        return 'Request Generated';
        }
        else if(cellValue == '2'){
            return 'Approved';
        }
        else if(cellValue == '3'){
            return 'Lease Generated';
        }
        else if(cellValue == '4'){
            return 'Transferred';
        }
        else if(cellValue == '5'){
            return 'Declined';
        }
        else{
            return '';
        }

    }
    function rentFmattr (cellValue, options, rowObject){

        //console.log("cell",cellValue);

            return 'Yes';

    }
    function occupantFmatter (cellValue, options, rowObject){

        console.log("cell",cellValue);
        if(cellValue==""){
            return 'No';
        }
        else{
            return 'Yes';
        }

    }


    function changeStatus(user_id){

        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "changeStatus",
                user_id:user_id,

            },
            success: function (response) {
                var response = JSON.parse(response);

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function selectFmatter1 (cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = '';
            if (rowObject['Status'] == '1') select = ['APPROVE','DECLINE'];
            if (rowObject['Status'] == '2') select = ['LEASE DETAILS'];
            if (rowObject['Status'] == '3') select = ['TRANSFER'];
            if (rowObject['Status'] == '4') select = [];
            if (rowObject['Status'] == '5') select = ['DELETE'];
            var data = '';
            if (select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }

            return data;
        }
    }
    $(document).on("change", "#tenantTrasTable .select_options", function (e) {
        e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var user_id = $(this).parent().prev().text();
        switch (action) {
            case "APPROVE":
                bootbox.confirm("Do you want to approve this transfer?", function (result) {
                    if (result == true) {
                        localStorage.setItem('approve_active','');
                        var approveActive = "one";
                        localStorage.setItem('approve_active',approveActive);
                        window.location.href = base_url + '/Tenant/TransferTenant?tenant_id='+user_id;
                        changeStatus(user_id);
                    }
                    else
                    {
                        $('#tenantTrasTable').trigger('reloadGrid');
                    }
                });
                break;
            case "LEASE DETAILS":
                localStorage.setItem('approve_active','');
                var approveActive = "one";
                localStorage.setItem('approve_active',approveActive);
                window.location.href = base_url + '/Tenant/TransferTenant?tenant_id='+user_id;
                break;
            case "TRANSFER":
                $("#transferConfirm").modal(show);
                gettenantTransfer(user_id,id);
              //  var data=' <a class="pop-add-icon select_building_popup" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#transferConfirm">'
                break;
            case "DECLINE":
                    bootbox.confirm("Do you want to decline this transfer?", function (result) {
                        if (result == true) {
                            declineTenant(user_id);
                        }
                        else
                        {
                            $('#tenantTrasTable').trigger('reloadGrid');
                        }
                    });
                //  var data=' <a class="pop-add-icon select_building_popup" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#transferConfirm">'
                break;
            case "DELETE":
                bootbox.confirm("Do you want to delete this record?", function (result) {
                    if (result == true) {
                        deleteTenant(user_id);
                    }
                    else
                    {
                        $('#tenantTrasTable').trigger('reloadGrid');
                    }
                });
                //  var data=' <a class="pop-add-icon select_building_popup" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#transferConfirm">'
                break;
            case "Edit":
                localStorage.setItem('edit_active', '');
                var editActiveTab = "tenant-detail-one";
                localStorage.setItem('edit_active', editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id=' + id;
                break;
        }
    });

    function gettenantTransfer(user_id,id){
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "gettenantTransfer",
                user_id:user_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $(".hidden_transfer_id").val(id);
                    $(".actual_transfer_date ").val(response.data.user);
                    $(".scheduled_transfer_date ").val(response.data.user);
                    $(".old_securtity ").val('0.00');
                    $(".new_securtity ").val(response.data.sec_dep.security_deposite);
                    $(".hidden_ten_tras_id ").val(response.data.tenant_id);
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
    $(document).on("click","#save_transfer_tenant",function () {
        save_actual_move_out();
    });
    function save_actual_move_out(){
       var tenant_id= $(".hidden_ten_tras_id ").val();
       var tranfer_id=$(".hidden_transfer_id").val();
       var formData=$("#tenant_transfer_confirm").serializeArray();
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "save_actual_move_out",
                tenant_id:tenant_id,
                formData:formData,
                tranfer_id:tranfer_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                   toastr.success("Tenant transferred successfully");
                    $('#tenantTrasTable').trigger('reloadGrid');
                    $("#transferConfirm").modal('hide');
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
    function declineTenant(user_id){
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "declineTenant",
                user_id:user_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#tenantTrasTable').trigger('reloadGrid');
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
    function deleteTenant(user_id){
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "deleteTenant",
                user_id:user_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#tenantTrasTable').trigger('reloadGrid');
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
});





