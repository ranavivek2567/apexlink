$(document).ready(function() {
    //checkFirstPaymentStatus();

    $(document).on("click",".edit_short",function () {
        $('#listing_tab').hide();
        $('#edit_tab').show();
        var user_id= $(".hidden_id_edit").val();
        geteditdata(user_id);
    });
    $(document).on("click",".flag_short",function () {
        var user_id=$(".hidden_id_edit").val();
        $('.tenant_id').val(user_id);
        $('#listing_tab').hide();
        $('#edit_tab').hide();
        $('.flag-tab').show();
        $(".nav-tabs").eq(0).html("");

    });
if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('#short_term').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('#short_term').find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getInitialData',
        data: {
            class: "TenantAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {


                if (data.data.ethnicity.length > 0) {
                    var ethnicityOption = "";
                    $.each(data.data.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='" + value.id + "'>" + value.title + "</option>";
                    });
                    //$('select[name="ethncity"]').html(ethnicityOption);
                    $('.ethnicity_short1').html(ethnicityOption);
                }
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
    $(document).on('click','#short_term tr td:not(:last-child)',function(e){
        e.preventDefault();
        $(".short_term_view_div").show();
        $(".short_term_grid").hide();
        //var id = $(this).closest("tr").attr('id');
        var user_id= $(this).closest("tr").find("td").eq(1).text();
        $(".hidden_id_edit").val(user_id);
        viewShortTerm(user_id);
    });
    $(document).on("click",".backshort",function () {
        $(".short_term_view_div").hide();
        $(".short_term_grid").show();

    });
    function viewShortTerm(user_id) {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "viewshortterm",
                user_id:user_id
            },
            success: function (response) {
              //  console.log(response);return;
                var response = JSON.parse(response);
                if(response.salutation=="Select"){
                    $(".renter_name").text(response.data.user_name);
                }else{
                    $(".renter_name").text(response.data.salutation+' '+ response.data.user_name);
                }
                $(".phone_no").text(response.data.phone_number);
                $(".property_name").text(response.data.property_name);
                $(".building_name").text(response.data.building_name);
                $(".company_name").text(response.data.company_name);
                $(".unit_no").text(response.data.unit);
                $(".phone_no").text(response.data.phone_number);
                $(".note_phone").text(response.data.phone_number_note);
                $(".office_ext").text('');
                $(".email_short").text(response.data.email);
                $(".hobbies").text(response.data.hobby);
                $(".martialStatus").text(response.data.marital);
                $(".ethnicity").text(response.data.title);
                $(".veteranShort").text(response.data.veteran);
                $(".bookingDates").text(response.data1);
                $(".booking_type").text(response.data.frequency);
                $('#shortimagege').attr('src', response.data.image);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    shorttermretntal();
    function shorttermretntal() {
  //      var idd= $('.hidden_form2').val();

        var table = 'short_term_rental';
        var columns = ['Renter Name','user_id','Email', 'Phone', 'Property Name','Unit','Unit_id','Unit_prefix','Rent('+default_currency_symbol+')','Days Remaining','Status ','Action'];
        var select_column = ['Edit','Email','Email History','Flag Bank','Text','SMS History','Run Background check','Book Now','Booking History','Cancel Booking'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [];
        var extra_columns = [];
        var joins = [{table: 'short_term_rental', column: 'user_id', primary: 'id', on_table: 'users'},{table: 'short_term_rental', column: 'property_id', primary: 'id', on_table: 'general_property'},{table: 'short_term_rental', column: 'unit_id', primary: 'id', on_table: 'unit_details'}
        ];
        var columns_options = [
            {name:'Renter Name',index:'name', width:100,searchoptions: {sopt: conditions},table:'users',formatter:colorformatter},
            {name:'user_id',index:'user_id',hidden:true, width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Email',index:'email', width:100,searchoptions: {sopt: conditions},table:'users'},
            {name:'Phone',index:'phone_number', width:100,searchoptions: {sopt: conditions},table:'users'},
            {name:'Property Name',index:'property_name', width:100,searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Unit Number',index:'unit_no', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',formatter:unitprefixformatter},
            {name:'Unit_id',index:'id',hidden:true, width:100,searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Unit_prefix',index:'unit_prefix',hidden:true, width:100,searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Rent(AFN)',index:'base_rent', width:100,searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Days Remaining',index:'end_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'count_remaining_days'},
            {name:'Status',index:'status', width:100,searchoptions: {sopt: conditions},table:'users',change_type:'short_term_status'},
            {name:'Action',index:'', width:100,title:false,searchoptions: {sopt: conditions},table:table,formatter:statusFmatter1},
        ];

        var ignore_array = [];
        jQuery("#short_term").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'short_term_rental.updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Short Term Renters",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }

    function shortterhistory(user_id) {
        var table = 'short_term_rental';
        var columns = ['Renter Name','Email', 'Phone', 'Property Name','Unit','Unit_id','Unit_prefix','Rent(AFN)'];
        var select_column = ['Edit','Email','Email History','Flag Bank','Text','SMS History','Run Background check','Book Now','Booking History','Cancel Booking'];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_where = [{column:'user_id',value:user_id,condition:'=',table:'short_term_rental'}];
        var extra_columns = [];
        var joins = [{table: 'short_term_rental', column: 'user_id', primary: 'id', on_table: 'users'},{table: 'short_term_rental', column: 'property_id', primary: 'id', on_table: 'general_property'},{table: 'short_term_rental', column: 'unit_id', primary: 'id', on_table: 'unit_details'}
        ];
        var columns_options = [
            {name:'Renter Name',index:'name', width:250,searchoptions: {sopt: conditions},table:'users',formatter:colorformatter},
            {name:'Email',index:'email', width:220,searchoptions: {sopt: conditions},table:'users'},
            {name:'Phone',index:'phone_number', width:220,searchoptions: {sopt: conditions},table:'users'},
            {name:'Property Name',index:'property_name', width:220,searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Unit Number',index:'unit_no', width:220, align:"left",searchoptions: {sopt: conditions},table:'unit_details',formatter:unitprefixformatter},
            {name:'Unit_id',index:'id',hidden:true, width:220,searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Unit_prefix',index:'unit_prefix',hidden:true, width:220,searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Rent(AFN)',index:'base_rent', width:220,searchoptions: {sopt: conditions},table:'unit_details'},
        ];

        var ignore_array = [];
        jQuery("#short_term_history_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_where:extra_where,
                extra_columns:extra_columns,
                deleted_at:'true'
            },
            viewrecords: true,
            sortname: 'short_term_rental.updated_at',
            sortorder: "desc",
            sorttype:'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Renter History",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:10,left:200,drag:true,resize:false} // search options
        );
    }
    function colorformatter(cellvalue, options, rowObject) {
        if(cellvalue != undefined){
            var html="";
            html="<span style='text-decoration: underline;color: #05A0E4; font-weight: bold;'>"+cellvalue+"</span>";
            return html;
        }
    }

    function unitprefixformatter(cellvalue, options, rowObject) {
        if(cellvalue != undefined){
        var con="";
        if(rowObject.Unit_prefix!="") {
            con = rowObject.Unit_prefix + "-" + cellvalue;
        }
        else {
            con=cellvalue;
        }
        return con;
        }
    }


    function statusFmatter1(cellvalue, options, rowObject) {
        if (rowObject !== undefined) {
            var select = ['Edit','Email','Email History','Flag Bank','Text','SMS History','Run Background check','Book Now','Booking History','Cancel Booking'];
            console.log('obj',rowObject);
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_optionss" data_id="' + rowObject.id + '" data-user_id="' + rowObject.user_id + '" data-unit="' + rowObject.id + '" ><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }


    $(document).on('change', '.select_optionss', function() {
        setTimeout(function(){ $(".select_options").val("default"); }, 200);
        $('#short_term').trigger( 'reloadGrid' );
        var base_url = window.location.origin;
        var opt = $(this).val();
        var id = $(this).attr('data_id');
        var email = $("tr#"+id).find("td").eq(2).text();
        var name = $("tr#"+id).find("td").eq(0).text();
        $(".hidden_name_val").val(name);
        console.log('email',email);
        var user_id = $(this).attr('data-user_id');
        var row_num = $(this).parent().parent().index() ;
        var status = $(this).attr('status');
        var is_default = $(this).attr('is_default');
        if (opt == 'Edit' || opt == 'EDIT') {
            var user_id= $(this).closest("tr").find("td").eq(1).text();
            $(".hidden_id_edit").val(user_id);
            $('#listing_tab').hide();
            $('#edit_tab').show();
            geteditdata(user_id);
        }
        if (opt == 'Email' || opt == 'EMAIL') {
            window.location.href = base_url + '/Communication/ComposeEmail';
        }

        if (opt == 'Email History' || opt == 'EMAIL HISTORY') {
            localStorage.setItem('predefined_mail',email);
            localStorage.setItem('table_green_id',id);
            localStorage.setItem('table_green_tableid', '#short_term');
            localStorage.setItem('table_green_url','/Communication/WaitingList');
            window.location.href = base_url + '/Communication/SentEmails';
        }
        if (opt == 'Flag Bank' || opt == 'FLAG BANK') {
             $('.tenant_id').val(user_id);
             $('#listing_tab').hide();
             $('#edit_tab').hide();
             $('.flag-tab').show();
            $(".nav-tabs").eq(0).html("");
            $("#flagged_for").val(name)
        }
        if (opt == 'Text' || opt == 'TEXT'){
            localStorage.setItem('table_green_id',id);
            localStorage.setItem('table_green_tableid', '#short_term');
            localStorage.setItem('table_green_url','/Communication/WaitingList');
            localStorage.setItem('predefined_text',email);
            window.location.href = base_url + '/Communication/AddTextMessage';
        }
        if (opt == 'SMS History' || opt == 'SMS HISTORY'){
            localStorage.setItem('table_green_id',id);
            localStorage.setItem('table_green_tableid', '#short_term');
            localStorage.setItem('table_green_url','/Communication/WaitingList');
            localStorage.setItem('predefined_text',email);
            window.location.href = base_url + '/Communication/TextMessage';
        }
        if (opt == 'Run Background check' || opt == 'RUN BACKGROUND CHECK'){
                    $("#backGroundCheckPop").modal('show');
        }
        if (opt == 'Book Now' || opt == 'Book Now'){
        $("#shortTermRental").modal(show);

          //  window.location.href = base_url + '/Tenant/ViewEditWatingList?id='+id;
        }
        if (opt == 'Booking History' || opt == 'BOOKING HISTORY'){
            $('#short_term_history_table').jqGrid('GridUnload');
            shortterhistory(user_id);
            $(".booking_history_shortTerm").show();
            $(".short_term_grid").hide();
           // window.location.href = base_url + '/Tenant/ViewEditWatingList?id='+id;
        }
        if (opt == 'Cancel Booking' || opt == 'CANCEL BOOKING'){
            bootbox.confirm({
                message: "Are you sure you want to cancel this booking",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        cancelbooking(user_id);
                    }else{
                        $('#short_term').trigger( 'reloadGrid' );
                    }
                }
            });

        }

    });
    $(document).on("click",".rental_history_back",function () {
        $(".booking_history_shortTerm").hide();
        $(".short_term_grid").show();
    });

    $(document).on("click",".selectPropertyEthnicity1",function(){
        $("#selectPropertyEthnicity1").show();
    });

    $(document).on("click",".selectPropertyHobbies1",function(){
        $("#selectPropertyHobbies1").show();
    });

    $(document).on("click",".selectPropertyMaritalStatus1",function(){
        $("#selectPropertyMaritalStatus1").show();
    });

    $(document).on("click",".selectPropertyVeteranStatus1",function(){
        $("#selectPropertyVeteranStatus1").show();
    });

    $(document).on("click",".add-popup .grey-btn",function(){
        var box = $(this).closest('.add-popup');
        $(box).hide();
    });
    function cancelbooking(user_id){
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "cancelbooking",
                user_id:user_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response);
                $('#short_term').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on("click", ".primary-tenant-phone-row-short1 .fa-plus-circle", function () {
        var clone = $(".primary-tenant-phone-row-short1:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".primary-tenant-phone-row-short1").first().after(clone);
        $(".primary-tenant-phone-row-short1:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-tenant-phone-row-short1:not(:eq(0))  .fa-minus-circle").show();
        var phoneRowLenght = $(".primary-tenant-phone-row-short1");
        console.log(phoneRowLenght);
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        } else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-tenant-phone-row-short1:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-short1:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on("click", ".primary-tenant-phone-row-short1 .fa-minus-circle", function () {

        var phoneRowLenght = $(".primary-tenant-phone-row-short1");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row-short1:eq(0) .fa-plus-circle").show();
        } else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row-short1:eq(0) .fa-plus-circle").hide();
        } else {
            $(".primary-tenant-phone-row-short1:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-tenant-phone-row-short1").remove();
    });
    $(document).on("click", ".email-plus-sign-short-term", function () {
        var emailRowLenght = $(".multipleEmailShortTerm1");
        if (emailRowLenght.length == 2) {

            $(".email-plus-sign-short-term").hide();
        }
        var clone = $(".multipleEmailShortTerm1:first").clone();
        clone.find('input[type=text]').val('');
        $(".multipleEmailShortTerm1").first().after(clone);
        $(".multipleEmailShortTerm1:not(:eq(0))  .email-plus-sign-short-term").remove();
        $(".multipleEmailShortTerm1:not(:eq(0))  .fa-minus-circle-short-term").show();

    });

    /*for multiple email textbox*/


    /*Remove Email Textbox*/
    $(document).on("click", ".fa-minus-circle-short-term", function () {
        var emailRowLenght = $(".multipleEmailShortTerm1");

        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".email-plus-sign-short-term1").show();
        }
        $(this).parents(".multipleEmailShortTerm1").remove();
    });

    /*Remove Email Textbox*/

    $(document).on("click", ".ssn-plus-sign-short_term", function () {
        var clone = $(".multipleSsnShortTerm1:first").clone();
        clone.find('input[type=text]').val('');
        $(".multipleSsnShortTerm1").first().after(clone);
        $(".multipleSsnShortTerm1:not(:eq(0))  .ssn-plus-sign-short_term").remove();
        $(".multipleSsnShortTerm1:not(:eq(0))  .ssn-remove-sign-short-term").show();

    });
    /* for multiple SSN textbox*/


    /*remove ssn textbox*/
    $(document).on("click", ".ssn-remove-sign-short-term", function () {
        $(this).parents(".multipleSsnShortTerm1").remove();
    });

    /*remove ssn textbox*/



    function geteditdata(id) {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "geteditdata",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var res = response.data[0];
                console.log('response',res);
               $.each(res,function(key,value){
               $('.'+key).val(value);
               });

                if(response.data[0].email2!="" && response.data[0].email2!=null){
                    var clone = $(".multipleEmailShortTerm1:first").clone();
                    clone.find('input[type=text]').val('');
                    $(".multipleEmailShortTerm1").first().after(clone);
                    $(".multipleEmailShortTerm1:not(:eq(0))  .email-plus-sign-short-term").remove();
                    $(".multipleEmailShortTerm1:not(:eq(0))  .fa-minus-circle-short-term").show();
                    $(".email:eq(1)").val(response.data[0].email2);
                }
                if(response.data[0].email3!="" && response.data[0].email3!=null){
                    var clone = $(".multipleEmailShortTerm1:first").clone();
                    clone.find('input[type=text]').val('');
                    $(".multipleEmailShortTerm1").first().after(clone);
                    $(".multipleEmailShortTerm1:not(:eq(0))  .email-plus-sign-short-term").remove();
                    $(".multipleEmailShortTerm1:not(:eq(0))  .fa-minus-circle-short-term").show();
                    $(".email-plus-sign-short-term").hide();
                    $(".email:eq(2)").val(response.data[0].email3);
                    $(".email:eq(1)").val(response.data[0].email2);
                }

                if((response.data[0].ssn_sin_id).length <= 1){
                    console.log('kk',response.data[0].ssn_sin_id);
                    $('input[name="ssn_id"]').val(response.data[0].ssn_sin_id);
                }
                else if((response.data[0].ssn_sin_id).length > 1){
                    var clone = $(".multipleSsnShortTerm1:first").clone();
                    clone.find('input[type=text]').val('');
                    $(".multipleSsnShortTerm1").first().after(clone);
                    $(".multipleSsnShortTerm1:not(:eq(0)) .ssn-plus-sign-short_term").remove();
                    $(".multipleSsnShortTerm1:not(:eq(0)) .fa-minus-circle").show();
                    // $(".ssn-plus-sign-short_term").hide();
                        for(var i=0;i<(response.data[0].ssn_sin_id).length;i++){
                            console.log("test",response.data[0].ssn_sin_id);
                            $('input[name="ssn_id"]:eq('+i+')').val(response.data[0].ssn_sin_id[i]);
                        }
                }
                if(response.data[0].phone_numberr =="" || response.data[0].phone_numberr == undefined){
                  //  alert('jj');
                    $(".phone_typee").val(response.data[0].phone_typ);
                    $(".career").val(response.data[0].career);
                    $(".codde").val(response.data[0].country_user);
                    $(".phone_numbeer").val(response.data[0].phone_number);
                    $("input[name=notePhone]").val(response.data[0].phone_number_note);
                    if(response.data[0].image!==null){
                        $('#short_term_edit').attr('src', response.data[0].image);
                    }
                }
                else if(response.data[0].phone_numberr !="" || response.data[0].phone_numberr != undefined){
                    alert('kk');
                    var clone = $(".primary-tenant-phone-row-short:first").clone();
                    clone.find('input[type=text]').val('');
                    $(".primary-tenant-phone-row-short1").first().after(clone);
                    $(".primary-tenant-phone-row-short1:not(:eq(0)) .fa-plus-circle").hide();
                    $(".primary-tenant-phone-row-short1:not(:eq(0)) .fa-minus-circle").show();
                    var phoneRowLenght = $(".primary-tenant-phone-row-short1");
                   // console.log(phoneRowLenght.length);
                    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
                    console.log('rr',response.data[0].phone_numberr);
                    $(".phone_typee:eq(0)").val(response.data[0].phone_typ);
                    $(".career:eq(0)").val(response.data[0].career);
                    //   $(".codde").val(response.data[0]);
                    $(".phone_numbeer:eq(0)").val(response.data[0].phone_number);

                    for(var i=1;i<=phoneRowLenght.length;i++){
                     $(".phone_typee:eq("+i+")").val(response.data[0].phone_type);
                     $(".career:eq("+i+")").val(response.data[0].carrier);
                     $(".codde:eq("+i+")").val(response.data[0].country_code);
                     $(".phone_numbeer:eq("+i+")").val(response.data[0].phone_numberr);
                     }
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

        $(document).on('click','#cancel_genifo',function(){
        $('#edit_tab').hide();
        $('#listing_tab').show();
        });

        $(document).on('click','#update_geninfo',function(){
        update_edit();
        });

    function update_edit(){
    var id = $('#user_id').val();

    var formarray= $('#edit_general_short_term').serializeArray();
    console.log('idd',id);
    $.ajax({
                type: 'post',
                url: '/ShortTermRental-Ajax',
                data: {
                    class: "TenantShortTermAjax",
                    action: "update_genInformation",
                    formData:formarray,
                    id:id
                },
                success: function (response) {
                   //  var response = JSON.parse(response);
                   //  var res = response.data[0];
                   //  console.log('response',res);
                   // $.each(res,function(key,value){

                   // $('.'+key).val(value);
                   // })
                    $('#short_term').trigger( 'reloadGrid' );
                    toastr.success("Record updated successfully");
                   $('#edit_tab').hide();
                   $('#listing_tab').show();
                },
                error: function (data){
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value){
                        $('#' + key + '_err').text(value);
                    });
                }
            });
    }

    function unit_data() {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "getunitdata",
            },
            success: function (response){
              var res= JSON.parse(response);
                $("#count_prop").val(res.count);
                $("#unit_detail").val(JSON.stringify(res.data));
                console.log('ff',JSON.stringify(res.data));
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function groupBy(arr, property) {
        return arr.reduce(function(memo, x) {
            if (!memo[x[property]]) { memo[x[property]] = []; }
            memo[x[property]].push(x);
            return memo;
        }, {});
    }

    calendar_data();
    var grouped_data = [];
    function calendar_data() {
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "getcalendardata",
            },
            success: function (response){
            var res= JSON.parse(response);
            console.log(res.data);
               var all_unit=[];
                var all_date=[];
                var all_name=[];
                grouped_data=groupBy(res.data, 'unit_no');
                console.log('getcalendardata',grouped_data);
             $.each(res.data,function (key,value){
            all_date.push(value.date);
            all_unit.push(value.unit_prefix+'-'+value.unit_no);
            all_name.push(value.name);
        });
        $("#all_dates").val(JSON.stringify(all_date));
        $("#all_units").val(JSON.stringify(all_unit));
        $("#all_names").val(JSON.stringify(all_name));

            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

   $('#search_list').click(function () {
       unit_noo=[];
       grouped_data=[];
       search_values();
       //calendar_data();
       setTimeout(function () {
           calc()
       },50);
       setTimeout(function () {
           name_show();
       },100);
   });

    $(document).on('keyup','#all_search',function () {
        //   grouped_data=[];
        unit_noo=[];
        grouped_data=[];
        search_data();
       // calendar_data();
          setTimeout(function () {
              calc();
          },105);
        setTimeout(function () {
            name_show();
        },100);
    });

    function search_values() {
        var st_date=$("#start_date").val();
        var end_date=$("#last_date").val();
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "getsearchdata",
                'st_date':st_date,
                'end_date':end_date
            },
            success: function (response){
                var res= JSON.parse(response);
                grouped_data=groupBy(res.data2, 'unit_no');
                $("#count_prop").val(res.count);
                $("#unit_detail").val(JSON.stringify(res.data));
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function search_data() {
        var a=$("#all_search").val();
        $.ajax({
            type: 'post',
            url: '/ShortTermRental-Ajax',
            data: {
                class: "TenantShortTermAjax",
                action: "getinputdata",
                'a':a
            },
            success: function (response){
                var res= JSON.parse(response);
                console.log('res',res);
                grouped_data=groupBy(res.data2, 'unit_no');
                $("#count_prop").val(res.count);
                $("#unit_detail").val(JSON.stringify(res.data));
            },
            error: function (data){
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    var monthNames = ["January", "February", "March", "April", "May","June","July",
            "August", "September", "October", "November","December"];
        var d2 = new Date();
        $('#dynamic_year').val(d2);
        var d4= new Date($('#dynamic_year').val());
        var year4=d4.getFullYear();
        var month5=d4.getMonth();
        var month3=d2.getMonth() + 1;
        var month2 = d2.getMonth() ;
        var year2=d2.getFullYear();
        var firstDay = new Date(year2, month2, 1);
        var lastDay = new Date(year2, month3, 0);
        var curr=monthNames[month5]+' '+year4;
        $('#date_dynamic').val(firstDay);
        $('#date_dynamic_last').val(lastDay);
        $('#montth').text(curr);
        $("#date_change").val(month3);
        $('.img-calendar').click(function () {
            unit_data();
            calendar_data();
            setTimeout(function () {
               calc()
           },100);
    });


    $(document).on('click','#left_inc',function () {
        month3=month3-1;
        $("#date_change").val(month3);
         month2=month2-1;
         firstDay = new Date(year2, month2, 1);
         lastDay = new Date(year2, month3, 0);
         $('#dynamic_year').val(firstDay);
         var dd=new Date($('#dynamic_year').val());
         var change_m=dd.getMonth();
         var change_y=dd.getFullYear();
         curr=monthNames[change_m]+' '+change_y;
         $('#montth').text(curr);
         $('#date_dynamic').val(firstDay);
         $('#date_dynamic_last').val(lastDay);
         unit_noo=[];
         calc();
        name_show();
    });

    $(document).on('click','#right_inc',function () {
        month3=month3 + 1;
        $("#date_change").val(month3);
        month2=month2 + 1;
        firstDay = new Date(year2, month2, 1);
        lastDay = new Date(year2, month3, 0);
        $('#dynamic_year').val(firstDay);
        var dd=new Date($('#dynamic_year').val());
        var change_m=dd.getMonth();
        var change_y=dd.getFullYear();
        curr=monthNames[change_m]+' '+change_y;
        $('#montth').text(curr);
        $('#date_dynamic').val(firstDay);
        $('#date_dynamic_last').val(lastDay);
        unit_noo=[];
        calc();
        name_show();
    });


    var unit_noo=[];
    function calc(){

        $('#calendar').modal('show');
        var rows=$("#count_prop").val();
        console.log('cnt',$("#count_prop").val());
        var unit=new Array();
        var ten_names=new Array();
        if($("#unit_detail").val() != '') unit=JSON.parse($("#unit_detail").val());
        if($("#all_names").val() != '') ten_names=JSON.parse($("#all_names").val());
        var unit_det=[];

        console.log(unit);
        $.each(unit,function (key,value) {
            var arr=value.property_name+" "+":"+" "+value.building_name+" "+":"+" "+value.unit_prefix+'-'+value.unit_no+" "+"("+currencySign+""+value.base_rent+".00"+")";
            var arr1=new Array();
               arr1={name:arr,title:value.unit_no};
               unit_det.push(arr1);
            //console.log('un',value.unit_prefix+'-'+value.unit_no);
            unit_noo.push(value.unit_prefix+'-'+value.unit_no);
        });
        console.log('arddddr',unit_noo);

        function daysInMonth(iMonth, iYear)
        {
            return 32 - new Date(iYear, iMonth, 32).getDate();
        }
        // function isWeekday(year, month, day) {
        //     var day = new Date(year, month, day).getDay();
        //     return day !=0 && day !=6;
        // }
        // function getWeekdaysInMonth(month, year) {
        //     var days = daysInMonth(month, year);
        //     var weekdays = 0;
        //     for(var i=0; i< days; i++) {
        //         if (isWeekday(year, month, i+1)) weekdays++;
        //     }
        //     return weekdays;
        // }
        Date.prototype.addDays = function(days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        };
        var f=$("#date_dynamic").val();
        f=new Date(f);
        f=f.getDate();
        var l=$("#date_dynamic_last").val();
        l=new Date(l);
        l=l.getDate();
         var d1 = new Date();
         var date1=d1.getDate();
         var month1 = d1.getMonth() + 1;
         var year1 = d1.getFullYear();
         var days1=daysInMonth(month1,year1);
        // var dayName = d.toString().split(' ')[0].substr(0,2);
         var total=l-f;

        var cols=[];
        for (var i=0;i<=total;i++){
            var a=$("#date_dynamic").val();
            a = new Date(a);
            a=a.addDays(i);
         //   console.log(a);
            var date=[a.getDate()];
            date.push(date);
            var month = a.getMonth() + 1;
            var year = a.getFullYear();
            var days=daysInMonth(month,year);
            var dayName = [a.toString().split(' ')[0].substr(0,2)];
            dayName.push(dayName);
                cols.push(dayName+" "+date);
        }

        var len=cols.length;
        var arr2=[];
        $.each(cols,function (key,value){
            var str = value.replace(/,/g,"");
            //str=str[0]+''+str[1]+' '+str[4]+''+((str[5])?(str[5]):'');
            var arr1={name:str};
            arr2.push(arr1);
        });

       var ff=$("#date_dynamic").val();
        ff=new Date(ff);
        var mm = ff.getMonth() + 1;
        console.log(mm);
        ff.setDate(1);
        var all_days = [];
        while ((ff.getMonth() +1) == mm) {
            var d = ff.getFullYear() + '-' + mm .toString().padStart(2, '0') + '-' + ff.getDate().toString().padStart(2, '0');
            all_days.push(d);
            ff.setDate(ff.getDate() + 1);
        }
  //      console.log('All Dates',$("#all_dates").val());
        console.log('All Units',$("#all_units").val());
        var unit_data,date_data = new Array();
        if($("#all_units").val() != '') unit_data=JSON.parse($("#all_units").val());
        if($("#all_units").val() != '') date_data=JSON.parse($("#all_dates").val());
        // console.log(unit_data);
         console.log(unit_data);
         console.log(all_days);


        // console.log('all',all_days);
        // console.log('unit_no',unit_noo);
        // console.log('unit_data',unit_data);
         console.log('unit_noo',unit_noo);
     //   (grouped_data.length > 1) ? grouped_data=grouped_data : grouped_data=[];
        // console.log('date_data',date_data);
        console.log('group_data',grouped_data);
        var markers = [];
         var groupDates = [];
        $.each(grouped_data, function (key, value) {
            var gdata = [];
            var gname = '';
            $.each(value, function (key1, value1) {
                gdata.push(value1.date);
                gname = value1.name
            });
            groupDates.push({'key': key, 'value': gdata,'name':gname});
        });
        console.log('groupDates',groupDates);
        $.each(unit_noo,function (key1,value1) {
            var arrayData = [];
           value1= value1.substring(value1.indexOf('-') + 1);
            console.log('val1',value1);
            $.each(groupDates, function (key2, value2) {
                if(value2.key == value1){
                    $.each(all_days, function (key3, value3) {
                        if((value2.value).includes(value3)){
                            arrayData.push(1);
                        } else {
                            arrayData.push(0);
                        }
                    });
                }
            });
            //markers.push({'unit_id':value1,'days':arrayData});
            markers.push(arrayData);
        });

       // var groupDates =  groupDates.filter(e => e.length);
        console.log('markers',markers);

         // $.each(unit_noo,function (key1,value1) {
         //     var arrayData = [];
         //     if(unit_data.includes(value1)) {
         //         console.log('value1',value1);
         //         //var intersection = unit_noo.filter(element => unit_data.includes(element));
         //         $.each(all_days, function (key, value) {
         //             console.log(value);
         //             if (date_data.includes(value)) {
         //                 arrayData.push(1);
         //             } else {
         //                 arrayData.push(0);
         //             }
         //         });
         //
         //     } else {
         //         arrayData.push(0);
         //     }
         //     markers.push(arrayData);
         // });

        // $.each(groupDates,function (key1,value1) {
        //     var arrayData = [];
        //     $.each(all_days, function (key2, value2) {
        //         $.each(grouped_data[value1], function (key, value) {
        //             if(value2 == value.date){
        //                 arrayData.push(1);
        //             }
        //         });
        //
        //     });
        //     markers.push(arrayData);
        // });

        // var markers =  markers.filter(e => e.length);

        //    var td=document.getElementsByClassName('TimeSheet-cell-selected');
        // console.log('dd',td);
        //   $.each(td,function (key,value){
        //     console.log('key',value)
        //   });
        // changeText();
        //   function changeText() {
        //     $("#cal td").each(function (key,value) {
        //         alert('kk');
        //         for (var i = 0; i < $(this).children.length; i++) {
        //             alert($(this).children(i).val());
        //         }
        //         // alert($(this).html());
        //         // $(this).text("hello");
        //         // alert($(this).html());
        //     });
        // }

        (rows > 0) ? rows=rows : rows=1;
        (unit_det.length >= 1) ? unit_det=unit_det : unit_det=[{name:"No Unit Available"}];

        console.log('dim',month);
        console.log('unit_det',year);
        var dimensions = [rows,len];
        var dayList = unit_det;
        var hourList = arr2;
       // console.log(hourList);
        var sheetData = markers;

            var updateRemark = function(sheet){
            var sheetStates = sheet.getSheetStates();
            var rowsCount = dimensions[0];
            var colsCount = dimensions[1];
            var rowRemark = [];
            var rowRemarkLen = 0;
            var remarkHTML = '';

            for(var row= 0, rowStates=[]; row<rowsCount; ++row){
                rowRemark = [];
                rowStates = sheetStates[row];
                for(var col=0; col<colsCount; ++col){
                    if(rowStates[col]===0 && rowStates[col-1]===1){
                        rowRemark[rowRemarkLen-1] += (col<=10?'0':'')+col+':00';
                    }else if(rowStates[col]===1 && (rowStates[col-1]===0 || rowStates[col-1]===undefined)){
                        rowRemarkLen = rowRemark.push((col<=10?'0':'')+col+':00-');
                    }
                    if(rowStates[col]===1 && col===colsCount-1){
                        rowRemark[rowRemarkLen-1] += '00:00';
                    }
                }
                // remarkHTML = rowRemark.join("，");
                // sheet.setRemark(row,remarkHTML==='' ? sheet.getDefaultRemark() : remarkHTML);
            }
        };
        var sheet = $("#J_timedSheet").TimeSheet({
            data: {
                dimensions : dimensions,
                colHead : hourList,
                rowHead : dayList,
                sheetHead : {name:"Property : Building: Unit (Rent Per Day)"},
                sheetData : sheetData,
                month:month,
                year:year
            },
            // remarks : {
            //     title : "Description",
            //     default : "N/A"
            // },
            end : function(ev,selectedArea){
                updateRemark(sheet);
            },

            // sheetClass: "",
            // start: false,


            getCellState : function(cellIndex){
                return sheetModel.getCellState(cellIndex);
            },

            getRowStates : function(row){
                return sheetModel.getRowStates(row);
            },

            getSheetStates : function(){
                return sheetModel.getSheetStates();
            },

            setRemark : function(row,html){
                if($.trim(html)!==''){
                    $(thisSheet.find(".TimeSheet-row")[row]).find(".TimeSheet-remark").prop("title",html).html(html);
                }
            },

            clean : function(){
                sheetModel.set(0,{});
                repaintSheet();
                cleanRemark();
            },

            getDefaultRemark : function(){
                return sheetOption.remarks.default;
            },

            disable : function(){
                thisSheet.undelegate(".umsSheetEvent");
            },

            enable : function(){
                eventBinding();
            },

        });
       // sheet.clean();
        updateRemark(sheet);

        $("#J_timingDisable").click(function(ev){
            sheet.disable();
        });

        $("#J_timingEnable").click(function(ev){
            sheet.enable();
        });

        $("#J_timingClean").click(function(ev){
            sheet.clean();
        });

        $("#J_timingSubmit").click(function(ev){

            var sheetStates = sheet.getSheetStates();
            var rowsCount = dimensions[0];
            var $submitDataDisplay = $("#J_dataDisplay") ;

            $submitDataDisplay.html("<b>Raw Data Submitted:</b><br/>[<br/>");

            for(var row= 0, rowStates=[]; row<rowsCount; ++row){
                rowStates = sheetStates[row];
                $submitDataDisplay.append('&nbsp;&nbsp;[ '+rowStates+' ]'+(row==rowsCount-1?'':',')+'<br/>');
            }

            $submitDataDisplay.append(']');
        });

        $("#J_timingIsFull").click(function(ev){
            alert(sheet.isFull());
        });

        $("#J_timingGetCell").click(function(ev){

            var cellIndex = $("#J_cellIndex").val().split(',');
            var cellData = sheet.getCellState(cellIndex);
            var $dataDisplay = $("#J_dataDisplay") ;
            $dataDisplay.html("<b>Cell Data At ["+cellIndex+"] : </b>"+cellData);
        });

        $("#J_timingGetRow").click(function(ev){
            var rowIndex = $("#J_rowIndex").val();
            var rowData = sheet.getRowStates(rowIndex);
            var $dataDisplay = $("#J_dataDisplay") ;
            $dataDisplay.html("<b>Row Data At "+rowIndex+" : </b>[ "+rowData+" ]");
        });


    };


    $('.img-calendar').click(function () {
        name_show();
    });

    function name_show() {
        setTimeout(function () {
            var ff=$("#date_dynamic").val();
            ff=new Date(ff);
            var mm = ff.getMonth() + 1;
            ff.setDate(1);
            var all_days = [];
            while ((ff.getMonth() +1) == mm) {
                var d = ff.getFullYear() + '-' + mm .toString().padStart(2, '0') + '-' + ff.getDate().toString().padStart(2, '0');
                all_days.push(d);
                ff.setDate(ff.getDate() + 1);
            }
            var groupnames = [];
            $.each(grouped_data, function (key, value) {
                //var gdata = [];
                var gdate = [];
                var gname  = [];
                $.each(value, function (key1, value1) {
                    var aa = (value1.name).split(" ");
                    aa=aa[0].substr(0,1);
                    var bb=value1.name.substring(value1.name.indexOf(" ") + 1);
                    var name=aa+"."+bb;
                    console.log('value1.name',name);
                    gdate.push(value1.date);
                    gname.push({'date':value1.date,'name':name});
                });
                groupnames.push({'key': key, 'value': gname,'dates':gdate});
            });
           // console.log('comb_arr',comb_arr);
           // console.log('nn',groupnames);
            // var data_unit=[];
            // $("tbody").find("tr").each(function () {
            //     $(this).find('td.TimeSheet-cell-selected').each(function (key,value) {
            //         var uni=$(this).attr('data-unit');
            //         data_unit.push(uni);
            //     })
            // });

            $.each(unit_noo,function (key,value){
                value=value.substring(value.indexOf('-') + 1);
                $.each(groupnames,function (key1,value1){
                    if(value==value1.key){
                        $.each(all_days,function (k1,v1){
                        if((value1.dates).includes(v1)){
                        $.each(value1.value,function (k,v){
                            var day =  new Date(v.date);
                            var tmonth = day.getMonth()+1;
                            var tyear = day.getFullYear();
                                day = day.getDate()-1;
                            var element = $("#"+value+' td');
                            var tdMonth = $("#"+value).find('td:first-child').attr('data-month');
                            var tdYear = $("#"+value).find('td:first-child').attr('data-year');
                            if(tmonth == tdMonth && tyear == tdYear) {
                                $.each(element, function (ke, va) {
                                    var data_id = $(va).attr('data-col');
                                    if (data_id == day) {
                                        $(va).text(v.name);
                                    }
                                })
                            }
                        })
                      }
                   });
                 }
              })
           })

            // $.each(unit_noo,function (key1,value1) {
            //     console.log('u_no2',unit_noo);
            //     value1=value1.substring(value1.indexOf('-') + 1);
            //         console.log('comp',value1,data_unit);
            //         if(data_unit.includes(value1)){
            //             $.each(groupnames, function (key2, value2) {
            //                 console.log('comp_g_names2',value2,value1);
            //                 if(value2.key.includes(value1)){
            //                     console.log('ff',value2);
            //                     console.log('gg',value2.value.length);
            //                     $.each(value2.value,function (k,v) {
            //                         console.log('hhhhh',$("#"+value1),v);
            //                         $("#"+value1).find('td.TimeSheet-cell-selected').text(v);
            //
            //                     });
            //                 }
            //             })
            //         }
            //
            // });

        },100);
    }
    $(document).on('click','.background_check_open',function () {
        $('#short_term').trigger( 'reloadGrid' );
        $("#backGroundCheckPop").modal('hide');
    });
});
