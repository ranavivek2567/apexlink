$(document).ready(function(){

getMaintenanceFiles();



var ticket_id = $(".ticket_id").val();
var tenant_id = $(".tenant_id").val();
$.ajax({
    url:'/tenantPortal?action=getMaintenanceInfo&class=TenantPortal&tenant_id='+tenant_id+"&ticket_id="+ticket_id,
    type: 'GET',
    success: function (data) {
       
       var tenantInfo =  JSON.parse(data);
      
     
     
       $.each(tenantInfo, function (key, value) {
           if(value!="")
                  {
      
         $('.' + key).val(value);   
        

                  }
                 });


setTimeout(function(){

$(".property").val(tenantInfo.property_id);
var triggerProerty = $(".property").trigger('change');
setTimeout(function(){
$(".unit").val(tenantInfo.unit_id);
$(".manager").val(tenantInfo.assign);
},500)



},500);
 
       $(".ticket_image1").html(tenantInfo.ticket_image1);
       $(".ticket_image2").html(tenantInfo.ticket_image2);
       $(".ticket_image3").html(tenantInfo.ticket_image3);


       editTicket();

    },
});









$(document).on("click",".udpateticketss",function(){
    $(".udpatetickets").trigger("click");
});


$(document).on("click",".cancelTicketss",function(){
    $(".cancelTicket").trigger("click");
});


$(document).on("click",".saveMaintenanceFile",function(){
    var tenant_id = $('.tenant_id').val();
    var form = $('#addMaintenanceFiles')[0];
    var formData = new FormData(form);
    formData.append('action','insertMaintenanceFile');
    formData.append('class','TenantPortal');
    formData.append('tenant_id',tenant_id);
      
    $.ajax({
        url:'/tenantPortal',
        type: 'POST',
        data: formData,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == "success"){
                toastr.success(response.message);
              setTimeout(function(){
                    $('#maintenanceFiles-table').trigger('reloadGrid');
                    $("#file_library").val('');
                    $(".img-upload-library-div").html('');
                   
                }, 400);

               
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });

});








  function getMaintenanceFiles(status) {

 var tenant_id = $('.tenant_id').val();

    var table = 'tenant_chargefiles';
    var columns = ['Id', 'Name', 'View','File_location','File_extension','type','Action'];
    var select_column = ['Edit','Delete','Send'];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_where = [{column:'user_id', value :tenant_id,condition:'='},{column:'type', value :'T',condition:'='}];
    var extra_columns = [];
    var joins = [];
    var columns_options = [
        {name:'Id',index:'id', width:300,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Name',index:'filename', width:300,searchoptions: {sopt: conditions},table:table},
        {name:'View',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:fileFormatter},
        {name:'File_location',index:'file_location', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'File_extension',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'type',index:'file_extension', width:100,hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'file_type', width:300,searchoptions: {sopt: conditions},table:table,formatter:selectMaintanenceFormatter},
    ];


    var ignore_array = [];
    jQuery("#maintenanceFiles-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_where:extra_where,
            extra_columns:extra_columns,
            deleted_at:'true'
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: '5',
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "File Library",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}



$(document).on("change",".maintenancefiles_main table .select_options",function(){
var action = $(this).val();
var id = $(this).attr('data-id');

    if(action=="Delete")
   {
        bootbox.confirm({
           message: "Do you want to delete this record ?",
           buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
           callback: function (result) {
               if (result == true) {
                 deleteTableData('tenant_chargefiles',id);
                   setTimeout(function(){
                       $('#maintenanceFiles-table').trigger('reloadGrid');

                   }, 400);
              
               }
             
           }
       });
   

   }
 
});




function selectMaintanenceFormatter(cellValue, options, rowObject)
{

   if(rowObject !== undefined) {
        var file_type =  rowObject.View;
        var location = rowObject.File_location;
        var path = upload_url+'company/'+location;

       var imageData = '';
       if(file_type == '1'){
            
            imageData = '<a href="'+path+'"><img width=200 height=200 src="'+path+'"></a>';
       } else {
           if (rowObject.File_extension == 'xlsx') {
               src = upload_url + 'company/images/excel.png';
               imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
           } else if (rowObject.File_extension == 'pdf') {
               src = upload_url + 'company/images/pdf.png';
               imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
           } else if (rowObject.File_extension == 'docx') {
               src = upload_url + 'company/images/word_doc_icon.jpg';
               imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
           }else if (rowObject.File_extension == 'txt') {
               src = upload_url + 'company/images/notepad.jpg';
                imageData = '<a href="'+path+'"><img class="img-upload-tab" width=100 height=100 src="' + src + '"></a>';
           }
        }


  var html = "<select editable='1' class='form-control select_options' data-id='"+rowObject.Id+"' data-path = '"+path+"' data-src= '"+src+"'>"

   html +=  "<option value=''>Select</option>";
   html +=  "<option value='Delete'>Delete</option>";
  

   html +="</select>";

    

        



   return html;


      
   }
}






function editTicket(){

$("#editTicket").validate({
    rules: {
         category: {
            required:true
        }, 
        property_id: {
            required:true
        }, 
         unit_id: {
            required:true
        },      
     },
    submitHandler: function (e) {
        var tenant_id = $(".tenant_id").val();
        var ticket_id = $(".ticket_id").val();
       // alert(ticket_id);
        var form = $('#editTicket')[0];
        var formData = new FormData(form);
        
         var ticket_image1 = $('.ticket_image1').html();
         var ticketImage1 = JSON.stringify(ticket_image1);


        var ticket_image2 = $('.ticket_image2').html();
        var ticketImage2 = JSON.stringify(ticket_image2);


        var ticket_image3 = $('.ticket_image3').html();
        var ticketImage3 = JSON.stringify(ticket_image3);


        formData.append('action','editTicket');
        formData.append('class','TenantPortal');
        
        formData.append('ticket_image1',ticketImage1);
        formData.append('ticket_image2',ticketImage2);
        formData.append('ticket_image3',ticketImage3);
        formData.append('tenant_id',tenant_id);
        formData.append('ticket_id',ticket_id);
       

         $.ajax({
            url:'/tenantPortal',
            type: 'POST',
            data: formData,
            success: function (response) {

                 var info =  JSON.parse(response);
                
                 if(info.status=="success")
                 {
                   toastr.success('Ticket Updated Successfully');
                   window.location.href = window.location.origin+'/TenantPortal/maintenance?tenant_id='+tenant_id;
                  
                 }     
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});

}



});
