$(document).ready(function () {
    var randomString = randomString1(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');

    $(document).on("click",".NewportfolioPopup",function(){
        $("#NewportfolioPopup").show();
        var rString = randomString1(6, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
        $("#NewportfolioPopup #portfolio_id").val(rString);
    });
    $(document).on("click",".add-popup .grey-btn",function(){
        var box = $(this).closest('.add-popup');
        $(box).hide();
    });
    $(document).on("click","#NewportfolioPopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewportfolioPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-portfolio-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addPortfolioPopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidatePortfoio").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewportfolioPopup').hide(500);
                    toastr.success(response.message);
                    $("#portfolio_name").val('');
                    console.log("test");
                    fetchAllPortfolio(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Portfolio already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click","#Newmanager",function(){
        $("#NewmanagerPopup").show();
    });
    $(document).on("click","#NewmanagerPopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewmanagerPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-manager-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addManagerPopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidateManager").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {

                    $('#dynamic_manager').html('');
                    var optionHtml = '<select name="manager_id[]" class="form-control select_manager" multiple="multiple" id="select_mangers_options">';
                    $.each(response.getdata, function(key, value) {
                        if (response.selected == value.id) {
                            var selected = 'selected="selected"';
                        } else {
                            var selected = '';
                        }
                        if (value.last_name) {
                            optionHtml += '<option ' + selected + ' value="' + value.id + '">' + value.first_name + ' ' + value.last_name + '</option>';
                        } else {
                            optionHtml += '<option ' + selected + ' value="' + value.id + '">' + value.name + '</option>';
                        }

                    });
                    $('#selectProperty #dynamic_manager').html(optionHtml);
                    $('.select_manager').multiselect({
                        includeSelectAllOption: true,
                        nonSelectedText: 'Manager Name'
                    });
                    $('#NewmanagerPopup').hide(500);
                    toastr.success(response.message);

                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Email already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".NewattachPopup",function(){
        $("#NewattachPopup").show();
    });
    $(document).on('click', '#selectProperty #NewattachPopupSave', function (e) {

        e.preventDefault();

        var formData = $('#NewattachPopup :input').serializeArray();

        $.ajax({
            type: 'post',
            url: '/add-attachgroup-popup',
            data: {form: formData,
                class: 'propertyDetail',
                action: 'addAttachgroupPopup'},
            beforeSend: function (xhr) {
                // checking portfolio validations
                $(".customValidateGroup1").each(function () {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#dynamic_groups').html('');
                    var optionHtml = '<select name="attach_groups" class="form-control attach_groups" multiple>';
                    $.each(response.getdata, function (key, value) {
                        if (response.selected == value.id) {
                            var selected = 'selected="selected"';
                        } else {
                            var selected = '';
                        }
                        optionHtml += '<option ' + selected + ' value="' + value.id + '">' + value.group_name + '</option>';

                    });
                    $('#dynamic_groups').html(optionHtml);
                    $('.attach_groups').multiselect({includeSelectAllOption: true, nonSelectedText: 'Property Groups'});
                    $('#NewattachPopup').hide(500);
                    toastr.success(response.message);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Property Group already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }
            },
            error: function (response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".property_type_options",function(){
        $("#NewtypePopup").show();
    });
    $(document).on("click","#NewtypePopupSave",function(e){
        e.preventDefault();
        var propType = $("#NewtypePopup input[name='@property_type']").val();
        console.log("propType"+propType);
        var formData = $('#NewtypePopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-propertytype-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addPropertytpePopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidatePropertyType").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewtypePopup').hide(500);
                    toastr.success(response.message);
                    var lastRecord = "<option value='"+response.lastid+"'>"+propType+"</option>";
                    fetchAllPropertytype(response.lastid);
                    $("#property_type").append(lastRecord);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Property Type already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $(document).on("click",".Newprofile",function(){
        $("#NewpstylePopup").show();
    });
    $(document).on("click","#NewpstylePopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewpstylePopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-propertystyle-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addPropertystylePopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidatePropertyStyle").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewpstylePopup').hide(500);
                    toastr.success(response.message);
                    fetchAllPropertystyle(response.lastid);

                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Property Style already exists!') {
                        bootbox.alert(response.message);
                        return false;
                    }

                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    $(document).on("click",".Newsubtype",function(){
        $("#NewsubtypePopup").show();
    });
    $(document).on("click","#NewsubtypePopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewsubtypePopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-propertysubtype-popup',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addPropertysubtypePopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidatePropertySubType").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewsubtypePopup').hide(500);
                    toastr.success(response.message);
                    fetchAllPropertysubtype(response.lastid);

                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Property Sub-Type already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".Newamenities",function(){
        $("#NewamenitiesPopup").show();
    });
    $(document).on("click","#NewamenitiesPopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewamenitiesPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/add-amenity-popup',
            data: {
                form: formData,
                class: 'commonPopup',
                action: 'addAmenityPopup'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidateAmenities").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewamenitiesPopup').hide(500);
                    toastr.success(response.message);
                    var lastid = response.lastid;
                    fetchAllAmenities(response.lastid)
                    //$("#"+lastid).attr('checked',true);
                    setTimeout(function() {
                        $("#" + lastid).attr("checked", true)
                    }, 1000);
                } else if (response.status == 'error' && response.code == 500) {
                    toastr.error(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".AddNewUnitTypeModalPlus2",function(){
        $("#NewunitPopup").show();
    });
    $(document).on("click","#NewunitPopupSave",function(e){
        e.preventDefault();
        var formData = $('#NewunitPopup :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addUnittype'
            },
            beforeSend: function(xhr) {
                // checking portfolio validations
                $(".customValidateUnitType").each(function() {
                    var res = validations(this);
                    if (res === false) {
                        xhr.abort();
                        return false;
                    }
                });
            },
            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#NewunitPopup').hide(500);
                    toastr.success(response.message);
                    fetchAllUnittypes(response.lastid);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Portfolio already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click","#newunittypesave",function(e){
       // e.preventDefault();
        var formData = $('#AddNewUnitData :input').serializeArray();
        $.ajax({
            type: 'post',
            url: '/property-ajax',
            data: {
                form: formData,
                class: 'propertyDetail',
                action: 'addUnittype'
            },

            success: function(response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#AddNewUnitModal').hide(500);
                    toastr.success(response.message);
                   setTimeout(function () {
                       fetchAllUnittypes(response.lastid);
                   },1000);
                } else if (response.status == 'error' && response.code == 500) {
                    if (response.message == 'Portfolio already exists!')
                        bootbox.alert(response.message);
                    return false;
                    $('.error').html('');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text(value);
                    });
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.warning(response.message);
                }

                onTop(true);
            },
            error: function(response) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function(key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });

    $(document).on("click",".propreerralicon",function(){
        //$(this).parent().next().next().show();
        $("#selectPropertyReferralResource1").show();
    });

    $(document).on("click",".add_single1", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var val = $(this).parent().prev().find("."+className).val();
        if (val == ""){
            $(this).parent().prev().find("."+className).next().text("Enter the value");
            return false;
        }else{
            $(this).parent().prev().find("."+className).next().text("");
        }
        savePopUpData(tableName, colName, val, selectName);
        var val = $(this).parent().prev().find("input[type='text']").val('');
    });

    $(document).on("click",".add_single3", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var val = $(this).parent().prev().find("."+className).val();
        if (val == ""){
            $(this).parent().prev().find("."+className).next().text("Enter the value");
            return false;
        }else{
            $(this).parent().prev().find("."+className).next().text("");
        }
        savePopUpData3(tableName, colName, val, selectName);
        var val = $(this).parent().prev().find("input[type='text']").val('');
    });

    $(document).on("click",".selectPropertyEthnicity",function(){
        $("#selectPropertyEthnicity1").show();
    });

    $(document).on("click",".selectPropertyHobbies",function(){
        $("#selectPropertyHobbies1").show();
    });

    $(document).on("click",".selectPropertyMaritalStatus",function(){
        $("#selectPropertyMaritalStatus1").show();
    });

    $(document).on("click",".selectPropertyVeteranStatus",function(){
        $("#selectPropertyVeteranStatus1").show();
    });

    $(document).on("click",".selectPropertyAnimalSex",function(){
        $(this).parent().next().next().show();
        //$("#selectPropertyAnimalSex1").show();
    });

    $(document).on("click",".selectPropertyPetSex",function(){
        $(this).parent().next().next().show();
        //$("#selectPropertyPetSex1").show();
    });


    $(document).on("click",".collectionreason",function(){
        $("#collectionreason1").show();
    });

    $(document).on("click",".additionalVeteranStatus",function(){
        $("#additionalVeteranStatus1").show();
    });

    $(document).on("click",".tenantCredentialType",function(){
        $("#tenantCredentialType1").show();
    });

    $(document).on("click",".additionalEthnicity",function(){
        $("#additionalEthnicity1").show();
    });

    $(document).on("click",".additionalMaritalStatus",function(){
        $("#additionalMaritalStatus1").show();
    });

    $(document).on("click",".additionalHobbies",function(){
        $("#additionalHobbies1").show();
    });

    $(document).on("click",".insertHoaType",function(){
        $("#selectPropertyHOAType").show();
    });

    $(document).on("click",".additionalReferralResource",function(){
        $("#additionalReferralResource1").show();
    });

    $(document).on("click",".companintPopupIcon",function(){
        $("#selectcompanintPopupIcon").show();
    });

    $(document).on("click",".eccupantPropertyEthnicity1",function(){
        $("#eccupantPropertyEthnicity1").show();
    });

    $(document).on("click",".occupantPropertyMaritalStatus1",function(){
        $("#occupantPropertyMaritalStatus1").show();
    });

    $(document).on("click",".occupantselectPropertyHobbies1",function(){
        $("#occupantselectPropertyHobbies1").show();
    });

    $(document).on("click",".occupantPropertyVeteranStatus1",function(){
        $("#occupantPropertyVeteranStatus1").show();
    });

    $(document).on("click",".occupantadditionalReferralResource1",function(){
        $("#occupantadditionalReferralResource1").show();
    });

    $(document).on("change","#add_contact #Salutation_modal", function () {
        var value = $(this).val();
        var insValue = "0";
        if (value == "Mr." || value == "Sir" || value == "Brother" || value == "Father"){
            insValue = "1";
        }else if(value == "Mrs." || value == "Ms." || value == "Madam" || value == "Sister" ||value == "Mother"){
            insValue = "2";
        }else{
            insValue = "0";
        }
        $("#add_contact #gender_modal").val(insValue);
    });

    getZipCode('#add_contact #zip_modal','90001','#add_contact #city_modal','#add_contact #State_modal','#add_contact #Country_modal','','');
    $("#add_contact #zip_modal").focusout(function () {
        getZipCode('#add_contact #zip_modal',$(this).val(),'#add_contact #city_modal','#add_contact #State_modal','#add_contact #Country_modal','','');
    });

});

function  savePopUpData(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/savePopUpData',
        data: {
            class: "TenantAjax",
            action: "savePopUpData",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                if (selectName == "hobbies[]" || selectName == "additional_hobbies[]" || selectName == "guestHobbies[]"){
                    $("select[name='"+selectName+"']").multiselect('destroy');
                    $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                    if(selectName == "guestHobbies[]"){
                        $("#selectPropertyHobbies1").hide();
                    }else{
                        $("select[name='"+selectName+"']").parent().parent().next().hide();
                    }
                }else{
                    $("select[name='"+selectName+"']").next().hide();
                }
                toastr.success(data.message);

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function  savePopUpData3(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/savePopUpData3',
        data: {
            class: "TenantAjax",
            action: "savePopUpData3",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                if (selectName == "hobbies[]" || selectName == "additional_hobbies[]" || selectName == "guestHobbies[]"){
                    $("select[name='"+selectName+"']").multiselect('destroy');
                    $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                    $("select[name='"+selectName+"']").parent().parent().next().hide();
                }else{
                    $("select[name='"+selectName+"']").next().hide();
                }
                toastr.success(data.message);

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function fetchAllUnittypes(id) {
    var propertyEditid = $("#property_editunique_id").val();

    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchAllUnittypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#unit_type').html(res.data);
            if (id != false) {
                $('#unit_type').val(id);

            }
            $("#default_rent").val(response.default_rent);

        },
    });
}

function fetchAllAmenities(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-all-amenities',
        data: {
            class: 'commonPopup',
            action: 'fetchAllAmenities'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#amenties_box').html(res.data);
            $('#amenties_box1').html(res.data);
        },
    });

}


function randomString1(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function validateCustomField(data_value, data_required, arg, element, id) {
    var msg = '';
    if (data_value == "") {
        if (data_required == 1) {
            msg = 'This field is required';
            $('#' + id).after("<span>" + msg + "</span>");
            return false;
        }
    }
    if (arg == 'text') {
        if (data_value.length > 150) {
            msg = 'Please enter character less than 150';
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'number') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 20) {
                msg = 'Please enter less than 20';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'currency') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        }

    } else if (arg == 'percentage') {
        if (isNaN(data_value)) {
            msg = 'Only number are allowed!';
            $('#' + id).next('.customError').text(msg);
            return false;
        } else {
            if (data_value > 100) {
                msg = 'Maximum value is 100';
                $('#' + id).next('.customError').text(msg);
                return false;
            }
        }
    } else if (arg == 'url') {
        var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
        var regex = new RegExp(expression);
        if (data_value.match(regex)) {
        } else {
            msg = 'Please enter a valid URL';
            $('#' + id).next('.customError').text(msg);
            return false;
        }
    }
    $('#' + id).next('.customError').text('');
    return true;
}
function fetchAllPortfolio(id) {

    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-portfolioname',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPortfolioname'},
        success: function (response) {
            var res = JSON.parse(response);
            console.log('portfolio',res.data);
            $('select[name="portfolio_id"]').html(res.data);
            if (id != false) {
                $('select[name="portfolio_id"]').val(id);

            }

        },
    });

}
function fetchAllPropertytype(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertytype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertytype'},
        success: function (response) {
            var res = JSON.parse(response);
            $('select[name="property_type"]').html(res.data);
            if (id != false) {
                $('select[name="property_type"]').val(id);
            }

        },
    });

}
function fetchAllPropertystyle(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertytype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertystyle'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#property_style_options').html(res.data);
            if (id != false) {
                $('#property_style_options').val(id);
            }
        },
    });

}
function fetchAllPropertysubtype(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-propertysubtype',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchPropertysubtype'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#property_subtype_options').html(res.data);
            if (id != false) {
                $('#property_subtype_options').val(id);
            }

        },
    });

}

function fetchAllgarage(id) {

    $.ajax({
        type: 'post',
        url: '/fetch-garagedata',
        data: {
            class: 'propertyDetail',
            action: 'fetchGaragedata'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#garage_options').html(res.data);
            if (id != false) {
                $('#garage_options').val(id);
            }

        },
    });

}
function fetchAllManagers(id) {

    $.ajax({
        type: 'post',
        url: '/fetch-managers',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllmanagers'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#select_mangers_options').html(res.data);
            $('#select_mangers_options').multiselect({
                columns: 1,
                placeholder: 'Select',
                selectAll: true
            });

            $('#select_mangers_options').change(function () {
//               alert('ddf');
            });


        },
    });

}
function fetchAllinsurancetypes(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-insurancetype',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllinsurancetypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#SelectInsuranceTypes').html(res.data);
            $('#SelectInsuranceTypes').val(id);
        },
    });
}

function fetchAllpolicytypes(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-policytype',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllpolicytypes'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#SelectPolicyTypes').html(res.data);
            $('#SelectPolicyTypes').val(id);
        },
    });
}
function fetchAllAttachgroups(id) {

    $.ajax({
        type: 'post',
        url: '/fetch-attachgroup',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllAttachgroups'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#select_group_options').html(res.data);
            $('#select_group_options').multiselect({
                columns: 1,
                placeholder: 'Select',
                selectAll: true});
        },
    });

}


//vendor values on owner select
function vendorValues(){
    var selectedowner = $(".ownerSelectclass option:selected").text();
    var count = $(".ownerSelectclass option:selected").length;
//         $(".vendor_1099_payer").val(selectedowner);
    var data = '';
    $('#vendor_id').val('');
    $(".ownerSelectclass").each(function () {
        if ($('option:selected', this).text() != 'Select') {
            if (data == '') {
                data += $('option:selected', this).text();
            } else {
                data += ", " + $('option:selected', this).text();
            }
        }
    });
    $('#vendor_id').val(data);



}
//fetch all owners
function getAllOwners() {
    var property_id = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/fetch-owners',
        data: {
            class: 'propertyDetail',
            action: 'fetchOwners',
            property_id: property_id,
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.ownerSelectclass').html(res.data);
        },
    });
}
function fetchAllFixture(id) {
    $.ajax({
        type: 'post',
        url: '/fetch-all-fixture',
        data: {
            class: 'propertyDetail',
            action: 'fetchAllfixture'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#fixture_type').html(res.data);
            if (id != false) {
                $('#fixture_type').val(id);
            }

        },
    });
}

function fetchAllReason(id) {
    var propertyEditid = $("#property_editunique_id").val();
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            propertyEditid: propertyEditid,
            class: 'propertyDetail',
            action: 'fetchAllReason'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#selectreasondiv').html(res.data);
            if (id != false) {
                $('#selectreasondiv').val(id);
            }

        },
    });
}
function getcurrentdatetime() {
    $.ajax({
        type: 'post',
        url: '/property-ajax',
        data: {
            class: 'propertyDetail',
            action: 'getcurrentdatetime'},
        success: function (response) {
            var response = JSON.parse(response);
            $('.last_renovation').val(response.timeZone);
            $(".last_renovation_time").val(response.renovationtime);
            $("#pick_up_date").val(response.timeZone);
            $("#pick_up_time").val(response.timeZone);
            $("#key_designator").val(default_name);
//            $("#management_start_date").val(response.timeZone);
            $("#loan_start_date").val(response.timeZone);
        }


    });
}
function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}