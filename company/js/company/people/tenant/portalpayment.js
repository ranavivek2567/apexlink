var formData22;
$(document).on('mouseover','.tooltipgridclass',function(){
    $(this).closest('td').css("overflow", "unset");
});
$(document).on('mouseout','.tooltipgridclass',function(){
    $(this).closest('td').css("overflow", "hidden");
});



$(document).ready(function() {
    /*payment process jquery starts here */
    $("#addCards").validate({
        rules: {
            card_number: {
                required:true,
                number:true,
                maxlength: 19,
                minlength: 9
            },
            exp_date: {
                required: true,
            },
            cvc: {
                required: true,
                number:true
            },
            holder_name: {
                required: true,
            },
        },
        submitHandler: function (e) {
            var tenant_id = $("#company_user_id").val();
            var form = $('#addCards')[0];
            var formData = new FormData(form);
            formData.append('action','addTenantCardDetails');
            formData.append('class','Stripe');
            formData.append('tenant_id',tenant_id);
            action = 'addAdditionalTenant';
            $.ajax({
                url:'/stripeCheckout',
                type: 'POST',
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status=="true")
                    {
                        toastr.success(response.message);
                        $(".card_number").val('');
                        $(".cvc").val('');
                        $("#addCards").hide();
                        
                        cardLists(tenant_id);
                        bankLists(tenant_id);
                    }
                    else
                    {
                        toastr.error(response.message);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });

    $('.exp_date').datepicker({
        yearRange: '2019:2040',
        changeMonth: true,
        changeYear: true,
        dateFormat: 'mm/yy'
    });

    $(document).on("change",".setType",function(){
        var value = $(this).val();
        if(value=='1')
        {
            $(".accounts").hide();
            $(".cards").show();
        }
        else
        {
            $(".accounts").show();
            $(".cards").hide();
        }

    });

    /*payment process jquery ends here */


    setTimeout(function () {
        if(localStorage.getItem('property_id') != null){
            $('#jqgridOptions').val('1');
            var grid = $("#tenant_listing"),f = [];
            f.push({field:"tenant_property.property_id",op:"eq",data:localStorage.getItem('property_id')},{field:"tenant_details.status",op:"eq",data:'1'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('property_id');
        }
    }, 500);

    /*search Move Out On quickNav Start*/
    $(document).on("click","#myInput",function (e){
        getTenantMoveOutDetails();
    });

    $(document).on("click",".modalhidevicting",function (e){
        $("#backGroundCheckPop").modal('hide');
    });


   

    $(document).on("keyup","#myInput",function(event){
        var keyString = $(this).val();
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantMoveOutSearch",
                keyValue: keyString
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var html = "";
                    if(response.data !== ''){
                        html += '<input type="hidden" id="search_id" value="">\n'+
                            '<table class="table table-hover table-dark" id="myTable"><thead>\n' +
                            '<tr class="header">\n' +
                            '<th>Tenant Name</th>\n' +
                            '<th>Phone</th>\n' +
                            '<th>Email</th>\n' +
                            '<th>propertyName</th>\n' +
                            '<th>Unit</th>\n' +
                            '<th>Rent()</th>\n' +
                            '<th>Balance()</th>\n' +
                            '</tr>\n' +
                            '</thead>\n' +
                            '<tbody>'+response.data+'</tbody>\n' +
                            '</table>';

                    }
                    $("#tenantData_search").html(html);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        keypressCodeSearch();
    });


    function keypressCodeSearch(){
        $(document).on("keypress","#myInput",function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {

                var id = $(".tenantIdget").val();
                var MoveOutTenantName = $('.tenantNameget').val();
                var path = window.location.origin;

                setTimeout(function () {
                    if(id == undefined){
                        return false;
                    }else{
                        $("#myInput").val(MoveOutTenantName);
                        window.location.href = path + '/Tenant/MoveOutTenant?id=' + id;
                    }
                }, 500);
                return false;
            }
        });
    }


    $(document).on("click","#new_chargeCode_form",function (e){
        $("#myTable").hide();
    });

    $(document).on("click", "#myTable tbody tr", function (e) {

        var id = $(this).children("input[type='hidden']").val();    // Find the row
        var base_url = window.location.origin;
        window.location.href = base_url + '/Tenant/MoveOutTenant?id='+id;

    })  ;

    if(localStorage.getItem('ElasticSearch')){
        var elasticSearchData = localStorage.getItem('ElasticSearch');
        setTimeout(function(){
            var grid = $("#tenant_listing"),f = [];
            f.push({field: "users.id", op: "eq", data: elasticSearchData,int:'true'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('ElasticSearch');
        },2000);
    }


    /*Harjinder search Move Out On quickNav End*/
    if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }

    if(localStorage.getItem("table_green_tableid")){
        setTimeout(function(){
            var tableId = localStorage.getItem("table_green_tableid")
            jQuery(tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery(tableId).find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('table_green_id');
            localStorage.removeItem('table_green_tableid');
        }, 2000);
    }

    if(localStorage.getItem("table_green_tableid")){
        setTimeout(function(){
            var tableId = localStorage.getItem("table_green_tableid")
            jQuery(tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery(tableId).find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('table_green_id');
            localStorage.removeItem('table_green_tableid');
        }, 2000);
    }

    $(document).on("change", ".tenant_type_status #jqgridOptions", function (e) {
        var statusTenantDetails = $(this).val();
        $('#tenant_listing').jqGrid('GridUnload');
        jqGrid('All',true,statusTenantDetails);

    });
    $(document).on('click','#AZ',function(){
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click','#allAlphabet',function(){
        var grid = $("#tenant_listing");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });



    $(document).on('click','#allAlphabet',function(){
        var grid = $("#tenant_listing");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });
    $(document).on('click','.getAlphabet',function(){
        var grid = $("#tenant_listing"),f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if(value != '0'){
            f.push({field:"name",op:"bw",data:search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    });




    
   $(document).on('click','#onlinePayment',function(){
   
            var id = $(".tenant_id").val();
                stripePaymentVariables(id);
                //fetchUserCardDetail(id);
                $("#company_user_id").val(id);
                getTenantInfo(id);
                //checkFirstPaymentStatus(id);
                checkPaymentMethod(id);
                //getRentAmount(id);
                getNewRentAmount(id);
                $(".select_options").val('default');
                $("#add-card").trigger('reset');
                cardLists(id);
                bankLists(id);
                $("#company_user_id").val(id);
                resetTenantModal();
                
   });


    $(document).on('change','.allselect',function () {
        console.log($(".allselect").is(":checked"));
        if($(".allselect").is(":checked")){
             
            var data='0,1,3';
        }else{
            var data='';

        }
        var dataarray=data.split(",");
        $("#payment_category").val(dataarray);
        $("#payment_category").multiselect("refresh");
    });

    function resetTenantModal(){


        $("#another_amoumt").hide(500);
        $("#another_amoumt").val('');
        $("#anotherAmountbutton").show();
        $("#fullAmountButton").hide();
        $("#paymentMethod").val("one-time-payment");
        $(".paymentIntervel").hide();
        $(".paymentIntervelcalander").hide();
        // Then refresh
        $("#payment_comment").val('');
        $("#paymentIntervel").val('weekly')
        $("#payment_category").multiselect("destroy");
        $('#payment_category').multiselect({
            nonSelectedText: 'Select Category',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth:'100%'
        });

        var data=data;
//Make an array
        var dataarray=data.split(",");
// Set the value
        $("#payment_category").val(dataarray);
// Then refresh
        $("#payment_category").multiselect("refresh");
        // $('input[type=checkbox][name=payment_terms]').prop("checked",false);
        // var   options = $("#payment_category").parent().find('input[type=checkbox]');
        // options.each(function () {
        //         $(this).prop('checked',false)
        // });
        //
        // $(".multiselect-selected-text").html("Select Category");

    }



    function checkPaymentMethod(user_id)
    {

        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
                "action": 'checkPaymentMethod',
                "class": 'Stripe',
                "user_id":user_id

            },
            success: function (response) {
                var response = JSON.parse(response);
                var msg = "<a href='#' class='changePaymentMethod' data-type='"+response.type+"'>"+response.message+"</a>";

                    if(response.type=='update')
                    {
                    
                    $('#tenant-payment').modal('toggle');
                    var paymentMethod = $("#paymentMethod").val();
                    if(paymentMethod=="one-time-payment")
                    {
                        $(".paymentIntervel").hide();
                        $(".paymentIntervelcalander").hide();
                        $(".amountField").hide();
                        $(".countField").hide();
                        $("#anotherAmountbutton").show();
                        
                    }
                    
                    }
                    else
                    {
                     
                    $('#set-method').modal('toggle');
                    }


                    $(".stripeMsg").html(msg);



              }
        });

    }





    /*Move out Tenant listing Start Harjinder 08-08-19*/




    function checkFirstPaymentStatus(user_id)
    {

        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
                "action": 'checkFirstPaymentStatus',
                "class": 'Stripe',
                "user_id":user_id

            },
            success: function (response) {


                if(response==1)
                {

                    $(".firstPaymentStatus").val(1);
                    toastr.warning("You have already made your first payment, Next payment amount will be deducted automatically");
                }
                else
                {
                    $(".firstPaymentStatus").val(0);
                    $("#savefinancialCard").attr("disabled", false);
                }


            }
        });

    }


    function addPrintAddress(id){
        $.ajax({
            type: 'post',
            url: '/tenantTypeAjax',
            data: {
                class: "TenantTypeAjax",
                action: "getAllAddress",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var address1 = response.property.address1;
                var address2 = response.property.address2;
                var address3 = response.property.address3;
                var address4 = response.property.address4;
                var city = response.property.city;
                var state = response.property.state;
                var zipcode = response.property.zipcode;
                var unitnubmer = response.unit.unit_prefix+"-"+response.unit.unit_no;
                var name = response.name;
                var html1 = "";
                var newAddress = "";
                var newAddress2 = "";

                if(name != ""){
                    html1 += '<label>'+name+'</label>';
                }

                if(address1 != "" && address2 == "" && address3 == "" && address4 == ""){
                    newAddress += '<label>'+address1+'</label>';
                }
                if(address1 != "" && address2 != "" && address3 == "" && address4 == ""){
                    newAddress += '<label>'+address1+" "+address2+'</label>';
                }
                if(address1 != "" && address2 != "" && address3 != "" && address4 == ""){
                    newAddress += '<label>'+address1+" "+address2+" "+address3+'</label>';
                }
                if(address1 != "" && address2 != "" && address3 != "" && address4 != ""){
                    newAddress += '<label>'+address1+" "+address2+" "+address3+" "+address4+'</label>';
                }

                html1 += '<label>'+newAddress+'</label>';

                if(unitnubmer != ""){
                    html1 += '<label>Unit#: '+unitnubmer+'</label>';
                }
                if(city != "" && state == "" && zipcode == ""){
                    newAddress2 += '<label>'+city+'</label>';
                }
                if(city != "" && state != "" && zipcode == ""){
                    newAddress2 += '<label>'+city +' '+state +'</label>';
                }
                if(city != "" && state != "" && zipcode != ""){
                    newAddress2 += '<label>'+city +' '+state +', ' +zipcode+'</label>';
                }

                html1 += '<label>'+newAddress2+'</label>';
                /* ----------------- */
                var html2 = "";
                var newAddress3 = "";
                var newAddress4 = "";
                var comp = response.company;
                if(name != ""){
                    html2 += '<label>'+comp.name+'</label>';
                }

                if(comp.address1 != "" && comp.address2 == "" && comp.address3 == "" && comp.address4 == ""){
                    newAddress3 += '<label>'+comp.address1+'</label>';
                }
                if(comp.address1 != "" && comp.address2 != "" && comp.address3 == "" && comp.address4 == ""){
                    newAddress3 += '<label>'+comp.address1+" "+comp.address2+'</label>';
                }
                if(comp.address1 != "" && comp.address2 != "" && comp.address3 != "" && comp.address4 == ""){
                    newAddress3 += '<label>'+comp.address1+" "+comp.address2+" "+comp.address3+'</label>';
                }
                if(comp.address1 != "" && comp.address2 != "" && comp.address3 != "" && comp.address4 != ""){
                    newAddress3 += '<label>'+comp.address1+" "+comp.address2+" "+comp.address3+" "+comp.address4+'</label>';
                }

                html2 += '<label>'+newAddress3+'</label>';


                if(comp.city != "" && comp.state == "" && comp.zipcode == ""){
                    newAddress4 += '<label>'+comp.city+'</label>';
                }
                if(comp.city != "" && comp.state != "" && comp.zipcode == ""){
                    newAddress4 += '<label>'+comp.city +' '+comp.state +'</label>';
                }
                if(comp.city != "" && comp.state != "" && comp.zipcode != ""){
                    newAddress4 += '<label>'+comp.city +' '+comp.state +', ' +comp.zipcode+'</label>';
                }

                html2 += '<label>'+newAddress4+'</label>';

                setTimeout(function(){
                    $("#print_envelope .print_tenant_address").html(html1);
                    $("#print_envelope .print_company_address").html(html2);
                },1000);
            }
        });
    }


    function getTenantMoveOutDetails() {
        //var id = $("#wrapper .moveOutTenant_id").val();
        //alert(id);
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantMoveOutDetails",
                // id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var html1 = "";
                if (response.status == 'success' && response.code == 200) {
                    /*for(var i=0; i < response.data.length; i++){
                        $('#myTable tbody').html(response.data);
                    }*/
                    html1 += '<table class="table table-hover table-dark" id="myTable"><thead>\n' +
                        '                           <tr class="header">\n' +
                        '                            <th>Tenant Name</th>\n' +
                        '                        <th>Email</th>\n' +
                        '                        <th>Phone</th>\n' +
                        '                        <th>propertyName</th>\n' +
                        '                        <th>Unit</th>\n' +
                        '                        <th>Rent()</th>\n' +
                        '                        <th>Balance()</th>\n' +
                        '                        </tr>\n' +
                        '                        </thead>\n' +
                        '                        <tbody>'+response.data+'</tbody>\n' +
                        '                        </table>';


                    $("#tenantData_search").html(html1);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /*Move out Tenant listing End Harjinder 08-08-19*/

    /**
     * tenant transfer search(Manjit)
     */

    $(document).on("click","#myInputTransfer",function (e){
        getTenantTransferDetails();
    });

    $(document).on("keyup","#myInputTransfer",function(event){
        var keyString = $(this).val();
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantMoveOutSearch",
                keyValue: keyString
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var html = "";
                    if(response.data !== ''){
                        html += '<input type="hidden" id="transfer_search_id" value="">\n'+
                            '<table class="table table-hover table-dark" id="myTransferTable"><thead>\n' +
                            '                           <tr class="header">\n' +
                            '                            <th>Tenant Name</th>\n' +
                            '                        <th>Email</th>\n' +
                            '                        <th>Phone</th>\n' +
                            '                        <th>propertyName</th>\n' +
                            '                        <th>Unit</th>\n' +
                            '                        <th>Rent()</th>\n' +
                            '                        <th>Balance()</th>\n' +
                            '                        </tr>\n' +
                            '                        </thead>\n' +
                            '                        <tbody>'+response.data+'</tbody>\n' +
                            '                        </table>';

                    }
                    $("#transferData_search").html(html);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        keypressCodeSearch1();
    });


    function keypressCodeSearch1(){
        $(document).on("keypress","#myInputTransfer",function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {

                var id = $(".tenantIdget").val();
                var MoveOutTenantName = $('.tenantNameget').val();
                var path = window.location.origin;

                setTimeout(function () {
                    $("#myInputTransfer").val(MoveOutTenantName);
                    window.location.href = path + '/Tenant/TransferTenant?id=' + id;
                }, 500);
                return false;
            }
        });
    }


    $(document).on("click","#new_chargeCodeTransfer_form",function (e){
        $("#myTransferTable").hide();
    });

    $(document).on("click", "#myTransferTable tbody tr", function (e) {
        var id = $(this).children("input[type='hidden']").val();    // Find the row
        var base_url = window.location.origin;
        window.location.href = base_url + '/Tenant/TransferTenant?id=' + id;

    })  ;

    /* Manjit search Tenant Transfer On quickNav End */

    /**
     * tenant transfer search table listing(Manjit)
     */

    function getTenantTransferDetails() {
        //var id = $("#wrapper .moveOutTenant_id").val();
        //alert(id);
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantMoveOutDetails",
                // id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var html1 = "";
                if (response.status == 'success' && response.code == 200) {
                    html1 += '<table class="table table-hover table-dark" id="myTransferTable"><thead>\n' +
                        '                           <tr class="header">\n' +
                        '                            <th>Tenant Name</th>\n' +
                        '                        <th>Phone</th>\n' +
                        '                        <th>Email</th>\n' +
                        '                        <th>propertyName</th>\n' +
                        '                        <th>Unit</th>\n' +
                        '                        <th>Rent()</th>\n' +
                        '                        <th>Balance()</th>\n' +
                        '                        </tr>\n' +
                        '                        </thead>\n' +
                        '                        <tbody>'+response.data+'</tbody>\n' +
                        '                        </table>';


                    $("#transferData_search").html(html1);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /* Manjit Tenant Transfer search End */
});


$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {   $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.open('https://www.victig.com', '_blank');
    }
});







/*$("#add-card").validate({
    rules: {
        cfirst_name: {
            required:true
        },
        clast_name: {
            required:true
        },
        ccard_number: {
            required:true,
            number:true
        },
        cexpiry_year: {
            required:true
        },
        cexpiry_month: {
            required:true
        },
        ccvv: {
            required:true,
            number:true,
        },
        cphoneNumber: {
            required:true
        },
        caddress1: {
            required:true
        },
        ccity: {
            required:true
        },
        cstate: {
            required:true
        },
        czip_code: {
            required:true
        },
        ccountry: {
            required:true
        },
        cCompany:{
            required:true
        }
    },
    submitHandler: function (e) {
        var checkPaymentStatus = $(".firstPaymentStatus").val();
        if(checkPaymentStatus==1)
        {
          toastr.warning("You have already made your first payment, Next payment amount will be deducted automatically");
          return false;
        }
        $("#loadingmessage").show();
        var form = $('#add-card')[0];
        var formData = new FormData(form);
        formData.append('action','addTenantCardPayment');
        formData.append('class','Stripe');

        var paidAmount = $('.stripe_amount').val();
        formData.append('paid_amount',paidAmount);




        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status=='success')
                {
                    $("#loadingmessage").hide();
                    toastr.success(response.message);
                    $('#tenant-payment').modal('toggle');
                    $("#ccvv").val("");
                    $("#ccard_number").val("");
                    $("#caddress1").val("");
                    $("#caddress2").val("");
                    localStorage.setItem("Message",'Payment has been done successfully.');
                    localStorage.setItem('rowcolorTenant', 'rowcolorTenant');
                    window.location.reload();

                }
                else
                {
                    $("#loadingmessage").hide();
                    toastr.error(response.message);
                }

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});
*/





function getTenantInfo(id)
{
    $.ajax({
        type: 'post',
        url: '/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getTenantZipCode',
            id:id},
        success: function (response) {
            var info = JSON.parse(response);
            setTimeout(function () {
                $("#czip_code").val(info.zipcode);
                var name = "("+info.name+")";
                $(".userName").html(name);
                $(".holder_name").val(info.name);
                getZipCode('#czip_code', info.zipcode, '.zipcity', '.zipstate', '.zipcountry', null, null);
            },500);



        },
    });


}





$(document).on("blur","#czip_code",function(){
    var value = $(this).val();
    getZipCode('#czip_code', value, '.zipcity', '.zipstate', '.zipcountry', null, null);
});


function getZipCode(element,zipcode,city,state,country,county,validation){
    $(validation).val('1');
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {zip_code: zipcode,class: 'CommonAjax', action: 'getZipcode'},
        success: function (response) {
            var data = JSON.parse(response);
            if(data.code == 200){

                if(city !== undefined)$(city).val(data.data.city);
                if(state !== undefined)$(state).val(data.data.state);
                if(country !== undefined)$(country).val(data.data.country);
                if(county !== undefined)$(county).val(data.data.county);
            } else {
                getAddressInfoByZip(element,zipcode,city,state,country,county,validation);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}




function getAddressInfoByZip(element,zip,city,state,country,county,validation){
    var addr = {};
    addr.element = element;
    addr.elecity = city;
    addr.elestate = state;
    addr.elecountry = country;
    addr.elecounty = county;
    addr.validation = validation;
    if(zip.length >= 5 && typeof google != 'undefined'){
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    response(addr);
                } else {
                    addr.success = false;
                    response(addr);
                }
            } else {
                addr.success = false;
                response(addr);
            }
        });
    } else {
        addr.success = false;
        response(addr);
    }
}



function getRentAmount(id)
{

    $.ajax({
        type: 'post',
        url: '/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getRentAmount',
            id:id},
        success: function (response) {
            var data = JSON.parse(response);
            $(".paidAmount").html(data.amount_paid);
            $(".stripe_amount").val(data.stripe_amount);
        },
    });



}


function flagFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        var flagValue = $(rowObject.Action).attr('flag');
        var flag = '';
        if(flagValue == 'yes'){

        } else {

        }
    }
}

function fetchUserCardDetail(id) {
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {
            class: 'CommonAjax',
            action: 'getCustomerDetails',
            id: id},
        success: function (response) {
            var res = JSON.parse(response);
            console.log(res.data.data);
            if(res.code == 200){
                $('#add-card [name ="cfirst_name"]').val(res.data.data.first_name);
                $('#add-card [name ="clast_name"]').val(res.data.data.last_name);
                $('#add-card [name ="cCompany"]').val(res.data.data.company_name);
                $('#add-card [name ="cphoneNumber"]').val(res.data.data.phone_number);
                $('#add-card [name ="caddress1"]').val(res.data.data.address1);
                $('#add-card [name ="caddress2"]').val(res.data.data.address2);
                $('#add-card [name ="ccity"]').val(res.data.data.city);
                $('#add-card [name ="cstate"]').val(res.data.data.state);
                $('#add-card [name ="czip_code"]').val(res.data.data.zipcode);
                $('#add-card [name ="ccountry"]').val(res.data.data.country);
            }
        },
    });
}

/**
 *  function to format tenant name
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function tenantName(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var flagValue = $(rowObject.Action).attr('flag');
        var id = $(rowObject.Action).attr('data_id');
        var flag = '';
        if (flagValue == 'yes') {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#flagTab" href="javascript:void(0);" data_url="/Vendor/ViewVendor?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
        } else {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }
}


/*payment process jquery starts here */

function cardLists(id)
{
    var tenant_id = id;
    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getTenantAllCards',
            "class": 'Stripe',
            "tenant_id":tenant_id
        },
        success: function (response) {


            $(".cardDetails").html(response);


        }
    });

}

function bankLists(id)
{
    var tenant_id = id;
    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getTenantAllBanks',
            "class": 'Stripe',
            "tenant_id":tenant_id
        },
        success: function (response) {


            $(".accountDetails").html(response);


        }
    });

}





/*payment process jquery ends here */


$(document).on("change","#paymentMethod",function(){
    var value = $(this).val();
    var tenant_id = $("#company_user_id").val();
    if(value=="reoccurring")
    {
        $(".paymentIntervel").show();
        $(".amountField").show();
        $(".countField").show();
        $("#anotherAmountbutton").hide();
        $("#fullAmountButton").hide()
        $("#another_amoumt").hide();
      var   options = $("#payment_category").parent().find('input[type=checkbox]');
        options.each(function () {

               $(this).prop('disabled',false)

        });

    }
    else
    {
        $(".paymentIntervel").hide();
        $(".paymentIntervelcalander").hide();
        $(".amountField").hide();
        $(".countField").hide();
        $("#anotherAmountbutton").show();
        getNewRentAmount(tenant_id);
    }

});




$(document).on("change","#paymentIntervel",function(){
    var value = $(this).val();
    if(value=="monthly")
    {
        $(".paymentIntervelcalander").show();
    }
    else
    {
        $(".paymentIntervelcalander").hide();
    }

});


$("#paymentIntervelcalander").datepicker({
    dateFormat: 'dd',
    changeMonth: true,
    changeYear: true
}).datepicker("setDate", new Date());





$(document).on("click",".changePaymentMethod",function(){
    $('#tenant-payment').modal('toggle');
    $('#set-method').modal('toggle');

});





function stripePaymentVariables(user_id){
    var stripe = Stripe('pk_test_jF5tXlX0qLcCR1YAX9WB8FaL');
    // Create an instance of Elements.
    var elements = stripe.elements();
    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    var cardElemLength = $("#card-element1").length;
    if (cardElemLength > 0) {
        card.mount('#card-element1');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form1');
        form.addEventListener('submit', function(event) {
            $(".submit_payment_class").attr("disabled",true);
            event.preventDefault();
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    var token_id = result.token.id;

                    stripeTokenHandler(token_id,user_id);
                }
            });
        });
    }
}



function stripeTokenHandler(token,user_id) {

    var currency = 'USD';
    var Amount = $(".stripe_amount").val();
    var days =   $(".days_remaining").val();
    var user_type = 'TENANT';
    var tenant_payment_data = $('#tenant_payment_interval_form').serialize();
    var formData = new FormData();
    formData.append('payment_data', tenant_payment_data);
    formData.append('action', 'tenantOneTimePayment');
    formData.append('class', 'Stripe');
    formData.append('token_id', token);
    formData.append('currency', currency);
    formData.append('amount', Amount);
    formData.append('user_type', user_type);
    formData.append('user_id', user_id);

    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData
        , beforeSend: function() {
            // setting a timeout
            $('#loadingmessage').show();
        },
        success: function (response) {
            $('#loadingmessage').hide();
            $(".submit_payment_class").attr("disabled",false)
            var response = JSON.parse(response);
            if(response.status=='success')
            {
                toastr.success('Payment has been done successfully');
                $('#stripe-checkout').modal('toggle');

            }
            else
            {
                toastr.error(response.message);
            }


        }
    });




}

$(document).on("click","#paymentDeduction",function(){
     formData22 = $('#tenant_payment_interval_form').serializeArray();
  //  $("#tenant_form_serialize_date").val(formData);

    var paymentMethod = $('#paymentMethod').val();
    var user_id       = $('#company_user_id').val();
    var amount        = $("#amount").val();
    var count         = $("#count").val();



    if (!$(".payment_terms").is(":checked")) {
        toastr.error("Check the terms & condition checkbox first.")
        return false;
        // do something if the checkbox is NOT checked
    }
    if(paymentMethod=="one-time-payment")
    {
        $('#tenant-payment').modal('toggle');
        $('#stripe-checkout').modal('toggle');

    }
    else
    {
        if($(".changePaymentMethod").html()=="Update your payment method")
        {
            var interval = $("#paymentIntervel").val();
            if(interval=="monthly")
            {
                var date =   $("#paymentIntervelcalander").val();/*For monthly date*/
            }
            else
            {
                var date = 0; /*date zero is only for weekly and bi-weekly.Tenant can set particular date for monthly payment only*/
            }

           var paymentCategory = $("#payment_category").val();
           //console.log(paymentCategory);

            $.ajax({
                url:'/stripeCheckout',
                type: 'POST',
                data: {

                    "action": 'setPaymentProcess',
                    "class": 'Stripe',
                    "paymentMethod":paymentMethod,
                    "interval":interval,
                    "date": date,
                    "user_id":user_id,
                    "amount":amount,
                    'paymentCategory':paymentCategory,
                    'count':count
                },
                success: function (response) {

                    var response = JSON.parse(response);
                    if(response.status=='success')
                    {
                        $('#tenant-payment').modal('toggle');
                        toastr.success(response.message);
                        $('#tenant_listing').trigger( 'reloadGrid' );
                       setTimeout(function(){
                        jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('.table').find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
                        localStorage.removeItem('rowcolorTenant');
                        }, 2000);
                      }
                    else
                    {
                        toastr.error(response.message);

                    }


                }
            });
        }
        else
        {
            toastr.error("Please add your payment method first");
            $('#tenant-payment').modal('toggle');

        }

    }

});



// $(document).on("click","#paymentDeduction",function(){
//     $("#tenant-payment").toggle();
//
// });



$(document).on("click",".changePaymentMethod",function(){
    var type = $(this).attr('data-type');
    if(type=='update')
    {
        $("#addCards").hide();
        $(".paymentAction").val('update');

    }
    else
    {
        $("#addCards").show();
        $(".paymentAction").val('add');
    }



});



$(document).on("change",".cardAction",function(){
 var tenant_id = $(".tenant_id").val();
 var cardAction = $(this).val();
 var card_id = $(this).attr('data-cardId');
 if(cardAction=='delete')
 {
     $(".cardAction").val('');
       bootbox.confirm("Do you want to delete this card?", function (result) {
        if (result == true) {
                   $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
            "action": 'deleteTenantCard',
            "class": 'Stripe',
            "card_id":card_id,
            "tenant_id":tenant_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                 toastr.success(response.message);

                        cardLists();
                        bankLists();




                    }
                });
            }
        });
    }
    else if(cardAction=='default')
    {
        $(".cardAction").val('');
        bootbox.confirm("are you sure you want to make this card as a default source?", function (result) {
            if (result == true) {
                $.ajax({
                    url:'/stripeCheckout',
                    type: 'POST',
                    data: {
                        "action": 'defaultTenantCard',
                        "class": 'Stripe',
                        "card_id":card_id,
                        "tenant_id":tenant_id
                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        toastr.success(response.message);

                        cardLists();
                        bankLists();



                    }
                });

            }
        });



    }
    else if(cardAction=='update')
    {
        $("#addCards").show();



    }


});



// $(document).on("click",'.payment_terms',function(){
//     if(this.checked)
//     {
//
//        // $('#paymentDeduction').prop('disabled', false);
//     }
//     else
//     {
//
//         $('#paymentDeduction').prop('disabled', true);
//     }
// });

$(document).on("click",'#anotherAmountbutton',function(){
    $("#another_amoumt").show(500);
    $("#anotherAmountbutton").hide();
    $("#fullAmountButton").show();
});

$(document).on("click",'#fullAmountButton',function(){
    $("#another_amoumt").hide(500);
    $("#another_amoumt").val('');
    $("#anotherAmountbutton").show();
    $("#fullAmountButton").hide();

    var user_id = $("#company_user_id").val();
    getNewRentAmount(user_id);
});


$(document).on("focusout","#another_amoumt",function(){
        $(".paidAmount").html($(this).val());
        $(".stripe_amount").val($(this).val().replace(/,/g, ''));
});


function getNewRentAmount(id)
{

    $.ajax({
        type: 'post',
        url: '/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getNewRentAmount',
            id:id},
        success: function (response) {
            var data = JSON.parse(response);
            $(".paidAmount").html(data.show_total_amount_due);
            $(".stripe_amount").val(data.total_amoun_due);
            var   options = $("#payment_category").parent().find('input[type=checkbox]');
            if(data.rent_to_be_paid == 0){
                options.each(function () {
                   if($(this).val() == 1){
                    $(this).prop('disabled',true)
                   }
                });
            }
            if(data.total_charges_due == 0){
                options.each(function () {
                    if($(this).val() == 2){
                        $(this).prop('disabled',true)
                    }
                });
            }
            if(data.total_security_cam_charges == 0){
                options.each(function () {
                    if($(this).val() == 3){
                        $(this).prop('disabled',true)
                    }
                });
            }
        },
    });
}



$('#payment_category').multiselect({
    nonSelectedText: 'Select Category',
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    buttonWidth:'100%'
});




var data="";
//Make an array
var dataarray=data.split(",");
// Set the value
$("#payment_category").val(dataarray);
// Then refresh
$("#payment_category").multiselect("refresh");

$(document).on("click",'.setPaymentInterval',function(){
        var user_id       = $('#company_user_id').val();
        $('#tenant-payment').modal('toggle');
        getPaymentIntervals(user_id);
        $('#set-interval').modal('toggle');
  });


function getPaymentIntervals(tenant_id)
{


   $.ajax({
    type: 'post',
    url: '/stripeCheckout',
    data: {
       "class": 'Stripe',
       'action': "getPaymentIntervals",
        "tenant_id":tenant_id
    },
    async: false,
    success: function (response) {

        $(".getPaymentIntervals").html(response);

    }
});

}


$(document).on("change",".status",function(){
 var interval_id = $(this).attr('data-id');

 var interval_type = $(this).val();
  if(interval_type=='cancel')
        {
            bootbox.confirm({
            message: "Are you sure you want to Cancel this Interval ?",
            buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
            callback: function (result) {
                if (result == true) {

                        $.ajax({
                        type: 'post',
                        url: '/stripeCheckout',
                        data: {
                        "class": 'Stripe',
                        'action': "changeIntervalStatus",
                        "interval_id":interval_id
                        },
                        async: false,
                        success: function (response) {
                            response = JSON.parse(response);
                            if(response.status=='true')
                            {
                              toastr.success(response.message);
                              $('#set-interval').modal('toggle');
                            }
                         }
                     });
                  }
               }
           });
         }
      });








$(document).on("click", "#paymentCancel", function (e) {
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $("#tenant-payment").modal('hide');
            }
        }
    });
});


$(document).on('keypress keyup','.numberonly',function(){
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }

});




















