$(document).ready(function() {
    var id = $("#moveout-tenantId").val();
    getAllChargesData(id);


    /*Cancel reason button*/
    $(".reasonCancelButton").click(function() {
        $("#selectreasonForLeaving").hide();
    });
    /*Cancel reason button*/

    if($("#addForwading-table tbody .NoRecordFound").val() == undefined)
    {
        $("#add-forwading-tenant").show();
    }else {
        $("#add-forwading-tenant").hide();
    }

    $(".addNew-charges").click(function () {
        $('#balanceModal').modal('show');
    });

    $("#add-forwading-tenant").click(function(){
        $("#moveout-three #tenantMoveOut-Forwarding").show();
        $("#moveout-three #add-forwading-tenant").hide();
        $("#moveout-three #frwdAdd_records").hide();
    });

    $(document).on("click",".cancel_MoveOut",function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#moveout-three #tenantMoveOut-Forwarding").hide();
                    $("#moveout-three #add-forwading-tenant").show();
                    $("#moveout-three #frwdAdd_records").show();
                }
            }
        });
    });

    $(document).on("click",".cancel_MoveOutUpdate",function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#moveout-three #updateForwordingAddress_Moveout").hide();
                    $("#moveout-three #frwdAdd_records").show();
                }
            }
        });
    });

    $('.calander').datepicker({
        //yearRange: '1919:2019',
        changeMonth: true,
        changeYear: true,
        dateFormat: jsDateFomat
    });

    var date = $.datepicker.formatDate(jsDateFomat, new Date());
    $(".calander").val(date);

    $(document).on("click",".propMoveOuticon",function(){
        $("#selectreasonForLeaving").show();
    });

    $(document).on("click",".add_single1", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var val = $(this).parent().prev().find("."+className).val();
        if (val == ""){
            $(this).parent().prev().find("."+className).next().text("Enter the value");
            return false;
        }else{
            $(this).parent().prev().find("."+className).next().text("");
        }
        savePopUpData(tableName, colName, val, selectName);
        var val = $(this).parent().prev().find("input[type='text']").val('');
    });

    function  savePopUpData(tableName, colName, val, selectName) {
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/savePopUpData',
            data: {
                class: "TenantAjax",
                action: "savePopUpData",
                tableName:tableName,
                colName: colName,
                val:val
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                    $("select[name='"+selectName+"']").append(option);
                    if (selectName == "hobbies[]" || selectName == "additional_hobbies[]"){
                        $("select[name='"+selectName+"']").multiselect('destroy');
                        $("select[name='"+selectName+"']").multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                        $("select[name='"+selectName+"']").parent().parent().next().hide();
                    }else{
                        $("select[name='"+selectName+"']").next().hide();
                    }
                    toastr.success(data.message);

                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    getTenantDetailsMoveOut();
    function getTenantDetailsMoveOut(id,action) {
        var id = $("#wrapper .moveOutTenant_id").val();
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantDetailsMoveOut",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('.grey-detail-box #tenantMoveOutName').html(response.data.TenantName);
                    $('.grey-detail-box #tenantMoveOutPropertyName').html(response.data.TenantPropertyName.property_name);
                    $('.grey-detail-box #tenantMoveOutUnit').html(response.data.TenantUnitName.unitName);
                    $('.grey-detail-box #moveOutBalanceDue').html(response.data.TenantLeaseDetails.balance);
                    $('.grey-detail-box #tenantMoveRent').html(response.data.TenantLeaseDetails.rent_amount);
                    $('.grey-detail-box #moveSecurityDeposit').html(response.data.TenantLeaseDetails.security_deposite);
                    $('.grey-detail-box #tenantMoveLeaseDate').html(response.data.TenantLeaseDate);
                    $('.grey-detail-box #tenantMoveOutEntity').html(response.data.TenantDetails.company_name);
                    $('.grey-detail-box #tenantMoveOutPhone').html(response.data.TenantDetails.phone_number);

                    if(response.data.TenantMoveOutNoticeDate == null){
                        $('#noticeDateMoveOut').val(response.data.TenantLeaseNoticeDate);
                    }else{
                        $('#noticeDateMoveOut').val(response.data.TenantMoveOutNoticeDate);
                    }
                    $('#scheduledMoveOutDate').val(response.data.TenantLeaseMoveOutDate);
                    $('#noOfKeysSignedAtMoveIn').val(response.data.TenantMoveInSigned.movein_key_signed);
                    $('#noOfKeysSigned_moveout').val(response.data.TenantMoveInSigned.movein_key_signed);
                    $('#noticePeriodMoveOut #notice-date').val(response.data.TenantLeaseDetails.notice_period);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
   }


    getActualMoveOutDate();
    function getActualMoveOutDate() {
        var id = $("#wrapper .moveOutTenant_id").val();
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getActualMoveOutDate",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#moveout-two #actualMoveOutDate').val(response.data.TenantMoveOutscheduledDate);
                    ActualMoveOutDate();
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });


    }


    $(document).on("change","#noticeDateMoveOut",function(e){
        e.preventDefault();

        var value = $(this).val();
        var scheduledMoveOutDate = $("#scheduledMoveOutDate").val();
        var noticePeriord = $(".notice-date").val();
        var d = $.datepicker.parseDate('mm/dd/yy', value);
        var g = $.datepicker.parseDate('mm/dd/yy', value);
        var h = $.datepicker.parseDate('mm/dd/yy', scheduledMoveOutDate);

        if (scheduledMoveOutDate == "") {
            alert("Scheduled MoveOut End Date is Blank !");
            $(this).val("");
            return false;
        }else{
            var newScheduledDate = d;
            if (noticePeriord == "30") {
                //d.setDate(d.getDate() - 30);
                newScheduledDate.setDate(newScheduledDate.getDate() + 30);
            }else if (noticePeriord == "60") {
                //d.setDate(d.getDate() - 60);
                newScheduledDate.setDate(newScheduledDate.getDate() + 60);
            }else if (noticePeriord == "90") {
                newScheduledDate.setDate(newScheduledDate.getDate() + 90);
            }else{
                $(".noticeDateMoveOut").val("");
            }

            var startDate = new Date(d);
            var endDate = new Date(h);
            var result = startDate - endDate;
            var newdate2 = myDateFormatter(d);
            var newdate3 = myDateFormatter(g);
            var newSDate = myDateFormatter(newScheduledDate);

            if (result < 0){
                $("#noticeDateMoveOut").val(newdate3);
                $("#scheduledMoveOutDate").val(newSDate);
            }else{
                $("#noticeDateMoveOut").val("");
                bootbox.alert("Please select date before "+newdate2);
            }
        }
    });

    function myDateFormatter(dateObject){
        var newdate = dateObject.getFullYear() + "-" + (dateObject.getMonth() + 1) + "-" + dateObject.getDate();
        var tmp = [];
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/getDateFormat',
            data: {class: "TenantAjax",action: "getDateformat",date: newdate,},
            async: false,
            global:false,
            success: function (response) {
                tmp = $.parseJSON(response);
            }
        });
        return tmp;
    }

    getTenantChargeCodeMoveOut();

    function getTenantChargeCodeMoveOut() {
        var id = $("#wrapper .moveOutTenant_id").val();
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantChargeCodeMoveOut",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('#chargeCode-table tbody').html(response.data);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    getReasonForLeavingDetails();
    function getReasonForLeavingDetails() {
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getReasonForLeavingDetails"
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    if (response.data.length > 0){
                        var reasonOption = "";
                        for(var i=0; i<response.data.length; i++){
                            var html='<option value="'+ response.data[i].id +'">'+ response.data[i].reasonName +'</option>';
                            $('select[name="reasonForLeaving"]').append(html);
                        }
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("click",'#updateMoveOutCancel',function(){
        // var id =  getParameterByName('id');
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href = '/Tenantlisting/Tenantlisting';
            }
        });
    });

    $(document).on("submit","#noticePeriodMoveOut",function(e) {
        e.preventDefault();
        var id = $("#wrapper .moveOutTenant_id").val();
        var formData = $('#noticePeriodMoveOut').serializeArray();

        $.ajax({
            type: 'POST',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "updateNoticePeriodData",
                id: id,
                form: formData
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    toastr.success("This Record Saved Successfully");
                    $("#moveout_tenant2").click();
                    getActualMoveOutDate();


                } else {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });


    });

    function ActualMoveOutDate (){
        var actDate = $("#actualMoveOutDate").val();
        var da = $.datepicker.parseDate('mm/dd/yy', actDate);
        if(da !== null){
        da.setDate(da.getDate() + 1);
        var newSDateNew = myDateFormatter(da);
        $(".unitAvailableDate").val(newSDateNew);
        }else{
            var d = new Date();
            var newSDateNew = myDateFormatter(d);
            d.setDate(d.getDate() + 1);
            var newSDateNew1 = myDateFormatter(d);
            $("#actualMoveOutDate").val(newSDateNew);
            $(".unitAvailableDate").val(newSDateNew1);
        }
    }

    $(document).on("submit","#updateDataRecordMoveOut",function(e) {
        e.preventDefault();
        var id = $("#wrapper .moveOutTenant_id").val();
        var formData = $('#updateDataRecordMoveOut').serializeArray();
        $.ajax({
            type: 'POST',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "updateRecordMoveOutData",
                id: id,
                form: formData
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                   // toastr.success("This Record Saved Successfully");
                    bootbox.confirm({
                        message: "Would you like to update the Actual-Move-Out Date?",
                        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                        callback: function (result) {
                            if (result === true) {
                                bootbox.alert("The Actual Move-Out Date Saved Successfully");
                                $( ".moveout_tenant3").parent().find("li").removeClass("active");
                                $(".moveout_tenant3").removeClass("disabled");

                                $( ".moveout_tenant3").trigger('click');
                                $( "#moveout-two").removeClass('active');
                                $( "#moveout_tenant3").trigger('click');
                                $( ".moveout_tenant3").addClass('active');
                                $( "#moveout-three").addClass('active');
                                $( "#moveout_tenant3").attr("aria-expanded",true);
                            }
                        }
                    });
                } else {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });


    });



    $(document).on("submit","#addForwordingAddress_Moveout",function(e) {

        e.preventDefault();
        var id = $("#wrapper .moveOutTenant_id").val();
        var formData = $('#addForwordingAddress_Moveout').serializeArray();

        $.ajax({
            type: 'POST',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "insertForwardAddressMvOut",
                id: id,
                form: formData
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    toastr.success("This Record Saved Successfully");
                    $("#moveout-three #tenantMoveOut-Forwarding").hide();
                    $("#moveout-three #frwdAdd_records").show();
                    getTenantForwordAddress();
                } else {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});
    $('.number_only').keydown(function (e) {
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });


    getTenantForwordAddress();
    function getTenantForwordAddress() {
        var id = $("#wrapper .moveOutTenant_id").val();
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantForwordAddress",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $('#addForwading-table tbody').html(response.data);
                    // $('#addForwading-table').load("tbody");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /*Ajax for charge code listing*/
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getCharges',
        data: {
            class: "TenantAjax",
            action: "getCharges"
        },
        success: function (response) {

            var data = $.parseJSON(response);
            if (data.status == "success") {
                var charges = data.data.charges;

                if (charges.length > 0){
                    var chargesOptions = '';
                    for (var i = 0; i < charges.length; i++){
                        chargesOptions += "<option value='"+charges[i].id+"'>"+charges[i].charge_code+"</option>";
                    }

                    $("#new_chargeCode_form .tax_chargeCode").html(chargesOptions);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

    /*Ajax for charge code listing*/
    $("#new_chargeCode_form").validate({
        rules: {
            tax_chargeCode: {
                required: true
            },
            amount: {
                required: true
            },
            new_chargeCode_description :{
                required: true
            }
        }
    });

    $(document).on("submit","#new_chargeCode_form",function(e) {
        e.preventDefault();
        if ($('#new_chargeCode_form').valid()){
            addChargeCode();
        }
        return false;
    });

    function addChargeCode(){
        /*Insert charge code balance tab MoveOut*/
            var id = $("#wrapper .moveOutTenant_id").val();
            var formData = $('#new_chargeCode_form').serializeArray();

            $.ajax({
                type: 'POST',
                url: '/moveOutTenant',
                data: {
                    class: "moveOutTenant",
                    action: "insertbalanceChargeCode",
                    id: id,
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);

                    if (response.status == 'success' && response.code == 200) {
                        toastr.success("This Record Saved Successfully");
                        $('#balanceModal').modal('hide');
                    } else {
                        toastr.error(response.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        /*Insert charge code balance tab MoveOut*/

    }


    /*Get Data from forwardAddress MoveOut Tenant*/


    function updateTenantForwordAddress() {
        var id = $("#wrapper .moveOutTenant_id").val();
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantForwordAddress",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    $("#MoveOut-ForwardingUpdate #FrwdAddress_FName1").val(response.dataDetails[0].frwdAddress_FName);
                    $("#MoveOut-ForwardingUpdate #FrwdAddress_LName1").val(response.dataDetails[0].frwdAddress_LName);
                    $("#MoveOut-ForwardingUpdate #FrwdAddress_Address1").val(response.dataDetails[0].frwdAddress_Address);
                    $("#MoveOut-ForwardingUpdate #FrwdAddress_Zip1").val(response.dataDetails[0].frwdAddress_Zip);
                    $("#MoveOut-ForwardingUpdate #FrwdAddress_Country1").val(response.dataDetails[0].frwdAddress_Country);
                    $("#MoveOut-ForwardingUpdate #FrwdAddress_State1").val(response.dataDetails[0].frwdAddress_State);
                    $("#MoveOut-ForwardingUpdate #FrwdAddress_City1").val(response.dataDetails[0].frwdAddress_City);
                    $("#MoveOut-ForwardingUpdate #FrwdAddress_Phone1").val(response.dataDetails[0].frwdAddress_Phone);
                    $("#MoveOut-ForwardingUpdate #FrwdAddress_Email1").val(response.dataDetails[0].email);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    /*Get Data from forwardAddress MoveOut Tenant*/


    /*Insert charge code balance tab MoveOut*/
    $(document).on("submit","#updateForwordingAddress_Moveout",function(e) {
        e.preventDefault();
        var id = $("#wrapper .moveOutTenant_id").val();
        var formData = $('#updateForwordingAddress_Moveout').serializeArray();

        $.ajax({
            type: 'POST',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "updateTenantForwordAddress",
                id: id,
                form: formData
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    toastr.success("This Record Update Successfully");
                    $("#moveout-three #MoveOut-ForwardingUpdate").hide();
                    $("#moveout-three #frwdAdd_records").css("display","block");
                    getTenantForwordAddress();
                } else {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    getTabDataRecord();
    function getTabDataRecord(){
        var id = $("#wrapper .moveOutTenant_id").val();
        $.ajax({
            type: 'POST',
            url:'/moveOutTenant',
            data:{
            class:"moveOutTenant",
            action:"getTabDataRecord",
                id:id
            },
            success: function(response){
                var response = JSON.parse(response);

                if(response.data.noOfKeysSigned_moveout == null){
                    $(".moveout_tenant3").addClass("disabled");
                    $(".moveout_tenant3.disabled #moveout_tenant3").click(function(){
                        toastr.error("Please complete the Move-Out  Details first.");
                        return false;
                    });
                }else{
                    $(".moveout_tenant3").removeClass("disabled");
                }
            }
        });
    };

    $(document).on('change', '#addForwading-table .select_options', function() {

        var opt = $(this).val();
        var id = $(this).attr('data_id');
        if (opt == 'EDIT' || opt == 'Edit') {
            $("#moveout-three #MoveOut-ForwardingUpdate").show();
            $("#moveout-three #frwdAdd_records").css("display","none");
            updateTenantForwordAddress();
        }
    });

    /*Insert charge code balance tab MoveOut*/

    /*Record Move Out date No. of Keys Signed Out*/
    $("#noOfKeysSigned_moveout").keyup(function(){
        var moveout = $(this).val();
        var moveout1 = $("#noOfKeysSignedAtMoveIn").val();
        if(moveout > moveout1){
            $("#noOfKeysSigned_moveout").val("");
        }else {
            $("#noOfKeysSigned_moveout").val(moveout);
        }
    });
    /*Record Move Out date No. of Keys Signed Out*/


    $(document).on("submit","#noticeBalanceTab",function(e) {
        e.preventDefault();
        var id = $("#wrapper .moveOutTenant_id").val();
        $.ajax({
            type: 'POST',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "updateMoveOutListData",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var base_url = window.location.origin;
                    localStorage.setItem("Message", "This Record Update Successfully");
                    localStorage.setItem("rowcolor",true);
                    window.location.href = base_url+"/Tenant/MoveOut";
                    $("#MoveOuttenant_listing").trigger('reloadGrid');
                    onTop(true);
                } else {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    });

    /*Save and continue button 3rd step and Cancel button Js MoveOut*/
    $(document).on("click",".saveContinueBtn",function(e) {
        toastr.success("This Information was successfully saved");
        $("#moveout_tenant4").click();
       // balanceSheetFun();
        var id = $("#moveout-tenantId").val();
        getAllChargesData(id);
        calculateSum();
    });

    /**
     * Auto fill the city, state and country when focus loose on zipcode field.
     */
    $(function () {

        $('#addForwordingAddress_Moveout #FrwdAddress_Zip').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });

        /**
         * Auto fill the city, state and country when focus loose on zipcode field.
         */
        $("#addForwordingAddress_Moveout #FrwdAddress_Zip").focusout(function () {
            getZipCode('#FrwdAddress_Zip',$(this).val(),'#FrwdAddress_City','#FrwdAddress_State','#FrwdAddress_Country','','#zip_code_validate');
        });

        /**
         * Auto fill the city, state and country when enter key is clicked.
         */
        $("#addForwordingAddress_Moveout #FrwdAddress_Zip").keydown(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                getZipCode('#FrwdAddress_Zip',$(this).val(),'#FrwdAddress_City','#FrwdAddress_State','#FrwdAddress_Country','','#zip_code_validate');
            }
        });
    });

    /*Clear Balance list tenant moveout*/


    function getAllChargesData(id)
    {
        $.ajax({
            url:'/moveOutTenant',
            type: 'POST',
            data: {

                "action": 'getAllChargesData',
                "class": 'moveOutTenant',
                "user_id":id

            },
            success: function (response) {
/*console.log("balance sheet>>>", response);*/
                $(".balance-sheet-charge").html(response);
               /* $('.reallocate').attr('data-type',type);
                $('.reallocate').attr('data-username',Name);
                $('.reallocate').attr('data-user_id',id);

                updateAllPayments();*/

            }
        });
    }
    /*Clear Balance list tenant moveout*/
    calculateSum();
    $(document).on("blur",".amount_num",function(){
        calculateSum();
    });

    function calculateSum() {
        var sum = 0;
        $(".amount_num").each(function() {
            var amount_value =  this.value.replace(/,/g, '');
            if(!isNaN(amount_value) && amount_value!=0) {
                sum += parseFloat(amount_value);
            }

        });
       /* console.log(sum);*/
        var dhdhtml = $(".total_charges_amount").html(sum.toFixed(2));
    }

    /*Apply creadit/debet modal*/
    $(document).on("change","#noticeBalanceTab .apply-data",function(){
        var value = $(this).val();

        var dataId = $(this).attr("data-id");
        var waiveOfAmount = $(".waiveOfAmount_"+dataId).val();
        var waiveOfComment = $(".waiveOfComment_"+dataId).val();
        var writeOfAmount = $(".writeOfAmount_"+dataId).val();
        var badDebtOfAmount = $(".badDebtOfAmount_"+dataId).val();
       /* var currentPayment = $(".currentPayment_"+dataId).val();*/
        var current_due_amt = $(".current_due_amt_"+dataId).text();


        $("#txtRemainingChargeAmount").removeClass('capital');

        console.log("hhh>",waiveOfAmount);
        console.log("hhh -->",waiveOfComment);
        console.log("hhh ----",writeOfAmount);
        console.log("hhh ----->",badDebtOfAmount);
        if(value == 'Apply'){
            $("#waiveOfAmount").val(waiveOfAmount);
            $("#waiveOfComment").val(waiveOfComment);
            $("#writeOfAmount").val(writeOfAmount);
            $("#badDebtOfAmount").val(badDebtOfAmount);
            $("#txtRemainingChargeAmount").val(current_due_amt);
            $('#apply-credit-modal').modal('show');
            getChargesApply(dataId);
            /*applyChargesUser(dataId);*/
        }
    });
    /*Apply creadit/debet modal*/



    var userId = $('#moveoutId').val();

    /*ajax for get charges */
    function getChargesApply(id){
        console.log("wel done!!");
            $.ajax({
                type: 'POST',
                url:'/moveOutTenant',
                data:{
                    class:"moveOutTenant",
                    action:"getCharges",
                    id:id,
                    userId:userId

                },
                success: function(response){
                    /*var response = JSON.parse(response);*/
                        var html = response;
                        $("#tbl-credit-data").html(html);
                        $woffamount = $("#waiveOfAmount").val();
                        setTimeout(function(){
                            $("#wavieddAmount_"+id).val($woffamount);
                        }, 1000);


                    var value = parseFloat($('#txtRemainingChargeAmount').val()) - parseFloat($("#waiveOfAmount").val());
                    $('.appliedAmount_'+id).val( value.toFixed(2) );
                    $('#appliedAmountHidden').val( value.toFixed(2) );
                        /*js popup*/
                    var creditBalance = $(".remaining_due_amt_"+id).val();
                    if(creditBalance == '0'){
                        $(".remaining_due_amt_"+id).attr("disabled", true);
                    }


                    /*if(creditBalance > '0'){
                        var creditB =$(".remaining_due_amt_"+id).val();
                    }*/

                    var curntdata = $(".current_due_amt_"+id).val();
                    if(curntdata == '0'){
                        $(".appliedAmount_"+id).attr("disabled", true);
                    }

                    var disabletext = $("#txtRemainingChargeAmount").val();
                    if(disabletext == '0'){
                        $(".appliedAmount_"+id).attr("disabled", true);
                    }
                        /*js popup*/

                    getCreditAmt(id);
                    applyChargesUser(id);

                }
            });
        };
    /*ajax for get charges */


    function getCreditAmt(id) {

    }

    function applyChargesUser(id){
        $(document).on("blur","#tbl-credit-data .creditGetData",function() {
            var value1 = parseFloat($('#appliedAmountHidden').val());
            var value2 = parseFloat($('.appliedAmount_' + id).val());


            var appValue = value1 - value2;
            var cGetValue = $('#remainingCdataOriginal').val();

            var actualCreditval = cGetValue - appValue;

            console.log("appValue >>", appValue);
            console.log("cGetValue >>", cGetValue);
            console.log("actualCreditval >>", actualCreditval);
            var addCredit = "";

            /*var cCreditValue = $('#remainingCdataOriginal').val();*/

            switch (true) {
                case (actualCreditval == '0'):
                    console.log("here");
                    $(".remaining_due_amt_" + id).val(actualCreditval);
                    var minusData = cGetValue - actualCreditval;
                    addCredit += minusData;
                    console.log("finalgetdata",addCredit);
                    break;
                case (actualCreditval < '0'):
                    console.log("here2");
                    toastr.warning("Please enter the valid Amount.");
                    $(".remaining_due_amt_" + id).val(cGetValue);
                    $(".appliedAmount_" + id).val(value1);
                    break;
                case (actualCreditval > cGetValue):
                    console.log("here2");
                    toastr.warning("Please enter the valid Amount.");
                    $(".remaining_due_amt_" + id).val(cGetValue);
                    $(".appliedAmount_" + id).val(value1);
                    break;
                default:
                    console.log("here3");
                    $(".remaining_due_amt_" + id).val(actualCreditval);
                    var minusData = cGetValue - actualCreditval;
                    addCredit += minusData;
                    console.log("finalgetdata",addCredit);
                    break;
            }

            $("#finalCreditGet").val(addCredit);
        });
        /*ajax for charges for remaining balance */

        $(document).on("click","#btnRemainingcredit",function(e) {
            e.preventDefault();
            /*js check*/
            var txtRemainingChargeAmount = $('#txtRemainingChargeAmount').val();
            if(txtRemainingChargeAmount == '0'){
                $(".appliedAmount_"+id).attr("disabled", true);
            }


            var appliedAmount = parseFloat($('.appliedAmount_'+id).val());
            var waveappliedAmount = parseFloat($('.wavieddAmount_'+id).val());

            var addCredit1 = parseFloat($('#finalCreditGet').val());

            var totalAmt = appliedAmount + waveappliedAmount + addCredit1;
            switch (true) {
                case (totalAmt > txtRemainingChargeAmount ):
                    toastr.warning("Please enter the valid Amount.");
                    break;
                case (totalAmt == txtRemainingChargeAmount):
                    var formData = $('#moveOutCredit').serializeArray();
                    var remainingCdataOriginal = $('#remainingCdataOriginal').val();
                    var amtPaid = $('#amtPaid').text();

                    /*js check*/
                    $.ajax({
                        type: 'POST',
                        url:'/moveOutTenant',
                        data:{
                            class:"moveOutTenant",
                            action:"insertRemainingCharges",
                            id:id,
                            formData:formData,
                            userId:userId,
                            remainingCdataOriginal:remainingCdataOriginal,
                            amtPaid:amtPaid

                        },
                        success: function(response){
                            var response = JSON.parse(response);
                            console.log("happiness",response);
                            if(response.status == "success"){
                                $('#apply-credit-modal').modal('hide');
                                window.location.reload();
                                toastr.success(data.message);
                            }
                        }
                    });
                    break;

                default:
                    toastr.warning("Please enter the valid Amount.");
                    break;
            }
        });
    };


    /*ajax for charges for remaining balance */

});

