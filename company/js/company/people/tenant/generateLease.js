
$(document).ready(function () {
    $(document).on('click','.signsubmit',function () {
        $('#btnSave').parent().show();
    });
    /*checkSignatureDetails();
    function checkSignatureDetails(){
        var tenant_id = getParameterByName('ti');
        var id = getParameterByName('id');
        console.log('tenant_id',tenant_id);
        console.log('id',id);
        $.ajax({
            type: 'post',
            url: '/EsignatureUser/sendESigatureMail',
            data: {
                class: "GenerateLease",
                action: "checkSignatureDetails",
                tenant_id:tenant_id,
                id:id

            },
            success: function (response) {
                var data = $.parseJSON(response);
            },
            error: function (data) {

            }
        });
    }*/

    $(document).on("click", ".e_sign_doc_url", function () {
        $("#esignature_disclosure").modal("show");
    });

    var typepdf = localStorage.getItem('createPDF');

    if (typepdf === 'createPDF'){
        setTimeout(function () {
            createPDf();
        },2000);
    }
    $(document).on("change","input[name='typing']",function () {
        if($(this).val() == "t"){
            $(".sketchingarea").hide();
            $(".typingarea").show();
        }else{
            $(".sketchingarea").show();
            $(".typingarea").hide();
        }
    });

    $(document).on("click",".accept_generate_lease", function () {
        var userId = $(".tenant_session_id").val();
        $.ajax({
            type: 'post',
            url: '/EsignatureUser/sendESigatureMail',
            data: {


                class: "GenerateLease",
                action: "sendESigatureMail",
                user_id: userId,
                type: 'tenant'
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    if (data.message != ""){
                        toastr.success(data.message);
                    }
                    setTimeout(function () {
                        var base_url = window.location.origin;
                        window.location.href = base_url + '/Tenantlisting/Tenantlisting';
                    },1000);
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {

                    $('#' + key + '_err').text(value);
                });
            }
        });

        $("#generate_online_lease").modal('hide');
        toastr.success("E-Sign document has been sent to tenant!");
        return false;
    });

    $(document).on("click","#btnDownload",function () {
        /*window.print();
        return false;*/
        var id = getParameters('id');
        var paramId = '';
        var userType = 'T';
        var ti = getParameters('ti');
        var ai = getParameters('ai');
        var oi = getParameters('oi');
        var gi = getParameters('gi');
        if (!ti){
            paramId = ti;
            userType = "T";
        }
        if (ai){
            paramId = ai;
            userType = "AT";
        }
        if (oi){
            paramId = oi;
            userType = "O";
        }
        if (gi){
            paramId = gi;
            userType = "G";
        }
        $.ajax({
            type: 'post',
            url: '/EsignatureUser/getSignatureInfo',
            data: {
                class: "GenerateLease",
                action: "createPdfFile",
                paramId: paramId,
                id: id,
                userType: userType,
                url: window.location.href
            },
            success: function (response) {
                var res= JSON.parse(response);
                if (res.code == 200) {
                    $('.reports-loader').css('visibility', 'hidden');
                    var link = document.createElement('a');
                    document.body.appendChild(link);
                    link.target = "_blank";
                    link.download = "Report.pdf";
                    link.href = res.data.record;
                    link.click();
                }
                //var data = JSON.parse(response);
            }
        });
    });

    var pathName = window.location.pathname;

    if (pathName == '/EsignatureUser/EsignatureLease'){
        getResidentialLeaseData();
    }

    /* tenant signature function start */
    $(document).on("click","#tenantSigHere", function () {
        var checkBoxValidation = checkCheckBoxValidation();
        if (!checkBoxValidation){
            $(".termconditionerror").removeClass("hide");
            return false;
        }else{
            $(".termconditionerror").addClass("hide");
        }
        var parentId = getParameters('id');
        var userId = getParameters('ti');

        $("#eSignModal .submitsign a").attr("data-user",userId);
        $("#eSignModal .submitsign a").attr("data-parent",parentId);
        $("#eSignModal .submitsign a").attr("data-type",'T');
        $("#eSignModal").modal('show');
        /* var checkSign=$(".tenantSection img").attr('src');
          if(checkSign!==undefined){
             $("#eSignModal").modal('show');
         }else{
              $('#btnSave').prop('disabled');
              $('#btnSave').hide();
             toastr.warning('Signature Already updated!');
         }

 */
        $('#defaultSignature').signature({syncField: '#signatureJSON'});
        $('#defaultSignature').signature('option', 'syncFormat', 'PNG');
    });
    $(document).on("click",'.clearSignature',function() {
        $('#defaultSignature').signature('clear');
    });

    $(document).on("click",".submitsign a",function(e){
        e.preventDefault();
        $('#redrawSignature').signature('enable').signature('draw', $('#signatureJSON').val());
        var dataUser = $(this).attr("data-user");
        var dataType = $(this).attr("data-type");
        var dataParent = $(this).attr("data-parent");
        var fontText = $(".typingtext").val();
        var fontfamily = $(".typingtext").attr("data-font");
        setTimeout(function () {
            var can = document.getElementById("canvas2");
            var pngUrl = can.toDataURL();
            saveSignatureImage(pngUrl,dataUser,dataType,dataParent,fontText,fontfamily);
        },1000);
    });

    $(document).on("click","#btnSave",function(e){
        e.preventDefault();
        var styleAttr = $(".graggalbeElem").attr("style");
        var parentId = getParameters('id');
        var tId = getParameters('ti');
        if (!tId) {
            tId = getParameters('ai');
            if (!tId) {
                tId = getParameters('gi');
                if (!tId) {
                    tId = getParameters('oi');
                    if (!tId) {

                        tId = getParameters('id');
                    }
                }
            }
        }

        $.ajax({
            type: 'post',
            url: '/EsignatureUser/saveSignatureImage',
            data: {
                class: "GenerateLease",
                action: "saveSignatureStyle",
                parentId: parentId,
                tId: tId,
                styleAttr: styleAttr
            },
            success: function (response) {
                var data = $.parseJSON(response);
                console.log(response);
                if (data.status == "success") {
                    toastr.success(data.message);
                    setTimeout(function () {
                        window.location.href = window.location.origin+'/EsignatureUser/thankyou';
                    },2000);
                } else {
                    $('#btnSave').hide();
                    toastr.warning(data.message);
                }
            }
        });
    });

    $('#redrawSignature').signature({disabled: true});
    $('#redrawSignature canvas').prop('id',"canvas2");

    $("#tenatSigHere").attr("id","");
    $(document).on("click",".addTeantSigHere", function () {
        var checkBoxValidation = checkCheckBoxValidation();
        if (!checkBoxValidation){
            $(".termconditionerror").removeClass("hide");
            return false;
        }else{
            $(".termconditionerror").addClass("hide");
        }
        var parentId = getParameters('id');
        var aId = getParameters('ai');

        $("#eSignModal .submitsign a").attr("data-user",aId);
        $("#eSignModal .submitsign a").attr("data-parent",parentId);
        $("#eSignModal .submitsign a").attr("data-type",'AT');
        $("#eSignModal").modal('show');
        $('#defaultSignature').signature({syncField: '#signatureJSON'});
        $('#defaultSignature').signature('option', 'syncFormat', 'PNG');
    });

    $(document).on("change",".termcondition", function(){
        if ($(this).is(':checked')){
            $(".termconditionerror").addClass("hide");
        }/*else{
            $(".termconditionerror").removeClass("hide");
        }*/
    });

    $(document).on("click",".guarantSigHere", function () {
        var checkBoxValidation = checkCheckBoxValidation();
        if (!checkBoxValidation){
            $(".termconditionerror").removeClass("hide");
            return false;
        }else{
            $(".termconditionerror").addClass("hide");
        }
        var parentId = getParameters('id');
        var gId = getParameters('gi');

        $("#eSignModal .submitsign a").attr("data-user",gId);
        $("#eSignModal .submitsign a").attr("data-parent",parentId);
        $("#eSignModal .submitsign a").attr("data-type",'G');
        $("#eSignModal").modal('show');
        $('#defaultSignature').signature({syncField: '#signatureJSON'});
        $('#defaultSignature').signature('option', 'syncFormat', 'PNG');
    });
    $(document).on("click",".ownerSigHere", function () {
        var checkBoxValidation = checkCheckBoxValidation();
        if (!checkBoxValidation){
            $(".termconditionerror").removeClass("hide");
            return false;
        }else{
            $(".termconditionerror").addClass("hide");
        }
        var parentId = getParameters('id');
        var oId = getParameters('oi');

        $("#eSignModal .submitsign a").attr("data-user",oId);
        $("#eSignModal .submitsign a").attr("data-parent",parentId);
        $("#eSignModal .submitsign a").attr("data-type",'O');
        $("#eSignModal").modal('show');
        $('#defaultSignature').signature({syncField: '#signatureJSON'});
        $('#defaultSignature').signature('option', 'syncFormat', 'PNG');
    });

    $('input[name="selectfonts"]').on('change', function() {
        var value = $(this).val();
        applyFont(value,".enterplaceholder,.typingtext");
    });


});

function checkCheckBoxValidation(){
    if ($('.termcondition').is(':checked')){
        return true;
    }
    return false;
}

function createPDf(){
    localStorage.setItem('createPDF','');
    window.print();
    window.close();
}

function saveSignatureImage(pngUrl,dataUser,dataType,dataParent,fontText,fontfamily) {
    // createPdf();
    var inputVal = $("input[name='typing']:checked").val();
    $.ajax({
        type: 'post',
        url: '/EsignatureUser/saveSignatureImage',
        data: {
            class: "GenerateLease",
            action: "saveSignatureImage",
            id: dataUser,
            dataType: dataType,
            dataParent: dataParent,
            inputVal: inputVal,
            img: pngUrl,
            fontText: fontText,
            fontfamily: fontfamily
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                $("#eSignModal").modal('hide');

                if (data.data.usertype == 'T'){
                    if (inputVal == 's'){
                        var tenantSingature ='<b class="graggalbeElem" style="float:left;width:150px;clear:both;"><img style="width: 100%;" src="'+data.data.signature_image+'"></b><br><b style="float:left;width:20%;clear:both; color: #32cbe1;" id="tenatSigHere">Change Signature</b><br>';
                    }else{
                        var tenantSingature ='<b class="graggalbeElem" style="float:left;width:150px;clear:both;font-family: '+fontfamily+'">'+fontText+'</b><br><b style="float:left;width:20%;clear:both; color: #32cbe1;" id="tenatSigHere">Change Signature</b><br>';
                    }

                    $('#tenantSigHere').html(tenantSingature);
                    var tenantDate ='<span style="background-color:#FFED87 !important;" href="javascript:;">'+data.data.updated_at+'</span>';
                    $('#tenantSigDateHere').html("Date: "+tenantDate);
                    var paramId = getParameters('ti');
                    sendEmailToGuarantor(paramId, "guarantor");
                }

                if (data.data.usertype == 'AT'){
                    if (inputVal == 's'){
                        var tenantSingature ='<b class="graggalbeElem" style="float:left;width:150px;clear:both;"><img style="width: 100%;" src="'+data.data.signature_image+'"></b><br><b style="float:left;width:20%;clear:both; color: #32cbe1;" id="addtenatSigHere">Change Signature</b><br>';
                    }else{
                        var tenantSingature ='<b class="graggalbeElem" style="float:left;width:150px;clear:both;font-family: '+fontfamily+'">'+fontText+'</b><br><b style="float:left;width:20%;clear:both; color: #32cbe1;" id="tenatSigHere">Change Signature</b><br>';
                    }
                    $('.addTeantSigHere-'+data.data.user_id).html(tenantSingature);
                    var tenantDate ='<span style="background-color:#FFED87 !important;" href="javascript:;">'+data.data.updated_at+'</span>';
                    $('.addTeantSigDateHere-'+data.data.user_id).html("Date: "+tenantDate);
                }

                if (data.data.usertype == 'G'){
                    if (inputVal == 's'){
                        var tenantSingature ='<b class="graggalbeElem" style="float:left;width:150px;clear:both;"><img style="width: 100%;" src="'+data.data.signature_image+'"></b><br><b style="float:left;width:20%;clear:both; color: #32cbe1;" id="guarantSigHere" class="guarantSigHere">Change Signature</b><br>';
                    }else{
                        var tenantSingature ='<b class="graggalbeElem" style="float:left;width:150px;clear:both; font-family: '+fontfamily+'">'+fontText+'</b><br><b style="float:left;width:20%;clear:both; color: #32cbe1;" id="guarantSigHere" class="guarantSigHere">Change Signature</b><br>';
                    }
                    $('.guarantSigHere-'+data.data.user_id).html(tenantSingature);
                    var tenantDate ='<span style="background-color:#FFED87 !important;" href="javascript:;">'+data.data.updated_at+'</span>';
                    $('.guarantDateHere-'+data.data.user_id).html("Date: "+tenantDate);
                    sendEmailToGuarantor(dataParent, "owner");
                }

                if (data.data.usertype == 'O'){

                    if (inputVal == 's'){
                        var tenantSingature ='<b class="graggalbeElem" style="float:left;width:150px;clear:both;"><img style="width: 100%;" src="'+data.data.signature_image+'"></b><br><b style="float:left;width:20%;clear:both; color: #32cbe1;" id="ownerSigHere" class="ownerSigHere">Change Signature</b><br>';
                    }else{
                        var tenantSingature ='<b class="graggalbeElem" style="float:left;width:150px;clear:both; font-family: '+fontfamily+'">'+fontText+'</b><br><b style="float:left;width:20%;clear:both; color: #32cbe1;" id="ownerSigHere" class="ownerSigHere">Change Signature</b><br>';
                    }
                    $('.ownerSigHere-'+data.data.user_id).html(tenantSingature);
                    var tenantDate ='<span style="background-color:#FFED87 !important;" href="javascript:;">'+data.data.updated_at+'</span>';
                    $('.ownerDateHere-'+data.data.user_id).html("Date: "+tenantDate);
                    sendEmailToGuarantor(dataParent, "manager");
                }
                $(".graggalbeElem").draggable();
                toastr.success(data.message);
                return false;
            }else{
                toastr.error(data.message);
            }
        }
    });

    return false;
}
function getParameterByName(name) {
    var regexS = "[\\?&]" + name + "=([^&#]*)",
        regex = new RegExp(regexS),
        results = regex.exec(window.location.search);
    if (results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
// createPdf();
// function createPdf(){
//     var id = getParameters('id');
//     var paramId = '';
//     var oi = getParameters('oi');
//
//     if (oi != '') {
//         paramId = oi;
//         var userType = "O";
//         console.log('userType',userType);
//         console.log('oi',oi);
//         $.ajax({
//             type: 'post',
//             url: '/EsignatureUser/saveSignatureImage',
//             data: {
//                 class: "GenerateLease",
//                 action: "createPdfFile3",
//                 paramId: paramId,
//                 id: id,
//                 userType: userType,
//                 html: document.getElementById("dvPdfToHtml").innerHTML,
//                 url: window.location.href
//             },
//             success: function (response) {
//                 console.log(response);
//                 var res = JSON.parse(response);
//             }
//         });
//     }
// }


function checkIfSignedOrNot(){
    var id = getParameterByName('id');
    var paramId = '';
    var userType = 'T';
    var ti = getParameterByName('ti');
    var ai = getParameterByName('ai');
    var oi = getParameterByName('oi');
    var gi = getParameterByName('gi');
    if (ti){
        id = ti;
        userType = "T";
    }
    if (ai){
        paramId = ai;
        userType = "AT";
    }
    if (oi){
        paramId = oi;
        userType = "O";
    }
    if (gi){
        paramId = gi;
        userType = "G";
    }
    $.ajax({
        type: 'post',
        url: '/EsignatureUser/getSignatureInfo',
        data: {
            class: "GenerateLease",
            action: "checkIfSignedOrNot",
            id: id,
            paramId: paramId,
            userType: userType
        },
        success: function (response) {
            var data = JSON.parse(response);
            if (data.status== "success" && data.code == 200){
                window.location.href = window.location.origin+'/EsignatureUser/Accessdenied';
            }
        }
    });
    return false;

}


function getResidentialLeaseData() {
    checkIfSignedOrNot();

    var leaseId = $(".leaseid").val();
    var paramId = getParameters('id');
    if (paramId == "0"){
        var paramtId = getParameters('ti');

    }else{
        var paramtId = getParameters('id');
    }

    $.ajax({
        type: 'post',
        url: '/EsignatureUser/residentialLeaseAgreement',
        data: {
            class: "TenantAjax",
            action: "residentialLeaseAgreement",
            id: paramtId
        },
        success: function (response) {
            var data = $.parseJSON(response);

            if (data.status == "success") {
                $(".TENANTNAME").text(data.data.leaseData.TENANTNAME);
                $(".LEASESTARTDATE").text(data.data.leaseData.LEASESTARTDATE);
                $(".LEASEENDDATE").text(data.data.leaseData.LEASEENDDATE);
                $(".ALLADDRESS").text(data.data.leaseData.ALLADDRESS);
                $(".RENTAMOUNT").text(data.data.leaseData.RENTAMOUNT);
                $(".GRADEPERIOD").text(data.data.leaseData.GRADEPERIOD);
                $(".SECURITYDEPOSITE").text(data.data.leaseData.SECURITYDEPOSITE);
                $(".RENTBEFOREDAY").text(data.data.leaseData.RENTBEFOREDAY);
                $(".NOTICEPERIODDAYS").text(data.data.leaseData.NOTICEPERIODDAYS);
                if (data.data.leaseData.COMPANYLOGO != "") {
                    $(".COMPANYLOGO").attr("src",data.data.leaseData.COMPANYLOGO);
                }

                var today = new Date();
                var date = (today.getMonth()+1) +'/'+today.getDate()+'/'+today.getFullYear()+' '+today.getHours()+':'+today.getMinutes();


                if (data.data.getTenantData.status ==  'success'){
                    var tenantHtml = '';
                    var tenantId = data.data.getTenantData.data['id'];
                    var tenantName = data.data.getTenantData.data['name'];
                    var tenantStatus = data.data.getTenantData.data['status'];
                    var tenant_image = data.data.getTenantData.data['signature_image'];
                    var tenant_text = data.data.getTenantData.data['signature_text'];
                    var updated_at = data.data.getTenantData.data['updated_at'];
                    var styles = data.data.getTenantData.data['style'];

                    var tId = getParameters('ti');
                    if (tId == tenantId){
                        if (tenantStatus == '0'){
                            var xsign = '<a class="tenantSigHere" style="background-color:#FFED87 !important;" title="Click here to sign" href="javascript:;">X (sign here)</a>';
                            var xdate ='<span style="font-size: 13px;">Date: <span style="background-color:#FFED87 !important;" href="javascript:;">'+date+'</span></span>';
                        }else{
                            if (tenant_image != ""){
                                var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+tenant_image+'"></b><br>';
                            }else{
                                var tenant_textSplit = tenant_text.split(",");
                                var xsign ='<b style="'+styles+'">'+tenant_textSplit[0]+'</b><br>';
                            }
                            var xdate ='<span style="font-size: 13px;"><span style="background-color:#FFED87 !important;" href="javascript:;">'+updated_at+'</span></span>';
                        }
                    }else{
                        if (tenantStatus == '0'){
                            var xsign ='{TenantSignature'+tenantId+'}';
                            var xdate ='{TenantDate}';
                        }else{
                            if (styles != undefined && styles != "") {
                                styles = styles;
                            }else{
                                styles = "float:left;width:150px;clear:both;";
                            }
                            if(tenant_image != ""){
                                var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+tenant_image+'"></b><br>';
                            }else{
                                var tenant_textSplit = tenant_text.split(",");
                                var xsign ='<b style="'+styles+'">'+tenant_textSplit[0]+'</b><br>';
                            }
                            var xdate ='<span style="font-size: 13px;">Date: <span style="background-color:#FFED87 !important;" href="javascript:;">'+updated_at+'</span></span>';
                        }
                    }
                    var tenantClass = "tenantSigHere-"+tenantId;
                    var tenantDate = "tenantSigDateHere-"+tenantId;
                    tenantHtml +='<b style="float:left;width:10%;clear:both;" class="'+tenantClass+'" id="tenantSigHere">'+xsign+'</b>';
                    tenantHtml += '<p style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;">'+tenantName+' :</p>';
                    tenantHtml += '<p class="'+tenantDate+'" id="tenantSigDateHere" style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;"> ';
                    tenantHtml += xdate+'</p><br>';

                    $(".tenantSection").html(tenantHtml);
                    var parentId = getParameters('id');
                    var tId = getParameters('ti');

                    if (parentId != "" && tId != ""){
                        var responseData = getSignatureInfo(tId, parentId,'T');
                    }
                }

                if (data.data.additionalTenantData.data.length > 0){
                    var additionalHtml = '';
                    for (var i = 0; i < data.data.additionalTenantData.data.length; i++){
                        var additionalId = data.data.additionalTenantData.data[i]['id'];
                        var additionalName = data.data.additionalTenantData.data[i]['name'];
                        var additionalStatus = data.data.additionalTenantData.data[i]['status'];
                        var additional_image = data.data.additionalTenantData.data[i]['signature_image'];
                        var additional_text = data.data.additionalTenantData.data[i]['signature_text'];
                        var updated_at = data.data.additionalTenantData.data[i]['updated_at'];
                        var styles = data.data.additionalTenantData.data[i]['style'];
                        if (styles != "" && styles != undefined) {
                            styles = styles;
                        }else{
                            styles = "float:left;width:150px;clear:both;";
                        }

                        var aId = getParameters('ai');
                        if (aId == additionalId){
                            if (additionalStatus == '0'){
                                var xsign = '<a class="addTeantSigHere" style="background-color:#FFED87 !important;" title="Click here to sign" href="javascript:;">X (sign here)</a>';
                                var xdate ='<span style="font-size: 13px;">Date: <span style="background-color:#FFED87 !important;" href="javascript:;">'+date+'</span></span>';
                            }else{
                                if (additional_image != ""){
                                    var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+additional_image+'"></b><br>';
                                }else{
                                    var additional_textSplit = additional_text.split(",");
                                    var xsign ='<b style="'+styles+'">'+additional_textSplit[0]+'</b><br>';
                                }
                                var xdate ='<span style="font-size: 13px;"> <span style="background-color:#FFED87 !important;" href="javascript:;">'+updated_at+'</span></span>';
                            }
                        }else{
                            if (additionalStatus == '0'){
                                var xsign ='{AdditionalTenantSignature'+additionalId+'}';
                                var xdate ='{AdditionalTenantDate}';
                            }else{
                                if(additional_image != ""){
                                    var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+additional_image+'"></b><br>';
                                }else{
                                    var additional_textSplit = additional_text.split(",");
                                    var xsign ='<b style="'+styles+'">'+additional_textSplit[0]+'</b><br>';
                                }

                                var xdate ='<span style="font-size: 13px;"> <span style="background-color:#FFED87 !important;" href="javascript:;">'+updated_at+'</span></span>';
                            }
                        }

                        var additionalClass = "addTeantSigHere-"+additionalId;
                        var additionalDate = "addTeantSigDateHere-"+additionalId;
                        additionalHtml +='<b style="float:left;width:10%;clear:both;" class="'+additionalClass+'" id="addTeantSigHere">'+xsign+'</b>';
                        additionalHtml += '<p style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;">'+additionalName+' :</p>';
                        additionalHtml += '<p class="'+additionalDate+'" id="addTeantSigDateHere" style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;"> Date :';
                        additionalHtml += xdate+'</p><br>';
                    }
                    $(".addTenantSection").html(additionalHtml);
                    var parentId = getParameters('id');
                    var aId = getParameters('ai');

                    if (parentId != "" && aId != ""){
                        var responseData = getSignatureInfo(aId, parentId,'AT');
                    }
                }

                if (data.data.guarantorsData.data.length > 0){
                    var guarantorHtml = '';
                    for (var i = 0; i < data.data.guarantorsData.data.length; i++){
                        var guarantorId = data.data.guarantorsData.data[i]['id'];
                        var guarantorName = data.data.guarantorsData.data[i]['name'];
                        var guarantorStatus = data.data.guarantorsData.data[i]['status'];
                        var gsignature_image = data.data.guarantorsData.data[i]['signature_image'];
                        var gsignature_text = data.data.guarantorsData.data[i]['signature_text'];
                        var gupdated_at = data.data.guarantorsData.data[i]['updated_at'];
                        var styles = data.data.guarantorsData.data[i]['style'];
                        if (styles != "" && styles != undefined) {
                            styles = styles;
                        }else{
                            styles = "float:left;width:150px;clear:both;";
                        }

                        var gId = getParameters('gi');
                        if (gId == guarantorId){
                            if (guarantorStatus == '0'){
                                var xsign = '<a class="guarantSigHere" style="background-color:#FFED87 !important;" title="Click here to sign" href="javascript:;">X (sign here)</a>';
                                var xdate ='<span style="font-size: 13px;">Date: <span style="background-color:#FFED87 !important;" href="javascript:;">'+date+'</span></span>';
                            }else{
                                if (gsignature_image != ""){
                                    var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+gsignature_image+'"></b><br>';
                                }else{
                                    var gsignature_textSplit = gsignature_text.split(",");
                                    var xsign ='<b style="'+styles+'">'+gsignature_textSplit[0]+'</b><br>';
                                }
                                var xdate ='<span style="font-size: 13px;"> <span style="background-color:#FFED87 !important;" href="javascript:;">'+gupdated_at+'</span></span>';
                            }
                        }else{
                            if (guarantorStatus == '0'){
                                var xsign ='{GuarantorSignature'+guarantorId+'}';
                                var xdate ='{GuarantorDate}';
                            }else{
                                if(gsignature_image != ""){
                                    var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+gsignature_image+'"></b><br>';
                                }else{
                                    var gsignature_textSplit = gsignature_text.split(",");
                                    var xsign ='<b style="'+styles+'">'+gsignature_textSplit[0]+'</b><br>';
                                }
                                var xdate ='<span style="font-size: 13px;"> <span style="background-color:#FFED87 !important;" href="javascript:;">'+gupdated_at+'</span></span>';
                            }
                        }

                        var guarantClass = "guarantSigHere-"+guarantorId;
                        var guarantDate = "guarantDateHere-"+guarantorId;
                        guarantorHtml +='<b style="float:left;width:10%;clear:both;" class="'+guarantClass+'" id="guarantSigHere">'+xsign+'</b>';
                        guarantorHtml += '<p style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;">'+guarantorName+' :</p>';
                        guarantorHtml += '<p class="'+guarantDate+'" id="guarantSigHereDate" style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;"> Date :';
                        guarantorHtml += xdate+'</p><br>';
                    }
                    $(".guarantorSection").html(guarantorHtml);
                    var parentId = getParameters('id');
                    var gId = getParameters('gi');

                    if (parentId != "" && gId != ""){
                        var responseData = getSignatureInfo(gId, parentId,'G');
                    }
                }


                if (data.data.managersData.data.length > 0){
                    var ownerHtml = '';

                    for (var i = 0; i < data.data.managersData.data.length; i++){
                        var ownerId = data.data.managersData.data[i]['id'];
                        var ownerName = data.data.managersData.data[i]['name'];
                        var ownerStatus = data.data.managersData.data[i]['status'];
                        var owner_image = data.data.managersData.data[i]['signature_image'];
                        var owner_text = data.data.managersData.data[i]['signature_text'];
                        var updated_at = data.data.managersData.data[i]['updated_at'];
                        var styles = data.data.managersData.data[i]['style'];
                        if (styles != "" && styles != undefined) {
                            styles = styles;
                        }else{
                            styles = "float:left;width:150px;clear:both;";
                        }

                        var oId = getParameters('oi');
                        if (oId == ownerId){
                            if (ownerStatus == '0'){
                                var xsign = '<a class="ownerSigHere" style="background-color:#FFED87 !important;" title="Click here to sign" href="javascript:;">X (sign here)</a>';
                                var xdate ='<span style="font-size: 13px;">Date: <span style="background-color:#FFED87 !important;" href="javascript:;">'+date+'</span></span>';
                            }else{
                                if (owner_image != ""){
                                    var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+owner_image+'"></b><br>';
                                }else{
                                    var owner_textSplit = owner_text.split(",");
                                    var xsign ='<b style="'+styles+'">'+owner_textSplit[0]+'</b><br>';
                                }

                                var xdate ='<span style="font-size: 13px;"> <span style="background-color:#FFED87 !important;" href="javascript:;">'+updated_at+'</span></span>';
                            }
                        }else{
                            if (ownerStatus == '0'){
                                var xsign ='{ManagerSignature'+ownerId+'}';
                                var xdate ='{ManagerDate}';
                            }else{
                                if(owner_image != ""){
                                    var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+owner_image+'"></b><br>';
                                }else{
                                    var owner_textSplit = owner_text.split(",");
                                    var xsign ='<b style="'+styles+'">'+owner_textSplit[0]+'</b><br>';
                                }
                                var xdate ='<span style="font-size: 13px;"> <span style="background-color:#FFED87 !important;" href="javascript:;">'+updated_at+'</span></span>';
                            }
                        }

                        var ownerClass = "ownerSigHere-"+ownerId;
                        var ownerDate = "ownerDateHere-"+ownerId;
                        ownerHtml +='<b style="float:left;width:10%;clear:both;" class="'+ownerClass+'" id="ownerSigHere">'+xsign+'</b>';
                        ownerHtml += '<p style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;">'+ownerName+' :</p>';
                        ownerHtml += '<p class="'+ownerDate+'" id="ownerSigHereDate" style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;"> Date :';
                        ownerHtml += xdate+'</p><br>';
                    }
                    $(".ownerSection").html(ownerHtml);
                    var parentId = getParameters('id');
                    var oId = getParameters('oi');

                    if (parentId != "" && oId != ""){
                        var responseData = getSignatureInfo(oId, parentId,'O');
                    }

                    $(".owner-manger-block").text("MANAGER [by Agent under Property Management Agreement]:");
                }else{
                    if (data.data.ownersData.data.length > 0){
                        var ownerHtml = '';

                        for (var i = 0; i < data.data.ownersData.data.length; i++){
                            var ownerId = data.data.ownersData.data[i]['id'];
                            var ownerName = data.data.ownersData.data[i]['name'];
                            var ownerStatus = data.data.ownersData.data[i]['status'];
                            var owner_image = data.data.ownersData.data[i]['signature_image'];
                            var owner_text = data.data.ownersData.data[i]['signature_text'];
                            var updated_at = data.data.ownersData.data[i]['updated_at'];
                            var styles = data.data.ownersData.data[i]['style'];
                            if (styles != "" && styles != undefined) {
                                styles = styles;
                            }else{
                                styles = "float:left;width:150px;clear:both;";
                            }

                            var oId = getParameters('oi');
                            if (oId == ownerId){
                                if (ownerStatus == '0'){
                                    var xsign = '<a class="ownerSigHere" style="background-color:#FFED87 !important;" title="Click here to sign" href="javascript:;">X (sign here)</a>';
                                    var xdate ='<span style="font-size: 13px;">Date: <span style="background-color:#FFED87 !important;" href="javascript:;">'+date+'</span></span>';
                                }else{
                                    if (owner_image != ""){
                                        var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+owner_image+'"></b><br>';
                                    }else{
                                        var owner_textSplit = owner_text.split(",");
                                        var xsign ='<b style="'+styles+'">'+owner_textSplit[0]+'</b><br>';
                                    }

                                    var xdate ='<span style="font-size: 13px;"> <span style="background-color:#FFED87 !important;" href="javascript:;">'+updated_at+'</span></span>';
                                }
                            }else{
                                if (ownerStatus == '0'){
                                    var xsign ='{OwnerSignature'+ownerId+'}';
                                    var xdate ='{OwnerDate}';
                                }else{
                                    if(owner_image != ""){
                                        var xsign ='<b style="'+styles+'"><img style="width: 100%;" src="'+owner_image+'"></b><br>';
                                    }else{
                                        var owner_textSplit = owner_text.split(",");
                                        var xsign ='<b style="'+styles+'">'+owner_textSplit[0]+'</b><br>';
                                    }
                                    var xdate ='<span style="font-size: 13px;"> <span style="background-color:#FFED87 !important;" href="javascript:;">'+updated_at+'</span></span>';
                                }
                            }

                            var ownerClass = "ownerSigHere-"+ownerId;
                            var ownerDate = "ownerDateHere-"+ownerId;
                            ownerHtml +='<b style="float:left;width:10%;clear:both;" class="'+ownerClass+'" id="ownerSigHere">'+xsign+'</b>';
                            ownerHtml += '<p style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;">'+ownerName+' :</p>';
                            ownerHtml += '<p class="'+ownerDate+'" id="ownerSigHereDate" style="padding: 0px;margin: 0px;font-size: 10px;text-align: left;clear:both;"> Date :';
                            ownerHtml += xdate+'</p><br>';
                        }
                        $(".ownerSection").html(ownerHtml);
                        var parentId = getParameters('id');
                        var oId = getParameters('oi');

                        if (parentId != "" && oId != ""){
                            var responseData = getSignatureInfo(oId, parentId,'O');
                        }
                    }

                    $(".owner-manger-block").text("LANDLORD [by Agent under Property Management Agreement]:");
                }


                if (data.data.pm.company_name != ""){
                    $(".pmname").text(data.data.pm.company_name);
                }
                if (data.data.pm.phone_number != ""){
                    $(".pmphone").text(data.data.pm.phone_number);
                }




            } else if (data.status == "error") {
                //toastr.error(data.message);
            } else {
                //toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function applyFont(font, classname) {
    font = font.replace(/\+/g, ' ');
    font = font.split(':');
    var fontFamily = font[0];
    var fontWeight = font[1] || 400;
    $(classname).css({fontFamily:"'"+fontFamily+"'", fontWeight:fontWeight});
    $(".typingtext").attr("data-font",fontFamily);
}

function getSignatureInfo(userId, parentId, type) {
    $.ajax({
        type: 'post',
        url: '/EsignatureUser/getSignatureInfo',
        data: {
            class: "GenerateLease",
            action: "getSignatureInfo",
            user_id: userId,
            parentId: parentId,
            type: type
        },
        success: function (response) {
            var data = $.parseJSON(response);
            var today = new Date();
            var date = (today.getMonth()+1) +'/'+today.getDate()+'/'+today.getFullYear()+' '+today.getHours()+':'+today.getMinutes();
            if (data.status == 'success'){
                if (data.data == ""){
                    if (data.checkData.type == 'G'){
                        var xsign = '<a class="guarantSigHere" style="background-color:#FFED87 !important;" title="Click here to sign" href="javascript:;">X (sign here)</a>';
                        var classId = "guarantSigHere-"+data.checkData.user_id;
                        $('.'+classId).html(xsign);
                        var dateHtml ='<span style="font-size: 13px;">Date: <span style="background-color:#FFED87 !important;" href="javascript:;">'+date+'</span></span>';
                        $(".guarantDateHere-"+data.checkData.user_id).html(dateHtml);
                    }

                    if (data.checkData.type == 'O'){
                        var xsign = '<a class="ownerSigHere" style="background-color:#FFED87 !important;" title="Click here to sign" href="javascript:;">X (sign here)</a>';
                        var classId = "ownerSigHere-"+data.checkData.user_id;
                        $('.'+classId).html(xsign);
                        var dateHtml ='<span style="font-size: 13px;">Date: <span style="background-color:#FFED87 !important;" href="javascript:;">'+date+'</span></span>';
                        $(".ownerDateHere-"+data.checkData.user_id).html(dateHtml);
                    }

                    if (data.checkData.type == 'AT'){
                        var xsign = '<a class="addTeantSigHere" style="background-color:#FFED87 !important;" title="Click here to sign" href="javascript:;">X (sign here)</a>';
                        var classId = "addTeantSigHere-"+data.checkData.user_id;
                        $('.'+classId).html(xsign);
                        var dateHtml ='<span style="font-size: 13px;">Date: <span style="background-color:#FFED87 !important;" href="javascript:;">'+date+'</span></span>';
                        $(".addTeantSigDateHere-"+data.checkData.user_id).html(dateHtml);
                    }

                    if (data.checkData.type == 'T'){
                        var xsign = '<a class="tenantSigHere" style="background-color:#FFED87 !important;" title="Click here to sign" href="javascript:;">X (sign here)</a>';
                        $('#tenantSigHere').html(xsign);
                        var dateHtml ='<span style="font-size: 13px;"><span style="background-color:#FFED87 !important;" href="javascript:;">'+date+'</span></span>';
                        $("#tenantSigDateHere").html(dateHtml);
                    }

                }
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function getParameters(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
}

function sendEmailToGuarantor(userId,userType) {
    $.ajax({
        type: 'post',
        url: '/EsignatureUser/sendESigatureMail',
        data: {
            class: "GenerateLease",
            action: "sendESigatureMail",
            user_id: userId,
            type: userType
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                toastr.success(data.message);
            } else if (data.status == "error") {
                //toastr.error(data.message);
            } else {
                //toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
