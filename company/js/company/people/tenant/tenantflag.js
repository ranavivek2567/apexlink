$(document).ready(function () {

    $('.flag-container #flagform11 input[name="date"]').datepicker({
        dateFormat: jsDateFomat
    });
    var currentDate = $.datepicker.formatDate(jsDateFomat, new Date());
    $('.flag-container #flagform11 input[name="date"]').val(currentDate);

    jQuery('.phone_format').mask('000-000-0000', {reverse: true});

    $(document).on("click",".flag-container .addbtnflag11",function(){
        $(this).hide();
        $("input[name='flag_reason']").val("");
        $(".flag-container #flagform11 .saveflag").text("Save");
        $(".flag-container #flagform11").show();
        var tenantId = $(".tenant_id").val();
        getFlagInfo(tenantId);
    });
    if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }
    $(document).on("click",".flag-container #flagform11 .grey-btn",function(){
        $('.flag-container .addbtnflag11').show();
        $(".flag-container #flagform11").hide();
    });

    $(document).on("click",".flag-container #flagform11 .saveflag",function(e){
        e.preventDefault();
        $(".flag-container #flagform11").trigger('submit');
        return false;
    });

    $(document).on("submit",".flag-container #flagform11", function(e){
        e.preventDefault();
        saveFlagInfo();
        return false;
    });
    jqGridFlags('All');

    $(document).on("change", "#flaglistingtable .select_options", function (e) {
        e.preventDefault();
        var tenantid = $(".tenant_id").val();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        switch (action) {
            case "Edit":
                flagChangeOption('getRecord',id);
                break;
            case "Delete":
                bootbox.confirm("Are you sure you want to delete this Flag?", function (result) {
                    if (result == true) {
                        flagChangeOption('deleted_at',id);
                    }
                });
                break;
            case "Completed":
                flagChangeOption('completed',id);
                break;
            default:
                window.location.href = window.location.href;
        }
    });
});

function flagChangeOption(fieldType,id) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/flagChangeOption',
        data: {
            class: "TenantAjax",
            action: "flagChangeOption",
            recordid: id,
            fieldType: fieldType
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                if (fieldType == "getRecord"){
                    if (response.data.country.length > 0){
                        var cnt = response.data.country;
                        var countryOption = "<option value='0'>Select</option>";
                        $.each(cnt, function (key, value) {
                            if (value.id == response.data.record.country_code){
                                countryOption += "<option value='"+value.id+"' data-id='"+value.code+"' selected>"+value.name+" ("+value.code+")"+"</option>";
                            }else{
                                countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                            }
                        });
                        $(".flag-container #flagform11 select[name='country_code']").html(countryOption);
                    }
                    var rcrd = response.data.record;
                    $(".flag-container .addbtnflag11").hide();
                    //$(".flag-container #flagform").trigger('reset');
                    $(".flag-container #flagform11 input[name='flagged_by_name']").val(rcrd.flag_by);
                    $(".flag-container #flagform11 input[name='date']").val(rcrd.date);
               //     $(".flag-container #flagform11 input[name='flag_name']").val(rcrd.flag_name);
                    $(".flag-container #flagform11 input[name='phone_number']").val(rcrd.flag_phone_number);
                    $(".flag-container #flagform11 input[name='flag_reason']").val(rcrd.flag_reason);
                    $(".flag-container #flagform11 select[name='status']").val(rcrd.completed);
                    $(".flag-container #flagform11 textarea[name='note']").val(rcrd.flag_note);
                    $(".flag-container #flagform11 input[name='record_id']").val(id);
                    $(".flag-container #flagform11 .saveflag").text("Update");
                    $(".flag-container #flagform11").show();

                }else{
                    getFlagCount();
                    var returnRes = update_users_flag('users',$(".tenant_id").val());
                    if (returnRes.status == "success" && returnRes.code == 200){
                        localStorage.setItem('rowcolorTenant', 'rowColor');
                        localStorage.setItem("Message", response.message);
                        setTimeout(function () {
                            window.location.href = window.location.origin + '/Tenantlisting/Tenantlisting/';
                        },1000);
                    }
                      //  toastr.success(response.message);
                    $('#flaglistingtable').trigger( 'reloadGrid' );
                    if (fieldType != "deleted_at"){
                        onTop(true,'flaglistingtable');
                    }
                }

            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

function getFlagInfo(tenantId){
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getFlagInfo',
        data: {
            class: "TenantAjax",
            action: "getFlagInfo",
            id: tenantId
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var currentDate = $.datepicker.formatDate(jsDateFomat, new Date());
                $('.flag-container #flagform11 input[name="date"]').val(currentDate);

              //;  $(".flag-container #flagform11 input[name='flagged_by_name']").val(data.data.propid.fullname);
                if (data.data.country.length > 0){
                    var countryOption = "<option value='0'>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        if (value.id == '220'){
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"' selected>"+value.name+" ("+value.code+")"+"</option>";
                        }else{
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                        }
                    });
                    $(".flag-container #flagform11 select[name='country_code']").html(countryOption);
                }
                var contactNumber = "";
                if (data.data.propid.phone_number != ""){
                    contactNumber = data.data.propid.phone_number;
                }else if(data.data.propid.mobile_number != ""){
                    contactNumber = data.data.propid.mobile_number;
                }else{
                    contactNumber = "";
                }
                default_form_data = $("#flagform11").serializeArray();
                
               // $(".flag-container #flagform11 input[name='phone_number']").val(contactNumber);

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


$(document).on("click",".resetHoa",function(){
  var defaultIgnoreArray = [];
 


     resetEditForm("#hoa_form",[],true,default_form_data,[]);
 

    

});

function saveFlagInfo() {
    var form = $(".flag-container #flagform11").serializeArray();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveFlagData',
        data: {
            class: "TenantAjax",
            action: "saveFlagData",
            form: form
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                getFlagCount();
                $(".flag-container #flagform11 .grey-btn").trigger("click");
                toastr.success(data.message);
                localStorage.setItem("Message", 'Record added successfully.');
                localStorage.setItem('rowcolor_vendor', 'rowColor');
                localStorage.setItem('rowcolorTenant', 'rowColor');
                window.location.href = window.location.origin + '/Tenantlisting/Tenantlisting/';
                $('#flaglistingtable').trigger( 'reloadGrid' );
                onTop(true,'flaglistingtable');
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

}

function executeHtml(){

}


function jqGridFlags(status) {
    var tenantId = $(".tenant_id").val();
    var table = 'flags';
    var columns = ['Date','Flag Name', 'Phone Number', 'Flag Reason','Completed','Note','Action'];
    var select_column = ['Edit','Delete','Completed'];
    //var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_phone'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];
    var extra_where = [];
    var extra_where = [{column:'object_type',value:'tenant',condition:'=',table:'flags'},{column:'object_id',value:tenantId,condition:'=',table:'flags'}];
    var pagination=[];
    var columns_options = [
        {name:'Date',index:'date',align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Flag Name',index:'flag_name',searchoptions: {sopt: conditions},table:table},
        {name:'Phone Number',index:'flag_phone_number', align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Flag Reason',index:'flag_reason', align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed', align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
        {name:'Note',index:'flag_note', align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'', title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},

    ];
    var ignore_array = [];
    jQuery("#flaglistingtable").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "flags",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:true,
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:0,left:400,drag:true,resize:false} // search options
    );
}
function statusFormatter (cellValue, options, rowObject){
    console.log("hereiam", cellValue);
    if (cellValue == 0)
        return "No";
    else if(cellValue == '1')
        return "Yes";
    else
        return 'No';

}

function dateFormatter (cellValue, options, rowObject){
    if (cellValue == 0)
        return "No";
    else if(cellValue == '1')
        return "Yes";
    else
        return 'No';
}




$(document).on("click",".resetFlag",function(){
  var defaultIgnoreArray = [];
  resetEditForm("#flagform11",[],true,default_form_data,[]);
 
});

