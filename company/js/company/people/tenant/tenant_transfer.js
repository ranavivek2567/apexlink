$(document).ready(function() {
    $(document).on("click",".lease_activate",function(){
toastr.error("Please fill transfer details first");
    });
    initializeLeasePage();
    tenant_lease_generate();
    getTenant();
    getCharges();
    var tenant_id = $("input[name='tenantapplytaxid']").val();
    $.ajax({
        url:'/tenantAjax?action=chargeDetails&class=tenantAjax&tenant_id='+tenant_id,
        type: 'GET',
        success: function (data) {
            $('.chargeData').html(data);
        },
        cache: false,
        contentType: false,
        processData: false
    });
    $.ajax({
        url:'/tenantAjax?action=chargeCode&class=tenantAjax',
        type: 'GET',
        success: function (data) {
            $('.tax_chargeCode').append(data);
        },
        cache: false,
        contentType: false,
        processData: false
    });

    function getTenant(){

        var tenant_id = $("input[name='tenantapplytaxid']").val();
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "getTenant",
                tenant_id:tenant_id

            },
            success: function (response) {

                var response = JSON.parse(response);

                var tenant_id = $("input[name='tenantapplytaxid']").val();

                if(response.data.all.last_name !== null) {

                    var name = response.data.all.first_name + " " + response.data.all.middle_name + " " + response.data.all.last_name;

                }
                else {
                    console.log('2');
                    var name = response.data.all.first_name ;
                }
                $("#tenant_name").text(name);
                var prop_name=response.data.property.property_name;
                $("#property_name").text(prop_name);
                $("#unit_name").text(response.data.unit.unit_prefix+"-"+response.data.unit.unit_no);
                $("#balance_due").text(1000);
                $("#rent_id").text(response.data.all.rent_amount);
                $("#sec_deposit").text(response.data.all.security_deposite);
                $("#start_date").text(response.data.all.start_date);
                $("#phone_num").text(response.data.all.phone_number);
                $("#company_name").text(response.data.all.company_name);
                 $("input[name='tenant_first_name']").val(response.data.all.first_name+" "+response.data.all.middle_name);
                $("input[name='tenant_last_name']").val(response.data.all.last_name);
                $("input[name='tenant_email']").val(response.data.all.email);
                $("input[name='tenant_phone_number']").val(response.data.all.phone_number);
                $("input[name='tenant_movein_date']").val(response.data.all.move_in);
                $("input[name='tenant_ssnid']").val(response.data.all.ssn_sin_id);
                $("input[name='tenant_id']").val(tenant_id);
           /* <option value="0">Select</option>
                    <option value="1">Dr.</option>
                    <option value="2">Mr.</option>
                    <option value="3">Mrs.</option>
                    <option value="4">Mr. &amp; Mrs.</option>
                    <option value="5">Ms.</option>
                    <option value="6">Sir.</option>
                    <option value="7">Madam</option>
                    <option value="8">Brother</option>
                    <option value="9">Sister</option>
                    <option value="10">Father</option>
                    <option value="11">Mother</option>*/
                if (response.data.all.salutation=='Mr.')
                {
                    selected_val="2";
                }
                if(response.data.all.salutation=='Dr.')
                {
                    selected_val="1";
                }
                if(response.data.all.salutation=='Mrs.')
                {
                    selected_val="3";
                }
                selected_phone=response.data.all.phone_type;
                $("select[name='tenant_salutation'] option[value='" + selected_val + "']").attr('selected', 'selected');
                $(".primary-tenant-phone-row-lease select[name='tenant_phone_type'] option[value='" + selected_phone + "']").attr('selected', 'selected');
                $("select[name='tenant_carrier'] option[value='" + response.data.all.carrier + "']").attr('selected', 'selected');
          /*  <option value="1">Select Type</option>
                <option value="2">Mobile</option>
                    <option value="3">Work</option>
                    <option value="4">Fax</option>
                    <option value="5">Home</option>
                    <option value="6">Other</option>*/

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }
    getPortfolio();
    function getPortfolio(){
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "getPortfolio"
            },
            success: function (response) {
                var html="";
                var response = JSON.parse(response)
                html += ' <option value="' + '0' + '">' + 'Select'+ '</option>';
                for (var i = 0; i < response.data.user.length; i++)
                {
                    html += ' <option value="' + response.data.user[i].id + '">' + response.data.user[i].portfolio_name + '</option>';
                }
                $("#portfolio").html(html);


            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }
    $(document).on("change","#portfolio",function(){
        var conceptName = $('#portfolio').find(":selected").text();
        getPropName(conceptName);
    });

    function getPropName(conceptName){
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "getPropName",
                conceptName: conceptName
            },
            success: function (response) {
                var html="";
                var response = JSON.parse(response);
                html += ' <option value="' + '0' + '">' + 'Select'+ '</option>';
                for (var i = 0; i < response.data.selected_data.length; i++)

                {
                    html += ' <option value="' + response.data.selected_data[i].id + '">' + response.data.selected_data[i].property_name + '</option>';
                }
                $("#prop_name").html(html);

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }
    $(document).on("change","#prop_name",function(){
        var prop_id = $('#prop_name').find(":selected").val();
        getBuilding(prop_id);
    });
    function getBuilding(prop_id){
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "getBuilding",
                prop_id:prop_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var html="";
                html += ' <option value="' + '0' + '">' + 'Select'+ '</option>';
                for (var i = 0; i < response.data.user.length; i++)

                {
                    html += ' <option value="' + response.data.user[i].id + '">' + response.data.user[i].building_name + '</option>';
                }
                $("#building_det").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }


    getUnit()
    function getUnit(){

        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "getUnit",

            },
            success: function (response) {
                var response = JSON.parse(response);
                var html="";
                html += ' <option value="' + '0' + '">' + 'Select'+ '</option>';
                for (var i = 0; i < response.data.user.length; i++)

                {
                    html += ' <option value="' + response.data.user[i].id + '">' + response.data.user[i].unit_type + '</option>';
                }
                $("#unit_type_id").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
        var html="";
        for (var i = 1; i < 101; i++)
        {
            html +=  ' <option value="' + [i] + '">' + [i] + '</option>';
        }
        $("#Bedrooms").html(html);
    var html1="";
    var numberBathroom = ["1","1½","2","2½","3","3½","4","4½","5","5½","6","6½","7","7½","8","8½","9","9½","10","10½","11","11½","12","12½","13","13½","14","14½","15","15½","16","16½","17","17½","18","18½","19","19½","20","20½","21","21½","22","22½","23","23½","24","24½","25","25½","26","26½","27","27½","28","28½","29","29½","30","30½","31","31½","32","32½","33","33½","34","34½","35","35½","36","36½","37","37½","38","38½","39","39½","40","40½","41","41½","42","42½","43","43½","44","44½","45","45½","46","46½","47","47½","48","48½","49","49½","50","50½","51","51½","52","52½","53","53½","54","54½","55","55½","56","56½","57","57½","58","58½","59","59½","60","60½","61","61½","62","62½","63","63½","64","64½","65","65½","66","66½","67","67½","68","68½","69","69½","70","70½","71","71½","72","72½","73","73½","74","74½","75","75½","76","76½","77","77½","78","78½","79","79½","80","80½","81","81½","82","82½","83","83½","84","84½","85","85½","86","86½","87","87½","88","88½","89","89½","90","90½","91","91½","92","92½","93","93½","94","94½","95","95½","96","96½","97","97½","98","98½","99","99½","100","100½","Other"];
   for(var j=0;j<numberBathroom.length;j++) {
       html1 += ' <option value="' +numberBathroom[j]+ '">' +numberBathroom[j] + '</option>';
   }
    $("#Bathrooms").html(html1);

    getAmenities()
    function getAmenities(){

        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "getAmenities",

            },
            success: function (response) {
                var response = JSON.parse(response);
                var html="";
                html += '';
                for (var i = 0; i < response.data.user.length; i++)

                {
                    html += ' <option value="' + response.data.user[i].id + '">' + response.data.user[i].name + '</option>';
                }
                $("#Amenities").html(html);
                $('select[name="Amenities"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on("click","#search_btn",function(){
        $("#search_table").show();
        getTable();
    });


    function getTable(){
        var port_id = $('#portfolio').find(":selected").val();
        var prop_id = $('#prop_name').find(":selected").val();
        var build_id = $('#building_det').find(":selected").val();
        var unit_id = $('#unit_type_id').find(":selected").val();
        var unit_type=$('#unit_type_id').find(":selected").val();
        var bedrooms=$('#Bedrooms').find(":selected").val();
        var bathroom=$('#Bathrooms').find(":selected").val();
        var floor=$('#floor').find(":selected").val();

        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "getTable",
                port_id:port_id,
                prop_id:prop_id,
                build_id:build_id,
                unit_id:unit_id,
                unit_type:unit_type,
                bedrooms:bedrooms,
                bathroom:bathroom,
                floor:floor

            },
            success: function (response) {
                var rentCurrSymbol = "("+currencySymbol+")";
                var response = JSON.parse(response);
                var html = "";
                html +=   '<thead>'+
                '<tr>'+
                    '<th scope="col"><center>'+'</center></th>'+
                '<th scope="col"><center>'+'Property'+'</center></th>'+
                    '<th style="display: none" scope="col"><center>'+'Property'+'</center></th>'+
                '<th scope="col"><center>'+'Type'+'</center> </th>'+
                '<th scope="col"><center>'+'Building'+'</center> </th>'+
                    '<th style="display: none" scope="col"><center>'+'Building'+'</center> </th>'+
                '<th scope="col"><center>'+'Unit'+'</center> </th>'+
                '<th style="display: none" scope="col"><center>'+'Unit'+'</center> </th>'+
                '<th scope="col"><center>'+'Status'+'</center> </th>'+
                '<th scope="col"><center>'+'Security Deposit'+rentCurrSymbol+'</center> </th>'+
                '<th scope="col"><center>'+'Rent'+rentCurrSymbol+'</center> </th>'+
                '<th style="display: none" scope="col"><center>'+'Rent'+'</center> </th>'+
                '</tr>'+
                '</thead>';

                if(response.data.user=="")
                {
                    $("#no_records").show();
                }
                else {
                    $("#no_records").hide();
                    for (var i = 0; i < response.data.user.length; i++) {

                        if (response.data.user[i].status == 1) {
                            var status = 'Vacant Available';
                        }
                        html +=
                            '<tr>' +
                            '<td>' + '<input type="radio" name="tenant_trnasfer" id="checked_tenant">' + '</td>' +
                            '<td>' + response.data.user[i].property_name + '</td>' +
                            '<td style="display: none"> ' + response.data.user[i].gpid + '</td>' +
                            '<td>' + response.data.user[i].unit_type + '</td>' +
                            '<td>' + response.data.user[i].building_name + '</td>' +
                            '<td style="display: none">' + response.data.user[i].bid + '</td>' +
                            '<td>' + response.data.user[i].unit_prefix +'-'+response.data.user[i].unit_no+ '</td>' +
                            '<td style="display: none">' + response.data.user[i].udid + '</td>' +
                            '<td>' + status + '</td>' +
                            '<td>' + response.data.user[i].security_deposit + '</td>' +
                            '<td>' + response.data.user[i].base_rent + '</td>' +
                            '<td style="display: none">' + response.data.user[i].utid + '</td>' +
                            '</tr>';
                    }
                }
                    $("#search_records").html(html);

                },

            error:function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    function saveTenantTransfer() {
        var checked=$("input:radio[name=tenant_trnasfer]:checked").val();
        if(checked!=undefined) {
            if(checked=='on') {
                var checked1=$("input:radio[name=tenant_trnasfer]:checked");
                    var property = checked1.parent().next().text();         // Retrieves the text within <td>
                    var property_id = checked1.parent().next().next().text();
                    var type = checked1.parent().next().next().next().text();
                    var building = checked1.parent().next().next().next().next().text();
                    var building_id = checked1.parent().next().next().next().next().next().text();
                    var unit = checked1.parent().next().next().next().next().next().next().text();
                    var unit_id = checked1.parent().next().next().next().next().next().next().next().text();
                    var status = checked1.parent().next().next().next().next().next().next().next().next().text();
                    var secuirty_deposite = checked1.parent().next().next().next().next().next().next().next().next().next().text();
                    var rent = checked1.parent().next().next().next().next().next().next().next().next().next().next().text();
                    var unit_type_id = checked1.parent().next().next().next().next().next().next().next().next().next().next().next().text();
            }
        }
        var tenant_id = $("input[name='tenantapplytaxid']").val();
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "saveTenantTransfer",
                property:property,
                property_id:property_id,
                type:type,
                building:building,
                building_id:building_id,
                unit:unit,
                unit_id:unit_id,
                status:status,
                secuirty_deposite:secuirty_deposite,
                rent:rent,
                unit_type_id:unit_type_id,
                tenant_id:tenant_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    var base_url = window.location.origin;
                    var tenant_id = $("input[name='tenantapplytaxid']").val();
                    window.location.href = base_url + '/Tenant/Transfer';
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('*This field is required');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text('*This field is required');
                    });
                }
                else if (response.code == 503) {
                   toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on("click",".save_transfer",function(){
        var check = jQuery.trim($("#search_records").html());
      var check2=  $("input[name='tenant_trnasfer']:checked").length;
       if(check=="")
       {
           toastr.error("Please search and select a Unit for the transfer.");
       }
       if(check!="" && check2==0){
               toastr.error("Please search and select a Unit for the transfer.");
       }

        if(check!="" && check2==1){

           saveTenantTransfer();
       }
    });

    function tenant_lease_generate(){
        var tenant_id = $("input[name='tenantapplytaxid']").val();
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "tenant_lease_generate",
                tenant_id:tenant_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                var unitName=(response.data.unit.unit_prefix)+'-'+(response.data.unit.unit_no);
                var checkCom=(response.data.companyCheck.tenant_contact);
                console.log('check',checkCom);
                $("#new_prop").text(response.data.property.property_name);
                $("#new_unit").text(unitName);
                $("#new_build").text(response.data.building.building_name);
                var transfer_salutation=(response.data.user.salutation);
                var phone_type=(response.data.user.phone_type);
                var carrier=(response.data.user.carrier);

                if(checkCom!=="") {
                    $(".entity_comapny_hide").show();
                    $(".entity_comapny").val(response.data.user.first_name);
                    $(".entity_comapny_check").attr("checked", true);
                }
                else
                {
                    $(".transfer_firstName").val(response.data.user.first_name);
                    $(".transfer_middleName").val(response.data.user.middle_name);
                    $(".transfer_lastName").val(response.data.user.last_name);
                    $("select[name='transfer_salutation'] option[value='" + transfer_salutation + "']").attr('selected', 'selected');
                }

                $(".transfer_email").val(response.data.user.email);
                $(".transfer_phone_no").val(response.data.user.phone_number);
                $(".transfer_ssn").val(response.data.ssn);
                $(".transfer_rent_amount").val(response.data.transfer.rental_approved);
                $(".transfer_sec_dep").val(response.data.transfer.secuirty_deposite);
                $("select[name='transfer_phoneType'] option[value='" + phone_type + "']").attr('selected', 'selected');
                $("select[name='transfer_carrier'] option[value='" + carrier + "']").attr('selected', 'selected');
                $(".phone-row-section").html(response.data.ids);

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
    $(document).on("click","#tenantTrns",function(){
       /* e.preventDefault();
         transferTenantLease();*/
        $("#addLateFee").trigger('submit');
    });

    $(document).on("submit","#addLateFee",function(e){
        e.preventDefault();
        if($("#addLateFee").validate()){
            taxpssDet();
            transferTenantLease();
            chargesFunc();
        }
        else{
            alert('hi');
        }
    });



    function transferTenantLease(){
        var tenant_id = $("input[name='tenantapplytaxid']").val();
        var formData = $('#transferTenantLease').serializeArray();
        var id_tenFone=$(".id_tenFone").val();

        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "transferTenantLease",
                tenant_id:tenant_id,
                formData:formData,
                id_tenFone:id_tenFone
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var base_url = window.location.origin;
                    var tenant_id = $("input[name='tenantapplytaxid']").val();
                    window.location.href = base_url + '/Tenant/Transfer';
                }
                else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('*This field is required');
                    $.each(response.data, function(key, value) {
                        $('#' + key).text('*This field is required');
                    });
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }
    function taxpssDet(){
        var tenant_id = $("input[name='tenantapplytaxid']").val();
        var formData = $('#tenant_tax_det').serializeArray();
        var notes=$('.transfer_notes').val();
       console.log(formData);
        // console.log(formData);
        $.ajax({
            type: 'post',
            url: '/TenantTransfer-Ajax',
            data: {
                class: "TenantTransferTypeAjax",
                action: "taxpssDet",
                tenant_id:tenant_id,
                formData:formData,
                notes:notes

            },
            success: function (response) {
                var response = JSON.parse(response);

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }

        });
    }

    /*for multiple email textbox*/
    $(document).on("click",".email-plus-sign",function(){
        var emailRowLenght = $(".multipleEmail");
        if (emailRowLenght.length == 2) {

            $(".email-plus-sign .fa-plus-circle").hide();
        }
        var clone = $(".multipleEmail:first").clone();
        clone.find('input[type=text ]').val(''); //harjinder
        clone.find('input[type=text ]').attr('disabled',false); //harjinder
        $(".multipleEmail").first().after(clone);
        $(".multipleEmail:not(:eq(0))  .email-plus-sign .fa-plus-circle").remove();
        $(".multipleEmail:not(:eq(0))  .email-remove-sign .fa-minus-circle").show();

    });

    /*Remove Email Textbox*/
    $(document).on("click",".email-remove-sign",function(){
        var emailRowLenght = $(".multipleEmail");

        if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
            $(".email-plus-sign .fa-plus-circle").show();
        }
        $(this).parents(".multipleEmail").remove();
    });

    /*Remove Email Textbox*/


   /* $(document).on("click",".primary-tenant-phone-row2 .fa-plus-circle",function(){
        var clone = $(".primary-tenant-phone-row2:first").clone();
        clone.find('input[type=text]').val(''); //harjinder
        $(".primary-tenant-phone-row2").first().after(clone);
        $(".primary-tenant-phone-row2:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-tenant-phone-row2:not(:eq(0))  .fa-minus-circle").show();
        var phoneRowLenght = $("#addTenant .primary-tenant-phone-row2");
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        }else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on("click",".primary-tenant-phone-row2 .fa-minus-circle",function(){

        var phoneRowLenght = $(".primary-tenant-phone-row2");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").show();
        }else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-tenant-phone-row2").remove();
    });*/
    $(document).on("click",".phone-row-section .primary-tenant-phone-row2 .fa-plus-circle",function(){
        var clone = $(".phone-row-section .primary-tenant-phone-row2:first").clone();
        console.log(clone);
        clone.find('input[type=text]').val(''); //harjinder
        clone.find('input[type=hidden]').val('');
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        $(".phone-row-section .primary-tenant-phone-row2").first().after(clone);
        $(".phone-row-section .primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").hide();
        $(".phone-row-section .primary-tenant-phone-row2:not(:eq(0)) .fa-minus-circle").show();
        var phoneRowLenght = $(".phone-row-section .primary-tenant-phone-row2");
        console.log(phoneRowLenght.length);
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
            clone.find(".fa-minus-circle").show();
        }else if (phoneRowLenght.length == 3) {
            clone.find(".fa-minus-circle").show();
            $(".phone-row-section .primary-tenant-phone-row2:eq(0) .fa-plus-circle").hide();
        }else{
            $(".phone-row-section .primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on("click",".phone-row-section .primary-tenant-phone-row2 .fa-minus-circle",function(){

        var phoneRowLenght = $(".primary-tenant-phone-row2");
        if (phoneRowLenght.length == 2) {
            $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").show();
        }else if (phoneRowLenght.length == 3) {
            $(".primary-tenant-phone-row2:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-tenant-phone-row2:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-tenant-phone-row2").remove();
    });

    $(document).on("click",".applyOneTime",function(){

        if(this.checked)
        {

            $("#lateFeeCharge-error").remove();
            $('.applyDaily').prop('checked', false);
            $('.oneTimeRadio1').prop('checked', true);

            $(".dailyRadio").attr("disabled", true);
            $(".dailyFeeLabel").attr("disabled", true);
            $(".dailyRadio").attr("disabled", true);
            $(".dailyRentLabel").attr("disabled", true);
            $(".oneTimeRadio1").attr('checked', 'checked');
            $(".oneTimeFeeLabel").removeAttr("disabled");
            $(".oneTimeRadio2").removeAttr("disabled");
            $(".oneTimeRadio1").removeAttr("disabled");
            $('.oneTimeFeeLabel').attr('name', 'lateFeeCharge');
            $('.removeAttr').val('');

        }
        else
        {
            $("#lateFeeCharge-error").remove();
            $(".oneTimeRadio1").prop('checked', false);
            $(".oneTimeRadio2").prop('checked', false);
            $(".oneTimeRadio1").attr('disabled', true);
            $(".oneTimeRadio2").attr('disabled', true);
            $(".oneTimeRentLabel").attr("disabled", true);
            $(".oneTimeFeeLabel").attr("disabled", true);
            $(".oneTimeRentLabel").attr("disabled", true);
            $(".removeAttr").removeAttr("name");
            $('.removeAttr').val('');



        }


    });



    $(document).on("click",".oneTimeRadio",function(){
        if( $(this).is(":checked") ){
            var val = $(this).val();


            if(val=='1')
            {
                $("#lateFeeCharge-error").remove();
                $(".removeAttr").removeAttr("name");
                $('.removeAttr').val('');
                $(".oneTimeRentLabel").attr("disabled", true);
                $(".oneTimeFeeLabel").attr("disabled", false);
                $('.oneTimeFeeLabel').attr('name', 'lateFeeCharge');


            }
            else
            {
                $("#lateFeeCharge-error").remove();
                $(".removeAttr").removeAttr("name");
                $('.removeAttr').val('');
                $(".oneTimeFeeLabel").attr("disabled", true);
                $(".oneTimeRentLabel").attr("disabled", false);
                $('.oneTimeRentLabel').attr('name', 'lateFeeCharge');
            }
        }

    });





    $(document).on("click",".applyDaily",function(){

        if(this.checked)
        {
            $("#lateFeeCharge-error").remove();
            $('.dailyFeeLabel').attr('name', 'lateFeeCharge');
            $('.removeAttr').val('');
            $('.applyOneTime').prop('checked', false);
            $('.dailyRadio1').prop('checked', true);

            $(".oneTimeRadio").attr("disabled", true);
            $(".oneTimeFeeLabel").attr("disabled", true);
            $(".oneTimeRadio").attr("disabled", true);
            $(".oneTimeRentLabel").attr("disabled", true);
            $(".dailyRadio1").attr('checked', 'checked');
            $(".dailyFeeLabel").removeAttr("disabled");
            $(".dailyRadio2").removeAttr("disabled");
            $(".dailyRadio1").removeAttr("disabled");
            $('.removeAttr').val('');

        }
        else
        {
            $("#lateFeeCharge-error").remove();
            $('.removeAttr').val('');
            $(".dailyRadio1").prop('checked', false);
            $('.removeAttr').val('');
            $(".dailyRadio2").prop('checked', false);
            $(".dailyRadio1").attr('disabled', true);
            $(".dailyRadio2").attr('disabled', true);
            $(".dailyRentLabel").attr("disabled", true);
            $(".dailyFeeLabel").attr("disabled", true);
            $(".dailyRentLabel").attr("disabled", true);
            $(".removeAttr").removeAttr("name");



        }


    });



    $(document).on("click",".dailyRadio",function(){
        if( $(this).is(":checked") ){
            var val = $(this).val();

            if(val=='1')
            {
                $("#lateFeeCharge-error").remove();
                $('.removeAttr').val('');
                $(".removeAttr").removeAttr("name");
                $(".dailyRentLabel").attr("disabled", true);
                $(".dailyFeeLabel").attr("disabled", false);
                $('.dailyFeeLabel').attr('name', 'lateFeeCharge');

            }
            else
            {
                $("#lateFeeCharge-error").remove();
                $('.removeAttr').val('');
                $(".removeAttr").removeAttr("name");
                $(".dailyFeeLabel").attr("disabled", true);
                $(".dailyRentLabel").attr("disabled", false);
                $('.dailyRentLabel').attr('name', 'lateFeeCharge');
            }
        }

    });






    $("#addLateFee").validate({
        rules: {
            lateFeeCharge: {
                required:true,
                number:true
            },
            gracePeriod: {
                required:true,
                number:true
            }
        }
    });


});

function chargesFunc() {
    var tenant_id = $("input[name='tenantapplytaxid']").val();


    var form = $('#addLateFee')[0];
    var formData = new FormData(form);
    formData.append('action', 'insertCharge');
    formData.append('class', 'tenantAjax');
    formData.append('tenant_id', tenant_id);
    action = 'insertCharge';
    $.ajax({

        url: '/tenantAjax',
        type: 'POST',
        data: formData,
        success: function (data) {
            var info = JSON.parse(data);
            $("#addLateFee").find("input[type=text]").val("");
            $("#addLateFee").find("input[type=radio]").prop("checked", false);
            $("#addLateFee").find("input[type=checkbox]").prop("checked", false);
            toastr.success(info.message);
        },
        cache: false,
        contentType: false,
        processData: false
    });

}
    $(document).on('click','#add_libraray_file',function(){
        $('#file_library').val('');
        $('#file_library').trigger('click');
    });
    var file_library = [];
    $(document).on('change','#file_library',function(){
        file_library = [];
        $.each(this.files, function (key, value) {
            var type = value['type'];
            var size = isa_convert_bytes_to_specified(value['size'], 'k');
            var upload_url = window.location.origin;
            if(size > 1030) {
                toastr.warning('Please select documents less than 1 mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k')+'kb';
                if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' || type == 'application/pdf' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'text/plain' || type == 'text/xml') {
                    file_library.push(value);
                    var src = '';
                    var reader = new FileReader();
                    $('#file_library_uploads').html('');
                    reader.onload = function (e) {
                        if (type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                            src = upload_url + '/company/images/excel.png';
                        } else if (type == 'application/pdf') {
                            src = upload_url + 'company/images/pdf.png';
                        } else if (type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                            src = upload_url + 'company/images/word_doc_icon.jpg';
                        } else if (type == 'text/plain') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else if (type == 'text/xml') {
                            src = upload_url + 'company/images/notepad.jpg';
                        } else {
                            src = e.target.result;
                        }
                        $("#file_library_uploads").append(
                            '<div class="row" style="margin:20px">' +
                            '<div class="col-sm-12 img-upload-library-div">' +
                            '<div class="col-sm-3"><img class="img-upload-tab' + key + '" width=100 height=100 src=' + src + '></div>' +
                            '<div class="col-sm-3 show-library-list-imgs-name' + key + '">' + value['name'] + '</div>' +
                            '<input type="hidden" class="fileLibraryInput" name="imgName' + key + '"  value="' + value['name'] + '" data_id="' + value['size'] + '">' +
                            '<div class="col-sm-3 show-library-list-imgs-size' + key + '">' + size + '</div>' +
                            '<div class="col-sm-3"><span id=' + key + ' class="delete_pro_img cursor"><button class="btn-warning">Delete</button></span></div></div></div>');
                    };
                    reader.readAsDataURL(value);
                } else {
                    toastr.warning('Please select file with .xlsx | .pdf | .docx | .txt | .xml extension only!');
                }
            }
        });
    });

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}
$(document).on('click','.delete_pro_img',function(){
    $(this).parent().parent().parent('.row').remove();
});

$(document).on('click','#remove_library_file',function(){
    $('#file_library_uploads').html('');
    $('#file_library').val('');
});
$("#addChargeForm").validate({
    rules: {
        chargeCode: {
            required:true
        },
        frequency: {
            required:true
        },
        amount: {
            required:true,
            number:true
        },
        startDate: {
            required:true
        },
        endDate: {
            required:true
        },

    },
    submitHandler: function (e) {

        var tenant_id = $("input[name='tenantapplytaxid']").val();


        var form = $('#addChargeForm')[0];
        var formData = new FormData(form);
        formData.append('action','insertChargeFormData');
        formData.append('class','tenantAjax');
        formData.append('tenant_id',tenant_id);
        action = 'insertCharge';
        $.ajax({

            url:'/tenantAjax',
            type: 'POST',
            data: formData,
            success: function (data) {
                $('.chargeData').html(data);
                $('.chargeForm').hide();
                $("#addChargeButton").show();
                $("#amount").val('');
                $(".tax_chargeCode").val('');

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }



});





function getCharges(){
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getCharges',
        data: {
            class: "TenantAjax",
            action: "getCharges"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var charges = data.data.charges;
                if (charges.length > 0){
                    var chargesOptions = '';
                    for (var i = 0; i < charges.length; i++){
                        chargesOptions += "<option value='"+charges[i].id+"'>"+charges[i].charge_code+"</option>";
                    }
                    $("#addTaxDetails .tax_chargeCode").html(chargesOptions);
                }

            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
$(document).on("click","#addChargeButton",function(){
    $(".chargeForm").fadeIn();
    $("#addChargeButton").hide();


});
$(document).on("click",".savechargecodepopup",function () {
    var chargeCode = $("#chargecode_popup input[name='charge_code']").val();
    var creditValue = $("#chargecode_popup select[name='credit_account']").val();
    var debitValue = $("#chargecode_popup select[name='debit_account']").val();
    var status = $("#chargecode_popup select[name='status']").val();
    var description = $("#chargecode_popup textarea[name='description']").val();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/saveChargeCodes',
        data: {
            class: "TenantAjax",
            action: "saveChargeCodes",
            chargeCode:chargeCode,
            creditValue:creditValue,
            debitValue:debitValue,
            status:status,
            description:description
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                $("#chargecode_popup").trigger('reset');
                $("#addchargescode").modal('hide');
                var lastId = data.last_insert_id;
                var option = "<option value='"+lastId+"' selected>"+data.data.charge_code+"</option>";
                $("#addTaxDetails .tax_chargeCode").append(option);
                $(".tax_chargeCode").append(option);
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });

});
$(document).on("click",".editEndDate",function(){
    var data_id = $(this).attr('data-id');
    $('.endDate_'+data_id).html("<input type='text' name='editDate[]' class='addCalander editCalander'>");
    $(".editCalander").datepicker({
        dateFormat: jsDateFomat,
        setDate: $.datepicker.formatDate(jsDateFomat, new Date())
    });
});


$(document).on("click",".editAmount",function(){
    var data_id = $(this).attr('data-id');
    $('.endAmount_'+data_id).html("<input type='text' name='editAmount[]'>");
});
$(document).on("click",".editChargeInfo",function(){
    var tenant_id = $("input[name='tenantapplytaxid']").val();
    var form = $('#editChargeDateAmount')[0];
    var formData = new FormData(form);
    formData.append('action','editChargeInfo');
    formData.append('class','tenantAjax');
    formData.append('tenant_id',tenant_id);

    action = 'insert';

    $.ajax({

        url:'/tenantAjax',
        type: 'POST',
        data: formData,
        success: function (data) {
            toastr.success("Charges applied successfully!");
            $('.chargeData').html(data);


        },
        cache: false,
        contentType: false,
        processData: false
    });


});

$(document).on("change",".addAdditionalTenant",function(){
    if(this.checked)
    {
        $('.additionalTenantHtml').show();

    }
    else
    {
        $('.additionalTenantHtml').hide();
    }

});
$(document).on("click","#saveAdditionalTenant",function(){

    $("#addAdditionalTenant").validate();

});
$(document).on("click",".AddAdditionalTenant",function () {
    addAdditionalInfo();
});
function addAdditionalInfo(){
    var tenant_id = $("input[name='tenantapplytaxid']").val();
    var formData = $('#transferTenantLease').serializeArray();
    console.log(formData);
    // console.log(formData);
    $.ajax({
        type: 'post',
        url: '/TenantTransfer-Ajax',
        data: {
            class: "TenantTransferTypeAjax",
            action: "addAdditionalInfo",
            tenant_id:tenant_id,
            formData:formData

        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                toastr.success(response.message);
                $("select[name='additional_salutation']").val('Select');
                $("select[name='additional_relationship']").val('0');
                $("select[name='additional_phoneType[]']").val('');
                $("select[name='additional_carrier[]']").val('1');
                $("select[name='additional_countryCode[]']").val('0');
                $("select[name='additional_tenantStatus']").val('0');
                $("input[name='additional_firstname']").val('');
                $("input[name='additional_middlename']").val('');
                $("input[name='additional_lastname']").val('');
                $("input[name='additional_phone[]']").val('');
                $("input[name='additional_email[]']").val('');
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('*This field is required');
                $.each(response.data, function(key, value) {
                    $('#' + key).text('*This field is required');
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }

    });
}
$(document).on("click",".additional-phonerow-plus-sign",function(){

    var clone = $(".additional_phone-row:first").clone();
    clone.find('input[type=text]').val('');
    $(".additional_phone-row").first().after(clone);
    $(".additional-phonerow-plus-sign:not(:eq(0))  .fa-plus-circle").remove();
    $(".additional_phone-row:not(:eq(0))  .additional-phonerow-remove-sign .fa-minus-circle").show();
    var phoneRowLenght = $("#addTenant .additional_phone-row");
    clone.find('.phone_format').mask('000-000-0000', {reverse: true});
    if (phoneRowLenght.length == 2) {
        clone.find(".fa-plus-circle").hide();
    }else if (phoneRowLenght.length == 3) {
        clone.find(".fa-plus-circle").hide();
        $(".additional_phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".additional_phone-row:not(:eq(0)) .fa-plus-circle").show();
    }
});




$(document).on("click",".additional-phonerow-remove-sign",function(){
    var phoneRowLenght = $(".additional_phone-row");
    if (phoneRowLenght.length == 2) {
        $(".additional_phone-row:eq(0) .fa-plus-circle").show();
    }else if (phoneRowLenght.length == 3) {
        $("additional_phone-row:eq(0) .fa-plus-circle").hide();
    }else{
        $(".additional_phone-row:not(:eq(0)) .fa-plus-circle").hide();
    }
    $(this).parents(".additional_phone-row").remove();

});
$(document).on("click",".additional_email-plus-sign",function(){
    var emailRowLenght = $(".additional_multipleEmail");

    if (emailRowLenght.length == 2) {
        $(".additional_email-plus-sign").hide();
    }else{
        $(".additional_email-plus-sign").show();
    }
    var clone = $(".additional_multipleEmail:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".additional_multipleEmail").first().after(clone);
    $(".additional_multipleEmail:not(:eq(0))  .additional_email-plus-sign").remove();
    $(".additional_multipleEmail:not(:eq(0))  .additional_email-remove-sign").show();

});

$(document).on("click",".additional_email-remove-sign",function(){
    var emailRowLenght = $(".additional_multipleEmail");

    if (emailRowLenght.length == 3 || emailRowLenght.length == 2) {
        $(".additional_email-plus-sign").show();
    }else{
        $(".additional_email-plus-sign").hide();
    }
    $(this).parents(".additional_multipleEmail").remove();
});
$(document).on("click",".additional_ssn-plus-sign",function(){
    var clone = $(".additional_multipleSsn:first").clone();
    clone.find('input[type=text]').val(''); //harjinder
    $(".additional_multipleSsn").first().after(clone);

    clone.find(".additional_ssn-plus-sign").remove();
    $(".additional_multipleSsn:not(:eq(0))  .additional_ssn-remove-sign").show();

});

$(document).on("click",".additional_ssn-remove-sign",function(){
    $(this).parents(".additional_multipleSsn").remove();
});
$.ajax({
    type: 'post',
    url: '/Tenantlisting/getInitialData',
    data: {
        class: "TenantAjax",
        action: "getIntialData"
    },
    success: function (response) {
        var data = $.parseJSON(response);
        if (data.status == "success") {

            if (data.data.state.state != ""){
                $("#driverProvince").val(data.data.state.state);
            }

            if (data.data.country.length > 0){
                var countryOption = "<option value='0'>Select</option>";
                $.each(data.data.country, function (key, value) {
                    countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                });
                $('.countycodediv select').html(countryOption);
                $('.emergencycountry').html(countryOption);
            }

            if (data.data.propertylist.length > 0){
                var propertyOption = "<option value=''>Select</option>";
                $.each(data.data.propertylist, function (key, value) {
                    propertyOption += "<option value='"+value.id+"' data-id='"+value.property_id+"'>"+value.property_name+"</option>";
                });
                $('#addTenant #property').html(propertyOption);
            }

            if (data.data.phone_type.length > 0){
                var phoneOption = "";
                $.each(data.data.phone_type, function (key, value) {
                    phoneOption += "<option value='"+value.id+"'>"+value.type+"</option>";
                });
                $('.primary-tenant-phone-row select[name="phoneType[]"]').html(phoneOption);
                $('.addition_tenant_block select[name="additional_phoneType"]').html(phoneOption);
            }

            if (data.data.carrier.length > 0){
                var carrierOption = "";
                $.each(data.data.carrier, function (key, value) {
                    carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                });
                $('.primary-tenant-phone-row select[name="carrier[]"]').html(carrierOption);
                $('.addition_tenant_block select[name="additional_carrier"]').html(carrierOption);
                $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                $('.additional_carrier').html(carrierOption);
            }

            if (data.data.referral.length > 0){
                var referralOption = "";
                $.each(data.data.referral, function (key, value) {
                    referralOption += "<option value='"+value.id+"'>"+value.referral+"</option>";
                });
                $('select[name="referralSource"]').html(referralOption);
                $('.addition_tenant_block select[name="additional_referralSource"]').html(referralOption);
            }

            if (data.data.ethnicity.length > 0){
                var ethnicityOption = "";
                $.each(data.data.ethnicity, function (key, value) {
                    ethnicityOption += "<option value='"+value.id+"'>"+value.title+"</option>";
                });
                $('select[name="ethncity"]').html(ethnicityOption);
                $('.addition_tenant_block select[name="additional_ethncity"]').html(ethnicityOption);
            }

            if (data.data.marital.length > 0){
                var maritalOption = "";
                $.each(data.data.marital, function (key, value) {
                    maritalOption += "<option value='"+value.id+"'>"+value.marital+"</option>";
                });
                $('select[name="maritalStatus"]').html(maritalOption);
                $('.addition_tenant_block select[name="maritalStatus"]').html(maritalOption);
                $('.additional_maritalStatus').html(maritalOption);
            }

            if (data.data.hobbies.length > 0){
                var hobbyOption = "";
                $.each(data.data.hobbies, function (key, value) {
                    hobbyOption += "<option value='"+value.id+"'>"+value.hobby+"</option>";
                });
                $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').html(hobbyOption);
                $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
            }

            if (data.data.veteran.length > 0){
                var veteranOption = "";
                $.each(data.data.veteran, function (key, value) {
                    veteranOption += "<option value='"+value.id+"'>"+value.veteran+"</option>";
                });
                $('select[name="veteranStatus"]').html(veteranOption);
                $('.additional_veteranStatus').html(veteranOption);
            }

            if (data.data.collection_reason.length > 0){
                var reasonOption = "";
                $.each(data.data.collection_reason, function (key, value) {
                    reasonOption += "<option value='"+value.id+"'>"+value.reason+"</option>";
                });
                $('.property_collection select[name="collection_reason"]').html(reasonOption);
            }

            if (data.data.credential_type.length > 0){
                var typeOption = "";
                $.each(data.data.credential_type, function (key, value) {
                    typeOption += "<option value='"+value.id+"'>"+value.credential_type+"</option>";
                });
                $('.tenant-credentials-control select[name="credentialType[]"]').html(typeOption);
            }


        } else if (data.status == "error") {
            toastr.error(data.message);
        } else {
            toastr.error(data.message);
        }
    },
    error: function (data) {
        var errors = $.parseJSON(data.responseText);
        $.each(errors, function (key, value) {
            $('#' + key + '_err').text(value);
        });
    }
});




function initializeLeasePage(){
    var tenantId = $("input[name='tenantapplytaxid']").val();
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getRentInfo',
        data: {
            class: "TenantAjax",
            action: "getRentInfo",
            id: tenantId
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var rentData = data.data.unit.base_rent;
                $("#rent_amount").val(rentData);
                var securityData = data.data.unit.security_deposit;
                $("#security_amount").val(securityData);
            }
        }
    });

    $('.move_in_date,.move_out_date,.end_date').datepicker({dateFormat: jsDateFomat});
    $(".start_date").datepicker({
        minDate: 0,
        beforeShowDay: function (date) {
            if (date.getDate() == 1) {
                return [true, ''];
            }
            return [false, ''];
        },
        onSelect: function(dateText, instance) {
            onStartDateChange(dateText);
        }
    });

    $(document).on("change",".lease_tenure",function(){
        var value       = $(this).val();
        var time_duration = parseInt($(".lease_term").val(), 10);
        changeTenureTerm(value,time_duration);
    });

    $(document).on("blur",".lease_term",function(){
        var value           = parseInt($(this).val());
        var leaseTenure     = $(".lease_tenure").val();

        var value = $(this).val();
        var notice_period = $('.notice_period').val();
        if (notice_period == "60" && value <= 1 ){
            $(".notice_period,.notice_date").val('');
            bootbox.alert("Notice Period should not greater then lease term 30");
            return false;
        }

        if (notice_period == "90" && value == 2 ){
            $(".notice_period,.notice_date").val('');
            bootbox.alert("Notice Period should not greater then lease term 60");
            return false;
        }

        changeTenureTerm(leaseTenure,value);
    });

    $(document).on("change",".notice_period",function(e){
        e.preventDefault();
        var value = $(this).val();
        var lease_term = $('.lease_term').val();
        if (value == "60" && lease_term <= 1 ){
            bootbox.alert("Notice Period should not greater then lease term 30");
            return false;
        }

        if (value == "90" && lease_term == 2 ){
            bootbox.alert("Notice Period should not greater then lease term 60");
            return false;
        }

        var endDate = $("#end_date").val();
        var d = $.datepicker.parseDate('mm/dd/yy', endDate);
        if (endDate == "") {
            alert("Lease End Date is Blank !");
            $(this).val("");
            return false;
        }else{
            if (value == "30") {
                d.setDate(d.getDate() - 30);
                var newdate = myDateFormatter(d);
                $(".notice_date").val(newdate);
            }else if (value == "60") {
                d.setDate(d.getDate() - 60);
                var newdate = myDateFormatter(d);
                $(".notice_date").val( newdate);
            }else if (value == "90") {
                d.setDate(d.getDate() - 90);
                var newdate = myDateFormatter(d);
                $(".notice_date").val( newdate);
            }else{
                $(".notice_date").val("");
            }
        }
    });

    $('.number_only').keydown(function (e) {
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });

    $(document).on('focusout','#rent_amount,#cam_amount,#security_amount,#increase_amount,#tax_value',function(){
        var id = this.id;
        if($('#' + id).val() != '' && $('#' + id).val().indexOf(".") == -1) {
            var bef = $('#' + id).val().replace(/,/g, '');
            var value = numberWithCommas(bef) + '.00';
            $('#' + id).val(value);
        } else {
            var bef = $('#' + id).val().replace(/,/g, '');
            $('#' + id).val(numberWithCommas(bef));
        }
    });

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $(document).on("change",".increased_by", function () {
        if ($(this).val() == "flat"){

            $(".label_amount").text("Amount ("+currencySign+")");
        }else{
            $(".label_amount").text("Percentage (%)");
        }
    });

    /* lease form validation */

    $(document).on("click",".save_lease_btn",function(e){
        e.preventDefault();
        var tenatntleaseuserid = $(".tenant_session_id").val();
        if (tenatntleaseuserid == "") {
            tenatntleaseuserid = $("input[name='tenant_session_id']").val();
        }
        $(".lease_user_id").val(tenatntleaseuserid);
        if ($('#save_lease_form').valid()){
            $("#save_lease_form").trigger("submit");
            $(".content-data").html("");
            $(".content-data").load("/Tenantlisting/getTenantChargePage");
        }
        return false;
    });

    $("#save_lease_form").on("submit",function(e){
        e.preventDefault();
        var formData = new FormData(this);
        saveTenantLease(formData);
        return false;
    });

    $("#save_lease_form").validate({
        rules: {
            'move_in_date':{required:true},
            'start_date1':{required:true},
            'lease_term':{required:true},
            'end_date1':{required:true},
            'notice_date':{required: true},
            'rent_amount':{required: true}
        },
        messages: {
            "move_in_date": "Select move in date",
            "start_date1": "Select start date",
            "lease_term": "Select lease term",
            "end_date1": "Enter end date",
            "notice_date": "Enter notice date",
            "rent_amount": "Enter rent amunt",
        }
    });

}

function onStartDateChange(dateText) {
    var d = $.datepicker.parseDate('mm/dd/yy', dateText);
    var currentDate = myDateFormatter(d);
    var time_tenure = $(".lease_tenure").val();
    var time_duration = parseInt($(".lease_term").val(), 10);
    if (time_tenure == "2") {
        d.setFullYear(d.getFullYear() + time_duration);
        d.setDate(d.getDate() - 1);
    }else{
        d.setMonth(d.getMonth() + time_duration);
        d.setDate(d.getDate() - 1);
    }

    var outdate = myDateFormatter(d);

    $('.move_out_date').datepicker('setDate', outdate);

    $(".end_date").attr("disabled", false);
    $(".end_date").datepicker("setDate", outdate);
    $("#end_date").val(getSlashDateFormat(d));
    $("#move_out_date").val(getSlashDateFormat(d));
    //$(".end_date").attr("disabled", true);

    if ($("#end_date").val() != "") {
        var leaseNoticePeriod = $(".notice_period").val();

        if (leaseNoticePeriod != "") {
            d.setDate(d.getDate() - parseInt(leaseNoticePeriod));
        }else{
            d.setDate(d.getDate());
        }

        var newdate = myDateFormatter(d);
        $(".notice_date").attr("readonly",false);
        $(".notice_date").val(newdate).attr("readonly",true);
    }

    $('#start_date').val(dateText);
    $('.start_date').val(currentDate);
}

function myDateFormatter(dateObject){
    var newdate = dateObject.getFullYear() + "-" + (dateObject.getMonth() + 1) + "-" + dateObject.getDate();
    var tmp = [];
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/getDateFormat',
        data: {class: "TenantAjax",action: "getDateformat",date: newdate,},
        async: false,
        global:false,
        success: function (response) {
            tmp = $.parseJSON(response);
        }
    });
    return tmp;
}

function getSlashDateFormat(dateObject){
    var newdate = (dateObject.getMonth() + 1) + "/" + dateObject.getDate() + "/" + dateObject.getFullYear();
    return newdate;
}


function changeTenureTerm(value,time_duration) {
    var leaseStart  = $("#start_date").val();
    var noticePeriod  = $(".notice_period").val();
    var d = $.datepicker.parseDate('mm/dd/yy', leaseStart);

    if (value == "2") {
        d.setFullYear(d.getFullYear() + time_duration);
        d.setDate(d.getDate() - 1);
    }else{
        d.setMonth(d.getMonth() + time_duration);
        d.setDate(d.getDate() - 1);
    }
    var outdate = myDateFormatter(d);

    $('.move_out_date').datepicker('setDate', outdate);
    $(".end_date").datepicker("setDate", outdate);
    var slashEndDate = getSlashDateFormat(d);
    $("#end_date").val(slashEndDate);
    $("#move_out_date").val(slashEndDate);
    var endDate = $("#end_date").val();
    var d = $.datepicker.parseDate('mm/dd/yy', endDate);

    if (endDate == "") {
        alert("Lease End Date is Blank !");
        $(this).val("");
        return false;
    }else{
        if (noticePeriod == "30") {
            d.setDate(d.getDate() - 30);
            var newdate = myDateFormatter(d);
            $(".notice_date").datepicker('setDate', newdate);
        }else if (noticePeriod == "60") {
            d.setDate(d.getDate() - 60);
            var newdate = myDateFormatter(d);
            $(".notice_date").datepicker('setDate', newdate);
        }else if (noticePeriod == "90") {
            d.setDate(d.getDate() - 90);
            var newdate = myDateFormatter(d);
            $(".notice_date").val(newdate);
        }else{
            $(".notice_date").val("");
        }
    }
}
jQuery('.phone_format').mask('000-000-0000', {reverse: true});
$('.number_only').keydown(function (e) {
    if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
        return false;
    }
});
