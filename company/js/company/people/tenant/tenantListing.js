var formData22;
$(document).on('mouseover','.tooltipgridclass',function(){
    $(this).closest('td').css("overflow", "unset");
});
$(document).on('mouseout','.tooltipgridclass',function(){
    $(this).closest('td').css("overflow", "hidden");
});



$(document).ready(function() {
    /*payment process jquery starts here */
    $("#addCards").validate({
        rules: {
            card_number: {
                required:true,
                number:true,
                maxlength: 19,
                minlength: 9
            },
            exp_date: {
                required: true,
            },
            cvc: {
                required: true,
                number:true
            },
            holder_name: {
                required: true,
            },
        },
        submitHandler: function (e) {
            var tenant_id = $("#company_user_id").val();
            var form = $('#addCards')[0];
            var formData = new FormData(form);
            formData.append('action','addTenantCardDetails');
            formData.append('class','Stripe');
            formData.append('tenant_id',tenant_id);
            action = 'addAdditionalTenant';
            $.ajax({
                url:'/stripeCheckout',
                type: 'POST',
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status=="true")
                    {
                        toastr.success(response.message);
                        $(".card_number").val('');
                        $(".cvc").val('');
                        $("#addCards").hide();
                        
                        cardLists(tenant_id);
                        bankLists(tenant_id);
                    }
                    else
                    {
                        toastr.error(response.message);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    });

    $('.exp_date').datepicker({
        yearRange: '2019:2040',
        changeMonth: true,
        changeYear: true,
        dateFormat: 'mm/yy'
    });

    $(document).on("change",".setType",function(){
        var value = $(this).val();
        if(value=='1')
        {
            $(".accounts").hide();
            $(".cards").show();
        }
        else
        {
            $(".accounts").show();
            $(".cards").hide();
        }

    });

    /*payment process jquery ends here */


    setTimeout(function () {
        if(localStorage.getItem('property_id') != null){
            $('#jqgridOptions').val('1');
            var grid = $("#tenant_listing"),f = [];
            f.push({field:"tenant_property.property_id",op:"eq",data:localStorage.getItem('property_id')},{field:"tenant_details.status",op:"eq",data:'1'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('property_id');
        }
    }, 500);

    /*search Move Out On quickNav Start*/
    $(document).on("click","#myInput",function (e){
        getTenantMoveOutDetails();
    });

    $(document).on("click",".modalhidevicting",function (e){
        $("#backGroundCheckPop").modal('hide');
    });


    jqGrid('All');

    $(document).on("keyup","#myInput",function(event){
        var keyString = $(this).val();
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantMoveOutSearch",
                keyValue: keyString
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var html = "";
                    if(response.data !== ''){
                        html += '<input type="hidden" id="search_id" value="">\n'+
                            '<table class="table table-hover table-dark" id="myTable"><thead>\n' +
                            '<tr class="header">\n' +
                            '<th>Tenant Name</th>\n' +
                            '<th>Phone</th>\n' +
                            '<th>Email</th>\n' +
                            '<th>propertyName</th>\n' +
                            '<th>Unit</th>\n' +
                            '<th>Rent()</th>\n' +
                            '<th>Balance()</th>\n' +
                            '</tr>\n' +
                            '</thead>\n' +
                            '<tbody>'+response.data+'</tbody>\n' +
                            '</table>';

                    }
                    $("#tenantData_search").html(html);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        keypressCodeSearch();
    });


    function keypressCodeSearch(){
        $(document).on("keypress","#myInput",function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {

                var id = $(".tenantIdget").val();
                var MoveOutTenantName = $('.tenantNameget').val();
                var path = window.location.origin;

                setTimeout(function () {
                    if(id == undefined){
                        return false;
                    }else{
                        $("#myInput").val(MoveOutTenantName);
                        window.location.href = path + '/Tenant/MoveOutTenant?id=' + id;
                    }
                }, 500);
                return false;
            }
        });
    }


    $(document).on("click","#new_chargeCode_form",function (e){
        $("#myTable").hide();
    });

    $(document).on("click", "#myTable tbody tr", function (e) {

        var id = $(this).children("input[type='hidden']").val();    // Find the row
        var base_url = window.location.origin;
        window.location.href = base_url + '/Tenant/MoveOutTenant?id='+id;

    })  ;

    if(localStorage.getItem('ElasticSearch')){
        var elasticSearchData = localStorage.getItem('ElasticSearch');
        setTimeout(function(){
            var grid = $("#tenant_listing"),f = [];
            f.push({field: "users.id", op: "eq", data: elasticSearchData,int:'true'});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
            localStorage.removeItem('ElasticSearch');
        },2000);
    }


    /*Harjinder search Move Out On quickNav End*/
    if(localStorage.getItem("rowcolorTenant")){
        setTimeout(function(){
            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery('.table').find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('rowcolorTenant');
        }, 2000);
    }

    if(localStorage.getItem("table_green_tableid")){
        setTimeout(function(){
            var tableId = localStorage.getItem("table_green_tableid")
            jQuery(tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery(tableId).find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('table_green_id');
            localStorage.removeItem('table_green_tableid');
        }, 2000);
    }

    if(localStorage.getItem("table_green_tableid")){
        setTimeout(function(){
            var tableId = localStorage.getItem("table_green_tableid")
            jQuery(tableId).find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
            jQuery(tableId).find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
            localStorage.removeItem('table_green_id');
            localStorage.removeItem('table_green_tableid');
        }, 2000);
    }

    $(document).on("change", ".tenant_type_status #jqgridOptions", function (e) {
        var statusTenantDetails = $(this).val();
        $('#tenant_listing').jqGrid('GridUnload');
        jqGrid('All',true,statusTenantDetails);

    });
    $(document).on('click','#AZ',function(){
        $('.AZ').hide();
        $('#apex-alphafilter').show();
    });

    $(document).on('click','#allAlphabet',function(){
        var grid = $("#tenant_listing");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });



    $(document).on('click','#allAlphabet',function(){
        var grid = $("#tenant_listing");
        $('.AZ').show();
        $('#apex-alphafilter').hide();
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:""});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        //$('#companyUser-table').trigger( 'reloadGrid' );
    });
    $(document).on('click','.getAlphabet',function(){
        var grid = $("#tenant_listing"),f = [];
        var value = $(this).attr('data_id');
        var search = $(this).text();
        if(value != '0'){
            f.push({field:"name",op:"bw",data:search});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    });

    alphabeticSearchTenant();
    function alphabeticSearchTenant(){
        $.ajax({
            type: 'post',
            url:'/Companies/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearchTenant",
                table: 'users',
                column: 'name',
                where: [
                    {column:'record_status',value:'0',condition:'=',table:'users'},
                    {column:'user_type',value:'2',condition:'=',table:'users'},
                    {column:'record_status',value:'1',condition:'=',table:'tenant_details'},
                    {column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'}
                ]
            },
            success : function(response){
                var response = JSON.parse(response);
                if(response.code == 200){
                    var html = '';

                    $.each(response.data, function(key,val) {
                        var color = '#05A0E4';
                        if(val == 0) color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });
    }

    function alphabeticSearch(){
        $.ajax({
            type: 'post',
            url:'/Companies/List/jqgrid',
            data: {class: 'jqGrid',
                action: "alphabeticSearch",
                table: 'users',
                column: 'name',
                where: [
                    {column:'record_status',value:'0',condition:'=',table:'users'},
                    {column:'user_type',value:'2',condition:'=',table:'users'},
                    {column:'record_status',value:'1',condition:'=',table:'tenant_details'},
                    {column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'}
                ]
            },
            success : function(response){
                var response = JSON.parse(response);
                if(response.code == 200){
                    var html = '';

                    $.each(response.data, function(key,val) {
                        var color = '#05A0E4';
                        if(val == 0) color = '#c5c5c5';
                        html += '<span class="getAlphabet" style="color:'+color+'" data_id="'+val+'">'+key+'</span>';
                    });
                    $('.AtoZ').html(html);
                }
            }
        });


    }
    $(document).on('click','#tenant_listing tr td:not(:last-child)',function(e){
        e.preventDefault();
        var base_url = window.location.origin;
        var id = $(this).closest("tr").attr('id');
        window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
        /* ('tr td:not(:last-child)')*/
    });
    /*  $('tr td:not(:last-child)');*/

    $(document).on("hover","#tenant_listing td:eq(2)",function () {
        var spanTag = "<span class='tooltiptext'>Tooltip text</span>";


    });

    function myCellFormatter(cellvalue, options, rowObject) {
        if (cellvalue != undefined){
            var returnHtml = "<span class='tooltiptext'>"+ cellvalue +"</span>";
            return returnHtml;
        }
    }
    /**
     * jqGrid Initialization function
     * @param status
     */

    function jqGrid(status, deleted_at,TenantStatus) {
        if(jqgridNewOrUpdated == 'true')
        {
            var sortOrder = 'desc';
            var sortColumn = 'users.updated_at';
        } else {
            var sortOrder = 'asc';
            var sortColumn = 'users.name';
        }

        var table = 'users';
        var rentCurrSymbol = "Rent ("+currencySymbol+")";
        var balanceCurrSymbol = "Balance ("+currencySymbol+")";
        var columns = ['Tenant Name','Phone', 'Notes', 'Email', 'portal_status', 'Unit_no','Property Name','Unit Number',rentCurrSymbol,balanceCurrSymbol,'Days Remaining','Status','Action'];
        //if(status===0) {
        var select_column = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant'];
        //}
        var joins = [{table:'users',column:'id',primary:'user_id',on_table:'tenant_property'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_details'},{table:'users',column:'id',primary:'user_id',on_table:'tenant_lease_details'},{table:'tenant_property',column:'property_id',primary:'id',on_table:'general_property'},{table:'tenant_property',column:'unit_id',primary:'id',on_table:'unit_details'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        if(TenantStatus!="" && TenantStatus!=null && TenantStatus!="All"){
            var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'},{column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'},{column:'status',value:TenantStatus,condition:'=',table:'tenant_details'}];
        } else{
            var extra_where = [{column:'user_type',value:'2',condition:'=',table:'users'},{column:'record_status',value:'1',condition:'=',table:'tenant_details'},{column:'record_status',value:'1',condition:'=',table:'tenant_lease_details'}];
        }
        //var pagination=[];
        var columns_options = [

            {name:'Tenant Name',index:'name', width:90,align:"left",searchoptions: {sopt: conditions},table:table,formatter:tenantName},
            /*{name:'Phone',index:'phone_number', width:100,searchoptions: {sopt: conditions},table:'tenant_phone',cellattr: function () { return '  data-toggle="tooltip" data-placement="top" class="phonenotehover" data-original-title="Tooltip on top">'; }},*/
            {name: 'Phone', index: 'id',title:false, width: 80,searchoptions: {sopt: conditions}, table: table, classes: 'cursor',change_type:'line_multiple',index2:'phone_number',index3:'other_work_phone_extension',join:{table: 'users', column: 'id', primary: 'user_id', on_table: 'tenant_phone'},name_id:'id',formatter:addToolTip,cellattr:cellAttri},
            // {name:'Phone',index:'phone_number', width:100,searchoptions: {sopt: conditions},table:'tenant_phone',cellattr:cellAttri,formatter:addToolTip},
            {name:'Notes',index:'phone_number_note',title:false,hidden:true, width:100,searchoptions: {sopt: conditions},table:table,formatter:myCellFormatter},
            {name:'Email',index:'email', width:80, align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'portal_status',index:'portal_status',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:table},
            {name:'Unit_no',index:'unit_no',hidden:true, width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details'},
            {name:'Property Name',index:'property_name', width:80, align:"left",searchoptions: {sopt: conditions},table:'general_property'},
            {name:'Unit Number',index:'unit_prefix', width:80, align:"left",searchoptions: {sopt: conditions},table:'unit_details',change_type: 'combine_column_hyphen2', extra_columns: ['unit_prefix', 'unit_no'],original_index: 'unit_prefix'},
            {name:rentCurrSymbol,index:'rent_amount', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:currencyFormatter},
            {name:balanceCurrSymbol,index:'balance', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter3},
            {name:'Days Remaining',index:'days_remaining', width:80, align:"left",searchoptions: {sopt: conditions},table:'tenant_lease_details',formatter:statusFormatter2},
            {name:'Status',index:'status', width:80,align:"left",searchoptions: {sopt: conditions},table:'tenant_details',attr:[{name:'tenant_id',value:'tenant_id'},{name:'flag',value:'tenant'}],formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

        ];
        var ignore_array = [];
        jQuery("#tenant_listing").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "users",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true,
                extra_where:extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Tenants",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:0,left:400,drag:true,resize:false} // search options
        );
    }
    function cellAttri(cellValue, options, rowObject) {
        if(rowObject !== undefined){

            if(rowObject.Notes != ''){
                return 'title = " "';
            }
        }
    }

    function addToolTip(cellValue, options, rowObject) {
        if(rowObject !== undefined){

            if(rowObject.Notes == ''){
                return cellValue;
            } else {

                return '<div class="tooltipgridclass">'+cellValue+'<span class="tooltiptextbotclass">'+rowObject.Notes+'</span></div>';
            }
        }
    }
    function getUnitDetails( cellvalue, options, rowObject){
        var string = "";
        if (cellvalue != undefined) {
            $.ajax({
                type: 'post',
                url: '/Tenantlisting/getUnitById',
                data: {
                    class: "TenantAjax",
                    action: "getUnitById",
                    id: cellvalue
                },
                async: false,
                success: function (response) {
                    var res = JSON.parse(response);
                    if (res.status == "success") {
                        string = res.data.unit_prefix + "-" + res.data.unit_no;
                    }
                }
            });
        }
        return string;
    }

    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 0)
            return "Inactive";
        else if(cellValue == '1')
            return "Active";
        else if(cellValue == '2')
            return "<span class='collection' style=color:red;>Evicting</span>";
        else if(cellValue == '3')
            return "<span class='collection' style=color:red;>In-Collection</span>";
        else if(cellValue == '4')
            return "<span class='collection' style=color:red;>Bankruptcy</span>";
        else if(cellValue == '5')
            return "<span class='collection' style=color:red;>Evicted</span>";
        else
            return '';

    }
    function statusFormatter2 (cellValue, options, rowObject){
        today=new Date();
        dt1 = new Date(today);
        dt2 = new Date(cellValue);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    }
    function currencyFormatter (cellValue, options, rowObject){
        if(cellValue!==undefined && cellValue!==""){
            return currencySymbol+''+cellValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        } else {
            return "";
        }
    }
    function statusFormatter3 (cellValue, options, rowObject){
        return currencySymbol+''+'1,000.00';
    }

    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var flag = $(cellvalue).attr('flag');
            var select = '';
            if(rowObject.portal_status=='1')
            {
            if(rowObject['Status'] == '0')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '1')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '2')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '3')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '4')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Tenant Portal','Send Password Activation Mail','Deactivate Tenant account','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '5')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            }
            else
            {
            if(rowObject['Status'] == '0')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Activate tenant account','Send Password Activation Mail','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '1')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Activate tenant account','Send Password Activation Mail','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '2')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Activate tenant account','Send Password Activation Mail','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '3')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '4')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','Activate tenant account','Send Password Activation Mail','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '5')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];
            if(rowObject['Status'] == '')  select = ['Edit','Apply Payment','Email','TEXT','Work Order','Add In-touch','In-Touch History','Enter Credit','New Invoice','CAM Charge','File Library','Payment History','Email History','TEXT History','Notes & History','Flag Bank','Send Tenant Statement','Run Background Check','Transfer Tenant','Move Out Tenant','HOA Violation Tracking','Print Envelope','Delete Tenant','ONLINE PAYMENT'];

            }
            var data = '';
            if(select != '') {
                var data = '<select flag="'+flag+'" class="form-control select_options" data_email="' + rowObject.Email +'" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }



    $(document).on('click','#import_tenant',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_tenant_type_div').show(500);
    });
    $(document).on("click", "#import_tenant_cancel_btn", function (e) {
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#import_tenant_type_div").hide(500);
                }
            }
        });
    });
    $("#importTenantTypeFormId").validate({
        rules: { import_file: {
                required: true
            },
        },
        submitHandler: function (form) {
            event.preventDefault();

            var formData = $('#importPropertyForm').serializeArray();
            var myFile = $('#import_file').prop('files');
            var myFiles = myFile[0];
            var formData = new FormData();
            formData.append('file', myFiles);
            formData.append('class', 'TenantTypeAjax');
            formData.append('action', 'importExcel');
            $.ajax({
                type: 'post',
                url: '/tenantTypeAjax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        toastr.success(response.message);

                        $("#import_tenant_type_div").hide(500)
                        $('#tenant_listing').trigger('reloadGrid');
                    } else if(response.status == 'failed' && response.code == 503){
                        toastr.error(response.message);
                        // $('.error').html(response.message);
                        // $.each(response.message, function (key, value) {
                        //     $('.'+key).html(value);
                        // });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });


    function deletetenant(id) {
        $.ajax({
            type: 'post',
            url: '/tenantTypeAjax',
            data: {
                class: "TenantTypeAjax",
                action: "deletetenant",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#tenant_listing').trigger( 'reloadGrid' );
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function changePortalStatus(id) {
        $.ajax({
            type: 'post',
            url: '/tenantTypeAjax',
            data: {
                class: "TenantTypeAjax",
                action: "changePortalStatus",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    $('#tenant_listing').trigger( 'reloadGrid' );
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /** Export unit type excel  */
    $(document).on("click",'#export_tenant',function(){
        var base_url = window.location.origin;
        var status = $("#jqGridStatus option:selected").val();
        var table = 'users';
        var table1 = 'general_property';
        var table2 = 'tenant_phone';
        var table3 = 'tenant_details';
        var table4 = 'tenant_lease_details';
        var table5 = 'unit_details';
        var table6 = 'building_detail';
        var table7 = 'tenant_property';
        window.location.href = base_url+"/tenantTypeAjax?status="+status+"&&table="+table+"&&table1="+table1+"&&table2="+table2+"&&table3="+table3+"&&table4="+table4+"&&table4="+table4+"&&table5="+table5+"&&table6="+table6+"&&table7="+table7+"&&action=exportExcel";
        $(this).off('click');
        return false;
    });


    $(document).on("change", "#tenant_listing .select_options", function (e) {
        e.preventDefault();
        var base_url = window.location.origin;
        var action = this.value;
        var id = $(this).attr('data_id');
        var name=  $("tr#"+id).find("td").eq(0).text();
        var elem = $(this);
        var data_email = $(this).attr('data_email');
        switch (action) {
            case "Edit":
                localStorage.setItem('edit_active','');
                localStorage.setItem('ten_name',name);
                var editActiveTab = "tenant-detail-one";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Apply Payment":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-four";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Email":
                localStorage.setItem('edit_active','');
                localStorage.setItem('predefined_mail',data_email);
                localStorage.setItem('table_green_id',id);
                localStorage.setItem('table_green_tableid', '#tenant_listing');
                localStorage.setItem('table_green_url','/Tenantlisting/Tenantlisting');
                window.location.href = base_url + '/Communication/ComposeEmail';
                break;
            case "TEXT":
                localStorage.setItem('edit_active','');
                localStorage.setItem('predefined_text',data_email);
                localStorage.setItem('table_green_id',id);
                localStorage.setItem('table_green_tableid', '#tenant_listing');
                localStorage.setItem('table_green_url','/Tenantlisting/Tenantlisting');
                window.location.href = base_url + '/Communication/AddTextMessage';
                break;
            case "Work Order":
                localStorage.setItem('edit_active','');
                localStorage.setItem('redirection_module', 'tenant_module');
                window.location.href = base_url + '/WorkOrder/AddWorkOrder?id='+id;
                break;
            case "Add In-touch":
                localStorage.setItem('edit_active','');
                /*var editActiveTab = "tenant-detail-fifteen";
                localStorage.setItem('edit_active',editActiveTab);*/
                window.location.href = base_url + "/Communication/NewInTouch?tid="+id+"&category=Person";
                break;
            case "In-Touch History":
                var propertyname=$('#'+id).find('td:first').text();
                localStorage.setItem("propertyname",propertyname);
                window.location.href = base_url +"/Communication/InTouch";
                break;
            case "Enter Credit":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-four";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "New Invoice":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-three";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "CAM Charge":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-three";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "File Library":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-eight";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Payment History":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-five";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Email History":
                localStorage.setItem('edit_active','');
                window.location.href = base_url + '/Communication/SentEmails?id='+id;
                break;
            case "TEXT History":
                localStorage.setItem('edit_active','');
                window.location.href = base_url + '/Communication/TextMessage?id='+id;
                break;
            case "Notes & History":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-seven";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Flag Bank":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-fifteen";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Send Tenant Statement":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-five";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Run Background Check":
                localStorage.setItem('edit_active','');
                $('#backGroundCheckPopCondition').modal('show');
                $('#btnBackgroundSelection').attr('tent_id',id);
                elem.val('default');
                break;
            case "Transfer Tenant":
                localStorage.setItem('edit_active','');
                window.location.href = base_url + '/Tenant/TransferTenant?tenant_id='+id;
                break;
            case "Move Out Tenant":
                localStorage.setItem('edit_active','');
                window.location.href = base_url + '/Tenant/MoveOutTenant?id='+id;
                break;
            case "Tenant Portal":
                $.ajax({
                    type: 'post',
                    url: '/tenantPortal',
                    data: {class: 'TenantPortal', action: 'getTenantPortal','tenant_id':id},
                    success : function(response){
                        var response =  JSON.parse(response);
                        if(response.status == 'success' && response.code == 200) {
                            window.open(base_url +response.portal_url, '_blank');
                        }else {
                            toastr.warning('Not redirecting due to technical issue.');
                        }
                    }
                });
                break;
            case "Activate tenant account":
                bootbox.confirm("Are you sure you want to do this action?", function (result) {
                    if (result == true) {
                        changePortalStatus(id);
                    } else {
                        window.location.href =  window.location.origin+'/Tenantlisting/Tenantlisting';
                    }
                });
                break;
            /* localStorage.setItem('edit_active','');
             window.location.href = base_url + '/Tenant-portal?tenant_id='+id;
             break;*/
            case "SEND PASSWORD ACTIVATION MAIL":
                localStorage.setItem('edit_active','');
                bootbox.alert("Please enter the valid email address/Login to portal of the concerned person to get the reset password mail (link).");
                break;
            case "Deactivate Tenant account":

                localStorage.setItem('edit_active','');
                bootbox.confirm("Are you sure you want to do this action?", function (result) {
                    if (result == true) {
                        changePortalStatus(id);
                    } else {
                        window.location.href =  window.location.origin+'/Tenantlisting/Tenantlisting';
                    }
                });
                break;
            case "HOA Violation Tracking":
                localStorage.setItem('edit_active','');
                var editActiveTab = "tenant-detail-forteen";
                localStorage.setItem('edit_active',editActiveTab);
                window.location.href = base_url + '/Tenant/ViewEditTenant?tenant_id='+id;
                break;
            case "Print Envelope":
                localStorage.setItem('edit_active','');
                $("#print_envelope").modal('show');
                addPrintAddress(id);
                elem.val("default");
                break;
            case "Delete Tenant":
                bootbox.confirm("Are you sure you want to delete this entry?", function (result) {
                    if (result == true) {
                        deletetenant(id);
                    } else {
                        window.location.href =  window.location.origin+'/Tenantlisting/Tenantlisting';
                    }
                });
                break;
            case "ONLINE PAYMENT":

                stripePaymentVariables(id);
                //fetchUserCardDetail(id);
                $("#company_user_id").val(id);
                getTenantInfo(id);
                //checkFirstPaymentStatus(id);
                checkPaymentMethod(id);
                //getRentAmount(id);
                getNewRentAmount(id);
                $(".select_options").val('default');
                $("#add-card").trigger('reset');
                cardLists(id);
                bankLists(id);
                $("#company_user_id").val(id);
                resetTenantModal();
                break;
             case "Activate tenant account":
                 bootbox.confirm("Are you sure you want to do this action?", function (result) {
                    if (result == true) {
                        changePortalStatus(id);
                    } else {
                        window.location.href =  window.location.origin+'/Tenantlisting/Tenantlisting';
                    }
                });
                break;
            default:
                window.location.href = base_url + '/Setting/ApexNewUser';
        }
    });
    $(document).on('change','.allselect',function () {
        console.log($(".allselect").is(":checked"));
        if($(".allselect").is(":checked")){
             
            var data='0,1,3';
        }else{
            var data='';

        }
        var dataarray=data.split(",");
        $("#payment_category").val(dataarray);
        $("#payment_category").multiselect("refresh");
    });

    function resetTenantModal(){


        $("#another_amoumt").hide(500);
        $("#another_amoumt").val('');
        $("#anotherAmountbutton").show();
        $("#fullAmountButton").hide();
        $("#paymentMethod").val("one-time-payment");
        $(".paymentIntervel").hide();
        $(".paymentIntervelcalander").hide();
        // Then refresh
        $("#payment_comment").val('');
        $("#paymentIntervel").val('weekly')
        $("#payment_category").multiselect("destroy");
        $('#payment_category').multiselect({
            nonSelectedText: 'Select Category',
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true,
            buttonWidth:'100%'
        });

        var data=data;
//Make an array
        var dataarray=data.split(",");
// Set the value
        $("#payment_category").val(dataarray);
// Then refresh
        $("#payment_category").multiselect("refresh");
        // $('input[type=checkbox][name=payment_terms]').prop("checked",false);
        // var   options = $("#payment_category").parent().find('input[type=checkbox]');
        // options.each(function () {
        //         $(this).prop('checked',false)
        // });
        //
        // $(".multiselect-selected-text").html("Select Category");

    }



    function checkPaymentMethod(user_id)
    {

        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
                "action": 'checkPaymentMethod',
                "class": 'Stripe',
                "user_id":user_id

            },
            success: function (response) {
                var response = JSON.parse(response);
                var msg = "<a href='#' class='changePaymentMethod' data-type='"+response.type+"'>"+response.message+"</a>";

                    if(response.type=='update')
                    {
                    
                    $('#tenant-payment').modal('toggle');
                    var paymentMethod = $("#paymentMethod").val();
                    if(paymentMethod=="one-time-payment")
                    {
                        $(".paymentIntervel").hide();
                        $(".paymentIntervelcalander").hide();
                        $(".amountField").hide();
                        $(".countField").hide();
                        $("#anotherAmountbutton").show();
                        
                    }
                    
                    }
                    else
                    {
                     
                    $('#set-method').modal('toggle');
                    }


                    $(".stripeMsg").html(msg);



              }
        });

    }





    /*Move out Tenant listing Start Harjinder 08-08-19*/




    function checkFirstPaymentStatus(user_id)
    {

        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
                "action": 'checkFirstPaymentStatus',
                "class": 'Stripe',
                "user_id":user_id

            },
            success: function (response) {


                if(response==1)
                {

                    $(".firstPaymentStatus").val(1);
                    toastr.warning("You have already made your first payment, Next payment amount will be deducted automatically");
                }
                else
                {
                    $(".firstPaymentStatus").val(0);
                    $("#savefinancialCard").attr("disabled", false);
                }


            }
        });

    }


    function addPrintAddress(id){
        $.ajax({
            type: 'post',
            url: '/tenantTypeAjax',
            data: {
                class: "TenantTypeAjax",
                action: "getAllAddress",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var address1 = response.property.address1;
                var address2 = response.property.address2;
                var address3 = response.property.address3;
                var address4 = response.property.address4;
                var city = response.property.city;
                var state = response.property.state;
                var zipcode = response.property.zipcode;
                var unitnubmer = response.unit.unit_prefix+"-"+response.unit.unit_no;
                var name = response.name;
                var html1 = "";
                var newAddress = "";
                var newAddress2 = "";

                if(name != ""){
                    html1 += '<label>'+name+'</label>';
                }

                if(address1 != "" && address2 == "" && address3 == "" && address4 == ""){
                    newAddress += '<label>'+address1+'</label>';
                }
                if(address1 != "" && address2 != "" && address3 == "" && address4 == ""){
                    newAddress += '<label>'+address1+" "+address2+'</label>';
                }
                if(address1 != "" && address2 != "" && address3 != "" && address4 == ""){
                    newAddress += '<label>'+address1+" "+address2+" "+address3+'</label>';
                }
                if(address1 != "" && address2 != "" && address3 != "" && address4 != ""){
                    newAddress += '<label>'+address1+" "+address2+" "+address3+" "+address4+'</label>';
                }

                html1 += '<label>'+newAddress+'</label>';

                if(unitnubmer != ""){
                    html1 += '<label>Unit#: '+unitnubmer+'</label>';
                }
                if(city != "" && state == "" && zipcode == ""){
                    newAddress2 += '<label>'+city+'</label>';
                }
                if(city != "" && state != "" && zipcode == ""){
                    newAddress2 += '<label>'+city +' '+state +'</label>';
                }
                if(city != "" && state != "" && zipcode != ""){
                    newAddress2 += '<label>'+city +' '+state +', ' +zipcode+'</label>';
                }

                html1 += '<label>'+newAddress2+'</label>';
                /* ----------------- */
                var html2 = "";
                var newAddress3 = "";
                var newAddress4 = "";
                var comp = response.company;
                if(name != ""){
                    html2 += '<label>'+comp.name+'</label>';
                }

                if(comp.address1 != "" && comp.address2 == "" && comp.address3 == "" && comp.address4 == ""){
                    newAddress3 += '<label>'+comp.address1+'</label>';
                }
                if(comp.address1 != "" && comp.address2 != "" && comp.address3 == "" && comp.address4 == ""){
                    newAddress3 += '<label>'+comp.address1+" "+comp.address2+'</label>';
                }
                if(comp.address1 != "" && comp.address2 != "" && comp.address3 != "" && comp.address4 == ""){
                    newAddress3 += '<label>'+comp.address1+" "+comp.address2+" "+comp.address3+'</label>';
                }
                if(comp.address1 != "" && comp.address2 != "" && comp.address3 != "" && comp.address4 != ""){
                    newAddress3 += '<label>'+comp.address1+" "+comp.address2+" "+comp.address3+" "+comp.address4+'</label>';
                }

                html2 += '<label>'+newAddress3+'</label>';


                if(comp.city != "" && comp.state == "" && comp.zipcode == ""){
                    newAddress4 += '<label>'+comp.city+'</label>';
                }
                if(comp.city != "" && comp.state != "" && comp.zipcode == ""){
                    newAddress4 += '<label>'+comp.city +' '+comp.state +'</label>';
                }
                if(comp.city != "" && comp.state != "" && comp.zipcode != ""){
                    newAddress4 += '<label>'+comp.city +' '+comp.state +', ' +comp.zipcode+'</label>';
                }

                html2 += '<label>'+newAddress4+'</label>';

                setTimeout(function(){
                    $("#print_envelope .print_tenant_address").html(html1);
                    $("#print_envelope .print_company_address").html(html2);
                },1000);
            }
        });
    }


    function getTenantMoveOutDetails() {
        //var id = $("#wrapper .moveOutTenant_id").val();
        //alert(id);
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantMoveOutDetails",
                // id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var html1 = "";
                if (response.status == 'success' && response.code == 200) {
                    /*for(var i=0; i < response.data.length; i++){
                        $('#myTable tbody').html(response.data);
                    }*/
                    html1 += '<table class="table table-hover table-dark" id="myTable"><thead>\n' +
                        '                           <tr class="header">\n' +
                        '                            <th>Tenant Name</th>\n' +
                        '                        <th>Email</th>\n' +
                        '                        <th>Phone</th>\n' +
                        '                        <th>propertyName</th>\n' +
                        '                        <th>Unit</th>\n' +
                        '                        <th>Rent()</th>\n' +
                        '                        <th>Balance()</th>\n' +
                        '                        </tr>\n' +
                        '                        </thead>\n' +
                        '                        <tbody>'+response.data+'</tbody>\n' +
                        '                        </table>';


                    $("#tenantData_search").html(html1);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /*Move out Tenant listing End Harjinder 08-08-19*/

    /**
     * tenant transfer search(Manjit)
     */

    $(document).on("click","#myInputTransfer",function (e){
        getTenantTransferDetails();
    });

    $(document).on("keyup","#myInputTransfer",function(event){
        var keyString = $(this).val();
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantMoveOutSearch",
                keyValue: keyString
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var html = "";
                    if(response.data !== ''){
                        html += '<input type="hidden" id="transfer_search_id" value="">\n'+
                            '<table class="table table-hover table-dark" id="myTransferTable"><thead>\n' +
                            '                           <tr class="header">\n' +
                            '                            <th>Tenant Name</th>\n' +
                            '                        <th>Email</th>\n' +
                            '                        <th>Phone</th>\n' +
                            '                        <th>propertyName</th>\n' +
                            '                        <th>Unit</th>\n' +
                            '                        <th>Rent()</th>\n' +
                            '                        <th>Balance()</th>\n' +
                            '                        </tr>\n' +
                            '                        </thead>\n' +
                            '                        <tbody>'+response.data+'</tbody>\n' +
                            '                        </table>';

                    }
                    $("#transferData_search").html(html);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        keypressCodeSearch1();
    });


    function keypressCodeSearch1(){
        $(document).on("keypress","#myInputTransfer",function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {

                var id = $(".tenantIdget").val();
                var MoveOutTenantName = $('.tenantNameget').val();
                var path = window.location.origin;

                setTimeout(function () {
                    $("#myInputTransfer").val(MoveOutTenantName);
                    window.location.href = path + '/Tenant/TransferTenant?id=' + id;
                }, 500);
                return false;
            }
        });
    }


    $(document).on("click","#new_chargeCodeTransfer_form",function (e){
        $("#myTransferTable").hide();
    });

    $(document).on("click", "#myTransferTable tbody tr", function (e) {
        var id = $(this).children("input[type='hidden']").val();    // Find the row
        var base_url = window.location.origin;
        window.location.href = base_url + '/Tenant/TransferTenant?id=' + id;

    })  ;

    /* Manjit search Tenant Transfer On quickNav End */

    /**
     * tenant transfer search table listing(Manjit)
     */

    function getTenantTransferDetails() {
        //var id = $("#wrapper .moveOutTenant_id").val();
        //alert(id);
        $.ajax({
            type: 'post',
            url: '/moveOutTenant',
            data: {
                class: "moveOutTenant",
                action: "getTenantMoveOutDetails",
                // id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var html1 = "";
                if (response.status == 'success' && response.code == 200) {
                    html1 += '<table class="table table-hover table-dark" id="myTransferTable"><thead>\n' +
                        '                           <tr class="header">\n' +
                        '                            <th>Tenant Name</th>\n' +
                        '                        <th>Phone</th>\n' +
                        '                        <th>Email</th>\n' +
                        '                        <th>propertyName</th>\n' +
                        '                        <th>Unit</th>\n' +
                        '                        <th>Rent()</th>\n' +
                        '                        <th>Balance()</th>\n' +
                        '                        </tr>\n' +
                        '                        </thead>\n' +
                        '                        <tbody>'+response.data+'</tbody>\n' +
                        '                        </table>';


                    $("#transferData_search").html(html1);

                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#ContactListing-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /* Manjit Tenant Transfer search End */
});


$(document).on('click','.background_check_open',function () {
    if( $(this).is(':checked') )
    {   $(this).prop('checked', false);
        // $('#backGroundCheckPop').modal('hide');
        // $('#backgroundReport').modal('show');
        window.open('https://www.victig.com', '_blank');
    }
});







/*$("#add-card").validate({
    rules: {
        cfirst_name: {
            required:true
        },
        clast_name: {
            required:true
        },
        ccard_number: {
            required:true,
            number:true
        },
        cexpiry_year: {
            required:true
        },
        cexpiry_month: {
            required:true
        },
        ccvv: {
            required:true,
            number:true,
        },
        cphoneNumber: {
            required:true
        },
        caddress1: {
            required:true
        },
        ccity: {
            required:true
        },
        cstate: {
            required:true
        },
        czip_code: {
            required:true
        },
        ccountry: {
            required:true
        },
        cCompany:{
            required:true
        }
    },
    submitHandler: function (e) {
        var checkPaymentStatus = $(".firstPaymentStatus").val();
        if(checkPaymentStatus==1)
        {
          toastr.warning("You have already made your first payment, Next payment amount will be deducted automatically");
          return false;
        }
        $("#loadingmessage").show();
        var form = $('#add-card')[0];
        var formData = new FormData(form);
        formData.append('action','addTenantCardPayment');
        formData.append('class','Stripe');

        var paidAmount = $('.stripe_amount').val();
        formData.append('paid_amount',paidAmount);




        $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status=='success')
                {
                    $("#loadingmessage").hide();
                    toastr.success(response.message);
                    $('#tenant-payment').modal('toggle');
                    $("#ccvv").val("");
                    $("#ccard_number").val("");
                    $("#caddress1").val("");
                    $("#caddress2").val("");
                    localStorage.setItem("Message",'Payment has been done successfully.');
                    localStorage.setItem('rowcolorTenant', 'rowcolorTenant');
                    window.location.reload();

                }
                else
                {
                    $("#loadingmessage").hide();
                    toastr.error(response.message);
                }

            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
});
*/


$(document).ready(function(){
    var min = new Date().getFullYear(),
        max = min + 49,
        select = document.getElementById('cexpiry_year');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        /*console.log('hgffhjfghf',opt);*/
        if(opt!=null)
        {
            opt.value = i;
            opt.innerHTML = i;
            select.appendChild(opt);

        }
    }
    $("#cphoneNumber").mask('000-000-0000', {reverse: true});



});


function getTenantInfo(id)
{
    $.ajax({
        type: 'post',
        url: '/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getTenantZipCode',
            id:id},
        success: function (response) {
            var info = JSON.parse(response);
            setTimeout(function () {
                $("#czip_code").val(info.zipcode);
                var name = "("+info.name+")";
                $(".userName").html(name);
                $(".holder_name").val(info.name);
                getZipCode('#czip_code', info.zipcode, '.zipcity', '.zipstate', '.zipcountry', null, null);
            },500);



        },
    });


}





$(document).on("blur","#czip_code",function(){
    var value = $(this).val();
    getZipCode('#czip_code', value, '.zipcity', '.zipstate', '.zipcountry', null, null);
});


function getZipCode(element,zipcode,city,state,country,county,validation){
    $(validation).val('1');
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {zip_code: zipcode,class: 'CommonAjax', action: 'getZipcode'},
        success: function (response) {
            var data = JSON.parse(response);
            if(data.code == 200){

                if(city !== undefined)$(city).val(data.data.city);
                if(state !== undefined)$(state).val(data.data.state);
                if(country !== undefined)$(country).val(data.data.country);
                if(county !== undefined)$(county).val(data.data.county);
            } else {
                getAddressInfoByZip(element,zipcode,city,state,country,county,validation);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}




function getAddressInfoByZip(element,zip,city,state,country,county,validation){
    var addr = {};
    addr.element = element;
    addr.elecity = city;
    addr.elestate = state;
    addr.elecountry = country;
    addr.elecounty = county;
    addr.validation = validation;
    if(zip.length >= 5 && typeof google != 'undefined'){
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    response(addr);
                } else {
                    addr.success = false;
                    response(addr);
                }
            } else {
                addr.success = false;
                response(addr);
            }
        });
    } else {
        addr.success = false;
        response(addr);
    }
}



function getRentAmount(id)
{

    $.ajax({
        type: 'post',
        url: '/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getRentAmount',
            id:id},
        success: function (response) {
            var data = JSON.parse(response);
            $(".paidAmount").html(data.amount_paid);
            $(".stripe_amount").val(data.stripe_amount);
        },
    });



}


function flagFormatter(cellValue, options, rowObject){
    if(rowObject !== undefined){
        var flagValue = $(rowObject.Action).attr('flag');
        var flag = '';
        if(flagValue == 'yes'){

        } else {

        }
    }
}

function fetchUserCardDetail(id) {
    $.ajax({
        type: 'post',
        url: '/common-ajax',
        data: {
            class: 'CommonAjax',
            action: 'getCustomerDetails',
            id: id},
        success: function (response) {
            var res = JSON.parse(response);
            console.log(res.data.data);
            if(res.code == 200){
                $('#add-card [name ="cfirst_name"]').val(res.data.data.first_name);
                $('#add-card [name ="clast_name"]').val(res.data.data.last_name);
                $('#add-card [name ="cCompany"]').val(res.data.data.company_name);
                $('#add-card [name ="cphoneNumber"]').val(res.data.data.phone_number);
                $('#add-card [name ="caddress1"]').val(res.data.data.address1);
                $('#add-card [name ="caddress2"]').val(res.data.data.address2);
                $('#add-card [name ="ccity"]').val(res.data.data.city);
                $('#add-card [name ="cstate"]').val(res.data.data.state);
                $('#add-card [name ="czip_code"]').val(res.data.data.zipcode);
                $('#add-card [name ="ccountry"]').val(res.data.data.country);
            }
        },
    });
}

/**
 *  function to format tenant name
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function tenantName(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        var flagValue = $(rowObject.Action).attr('flag');
        var id = $(rowObject.Action).attr('data_id');
        var flag = '';
        if (flagValue == 'yes') {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a class="classFlagRedirect" data_href="#flagTab" href="javascript:void(0);" data_url="/Vendor/ViewVendor?id='+id+'" ><img src="/company/images/Flag.png"></a></span>';
        } else {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }
}


/*payment process jquery starts here */

function cardLists(id)
{
    var tenant_id = id;
    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getTenantAllCards',
            "class": 'Stripe',
            "tenant_id":tenant_id
        },
        success: function (response) {


            $(".cardDetails").html(response);


        }
    });

}

function bankLists(id)
{
    var tenant_id = id;
    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        data: {
            "action": 'getTenantAllBanks',
            "class": 'Stripe',
            "tenant_id":tenant_id
        },
        success: function (response) {


            $(".accountDetails").html(response);


        }
    });

}





/*payment process jquery ends here */


$(document).on("change","#paymentMethod",function(){
    var value = $(this).val();
    var tenant_id = $("#company_user_id").val();
    if(value=="reoccurring")
    {
        $(".paymentIntervel").show();
        $(".amountField").show();
        $(".countField").show();
        $("#anotherAmountbutton").hide();
        $("#fullAmountButton").hide()
        $("#another_amoumt").hide();
      var   options = $("#payment_category").parent().find('input[type=checkbox]');
        options.each(function () {

               $(this).prop('disabled',false)

        });

    }
    else
    {
        $(".paymentIntervel").hide();
        $(".paymentIntervelcalander").hide();
        $(".amountField").hide();
        $(".countField").hide();
        $("#anotherAmountbutton").show();
        getNewRentAmount(tenant_id);
    }

});




$(document).on("change","#paymentIntervel",function(){
    var value = $(this).val();
    if(value=="monthly")
    {
        $(".paymentIntervelcalander").show();
    }
    else
    {
        $(".paymentIntervelcalander").hide();
    }

});


$("#paymentIntervelcalander").datepicker({
    dateFormat: 'dd',
    changeMonth: true,
    changeYear: true
}).datepicker("setDate", new Date());





$(document).on("click",".changePaymentMethod",function(){
    $('#tenant-payment').modal('toggle');
    $('#set-method').modal('toggle');

});





function stripePaymentVariables(user_id){
    var stripe = Stripe('pk_test_jF5tXlX0qLcCR1YAX9WB8FaL');
    // Create an instance of Elements.
    var elements = stripe.elements();
    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    var cardElemLength = $("#card-element1").length;
    if (cardElemLength > 0) {
        card.mount('#card-element1');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form1');
        form.addEventListener('submit', function(event) {
            $(".submit_payment_class").attr("disabled",true);
            event.preventDefault();
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    var token_id = result.token.id;

                    stripeTokenHandler(token_id,user_id);
                }
            });
        });
    }
}



function stripeTokenHandler(token,user_id) {

    var currency = 'USD';
    var Amount = $(".stripe_amount").val();
    var days =   $(".days_remaining").val();
    var user_type = 'TENANT';
    var tenant_payment_data = $('#tenant_payment_interval_form').serialize();
    var formData = new FormData();
    formData.append('payment_data', tenant_payment_data);
    formData.append('action', 'tenantOneTimePayment');
    formData.append('class', 'Stripe');
    formData.append('token_id', token);
    formData.append('currency', currency);
    formData.append('amount', Amount);
    formData.append('user_type', user_type);
    formData.append('user_id', user_id);

    $.ajax({
        url:'/stripeCheckout',
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData
        , beforeSend: function() {
            // setting a timeout
            $('#loadingmessage').show();
        },
        success: function (response) {
            $('#loadingmessage').hide();
            $(".submit_payment_class").attr("disabled",false)
            var response = JSON.parse(response);
            if(response.status=='success')
            {
                toastr.success('Payment has been done successfully');
                $('#stripe-checkout').modal('toggle');

            }
            else
            {
                toastr.error(response.message);
            }


        }
    });




}

$(document).on("click","#paymentDeduction",function(){
     formData22 = $('#tenant_payment_interval_form').serializeArray();
  //  $("#tenant_form_serialize_date").val(formData);

    var paymentMethod = $('#paymentMethod').val();
    var user_id       = $('#company_user_id').val();
    var amount        = $("#amount").val();
    var count         = $("#count").val();



    if (!$(".payment_terms").is(":checked")) {
        toastr.error("Check the terms & condition checkbox first.")
        return false;
        // do something if the checkbox is NOT checked
    }
    if(paymentMethod=="one-time-payment")
    {
        $('#tenant-payment').modal('toggle');
        $('#stripe-checkout').modal('toggle');

    }
    else
    {
        if($(".changePaymentMethod").html()=="Update your payment method")
        {
            var interval = $("#paymentIntervel").val();
            if(interval=="monthly")
            {
                var date =   $("#paymentIntervelcalander").val();/*For monthly date*/
            }
            else
            {
                var date = 0; /*date zero is only for weekly and bi-weekly.Tenant can set particular date for monthly payment only*/
            }

           var paymentCategory = $("#payment_category").val();
           //console.log(paymentCategory);

            $.ajax({
                url:'/stripeCheckout',
                type: 'POST',
                data: {

                    "action": 'setPaymentProcess',
                    "class": 'Stripe',
                    "paymentMethod":paymentMethod,
                    "interval":interval,
                    "date": date,
                    "user_id":user_id,
                    "amount":amount,
                    'paymentCategory':paymentCategory,
                    'count':count
                },
                success: function (response) {

                    var response = JSON.parse(response);
                    if(response.status=='success')
                    {
                        $('#tenant-payment').modal('toggle');
                        toastr.success(response.message);
                        $('#tenant_listing').trigger( 'reloadGrid' );
                       setTimeout(function(){
                        jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('.table').find('tr:eq(1)').find('td:eq(11)').addClass("green_row_right");
                        localStorage.removeItem('rowcolorTenant');
                        }, 2000);
                      }
                    else
                    {
                        toastr.error(response.message);

                    }


                }
            });
        }
        else
        {
            toastr.error("Please add your payment method first");
            $('#tenant-payment').modal('toggle');

        }

    }

});



// $(document).on("click","#paymentDeduction",function(){
//     $("#tenant-payment").toggle();
//
// });



$(document).on("click",".changePaymentMethod",function(){
    var type = $(this).attr('data-type');
    if(type=='update')
    {
        $("#addCards").hide();
        $(".paymentAction").val('update');

    }
    else
    {
        $("#addCards").show();
        $(".paymentAction").val('add');
    }



});



$(document).on("change",".cardAction",function(){
 var tenant_id = $(".tenant_id").val();
 var cardAction = $(this).val();
 var card_id = $(this).attr('data-cardId');
 if(cardAction=='delete')
 {
     $(".cardAction").val('');
       bootbox.confirm("Do you want to delete this card?", function (result) {
        if (result == true) {
                   $.ajax({
            url:'/stripeCheckout',
            type: 'POST',
            data: {
            "action": 'deleteTenantCard',
            "class": 'Stripe',
            "card_id":card_id,
            "tenant_id":tenant_id
            },
            success: function (response) {
                var response = JSON.parse(response);
                 toastr.success(response.message);

                        cardLists();
                        bankLists();




                    }
                });
            }
        });
    }
    else if(cardAction=='default')
    {
        $(".cardAction").val('');
        bootbox.confirm("are you sure you want to make this card as a default source?", function (result) {
            if (result == true) {
                $.ajax({
                    url:'/stripeCheckout',
                    type: 'POST',
                    data: {
                        "action": 'defaultTenantCard',
                        "class": 'Stripe',
                        "card_id":card_id,
                        "tenant_id":tenant_id
                    },
                    success: function (response) {
                        var response = JSON.parse(response);
                        toastr.success(response.message);

                        cardLists();
                        bankLists();



                    }
                });

            }
        });



    }
    else if(cardAction=='update')
    {
        $("#addCards").show();



    }


});



// $(document).on("click",'.payment_terms',function(){
//     if(this.checked)
//     {
//
//        // $('#paymentDeduction').prop('disabled', false);
//     }
//     else
//     {
//
//         $('#paymentDeduction').prop('disabled', true);
//     }
// });

$(document).on("click",'#anotherAmountbutton',function(){
    $("#another_amoumt").show(500);
    $("#anotherAmountbutton").hide();
    $("#fullAmountButton").show();
});

$(document).on("click",'#fullAmountButton',function(){
    $("#another_amoumt").hide(500);
    $("#another_amoumt").val('');
    $("#anotherAmountbutton").show();
    $("#fullAmountButton").hide();

    var user_id = $("#company_user_id").val();
    getNewRentAmount(user_id);
});


$(document).on("focusout","#another_amoumt",function(){
        $(".paidAmount").html($(this).val());
        $(".stripe_amount").val($(this).val().replace(/,/g, ''));
});


function getNewRentAmount(id)
{

    $.ajax({
        type: 'post',
        url: '/stripeCheckout',
        data: {
            class: 'Stripe',
            action: 'getNewRentAmount',
            id:id},
        success: function (response) {
            var data = JSON.parse(response);
            $(".paidAmount").html(data.show_total_amount_due);
            $(".stripe_amount").val(data.total_amoun_due);
            var   options = $("#payment_category").parent().find('input[type=checkbox]');

            if(data.rent_to_be_paid == 0){
                options.each(function () {
                   if($(this).val() == 1){
                    $(this).prop('disabled',true)
                   }
                });
            }
            if(data.total_charges_due == 0){
                options.each(function () {
                    if($(this).val() == 2){
                        $(this).prop('disabled',true)
                    }
                });
            }
            if(data.total_security_cam_charges == 0){
                options.each(function () {
                    if($(this).val() == 3){
                        $(this).prop('disabled',true)
                    }
                });
            }

            if(data.rent_to_be_paid == 0 && data.total_charges_due == 0 && data.total_security_cam_charges == 0){
                    options.each(function () {
                        if($(this).val() == 0){
                            $(this).prop('disabled',true)
                        }
                    });
            }
        },
    });
}



$('#payment_category').multiselect({
    nonSelectedText: 'Select Category',
    enableFiltering: true,
    enableCaseInsensitiveFiltering: true,
    buttonWidth:'100%'
});




var data="";
//Make an array
var dataarray=data.split(",");
// Set the value
$("#payment_category").val(dataarray);
// Then refresh
$("#payment_category").multiselect("refresh");

$(document).on("click",'.setPaymentInterval',function(){
        var user_id       = $('#company_user_id').val();
        $('#tenant-payment').modal('toggle');
        getPaymentIntervals(user_id);
        $('#set-interval').modal('toggle');
  });


function getPaymentIntervals(tenant_id)
{


   $.ajax({
    type: 'post',
    url: '/stripeCheckout',
    data: {
       "class": 'Stripe',
       'action': "getPaymentIntervals",
        "tenant_id":tenant_id
    },
    async: false,
    success: function (response) {

        $(".getPaymentIntervals").html(response);

    }
});

}


$(document).on("change",".status",function(){
 var interval_id = $(this).attr('data-id');

 var interval_type = $(this).val();
  if(interval_type=='cancel')
        {
            bootbox.confirm({
            message: "Are you sure you want to Cancel this Interval ?",
            buttons: {confirm: {label: 'Ok'}, cancel: {label: 'Cancel'}},
            callback: function (result) {
                if (result == true) {

                        $.ajax({
                        type: 'post',
                        url: '/stripeCheckout',
                        data: {
                        "class": 'Stripe',
                        'action': "changeIntervalStatus",
                        "interval_id":interval_id
                        },
                        async: false,
                        success: function (response) {
                            response = JSON.parse(response);
                            if(response.status=='true')
                            {
                              toastr.success(response.message);
                              $('#set-interval').modal('toggle');
                            }
                         }
                     });
                  }
               }
           });
         }
      });








$(document).on("click", "#paymentCancel", function (e) {
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $("#tenant-payment").modal('hide');
            }
        }
    });
});


$(document).on('keypress keyup','.numberonly',function(){
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }

});




















