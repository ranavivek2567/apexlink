

$(document).on('change','#complaint_by_about_status',function(){
    var grid = $("#ownerComplaints-table"), f = [];
    var value = $(this).val();
    if (value != 'all') {
        f.push({field: "complaints.complaint_by_about", op: "eq", data: value});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData, {filters: JSON.stringify(f)});
        grid.trigger("reloadGrid", [{page: 1, current: true}]);
    } else {
        grid[0].p.search = false;
        $.extend(grid[0].p.postData, {filters: ""});
        grid.trigger("reloadGrid", [{page: 1, current: true}]);
    }
});


ownerComplaintsForEdit();
function ownerComplaintsForEdit(status) {
    var table = 'complaints';
    var columns = ['<input type="checkbox" id="select_all_complaint_checkbox">','Complaint Id', 'Complaint', 'Complaint Type','Notes','Date','Other','Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [{table: 'complaints', column: 'complaint_type_id', primary: 'id', on_table: 'complaint_types'}];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = ['complaints.status'];
    var extra_where = [{column: 'object_id', value: id, condition: '='},{column: 'module_type', value: "owner", condition: '='}];
    var columns_options = [
        {name:'Id',index:'id', width:100,align:"center",sortable:false,searchoptions: {sopt: conditions},table:table,search:false,formatter:actionCheckboxFmatter},
        {name:'Complaint Id',index:'complaint_id', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Complaint',index:'complaint_by_about', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Complaint Type',index:'complaint_type', width:100,align:"center",searchoptions: {sopt: conditions},table:"complaint_types"},
        {name:'Notes',index:'complaint_note', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Date',index:'complaint_date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Other',index:'other_notes',width:200,align:"center",searchoptions: {sopt: conditions},search:false,table:table},
        {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:complaintsActionFmatter}
    ];
    var ignore_array = [];
    jQuery("#ownerComplaints-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'complaints.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Complaints",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {top: 10, left: 200, drag: true, resize: false} // search options
    );
}



/**
 * jqGrid function to format checkbox column
 * @param status
 */

function actionCheckboxFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {

        var data = '';
        var data = '<div class="owner_complaint_checkbox"><input type="checkbox" name="complaint_checkbox[]" class="complaint_checkbox" id="complaint_checkbox_'+rowObject.id+'" data_id="' + rowObject.id + '"/></div>';
        return data;
    }
}


/**
 * jqGrid function to format action column
 * @param status
 */

function complaintsActionFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var editable = $(cellvalue).attr('editable');
        var select = '';
        select = ['Edit','Delete'];
        var data = '';

        if(select != '') {

            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                if(editable == '0' && (val == 'delete' || val == 'Delete')){
                    return true;
                }
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}

$(document).on('click','#new_complaint_btn',function () {
    $('.new_complaint_btn_div').hide();
    $('#other_notes_div').hide();
    $('#new_complaint_div').show(500);
    $('#new_complaint_div .time_span').text('');
    $('#complaint_id').val(randomNumberString(6)).prop('readonly', false);
    $('#complaint_by_owner').prop('checked', true);
    $('#complaint_save_btn').text('Save');
    $("#complaint_note,#other_complaint_notes, #edit_complaint_id").val('');
    $("#complaint_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
    $('#complaint_type_options').prop('selectedIndex','');
});

$(document).on('click','#clearAddComplaintForm',function () {
    bootbox.confirm("Do you want to clear this complaint form?", function (result) {
        if (result == true) {
            var edit_complaint_id = $('#edit_complaint_id').val();
            if (edit_complaint_id == '') {
                $('#complaint_id').val(randomNumberString(6)).prop('readonly', false);
                $('#new_complaint_div .time_span').text('');
                $('#complaint_by_owner').prop('checked', true);
                $('#complaint_save_btn').text('Save');
                $('#other_notes_div').hide();
                $("#complaint_note,#other_complaint_notes, #edit_complaint_id").val('');
                $("#complaint_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
                $('#complaint_type_options').prop('selectedIndex', '');
            } else {
                getDataByIdComplaints(edit_complaint_id);
            }
        }
    });
});


//on complaint cancel
$(document).on('click','#complaint_cancel',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#flag_id').val('');
                $('#new_complaint_div').hide(500);
                $('.new_complaint_btn_div').show();
            }
        }
    });
});

//adding custom fields
$('#complaint_save_btn').on('click',function(event) {
    event.preventDefault();
    var formData = $('#new_complaint_div :input').serializeArray();
    $.ajax({
        type: 'post',
        url: '/EditOwnerAjax',
        data: {
            form: formData,
            class:'EditOwnerAjax',
            action:'addEditComplaint',
            owner_edit_id:owner_edit_id,
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
                $('#new_complaint_div').hide(500);
                $('.new_complaint_btn_div').show();
                $('#ownerComplaints-table').trigger('reloadGrid');
                toastr.success(response.message);
            } else if(response.code == 200) {
                toastr.error(response.message);
            }

            setTimeout(function(){
                jQuery('#ownerComplaints-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                jQuery('#ownerComplaints-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
            }, 1000);
        },
        error: function (data) {
            console.log(data);
        }
    });
});


$(document).on('change', '#complaint_type_options', function () {
    if($(this).val() == '18'){
        $('#other_notes_div').show();
    } else {
        $('#other_notes_div').hide();
    }
});

function getDataByIdComplaints(data_id){
    $.ajax({
        type: 'post',
        url: '/EditOwnerAjax',
        data: {
            class: 'EditOwnerAjax',
            action: 'getComplaintDetailById',
            id: data_id
        },
        success: function (response) {

            var response = JSON.parse(response);
            if (response.code == 200) {

                $('#ownerComplaints-table').find('.green_row_left, .green_row_right').each(function(){
                    $(this).removeClass("green_row_left green_row_right");
                });
                var complaint_data = response.data;
                edit_date_time(complaint_data.updated_at);
                $('#complaint_save_btn').html('Update');
                $('.new_complaint_btn_div').hide();
                $('#new_complaint_div').show(500);
                $('#complaint_id').val(complaint_data.complaint_id).prop('readonly', 'readonly');

                if(complaint_data.complaint_date && complaint_data.complaint_date != ''){
                    $("#complaint_date").datepicker({dateFormat: datepicker}).datepicker("setDate", complaint_data.complaint_date);
                } else {
                    $("#complaint_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
                }
                if (complaint_data.complaint_type_id == '' || complaint_data.complaint_type_id == null) {
                    $('#complaint_type_options').val('');
                }

                if (complaint_data.complaint_type_id && complaint_data.complaint_type_id != '') {
                    $('#complaint_type_options').val(complaint_data.complaint_type_id);
                }

                var  complaint_by_about = complaint_data.complaint_by_about;
                complaint_by_about = (complaint_by_about)?complaint_by_about.toLowerCase() : '';
                if(complaint_by_about == "complaint by owner"){
                    $('#complaint_by_owner').prop('checked', true);
                } else {
                    $('#complaint_about_owner').prop('checked', true);
                }
                $('#complaint_note').val(complaint_data.complaint_note);
                if(complaint_data.complaint_type_id == '18'){
                    $('#other_notes_div').show();
                    $('#other_complaint_notes').val(complaint_data.other_notes);
                } else {
                    $('#other_notes_div').hide();
                    $('#other_complaint_notes').val(complaint_data.other_notes);
                }

            } else if (response.status == 'error' && response.code == 503) {
                toastr.error(response.message);
            } else {
                toastr.warning('Record not fetched due to some technical error.');
            }
        }
    });
}

/**
 *change function to perform various actions(edit ,delete)
 * @param status
 */
jQuery(document).on('change', '#ownerComplaints-table .select_options', function () {
    var select_options = $(this).val();
    var row_num = $(this).parent().parent().index() ;
    var data_id = $(this).attr('data_id');
    if (select_options == 'Edit' || select_options == 'edit') {
        $('#new_complaint :input').val('');

        $('#edit_complaint_id').val(data_id);
        $('#ownerComplaints-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        $('#ownerComplaints-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        getDataByIdComplaints(data_id);
        // window.location.href = '/MasterData/AddPropertyType';

    } else if (select_options == 'Print Envelope') {
        $.ajax({
            type: 'post',
            url: '/OwnerAjax',
            data: {class: 'OwnerAjax', action: 'getCompanyData', 'building_id': data_id},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $("#PrintEnvelope").modal('show');
                    $("#company_name").text(response.data.data.company_name)
                    $("#address1").text(response.data.data.address1)
                    $("#address2").text(response.data.data.address2)
                    $("#address3").text(response.data.data.address3)
                    $("#address4").text(response.data.data.address4)
                    $(".city").text(response.data.data.city)
                    $(".state").text(response.data.data.state)
                    $(".postal_code").text(response.data.data.zipcode)
                    $("#building_name").text(response.building.data.building_name)
                    $("#building_address").text(response.building.data.address)


                } else {
                    toastr.warning('Record not updated due to technical issue.');
                }
            }
        });

    } else if (select_options == 'Delete') {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/EditOwnerAjax',
                        data: {class: 'EditOwnerAjax', action: 'deleteComplaint', id: data_id},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success('Record deleted successfully.');
                            } else if (response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            } else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
                $("#ownerComplaints-table").trigger('reloadGrid');
            }
        });
    }
    $('.select_options').prop('selectedIndex', 0);
});



$(document).on("change","#select_all_complaint_checkbox",function(e){
    var inputs = $('#ownerComplaints-table .owner_complaint_checkbox input[type=checkbox]');
    if(e.originalEvent === undefined) {
        var allChecked = true;
        inputs.each(function(){
            allChecked = allChecked && this.checked;
        });
        this.checked = allChecked;
    } else {
        inputs.prop('checked', this.checked);
    }
});

$(document).on('change', '#ownerComplaints-table .owner_complaint_checkbox input[type=checkbox]', function(){
    $('#select_all_complaint_checkbox').trigger('change');
});

/*jquery to open modal for complaint box to print */
$(document).on("click", '#print_email_button', function () {
    favorite = [];
    var no_of_checked = $('[name="complaint_checkbox[]"]:checked').length;
    if (no_of_checked == 0) {
        toastr.warning('Please select at least one complaint.');
        return false;
    }
    $.each($("input[name='complaint_checkbox[]']:checked"), function () {
        favorite.push($(this).attr('data_id'));
    });

    $.ajax({
        type: 'post',
        url: '/EditOwnerAjax',
        data: {
            class: 'EditOwnerAjax',
            action: 'getComplaintsDataForPrint',
            'complaint_ids': favorite,
            'owner_id' : id
        },
        success: function (response) {
            var response = JSON.parse(response);
            if (response.status == 'success' && response.code == 200) {
                $("#print_complaint").modal('show');
                $("#modal-body-complaints").html(response.html)
            } else {
                toastr.warning('Record not updated due to technical issue.');
            }
        }
    });
});

function PrintElem(elem)
{
    Popup($(elem).html());
}

function Popup(data)
{
    var base_url = window.location.origin;
    var mywindow = window.open('', 'my div');
    $(mywindow.document.head).html('<link rel="stylesheet" href="'+base_url+'/company/css/main.css" type="text/css" /><style> li {font-size:20px;list-style-type:none;margin: 5px 0;\n' +
        'font-weight:bold;} .right-detail{\n' +
        '        position:relative;\n' +
        '        left:+400px;\n' +
        '    }</style>');
    $(mywindow.document.body).html( '<body>' + data + '</body>');
    mywindow.document.close();
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    if(mywindow.close()){

    }
    $("#PrintEnvelope").modal('hide');
    return true;
}

/*function to print element by id */
function sendComplaintsEmail(elem)
{
    var mail_length = $('#print_complaint #modal-body-complaints>table').length;

    var owner_email = $('#email').val();
    SendMail($(elem).html(), mail_length, owner_email);
}


function SendMail(data, mail_length, owner_email){
    $.ajax({
        type: 'post',
        url:'/EditOwnerAjax',
        data: {
            class: 'EditOwnerAjax',
            action: 'SendComplaintMail',
            data_html: data,
            'complaint_ids': favorite,
            'owner_id' : id,
            mail_length : mail_length,
            owner_email : owner_email,
        },
        success : function(response){
            var response =  JSON.parse(response);
            if(response.status == 'success' && response.code == 200) {
                toastr.success('Mail sent successfully.');
                $("#print_complaint").modal('hide');
            }else {
                toastr.warning('Mail not send due to technical issue.');
            }
        }
    });
}


fetchAllcomplaintType();
function fetchAllcomplaintType(id) {
    $.ajax({
        type: 'post',
        url: '/EditOwnerAjax',
        data: {
            class: 'editOwnerAjax',
            action: 'fetchAllComplaintType'},
        success: function (response) {
            var res = JSON.parse(response);
            $('#complaint_type_options').html(res.data);
            if (id != false) {
                $('#complaint_type_options').val(id);
            }
        },
    });
}