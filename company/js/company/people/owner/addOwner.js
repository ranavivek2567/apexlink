$(document).ready(function () {
    $("#selectProperty").addClass("modalScroll");
    $("body").addClass("modalopen-hide");
});
$(function () {
    initialData();


    /*for multiple SSN textbox*/
    $(document).on("click",".ssn-plus-sign",function(){
        var clone = $("#multiple_ssn_div_id").clone().insertAfter("div.multipleSsn:last");
        clone.find('input[type=text]').val('')
        $(".multipleSsn:not(:eq(0))  .ssn-plus-sign").remove();
        $(".multipleSsn:not(:eq(0))  .ssn-remove-sign").show();
    });
    /* for multiple SSN textbox*/

    /*remove ssn textbox*/
    $(document).on("click",".ssn-remove-sign",function(){
        $(this).parents(".multipleSsn").remove();
    });

    $(document).on("click",".primary-owner-phone-row .fa-plus-circle",function(){
        var clone = $("#primary-owner-phone-row-divId").clone().insertAfter("div.primary-owner-phone-row:last");
        clone.find('.work_extension_div').hide();
        clone.find('input[type=text]').val('');
        clone.find('select').next().text('');
        clone.find('input[type=text]').next().text('');
        clone.find('select').val('');
        clone.find('.countycodediv').find('select').val(220);
        // var c = clone.find('.countycodediv').find('#country_code').val(220);

        // console.log('fsdfsd>>>', c);
        $(".primary-owner-phone-row:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-owner-phone-row:not(:eq(0))  .fa-times-circle").show();

        var phoneRowLenght = $(".primary-owner-phone-row");
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        }else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-owner-phone-row:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-owner-phone-row:not(:eq(0)) .fa-plus-circle").show();
        }
    });


/**    $(document).on('change','#phone_type',function(){
        var phone_type = $(this).val();

        if(phone_type == '2' || phone_type == '5')
        {
            $('#work_phone_extension-error').text('');
            $(this).closest('div').next().show();
            $(this).closest('div').next().find('#work_phone_extension').removeAttr("disabled");
        } else {
            $(this).closest('div').next().hide();
            $(this).closest('div').next().find('#work_phone_extension').prop('disabled', true);
        }
    });
*/
    $(document).on('change','#phone_type',function(){
        var phone_type = $(this).val();

        console.log( 'aaaaa>>>>>',$(this).parent().next('div').next('div'));
        if(phone_type == '2' || phone_type == '5')
        {
            $('#work_phone_extension-error').text('');
            console.log( $(this).closest('div'));
            $(this).parent().next('div').next('div').show();
        } else {
            $(this).parent().next('div').next('div').hide();
        }
    });


    $(document).on("click",".primary-owner-phone-row .fa-times-circle",function(){

        var phoneRowLenght = $(".primary-owner-phone-row");
        if (phoneRowLenght.length >= 2) {
            $(".primary-owner-phone-row:eq(0) .fa-plus-circle").show();
        }else if (phoneRowLenght.length == 3) {
            $(".primary-owner-phone-row:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-owner-phone-row:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-owner-phone-row").remove();
    });

    /*for multiple email textbox*/
    $(document).on("click",".email-plus-sign",function(){
        var emailDivLength = $('.multipleEmail').length;
        if (emailDivLength <= 2) {
            var clone = $("#multiple_email_div_id").clone().insertAfter("div.multipleEmail:last");
            clone.find('input[type=text]').val('');
            // clone.find('#email[]-error').next().text('');
            $(".multipleEmail:not(:eq(0)) .email-plus-sign .fa-plus-circle").remove();
            $(".multipleEmail:not(:eq(0)) .email-remove-sign .fa-times-circle").show();
            if (emailDivLength == 2) {
                $('.email-plus-sign').hide();
            }
        } else {
            $('.email-plus-sign').hide();
        }

    });
    /*for multiple email textbox*/

    /*Remove Email Clone Textbox*/
    $(document).on("click",".email-remove-sign",function(){
        $(this).parents(".multipleEmail").remove();
        $(".email-plus-sign").show();
    });
    /*Remove Email Clone Textbox*/



    $(document).on("click",".add-emergency-contant",function(){

        var clone = $("#owner-emergency-contact_div_id:first").clone().insertAfter("div.owner-emergency-contact:last");
        clone.find('input[type=text]').val('');
        clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});
        clone.find(".add-emergency-contant").hide();
        clone.find("#otherRelationDiv").hide();
        clone.find(".remove-emergency-contant").show();
    });
    $(document).on("click",".remove-emergency-contant",function(){
        $(this).parents(".owner-emergency-contact").remove();
    });


    $(document).on('change','#relationship',function(){
        var relation = $(this).val();

        if(relation == '9')
        {
            $(this).closest('div').next().show();
        } else {
            $(this).closest('div').next().hide();
        }
    });


    $(document).on("click",".add-chart-bank",function(){
        var clone = $(".divBankAccount:first").clone();
        clone.find('input[type=text]').val('');
        // clone.find(".add-chart-bank").hide();
        clone.find(".remove-chart-bank").show();
        $(".divBankAccount").first().after(clone);
        $(".divBankAccount .add-chart-bank").hide();
    });
    $(document).on("click",".remove-chart-bank",function(){
        $(this).parents(".divBankAccount").remove();
        $(".divBankAccount .add-chart-bank").show();
    });


});
/**
 *Get location on the basis of zipcode
 */

function getAddressInfoByZip3(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    setTimeout(function(){
                        response3(addr);
                    }, 2000);

                } else {
                    response3({success:false});
                }
            } else {
                response3({success:false});
            }
        });
    } else {
        response3({success:false});
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response3(obj){
    if(obj.success){
        $("#addOwner #country").val(obj.country)
        $('#addOwner #city,#acc_city,#fccity,#fpcity').val(obj.city);
        $('#addOwner #state,acc_state,#fcstate,#fpstate').val(obj.state);

    } else {
        $("#addOwner #country").val('')
        $('#addOwner #city,#acc_city,#fccity,#fpcity').val('');
        $('#addOwner #state,acc_state,#fcstate,#fpstate').val('');
    }
}


function initialData() {
    $.ajax({
        type: 'post',
        url: '/OwnerAjax',
        data: {
            class: "OwnerAjax",
            action: "getIntialData"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {

                if (data.data.state.state != ""){
                    $("#driverProvince").val(data.data.state.state);
                }

                if (data.data.country.length > 0){
                    var countryOption = "<option value=''>Select</option>";
                    $.each(data.data.country, function (key, value) {
                        if(value.code == '+1'){
                            countryOption += "<option selected value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                        } else {
                            countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                        }

                    });
                    $('.countycodediv select').html(countryOption);
                    $('#flag_country_code').html(countryOption);
                    $('.emergencycountry').html(countryOption);
                }

                if (data.data.property_list.length > 0){
                    var propertyOption = "<option value=''>Select</option>";
                    $.each(data.data.property_list, function (key, value) {
                        if(value.property_percent_owned < 100){
                            propertyOption += "<option value='"+value.id+"' property_percent_owned='"+value.property_percent_owned+"' data-id='"+value.property_id+"' >"+value.property_name+"</option>";
                        }
                    });
                    $('#property_owned_id').html(propertyOption);
                }

                if (data.data.phone_type.length > 0){
                    var phoneOption = "";
                    $.each(data.data.phone_type, function (key, value) {
                        phoneOption += "<option value='"+value.id+"'>"+value.type+"</option>";
                    });
                    $('.primary-owner- email-plus-signphone-row select[name="phoneType[]"]').html(phoneOption);
                    $('.addition_owner_block select[name="additional_phoneType"]').html(phoneOption);
                }

                if (data.data.carrier.length > 0){
                    var carrierOption = "<option value=''>Select</option>";
                    $.each(data.data.carrier, function (key, value) {
                        carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                    });
                    $('.primary-owner-phone-row select[name="carrier[]"]').html(carrierOption);
                    $('.addition_owner_block select[name="additional_carrier"]').html(carrierOption);
                    $('.property_guarantor_form1 select[name="guarantor_carrier_1[]"]').html(carrierOption);
                    $('.additional_carrier').html(carrierOption);
                }

                if (data.data.referral.length > 0){
                    var referralOption =  "<option value=''>Select</option>";
                    $.each(data.data.referral, function (key, value) {
                        referralOption += "<option value='"+value.id+"'>"+value.referral+"</option>";
                    });
                    $('select[name="referral_source"]').html(referralOption);
                }

                if (data.data.ethnicity.length > 0){
                    var ethnicityOption = "<option value=''>Select</option>";
                    $.each(data.data.ethnicity, function (key, value) {
                        ethnicityOption += "<option value='"+value.id+"'>"+value.title+"</option>";
                    });
                    $('select[name="ethncity"]').html(ethnicityOption);
                    $('.addition_owner_block select[name="additional_ethncity"]').html(ethnicityOption);
                }

                if (data.data.marital.length > 0){
                    var maritalOption =  "<option value=''>Select</option>";
                    $.each(data.data.marital, function (key, value) {
                        maritalOption += "<option value='"+value.id+"'>"+value.marital+"</option>";
                    });

                    $('select[name="maritalStatus"]').html(maritalOption);
                    $('.marital_status').html(maritalOption);
                }

                if (data.data.hobbies.length > 0){
                    var hobbyOption = "";
                    $.each(data.data.hobbies, function (key, value) {
                        hobbyOption += "<option value='"+value.id+"'>"+value.hobby+"</option>";
                    });
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').html(hobbyOption);
                    $('select[name="hobbies[]"],select[name="additional_hobbies[]"]').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select'});
                }

                if (data.data.veteran.length > 0){
                    var veteranOption = "<option value=''>Select</option>";
                    $.each(data.data.veteran, function (key, value) {
                        veteranOption += "<option value='"+value.id+"'>"+value.veteran+"</option>";
                    });
                    $('select[name="veteran_status"]').html(veteranOption);
                    $('.veteran_status').html(veteranOption);
                }

                if (data.data.collection_reason.length > 0){
                    var reasonOption = "";
                    $.each(data.data.collection_reason, function (key, value) {
                        reasonOption += "<option value='"+value.id+"'>"+value.reason+"</option>";
                    });
                    $('.property_collection select[name="collection_reason"]').html(reasonOption);
                }

                if (data.data.credential_type.length > 0){
                    var typeOption = "<option value=''>Select</option>";
                    $.each(data.data.credential_type, function (key, value) {
                        typeOption += "<option value='"+value.id+"'>"+value.credential_type+"</option>";
                    });
                    $('.owner-credentials-control select[name="credentialType[]"]').html(typeOption);
                }
                if (data.data.account_name.length > 0){
                    var len = data.data.account_name.length;
                    var typeOption = "<option value=''>Select</option>";
                    var a = 0;
                    $.each(data.data.account_name, function (key, value) {
                        a = a+1;
                        if(a == len){
                            typeOption += "<option value='"+value.id+"' selected>"+value.account_name+"</option>";
                        } else {
                            typeOption += "<option value='"+value.id+"'>"+value.account_name+"</option>";
                        }

                    });
                    $('select[name="account_name[]"]').html(typeOption);
                }
                if (data.data.chart_accounts.length > 0){
                    var typeOption = "<option value=''>Select</option>";
                    $.each(data.data.chart_accounts, function (key, value) {
                        if(value.is_default == 0){
                            typeOption += "<option value='"+value.id+"' selected>"+value.account_name+'-'+value.account_code+"</option>";
                        } else {
                            typeOption += "<option value='"+value.id+"'>"+value.account_name+'-'+value.account_code+"</option>";
                        }

                    });
                    $('select[name="account_for_transaction[]"]').html(typeOption);
                }
                if (data.data.account_type.length > 0){
                    var typeOption = "<option value=''>Select</option>";
                    $.each(data.data.account_type, function (key, value) {
                        typeOption += "<option data-rangefrom="+value.range_from+" data-rangeto="+value.range_to+" value="+value.id+">"+value.account_type_name+"</option>";
                    });
                    $('select[name="account_type_id"]').html(typeOption);
                }

                // getZipCode('#addOwner #zipcode','10001','#addOwner #city','#addOwner #state','#addOwner #country','');
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}



/*notice period section copy*/
$(document).on("click",".add-notice-period",function(){
    var length = $('.credential-clone').length;
    var clone = $("#owner-credentials-control_divId").clone().insertAfter("div.owner-credentials-control:last");
    clone.find('input[type=text]').val('');
    clone.find('select[name="credentialType[]"]').val('');

    clone.find('#acquireDate')
        .removeClass('hasDatepicker')
        .removeData('datepicker')
        .removeAttr('id')
        .attr('id','acquireDate'+length)
        .unbind()
        .datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat})
        .datepicker("setDate", new Date());

    clone.find('#expirationDate')
        .removeClass('hasDatepicker')
        .removeData('datepicker')
        .removeAttr('id')
        .attr('id','expirationDate'+length)
        .unbind()
        .datepicker({yearRange: '+0:+100', changeMonth: true, changeYear: true, dateFormat: jsDateFomat, minDate: new Date()})
        .datepicker("setDate", new Date());

    // $(".owner-credentials-control").first().after(clone);
    clone.find(".add-notice-period").hide();
    clone.find(".remove-notice-period").show();
});


$(document).on("change", ".acquireDateClass", function () {
    var dynAcquireDivId= $(this).attr('id');
    var dynExpireDiveId= $("#"+dynAcquireDivId).closest('div').next().find('.expirationDateClass').attr('id');
    var value = $(this).val();

    $("#"+dynExpireDiveId).datepicker({minDate: value});
});

$(document).on("change", ".expirationDateClass", function () {
    var dynExpireDiveId = $(this).attr('id');
    var dynAcquireDivId= $("#"+dynExpireDiveId).closest('div').prev().find('.acquireDateClass').attr('id');
    var value = $(this).val();

    $("#"+dynAcquireDivId).datepicker({maxDate: value});
});

$(document).on("click",".remove-notice-period",function(){
    $(this).parents(".owner-credentials-control").remove();
});

/*Notest Textarea copy*/
$(document).on("click",".add-icon-textarea",function(){
    var clone = $("#ownerTxtAreaDivId").clone().insertAfter("div.ownerTxtArea:last");
    clone.find('.notes').val('');
    clone.find('.notes').on('keydown', function(event) {
        if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
            var $t = $(this);
            event.preventDefault();
            var char = String.fromCharCode(event.keyCode);
            $t.val(char + $t.val().slice(this.selectionEnd));
            this.setSelectionRange(1,1);
        }
    });


    clone.find(".add-icon-textarea").hide();
    clone.find(".remove-icon-textarea").show();

    var phoneRowLenght = $(".ownerTxtArea");
    if (phoneRowLenght.length == 2) {
        clone.find(".fa-plus-circle").hide();
    }else if (phoneRowLenght.length == 3) {
        clone.find(".fa-plus-circle").hide();
        $(".ownerTxtArea:eq(0) .fa-plus-circle").hide();
    }else{
        $(".ownerTxtArea:not(:eq(0)) .fa-plus-circle").show();
    }
});


$(document).on("click",".remove-icon-textarea",function(){
    var phoneRowLenght = $(".ownerTxtArea");
    if (phoneRowLenght.length >= 2) {
        $(".ownerTxtArea:eq(0) .fa-plus-circle").show();
    }else if (phoneRowLenght.length == 3) {
        $(".ownerTxtArea:eq(0) .fa-plus-circle").hide();
    }else{
        $(".ownerTxtArea:not(:eq(0)) .fa-plus-circle").show();
    }

    $(this).parents(".ownerTxtArea").remove();
});
/**** Clone - Property owned div****/
$(document).on("click",".add_property_div_plus",function() {
    var propertyDivLength = $('.owner_property_owned_outer').length;
    if (propertyDivLength <= 2) {
        var clone = $("#div_add_owner").clone().insertAfter("div.owner_property_owned_outer:last");
        clone.find('#property_owned_id').val('');
        clone.find('#property_owned_id').attr('id','property_owned_id'+propertyDivLength);
        clone.find('#property_percent_owned').val('100');
        clone.find('#property_percent_owned').attr('id','property_percent_owned'+propertyDivLength);
        $(".owner_property_owned_outer:not(:eq(0))  .add_property_div_plus").remove();
        $(".owner_property_owned_outer:not(:eq(0))  .remove_property_div_minus").show();
        if (propertyDivLength == 2) {
            $('.add_property_div_plus').hide();
        }
    } else {
        $('.add_property_div_plus').hide();
    }
});
/**** Remove Clone - Property owned div****/
$(document).on("click",".remove_property_div_minus",function(){
    $(this).parents(".owner_property_owned_outer").remove();
    $(".owner_property_owned_outer .add_property_div_plus").show();
});


//CropIT Image

$(document).on("click","#cropImageTrigger",function() {
    $('.cropit-image-input').trigger('click');
});
$(document).on("change",".cropit-image-input",function(){

    photo_videos = [];
    var fileData = this.files;
    var type= fileData.type;
    var elem = $(this);
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var uploadType = 'image';
        var validSize = '1030';
        var arrayType = validImageTypes;

        if ($.inArray(type, arrayType) < 0) {
            toastr.warning('Please select file with valid extension only!');
        } else {
            if(parseInt(size) <= 10)
            {
                toastr.warning('Image is too small.');
                return false;
            }
            if (parseInt(size) > validSize) {
                var validMb =  1;
                toastr.warning('Please select documents less than '+validMb+' mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                photo_videos.push(value);
                var src = '';
                var reader = new FileReader();
                $('#photo_video_uploads').html('');


                $(".popup-bg").show();
                elem.next().show();

            }

        }

    });

});

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}


$(document).ready(function(){
    $('.image-editor').cropit({
        imageState: {
            // src: subdomain_url+'500/400',
            src: subdomain_url+'500/400',
        },
    });

    $(document).on("click",'.export',function(){
        $(this).parents('.cropItData').hide();
        $(this).parent().prev().val('');
        var dataVal = $(this).attr("data-val");
        var imageData = $(this).parents('.image-editor').cropit('export');
        var image = new Image();
        image.src = imageData;
        $(".popup-bg").hide();
        $(this).parent().parent().prev().find('div').html(image);
    });

});

$(document).on("click",".closeimagepopupicon",function () {
    $(".popup-bg").hide();
    $(this).parent().hide();
});

$(document).on("click","#if_entity_name_display",function () {
    if ($(this).is(":checked")) {
        $('#ethnicity_to_ssn_div').hide();
    } else {
        $('#ethnicity_to_ssn_div').show();
    }
});

$(document).on("change", '.property_owned_name', function () {
    var selected_value = '';
    var owners = [];
    var owner_id = $(".property_owned_name");
    selected_value = $(this).val();
    $.each($(".property_owned_name option:selected"), function () {
        owners.push($(this).val());
    });
    if (owner_id.length > 0) {
        var itemtoRemove = selected_value;
        owners.splice($.inArray(itemtoRemove, owners), 1);
        if (jQuery.inArray(selected_value, owners) != -1) {
            $(this).prop('selectedIndex', 0);
            toastr.warning('Property already selected.')
        } else {
            var property_percent_owned = $('option:selected', this).attr('property_percent_owned');
            var remaining_percent = 100 - property_percent_owned;

            var percent_owned_id = $(this).parent().parent().parent().find('.property_percent_owned').attr('id');
            $('#'+percent_owned_id).val(remaining_percent).attr('percentage_limit', remaining_percent);
        }
    }
});

$(document).on("blur", '.property_percent_owned', function () {
    var percentage_limit = $(this).attr('percentage_limit');
    var prop_percent = $(this).val();
    if( parseFloat(prop_percent)> parseFloat(percentage_limit)) {
        $(this).val(percentage_limit);
        toastr.warning('Exceed the limit of remaining percentage of this property.');
    }
});


/*$(document).on("blur", ".address_field", function(e) {
    jQuery.validator.classRuleSettings.unique = {
        unique: true
    };

    jQuery.validator.addMethod("unique", function(value, element, params) {
        var prefix = params;
        var selector = jQuery.validator.format("[name!='{0}'][name^='{1}'][unique='{1}']", element.name, prefix);
        var matches = new Array();
        $(selector).each(function(index, item) {
            if (value == $(item).val()) {
                matches.push(item);
            }
        });

        return matches.length == 0;
    }, "* Duplicate entry");

});

$(".address_field").on('blur', function(evt) {
    var $field1 = $(this).val();
    $field1 = $field1.length;
    if($field1 <= 1){
        $(this).next('.error').text('');
    }
});*/

$("#addOwner").validate({
    ignore: [':hidden'],
    rules: {
        first_name: {
            required: true
        },
        last_name: {
            required: true
        },
        'carrier[]': {
            required: true,
        },
        'phone_number[]': {
            required: true,
        },
        'email[]': {
            required: true,
            email: true,
        },
        'property_id[]': {
            required: true
        },
        'emergency_email[]': {
            email: true,
        },
        // 'work_phone_extension[]': {
        //     required: true
        // }
    }

});

/*** Submit add owner form***/
$("#addOwner").on("submit",function(e) {
    e.preventDefault();
    var id =  getParameterByName('id');

    var validator = $("#addOwner").validate(), errors = validator.errorList;
    if(errors[5] !== undefined) {
        if (errors[5].message) {
            toastr.warning('Owner must be linked to a property.');
            $('html, body').animate({
                scrollTop: ($('#propertyOwnedDiv').offset().top)
            },500);
        }
    }
    if ($("#addOwner").valid()) {
        if(id)
        {
            updateOwnerDetails(id);
        }else {
            insertOwnerDetails();
        }

    }

});

$(document).on("click", "#cancel_add_owner_btn", function(e) {
    bootbox.confirm("Do you want to cancel this action now?", function(result) {
        if (result == true) {
            window.location.href = '/People/Ownerlisting';
        }
    });
});
$(document).on("click", "#reset_add_owner_btn", function(e) {
            window.location.href = '/People/AddOwners';
});
function insertOwnerDetails() {
    var form = $('#addOwner')[0];

    var formData = new FormData(form);
    formData.append('action','insert');
    formData.append('class','OwnerAjax');

    action = 'insert';
    var owner_image = $('.owner_image').html();
    var ownerImage = JSON.stringify(owner_image)
    formData.append('owner_image',ownerImage);
    formData.append('stripe_detail_form_data', stripe_detail_form_data);
    var custom_field = [];
    $(".custom_field_html input").each(function(){
        var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
        custom_field.push(data);
    });
    var custom_field = JSON.stringify(custom_field);

    // var a =  JSON.stringify(custom_field);
    // formData.append('custom_field',a);
    formData.append('custom_field', custom_field);

    $.ajax({
        url:'/OwnerAjax',
        type: 'POST',
        data: formData,
        success: function (response) {
            var response = JSON.parse(response);
           console.log(response);
            if(response.OwnerUserDetails.status == "success" && response.OwnerUserDetails.owner_id != ""){
                localStorage.setItem("Message", 'Record added successfully.');
                localStorage.setItem('rowcolor', 'rowColor');
                // setTimeout(function(){
                //     jQuery('#owner_listing').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                //     jQuery('#owner_listing').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                //     localStorage.removeItem('rowcolor');
                // }, 1000);
                window.location.href = '/People/Ownerlisting';
            } else if (response.OwnerUserDetails.code == 500 && response.OwnerUserDetails.status == 'error'){
                toastr.warning(response.OwnerUserDetails.message);
            } else if (response.OwnerUserDetails.code == 400 && response.OwnerUserDetails.status == 'error'){
                toastr.warning(response.OwnerUserDetails.message);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });

}
function updateOwnerDetails(id) {


    var form = $('#addOwner')[0];

    var formData = new FormData(form);
    formData.append('action','update');
    formData.append('class','OwnerAjax');
    formData.append('edit_id',id);

    var owner_image = $('.owner_image').html();
    var ownerImage = JSON.stringify(owner_image)
    formData.append('owner_image',ownerImage);
    var custom_field = [];
    $(".custom_field_html input").each(function(){
        var data = {'name':$(this).attr('name'),'value':$(this).val(),'id':$(this).attr('data_id'),'is_required':$(this).attr('data_required'),'data_type':$(this).attr('data_type'),'default_value':$(this).attr('data_value')};
        custom_field.push(data);
    });
    var custom_field = JSON.stringify(custom_field);

    // var a =  JSON.stringify(custom_field);
    // formData.append('custom_field',a);
    formData.append('custom_field', custom_field);

    $.ajax({
        url:'/OwnerAjax',
        type: 'POST',
        data: formData,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == "success" && response.owner_id != ""){
                localStorage.setItem("Message", 'Record updated successfully.');
                localStorage.setItem('rowcolor', 'rowColor');
                // setTimeout(function(){
                //     jQuery('#owner_listing').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                //     jQuery('#owner_listing').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                //     localStorage.removeItem('rowcolor');
                // }, 1000);
               window.location.href = '/People/Ownerlisting';
            } else if (response.OwnerUserDetails.code == 500 && response.OwnerUserDetails.status == 'error'){
                toastr.warning(response.OwnerUserDetails.message);
            } else if (response.OwnerUserDetails.code == 400 && response.OwnerUserDetails.status == 'error'){
                toastr.warning(response.OwnerUserDetails.message);
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });

}
$(document).on('change','#salutation',function(){
    var salutation = $(this).val();

    if(salutation == 'Mrs.' || salutation == 'Ms.' || salutation == 'Madam' || salutation == 'Sister' || salutation == 'Mother')
    {
        $('#maiden_name').parents('.col-sm-3').show();
        $("#gender").val("2");
    } else {
        $("#gender").val("1");
        $('#maiden_name').parents('.col-sm-3').hide();
    }
});

$(document).on("blur", '#entity_company', function () {
    var entity_company = $(this).val();
    if( entity_company !== '') {
        $('#tax_payer_name').val(entity_company);
        $('.company_name_as_tax_payer_div').show();
    } else {
        $('#tax_payer_name').val('');
        $('.company_name_as_tax_payer_div').hide();
    }
});

$(document).on("blur", '#tax_payer_name', function () {
    var tax_payer_name = $(this).val();
    var entity_company_name = $('#entity_company').val();
    if( tax_payer_name == entity_company_name) {
        $('#company_name_as_tax_payer').prop('checked', true);
    } else {
        $('#company_name_as_tax_payer').prop('checked', false);
    }
});

$(document).on('click', '#company_name_as_tax_payer', function () {
    var entity_company_name = $('#entity_company').val();
    if ($('#company_name_as_tax_payer').is(":checked")) {
        $('#tax_payer_name').val(entity_company_name);
    } else {
        $('#tax_payer_name').val('');
    }
});


getCompanyDefaultData();
// var default_zipcode = '10001';
function getCompanyDefaultData() {

    $.ajax({
        type: 'post',
        url: '/get-company-default-data',
        data: {
            class: 'commonPopup',
            action: 'getCompanyDefaultData'},
        success: function (response) {
            var res = JSON.parse(response);
            if(res.record.zip_code) {
                getAddressInfoByZip3(res.record.zip_code);
                $('#zipcode,#fczipcode,#fpzipcode').val(res.record.zip_code);
                $('#selectProperty #propertyZipcode').val(res.record.zip_code);
                default_zipcode = res.record.zip_code;
                //getZipCode('#addOwner #zipcode',res.record.zip_code,'#addOwner #city','#addOwner #state','#addOwner #country','');




            } else {
                $('#addOwner #zipcode,#fczipcode,#fpzipcode').val('10001');
                $('#selectProperty #propertyZipcode').val('10001');
                //getZipCode('#addOwner #zipcode','10001','#addOwner #city','#addOwner #state','#addOwner #country','');
                getAddressInfoByZip3('10001')

            }
        },
    });
}



/*Online Payment*/
$(document).on('click','.onlinePaymentInfo',function(e){
    e.preventDefault();
    var first_name = $('#addOwner #first_name').val();
    // alert(first_name);
    var last_name = $('#addOwner #last_name').val();
    var company_name = $('#addOwner #entity_company').val();
    var address1 = $('#addOwner #address1').val();
    var address2 = $('#addOwner #address2').val();
    var state = $('#addOwner #state').val();                       																							$('#ffirst_name').val(first_name);
    var city = $('#addOwner #city').val();                       																							$('#ffirst_name').val(first_name);
    var zipcode = $('#addOwner #zipcode').val();
    var phoneNumber = $('#addOwner #phone_number').val();

    $('#ffirst_name').val(first_name);
    $('#flast_name').val(last_name);
    $('#cCompany').val(company_name);
    $('#caddress1').val(address1);
    $('#caddress2').val(address2);
    $('#cphoneNumber').val(phoneNumber);
    $('#ccity').val(city);
    $('.cstate').val(state);
    $('.czip_code').val(zipcode);

});

$(document).on('click','#financial-info .basicDiv',function(){
    $(".basic-payment-detail").show();
    $(".apx-adformbox-content").hide();
    $(this).addClass("active");
    $(".paymentDiv").removeClass("active");
});
$(document).on('click','#financial-info .paymentDiv',function(){
    $(".basic-payment-detail").hide();
    $(".apx-adformbox-content").show();
    $(this).addClass("active");
    $(".basicDiv").removeClass("active");
});
/*Online Payment*/

document.getElementById('rotate-ccw').addEventListener('click', function() {
    $('.image-editor').cropit('rotateCCW')
});
document.getElementById('rotate-cw').addEventListener('click', function() {
    $('.image-editor').cropit('rotateCW')
});





/*Online Payment*/
$(document).on('click','.onlinePaymentInfo',function(e){
    e.preventDefault();
    var first_name = $('#addOwner #first_name').val();
    // alert(first_name);
    var last_name = $('#addOwner #last_name').val();
    var company_name = $('#addOwner #entity_company').val();
    var address1 = $('#addOwner #address1').val();
    var address2 = $('#addOwner #address2').val();
    var state = $('#addOwner #state').val();                       																							$('#ffirst_name').val(first_name);
    var city = $('#addOwner #city').val();                       																							$('#ffirst_name').val(first_name);
    var zipcode = $('#addOwner #zipcode').val();
    var phoneNumber = $('#addOwner #phone_number').val();

    $('#ffirst_name').val(first_name);
    $('#flast_name').val(last_name);
    $('#cCompany').val(company_name);
    $('#caddress1').val(address1);
    $('#caddress2').val(address2);
    $('#cphoneNumber').val(phoneNumber);
    $('#ccity').val(city);
    $('.cstate').val(state);
    $('.czip_code').val(zipcode);

});

$(document).on('click','#financial-info .basicDiv',function(){
    $(".basic-payment-detail").show();
    $(".apx-adformbox-content").hide();
    $(this).addClass("active");
    $(".paymentDiv").removeClass("active");
});
$(document).on('click','#financial-info .paymentDiv',function(){
    $(".basic-payment-detail").hide();
    $(".apx-adformbox-content").show();
    $(this).addClass("active");
    $(".basicDiv").removeClass("active");
});
/*Online Payment*/

document.getElementById('rotate-ccw').addEventListener('click', function() {
    $('.image-editor').cropit('rotateCCW')
});
document.getElementById('rotate-cw').addEventListener('click', function() {
    $('.image-editor').cropit('rotateCW')
});



