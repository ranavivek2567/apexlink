$(document).ready(function () {
    var base_url = window.location.origin;


    getPortalInitialData();
    function getPortalInitialData(billing_info_as_contact_info = null) {
        $.ajax({
            type: 'post',
            url: '/OwnerPortal-Ajax',
            data: {
                class: 'viewAccountInfoAjax',
                action: 'goToOwnerPortal',
                id: owner_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var user_detail_data      = response.data.user_data;
                    var owner_detail_data     = response.data.owner_detail;
                    var owner_billing_data     = response.data.owner_billing_data;
                    var emergency_detail_data = response.emergency_data;

                    $.each(user_detail_data, function (key, value) {
                        $('.' + key).text(value);

                        if (owner_billing_data) {
                            if (owner_billing_data.billing_info_as_contact_info && owner_billing_data.billing_info_as_contact_info !== '') {
                                if (owner_billing_data.billing_info_as_contact_info == '1') {
                                    $('#billing_info_as_contact_info').prop('checked', true);
                                    $('.billing_' + key).text(value);
                                } else {
                                    $('#billing_info_as_contact_info').prop('checked', false);
                                }
                            }
                        }
                    });

                    if (owner_detail_data.owner_image != '') {
                        $('.owner_image').html(owner_detail_data.owner_image);
                    }

                    var emergency_detail_data_html = '';
                    if (emergency_detail_data.length > 0) {
                        $.each(emergency_detail_data, function (key, value) {
                            emergency_detail_data_html += "<div class=\"row\" style='margin-bottom: 15px;'>\n" +
                                "<div class=\"col-sm-6\">\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Emergency Contact Name : </label>\n" +
                                "<span class=\"emeregency_name\">" + value.emergency_contact_name + "</span>\n" +
                                "</div>\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Country Code :</label>\n" +
                                "<span class=\"country_code\"> " + value.emergency_country_code + "</span>\n" +
                                "</div>\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Emergency Contact Phone : </label>\n" +
                                "<span class=\"emeregency_contact\">" + value.emergency_phone_number + "</span>\n" +
                                "\</div>\n" +
                                " </div>\n" +
                                "<div class=\"col-sm-6\">\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Emergency Contact Email : </label>\n" +
                                "<span class=\"emeregency_email\">" + value.emergency_email + "</span>\n" +
                                "</div>\n" +
                                "<div class=\"col-xs-12\">\n" +
                                "<label class=\"text-right\">Emergency Contact Relation : </label>\n" +
                                "<span class=\"emeregency_relation\">" + value.emergency_relation + "</span>\n" +
                                "</div>\n" +
                                "</div>\n" +
                                "</div>"
                        });
                        $('.emergency_detail_data').html(emergency_detail_data_html);
                    }

                    $('.select_options').prop('selectedIndex', 0);

                } else {
                    toastr.warning('Site not redirecting due to technical issue.');
                }
            }
        });
    }



    $.validator.addMethod("password_regex", function(value, element) {
        var password_regex = this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9])/.test(value);
        return password_regex;
    }, "* Atleast one alphanumeric and a numeric character required");


    $( "#changePasswordFormId" ).validate({
        rules: {
            password:{
                required:true,
                password_regex : true
            },
            new_password:{
                required:true,
                minlength:8,
                password_regex : true
            },
            confirm_new_password:{
                required:true,
                minlength:8,
                password_regex : true,
                equalTo :'#new_password'
            }
        },
        messages: {
            new_password: {
                minlength: "* Minimum 8 characters allowed",
            },
            confirm_new_password: {
                equalTo: "* Fields do not match",
                minlength: "* Minimum 8 characters allowed",
            },

        }
    });

    $('#changePasswordFormId').on('submit',function (e) {
        e.preventDefault();
        var current_password = $("#password").val();
        var new_password = $("#new_password").val();
        var confirm_new_password = $("#confirm_new_password").val();
        if ($("#changePasswordFormId").valid()) {
            changeOwnerPassword(current_password, new_password, confirm_new_password);
        }
        return false;
    });

    function changeOwnerPassword(current_password, new_password, confirm_new_password) {
        $.ajax({
            type: 'post',
            url: '/OwnerPortal-Ajax',
            data: {
                class       : 'viewAccountInfoAjax',
                action      : 'changePassword',
                id          : owner_id,
                currentPass : current_password,
                nPassword   : new_password,
                cPassword   : confirm_new_password,
            },

            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("Password changed successfully.");
                    $("#password").val('');
                    $("#new_password").val('');
                    $("#confirm_new_password").val('');
                    $("#strength").empty();
                    if ($('#result').hasClass('good')){
                        $("#password_checker span").removeClass("good");
                    }
                    if ($('#result').hasClass('short')){
                        $("#password_checker span").removeClass("short");
                    }
                    if ($('#result').hasClass('weak')){
                        $("#password_checker span").removeClass("weak");
                    }
                    if ($('#result').hasClass('strong')){
                        $("#password_checker span").removeClass("strong");
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    $(document).on('click','#cancel_change_password',function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href =  base_url+'/Owner/MyAccount/AccountInfo';
                }
            }
        });
    });

    /**
     * jquery to redirect for edits
     */
    $(document).on('click', '.edit_redirection', function () {
        var redirection_data = $(this).attr('redirection_tab');
        localStorage.setItem("AccordionHref",redirection_data);
        window.location.href = window.location.origin+'/Owner/MyAccount/UpdateAccountInfo';
    });

});
$(document).on("click","#cropImageTrigger",function() {
    $('.cropit-image-input').trigger('click');
});
$(document).on("change",".cropit-image-input",function(){

    photo_videos = [];
    var fileData = this.files;
    var type= fileData.type;
    var elem = $(this);
    $.each(this.files, function (key, value) {
        var type = value['type'];
        var size = isa_convert_bytes_to_specified(value['size'], 'k');
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
        var uploadType = 'image';
        var validSize = '1030';
        var arrayType = validImageTypes;

        if ($.inArray(type, arrayType) < 0) {
            toastr.warning('Please select file with valid extension only!');
        } else {

            if (parseInt(size) > validSize) {
                var validMb =  1;
                toastr.warning('Please select documents less than '+validMb+' mb!');
            } else {
                size = isa_convert_bytes_to_specified(value['size'], 'k') + 'kb';
                photo_videos.push(value);
                var src = '';
                var reader = new FileReader();
                $('#photo_video_uploads').html('');


                $(".popup-bg").show();
                elem.next().show();

            }

        }

    });

});

function isa_convert_bytes_to_specified(bytes, to) {
    var formulas =[];
    formulas['k']= (bytes / 1024).toFixed(1);
    formulas['M']= (bytes / 1048576).toFixed(1);
    formulas['G']= (bytes / 1073741824).toFixed(1);
    return formulas[to];
}


$(document).ready(function(){
    $('.image-editor').cropit({
        imageState: {
            // src: subdomain_url+'500/400',
        },
    });

    $(document).on("click",'.export',function(){
        $(this).parents('.cropItData').hide();
        $(this).parent().prev().val('');
        var dataVal = $(this).attr("data-val");
        var imageData = $(this).parents('.image-editor').cropit('export');
        var image = new Image();
        image.src = imageData;


        $(".popup-bg").hide();
        $(this).parent().parent().prev().find('div').html(image);
        // var owner_id =  localStorage.owner_portal_id;
        console.log('aaaa>>>>>', owner_id);
        var formData = new FormData();
        formData.append('action', 'updateOwnerImage');
        formData.append('class', 'updateOwnerImage');
        var imgData = $('.owner_image.img-outer').html();
        formData.append('image', imgData);
        formData.append('id', owner_id);
        $.ajax({
            url:'/OwnerPortalEdit-Ajax',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success"){
                    toastr.success(response.message);
                } else if (response.OwnerUserDetails.code == 500 && response.OwnerUserDetails.status == 'error'){
                    toastr.warning(response.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

});

$(document).on("click",".closeimagepopupicon",function () {
    $(".popup-bg").hide();
    $(this).parent().hide();
});


/** Conversation div js  starts*/
$(document).on("click","#add_owner_conversation_btn",function(){
    $('.add_owner_conversation_div').find('textarea').val('');
    var validator = $("#add_owner_conversation_form_id").validate();
    validator.resetForm();
    fetchAllProblemCategory();
    getOwnersPropertyManager(owner_id, 'owner_tab');
    $('.add_owner_conversation_div').show();
});

$(document).on("click","#cancel_add_owner_conversation",function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('.add_owner_conversation_div').hide();
        }
    });
});

getOwnersPropertyManager(owner_id, 'owner_tab');
function getOwnersPropertyManager(owner_id, owner_tab) {
    $.ajax({
        type: 'POST',
        url: '/Conversation-Ajax',
        data: {
            class: "conversationAjax",
            action: "getUserPropertyManagerList",
            id:owner_id,
            active_tab:owner_tab
        },
        success: function (response) {
            var response = JSON.parse(response);
            $('.select_mangers_option').html(response.data);

        }
    });
}

fetchAllConversations('', owner_id);
function fetchAllConversations(selected_manager,selected_user){
    var active_tab = 'owner_tab';
    $.ajax({
        type: 'post',
        url: '/Conversation-Ajax',
        data: {
            class: 'conversationAjax',
            action: 'getAllConversationByType',
            active_tab : active_tab,
            selected_manager : selected_manager,
            selected_user : selected_user,
            is_portal: 'Owner_Portal'
        },
        success: function (response) {
            var res = JSON.parse(response);
            var html = '';
            $.each(res.data, function (key, val) {
                var user_type = '';
                if (val.comment_by == 'yes'){
                    user_type = val.user_type;
                } else {
                    if (val.user_type == 1) {
                        user_type = 'Tenant'
                    } else if (val.user_type == 2) {
                        user_type = 'Owner'
                    } else {
                        user_type = 'Vendor'
                    }
                }
                var user_image = '<img src='+default_Image+'>';
                if(val.selected_user_image){
                    if (val.selected_user_image.owner_image && val.selected_user_image.owner_image !='undefined') {
                        user_image = val.selected_user_image.owner_image;
                    }
                    if (val.selected_user_image.tenant_image && val.selected_user_image.tenant_image !='undefined') {
                        user_image = val.selected_user_image.tenant_image;
                    }
                    if (val.selected_user_image.image && val.selected_user_image.image !='undefined') {
                        user_image = val.selected_user_image.image;
                    }
                }

                html += '<li >\n' +
                    '<div class="row">\n' +
                    '<div class="col-sm-1 conversation-lt">\n' +
                    '<div class="img-outer">\n' +user_image+
                    '</div>\n' +
                    '</div>\n' +
                    '<div class="col-sm-10 pad-none conversation-rt">\n' +
                    '<span class="dark_name">'+val.selected_user_name+'('+user_type+') :</span>\n' +
                    '<span class="light_name">'+val.login_user_name+'('+val.login_user_role+')</span>\n' +
                    '<span class="dark_name">'+val.problem_category_name+'</span>\n' +
                    '<span class="black_text" >'+val.description+'</span>\n' +
                    '<div class="conversation-time clear">\n' +
                    '<span class="black_text" >'+val.created_date+'-</span>\n' +
                    '<a class="light_name comment_on_conversation" conversation_id="'+val.id+'">Comment</a>\n' +
                    '</div>\n' +
                    '<div class="viewConversationCommentDiv">\n' +
                    '</div>'+
                    '<div class="textConversationCommentDiv">\n' +
                    '</div>'+
                    '</div>\n' +
                    '<div class="col-sm-1 pull-right">\n' +
                    '<span class="viewAllComment" conversation_id="'+val.id+'" ><i class="fa fa-plus-circle" aria-hidden="true"></i></span> \n' +
                    '</div>\n' +
                    '</div>\n' +
                    '</li>';
            });
            $('.conversation_list').html(html);
        },
    });
}

$(document).on("click",".viewAllComment",function () {
    var active_tab = 'owner_tab';
    var conversation_id = $(this).attr('conversation_id');
    var viewCommentHtml = $(this).parent().prev().find('.viewConversationCommentDiv');
    var viewCommentFindActive = $(this).parent().prev().find('.viewConversationCommentDiv').hasClass('active');
    if(viewCommentFindActive) {
        viewCommentHtml.html('');
        viewCommentHtml.removeClass('active');
    } else {
        $.ajax({
            type: 'post',
            url: '/Conversation-Ajax',
            data: {
                class: 'conversationAjax',
                action: 'getAllConversationByType',
                conversation_id: conversation_id,
                active_tab : active_tab
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.status == 'success' && response.code == 200) {
                    var comment_html = '';
                    $.each(response.data, function (key, val) {
                        var user_image = '<img src='+default_Image+'>';
                        if(val.selected_user_image){
                            if (val.selected_user_image.owner_image && val.selected_user_image.owner_image !='undefined') {
                                user_image = val.selected_user_image.owner_image;
                            }
                            if (val.selected_user_image.tenant_image && val.selected_user_image.tenant_image !='undefined') {
                                user_image = val.selected_user_image.tenant_image;
                            }
                            if (val.selected_user_image.image && val.selected_user_image.image !='undefined') {
                                user_image = val.selected_user_image.image;
                            }
                        }

                        comment_html += '<div style="" id="ConversationOwnerComment_0">\n' +
                            '<div class="conversation-comment-grey removeClass">\n' +
                            '<div class="conversation-arrow"></div>\n' +
                            '<div class="col-sm-1 conversation-lt">\n' +
                            '<div class="img-outer">\n' +user_image+
                            '</div>\n' +
                            '</div>\n' +
                            '<div class="col-sm-10 conversation-rt">\n' +
                            '<span class="dark_name">' + val.login_user_name + '(' + val.login_user_role + ') :</span>\n' +
                            '<span class="black_text" >' + val.description + '</span>\n' +
                            '<div class="conversation-comment-time">' + val.created_date + '</div>\n' +
                            '</div>\n' +
                            '</div>\n' +
                            '</div>'
                    });
                    viewCommentHtml.append(comment_html);
                    viewCommentHtml.addClass('active');
                } else if (response.status == 'error' && response.code == 502) {
                    toastr.error(response.message);
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            }
        });
    }
});


$(document).on("click",".comment_on_conversation",function () {
    var conversation_id = $(this).attr('conversation_id');
    $('.textConversationCommentDiv').html('');
    var commentHtml = '<form id="sendConversationCommentForm">\n' +
        '<input type="hidden" name="conversation_id" value="'+conversation_id+'">\n' +
        '<textarea class="form-control capital" name="comment_text" rows="6"></textarea>\n' +
        '<div class="btn-outer top-marginCls">\n' +
        '<button class="blue-btn sendConversationCommentBtn">Send</button>\n' +
        '<button  type="button" class="grey-btn cancelConversationCommentDiv">Cancel</button>\n' +
        '</div>\n' +
        '</form>\n';
    $(this).parent().next().next().html(commentHtml);
});
$(document).on("click",".cancelConversationCommentDiv",function () {
    console.log('cancel>>>',  $(this).parent());
    $(this).parents('.textConversationCommentDiv').html('');
});

$(document).on('submit','#sendConversationCommentForm',function(e) {
    e.preventDefault();
    var active_tab = 'owner_tab';
    var form = $('#sendConversationCommentForm')[0];
    var formData = new FormData(form);

    formData.append('class', 'conversationAjax');
    formData.append('action', 'sendCommentOnConversation');
    formData.append('active_tab', active_tab);
    formData.append('is_portal', 'Owner_Portal');

    $.ajax({
        url: '/Conversation-Ajax',
        type: 'post',
        data:  formData,
        contentType: false,
        processData: false,
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                fetchAllConversations('',owner_id);
                toastr.success("Record created successfully.");
            }else if(response.status == 'error' && response.code == 502){
                toastr.error(response.message);
            } else if(response.status == 'error' && response.code == 400){
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.'+key).html(value);
                });
            }
        }
    });
});

$("#add_owner_conversation_form_id").validate({
    rules: {
        selected_user_id: {
            required: true
        },
        problem_category: {
            required: true
        },
        description: {
            required: true
        }
    },
    submitHandler: function (form) {
        event.preventDefault();

        var form = $('#add_owner_conversation_form_id')[0];
        var formData = new FormData(form);


        formData.append('class', 'conversationAjax');
        formData.append('action', 'addConversation');
        formData.append('active_tab', 'owner_tab');
        formData.append('is_portal', 'Owner_Portal');
        console.log('formData>>', formData);

        $.ajax({
            url: '/Conversation-Ajax',
            type: 'post',
            data:  formData,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    $('.add_owner_conversation_div').hide();
                    fetchAllConversations('',owner_id);
                    toastr.success("Record created successfully.");
                }else if(response.status == 'error' && response.code == 502){
                    toastr.error(response.message);
                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }
            }
        });
    }
});

function fetchAllProblemCategory(){
    $.ajax({
        type: 'post',
        url: '/Conversation-Ajax',
        data: {
            class: 'conversationAjax',
            action: 'getAllProblemCategory',
        },
        success: function (response) {
            var res = JSON.parse(response);
            $('.problem_category').html(res.data);
        },
    });
}

/** Conversation div js ends **/

/**** Save problem category popup data starts ****/

$(document).on("click",".pop-add-icon",function () {
    $(".add-popup-body input[type='text']").val('');
    var text_id = $(this).closest('div').find('.add-popup').attr('id');
    $("#"+text_id+" .required").text('');
    $(this).closest('div').find('.add-popup').show();
});

$(document).on("click",".add-popup .grey-btn",function(){
    var box = $(this).closest('.add-popup');
    $(box).hide();
});

$(document).on("click",".add_single1", function () {
    var tableName = $(this).attr("data-table");
    var colName = $(this).attr("data-cell");
    var className = $(this).attr("data-class");
    var selectName = $(this).attr("data-name");
    var val = $(this).parent().prev().find("."+className).val();

    if (val == ""){
        $(this).parent().prev().find("."+className).next().text("* This field is required");
        return false;
    }else{
        $(this).parent().prev().find("."+className).next().text("");
    }

    savePopUpData(tableName, colName, val, selectName);
    var val = $(this).parent().prev().find("input[type='text']").val('');
});

function  savePopUpData(tableName, colName, val, selectName) {
    $.ajax({
        type: 'post',
        url: '/Tenantlisting/savePopUpData',
        data: {
            class: "TenantAjax",
            action: "savePopUpData",
            tableName:tableName,
            colName: colName,
            val:val
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success") {
                var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                $("select[name='"+selectName+"']").append(option);
                $("select[name='"+selectName+"']").next().hide();
                toastr.success(data.message);
            } else if (data.status == "error") {
                toastr.error(data.message);
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
$(document).on("click",".cancel",function(){
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            location.reload();
        }
    });

});
/**** Save problem category popup data ends here****/

$(document).on('click','#clearAddConversationForm',function() {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('.add_owner_conversation_div').find('textarea').val('');
            var validator = $("#add_owner_conversation_form_id").validate();
            validator.resetForm();
            fetchAllProblemCategory();
            getOwnersPropertyManager(owner_id, 'owner_tab');
        }
    });
});
$(document).on("click","#clearAddProblemCategoryForm",function(){
    resetFormClear('#selectProblemCategoryPopup',[], 'div',false);
    $(this).parents('.add-popup span.required').html('');
});
$(document).on('click','#clearChangePasswordForm',function() {
    bootbox.confirm("Do you want to Clear this form?", function (result) {
        if (result == true) {
            resetFormClear('#changePasswordFormId', [], 'form', false);
        }
    });
});