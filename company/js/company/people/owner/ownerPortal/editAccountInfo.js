$(document).ready(function () {
    var base_url = window.location.origin;


    setTimeout(function(){
        if(localStorage.getItem("AccordionHref")) {
            var message = localStorage.getItem("AccordionHref");
            console.log('message>>', message);
            $('.'+message).addClass('active');
            $('#'+message).addClass('active');
            // localStorage.removeItem('AccordionHref');
        }
    }, 250);



    $(document).on('change','#salutation',function(){
        var salutation = $(this).val();

        if(salutation == 'Mrs.' || salutation == 'Ms.' || salutation == 'Madam' || salutation == 'Sister' || salutation == 'Mother')
        {
            $('#maiden_name').parents('.col-sm-3').show();
            $("#gender").val("2");
        } else {
            $("#gender").val("1");
            $('#maiden_name').parents('.col-sm-3').hide();
        }
    });
    getPortalInitialData();
    function getPortalInitialData(checkBilling = null) {
        $.ajax({
            type: 'post',
            url: '/OwnerPortalEdit-Ajax',
            data: {
                class: 'editAccountInfoAjax',
                action: 'viewForEditAccountInfo',
                id: owner_id,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var user_detail_data      = response.data.user_data;
                    var owner_detail_data     = response.data.owner_detail;
                    var owner_billing_data     = response.data.owner_billing_data;
                    var emergency_detail_data = response.emergency_data;

                    if (response.data.carrier.length > 0){
                        var carrierOption = "<option value=''>Select</option>";
                        $.each(response.data.carrier, function (key, value) {
                            carrierOption += "<option value='"+value.id+"'>"+value.carrier+"</option>";
                        });
                        $('.primary-owner-phone-row select[name="carrier[]"]').html(carrierOption);
                        $('.additional_carrier').html(carrierOption);
                    }
                    if (response.data.country.length > 0){
                        var countryOption = "<option value=''>Select</option>";
                        $.each(response.data.country, function (key, value) {
                            if(value.code == '+1'){
                                countryOption += "<option selected value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                            } else {
                                countryOption += "<option value='"+value.id+"' data-id='"+value.code+"'>"+value.name+" ("+value.code+")"+"</option>";
                            }

                        });
                        $('.countycodediv select').html(countryOption);
                        $('.emergencycountry').html(countryOption);
                    }

                    if (checkBilling){
                        $.each(user_detail_data, function (key, value) {
                            $('#billing_' + key).val(value);
                            $('#billing_' + key).prop('readonly', true);
                        });
                    } else {
                        $.each(user_detail_data, function (key, value) {
                            $('#contact-info-tab #' + key).val(value);
                            if(key == 'gender'){
                                if(value == '2')
                                {
                                    $('#maiden_name').parents('.col-sm-3').show();
                                } else {
                                    $('#maiden_name').parents('.col-sm-3').hide();
                                }
                            }

                        });


                        if (owner_billing_data) {
                            if (owner_billing_data.billing_info_as_contact_info && owner_billing_data.billing_info_as_contact_info !== '') {
                                if (owner_billing_data.billing_info_as_contact_info == '1') {
                                    $.each(user_detail_data, function (key, value) {
                                        $('#billing_' + key).val(value);
                                        $('#billing_' + key).prop('readonly', true);
                                    });
                                    $('#billing_info_as_contact_info_id').prop('checked', true);
                                }
                            } else {
                                $.each(owner_billing_data, function (key, value) {

                                    $('#' + key).val(value);
                                    $('#billing_info_as_contact_info').prop('checked', false);
                                    $('#' + key).prop('readonly', false);
                                });
                            }
                        }


                        if (owner_detail_data.owner_image != '') {
                            $('#owner_image').html(owner_detail_data.owner_image);
                        }

                        console.log('emergency__data>>', owner_detail_data.emergency_contact_name);
                        if(owner_detail_data && owner_detail_data.emergency_contact_name && owner_detail_data.emergency_contact_name.length>=1)
                        {
                            selectEmergencyRow(owner_detail_data.emergency_contact_name,owner_detail_data.edit_emergency_code,owner_detail_data.emergency_email,owner_detail_data.emergency_phone_number,owner_detail_data.emergency_relation_new,owner_detail_data.emergency_other_relation);
                        }

                        if(owner_detail_data && owner_detail_data.contact_carrier_data && owner_detail_data.contact_carrier_data.length>=1)
                        {
                            selectPhoneRow(owner_detail_data.contact_carrier_data,owner_detail_data.carrier);
                        }

                        if(owner_detail_data && owner_detail_data.email && owner_detail_data.email.length>=1)
                        {
                            selectEmailRow(owner_detail_data.email);
                        }

                        $('.select_options').prop('selectedIndex', 0);

                    }
                } else {
                    toastr.warning('Site not redirecting due to technical issue.');
                }
            }
        });
    }

    function selectEmergencyRow(emergency_contact_name,emergency_country_code,emergency_email,emergency_phone_number,emergency_relation, emergency_other_relation) {

        $.each(emergency_contact_name, function (key, value) {
            if (key != 0) {
                var clone = $("#owner-emergency-contact_div_id:first").clone().insertAfter("div.owner-emergency-contact:last");
                clone.find('input[type=text]').val('');
                clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});
                clone.find(".add-emergency-contant").hide();
                clone.find(".remove-emergency-contant").show();
            }
        });


        $('.owner-emergency-contact').each(function (key,value) {
            $(this).find('input[name="emergency_contact_name[]"]').addClass('emergency_contact_name'+key);
            $(this).find('select[name="emergency_relation[]"]').addClass('emergency_relation'+key);
            $(this).find('select[name="emergency_country[]"]').addClass('emergency_country'+key);
            $(this).find('input[name="emergency_phone[]"]').addClass('emergency_phone'+key);
            $(this).find('input[name="emergency_email[]"]').addClass('emergency_email'+key);
        });

        setTimeout(function () {
            $('.owner-emergency-contact').each(function (key,value) {
                var keyD = parseInt(key);

                $('.emergency_contact_name'+keyD).val((emergency_contact_name)?emergency_contact_name[keyD]:'');
                $('.emergency_relation'+keyD).val((emergency_relation)?emergency_relation[keyD]:'');
                // if(emergency_relation[keyD] == 'Other'){
                //     $('#otherRelationDiv').show();
                //     $('.emergency_relation'+keyD).val((emergency_other_relation)?emergency_other_relation[keyD]:'');
                // } else {
                //     $('#otherRelationDiv').hide();
                //     $('#emergency_other_relation').val('');
                // }

                $('.emergency_phone'+keyD).val((emergency_phone_number)?emergency_phone_number[keyD]:'');
                $('.emergency_email'+keyD).val((emergency_email)? emergency_email[keyD]:'');

                if(emergency_relation[keyD] == '9')
                {
                    $('.emergency_relation'+keyD).parents('.owner-emergency-contact').find('#otherRelationDiv').show();
                    $('.emergency_relation'+keyD).parents('.owner-emergency-contact').find('#emergency_other_relation').val(emergency_other_relation[keyD]);
                    $('.emergency_relation'+keyD).parents('.owner-emergency-contact').find('input').attr('disabled',false);
                }

            },500);


            setTimeout(function () {
                $('.emergencycountry option').each(function(key,value){
                    var country = $(this).attr('data-id');
                    $(this).val(country);
                });

                $.each(emergency_country_code, function (key, value) {
                    var keyD = parseInt(key);
                    $('.emergency_country' + keyD).val((value) ? value : '');
                });
            },600);
        })
    }

    function selectPhoneRow(contact_carrier_data,contact_phone_type) {
        var n = contact_carrier_data.length;
        for(i=0; i<n; i++){
            if(i!=0)
            {
                var clone = $("#primary-owner-phone-row-divId").clone().insertAfter("div.primary-owner-phone-row:last");
                clone.find('input[type=text]').val('');
                $(".primary-owner-phone-row:not(:eq(0))  .fa-plus-circle").hide();
                $(".primary-owner-phone-row:not(:eq(0))  .fa-times-circle").show();

                var phoneRowLenght = $(".primary-owner-phone-row");
                clone.find('.phone_format').mask('000-000-0000', {reverse: true});
                if (phoneRowLenght.length == 2) {
                    clone.find(".fa-plus-circle").hide();
                }else if (phoneRowLenght.length == 3) {
                    clone.find(".fa-plus-circle").hide();
                    $(".primary-owner-phone-row:eq(0) .fa-plus-circle").hide();
                }else{
                    $(".primary-owner-phone-row:not(:eq(0)) .fa-plus-circle").show();
                }
            }

        }

        $('.primary-owner-phone-row').each(function (key,value) {

            $(this).find('select[name="carrier[]"]').addClass('carrier_'+key);
            $(this).find('input[name="work_phone_extension[]"]').addClass('work_phone_extension_'+key);
            $(this).find('select[name="country_code[]"]').addClass('country_code_'+key);
            $(this).find('select[name="phone_type[]"]').addClass('phone_type_'+key);
            $(this).find('input[name="phone_number[]"]').addClass('phone_number_'+key);
        });

        setTimeout(function () {
            $('.primary-owner-phone-row').each(function (key,value) {
                var keyD = parseInt(key);
                if(contact_carrier_data[keyD]['phone_type'] == '5' || contact_carrier_data[keyD]['phone_type'] == '2')
                {
                    $('.phone_type_'+keyD).parents('.primary-owner-phone-row').find('.work_extension_div').show();
                    $('.phone_type_'+keyD).parents('.primary-owner-phone-row').find('.work_phone_extension_'+keyD).val(contact_carrier_data[keyD]['work_phone_extension']);
                    $('.phone_type_'+keyD).parents('.primary-owner-phone-row').find('input').attr('disabled',false);
                }

                $('.carrier_'+keyD).val((contact_carrier_data)?contact_carrier_data[keyD]['carrier']:'');
                $('.country_code_'+keyD).val((contact_carrier_data)?contact_carrier_data[keyD]['country_code']:'');
                $('.phone_type_'+keyD).val((contact_carrier_data)?contact_carrier_data[keyD]['phone_type']:'');
                $('.phone_number_'+keyD).val((contact_carrier_data)?contact_carrier_data[keyD]['phone_number']:'');


            });
        },500)
    }

    function selectEmailRow(OwnerDataDetail) {
        $.each(OwnerDataDetail, function (key, value) {
            if(key!=0)
            {
                var clone = $("#multiple_email_div_id").clone().insertAfter("div.multipleEmail:last");
                clone.find('input[type=text]').val(value);
                $(".multipleEmail:not(:eq(0))  .email-plus-sign .fa-plus-circle").remove();
                $(".multipleEmail:not(:eq(0))  .email-remove-sign .fa-times-circle").show();
                if(key == 2){
                    $('.email-plus-sign').hide();
                }
                console.log('key', key);
            } else {
                $('#multiple_email_div_id input').val(value);
            }
        });
    }

    $(document).on('change','#billing_info_as_contact_info_id',function(){
        if ($(this).is(':checked')) {
            getPortalInitialData(true);
        } else {
            var inputs = $('#owner_billing_info_form_id :input');
            inputs.each(function() {
                $(this).prop('readonly', false);
                $(this).val('');
            });
        }
    });

    $(document).on('submit','#owner_billing_info_form_id',function (e) {
        e.preventDefault();
        var form_data = $(this).serializeArray();
        updateBillingInfo(form_data);
        return false;
    });
    function updateBillingInfo(form_data) {
        $.ajax({
            type: 'post',
            url: '/OwnerPortal-Ajax',
            data: {
                class  : 'viewAccountInfoAjax',
                action : 'updateOwnerBillingInfo',
                form   : form_data,
                id     : owner_id
            },

            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                    window.location.href = base_url + '/Owner/MyAccount/AccountInfo';
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    $.validator.addMethod("password_regex", function(value, element) {
        var password_regex = this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9])/.test(value);
        return password_regex;
    }, "* Atleast one alphanumeric and a numeric character required");


    $( "#changePasswordFormId" ).validate({
        rules: {
            password:{
                required:true,
                password_regex : true
            },
            new_password:{
                required:true,
                minlength:8,
                password_regex : true
            },
            confirm_new_password:{
                required:true,
                minlength:8,
                password_regex : true,
                equalTo :'#new_password'
            }
        },
        messages: {
            new_password: {
                minlength: "* Minimum 8 characters allowed",
            },
            confirm_new_password: {
                equalTo: "* Fields do not match",
                minlength: "* Minimum 8 characters allowed",
            },

        }
    });

    $('#changePasswordFormId').on('submit',function (e) {
        e.preventDefault();
        var current_password = $("#password").val();
        var new_password = $("#new_password").val();
        var confirm_new_password = $("#confirm_new_password").val();
        if ($("#changePasswordFormId").valid()) {
            changeOwnerPassword(current_password, new_password, confirm_new_password);
        }
        return false;
    });

    function changeOwnerPassword(current_password, new_password, confirm_new_password) {
        $.ajax({
            type: 'post',
            url: '/OwnerPortal-Ajax',
            data: {
                class       : 'viewAccountInfoAjax',
                action      : 'changePassword',
                id          : owner_id,
                currentPass : current_password,
                nPassword   : new_password,
                cPassword   : confirm_new_password,
            },

            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("Password changed successfully.");
                    $("#password").val('');
                    $("#new_password").val('');
                    $("#confirm_new_password").val('');
                } else if (response.status == 'error' && response.code == 400) {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    $(document).on('click','#cancel_update_billing_info_btn',function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href =  base_url+'/Owner/MyAccount/AccountInfo';
                }
            }
        });
    });


    $(document).on('click','#cancel_change_password',function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href =  base_url+'/Owner/MyAccount/AccountInfo';
                }
            }
        });
    });

    $(document).on('click','#cancel_contact_info',function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href =  base_url+'/Owner/MyAccount/AccountInfo';
                }
            }
        });
    });

    /*for multiple email textbox*/
    $(document).on("click",".email-plus-sign",function(){
        var emailDivLength = $('.multipleEmail').length;
        if (emailDivLength <= 2) {
            var clone = $("#multiple_email_div_id").clone().insertAfter("div.multipleEmail:last");
            clone.find('input[type=text]').val('');
            clone.find('input[type=text]').next().text('');
            $(".multipleEmail:not(:eq(0))  .email-plus-sign .fa-plus-circle").remove();
            $(".multipleEmail:not(:eq(0))  .email-remove-sign .fa-times-circle").show();
            if (emailDivLength == 2) {
                $('.email-plus-sign').hide();
            }
        } else {
            $('.email-plus-sign').hide();
        }

    });
    /*for multiple email textbox*/

    /*Remove Email Clone Textbox*/
    $(document).on("click",".email-remove-sign",function(){
        $(this).parents(".multipleEmail").remove();
        $(".email-plus-sign").show();
    });
    /*Remove Email Clone Textbox*/



    $(document).on("click",".add-emergency-contant",function(){

        var clone = $("#owner-emergency-contact_div_id:first").clone().insertAfter("div.owner-emergency-contact:last");
        clone.find('input[type=text]').val('');
        clone.find('input[name="emergency_phone[]"]').mask('000-000-0000', {reverse: true});
        clone.find(".add-emergency-contant").hide();
        clone.find("#otherRelationDiv").hide();
        clone.find(".remove-emergency-contant").show();
    });
    $(document).on("click",".remove-emergency-contant",function(){
        $(this).parents(".owner-emergency-contact").remove();
    });



    $("#updateContactInfo").validate({
        ignore: [':hidden'],
        rules: {
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            'carrier[]': {
                required: true,
            },
            'phone_number[]': {
                required: true,
            },
            'email[]': {
                required: true,
                email: true,
            },
            'emergency_email[]': {
                email: true,
            },
            'work_phone_extension[]': {
                required: true
            }
        }

    });


    /*** Submit Update owner form***/
    $("#updateContactInfo").on("submit",function(e) {
        e.preventDefault();

        var validator = $("#updateContactInfo").validate();

        if ($("#updateContactInfo").valid()) {
            updateOwnerDetails(owner_id);
        }

    });

    function updateOwnerDetails(owner_id) {

        var form = $('#updateContactInfo')[0];

        var formData = new FormData(form);
        formData.append('action','update');
        formData.append('class','editAccountInfoAjax');
        formData.append('edit_id',owner_id);
        $.ajax({
            url:'/OwnerPortalEdit-Ajax',
            type: 'POST',
            data: formData,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == "success" && response.owner_id != ""){
                    localStorage.setItem("Message", 'Record updated successfully.');
                    localStorage.setItem('rowcolor', 'rowColor');
                    window.location.href = '/Owner/MyAccount/AccountInfo';
                } else if (response.OwnerUserDetails.code == 500 && response.OwnerUserDetails.status == 'error'){
                    toastr.warning(response.OwnerUserDetails.message);
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    }


    $(document).on("click",".primary-owner-phone-row .fa-plus-circle",function(){
        var clone = $("#primary-owner-phone-row-divId").clone().insertAfter("div.primary-owner-phone-row:last");
        clone.find('.work_extension_div').hide();
        clone.find('input[type=text]').val('');
        clone.find('select').next().text('');
        clone.find('input[type=text]').next().text('');
        clone.find('select').val('');
        clone.find('.countycodediv').find('select').val(220);
        // var c = clone.find('.countycodediv').find('#country_code').val(220);

        // console.log('fsdfsd>>>', c);
        $(".primary-owner-phone-row:not(:eq(0))  .fa-plus-circle").hide();
        $(".primary-owner-phone-row:not(:eq(0))  .fa-times-circle").show();

        var phoneRowLenght = $(".primary-owner-phone-row");
        clone.find('.phone_format').mask('000-000-0000', {reverse: true});
        if (phoneRowLenght.length == 2) {
            clone.find(".fa-plus-circle").hide();
        }else if (phoneRowLenght.length == 3) {
            clone.find(".fa-plus-circle").hide();
            $(".primary-owner-phone-row:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-owner-phone-row:not(:eq(0)) .fa-plus-circle").show();
        }
    });

    $(document).on('change','#phone_type',function(){
        var phone_type = $(this).val();

        if(phone_type == '2' || phone_type == '5')
        {
            $('#work_phone_extension-error').text('');
            $(this).closest('div').next().show();
            $(this).closest('div').next().find('#work_phone_extension').removeAttr("disabled");
        } else {
            $(this).closest('div').next().hide();
            $(this).closest('div').next().find('#work_phone_extension').prop('disabled', true);
        }
    });

    $(document).on("click",".primary-owner-phone-row .fa-times-circle",function(){

        var phoneRowLenght = $(".primary-owner-phone-row");
        if (phoneRowLenght.length >= 2) {
            $(".primary-owner-phone-row:eq(0) .fa-plus-circle").show();
        }else if (phoneRowLenght.length == 3) {
            $(".primary-owner-phone-row:eq(0) .fa-plus-circle").hide();
        }else{
            $(".primary-owner-phone-row:not(:eq(0)) .fa-plus-circle").show();
        }

        $(this).parents(".primary-owner-phone-row").remove();
    });

    $('#updateContactInfo #zipcode').focusout(function () {
        getZipCode('#updateContactInfo #zipcode', $(this).val(),'#updateContactInfo #city','#updateContactInfo #state','#updateContactInfo #country','');
    });


    $(document).on('click','#clearChangePasswordForm',function() {
        bootbox.confirm("Do you want to Clear this form?", function (result) {
            if (result == true) {
                resetFormClear('#changePasswordFormId', [], 'form', false);
            }
        });
    });

    $(document).on('click','#clearBillingInfoForm',function() {
        bootbox.confirm("Do you want to Clear this form?", function (result) {
            if (result == true) {
                $("#owner_billing_info_form_id :input").prop("readonly", false);
                $("#billing_info_as_contact_info_id").prop("checked", false);
                resetFormClear('#owner_billing_info_form_id', [], 'form', false);
            }
        });
    });



    /** Conversation div js  starts*/
    $(document).on("click","#add_owner_conversation_btn",function(){
        $('.add_owner_conversation_div').find('textarea').val('');
        var validator = $("#add_owner_conversation_form_id").validate();
        validator.resetForm();
        fetchAllProblemCategory();
        getOwnersPropertyManager(owner_id, 'owner_tab');
        $('.add_owner_conversation_div').show();
    });

    $(document).on("click","#cancel_add_owner_conversation",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $('.add_owner_conversation_div').hide();
            }
        });
    });

    getOwnersPropertyManager(owner_id, 'owner_tab');
    function getOwnersPropertyManager(owner_id, owner_tab) {
        $.ajax({
            type: 'POST',
            url: '/Conversation-Ajax',
            data: {
                class: "conversationAjax",
                action: "getUserPropertyManagerList",
                id:owner_id,
                active_tab:owner_tab
            },
            success: function (response) {
                var response = JSON.parse(response);
                $('.select_mangers_option').html(response.data);

            }
        });
    }

    fetchAllConversations('', owner_id);
    function fetchAllConversations(selected_manager,selected_user){
        var active_tab = 'owner_tab';
        $.ajax({
            type: 'post',
            url: '/Conversation-Ajax',
            data: {
                class: 'conversationAjax',
                action: 'getAllConversationByType',
                active_tab : active_tab,
                selected_manager : selected_manager,
                selected_user : selected_user,
                is_portal: 'Owner_Portal'
            },
            success: function (response) {
                var res = JSON.parse(response);
                var html = '';
                $.each(res.data, function (key, val) {
                    var user_type = '';
                    if (val.comment_by == 'yes'){
                        user_type = val.user_type;
                    } else {
                        if (val.user_type == 1) {
                            user_type = 'Tenant'
                        } else if (val.user_type == 2) {
                            user_type = 'Owner'
                        } else {
                            user_type = 'Vendor'
                        }
                    }
                    var user_image = '<img src='+default_Image+'>';
                    if(val.selected_user_image){
                        if (val.selected_user_image.owner_image && val.selected_user_image.owner_image !='undefined') {
                            user_image = val.selected_user_image.owner_image;
                        }
                        if (val.selected_user_image.tenant_image && val.selected_user_image.tenant_image !='undefined') {
                            user_image = val.selected_user_image.tenant_image;
                        }
                        if (val.selected_user_image.image && val.selected_user_image.image !='undefined') {
                            user_image = val.selected_user_image.image;
                        }
                    }

                    html += '<li >\n' +
                        '<div class="row">\n' +
                        '<div class="col-sm-1 conversation-lt">\n' +
                        '<div class="img-outer">\n' +user_image+
                        '</div>\n' +
                        '</div>\n' +
                        '<div class="col-sm-10 pad-none conversation-rt">\n' +
                        '<span class="dark_name">'+val.selected_user_name+'('+user_type+') :</span>\n' +
                        '<span class="light_name">'+val.login_user_name+'('+val.login_user_role+')</span>\n' +
                        '<span class="dark_name">'+val.problem_category_name+'</span>\n' +
                        '<span class="black_text" >'+val.description+'</span>\n' +
                        '<div class="conversation-time clear">\n' +
                        '<span class="black_text" >'+val.created_date+'-</span>\n' +
                        '<a class="light_name comment_on_conversation" conversation_id="'+val.id+'">Comment</a>\n' +
                        '</div>\n' +
                        '<div class="viewConversationCommentDiv">\n' +
                        '</div>'+
                        '<div class="textConversationCommentDiv">\n' +
                        '</div>'+
                        '</div>\n' +
                        '<div class="col-sm-1 pull-right">\n' +
                        '<span class="viewAllComment" conversation_id="'+val.id+'" ><i class="fa fa-plus-circle" aria-hidden="true"></i></span> \n' +
                        '</div>\n' +
                        '</div>\n' +
                        '</li>';
                });
                $('.conversation_list').html(html);
            },
        });
    }

    $(document).on("click",".viewAllComment",function () {
        var active_tab = 'owner_tab';
        var conversation_id = $(this).attr('conversation_id');
        var viewCommentHtml = $(this).parent().prev().find('.viewConversationCommentDiv');
        var viewCommentFindActive = $(this).parent().prev().find('.viewConversationCommentDiv').hasClass('active');
        if(viewCommentFindActive) {
            viewCommentHtml.html('');
            viewCommentHtml.removeClass('active');
        } else {
            $.ajax({
                type: 'post',
                url: '/Conversation-Ajax',
                data: {
                    class: 'conversationAjax',
                    action: 'getAllConversationByType',
                    conversation_id: conversation_id,
                    active_tab : active_tab
                },
                success: function (response) {
                    var response = JSON.parse(response);

                    if (response.status == 'success' && response.code == 200) {
                        var comment_html = '';
                        $.each(response.data, function (key, val) {
                            var user_image = '<img src='+default_Image+'>';
                            if(val.selected_user_image){
                                if (val.selected_user_image.owner_image && val.selected_user_image.owner_image !='undefined') {
                                    user_image = val.selected_user_image.owner_image;
                                }
                                if (val.selected_user_image.tenant_image && val.selected_user_image.tenant_image !='undefined') {
                                    user_image = val.selected_user_image.tenant_image;
                                }
                                if (val.selected_user_image.image && val.selected_user_image.image !='undefined') {
                                    user_image = val.selected_user_image.image;
                                }
                            }

                            comment_html += '<div style="" id="ConversationOwnerComment_0">\n' +
                                '<div class="conversation-comment-grey removeClass">\n' +
                                '<div class="conversation-arrow"></div>\n' +
                                '<div class="col-sm-1 conversation-lt">\n' +
                                '<div class="img-outer">\n' +user_image+
                                '</div>\n' +
                                '</div>\n' +
                                '<div class="col-sm-10 conversation-rt">\n' +
                                '<span class="dark_name">' + val.login_user_name + '(' + val.login_user_role + ') :</span>\n' +
                                '<span class="black_text" >' + val.description + '</span>\n' +
                                '<div class="conversation-comment-time">' + val.created_date + '</div>\n' +
                                '</div>\n' +
                                '</div>\n' +
                                '</div>'
                        });
                        viewCommentHtml.append(comment_html);
                        viewCommentHtml.addClass('active');
                    } else if (response.status == 'error' && response.code == 502) {
                        toastr.error(response.message);
                    } else if (response.status == 'error' && response.code == 400) {
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.' + key).html(value);
                        });
                    }
                }
            });
        }
    });


    $(document).on("click",".comment_on_conversation",function () {
        var conversation_id = $(this).attr('conversation_id');
        $('.textConversationCommentDiv').html('');
        var commentHtml = '<form id="sendConversationCommentForm">\n' +
            '<input type="hidden" name="conversation_id" value="'+conversation_id+'">\n' +
            '<textarea class="form-control capital" name="comment_text" rows="6"></textarea>\n' +
            '<div class="btn-outer top-marginCls">\n' +
            '<button class="blue-btn sendConversationCommentBtn">Send</button>\n' +
            '<button  type="button" class="grey-btn cancelConversationCommentDiv">Cancel</button>\n' +
            '</div>\n' +
            '</form>\n';
        $(this).parent().next().next().html(commentHtml);
    });
    $(document).on("click",".cancelConversationCommentDiv",function () {
        console.log('cancel>>>',  $(this).parent());
        $(this).parents('.textConversationCommentDiv').html('');
    });

    $(document).on('submit','#sendConversationCommentForm',function(e) {
        e.preventDefault();
        var active_tab = 'owner_tab';
        var form = $('#sendConversationCommentForm')[0];
        var formData = new FormData(form);

        formData.append('class', 'conversationAjax');
        formData.append('action', 'sendCommentOnConversation');
        formData.append('active_tab', active_tab);
        formData.append('is_portal', 'Owner_Portal');

        $.ajax({
            url: '/Conversation-Ajax',
            type: 'post',
            data:  formData,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){
                    fetchAllConversations('',owner_id);
                    toastr.success("Record created successfully.");
                }else if(response.status == 'error' && response.code == 502){
                    toastr.error(response.message);
                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }
            }
        });
    });

    $("#add_owner_conversation_form_id").validate({
        rules: {
            selected_user_id: {
                required: true
            },
            problem_category: {
                required: true
            },
            description: {
                required: true
            }
        },
        submitHandler: function (form) {
            event.preventDefault();

            var form = $('#add_owner_conversation_form_id')[0];
            var formData = new FormData(form);


            formData.append('class', 'conversationAjax');
            formData.append('action', 'addConversation');
            formData.append('active_tab', 'owner_tab');
            formData.append('is_portal', 'Owner_Portal');
            console.log('formData>>', formData);

            $.ajax({
                url: '/Conversation-Ajax',
                type: 'post',
                data:  formData,
                contentType: false,
                processData: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        $('.add_owner_conversation_div').hide();
                        fetchAllConversations('',owner_id);
                        toastr.success("Record created successfully.");
                    }else if(response.status == 'error' && response.code == 502){
                        toastr.error(response.message);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                }
            });
        }
    });

    function fetchAllProblemCategory(){
        $.ajax({
            type: 'post',
            url: '/Conversation-Ajax',
            data: {
                class: 'conversationAjax',
                action: 'getAllProblemCategory',
            },
            success: function (response) {
                var res = JSON.parse(response);
                $('.problem_category').html(res.data);
            },
        });
    }

    /** Conversation div js ends **/

    /**** Save problem category popup data starts ****/

    $(document).on("click",".pop-add-icon",function () {
        $(".add-popup-body input[type='text']").val('');
        var text_id = $(this).closest('div').find('.add-popup').attr('id');
        $("#"+text_id+" .required").text('');
        $(this).closest('div').find('.add-popup').show();
    });

    $(document).on("click",".add-popup .grey-btn",function(){
        var box = $(this).closest('.add-popup');
        $(box).hide();
    });

    $(document).on("click",".add_single1", function () {
        var tableName = $(this).attr("data-table");
        var colName = $(this).attr("data-cell");
        var className = $(this).attr("data-class");
        var selectName = $(this).attr("data-name");
        var val = $(this).parent().prev().find("."+className).val();

        if (val == ""){
            $(this).parent().prev().find("."+className).next().text("* This field is required");
            return false;
        }else{
            $(this).parent().prev().find("."+className).next().text("");
        }

        savePopUpData(tableName, colName, val, selectName);
        var val = $(this).parent().prev().find("input[type='text']").val('');
    });

    function  savePopUpData(tableName, colName, val, selectName) {
        $.ajax({
            type: 'post',
            url: '/Tenantlisting/savePopUpData',
            data: {
                class: "TenantAjax",
                action: "savePopUpData",
                tableName:tableName,
                colName: colName,
                val:val
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if (data.status == "success") {
                    var option = "<option value='"+data.data.last_insert_id+"' selected>"+data.data.col_val+"</option>";
                    $("select[name='"+selectName+"']").append(option);
                    $("select[name='"+selectName+"']").next().hide();
                    toastr.success(data.message);
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on("click",".cancel",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                location.reload();
            }
        });

    });

    $(document).on('click','#clearAddConversationForm',function() {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $('.add_owner_conversation_div').find('textarea').val('');
                var validator = $("#add_owner_conversation_form_id").validate();
                validator.resetForm();
                fetchAllProblemCategory();
                getOwnersPropertyManager(owner_id, 'owner_tab');
            }
        });
    });
    $(document).on("click","#clearAddProblemCategoryForm",function(){
        resetFormClear('#selectProblemCategoryPopup',[], 'div',false);
        $(this).parents('.add-popup span.required').html('');
    });
    /**** Save problem category popup data ends here****/

    $(document).on("click","#clearEditContactForm",function(){
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                location.reload();
            }
        });
    });



});
