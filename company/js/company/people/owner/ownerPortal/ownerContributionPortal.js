$(document).ready(function () {

    $('#start_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: datepicker_format});
    $('#end_date').datepicker({yearRange: '-100:+100', changeMonth: true, changeYear: true, dateFormat: datepicker_format});

    $(document).on('change','.common_search_class',function(){
        var grid = $("#owner_contribution_table"),f = [];
        var start_date    = $('#start_date').val() == ''?'all':$('#start_date').val();
        var end_date      = $('#end_date').val() == ''?'all':$('#end_date').val();
        f.push(
            {field: "owner_contribution.created_at", op: "dateBetween", data: start_date, data2:end_date}
        );
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f)});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    });
    listOwnerContributionJqGrid(owner_id);
    function listOwnerContributionJqGrid(selected_owner_id, deleted_at) {
        var sortOrder = 'desc';
        var sortColumn = 'owner_contribution.created_at';
        var table = 'owner_contribution';
        var select_column = '';
        var columns = ['Owner Name','Property Name', 'transation_type', 'bank_account_number', 'created_at', 'Account No.', 'COA','Amount('+default_currency_symbol+')'];
        var joins = [
            {table: 'owner_contribution', column: 'owner_id', primary: 'id', on_table: 'users'},
            {table: 'owner_contribution', column: 'property_id', primary: 'id', on_table: 'general_property'},
            {table: 'owner_contribution', column: 'chart_of_account_id', primary: 'id', on_table: 'company_chart_of_accounts'},
            {table: 'owner_contribution', column: 'bank_account_id', primary: 'id', on_table: 'company_accounting_bank_account'},
        ];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = [];
        if (selected_owner_id != '') {
            var extra_where = [
                {column: 'user_type', value: '4', condition: '=', table: 'users'},
                { column: 'owner_id', value: selected_owner_id, condition: '=', table: 'owner_contribution'}
            ];
        } else {
            var extra_where = [{column: 'user_type', value: '4', condition: '=', table: 'users'}];
        }
        var columns_options = [
            {name: 'Owner Name', index: 'name', width: 60, align: "center", searchoptions: {sopt: conditions}, table: 'users', classes: 'cursor', formatter: ownerName },
            { name: 'Property Name', index: 'property_name', width: 60, align: "center", searchoptions: {sopt: conditions}, table: 'general_property', classes: 'pointer', formatter: propertyName},
            /*hidden fields*/
            {name: 'transation_type', index: 'transation_type', hidden: true, alias: 'property_id', width: 60, align: "center", searchoptions: {sopt: conditions}, table: table},
            {name: 'bank_account_number', index: 'bank_account_number', hidden: true, width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'company_accounting_bank_account'},
            {name: 'created_at', index: 'created_at', hidden: true, width: 130, align: "center", searchoptions: {sopt: conditions}, table: table},
            /*hidden fields*/
            {name: 'Account No.', index: 'bank_name', width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'company_accounting_bank_account', formatter: accountNameNumber},
            {name: 'COA', index: 'account_name', width: 130, align: "center", searchoptions: {sopt: conditions}, table: 'company_chart_of_accounts'},
            {name: 'Amount', index: 'amount', width: 130, align: "center", searchoptions: {sopt: conditions}, table: table,  formatter: amount}
        ];
        var ignore_array = [];

        jQuery("#owner_contribution_table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "owner_contribution",
                select: select_column,
                columns_options: columns_options,
                // status: '',
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: true,
                extra_where: extra_where
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: sortOrder,
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Owner Contribution",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 400, drag: true, resize: false} // search options
            // {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     *  Function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

    /**
     * Function to format owner name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ownerName(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
        }
    }

    function amount(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            return default_currency_symbol + cellValue;
        }
    }

    function accountNameNumber(cellValue, options, rowObject) {
        if (rowObject !== undefined) {
            var account_name = rowObject['Account No.'];
            var transation_type = rowObject['transation_type'];
            var bank_account_number = rowObject['bank_account_number'];
            if (transation_type == 'Check' || transation_type == 'Cash'){
                account_name = '';
            } else {
                account_name = account_name+' '+bank_account_number;
            }
            return  account_name;
        }
    }
});