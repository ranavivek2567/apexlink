function getParameterByName( name ){
    var regexS = "[\\?&]"+name+"=([^&#]*)",
        regex = new RegExp( regexS ),
        results = regex.exec( window.location.search );
    if( results == null ){
        return "";
    } else{
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}
var owner_edit_id =  getParameterByName('id');
if(localStorage.getItem("rowcolorTenant")){
    setTimeout(function(){
        jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
        jQuery('.table').find('tr:eq(1)').find('td:eq(8)').addClass("green_row_right");
        localStorage.removeItem('rowcolorTenant');
    }, 2000);
}

//display add portfolio div
$(document).on('change','.country_code_0 ',function () {
    $('#flag_country_code').val($('.country_code_0').val());
});
$(document).on('blur','.phone_number_0 ',function () {
    $("#flag_phone_number").val($('.phone_number_0').val());
});
$(document).on('click','#new_flag',function () {
    $('.new_flag_btn_div').hide();
    $('#flag_form_div').show(500);
    $('#flag_form_div .time_span').text('');
    $('#flag_id').val('');
    $('#flagSaveBtnId').text('Save');
    $("#flag_flag_by").val(default_login_user_name);
    $("#flag_flag_name,#flag_flag_reason,#flag_note").val('');
    $('#flag_country_code').val($('.country_code_0').val());
    $("#flag_flag_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());
    $('#completed').prop('selectedIndex',0);
    $("#flagged_for").val($('#first_name').val()+' '+$('#last_name').val());
});

//on flag cancel
$(document).on('click','#flagCancel',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#flag_id').val('');
                $('#flag_form_div').hide(500);
                $('.new_flag_btn_div').show();
            }
        }
    });
});

$(document).on("click","#clearAddFlagForm",function(){
    bootbox.confirm("Do you want to clear this flag form?", function (result) {
        if (result == true) {
            var editFlagId = $('#flag_id').val();
            if (editFlagId == '') {
                $('#flag_id').val('');
                $('#flagSaveBtnId').text('Save');
                $('#flag_form_div .time_span').text('');
                $("#flag_flag_by").val(default_login_user_name);
                $("#flag_flag_name,#flag_flag_reason,#flag_note").val('');
                $('#flag_country_code').val($('.country_code_0').val());
                $("#flag_flag_date").datepicker({dateFormat: datepicker}).datepicker("setDate", new Date());;
                $('#completed').prop('selectedIndex', 0);
                $("#flag_phone_number").val($('.phone_number_0').val());
            } else {
                getDataByIdFlag(editFlagId);
            }
        }
    });
});

//adding custom fields
$('#flagSaveBtnId').on('click',function(event) {
    event.preventDefault();
    //checking custom field validation
    var formData = $('#flag_form_div :input').serializeArray();
    console.log('formData>>', formData);
    $.ajax({
        type: 'post',
        url: '/flag-ajax',
        data: {form: formData,class:'Flag',action:'create_flag',object_id:owner_edit_id,object_type:'owner'},
        success: function (response) {
            var response = JSON.parse(response);
            if(response.code == 200){
                console.log('ADDD>>', response);
                getFlagCount();
                $('#flag_form_div').hide(500);
                $('.new_flag_btn_div').show();
                $('#ownerFlags-table').trigger('reloadGrid');
              //  toastr.success(response.message);

                var returnRes = update_users_flag('users',owner_edit_id);
                if (returnRes.status == "success" && returnRes.code == 200){
                    localStorage.setItem("Message", response.message);
                    localStorage.setItem('rowcolorTenant', 'rowColor');
                    setTimeout(function () {
                        window.location.href = window.location.origin + '/People/Ownerlisting';
                    },1000);
                }

                setTimeout(function(){
                    jQuery('#ownerFlags-table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                    jQuery('#ownerFlags-table').find('tr:eq(1)').find('td').last().addClass("green_row_right");
                }, 1000);
            } else if(response.code == 200) {
                toastr.error(response.message);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
});

ownerFlags('All');
function ownerFlags(status) {
    var table = 'flags';
    var columns = ['Date', 'Flag Name', 'Phone Number','Flag Reason','Completed','Note', 'Action'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_columns = [];
    var extra_where = [{column: 'object_id', value: owner_edit_id, condition: '='},{column: 'object_type', value: "owner", condition: '='}];
    var columns_options = [
        {name:'Date',index:'date', width:100,searchoptions: {sopt: conditions},table:table,change_type:'date'},
        {name:'Flag Name',index:'flag_name',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Phone',index:'flag_phone_number', width:100,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:phoneNumberFormat},
        {name:'Flag Reason',index:'flag_reason',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Completed',index:'completed', width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true,table:table,formatter:isCompletedFormatter},
        {name:'Note',index:'flag_note',width:100,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Action',index:'select',width:100, title: false,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter: actionFmatter}
    ];
    var ignore_array = [];
    jQuery("#ownerFlags-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Flags",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {top: 10, left: 200, drag: true, resize: false}
    );
}

/**
 * jqGrid function to format completed column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function phoneNumberFormat(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        if(cellValue !== undefined || cellValue != '' ){
            return cellValue.replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
        }
        else {
            return '';
        }
    }
}
/**
 * jqGrid function to format completed column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function isCompletedFormatter(cellValue, options, rowObject) {
    if (cellValue == '1')
        return "True";
    else if (cellValue == '0')
        return "False";
    else
        return '';
}

/**
 * function to change action column
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function actionFmatter (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject.Completed == 1) select = ['Edit','Delete'];
        if(rowObject.Completed == 0) select = ['Edit','Delete','Completed'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }

}

function getDataByIdFlag(id){
    $.ajax({
        type: 'post',
        url: '/flag-ajax',
        data: {id: id,class:'Flag',action:'get_flags'},
        success: function (response) {
            $("#flagged_for").val($('#first_name').val()+' '+$('#last_name').val());
            $("#flag_flag_date").datepicker({dateFormat: datepicker});
            $('.new_flag_btn_div').hide();
            $("#flag_form_div").show(500);
            $('#flagSaveBtnId').text('Update');
            var data = $.parseJSON(response);
            console.log('ADDD>>', data);
            if (data.code == 200) {
                edit_date_time(data.data.updated_at);
                $("#flag_id").val(data.data.id);
                $("#flag_id").val(data.data.id);
                $.each(data.data, function (key, value) {
                    $('#flag_' + key ).val(value);
                    if(key == 'completed'){
                        $('#completed').val(value);
                    }
                    if(key == 'flag_phone_number'){
                        $('#flag_phone_number').val(value);
                    }
                    if(key == 'flag_note'){
                        $('#flag_note').val(value);
                    }
                    if (key == 'date'){
                        $('#flag_flag_date').val(value);
                    }
                });
            } else if (data.code == 500){
                toastr.error(data.message);
            } else{
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}
/**  List Action Functions  */
$(document).on('change', '#ownerFlags-table .select_options', function() {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    var row_num = $(this).parent().parent().index() ;
    if (opt == 'Edit' || opt == 'EDIT') {
        // var validator = $( "#flagForm" ).validate();
        // validator.resetForm();
        $('#ownerFlags-table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        console.log('sdsdsds');
        jQuery('#ownerFlags-table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        jQuery('#ownerFlags-table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");
        getDataByIdFlag(id);
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'deleteFlag'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                getFlagCount();
                             //   toastr.success(response.message);
                                var returnRes = update_users_flag('users',owner_edit_id);
                                if (returnRes.status == "success" && returnRes.code == 200){
                                    localStorage.setItem("Message", response.message);
                                    localStorage.setItem('rowcolorTenant', 'rowColor');
                                    setTimeout(function () {
                                        window.location.href = window.location.origin + '/People/Ownerlisting';
                                    },1000);
                                }

                                $('#ownerFlags-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    } else if (opt == 'Completed' || opt == 'COMPLETED') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to complete this flag ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/flag-ajax',
                        data: {id: id,class:'Flag',action:'flagCompleted'},
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                getFlagCount();
                                console.log('getFlagCount();');
                             //   toastr.success(response.message);
                                var returnRes = update_users_flag('users',owner_edit_id);
                                if (returnRes.status == "success" && returnRes.code == 200){
                                    localStorage.setItem("Message", response.message);
                                    localStorage.setItem('rowcolorTenant', 'rowColor');
                                    setTimeout(function () {
                                        window.location.href = window.location.origin + '/People/Ownerlisting';
                                    },1000);
                                }

                                $('#ownerFlags-table').trigger('reloadGrid');
                            } else if(response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }

                        }
                    });
                }

            }
        });
    }
    $('.select_options').prop('selectedIndex',0);
});
