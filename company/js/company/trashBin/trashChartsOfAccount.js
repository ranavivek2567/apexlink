$(document).ready(function () {
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid(status, deleted_at) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'company_chart_of_accounts';
        var columns = ['Account Code', 'Account Name', 'Account Type','Reporting Code','Status', 'Action'];
        var select_column = ['Edit', 'Deactivate', 'Delete'];
        var joins = [
            {
                table: 'company_chart_of_accounts',
                column: 'sub_account',
                primary: 'id',
                on_table: 'company_account_sub_type'
            },
            {
                table: 'company_chart_of_accounts',
                column: 'account_type_id',
                primary: 'id',
                on_table: 'company_account_type'
            }
        ];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['company_chart_of_accounts.status', 'company_chart_of_accounts.deleted_at', 'company_chart_of_accounts.updated_at'];
        var columns_options = [
            {
                name: 'Account Code',
                index: 'account_code',
                width: 90,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table
            },
            {name: 'Account Name', index: 'account_name', width: 100, searchoptions: {sopt: conditions}, table: table},
            {
                name: 'Account Type',
                index: 'account_type_name',
                width: 100,
                searchoptions: {sopt: conditions},
                table: 'company_account_type'
            },
            {name:'Reporting Code',index:'reporting_code', width:100,searchoptions: {sopt: conditions},table:table,formatter:reportingFormatter},
            {
                name: 'Status',
                index: 'status',
                width: 80,
                align: "center",
                searchoptions: {sopt: conditions},
                table: table,
                formatter:statusFormatter
            },
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreChartOfAccount' title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table}
        ];
        var ignore_array = [];
        jQuery("#chart_id").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_chart_of_accounts",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns,
                deleted_at: false
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: 'desc',
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Chart of Accounts",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {},
            {},
            {},
            {top: 200, left: 200, drag: true, resize: false}
        );
    }
    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == '1')
            return "InActive";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }
    function reportingFormatter (cellValue, options, rowObject){
        if (cellValue == '1')
            return "C";
        else if(cellValue == '2')
            return "L";
        else if(cellValue == '3')
            return "M";
        else if(cellValue == '4')
            return "B";
    else
            return '';
    }
    function restoreChartOfAccount(id){
        $.ajax({
            type: 'post',
            url: '/AddChartOfAccount-Ajax',
            data: {
                class: "ChartOfAccountAjax",
                action: "restoreChartOfAccount",
                cuser_id: id

            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $("#chart_id").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    $(document).on("click", "#restoreChartOfAccount", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restoreChartOfAccount(id);
            } else {
                window.location.href =  window.location.origin+'/TrashBin/ChargeCode'
            }
        });
        /* var action = this.value;
         var id = $(this).attr('data_id');
     */
    });
});