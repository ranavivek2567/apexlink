$(document).ready(function () {
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        var protocol = location.port;
        if (protocol != "") {
            var webpath = location.protocol+"//"+location.hostname+":"+protocol;
        }else{
            var webpath = location.protocol+"//"+location.hostname;
        }
        var table = 'company_user_roles';
        var columns = ['Role Name', 'Status', 'Action'];
        var select_column = ['Edit','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var pagination;
        var columns_options = [

            {name:'Role Name',index:'role_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'', title: false, width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: function () {
                    return "<img src='"+webpath+"/company/images/icon6.png' id='restoreManageUserRole' title='Restore' alt='my image'/>";
                }, edittype: 'select',search:false,table:table}

        ];
        var ignore_array = [];
        jQuery("#manageuser_id").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_user_roles",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:false
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Manage User Role",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == '1')
            return "InActive";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }
    function restoreManageUserRole(id){
        $.ajax({
            type: 'post',
            url: '/UserRoles-Ajax',
            data: {
                class: "manageUserRoles",
                action: "restoreManageUserRole",
                cuser_id: id

            },
            success: function (response) {
                var response = JSON.parse(response);
                toastr.success(response.message);
                $("#manageuser_id").trigger('reloadGrid');
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    }

    $(document).on("click", "#restoreManageUserRole", function (e) {
        e.preventDefault();
        var id = $(this).parents("tr").attr('id');
        console.log(id);
        bootbox.confirm("Do you want to restore this record?", function (result) {
            if (result == true) {
                restoreManageUserRole(id);
            } else {
                window.location.href =  window.location.origin+'/TrashBin/ManageUser'
            }
        });
        /* var action = this.value;
         var id = $(this).attr('data_id');
     */
    });

});