$(document).ready(function(){

    var currentPagePath = window.location.pathname;
    //show if data already exists in default settings
    $.ajax({
        type: 'post',
        url: '/timesettings-ajax',
        data: {
            class: "TimeSettingsAjax",
            action: "timeviewSettings"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            // alert("hello");
            if (data.status == "success"){
                var result = data.data;
                $.each(result, function (key, value) {
                    $('#'+key).val(value);
                });
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
        }
    });

    //show if data already exists in time settings
    $.ajax({
        type: 'post',
        url: '/timesettings-ajax',
        data: {
            class: "TimeSettingsAjax",
            action: "viewTimeSettings"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success"){
                var result = data.data;
                var timezoneId = data.timezoneId;

                var html = '';
                html += '<option value="0">Select time zone</option>';
                $.each(result, function (key, value) {

                    var check= '';
                    if(value.id == timezoneId){
                        // console.log(value.id);
                        check ='selected';

                    }
                        html += '<option '+check+' value="'+value.id+'">'+value.code+' '+value.name+'</option>';
                        $(".gettimezone").html(html);

                });
            } else {
                toastr.error(data.message);
            }
        },
        error: function (data) {
        }
    });

    //Ajax to create a settings
    $("#settingsForm").submit(function( event ) {
        event.preventDefault();
        if ($('#settingsForm').valid()) {
            var formData = $('#settingsForm').serializeArray();
            $.ajax({
                type: 'post',
                url: '/timesettings-ajax',
                data: {class: 'TimeSettingsAjax', action: "addtimeSettings", form: formData},
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success'){
                        //toastr.success(response.message);
                        localStorage.setItem("Message", 'Record updated successfully.');
                        location.reload();
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });
    //Ajax to create a time settings
    $("#timesettingsForm").submit(function( event ) {
        event.preventDefault();
        // alert("hello");
        if ($('#timesettingsForm').valid()) {
            // alert("hello");
            var formData = $('#timesettingsForm').serializeArray();
            //console.log(formData);
            $.ajax({
                type: 'post',
                url: '/timesettings-ajax',
                data: {class: 'TimeSettingsAjax', action: "addTimeSettings", form: formData},
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success'){
                       // toastr.success(response.message);
                        localStorage.setItem("Message", 'Record updated successfully.');
                        location.reload();
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }


    });


    /* Phone number format setting*/
    jQuery('input[name="phone_number"]').mask('000-000-0000', {reverse: true});


    /* cancel button event */
    $(document).on("click",".yes-cancel-time",function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href = '/User/AccountSetup';
                }
            }
        });
    });


});



