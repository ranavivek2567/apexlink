$(document).ready(function(){
    var currentPagePath = window.location.pathname;
    //show if data already exists in default settings
    $.ajax({
        type: 'post',
        url: '/settings-ajax',
        data: {
            class: "DefaultSettingsAjax",
            action: "viewSettings"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success"){
                var result = data.data;
                $.each(result, function (key, value) {
                    $('#'+key).val(value);
                });
            }
        },
        error: function (data) {
        }
    });

    //show if data already exists in clock swttings
    $.ajax({
        type: 'post',
        url: '/settings-ajax',
        data: {
            class: "DefaultSettingsAjax",
            action: "viewClockSettings"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success"){
                var result = data.data;
                $.each(result, function (key, value) {

                    if (key == 'default_date_format') {
                        var html = '';
                        if (value == 1) {
                            html += '<option value="1">MM/DD/YYYY (Day)</option>';
                            html += '<option value="2">MM-DD-YYYY (Day)</option>';
                            html += '<option value="3">MM.DD.YYYY (Day)</option>';
                            html += '<option value="13">MONTH DD, YYYY (Day)</option>';
                        }else if(value == 2){
                            html += '<option value="4">DD/MM/YYYY (Day)</option>';
                            html += '<option value="5">DD-MM-YYYY (Day)</option>';
                            html += '<option value="6">DD.MM.YYYY (Day)</option>';
                            html += '<option value="14">DD MONTH, YYYY (Day)</option>';
                        }else if(value == 3){
                            html += '<option value="7">YYYY/MM/DD (Day)</option>';
                            html += '<option value="8">YYYY-MM-DD (Day)</option>';
                            html += '<option value="9">YYYY.MM.DD (Day)</option>';
                        }else{
                            html += '<option value="10">YYYY/DD/MM (Day)</option>';
                            html += '<option value="11">YYYY-DD-MM (Day)</option>';
                            html += '<option value="12">YYYY.DD.MM (Day)</option>';
                        }
                        $("#date_format").html(html);
                    }
                    if ( key == 'date_format' ) {
                        $('#date_format [value="'+value+'"]').attr('selected', 'true');
                    }
                    if (key == 'default_clock_format') {
                        if(value == "12"){
                            $(".time-format").text("12 Hour Clock 08:10:21");
                        }else{
                            $(".time-format").text("24 Hour Clock 17:45:55");
                        }
                    }

                    $('#'+key).val(value);
                });
                $('#c_id').val(result.id);
            }
        },
        error: function (data) {
        }
    });

    //get country code
    /*$.ajax({
        type:'post',
        url:'/helper-ajax',
        data:{class:'HelperAjax',action:"getCountryCode"},
        success:function(response){
            var data = JSON.parse(response);
            var html = '';
            $.each(data.data, function (key, value) {
                html += '<option value='+value.id+'>'+value.name+'('+value.code+')</option>';
            });
            $('#countryCode').append(html);
        },
        error:function(data){

        }
    });*/

    var getLocalStorageVaue = localStorage.getItem('superadmin_admin_nav');
    toggleSidebarMenu(getLocalStorageVaue);
    localStorage.setItem('superadmin_admin_nav',0);
    $(document).on("click","#leftnav2 .sub-item",function(){
        localStorage.setItem('superadmin_admin_nav',1);
    });



    //Ajax to create a settings
    $("#settingsForm").submit(function( event ) {
        event.preventDefault();
        if ($('#settingsForm').valid()) {
            var formData = $('#settingsForm').serializeArray();
            $.ajax({
                type: 'post',
                url: '/settings-ajax',
                data: {class: 'DefaultSettingsAjax', action: "addSettings", form: formData},
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success'){
                        toastr.success(response.message);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {

                }
            });
        }
    });
    //Ajax to create a clock settings
    $("#clocksettingsForm").submit(function( event ) {
        event.preventDefault();
        if ($('#clocksettingsForm').valid()) {
            var formData = $('#clocksettingsForm').serializeArray();
            $.ajax({
                type: 'post',
                url: '/settings-ajax',
                data: {
                    class: 'DefaultSettingsAjax',
                    action: "addClockSettings",
                    form: formData
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success'){
                        toastr.success(response.message);
                        $('#formated_date').text(response.data.formatted_date);

                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {

                }
            });
        }


    });




    //default clock settings
    /* change time format on clock toggle */
    jQuery(document).on("change",".dclock_format",function(e) {
        e.preventDefault();
        var clockVal = jQuery(this).val();
        if(clockVal == "12"){
            jQuery(".time-format").val("12 Hour Clock 08:10:21");
        }else{
            jQuery(".time-format").val("24 Hour Clock 17:45:55");
        }
    });

    /* Phone number format setting*/
    jQuery('input[name="phone_number"]').mask('000-000-0000', {reverse: true});

    /* Added default date and clock variation based on default date format */
    $('#date_format option').each(function() {
        if ($.inArray( parseInt($(this).attr('value')),[1,2,3,13] ) > -1) {
            $(this).show();
        }else{
            $(this).hide();
        }

    });
    $(document).on("change","#default_date_format", function(){
        var html = '';
        if ($(this).val() == 1) {
            html += '<option value="1">MM/DD/YYYY (Day)</option>';
            html += '<option value="2">MM-DD-YYYY (Day)</option>';
            html += '<option value="3">MM.DD.YYYY (Day)</option>';
            html += '<option value="13">MONTH DD, YYYY (Day)</option>';
        }else if($(this).val() == 2){
            html += '<option value="4">DD/MM/YYYY (Day)</option>';
            html += '<option value="5">DD-MM-YYYY (Day)</option>';
            html += '<option value="6">DD.MM.YYYY (Day)</option>';
            html += '<option value="14">DD MONTH, YYYY (Day)</option>';
        }else if($(this).val() == 3){
            html += '<option value="7">YYYY/MM/DD (Day)</option>';
            html += '<option value="8">YYYY-MM-DD (Day)</option>';
            html += '<option value="9">YYYY.MM.DD (Day)</option>';
        }else{
            html += '<option value="10">YYYY/DD/MM (Day)</option>';
            html += '<option value="11">YYYY-DD-MM (Day)</option>';
            html += '<option value="12">YYYY.DD.MM (Day)</option>';
        }
        $("#date_format").html(html);

    });

    $(document).on("change","#default_clock_format", function(){
        if ($(this).val() == 12 ) {
            $(".time-format").text("12 Hour Clock 08:10:21");
        }else{
            $(".time-format").text("24 Hour Clock  17:21:45");
        }
    });

      function toggleSidebarMenu(value){
        if (value == 1 ) {
            $(".default-sidebar").removeClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",true);
            $("#leftnav2").addClass("in");
            $("#leftnav2").removeAttr("style");
        }else{
            $(".default-sidebar").addClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",false);
            $("#leftnav2").removeClass("in");
            $("#leftnav2").css("height","0px");
        }
    }

});

//add settings form client side validations
$("#settingsForm").validate({
    rules: {
        company_name: {
            maxlength: 30,
            required:true
        },
        timeout: {
            required: true
        },
        zip_code: {
            maxlength: 30,
            number:true
        },
        address1: {
            required: true,
            maxlength: 255
        }
    }
});

//add clock settings form client side validations
$("#clocksettingsForm").validate({
    rules: {
        default_date_format: {
            required:true
        },
        date_format: {
            required: true
        },
        default_clock_format: {
            required: true
        }
    }
});

/*email signature*/


$(document).on("click", "#email-signature .email_default_save", function(e){
    e.preventDefault();
    var userId = $("#email_signature #user_id_hidden").val();
    //var email_signature = $("textarea[name='email_signature_default']").val();
    var email_signature = $('.summernote').summernote('code');
    emailsignature(userId,email_signature);
});
function emailsignature(userId,email_signature) {
    $.ajax({
        type: 'post',
        url: '/settings-ajax',
        data: {
            class: "DefaultSettingsAjax",
            action: "addSignature",
            id: userId,
            email_signature: email_signature
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                toastr.success("The email signature has been added.");
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }


        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}


/*preference tab default setting start*/
$('#preference_tab').on('submit',function(e){
    e.preventDefault();

    var formData = $('#preference_tab').serializeArray();

    var notifications_to_me = [];
    var notifications_to_meinner = [];
    $.each($("input[name='notifications_to_me']"), function(){
        var key1 = $(this).attr('data-attr');
        var value = $(this).val();
        var obj = {key1 :value};
        if ($(this).is(':checked')) {
            notifications_to_me.push(value);
        }else{
            notifications_to_me.push('');
        }
    });
    var userId = $("#preference_tab #user_id_preferences").val();


    if ($('#show_last_name').is(":checked"))
    {
        var show_last_name = 1;
    } else {
        var show_last_name = 0;
    }
    if ($('#birthday_notifications').is(":checked"))
    {
        var birthday_notifications = 1;
    } else {
        var birthday_notifications = 0;
    }
    if ($('#inventory_alert').is(":checked"))
    {
        var inventory_alert = 1;
    } else {
        var inventory_alert = 0;
    }
    if ($('#activate_2fa').is(":checked"))
    {
        var activate_2fa = 1;
    } else {
        var activate_2fa = 0;
    }
    if ($('#insurance_alert_check').is(":checked"))
    {
        var insurance_alert = $("input[name='insurance_alert']").val();
    } else {
        var insurance_alert = '';
    }
    if ($('#in_touch').is(":checked"))
    {
        var in_touch = 1;
    } else {
        var in_touch = 0;
    }

    $.ajax({
        type: 'post',
        url: '/settings-ajax',
        data: {
            class: 'DefaultSettingsAjax',
            action: 'PreferanceCompanyAjax',
            form: formData,
            id: userId,
            show_last_name:show_last_name,
            birthday_notifications:birthday_notifications,
            inventory_alert:inventory_alert,
            activate_2fa:activate_2fa,
            insurance_alert:insurance_alert,
            in_touch:in_touch,
            notifications_to_me:notifications_to_me
        },
        success: function (response) {
            var response = JSON.parse(response);

            if (response.status == 'success' && response.code == 200) {
                console.log('response>>>', response);
                $('#user_name').text(response.data.user_name);
                $("#audit_trail_list").trigger('reloadGrid');
                toastr.success("The record updated successfully");
            } else if (response.status == 'error' && response.code == 400) {
                $('.error').html('');
                $.each(response.data, function (key, value) {
                    $('.' + key).html(value);
                });
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
});

function goBack() {
    window.history.back();
}


$.ajax({
    type: 'post',
    url: '/settings-ajax',
    data: {
        class: "DefaultSettingsAjax",
        action: "viewPreferanceCompany"
    },
    success: function (response) {
        var data = $.parseJSON(response);
        if (data.status == "success"){
            var result = data.data;
            if(result)
            {
                var notificationData = data.data.notifications_to_me[0];
                if(notificationData !== undefined) {
                    for (var i = 0; i < notificationData.length; i++) {
                        if (notificationData[i] !== "") {
                            //console.log(notificationData[i]);
                            //console.log("if");
                            $("#pref-" + i).attr("checked", true);
                        } else {
                            $("#pref-" + i).attr("checked", false);
                            //console.log(notificationData[i]);
                            //console.log("else");
                        }
                    }
                }
            }

        }
    },
    error: function (data) {
    }
});

$.ajax({
    type: 'post',
    url: '/settings-ajax',
    data: {
        class: "DefaultSettingsAjax",
        action: "viewEmailSignature"
    },
    success: function (response) {
        var data = $.parseJSON(response);
       // alert(data.status);
        if (data.status == "success"){
            if(data.data.email_signature != ''){
                jQuery("input[name='email_signature']").attr("checked",true);
                // $('.note-editable').html(data.email_signature);
                    $(".summernote").summernote("code", data.data.email_signature);
                   // $('.summernote').val(data.data.email_signature);


            }

            var result = data.data;
            $.each(result, function (key, value) {
                $('#'+key).val(value);
            });
        }
    },
    error: function (data) {
    }
});
/*preference tab default setting start*/


/*------------------------------------ Audit trail user --------------------------------- */


$(document).on('change','#audit_user_select',function(){
    var grid = $("#audit_trail_list");
    var value = $(this).val();
    grid.jqGrid('GridUnload');
    jqGrid('All',value);
});

//intializing jqGrid
jqGrid('All','0');

/**
 * jqGrid Intialization function
 * @param status
 */
function jqGrid(status,extravalue) {
    var table = 'audit_trial';
    var columns = ['User Name','Role','IP Address','Action ','Action time','Description'];
    var extra_dropdown = (extravalue != '0')?{column:'users.user_type',value:extravalue}:{};
    var select_column = [];
    var joins = [{table:'audit_trial',column:'user_id',primary:'id',on_table:'users'}];//[{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['users.user_type'];
    var columns_options = [
        {name:'User Name',index:'name', width:200,align:"left",searchoptions: {sopt: conditions},table:'users'},
        {name:'Role',index:'role', width:180,align:"left",searchoptions: {sopt: conditions},table:table},
        {name:'IP Address',index:'ip_address',align:"left", width:190,searchoptions: {sopt: conditions},table:table},
        {name:'Action ',index:'action', align:"center",width:200,searchoptions: {sopt: conditions},table:table},
        {name:'Action time',index:'created_at', align:"left",width:150,searchoptions: {sopt: conditions},table:table},
        {name:'Description',index:'description', width:200, align:"left",searchoptions: {sopt: conditions},table:table}

    ];
    var ignore_array = [];
    jQuery("#audit_trail_list").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        sortname: 'id',
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            //select: select_column,
            columns_options: columns_options,
            status: status,
            extra_dropdown:extra_dropdown,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'no'
        },
        viewrecords: true,
        sortorder: "asc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Audit Trail",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:100,left:200,drag:true,resize:false} // search options
    );
}


$.ajax({
    type: 'post',
    url: '/settings-ajax',
    data: {
        class: "DefaultSettingsAjax",
        action: "viewPreferanceCompany"
    },
    success: function (response) {
        var data = $.parseJSON(response);
        if (data.status == "success"){
            var result = data.data;
            if(data.data.notifications_to_me !== false) {
                var notificationData = data.data.notifications_to_me[0].length;
                if(notificationData == '3'){
                    $("#notification_to_me").attr("checked", true);
                }
                $.each(data.data.notifications_to_me[0], function (key, value) {
                    if(value.key1 == 'Online maintenance requests are received') $("#pref-1").attr("checked", true);
                    if(value.key1 == 'New rental_applications are received') $("#pref-2").attr("checked", true);
                    if(value.key1 == 'BCC me on sent emails') $("#pref-3").attr("checked", true);
                });
            }
            if(data.data.activate_2fa == '1') $("#activate_2fa").attr("checked", true);
            if(data.data.birthday_notifications == '1') $("#birthday_notifications").attr("checked", true);
            if(data.data.in_touch == '1') $("#in_touch").attr("checked", true);
            if(data.data.insurance_alert == '1') $("#insurance_alert_check").attr("checked", true);
            if(data.data.inventory_alert == '1') $("#inventory_alert").attr("checked", true);
            if(data.data.show_last_name == '1') $("#show_last_name").attr("checked", true);
        }
    },
    error: function (data) {
    }
});







