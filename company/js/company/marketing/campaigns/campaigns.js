$(document).on('click', '.add-campaign-btn', function() {
$(".add-campaign-div").show();
    $(".add_campains_btn").text('Save');
    $("#add_new_campaign_form").trigger('reset');
    $(".start_date_campaign").datepicker({
        dateFormat: datepicker, changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());

    $(".end_date_campaign").datepicker({
        dateFormat: datepicker, changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());
});
$(document).ready(function () {
    jqGridCampaigns();
    $(".start_date_campaign").datepicker({
        dateFormat: datepicker, changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());

    $(".end_date_campaign").datepicker({
        dateFormat: datepicker, changeMonth: true,
        changeYear: true
    }).datepicker("setDate", new Date());

    jQuery('.call_track_number').mask('000-000-0000', {reverse: true});
});

$(document).on('click', '.cancel_campains_btn', function () {
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $(".add-campaign-div").hide(500);
        }
    });

});
$(document).on("change", ".start_date_campaign", function () {
    var dynclass = $(this).attr('id');
    var dynmid = $("#"+dynclass).closest('div').next().find('input').attr('id');
    var valuedatepicker=$(this).val();
    $("#"+dynmid).datepicker({dateFormat: datepicker,
        minDate: valuedatepicker,
    });


});
$(document).on("change", ".end_date_campaign", function () {
    var dynclass = $(this).attr('id');
    var dynmid = $("#"+dynclass).closest('div').prev().find('input').attr('id');
    var valuedatepicker=$(this).val();
    $("#"+dynmid).datepicker({dateFormat: datepicker,
        maxDate: valuedatepicker,
    });
});
$("#add_new_campaign_form").validate({
    rules: {
        source_name: {
            required: true
        },
        compaign_name: {
            required: true
        },
        call_track_number :{
            required: true
        }
    }
});
$(document).on("submit","#add_new_campaign_form",function (e) {
    e.preventDefault();
    if ($('#add_new_campaign_form').valid()) {
        var id=$("#campaign_id").val();
        var formData = $('#add_new_campaign_form').serializeArray();
        $.ajax({
            type: 'post',
            url: '/Marketing/campaignAjax',
            data: {
                form: formData,
                class: 'campaign',
                action: 'addCampaign',
                id:id

            },
            success: function (result) {
                var response = JSON.parse(result);
                if ((response.status == 'success') && (response.code == 200)) {
                    setTimeout(function(){
                        jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                        jQuery('.table').find('tr:eq(1)').find('td:eq(5)').addClass("green_row_right");
                        toastr.success(response.message);
                    }, 2000);
                    $(".add-campaign-div").hide();
                    $('#campaign-table').trigger('reloadGrid');
                }
                else if (response.code == '500') {
                    toastr.warning(response.message);
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }

});
function jqGridCampaigns(status, deleted_at) {

    var table = 'campaign';
    var columns = ['Source Name', 'Campaign Name', 'Start Date', 'End Date', 'Call Tracking No','Action'];
    var select_column = ['Edit', 'Delete'];
    var joins = [];
    var conditions = ["eq", "bw", "ew", "cn", "in"];
    var extra_where = [];
    var extra_columns = [];
    var columns_options = [
        {name: 'Source Name', index: 'source_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Campaign Name', index: 'compaign_name', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Start Date', index: 'start_date', company_insurance_typewidth: 200, align: "center", searchoptions: {sopt: conditions}, table: table, change_type: 'date'},
        {name: 'End Date', index: 'end_date', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table,change_type: 'date'},
        {name: 'Call Tracking No', index: 'call_track_number', width: 200, align: "center", searchoptions: {sopt: conditions}, table: table},
        {name: 'Action', index: 'select', title: false, width: 200, align: "center", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table}
    ];
    var ignore_array = [];
    jQuery("#campaign-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore: ignore_array,
            joins: joins,
            extra_columns: extra_columns,
            deleted_at: deleted_at,
            extra_where: extra_where
        },
        viewrecords: true,
        sortname: 'campaign.updated_at',
        sortorder: "desc",
        sorttype: 'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 10,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Campaings",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top: 10, left: 400, drag: true, resize: false} // search options
    );
}

$(document).on('change', '#campaign-table .select_options', function () {
    var opt = $(this).val();
    var id = $(this).attr('data_id');
    $("#campaign_id").val(id);
    $(".add_campains_btn").text('Update');
    if (opt == 'Edit' || opt == 'EDIT') {
        $(".add-campaign-div").show();
        $.ajax({
            type: 'post',
            url: '/Marketing/campaignAjax',
            data: {
                id: id,
                class: 'campaign',
                action: 'getCampaign'},
            success: function (response) {
                var res = $.parseJSON(response);
                if (res.code == 200) {
                    $(".source_name").val(res.data.source_name);
                    $(".compaign_name").val(res.data.compaign_name);
                    $('.start_date_campaign').val(res.data.start_date);
                    $('.end_date_campaign').val(res.data.end_date);
                    $('.call_track_number').val(res.data.call_track_number);

                    defaultFormData = $('#add_new_campaign_form').serializeArray();
                } else if (data.status == "error") {
                    toastr.error(data.message);
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
        $("#campaign-table").trigger('reloadGrid');
    } else if (opt == 'Delete' || opt == 'DELETE') {
        opt = opt.toLowerCase();
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/Marketing/campaignAjax',
                        data: {
                            id: id,
                            class: 'campaign',
                            action: 'deleteCampaign'
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                            } else if (response.code == 500) {
                                toastr.warning(response.message);
                            } else {
                                toastr.error(response.message);
                            }
                            $('#campaign-table').trigger('reloadGrid');
                        }
                    });
                }
                $('#campaign-table').trigger('reloadGrid');
            }
        });
    }
});

$(document).on('click','.clearFormReset',function(){
    var formid=$(this).attr('rel');
    var dataid=$(this).attr('data-id');
    var text=$("."+dataid).text();
    if(text == 'Update'){
        resetEditForm("#"+formid,['start_date','end_date'],true,defaultFormData,[]);
    }else{
        resetFormClear('#'+formid,['start_date','end_date'],'form',false);
    }

});