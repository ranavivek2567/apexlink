
$(document).ready(function () {
    getPropertyAddress();
    initMap();
});
var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 38.380422, lng: -100.658859},
        zoom: 4
    });
}
function getPropertyAddress() {
    $.ajax({
        type: 'post',
        url: '/Marketing/map',
        data:{
            class:'map',
            action:'getMapData'
        },
        success: function (result) {
            var response = JSON.parse(result);
            if ((response.status == 'success')) {
                $.each(response.data,function (key,value) {
                    // Place each marker on the map
                     var lat=parseFloat(value.latitude);
                     var long=parseFloat(value.longitude);
                    if(lat !== undefined && long !== undefined) {
                        var position = {lat: lat, lng: long};
                        var infowindow = new google.maps.InfoWindow({
                            content: '<div>'+value.property_name+'</div>',
                            maxWidth: 200
                        });
                        var marker = new google.maps.Marker({
                            position: position,
                            map: map,
                            title: 'title'
                        });

                        marker.addListener('click', function() {
                            infowindow.open(map, marker);
                        });
                    }

                });

            }
        }, error: function (jqXHR, status, err) {
            console.log(err);
        },
    });
}
