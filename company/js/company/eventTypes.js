/**
 * jqGrid function to format status
 * @param cellValue
 * @param options
 * @param rowObject
 * @returns {string}
 */
function statusFormatter (cellValue, options, rowObject){
    if (cellValue == 1)
        return "Active";
    else if(cellValue == '0')
        return "Inactive";
    else
        return '';
}
function statusFormatter1 (cellValue, options, rowObject){
    if (cellValue == 0)
        return "No";
    else if(cellValue == '1')
        return "Yes";
    else
        return '';
}
function statusFmatter1 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';

        if(rowObject['Status'] == '1' && rowObject['editable'] == '0')  select = ['Edit','Deactivate','Delete'];
        if(rowObject['Status'] == '1' && rowObject['editable'] == '1')  select = ['Edit','Deactivate'];
        if((rowObject['Status'] == '0' || rowObject.Status == '' )&& rowObject['editable'] == '0')  select = ['Edit','Activate','Delete'];
        if((rowObject['Status'] == '0' || rowObject.Status == '' ) && rowObject['editable'] == '1')  select = ['Edit','Activate'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}
function statusFmatter2 (cellvalue, options, rowObject){
    if(rowObject !== undefined) {
        var select = '';
        if(rowObject['is_editable'] == '1')  select = ['Edit','Deactivate'];
        if(rowObject['is_editable'] == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
        var data = '';
        if(select != '') {
            var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
            $.each(select, function (key, val) {
                data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
            });
            data += '</select>';
        }
        return data;
    }
}
var base_url = window.location.origin;
$(document).on("change", "#jqGridStatus", function (e) {
    var status = $(this).val();
    $('#eventType-table').jqGrid('GridUnload');

    jqGrid(status,true);

});




/**
 * jqGrid Initialization function
 * @param status
 */
jqGrid('All')
function jqGrid(status, deleted_at) {
    var table = 'company_event_type';
    var columns = ['Event type','Event Description', 'Set as default', 'editable','Status', 'Action'];
    if (status===0) {
        var select_column = ['Edit','Deactivate','Delete'];
        console.log(status);
        console.log("-------");
    }else {
        var select_column = ['Edit','Activate','Delete'];
        console.log(status);
        console.log("========");
    }
    var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [];

    var columns_options = [

        {name:'Event type',index:'event_type', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
        {name:'Event Description',index:'description', width:100,searchoptions: {sopt: conditions},table:table},
        {name:'Set as default',index:'is_default', width:80, align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter1},
        {name:'editable',index:'is_editable', hidden:true,searchoptions: {sopt: conditions},table:table},
        {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
        {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:statusFmatter1},

    ];
    var ignore_array = [];
    jQuery("#eventType-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "company_event_type",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:deleted_at
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Event Types",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:200,left:200,drag:true,resize:false} // search options
    );
}

$(document).ready(function () {
    var base_url = window.location.origin;

    var status =  localStorage.getItem("active_inactive_status");
    console.log(status);


    /** Show add new unit type div on add new button click */
    $(document).on('click','#addUnitTypeButton',function () {
        $('#event_type').prop('disabled', false);
        $('#unit_type').val('').prop('disabled', false);
        $('#description').val('');
        $("#user_id_hidden").val('');
        $("#event_type").val('');
        $('#event_typeErr').text('');
        $('#descriptionErr').text('');
        headerDiv.innerText = "Add Event Type";
        $('#is_default').prop('checked', false);
        $('#saveBtns').val('Save');
        $('#add_unit_type_div').show(500);
    });

    /** Show import excel div on import excel button click */
    $(document).on('click','#importUnitTypeButton',function () {
        $("#import_file-error").text('');
        $("#import_file").val('');
        $('#import_unit_type_div').show(500);
    });

    /** Hide add new unit type div on cancel button click */
    $(document).on("click", "#add_unit_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#add_unit_type_div").hide(500);
            } else {
            }
        });
    });



    /** Hide import excel div on cancel button click */
    $(document).on("click", "#import_unit_cancel_btn", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#import_unit_type_div").hide(500);
            } else {
            }
        });
    });


    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "Inactive";
        else
            return '';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Yes";
        else if(cellValue == '0')
            return "No";
        else
            return '';
    }


    /*change status event type*/
    function changeStatuseventType(id,action) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ApexNewUserAjax",
                action: "changeStatuseventType",
                apexnewuser_id: id,
                status_type: action,
            },
            success: function (response) {

                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The status has been changed.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#eventType-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }





    /** Export sample unit type excel */
    $(document).on("click",'#exportSampleExcel',function(){
        window.location.href = base_url+"/UnitType-Ajax?action=exportSampleExcel";
    })

    /** Export unit type excel  */
    $(document).on("click",'#exportUnitTypeButton',function(){
        var status = $("#jqGridStatus option:selected").val();
        var table =  'company_unit_type';
        window.location.href = base_url+"/UnitType-Ajax?status="+status+"&&table="+table+"&&action=exportExcel";
    });
    /*  insert into event type*/

    function saveCategory(){

        var event_type = $("input[name='event_type']").val();
        var desc = $("textarea[name='description']").val();
        var user_id_hidden = $("input[name='user_id_hidden']").val();
        var is_default = '0';
        if ($('#is_default').is(":checked")) {
            is_default = '1';   // it is checked
        }


        // console.log(event_type);


        $.ajax({
            type: 'post',
            url: '/MasterData/EventType-Ajax',
            data: {
                class: 'EventTypeAjax',
                action: "insert",
                event_type: event_type,
                desc: desc,
                user_id_hidden:user_id_hidden,
                is_default:is_default

            },
            success: function (response) {
                console.log('response>>>', response);
                var response = JSON.parse(response);
                if(response.status == 'success' && response.code == 200){

                    $('#eventType-table').trigger( 'reloadGrid' );
                    $("#add_event_type").trigger("reset");
                    $("#add_unit_type_div").hide(500);
                    toastr.success(response.message);
                    onTop(true);


                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('*This field is required');
                    $.each(response.data, function (key, value) {
                        $('*This field is required'+key).html(value);
                    });
                }
                else if (response.status == 'error' && response.code == 503) {
                    toastr.warning(response.message);
                }
            },
            error: function (data) {
            }
        });


    };

    function geteventType(id){
        $.ajax({
            type: 'post',
            url: '/MasterData/EventType-Ajax',
            data: {
                class: "geteventType",
                action: "geteventType",
                cuser_id: id
            },
            success: function (response) {

                var response = JSON.parse(response);
                $("#add_event_type input[name='event_type']").val(response.event_type);
                $("#add_event_type textarea[name='description']").val(response.description);
                if (response.is_default==1) {
                    $('#is_default').prop('checked', true);
                }
                else {
                    $("#is_default").prop('checked', false);
                }


            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function update_event_status(id,action) {
        $.ajax({
            type: 'post',
            url: '/MasterData/EventType-Ajax',
            data: {
                class: "EventTypeAjax",
                action: "update_event_status",
                user_id: id,
                status_type: action,

            },
            success: function (response) {

                var response = JSON.parse(response);
                responseData = response.data;
                if (response.status == 'success' && response.code == 200) {

                    if(responseData.status==1) {
                        toastr.success("Record Activated Successfully.");
                    }
                    else{
                        toastr.success("Record Deactivated Successfully.");
                    }
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }else if(response.status == 'error' && response.code == 503){
                    toastr.error("A default value cannot be deactivated");
                }
                localStorage.setItem("Message","Event Updated Successfully")
                localStorage.setItem("rowcolor",true)
                $('#eventType-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    function deleteeventType(id) {
        $.ajax({
            type: 'post',
            url: '/MasterData/EventType-Ajax',
            data: {
                class: "EventTypeAjax",
                action: "deleteeventType",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The record deleted successfully.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                else if(response.status == 'error' && response.code == 503){
                    toastr.error("A default value cannot be deleted");
                }
                $('#eventType-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
    $(document).on("change", "#eventType-table .select_options", function (e) {
        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');
        var checkDelete = $('option[value="Delete"]',this).length;

        if (checkDelete == "0"){
            $("input[name='event_type']").attr("disabled",true);
        }else{
            $("input[name='event_type']").attr("disabled",false);
        }

        console.log('-----');

        switch(action) {

            case "Edit":
                //Deactivate or Activate manage user
                headerDiv.innerText = "Edit Event Type";
                $('#event_typeErr').text('');
                $('#descriptionErr').text('');
                $('#saveBtns').val('Update');
                $('.clearFormReset').hide();
                $('.resetbtn').show();
                $("#add_event_type #user_id_hidden").val(id);
                $("#add_unit_type_div").show();
                $('#eventType-table').trigger('reloadGrid');
                geteventType(id);
                break;
            case "Deactivate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                    if (result == true) {
                        update_event_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/MasterData/AddEventType'
                    }
                });
                break;
            case "Activate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                    if (result == true) {
                        update_event_status(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/MasterData/AddEventType'
                    }
                });
                break;
            case "Delete":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this record?", function (result) {
                    if (result == true) {
                        deleteeventType(id);
                    } else {
                        window.location.href =  window.location.origin+'/MasterData/AddEventType'
                    }
                });
                break;
            default:
                window.location.href = base_url+'/MasterData/AddEventType';
        }

        return false;
    });

    $(document).on("click", "#saveBtns", function (e) {
        e.preventDefault();

                    saveCategory();

    });
    $(document).on("click", ".clearFormReset", function () {
       $('#add_event_type')[0].reset();
    });

    $(document).on("click", ".resetbtn", function () {
      var id = $("#add_event_type #user_id_hidden").val();
        geteventType(id);
    });
});