$(document).ready(function () {
    // alert("dsafsfdsf");
    /**
     * call dynamic view listing User alerts data
     */
    getUserAlert();
    function getUserAlert(){
        $.ajax({
            type: 'post',
            url: '/UserAlert-Ajax',
            data: {
                class: 'userAlert',
                action: 'userAlertEdit',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $('.useralert').html(response.html);
                    $('.useralertowner').html(response.owner);
                    $('.useralertvendor').html(response.vendor);
                    $('.useralertlease').html(response.lease);
                    $('.useralertpayment').html(response.payment);
                    $('.useralertcommunication').html(response.communication);
                    $('.useralertbusiness').html(response.business);
                    $('.useralertmaintenance').html(response.maintenance);
                    $('.useralertlead').html(response.lead);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /** Hide user alerts edit action on cancel button click */
    $(document).on("click", "#userAlertCancel", function (e) {
        e.preventDefault();
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $("#useralertmodel").hide(500);
                }else{
                    $("#useralertmodel").modal('show');
                    $(".useralertmodal").addClass('in');
                }
            }
        });
    });

    $(document).on('click','#testMailCancel',function () {
        $(".emailtest").val('');
    });


    /**
     *change function to perform various actions(edit)
     * @param status
     */
    jQuery(document).on('change','.select_options',function () {
        var select_options = $(this).val();
        var data_id = $(this).attr('data_id');
        var  module_type = $(this).attr('module_type');
        var  alert_name = $(this).attr('alert_name');
        // console.log(alert_name);
        // var id = $(this).attr('id');
        if (select_options == 'Edit'){

            $.ajax({
                type: 'post',
                url: '/UserAlert-Ajax',
                data: {class: 'userAlert', action: 'view', 'data_id': data_id},
                success: function (response) {
                    var response = JSON.parse(response);
                    // var id = $("#jqGridStatus option:selected").attr("val");
                    // var myJSON = JSON.stringify(response.data.send_to_users);
                    if (response.status == 'success' && response.code == 200) {
                        // console.log(response);
                        $("input[type=checkbox]").prop("checked",false);

                        $("#useralertmodel").modal('show');
                        $("#alert_edit_id").val(data_id);
                        $("#no_of_days_before-error").val('');
                        $(".field_alert_name").html(response.data.alert_name);
                        $(".field_alert_description").html(response.data.description);
                        $("#user_alert_id").val(response.data.id);
                        $("#status").val(response.data.status);
                        $("#no_of_days_before").val(response.data.no_of_days_before);
                        $("#alert_subject").val(response.data.subject);
                        $('.field_sendto').val(response.data);
                        $('#no_of_days_before-error').text('');
                        $('#alert_subject-error').text('');
                        $('#status-error').text('');
            //<--13-3-2020--    
                        if(response.data.alert_type == "Real Time"){
                            $("#no_of_days_before").prop("disabled", true);
                            // console.log("Shiva,");
                        }else{
                            $("#no_of_days_before").prop("disabled", false);
                        }
            //---end---> 
                        $.each(response.data.send_to_users, function( index, value ) {
                            $("input[type=checkbox][value="+value+"]").prop("checked",true);
                        });

                    }else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                }
            });
        } else if (select_options == 'Preview'){

            $.ajax({
                type: 'post',
                url: '/UserAlert-Ajax',
                data: {class: 'userAlert', action: 'view', 'data_id': data_id},
                success: function (response) {
                    var response = JSON.parse(response);
                    // console.log(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#useralertnamemodel").modal('show');
                        $("#alert_edit_id").val(data_id);
                        $(".field_alert_name2").html(response.data.alert_message);
                    }else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                }
            });
        } else if (select_options == 'testMail'){
            $(".emailtest").val('');
            $.ajax({
                type: 'post',
                url: '/UserAlert-Ajax',
                data: {class: 'userAlert', action: 'view', 'data_id': data_id},
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        $("#useralerttestmail").modal('show');
                        $("#alert_edit_id").val(data_id);
                        $("#module_type").val(module_type);
                        $("#test_alert_name").val(alert_name);
                        $('#email-error').text('');
                        // $(".field_alert_name2").html(response.data.alert_name);
                    }else {
                        toastr.warning('Record not updated due to technical issue.');
                    }
                }
            });
        }
        $(".select_options").prop('selectedIndex', 0);
    });
    /** Add/Edit new alert */
    $("#update_alert_form").validate({
        rules: {
            no_of_days_before: {
                required: true
            },
            subject:{
                required: true
            },
            status: {
                required: true
            },
            'send_to_users[]':{
                require_from_group: true

            }

        },
        messages: {
            no_of_days_before:{
                required: "* This field is required.",
            }
        },
        submitHandler: function () {
            // event.preventDefault();
            var form = $('#update_alert_form')[0];
            var formData = new FormData(form);
            formData.append('action','update');
            formData.append('class','userAlert');
            $.ajax({
                type: 'post',
                url: '/UserAlert-Ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {
                        toastr.success(response.message);
                        getUserAlert();
                        $('#useralertmodel').modal('toggle');
                    } else if (response.status == 'error' && response.code == 503) {
                        toastr.warning(response.message);
                    }
                }
            });
        }
    });
    jQuery.validator.addMethod("require_from_group", function(value, element, options) {

        var checkedVal = $("input[name='send_to_users[]']:checked").val();
        if(checkedVal  != "1" && checkedVal != "2"){
            toastr.warning("Please select atleast one recipient");
            return false;
        }
        return true;
    },"");

    // <---13-3-2020
    $(document).on("blur", "#no_of_days_before", function(){
        var val = $(this).val();
        if(val < 3){
            $("#no_of_days_before").val("");
            toastr.warning("Value Must be Greater than 3");
            return false;
        }
    });
    // ---->
    getUserAlertRoles();
    function getUserAlertRoles(){
        $.ajax({
            type: 'post',
            url: '/UserAlert-Ajax',
            data: {
                class: 'userAlert',
                action: 'userRoles',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var field_send_to = '';
                    $.each(response.data, function (key, value) {
                        field_send_to += '<div class="check-outer"><input class="" type="checkbox" value="'+value.id+'" name="send_to_users[]"> <label>'+value.role_name+'</label></div>';
                    });
                    $('.field_send_to').html(field_send_to);
                } else {
                    toastr.error(response.message);
                }
            }
        });
    }

    /**
     * testmail functionality
     */
    $("#useralert_testmail").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            email:{
                required: "* This field is required."

            }
        },
        submitHandler: function () {
            // event.preventDefault();
            var form = $('#useralert_testmail')[0];
            var email = $('#email').val();
            var alert_name = $('#test_alert_name').val();
            var data_id = $(this).attr('data_id');
            var module_type = $("#module_type").val();
            var formData = {
                'alert_name' : alert_name,
                'email': email,
                'module_type': module_type
            }
            // console.log(formData);
            var formData = new FormData(form);
            formData.append('action','testmail');
            formData.append('class','userAlert');
            $.ajax({
                type: 'post',
                url: '/UserAlert-Ajax',
                processData: false,
                contentType: false,
                data: formData,
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response != '') {
                        if (response.status == 'success' && response.code == 200) {
                            $("#alert_edit_id").val(data_id);
                            $("#test_alert_name").val(response.data.alert_name);
                            // getUserAlert();
                            $('#email').val('');
                            $("#useralerttestmail").modal('hide');
                            toastr.success(response.message);
                        } else if (response.status == 'error' && response.code == 503) {
                            toastr.warning(response.message);
                        }
                    }else{
                        toastr.warning('Unable to send email due to technical isuue.');
                    }
                },
                error: function (data) {
                    toastr.warning('Unable to send email due to technical isuue.');
                }
            });
        }
    });
});