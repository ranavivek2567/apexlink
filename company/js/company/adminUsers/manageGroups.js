$(document).ready(function (){

    /** Show add new unit type div on add new button click */


    /** Hide add new unit type div on cancel button click */

    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid() {
        var table = 'calendar_groups';
        var columns = ['Group Names','Description', 'Members','id','Action'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var pagination;
        var columns_options = [
            {name:'Group',index:'calendar_names', width:200, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Description',index:'description', width:200, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Members',index:'members', width:200, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'id',index:'id', width:200, align:"center",hidden:true,searchoptions: {sopt: conditions},table:table},
            {name:'Action',index:'', title: false, width:200,align:"right",sortable:false,cellEdit: true, editable: true,formatter:acttion_formatter},
        ];
        var ignore_array = [];
        jQuery("#manageVehicle-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "calendar_groups",
                columns_options: columns_options,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:true
            },
            viewrecords: true,
            sortname: 'id',
            sortorder: "asc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Vehicles",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }

    /**
     * jqGrid function to currency status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */

    /*  insert into vehicle type*/
    function acttion_formatter (cellValue, options, rowObject){
        var data="";
           if (cellValue != undefined){
               console.log(rowObject);
                     data = '<div class="edit_group" style="text-align: center">' +
                        '<a color="#222222;" style="cursor: pointer; text-decoration: underline;" id="edit_permissions">Edit</a>'+
                      '<input type="hidden" value="'+rowObject.Group+'" class="cal_info" data-val="'+rowObject.id+'">'+
                        '</div>';
            }
        return data;
    }
        $(document).on('click','#edit_permissions',function () {
            var val=$(this).next().val();
            var id=$(this).next().attr('data-val');
            $("#grp_name").val(val);
            $(".hidden_grp_id").val(id);
            $("#manage_groups_id").show();
        });
        $.ajax({
        type: 'post',
        url: '/User/ManageGroupsAjax',
        data: {
            class: "manageGroups",
            action: "usersData",
        },
        success: function (response) {
            var response = JSON.parse(response);
            if(response.status == 'success' && response.code == 200){
                  $("#select_user").append(response.html);
                  $('#select_user').multiselect('destroy');
                  $('#select_user').multiselect({includeSelectAllOption: true,nonSelectedText: 'Select', search: true});
                // $("#select_user").multiselect({
                //     allSelectedText: 'All Selected',
                //     enableFiltering: true,
                //     nonSelectedText: 'Select',
                //     includeSelectAllOption: true,
                //     search: true,
                // }).multiselect('selectAll', false).multiselect('updateButtonText');
            }
        }
       });
      $(document).on('click','.cancel_grp',function () {
          $("#manage_groups_id").hide();
      });
      $(document).on('click','#save_group',function () {
          var checked_val=$("#select_user").val();
          var id=$(".hidden_grp_id").val();
          console.log(id);
          var description=$("#description").val();
          $.ajax({
              type: 'post',
              url: '/User/ManageGroupsAjax',
              data: {
                  class: "manageGroups",
                  action: "update_permissions",
                  id:id,
                  checked_val:checked_val,
                  description:description
              },
              success: function (response) {
                  var response = JSON.parse(response);
                  if(response.status == 'success' && response.code == 200){
                   toastr.success(response.message);
                 setTimeout(function () {
                  window.location.reload()
                 },2500);
                  }
              }
          });
      });

});