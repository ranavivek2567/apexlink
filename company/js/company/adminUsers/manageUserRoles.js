$(document).ready(function (){

    var status =  localStorage.getItem("active_inactive_status");
    if(status !== undefined) {
        if ($("#ManageUserRoles-table")[0].grid) {
            $('#ManageUserRoles-table').jqGrid('GridUnload');
        }
        /*intializing jqGrid*/
        if(status == 'all'){
            jqGrid('All');
        }  else {
            jqGrid(status);
        }
        $('#jqGridStatus option[value='+status+']').attr("selected", "selected");

    }else{
        if ($("#ManageUserRoles-table")[0].grid) {
            $('#ManageUserRoles-table').jqGrid('GridUnload');
        }
        jqGrid('All');
    }

    /*jqGrid status*/
    $('#jqGridStatus').on('change',function(){
        var selected = this.value;
        $('#ManageUserRoles-table').jqGrid('GridUnload');
        $('#add_user_role_div').hide(500);
        $('.listUserRoleDiv').show(500);
        changeGridStatus(selected);
        jqGrid(selected);
    });
    /** Show add new unit type div on add new button click */
    $(document).on('click','#addUserRoleBtn',function () {
        $('#role_name').val('').prop('disabled', false);
        $('#role_nameErr').text('');
        $('#role_name-error').text('');
        $('#role').prop("disabled", true);
        $('#copy_role').prop('checked', false);
        $('#edit_user_role_id').val('');
        $('#copy_user_role_id').val('');
        $('#addUserRoleSaveBtn').val('Save');
        $('#add_user_role_div').show(500);
        $('.listUserRoleDiv').hide(500);
        $('#tree-container').show(500);
        $('#tree-container').jstree({
            'plugins': ["wholerow", "checkbox"]
        });
        $('#tree-container_copy').jstree('destroy');
        $('#tree-container_edit').jstree('destroy');
        $('#tree-container').jstree(true).deselect_all();
        getAllUserRoles()
    });

    $('#copy_role').on('click',function() {
        if ($('#copy_role').is(":checked")) {
            var copy_role = '1';
            $('#role').prop("disabled", false);
        } else {
            var  copy_role = '0';
            $('#tree-container_copy').jstree('destroy');
            $('#tree-container').jstree({
                'plugins': ["wholerow", "checkbox"]
            });
            $('#tree-container').show();
            $('#tree-container_edit').jstree('destroy');
            $('#role').val('').prop("disabled", true);
        }
    });


    /** Get all user roles */
    function getAllUserRoles(){
        $.ajax({
            type: 'post',
            url: '/UserRoles-Ajax',
            data: {
                class: "manageUserRoles",
                action: "getAllUserRoles",
            },
            success: function (response) {
                var response = JSON.parse(response);
                var data = response.data;
                $('#role')[0].options.length = 0;
                $('#role').append($('<option>', { value : '' }).text('Select'));
                $.each(data, function(key, value) {
                    $('#role')
                        .append($('<option>', { value : value['id'] }).text(value['role_name']));
                });
            }
        });
    }

    /** Hide add new unit type div on cancel button click */
    $(document).on("click", "#addUserRoleCancelBtn", function (e) {
        e.preventDefault();
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#add_user_role_div").hide(500);
                $(".listUserRoleDiv").show(500);
            } else {
            }
        });
    });

    /** select previous roles on change roles **/
    $('#role').on('change',function(){
        var role_id = this.value;
        if(this.value == ''){
            $('#tree-container').show();
            $('#tree-container_copy').hide();
        } else {
            $.ajax({
                url: "/UserRoles-Ajax",
                method: 'post',
                data: {
                    class: "manageUserRoles",
                    action: "getAllUserRoles",
                    'role_id': role_id,
                },
                success: function (result) {
                    var result = JSON.parse(result);
                    if (result.status == 'success') {
                        $('#copy_user_role_id').val(role_id);
                        $('#tree-container_copy').jstree('destroy');
                        $('#tree-container_edit').hide();
                        $('#tree-container').hide();
                        $('#tree-container_copy').show();
                        $('#tree-container_copy').jstree({
                            'plugins': ["wholerow", "checkbox"],
                            'core' : {
                                expand_selected_onload : false,
                                'data': result.data.role
                            },
                        });
                    } else {
                        console.log('result>>>',result);
                    }
                },
                error: function (data) {
                    console.log('error>>>', data);
                }
            });

        }
    });

    /** Add new user role **/
    $('#add_user_role_form').validate({
        rules: {
            role_name:{
                required: true
            }
        },submitHandler: function () {
            var role_name = $('#role_name').val();
            var edit_user_role_id = $('#edit_user_role_id').val();
            var copy_user_role_id = $('#copy_user_role_id').val();;
            var role_checkbox,roles;
            var checked_ids = '';

            if(edit_user_role_id == '' && copy_user_role_id == ''){
                role_checkbox = $('#tree-container').jstree(true).get_json('#', {flat:true});
                roles = JSON.stringify(role_checkbox);

                checked_ids =  $('#tree-container').jstree(true).get_selected();
            } else {
                if($('input[name=copy_role]:checked').length > 0 ){
                    role_checkbox = $('#tree-container_copy').jstree(true).get_json('#', {flat:true});
                    roles = JSON.stringify(role_checkbox);

                    checked_ids =  $('#tree-container_copy').jstree(true).get_selected();
                } else {
                    role_checkbox = $('#tree-container_edit').jstree(true).get_json('#', {flat:true});
                    roles = JSON.stringify(role_checkbox);

                    checked_ids =  $('#tree-container_edit').jstree(true).get_selected();
                }
            }

            checked_ids = (checked_ids == '' || checked_ids == null) ? '' : checked_ids;

            var formData = {
                'role_name': role_name,
                'checked_ids' : checked_ids,
                'role': roles,
                'edit_user_role_id':edit_user_role_id
            };
            var action;
            if(edit_user_role_id){
                action = 'update';
            } else {
                action = 'insert';
            }

            $.ajax({
                url: "/UserRoles-Ajax",
                method: 'post',
                data: {
                    class: "manageUserRoles",
                    action: action,
                    form: formData,
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.code == 503 || response.status == 'error'){
                        toastr.error(response.message);
                    }

                    if (response.status == 'success') {
                        $('#add_user_role_form')[0].reset();
                        $('#add_user_role_div').hide(500);
                        $('.listUserRoleDiv').show(500);
                        $("#ManageUserRoles-table").trigger('reloadGrid');
                        toastr.success(response.message);
                        onTop(true);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });

        }
    })

    /**
     * jqGrid Initialization function
     * @param status
     */
    function jqGrid(status, deleted_at) {
        var table = 'company_user_roles';
        var columns = ['Role Name', 'Status', 'Action'];
        var select_column = ['Edit','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var pagination;
        var columns_options = [

            {name:'Role Name',index:'role_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFormatter},
            {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table,formatter:actionFormatter}

        ];
        var ignore_array = [];
        jQuery("#ManageUserRoles-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_user_roles",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Manage User Role",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {top:200,left:200,drag:true,resize:false}
        );
    }

    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter (cellValue, options, rowObject){
        if (cellValue == 1)
            return "Active";
        else if(cellValue == '0')
            return "InActive";
        else
            return '';
    }

    /**
     * jqGrid function to format action column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function actionFormatter (cellValue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            var role_name = rowObject['Role Name'];
            var editable = $(cellValue).attr('editable');
            if(rowObject.Status == 1)  select = ['Edit','Deactivate','Delete'];
            if(rowObject.Status == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete'];
            if(role_name == 'Admin')  select = '';
            if(role_name == 'Property Manager')  select = ['Edit'];
            var data = '';
            if(select != '') {
                var data = '<select ' +
                    ' class="form-control select_options"  data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    if(editable == '0' && (val == 'delete' || val == 'Delete')){
                        return true;
                    }
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }

    /** Action tab's select options */
    $(document).on("change", "#ManageUserRoles-table .select_options", function (e) {
        e.preventDefault();
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        var action = this.value;
        var id = $(this).attr('data_id');

        switch(action) {

            case "Edit":
                $('#copy_role').prop('checked', false);
                $('#addUserRoleSaveBtn').val('Update');
                $('#add_user_role_div').show(500);
                $('.listUserRoleDiv').hide(500);
                $('#tree-container').hide();
                $('#tree-container_copy').jstree('destroy');
                $(".clear-btn").text('Reset');
                $(".clear-btn").addClass('formreset');
                $(".clear-btn").removeClass('clearFormReset');
                getAllUserRoles();
                getUserRole(id);

                break;

            case "Delete":
                bootbox.confirm("Do you want to delete this user?", function (result) {
                    if (result == true) {
                        $.ajax({
                            url: "/UserRoles-Ajax",
                            method: 'post',
                            data: {
                                class: "manageUserRoles",
                                action: "delete",
                                'role_id': id,
                            },
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                        $("#ManageUserRoles-table").trigger('reloadGrid');
                    }
                });
                break;
            case "Deactivate":
                bootbox.confirm("Do you want to deactivate this record?", function (result) {
                    if (result == true) {
                        changeStatusManageUserRole(id,action);
                    }
                });
                break;
            case "Activate":
                bootbox.confirm("Do you want to activate this record?", function (result) {
                    if (result == true) {
                        changeStatusManageUserRole(id,action);
                    }
                });
                break;
            default:
                window.location.href = base_url+'/user/ManageUserRoles';
        }

        return false;
    });

    /** Change status function i.e. Activate/Deactivate **/
    function changeStatusManageUserRole(id,status) {
        var status = status == 'Activate' ? '1' : '0';
        $.ajax({
            url: "/UserRoles-Ajax",
            method: 'post',
            data: {
                class: "manageUserRoles",
                action: "updateStatus",
                'role_id': id,
                'status': status,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success(response.message);
                }
                $('#ManageUserRoles-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function getUserRole(role_id){
        $.ajax({
            url: "/UserRoles-Ajax",
            method: 'post',
            data: {
                class: "manageUserRoles",
                action: "getAllUserRoles",
                'role_id': role_id,
            },
            success: function (response) {

                var response = JSON.parse(response);
                $('#edit_user_role_id').val(response.data.id);
                if(response.data.role_name == "Property Manager"){
                    $('#role_name').val(response.data.role_name).prop('disabled', true);
                } else {
                    $('#role_name').val(response.data.role_name).prop('disabled', false);
                }
                $('#role_nameErr').text('');
                $('#role_name-error').text('');
                $('#role').prop("disabled", true);

                $('#tree-container_edit').jstree('destroy');
                $('#tree-container_copy').hide();
                $('#tree-container').hide();
                $('#tree-container_edit').jstree({
                    'plugins': ["wholerow", "checkbox"],
                    'core' : {
                        expand_selected_onload : false,
                        'data': response.data.role
                    },
                });
                $('#tree-container_edit').show();
                defaultFormData = $('#add_user_role_form').serializeArray();
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});