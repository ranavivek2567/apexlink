/**
 * jqGrid Initialization function
 * @param status
 */
jqGrid('All');
function jqGrid(status) {
    var table = 'audit_trial';
    var columns = ['Email','Full Name', 'Login Time'];
    var select_column=[];
    var joins = [{table:'audit_trial',column:'user_id',primary:'id',on_table:'users'}];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = [ ];
    var extra_where = [{column:'action',value:'login',condition:'=',table:table}];
    var columns_options = [
        {name:'Email',index:'email', width:100,align:"center",searchoptions: {sopt: conditions},table:'users'},
        {name:'Full Name',index:'first_name', width:100,align:"center",searchoptions: {sopt: conditions},table:'users',change_type:'last_name_first'},
        {name:'Login Time',index:'created_at', width:100,align:"center",searchoptions: {sopt: conditions},table:'audit_trial',change_type:'date_time_format'}
    ];
    var ignore_array = [];
    jQuery("#loginHistory-table").jqGrid({
        url: '/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: "audit_trial",
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            deleted_at:'no',
            extra_where:extra_where
        },
        viewrecords: true,
        sortname: 'audit_trial.updated_at',
        sortorder: "desc",
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: pagination,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Logins",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {top:200,left:200,drag:true,resize:false}
    );
}
