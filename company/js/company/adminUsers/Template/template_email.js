$(document).ready(function () {

    //jqGrid status
    $('#jqGridStatus').on('change', function () {
        var selected = this.value;
        var deleted_at = true;
        $('#emailTemplates-table').jqGrid('GridUnload');
        if (selected == 4)
            deleted_at = false;
        jqGrid(selected);
    });





    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    var protocol = location.port;
    if (protocol != "") {
        var webpath = location.protocol+"//"+location.hostname+":"+protocol;
    }else{
        var webpath = location.protocol+"//"+location.hostname;
    }
    function jqGrid(status) {
        var table = 'emailtemplatesadmin';
        var columns = ['Email Title','Action'];
        var select_column = [];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['emailtemplatesadmin.deleted_at,emailtemplatesadmin.updated_at'];
        var columns_options = [
            {name: 'Email Title', index: 'template_title', width: 135,  align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            /*{name: 'Action', index: 'select', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:'actions',editable:true},*/
            {name:'Action',index:'id',classes: 'textCenterTd', title: false, width:135,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: imageRedirect, edittype: 'select',search:false,table:table},
        ];
        var ignore_array = [];
        jQuery("#emailTemplates-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of templates",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 200, left: 400, drag: true, resize: false} // search options
        );
    }
    function actionFormatterObject(cellValue, options, rowObject){
        if(rowObject !== undefined){
            //setTimeout(function(){ $('#inspection-table .select_options').attr('object_id',rowObject.ObjectID); }, 500);
            return cellValue;
        }

    }

    function imageRedirect(cellValue, options, rowObject){

        if(rowObject !== undefined){
            return "<a href='/Template/EmailTemplateEdit?id="+rowObject.id+"'><img src='"+webpath+"/company/images/edit-icon.png' id='' title='Edit' alt='my image' style='cursor: pointer; '/></a>";
        }

    }

    /*Get Property/Building Details Name List*/
    emailTemplateDataDetails();
    function emailTemplateDataDetails(){
        var id = $("#editTemplate_mode #editTempale_id").val();
        //alert(id);
        $.ajax({
            type: 'POST',
            url: '/EditEmailTemplate-Ajax',
            data: {
                class: "editEmailTemplate",
                action: "emailTemplateDataDetails",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var html ="";
                console.log(response.data);
                console.log(response.data.templateTitle.template_title);
                //console.log("------------------");
                if (response.code == 200) {
                    $('#editTemplate_mode #tempLetterName').val(response.data.templateTitle.template_title);
                    if(response.data.templateTags.length > 0){
                        for(var i=0; i<response.data.templateTags.length; i++){
                            var html='<option value="'+ response.data.templateTags[i].tag_name +'">'+ response.data.templateTags[i].tag_name +'</option>';
                            var hee = $("#editTemplateEmail #tagEmailTemplate").append(html);
                            //$("#editTemplateEmail #tagEmailTemplate").hide();
                           // alert(hee);
                        }

                    }
                    /*$("#editTemplate_mode .summernote").summernote("code", "<iframe id='myframe'  frameborder='0' allowtransparency='true' title='Rich Text AreaPress ALT-F10 for toolbar. Press ALT-0 for help' style='width: 100%; height: 308px; display: block;'></iframe>");
                    document.getElementById('myframe').contentWindow.document.write(response.data.template_html);*/
                    $("#editTemplate_mode .summernote").summernote("code",response.data.templateTitle.template_html );
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /*dropdown on selected values*/
    $(document).on("change","#editTemplateEmail #tagEmailTemplate",function(){
        var selectedValue = $('#editTemplateEmail #tagEmailTemplate').find(":selected").text();

        $('.summernote').summernote('editor.saveRange');
        $('.summernote').summernote('editor.restoreRange');
        $('.summernote').summernote('editor.focus');
        $('.summernote').summernote('editor.insertText', "{"+selectedValue+"}");
        //  $("#editTemplate_mode .summernote").summernote("insertText","{"+selectedValue+"}");

    });


    /*reset orignal value email template*/
    $(document).on("click","#editTemplateEmail #resetBtn",function(){
        window.location.reload();
    });
    /*reset orignal value email template*/



    $(document).on("submit","#editTemplateEmail",function(e){
        e.preventDefault();
        //var template_key = $("#editTemplateEmail .editor-email-signature .note-editable #myframe").contents().find('body input[type=hidden]').val();
        var template_key = $("#editTemplateEmail .editor-email-signature .note-editable").find('.wrapper input[type=hidden]').val();
        var template_html = $("#editTemplateEmail .editor-email-signature .note-editable").html();
       // alert(template_html);
       // var template_html = document.getElementById('myframe').contentWindow.document.documentElement.outerHTML;
        //var template_html = $("#editTemplateEmail .editor-email-signature .note-editable #myframe").contents().attr( "html" );
       // alert(template_key);
        $.ajax({
            type: 'POST',
            url: '/EditEmailTemplate-Ajax',
            data: {
                class: "editEmailTemplate",
                action: "updateTemplateEdit",
                template_key: template_key,
                template_html: template_html
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log(response.data);
                if (response.status == 'success' && response.code == 200) {
                    var base_url = window.location.origin;
                    localStorage.setItem("Message", "This record saved successfully.");
                    localStorage.setItem("rowcolor",true);
                    window.location.href = base_url+"/Template/EmailTemplate";
                    $("#emailTemplates-table").trigger('reloadGrid');
                    onTop(true);


                } else {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });

    });


    /*Cancel Button*
    /** Cancel button click*/
    $(document).on('click','#update_email_cancel_btn',function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    var base_url = window.location.origin;
                    window.location.href = base_url+"/Template/EmailTemplate";
                    }
                }

        });
    });
    /*Cancel Button*/

    $(document).on('click','.ResetTemplate',function () {
        bootbox.confirm("Do you want to reset this template?", function (result) {
            if (result == true) {
               location.reload();
            }
        });
    });

});