getAllUserRoles();
/** Get all user roles */
function getAllUserRoles() {
    $.ajax({
        type: 'post',
        url: '/UserRoles-Ajax',
        data: {
            class: "manageUserRoles",
            action: "getAllUserRoles",
        },
        success: function (response) {
            var response = JSON.parse(response);
            var data = response.data;
            $('#role_select')[0].options.length = 0;
            $('#role_select').append($('<option>', {value: ''}).text('Select'));
            $.each(data, function (key, value) {
                $('#role_select')
                    .append($('<option>', {value: value['id']}).text(value['role_name']));
            });
        }
    });
}

$('#role_select').on('change', function () {
    $('#save_role_athzation_btn_div').hide();
    var role_id = this.value;
    $('#role_id').val('');
    if (role_id == ''){
        $('.edit_btn').hide();
        $('#user_with_properties').hide();
    } else {
        $('.edit_btn').show();
        $('#user_with_properties').show();
        $.ajax({
            type: 'post',
            url: '/RoleAuthorization-Ajax',
            data: {
                class: "roleAuthorization",
                action: "getAllUsers",
                role_id: role_id
            },
            success: function (response) {
                var response = JSON.parse(response);

                $('#role_id').val(role_id);
                $('#properties_list').find("thead, tbody").remove();
                var table_data = '';
                table_data +='<thead>\n' +
                    '<tr class="table_header_values">\n' +
                    '<th class="table-heading">Property</th>';
                if(response.users != '') {
                    $.each(response.users, function (key, value) {
                        table_data += '<th>' + value.user_name + '</th>'
                    });
                }
                table_data +='</tr>' +
                    '</thead>'+
                    '<tbody>';
                $.each(response.properties, function(key, value) {
                    table_data += '<tr><td>'+value.property_name+'</td>';
                    if(response.users != '') {
                        $.each(response.users, function (key1, value1) {
                            table_data += '<td><input class="role_authorization_checkbox" type="checkbox" property_id="' + value.id + '" user_id="'+value1.id+'" role_id="' + value1.role + '" ></td>'
                        });
                    }
                    table_data +='</tr>';
                });
                table_data +='</tbody>';
                $('#properties_list').append(table_data);

                $("input:checkbox").attr('disabled', true);
            }
        });
    }
});

$('#editRolesBtn').on('click', function () {
    $("input:checkbox").attr('disabled', false);
   $('#save_role_athzation_btn_div').show();
});

$('#cancel_role_athzation_btn').on('click', function () {
    $("input:checkbox").prop('checked', false).attr('disabled', true);
    bootbox.confirm("Do you want to cancel this action now?", function (result) {
        if (result == true) {
            $('#save_role_athzation_btn_div').hide();
        }
    });
});
$("#userAuthorizationForm").validate({
    rules: {

    }, submitHandler: function (form) {
        var role_id = $('#role_id').val();
        var authorization_arr = [];
        $('input.role_authorization_checkbox:checkbox:checked').each(function () {
            authorization_arr.push({
                property_id: $(this).attr('property_id'),
                user_id: $(this).attr('user_id'),
                role_id: $(this).attr('role_id')
            });
        });
        console.log('authorization_arr>>>', authorization_arr);
        if(authorization_arr.length <= 0){
            toastr.warning('No record selected.');
        } else {
            $.ajax({
                type: 'post',
                url: '/RoleAuthorization-Ajax',
                data: {
                    class: "roleAuthorization",
                    action: "insert",
                    authorization_arr: authorization_arr,
                    role_id: role_id
                },
                success: function (response) {
                    var response = JSON.parse(response);

                    $("input:checkbox").attr('disabled', true);
                    $('#save_role_athzation_btn_div').hide();

                    console.log('res>>>', response); return;

                    $('#properties_list').find("thead, tbody").remove();
                    var table_data = '';
                    table_data +='<thead>\n' +
                        '<tr class="table_header_values">\n' +
                        '<th class="table-heading">Property</th>';
                    if(response.users != '') {
                        $.each(response.users, function (key, value) {
                            table_data += '<th>' + value.user_name + '</th>'
                        });
                    }
                    table_data +='</tr>' +
                        '</thead>'+
                        '<tbody>';
                    $.each(response.properties, function(key, value) {
                        table_data += '<tr><td>'+value.property_name+'</td>';
                        if(response.users != '') {
                            $.each(response.users, function (key1, value1) {
                                table_data += '<td><input class="role_authorization_checkbox" type="checkbox" property_id="' + value.id + '" user_id="'+value1.id+'" role_id="' + value1.role + '" ></td>'
                            });
                        }
                        table_data +='</tr>';
                    });
                    table_data +='</tbody>';
                    $('#properties_list').append(table_data);

                    $("input:checkbox").attr('disabled', true);
                }
            });
        }
    }
});




