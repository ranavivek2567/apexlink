$(document).ready(function (){

    /** Show add new unit type div on add new button click */
    var thisYear = (new Date()).getFullYear();
    $(document).on('click','#addVehicleButton',function () {
        var maxOffset = 50;

        $('#year_of_vehicle').empty();
        for (var i = 0; i <= maxOffset; i++) {
            var year = thisYear - i;
            $("#year_of_vehicle").append('<option value="' +year + '">' + year + '</option>');
        }
        $("#add_vehicle_type").trigger("reset");
        headerDiv.innerText = "Add New Vehicle";
        var vehicle = randomNumberString(5);
        $('#vehicle').val(vehicle);
        $('#saveBtn').val('Save');
        $('#vehicle_edit_id').val('');
        $('#amountErr').text('');
        $('#amount-error').text('');
        $('#starting_mileageErr').text('');
        $('#starting_mileage-error').text('');
        $('#add_vehicle_div').show(500);
    });
    /** Hide add new unit type div on cancel button click */
    $(document).on("click", "#add_vehicle_cancel_btn", function (e) {
        e.preventDefault();
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                $("#add_vehicle_div").hide(500);
            } else {
            }
        });
    });
    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All')
    function jqGrid(status, deleted_at) {
        var table = 'company_manage_vehicle';
        var columns = ['Vehicle Name','Vehicle #', 'Vehicle Type', 'Make', 'Model','VIN #','Year','Date Purchased','Vehicle Prize('+default_currency_symbol+')','Action'];
        var select_column = ['Edit','Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = [];
        var pagination;
        var columns_options = [

            {name:'Vehicle Name',index:'vehicle_name', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Vehicle #',index:'vehicle', width:100,searchoptions: {sopt: conditions},table:table},
            {name:'Vehicle Type',index:'vehicle_type', width:80,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Make',index:'make', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Model',index:'model', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'VIN #',index:'vin', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Year',index:'year_of_vehicle', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Date Purchased',index:'date_purchased', width:80, align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Vehicle Prize('+default_currency_symbol+')',index:'amount', width:80, align:"center",searchoptions: {sopt: conditions},table:table, formatter:ammountFormatter },
            {name:'Action',index:'', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},

        ];
        var ignore_array = [];
        jQuery("#manageVehicle-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: "company_manage_vehicle",
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns,
                deleted_at:deleted_at
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Vehicles",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:200,left:200,drag:true,resize:false} // search options
        );
    }

    /**
     * jqGrid function to currency status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function ammountFormatter (cellValue, options, rowObject){
        if (cellValue != '')
            return default_currency_symbol+cellValue;
        else
            return '';
    }
    /*  insert into vehicle type*/

    $("#add_vehicle_type").validate({
        rules: {
            starting_mileage: {
                maxlength: 30,
                number:true
            },
            amount: {
                maxlength: 30,
                number:true
            },
        },
        messages: {
            starting_mileage: {
                number:'* Please add numerical values only'
            },
            amount: {
                number:'* Please add numerical values only'
            }
        }
    });
    $(document).on("click","#saveBtn", function(e) {
        e.preventDefault();
        if ($('#add_vehicle_type').valid()) {
            var formdata = $('#add_vehicle_type').serializeArray();
            var vehicle_edit_id = $('#vehicle_edit_id').val();

            if (vehicle_edit_id){
                var action =  'update'
            } else {
                var action = 'insert'
            }

            $.ajax({
                type: 'post',
                url: '/manageVehicle-Ajax',
                data: {
                    class: 'ManageVehicleAjax',
                    action: action,
                    form: formdata
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == 'success' && response.code == 200) {

                        $('#manageVehicle-table').trigger('reloadGrid');
                        $("#add_vehicle_type").trigger("reset");
                        $("#add_vehicle_div").hide(500);
                        toastr.success(response.message);
                        onTop(true);
                    }else if (response.status == 'error' && response.code == 500) {
                       toastr.error(response.message);
                    } else if (response.status == 'error' && response.code == 400) {
                        console.log('errors>>', response);
                        $.each(response.data, function (key, value) {
                            $('#' + key).text(value);
                        });
                    }
                },
                error: function (data) {
                }
            });
        }
    });

    function getVehicleDetail(id){
        $.ajax({
            type: 'post',
            url: '/manageVehicle-Ajax',
            data: {
                class: "ManageVehicleAjax",
                action: "view",
                id: id
            },
            success: function (response) {

                var response = JSON.parse(response);
                headerDiv.innerText = "Edit Vehicle";
                $('#saveBtn').val('Update');
                $.each(response, function (key, value) {
                    $('#' + key ).val(value);
                });
                $('#vehicle_edit_id').val(response.id);
                $('#year_of_vehicle').empty();
                for (var i = 0; i <= 50; i++) {
                    var year = thisYear - i;
                    if(response.year_of_vehicle == year){
                        $("#year_of_vehicle").append('<option value="' +year + '" selected>' + year + '</option>');
                    } else {
                        $("#year_of_vehicle").append('<option value="' +year + '">' + year + '</option>');
                    }
                }
                defaultFormData = $('#add_vehicle_type').serializeArray();
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("change", "#manageVehicle-table .select_options", function (e) {
        e.preventDefault();
        var action = this.value;
        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        setTimeout(function(){ $(".select_options").val("default"); }, 200);

        $('.table').find('.green_row_left, .green_row_right').each(function(){
            $(this).removeClass("green_row_left green_row_right");
        });
        $('.table').find('tr:eq('+row_num+')').find('td:eq(0)').addClass("green_row_left");
        $('.table').find('tr:eq('+row_num+')').find('td').last().addClass("green_row_right");


        switch(action) {

            case "Edit":
                $(".clear-btn").text('Reset');
                $(".clear-btn").addClass('formreset');
                $(".clear-btn").removeClass('clearFormReset');
                getVehicleDetail(id);
                $("#add_vehicle_div").show();
                break;

            case "Delete":
                bootbox.confirm({
                    message: "Do you want to delete this record ?",
                    buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                    callback: function (result) {
                        if (result == true) {
                            $.ajax({
                                type: 'post',
                                url: '/manageVehicle-Ajax',
                                data: {
                                    class: "ManageVehicleAjax",
                                    action: "delete",
                                    id: id
                                },
                                success: function (response) {
                                    var response = JSON.parse(response);
                                    if (response.status == 'success' && response.code == 200) {
                                        toastr.success(response.message);
                                    } else {
                                        toastr.error(response.message);
                                    }
                                }
                            });
                        }
                        $('#manageVehicle-table').trigger('reloadGrid');
                    }
                });

                break;
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }

        return false;
    });
});