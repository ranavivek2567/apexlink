$(document).ready(function () {

    /*jqGrid status*/
    $('#jqGridStatus').on('change', function () {
        var selected = this.value;
        var deleted_at = true;
        $('#property-table').jqGrid('GridUnload');
        if (selected == 4)
            deleted_at = false;
        jqGrid(selected);
    });





    /**
     * jqGrid Initialization function
     * @param status
     */
    jqGrid('All');
    function jqGrid(status) {
        var table = 'property_inspection';
        var columns = ['Serial Number', 'Date of Inspection', 'Property Name', 'Property Address', 'Building','ObjectID', 'Unit Number', 'Inspection Name', 'Inspection Type', 'Action'];
        var select_column = ['Edit','View Details','Delete'];
        var joins = [];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_columns = ['property_inspection.deleted_at','property_inspection.updated_at', 'property_inspection.object_id', 'property_inspection.object_type', 'property_inspection.notes', 'property_inspection.inspection_area', 'property_inspection.custom_field', 'property_inspection.building_address'];


        var columns_options = [
            {name: 'Serial Number', index: 'id', width: 90, align: "left", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Date of Inspection', index: 'inspection_date', width: 100, searchoptions: {sopt: conditions}, table: table, change_type:'date', classes: 'pointer'},
            {name: 'Property Name', index: 'property_name', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter: propertyName, classes: 'pointer'},
            {name: 'Property Address', index: 'property_address', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Building', index: 'building_name', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'ObjectID', index: 'object_id', hidden:true,width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Unit Number', index: 'unit_name', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer'},
            {name: 'Inspection Name', index: 'inspection_name', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, classes: 'pointer',formatter:statusFormatter},
            {name: 'Inspection Type', index: 'inspection_type', width: 80, align: "center", searchoptions: {sopt: conditions}, table: table, formatter:isDefaultFormatter, classes: 'pointer'},
            {name: 'Action', index: 'select', title: false, width: 80, align: "right", sortable: false, cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select', search: false, table: table,formatter:actionFormatterObject}
        ];
        var ignore_array = [];
        jQuery("#inspection-table").jqGrid({
            url: '/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns: extra_columns
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "List of Inspections",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, /* edit options*/
            {}, /* add options*/
            {}, /*del options*/
            {top: 200, left: 400, drag: true, resize: false} /* search options*/
        );
    }

    jQuery("#chngroup").change(function(){
        var vl = $(this).val();
        if(vl) {
            if(vl == "clear") {
                jQuery("#inspection-table").jqGrid('groupingRemove',true);
            } else {
                jQuery("#inspection-table").jqGrid('groupingGroupBy',vl);
            }
        }
    });
$(document).on('change','.property_list_inspections',function () {
    var id=$(this).val();
    propfilter(id);
});

$(document).on('change','.building_list_inspections',function () {
    var id=$(this).val();
    buildingfilter(id);
});
    function propfilter(id)
    {
        var grid = $("#inspection-table"),f = [];
        var property_id = $('.property_list_inspections :selected').text();
        if(property_id !=='Select'){
        f.push({field: "property_inspection.property_name", op: "eq", data: property_id});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
        else{
            property_id="all";
            f.push({field: "property_inspection.property_name", op: "eq", data: property_id});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    }
    function buildingfilter(id)
    {
        var grid = $("#inspection-table"),f = [];
        var property_id = $('.building_list_inspections :selected').text();
        if(property_id !=='Select'){
        f.push({field: "property_inspection.building_name", op: "eq", data: property_id});
        grid[0].p.search = true;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
        else{
            property_id="all";
            f.push({field: "property_inspection.building_name", op: "eq", data: property_id});
            grid[0].p.search = true;
            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
            grid.trigger("reloadGrid",[{page:1,current:true}]);
        }
    }

    $(document).on('click', '#inspection-table tr td:not(:last-child)', function () {
        var id = $(this).closest('tr').attr('id');
        window.location.href = '/Inspection/InspectionView?id=' + id;
    });
    /**
     *  function to format property name
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function propertyName(cellValue, options, rowObject) {
        if(rowObject !== undefined) {
            var flagValue = $(rowObject.Action).attr('flag');
            var id = $(rowObject.Action).attr('data_id');
            var flag = '';
            if (flagValue == 'yes') {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '<a id="flag" href="javascript:void(0);" data_url="/Property/PropertyView?id="' + id + '"><img src="'+upload_url+'/company/images/Flag.png"></a></span>';
            } else {
                return '<span style="color:#05A0E4;text-decoration:underline;font-weight: bold;">' + cellValue + '</span>';
            }
        }

    }


    /**
     * jqGrid function to format status
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function statusFormatter(cellValue, options, rowObject) {
        if (cellValue == 1)
            return "Move In Inspection";
        else if (cellValue == '2')
            return "Move Out Inspection";
        else if (cellValue == '3')
            return "Weekly Inspection";
        else if (cellValue == '4')
            return "Monthly Inspection";
        else if (cellValue == '5')
            return "Six-Month Inspection";
        else if (cellValue == '6')
            return "Annual Inspection";
        else if (cellValue == '7')
            return "General Inspection";
        else if (cellValue == '8')
            return "Emergency Inspection";
        else if (cellValue == '9')
            return "Fd";
        else if (cellValue == '10')
            return "Other";
        else
            return '';
    }

    /**
     * jqGrid function to format is_default column
     * @param cellValue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function isDefaultFormatter(cellValue, options, rowObject) {
        if (cellValue == '1')
            return "Exterior";
        else if (cellValue == '2')
            return "Interior";
        else
            return '';
    }

    function actionFormatterObject(cellValue, options, rowObject){
        if(rowObject !== undefined){
            setTimeout(function(){ $('#inspection-table .select_options').attr('object_id',rowObject.ObjectID); }, 500);
            return cellValue;
        }

    }

    /*Get Property/Building Details Name List*/
    PropertyBuildingDetails();
    function PropertyBuildingDetails(){
        $.ajax({
            type: 'POST',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail",
                action: "PropertyBuildingDetails"
            },
            success: function (response) {
                var html="";
                var response = JSON.parse(response);
                for (var i = 0; i < response.data.propertyDetails.length; i++) {
                    html += ' <option value="' + response.data.propertyDetails[i].id + '" data-id="' + response.data.propertyDetails[i].id + '">' + response.data.propertyDetails[i].property_name + '</option>';
                }
                $("#tickets-inspection #property_list_inspection").html("<option value=''>Select</option>" + html);
                $("#selectNewPopup #property_list_inspection").html("<option value=''>Select</option>" + html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("change","#tickets-inspection #property_list_inspection",function(){
        var conceptName = $('#tickets-inspection #property_list_inspection').find(":selected").text();
        var conceptId = $('#tickets-inspection #property_list_inspection').find(":selected").attr("data-id");
        getPropName(conceptName,conceptId);
    });



    function getPropName(conceptName,conceptId) {
        var property_id = conceptId;

        $.ajax({
            type: 'POST',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail",
                action: "getInspectionBuildingDetails",
                id:property_id
            },
            success: function (response) {
                var html = ""
                var response = JSON.parse(response);
                if(response.data.length > 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        html += ' <option value="' + response.data[i].id + '" data-id="' + response.data[i].id + '">' + response.data[i].building_name + '</option>';
                    }
                    $("#tickets-inspection #building_list_inspection").html("<option value=''>Select</option>" + html);
                }else {
                    $("#tickets-inspection #building_list_inspection").html("<option value=''>This Property has no Building</option>");
                    $("#tickets-inspection #unit_list_inspection").html("<option value=''>This Building has no Unit</option>");
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("change","#tickets-inspection #building_list_inspection",function(){
        var conceptNameBuilding = $('#tickets-inspection #building_list_inspection').find(":selected").text();
        var conceptIdBuilding = $('#tickets-inspection #building_list_inspection').find(":selected").attr("data-id");

        getBuildingPropName(conceptNameBuilding,conceptIdBuilding);
    });

    function getBuildingPropName(conceptNameBuilding,conceptIdBuilding) {
        var building_id = conceptIdBuilding;
        $.ajax({
            type: 'POST',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail",
                action: "getInspectionUnitDetails",
                id:building_id
            },
            success: function (response) {
                var html = ""
                var response = JSON.parse(response);
                if(response.data.length > 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        html += ' <option value="' + response.data[i].id + '">' + response.data[i].unit_no + '</option>';
                    }
                    $("#tickets-inspection #unit_list_inspection").html("<option value=''>Select</option>" + html);
                }else {
                    $("#tickets-inspection #unit_list_inspection").html("<option value=''>This Building has no Unit</option>");
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /*get selected Id from property in add new inspection*/
    $(document).on("change","#selectNewPopup #property_list_inspection",function(){
        var conceptName1 = $('#selectNewPopup #property_list_inspection').find(":selected").text();
        var conceptId1 = $('#selectNewPopup #property_list_inspection').find(":selected").attr("data-id");
        getPropName1(conceptName1,conceptId1);
    });


    function getPropName1(conceptName1,conceptId1) {
        var property_id = conceptId1;

        $.ajax({
            type: 'POST',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail",
                action: "getInspectionBuildingDetails1",
                id:property_id
            },
            success: function (response) {
                var html = ""
                var response = JSON.parse(response);
                if(response.data.length > 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        html += ' <option value="' + response.data[i].id + '" data-id="' + response.data[i].id + '">' + response.data[i].building_name + '</option>';
                    }
                    $("#selectNewPopup #building_list_inspection").html("<option value=''>Select</option>" + html);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).on("change","#selectNewPopup #building_list_inspection",function(){
        var conceptNameBuilding1 = $('#selectNewPopup #building_list_inspection').find(":selected").text();
        var conceptIdBuilding1 = $('#selectNewPopup #building_list_inspection').find(":selected").attr("data-id");
        getBuildingPropName1(conceptNameBuilding1,conceptIdBuilding1);
    });

    function getBuildingPropName1(conceptNameBuilding1,conceptIdBuilding1) {
        var building_id = conceptIdBuilding1;
        $.ajax({
            type: 'POST',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail",
                action: "getInspectionUnitDetails1",
                id:building_id
            },
            success: function (response) {
                var html = ""
                var response = JSON.parse(response);
                if(response.data.length > 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        html += ' <option value="' + response.data[i].id + '">' + response.data[i].unit_no + '</option>';
                    }
                    $("#selectNewPopup #unit_list_inspection").html("<option value=''>Select</option>" + html);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $("#selectNewPopup #addNewInspection").validate({
        rules: {
            'property_list_inspection': {required: true},
            'building_list_inspection': {required: true},
            'unit_list_inspection': {required: true},
        }
    });

    $(document).on("click","#selectNewPopup #saveCustomField",function(){
        if($('#addNewInspection').valid()){}
    });



    /*get selected Id from property in add new inspection click*/
    $(document).on("change","#selectNewPopup #property_list_inspection",function(){
        var conceptNameValue = $('#selectNewPopup #property_list_inspection').find(":selected").val();
        getPropNameId(conceptNameValue);
    });

    function getPropNameId(conceptNameValue) {
        $(document).on("click","#selectNewPopup #saveCustomField",function(){
            if($('#addNewInspection').valid()){
                window.location.href =  window.location.origin + "/Property/PropertyInspection?id="+ conceptNameValue;
            }
        });
    }
    /*get selected Id from property in add new inspection click*/

    /*get selected Id from building in add new inspection click*/
    $(document).on("change","#selectNewPopup #building_list_inspection",function(){
        var conceptNameValueBuilding = $('#selectNewPopup #building_list_inspection').find(":selected").val();
        getBuildNameId(conceptNameValueBuilding);
    });

    function getBuildNameId(conceptNameValueBuilding) {
        $(document).on("click","#selectNewPopup #saveCustomField",function(){
            if($('#addNewInspection').valid()) {
                window.location.href = window.location.origin + "/Building/BuildingInspection?id=" + conceptNameValueBuilding;
            }

        });
    }
    /*get selected Id from building in add new inspection click*/

    /*get selected Id from building in add new inspection click*/
    $(document).on("change","#selectNewPopup #unit_list_inspection",function(){
        var conceptNameValueUnit = $('#selectNewPopup #unit_list_inspection').find(":selected").val();
        getUnitNameId(conceptNameValueUnit);
    });

    function getUnitNameId(conceptNameValueUnit) {
        $(document).on("click","#selectNewPopup #saveCustomField",function(){
            localStorage.setItem("inspection_value", "edit_inspection");
            if($('#addNewInspection').valid()) {
                window.location.href = window.location.origin + "/Unit/UnitInspection?id=" + conceptNameValueUnit;
            }

        });
    }
    /*get selected Id from building in add new inspection click*/

/*Delete row data from select option*/
    $(document).on('change', '#inspection-table .select_options', function() {

        var opt = $(this).val();

        var id = $(this).attr('data_id');
        var row_num = $(this).parent().parent().index() ;
        if(opt == 'View Details' || opt == 'VIEW DETAILS'){
            localStorage.setItem("inspection_value", "edit_inspection");
            window.location.href = '/Inspection/InspectionView?id='+id;
        } else if (opt == 'Delete' || opt == 'DELETE') {
            opt = opt.toLowerCase();
            bootbox.confirm({
                message: "Do you want to delete this record ?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $.ajax({
                            type: 'post',
                            url: '/Building/AddBuildingDetail',
                            data: {class: 'buildingDetail', action: "deleteDataInspection", id: id},
                            success: function (response) {
                                var response = JSON.parse(response);
                                if (response.status == 'success' && response.code == 200) {
                                    toastr.success(response.message);
                                } else {
                                    toastr.error(response.message);
                                }
                            }
                        });
                    }
                    $('#inspection-table').trigger('reloadGrid');
                }
            });
        }else if(opt == 'Edit' || opt == 'edit'){
            localStorage.setItem("inspection_value", "edit_inspection");
            window.location.href = '/Building/BuildingInspection?id='+id;
        }
    });





    /**
     * jqGrid Intialization function
     * @param status
     */
    jqGridPhotovideos('All');
    function jqGridPhotovideos(status) {
        var table = 'inspection_photos';
        var columns = ['Name','Preview','Action'];
        var select_column = ['Delete'];
        var joins = [];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['inspection_photos.module_type'];
        var extra_where = [];
        var columns_options = [
            {name:'Name',index:'file_name',width:400,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Preview',index:'file_location',width:450,align:"center",searchoptions: {sopt: conditions},search:false,table:table,formatter:photoFormatter},
            {name:'Action',index:'select',width:430,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, hidden:true,formatter: 'select', edittype: 'select',search:false,table:table,title:false}
        ];
        var ignore_array = [];
        jQuery("#inspectionPhotos-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            sortname: 'updated_at',
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore: ignore_array,
                joins: joins,
                extra_columns:extra_columns,
                extra_where:extra_where
            },
            viewrecords: true,
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: pagination,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Photos/Virtual Tour Videos",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {},
            {},
            {},
            {top:10,left:200,drag:true,resize:false}
        );
    }


    /**
     * function to format photos and videos
     * @param cellvalue
     * @param options
     * @param rowObject
     * @returns {string}
     */
    function photoFormatter(cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var path = upload_url+'company/'+rowObject.Preview;
            var appendType = '<a href="' + path + '" target="_blank"><img width=100 height=100 src="' + path + '"></a>';

            return appendType;
        }
    }

    getInspectionListView();

    function getInspectionListView() {
        var id = $(".main-content #inspection_uniqueId").val();
        $.ajax({
            type: 'POST',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail",
                action: "getInspectionListView",
                id:id
            },
            success: function (response) {
                var response = JSON.parse(response);
                var id_insp = response.data.id;
                if (response.code == 200) {
                    $('.apx-adformbox-content .view-outer #DateOfInspection').html(response.data.inspection_date);
                    $('.apx-adformbox-content .view-outer #propertyAddressInspection').html(response.data.property_address);



                    if (response.data.inspection_name == '1'){
                        var inspection_name = "Move In Inspection";
                    }else if (response.data.inspection_name == '2'){
                        var inspection_name= "Move Out Inspection";
                    }else if (response.data.inspection_name == '3'){
                        var inspection_name= "Weekly Inspection";
                    }else if (response.data.inspection_name == '4'){
                        var inspection_name= "Monthly Inspection";
                    }else if (response.data.inspection_name == '5'){
                        var inspection_name= "Six-Month Inspection";
                    }else if (response.data.inspection_name == '6'){
                        var inspection_name= "Annual Inspection";
                    }else if (response.data.inspection_name == '7'){
                        var inspection_name= "General Inspection";
                    }else if (response.data.inspection_name == '8'){
                        var inspection_name= "Emergency Inspection";
                    }else if (response.data.inspection_name == '9'){
                        var inspection_name= "Fd";
                    }else if (response.data.inspection_name == '10'){
                        var inspection_name= "Others";
                    }

                    $('.apx-adformbox-content .view-outer #nameInspection').html(inspection_name);

                    if (response.data.inspection_type == '1'){
                        var inspection_type = "Exterior";
                    }else if (response.data.inspection_type == '2'){
                        var inspection_type= "Interior";
                    }
                    $('.apx-adformbox-content .view-outer #TypeInspection').html(inspection_type);
                    $('.apx-adformbox-content .view-outer #propertyNameInspection').html(response.data.property_name);
                    $('.apx-adformbox-content .view-outer #buildingInspection').html(response.data.building_name);
                    $('.apx-adformbox-content .view-outer #numberOfUnitInspection').html(response.data.unit_name);
                    $('.apx-adformbox-content .view-outer #notesInspection').html(response.data.notes);
                    if (response.data.building_name == 'N/A' && response.data.unit_name == 'N/A' ) {

                        var url = window.location.origin + "/Property/PropertyInspection?id="+id_insp;

                        $("#viewInspectionList .edit-outer").html('<a class="editInspectionLink" href="'+url+'">' +
                            '                                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i>\n' +
                            '                                                Edit</a>');

                    }else if(response.data.unit_name == 'N/A' ) {
                        var url = window.location.origin + "/Building/BuildingInspection?id="+id_insp;

                        $("#viewInspectionList .edit-outer").html('<a class="editInspectionLink" href="'+url+'">' +
                            '<img width="12" height="12" src="'+upload_url + 'company/images/edit-icon.png" alt="" />\n' +
                            '                                                Edit</a>');
                    }else{
                        var url = window.location.origin + "/Unit/UnitInspection?id="+id_insp;


                        $("#viewInspectionList .edit-outer").html('<a class="editInspectionLink" href="'+url+'">' +
                            '<img width="12" height="12" src="'+upload_url + 'company/images/edit-icon.png" alt="" />\n' +
                            '                                                Edit</a>');
                    }
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    getInspectionCustomFeildView();
    function getInspectionCustomFeildView() {
        var id = $(".main-content #inspection_uniqueId").val();
        $.ajax({
            type: 'POST',
            url: '/Building/AddBuildingDetail',
            data: {
                class: "buildingDetail", action: "getInspectionCustomFeildView", id: id},
            success: function (response) {
                var response = JSON.parse(response);
                if (response.code == 200) {
                    getCustomFieldForViewMode(response.data);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    /**
     * function to show custom field
     * @param editCustomFieldData
     */
    function getCustomFieldForViewMode(editCustomFieldData){
        $.ajax({
            type: 'get',
            url: '/CustomField/get',
            success: function (response) {
                var response = JSON.parse(response);
                var custom_array = [];
                if(response.code == 200 && response.message == 'Record retrieved successfully'){
                    $('.custom_field_html_view_mode').html('');
                    $('.custom_field_msg').html('');
                    var html ='';
                    $.each(response.data, function (key, value) {
                        var default_val = '';
                        if(editCustomFieldData) {
                            default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                            $.each(editCustomFieldData, function (key1, value1) {
                                if (value1.id == value.id) {
                                    default_val = value1.value;
                                }
                            });
                        } else {
                            default_val = (value.default_value != '' && value.default_value != null) ? value.default_value : '';
                        }
                        var required = (value.is_required == '1')?' <em class="red-star">*</em>':'';
                        var name = value.field_name.replace(/ /g, "_");
                        var currency_class = (value.data_type == 'currency')?' amount':'';
                        if(value.data_type == 'date') {
                            custom_array.push(name);
                        }
                        var readonly = 'readonly';
                        html += '<div class="row custom_field_class">';
                        html += '<div class=col-sm-6>';
                        html += '<label>'+value.field_name+required+'</label>';
                        html += '<input type="text" readonly data_type="'+value.data_type+'" data_value="'+value.default_value+'" '+readonly+' data_id="'+value.id+'" data_required="'+value.is_required+'" name="'+name+'" id="'+name+'" value="'+default_val+'" class="form-control changeCustom add-input'+currency_class+'">';

                        if(value.is_editable == 1 || value.is_deletable == 1) {
                            if (value.is_editable == 1) {
                                html += '<span id="' + value.id + '" class="editCustomField"><i class="fa fa-edit"></i></span>';
                            }
                            if (value.is_deletable == 1) {
                                html += '<span id="' + value.id + '" class="deleteCustomField"><i class="fa fa-times-circle""></i></span>';
                            }
                        }
                        html += '<span class="customError required"></span>';
                        html += '</div>';
                        html += '</div>';

                    });
                    $(html).prependTo('.custom_field_html_view_mode');
                } else {
                    $('.custom_field_msg').html('No Custom Fields');
                    $('.custom_field_html_view_mode').html('');
                }
                triggerDatepicker(custom_array)
            },
            error: function (data) {
            }
        });
    }

    /**
     * function to trigger datepicker for custom fields
     * @param custom_array
     */
    function triggerDatepicker(custom_array){
        $.each(custom_array, function (key, value) {
            $('#customField'+value).datepicker({
                dateFormat: datepicker
            });
        });
    }









});