$(document).on('change','#open_work_order #ow_order_portfolio_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'fetchModalBalanceDetailOnchange',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#open_work_order #ow_order_property_id").multiselect("destroy");
            $('#open_work_order #ow_order_property_id').html(data.property_ddl);
            setTimeout(function(){
                $('#open_work_order #ow_order_property_id').trigger('change');
            },1000);
                $('#open_work_order #ow_order_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});



$(document).on('change','#open_work_order #ow_order_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangePropertyTenant',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#open_work_order #ow_order_tenant_id").multiselect("destroy");
            $('#open_work_order #ow_order_tenant_id').html(data.tenant_ddl);
            $('#open_work_order #ow_order_tenant_id').multiselect({
                includeSelectAllOption: true,
                allSelectedText: 'All Selected',
                enableFiltering: true,
                nonSelectedText: 'Select'
            }).multiselect('selectAll', false).multiselect('updateButtonText');  
            // console.log(data.tenant_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});