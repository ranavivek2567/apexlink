$(document).on('change','#owner_statement #owner_statement_portfolio_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'fetchModalBalanceDetailOnchange',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#owner_statement #owner_statement_property_id").multiselect("destroy");
            $('#owner_statement #owner_statement_property_id').html(data.property_ddl);
            setTimeout(function(){
                $('#owner_statement #owner_statement_property_id').trigger('change');
            },1000);
                $('#owner_statement #owner_statement_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});


$(document).on('change','#owner_statement #owner_statement_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onChangePropertyOwner',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#owner_statement #owner_statement_owner_name").multiselect("destroy");
            $('#owner_statement #owner_statement_owner_name').html(data.owner_ddl);
                $('#owner_statement #owner_statement_owner_name').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});


$(document).on('change','#owner_statement #owner_statement_status',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onChangeStatusOwner',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            // console.log(data.owner_ddl);
            $("#owner_statement #owner_statement_owner_name").multiselect("destroy");
            $('#owner_statement #owner_statement_owner_name').html(data.owner_ddl);
                $('#owner_statement #owner_statement_owner_name').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});