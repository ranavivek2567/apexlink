$(document).ready(function () {
//    add();
   $(document).on("click",".reports-outer .reports-column ul li",function(){
        var val = $(this).find("a").text();
            val = val.toLowerCase();
            val = val.replace(/ /g, "-");
            val=val.trim();
            // if(val=='balance-sheet'){
            //     var html ='<div class="modal fade" id="backGroundCheckPop" role="dialog"><div class="modal-dialog modal-md"><div class="modal-content  pull-left" style="width: 100%;"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title text-center">Disclaimer</h4></div><div class="modal-body"><div class="col-sm-12"><p style="font-size: 14px; line-height: 20px;"> I authorize ApexLink Inc. to submit this request to generate and present a background/any check report for the Person/Company I have listed. I understand that ApexLink Inc. is not responsible or liable for any data breach, loss/loss of profits or any claim against the filer (Myself or my company) by any party. I understand that in no event is ApexLink Inc. liable for any and all damages. I also understand that ApexLink Inc. grants no implied warranties, including without limitation, warranties, of merchantability, or of fitness for any particular purpose. </p></div><div class="col-sm-12"><input type="checkbox" name="" class="background_check_open"><label style="display: inline;"> Check to proceed further.</label></div><div class="col-sm-12 btn-outer mg-btm-20"><input type="button" class="blue-btn"  data-dismiss="modal" value="Cancel"></div></div></div></div></div>';
            // $(".bread-search-outer").html(html);
            // $('#backGroundCheckPop').modal('show');

            // }
            // console.log(val);

            switch(val){
                case("balance-sheet"):
                    $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'commonReportsFunction',
                            action: 'fetchModalData'
                        
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                $('#balance-sheet #bl_sheet_portfolio_id').append(data.portfolio_ddl);
                                $('#balance-sheet #bl_sheet_portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                                $('#balance-sheet #bl_sheet_property_id').append(data.property_ddl);
                                $('#balance-sheet #bl_sheet_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#balance-sheet').modal('show');
                break; 
                       
                //2nd Case
                case("balance-sheet-comparison(2-years)"):
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchModalData'
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#bl-comparison-modal #bl_comparison_portfolio_id').append(data.portfolio_ddl);
                            $('#bl-comparison-modal #bl_comparison_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');

                            $('#bl-comparison-modal #bl_comparison_property_id').append(data.property_ddl);
                            $('#bl-comparison-modal #bl_comparison_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                    $('#bl-comparison-modal').modal('show');
                break;

                // 3rd Case Income Statement Details
                case("income-statement-details"):
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchModalData'
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#income_statement_detail #is_detail_portfolio_id').append(data.portfolio_ddl);
                            $('#income_statement_detail #is_detail_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');

                            $('#income_statement_detail #is_detail_property_id').append(data.property_ddl);
                            $('#income_statement_detail #is_detail_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                    $('#income_statement_detail').modal('show');
                break;

                //Case 4
                case("income-statement-comparison-(2-years)"):
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchModalData'
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#income_statement_comparison #is_comparison_portfolio_id').append(data.portfolio_ddl);
                            $('#income_statement_comparison #is_comparison_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#income_statement_comparison #is_comparison_property_id').append(data.property_ddl);
                            $('#income_statement_comparison #is_comparison_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                    $('#income_statement_comparison').modal('show');
                break;

                //Case 5
                case("profit-&-loss"):
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchModalData'
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#profit_loss #profit_loss_portfolio_id').append(data.portfolio_ddl);
                            $('#profit_loss #profit_loss_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#profit_loss #profit_loss_property_id').append(data.property_ddl);
                            $('#profit_loss #profit_loss_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                    $('#profit_loss').modal('show');
                break;

                //Case 6
                case("profit-&-loss-detail"):
                $.ajax({
                    type: "POST",
                    url: "/CommonReportsModal-Ajax",
                    data:{
                        class: "commonReportsFunction",
                        action : "fetchModalData"
                    },
                    success : ( response )=>{
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#profit_loss_detail #pl_detail_portfolio_id').append(data.portfolio_ddl);
                            $('#profit_loss_detail #pl_detail_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#profit_loss_detail #pl_detail_property_id').append(data.property_ddl);
                            $('#profit_loss_detail #pl_detail_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#profit_loss_detail').modal('show');
                break;

                //Case 7
                case("profit-&-loss---12-months"):
                $.ajax({
                    type: "POST",
                    url: "/CommonReportsModal-Ajax",
                    data:{
                        class: "commonReportsFunction",
                        action : "fetchModalData"
                    },
                    success : ( response )=>{
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#profit_loss_12months #pl_12months_portfolio_id').append(data.portfolio_ddl);
                            $('#profit_loss_12months #pl_12months_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#profit_loss_12months #pl_12months_property_id').append(data.property_ddl);
                            $('#profit_loss_12months #pl_12months_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#profit_loss_12months').modal('show');
                break;

                //Case 8
                case("profit-&-loss-comparison-(2-years)"):
                $.ajax({
                    type: "POST",
                    url: "/CommonReportsModal-Ajax",
                    data:{
                        class: "commonReportsFunction",
                        action : "fetchModalData"
                    },
                    success : ( response )=>{
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#profit_loss_2years #pl_2years_portfolio_id').append(data.portfolio_ddl);
                            $('#profit_loss_2years #pl_2years_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#profit_loss_2years #pl_2years_property_id').append(data.property_ddl);
                            $('#profit_loss_2years #pl_2years_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#profit_loss_2years').modal('show');
                break;

                //Case 9
                case("profit-&-loss-consolidated"):
                $.ajax({
                    type: "POST",
                    url: "/CommonReportsModal-Ajax",
                    data:{
                        class: "commonReportsFunction",
                        action : "fetchModalData"
                    },
                    success : ( response )=>{
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#profit_loss_consolidated #pl_consolidated_portfolio_id').append(data.portfolio_ddl);
                            $('#profit_loss_consolidated #pl_consolidated_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#profit_loss_consolidated #pl_consolidated_property_id').append(data.property_ddl);
                            $('#profit_loss_consolidated #pl_consolidated_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#profit_loss_consolidated').modal('show');
                break;

    //<----AR AGING    
                //Case 10
                case("a/r-aging-summary"):
                // alert("Working");
                // $.ajax({
                //     type: "POST",
                //     url: "/CommonReportsModal-Ajax",
                //     data:{
                //         class: "commonReportsFunction",
                //         action : "fetchModalData"
                //     },
                //     success : ( response )=>{
                //         var data = $.parseJSON(response);
                //         if (data.status == "success") {
                //             // console.log('reportPop>>>>>>',data.portfolio_ddl);
                //             $('#ar_aging_summary #ar_aging_summary_portfolio_id').append(data.portfolio_ddl);
                //             $('#ar_aging_summary #ar_aging_summary_portfolio_id').multiselect({
                //                 includeSelectAllOption: true,
                //                 allSelectedText: 'All Selected',
                //                 enableFiltering: true,
                //                 nonSelectedText: 'Select'
                //             }).multiselect('selectAll', false).multiselect('updateButtonText');
                //             $('#ar_aging_summary #ar_aging_summary_property_id').append(data.property_ddl);
                //             $('#ar_aging_summary #ar_aging_summary_property_id').multiselect({
                //                 includeSelectAllOption: true,
                //                 allSelectedText: 'All Selected',
                //                 enableFiltering: true,
                //                 nonSelectedText: 'Select'
                //             }).multiselect('selectAll', false).multiselect('updateButtonText');
                //         } else {
                //             toastr.error(data.message);
                //         }
                //     },
                //     error: function (data) {
                //         var errors = $.parseJSON(data.responseText);
                //         $.each(errors, function (key, value) {
                //             $('#' + key + '_err').text(value);
                //         });
                //     }
                // });
                $('#ar_aging_summary').modal('show');
                break;
                
                //Case 11
                case("a/r-aging-detail"):
                    // alert("working");
                    $("#ar_aging_detail").modal('show');
                break;

        //AP Aging
                //Case 12
                case("a/p-aging-summary"):
                $.ajax({
                    type: "POST",
                    url: "/CommonReportsModal-Ajax",
                    data:{
                        class: "commonReportsFunction",
                        action : "fetchModalData"
                    },
                    success : ( response )=>{
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#ap_aging_summary #ap_aging_summary_portfolio_id').append(data.portfolio_ddl);
                            $('#ap_aging_summary #ap_aging_summary_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#ap_aging_summary #ap_aging_summary_property_id').append(data.property_ddl);
                            $('#ap_aging_summary #ap_aging_summary_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');

                            $('#ap_aging_summary #ap_aging_summary_vendor_id').append(data.vendor_ddl);
                            $('#ap_aging_summary #ap_aging_summary_vendor_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#ap_aging_summary').modal('show');
                break;

                //Case 13
                case("a/p-aging-detail"):
                $.ajax({
                    type: "POST",
                    url: "/CommonReportsModal-Ajax",
                    data:{
                        class: "commonReportsFunction",
                        action : "fetchModalData"
                    },
                    success : ( response )=>{
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#ap_aging_detail #ap_aging_detail_portfolio_id').append(data.portfolio_ddl);
                            $('#ap_aging_detail #ap_aging_detail_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#ap_aging_detail #ap_aging_detail_property_id').append(data.property_ddl);
                            $('#ap_aging_detail #ap_aging_detail_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                           
                            $('#ap_aging_detail #ap_aging_detail_vendor_id').append(data.vendor_ddl);
                            $('#ap_aging_detail #ap_aging_detail_vendor_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#ap_aging_detail').modal('show');
                break;

                //Case 14
                case("audit-log"):
                // alert("Working");
                $('#audit_log').modal('show');
                break;

                //Case 15
                case("income-register"):
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchModalData'
                    
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#income_register #income_register_portfolio_id').append(data.portfolio_ddl);
                            $('#income_register #income_register_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#income_register #income_register_property_id').append(data.property_ddl);
                            $('#income_register #income_register_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#income_register').modal('show');
                break; 

    
                //Case 16
                case("charge-detail"):
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchModalData'
                    
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#charge_detail #charge_detail_portfolio_id').append(data.portfolio_ddl);
                            $('#charge_detail #charge_detail_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#charge_detail #charge_detail_property_id').append(data.property_ddl);
                            $('#charge_detail #charge_detail_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#charge_detail').modal('show');
                break; 
    
                //case 17
                case("expenses-by-vendor"):
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchModalData'
                    
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#expenses_by_vendor #expenses_by_vendor_portfolio_id').append(data.portfolio_ddl);
                            $('#expenses_by_vendor #expenses_by_vendor_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#expenses_by_vendor #expenses_by_vendor_property_id').append(data.property_ddl);
                            $('#expenses_by_vendor #expenses_by_vendor_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                          
                            $('#expenses_by_vendor #expenses_by_vendor_vendor_id').append(data.vendor_ddl);
                            $('#expenses_by_vendor #expenses_by_vendor_vendor_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#expenses_by_vendor').modal('show');
                break; 
                
                
                //case 18
                case("property-statement"):
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchModalData'
                    
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.portfolio_ddl);
                            $('#property_statement #property_statement_portfolio_id').append(data.portfolio_ddl);
                            $('#property_statement #property_statement_portfolio_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                            $('#property_statement #property_statement_property_id').append(data.property_ddl);
                            $('#property_statement #property_statement_property_id').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });
                $('#property_statement').modal('show');
                break; 
                

                //Case 19
                case("bank-account-activity"):
                //  alert("Working");
                $.ajax({
                    type: 'POST',
                    url: '/CommonReportsModal-Ajax',
                    data: {
                        class: 'commonReportsFunction',
                        action: 'fetchBankName'
                    },
                    success: function (response) {
                        var data = $.parseJSON(response);
                        if (data.status == "success") {
                            // console.log('reportPop>>>>>>',data.bank_ddl);
                            $('#bank_account #bank_account_bank_name').append(data.bank_ddl);
                            $('#bank_account #bank_account_bank_name').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        } else {
                            toastr.error(data.message);
                        }
                    },
                    error: function (data) {
                        var errors = $.parseJSON(data.responseText);
                        $.each(errors, function (key, value) {
                            $('#' + key + '_err').text(value);
                        });
                    }
                });   
                $('#bank_account').modal('show');
                break;
    
    
                 //Case 20
                 case("voided-checks"):
                //  alert("Working");
                 $('#voided_check').modal('show');
                 break;
    
                 //case 21
                 case("trial-balance"):
                 $.ajax({
                     type: 'POST',
                     url: '/CommonReportsModal-Ajax',
                     data: {
                         class: 'commonReportsFunction',
                         action: 'fetchModalData'
                     
                     },
                     success: function (response) {
                         var data = $.parseJSON(response);
                         if (data.status == "success") {
                             // console.log('reportPop>>>>>>',data.portfolio_ddl);
                             $('#trial_balance #trial_balance_portfolio_id').append(data.portfolio_ddl);
                             $('#trial_balance #trial_balance_portfolio_id').multiselect({
                                 includeSelectAllOption: true,
                                 allSelectedText: 'All Selected',
                                 enableFiltering: true,
                                 nonSelectedText: 'Select'
                             }).multiselect('selectAll', false).multiselect('updateButtonText');
                             $('#trial_balance #trial_balance_property_id').append(data.property_ddl);
                             $('#trial_balance #trial_balance_property_id').multiselect({
                                 includeSelectAllOption: true,
                                 allSelectedText: 'All Selected',
                                 enableFiltering: true,
                                 nonSelectedText: 'Select'
                             }).multiselect('selectAll', false).multiselect('updateButtonText');
                         } else {
                             toastr.error(data.message);
                         }
                     },
                     error: function (data) {
                         var errors = $.parseJSON(data.responseText);
                         $.each(errors, function (key, value) {
                             $('#' + key + '_err').text(value);
                         });
                     }
                 });
                 $('#trial_balance').modal('show');
                 break; 
                 
                
                 //case 22
                 case("trial-balance-consolidated"):
                 $.ajax({
                     type: 'POST',
                     url: '/CommonReportsModal-Ajax',
                     data: {
                         class: 'commonReportsFunction',
                         action: 'fetchModalData'
                     
                     },
                     success: function (response) {
                         var data = $.parseJSON(response);
                         if (data.status == "success") {
                             // console.log('reportPop>>>>>>',data.portfolio_ddl);
                             $('#trial_balance_consolidated #trial_balance_consolidated_portfolio_id').append(data.portfolio_ddl);
                             $('#trial_balance_consolidated #trial_balance_consolidated_portfolio_id').multiselect({
                                 includeSelectAllOption: true,
                                 allSelectedText: 'All Selected',
                                 enableFiltering: true,
                                 nonSelectedText: 'Select'
                             }).multiselect('selectAll', false).multiselect('updateButtonText');
                             $('#trial_balance_consolidated #trial_balance_consolidated_property_id').append(data.property_ddl);
                             $('#trial_balance_consolidated #trial_balance_consolidated_property_id').multiselect({
                                 includeSelectAllOption: true,
                                 allSelectedText: 'All Selected',
                                 enableFiltering: true,
                                 nonSelectedText: 'Select'
                             }).multiselect('selectAll', false).multiselect('updateButtonText');
                         } else {
                             toastr.error(data.message);
                         }
                     },
                     error: function (data) {
                         var errors = $.parseJSON(data.responseText);
                         $.each(errors, function (key, value) {
                             $('#' + key + '_err').text(value);
                         });
                     }
                 });
                 $('#trial_balance_consolidated').modal('show');
                 break; 
                 
                  //case 23
                  case("adjusted-trial-balance"):
                //   alert("working");
                  $.ajax({
                      type: 'POST',
                      url: '/CommonReportsModal-Ajax',
                      data: {
                          class: 'commonReportsFunction',
                          action: 'fetchModalData'
                      
                      },
                      success: function (response) {
                          var data = $.parseJSON(response);
                          if (data.status == "success") {
                              // console.log('reportPop>>>>>>',data.portfolio_ddl);
                              $('#adjust_trial_balance #at_balance_portfolio_id').append(data.portfolio_ddl);
                              $('#adjust_trial_balance #at_balance_portfolio_id').multiselect({
                                  includeSelectAllOption: true,
                                  allSelectedText: 'All Selected',
                                  enableFiltering: true,
                                  nonSelectedText: 'Select'
                              }).multiselect('selectAll', false).multiselect('updateButtonText');
                              $('#adjust_trial_balance #at_balance_property_id').append(data.property_ddl);
                              $('#adjust_trial_balance #at_balance_property_id').multiselect({
                                  includeSelectAllOption: true,
                                  allSelectedText: 'All Selected',
                                  enableFiltering: true,
                                  nonSelectedText: 'Select'
                              }).multiselect('selectAll', false).multiselect('updateButtonText');
                          } else {
                              toastr.error(data.message);
                          }
                      },
                      error: function (data) {
                          var errors = $.parseJSON(data.responseText);
                          $.each(errors, function (key, value) {
                              $('#' + key + '_err').text(value);
                          });
                      }
                  });
                  $('#adjust_trial_balance').modal('show');
                  break; 
     
     
    
    
    ///<----Tenant Reports------
                        //Case 1
                        case("tenant-ledger"):
                        $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#tenant_ledger #tenant_ledger_portfolio_id').append(data.portfolio_ddl);
                                    $('#tenant_ledger #tenant_ledger_portfolio_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_ledger #tenant_ledger_property_id').append(data.property_ddl);
                                    $('#tenant_ledger #tenant_ledger_property_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_ledger #tenant_ledger_tenant_id').append(data.tenant_ddl);
                                    $('#tenant_ledger #tenant_ledger_tenant_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        $('#tenant_ledger').modal('show');
                        break; 
                        

                        //Case 2
                        case("tenant-statement"):
                        $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#tenant_statement #tenant_statement_portfolio_id').append(data.portfolio_ddl);
                                    $('#tenant_statement #tenant_statement_portfolio_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_statement #tenant_statement_property_id').append(data.property_ddl);
                                    $('#tenant_statement #tenant_statement_property_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_statement #tenant_statement_tenant_id').append(data.tenant_ddl);
                                    $('#tenant_statement #tenant_statement_tenant_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        $('#tenant_statement').modal('show');
                        break; 
                        

                        //Case 3
                        case("tenant-payment"):
                        $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#tenant_payment #tenant_payment_portfolio_id').append(data.portfolio_ddl);
                                    $('#tenant_payment #tenant_payment_portfolio_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_payment #tenant_payment_property_id').append(data.property_ddl);
                                    $('#tenant_payment #tenant_payment_property_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_payment #tenant_payment_tenant_id').append(data.tenant_ddl);
                                    $('#tenant_payment #tenant_payment_tenant_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        $('#tenant_payment').modal('show');
                        break; 


                        //Case 4
                        case("tenant-consolidated-payment"):
                        $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#tenant_consolidated_payment #tc_payment_portfolio_id').append(data.portfolio_ddl);
                                    $('#tenant_consolidated_payment #tc_payment_portfolio_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_consolidated_payment #tc_payment_property_id').append(data.property_ddl);
                                    $('#tenant_consolidated_payment #tc_payment_property_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_consolidated_payment #tc_payment_tenant_id').append(data.tenant_ddl);
                                    $('#tenant_consolidated_payment #tc_payment_tenant_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        $('#tenant_consolidated_payment').modal('show');
                        break; 
                            
                        //Case 5
                        case("tenant-online-payment-filters"):
                        $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#tenant_online_payment #to_payment_portfolio_id').append(data.portfolio_ddl);
                                    $('#tenant_online_payment #to_payment_portfolio_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_online_payment #to_payment_property_id').append(data.property_ddl);
                                    $('#tenant_online_payment #to_payment_property_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#tenant_online_payment #to_payment_tenant_id').append(data.tenant_ddl);
                                    $('#tenant_online_payment #to_payment_tenant_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        $('#tenant_online_payment').modal('show');
                        break; 

                         //Case 6
                         case("tenant-unpaid-charges-filters"):
                         $.ajax({
                             type: "POST",
                             url: "/CommonReportsModal-Ajax",
                             data:{
                                 class: "commonReportsFunction",
                                 action : "fetchModalData"
                             },
                             success : ( response )=>{
                                 var data = $.parseJSON(response);
                                 if (data.status == "success") {
                                     // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                     $('#tenant_unpaid_charges #tu_charges_portfolio_id').append(data.portfolio_ddl);
                                     $('#tenant_unpaid_charges #tu_charges_portfolio_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#tenant_unpaid_charges #tu_charges_property_id').append(data.property_ddl);
                                     $('#tenant_unpaid_charges #tu_charges_property_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#tenant_unpaid_charges #tu_charges_tenant_id').append(data.tenant_ddl);
                                     $('#tenant_unpaid_charges #tu_charges_tenant_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                 } else {
                                     toastr.error(data.message);
                                 }
                             },
                             error: function (data) {
                                 var errors = $.parseJSON(data.responseText);
                                 $.each(errors, function (key, value) {
                                     $('#' + key + '_err').text(value);
                                 });
                             }
                         });
                         $('#tenant_unpaid_charges').modal('show');
                         break; 
                         
                                         
                         //Case 7
                         case("delinquent-tenant"):
                         $.ajax({
                             type: "POST",
                             url: "/CommonReportsModal-Ajax",
                             data:{
                                 class: "commonReportsFunction",
                                 action : "fetchModalData"
                             },
                             success : ( response )=>{
                                 var data = $.parseJSON(response);
                                 if (data.status == "success") {
                                     // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                     $('#delinquent_tenant #delinquent_tenant_portfolio_id').append(data.portfolio_ddl);
                                     $('#delinquent_tenant #delinquent_tenant_portfolio_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#delinquent_tenant #delinquent_tenant_property_id').append(data.property_ddl);
                                     $('#delinquent_tenant #delinquent_tenant_property_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#delinquent_tenant #delinquent_tenant_tenant_id').append(data.tenant_ddl);
                                     $('#delinquent_tenant #delinquent_tenant_tenant_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                 } else {
                                     toastr.error(data.message);
                                 }
                             },
                             error: function (data) {
                                 var errors = $.parseJSON(data.responseText);
                                 $.each(errors, function (key, value) {
                                     $('#' + key + '_err').text(value);
                                 });
                             }
                         });
                         $('#delinquent_tenant').modal('show');
                         break; 
 
                         //Case 8
                         case("delinquent-tenant-email/text"):
                         $.ajax({
                             type: "POST",
                             url: "/CommonReportsModal-Ajax",
                             data:{
                                 class: "commonReportsFunction",
                                 action : "fetchModalData"
                             },
                             success : ( response )=>{
                                 var data = $.parseJSON(response);
                                 if (data.status == "success") {
                                     // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                     $('#delinquent_tenant_email #dl_tenant_email_portfolio_id').append(data.portfolio_ddl);
                                     $('#delinquent_tenant_email #dl_tenant_email_portfolio_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#delinquent_tenant_email #dl_tenant_email_property_id').append(data.property_ddl);
                                     $('#delinquent_tenant_email #dl_tenant_email_property_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#delinquent_tenant_email #dl_tenant_email_tenant_id').append(data.tenant_ddl);
                                     $('#delinquent_tenant_email #dl_tenant_email_tenant_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                 } else {
                                     toastr.error(data.message);
                                 }
                             },
                             error: function (data) {
                                 var errors = $.parseJSON(data.responseText);
                                 $.each(errors, function (key, value) {
                                     $('#' + key + '_err').text(value);
                                 });
                             }
                         });
                         $('#delinquent_tenant_email').modal('show');
                         break;  

                         //Case 9
                         case("consolidated-delinquent-tenant"):
                         $.ajax({
                             type: "POST",
                             url: "/CommonReportsModal-Ajax",
                             data:{
                                 class: "commonReportsFunction",
                                 action : "fetchModalData"
                             },
                             success : ( response )=>{
                                 var data = $.parseJSON(response);
                                 if (data.status == "success") {
                                     // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                     $('#consolidated_dlt #consolidated_dlt_portfolio_id').append(data.portfolio_ddl);
                                     $('#consolidated_dlt #consolidated_dlt_portfolio_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#consolidated_dlt #consolidated_dlt_property_id').append(data.property_ddl);
                                     $('#consolidated_dlt #consolidated_dlt_property_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#consolidated_dlt #consolidated_dlt_tenant_id').append(data.tenant_ddl);
                                     $('#consolidated_dlt #consolidated_dlt_tenant_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                 } else {
                                     toastr.error(data.message);
                                 }
                             },
                             error: function (data) {
                                 var errors = $.parseJSON(data.responseText);
                                 $.each(errors, function (key, value) {
                                     $('#' + key + '_err').text(value);
                                 });
                             }
                         });
                         $('#consolidated_dlt').modal('show');
                         break;  

                         //Case 10
                         case("short-term-renter-list"):
                         $.ajax({
                             type: "POST",
                             url: "/CommonReportsModal-Ajax",
                             data:{
                                 class: "commonReportsFunction",
                                 action : "fetchModalData"
                             },
                             success : ( response )=>{
                                 var data = $.parseJSON(response);
                                 if (data.status == "success") {
                                     // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                     $('#short_term_renter #shrt_term_portfolio_id').append(data.portfolio_ddl);
                                     $('#short_term_renter #shrt_term_portfolio_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#short_term_renter #shrt_term_property_id').append(data.property_ddl);
                                     $('#short_term_renter #shrt_term_property_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                 } else {
                                     toastr.error(data.message);
                                 }
                             },
                             error: function (data) {
                                 var errors = $.parseJSON(data.responseText);
                                 $.each(errors, function (key, value) {
                                     $('#' + key + '_err').text(value);
                                 });
                             }
                         });
                         $('#short_term_renter').modal('show');
                         break;  

                        //Case 11
                         case("tenant-eeo-report"):
                         $.ajax({
                             type: "POST",
                             url: "/CommonReportsModal-Ajax",
                             data:{
                                 class: "commonReportsFunction",
                                 action : "fetchModalData"
                             },
                             success : ( response )=>{
                                 var data = $.parseJSON(response);
                                 if (data.status == "success") {
                                     // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                     $('#tenant_eeo_report #eeo_report_portfolio_id').append(data.portfolio_ddl);
                                     $('#tenant_eeo_report #eeo_report_portfolio_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                     $('#tenant_eeo_report #eeo_report_property_id').append(data.property_ddl);
                                     $('#tenant_eeo_report #eeo_report_property_id').multiselect({
                                         includeSelectAllOption: true,
                                         allSelectedText: 'All Selected',
                                         enableFiltering: true,
                                         nonSelectedText: 'Select'
                                     }).multiselect('selectAll', false).multiselect('updateButtonText');
                                 } else {
                                     toastr.error(data.message);
                                 }
                             },
                             error: function (data) {
                                 var errors = $.parseJSON(data.responseText);
                                 $.each(errors, function (key, value) {
                                     $('#' + key + '_err').text(value);
                                 });
                             }
                         });
                         $('#tenant_eeo_report').modal('show');
                         break;
                         
            //Maintenance Reports
                        
                        //Case 1
                        case("open-work-order"):
                        $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#open_work_order #ow_order_portfolio_id').append(data.portfolio_ddl);
                                    $('#open_work_order #ow_order_portfolio_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#open_work_order #ow_order_property_id').append(data.property_ddl);
                                    $('#open_work_order #ow_order_property_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#open_work_order #ow_order_tenant_id').append(data.tenant_ddl);
                                    $('#open_work_order #ow_order_tenant_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        $('#open_work_order').modal('show');
                        break;
                        

                //Owner Reports
                        //Case 1
                        case("owner-statement"):
                        $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#owner_statement #owner_statement_portfolio_id').append(data.portfolio_ddl);
                                    $('#owner_statement #owner_statement_portfolio_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#owner_statement #owner_statement_property_id').append(data.property_ddl);
                                    $('#owner_statement #owner_statement_property_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');

                                    $('#owner_statement #owner_statement_owner_name').append(data.owner_ddl);
                                    $('#owner_statement #owner_statement_owner_name').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        $('#owner_statement').modal('show');
                        break;
                        
                //Vendor Reports
                        //Case 1
                        case("expense-by-vendor"):
                        $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#expense_by_vendor #eb_vendor_portfolio_id').append(data.portfolio_ddl);
                                    $('#expense_by_vendor #eb_vendor_portfolio_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#expense_by_vendor #eb_vendor_property_id').append(data.property_ddl);
                                    $('#expense_by_vendor #eb_vendor_property_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#expense_by_vendor #eb_vendor_vendor_id').append(data.vendor_ddl);
                                    $('#expense_by_vendor #eb_vendor_vendor_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        $('#expense_by_vendor').modal('show');
                        break;

                        //Case 2
                        case("vendor-bills"):
                        // alert("working");
                         $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#vendor_bill #vendor_bill_vendor_id').append(data.vendor_ddl);
                                    $('#vendor_bill #vendor_bill_vendor_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        $('#vendor_bill').modal('show');
                        break;

                        //Case 3
                        case("transaction-by-vendor"):
                        // alert("working");
                        $.ajax({
                            type: "POST",
                            url: "/CommonReportsModal-Ajax",
                            data:{
                                class: "commonReportsFunction",
                                action : "fetchModalData"
                            },
                            success : ( response )=>{
                                var data = $.parseJSON(response);
                                if (data.status == "success") {
                                    // console.log('reportPop>>>>>>',data.portfolio_ddl);
                                    $('#transaction_vendor #tv_vendor_id').append(data.vendor_ddl);
                                    $('#transaction_vendor #tv_vendor_id').multiselect({
                                        includeSelectAllOption: true,
                                        allSelectedText: 'All Selected',
                                        enableFiltering: true,
                                        nonSelectedText: 'Select'
                                    }).multiselect('selectAll', false).multiselect('updateButtonText');
                                    $('#transaction_vendor').modal('show');
                                } else {
                                    toastr.error(data.message);
                                }
                            },
                            error: function (data) {
                                var errors = $.parseJSON(data.responseText);
                                $.each(errors, function (key, value) {
                                    $('#' + key + '_err').text(value);
                                });
                            }
                        });
                        break;

                        // nidhi Mam code
                        case("balance-sheet-detail"):
                        $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'balanceSheetReport',
                            action: 'fetchModalData'
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('#balance-sheet-detail #portfolio_id').append(data.portfolio_ddl);
                                $('#balance-sheet-detail #portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                $('#balance-sheet-detail #balance_property_id').append(data.property_ddl);
                                $('#balance-sheet-detail #balance_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#balance-sheet-detail').modal('show');
                break;
                case 'balance-sheet-consolidated': 
                
                
                        $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'balanceSheetReport',
                            action: 'fetchModalData'
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('#balance-sheet-consolidated #consolidated_portfolio_id').append(data.portfolio_ddl);
                                $('#balance-sheet-consolidated #consolidated_portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                $('#balance-sheet-consolidated #consolidated_property_id').append(data.property_ddl);
                                $('#balance-sheet-consolidated #consolidated_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#balance-sheet-consolidated').modal('show');
                break;  
    
                case 'income-statement': 
               
                
                        $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'balanceSheetReport',
                            action: 'fetchModalData'
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('#income_statement #income_statement_portfolio_id').append(data.portfolio_ddl);
                                $('#income_statement #income_statement_portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                $('#income_statement #income_statement_property_id').append(data.property_ddl);
                                $('#income_statement #income_statement_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#income_statement').modal('show');
                break; 
                case 'income-statement-consolidated': 
                        $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'balanceSheetReport',
                            action: 'fetchModalData'
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('#income_statement_consolidated #income_consolidated_portfolio_id').append(data.portfolio_ddl);
                                $('#income_statement_consolidated #income_consolidated_portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                $('#income_statement_consolidated #income_consolidated_property_id').append(data.property_ddl);
                                $('#income_statement_consolidated #income_consolidated_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#income_statement_consolidated').modal('show');
                break;  
                case 'income-statement---12-months-comparison': 
                        $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'balanceSheetReport',
                            action: 'fetchModalData'
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('#income_statement_months #income_months_portfolio_id').append(data.portfolio_ddl);
                                $('#income_statement_months #income_months_portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                $('#income_statement_months #income_months_property_id').append(data.property_ddl);
                                $('#income_statement_months #income_months_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#income_statement_months').modal('show');
                break;
    
                case 'cash-flow': 
                        $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'balanceSheetReport',
                            action: 'fetchModalData'
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('#cash #cash_portfolio_id').append(data.portfolio_ddl);
                                $('#cash #cash_portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                $('#cash #cash_property_id').append(data.property_ddl);
                                $('#cash #cash_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#cash').modal('show');
                break;  
    
                case 'cash-flow-12-months': 
                        $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'balanceSheetReport',
                            action: 'fetchModalData'
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('#cash_flow_months #cash_flow_portfolio_id').append(data.portfolio_ddl);
                                $('#cash_flow_months #cash_flow_portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                $('#cash_flow_months #cash_flow_property_id').append(data.property_ddl);
                                $('#cash_flow_months #cash_flow_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#cash_flow_months').modal('show');
                break; 
    
                case 'cash-flow-comparison': 
                        $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'balanceSheetReport',
                            action: 'fetchModalData'
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('#cash_flow_comparsions #cash_flow_comparsion_portfolio_id').append(data.portfolio_ddl);
                                $('#cash_flow_comparsions #cash_flow_comparsion_portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                $('#cash_flow_comparsions #cash_flow_comparsion_property_id').append(data.property_ddl);
                                $('#cash_flow_comparsions #cash_flow_comparsion_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#cash_flow_comparsions').modal('show');
                break; 
                case 'equity-statement': 
                        $.ajax({
                        type: 'POST',
                        url: '/CommonReportsModal-Ajax',
                        data: {
                            class: 'balanceSheetReport',
                            action: 'fetchModalData'
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('#gfrm1_equaity #equity_portfolio_id').append(data.portfolio_ddl);
                                $('#gfrm1_equaity #equity_portfolio_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                $('#gfrm1_equaity #equity_balance_property_id').append(data.property_ddl);
                                $('#gfrm1_equaity #equity_balance_property_id').multiselect({
                                    includeSelectAllOption: true,
                                    allSelectedText: 'All Selected',
                                    enableFiltering: true,
                                    nonSelectedText: 'Select'
                                }).multiselect('selectAll', false).multiselect('updateButtonText');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                    $('#gfrm1_equaity').modal('show');
                break; 
    
                case 'account-totals': 
                        // $.ajax({
                        // type: 'POST',
                        // url: '/CommonReportsModal-Ajax',
                        // data: {
                        //     class: 'balanceSheetReport',
                        //     action: 'fetchModalData'
                        // },
                    //     success: function (response) {
                    //         var data = $.parseJSON(response);
                    //         if (data.status == "success") {
                    //             $('#gfrm1_accountsgfrm1_accounts #equity_portfolio_id').append(data.portfolio_ddl);
                    //             $('#gfrm1_accounts #equity_portfolio_id').multiselect({
                    //                 includeSelectAllOption: true,
                    //                 allSelectedText: 'All Selected',
                    //                 enableFiltering: true,
                    //                 nonSelectedText: 'Select'
                    //             }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                    //             $('#gfrm1_accounts #equity_balance_property_idequity_balance_property_id').append(data.property_ddl);
                    //             $('#gfrm1_accounts #equity_balance_property_id').multiselect({
                    //                 includeSelectAllOption: true,
                    //                 allSelectedText: 'All Selected',
                    //                 enableFiltering: true,
                    //                 nonSelectedText: 'Select'
                    //             }).multiselect('selectAll', false).multiselect('updateButtonText');
                    //         } else {
                    //             toastr.error(data.message);
                    //         }
                    //     },
                    //     error: function (data) {
                    //         var errors = $.parseJSON(data.responseText);
                    //         $.each(errors, function (key, value) {
                    //             $('#' + key + '_err').text(value);
                    //         });
                    //     }
                    // });
                    $('#gfrm1_accounts').modal('show');
                break;
                case 'expense-distribution': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gmf2_expenses #expenses_portfolio_id').append(data.portfolio_ddl);
                                            $('#gmf2_expenses #expenses_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gmf2_expenses #expenses_property_id').append(data.property_ddl);
                                            $('#gmf2_expenses #expenses_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gmf2_expenses').modal('show');
                            break;
                            case 'general-ledger': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gmf3_ledger #ledger_portfolio_id').append(data.portfolio_ddl);
                                            $('#gmf3_ledger #ledger_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gmf3_ledger #ledger_property_id').append(data.property_ddl);
                                            $('#gmf3_ledger #ledger_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gmf3_ledger').modal('show');
                            break;
                            case 'security-deposit': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gmf4_security #security_portfolio_id').append(data.portfolio_ddl);
                                            $('#gmf4_security #security_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gmf4_security #security_property_id').append(data.property_ddl);
                                            $('#gmf4_security #security_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gmf4_security').modal('show');
                            break; 
                        case 'expense-register': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        console.log('data>>', data);
                                        if (data.status == "success") {
                                            $('#gfrm1_exp_register #exp_register_portfolio_id').append(data.portfolio_ddl);
                                            $('#gfrm1_exp_register #exp_register_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gfrm1_exp_register #exp_register_property_id').append(data.property_ddl);
                                            $('#gfrm1_exp_register #exp_register_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gfrm1_exp_register #exp_register_vendor_id').append(data.vendor_ddl);
                                            $('#gfrm1_exp_register #exp_register_vendor_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gfrm1_exp_register').modal('show');
                            break;
                            case 'open-invoices': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gmf4_open_invoice #open_invoice_portfolio_id').append(data.portfolio_ddl);
                                            $('#gmf4_open_invoice #open_invoice_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gmf4_open_invoice #open_invoice_property_id').append(data.property_ddl);
                                            $('#gmf4_open_invoice #open_invoice_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                            $('#gmf4_open_invoice #open_invoice_tenant_id').append(data.tenant_ddl);
                                            $('#gmf4_open_invoice #open_invoice_tenant_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gmf4_open_invoice').modal('show');
                            break;
                            case 'collection': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gmf_collection #collection_portfolio_id').append(data.portfolio_ddl);
                                            $('#gmf_collection #collection_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gmf_collection #collection_property_id').append(data.property_ddl);
                                            $('#gmf_collection #collection_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                            $('#gmf_collection #collection_tenant_id').append(data.tenant_ddl);
                                            $('#gmf_collection #collection_tenant_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gmf_collection').modal('show');
                            break;
                            case 'audit-log': 
                        // $.ajax({
                        // type: 'POST',
                        // url: '/CommonReportsModal-Ajax',
                        // data: {
                        //     class: 'balanceSheetReport',
                        //     action: 'fetchModalData'
                        // },
                    //     success: function (response) {
                    //         var data = $.parseJSON(response);
                    //         if (data.status == "success") {
                    //             $('#gfrm1_accountsgfrm1_accounts #equity_portfolio_id').append(data.portfolio_ddl);
                    //             $('#gfrm1_accounts #equity_portfolio_id').multiselect({
                    //                 includeSelectAllOption: true,
                    //                 allSelectedText: 'All Selected',
                    //                 enableFiltering: true,
                    //                 nonSelectedText: 'Select'
                    //             }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                    //             $('#gfrm1_accounts #equity_balance_property_idequity_balance_property_id').append(data.property_ddl);
                    //             $('#gfrm1_accounts #equity_balance_property_id').multiselect({
                    //                 includeSelectAllOption: true,
                    //                 allSelectedText: 'All Selected',
                    //                 enableFiltering: true,
                    //                 nonSelectedText: 'Select'
                    //             }).multiselect('selectAll', false).multiselect('updateButtonText');
                    //         } else {
                    //             toastr.error(data.message);
                    //         }
                    //     },
                    //     error: function (data) {
                    //         var errors = $.parseJSON(data.responseText);
                    //         $.each(errors, function (key, value) {
                    //             $('#' + key + '_err').text(value);
                    //         });
                    //     }
                    // });
                    $('#gfrm1_aduit_log').modal('show');
                break; 
                case 'recurring-journal-entries': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gmf4_recurring #recurring_portfolio_id').append(data.portfolio_ddl);
                                            $('#gmf4_recurring #recurring_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gmf4_recurring #recurring_property_id').append(data.property_ddl);
                                            $('#gmf4_recurring #recurring_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gmf4_recurring').modal('show');
                            break;  
                            case 'recent-automatic-transactions': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gf1_auto_trans #automatic_portfolio_id').append(data.portfolio_ddl);
                                            $('#gf1_auto_trans #automatic_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gf1_auto_trans #automatic_property_id').append(data.property_ddl);
                                            $('#gf1_auto_trans #automatic_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gf1_auto_trans').modal('show');
                            break;  
                            case 'transaction-detail-by-account': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gf1_transactions #transactions_portfolio_id').append(data.portfolio_ddl);
                                            $('#gf1_transactions #transactions_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gf1_transactions #transactions_property_id').append(data.property_ddl);
                                            $('#gf1_transactions #transactions_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gf1_transactions').modal('show');
                            break;  
                            case 'journal-entry': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gf1_journal #journal_portfolio_id').append(data.portfolio_ddl);
                                            $('#gf1_journal #journal_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gf1_journal #journal_property_id').append(data.property_ddl);
                                            $('#gf1_journal #journal_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gf1_journal').modal('show');
                            break; 
    
                            case 'deposit-register': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gf1_deposit #deposit_portfolio_id').append(data.portfolio_ddl);
                                            $('#gf1_deposit #deposit_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gf1_deposit #deposit_property_id').append(data.property_ddl);
                                            $('#gf1_deposit #deposit_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gf1_deposit').modal('show');
                            break; 
                            case 'check-register': 
                                   
                                $('#gf1_check').modal('show');
                            break; 
    
                             case 'budget-to-actual-comparison': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gf1_annual_budget #annual_budget_portfolio_id').append(data.portfolio_ddl);
                                            $('#gf1_annual_budget #annual_budget_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gf1_annual_budget #annual_budget_property_id').append(data.property_ddl);
                                            $('#gf1_annual_budget #annual_budget_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gf1_annual_budget').modal('show');
                            break;
    
                             case 'budget-vs.-actual': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#gf1_budget_annual #budget_annual_portfolio_id').append(data.portfolio_ddl);
                                            $('#gf1_budget_annual #budget_annual_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#gf1_budget_annual #budget_annual_property_id').append(data.property_ddl);
                                            $('#gf1_budget_annual #budget_annual_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#gf1_budget_annual').modal('show');
                            break;  
                           
                             case 'adjusting-journal-entries': 
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#adjusting_journal #adjusting_journal_portfolio_id').append(data.portfolio_ddl);
                                            $('#adjusting_journal #adjusting_journal_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
    
                                            $('#adjusting_journal #adjusting_journal_property_id').append(data.property_ddl);
                                            $('#adjusting_journal #adjusting_journal_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                });
                                $('#adjusting_journal').modal('show');
                            break; 
    
                            case 'payment-plan-consolidated': 
                                
                                $('#emp1_ppc').modal('show');
                            break; 
                            case 'inventory-transfer-': 
                               
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#ir_model #inventory_property_id').append(data.property_ddl);
                                            $('#ir_model #inventory_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                }); 
                                $('#ir_model').modal('show');
                            break;
    
                            case 'suppliers-purchase': 
                               
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#ir_supplier #supplier_property_id').append(data.property_ddl);
                                            $('#ir_supplier #supplier_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                }); 
                                $('#ir_supplier').modal('show');
                            break;
    
                            case 'inventory-transfer-': 
                               
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#ir_model #inventory_property_id').append(data.property_ddl);
                                            $('#ir_model #inventory_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                }); 
                                $('#ir_model').modal('show');
                            break;
    
                            case 'vacant-unit': 
                               
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#property_vacant #vacant_portfolio_id').append(data.portfolio_ddl);
                                            $('#property_vacant #vacant_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                            $('#property_vacant #vacant_property_id').append(data.property_ddl);
                                            $('#property_vacant #vacant_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                }); 
                                $('#property_vacant').modal('show');
                            break;
                            case 'property-statement': 
                               
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#property_statement #properties_portfolio_id').append(data.portfolio_ddl);
                                            $('#property_statement #properties_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                            $('#property_statement #properties_property_id').append(data.property_ddl);
                                            $('#property_statement #properties_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                }); 
                                $('#property_statement').modal('show');
                            break;
                            case 'unpaid-bills-by-property': 
                               
                                    $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'fetchModalData'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                            $('#ps_unbill #unbill_portfolio_id').append(data.portfolio_ddl);
                                            $('#ps_unbill #unbill_portfolio_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                            $('#ps_unbill #unbill_property_id').append(data.property_ddl);
                                            $('#ps_unbill #unbill_property_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                            $('#ps_unbill #unbill_vendor_id').append(data.vendor_ddl);
                                            $('#ps_unbill #unbill_vendor_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                }); 
                                $('#ps_unbill').modal('show');
                            break;
                             case 'rental-application': 
    
                             $.ajax({
                                    type: 'POST',
                                    url: '/CommonReportsModal-Ajax',
                                    data: {
                                        class: 'balanceSheetReport',
                                        action: 'applicantName'
                                    },
                                    success: function (response) {
                                        var data = $.parseJSON(response);
                                        if (data.status == "success") {
                                             $("#rental_applicant_id").multiselect("destroy");
                                            $('#pr_rental #rental_applicant_id').append(data.applicant_ddl);
                                            $('#pr_rental #rental_applicant_id').multiselect({
                                                includeSelectAllOption: true,
                                                allSelectedText: 'All Selected',
                                                enableFiltering: true,
                                                nonSelectedText: 'Select'
                                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                                        } else {
                                            toastr.error(data.message);
                                        }
                                    },
                                    error: function (data) {
                                        var errors = $.parseJSON(data.responseText);
                                        $.each(errors, function (key, value) {
                                            $('#' + key + '_err').text(value);
                                        });
                                    }
                                }); 
                                $('#pr_rental').modal('show');
                            break;
                        //end

                        default:
                          
            }
            // $("#"+val).modal("show");
   });
       
    
});

//<--- Nidhi Code
$(document).on('click','.test', function (){
    	
    var portfolio = $('#portfolio_id').val();
   // console.log(portfolio);
    var property= $('#balance_property_id').val();  
    $.ajax({
        type: 'post',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'commonReportsFunction',
            action: 'balanceDetailData',
            portfolio : portfolio,
            property : property
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if(data.status == "success"){
                $('#balance-sheet-detail').modal('hide');
                var url = "/Reporting/BalanceSheetDetailedReport"
                window.location.href = base_url+url;
                
            }
        },
        error : ( error )=>{
            console.log(error);
        }
    });
});

//end----> 

$(document).on('change','#balance-sheet #bl_sheet_portfolio_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'fetchModalBalanceDetailOnchange',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#bl_sheet_property_id").multiselect("destroy");
            $('#balance-sheet #bl_sheet_property_id').html(data.property_ddl);
                $('#balance-sheet #bl_sheet_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});

$(document).on("click" , "#blSheetSubmit_btn" , function (){
    var portfolio = $('#portfolio_id').val();
    var property= $('#balanaceSheet_property_id').val();  
    $.ajax({
        type: 'post',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'commonReportsFunction',
            action: 'balanceSheetViewData',
            portfolio : portfolio,
            property : property
        },
        success: function (response) {
            var data = $.parseJSON(response);
            // console.log(data);
            if(data.status == "success"){
                $('#balance-sheet').modal('hide');
                //var url = document.URL.includes("/Reporting/commonReportsFunction");
                var url = "/Reporting/commonReportsFunction"
                window.open(base_url+url);
                // if(url==true){
                //     //console.log("here222");
                //     window.open(base_url + data.url);
                // }else{
                //     window.location.reload();
                // }
            }
        },
        error : ( error )=>{
            console.log(error);
        }
    });
});


//Date Format

var d2 = new Date();
        var month3=d2.getMonth() + 1;
        var month2 = d2.getMonth() ;
        var year2=d2.getFullYear();
        var firstDay = new Date(year2, month2, 1);
        var lastDay = new Date(year2, month3, 0);

        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        var week=(lastDay.getDay());
            week=(days[week]).substr(0,3);
            lastDay= (lastDay.getMonth()+1)+'/'+lastDay.getDate()+'/'+lastDay.getFullYear()+' '+'('+week+'.'+')';

        var week1=(firstDay.getDay());
                week1=(days[week1]).substr(0,3);
                firstDay= (firstDay.getMonth()+1)+'/'+firstDay.getDate()+'/'+firstDay.getFullYear()+' '+'('+week1+'.'+')';
       
        var week2=(d2.getDay());
        week2=(days[week2]).substr(0,3);
      
        d2 = (d2.getMonth()+1)+'/'+d2.getDate()+'/'+d2.getFullYear()+' '+'('+week2+'.'+')';
        
        $(".end_date").val(lastDay);
        $(".start_date").val(firstDay);
        $(".current_date").val(d2);
        
        // console.log(lastDay);
        // console.log(firstDay);
        // console.log(d2);

        //FromDate  Format
        var fromdate = new Date();
        fromdate.setFullYear(fromdate.getFullYear() - 1);
        var week3 = (fromdate.getDay());
        week3 = (days[week3].substr(0,3));
        fromdate = (fromdate.getMonth()+1)+'/'+fromdate.getDate()+'/'+fromdate.getFullYear()+' '+'('+week3+'.'+')';
        // console.log(fromdate);
        $(".from_date").val(fromdate);


