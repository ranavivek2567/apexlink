$(document).ready(function () {
    var lenght=$(".container-fluid").find('.lease-container').length;
    //console.log(lenght);
    if(lenght>1){

        console.log("hello");
        // $(".container-fluid").find('.lease-container').eq(0).remove();
        // $(".container-fluid").find('.bread-search-outer').eq(0).remove();
    }

    $("#reports").addClass("active");
    $(document).on('click','.listPopupdata .tenantList',function () {
        var html = "";

        $('.active_modal').removeClass('active_modal');
        $(this).addClass('active_modal');
        var dataAttr = $(this).attr('data-value');
        var data = $.parseJSON(dataAttr);
        if (data.title == 'Property Listing Report' || data.title == 'Vendor List Consolidated'){
            var systemDate = new Date();
            var year = systemDate.getFullYear();
            var month = systemDate.getMonth()+1;
            var date = systemDate.getDate();
            var hour= systemDate.getHours();
            var minutes= systemDate.getMinutes();
            var seconds= systemDate.getSeconds();
            var dateformats = year +'-'+month+'-'+date+' '+hour+':'+minutes+':'+seconds;

            $.ajax({
                type: 'POST',
                url: '/Reporting-Ajax',
                data: {
                    class: 'reportsPopupData',
                    action: 'sendDirectData',
                    title:data.title,
                    dateformats: dateformats,
                },
                success: function (response) {
                    var data = $.parseJSON(response);
                    //  console.log('reportPop',data);
                    if (data.status == "success") {
                        //= window.location.href = base_url+data.url;
                        //   console.log(data.url);
                        $('#vendorList').modal('hide');
                        window.open(base_url + data.url);
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
            return false;
        }
        if(data.label!=undefined){
        if(data.label.length > 0 ){
            for(var i = 0; i < data.label.length; i++){
                $('#vendorList .modal-title').text(data.title);

                if(data.field[i].type != 'radio') {
                    html += '<div class="col-sm-12">';
                    html += '<label is_required="'+data.label[i].is_required+'">'+ data.label[i].name ;
                    var is_required_class = '';
                    if (data.label[i].is_required == 'yes') {
                        html += '<em class="red-star">*</em></label>';
                        is_required_class = 'is_required';
                    } else {
                        html += '</label>';
                    }
                }
                if(data.field.length > 0 ) {
                    if(data.field[i].type == 'text') {
                        html += '<input  type="' + data.field[i].type + '" name="' + data.field[i].name + '" id="' + data.field[i].id + '" class="' +is_required_class+' '+data.field[i].class + '" />';
                    }
                    if(data.field[i].type == 'select'){
                        html += '<select type="' + data.field[i].type + '" name="' + data.field[i].name + '" id="' + data.field[i].id + '" class="' +is_required_class+' ' + data.field[i].class + '"></select>';
                    }
                    if(data.field[i].type == 'multiselect'){
                        html += '<select type="' + data.field[i].type + '" name="' + data.field[i].name + '" id="' + data.field[i].id + '" class="'+is_required_class+' ' + data.field[i].class + '" multiple="multiple"></select>';
                    }
                    if(data.field[i].type == 'checkbox'){
                        html += '<input type="' + data.field[i].type + '" name="' + data.field[i].name + '" class="'+is_required_class+' ' + data.field[i].class + '" />';
                    }
                    if(data.field[i].type == 'radio'){
                        html += '<div class="radioDiv" style="margin-bottom: 10px; margin-left: 10px;"><input type="' + data.field[i].type + '" name="' + data.field[i].name + '" class="'+is_required_class+' '+ data.field[i].class + '" value="'+  [i]  +'"/>'+ data.label[i].name+'</div>';
                    }
                }

                if (data.data.length > 0) {
                    if (data.data[i].table != ''  && data.data[i].table != undefined) {
                        getDataReports(data.data[i].table,data.data[i].column, data.data[i].field_name,data.data[i].defaultData,data.data[i].changed_column);
                    }
                }

                html += '</div>';
            }
            var report_columns = '';
            if (data.alternate_report_columns != ''){
                report_columns = data.alternate_report_columns;
            } else {
                report_columns = data.report_columns;
            }



            var data1 = JSON.stringify(dataAttr);
            html += '<input type="hidden" name="report_db_query" value="'+data.report_db_query+'">';
            html += '<input type="hidden" name="db_query_after_where" value="'+data.db_query_after_where+'">';
            html += '<input type="hidden" name="report_columns" value="'+data.report_columns+'">';
            html += '<input type="hidden" name="alternate_report_columns" value="'+data.alternate_report_columns+'">';
            html += '<input type="hidden" name="report_url" value="'+data.report_url+'">';
            html += '<input type="hidden" name="report_join_table" value="'+data.report_join_table+'">';
            html += "<input type='hidden' name='modal_filter_data[]' value='"+data1+"'>";
            html += "<input type='hidden' name='report_date_filter_table' value='"+data.report_date_filter_table+"'>";

            $('#vendorList .modal-body #modalDataClass').html(html);


            $( ".pet_report" ).prop( "checked", true );
            $("input[name='current_date']").datepicker({
                dateFormat: date_format,
                autoclose: true,
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", new Date());
            $("input[name='current_date']").addClass('calander');
            $("input[name='start_date']").addClass('calander');
            $("input[name='end_date']").addClass('calander');

            var date = new Date(), y = date.getFullYear(), m = date.getMonth();
            var firstDay = new Date(y, m, 1);
            var lastDay = new Date(y, m + 1, 0);

            $("input[name='start_date']").datepicker({
                dateFormat: date_format,
                autoclose: true,
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", firstDay);

            $("input[name='end_date']").datepicker({
                dateFormat: date_format,
                autoclose: true,
                changeMonth: true,
                changeYear: true
            }).datepicker("setDate", lastDay);

            var year = '';
            for (var i = y; i > y-101; i--)
            {
                year += "<option value='"+i+"'>"+i+"</option>";
            }

            $("select[name='start_year']").html(year);
            $("select[name='last_year']").html(year);

            var tenant_insurance= "<option value='All'> All</option>" +
                "<option value='yes'>Yes</option>" +
                "<option value='no'>No</option>";
            $("select[name='envelope_size']").html(tenant_insurance);

            var print_envelope = "<option value='1'>41/8 * 91⁄2</option>" +
                "<option value='2'>5/4</option>";
            $("select[name='envelope_size']").html(print_envelope);

            var mailing_label_type = "<option value='1'> HP Avery 5260 3x10</option>" +
                "<option value='2'> HP Avery 5260 2x10</option>";
            $("select[name='mailing_label_type']").html(mailing_label_type);

            var tenant_insurance = "<option value='all'> All</option>" +
                "<option value='yes'> Yes</option>"+
                "<option value='no'> No</option>";
            $("select[name='tenant_insurance']").html(tenant_insurance);
        }
    }

        $('.calander').attr('readonly', true);
        if(data.title !='Tenant Ledger' && data.title != 'Tenant Statement'){
            $('#vendorList').modal('show');
        }
    });

    function getDataReports(table, columns, field_name, default_data, changed_column) {
        $.ajax({
            type: 'POST',
            url: '/Reporting-Ajax',
            data: {
                class: 'reportsPopupData',
                action: 'fetchModalData',
                table: table,
                columns: columns,
                field_name: field_name,
                default_data: default_data,
                changed_column: changed_column
            },
            success: function (response) {
                var data = $.parseJSON(response);
                //      console.log(data);
                if (data.status == "success"){

                    if (data.data.length > 0 ) {
                        var propertyList = "";

                        if (data.field_name == 'portfolio[]') {
                            for (var i = 0; i < data.data.length; i++) {
                                propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].portfolio_name + '</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                            }
                        }

                        if (data.field_name == 'property[]') {
                            for (var i = 0; i < data.data.length; i++) {
                                propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].property_name + '</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                            }
                        }
                        if (data.field_name == 'building[]') {
                            for (var i = 0; i < data.data.length; i++) {
                                propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].building_name + '</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                            }
                        }

                        if (data.field_name == 'unit[]') {
                            for (var i = 0; i < data.data.length; i++) {
                                propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].unit_prefix + '-' +data.data[i].unit_no + '</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                            }
                        }

                        if (data.field_name == 'tenant[]' || data.field_name == 'owner[]' || data.field_name == 'vendor[]'|| data.field_name == 'contact[]'|| data.field_name == 'vendor') {

                            if (data.data.length > 0) {
                                for (var i = 0; i < data.data.length; i++) {
                                    propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].name + '</option>';
                                    $('select[name="' + data.field_name + '"]').append(propertyList);
                                }
                            }
                        }
                        if (data.field_name == 'status') {
                            // console.log('status',data);
                            $('select[name="' + data.field_name + '"]').attr('data_next',data.changed_column).append(data.data);
                        }
                        if (data.field_name == 'work_order_status') {
                            propertyList = '<option value="">Select or Apply Filter for all Status</option>';
                            $('select[name="' + data.field_name + '"]').append(propertyList);
                            for (var i = 0; i < data.data.length; i++) {
                                propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].work_order_status + '</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                            }
                        }
                        if (data.field_name == 'owner_type[]') {
                            $('select[name="' + data.field_name + '"]').append(data.data);
                        }

                        if (data.field_name == 'property_group_name') {
                            propertyList = '<option value="">Select or Apply Filter for all Groups</option>';
                            $('select[name="' + data.field_name + '"]').append(propertyList);
                            for (var i = 0; i < data.data.length; i++) {
                                propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].group_name + '</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                            }

                        }

                        if (data.field_name == 'vehicle_name[]') {
                            for (var i = 0; i < data.data.length; i++) {
                                propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].vehicle_name + '</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                            }
                        }

                        if (data.field_name == 'vehicle_color[]') {
                            for (var i = 0; i < data.data.length; i++) {
                                propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].color+ '</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                            }
                        }
                        if (data.field_name == 'taxpayer_name[]') {
                            if (data.data.length > 0) {
                                for (var i = 0; i < data.data.length; i++) {
                                    propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].tax_payer_name + '</option>';
                                    $('select[name="' + data.field_name + '"]').append(propertyList);
                                }
                            }
                        }

                        if (data.field_name == 'vendor_type') {
                            if (data.data.length > 0) {
                                var propertyList = '<option value="all">Select and apply a filter for all Vendors</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                                for (var i = 0; i < data.data.length; i++) {
                                    propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].vendor_type + '</option>';
                                    $('select[name="' + data.field_name + '"]').append(propertyList);
                                }
                            }
                        }

                        if (data.field_name == 'vendor') {
                            if (data.data.length > 0) {
                                var propertyList = '<option value="0">Select</option>';
                                // $('select[name="' + data.field_name + '"]').append(propertyList);
                                for (var i = 0; i < data.data.length; i++) {
                                    propertyList += '<option value="' + data.data[i].id + '">' + data.data[i].name + '</option>';
                                }
                                $('select[name="' + data.field_name + '"]').html(propertyList);
                            }
                        }


                        if (data.field_name == 'work_category') {

                            if (data.data.length > 0) {
                                var propertyList = '<option value="all">Select and apply a filter for all Categories</option>';
                                $('select[name="' + data.field_name + '"]').append(propertyList);
                                for (var i = 0; i < data.data.length; i++) {
                                    propertyList = '<option value="' + data.data[i].id + '">' + data.data[i].category + '</option>';
                                    $('select[name="' + data.field_name + '"]').append(propertyList);
                                }
                            }
                        }

                        if(data.field_name != 'property_group_name' && data.field_name != 'work_order_status' && data.field_name != 'status' && data.field_name != 'vendor_type' && data.field_name != 'vendor' &&  data.field_name != 'work_category' ) {
                            $('#modalDataClass select[name="' + data.field_name + '"]').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        }
                    } else {
                        if (data.field_name == 'property_group_name') {
                            propertyList = '<option value="">Select or Apply Filter for all Groups</option>';
                            $('select[name="' + data.field_name + '"]').append(propertyList);
                        }

                        if(data.field_name != 'property_group_name') {
                            $('#modalDataClass select[name="' + data.field_name + '"]').multiselect({
                                includeSelectAllOption: true,
                                allSelectedText: 'All Selected',
                                enableFiltering: true,
                                nonSelectedText: 'Select'
                            }).multiselect('selectAll', false).multiselect('updateButtonText');
                        }
                    }
                } else {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }


    $(document).on('change', '.onChangeStatus', function () {
        var getVariable = $(this).attr('id');
        var change_next = $(this).attr('data_next');
        var formData = $('#popupReportData .form-control').serializeArray();
        var dataAttr = $('.listPopupdata .active_modal').attr('data-value');
        if (dataAttr != "" && dataAttr != undefined){
            var data = $.parseJSON(dataAttr);
        }
        var dataObj ={'columnsData':{'users':['1']}, 'tableDetail':data};
        $(formData).each(function( index, element ) {
            // element == this
            var data_id = $('#popupReportData [name="'+element.name+'"]').attr('id');
            if(data_id != change_next && data_id != 'current_date') {
                var dataValue = $('#popupReportData [name="' + element.name + '"]').val();
                var eleName = element.name.replace("[]", "");
                dataObj['columnsData'][eleName] = dataValue;
            }
        });
        getDataOnChangeDDl(dataObj, getVariable);
        // console.log('property',$('#popupReportData [name="property[]"]').val());
        console.log('formData',dataObj);
    });

    $(document).on('change', '.onchange', function () {

        var getVariable = $(this).attr('id');
        var modal_title = $('.modal-title').html();
        // console.log(getVariable);

        var selectedColumn = $(this).val();
        var selectedColumnName= $(this).attr('id');

        var portfolio_ids = $('#portfolio_id').val();
        //  var portfolioDynamic = $('#portfolio_id').attr('name');

        var property_ids = $('#property_id').val();
        //  var propertyDynamic = $('#portfolio_id').attr('name');
        var tenant_ids = $('#tenant_id').val();
        //  var tenantDynamic = $('#portfolio_id').attr('name');
        // var status = $('#status').val() == 'All'?null:$('#status').val();
        var status = ($('#status').val() == '')?'':$('#status').val();
        //   var statusDynamic = $('#portfolio_id').attr('name');


        var dataAttr = $('.listPopupdata .active_modal').attr('data-value');
        if (dataAttr != "" && dataAttr != undefined){
            var data = $.parseJSON(dataAttr);
        }

        /*  if(getVariable == 'portfolio_id'){
              var dataObj ={'columnsData':{'portfolio':portfolio_ids,'users':['1']}, 'tableDetail':data};
          } else {
              var dataObj ={'columnsData':{'portfolio':portfolio_ids, 'property':property_ids,'users':['1']}, 'tableDetail':data};
          }*/

        //if(getVariable == 'portfolio_id'){
        //     var dataObj ={'columnsData':{'portfolio':portfolio_ids,'users':['1']}, 'tableDetail':data};
        // } else {

        // }


// console.log(modal_title);
        switch(modal_title) {
            case 'Rent Roll Filters':
                // console.log('tenantData',data.tenant_id);
                if (portfolio_ids.length > 0) {
                    var dataObj ={'columnsData':{'users':['1'],'portfolio':portfolio_ids, 'property':property_ids, 'status':status}, 'tableDetail':data};
                    getDataOnChangeDDl(dataObj, getVariable);
                } else {
                    //  var propertyHTML = '<select type="multiselect" name="property[]" id="property_id" class="is_required form-control multiSelectfield onchange" multiple="multiple"><option value="">Select</option></select>';
                    $('#property_id').html('');
                    $('#property_id').multiselect('rebuild');
                    $('#status').val('');
                }
                break;
            case 'Tenant List Filters':
                if (portfolio_ids.length > 0) {
                    var dataObj ={'columnsData':{'users':['1'],'portfolio':portfolio_ids, 'property':property_ids}, 'tableDetail':data};
                    getDataOnChangeDDl(dataObj, getVariable);
                } else {
                    //  var propertyHTML = '<select type="multiselect" name="property[]" id="property_id" class="is_required form-control multiSelectfield onchange" multiple="multiple"><option value="">Select</option></select>';
                    $('#property_id').html('');
                    $('#property_id').multiselect('rebuild');
                }
                break;
            // case 'Completed Work Order Filters':
            case 'Pet Detail Report Filters':
                if (property_ids.length > 0) {
                    var dataObj ={'columnsData':{'users':['1'],'property':property_ids}, 'tableDetail':data};
                    getDataOnChangeDDl(dataObj, getVariable);
                } else {
                    //  var propertyHTML = '<select type="multiselect" name="property[]" id="property_id" class="is_required form-control multiSelectfield onchange" multiple="multiple"><option value="">Select</option></select>';
                    $('#tenant').html('');
                    $('#tenant').multiselect('rebuild');
                }
                break;
            case 'Print Envelope for Tenant Filters':
                // console.log(';dsdsdsdsdsds');
                if (property_ids.length > 0) {
                    var dataObj ={'columnsData':{'users':['1'],'property':property_ids}, 'tableDetail':data};
                    getDataOnChangeDDl(dataObj, getVariable);
                } else {
                    console.log(';dsdsdsdsdsds');
                    //  var propertyHTML = '<select type="multiselect" name="property[]" id="property_id" class="is_required form-control multiSelectfield onchange" multiple="multiple"><option value="">Select</option></select>';
                    $('#tenant').html('');
                    $('#tenant').multiselect('rebuild');
                }
                break;
            case 'Property List Filters':
                if ($('#status').val() != '') {
                    var dataObj = {'columnsData': {'users': ['1'], 'status': status}, 'tableDetail': data};
                    getDataOnChangeDDl(dataObj, getVariable);
                } else {
                    $('#owner_id').html('').multiselect('rebuild');
                }
                break;
            case 'Unit List Filters':
                if (portfolio_ids.length > 0) {
                    var dataObj ={'columnsData':{'users':['1'],'portfolio':portfolio_ids, 'property':property_ids}, 'tableDetail':data};
                    getDataOnChangeDDl(dataObj, getVariable);
                } else {
                    //  var propertyHTML = '<select type="multiselect" name="property[]" id="property_id" class="is_required form-control multiSelectfield onchange" multiple="multiple"><option value="">Select</option></select>';
                    $('#property').html('');
                    $('#property').multiselect('rebuild');
                }
                // } else {
                //     console.log(';dsdsdsdsdsds');
                //     //  var propertyHTML = '<select type="multiselect" name="property[]" id="property_id" class="is_required form-control multiSelectfield onchange" multiple="multiple"><option value="">Select</option></select>';
                //     $('#tenant').html('');
                //     $('#tenant').multiselect('rebuild');
                //}
                break;
            default:
                console.log('sdfsdgsdf');
            // code block
        }

    });

    function getDataOnChangeDDl(obj,getVariable) {
        $.ajax({
            type: 'POST',
            url: '/Reporting-Ajax',
            data: {
                class: 'reportsPopupData',
                action: 'fetchModalDataOnChange',
                obj: obj
            },
            success: function (response) {

                var data = $.parseJSON(response);
                if (data.status == "success"){
                    // var portfolioList = "";
                    // if (data.portfolio_id.length > 0 ) {
                    //     for (var i = 0; i < data.portfolio_id.length; i++) {
                    //         portfolioList = '<option value="' + data.portfolio_id[i].id + '">' + data.portfolio_id[i].portfolio_name + '</option>';
                    //         $('select[name="portfolio[]"]').html(portfolioList);
                    //     }
                    // }

                    data = data.data;
                    if(getVariable == 'portfolio_id') {
                        var propertyList = '';
                        if (data.property_id.length > 0 && data.property_id !== undefined) {
                            for (var i = 0; i < data.property_id.length; i++) {
                                if (data.property_id[i].id != null) {
                                    propertyList += '<option value="' + data.property_id[i].id + '">' + data.property_id[i].property_name + '</option>';
                                }
                            }
                            $('select[name="property[]"]').html(propertyList);
                            $('#modalDataClass select[name="property[]"]')
                                .multiselect('rebuild')
                                .multiselect('selectAll', false)
                                .multiselect("deselectAll", false);
                            $('select[name="tenant[]"]').html('');
                            $('#modalDataClass select[name="tenant[]"]')
                                .multiselect('rebuild')
                                .multiselect('selectAll', false)
                                .multiselect("deselectAll", false);
                        }
                    }

                    if(getVariable == 'property_id') {
                        if($('#property_id').val() != '') {
                            var tenantList = '';
                            if (data.tenant_id.length > 0) {
                                for (var i = 0; i < data.tenant_id.length; i++) {

                                    if (data.tenant_id[i].id != '') {
                                        tenantList += '<option value="' + data.tenant_id[i].id + '">' + data.tenant_id[i].name + '</option>';
                                    }
                                }
                                $('select[name="tenant[]"]').html(tenantList);
                                $('#modalDataClass select[name="tenant[]"]')
                                    .multiselect('rebuild')
                                    .multiselect('selectAll', false)
                                    .multiselect("deselectAll", false);
                            }
                        } else {
                            $('#tenant').html('').multiselect('rebuild');
                        }
                    }
                    if(getVariable == 'status') {
                        var change_next = $('#'+getVariable).attr('data_next');
                        // console.log('change_next',change_next);
                        switch(change_next) {
                            case 'tenant':
                                // console.log('tenantData',data.tenant_id);
                                if($('#tenant').val() !== undefined) {
                                    var tenantList = '';
                                    if (data.tenant_id.length > 0) {
                                        for (var i = 0; i < data.tenant_id.length; i++) {
                                            if (data.tenant_id[i].id != '') {
                                                tenantList += '<option value="' + data.tenant_id[i].id + '">' + data.tenant_id[i].name + '</option>';
                                            }
                                        }
                                        $('select[name="tenant[]"]').html(tenantList);
                                        $('#modalDataClass select[name="tenant[]"]')
                                            .multiselect('rebuild')
                                            .multiselect('selectAll', false)
                                            .multiselect("deselectAll", false);
                                    }
                                }
                                break;
                            case 'owner_id':
                                if($('#owner_id').val() !== undefined) {
                                    var tenantList = '';
                                    if (data != '' && data.owner_id.length > 0) {
                                        for (var i = 0; i < data.owner_id.length; i++) {
                                            if (data.owner_id[i].id != '') {
                                                tenantList += '<option value="' + data.owner_id[i].id + '">' + data.owner_id[i].name + '</option>';
                                            }
                                        }
                                        $('select[name="owner[]"]').html(tenantList);
                                        $('#modalDataClass select[name="owner[]"]')
                                            .multiselect('rebuild')
                                            .multiselect('selectAll', false)
                                            .multiselect("deselectAll", false);
                                    } else {
                                        $('#owner_id').html('').multiselect('rebuild');
                                    }
                                }
                                break;
                            default:
                                console.log('sdfsdgsdf');
                            // code block
                        }
                    }
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function checkRequiredFields(){
        var favorite = [];
        $('#popupReportData .is_required').each(function(key, value) {
            favorite.push($(this).attr('name'));
        });
        return favorite;
    }


    $(document).on('click','.submit_btn',function () {
        var favorite = checkRequiredFields();
        var rules = new Object();
        var messages = new Object();
        if (favorite.length > 0) {
            $.each(favorite, function (k, v) {
                rules[v] = {required: true};
            });
        }
        console.log('rules>>>', rules);
        $("#popupReportData").validate({
            rules: rules
        });

        if ($("#popupReportData").valid()) {
            // console.log($("#popupReportData").isValid());
            var title = $('#popupReportData .modal-title').text();
            //$('#vendorList').addClass('in');
            $('#vendorList').removeClass('in').removeAttr("style");
            // var htmlFilter = $('#vendorList').parent().html();
            var vendor_list_modal = $('#vendorList').wrap('<p>').parent().html();
            // var email_modal = $('#sentMailModalReport').wrap('<p>').parent().html();


            //var filterhtml=$('#filterss').html();
            var systemDate = new Date();
            var year = systemDate.getFullYear();
            var month = systemDate.getMonth()+1;
            var date = systemDate.getDate();
            var hour= systemDate.getHours();
            var minutes= systemDate.getMinutes();
            var seconds= systemDate.getSeconds();
            var dateformats = year +'-'+month+'-'+date+' '+hour+':'+minutes+':'+seconds;

            var formData = new FormData($('#popupReportData')[0]);
            formData.append('action', 'sendData');
            formData.append('class', 'reportsPopupData');
            formData.append('title', title);
            formData.append('htmlFilter', vendor_list_modal);
            formData.append('dateformats', dateformats);

            $.ajax({
                type: 'POST',
                url: '/Reporting-Ajax',
                data: formData,
                dateformats: dateformats,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    var data = $.parseJSON(response);
                    if (data.status == "success") {
                        //console.log("here");
                        //= window.location.href = base_url+data.url;
                        //   console.log(data.url);
                        $('#vendorList').modal('hide');

                        var url = document.URL.includes("/Reporting/Reporting");
                        //console.log(url);
                        if(url==true){
                            //console.log("here222");
                            window.open(base_url + data.url);
                        }else{
                            // console.log("here3333");
                            //$(".lease-outer").html('');
                            window.location.reload();

                            /* setTimeout(function () {
                                 console.log("hello");
                                 $(".container-fluid").find('.lease-container').eq(0).html('');
                                 $(".container-fluid").find('.bread-search-outer').eq(0).html('');
                             }, 1000);*/

                        }
                        //
                    } else {
                        toastr.error(data.message);
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });



    document.addEventListener ("keydown", function (zEvent) {

        if (zEvent.key === "t") {
            //  if($(".calander").is(":focus")){
            var focused = document.activeElement;
            if($(focused).hasClass('calander')){
                var name1=$(focused).attr("name");
                var date = $.datepicker.formatDate(jsDateFomat, new Date());
                var currencySign = currencySign;
                $("input[name='"+name1+"']").val(date);
            }
        }
        if (zEvent.key === "y") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')){
                var name1=$(focused).attr("name");
                var date = $.datepicker.formatDate(jsDateFomat, new Date());
                var result = new Date(date);
                result.setDate(result.getDate() - 1);
                var date1 = $.datepicker.formatDate(jsDateFomat, new Date(result));
                var currencySign = currencySign;
                $("input[name='"+name1+"']").val(date1);
            }
        }
        if (zEvent.key === "+") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                //console.log(name1);return;
                var value =$("input[name='"+name1+"']").val();
                var result = new Date(value);
                result.setDate(result.getDate() + 1);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(result));
                $("input[name='"+name1+"']").val(date);
            }
        }
        if (zEvent.key === "-") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                //console.log(name1);return;
                var value =$("input[name='"+name1+"']").val();
                var result = new Date(value);
                result.setDate(result.getDate() - 1);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(result));
                $("input[name='"+name1+"']").val(date);
            }
        }
        if (zEvent.key === "F9") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                var timestmp = new Date().setFullYear(new Date().getFullYear(), 0, 1);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='"+name1+"']").val(date);
            }
        } if (zEvent.key === "F10") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                var timestmp = new Date().setFullYear(new Date().getFullYear(), 11, 31);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='"+name1+"']").val(date);
            }
        }if (zEvent.key === "w") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                d = new Date();
                var day = d.getDay();
                var timestmp = new Date().setFullYear(new Date().getFullYear(),  new Date().getMonth(),d.getDate() - day + (day == 0 ? -6:1));
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='"+name1+"']").val(date);
            }
        }if (zEvent.key === "k") {
            var focused = document.activeElement;
            if($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                d = new Date();
                // var day = d.getDay();
                var timestmp = new Date().setFullYear(new Date().getFullYear(),  new Date().getMonth(),d.getDate() - (d.getDay() - 1) + 6);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='"+name1+"']").val(date);
            }
        }
        if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "m") {
            var focused = document.activeElement;
            if ($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                var timestmp = new Date().setFullYear(new Date().getFullYear(),  new Date().getMonth(), 1);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='" + name1 + "']").val(date);
            }
        }if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.key === "n") {
            var focused = document.activeElement;
            if ($(focused).hasClass('calander')) {
                var name1 = $(focused).attr("name");
                var timestmp = new Date().setFullYear(new Date().getFullYear(),  new Date().getMonth()+1, 0);
                var date = $.datepicker.formatDate(jsDateFomat, new Date(timestmp));
                $("input[name='" + name1 + "']").val(date);
            }
        }
    });

});
