$(document).ready(function () { 
   
    $(document).on('change','#gfrm1_equaity #equity_portfolio_id',function(){
        var val=$(this).val();

        $.ajax({
            type: 'POST',
            url: '/CommonReportsModal-Ajax',
            data: {
                class: 'balanceSheetReport',
                action: 'fetchModalBalanceDetailOnchange',
                id : val
            },
            success: ( response )=>{
             
                var data = $.parseJSON(response);
                $("#equity_balance_property_id").multiselect("destroy");
                $('#gfrm1_equaity #equity_balance_property_id').html(data.property_ddl);
                    $('#gfrm1_equaity #equity_balance_property_id').multiselect({
                        includeSelectAllOption: true,
                        allSelectedText: 'All Selected',
                        enableFiltering: true,
                        nonSelectedText: 'Select'
                    }).multiselect('selectAll', false).multiselect('updateButtonText');
            },
            error: ( err )=>{
                console.log(err);
            } 
            
        });
    });
});
