$(document).on('change','#ap_aging_detail #ap_aging_detail_portfolio_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'fetchModalBalanceDetailOnchange',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#ap_aging_detail #ap_aging_detail_property_id").multiselect("destroy");
            $('#ap_aging_detail #ap_aging_detail_property_id').html(data.property_ddl);
            setTimeout(function(){
                $('#ap_aging_detail #ap_aging_detail_property_id').trigger('change');
            },1000);
                $('#ap_aging_detail #ap_aging_detail_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});



//Getting Vendors on Change
$(document).on('change','#ap_aging_detail #ap_aging_detail_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangePropertyVendor',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#ap_aging_detail #ap_aging_detail_vendor_id").multiselect("destroy");
            $('#ap_aging_detail #ap_aging_detail_vendor_id').html(data.vendor_ddl);
                $('#ap_aging_detail #ap_aging_detail_vendor_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            // console.log(data.vendor_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});