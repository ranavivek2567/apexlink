$(document).ready(function () { 
    $(document).on('change','#gfrm1_exp_register #exp_register_portfolio_id',function(){
        var val=$(this).val();

        $.ajax({
            type: 'POST',
            url: '/CommonReportsModal-Ajax',
            data: {
                class: 'balanceSheetReport',
                action: 'fetchModalBalanceDetailOnchange',
                id : val
            },
            success: ( response )=>{
             
                var data = $.parseJSON(response);
                $("#security_property_id").multiselect("destroy");
                $('#gfrm1_exp_register #exp_register_property_id').html(data.property_ddl);
                setTimeout(function(){
                    $('#gfrm1_exp_register #exp_register_property_id').trigger('change');
                },1000);
                    $('#gfrm1_exp_register #exp_register_property_id').multiselect({
                        includeSelectAllOption: true,
                        allSelectedText: 'All Selected',
                        enableFiltering: true,
                        nonSelectedText: 'Select'
                    }).multiselect('selectAll', false).multiselect('updateButtonText');
            },
            error: ( err )=>{
                console.log(err);
            } 
            
        });
    });

    // vendor

    $(document).on('change','#gfrm1_exp_register #exp_register_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangePropertyVendor',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#exp_register_vendor_id").multiselect("destroy");
            $('#gfrm1_exp_register #exp_register_vendor_id').html(data.vendor_ddl);
                $('#gfrm1_exp_register #exp_register_vendor_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            // console.log(data.vendor_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});
});
