$(document).on('change','#expenses_by_vendor #expenses_by_vendor_portfolio_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'fetchModalBalanceDetailOnchange',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#expenses_by_vendor #expenses_by_vendor_property_id").multiselect("destroy");
            $('#expenses_by_vendor #expenses_by_vendor_property_id').html(data.property_ddl);
            setTimeout(function(){
                $('#expenses_by_vendor #expenses_by_vendor_property_id').trigger('change');
            },1000);
                $('#expenses_by_vendor #expenses_by_vendor_property_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});


//Getting Vendors on Change
$(document).on('change','#expenses_by_vendor #expenses_by_vendor_property_id',function(){
    var val=$(this).val();
    $.ajax({
        type: 'POST',
        url: '/CommonReportsModal-Ajax',
        data: {
            class: 'balanceSheetReport',
            action: 'onchangePropertyVendor',
            id : val
        },
        success: ( response )=>{
            // console.log(id.id);
            var data = $.parseJSON(response);
            $("#expenses_by_vendor #expenses_by_vendor_vendor_id").multiselect("destroy");
            $('#expenses_by_vendor #expenses_by_vendor_vendor_id').html(data.vendor_ddl);
                $('#expenses_by_vendor #expenses_by_vendor_vendor_id').multiselect({
                    includeSelectAllOption: true,
                    allSelectedText: 'All Selected',
                    enableFiltering: true,
                    nonSelectedText: 'Select'
                }).multiselect('selectAll', false).multiselect('updateButtonText');
            // console.log(data.vendor_ddl);
        },
        error: ( err )=>{
            console.log(err);
        } 
        
    });
});