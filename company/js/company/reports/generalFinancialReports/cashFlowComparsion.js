$(document).ready(function () { 
   
    $(document).on('change','#cash_flow_comparsions #cash_flow_comparsion_portfolio_id',function(){
        var val=$(this).val();

        $.ajax({
            type: 'POST',
            url: '/CommonReportsModal-Ajax',
            data: {
                class: 'balanceSheetReport',
                action: 'fetchModalBalanceDetailOnchange',
                id : val
            },
            success: ( response )=>{
             
                var data = $.parseJSON(response);
                $("#cash_flow_comparsion_property_id").multiselect("destroy");
                $('#cash_flow_comparsions #cash_flow_comparsion_property_id').html(data.property_ddl);
                    $('#cash_flow_comparsions #cash_flow_comparsion_property_id').multiselect({
                        includeSelectAllOption: true,
                        allSelectedText: 'All Selected',
                        enableFiltering: true,
                        nonSelectedText: 'Select'
                    }).multiselect('selectAll', false).multiselect('updateButtonText');
            },
            error: ( err )=>{
                console.log(err);
            } 
            
        });
    });
});
