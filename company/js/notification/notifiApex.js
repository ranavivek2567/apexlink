$(document).ready(function(){

    /**
     * call dynamic view listing User alerts data
     */
    getUserNotification();

    function getUserNotification(){
        $.ajax({
            type: 'post',
            url: '/notification-ajax',
            data: {
                class: 'notificationsAjax',
                action: 'notificationsView',
            },
            success: function (response) {
                var response = JSON.parse(response);
                console.log("UserNotification >>",response);
                if (response.status == 'success' && response.code == 200) {
                    $('.userNotification').html(response.html);
                } else {
                    toastr.error(response.message);
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

});