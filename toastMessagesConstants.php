<?php
//Constants for success
define('SUCCESS','success');
define('SUCCESS_CODE',200);
define('SUCCESS_FETCHED','Record Fetched Successfully.');
define('SUCCESS_ADDED','Record Added Successfully.');
define('SUCCESS_UPDATED','Record Updated Successfully.');
define('SUCCESS_DELETE_DOC','Document Deleted Successfully.');
//Constants for error/failed
define('ERROR','error');
define('ERROR_CODE',400);
define('ERROR_SERVER_CODE',500);
define('ERROR_SERVER_MSG','Internal Server Error!');
define('ERROR_NO_RECORD_FOUND','No Record Found!');
define('ERROR_VALIDATION','Validation Errors!');
define('ERROR_DELETE_DOC','Error occurred while deleting document.');
//Constants for warning
define('WARNING','warning');

//Constants for payment
define('SUBSCRIPTION_EXPIRED','Subscription Has Been Expired!');
define('SUBSCRIPTION_VALID','Subscription Is Valid.');
