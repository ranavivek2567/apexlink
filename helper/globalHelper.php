<?php
/**
 * Created by PhpStorm.
 * User: ranavivek2567
 * Date: 1/17/2019
 * Time: 3:23 PM
 */
if (basename($_SERVER['PHP_SELF']) == basename(__FILE__)) {
    //include_once ('../../constants.php');
    header('Location: ' . BASE_URL);
};

include_once( SUPERADMIN_DIRECTORY_URL."/vendor/autoload.php");
$stripe = \Stripe\Stripe::setApiKey(Live_Stripe_KEY);
////\Stripe\Stripe::setApiVersion("2019-08-14");
function createAccount($request){
//        print_r($request);
    try {
       // $user_id = $request['user_id'];
        $account = \Stripe\Account::create([
            "type" => "custom",
            "country" => "US",
            'email'=>$request["email"],
            "requested_capabilities" => ["card_payments", "transfers"],
            'business_type' =>$request["business_type"]
        ]);

        $account_id = $account->id;

        try{
            if ($account_id) {
                $updated_account = \Stripe\Account::update(
                    $account_id,
                    [
                        'tos_acceptance' => [
                            'date' => time(),
                            'ip' => $_SERVER['REMOTE_ADDR'] // Assumes you're not using a proxy
                        ],
                        [
                            'metadata' => ['internal_id' => '1'],
                        ]
                    ]
                );

            }
            return array('code' => 200, 'status' => 'success','account_id'=>$account_id,'message' => "Account Verified Successfully");
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    } catch (Exception $e){
        print_r($e->getMessage());
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}


function uploadDocuments($request){
    try {
        $fp = fopen($request["path"], 'r');
        $document_upload_id =  \Stripe\File::create([
            'purpose' => 'identity_document',
            'file' => $fp
        ]);
        return array('code' => 200, 'status' => 'success','document_id'=>$document_upload_id->id,'message' => "Account Verified Successfully");
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}


function enableAccount($request){
    try {
        $account_id = $request['stripe_account_id'];

        try{
            if ($account_id) {
                $bussiness_profile_array =[];
                $bussiness_profile_array = ['url'=>$request["website_url"],'product_description'=>$request["product_description"],'mcc'=>$request["mcc"],'support_email'=>$request["support_email"]];
                if(empty($bussiness_profile_array['mcc'])){
                    unset($bussiness_profile_array['mcc']);
                }
                $updated_account = \Stripe\Account::update(
                    $account_id,
                    [
                        'individual' => [
                            'first_name' => $request["first_name"],
                            'last_name' => $request["last_name"],// Assumes you're not using a proxy
                            'email' => $request["email"],
                            'address'=>['city'=>$request["city"],'country'=>$request["country"],'line1'=>$request["line1"],'line2'=>$request["line2"],'postal_code'=>$request["postal_code"],'state'=>$request["state"]],
                            'ssn_last_4'=>$request["ssn_last_4"],
                            'phone'=>$request["phone"],
                            'dob'=>['day'=>$request["day"],'month'=>$request["month"],'year'=>$request["year"]],
                            'verification'=>['document'=>["front"=>$request["document_front"],"back"=>$request["document_back"]]],
                        ],
                        'business_profile'=>$bussiness_profile_array,
//                        'business_profile'=>['url'=>$request["website_url"]],
                        'external_account'=>['object'=>'bank_account','country'=>$request["country"],'currency'=>$request["currency"],'account_holder_type'=>'individual','routing_number'=>
                            $request["routing_number"],'account_number'=>$request["account_number"]],
                    ]
                );
            }
            return array('code' => 200, 'status' => 'success','account_data'=>$updated_account,'message' => "Account Verified Successfully");
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}



function enableCompanyAccount($request){
    try {
        $account_id = $request['stripe_account_id'];
        try{
            if ($account_id) {
                try{
                $updated_account = \Stripe\Account::update(
                    $account_id,
                    [
                        'company' => [
                            'name' => $request["company_name"],
                            'address'=>['city'=>$request["company_city"],'country'=>$request["company_country"],'line1'=>$request["company_line1"],'line2'=>$request["company_line2"],'postal_code'=>$request["company_postal_code"],'state'=>$request["company_state"]],
                            'phone'=>$request["company_phone"],
                            'tax_id'=>$request["company_tax_id"],
                            'directors_provided'=>'false',
                            'owners_provided'=>'true'

                        ],
                        'business_profile'=>['url'=>$request["website_url"],'product_description'=>$request["product_description"],'mcc'=>$request["mcc"],'support_email'=>$request["support_email"]],
                        'external_account'=>['object'=>'bank_account','country'=>$request["country"],'currency'=>$request["currency"],'account_holder_type'=>'company','routing_number'=>
                            $request["routing_number"],'account_number'=>$request["account_number"]],
                    ]
                );
                if($updated_account){
                    $create_person = createPerson($request,$account_id);
                  if($create_person["code"] == 400){
                        return $create_person;
                  }

                }
                }catch (Exception $e){
                    return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
                }


            }
            return array('code' => 200, 'status' => 'success','account_data'=>$updated_account,'message' => "Account Verified Successfully");
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}


function updateCompanyAccount($request){
    try {
        $account_id = $request['stripe_account_id'];
        try{
            if ($account_id) {
                try{
                    $updated_account = \Stripe\Account::update(
                        $account_id,
                        [
                            'company' => [
                                'name' => $request["company_name"],
                                'address'=>['city'=>$request["company_city"],'country'=>$request["company_country"],'line1'=>$request["company_line1"],'line2'=>$request["company_line2"],'postal_code'=>$request["company_postal_code"],'state'=>$request["company_state"]],
                                'phone'=>$request["company_phone"],
                                'tax_id'=>$request["company_tax_id"],
                                'directors_provided'=>'false',
                                'owners_provided'=>'true'

                            ],
                            'business_profile'=>['url'=>$request["website_url"],'product_description'=>$request["product_description"],'mcc'=>$request["mcc"],'support_email'=>$request["support_email"]],
                            'external_account'=>['object'=>'bank_account','country'=>$request["country"],'currency'=>$request["currency"],'account_holder_type'=>'company','routing_number'=>
                                $request["routing_number"],'account_number'=>$request["account_number"]],
                        ]
                    );
                    if($updated_account){
                        $request["account_id"] = $account_id;
                       $all_persons =  \Stripe\Account::allPersons(
                            $account_id,
                            ['limit' => 3]
                        );
                        $person_id = '';
                       if(isset($all_persons["data"][0]) && !empty($all_persons["data"][0]) && !isset($all_persons["data"][0]["verification"]["status"]) ){
                           $person_id = $all_persons["data"][0]["id"];
                           $update_person = updatePerson($request,$account_id,$person_id);
                           if($update_person["code"] == 400){
                               return $update_person;
                           }
                       }
                    }
                }catch (Exception $e){
                    return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
                }


            }
            return array('code' => 200, 'status' => 'success','account_data'=>$updated_account,'message' => "Account Verified Successfully");
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}





function createPerson($request,$account_id){
    try{
    $person = \Stripe\Account::createPerson(
        $account_id,
        [
            'first_name' => $request["first_name"],
                            'last_name' => $request["last_name"],// Assumes you're not using a proxy
                            'email' => $request["email"],
                            'address'=>['city'=>$request["city"],'country'=>$request["country"],'line1'=>$request["line1"],'line2'=>$request["line2"],'postal_code'=>$request["postal_code"],'state'=>$request["state"]],
                            'ssn_last_4'=>$request["ssn_last_4"],
                            'phone'=>$request["phone"],
                            'dob'=>['day'=>$request["day"],'month'=>$request["month"],'year'=>$request["year"]],
                            'verification'=>['document'=>["front"=>$request["document_front"],"back"=>$request["document_back"]]],
            'relationship' =>['owner'=>true,'percent_ownership'=>'100','representative'=> true,'title'=>'CEO'],
        ]

    );
        return array('code' => 200, 'status' => 'success','account_data'=>$person,'message' => "Person Created Successfully");
      } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }
}

function updatePerson($request,$account_id,$person_id){
    try{
        $person = \Stripe\Account::updatePerson(
            $account_id,
            $person_id,
            [
                'first_name' => $request["first_name"],
                'last_name' => $request["last_name"],// Assumes you're not using a proxy
                'email' => $request["email"],
                'address'=>['city'=>$request["city"],'country'=>$request["country"],'line1'=>$request["line1"],'line2'=>$request["line2"],'postal_code'=>$request["postal_code"],'state'=>$request["state"]],
                'ssn_last_4'=>$request["ssn_last_4"],
                'phone'=>$request["phone"],
                'dob'=>['day'=>$request["day"],'month'=>$request["month"],'year'=>$request["year"]],
                'verification'=>['document'=>["front"=>$request["document_front"],"back"=>$request["document_back"]]],
                'relationship' =>['owner'=>true,'percent_ownership'=>'100','representative'=> true,'title'=>'CEO'],
            ]

        );
        return array('code' => 200, 'status' => 'success','account_data'=>$person,'message' => "Person Created Successfully");
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }
}




function createCustomer($request){
    try {
        $customer = \Stripe\Customer::create([
            'email' => $request["email"]
        ]);
        return array('code' => 200, 'status' => 'success','account_data'=>$customer->id,'customer_data'=>$customer,'message' => "Account Verified Successfully");
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }
}

function getCustomer($request){
    try {
        $customer = \Stripe\Customer::retrieve($request["customer_id"]);
        return array('code' => 200, 'status' => 'success','account_data'=>$customer->id,'customer_data'=>$customer,'message' => "Account Verified Successfully");

    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}

function getConnectedAccount($request){
    try {
        $connected_account =\Stripe\Account::retrieve(['id'=>$request["account_id"],'expand' => ['individual'],]);;
        return array('code' => 200, 'status' => 'success','account_data_id'=>$connected_account->id,'account_data'=>$connected_account,'message' => "Account Verified Successfully");

    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}










function createChargeToken($request){
//    print_r($request);exit;
    try {
//        $token = \Stripe\Token::create(
//            ["customer" => $request["customer_id"]],
//            ["stripe_account" => $request["stripe_account"]]);

//        $cards = \Stripe\Customer::allSources(
//            $request["customer_id"],
//            [
//                'limit' => 3,
//                'object' => 'card',
//            ]
//        );

        $token = \Stripe\Token::create(["customer"=>"cus_Fk2nOvtDhQV2Da"]);
        echo "<pre>";
        //  $token = \Stripe\Token::create( ["customer" => $request["customer_id"]]);
        print_r($token);

        return array('code' => 200, 'status' => 'success','token_id'=>$token->id,'message' => "Account Verified Successfully");

    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}

function createCharge($request){
//    print_r($request);exit;
    try {
        $charge = \Stripe\Charge::create([
            "amount" => $request["amount"],
            "currency" => $request["currency"],
            "source" => $request["token"],
            "customer" => $request["customer_id"],
            "transfer_data" => [
                "destination" => $request["destination_account_id"],
            ]
        ]);
        return array('code' => 200, 'status' => 'success','charge_id'=>$charge->id,'charge_data'=>$charge,'message' => "Payment has been done successfully.");

    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}



function createSuperAdminCharge($request){
//    print_r($request);exit;
    try {
        if(isset($request['company_info'])){
            $company_description = $request['company_info'];
        } else {
            $company_description = $_SESSION['company_name'] . " (" . $_SESSION['admindb_id'] . ")";
        }
        $charge = \Stripe\Charge::create([
            "amount" => $request["amount"],
            "currency" => $request["currency"],
            "source" => $request["token"],
            "customer" => $request["customer_id"],
            "description"=>$company_description
        ]);
        return array('code' => 200, 'status' => 'success','charge_id'=>$charge->id,'charge_data'=>$charge,'message' => "Account Verified Successfully");
        die;
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}


function addCard($request){
    try {
        try {
            $card_token = \Stripe\Token::create([
                'card' => [
                    'number' => $request["card_number"],
                    'exp_month' => $request["exp_month"],
                    'exp_year' => $request["exp_year"],
                    'cvc' => $request["cvc"]
                ]
            ]);

        } catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
        $card = \Stripe\Customer::createSource(
            $request["customer_id"],
            [
                'source' => $card_token->id,
            ]
        );
        return array('code' => 200, 'status' => 'success','charge_id'=>$card->id,'card_data'=>$card,'message' => "Card Attachted successfully");

    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}


function addBankAcoount ($request){
    try {
        try {
            $bank_token = \Stripe\Token::create([
                'bank_account' => [
                    'country' => $request["country"],
                    'currency' => $request["currency"],
                    'account_holder_name' => $request["account_holder_name"],
                    'account_holder_type' => $request["account_holder_type"],
                    'routing_number' => $request["routing_number"],
                    'account_number' => $request["account_number"],
                ]
            ]);

        } catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }

        try {
            $bank = \Stripe\Customer::createSource(
                $request["customer_id"],
                [
                    'source' => $bank_token->id,
                ]
            );
        }
        catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }

        try {
            $update_account = \Stripe\Customer::update(
                $request["customer_id"],
                ['default_source' => $bank->id]
            );

            $bank_account = \Stripe\Customer::retrieveSource(
                $request["customer_id"],
                $bank->id
            );

// verify the account
            $bank_account->verify(['amounts' => [32, 45]]);
        } catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }


        return array('code' => 200, 'status' => 'success','bank_id'=>$bank->id,'bank_data'=>$bank,'message' => "Account Verified Successfully");

    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}




function refundCharge ($request){
    try {
        $re = \Stripe\Refund::create([
            "charge" => $request["charge_id"],
            "reverse_transfer" => true
        ]);
        return array('code' => 200, 'status' => 'success','refund_id'=>$re->id,'refund_data'=>$re,'message' => "Refunded successfully");
        exit;
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}

function refundAllList($request) {
    try {
        \Stripe\Refund::all(['limit' => 3]);
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }
}



function saveStripeAccountDetails($account_id,$company_id){
    $upadte_data['stripe_account_id'] = $account_id;
    $sqlData = $this->createSqlColValPair($upadte_data);
    $query = "UPDATE users SET ".$sqlData['columnsValuesPair'];
    $connection = $this->getCompanyConnection($company_id);
    $stmt1 = $connection->prepare($query);
    $stmt1->execute();
}

/**
 * function to add bank account to customer (adding source to customer in stripe)
 * @param $customer_id
 * @param $stripe_account_token
 * @return array
 */
function addStripeSourceBank($customer_id,$stripe_account_token){
    try{
        $bank_account = \Stripe\Customer::createSource(
            $customer_id,
            [
                'source' => $stripe_account_token,
            ]
        );
        return array('code' => 200, 'status' => 'success','response'=>$bank_account,'message' => "Source added successfully");
    } catch(Exception $exception){
        return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());
    }
}
function getDefaultResource($cid,$sid)
{
        try {
            $detail= \Stripe\Customer::retrieveSource(
                $cid,
                $sid
                );
            return array('code' => 200, 'status' => 'success','response'=>$detail,'message' => "Source Retrieved successfully");
        } catch(Exception $exception){
            return array('code' => 400, 'status' => 'failed','message' => $exception->getMessage());
        }
}
function updateCustomer($customer_id,$sourceId)
{
    try {
        $update_account = \Stripe\Customer::update( "$customer_id", [
            'default_source' => "$sourceId"
        ]);
        return array('code' => 200, 'status' => 'success','response'=>$update_account,'message' => "Source Retrieved successfully");
    } catch (Exception $exception) {
        return array('code' => 400, 'status' => 'failed', 'message' => $exception->getMessage());
    }
}
function Allsource($customer_id,$paymentType)
{
    try {
        if($paymentType == 'card'){
            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 1,
                    'object' => 'card',
                ]
            );
        }else{
            $cards = \Stripe\Customer::allSources(
                "$customer_id",
                [
                    'limit' => 1,
                    'object' => 'bank_account',
                ]
            );

        }
        return $cards;

    } catch (Exception $exception) {
        return array('code' => 400, 'status' => 'failed', 'message' => $exception->getMessage());
    }
}


/**
 * function to update account  (adding source to customer in stripe)
 * @param $customer_id
 * @param $stripe_account_token
 * @return array
 */

function updateAccount($request){
    try {
        $account_id = $request['stripe_account_id'];
        // dd($account_id);
        try{
            if ($account_id) {
                $updated_account = \Stripe\Account::update(
                    $account_id,
                    [
                        'individual' => [

                            'address'=>['line1'=>$request["line1"],'line2'=>$request["line2"]],
                            'ssn_last_4'=>$request["ssn_last_4"],
                            'phone'=>$request["phone"],
                            'dob'=>['day'=>$request["day"],'month'=>$request["month"],'year'=>$request["year"]],
                            'verification'=>['document'=>["front"=>$request["document_front"],"back"=>$request["document_back"]]],
                        ],
                        'business_profile'=>['url'=>$request["website_url"],'product_description'=>$request["product_description"],'mcc'=>5734,'support_email'=>$request["support_email"]],
//                        'business_profile'=>['url'=>$request["website_url"]],
                        'external_account'=>['object'=>'bank_account','country'=>$request["country"],'currency'=>$request["currency"],'account_holder_type'=>'individual','routing_number'=>
                            $request["routing_number"],'account_number'=>$request["account_number"]],
                    ]
                );

            }
            return array('code' => 200, 'status' => 'success','account_data'=>$updated_account,'message' => "Account Verified Successfully");
        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}

function updatePropertyAccount($request){
    try {
        $account_id = $request['account_id'];
        try{
//            dd($account_id);
            if ($account_id) {
                $updated_account = \Stripe\Account::update(
                    $account_id,
                    [
                        'external_account'=>['object'=>'bank_account','country'=>'US','currency'=>'USD','account_holder_type'=>'individual','routing_number'=>
                            $request["routing_number"],'account_number'=>$request["account_number"],'default_for_currency'=>'1'],
                    ]
                );
//                dd($updated_account);
                return array('code' => 200, 'status' => 'success','account_data'=>$updated_account,'message' => "Account Verified Successfully");
            }else{
//                dd('$updated_account');
                return array('code' => 503, 'status' => 'warning','message' => "Setup Payment method first");
            }

        }catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    } catch (Exception $e){
        return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
    }

}

function getPerson($request)
{
    try {
        $all_persons =  \Stripe\Account::allPersons(
            $request["account_id"]
        );
        return array('code' => 200, 'status' => 'success','person_detail'=>$all_persons,'message' => "Person Detail fetched sucessully.");

    } catch (Exception $exception) {
        return array('code' => 400, 'status' => 'failed', 'message' => $exception->getMessage());
    }
}

    function  createCard($card,$cvv,$year,$month,$requestData){
        try {
            $request=$requestData['customer_id'];
            $token = \Stripe\Token::create([
                'card' => [
                    'number'    => $card,
                    'exp_month' => $month,
                    'exp_year'  => $year,
                    'cvc'       => $cvv,
                ],
            ]);
            $token_id = $token->id;
            $customer = \Stripe\Customer::retrieve($request);
            $card = $customer->sources->create(array("source" => $token_id));
            $result['message']    = "Card Added successfully";
            $result['status']   = 'true';
            return  $result;
        } catch (Exception $e){
            return array('code' => 400, 'status' => 'failed','message' => $e->getMessage());
        }
    }


function generateTransactionId()
{
    try {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < 12; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
        return 'TRAN-'.$randomString;
    } catch (Exception $exception) {
        return array('code' => 400, 'status' => 'failed', 'message' => $exception->getMessage());
    }
}




?>
