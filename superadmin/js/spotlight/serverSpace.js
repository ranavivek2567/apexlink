$(document).ready(function () {
    var serverChart = document.getElementById("moveInsCharts").getContext('2d');

    serverChartSpace();

    function serverChartSpace() {
        $.ajax({
            type: 'POST',
            url: '/serverSpace',
            data: {
                class: "serverSpace",
                action: "getSpaceGraph",
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    var freespaceData = parseFloat(response.freespace);
                    var totalspaceData = parseFloat(response.totalspace);
                    var usedSpaceGb = parseFloat(response.usedSpaceGb);
                    var myBarChart = new Chart(serverChart, {
                        type: 'bar',
                        label:['Server1','Server2'],
                        data: {
                            labels: ["Total space","Free Space","Used Space"],
                            datasets: [{
                                label: 'Series:1',
                                data: [totalspaceData,freespaceData,usedSpaceGb],
                                backgroundColor: [
                                    'rgb(0,128,0)',
                                    'rgb(214, 68, 27)'

                                ],
                                borderWidth: 1,
                                barThickness: 50
                            },{
                                label: 'Series:2',
                                data: [180,60,120],
                                backgroundColor: [
                                    'rgb(0,128,0)',
                                    'rgb(214, 68, 27)'

                                ],
                                borderWidth: 1,
                                barThickness: 50
                            }]
                        },
                        options: {
                            scales: {
                                xAxes: [{
                                    gridLines: {
                                        display: false
                                    },
                                }],
                                yAxes: [{
                                    display: true,
                                    ticks: {
                                        beginAtZero: true,
                                        //  steps: 50,
                                        stepValue: 0.5,
                                        stepSize: 10,
                                        //  scaleStepWidth : 10,
                                        // scaleSteps : 10,
                                        max: totalspaceData
                                    }
                                }]
                            }
                        }
                    });
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });

    }

})