$(document).ready(function () {
    $('.chartbtn').hide();
    $('.filterby').hide();


    $(document).on('click','.rptbtn',function () {
        $(this).hide();
        $(this).closest('.mob-change').find('.filterby').show();
        $(this).closest('.mob-change').find('.chartbtn').show();
       // $(this).closest('.mob-change').find('.Filterchart').hide();
        var  canvasID = $(this).attr('data-id');
        var tableID = $(this).attr('data-tableid');
        $('#'+canvasID).parent().children('.listing-download-icon').hide();

        switch(canvasID) {
            case "usersChart":
                $('#'+tableID).jqGrid('GridUnload');
                ActiveUsers();
                $('#user_report_filter .blue-btn').addClass('active_report_filter');
                $('#user_report_filter .clear-btn').attr('data-id','usersfilterby');
                break;
           case "paidFreeChart":
                $('#'+tableID).jqGrid('GridUnload');
               PaidFreeUsers();
               $('#user_report_filter .blue-btn').addClass('paidfree_report_filter');
               $('#user_report_filter .clear-btn').attr('data-id','activefilterby');
               break;
            case "planUsersDataCharts":
                $('#'+tableID).jqGrid('GridUnload');
               PlanUsers();
                $('#user_report_filter .blue-btn').addClass('paidfree_report_filter');
                $('#user_report_filter .clear-btn').attr('data-id','plansfilterby');
                break;
            case "revenueGeneratedChart":
                $('#'+tableID).jqGrid('GridUnload');
                RevenueGenerateUsers();
              //  $('#user_report_filter .blue-btn').addClass('paidfree_report_filter');
            //    $('#user_report_filter .clear-btn').attr('data-id','plansfilterby');
                break;
               // RevenueGenerateUsers();
            default:
                break;
        }

        var tableshowID = $(this).attr('tableshowID');
        $('.'+tableshowID).show();
        $('#'+canvasID).hide();

        var $grid = $("#"+tableID),
            newWidth = $grid.closest(".ui-jqgrid").parent().width();
        $grid.jqGrid("setGridWidth", newWidth, true);
    });

    $('.chartbtn').click(function () {

        $(this).hide();
        $(this).closest('.mob-change').find('.filterby').hide();
        $(this).closest('.mob-change').find('.rptbtn').show();
      //  $(this).closest('.mob-change').find('.Filterchart').show();
        var  canvasID = $(this).attr('data-id');
        var tableID = $(this).attr('data-tableid');

        var tableshowID = $(this).attr('tableshowID');
        $('#'+canvasID).show();
        $('#'+canvasID).parent().children('.listing-download-icon').show();
        $('.'+tableshowID).hide();
    });

    getActiveInactiveUsers();
    getPaidFreeUsers();
    getPlansUsers();
    getRevenueGenerated();



    /*  Print/Download  images Start*/
    var passId;
    $(document).on('click','.listing-download-icon',function () {
        $('.active_ul').removeClass('active_ul');
        $(this).addClass('active_ul');
        $(this).find('.listing-download-list').toggle();
    });

    $("body").click(function(e){
        if(e.target.className !== "listing-download-list"){
            $(".listing-download-list").hide();
        }
    });

    /*print options Start*/
    $(document).on('click','.printImageData',function () {
        var passId = $(this).parent().attr('data-id');
        printCanvas(passId);
    });

    function printCanvas(passId)
    {
        const dataUrl = document.getElementById(passId).toDataURL();

        let windowContent = '<!DOCTYPE html>';
        windowContent += '<html>';
        windowContent += '<head><title>Print canvas</title></head>';
        windowContent += '<body>';
        windowContent += '<img src="' + dataUrl + '">';
        windowContent += '</body>';
        windowContent += '</html>';

        const printWin = window.open('', '', 'width=' + screen.availWidth + ',height=' + screen.availHeight);
        printWin.document.open();
        printWin.document.write(windowContent);

        printWin.document.addEventListener('load', function() {
            printWin.focus();
            printWin.print();
            printWin.document.close();
            printWin.close();
        }, true);
    }
    /*print options End*/

    /*Download PNG Image*/
    $(document).on('click','.download',function () {
        var passId2 = $(this).parent().parent().attr('data-id');
        download_image(passId2);
    });
    function download_image(passId2) {
        var canvas = document.getElementById(passId2);
        image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        var link = document.createElement('a');
        link.download = "my-image.png";
        link.href = image;
        link.click();
    }
    /*Download PNG Image*/

    /*Download jpeg Image*/
    $(document).on('click', '.download1', function () {
        var passId3 = $(this).parent().parent().attr('data-id');
        download_image1(passId3);
    });
    function download_image1(passId3) {
        var canvas = document.getElementById(passId3);
        image = canvas.toDataURL('image/jpg',1.0).replace("image/jpg", "image/octet-stream");
        var link = document.createElement('a');
        link.download = "my-image.jpeg";
        link.href = image;
        link.click();
    }
    /*Download jpeg Image*/


    /*Receivables / Payables Start*/
    var usersChart = document.getElementById('usersChart');
    //alert(receivablesChart);debugger;
    var users = new Chart(usersChart, {
        type: 'pie',
        data: {
            datasets: [{
                label: '# of Votes',
                data: [100, 0],
                backgroundColor: [
                    'rgb(4, 180, 4)',
                    'rgb(1, 1, 223)'
                ],
                borderColor: [
                    'rgb(255,255,255)',
                    'rgb(255,255,255)'
                ],
                borderWidth: 1
            }],
            labels: ['Active Users', 'InActive Users']
        }
    });
    /* Receivables / Payables End */
    /*Receivables / Payables Start*/
    var paidFreeChart = document.getElementById('paidFreeChart');
    //alert(receivablesChart);debugger;
    var paidFree = new Chart(paidFreeChart, {
        type: 'pie',
        data: {
            datasets: [{
                label: '# of Votes',
                data: [100, 0],
                backgroundColor: [
                    'rgb(255, 128, 0)',
                    'rgb(223, 1, 215)'
                ],
                borderColor: [
                    'rgb(255,255,255)',
                    'rgb(255,255,255)'
                ],
                borderWidth: 1
            }],
            labels: ['Paid Users % ', 'Free Users %']
        }
    });
    /* Receivables / Payables End */

   /* var piedata = [{
        value: 20,
        color: "#637b85"
    }, {
        value: 30,
        color: "#2c9c69"
    }, {
        value: 40,
        color: "#dbba34"
    }, {
        value: 10,
        color: "#c62f29"
    }];
*/
   /* var canvas = document.getElementById("planUsersDataCharts");
    var ctx = canvas.getContext("2d");
    var myPieChart = new Chart(ctx,{
        type: 'pie',
        data: piedata,
       // options: options
    });*/
  //  new Chart(ctx).Pie(data);

    var userPlanChart = document.getElementById('planUsersDataCharts');
    var planUsersData = new Chart(userPlanChart, {
        type: 'pie',
        data: {
            datasets: [{
                label: '# of Votes',
                data: [100,20,30,40,60],
                backgroundColor: [
                    'rgb(255, 128, 0)',
                    'rgb(223, 1, 215)'
                ],
                borderWidth: 1,
            }],
            labels: ['1 plan','2 plan']
        }
    });

    /* Revenue Generated Start */
    var revenue = document.getElementById('revenueGeneratedChart');
    var RevenueGenerated = new Chart(revenue, {
        type: 'pie',
        data: {
            datasets: [{
                label: '# of Votes',
                data: [100,20,30,40,60],
                backgroundColor: [
                    'rgb(255, 128, 0)',
                    'rgb(223, 1, 215)'
                ],
                borderWidth: 1,
            }],
            labels: ['1 plan','2 plan']
        }
    });
    /* Revenue Generated End*/

    /* Pie Chart Lease Status ajax Start */
    function getActiveInactiveUsers() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getActiveInactiveUsers",
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    if (isNaN(response.dataActive && response.dataInactive)) {
                        return 0;
                    }
                    if (response.dataActive.length && response.dataInactive.length > 0) {
                        var dataInsStus = {
                            datasets: [{
                                //label: [,'sdadsa'],
                                data: [response.dataActive, response.dataInactive],
                                backgroundColor: [
                                    'rgb(4, 180, 4)',
                                    'rgb(1, 1, 223)'
                                ],
                                borderColor: [
                                    'rgb(255,255,255)',
                                    'rgb(255,255,255)'
                                ],
                                borderWidth: 1
                            }],
                            labels: ['Active % ', 'Inactive %']
                        }

                        users.data = dataInsStus;
                        users.update();

                    }
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
    /*Pie Chart Lease status ajax End*/

    /* Pie Chart Lease Status ajax Start */
    function getPaidFreeUsers() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getPaidFreeUsers",
            },
            success: function (response) {
                var response = JSON.parse(response);
              //  console.log('paidFree',response);
                if (response.status == 'success' && response.code == 200) {
                    if (isNaN(response.datapaidUsers && response.datafreeUsers)) {
                        return 0;
                    }
                    if (response.datapaidUsers.length && response.datafreeUsers.length > 0) {
                        //   console.log(response);
                        var dataInsStus = {
                            datasets: [{
                                //label: [,'sdadsa'],
                                data: [response.datapaidUsers, response.datafreeUsers],
                                backgroundColor: [
                                    'rgb(255, 128, 0)',
                                    'rgb(223, 1, 215)'
                                ],
                                borderColor: [
                                    'rgb(255,255,255)',
                                    'rgb(255,255,255)'
                                ],
                                borderWidth: 1
                            }],
                            labels: ['Paid Users % ', 'Free Users %']
                        }

                        paidFree.data = dataInsStus;
                        paidFree.update();

                    }
                }
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
    /*Pie Chart Lease status ajax End*/

    /* Pie Chart Lease Status ajax Start */
    var colors =[];
    function getPlansUsers() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getPlansUsers",
            },
            success: function (response) {
                var response = JSON.parse(response);
            //    console.log(response);

              var data = response.Users;
             var labels = response.Plans;
             for(i=0;i < data.length;i++){
            var rndmcolor = random_rgba();
                 colors.push(rndmcolor);
             }
                planUsersData.data.datasets[0].backgroundColor = colors;
          //   console.log( planUsersData.data.datasets[0].backgroundColor );
                planUsersData.data.labels = labels;
                planUsersData.data.datasets[0].data = data;
                planUsersData.update();
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
    /*Pie Chart Lease status ajax End*/

    function random_rgba() {
        var o = Math.round, r = Math.random, s = 255;
       var color = 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + r().toFixed(1) + ')';
     //  console.log(colors.includes(color));
          if(colors.includes(color)== 'true') {
              random_rgba();
          } else {
              return color;
          }
    }

    /* Revenue Generated */
    function getRevenueGenerated() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "getRevenueGenerateUsers",
            },
            success: function (response) {
               var response = JSON.parse(response);
                var data = response.data.Revenue;
                var countrys = [];
                var amt = [];
                var Revenuecolors = [];
                var i=0;
                for(i=0;i < data.length;i++){
                    countrys.push(data[i].country);
                    amt.push(data[i].totalAmt);
                    var rndmcolor = random_rgba();
                    Revenuecolors.push(rndmcolor);
                }
                RevenueGenerated.data.datasets[0].backgroundColor = Revenuecolors;
                RevenueGenerated.data.labels = countrys;
                RevenueGenerated.data.datasets[0].data = amt;
                RevenueGenerated.update();
            }, error: function (jqXHR, status, err) {
                console.log(err);
            },
        });
    }
    /* Revenue Generated*/

    /* JQGrid of ACtive users pie chart Start*/
    function ActiveUsers() {
        var sortColumn = 'users.updated_at';
        var table = 'users';
        var columns = ['Country', 'State/Province', 'Total No. of Users', 'Active Users'];
        var query = "SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country) as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as active FROM users WHERE ( users.deleted_at IS NULL AND users.status = '1') AND users.country <>'' AND users.state <>''  GROUP BY users.state";
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [{column:'status',value:'1',condition:'='}];
        var extra_columns = [];
        var columns_options = [{ name: 'Country', index: 'country',  width: 100,  searchoptions: {sopt: conditions}, table: table},
            {name: 'State/Province', index: 'state', width: 100, searchoptions: {sopt: conditions}, table: table},
           {name: 'Total No. of Users', index: 'count', width: 100, searchoptions: {sopt: conditions}, table: ''},
           {name: 'Active Users', index: 'active', width: 100, searchoptions: {sopt: conditions}, table: ''}
        ];
        var ignore_array = [];
        jQuery("#ActiveUserTable").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                query: query,
                columns_options: columns_options,
                ignore: ignore_array,
             //   joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Active Users",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false,onSearch: function(){
                    var grid = $("#ActiveUserTable"),f = [];
                   var label = $('.selectLabel').val();
                    var value = $('.input-elm').val();
                    var condition = $('.selectopts').val();
                    if(value != '') {
                        console.log("sdasa>>", value);

                        if (condition == 'eq') {
                            var where = label + ' = "'+value+'"';
                        }
                        if (condition == 'bw') {
                            var where = label + " LIKE '"+value+"%'";
                        }
                        if (condition == 'ew') {
                            var where = label + ' LIKE "%'+value+'"';
                        }
                        if (condition == 'cn') {
                            var where = label + " LIKE '%"+value+"%'";
                        }
                        if (condition == 'in') {
                            var where = label + " IN ('"+value +"')";
                        }
                        if(label == 'count'){
                            /*var query = "SELECT a.TotalUsers,a.id,a.country FROM   (SELECT DISTINCT(users.country) as country,users.id as id,(SELECT SUM(plan_price) FROM users u1 WHERE u1.country=users.country) as totalAmt FROM users WHERE users.country IS NOT NULL GROUP BY users.country) a WHERE  a."+where;*/
                            var query = "SELECT a.count,a.id,a.country,a.state,a.active FROM   (SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country) as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as active FROM users WHERE users.country <>'' AND users.state <>''  GROUP BY users.state) a WHERE  a."+where;
                        }  else if(label == 'active') {
                            var query = "SELECT a.active,a.id,a.country,a.state,a.count FROM   (SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country) as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as active FROM users WHERE  users.country <>'' AND users.state <>''  GROUP BY users.state) a WHERE  a."+where;
                        }else {
                            var query = "SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country) as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as active FROM users WHERE (  "+ where +" ) AND users.country <>'' AND users.state <>''  GROUP BY users.state";
                        }
                        console.log(query);
                        grid[0].p.search = false;
                        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),query:query});
                        grid.trigger("reloadGrid",[{page:1,current:true}]);
                    }
                }} // search options
        );
    }
    function activeusers(cellValue, options, rowObject) {
        if(rowObject !== undefined || rowObject !== 'null') {
            if(cellValue == ''){
                return '0';
            }
            return cellValue;
        }
    }
    /* JQGrid of Active users pie chart End*/

    function PaidFreeUsers() {
        var sortColumn = 'users.updated_at';
        var table = 'users';
        var columns = ['Country', 'State/Province', 'Total No. of Active Users', 'Paid Users'];
        var query = "SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.free_plan='0') as paid FROM users WHERE ( users.deleted_at IS NULL AND users.status = '1') AND users.country <>'' AND users.state <>''  GROUP BY users.state";
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [{column:'status',value:'1',condition:'='}];
        var extra_columns = [];
        var columns_options = [{ name: 'Country', index: 'country',  width: 100,  searchoptions: {sopt: conditions}, table: table},
            {name: 'State/Province', index: 'state', width: 100, searchoptions: {sopt: conditions}, table: table},
            {name: 'Total No. of Active Users', index: 'count', width: 100, searchoptions: {sopt: conditions}, table: ''},
            {name: 'Paid Users', index: 'paid', width: 100, searchoptions: {sopt: conditions}, table: '',formatter:paidUsers}
        ];
        var ignore_array = [];
        jQuery("#FreePaidUserTable").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                query: query,
                columns_options: columns_options,
                ignore: ignore_array,
                //   joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Active Users",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false,onSearch: function(){
                    var grid = $("#FreePaidUserTable"),f = [];
                    var label = $('.selectLabel').val();
                    var value = $('.input-elm').val();
                    var condition = $('.selectopts').val();
                    if(value != '') {
                        console.log("sdasa>>", value);

                        if (condition == 'eq') {
                            var where = label + ' = "'+value+'"';
                        }
                        if (condition == 'bw') {
                            var where = label + " LIKE '"+value+"%'";
                        }
                        if (condition == 'ew') {
                            var where = label + ' LIKE "%'+value+'"';
                        }
                        if (condition == 'cn') {
                            var where = label + " LIKE '%"+value+"%'";
                        }
                        if (condition == 'in') {
                            var where = label + " IN ('"+value +"')";
                        }

                        if(label == 'count'){
                            /*var query = "SELECT a.TotalUsers,a.id,a.country FROM   (SELECT DISTINCT(users.country) as country,users.id as id,(SELECT SUM(plan_price) FROM users u1 WHERE u1.country=users.country) as totalAmt FROM users WHERE users.country IS NOT NULL GROUP BY users.country) a WHERE  a."+where;*/
                            var query = "SELECT a.count,a.id,a.country,a.state,a.paid FROM   (SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.free_plan='0') as paid FROM users WHERE  users.country <>'' AND users.state <>''  GROUP BY users.state) a WHERE  a."+where;
                        }  else if(label == 'paid') {
                            var query = "SELECT a.paid,a.id,a.country,a.state,a.count FROM   (SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.free_plan='0') as paid FROM users WHERE  users.country <>'' AND users.state <>''  GROUP BY users.state) a WHERE  a."+where;
                        }else{
                            var query = "SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.free_plan='0') as paid FROM users WHERE ( " + where + ") AND users.country <>'' AND users.state <>''  GROUP BY users.state";
                        }
                        console.log("sdada",query);
                        grid[0].p.search = false;
                        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),query:query});
                        grid.trigger("reloadGrid",[{page:1,current:true}]);
                    }
                }
            } // search options
        );
    }

    function paidUsers(cellValue, options, rowObject) {
        if(rowObject !== undefined ) {
            if(cellValue == '' || cellValue == 'null'){
                return '0';
            }
            return cellValue;
        }
    }
    function RevenueGenerateUsers(){
            var sortColumn = 'users.updated_at';
            var table = 'users';
            var columns = ['Country', 'Revenue Generated'];
            var query = "SELECT DISTINCT(users.country),users.id,(SELECT SUM(plan_price) FROM users u1 WHERE u1.country=users.country) as totalAmt FROM `users` WHERE users.country IS NOT NULL  GROUP BY users.country";
            var conditions = ["eq", "bw", "ew", "cn", "in"];
            var extra_where = [{column:'status',value:'1',condition:'='}];
            var extra_columns = [];
            var columns_options = [{ name: 'Country', index: 'country',  width: 50,  searchoptions: {sopt: conditions}, table: table},
                {name: 'Revenue Generated', index: 'totalAmt', width: 50, searchoptions: {sopt: conditions}, table: table ,formatter:Amtformatter}
            ];
            var ignore_array = [];
            jQuery("#RevenueGenerateTable").jqGrid({
                url: '/Companies/List/jqgrid',
                datatype: "json",
                height: '100%',
                autowidth: true,
                colNames: columns,
                colModel: columns_options,
                pager: true,
                mtype: "POST",
                postData: {
                    q: 1,
                    class: 'jqGrid',
                    action: "listing_ajax",
                    table: table,
                    //  select: select_column,
                    query: query,
                    columns_options: columns_options,
                    ignore: ignore_array,
                    //   joins: joins,
                    extra_where: extra_where,
                    extra_columns: extra_columns,
                    deleted_at: 'true'
                },
                viewrecords: true,
                sortname: sortColumn,
                sortorder: "desc",
                sorttype: 'date',
                sortIconsBeforeText: true,
                headertitles: true,
                rowNum: '5',
                rowList: [5, 10, 20, 30, 50, 100, 200],
                caption: "Revenue Generated",
                pginput: true,
                pgbuttons: true,
                navOptions: {
                    edit: false,
                    add: false,
                    del: false,
                    search: true,
                    filterable: true,
                    refreshtext: "Refresh",
                    reloadGridOptions: {fromServer: true}
                }
            }).jqGrid("navGrid",
                {
                    edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
                },
                {}, // edit options
                {}, // add options
                {}, //del options
                {top: 10, left: 200, drag: true, resize: false,onSearch: function(){
                        var grid = $("#RevenueGenerateTable"),f = [];
                        var label = $('.selectLabel').val();
                        var value = $('.input-elm').val();
                        var condition = $('.selectopts').val();
                        var country = $('#revenue_report_filter .country').val();
                        if(value != '') {
                            console.log("sdasa>>", value);

                            if (condition == 'eq') {
                                var where = label + ' = "'+value+'"';
                            }
                            if (condition == 'bw') {
                                var where = label + " LIKE '"+value+"%'";
                            }
                            if (condition == 'ew') {
                                var where = label + ' LIKE "%'+value+'"';
                            }
                            if (condition == 'cn') {
                                var where = label + " LIKE '%"+value+"%'";
                            }
                            if (condition == 'in') {
                                var where = label + " IN ('"+value +"')";
                            }
                            if(label == 'totalAmt'){
                                var query = "SELECT a.totalAmt,a.id,a.country FROM   (SELECT DISTINCT(users.country) as country,users.id as id,(SELECT SUM(plan_price) FROM users u1 WHERE u1.country=users.country) as totalAmt FROM users WHERE users.country IS NOT NULL GROUP BY users.country) a WHERE  a."+where;
                            }  else {
                                var query = "SELECT DISTINCT(users.country),users.id,(SELECT SUM(plan_price) FROM users u1 WHERE u1.country=users.country) as totalAmt FROM `users` WHERE ( "+where+") AND users.country IS NOT NULL GROUP BY users.country";
                            }
                            console.log(query);
                            grid[0].p.search = false;
                            $.extend(grid[0].p.postData,{filters:JSON.stringify(f),query:query});
                            grid.trigger("reloadGrid",[{page:1,current:true}]);
                        }
                    }
                } // search options
            );
        }

        function Amtformatter(cellvalue, options, rowObject) {
            if (cellvalue != undefined) {
                var symbol = default_symbol;
                var amt = changeToFloat(cellvalue.toString());
                return symbol+amt;
            }
        }


    function PlanUsers(){
        var sortColumn = 'users.updated_at';
        var table = 'users';
        var columns = ['Company', 'Current Plan', 'Plan Date', 'No. of Units'];
        var conditions = ["eq", "bw", "ew", "cn", "in"];
        var extra_where = [{column:'status',value:'1',condition:'='}];
        var extra_columns = [];
        var joins = [{
            table: 'users',
            column: 'subscription_plan',
            primary: 'id',
            on_table: 'plans'
        }];
        var columns_options = [{ name: 'Company', index: 'company_name',  width: 100,  searchoptions: {sopt: conditions}, table: table},
            {name: 'Current Plan', index: 'plan_name', width: 100, searchoptions: {sopt: conditions}, table:'plans'},
            {name: 'Plan Date', index: 'created_at', width: 100, searchoptions: {sopt: conditions}, table: table },
            {name: 'No. of Units', index: 'number_of_units', width: 100, searchoptions: {sopt: conditions}, table:'plans'}
        ];
        var ignore_array = [];

        jQuery("#PlanTable").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                //  select: select_column,
                columns_options: columns_options,
                ignore: ignore_array,
                joins: joins,
                extra_where: extra_where,
                extra_columns: extra_columns,
                deleted_at: 'true'
            },
            viewrecords: true,
            sortname: sortColumn,
            sortorder: "desc",
            sorttype: 'date',
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: '5',
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Users Plan",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit: false, add: false, del: false, search: true, reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top: 10, left: 200, drag: true, resize: false} // search options
        );
    }

    $(document).on('click','.filterby',function () {
        var ReportFilter = $(this).attr('data-id');
        switch (ReportFilter) {
            case "usersfilterby":
                UsersReportFilter();
                break;
            case "paidFreefilterby":
                UsersReportFilter();
                break;
            case "useplanfilterby":
               UserPlanReportFilter();
                break;
            case "revenuefilterby":
              RevenueReportFilter();
                break;
            default:
                break;
        }
    });

    $(document).on('click','.reset_btn',function () {
        var ReportFilter =  $(this).attr('data-id');
        switch (ReportFilter) {
            case "usersfilterby":
                UsersReportFilter();
                break;
            case "activefilterby":
                UsersReportFilter();
                break;
            case "companyfilterby":
                UserPlanReportFilter();
                break;
            case "revenuefilterby":
                RevenueReportFilter();
                break;
            default:
                break;
        }

    });

    function  RevenueReportFilter() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "GetCountries",
            },
            dataType: 'json',
            success: function (response) {
                var option ="<option value='all'>Select Country</option>";
                response['data'].forEach(function(key){
                    if(key.name!= null) {
                        option += '<option value="' + key.name + '">' + key.name + '</option>';
                    }
                });
                $('#revenue_report_filter .country').html(option);
            }
        });
        return false;
    }
    function UsersReportFilter() {
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "GetCountries",
            },
            dataType: 'json',
            success: function (response) {
                var option ="<option value='all'>Select Country</option>";
                response['data'].forEach(function(key){
                    if(key.name!= null) {
                        option += '<option value="' + key.name + '">' + key.name + '</option>';
                    }
                });
                $('#user_report_filter .country').html(option);
                $('#user_report_filter #state').empty().append('<option selected="selected" value="all">Select State</option>');
            }
        });
        return false;
    }

    function UserPlanReportFilter(){
        $.ajax({
            type: 'POST',
            url: '/Spotlight',
            data: {
                class: "Spotlight",
                action: "GetCompaniesAndPlans",
            },
            dataType: 'json',
            success: function (response) {
              //  console.log(response);
                var option ="<option val=''>Select Company</option>";
                var plan ='';
                response['data']['Company'].forEach(function(key){
                    if(key.company_name!= null){
                        option += '<option value="'+key.company_name+'">'+key.company_name+'</option>';
                    }
                });

                $('#userplan_report_filter .company').html(option);

                response['data']['Plans'].forEach(function(key){
                    plan += '<option value="'+key.id+'">'+key.plan_name+'</option>';
                });

                $('#userplan_report_filter #plans').html(plan);
            }
        });
        return false;
    }

$(document).on('change','#user_report_filter .country',function(){
   var countryID = $(this).val();
    $.ajax({
        type: 'POST',
        url: '/Spotlight',
        data: {
            class: "Spotlight",
            action: "GetStateByCountry",
            id:countryID,
        },
        dataType: 'json',
        success: function (response) {
            var option = "<option value='all'>Select State</option>";
            response['data'].forEach(function (key) {
                if(key.name!= null) {
                option += '<option value="' + key.name + '">' + key.name + '</option>';
            }
            });
            $('#user_report_filter #state').html(option);
        }
    });
    return false;
    });

    $(document).on('click','.active_report_filter',function (e) {
        e.preventDefault();
        searchActiveUsersFilters();
        $('#UserReportFilter').modal('hide');
    });

    $(document).on('click','.paidfree_report_filter',function (e) {
        e.preventDefault();
        searchPaidUsersFilters();
        $('#UserReportFilter').modal('hide');
    });

    $(document).on('click','.plan_report_filter',function (e) {
        e.preventDefault();
        searchUsersPlanFilters();
        $('#UserPlanReportFilter').modal('hide');
    });

    $(document).on('click','.Revenuegenerated_report_filter',function (e) {
        e.preventDefault();
        searchRevenueFilters();
        $('#UserPlanReportFilter').modal('hide');
    });

//$(document).on('click','.fm-button-text',function () {

//});

    function searchActiveUsersFilters(){
        var grid = $("#ActiveUserTable"),f = [];
        var country = $('#user_report_filter .country').val();
        var state = $('#user_report_filter #state').val();
        var filterArray =  {'country':country,'state':state};
        // f.push({field: "users.country", op: "eq", data: country},{field: "users.state", op: "eq", data: state});
        // grid[0].p.search = true;
        // $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
        // grid.trigger("reloadGrid",[{page:1,current:true}]);
        var where = "users.deleted_at IS NULL AND users.status = '1' AND ";
        $.each(filterArray,function(key,value){
            if(value != 'all'){
                where += 'users.'+key+' = "'+value+'" AND ';
            }
        });

        where = where.slice(0, -4);
        var query = "SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country) as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as active FROM users WHERE ( "+where+") AND users.country <>'' AND users.state <>''  GROUP BY users.state";
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),query:query});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchPaidUsersFilters() {
        var grid = $("#FreePaidUserTable"),f = [];
        var country = $('#user_report_filter .country').val();
        var state = $('#user_report_filter #state').val();
        var filterArray =  {'country':country,'state':state};
        var where = "users.deleted_at IS NULL AND users.status = '1' AND ";
        $.each(filterArray,function(key,value){
            if(value != 'all'){
                where += 'users.'+key+' = "'+value+'" AND ';
            }
        });
        where = where.slice(0, -4);
        //users.deleted_at IS NULL AND users.status = '1') GROUP BY users.state
        var query = "SELECT users.id,users.state, users.country,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.status='1') as count,(SELECT count(id) FROM users as u1 WHERE u1.state=users.state AND u1.country=users.country AND u1.free_plan='0') as paid FROM users WHERE ( "+where+") AND users.country <>'' AND users.state <>''  GROUP BY users.state";
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),query:query});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }
    function searchUsersPlanFilters() {
     var grid = $("#PlanTable"),f = [];
     var company = $('#userplan_report_filter .company').val();
     var plan = $('#userplan_report_filter #plans').val();
     f.push({field: "users.company_name", op: "eq", data: company},{field: "users.subscription_plan", op: "eq", data: plan});
     grid[0].p.search = true;
     $.extend(grid[0].p.postData,{filters:JSON.stringify(f),status:'all'});
     grid.trigger("reloadGrid",[{page:1,current:true}]);
    }

    function searchRevenueFilters() {
        var grid = $("#RevenueGenerateTable"),f = [];
        var country = $('#revenue_report_filter .country').val();
       // alert(country);
        var query = "SELECT DISTINCT(users.country),users.id,(SELECT SUM(plan_price) FROM users u1 WHERE u1.country=users.country) as totalAmt FROM `users` WHERE users.country IS NOT NULL AND  users.country = '"+country+"' GROUP BY users.country";

        //   f.push({field: "users.country", op: "eq", data: country});
        grid[0].p.search = false;
        $.extend(grid[0].p.postData,{filters:JSON.stringify(f),query:query});
        grid.trigger("reloadGrid",[{page:1,current:true}]);
    }
});
