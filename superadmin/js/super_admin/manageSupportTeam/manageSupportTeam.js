

$(document).ready(function () {

    //Add new support team member
    $("#addNewMemberForm").submit(function( event ) {
        // console.log('cssds', addNewMemberForm);
        event.preventDefault();
        if ($('#addNewMemberForm').valid()) {
            var formData = $('#addNewMemberForm').serializeArray();

            var edit_support_id = $('#edit_support_id').val();

            var action ;

            if(edit_support_id)
            {
                action = 'update';

            } else {
                action = 'insert';
            }

            $.ajax({
                type: 'post',
                url: '/Support/SupportAjax',
                data: {
                    class       : "ManageSupportTeamAjax",
                    action      : action,
                    form        : formData,
                    id          : edit_support_id
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.status == "success")
                    {
                        window.location.href = "/Support/Index";
                        localStorage.setItem("rowcolor", 'add colour');   //add green border
                        toastr.error(response.message);
                    } else if(response.status == "warning")
                    {
                        toastr.error(response.message);
                    }else if(response.status == 'error' && response.code == 400){
                        // console.log('sdsdsdsds', response)
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('#'+key).text(value);
                        });
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }
    });

    /**  List Action Functions  */
    $(document).on('change', '.select_options', function() {
        var opt = $(this).val();
        var id = $(this).attr('data_id');

        if (opt == 'Edit' || opt == 'EDIT') {

            window.location.href = '/Support/AddSupportMember/' + id;
            $('#saveBtnId').text('Update');
            $('.clearForm').hide();
            $('.ResetForm').show();
        } else if (opt == 'Delete' || opt == 'DELETE' ) {
            bootbox.confirm("Are you sure want to delete this record ?", function (result) {
                if (result == true) {
                    $.ajax({
                        type: 'post',
                        url: '/Support/SupportAjax',
                        data: {
                            class: "ManageSupportTeamAjax",
                            action: "delete",
                            id: id
                        },
                        success: function (response) {
                            var response = JSON.parse(response);
                            console.log('response>>>', response);
                            if (response.status == 'success' && response.code == 200) {
                                toastr.success("The record deleted successfully.");
                                $('#SupportTeam-table').trigger( 'reloadGrid' );
                            } else if (response.status == 'error' && response.code == 400) {
                                $('.error').html('');
                                $.each(response.data, function (key, value) {
                                    $('.' + key).html(value);
                                });
                            }
                            $('#users-table').trigger('reloadGrid');
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // alert(key+value);
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                } else {
                    $('#Plans-table').trigger('reloadGrid');
                }
            });

        } else {
        }
    });




    $(document).on('click','.clearForm',function () {
        bootbox.confirm("Do you want to clear this form?", function (result) {
            if (result == true) {
                $('#addNewMemberForm')[0].reset();
            }
        });

    });

    $(document).on('click','.ResetForm',function () {
        bootbox.confirm("Do you want to reset this form?", function (result) {
            if (result == true) {
                location.reload();
            }
        });
    });

});


