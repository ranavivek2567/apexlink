$(document).ready(function(){
    var currentPagePath = window.location.pathname;

    $(document).on("click",".addapexlinknewbtn", function(){
        $(".add_apexlink_new").show();
        $(".apexlink_new_listing").hide();
    });

    // $(document).on("click",".cancelapexnew", function(){
    //     $(".add_apexlink_new").hide();
    //     $(".apexlink_new_listing").show();
    // });

    $(document).on('click','.cancelapexnew',function(){
        bootbox.confirm({
            message: "Do you want to cancel this action now?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    window.location.href = '/Setting/ApexLinkNew';
                }
            }
        });
    });

    $(document).on("click",".saveapexbtn", function(e){
        e.preventDefault();
        var title = $("input[name='title']").val();
        var date = $("input[name='date']").val();
        var time = $("input[name='time']").val();
        var desc = $("textarea[name='description']").val();
        if ($("#addapexnewform").valid()) {
            $.ajax({
                type: 'post',
                url: '/user-ajax',
                data: {
                    class: 'UserAjax', 
                    action: "createApexlinkList", 
                    title: title,
                    date: date,
                    time: time,
                    desc: desc,
                },
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        $(".add_apexlink_new").hide();
                        $("#addapexnewform")[0].reset();
                        $(".apexlink_new_listing").show();
                        toastr.success("Record is saved successfully");
                        // localStorage.setItem("rowcolor",true);
                        $('#apexlink_new_table').trigger( 'reloadGrid' );
                        setTimeout(function(){
                            jQuery('.table').find('tr:eq(1)').find('td:eq(0)').addClass("green_row_left");
                            jQuery('.table').find('tr:eq(1)').find('td:eq(5)').addClass("green_row_right");
                        }, 300);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                }
            });
        }
        
    });

    $("input[name='date']").datepicker({
        dateFormat: date_format,
        autoclose: true,
        changeMonth: true,
        changeYear: true
    });
    //$("input[name='time']").timepicker({ timeFormat: 'h:mm:ss' });
    $("input[name='time']").timepicker({ timeFormat: 'h:mm p' });

    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "True";
        else
            return "False";
    }

    // function statusFmatter1(cellvalue, options, rowObject){
    //     console.log(rowObject);
    //     //console.log(options);
    //     //<select class="form-control select_options" data_id="1"><option value="default">SELECT</option><option value="View">VIEW</option><option value="Edit">EDIT</option><option value="Enable">ENABLE</option><option value="Delete">DELETE</option></select>
    //     if (cellvalue == 1)
    //         return "True";
    //     else
    //         return "False";
    // }

    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if(rowObject['Is Enabled'] == '1')  select = ['View','Edit','Disable','Delete'];
            if(rowObject['Is Enabled'] == '0' || rowObject.Status == '')  select = ['View','Edit','Enable','Delete'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }


    function roleFmatter (cellvalue, options, rowObject){
        if (cellvalue == "1")
            return "Super Admin";
        else
            return "Super Admin";
    }

    //intializing jqGrid
    jqGrid('All');

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status) {
        console.log("im");
        
        var table = 'apexlink_list';
        var columns = ['Title', 'Date', 'Time', 'Description', 'Is Enabled', 'Action'];
        console.log(status);
        console.log('+++++++');
        if (status===0) {
            var select_column = ['View','Edit','Disable','Delete'];
            console.log(status);
            console.log("-------");
        }else {
           var select_column = ['View', 'Edit', 'Enable', 'Delete'];
            console.log(status);
            console.log("========");
        }
        var joins = [];//[{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['apexlink_list.deleted_at'];
        var columns_options = [
            {name:'Title',index:'title', width:90,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Date',index:'date', width:100,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'date'},
            {name:'Time',index:'time', width:80, align:"center",searchoptions: {sopt: conditions},table:table,change_type:'time'},
            {name:'Description',index:'description', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Is Enabled',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            {name:'Action',index:'select', width:80,align:"center",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: statusFmatter1, edittype: 'select',search:false, editoptions: { dataInit: function( elem ){
            } }}

        ];
        var ignore_array = [];
        jQuery("#apexlink_new_table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: 5,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Apexlink New List",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:100,left:200,drag:false,resize:false} // search options
        );
    }
                //enable text fields
    $(document).ready(function () {
        $('#disable_remove').click(function () {
            $('.form-control').removeAttr("disabled")
            $(".hide_btn").show();
            $(".feedback_div").hide();
            $(".edit_apex_new_btn").hide();

        });

    });


    $(document).on("change", "#gbox_apexlink_new_table .select_options", function (e) {
        var action = this.value;
        var id = $(this).attr('data_id');

        switch(action) {
            case "View":
                $(".apexlink_new_listing").hide();
                $(".list_hide").show();
                $('#edit_apexlink_new .form-control').attr("disabled",true);
                $("#edit_apexlink_new #user_id_hidden").val(id);
                $('#btnSubmit').removeAttr("disabled");
                // $(".edit_apex_new_btn").show();
                getApexlinkList(id);
                whatsNewFeedback(id);
                break;

            case "Edit":
                $(".apexlink_new_listing").hide();
                $(".list_hide").show();
                $(".listing_users").hide();
                $(".add_users_list").show();
                $("#edit_apexlink_new #user_id_hidden").val(id);
                $('#edit_apexlink_new .form-control').removeAttr("disabled");
                $(".hide_btn").show();
                $(".edit_apex_new_btn").hide();
                $("#apexlink_edit_case").hide();

                // $("#disable_remove").hide();
                  getApexlinkList(id);
                break;

            case "Disable":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        changeStatusApexNewUser(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Enable":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        changeStatusApexNewUser(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;
            case "Delete":
                //delete manage user
                bootbox.confirm("Do you want to delete this user?", function (result) {
                    if (result == true) {
                        deleteApexNewUser(id);
                    } else {
                       // window.location.href =  window.location.origin+'/Setting/ApexNewUser'
                    }
                });
                break;


            case "Change Password":
                //change manage user password
                $('#changepassword').modal({backdrop: 'static',keyboard: false})
                $("#changepassword").modal('show');
                $("#changepassword input[name='id']").val(id);
                break;
            default:
                window.location.href = base_url+'/Setting/ApexNewUser';
        }
        $('.select_options').prop('selectedIndex',0);
    });


    /*Delete apexlink new apexlink what's new*/

    function deleteApexNewUser(id) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ApexNewUserAjax",
                action: "deleteApexNewUser",
                apexnewuser_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("Record deleted successfully.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#apexlink_new_table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    

    function changeStatusApexNewUser(id,action) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ApexNewUserAjax",
                action: "changeStatusApexNewUser",
                apexnewuser_id: id,
                status_type: action,
            },
            success: function (response) {

                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The status has been changed.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#apexlink_new_table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    //////////////////////////////////////////////

   
    $("#updateUserForm").submit(function( event ) {
        event.preventDefault();
         if ($('#updateUserForm').valid()) {
            var formData = $('#updateUserForm').serializeArray();
            $.ajax({
                type: 'post',
                url: '/user-ajax',
                data: {class: 'UserAjax', action: "updateUser", form: formData},
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        localStorage.setItem("rowcolor",true)
                        window.location.href = '/Setting/ManageUser';
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                }
            });
         }
    });
    /**
     * To change the format of fax
     */
    if (currentPagePath == "/Setting/AddManageUser" || currentPagePath == "/Setting/ManageUser" ) {
        toggleSidebarMenu(1);
    }

    function toggleSidebarMenu(value){
        if (value == 1 ) {
            $(".default-sidebar").removeClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",true);
            $("#leftnav2").addClass("in");
            $("#leftnav2").removeAttr("style");
        }else{
            $(".default-sidebar").addClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",false);
            $("#leftnav2").removeClass("in");
            $("#leftnav2").css("height","0px");
        }
    }

    

    function deleteManageUser(id) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ManageUserAjax",
                action: "deleteManageUser",
                manageuser_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The record deleted successfully.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#users-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function getApexlinkList(id){
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "getApexlinkList",
                action: "getApexlinkList",
                user_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
               $("#edit_apexlink_new input[name='title']").val(response.title);
               $("#edit_apexlink_new input[name='date']").val(response.date);
                // $("#edit_apexlink_new input[name='time']").val(response.time);
                 $("#edit_apexlink_new input[name='time']").timepicker('setTime', response.time);
                $("#edit_apexlink_new textarea[name='description']").val(response.description);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function whatsNewFeedback(id){
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "whatsNewFeedback",
                action: "whatsNewFeedback",
                id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                responseData = response.data;
                var html = '';
                for (var i = 0; i < responseData.length; i++) {
                    //if (i == 0) {
                        html += '<tr>' +
                                '<td>'+responseData[i].company_name+'</td>' +
                                '<td>'+responseData[i].company_admin+'</td>' +
                                '<td>'+responseData[i].feedback+'</td>' +
                                '</tr>';
                   // }else{
                       // html += '<tr><td> </td></tr>';
                    //}
                }
                $("#apexNew_feedback tbody").html(html);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

   $(document).on("click",".updateForm", function(e){
       e.preventDefault();
       if($('#edit_apexlink_new').valid()) {
           var id = $("#edit_apexlink_new input[name='user_id_hidden']").val();
           var title = $("#edit_apexlink_new input[name='title']").val();
           var date = $("#edit_apexlink_new input[name='date']").val();
           var time = $("#edit_apexlink_new input[name='time']").val();
           var description = $("#edit_apexlink_new textarea[name='description']").val();

           update_apexlink_list(id, title, date, time, description);
       }

    });

    function update_apexlink_list(id,title,date,time,description){
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "update_apexlink_list",
                action: "update_apexlink_list",
                id: id,
                title: title,
                date: date,
                time: time,
                description: description
            },
            success: function (response) {
                var response = JSON.parse(response);

                if(response.status == 'success' && response.code == 200){
                    localStorage.setItem("rowcolor",true)
                    localStorage.setItem("apexlink_new_row_colour",true)

                    window.location.href = '/Setting/ApexLinkNew';

                } else if(response.status == 'error' && response.code == 400){
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.'+key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function changeStatusManageUser(id,action) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ManageUserAjax",
                action: "changeStatusManageUser",
                manageuser_id: id,
                status_type: action,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The status has been changed.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#users-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
});

// function to validate first password
$.validator.addMethod("pwcheck", function(value, element, error) {
    if (error) {
        $.validator.messages.pwcheck = '';
        return false;
    }
    return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(value)
});

$( "#change_pass_form" ).validate({
      rules: {
            password:{
                required:true,
                minlength:8,
                /*//pwcheck:true
                remote: {
                    url: "/user-ajax",
                    type: "post",
                    data: {
                      password: function() {
                        return $( "input[name='password']" ).val();
                      },
                      action: function() {
                        return "checkCurrentPassword";
                      }
                    },
                }*/
            },
            newpassword:{
                required:true,
                minlength:8,
                //pwcheck:true
            },
            confirmpassword:{
                minlength:8,
                equalTo :'#newpassword2'
            }
      },
     /*messages: {
        password: {
          remote: "Incorrect Password!"
        }
      }*/
});


    /* change password validation trigger */

    $(document).on("keyup","input[name='password']",function(){
        var password = $(this).val();
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "checkCurrentPassword",
                action: "checkCurrentPassword",
                password: password,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    $("input[name='password'], #password-error").addClass("valid ");
                    $("input[name='password']").removeClass("error");
                    $("#password-error").text("");
                } else if (response.status == 'error' && response.code == 400) {
                    $("input[name='password']").addClass("error");
                    $("input[name='password'], #password-error").removeClass("valid");
                    $("#password-error").text(response.message);
                   return false;
                }
            }
        });

    });

$(document).on('click','.clearForm',function () {
$('#addapexnewform')[0].reset();
});

$(document).on('click','.ResetApexlinknewForm',function () {
  var id =  $("#edit_apexlink_new #user_id_hidden").val();

    getApexlinkList(id);
});

});