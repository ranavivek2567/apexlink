$(document).ready(function(){
    var currentPagePath = window.location.pathname;

    //Ajax to create a new user
    $("#addUserForm").submit(function( event ) {
        event.preventDefault();
        if ($('#addUserForm').valid()) {
           var formData = $('#addUserForm').serializeArray();
            /*   access_key = '0849d89f9dde0bb65438cafd040e7a87';
               var work_phone =  $('input[name="mobile_number"]').val();
               var countryCode =  $('#countryCode').val();
              $.ajax({
                  type: 'post',
                  url: 'http://apilayer.net/api/validate?access_key='+access_key+"&number="+work_phone+"&country_code="+countryCode,
                  success: function (response) {
                      console.log(response.valid);
                      if(response.valid == true){*/
                        SubmitUserInformation(formData);
                   /* }else{
                        localStorage.setItem("Message", 'Please Enter valid Mobile Number');
                        localStorage.setItem("rowcolor",true)
                    }
                },
                error: function (data) {
                }
            });*/
        }
    });

    function SubmitUserInformation(formData){
          $.ajax({
                   type: 'post',
                   url: '/user-ajax',
                   data: {class: 'UserAjax', action: "createUser", form: formData},
                   success: function (response) {
                       var response = JSON.parse(response);
                       if(response.status == 'success' && response.code == 200){
                           localStorage.setItem("Message", "User added Successfully");
                           localStorage.setItem("rowcolor",true);
                           window.location.href = '/Setting/ManageUser';

                          // toastr.success("Record is saved successfully");
                       } else if(response.status == 'error' && response.code == 400){
                           $('.error').html('');
                           $.each(response.data, function (key, value) {
                               $('.'+key).html(value);
                           });
                       }
                   },
                   error: function (data) {
                   }
               });
          return false;
        }

    $("#updateUserForm").submit(function( event ) {
        event.preventDefault();
        if ($('#updateUserForm').valid()) {
            var formData = $('#updateUserForm').serializeArray();
            $.ajax({
                type: 'post',
                url: '/user-ajax',
                data: {class: 'UserAjax', action: "updateUser", form: formData},
                success: function (response) {

                    var response = JSON.parse(response);
                    if(response.status == 'success' && response.code == 200){
                        localStorage.setItem("rowcolor",true)
                        localStorage.setItem("Message", "User updated Successfully");
                        window.location.href = '/Setting/ManageUser';
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                }
            });
        }
    });
    /**
     * To change the format of fax
     */
    jQuery('.number_format').mask('000-000-0000', {reverse: true});

    if (currentPagePath == "/Setting/AddManageUser" || currentPagePath == "/Setting/ManageUser" ) {
        toggleSidebarMenu(1);
    }

    function toggleSidebarMenu(value){
        if (value == 1 ) {
            $(".default-sidebar").removeClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",true);
            $("#leftnav2").addClass("in");
            $("#leftnav2").removeAttr("style");
        }else{
            $(".default-sidebar").addClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",false);
            $("#leftnav2").removeClass("in");
            $("#leftnav2").css("height","0px");
        }
    }

    /*jqgrid*/
    $('#manage-user-status').on('change',function(){
        var selected = this.value;
        console.log(selected);
        $('#users-table').jqGrid('GridUnload');
        jqGrid(selected);
    });

    function statusFmatter (cellvalue, options, rowObject){
        if (cellvalue == 1)
            return "Active";
        else
            return "InActive";
    }

    function roleFmatter (cellvalue, options, rowObject){
        if (cellvalue == "1")
            return "Super Admin";
        else
            return "Super Admin";
    }
    function statusFmatter1 (cellvalue, options, rowObject){
        if(rowObject !== undefined) {
            var select = '';
            if($(cellvalue).attr('data_id') == '1' || $(cellvalue).attr('data_id') == '2' || $(cellvalue).attr('data_id') == '3') return select;
            if(rowObject['Status'] == '1')  select = ['Edit','Deactivate','Delete','Change Password'];
            if(rowObject['Status'] == '0' || rowObject.Status == '')  select = ['Edit','Activate','Delete','Change Password'];
            var data = '';
            if(select != '') {
                var data = '<select class="form-control select_options" data_id="' + rowObject.id + '"><option value="default">SELECT</option>';
                $.each(select, function (key, val) {
                    data += '<option value="' + val + '">' + val.toUpperCase() + '</option>';
                });
                data += '</select>';
            }
            return data;
        }
    }
    //intializing jqGrid
    jqGrid('All');

    /**
     * jqGrid Intialization function
     * @param status
     */
    function jqGrid(status) {
        var table = 'users';
        var columns = ['Name', 'Email', 'Phone', 'Last login', 'Role', 'Status', 'Action'];
        if (status==0) {
            var select_column = ['Edit','Activate','Delete','Change Password'];
        }else {
            var select_column = ['Edit','Deactivate','Delete','Change Password'];
        }


        // var select_column = ['Edit','Deactivate','Delete','Change Password'];
        var joins = [];//[{table:'users',column:'subscription_plan',primary:'id',on_table:'plans'}];
        var conditions = ["eq","bw","ew","cn","in"];
        var extra_columns = ['users.deleted_at'];
        var columns_options = [
            {name:'Name',index:'name', width:90,align:"center",searchoptions: {sopt: conditions},table:table,change_type:'name'},
            {name:'Email',index:'email', width:100,align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Phone',index:'mobile_number', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Last login',index:'created_at', width:80, align:"center",searchoptions: {sopt: conditions},table:table},
            {name:'Role',index:'user_type', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:roleFmatter},
            {name:'Status',index:'status', width:80,align:"center",searchoptions: {sopt: conditions},table:table,formatter:statusFmatter},
            {name:'Action',index:'select', width:80,align:"center",sortable:false,cellEdit: true, title: false, cellsubmit: 'clientArray', editable: true,formatter: statusFmatter1, edittype: 'select',search:false, editoptions: { dataInit: function( elem ){
                    } }}

        ];
        var ignore_array = [{column:'users.user_type',value:'0'},{column:'users.id',value:'1'},{column:'users.id',value:'2'},{column:'users.id',value:'3'}];
        jQuery("#users-table").jqGrid({
            url: '/Companies/List/jqgrid',
            datatype: "json",
            height: '100%',
            width: '100%',
            autowidth: true,
            colNames: columns,
            colModel: columns_options,
            pager: true,
            mtype: "POST",
            postData: {
                q: 1,
                class: 'jqGrid',
                action: "listing_ajax",
                table: table,
                select: select_column,
                columns_options: columns_options,
                status: status,
                ignore:ignore_array,
                joins:joins,
                extra_columns:extra_columns
            },
            viewrecords: true,
            sortname: 'updated_at',
            sortorder: "desc",
            sortIconsBeforeText: true,
            headertitles: true,
            rowNum: 5,
            rowList: [5, 10, 20, 30, 50, 100, 200],
            caption: "Manage Users",
            pginput: true,
            pgbuttons: true,
            navOptions: {
                edit: false,
                add: false,
                del: false,
                search: true,
                filterable: true,
                refreshtext: "Refresh",
                reloadGridOptions: {fromServer: true}
            }
        }).jqGrid("navGrid",
            {
                edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
            },
            {}, // edit options
            {}, // add options
            {}, //del options
            {top:100,left:200,drag:false,resize:false} // search options
        );
    }





    $(document).on("change", "#users-table .select_options", function (e) {
        var action = this.value;
        var id = $(this).attr('data_id');
        switch(action) {
            case "Edit":
                $(".listing_users").hide();
                $(".add_users_list").show();
                var  hello = getUserDetails(id);
                // console.log(hello);
                // console.log("----------");
                $(document).on("click",".main-tabs .default_tab a", function(e){
                    $(".add_users_list").show();
                    $(".listing_users").hide();
                });
                $(document).on("click",".main-tabs .clock_tab a", function(e){
                    $(".listing_users").show();
                    $(".add_users_list").hide();
                });
                break;
            case "Deactivate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        changeStatusManageUser(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ManageUser'
                    }
                });
                break;
            case "Activate":
                //Deactivate or Activate manage user
                bootbox.confirm("Do you want to "+action+" this user?", function (result) {
                    if (result == true) {
                        changeStatusManageUser(id,action);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ManageUser'
                    }
                });
                break;
            case "Delete":
                //delete manage user
                bootbox.confirm("Do you want to delete this user?", function (result) {
                    if (result == true) {
                        deleteManageUser(id);
                    } else {
                        window.location.href =  window.location.origin+'/Setting/ManageUser'
                    }
                });
                break;
            case "Change Password":
                //change manage user password
                var validator = $( "#change_modal_password" ).validate();
                validator.resetForm();
                $('#change_modal_password').trigger('reset');
                $('#changepassword').modal({backdrop: 'static',keyboard: false})
                $("#changepassword").modal('show');
                $("#changepassword input[name='id']").val(id);
                break;
            default:
                window.location.href = base_url+'/Setting/ManageUser';
        }
    });

    function deleteManageUser(id) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ManageUserAjax",
                action: "deleteManageUser",
                manageuser_id: id
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The record deleted successfully.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#users-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



    $(document).on("click", ".change_password", function(){
        var userId = $("#changepassword input[name='id']").val();
        var newPassword = $("input[name='new_password']").val();
        var confirmPassword = $("input[name='confirm_password']").val();
        if($('#change_modal_password').valid()) {
            changepassword(userId,newPassword,confirmPassword);
        }
    });
    function changepassword(userId,newPassword,confirmPassword){
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "changepassword",
                action: "changePassword_users",
                id: userId,
                nPassword: newPassword,
                cPassword: confirmPassword
            },
            success: function (response) {
                var response = JSON.parse(response);

                if (response.code == 400) {
                    if (response.data.NewPasswordErr[0] != "") {
                        $("#passwordErr").text(response.data.NewPasswordErr[0]);
                    }
                    if (response.data.NewPasswordErr[0] != "") {
                        $("#npasswordErr").text(response.data.NewPasswordErr[0]);
                    }
                }else if(response.code == 200){
                    $("#passwordErr, #npasswordErr").text('');
                    $("#changepassword").modal('hide');
                    toastr.success("Password changed successfully");
                }
                $('#users-table').trigger( 'reloadGrid' );

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    function changeStatusManageUser(id,action) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "ManageUserAjax",
                action: "changeStatusManageUser",
                manageuser_id: id,
                status_type: action,
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("The status has been changed.");
                } else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
                $('#users-table').trigger( 'reloadGrid' );
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    jQuery.validator.setDefaults({
        debug: true,
        success: "valid"
    });

// function to validate first password
    $.validator.addMethod("pwcheck", function(value, element, error) {
        if (error) {
            $.validator.messages.pwcheck = '';
            return false;
        }
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/.test(value)
    });
    $.validator.addMethod("password_regex", function(value, element) {
        var password_regex = this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9])/.test(value);
        return password_regex;
    }, "* Atleast one aphanumeric and a numeric character required");

    $( "#change_pass_form" ).validate({
        rules: {
            password:{
                required:true,
                password_regex : true
            },
            newpassword:{
                required:true,
                minlength: 8,
                password_regex : true
            },
            confirmpassword:{
                required:true,
                minlength: 8,
                password_regex : true,
                equalTo :'#newpassword2'
            }
        },
        messages: {
            newpassword: {
                minlength: "* Minimum 8 characters allowed",
            },
            confirmpassword: {
                equalTo: "* Fields do not match",
                minlength: "* Minimum 8 characters allowed",
            },

        }
    });


    /* change password validation trigger */

    // $(document).on("keyup","input[name='password']",function(){
    //     var password = $(this).val();
    //     $.ajax({
    //         type: 'post',
    //         url: '/user-ajax',
    //         data: {
    //             class: "checkCurrentPassword",
    //             action: "checkCurrentPassword",
    //             password: password,
    //         },
    //         success: function (response) {
    //             var response = JSON.parse(response);
    //             if (response.status == 'success' && response.code == 200) {
    //                 $("input[name='password'], #password-error").addClass("valid ");
    //                 $("input[name='password']").removeClass("error");
    //                 $("#password-error").text("");
    //             } else if (response.status == 'error' && response.code == 400) {
    //                 $("input[name='password']").addClass("error");
    //                 $("input[name='password'], #password-error").removeClass("valid");
    //                 $("#password-error").text(response.message);
    //                 return false;
    //             }
    //         }
    //     });
    //
    // });

    $(document).on("click",".changePassBtn", function(e){
        e.preventDefault();
        var current_password = $("#current_password").val();
        var password = $("#newpassword2").val();
        var cpassword = $("input[name='confirmpassword']").val();

        var validation = $("#change_pass_form").valid();
        // console.log("current_password>>>>>>"+current_password);
        if (validation !== false) {
            changeAdminPassword(current_password, password, cpassword);
        }
        return false;
    });

    function changeAdminPassword(current_password, password, cpassword) {
        $.ajax({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "changePassword",
                action: "changePassword",
                current_password: current_password,
                nPassword: password,
                cPassword: cpassword,
                id: '',
            },
            success: function (response) {
                var response = JSON.parse(response);
                if (response.status == 'success' && response.code == 200) {
                    toastr.success("Password changed successfully");
                    $("#current_password").val('');
                    $("#newpassword2").val('');
                    $("input[name='confirmpassword']").val('');
                } else if (response.status == 'error' && response.code == 503) {
                    toastr.error(response.message);
                }else if (response.status == 'error' && response.code == 400) {
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        $('.' + key).html(value);
                    });
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }

    $(document).ready(function() {
        $(document).on("click",".cancel_change_password",function(){
            bootbox.confirm("Do you want to cancel this action now ?", function (result) {
                if (result == true) {
                    window.location.href = '/Setting/ManageUser';
                }
            });
        });


    });

    $(document).on("click","#cancel_modal1 .modal-footer .yes-cancel",function(){
        //alert("here");
        window.location.href = "/Dashboard/Dashboard";
    });

    $(document).on("click","#cancel_modal_add .modal-footer .yes-cancel",function(){
        //alert("here");
        window.location.href = "/Setting/AddManageUser";
    });

    $(document).ready(function() {
        $(document).on('click','.yes-cancel-time',function(){
            bootbox.confirm({
                message: "Do you want to cancel this action now?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $(".listing_users").show();
                        $(".add_users_list").hide();
                        window.location.href = '/Setting/ManageUser';
                    }
                }
            });
        });

    });

    $(document).ready(function() {
        $(document).on('click','.cancel_manage_user',function(){
            bootbox.confirm({
                message: "Do you want to cancel this action now?",
                buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
                callback: function (result) {
                    if (result == true) {
                        $(".listing_users").show();
                        $(".add_users_list").hide();
                        window.location.href = '/Setting/ManageUser';
                    }
                }
            });
        });
    });

    $('.modal_changepass').on('click',function(){
        document.getElementById("change_modal_password").reset();
        var validator = $( "#change_modal_password" ).validate();
        validator.resetForm();
        $('#users-table').trigger( 'reloadGrid' );
    });

});


$(document).on('click','.clearFormReset',function () {
    bootbox.confirm({
        message: "Do you want to clear this form ?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                $('#addUserForm')[0].reset();
            }
        }
    });
});

$(document).on('click','.FormReset',function () {
    $('#change_pass_form')[0].reset();
});

$(document).on('click','.Reset_Form',function () {
   var id = $('#user_data_id').val();
    getUserDetails(id);

});
function getUserDetails(id){
    $.ajax({
        type: 'post',
        url: '/user-ajax',
        data: {
            class: "getUserDetails",
            action: "getUserDetails",
            user_id: id
        },
        success: function (response) {
            var response = JSON.parse(response);
            console.log(response.status);
            $("#updateUserForm input[name='first_name']").val(response.first_name);
            $("#updateUserForm input[name='mi']").val(response.mi);
            $("#updateUserForm input[name='last_name']").val(response.last_name);
            $("#updateUserForm input[name='maiden_name']").val(response.maiden_name);
            $("#updateUserForm input[name='nick_name']").val(response.nick_name);
            $("#updateUserForm input[name='email']").val(response.email);
            $("#updateUserForm select[name='status'] option[value=" + response.status+"]").attr("selected","selected") ;
            $("#updateUserForm input[name='work_phone']").val(response.work_phone);
            $("#updateUserForm select[name='country'] option[value=" + response.country+"]").attr("selected","selected") ;
            $("#updateUserForm input[name='mobile_number']").val(response.mobile_number);
            $("#updateUserForm input[name='fax']").val(response.fax);
            $("#updateUserForm input[name='id']").val(id);
            $("#updateUserForm #status").val(response.status);
            $("#updateUserForm input[name='home_phone']").val(response.home_phone);
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                $('#' + key + '_err').text(value);
            });
        }
    });
}

