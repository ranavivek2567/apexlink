/**
 *Get location on the basis of zipcode
 */

function getAddressInfoByZip(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    response(addr);
                } else {
                    response({success:false});
                }
            } else {
                response({success:false});
            }
        });
    } else {
        response({success:false});
    }
}

function getAddressInfoByZipforFinance(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    responseFinance(addr);
                } else {
                    responseFinance({success:false});
                }
            } else {
                responseFinance({success:false});
            }
        });
    } else {
        responseFinance({success:false});
    }
}
/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function responseFinance(obj){
    //console.log(obj);
    if(obj.success){
        $('#fcity').val(obj.city);
        $('#fstate').val(obj.state);
        $('#fcountry').val(obj.country);
    } else {
        $('#fcity').val('');
        $('#fstate').val('');
        $('#fcountry').val('');
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response(obj){
    //console.log(obj);
    if(obj.success){
        $('#city').val(obj.city);
        $('#state').val(obj.state);
        $('#country').val(obj.country);
    } else {
        $('#city').val('');
        $('#state').val('');
        $('#country').val('');
    }
}

/**
 * Set the fixed length for fields
 */
$(document).on('input', '.fixedLength',function () {

    var fixkey = $(this).val();
    var fixedLength = $(this).attr('fixedLength');
    fixedLength = parseInt(fixedLength)

    if(fixkey && fixedLength)
    {
        //  console.log(fixkey,fixedLength)
        if(fixkey.length < fixedLength)
        {
            document.onkeydown = function (e) {

                var key = e.charCode || e.keyCode;
                if ((key < 48 || key > 57) || (key < 96 || key > 105) ) {
                    e.preventDefault();
                } else {

                }
            }
        }
    }

});

$(document).on('blur input','#password,#password_confirmation',function () {
    var password = $('#password').val();
    if(password.length < 8)
    {
        $('.passwordErr').html('Minimum 8 characters allowed');
        $('#password_checker #result').hide();
        $('span#result .bar').hide();
        $('span#result #strength').text('');
        $('span#result').removeClass();
    } else {
        $('#password_checker #result').show();
    }

    var cpassword = $('#password_confirmation').val();
    // console.log(password_len,cpassword_len);
    if((password.length <= cpassword.length) && password.length > 4)
    {
        if(password != cpassword)
        {
            $('.password_confirmationErr').html('Password does not match with Confirm Password');
        }
    }
    if(password == cpassword)
    {
        $('.password_confirmationErr').html('');
    }

});

$(function () {

    $('#addCompanyForm').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    /**
     * If the letter is not digit in fax then don't type anything.
     */
    $("#fax").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /**
     * To change the format of fax
     */
    $("input[name='fax']").keyup(function() {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });

    /**
     * If the letter is not digit in phone number then don't type anything.
     */
    $("#phone_number,#fphone_number,#fssn").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    /**
     * To change the format of phone number
     */
    $("input[name='phone_number']").keydown(function() {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });
    $("input[name='fphone_number']").keydown(function() {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });
    $("input[name='fssn']").keydown(function() {
        $(this).val($(this).val().replace(/^(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"));
    });
    /**
     * Auto fill the city, state and country when focus loose on zipcode field.
     */
    $( "#zipcode" ).focusout(function() {
        getAddressInfoByZip($(this).val());
    });
    $( "#fzipcode" ).focusout(function() {
        getAddressInfoByZipforFinance($(this).val());
    });
    /**
     * Auto fill the city, state and country when enter key is clicked.
     */
    $( "#zipcode" ).keydown(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13') {
            getAddressInfoByZip($(this).val());
        }
    });


    /**
     * show and hide Free Plan div
     */
    $('#free_plan').change(function () {
        var free_plan = $(this).prop('checked');
        if(free_plan){
            $('.freeplan').show();
        } else {
            $('.freeplan').hide();
        }
    });

    /**
     * show and hide EFT div
     */
    $('#country_code').change(function () {
        var country = $(this).val();
        country  = $.trim(country);
        if( country == 'UnitedStates')
        {
            $('#UnitedStates').show();
            $('#Canada').hide();
        } else if(country == 'Canada') {
            $('#UnitedStates').hide();
            $('#Canada').show();
        } else {
            $('#Canada').hide();
            $('#UnitedStates').hide();
        }

    })

    /**
     * Show popup for cancel button
     */
    $('#Cancel').on('click',function () {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                var url = "/Companies";
                window.location.href = url;
            }
        });
    })

    function plan_calc()
    {
        var arr = [];
        var textValue = $('#term_plan :selected').text();
        var PlanPrice = $("#subscription_plan option:selected").attr('price');
        var AllowedUnits = $("#subscription_plan option:selected").attr('units');
        var PricePunit = $("#subscription_plan option:selected").attr('pricepunit');

        if(PricePunit)
        {
            arr = AllowedUnits.split('-');
        }
        var AllowedUnit = PricePunit * arr[1];
        var txtPlanPricewithoutFormat = parseFloat(PlanPrice) + parseFloat(AllowedUnit);
        //  var txtPlanPrice = (AddCompany.formatNumber(+PlanPrice + +AllowedUnit));
        var result;
        // console.log(textValue);

        if( $("#pay_plan_price").is(":disabled")) {

            if (textValue == "Free Trial") {
                $('#discount').val(PricePunit);
                $("#plan_price").val(AddCompany.formatNumber(0));
                $("#plan_price").val(AddCompany.formatNumber(PlanPrice));
            }
            else if (textValue == "Monthly") {
                //$('#discount').val(0);
                result = txtPlanPricewithoutFormat * 1;
                if(isNaN(result))
                {
                    result = '';
                }
                // console.log('ABC=='+result);
                if(isNaN(result))
                {
                    result = '';
                }
                $('#plan_price').val(AddCompany.formatNumber(result));
            }
            else if (textValue == "Quarterly") {
                // $('#discount').val(0);
                result = txtPlanPricewithoutFormat * 3;
                if(isNaN(result))
                {
                    result = '';
                }
                $('#plan_price').val(AddCompany.formatNumber(result));
            }
            else if (textValue == "Semi-Annual") {
                // $('#discount').val(0);
                result = txtPlanPricewithoutFormat * 6;
                if(isNaN(result))
                {
                    result = '';
                }
                $('#plan_price').val(AddCompany.formatNumber(result));
            }
            else if (textValue == "Annual") {
                $('#discount').val(AllowedUnit);
                result = txtPlanPricewithoutFormat * 12;
                if(isNaN(result))
                {
                    result = '';
                }
                $('#plan_price').val(AddCompany.formatNumber(result));
            }
            var PricePe = $('#plan_price').val().replace(/[,\s]/g, '');

            var discount =  $('#discount').val();
            var planPrice = parseInt(PricePe) - parseInt(discount);
            if(isNaN(planPrice))
            {
                planPrice = '';
            }
            if(!planPrice)
            {
                planPrice = result;
            }
            $('#pay_plan_price').val(AddCompany.formatNumber(planPrice));
        }

    }

    $('#subscription_plan,#term_plan').change(function () {
        plan_calc();
    });

    $('#discount').on('input click',function(e) {
        if(!$('#free_plan').is(':checked'))
        {
            var PricePe = $('#plan_price').val().replace(/[,\s]/g, '');
            var discount =  $('#discount').val();
            var PlanPrice = PricePe - discount;
            PlanPrice = AddCompany.formatNumber(PlanPrice);
            $('#pay_plan_price').val(PlanPrice);
        }
    });


    // changeDateFormat();



    $('#extended_days,#free_plan').on('input click,keydown',function(e){
        var remaining_days  = $('#remaining_days').attr('remaining_days') || 0;
        var left_days = $('#extended_days').val() || 0;
        if(remaining_days  && !isNaN(left_days) && !isNaN(remaining_days))
        {
            var extended_days   = parseInt(left_days) + parseInt(remaining_days);
            $('#remaining_days').val(extended_days);
            var remaining_days  = $('#remaining_days').val();
            var expiration_date  = $('#created_at').val() || new Date();
            //alert(expiration_date);
            addDays(expiration_date,remaining_days)
        }

        if($('#free_plan').is(':checked'))
        {
            $('#pay_plan_price').val('0.00').prop('disabled','disabled');
            // $('#pay_plan_price').val('0.00');
        }else
        {
            $('#pay_plan_price').removeAttr('disabled');
            plan_calc();
            var PricePe = $('#plan_price').val().replace(/[,\s]/g, '');
            var discount =  $('#discount').val();
            $('#pay_plan_price').val(PricePe - discount);
        }

    })


    $('.expiration_date_check #free_plan').on('input click',function(e){
        var expiration_date  = $('#expiration_date').val();
        if(!expiration_date)
        {
            changeDateFormat();
        }
    });

    function addDays(date, days) {
        var date = moment(date);
        var days = (days)? parseInt(days) : 0;
        var newdate = moment(date).add(days, 'days');
        var dd = (newdate.date() < 10 ? '0' : '') + newdate.date();
        var mm = ((newdate.month() + 1) < 10 ? '0' : '') + (newdate.month() + 1);
        var y = newdate.year();
        var hour = (newdate.hours() < 10 ? '0' : '') + newdate.hours();
        var minutes = (newdate.minutes() < 10 ? '0' : '') + newdate.minutes();
        var seconds = (newdate.seconds() < 10 ? '0' : '') + newdate.seconds();
        var someFormattedDate = y + '-' + mm + '-' + dd+' '+hour+':'+minutes+':'+seconds;
        $('#expiration_date').val(someFormattedDate);
        expirationFormat(someFormattedDate);

    }
    function expirationFormat(dateValue)
    {
        if(dateValue)
        {
            var dateResponse = $.ajax
            ({
                type: 'post',
                url: '/company-user-ajax',
                data: {
                    class: "CompanyUserAjax",
                    action: "ajaxDateFormatSet",
                    dateValue: dateValue
                },
                success: function (response){
                    dateResponse = $.parseJSON(response);
                    $('#expiration_date_label').text(dateResponse)
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }

    }
    function changeDateFormat() {
        var remaining_days  = $('#remaining_days').attr('remaining_days') || 0;
        var days = (remaining_days)? parseInt(remaining_days) : 0;
        var newdate = moment().add(days, 'days');

        var dd = (newdate.date() < 10 ? '0' : '') + newdate.date();
        var mm = ((newdate.month() + 1) < 10 ? '0' : '') + (newdate.month() + 1);
        var y = newdate.year();
        var hour = (newdate.hours() < 10 ? '0' : '') + newdate.hours();
        var minutes = (newdate.minutes() < 10 ? '0' : '') + newdate.minutes();
        var seconds = (newdate.seconds() < 10 ? '0' : '') + newdate.seconds();
        var someFormattedDate = y + '-' + mm + '-' + dd+' '+hour+':'+minutes+':'+seconds;


        $('#expiration_date').val(someFormattedDate);
        var expiration_date = $('#expiration_date').val() || '';
        expirationFormat(someFormattedDate);
        if(!expiration_date)
        {
            setTimeout(function () {
                $('#expiration_date').val(someFormattedDate);
                expirationFormat(someFormattedDate);
            },500)
        }
    }

    // function convertDate(inputFormat) {
    //         function pad(s) { return (s < 10) ? '0' + s : s; }
    //         var d = new Date(inputFormat);
    //         return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
    //     }

    var AddCompany = {
        //For Number Format
        formatNumber: function (number) {
            if (number != null && number != "" && number != undefined && number != 0) {
                if (number.toString().indexOf(',') > -1) {
                    while (number.search(",") >= 0) {
                        number = (number + "").replace(',', '');
                    }
                }
                var number = parseFloat(number).toFixed(2) + '';
                var x = number.split('.');
                var x1 = x[0];
                var x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }
            else {
                var number = 0;
                var number = parseFloat(number).toFixed(2) + '';
                var x = number.split('.');
                var x1 = x[0];
                var x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }
        },
        //Remove Comma Format
        removeCommas: function (str) {
            if (str != null && str != "" && str != undefined && str != 0) {
                while (str.search(",") >= 0) {
                    str = (str + "").replace(',', '');
                }
            }
            return str;
        },
    }
});

$(document).ready(function(){
    $(document).on('click','.toggle-password',function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    $('#company_name,#domain_name,#email').on('input click',function(e){
        var dom_name = $(this).val();
        var input_name = $(this).attr('name');
        var type = 'company';
        var imgCompanyAvailable = '#imgCompanyAvailable';
        var company_required = '.company_required';
        //  console.log(input_name);

        if(input_name == 'domain_name')
        {
            type = 'domain';
            var imgCompanyAvailable = '#imgDomainAvailable';
            var company_required = '.domain_required';
        } else if(input_name == 'email'){
            type = 'email';
        }
        if(dom_name && (dom_name.length >= 1))
        {
            $.ajax
            ({
                type: 'post',
                url: '/company-user-ajax',
                data: {
                    class: "CompanyUserAjax",
                    action: "CheckDomainCompanyExist",
                    company_name: dom_name,
                    type: type,
                },
                success: function (response) {
                    var response = $.parseJSON(response);
                 //   console.log(response);
                    if(response.status == 'success')
                    {
                        if(type=='company') {
                            jQuery(imgCompanyAvailable).show();
                            jQuery(company_required).hide();
                        }
                    } else {
                        if(type=='company') {
                            jQuery(imgCompanyAvailable).hide();
                            jQuery(company_required).show();
                        }
                     if(type=='domain'){
                            $('.domain_nameErr').html(response.message);
                        } else if (type=='email'){
                            $('.emailErr').html(response.message);
                        }
                    }
                },
                error: function (data) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors, function (key, value) {
                        // alert(key+value);
                        $('#' + key + '_err').text(value);
                    });
                }
            });
        }


    })

    /**
     * Reset the advance search dialog box
     */
    $('#ResetAdvanceSearchDialog,#refresh').on('click', function () {
        $('#searchform')[0].reset();
        var company_status = $('#company_status option:selected').val();

        data_table(company_status,null,null,null,null);

        table.ajax.reload();
    });

    $('#searchform').keypress(function(e){
        if ( e.which == 13 ) return false;
    });

    /**
     * If the letter is not digit in fax then don't type anything.
     */
    $("#extended_days,#routing_number,#origin,#trace_number,#originator_id_number,#cpa_code,#transit_number,#account_number").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    /***Password JS*/
    $(document).ready(function(){

        $("#phone_number").intlTelInput();
        $('.phone_num li.country').on("click",function(){
            var dial_code = $(this).attr('data-dial-code');
            $('#phone_number_prefix').val(dial_code);
        });
        $("#phone_number").on('input',function(){
            var dial_code = $('li.country.active').attr('data-dial-code');
            $('#phone_number_prefix').val(dial_code);
        });


        $("#fphone_number").intlTelInput();
        $('.fphone_num li.country').on("click",function(){
            var dial_code = $(this).attr('data-dial-code');
            $('.fphone_num #phone_number_prefix').val(dial_code);
        });
        $("#fphone_number").on('input',function(){
            var dial_code = $('li.country.active').attr('data-dial-code');
            $('#fphone_number_prefix').val(dial_code);
        });

        var country_prefix = '';
        var prefix_value = $('.phone_num #phone_number_prefix').val();
        if(prefix_value)
        {
            country_prefix =  $("[data-dial-code="+prefix_value+"]").attr('data-country-code');
            $('.phone_num .selected-flag .iti-flag').addClass(country_prefix);
        }
        var fcountry_prefix = '';
        var fprefix_value = $('.fphone_num #phone_number_prefix').val();
        if(fprefix_value)
        {
            country_prefix =  $("[data-dial-code="+fprefix_value+"]").attr('data-country-code');
            $('.fphone_num .selected-flag .iti-flag').addClass(country_prefix);
        }

    });

    localStorage.removeItem("financial_data");

    $('#addCompanyForm').on('submit',function(e){
        e.preventDefault();

        var formData = $('#addCompanyForm').serializeArray();
        var edit_id = $('#edit_id').val();
        var action;
        var freeplan=  $('#free_plan').is(":checked");

        if (freeplan)
        {
            var free_plan = 1;
        } else {
            var free_plan = 0;
        }
        var account_type = $("input[name='account_type']:checked").val();
        if(edit_id)
        {
            action = 'update';
        } else {
            action = 'insert';
        }
        $("#add_company_button").attr("disabled",true)
        $(".overlay").show();

        var financial_data = '';
        financial_data = JSON.parse(localStorage.getItem('financial_data'));

       $('#loadingmessage').show();
        toastr.clear();
        $.ajax({
            type: 'post',
            url: '/company-user-ajax',
            data: {class: 'CompanyUserAjax', action: action, form: formData,free_plan:free_plan,account_type:account_type,financial_data:financial_data},
            success : function(response){
                var response = JSON.parse(response);

                if(response.status == 'success' && response.code == 200){

                   $('#loadingmessage').hide();
                    $("#add_company_button").attr("disabled",false);
                    toastr.success('Mail sent successfully.');
                    localStorage.removeItem("financial_data");
                    localStorage.setItem("rowcolor_new",true)
                    window.location.href= '/Companies/List';
                } else if(response.status == 'error' && response.code == 400){

                    if(response.message == 'online payment error')
                    {
                        toastr.error('Please fill all online payment required fields.');
                    } else if(response.message == 'Domain already exist!'){
                        toastr.error('Domain Name not available');
                    }else {
                        toastr.error('Please fill all required fields.');
                    }
                   $('#loadingmessage').hide();
                    $('.error').html('');
                    $.each(response.data, function (key, value) {
                        if (value == '* Minimum 9 characters allowed') {
                            $('.' + key).html('* Minimum 9 characters allowed');
                            if(value == '*Minimum 10 characters allowed')
                            {
                                $('.' + key).html('* Minimum 9 characters allowed');
                            }
                        } else  if (value == '* Minimum 15 characters allowed') {
                            $('.' + key).html('* Minimum 15 characters allowed');
                        } else {

                            if (value == "Domain already exist!") {
                                $('.' + key).html('Domain Name not available');
                            } else {
                                $('.' + key).html('* This field is required');
                            }
                        }
                    });

                }
            },
            error: function (response) {
                // console.log(response);
            }
        });
    });
    // To show Accordian Open on load
    $('#collapseOne').collapse({'toggle': false});
    $('#collapseTwo').collapse({'toggle': false});

    //Fetch default Currency List
    var edit_id = $('#edit_id').val();
    var view_id = $('#view_id').val();

    if(view_id)
    {
        edit_id = view_id;
    }
    var page = $('#page').val();
    $.ajax
    ({
        type: 'post',
        url: '/company-user-ajax',
        data: {
            class: "CompanyUserAjax",
            action: "getOnLoadCompanyUserData",
            id:edit_id
        },
        success: function (response) {
            var response = $.parseJSON(response);
            var default_currency_data = response['data']['default_currency'];
            var plan_data = response['data']['plans'];
            var company_data = response['data']['company_data'];

            //Adds Lists in Default Currency list
            var default_currency_html = '<option value="">Select</option>';
            $(default_currency_data).each(function(key, $value){
                var select=($value['id'] == 175)?'selected':'';
                default_currency_html += "<option "+ select +" value="+$value['id']+" symbol="+$value['symbol']+"  $selected_currency>"+$value['currency']+"("+$value['symbol']+")</option>";
            });
            $('#default_currency').html(default_currency_html);

            //Adds Lists in Plan list
            var plan_html = '<option value="">Select</option>';
            $(plan_data).each(function(key, $value){
                plan_html += "<option value="+$value['id']+"  units="+$value['number_of_units']+" pricepunit='1' price='75'  >"+$value['plan_name']+" </option>";
            });
            $('#subscription_plan').html(plan_html);
          //  $('#term_plan').val(company_data.term_plan);
            if(company_data)
            {

                var abc = {};
                var main_v;
                jQuery('.country').each(function(){
                    var key = jQuery(this).attr("data-dial-code");
                    var value = jQuery(this).attr("data-country-code");
                    if(key == company_data['phone_number_prefix'])
                    {
                        $("#phone_number").intlTelInput(
                            "setCountry",value
                        );
                    }
                });

                //console.log(company_data['phone_number_prefix']);
                $.each(company_data, function (key, value) {
                    console.log(key);
                    if(key == 'expiration_date_label')
                    {
                        $('#' + key).text(value);
                    }
                    else {
                        $('#' + key).val(value);
                        if(key=='expiration_date') {
                            $('.expires_on').html(value);
                        }
                    }


                    if(view_id)
                    {
                        if(key == 'free_plan') {
                            if (value == 1){
                                $('.plan_type' ).text('Free Trail');
                            } else {
                                $('.plan_type' ).text('Paid');
                            }
                        }
                        if(key != 'subscription_plan') {
                            $('.' + key).text(value);
                        } else {
                            $.each(plan_data, function (key1, value1) {
                                if (value == value1['id']){
                                    $('.' + key).text(value1['plan_name']);
                                }
                            });
                        }

                    }
                });

                if(company_data.free_plan == '0')
                {
                    $('.extended_days').text('0');
                  //  var RemainingDays = company_data.expiration_date -
                    $('#RemainingDays').text('0');
                }
                $('#mi').val(company_data.middle_name);
                $('#password').val(company_data.actual_password);
                $('#password_confirmation').val(company_data.actual_password);
                //debugger
                var plan_p = company_data.plan_price;
                plan_p = plan_p.replace(/,\s?/g, "");
                plan_p = (plan_p)?plan_p:0;

                var discount_p = company_data.discount;
                discount_p = discount_p.replace(/,\s?/g, "");
                discount_p = (discount_p)?discount_p:0;

                var total_plan_price = parseFloat(plan_p) -  parseFloat(discount_p);
                total_plan_price = total_plan_price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                $('#pay_plan_price').val( total_plan_price );

                if(company_data.account_type == '0')
                {
                    $("#account_type_cash").prop('checked', false);
                    $("#account_type_accural").prop('checked', true);
                }
                if(company_data.account_type == '1')
                {
                    $("#account_type_cash").prop('checked', true);
                    $("#account_type_accural").prop('checked', false);
                }
                if(company_data.free_plan == '1')
                {
                    $("#free_plan").prop('checked', true);
                    $('.freeplan').show();
                } else {
                    $('.freeplan').hide();
                }
                $('#country_code').val(company_data.eft_country);
                if(company_data.eft_country == 'UnitedStates')
                {
                    $('#UnitedStates').show();
                    $('#Canada').hide();
                } else {
                    $('#UnitedStates').hide();
                }

                if(company_data.eft_country == 'Canada')
                {
                    $('#Canada').show();
                    $('#UnitedStates').hide();
                } else {
                    $('#Canada').hide();
                }

            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });

    $(document).ready(function(){
        $('input').on('input',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).siblings('.error').html('');
            }
        });
        $('#phone_number').on('input',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).parents('.intl-tel-input').siblings('.error').html('');
            }
        });
        $('select').on('change',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).siblings('.error').html('');
            }
        });
        $('#fphone_number').on('input',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).parents('.intl-tel-input').siblings('.error').html('');
            }
        });
        $('#fbirth_date').on('change',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).siblings('.error').html('');
            }
        });
        $('#faddress').on('input',function(){
            var textb = $(this).val();
            if(textb.length){
                $(this).siblings('.error').html('');
            }
        });

    });


})
$( function() {
    $( "#fbirth_date" ).datepicker({  maxDate: new Date(),  changeMonth: true,
        changeYear: true });
} );

function goBack() {
    window.location.href='/Companies/List';
    //window.history.back();
}

$(document).on('click','#closePop',function(e){
    e.preventDefault();
    // $('#financialInfo')[0].reset();
    $('#financial-info').modal('toggle');
});

$(document).on('click','#online_payments',function(e){
    e.preventDefault();
    var first_name = $('#first_name').val();
    var last_name = $('#last_name').val();
    var phone_number = $('#phone_number').val();
    var birth_date = $('#birth_date').val();
    var zipcode = $('#zipcode').val();
    var city = $('#city').val();
    var state = $('#state').val();
    var email = $('#email').val();
    var phone_prefix = $('input#phone_number_prefix').val();
    var address1 = $('#address1').val();
    var address2 = $('#address2').val();
    $('.fphone_num input#phone_number_prefix').val(phone_prefix);
    $('.fphone_num li.country').each(function (key,value) {
        if($(this).attr('data-dial-code') == phone_prefix)
        {
            var countrycode=$(this).attr('data-country-code');
            $("#fphone_number").intlTelInput(
                "setCountry",countrycode
            );
        }
    });

    $('#ffirst_name').val(first_name);
    $('#flast_name').val(last_name);
    $('#fphone_number').val(phone_number);
    $('#fbirth_date').val(birth_date);
    $('#fzipcode').val(zipcode);
    $('#fcity').val(city);
    $('#fstate').val(state);
    $('#femail').val(email);
    $('#faddress').val(address1);
    $('#faddress2').val(address2);



    $('#financial-info').modal('toggle');
});

$(document).ready(function(){
    $(document).on('click','#savefinancial',function (e) {
        e.preventDefault();
        var financial_data = $( "#financialInfo" ).serializeArray();
        console.log(financial_data);
        $.ajax
        ({
            type: 'post',
            url: '/company-user-ajax',
            data: {
                class: "CompanyUserAjax",
                action: "validateFinancialData",
                data:financial_data
            },
            success: function (response) {
                var response = $.parseJSON(response);
                if(response.code == 400)
                {
                    $.each(response.data, function (key) {
                        $('.' + key).text('* This field is required');
                    });
                }else{
                    localStorage.setItem('financial_data',JSON.stringify(financial_data));
                    $('#financial-info').modal('toggle');
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    $('.' + key + 'Err').text(value);
                });
            }
        });


    });
});
$(document).ready(function () {
    $(document).on("click", "#cancel_company", function (e) {
        bootbox.confirm("Do you want to cancel this action now?", function (result) {
            if (result == true) {
                window.location.href='/Companies/List';
            }
        });
    });
});
$(document).on('blur','#domain_name',function () {
    var re = /[^a-zA-Z0-9\-]/;
    var val = $(this).val();
    if(re.test(val)) {
        $(this).val('');
        $('#imgDomainAvailable').hide();
        $('.domain_nameErr').text('Domain name is invalid.');
    }
    if(val.length <= 0)
    {
        $('#imgDomainAvailable').hide();
    }
});

$(document).on('click','.clearForm',function () {
    bootbox.confirm("Do you want to clear this form?", function (result) {
        if (result == true) {
            location.reload();
         //   $('#addCompanyForm')[0].reset();
        }
    });

});