jqGrid('All');
// alert("zsgcfjhzx");
function jqGrid(status) {

    var table = 'communication_email';
    var columns = ['Name','To', 'User','Company','Subject','Date','Action'];
    var select_column = ['Delete','Open'];
    var joins = [{table:'communication_email',column:'email_to',primary:'email',on_table:'users'}];
    //var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['communication_email.updated_at','communication_email.created_at'];
    var extra_where = [{column: 'type', value: 'G', condition: '='},{column: 'gtype', value: 'E', condition: '='}];
    var status=2;
    var columns_options = [
        {name:'Name',index:'user_type', width:90,align:"center",searchoptions: {sopt: conditions},table:table, classes: 'pointer',formatter: userType},
        {name:'To',index:'email_to', align:"center", width:100,searchoptions: {sopt: conditions},table:table, classes: 'pointer',formatter: visiblityChecker},
        {name:'User',index:'user_type', width:80, align:"center",table:table,classes: 'pointer',formatter: userType},
        {name:'Company',index:'company_name', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        {name:'Subject',index:'email_subject', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        {name:'Date',index:'created_at', width:80, align:"center",table:table},
        {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},
        // {name:'parent',hidden:true,index:'email_parent_id', width:80, align:"center",table:table},
    ];
    var ignore_array = [];
    jQuery("#communication-draft-mail-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where,
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 10,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Drafted Group Emails",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false}
    );
}

jqGrid2('All');

function jqGrid2(status) {
    var table = 'communication_email';
    var columns = ['RecipientName','To', 'User','Company','Message', 'Date','Action'];
    var select_column = ['Delete','Open'];
    var joins = [{table:'communication_email',column:'email_to',primary:'email',on_table:'users'}];
    //var joins = [];
    var conditions = ["eq","bw","ew","cn","in"];
    var extra_columns = ['communication_email.updated_at','communication_email.created_at'];
    var extra_where = [{column: 'type', value: 'G', condition: '='},{column: 'gtype', value: 'T', condition: '='}];
    var status=2;
    var columns_options = [
        {name:'RecipientName',index:'user_type', width:90,align:"center",searchoptions: {sopt: conditions},table:table, classes: 'pointer',formatter: userType},
        {name:'To',index:'email_to', align:"center", width:100,searchoptions: {sopt: conditions},table:table, classes: 'pointer',formatter: visiblityChecker},
        {name:'User',index:'user_type', width:80, align:"center",table:table,classes: 'pointer',formatter: userType},
        {name:'Company',index:'company_name', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        {name:'Message',index:'email_subject', width:80, align:"center",searchoptions: {sopt: conditions},table:table,classes: 'pointer'},
        // {name:'parent',hidden:true,index:'email_parent_id', width:80, align:"center",table:table},
        {name:'Date',index:'created_at', width:80, align:"center",table:table},
        {name:'Action',index:'select', title: false, width:80,align:"right",sortable:false,cellEdit: true, cellsubmit: 'clientArray', editable: true, formatter: 'select', edittype: 'select',search:false,table:table},

    ];
    var ignore_array = [];
    jQuery("#communication-draft-mesg-table").jqGrid({
        url: '/Companies/List/jqgrid',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: columns,
        colModel: columns_options,
        pager: true,
        mtype: "POST",
        postData: {
            q: 1,
            class: 'jqGrid',
            action: "listing_ajax",
            table: table,
            select: select_column,
            columns_options: columns_options,
            status: status,
            ignore:ignore_array,
            joins:joins,
            extra_columns:extra_columns,
            extra_where:extra_where,
        },
        viewrecords: true,
        sortname: 'updated_at',
        sortorder: "desc",
        sorttype:'date',
        sortIconsBeforeText: true,
        headertitles: true,
        rowNum: 10,
        rowList: [5, 10, 20, 30, 50, 100, 200],
        caption: "List of Draft Group Text Messages",
        pginput: true,
        pgbuttons: true,
        navOptions: {
            edit: false,
            add: false,
            del: false,
            search: true,
            filterable: true,
            refreshtext: "Refresh",
            reloadGridOptions: {fromServer: true}
        }
    }).jqGrid("navGrid",
        {
            edit:false,add:false,del:false,search:true,reloadGridOptions: {fromServer: true}
        },
        {}, // edit options
        {}, // add options
        {}, //del options
        {top:10,left:400,drag:true,resize:false}
    );
}

function visiblityChecker(cellValue, options, rowObject) {
    if (rowObject !== undefined) {
        var fieldValue = rowObject.parent;

        if(fieldValue)
        {
            return '';
        } else {
            return cellValue;
        }

    }
}

function userType(cellValue, options, rowObject) {
    if(rowObject !== undefined) {
        switch (cellValue) {
            case '2':
                user = "Tenant";
                break;
            case '4':
                user = "Owner";
                break;
            case '3':
                user = "Vendor";
                break;
            case '5':
                user = "Other Contacts";
                break;
            case '6':
                user = "Guest Card";
                break;
            case '7':
                user = "Rental Application";
            case '8':
                user = "Employee";
                break;
            default:
                user = "";


        }
        return user;
    }

}


jQuery(document).on('change','.select_options',function () {
    var select_options = $(this).val();
    var data_id = $(this).attr('data_id');
    if(select_options == 'Delete') {
        bootbox.confirm({
            message: "Do you want to delete this record ?",
            buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: "/Communication/GroupMessageAjax",
                        method: 'post',
                        data: {
                            class: "groupMessageAjax",
                            action: "delete",
                            'id': data_id,
                        },
                        success : function(response){
                            var response =  JSON.parse(response);
                            if(response.status == 'success' && response.code == 200) {
                                toastr.success(response.message);
                                $('#communication-draft-mail-table').trigger('reloadGrid');
                            } else if(response.status == 'error' && response.code == 503) {
                                toastr.error(response.message);
                            }else {
                                toastr.warning('Record not updated due to technical issue.');
                            }
                        }
                    });
                }
            }
        });
    } else if(select_options == 'Open')
    {
        localStorage.setItem('composer_mail_id',data_id);
        localStorage.setItem('composer_mail_type','draft');
        window.location.href='/Communication/AddGroupMessage'
    }
    $('.select_options').prop('selectedIndex',0);
});

$('.text_tab').hide();

$(document).on('click','.drafted_mail',function () {
    $('.email_tab').show();
    $('.text_tab').hide();
});

$(document).on('click','.drafted_message',function () {
    $('.email_tab').hide();
    $('.text_tab').show();
});

var gEmailTabSelecttion = localStorage.getItem('gEmailTabSelecttion');
if(gEmailTabSelecttion)
{
    $('.email_tab').hide();
    $('.text_tab').show();
    localStorage.removeItem('gEmailTabSelecttion');
}

getView();

function getView()
{
    var compose_mail_id = localStorage.getItem("composer_mail_id");
    var composer_mail_type = localStorage.getItem("composer_mail_type");

    $.ajax({
        url:'/Communication/ComposeMailAjax',
        method: 'post',
        data: {
            class: "PropertyUnitAjax",
            action: "getComposeForView",
            'id': compose_mail_id,
        },
        success: function (data) {
            localStorage.removeItem('composer_mail_id');
            localStorage.removeItem('composer_mail_type');
            info =  JSON.parse(data);
            if(info.status=="success"){
                var res = info.data;
                if(composer_mail_type == 'draft' || composer_mail_type == 'forward')
                {

                    if(res.gtype == 'E')
                    {
                        $('.selectType').val('1');
                        $('.email_container').show();
                        $('.email_container textarea,.email_container .subject').attr('disabled',false);
                        $('.text_container textarea').attr('disabled',true);
                    }
                    if(res.gtype == 'T')
                    {
                        $('.selectType').val('2');
                        $('.text_container').show();
                        $('.text_container textarea').attr('disabled',false);
                        $('.email_container textarea,.email_container .subject').attr('disabled',true);
                    }
                    if(res.selected_user_type != 'All')
                    {
                        $('.selectSubRecepient').show();
                        $('.selectRecepient').val(res.selected_user_type);
                    } else {
                        $('.selectRecepient').val('all');

                    }


                }
                if(composer_mail_type == 'draft')
                {
                    $('#compose_mail_id').val(res.id);
                }

                if(composer_mail_type == 'forward')
                {
                    localStorage.removeItem('composer_mail_id')
                }

                $('.subject').val(res.email_subject);
                $('.summernote').summernote('code', res.email_message);

                $('textarea[name="tmesgbody"]').text(res.email_message);


                var mail_to = res.user_name;
                if(mail_to)
                {
                    mail_to = mail_to.split(',');
                    selectDropdownRecepient(res.selected_user_type,mail_to);
                }


            }
        },

    });
}
