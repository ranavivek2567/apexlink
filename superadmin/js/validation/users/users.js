

$(document).ready(function () {

    /*To Reset form */
    $(document).on("click", "#cancel", function () {
        $("#login")[0].reset();
        var validator = $("#login").validate();
        validator.resetForm();
    });
    /*To Reset form */
    $(document).on("click", "#forgot_cancel,#reset_cancel", function () {
        window.location.href = '/';
    });
});
//login validation
$("#login").validate({
    rules: {
        email: {
            required: true,
            email: true,
            maxlength: 50
        },
        password: {
            required: true,
        }
    },
    submitHandler: function (form) {
        var email = $("#email").val();
        var pass = $("#password").val();
        if($("#remember").is(':checked'))
            var remember = 'on';  // checked
        else
            var remember = '';  // unchecked

        $.ajax
        ({
            type: 'post',
            url: '/user-ajax',
            data: {
                class: "UserAjax",
                action: "login",
                email: email,
                password: pass,
                remember: remember
            },
            success: function (response) {
                var data = $.parseJSON(response);

                if (data.status == "success")
                {
                    if(data.data.last_user_url){
                        window.location.href = data.data.last_user_url ;
                    } else {
                        window.location.href = "/Dashboard/Dashboard" ;
                    }
                } else
                {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }
});
//forgot password validation
$("#forgot_password").validate({
    rules: {
        email: {
            required: true,
            email: true,
            maxlength: 50
        }
    },
    messages: {
        email: {
            email: "* Invalid email address",
        }
    },
    submitHandler: function (form) {
        var email = $("#email").val();
        $.ajax
        ({
            type: 'post',
            url: '/User/forgotPassword',
            data: {
                class: "UserAjax",
                action: "forgotPassword",
                email: email
            },
            success: function (response) {
                var data = $.parseJSON(response);
                if(data.status == 'success') {
                    var to = [];
                    to.push(data[0].email);
                    var subject = 'Forgot Password';
                    var message = data[0].message;
                    $.ajax
                    ({
                        type: 'post',
                        url: 'https://cors-anywhere.herokuapp.com/http://rentconnect.net/apex-email/index.php/api/email',
                        data: {
                            action: "SendMailPhp",
                            to: to,
                            subject: subject,
                            portal: 1,
                            message: message,
                        },
                        success: function (response) {
                            var data = $.parseJSON(response);
                            if (data.status == "success") {
                                $('.login-inner').hide();
                                $('#click_here_login_link').show();
                                toastr.success('The Email was sent successfully.');
                            } else {
                                toastr.error(data.message);
                            }
                        },
                        error: function (data) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                                // alert(key+value);
                                $('#' + key + '_err').text(value);
                            });
                        }
                    });
                } else {
                    toastr.warning(data.message);
                }

            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



});

$.validator.addMethod("password_regex", function(value, element) {
    var password_regex = this.optional(element) || /^(?=.*[a-zA-Z])(?=.*[0-9])/.test(value);
    return password_regex;
}, "* Atleast one aphanumeric and a numeric character required");

//reset password validation
$("#reset_password").validate({
    rules: {
        new_password: {
            required: true,
            minlength: 8,
            password_regex : true
        },
        confirm_password: {
            required: true,
            minlength: 8,
            password_regex : true,
            equalTo: "#new_password"
        },
    },
    messages: {
        new_password: {
            minlength: "* Minimum 8 characters allowed",
        },
        confirm_password: {
            minlength: "* Minimum 8 characters allowed",
            equalTo:  "* Fields do not match",
        },
    },
    submitHandler: function (form) {
        var email = $("#email").val();
        var password = $("#new_password").val();
        var forgot_password_token = $("#forgot_password_token").val();
        $.ajax
        ({
            type: 'post',
            url: '/User/resetPassword',
            data: {
                class: "UserAjax",
                action: "resetPassword",
                email: email,
                password: password,
                forgot_password_token: forgot_password_token
            },
            success: function (response) {
                var data = $.parseJSON(response);
                console.log('data>>>', data);
                if (data.status == "success")
                {
                    window.location.href = "/";
                    toastr.success(data.message);

                } else
                {
                    toastr.error(data.message);
                }
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors, function (key, value) {
                    // alert(key+value);
                    $('#' + key + '_err').text(value);
                });
            }
        });
    }



});

//Logout validation
$(document).on("click", "#logout", function () {
    // alert('asasasa');
    //  debugger
    $.ajax
    ({
        type: 'post',
        url: '/user-ajax',
        data: {
            class: "UserAjax",
            action: "logout",
            data :window.location.pathname
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success")
            {
                window.location.href = "/";
            } else
            {
                toastr.error(data.message);
            }
        },
        error: function (data) {
            var errors = $.parseJSON(data.responseText);
            $.each(errors, function (key, value) {
                // alert(key+value);
                $('#' + key + '_err').text(value);
            });
        }
    });
});
