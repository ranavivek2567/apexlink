/**
 *Get location on the basis of zipcode
 */
function getAddressInfoByZip(zip){
    if(zip.length >= 5 && typeof google != 'undefined'){
        var addr = {};
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': zip }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK){
                if (results.length >= 1) {
                    for (var ii = 0; ii < results[0].address_components.length; ii++){
                        //var street_number = route = street = city = state = zipcode = country = formatted_address = '';
                        var types = results[0].address_components[ii].types.join(",");
                        if (types == "street_number"){
                            addr.street_number = results[0].address_components[ii].long_name;
                        }
                        if (types == "route" || types == "point_of_interest,establishment"){
                            addr.route = results[0].address_components[ii].long_name;
                        }
                        if (types == "sublocality,political" || types == "locality,political" || types == "neighborhood,political" || types == "administrative_area_level_2,political"){
                            addr.city =  results[0].address_components[ii].short_name ;
                        }
                        if (types == "administrative_area_level_1,political"){
                            addr.state = results[0].address_components[ii].short_name;
                        }
                        if (types == "postal_code" || types == "postal_code_prefix,postal_code"){
                            addr.zipcode = results[0].address_components[ii].long_name;
                        }
                        if (types == "country,political"){
                            addr.country = results[0].address_components[ii].long_name;
                        }
                    }
                    addr.success = true;
                    /* for (name in addr){
                         console.log('### google maps api ### ' + name + ': ' + addr[name] );
                     }*/
                    response(addr);
                } else {
                    response({success:false});
                }
            } else {
                response({success:false});
            }
        });
    } else {
        response({success:false});
    }
}

/**
 * Set values in the city, state and country when focus loose from zipcode field.
 */
function response(obj){
    //console.log(obj);
    if(obj.success){
        $('#city').val(obj.city);
        $('#state').val(obj.state);
        $('#country').val(obj.country);
    } else {
        $('#city').val('');
        $('#state').val('');
        $('#country').val('');
    }
}


$(document).on('focusout','#zip_code',function(){
    getAddressInfoByZip($(this).val());
});

$("#zipcode").keydown(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        getAddressInfoByZip($(this).val());
    }
});


$(document).ready(function(){
    var currentPagePath = window.location.pathname;
    //show if data already exists in default settings
    $.ajax({
        type: 'post',
        url: '/settings-ajax',
        data: {
            class: "DefaultSettingsAjax",
            action: "viewSettings"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success"){
                var result = data.data;

                $.each(result, function (key, value) {
                    $('#'+key).val(value);
                });
            }
        },
        error: function (data) {
        }
    });

    //show if data already exists in clock swttings
    $.ajax({
        type: 'post',
        url: '/settings-ajax',
        data: {
            class: "DefaultSettingsAjax",
            action: "viewClockSettings"
        },
        success: function (response) {
            var data = $.parseJSON(response);
            if (data.status == "success"){
                var result = data.data;
                $.each(result, function (key, value) {

                    if (key == 'default_date_format') {
                        var html = '';
                        if (value == 1) {
                            html += '<option value="1">MM/DD/YYYY (Day)</option>';
                            html += '<option value="2">MM-DD-YYYY (Day)</option>';
                            html += '<option value="3">MM.DD.YYYY (Day)</option>';
                            html += '<option value="13">MONTH DD, YYYY (Day)</option>';
                        }else if(value == 2){
                            html += '<option value="4">DD/MM/YYYY (Day)</option>';
                            html += '<option value="5">DD-MM-YYYY (Day)</option>';
                            html += '<option value="6">DD.MM.YYYY (Day)</option>';
                            html += '<option value="14">DD MONTH, YYYY (Day)</option>';
                        }else if(value == 3){
                            html += '<option value="7">YYYY/MM/DD (Day)</option>';
                            html += '<option value="8">YYYY-MM-DD (Day)</option>';
                            html += '<option value="9">YYYY.MM.DD (Day)</option>';
                        }else{
                            html += '<option value="10">YYYY/DD/MM (Day)</option>';
                            html += '<option value="11">YYYY-DD-MM (Day)</option>';
                            html += '<option value="12">YYYY.DD.MM (Day)</option>';
                        }
                        $("#date_format").html(html);
                    }
                    if ( key == 'date_format' ) {
                        $('#date_format [value="'+value+'"]').attr('selected', 'true');
                    }
                    if (key == 'default_clock_format') {
                        if(value == "12"){
                            $(".time-format").text("12 Hour Clock 08:10:21");
                        }else{
                            $(".time-format").text("24 Hour Clock 17:45:55");
                        }
                    }

                    $('#'+key).val(value);
                });
                $('#c_id').val(result.id);
            }
        },
        error: function (data) {
        }
    });

    //get country code
    /*$.ajax({
        type:'post',
        url:'/helper-ajax',
        data:{class:'HelperAjax',action:"getCountryCode"},
        success:function(response){
            var data = JSON.parse(response);
            var html = '';
            $.each(data.data, function (key, value) {
                html += '<option value='+value.id+'>'+value.name+'('+value.code+')</option>';
            });
            $('#countryCode').append(html);
        },
        error:function(data){
            console.log(data);
        }
    });*/

    var getLocalStorageVaue = localStorage.getItem('superadmin_admin_nav');
    toggleSidebarMenu(getLocalStorageVaue);
    localStorage.setItem('superadmin_admin_nav',0);
    $(document).on("click","#leftnav2 .sub-item",function(){
        localStorage.setItem('superadmin_admin_nav',1);
    });



    //Ajax to create a settings
    $("#settingsForm").submit(function( event ) {
        event.preventDefault();
        if ($('#settingsForm').valid()) {
            var formData = $('#settingsForm').serializeArray();
            $.ajax({
                type: 'post',
                url: '/settings-ajax',
                data: {class: 'DefaultSettingsAjax', action: "addSettings", form: formData},
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success'){
                        toastr.success(response.message);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }
    });
    //Ajax to create a clock settings
    $("#clocksettingsForm").submit(function( event ) {
        event.preventDefault();
        if ($('#clocksettingsForm').valid()) {
            var formData = $('#clocksettingsForm').serializeArray();
            $.ajax({
                type: 'post',
                url: '/settings-ajax',
                data: {class: 'DefaultSettingsAjax', action: "addClockSettings", form: formData},
                success: function (response) {
                    var response = JSON.parse(response);
                    if(response.status == 'success'){
                         toastr.success(response.message);
                         $('#formatted_date').text(response.data.formatted_date);
                    } else if(response.status == 'error' && response.code == 400){
                        $('.error').html('');
                        $.each(response.data, function (key, value) {
                            $('.'+key).html(value);
                        });
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }


    });




    //default clock settings
    /* change time format on clock toggle */
    jQuery(document).on("change",".dclock_format",function(e) {
        e.preventDefault();
        var clockVal = jQuery(this).val();
        if(clockVal == "12"){
            jQuery(".time-format").val("12 Hour Clock 08:10:21");
        }else{
            jQuery(".time-format").val("24 Hour Clock 17:45:55");
        }
    });

    /* Phone number format setting*/
    jQuery('input[name="phone_number"]').mask('000-000-0000', {reverse: true});

    /* Added default date and clock variation based on default date format */
    $('#date_format option').each(function() {
        if ($.inArray( parseInt($(this).attr('value')),[1,2,3,13] ) > -1) {
            $(this).show();
        }else{
            $(this).hide();
        }

    });
    $(document).on("change","#default_date_format", function(){
        var html = '';
        if ($(this).val() == 1) {
            html += '<option value="1">MM/DD/YYYY (Day)</option>';
            html += '<option value="2">MM-DD-YYYY (Day)</option>';
            html += '<option value="3">MM.DD.YYYY (Day)</option>';
            html += '<option value="13">MONTH DD, YYYY (Day)</option>';
        }else if($(this).val() == 2){
            html += '<option value="4">DD/MM/YYYY (Day)</option>';
            html += '<option value="5">DD-MM-YYYY (Day)</option>';
            html += '<option value="6">DD.MM.YYYY (Day)</option>';
            html += '<option value="14">DD MONTH, YYYY (Day)</option>';
        }else if($(this).val() == 3){
            html += '<option value="7">YYYY/MM/DD (Day)</option>';
            html += '<option value="8">YYYY-MM-DD (Day)</option>';
            html += '<option value="9">YYYY.MM.DD (Day)</option>';
        }else{
            html += '<option value="10">YYYY/DD/MM (Day)</option>';
            html += '<option value="11">YYYY-DD-MM (Day)</option>';
            html += '<option value="12">YYYY.DD.MM (Day)</option>';
        }
        $("#date_format").html(html);

    });

    $(document).on("change","#default_clock_format", function(){
        if ($(this).val() == 12 ) {
            $(".time-format").text("12 Hour Clock 08:10:21");
        }else{
            $(".time-format").text("24 Hour Clock  17:21:45");
        }
    });

    /* cancel button event */
    $(document).on("click",".yes-cancel",function(){
        window.location.href = "/Dashboard/Dashboard";
    });

    function toggleSidebarMenu(value){
        console.log(value);
        if (value == 1 ) {
            $(".default-sidebar").removeClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",true);
            $("#leftnav2").addClass("in");
            $("#leftnav2").removeAttr("style");
        }else{
            $(".default-sidebar").addClass("collapsed");
            $(".default-sidebar, #leftnav2").attr("aria-expanded",false);
            $("#leftnav2").removeClass("in");
            $("#leftnav2").css("height","0px");
        }
    }

});

//add settings form client side validations
$("#settingsForm").validate({
    rules: {
        company_name: {
            maxlength: 30,
            required:true
        },
        timeout: {
            required: true
        },
        zip_code: {
            maxlength: 30,
            number:true
        },
        address1: {
            required: true,
            maxlength: 255
        }
    }
});

//add clock settings form client side validations
$("#clocksettingsForm").validate({
    rules: {
        default_date_format: {
            required:true
        },
        date_format: {
            required: true
        },
        default_clock_format: {
            required: true

        }
    }
});

$(document).on('click','.cancel_action',function(){
    bootbox.confirm({
        message: "Do you want to cancel this action now?",
        buttons: {confirm: {label: 'Yes'}, cancel: {label: 'No'}},
        callback: function (result) {
            if (result == true) {
                window.location.href = '/Dashboard/Dashboard';
            }
        }
    });
});

$(document).on('click','.ResetForm',function() {
location.reload();

});

$(document).on('click','.ResetFormdateClock',function(){
    $('#clocksettingsForm')[0].reset();
});


$(document).on('click','.clearForm',function () {
    bootbox.confirm("Do you want to clear this message?", function (result) {
        if (result == true) {
            $("#composeEmail .form-control").val('');
            $('.note-editable').text('');
            // $("#composeEmail .note-editable p").text('');
           /* $("#composeEmail .subject").val('');*/
            //  resetFormClear('#sendEmail',[''],'form',true);

        }
    });

});